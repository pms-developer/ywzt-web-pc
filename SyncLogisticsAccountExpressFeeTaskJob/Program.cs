﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SyncLogisticsAccountExpressFeeTaskJob
{
    internal class Program
    {
        private static MeioErpLogisticsPackageAccountIBLL logisticsPackageAccountIBLL = new MeioErpLogisticsPackageAccountBLL();
        private static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            try
            {
                log.Info($"\r\n{DateTime.Now}：开始同步快递账单运单号");
                //推送仓库租金账单
                logisticsPackageAccountIBLL.SyncExpressFee();
                log.Info($"\r\n{DateTime.Now}：结束同步快递账单运单号");
            }
            catch (Exception ex)
            {
                log.Error($"\r\n{DateTime.Now}：{ex.Message}");
            }
        }
    }
}
