﻿using Autofac; 


namespace DbContext
{
   
    public static partial class ServiceCollectionExtension
    {
        public static void ConfigureDbContext(this ContainerBuilder builder, 
            string mysqlConnectionstring, string oralceConnectionstring)
        {
            builder.RegisterType<DefaultDbCollection>().As<IDefaultDbCollection>().SingleInstance();
            builder.RegisterType<OralceDbCollection>().As<IOralceDbCollection>().SingleInstance();
            builder.RegisterType<DefaultDbContext>().As<IDefaultDbContext>().WithParameter("queryDbString", mysqlConnectionstring).WithParameter("commandDbString", mysqlConnectionstring).SingleInstance();
            builder.RegisterType<OracleDbContext>().As<IOracleDbContext>().WithParameter("queryDbString", oralceConnectionstring).WithParameter("commandDbString", oralceConnectionstring).SingleInstance();
        }
    }
}
