﻿
using Oracle.ManagedDataAccess.Client;
using System.Data;

using System.Diagnostics.CodeAnalysis;

namespace DbContext
{
    [ExcludeFromCodeCoverage]
    public class OracleDbContext : IOracleDbContext
    {
        private readonly string _queryDbString;
        private readonly string _commandDbString;

        public OracleDbContext(string queryDbString, string commandDbString)
        {
            _queryDbString = queryDbString;
            _commandDbString = commandDbString;
        }

     
        public IDbConnection QueryDbConnection => new OracleConnection(_queryDbString);

     
        public IDbConnection CommandDbConnection => new OracleConnection(_commandDbString);

    }   
}
