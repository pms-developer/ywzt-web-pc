﻿using Dapper;
using sdmap.ext;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace DbContext
{
    [ExcludeFromCodeCoverage]
    public class DefaultDbCollection : IDefaultDbCollection
    {
        private readonly SdmapContext _ctx;
        private readonly IDefaultDbContext _dbContext;
        public DefaultDbCollection(SdmapContext ctx, IDefaultDbContext dbContext)
        {
            _ctx = ctx;
            _dbContext = dbContext;
        }
        public async Task<T> QueryFirstOrDefaultAsync<T>(string statementId, object parameters = null)
        {
#if DEBUG
            Trace.TraceInformation("SDMapQuery: \r\n -->executing command: {0}  ", _ctx.Emit(statementId, parameters).Replace("\n", "").Replace("\r", ""));
#endif
            return await _ctx.QueryFirstOrDefaultAsync<T>(_dbContext.QueryDbConnection, statementId, parameters);
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string statementId, object parameters = null)
        {
#if DEBUG
            Trace.TraceInformation("SDMapQuery: \r\n -->executing command: {0}  ", _ctx.Emit(statementId, parameters).Replace("\n", "").Replace("\r", ""));
#endif
            return await _ctx.QueryAsync<T>(_dbContext.QueryDbConnection, statementId, parameters);
        }
        public async Task<int> ExecuteAsync(string statementId, object parameters = null)
        {
         
            return await _ctx.ExecuteAsync(_dbContext.CommandDbConnection, statementId, parameters);
        }
        public async Task<T> ExecuteScalarAsync<T>(string statementId, object parameters = null)
        {
            return await _ctx.ExecuteScalarAsync<T>(_dbContext.CommandDbConnection, statementId, parameters);
        }

        public async Task ExecuteSql(string sql)
        {
           await _dbContext.CommandDbConnection.ExecuteAsync(sql, commandTimeout:7200);
        }
       
    }
    public interface IDefaultDbCollection
    {
        Task<T> QueryFirstOrDefaultAsync<T>(string statementId, object parameters = null);
        Task<IEnumerable<T>> QueryAsync<T>(string statementId, object parameters = null);
        Task<int> ExecuteAsync(string statementId, object parameters = null);
        Task<T> ExecuteScalarAsync<T>(string statementId, object parameters = null);

        Task ExecuteSql(string sql);
    }
}
