﻿using MySql.Data.MySqlClient;
using System.Data;
using System.Diagnostics.CodeAnalysis;

namespace DbContext
{
    [ExcludeFromCodeCoverage]
    public class DefaultDbContext : IDefaultDbContext
    {
        private readonly string _queryDbString;
        private readonly string _commandDbString;

        public DefaultDbContext(string queryDbString, string commandDbString)
        {
            _queryDbString = queryDbString;
            _commandDbString = commandDbString;
        }

        public IDbConnection QueryDbConnection => new MySqlConnection(_queryDbString);

        public IDbConnection CommandDbConnection => new MySqlConnection(_commandDbString);

    }
    public interface IDefaultDbContext
    {
        IDbConnection QueryDbConnection { get; }
        IDbConnection CommandDbConnection { get; }
    }

    public interface IOracleDbContext
    {
        IDbConnection QueryDbConnection { get; }
        IDbConnection CommandDbConnection { get; }
    }
}
