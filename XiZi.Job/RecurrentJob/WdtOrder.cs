﻿using Autofac;
using Autofac.Core;
using Domain.Models;
using Domain.Service.Interface;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace XiZi.Job.RecurrentJob
{
    public class WdtOrder
    {
        private readonly IOrderService _orderService;
        private readonly INCService _ncService;
        private readonly IDYService _dYService;


        public WdtOrder()
        {
            // _orderService = (IOrderService)Hangfire.JobActivatorScope.Current.Resolve(typeof(IOrderService));

            _orderService = (IOrderService)JobActivator.Current.BeginScope().Resolve(typeof(IOrderService));
            _ncService = (INCService)JobActivator.Current.BeginScope().Resolve(typeof(INCService));
            _dYService = (IDYService)JobActivator.Current.BeginScope().Resolve(typeof(IDYService));

        }


        public void SyncOrderToXizi()
        {
            _orderService.SyncOrder().ConfigureAwait(false).GetAwaiter();
        }
        public void SyncDYOrderToXizi()
        { 
           _dYService.SyncDYGetOrder().ConfigureAwait(false).GetAwaiter();
        }

        public void SyncWdtOrder()
        {
            WdtClientConfig config = new WdtClientConfig()
            {
                Sid = ConfigurationManager.AppSettings["sid"],
                AppKey = ConfigurationManager.AppSettings["appkey"],
                Appsecret = ConfigurationManager.AppSettings["appsecret"],
                GatewayUrl = ConfigurationManager.AppSettings["gatewayUrl"],
                Page_size = 100,
                Page_no = 0
            };

            config.Start_time = _orderService.GetApiLastStartTime(1).ConfigureAwait(false).GetAwaiter().GetResult();
            config.LastEndTime = config.Start_time.Value.AddHours(1);
            _orderService.SaveWdtOdrer(config).ConfigureAwait(false).GetAwaiter();
            while (config.LastEndTime <= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day))
            {
                config.Start_time = config.LastEndTime;
                config.LastEndTime = config.Start_time.Value.AddHours(1);
                _orderService.SaveWdtOdrer(config).ConfigureAwait(false);
            }
        }

        public void SyncPartQimenOrder()
        {
            string[] shopNos = new string[] { "146", "0013", "0048", "1101" };

            foreach (string shop in shopNos)
            {
                WdtQimenConfig config = new WdtQimenConfig()
                {
                    Sid = ConfigurationManager.AppSettings["sid"],
                    AppKey = ConfigurationManager.AppSettings["QimenAppkey"],
                    Appsecret = ConfigurationManager.AppSettings["QimenAppsecret"],
                    ServerUrl = ConfigurationManager.AppSettings["QimenServerUrl"],
                    TargetAppKey = ConfigurationManager.AppSettings["QimenTargetAppKey"],
                    Page_size = 100,
                    Page_no = 0,
                    ShopNo = shop
                };

                config.Start_time = _orderService.GetApiLastStartTime(2).ConfigureAwait(false).GetAwaiter().GetResult();
                config.End_time = config.Start_time.AddMinutes(30);
                if (config.End_time > DateTime.Now)
                {
                    return;
                }
                _orderService.SaveQimenOdrer(config).ConfigureAwait(false).GetAwaiter();
                while (config.End_time <= DateTime.Now.AddHours(-1))
                {
                    config.Start_time = config.End_time;
                    config.End_time = config.Start_time.AddMinutes(30);
                    _orderService.SaveQimenOdrer(config).ConfigureAwait(false).GetAwaiter();
                }
            }        
        }

        public void SyncQimenOrder()
        {
            WdtQimenConfig config = new WdtQimenConfig()
            {
                Sid = ConfigurationManager.AppSettings["sid"],
                AppKey = ConfigurationManager.AppSettings["QimenAppkey"],
                Appsecret = ConfigurationManager.AppSettings["QimenAppsecret"],
                ServerUrl = ConfigurationManager.AppSettings["QimenServerUrl"],
                TargetAppKey = ConfigurationManager.AppSettings["QimenTargetAppKey"],
                Page_size = 100,
                Page_no = 0
            };

            config.Start_time = _orderService.GetApiLastStartTime(2).ConfigureAwait(false).GetAwaiter().GetResult();
            config.End_time = config.Start_time.AddMinutes(30);
            if (config.End_time > DateTime.Now)
            {
                return;
            }
            _orderService.SaveQimenOdrer(config).ConfigureAwait(false).GetAwaiter();
            while (config.End_time <= DateTime.Now.AddHours(-1))
            {
                config.Start_time = config.End_time;
                config.End_time = config.Start_time.AddMinutes(30);
                _orderService.SaveQimenOdrer(config).ConfigureAwait(false).GetAwaiter();
            }
        }
    }
}