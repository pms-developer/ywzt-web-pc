﻿using Autofac;
using Autofac.Core;
using Domain.Models;
using Domain.Service.Interface;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace XiZi.Job.RecurrentJob
{
    public class NcMaterialJob
    {
        private readonly INCService _ncService;

        public NcMaterialJob()
        {
            // _orderService = (IOrderService)Hangfire.JobActivatorScope.Current.Resolve(typeof(IOrderService));
            _ncService = (INCService)JobActivator.Current.BeginScope().Resolve(typeof(INCService));
        }

        public void SyncNCMaterial()
        {
            var result = _ncService.GetMaterialList().ConfigureAwait(false).GetAwaiter().GetResult();


            _ncService.SaveMaterialList(result, "nc_material").ConfigureAwait(false).GetAwaiter();

        }
    }
}