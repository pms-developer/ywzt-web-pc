﻿using Aspose.Cells;
using Domain.Models;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace XiZi.Job.Controllers
{
    public class NCApiController: Controller
    {     
        private readonly INCService _ncService; 

        public NCApiController(INCService ncService)
        {
            _ncService = ncService;
        }

        [HttpPost]
        public async Task<JsonResult> AddSaleInvoice(string url,NcSaleInvoice saleInvoices)
        {              
            if (string.IsNullOrEmpty(url))
            {
                url = "http://113.240.68.121:9001/service/XChangeServlet";
            }

            var result = await _ncService.AddSaleInvoice(url, saleInvoices);
            var node= GetXMlNode(result);

            return new JsonResult { 
                Data = new {code= GetXMLNodeText(node,"resultcode"), message= GetXMLNodeText(node,"resultdescription"),invoiceCode= GetXMLNodeText(node,"content") },
                JsonRequestBehavior = JsonRequestBehavior.DenyGet };
        }


        //public async Task<JsonResult> AddSaleInvoice(string url)
        //{
        //    string requestContent = GetXmlString(@"E:\NC\xml\sale_invoice.xml");

        //  //  var result = await _ncService.AddSaleInvoice(url, requestContent);

        //    return new JsonResult { Data = requestContent, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}


        XmlNode GetXMlNode(string xmlString)
        {
            StringReader Reader = new StringReader(xmlString);

            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(Reader);

            XmlNode sendresult = xmlDoc.SelectSingleNode("ufinterface/sendresult"); //取得节点名为node的XmlNode集合
            return sendresult;
        }

        string GetXMLNodeText(XmlNode node,string nodeName)
        {            
            var content= node.SelectSingleNode(nodeName).InnerText;
            string resultcode= node.SelectSingleNode("resultcode").InnerText;
            string resultdescription= node.SelectSingleNode("resultdescription").InnerText;

            return node.SelectSingleNode(nodeName).InnerText;
        }


        string GetXmlString(string strFile)
        {
            // Load the xml file into XmlDocument object.
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(strFile);
            }
            catch (XmlException e)
            {
                Console.WriteLine(e.Message);
            }
            // Now create StringWriter object to get data from xml document.
            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmlDoc.WriteTo(xw);
            return sw.ToString();
        }

        string ConvertXmlToJson()
        {          

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(@"E:\NC\xml\sale_invoice.xml");

            string jsonText = JsonConvert.SerializeXmlNode(xmlDoc);
            return jsonText;
        }
    }
}