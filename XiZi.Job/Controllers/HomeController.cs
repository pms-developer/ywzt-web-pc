﻿using Aspose.Cells;
using Domain.Models;
using Domain.Service.Interface;
using Dop.Api.OrderDownloadSettleItemToShop;
using Dop.Api.OrderDownloadShopAccountItem;
using Dop.Api.OrderDownloadShopAccountItemFile;
using Dop.Api.OrderDownloadToShop;
using Dop.Api.OrderGetShopAccountItemFile;
using Dop.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace XiZi.Job.Controllers
{
    public class HomeController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IApiService _apiService;

        private readonly IWdtService _wdtService;
        private readonly INCService _ncService;
        private readonly IDYService _dYService;

        private AccessToken accessToken;



        public HomeController(IOrderService orderService, IApiService apiService, IWdtService wdtService, INCService ncService, IDYService dYService)
        {
            _orderService = orderService;
            _apiService = apiService;
            _wdtService = wdtService;
            _ncService= ncService;
            _dYService = dYService;

            //GlobalConfig.GetGlobalConfig().AppKey = "7133388982120875550";
            //GlobalConfig.GetGlobalConfig().AppSecret = "31fa25c5-170e-4c20-a8ba-0e35962cc37a";

            //accessToken = AccessTokenBuilder.Build(3545060);//入参为shopId 
        }

        public ActionResult Index()
        {
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(@"E:\NC\xml\sale_invoice.xml");

            //string jsonText = JsonConvert.SerializeXmlNode(xmlDoc);

            //var downloadId= OrderDownloadShopAccountItemRequest();
            // Thread.Sleep(1000 * 30);
            // downloadToShop(downloadId);

            //var ret=_dYService.SyncDYGetOrder();

            //var od =new Domain.Models.DTO.ApiDto();
            //od.Id = 1;
            //od.ApiName = "dsadasd";
            //InsertMongodb(od);
            return View();
        }
        public void InsertMongodb(object response)
        {
            var jsonStr = JsonConvert.SerializeObject(response);
            MongoDB.Bson.BsonDocument bson = MongoDB.Bson.BsonDocument.Parse(jsonStr);
            DbContext.Default.MongoHelper<MongoDB.Bson.BsonDocument> dyOrdersMongoHelper = new DbContext.Default.MongoHelper<MongoDB.Bson.BsonDocument>("dyTable");
            dyOrdersMongoHelper.Insert(bson);
        }

        public int GetTimeStamp(DateTime dt)
        {
            DateTime dateStrat = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            int timeStamp = Convert.ToInt32((dt - dateStrat).TotalSeconds);
            return timeStamp;
        }
      
        private void downloadToShop(string downloadId,int retryTime=1)
        {
            var request = new OrderDownloadShopAccountItemFileRequest();
            var param = request.BuildParam();
            param.DownloadId = downloadId;
            var response = request.Execute<OrderDownloadShopAccountItemFileResponse>(accessToken);
            if (response.Code == 10000)
            {
                var url = response.Data.Url;
            }
            else
            {
                if(retryTime<10)
                {
                    Thread.Sleep(1000 * 30);
                    retryTime += 1;
                    downloadToShop(downloadId, retryTime);
                }
            }
          
        }

        private string OrderDownloadShopAccountItemRequest()
        {
            var request = new OrderDownloadShopAccountItemRequest();
            var param = request.BuildParam();
            param.StartTime = "2022-08-13 00:00:00";
            param.EndTime = "2022-08-14 00:00:00";
            param.AccountType = 0;
            param.BizType = 0;
            param.TimeType = 1;
            var response = request.Execute<OrderDownloadShopAccountItemResponse>(accessToken);
            if (response.Code== 10000)
            {
                return response.Data.DownloadId;
            }

            return string.Empty;
            
        }

        private void OrderGetShopAccountItemFile()
        {
            //获取access_token方法
            GlobalConfig.GetGlobalConfig().AppKey = "7133388982120875550";
            GlobalConfig.GetGlobalConfig().AppSecret = "31fa25c5-170e-4c20-a8ba-0e35962cc37a";

            AccessToken accessToken = AccessTokenBuilder.Build(3545060L);//入参为shopId 

            var request = new OrderGetShopAccountItemFileRequest();
            var param = request.BuildParam();
            param.StartDate = "2022-08-01";
            param.EndDate = "2022-08-01";
            var response = request.Execute<OrderGetShopAccountItemFileResponse>(accessToken);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public async Task<JsonResult> NCMaterialApi()
        {

           var result=await _ncService.GetMaterialList();

            await _ncService.SaveMaterialList(result, "nc_material");


            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> NCSpplierllApi()
        {

            var result = await _ncService.GetSpplierlList();

            await _ncService.SaveSpplierList(result, "nc_supplier");


            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> NCWarehouseApi()
        {

            var result = await _ncService.GetWarehouseList();

            await _ncService.SaveWarehouseList(result, "nc_stordoc");


            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> NCShopApi()
        {

            var result = await _ncService.GetShopList();

            await _ncService.SaveShopList(result, "nc_shop");


            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> WdtOrderApi()
        {
            WdtClientConfig config = new WdtClientConfig()
            {
                Sid = ConfigurationManager.AppSettings["sid"],
                AppKey = ConfigurationManager.AppSettings["appkey"],
                Appsecret = ConfigurationManager.AppSettings["appsecret"],
                GatewayUrl = ConfigurationManager.AppSettings["gatewayUrl"],
                Page_size = 100,
                Page_no = 0
            };
          //  config.Start_time = DateTime.Parse(ConfigurationManager.AppSettings["start_time"]);
             config.Start_time =await _orderService.GetApiLastStartTime(1);
            config.LastEndTime = config.Start_time.Value.AddHours(1);


            await _orderService.SaveWdtOdrer(config);
            while (config.LastEndTime < new DateTime(2022, 01, 02, 0, 0, 0))
            {
                config.Start_time = config.LastEndTime;
                config.LastEndTime = config.Start_time.Value.AddHours(1);
                await _orderService.SaveWdtOdrer(config);
            }

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> WdtShopApi()
        { 

            WdtClientConfig config =await _apiService.GetApiConfig(4);

            var shops = await _wdtService.GetShop(config);
            await _wdtService.SaveShop(shops, "wdt_shop");

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> WdtProviderApi()
        {

            WdtClientConfig config = await _apiService.GetApiConfig(5);

            var providers = await _wdtService.GetPurchaseProvider(config);
            await _wdtService.SavePurchaseProvider(providers, "wdt_purchase_provider");

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> WdtWareHouseApi()
        {

            WdtClientConfig config = await _apiService.GetApiConfig(6);

            var providers = await _wdtService.GetWareHouse(config);
            await _wdtService.SaveWareHouse(providers, "wdt_warehouse");

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> WdtGoodsClassApi()
        {

            WdtClientConfig config = await _apiService.GetApiConfig(7);

            var dataList = await _wdtService.GetGoodsClass(config);
            await _wdtService.SaveGoodsClass(dataList, "wdt_goods_class");

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> WdtGoodsBrandApi()
        {

            WdtClientConfig config = await _apiService.GetApiConfig(8);

            var dataList =  _wdtService.GetGoodsGrand(config);
            await _wdtService.SaveGoodsGrand(dataList, "wdt_goods_brand");

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public async Task<JsonResult> WdtGoodsApi()
        {

            WdtClientConfig config = await _apiService.GetApiConfig(9);

            while (config.LastEndTime.HasValue & config.LastEndTime < DateTime.Now)
            {
                config.Start_time = config.LastEndTime;

                config.LastEndTime = config.Start_time.Value.AddDays(30);              

                var dataList = _wdtService.GetGoods(config);
                if (dataList != null && dataList.Count > 0)
                {
                    await _wdtService.SaveGoods(dataList, "wdt_goods");
                }
            }

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> WdtPlatformGoodsApi()
        {

            WdtClientConfig config = await _apiService.GetApiConfig(10);
            while(config.LastEndTime.HasValue&config.LastEndTime<DateTime.Now)
            {
                config.Start_time = config.LastEndTime;

                config.LastEndTime = config.Start_time.Value.AddDays(30);
                var dataList = _wdtService.GetPlatformGoods(config);
                if(dataList!=null &&dataList.Count>0)
                {
                    await _wdtService.SavePlatformGoods(dataList, "wdt_platform_goods");
                }
                
            }

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public async Task<JsonResult> QmApi()
        {
            string[] shopNos = new string[] { "0013" };

            foreach (string shop in shopNos)
            {

                WdtQimenConfig config = new WdtQimenConfig()
                {
                    Sid = ConfigurationManager.AppSettings["sid"],
                    AppKey = ConfigurationManager.AppSettings["QimenAppkey"],
                    Appsecret = ConfigurationManager.AppSettings["QimenAppsecret"],
                    ServerUrl = ConfigurationManager.AppSettings["QimenServerUrl"],
                    TargetAppKey = ConfigurationManager.AppSettings["QimenTargetAppKey"],
                    Page_size = 100,
                    Page_no = 0,
                    ShopNo = shop
                };
               // config.Start_time = DateTime.Parse(ConfigurationManager.AppSettings["start_time"]);
                config.Start_time = await _orderService.GetApiLastStartTime(2);
                
               
                config.End_time = config.Start_time.AddMinutes(30);
                await _orderService.SaveQimenOdrer(config);
                while (config.End_time <= DateTime.Now)
                {
                    config.Start_time = config.End_time;
                    config.End_time = config.Start_time.AddMinutes(30);
                    await _orderService.SaveQimenOdrer(config);
                }
            }

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public async Task<JsonResult> QmHistoryApi()
        {
            string[] shopNos = new string[] { "146", "0013", "0048", "1101" };

            foreach (string shop in shopNos)
            {
                WdtQimenConfig config = new WdtQimenConfig()
                {
                    Sid = ConfigurationManager.AppSettings["sid"],
                    AppKey = ConfigurationManager.AppSettings["QimenAppkey"],
                    Appsecret = ConfigurationManager.AppSettings["QimenAppsecret"],
                    ServerUrl = ConfigurationManager.AppSettings["QimenServerUrl"],
                    TargetAppKey = ConfigurationManager.AppSettings["QimenTargetAppKey"],
                    Page_size = 100,
                    Page_no = 0,
                    ShopNo = shop
                };
               //config.Start_time = DateTime.Parse(ConfigurationManager.AppSettings["start_time"]);
               config.Start_time = await _orderService.GetApiLastStartTime(3);
                config.End_time = config.Start_time.AddHours(1);
                await _orderService.SaveQimenHistoryOdrer(config);
                while (config.End_time <= DateTime.Now.AddDays(-88))
                {
                    config.Start_time = config.End_time;
                    config.End_time = config.Start_time.AddHours(1);
                    await _orderService.SaveQimenHistoryOdrer(config);
                }
            }

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public async Task<JsonResult> ExcelJs()
        {
            await _orderService.SaveQnjsExcel(@"C:\Users\Administrator\Desktop\计划.xlsx");

            await _orderService.GetAllPlan();

          

            return new JsonResult { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}