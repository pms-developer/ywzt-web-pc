﻿using Aspose.Cells;
using Domain.Models;
using Domain.Models.Entity;
using Domain.Service.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace XiZi.Job.Controllers
{
    public class XiziApiController : Controller
    {
        private readonly IShopInfoService _shopInfoService;

        public XiziApiController(IShopInfoService shopInfoService)
        {
            _shopInfoService = shopInfoService;
        }

        public async Task<JsonResult> GetWDTShopCode(string shopId)
        {
            var shopid= shopId??string.Empty;
            var result =await _shopInfoService.GetWdtShopCode(shopId);

            return new JsonResult
            {
                Data = new { code = "200", ShopInfo = result },
                JsonRequestBehavior= JsonRequestBehavior.AllowGet
            };
           
        }   
    }
}