﻿using Aspose.Cells;
using Domain.Models;
using Domain.Models.Entity;
using Domain.Service.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace XiZi.Job.Controllers
{
    public class ApiController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IApiService _apiService;

        private readonly IWdtService _wdtService; 

        public ApiController(IOrderService orderService, IApiService apiService, IWdtService wdtService)
        {
            _orderService = orderService;
            _apiService = apiService;
            _wdtService = wdtService;
        }
       
        public  JsonResult GetShopOrder(DateTime? startTime,DateTime? endTime,string tradeNo,string src_tid)
        {

            WdtQimenConfig config = new WdtQimenConfig()
            {
                Sid = ConfigurationManager.AppSettings["sid"],
                AppKey = ConfigurationManager.AppSettings["QimenAppkey"],
                Appsecret = ConfigurationManager.AppSettings["QimenAppsecret"],
                ServerUrl = ConfigurationManager.AppSettings["QimenServerUrl"],
                TargetAppKey = ConfigurationManager.AppSettings["QimenTargetAppKey"],
                ShopNo = "1013",
                Page_size = 100,
                Page_no = 0,
                TradeNo = tradeNo,
                SrcTid = src_tid
            };

            if(startTime.HasValue)
            {
                config.Start_time = startTime.Value;
            }

            if(endTime.HasValue)
            {
                config.End_time = endTime.Value;
            } 

            var result =  _orderService.GetQimenAllPagedOrder(config);
           

            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}