﻿using Aspose.Cells;
using Domain.Models;
using Domain.Models.Entity;
using Domain.Service.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace XiZi.Job.Controllers
{
    public class WdtApiController : Controller
    {
      
        private readonly IBillingService _billingService; 

        public WdtApiController(IBillingService billingService)
        {
            _billingService = billingService;
        }
       
        public  JsonResult GetBilling(DateTime startTime,DateTime endTime,string shopNo)
        {

            WdtQimenConfig config = new WdtQimenConfig()
            {
                Sid = ConfigurationManager.AppSettings["sid"],
                AppKey = ConfigurationManager.AppSettings["QimenAppkey"],
                Appsecret = ConfigurationManager.AppSettings["QimenAppsecret"],
                ServerUrl = ConfigurationManager.AppSettings["QimenServerUrl"],
                TargetAppKey = ConfigurationManager.AppSettings["QimenTargetAppKey"],
                Start_time = startTime,
                End_time = endTime,
                ShopNo = shopNo,
                Page_size = 100,
                Page_no = 0              
            };

            

            var result = _billingService.GetQimenAllPagedBilling(config);
           

            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}