﻿using Autofac;
using Autofac.Integration.Mvc;
using DbContext;
using Domain;
using Hangfire;
using Hangfire.MySql;
using Microsoft.Owin;
using Owin;
using System;
using System.Configuration;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;
using XiZi.Excel;
using XiZi.Job.RecurrentJob;

[assembly: OwinStartup(typeof(XiZi.Job.OwinStartup))]

namespace XiZi.Job
{
    public class OwinStartup
    {
        public async void Configuration(IAppBuilder app)
        {

            string mysqlConnectionString = ConfigurationManager.ConnectionStrings["mysql"].ConnectionString;
            string oracleConnectionString = ConfigurationManager.ConnectionStrings["oracle"].ConnectionString;

            GlobalConfiguration.Configuration.UseStorage(new MySqlStorage(mysqlConnectionString, new MySqlStorageOptions() { TablesPrefix= "hangfire" }));

            var builder = new ContainerBuilder();
            builder.ConfigureDbContext(mysqlConnectionString, oracleConnectionString);
            builder.ConfigureDomain();
            builder.ConfigureExcel();
        
            app.UseHangfireServer();//开始使用Hangfire服务
            app.UseHangfireDashboard("/hangfire", new DashboardOptions()
            {
                Authorization = new[] { new CustomAuthorizeFilter() }
            });

            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<WdtOrder>().AsSelf().InstancePerBackgroundJob();
        

            var container = builder.Build();
             GlobalConfiguration.Configuration.UseAutofacActivator(container);
            JobActivator.Current = new AutofacJobActivator(container);          

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            //RecurringJob.AddOrUpdate<NcMaterialJob>("SyncNCMaterial", order => order.SyncNCMaterial(), Cron.Daily(2), TimeZoneInfo.Local);
            //RecurringJob.AddOrUpdate<WdtOrder>("GetQimenOrder", order => order.SyncQimenOrder(), Cron.Daily(1), TimeZoneInfo.Local);
            //RecurringJob.AddOrUpdate<WdtOrder>("SyncOrderToXiZi", order => order.SyncOrderToXizi(), Cron.Daily(3), TimeZoneInfo.Local); 

            //每天执行抖音数据抓取
            RecurringJob.AddOrUpdate<WdtOrder>("SyncDYOrderToXiZi", order => order.SyncDYOrderToXizi(), Cron.Daily(1), TimeZoneInfo.Local);

        }
    }
}
