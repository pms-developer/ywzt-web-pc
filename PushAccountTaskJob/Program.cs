﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PushAccountTaskJob
{
    internal class Program
    {
        private static MeioErpWarehouseRentAccountIBLL warehouseRentAccountIBLL = new MeioErpWarehouseRentAccountBLL();
        private static MeioErpLogisticsPackageAccountIBLL logisticsPackageAccountIBLL = new MeioErpLogisticsPackageAccountBLL();
        private static MeioErpInStorageAccountIBLL inStorageAccountIBLL = new MeioErpInStorageAccountBLL();
        private static MeioErpDeliveryAccountIBLL deliveryAccountIBLL = new MeioErpDeliveryAccountBLL();
        private static MeioErpCheckInventoryAccountIBLL checkInventoryAccountIBLL = new MeioErpCheckInventoryAccountBLL();

        private static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            try
            {
                log.Info($"\r\n{DateTime.Now}：开始推送账单");
                //推送仓库租金账单
                warehouseRentAccountIBLL.PushWarehouseRentAccount(true);
                //推送入库账单
                inStorageAccountIBLL.PushInStorageAccount(true);
                //推送出库账单
                deliveryAccountIBLL.PushDeliveryAccount(true);
                //推送盘点账单
                checkInventoryAccountIBLL.PushCheckInventoryAccount(true);
                //推送快递打包账单
                logisticsPackageAccountIBLL.PushLogisticsPackageAccount(true);
                log.Info($"\r\n{DateTime.Now}：结束推送账单");
            }
            catch (Exception ex)
            {
                log.Error($"{DateTime.Now}：{ex.Message}");
            }
        }
    }
}
