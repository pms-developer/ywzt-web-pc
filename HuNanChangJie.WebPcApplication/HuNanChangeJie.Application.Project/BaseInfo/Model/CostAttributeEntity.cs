﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.BaseInfo.Model
{
    /// <summary>
    /// 成本属性实体
    /// </summary>
    public class CostAttributeEntity
    {
        #region  实体成员 
        /// <summary> 
        /// 主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("ID")]
        public string ID { get; set; }
        /// <summary> 
        /// 名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary> 
        /// 排序号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary> 
        /// 是否启用 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISENABLE")]
        public bool? IsEnable { get; set; }
        /// <summary> 
        /// 是否已部署 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISDEPLOY")]
        public bool? IsDeploy { get; set; } = false;
        /// <summary> 
        /// 说明 
        /// </summary> 
        /// <returns></returns> 
        [Column("REMARK")]
        public string Remark { get; set; }

        /// <summary>
        /// 列名
        /// </summary>
        public string ColumnName { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}
