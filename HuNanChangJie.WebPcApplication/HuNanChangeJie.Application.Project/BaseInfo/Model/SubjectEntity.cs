﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.BaseInfo.Model
{
    /// <summary>
    /// 收支科目实体
    /// </summary>
    public class SubjectEntity
    {
        private string code;
        #region  实体成员 
        /// <summary> 
        /// 主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("ID")]
        public string ID { get; set; }
        /// <summary> 
        /// 父级ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("PARENTID")]
        public string ParentId { get; set; }
        /// <summary> 
        /// 科目编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("CODE")]
        public string Code
        {
            get
            {
                if (string.IsNullOrWhiteSpace(PostfixCode)) return code;
                return ParentCode + PostfixCode;
            }
            set
            {
                code = value;
            }
        }
        /// <summary> 
        /// 科目名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary> 
        /// 收支类型 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAYMENTTYPE")]
        public string PaymentType { get; set; }

        /// <summary>
        /// 是否系统科目
        /// </summary>
        [Column("IsSystem")]
        public bool? IsSystem { get; set; } = false;
        /// <summary> 
        /// 是否项目科目 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISPROJECT")]
        public bool? IsProject { get; set; }
        /// <summary> 
        /// 是否管理科目 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISMANAGEMENT")]
        public bool? IsManagement { get; set; }
        /// <summary> 
        /// 成本属性ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("COSTATTRIBUTEID")]
        public string CostAttributeId { get; set; }
        /// <summary> 
        /// 是否启用 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISENABLE")]
        public bool? IsEnable { get; set; } = true;
        /// <summary> 
        /// 排序号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SORTCODE")]
        public int? SortCode { get; set; }

        /// <summary>
        /// 科目全名
        /// </summary>
        public string FullName { get; set; }

        /// <summary> 
        /// 说明 
        /// </summary> 
        /// <returns></returns> 
        [Column("REMARK")]
        public string Remark { get; set; }

        [NotMapped]
        public string CostAttributeName { get; set; }

        [NotMapped]
        public string ParentCode { get; set; }

        [NotMapped]
        public string PostfixCode
        {
            get;
            set;
        }

        [NotMapped]
        public string ParentName { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}
