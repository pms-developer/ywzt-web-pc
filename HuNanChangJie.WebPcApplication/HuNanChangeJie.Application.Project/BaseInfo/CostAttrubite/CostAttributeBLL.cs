﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangJie.Util;

namespace HuNanChangeJie.Application.Project.BaseInfo.CostAttrubite
{
    public class CostAttributeBLL:ICostAttributeBLL
    {
        private CostAttributeService costAttributeService = new CostAttributeService();

        public void Delete(string keyValue)
        {
            try
            {
                costAttributeService.Delete(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            } 
        }

        public CostAttributeEntity GetFormData(string keyValue)
        {
            try
            {
                return costAttributeService.GetFormData(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public IEnumerable<CostAttributeEntity> GetList()
        {
            try
            {
                return costAttributeService.GetList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public void Save(string keyValue,string type,CostAttributeEntity entity)
        {
            try
            {
                costAttributeService.Save(keyValue,type,entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public void SetDeploy(string keyValue, bool isDeploy = true)
        {
            try
            {
                costAttributeService.SetDeploy(keyValue, isDeploy);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public void SetEnable(string keyValue, bool isEnable = true)
        {
            try
            {
                costAttributeService.SetEnable(keyValue, isEnable);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }
    }
}
