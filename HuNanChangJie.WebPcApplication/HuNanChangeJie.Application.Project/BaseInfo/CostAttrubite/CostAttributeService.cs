﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using HuNanChangJie.Util.SqlBase;

namespace HuNanChangeJie.Application.Project.BaseInfo.CostAttrubite
{
    public class CostAttributeService:RepositoryFactory
    {
        public void Delete(string keyValue)
        {
            try
            {
                BaseRepository().Delete<CostAttributeEntity>(i => i.ID == keyValue);
            }
            catch(Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public IEnumerable<CostAttributeEntity> GetList()
        {
            try
            {
                return BaseRepository().FindList<CostAttributeEntity>().OrderBy(i=>i.SortCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public CostAttributeEntity GetFormData(string keyValue)
        {
            try
            {
                return BaseRepository().FindEntity<CostAttributeEntity>(i => i.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public void Save(string keyValue,string type,CostAttributeEntity entity)
        {
            try
            {
                entity.ID = keyValue;
                if (type == "add")
                {
                    BaseRepository().Insert(entity);
                }
                else
                {
                    BaseRepository().Update(entity);
                }
                
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public void SetDeploy(string keyValue, bool isDeploy)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                
                var info = BaseRepository().FindEntity<CostAttributeEntity>(i=>i.ID==keyValue);
                if (string.IsNullOrWhiteSpace(info.ColumnName))
                {
                    var colnumName = Str.PinYin(info.Name);
                    var count = 0;
                    while (BaseRepository().FindEntity<CostAttributeEntity>(i => i.ColumnName == colnumName && i.ID!=keyValue) != null)
                    {
                        count++;
                        colnumName += count;
                    }
                    info.ColumnName = colnumName;
                    var tableList = new List<string>
                    {
                        "Project_BiddingPriceQuantities",
                        "Project_ConstructionBudgetQuantities",
                        "Project_BidQuotationQuantities",
                        "Project_BidQuantities"
                    };
                    var dataBaseType = BaseRepository().DataBaseType;
                    SqlBase sqlbase=null;
                    foreach (var tableName in tableList)
                    {
                        switch (dataBaseType)
                        {
                            case DataBaseType.MySql:
                                sqlbase = new MySql(tableName);
                                break;
                            case DataBaseType.Oracle:
                                sqlbase = new Oracle(tableName);
                                break;
                            case DataBaseType.SqlServer:
                                sqlbase = new SqlServer(tableName);
                                break;
                        }
                        var sql = sqlbase.AddFiled(colnumName, "decimal", 18);
                        db.ExecuteBySql(sql);
                    }
                }
                info.Modify(keyValue);
                info.IsDeploy = isDeploy;
                db.Update(info);

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public void SetEnable(string keyValue, bool isEnable)
        {
            try
            {
                var info = new CostAttributeEntity();
                info.Modify(keyValue);
                info.IsEnable = isEnable;
                BaseRepository().Update(info);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }
    }
}
