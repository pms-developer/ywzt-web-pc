﻿using HuNanChangeJie.Application.Project.BaseInfo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.BaseInfo.CostAttrubite
{
    public interface ICostAttributeBLL
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="entity"></param>
        void Save(string keyValue,string type,CostAttributeEntity entity);

        /// <summary>
        /// 删除成本属性
        /// </summary>
        /// <param name="keyValue">主键</param>
        void Delete(string keyValue);

        /// <summary>
        /// 设置启用状态
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="isEnable">启用状态</param>
        void SetEnable(string keyValue, bool isEnable=true);
        CostAttributeEntity GetFormData(string keyValue);

        /// <summary>
        /// 设置部署状态
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="isDeploy">部署状态</param>
        void SetDeploy(string keyValue, bool isDeploy = true);

        /// <summary>
        /// 获取成本属性列表
        /// </summary>
        /// <returns></returns>
        IEnumerable<CostAttributeEntity> GetList();
    }
}
