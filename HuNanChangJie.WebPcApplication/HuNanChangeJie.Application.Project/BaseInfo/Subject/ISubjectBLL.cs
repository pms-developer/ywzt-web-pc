﻿using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.BaseInfo.Subject
{
    public interface ISubjectBLL
    {
        void Save(string keyValue, string type, SubjectEntity entity);

        void Delete(string keyValue);

        IEnumerable<SubjectEntity> GetList();

        void SetEnable(string keyValue, bool isEnable = true);
        SubjectEntity GetFormData(string keyValue);
        SubjectEntity GetParentInfo(string keyValue);
        IEnumerable<TreeModel> GetTree();
        bool CheckCodeIsExist(string code);
        IEnumerable<SubjectEntity> GetSubjectList(string type);
    }
}
