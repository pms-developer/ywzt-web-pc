﻿using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.BaseInfo.Subject
{
    public class SubjectService : RepositoryFactory
    {
        public void Delete(string keyValue)
        {
            try
            {
                BaseRepository().Delete<SubjectEntity>(i=>i.ID==keyValue && i.IsSystem==false);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public IEnumerable<SubjectEntity> GetList()
        {
            try
            {
                var sql = "select b.Name as CostAttributeName,a.* from Base_Subject a left join Base_CostAttribute b on a.CostAttributeId = b.id";
                return BaseRepository().FindList<SubjectEntity>(sql).OrderBy(i => i.SortCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public SubjectEntity GetFormData(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(" select Sub.*,P.ParentCode,p.ParentName from Base_Subject as Sub ");
                sb.Append(" left join (select P.FullName as ParentName,P.Code as ParentCode,p.ID as PID from Base_Subject P) P");
                sb.Append(" on sub.ParentId = p.PID");
                sb.Append(" where Id=@keyValue");
                var table= BaseRepository().FindTable(sb.ToString(),new { keyValue });
                var info = new SubjectEntity();
                if (table == null) return info;
                var row = table.Rows[0];
                 
                info.ID = row["ID"].ToString();
                info.ParentId = row["ParentId"].ToString();
                info.Name = row["Name"].ToString();
                var code= row["Code"].ToString(); 
                info.Code = code;
                info.CostAttributeId = row["CostAttributeId"].ToString();
                info.IsEnable = row["IsEnable"].ToBool();
                info.IsManagement = row["IsManagement"].ToBool();
                info.IsProject = row["IsProject"].ToBool();
                info.IsSystem = row["IsSystem"].ToBool();
                var pCode= row["ParentCode"].ToString();
                info.ParentCode = pCode;
                info.FullName = row["FullName"].ToString();
                info.ParentName = row["ParentName"].ToString();
                info.PaymentType = row["PaymentType"].ToString();
                info.Remark = row["Remark"].ToString();
                info.SortCode = row["SortCode"].ToInt();
                info.PostfixCode = code.Substring(pCode.Length, code.Length - pCode.Length);
                return info;

                
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public SubjectEntity GetParentInfo(string keyValue)
        {
            try
            {
                var entity= BaseRepository().FindEntity<SubjectEntity>(i => i.ID == keyValue);
                entity.ParentCode= entity.Code;
                entity.ParentName = entity.FullName;
                return entity;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public void Save(string keyValue, string type, SubjectEntity entity)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                entity.ID = keyValue;
                if (type == "add")
                {
                    entity.Create();
                    entity.ParentId = keyValue;
                    db.Insert(entity);
                }
                else
                {
                    db.Update(entity);
                    {
                        var projectSubjectList = db.FindList<ProjectSubjectEntity>(i => i.BaseSubjectId == keyValue);
                        var list = new List<ProjectSubjectEntity>();
                        foreach (var item in projectSubjectList)
                        {
                            var info = new ProjectSubjectEntity();
                            info = item;
                            info.Copy(entity);
                            list.Add(info);
                        }
                        db.Update(list);
                    }

                    {
                        var tbysList = db.FindList<Tbys_SubjectEntity>(i => i.BaseSubjectId == keyValue);
                        var list = new List<Tbys_SubjectEntity>();
                        foreach (var item in tbysList)
                        {
                            var info = new Tbys_SubjectEntity();
                            info = item;
                            info.Copy(entity);
                            list.Add(info);
                        }
                        db.Update(list);
                    }

                    {
                        var tbbjList = db.FindList<Tbbj_SubjectEntity>(i => i.BaseSubjectId == keyValue);
                        var list = new List<Tbbj_SubjectEntity>();
                        foreach (var item in tbbjList)
                        {
                            var info = new Tbbj_SubjectEntity();
                            info = item;
                            info.Copy(entity);
                            list.Add(info);
                        }
                        db.Update(list);
                    }
                    {
                        var zbjList = db.FindList<Zbj_SubjectEntity>(i => i.BaseSubjectId == keyValue);
                        var list = new List<Zbj_SubjectEntity>();
                        foreach (var item in zbjList)
                        {
                            var info = new Zbj_SubjectEntity();
                            info = item;
                            info.Copy(entity);
                            list.Add(info);
                        }
                        db.Update(list);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public bool CheckCodeIsExist(string code)
        {
            try
            {
                var info = BaseRepository().FindEntity<SubjectEntity>(i => i.Code == code);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public IEnumerable<SubjectEntity> GetSubjectList(string type)
        {
            try
            {
                return BaseRepository().FindList<SubjectEntity>(i=>i.PaymentType==type && i.IsEnable==true).OrderBy(i => i.SortCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public void SetEnable(string keyValue, bool isEnable)
        {
            try
            {
                var info = new SubjectEntity();
                info.Modify(keyValue);
                info.IsEnable = isEnable;
                BaseRepository().Update(info);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }
    }
}
