﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;
using HuNanChangJie.Util;

namespace HuNanChangeJie.Application.Project.BaseInfo.Subject
{
    public class SubjectBLL : ISubjectBLL
    {
        private SubjectService serviceSubject = new SubjectService();
        private ICache cache = CacheFactory.CaChe();
        private readonly string cacheKey = $"hncjpms_BaseSubject";
        public void Delete(string keyValue)
        {
            try
            {
                serviceSubject.Delete(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public SubjectEntity GetFormData(string keyValue)
        {
            try
            {
                return serviceSubject.GetFormData(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public IEnumerable<SubjectEntity> GetList()
        {
            try
            {
                List<SubjectEntity> list;
                list = cache.Read<List<SubjectEntity>>(cacheKey, CacheId.Subject);
                if (list == null)
                {
                    return serviceSubject.GetList();
                }
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public SubjectEntity GetParentInfo(string keyValue)
        {
            try
            {
                return serviceSubject.GetParentInfo(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public IEnumerable<TreeModel> GetTree()
        {
            try
            {
                var list = GetList();

                var treeList = new List<TreeModel>();
                foreach (var item in list)
                {
                    LoadTreeMode(treeList, item);
                }
                return treeList.ToTree("0");
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        private void LoadTreeMode(List<TreeModel> list, SubjectEntity entity)
        {
            var tree = new TreeModel();
            tree.id = entity.ID;
            tree.text = entity.Code + " " + entity.Name;
            tree.value = entity.ID;
            tree.showcheck = true;
            tree.checkstate = 0;
            tree.isexpand = false;
            tree.parentId = entity.ParentId;
            tree.FullName = entity.FullName;

            list.Add(tree);

        }

        public void Save(string keyValue, string type, SubjectEntity entity)
        {
            try
            {
                serviceSubject.Save(keyValue, type, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public void SetEnable(string keyValue, bool isEnable = true)
        {
            try
            {
                serviceSubject.SetEnable(keyValue, isEnable);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public bool CheckCodeIsExist(string code)
        {
            try
            {
                return serviceSubject.CheckCodeIsExist(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public IEnumerable<SubjectEntity> GetSubjectList(string type)
        {
            try
            {
                return serviceSubject.GetSubjectList(type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }
    }
}
