﻿using HuNanChangeJie.Application.Project.IBll;
using HuNanChangeJie.Application.Project.Model;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Application.Organization;
using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Team
{
    public class TeamBll : ITeamBll
    {
        private DataItemIBLL dataItemIBLL = new DataItemBLL();
        private TeamService teamService = new TeamService();
        private UserIBLL userIBLL = new UserBLL();
        private ICache cache = CacheFactory.CaChe();

        private readonly string cacheKey;
        private readonly string projectId;
        public TeamBll(string projectId)
        {
            this.projectId = projectId;
            cacheKey = $"hncjpms_projectRole_{projectId}";
        }

        public List<TeamEntity> GetTeamList()
        {
            try
            {
                //teamsList;
                //teamsList = cache.Read<List<TeamEntity>>(cacheKey, CacheId.ProjectRole);
                //if (teamsList?.Count==0 ||teamsList == null)
                //{
                var itemId = ConfigurationManager.AppSettings["ProjectRoleKey"].ToString();
                var roleList = dataItemIBLL.GetDetailListByItemIdNoCache(itemId);
                var relationList = GetProjectRoleRelationList();
                List<TeamEntity> teamsList = new List<TeamEntity>();
                foreach (var role in roleList)
                {
                    var info = new TeamEntity();
                    info.ProjectId = projectId;
                    info.RoleId = role.F_ItemDetailId;
                    info.RoleName = role.F_ItemName;
                    info.UserList = new List<TeamUser>();
                    foreach (var item in relationList)
                    {
                        if (role.F_ItemDetailId != item.RoleId) continue;
                        var userInfo = new TeamUser();
                        userInfo.UserId = item.UserId;
                        string userName = "";
                        UserEntity userdata = userIBLL.GetEntityByUserId(item.UserId);
                        if (userdata != null)
                        {
                            userName = userdata.F_RealName;
                        }
                        userInfo.UserName = userName;
                        info.UserList.Add(userInfo);
                    }
                    teamsList.Add(info);
                    //}
                    //cache.Write(cacheKey, teamsList, CacheId.ProjectRole);
                }
                return teamsList;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        private IEnumerable<ProjectRoleRelationEntity> GetProjectRoleRelationList()
        {
            try
            {
                return teamService.GetProjectRoleRelationList(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public void SaveTeam(List<TeamEntity> teamList)
        {
            try
            {
                teamService.SaveTeam(teamList,projectId);
                cache.Remove(cacheKey, CacheId.ProjectRole);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }
    }
}
