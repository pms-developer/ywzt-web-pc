﻿using HuNanChangeJie.Application.Project.Model;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Application.Organization;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Team
{
    public class TeamService : RepositoryFactory
    {
        public IEnumerable<ProjectRoleRelationEntity> GetProjectRoleRelationList(string projectId)
        {
            try
            {
                return BaseRepository().FindList<ProjectRoleRelationEntity>(i => i.ProjectId == projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        /// <summary>
        /// 获取项目角色关系
        /// </summary>
        /// <param name="projectId">项目id</param>
        /// <param name="userId">用户id</param>
        /// <returns></returns>
        public IEnumerable<ProjectRoleRelationEntity> GetProjectRoleRelationList(string projectId,string userId)
        {
            try
            {
                return BaseRepository().FindList<ProjectRoleRelationEntity>(i => i.ProjectId == projectId && i.UserId == userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public void SaveTeam(List<TeamEntity> teamList,string projectId)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                db.Delete<ProjectRoleRelationEntity>(i => i.ProjectId == projectId);
                foreach (var role in teamList)
                {
                    if (string.IsNullOrEmpty(role.RoleId))
                    {
                        string id = Guid.NewGuid().ToString();
                        db.Insert<DataItemDetailEntity>(new DataItemDetailEntity {  F_ItemDetailId = id, F_ItemName = role.RoleName, F_ItemId= "828b8d88-0f24-4493-94fc-99ced0f1b7c4",  F_ItemValue=role.RoleName, F_SortCode=999 });
                        role.RoleId = id;
                        role.ProjectId = projectId;
                    }
                    if (role.UserList == null) continue;
                    foreach (var user in role.UserList)
                    {
                        var info = new ProjectRoleRelationEntity();
                        info.ProjectId = projectId;
                        info.RoleId = role.RoleId;
                        info.ID = Guid.NewGuid().ToString();
                        info.UserId = user.UserId;
                        db.Insert(info);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }
    }
}
