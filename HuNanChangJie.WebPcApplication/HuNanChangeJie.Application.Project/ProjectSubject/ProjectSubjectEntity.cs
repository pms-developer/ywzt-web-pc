﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangeJie.Application.Project.BaseInfo.Model;

namespace HuNanChangeJie.Application.Project.ProjectSubject
{
    public class BaseSubjectModle
    {
        #region  实体成员 
        /// <summary> 
        /// ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("ID")]
        public string ID { get; set; }
        /// <summary> 
        /// 项目ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary> 
        /// 基础科目ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("BASESUBJECTID")]
        public string BaseSubjectId { get; set; }

        [NotMapped]
        public string CostAttributeName { get; set; }

        /// <summary> 
        /// 创建时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        /// <summary> 
        /// 是否显示 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISSHOW")]
        public bool? IsShow { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SortCode")]
        public int? SortCode { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 是否占用
        /// </summary>
        public bool? IsOccupy { get; set; }

        /// <summary>
        /// 基础科目父级ID
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 是否系统科目
        /// </summary>
        public bool? IsSystem { get; set; }

        /// <summary>
        /// 是否项目科目
        /// </summary>
        public bool? IsProject { get; set; }

        /// <summary>
        /// 是否管理科目
        /// </summary>
        public bool? IsManagement { get; set; }

        /// <summary>
        /// 成本属性ID
        /// </summary>
        public string CostAttributeId { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool? IsEnable { get; set; }

        /// <summary>
        /// 收支类型
        /// </summary>
        public string PaymentType { get; set; }

        /// <summary>
        /// 科目全名
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// 取值方式
        /// </summary>
        public string ValueType { get; set; }

        /// <summary>
        /// 费率
        /// </summary>
        public decimal? Rate { get; set; }

        /// <summary>
        /// 计算公式
        /// </summary>
        public string Formula { get; set; }

        /// <summary>
        /// 预算成本
        /// </summary>
        public decimal? BudgetCost { get; set; }

        /// <summary>
        /// 预算金额
        /// </summary>
        public decimal? BudgetAmount { get; set; }

        /// <summary>
        /// 占比
        /// </summary>
        public decimal? Ratio { get; set; }

        /// <summary>
        /// 预算描述
        /// </summary>
        public string BudgetRemark { get; set; }

        /// <summary>
        /// 实际发生成本（含税）
        /// </summary>
        public decimal? ActuallyOccurred { get; set; }

        /// <summary>
        /// 实际发生成本（不含税）
        /// </summary>
        public decimal? AOAfterTax { get; set; }

        /// <summary>
        /// 待发生成本
        /// </summary>
        public decimal? PendingCost { get; set; }

        /// <summary>
        /// 竣工结算成本
        /// </summary>
        public decimal? FinalCost { get; set; }

        /// <summary>
        /// 超预算金额
        /// </summary>
        public decimal? ExceedBudgetAmount { get; set; }

        /// <summary>
        /// 超预算比例
        /// </summary>
        public decimal? ExceedBudgetRate { get; set; }

        /// <summary>
        /// 临时值
        /// </summary>
        public decimal? TempValue { get; set; }


        /// <summary>
        /// 说明
        /// </summary>
        public string Remark { get; set; }

        [NotMapped]
        public string EditType { get; set; }

        [NotMapped]
        public string rowState { get; set; }

        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        private void SetValue()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }

        /// <summary>
        /// 复制对象
        /// </summary>
        /// <param name="item"></param>
        /// <param name="currentProjectId"></param>
        public void Copy(BaseSubjectModle item, string currentProjectId)
        {
            SetValue();
            this.IsEnable = item.IsEnable;
            this.IsShow = item.IsShow;
            this.ProjectId = currentProjectId;
            this.BaseSubjectId = item.BaseSubjectId;
            this.ParentId = item.ParentId;
            this.SortCode = item.SortCode;
            this.Name = item.Name;
            this.Code = item.Code;
            this.IsOccupy = item.IsOccupy;
            this.ParentId = item.ParentId;
            this.IsSystem = item.IsSystem;
            this.IsProject = item.IsProject;
            this.IsManagement = item.IsManagement;
            this.CostAttributeId = item.CostAttributeId;
            this.PaymentType = item.PaymentType;
            this.Remark = item.Remark;
            this.FullName = item.FullName;
        }

        public void Copy(SubjectEntity item, string projectId)
        {
            SetValue();
            
            this.ProjectId = projectId;
            this.BaseSubjectId = item.ID;
            Copy(item);
        }

        public void Copy(SubjectEntity item)
        {
            this.IsEnable = item.IsEnable;
            this.SortCode = item.SortCode;
            this.Name = item.Name;
            this.Code = item.Code;
            this.ParentId = item.ParentId;
            this.IsSystem = item.IsSystem;
            this.IsProject = item.IsProject;
            this.IsManagement = item.IsManagement;
            this.CostAttributeId = item.CostAttributeId;
            this.IsEnable = item.IsEnable;
            this.PaymentType = item.PaymentType;
            this.Remark = item.Remark;
            this.FullName = item.FullName;
            
        }

        public void Copy(ProjectSubjectEntity item)
        {
            SetValue();
            this.IsEnable = item.IsEnable;
            this.IsShow = item.IsShow;
            this.ProjectId = item.ProjectId;
            this.BaseSubjectId = item.BaseSubjectId;
            this.ParentId = item.ParentId;
            this.SortCode = item.SortCode;
            this.Name = item.Name;
            this.Code = item.Code;
            this.IsOccupy = item.IsOccupy;
            this.ParentId = item.ParentId;
            this.IsSystem = item.IsSystem;
            this.IsProject = item.IsProject;
            this.IsManagement = item.IsManagement;
            this.CostAttributeId = item.CostAttributeId;
            this.PaymentType = item.PaymentType;
            this.Remark = item.Remark;
            this.FullName = item.FullName;
        }
    }
    public class ProjectSubjectEntity : BaseSubjectModle
    {
        /// <summary>
        /// 預算ID
        /// </summary>
        public string ConstructionBudgetId { get; set; }
        /// <summary>
        /// 科目类型-- 1 材料 2 费用
        /// </summary>
        [Column("Category")]
        public int? Category { get; set; }

    }

    /// <summary>
    /// 中标价科目实体
    /// </summary>
    public class Zbj_SubjectEntity : BaseSubjectModle
    {

    }

    /// <summary>
    /// 投标预算科目实体
    /// </summary>
    public class Tbys_SubjectEntity : BaseSubjectModle
    {
    }

    /// <summary>
    /// 投标报价科目实体
    /// </summary>
    public class Tbbj_SubjectEntity : BaseSubjectModle
    {
    }
}
