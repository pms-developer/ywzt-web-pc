﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangeJie.Application.Project.BaseInfo.Subject;
using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;
using HuNanChangJie.Util;

namespace HuNanChangeJie.Application.Project.ProjectSubject
{
    public class ProjectSubjectBll : IProjectSubject
    {
        private ProjectSubjectService service = new ProjectSubjectService();

        private ICache cache = CacheFactory.CaChe();
        private string cacheKey;

        public void Copy(string currentProjectId, string targetProjectId)
        {
            try
            {
                service.Copy(currentProjectId, targetProjectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public ResultInfo Delete(string BaseSubjectId,string projectId)
        {
            try
            {
                var result = new ResultInfo();
                var isOccupy = service.CheckIsOccupy(BaseSubjectId);
                if (isOccupy)
                {
                    result.Flag = false;
                    result.Info = "当前科目或其下级科目已被占用，无法删除";
                    return result;
                }
                else
                {
                    service.Delete(BaseSubjectId, projectId);
                }
                result.Flag = true;
                result.Info = "删除成功";
                return result;

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetList(string projectId)
        {
            try
            {
                List<ProjectSubjectEntity> list;
                cacheKey = $"hncjpms_projectSubject_{projectId}";
                list = cache.Read<List<ProjectSubjectEntity>>(cacheKey, CacheId.ProjectSubject);
                if (list?.Count == 0 || list == null)
                {
                    return service.GetList(projectId);
                }
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetListByBudetId(string budgetId)
        {
            try
            {
                List<ProjectSubjectEntity> list;
                cacheKey = $"hncjpms_projectSubject_{budgetId}";
                list = cache.Read<List<ProjectSubjectEntity>>(cacheKey, CacheId.ProjectSubject);
                if (list?.Count == 0 || list == null)
                {
                    return service.GetListByBudetId(budgetId);
                }
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetList(IEnumerable<string> projectSubjectIdList)
        {
            try
            {
                return service.GetList(projectSubjectIdList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetSubjectList(string projectId)
        {
            try
            {
                var subjectList = new List<ProjectSubjectEntity>();
                var list = GetList(projectId);
                var query = from item in list
                            where item.PaymentType == "out"
                            select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }


        public void Insert(string projectId, string subjectIds)
        {
            try
            {
                if (subjectIds.Contains(","))
                {
                    var ids = subjectIds.Split(',');
                    ISubjectBLL subjectBll = new SubjectBLL();
                    var baesList = subjectBll.GetList();
                    var insertList = new List<ProjectSubjectEntity>();
                    for (var i = 0; i < ids.Length; i++)
                    {
                        var baseInfo = baesList.First(j => j.ID == ids[i]);
                        if (baseInfo == null) continue;

                        var isExist = service.CheckExistByBaseSubjectId(ids[i], projectId);
                        if (isExist) continue;

                        var item = new ProjectSubjectEntity();
                        item.Copy(baseInfo, projectId);
                        insertList.Add(item);
                    }
                    if (insertList.Count == 0) return;

                    Save(insertList, projectId);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public void Save(List<ProjectSubjectEntity> subjectList, string projectId)
        {
            try
            {
                service.Save(subjectList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public ProjectSubjectEntity GetEntity(string baseSubjectId, string projectID)
        {
            try
            {
                return service.GetEntity(baseSubjectId, projectID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public void UpdateInfo(ProjectSubjectEntity projectSubjectInfo)
        {
            try
            {
                service.UpdateInfo(projectSubjectInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }
    }
}
