﻿using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.ProjectSubject
{
    public interface IProjectSubject
    {
        /// <summary>
        /// 删除科目
        /// </summary>
        /// <param name="BaseSubjectId"></param>
        ResultInfo Delete(string BaseSubjectId,string projectId);

        /// <summary>
        /// 复制其他项目科目
        /// </summary>
        /// <param name="cuurentProjectId"></param>
        /// <param name="targetProjectId"></param>
        void Copy(string cuurentProjectId,string targetProjectId);
        IEnumerable<ProjectSubjectEntity> GetSubjectList(string projectId);

        /// <summary>
        /// 保存科目
        /// </summary>
        /// <param name="subjectList"></param>
        /// <param name="projectId"></param>
        void Save(List<ProjectSubjectEntity> subjectList,string projectId);
        void Insert(string projectId, string subjectIds);

        /// <summary>
        /// 获取项目科目集合
        /// </summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        IEnumerable<ProjectSubjectEntity> GetList(string projectId);

        /// <summary>
        /// 获取项目科目集合
        /// </summary>
        /// <param name="budgetId">项目ID</param>
        /// <returns></returns>
        IEnumerable<ProjectSubjectEntity> GetListByBudetId(string budgetId);
    }
}
