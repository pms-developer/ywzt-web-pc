﻿using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.ProjectSubject
{
    public class ProjectSubjectService : RepositoryFactory
    {
        public void Delete(string BaseSubjectId, string projectId)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                var projectInfo = BaseRepository().FindEntity<ProjectSubjectEntity>(i => i.BaseSubjectId == BaseSubjectId && i.ProjectId == projectId);
                if (projectInfo != null)
                {
                    var projectList = new List<ProjectSubjectEntity>();
                    projectList.Add(projectInfo);
                    LoadProjectChilds(projectList, projectInfo.BaseSubjectId, projectInfo.ProjectId);
                    db.Delete(projectList);
                }

                var tbysInfo = BaseRepository().FindEntity<Tbys_SubjectEntity>(i => i.BaseSubjectId == BaseSubjectId && i.ProjectId == projectId);
                if (tbysInfo != null)
                {
                    var tbysList = new List<Tbys_SubjectEntity>();
                    tbysList.Add(tbysInfo);
                    LoadTbysChilds(tbysList, projectInfo.BaseSubjectId, projectInfo.ProjectId);
                    db.Delete(tbysList);
                }

                var tbbjInfo = BaseRepository().FindEntity<Tbbj_SubjectEntity>(i => i.BaseSubjectId == BaseSubjectId && i.ProjectId == projectId);
                if (tbbjInfo != null)
                {
                    var tbbjList = new List<Tbbj_SubjectEntity>();
                    tbbjList.Add(tbbjInfo);
                    LoadTbbjChilds(tbbjList, projectInfo.BaseSubjectId, projectInfo.ProjectId);
                    db.Delete(tbbjList);
                }

                var zbjInfo = BaseRepository().FindEntity<Zbj_SubjectEntity>(i => i.BaseSubjectId == BaseSubjectId && i.ProjectId == projectId);
                if (zbjInfo != null)
                {
                    var zbjList = new List<Zbj_SubjectEntity>();
                    zbjList.Add(zbjInfo);
                    LoadZbjChilds(zbjList, projectInfo.BaseSubjectId, projectInfo.ProjectId);
                    db.Delete(zbjList);
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public void Copy(string currentProjectId, string targetProjectId)
        {
            try
            {
                var targetList = BaseRepository().FindList<ProjectSubjectEntity>(i => i.ProjectId == targetProjectId);
                var newList = new List<ProjectSubjectEntity>();
                foreach (var item in targetList)
                {
                    var info = new ProjectSubjectEntity();
                    info.Copy(item, currentProjectId);
                    newList.Add(info);
                }
                BaseRepository().Insert(newList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetList(IEnumerable<string> projectSubjectIdList)
        {
            try
            {
                return BaseRepository().FindList<ProjectSubjectEntity>(i => projectSubjectIdList.Contains(i.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        /// <summary>
        /// 是否被占用
        /// </summary>
        /// <param name="BaseSubjectId"></param>
        /// <returns></returns>
        public bool CheckIsOccupy(string BaseSubjectId)
        {
            var projectInfo = BaseRepository().FindEntity<ProjectSubjectEntity>(i => i.BaseSubjectId == BaseSubjectId);
            if (projectInfo.IsOccupy == true) return true;
            var projectList = new List<ProjectSubjectEntity>();
            LoadProjectChilds(projectList, projectInfo.BaseSubjectId, projectInfo.ProjectId);
            var projectChlidInfo = projectList.Find(i => i.IsOccupy == true);

            var tbysInfo = BaseRepository().FindEntity<Tbys_SubjectEntity>(i => i.BaseSubjectId == BaseSubjectId);
            if (tbysInfo.IsOccupy == true) return true;
            var tbysList = new List<Tbys_SubjectEntity>();
            LoadTbysChilds(tbysList, projectInfo.BaseSubjectId, projectInfo.ProjectId);
            var tbysChlidInfo = tbysList.Find(i => i.IsOccupy == true);

            var tbbjInfo = BaseRepository().FindEntity<Tbbj_SubjectEntity>(i => i.BaseSubjectId == BaseSubjectId);
            if (tbbjInfo.IsOccupy == true) return true;
            var tbbjList = new List<Tbbj_SubjectEntity>();
            LoadTbbjChilds(tbbjList, projectInfo.BaseSubjectId, projectInfo.ProjectId);
            var tbbjChlidInfo = tbysList.Find(i => i.IsOccupy == true);

            var zbjInfo = BaseRepository().FindEntity<Zbj_SubjectEntity>(i => i.BaseSubjectId == BaseSubjectId);
            if (zbjInfo.IsOccupy == true) return true;
            var zbjList = new List<Zbj_SubjectEntity>();
            LoadZbjChilds(zbjList, projectInfo.BaseSubjectId, projectInfo.ProjectId);
            var zbjChlidInfo = tbysList.Find(i => i.IsOccupy == true);

            if (zbjChlidInfo == null && tbbjChlidInfo == null && tbysChlidInfo == null && projectChlidInfo == null) return false;

            return true;
        }

        private void LoadProjectChilds(List<ProjectSubjectEntity> list, string parentId, string projectId)
        {
            var subList = BaseRepository().FindList<ProjectSubjectEntity>(i => i.ParentId == parentId && i.ProjectId == projectId);
            if (subList.Count() > 0)
            {
                list.AddRange(subList);
                foreach (var item in subList)
                {
                    var info = BaseRepository().FindEntity<ProjectSubjectEntity>(i => i.ID == item.ID);
                    LoadProjectChilds(list, info.BaseSubjectId, projectId);
                }
            }
        }

        public void UpdateInfo(ProjectSubjectEntity projectSubjectInfo)
        {
            try
            {
                BaseRepository().Update(projectSubjectInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public ProjectSubjectEntity GetEntity(string baseSubjectId, string projectID)
        {
            try
            {
                return BaseRepository().FindEntity<ProjectSubjectEntity>(i => i.BaseSubjectId == baseSubjectId && i.ProjectId == projectID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        private void LoadTbbjChilds(List<Tbbj_SubjectEntity> list, string parentId, string projectId)
        {
            var subList = BaseRepository().FindList<Tbbj_SubjectEntity>(i => i.ParentId == parentId && i.ProjectId == projectId);
            if (subList.Count() > 0)
            {
                list.AddRange(subList);
                foreach (var item in subList)
                {
                    var info = BaseRepository().FindEntity<Tbbj_SubjectEntity>(i => i.ID == item.ID);
                    LoadTbbjChilds(list, info.BaseSubjectId, projectId);
                }
            }
        }

        private void LoadTbysChilds(List<Tbys_SubjectEntity> list, string parentId, string projectId)
        {
            var subList = BaseRepository().FindList<Tbys_SubjectEntity>(i => i.ParentId == parentId && i.ProjectId == projectId);
            if (subList.Count() > 0)
            {
                list.AddRange(subList);
                foreach (var item in subList)
                {
                    var info = BaseRepository().FindEntity<Tbys_SubjectEntity>(i => i.ID == item.ID);
                    LoadTbysChilds(list, info.BaseSubjectId, projectId);
                }
            }
        }

        private void LoadZbjChilds(List<Zbj_SubjectEntity> list, string parentId, string projectId)
        {
            var subList = BaseRepository().FindList<Zbj_SubjectEntity>(i => i.ParentId == parentId && i.ProjectId == projectId);
            if (subList.Count() > 0)
            {
                list.AddRange(subList);
                foreach (var item in subList)
                {
                    var info = BaseRepository().FindEntity<Zbj_SubjectEntity>(i => i.ID == item.ID);
                    LoadZbjChilds(list, info.BaseSubjectId, projectId);
                }
            }
        }

        public void Save(List<ProjectSubjectEntity> subjectList)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                db.Insert(subjectList);
                var tbysList = new List<Tbys_SubjectEntity>();
                var tbbjList = new List<Tbbj_SubjectEntity>();
                var zbjlList = new List<Zbj_SubjectEntity>();
                foreach (var item in subjectList)
                {
                    var tbys = new Tbys_SubjectEntity();
                    tbys.Copy(item);
                    tbysList.Add(tbys);

                    var tbbj = new Tbbj_SubjectEntity();
                    tbbj.Copy(item);
                    tbbjList.Add(tbbj);

                    var zjb = new Zbj_SubjectEntity();
                    zjb.Copy(item);
                    zbjlList.Add(zjb);
                }
                db.Insert(tbysList);
                db.Insert(tbbjList);
                db.Insert(zbjlList);
                db.Commit();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetList(string projectId)
        {
            try
            {
                var sql = "select b.Name as CostAttributeName,a.* from Project_Subject a left join Base_CostAttribute b on a.CostAttributeId = b.id where projectId=@projectId";
                return BaseRepository().FindList<ProjectSubjectEntity>(sql, new { projectId }).OrderBy(i => i.Code); ;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetListByBudetId(string budgetId)
        {
            try
            {
                var sql = "select  a.* from project_subject a join project_constructionbudget b on a.ConstructionBudgetId = b.ID where b.ID=@budgetId";
                return BaseRepository().FindList<ProjectSubjectEntity>(sql, new { budgetId }).OrderBy(i => i.Code); ;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetList(string projectId, List<SubjectEntity> baseList)
        {
            try
            {
                var info = new ProjectSubjectEntity();
                var baseinfo = BaseRepository().dbcontext.Entry(info);
                var list = BaseRepository().FindList<ProjectSubjectEntity>(i => i.ProjectId == projectId);
                //list =(from data in list join list2 on list.)
                return null;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public bool CheckExistByBaseSubjectId(string subjectId, string projectId)
        {
            try
            {
                var info = BaseRepository().FindEntity<ProjectSubjectEntity>(i => i.BaseSubjectId == subjectId && i.ProjectId == projectId);
                return info != null;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }
    }
}
