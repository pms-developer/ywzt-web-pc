﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.IBll
{
    public interface ITeamBll
    {
        List<TeamEntity> GetTeamList();

        void SaveTeam(List<TeamEntity> teamList);
    }
}
