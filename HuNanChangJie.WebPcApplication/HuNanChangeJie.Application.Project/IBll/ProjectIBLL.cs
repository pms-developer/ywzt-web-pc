﻿using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.Model;
using HuNanChangJie.Util;

namespace HuNanChangeJie.Application.Project.IBll
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-04 15:18
    /// 描 述：项目基本信息
    /// </summary>
    public interface ProjectIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<ProjectEntity> GetPageList(XqPagination pagination, string queryJson, UserInfo userInfo);

        IEnumerable<ProjectEntity> GetAllProjectList();

        /// <summary>
        /// 获取项目名称
        /// </summary>
        /// <returns></returns>
        object GetProjectName();
        /// <summary>
        /// 获取Base_CJ_Project表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        ProjectEntity GetProjectEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);

        string AuditForm(string keyValue);


        string UnAuditForm(string keyValue);


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, string type, ProjectEntity entity);
        ProjectEntity GetCustomerInfo(string projectId);

        string InsertConcernEntity(string project_id);

        string DeleteConcernEntity(string cid);
        #endregion

    }
}
