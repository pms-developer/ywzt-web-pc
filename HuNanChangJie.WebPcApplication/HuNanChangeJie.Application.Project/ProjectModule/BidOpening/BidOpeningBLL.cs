﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-18 12:15
    /// 描 述：开标登记
    /// </summary>
    public class BidOpeningBLL : BidOpeningIBLL
    {
        private BidOpeningService bidOpeningService = new BidOpeningService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_BidOpeningEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return bidOpeningService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_BidOpeningDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_BidOpeningDetailsEntity> GetProject_BidOpeningDetailsList(string keyValue)
        {
            try
            {
                return bidOpeningService.GetProject_BidOpeningDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_BidOpening表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_BidOpeningEntity GetProject_BidOpeningEntity(string keyValue)
        {
            try
            {
                return bidOpeningService.GetProject_BidOpeningEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_BidOpeningDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_BidOpeningDetailsEntity GetProject_BidOpeningDetailsEntity(string keyValue)
        {
            try
            {
                return bidOpeningService.GetProject_BidOpeningDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                bidOpeningService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_BidOpeningEntity entity,List<Project_BidOpeningDetailsEntity> project_BidOpeningDetailsList,string deleteList,string type)
        {
            try
            {
                bidOpeningService.SaveEntity(keyValue, entity,project_BidOpeningDetailsList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
