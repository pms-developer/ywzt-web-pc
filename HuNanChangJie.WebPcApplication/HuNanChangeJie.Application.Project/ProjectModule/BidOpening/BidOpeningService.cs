﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-18 12:15
    /// 描 述：开标登记
    /// </summary>
    public class BidOpeningService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_BidOpeningEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.BidDate,
                t.Address,
                t.OpenDate,
                t.WinBidDate,
                t.IsWinBid,
                t.WinBidPrice,
                t.PredictCost,
                t.GrossProfitRate,
                t.OperationId,
                t.OperationDeparmentId,
                t.Remark
                ");
                strSql.Append("  FROM Project_BidOpening t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                var list=this.BaseRepository().FindList<Project_BidOpeningEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_BidOpeningDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_BidOpeningDetailsEntity> GetProject_BidOpeningDetailsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Project_BidOpeningDetailsEntity>(t=>t.BidOpeningId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_BidOpening表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_BidOpeningEntity GetProject_BidOpeningEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_BidOpeningEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_BidOpeningDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_BidOpeningDetailsEntity GetProject_BidOpeningDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_BidOpeningDetailsEntity>(t=>t.BidOpeningId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_BidOpeningEntity = GetProject_BidOpeningEntity(keyValue); 
                db.Delete<Project_BidOpeningEntity>(t=>t.ID == keyValue);
                db.Delete<Project_BidOpeningDetailsEntity>(t=>t.BidOpeningId == project_BidOpeningEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_BidOpeningEntity entity,List<Project_BidOpeningDetailsEntity> project_BidOpeningDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_BidOpeningEntityTmp = GetProject_BidOpeningEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_BidOpeningDetailsUpdateList= project_BidOpeningDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_BidOpeningDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_BidOpeningDetailsInserList= project_BidOpeningDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_BidOpeningDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.BidOpeningId = project_BidOpeningEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_BidOpeningDetailsEntity item in project_BidOpeningDetailsList)
                    {
                        item.BidOpeningId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
