﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    public class BidBudgetMaterialsEntity : MaterialsBaseEntity, IBidBudgetEntity
    {
        /// <summary>
        /// 投标预算ID
        /// </summary>
        public string ProjectBidBudgetId { get; set; }
    }
}
