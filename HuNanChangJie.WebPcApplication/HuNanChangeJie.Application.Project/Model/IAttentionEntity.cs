﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 项目管理接口
    /// </summary>
    public interface IAttentionEntity
    {
        /// <summary>
        /// ID
        /// </summary>
        string ID { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        string ProjectId { get; set; }


        /// <summary>
        /// 用户ID
        /// </summary>
        string UserId { get; set; }
    }
}
