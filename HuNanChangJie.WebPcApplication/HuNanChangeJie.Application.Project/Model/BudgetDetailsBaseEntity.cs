﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util.Attributes;
using Newtonsoft.Json;
using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    public class BudgetDetailsBaseEntity:IProjectEntity
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// 取值方式
        /// </summary>
        [Obsolete]
        public string ValueType { get; set; }

        /// <summary>
        ///  取值方式
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public ValueTypeEnum ValueTypeEunm
        {
            get
            {
                return (ValueTypeEnum)Enum.Parse(typeof(ValueTypeEnum), ValueType);
            }
            set
            {
                ValueType = value.ToString();
            }
        }

        /// <summary>
        /// 费率
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal Rate { get; set; }

        /// <summary>
        /// 公式
        /// </summary>
        public string Formula { get; set; }

        /// <summary>
        /// 预算金额、投标金额、中标金额
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal BudgetAmount { get; set; }

        /// <summary>
        /// 占比
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal Ratio { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }
    }
}
