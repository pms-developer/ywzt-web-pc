﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 施工预算工程量清单
    /// </summary>
    public class ConstructionBudgetQuantitiesEntity : QuantitiesBaseEntity, IConstructionBudgetEntity
    {
        /// <summary>
        /// 施工预算ID
        /// </summary>
        public string ProjectConstructionBudgetId { get; set; }
    }
}
