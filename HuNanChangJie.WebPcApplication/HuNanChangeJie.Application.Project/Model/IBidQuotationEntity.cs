﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    public interface IBidQuotationEntity
    {
        /// <summary>
        /// 投标报价ID
        /// </summary>
        string ProjectBidQuotationId { get; set; }
    }
}
