﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    public class BiddingPriceMaterialsEntity : MaterialsBaseEntity, IBiddingPriceEntity
    {
        /// <summary>
        /// 中标价ID
        /// </summary>
        public string ProjectBiddingPriceId { get; set; }
    }
}
