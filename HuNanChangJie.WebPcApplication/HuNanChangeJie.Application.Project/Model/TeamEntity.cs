﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 项目团队成员实体
    /// </summary>
    public class TeamEntity
    {
        public string RoleId { get; set; }

        public string RoleName { get; set; }

        public string ProjectId { get; set; }

        public List<TeamUser> UserList { get; set; }
    }

    public class TeamUser
    {
        public string UserId { get; set; }

        public string UserName { get; set; }
    }
}
