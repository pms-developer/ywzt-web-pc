﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 中标价工程量清单实体
    /// </summary>
    public class BiddingPriceQuantitiesEntity : QuantitiesBaseEntity, IBiddingPriceEntity
    {
        /// <summary>
        /// 中标价ID
        /// </summary>
        public string ProjectBiddingPriceId { get; set; }
    }
}
