﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 预算相关主表基实体
    /// </summary>
    public class BudgetBaseEntity : SysDefaultFieldsBaseEntity,IProjectEntity
    {

        /// <summary>
        /// 预算/报价/中标总金额
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// 投标负责人ID
        /// </summary>
        public string BidUserId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

    }
}
