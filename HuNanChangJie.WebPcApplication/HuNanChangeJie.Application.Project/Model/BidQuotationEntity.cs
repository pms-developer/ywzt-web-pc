﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 投标报价实体
    /// </summary>
    public class BidQuotationEntity:BudgetBaseEntity
    {
        /// <summary>
        /// 投标预算金额
        /// </summary>
        [NotMapped]
        public decimal BidBudgetPrice { get; set; }
    }
}
