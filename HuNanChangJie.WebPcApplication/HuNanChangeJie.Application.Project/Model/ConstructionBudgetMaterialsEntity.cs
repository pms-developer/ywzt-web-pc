﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 施工预算材料清单实体
    /// </summary>
    public class ConstructionBudgetMaterialsEntity : MaterialsBaseEntity, IConstructionBudgetEntity
    {
        /// <summary>
        /// 施工预算ID
        /// </summary>
        public string ProjectConstructionBudgetId { get; set; }
    }
}
