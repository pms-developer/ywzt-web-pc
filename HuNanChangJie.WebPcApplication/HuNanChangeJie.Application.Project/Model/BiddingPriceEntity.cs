﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 中标价实体
    /// </summary>
    public class BiddingPriceEntity:BudgetBaseEntity
    {
        /// <summary>
        /// 投标预算金额
        /// </summary>
        [NotMapped]
        public decimal BidBudgetPrice { get; set; }

        /// <summary>
        /// 投标报价金额
        /// </summary>
        [NotMapped]
        public decimal BidQuotationPrice { get; set; }
    }
}
