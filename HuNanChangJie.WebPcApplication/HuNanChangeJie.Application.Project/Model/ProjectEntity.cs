﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-04 15:18
    /// 描 述：项目基本信息
    /// </summary>
    public class ProjectEntity 
    {
        #region  实体成员
        /// <summary>
        /// 省
        /// </summary>
        [Column("PROVINCEID")]
        public string ProvinceID { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        [Column("CITYID")]
        public string CityID { get; set; }
        /// <summary>
        /// 区
        /// </summary>
        [Column("DISTRICT")]
        public string District { get; set; }
        /// <summary>
        /// 项目所在地
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }

        /// <summary>
        /// 项目编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 项目类型
        /// </summary>
        [Column("TYPE")]
        public string Type { get; set; }
        /// <summary>
        /// 项目阶段
        /// </summary>
        [Column("SECTION")]
        public string Section { get; set; }
        /// <summary>
        /// 业务员
        /// </summary>
        [Column("MARKETINGSTAFFID")]
        public string MarketingStaffID { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SortCode")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECT_ID")]
        public string Project_ID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 业务部门
        /// </summary>
        [Column("MARKETINGDEPARTMENTID")]
        public string MarketingDepartmentID { get; set; }
        /// <summary>
        /// 项目来源
        /// </summary>
        [Column("SOURCE")]
        public string Source { get; set; }
        /// <summary>
        /// 项目等级
        /// </summary>
        [Column("GRADE")]
        public string Grade { get; set; }
        /// <summary>
        /// 项目模式
        /// </summary>
        [Column("MODE")]
        public string Mode { get; set; }
        /// <summary>
        /// 项目说明
        /// </summary>
        [Column("DESCRIPTION")]
        public string Description { get; set; }
        /// <summary>
        /// 客户名称
        /// </summary>
        [Column("CUSTOMERID")]
        public string CustomerID { get; set; }
        /// <summary>
        /// 预估目标成本
        /// </summary>
        [Column("PRETARGETCOST")]
        public decimal? PreTargetCost { get; set; } = 0;
        /// <summary>
        /// 预估合同总价
        /// </summary>
        [Column("PRECONTRACTPRICE")]
        public decimal? PreContractPrice { get; set; } = 0;
        /// <summary>
        /// 预计预付款比例
        /// </summary>
        [Column("PREPAYRATIO")]
        public string PrePayRatio { get; set; }
        /// <summary>
        /// 预计签约日期
        /// </summary>
        [Column("PRECONTRACTDATE")]
        public DateTime? PreContractDate { get; set; }
        /// <summary>
        /// 预计签约金额
        /// </summary>
        [Column("PRESIGNPRICE")]
        public decimal? PreSignPrice { get; set; }
        /// <summary>
        /// 预计开工日期
        /// </summary>
        [Column("PRESTARTINGDATE")]
        public DateTime? PreStartingDate { get; set; }
        /// <summary>
        /// 预计完工日期
        /// </summary>
        [Column("PREFINISHDATE")]
        public DateTime? PreFinishDate { get; set; }
        /// <summary>
        /// 成交概率
        /// </summary>
        [Column("PROBABILITY")]
        public string Probability { get; set; }
        /// <summary>
        /// 最终用户
        /// </summary>
        [Column("FINALUSERID")]
        public string FinalUserID { get; set; }
        /// <summary>
        /// 客户经理
        /// </summary>
        [Column("ACCOUNTMANAGER")]
        public string AccountManager { get; set; }
        /// <summary>
        /// 大客户经理
        /// </summary>
        [Column("BIGACCOUNTMANAGER")]
        public string BigAccountManager { get; set; }
        /// <summary>
        /// 设计负责人
        /// </summary>
        [Column("DESIGNER")]
        public string Designer { get; set; }
        /// <summary>
        /// 投标负责人
        /// </summary>
        [Column("DIDDER")]
        public string Didder { get; set; }
        /// <summary>
        /// 项目经理
        /// </summary>
        [Column("PROJECTMANAGER")]
        public string ProjectManager { get; set; }

        /// <summary>
        /// 项目承包方式
        /// </summary>
        public string Xmcbfs { get; set; }

        /// <summary>
        /// 项目联系人
        /// </summary>
        public string Xmlxr { get; set; }

        /// <summary>
        /// 项目联系人电话
        /// </summary>
        public string Xmlxrhd { get; set; }

        /// <summary>
        /// 申请登记人
        /// </summary>
        public string Sqdjr { get; set; }

        /// <summary>
        /// 申请登记人电话
        /// </summary>
        public string Sqdjrdh { get; set; }

        /// <summary>
        /// 是否已验收
        /// </summary>
        public bool? IsAcceptance { get; set; }

        [Obsolete]
        public string AuditStatus { get; set; }



        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("PHONE")]
        public string Phone { get; set; }



        /// <summary>
        /// 身份证号码
        /// </summary>
        [Column("IDCARD")]
        public string IdCard { get; set; }



        /// <summary>
        /// 项目经理地址
        /// </summary>
        [Column("PROJECTMANAGEADDRESS")]
        public string ProjectManageAddress { get; set; }


        /// <summary>
        /// 联系人
        /// </summary>
        [Column("CUSTOMERCONTACTPEOPLE")]
        public string CustomerContactPeople { get; set; }


        /// <summary>
        /// 联系方式
        /// </summary>
        [Column("CUSTOMERCONTACT")]
        public string CustomerContact { get; set; }


        /// <summary>
        /// 施工班组
        /// </summary>
        [Column("CONSTRUCTIONTEAM")]
        public string ConstructionTeam { get; set; }


        /// <summary>
        /// 施工班组联系人
        /// </summary>
        [Column("CONSTRUCTIONTEAMCONTACTPEOPLE")]
        public string ConstructionTeamContactPeople { get; set; }


        /// <summary>
        /// 施工班组联系方式
        /// </summary>
        [Column("CONSTRUCTIONTEAMCONTACT")]
        public string ConstructionTeamContact { get; set; }


        /// <summary>
        /// 主材班组
        /// </summary>
        [Column("ADVOCATETEAM")]
        public string AdvocateTeam { get; set; }

        /// <summary>
        /// 主材班组联系人
        /// </summary>
        [Column("ADVOCATETEAMCONTACTPEOPLE")]
        public string AdvocateTeamContactPeople { get; set; }

        /// <summary>
        /// 主材班组联系方式
        /// </summary>
        [Column("ADVOCATETEAMCONTACT")]
        public string AdvocateTeamContact { get; set; }
        /// <summary>
        /// 电商平台
        /// </summary>
        /*[Column("PlatformId")]
        public string PlatformId { get; set; }*/

        /// <summary>
        /// 部门
        /// </summary>
        /*[Column("DepartmentId")]
        public string DepartmentId { get; set; }*/


        [NotMapped]
        public AuditStatus AuditStatusEnum
        {
            get
            {
                if (string.IsNullOrEmpty(AuditStatus))
                {
                    return HuNanChangJie.SystemCommon.AuditStatus.Normal;
                }
                else
                {
                    return (AuditStatus)Enum.Parse(typeof(AuditStatus), AuditStatus);
                }
            }
            set
            {
                AuditStatus = value.ToString();
            }
        }

        /// <summary>
        /// 项目状态
        /// </summary>
        [Column("STATE")]
        public int? State { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
        #region  扩展字段

        [NotMapped]
        public string CustomerName { get; set; }

        [NotMapped]
        public string PlatformName { get; set; }

        

        [NotMapped]
        public string ProjectType { get; set; }

        [NotMapped]
        public string CustomerAddress { get; set; }

        [NotMapped]
        public string CustomerPhone { get; set; }

        [NotMapped]
        public string CustomerPostCode { get; set; }

        [NotMapped]
        public string CustomerContacts { get; set; }


        /// <summary>
        /// 是否为关注
        /// </summary>
         [NotMapped]
        public string unConcern { get; set; }

        /// <summary>
        /// 关注ID
        /// </summary>
        [NotMapped]
        public string cID { get; set; }
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            //this.CreationName = userInfo.realName;
            //this.Company_ID = userInfo.companyId;
            //this.CompanyCode = userInfo.CompanyCode;
            //this.CompanyName = userInfo.CompanyName;
            //this.CompanyFullName = userInfo.CompanyFullName;
        }
        #endregion
    }
}

