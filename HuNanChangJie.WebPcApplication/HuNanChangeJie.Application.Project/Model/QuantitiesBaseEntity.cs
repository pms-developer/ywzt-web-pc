﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 工程量清单实体基类
    /// </summary>
    public class QuantitiesBaseEntity : IProjectEntity
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// 清单编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 清单名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目特征
        /// </summary>
        public string Feature { get; set; }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 工程量
        /// </summary>
        public decimal Quantities { get; set; }

        /// <summary>
        /// 综合单价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 合价
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
