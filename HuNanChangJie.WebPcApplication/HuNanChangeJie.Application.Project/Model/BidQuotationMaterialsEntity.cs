﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    public class BidQuotationMaterialsEntity : MaterialsBaseEntity, IBidQuotationEntity
    {
        /// <summary>
        /// 投标报价ID
        /// </summary>
        public string ProjectBidQuotationId { get; set; }
    }
}
