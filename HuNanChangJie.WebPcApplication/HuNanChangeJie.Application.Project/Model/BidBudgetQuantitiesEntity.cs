﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 投标工程量清单实体
    /// </summary>
    public class BidBudgetQuantitiesEntity : QuantitiesBaseEntity, IBidBudgetEntity
    {
        /// <summary>
        /// 投标预算ID
        /// </summary>
        public string ProjectBidBudgetId { get; set; }
    }
}
