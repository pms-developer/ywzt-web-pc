﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 材料清单基类实体
    /// </summary>
    public class MaterialsBaseEntity : IProjectEntity
    {
        /// <summary>
        /// ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// 清单编码
        /// </summary>
        public string ListCode { get; set; }

        /// <summary>
        /// 材料编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 材料名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 品牌
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// 规格型号
        /// </summary>
        public string ModelNumber { get; set; }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 预算单价、投标价、中标价
        /// </summary>
        public decimal BudgetPrice { get; set; }

        /// <summary>
        /// 合计金额
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// 销售价
        /// </summary>
        public decimal SellPrice { get; set; }

        /// <summary>
        /// 成本价
        /// </summary>
        public decimal CostPrice { get; set; }

        /// <summary>
        /// 市场价
        /// </summary>
        public decimal BazaarPrice { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

    }
}
