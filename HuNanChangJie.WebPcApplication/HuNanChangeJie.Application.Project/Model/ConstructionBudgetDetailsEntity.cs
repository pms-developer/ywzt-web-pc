﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    public class ConstructionBudgetDetailsEntity: BudgetDetailsBaseEntity, IConstructionBudgetEntity
    {
        /// <summary>
        /// 施工预算ID
        /// </summary>
        public string ProjectConstructionBudgetId { get; set; }

        /// <summary>
        /// 投标预算
        /// </summary>
        [NotMapped]
        public decimal BidBudgetPrice { get; set; }

        /// <summary>
        /// 投标报价
        /// </summary>
        [NotMapped]
        public decimal BidQuotationPrice { get; set; }

        /// <summary>
        /// 中标价
        /// </summary>
        [NotMapped]
        public decimal BiddingPrice { get; set; }
        
    }
}
