﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    public interface IBiddingPriceEntity
    {
        /// <summary>
        /// 中标价ID
        /// </summary>
        string ProjectBiddingPriceId { get; set; }
    }
}
