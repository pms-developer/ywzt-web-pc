﻿using HuNanChangJie.Util.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 施工预算实体
    /// </summary>
    public class ConstructionBudgetEntity:BudgetBaseEntity
    {
        /// <summary>
        /// 税金
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal Taxes { get; set; }

        /// <summary>
        /// 毛利率
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal GrossProfitRate { get; set; }

        /// <summary>
        /// 工程合同金额
        /// </summary
        [NotMapped]
        public decimal ConstractPrice { get; set; }
    }
}
