﻿using HuNanChangJie.Util.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    
    /// <summary>  
    /// 创 建：超级管理员 
    /// 日 期：2020-08-18 11:32 
    /// 描 述：人员及证照扣款记录 
    /// </summary> 
    public class Project_StaffRecordEntity
    {

        #region  实体成员 
        /// <summary> 
        /// ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("ID")]
        public string ID { get; set; }
        /// <summary> 
        /// 单据类型 
        /// </summary> 
        /// <returns></returns> 
        [Column("BILLTYPE")]
        public string BillType { get; set; }
        /// <summary> 
        /// 扣款金额 
        /// </summary> 
        /// <returns></returns> 
        [Column("AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Amount { get; set; }
        /// <summary>  
        /// 扣款日期 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATEDATE")]
        public DateTime? CreateDate { get; set; }
        /// <summary> 
        /// 项目Id 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary> 
        /// 人员或证照ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("STAFFID")]
        public string StaffId { get; set; }
        /// <summary> 
        /// 是否尾款（不足月金额） 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISLAST")]
        public bool? IsLast { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}
