﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    public interface IBidBudgetEntity
    {
        /// <summary>
        /// 投标预算ID
        /// </summary>
        string ProjectBidBudgetId { get; set; }
    }
}
