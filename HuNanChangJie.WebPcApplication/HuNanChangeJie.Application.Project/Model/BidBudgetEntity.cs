﻿using HuNanChangJie.Util.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    public class BidBudgetEntity:BudgetBaseEntity
    {
        /// <summary>
        /// 预计投标时间
        /// </summary>
        public DateTime BidDate { get; set; }

        /// <summary>
        /// 预算成本金额
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal CostAmount { get; set; }

        /// <summary>
        /// 利润率
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal ProfitRate { get; set; }

    }
}
