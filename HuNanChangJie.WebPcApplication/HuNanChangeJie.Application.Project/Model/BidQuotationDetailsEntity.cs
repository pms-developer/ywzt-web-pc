﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 投标报价单详情实体
    /// </summary>
    public class BidQuotationDetailsEntity: BudgetDetailsBaseEntity, IBidQuotationEntity
    {
        /// <summary>
        /// 项目投标报价ID
        /// </summary>
        public string ProjectBidQuotationId { get; set; }

        /// <summary>
        /// 投标预算金额
        /// </summary>
        [NotMapped]
        public decimal BidBudgetPrice { get; set; }
    }
}
