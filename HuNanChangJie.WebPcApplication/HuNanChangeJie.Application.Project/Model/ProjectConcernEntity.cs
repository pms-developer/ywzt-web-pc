﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 我关注的项目实体
    /// </summary>
    public class ProjectConcernEntity : IProjectEntity
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 项目主键
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// 用户主键
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 关注时间
        /// </summary>
        public DateTime CreationDate { get; set; }
    }
}
