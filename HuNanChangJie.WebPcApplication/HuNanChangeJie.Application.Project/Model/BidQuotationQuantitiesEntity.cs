﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 投标报价工程量清单实体
    /// </summary>
    public class BidQuotationQuantitiesEntity : QuantitiesBaseEntity, IBidQuotationEntity
    {
        /// <summary>
        /// 投标报价ID
        /// </summary>
        public string ProjectBidQuotationId { get; set; }
    }
}
