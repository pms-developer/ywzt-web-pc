﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 中标价详情实体
    /// </summary>
    public class BiddingPriceDetailsEntity: BudgetDetailsBaseEntity, IBiddingPriceEntity
    {
        /// <summary>
        /// 项目中标价ID
        /// </summary>
        public string ProjectBiddingPriceId { get; set; }

        /// <summary>
        /// 投标预算金额
        /// </summary>
        [NotMapped]
        public decimal BidBudgetPrice { get; set; }

        /// <summary>
        /// 投标报价金额
        /// </summary>
        [NotMapped]
        public decimal BidQuotationPrice { get; set; }
    }
}
