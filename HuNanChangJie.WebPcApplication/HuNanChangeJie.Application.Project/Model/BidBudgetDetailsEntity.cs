﻿using HuNanChangJie.Util.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 投标预算详情实体
    /// </summary>
    public class BidBudgetDetailsEntity: BudgetDetailsBaseEntity, IBidBudgetEntity
    {
        /// <summary>
        /// 项目投标预算ID
        /// </summary>
        public string ProjectBidBudgetId { get; set; }

        /// <summary>
        /// 预算成本
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal BudgetCost { get; set; }
    }
}
