﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangeJie.Application.Project.Model
{
    /// <summary>
    /// 施工预算工程量清单接口
    /// </summary>
    public interface IConstructionBudgetEntity
    { 
        /// <summary>
        /// 施工预算ID
        /// </summary>
        string ProjectConstructionBudgetId { get; set; }
    }
}
