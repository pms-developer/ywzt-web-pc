﻿using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.IBll;
using HuNanChangJie.Util;
using HuNanChangeJie.Application.Project.Model;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangeJie.Application.Project.ProjectBaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-04 15:18
    /// 描 述：项目基本信息
    /// </summary>
    public class ProjectBLL : ProjectIBLL
    {
        private ProjectService projectService = new ProjectService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<ProjectEntity> GetPageList(XqPagination pagination, string queryJson, UserInfo userInfo)
        {
            try
            {
                return projectService.GetPageList(pagination, queryJson,userInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取项目名称
        /// </summary>
        /// <returns></returns>
        public object GetProjectName()
        {
            try
            {
                return projectService.GetProjectName();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
            /// <summary>
            /// 获取Base_CJ_Project表实体数据
            /// <param name="keyValue">主键</param>
            /// <summary>
            /// <returns></returns>
            public ProjectEntity GetProjectEntity(string keyValue)
        {
            try
            {
                return projectService.GetProjectEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public string AuditForm(string keyValue)
        {
            try
            {
               return projectService.AuditForm(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public string UnAuditForm(string keyValue)
        {
            try
            {
                  return projectService.UnAuditForm(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue,string type, ProjectEntity entity)
        {
            try
            {
                projectService.SaveEntity(keyValue, type, entity);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("ProjectCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public string GetSupplierId(string keyValue)
        {
            try
            {
                return "";
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public ProjectEntity GetCustomerInfo(string projectId)
        {
            try
            {
                return projectService.GetCustomerInfo(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<ProjectEntity> GetAllProjectList()
        {
            try
            {
                return projectService.GetAllProjectList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public string InsertConcernEntity(string project_id)
        {
            try
            {
                return projectService.InsertConcernEntity(project_id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public string DeleteConcernEntity(string cid)
        {
            try
            {
                return projectService.DeleteConcernEntity(cid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
