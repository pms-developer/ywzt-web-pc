﻿
using Dapper;
using HuNanChangeJie.Application.Project.Model;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.SystemCommon.ModuleInfoAuthorization;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HuNanChangeJie.Application.Project.ProjectBaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-04 15:18
    /// 描 述：项目基本信息
    /// </summary>
    public class ProjectService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<ProjectEntity> GetPageList(XqPagination pagination, string queryJson, UserInfo userInfo)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                distinct(t.ID) ID,
                t.Code,
                t.ProjectName,
                t.CustomerID,
                t.Type,
                t.MarketingStaffID,
                t.MarketingDepartmentID,
                t.Section,
                t.Address,
                t.Company_ID,
                t.Grade,
                t.Mode,
                t.Probability,
                t.AuditStatus,
                t.AuditorName,
                t.Workflow_ID,
                t.Auditor_ID,t.CreationDate,
                t.CompanyName as CompanyName,
                cc.Name as ProjectType,
                cp.ID as cid,
                (case when cp.ID is null then 0 else 1 end) as unConcern,p.f_areaname ProvinceID,c.f_areaname CityID,d.f_areaname District 
                ");
                strSql.Append("  FROM Base_CJ_Project t ");
                /*strSql.Append("  left join base_platform as bb on t.PlatformId=bb.F_PlatformId ");*/
                strSql.Append("  left join Base_ProjectType as cc on t.Type=cc.ID ");
                strSql.Append($" left join (select * from Base_CJ_ProjectConcern where userid='{userInfo.userId}')  cp on t.ID=cp.ProjectID ");

                strSql.Append("  left join Base_Area as p on t.ProvinceID=p.f_areaid ");

                strSql.Append("  left join Base_Area as c on t.CityID=c.f_areaid ");

                strSql.Append("  left join Base_Area as d on t.District=d.f_areaid ");
                if (!userInfo.isSystem)
                {
                    #region 项目角色
                    strSql.Append(" left join  ");
                    strSql.Append("(select Max(UserID) UserID,ProjectId from Base_CJ_ProjectRoleRelation group by UserID,ProjectId) r ");
                    strSql.Append(" on t.ID=r.ProjectId  ");
                    #endregion
                    #region 项目授权Sql
                    var userTbAlias = "UserTb";
                    var cmpyTbAlias = "CmpyTb";
                    var depaTbAlias = "DepaTb";
                    var roleTbAlias = "RoleTb";
                    var infoAuth = new ModuleInfoAuthorization(userInfo.userId, userInfo.companyIds, userInfo.departmentIds, userInfo.roleIdList, userTbAlias, cmpyTbAlias, depaTbAlias, roleTbAlias);
                    infoAuth.CreateChildSql(strSql, "t", userTbAlias, ObjectType.User, ModuleType.Project);
                    infoAuth.CreateChildSql(strSql, "t", cmpyTbAlias, ObjectType.Company, ModuleType.Project);
                    infoAuth.CreateChildSql(strSql, "t", depaTbAlias, ObjectType.Department, ModuleType.Project);
                    infoAuth.CreateChildSql(strSql, "t", roleTbAlias, ObjectType.Role, ModuleType.Project);
                    #endregion
                    strSql.Append($"  WHERE (r.UserId='{userInfo.userId}' ");
                    strSql.Append($" or t.Creation_Id='{userInfo.userId}' ");//自己创建 
                    strSql.Append(infoAuth.AuthorizationSql);
                    strSql.Append(") ");
                }
                else
                {
                    strSql.Append(" WHERE 1=1  ");
                }

                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }

                if (!queryParam["CityID"].IsEmpty())
                {
                    dp.Add("CityID", "%" + queryParam["CityID"].ToString() + "%", DbType.String);
                    strSql.Append(" AND c.F_AreaName Like @CityID ");
                }

                /*if (!queryParam["CustomerName"].IsEmpty())
                {
                    dp.Add("CustomerName", "%" + queryParam["CustomerName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND bb.FullName Like @CustomerName ");
                }*/

                if (!queryParam["ProjectName"].IsEmpty())
                {
                    dp.Add("ProjectName", "%" + queryParam["ProjectName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ProjectName Like @ProjectName ");
                }

                if (!queryParam["urltype"].IsEmpty())
                {
                    var urltype = queryParam["urltype"].ToString();
                    if (urltype == "isconcern")
                    {
                        var ProjectIds = this.BaseRepository().FindList<ProjectConcernEntity>(a => a.UserId == userInfo.userId).Select(w => w.ProjectId).ToList();
                        string str = "'" + string.Join("','", ProjectIds.ToArray()) + "'";
                        //dp.Add("ID", str, DbType.String);
                        strSql.Append(string.Format($" AND t.ID in ({str})"));
                    }

                    if (urltype == "ismarketingstaffid")
                    {
                        dp.Add("marketingstaffid", userInfo.userId, DbType.String);
                        strSql.Append(" AND t.marketingstaffid = @marketingstaffid");
                    }

                    if (urltype == "iscreation")
                    {
                        dp.Add("creation_id", userInfo.userId, DbType.String);
                        strSql.Append(" AND t.creation_id = @creation_id");
                    }

                    if (urltype == "isprojectmanager")
                    {
                        dp.Add("ProjectManager", userInfo.userId, DbType.String);
                        strSql.Append(" AND t.ProjectManager = @ProjectManager");
                    }

                    if (urltype == "isparty")
                    {
                        strSql.Append(string.Format(@"AND t.ID in (select a.moduleInfoId from [dbo].[Base_ModuletInfoAuthorization] a,Base_User c,Base_CJ_ProjectRoleRelation d
                                            where(a.ObjectId = d.RoleId and c.F_UserId = d.UserId) or
                                            (a.ObjectId = c.F_DepartmentId or a.ObjectId = c.F_UserId or a.ObjectId = c.F_CompanyId) and
                                            c.F_UserId = '" + userInfo.userId + "')"));
                    }
                }

                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectId = @ProjectId ");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type = @Type ");
                }
                if (!queryParam["Section"].IsEmpty())
                {
                    dp.Add("Section", queryParam["Section"].ToString(), DbType.String);
                    strSql.Append(" AND t.Section = @Section ");
                }
                if (!queryParam["MarketingStaffID"].IsEmpty())
                {
                    dp.Add("MarketingStaffID", queryParam["MarketingStaffID"].ToString(), DbType.String);
                    strSql.Append(" AND t.MarketingStaffID = @MarketingStaffID ");
                }
                if (!queryParam["MarketingDepartmentID"].IsEmpty())
                {
                    dp.Add("MarketingDepartmentID", queryParam["MarketingDepartmentID"].ToString(), DbType.String);
                    strSql.Append(" AND t.MarketingDepartmentID = @MarketingDepartmentID ");
                }
                if (!queryParam["Company_ID"].IsEmpty())
                {
                    dp.Add("Company_ID", queryParam["Company_ID"].ToString(), DbType.String);
                    strSql.Append(" AND t.Company_ID = @Company_ID ");
                }
                if (!queryParam["Mode"].IsEmpty())
                {
                    dp.Add("Mode", queryParam["Mode"].ToString(), DbType.String);
                    strSql.Append(" AND t.Mode = @Mode ");
                }
                if (!queryParam["Grade"].IsEmpty())
                {
                    dp.Add("Grade", queryParam["Grade"].ToString(), DbType.String);
                    strSql.Append(" AND t.Grade = @Grade ");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type = @Type ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                return this.BaseRepository().FindList<ProjectEntity>(strSql.ToString(), dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<ProjectEntity> GetAllProjectList()
        {
            try
            {
                return BaseRepository().FindList<ProjectEntity>(i => i.AuditStatus == "2");
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取项目名称
        /// </summary>
        /// <returns></returns>
        public object GetProjectName()
        {
            try
            {

                return BaseRepository().FindTable(@"SELECT [ProjectName] FROM [dbo].[Base_CJ_Project]");
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public ProjectEntity GetCustomerInfo(string projectId)
        {
            try
            {
                var sql = @"select a.*,
                            b.FullName as CustomerName,
                            b.Address as CustomerAddress,
                            b.Phone as CustomerPhone,
                            b.Postcode as CustomerPostCode,
                            b.Contacts as CustomerContacts 
                            from Base_CJ_Project as a left join Base_Customer as b
                            on a.CustomerID=b.ID where a.id=@projectId";
                return BaseRepository().FindEntity<ProjectEntity>(sql, new { projectId });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Base_CJ_Project表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public ProjectEntity GetProjectEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<ProjectEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<ProjectEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public string AuditForm(string keyValue)
        {
            try
            {
                string message = "";

                ProjectEntity projectEntity = BaseRepository().FindEntity<ProjectEntity>(keyValue);

                if (projectEntity.AuditStatus == "2")
                {
                    message = "当前记录已审核通过,无法再次审核！";
                }
                else
                {
                    projectEntity.AuditStatus = "2";
                    this.BaseRepository().Update(projectEntity);
                }

                return message;

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public string UnAuditForm(string keyValue)
        {
            try
            {
                string message = "";

                ProjectEntity projectEntity = BaseRepository().FindEntity<ProjectEntity>(keyValue);

                if (projectEntity.AuditStatus != "2")
                {
                    message = "当前记录不能进行反审核操作！";
                }
                else
                {
                    projectEntity.AuditStatus = "4";
                    this.BaseRepository().Update(projectEntity);
                }
                return message;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, string type, ProjectEntity entity)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 添加实体数据（新增）
        /// <param name="project_id">项目id</param>
        /// <summary>
        /// <returns></returns>
        public string InsertConcernEntity(string project_id)
        {
            try
            {
                var aa = this.BaseRepository().FindList<ProjectConcernEntity>(w => w.ProjectId == project_id).Where(a => a.UserId == LoginUserInfo.UserInfo.userId).ToList();
                if (aa.Count > 0)
                {
                    return "-1";
                }
                ProjectConcernEntity projectConcernEntity = new ProjectConcernEntity();
                projectConcernEntity.ProjectId = project_id;
                projectConcernEntity.UserId = LoginUserInfo.UserInfo.userId;
                projectConcernEntity.ID = Guid.NewGuid().ToString();
                projectConcernEntity.CreationDate = DateTime.Now;
                return this.BaseRepository().Insert(projectConcernEntity).ToString();

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="cid">主键</param>
        /// <summary>
        /// <returns></returns>
        public string DeleteConcernEntity(string cid)
        {
            try
            {
                return this.BaseRepository().Delete<ProjectConcernEntity>(t => t.ID == cid).ToString();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
