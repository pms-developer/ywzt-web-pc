﻿using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web
{
    /// <summary>
    /// 日 期：2017.03.07
    /// 描 述：对HtmlHelper类进行扩展
    /// </summary>
    public static class HtmlHelperExtensions
    {
        private static ICache cache = CacheFactory.CaChe();
        /// <summary>
        /// 往页面中写入js文件
        /// </summary>
        /// <param name="htmlHelper">需要扩展对象</param>
        /// <param name="jsFiles">文件路径</param>
        /// <returns></returns>
        public static MvcHtmlString AppendJsFile(this HtmlHelper htmlHelper, params string[] jsFiles)
        {
            string jsFile = "";
            foreach (string file in jsFiles)
            {
                if (jsFile != "")
                {
                    jsFile += ",";
                }
                jsFile += file;
            }
            string jsStr = "";
            if (Config.GetValue("JsCompressorCache") == "true")
            {
                jsStr = cache.Read<string>(jsFile, CacheId.jscss);
            }
            if (string.IsNullOrEmpty(jsStr))
            {
                jsStr = JsCssHelper.ReadJSFile(jsFiles);
                cache.Write<string>(jsFile, jsStr, CacheId.jscss);
            }
            
            StringBuilder content = new StringBuilder();
            string jsFormat = "<script>{0}</script>";
            
            content.AppendFormat(jsFormat, jsStr);
            return new MvcHtmlString(content.ToString());
        }
        /// <summary>
        /// 往页面中写入css样式
        /// </summary>
        /// <param name="htmlHelper">需要扩展对象</param>
        /// <param name="cssFiles">文件路径</param>
        /// <returns></returns>
        public static MvcHtmlString AppendCssFile(this HtmlHelper htmlHelper, params string[] cssFiles)
        {
            string cssFile = "";
            foreach (string file in cssFiles)
            {
                if (cssFile != "")
                {
                    cssFile += ",";
                }
                cssFile += file;
            }
            string cssStr = "";
            if (Config.GetValue("JsCompressorCache") == "true")
            {
               cssStr = cache.Read<string>(cssFile, CacheId.jscss);
            }
            

            if (string.IsNullOrEmpty(cssStr))
            {
               var url =  HttpContext.Current.Request.ApplicationPath;


               cssStr = JsCssHelper.ReadCssFile(cssFiles);
                if (url != "/")
                {
                    cssStr = cssStr.Replace("url(", "url(" + url);
                }


                cache.Write<string>(cssFile, cssStr, CacheId.jscss);
            }
            StringBuilder content = new StringBuilder();
            string cssFormat = "<style>{0}</style>";
            content.AppendFormat(cssFormat, cssStr);
            return new MvcHtmlString(content.ToString());
        }

        #region  权限模块
        /// <summary>
        /// 设置当前页面地址
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns></returns>
        
        public static MvcHtmlString SetCurrentUrl(this HtmlHelper htmlHelper)
        {
            //HttpRequest hr = HttpContext.Current.Request;
            //string baseUrl = hr.Url.AbsolutePath;
            //string queryUrl = "";
            //var urlNoFilter = System.Configuration.ConfigurationManager.AppSettings["UrlNoFilter"];
            //if (urlNoFilter != null)
            //{
            //    string[] filterArr = urlNoFilter.ToString().ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            //    foreach (string key in hr.QueryString)
            //    {
            //        string value = hr.QueryString.Get(key);
            //        if (filterArr.Contains(key.ToLower()))
            //        {
            //            if (!string.IsNullOrEmpty(queryUrl))
            //                queryUrl += "&";
            //            queryUrl += $"{key}={value}";
            //        }
            //    }
            //}

            //string currentUrl = baseUrl + (string.IsNullOrEmpty(queryUrl) ? "" : ("?" + queryUrl));
            string currentUrl = (string)WebHelper.GetHttpItems("currentUrl");
            string currentModuleId = "";
            if (!string.IsNullOrEmpty(currentUrl))
            {
                //Regex regex = new Regex(".*?(moduleId=.*?)&?.*");
                //Match match = regex.Match(currentUrl);
                //if (match.Success)
                //{

                //}
                //Regex regex1 = new Regex(".*?moduleId=(.*?)&?.*");
                //Match match1 = regex1.Match(currentUrl);
                //if (match1.Success)
                //{

                //}

                NameValueCollection nvc = ExtractQueryParams(currentUrl.ToLower());
                if (!string.IsNullOrEmpty(nvc["moduleid"]))
                    currentModuleId = nvc["moduleid"];
            }


            var removeParamList = new List<string> { "moduleId", "projectId" };
            if (currentUrl.Contains("?"))
            {
                var strs = currentUrl.Split('?');
                foreach (var item in removeParamList)
                {
                    if (!currentUrl.Contains(item)) continue;
                    currentUrl = AnalysisUrl(currentUrl, item);
                }
            }

            
            return new MvcHtmlString("<script>var mkCurrentUrl='" + currentUrl + "';var mkCurrentModuleId='" + currentModuleId + "';var mkModuleButtonList;var mkModuleColumnList;var mkModule;</script>");
        }
        private static NameValueCollection ExtractQueryParams(string url)
        {
            int startIndex = url.IndexOf("?");
            NameValueCollection values = new NameValueCollection();

            if (startIndex <= 0)
                return values;

            string[] nameValues = url.Substring(startIndex + 1).Split('&');

            foreach (string s in nameValues)
            {
                string[] pair = s.Split('=');

                string name = pair[0];
                string value = string.Empty;

                if (pair.Length > 1)
                    value = pair[1];

                values.Add(name, value);
            }

            return values;
        }
        private static string AnalysisUrl(string url,string paramName)
        {
            var newUrl = "";
            if (!url.Contains(paramName)) return newUrl;
            var s = url.Split(new[] { paramName }, StringSplitOptions.None);
            var frontStr = s[0];
            var behindStr = s[1];
                
            if (frontStr.LastIndexOf('&') >= 0)//分割后去除末尾的"&"符号
            {
                frontStr=frontStr.Substring(0, frontStr.Length - 1);
            }
            newUrl+= frontStr;
            if (behindStr.Contains("&"))//如果后半部分包含其他参数
            {
                var index = behindStr.IndexOf('&');
                behindStr = behindStr.Substring(index, behindStr.Length- index);
                newUrl +=behindStr;
            }
            return newUrl;
        }

        #endregion
    }
}