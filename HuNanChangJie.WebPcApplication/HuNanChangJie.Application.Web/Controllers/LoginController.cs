﻿using HuNanChangJie.Application.Organization;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Util;
using HuNanChangJie.Util.Operat;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using DingTalk.Api.Response;
using DingTalk.Api;
using DingTalk.Api.Request;
using System;
using NPOI.SS.Formula.Functions;

namespace HuNanChangJie.Application.Web.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.03.08
    /// 描 述：登录控制器
    /// </summary>
    [HandlerLogin(FilterMode.Ignore)]
    public class LoginController : MvcControllerBase
    {
        #region  模块对象
        private UserIBLL userBll = new UserBLL();
        #endregion
        DataSourceIBLL dataSourceIBLL = new DataSourceBLL();

        #region  视图功能
        /// <summary>
        /// 默认页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Default()
        {
            return RedirectToAction("Index", "Login");
        }
        /// <summary>
        /// 登录页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.errornum = OperatorHelper.Instance.GetCurrentErrorNum();
            ViewBag.dingerrormessage = "";
            if (Request["code"] != null && Request["state"] != null)
            {
                string companyId = Request["state"].ToString();
                string code = Request["code"].ToString();

                CompanyBLL companyBLL = new CompanyBLL();
                CompanyEntity companyEntity = companyBLL.GetEntity(companyId);
                if (companyEntity != null)
                {
                    OapiSnsGetuserinfoBycodeResponse response = new OapiSnsGetuserinfoBycodeResponse();
                    try
                    {
                        string qrAppId = companyEntity.DingQrAppkey;
                        string qrAppSecret = companyEntity.DingQrAppsecret;
                        if (string.IsNullOrWhiteSpace(qrAppId) || string.IsNullOrWhiteSpace(qrAppSecret))
                        {
                            ViewBag.dingerrormessage = "请配置公司钉钉扫码登录信息！";
                        }
                        else
                        {
                            DefaultDingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/sns/getuserinfo_bycode");
                            OapiSnsGetuserinfoBycodeRequest req = new OapiSnsGetuserinfoBycodeRequest();
                            req.TmpAuthCode = code;
                            response = client.Execute(req, qrAppId, qrAppSecret);

                            if (response.Errcode == 0)
                            {
                                string unionId = response.UserInfo.Unionid;

                                UserEntity userEntity = userBll.GetEntityByUnionIdOrOpenId(unionId);
                                if (userEntity != null)
                                {
                                    OperatorHelper.Instance.AddLoginUser(userEntity.F_Account, "hncjpms_PC", null);//写入缓存信息

                                    LogEntity logEntity = new LogEntity();
                                    logEntity.F_CategoryId = 1;
                                    logEntity.F_OperateTypeId = ((int)OperationType.Login).ToString();
                                    logEntity.F_OperateType = EnumAttribute.GetDescription(OperationType.Login);
                                    logEntity.F_OperateAccount = userEntity.F_Account + "(" + userEntity.F_RealName + ")";
                                    logEntity.F_OperateUserId = !string.IsNullOrEmpty(userEntity.F_UserId) ? userEntity.F_UserId : userEntity.F_Account;
                                    logEntity.F_Module = Config.GetValue("SoftName");
                                    logEntity.F_ExecuteResult = 1;
                                    logEntity.F_ExecuteResultJson = "钉钉扫码登录成功";
                                    logEntity.WriteLog();
                                    OperatorHelper.Instance.ClearCurrentErrorNum();

                                    return Redirect("/Home/Index");
                                }
                                else
                                {
                                    ViewBag.dingerrormessage = "请在钉钉工作台应用绑定用户后使用扫码登录";
                                }
                            }
                            else
                            {
                                ViewBag.dingerrormessage = response.Errmsg;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ViewBag.dingerrormessage = e.Message;
                    }
                }
                else
                {
                    ViewBag.dingerrormessage = "未查询到登录二维码匹配公司";
                }
            }

            var listCompany = new CompanyBLL().GetList().Where(m => !string.IsNullOrEmpty(m.DingQrAppkey) && !string.IsNullOrEmpty(m.DingQrAppsecret)).ToList();

            ViewBag.Company = listCompany;
            return View("Top");
        }
        #endregion

        #region  获取数据
        /// <summary>
        /// 生成验证码
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult VerifyCode()
        {
            return File(new VerifyCode().GetVerifyCode(), @"image/Gif");
        }

        /// <summary>
        /// 获取用户登录信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        [HandlerLogin(FilterMode.Enforce)]
        public ActionResult GetUserInfo()
        {
            var data = LoginUserInfo.Get();
            data.imUrl = Config.GetValue("IMUrl");
            data.password = null;
            data.secretkey = null;
            return JsonResult(data);
        }
        /// <summary>
        /// 获取多语言分类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetLanguageType() {
            var data= dataSourceIBLL.GetDataTable("LanguageType", "");
            return JsonResult(data);
        }
        #endregion

        #region  提交数据
        /// <summary>
        /// 安全退出
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [HandlerLogin(FilterMode.Enforce)]
        public ActionResult OutLogin()
        {
            var userInfo = LoginUserInfo.Get();
            LogEntity logEntity = new LogEntity();
            logEntity.F_CategoryId = 1;
            logEntity.F_OperateTypeId = ((int)OperationType.Exit).ToString();
            logEntity.F_OperateType = EnumAttribute.GetDescription(OperationType.Exit);
            logEntity.F_OperateAccount = userInfo.account + "(" + userInfo.realName + ")";
            logEntity.F_OperateUserId = userInfo.userId;
            logEntity.F_ExecuteResult = 1;
            logEntity.F_ExecuteResultJson = "退出系统";
            logEntity.F_Module = Config.GetValue("SoftName");
            logEntity.WriteLog();
            Session.Abandon();                                          //清除当前会话
            Session.Clear();                                            //清除当前浏览器所有Session
            OperatorHelper.Instance.EmptyCurrent();
            return Success("退出系统");
        }
        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="verifycode">验证码</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [HandlerValidateAntiForgeryToken]
        public ActionResult CheckLogin(string username, string password, string verifycode)
        {

            int error = OperatorHelper.Instance.GetCurrentErrorNum();
            if (error >= 3)
            {
                #region  验证码验证
                verifycode = Md5Helper.Encrypt(verifycode.ToLower(), 16);
                if (Session["session_verifycode"].IsEmpty() || verifycode != Session["session_verifycode"].ToString())
                {
                    return Fail("验证码错误");
                }
                #endregion
            }
            
            #region  内部账户验证
            UserEntity userEntity = userBll.CheckLogin(username, password);

            #region  写入日志
            LogEntity logEntity = new LogEntity();
            logEntity.F_CategoryId = 1;
            logEntity.F_OperateTypeId = ((int)OperationType.Login).ToString();
            logEntity.F_OperateType = EnumAttribute.GetDescription(OperationType.Login);
            logEntity.F_OperateAccount = username + "(" + userEntity.F_RealName + ")";
            logEntity.F_OperateUserId = !string.IsNullOrEmpty(userEntity.F_UserId) ? userEntity.F_UserId : username;
            logEntity.F_Module = Config.GetValue("SoftName");
            #endregion

            if (!userEntity.LoginOk)//登录失败
            {
                //写入日志
                logEntity.F_ExecuteResult = 0;
                logEntity.F_ExecuteResultJson = "登录失败:" + userEntity.LoginMsg;
                logEntity.WriteLog();
                int num = OperatorHelper.Instance.AddCurrentErrorNum();
                return Fail(userEntity.LoginMsg, num);
            }
            else
            {
                OperatorHelper.Instance.AddLoginUser(userEntity.F_Account, "hncjpms_PC", null);//写入缓存信息
                //写入日志
                logEntity.F_ExecuteResult = 1;
                logEntity.F_ExecuteResultJson = "登录成功";
                logEntity.WriteLog();
                OperatorHelper.Instance.ClearCurrentErrorNum();
                return Success("登录成功");
            }
            #endregion
        }
        #endregion
    }
}