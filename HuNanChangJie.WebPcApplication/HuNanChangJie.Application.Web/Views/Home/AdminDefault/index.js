﻿/*
 * 日 期：2017.03.16
 * 描 述：经典风格皮肤	
 */
var bootstrap = function ($, Minke) {
    "use strict";
    // 菜单操作
    var meuns = {
        init: function () {


            this.load();
            this.bind();
        },
        load: function () {
            var modulesTree = Minke.clientdata.get(['modulesTree']);
            // 第一级菜单
            var parentId = '0';
            var modules = modulesTree[parentId] || [];
            var $firstmenus = $('<ul class="mk-first-menu-list"></ul>');
            for (var i = 0, l = modules.length; i < l; i++) {
                var item = modules[i];
                if (item.F_IsMenu == 1) {
                    var $firstMenuItem = $('<li></li>');
                    if (!!item.F_Description) {
                        $firstMenuItem.attr('title', item.F_Description);
                    }
                    var menuItemHtml = '<a id="' + item.F_ModuleId + '" href="javascript:void(0);" class="mk-menu-item">';
                    menuItemHtml += '<i class="' + item.F_Icon + ' mk-menu-item-icon"></i>';
                    menuItemHtml += '<span class="mk-menu-item-text">' + item.F_FullName + '</span>';
                    menuItemHtml += '<span class="mk-menu-item-arrow"></span></a>';
                    $firstMenuItem.append(menuItemHtml);
                    // 第二级菜单
                    var secondModules = modulesTree[item.F_ModuleId] || [];
                    var $secondMenus = $('<ul class="mk-second-menu-list"></ul>');
                    var secondMenuHad = false;
                    for (var j = 0, sl = secondModules.length ; j < sl; j++) {
                        var secondItem = secondModules[j];
                        if (secondItem.F_IsMenu == 1) {
                            secondMenuHad = true;
                            var $secondMenuItem = $('<li></li>');
                            if (!!secondItem.F_Description) {
                                $secondMenuItem.attr('title', secondItem.F_Description);
                            }
                            var secondItemHtml = '<a id="' + secondItem.F_ModuleId + '" href="javascript:void(0);" class="mk-menu-item" >';
                            secondItemHtml += '<i class="' + secondItem.F_Icon + ' mk-menu-item-icon"></i>';
                            secondItemHtml += '<span class="mk-menu-item-text">' + secondItem.F_FullName + '</span>';
                            secondItemHtml += '</a>';

                            $secondMenuItem.append(secondItemHtml);
                            // 第三级菜单
                            var threeModules = modulesTree[secondItem.F_ModuleId] || [];
                            var $threeMenus = $('<ul class="mk-three-menu-list"></ul>');
                            var threeMenuHad = false;
                            for (var m = 0, tl = threeModules.length ; m < tl; m++) {
                                var threeItem = threeModules[m];
                                if (threeItem.F_IsMenu == 1) {
                                    threeMenuHad = true;
                                    var $threeMenuItem = $('<li></li>');
                                    $threeMenuItem.attr('title', threeItem.F_FullName);
                                    var threeItemHtml = '<a id="' + threeItem.F_ModuleId + '" href="javascript:void(0);" class="mk-menu-item" >';
                                    threeItemHtml += '<i class="' + threeItem.F_Icon + ' mk-menu-item-icon"></i>';
                                    threeItemHtml += '<span class="mk-menu-item-text">' + threeItem.F_FullName + '</span>';
                                    threeItemHtml += '</a>';
                                    $threeMenuItem.append(threeItemHtml);
                                    $threeMenus.append($threeMenuItem);
                                }
                            }
                            if (threeMenuHad) {
                                $secondMenuItem.addClass('mk-meun-had');
                                $secondMenuItem.append($threeMenus);
                            }
                            $secondMenus.append($secondMenuItem);
                        }
                    }
                    if (secondMenuHad) {
                        $firstMenuItem.append($secondMenus);
                    }
                    $firstmenus.append($firstMenuItem);
                }
            }
            $('#frame_menu').html($firstmenus);


            // 语言包翻译
            $('.mk-menu-item-text').each(function () {
                var $this = $(this);
                var text = $this.text();
                Minke.language.get(text, function (text) {
                    $this.text(text);
                    $this.parent().parent().attr('title', text);
                });
            });
        },
        bind: function () {
            $("#frame_menu").mkscroll();
            $("#frame_menu .mk-first-menu-list > li").hover(function (e) {// 一级菜单选中的时候判断二级菜单的位置
                //$('#frame_menu').width(4000);
                var $secondMenu = $(this).find('.mk-second-menu-list');
                var length = $secondMenu.find('li').length;
                if (length > 0) {
                    $secondMenu.css('top', '0px');
                    var secondMenuTop = $(this).offset().top + $secondMenu.height() + 23;
                    var bodyHeight = $(window).height();
                    if (secondMenuTop > bodyHeight) {
                        $secondMenu.css('top', '-' + (secondMenuTop - bodyHeight) + 'px');
                    }
                }
            }, function (e) {
                $('#frame_menu').width(80);
            });

            $("#frame_menu .mk-second-menu-list > li.mk-meun-had").hover(function (e) {// 二级菜单选中的时候判断三级菜单的位置
                var $ul = $(this).find('.mk-three-menu-list');
                $ul.css('top', '-9px');
                var ulTop = $(this).offset().top + $ul.height() + 23;
                var bodyHeight = $(window).height();
                if (ulTop > bodyHeight) {
                    $ul.css('top', '-' + (ulTop - bodyHeight) + 'px');
                }
            });

            // 添加点击事件
            $('#frame_menu .mk-menu-item').on('click', function () {
                var $obj = $(this);
                var id = $obj.attr('id');
                var _module = Minke.clientdata.get(['modulesMap', id]);
                switch (_module.F_Target) {
                    case 'iframe':// 窗口
                        if (Minke.validator.isNotNull(_module.F_UrlAddress).code) {
                            Minke.frameTab.open(_module);
                        }
                        else {

                        }
                        break;
                }
            });
        }
    };
    meuns.init();


    var companyMap = {};
    var departmentMap = {};
    var userMap = {};

    var imUserId = '';

    var getHeadImg = function (user) {
        var url = top.$.rootUrl;
        switch (user.img) {
            case '0':
                url += '/Content/images/head/on-girl.jpg';
                break;
            case '1':
                url += '/Content/images/head/on-boy.jpg';
                break;
            default:
                url += '/OrganizationModule/User/GetImg?userId=' + user.id;
                break;
        }
        return url;
    };
    // 发送聊天信息
    var sendMsg = function (msg, time) {
        var loginInfo = Minke.clientdata.get(['userinfo']);
        Minke.clientdata.getAsync('user', {
            key: loginInfo.userId,
            callback: function (data, op) {
                data.id = op.key;
                var _html = '\
                <div class="im-me">\
                    <div class="headimg"><img src="'+ getHeadImg(data) + '"></div>\
                    <div class="arrow"></div>\
                    <span class="content">'+ msg + '</span>\
                </div>';

                if (time && time != '') {
                    $('.mk-im-msgcontent .mk-scroll-box').append('<div class="im-time">' + time + '</div>');
                }

                $('.mk-im-msgcontent .mk-scroll-box').append(_html);
                $('.mk-im-msgcontent').mkscrollSet('moveBottom');
            }
        });
    };
    // 接收聊天消息
    var revMsg = function (userId, msg, time) {
        Minke.clientdata.getAsync('user', {
            key: userId,
            callback: function (data, op) {
                data.id = op.key;
                var _html = '\
                <div class="im-other">\
                    <div class="headimg"><img src="'+ getHeadImg(data) + '"></div>\
                    <div class="arrow"></div>\
                    <span class="content">'+ msg + '</span>\
                </div>';

                if (time && time != '') {
                    $('.mk-im-msgcontent .mk-scroll-box').append('<div class="im-time">' + time + '</div>');
                }

                $('.mk-im-msgcontent .mk-scroll-box').append(_html);
                $('.mk-im-msgcontent').mkscrollSet('moveBottom');
            }
        });
    };


    // 即时通讯
    var im = {
        init: function () {
            this.bind();
            this.load();
        },
        load: function () {
            // 获取下公司列表
            Minke.clientdata.getAllAsync('company', {
                callback: function (data) {
                    $.each(data, function (_id, _item) {
                        companyMap[_item.parentId] = companyMap[_item.parentId] || [];
                        _item.id = _id;
                        companyMap[_item.parentId].push(_item);
                    });
                    var $list = $('#im_content_userlist .mk-scroll-box');
                    $.each(companyMap["0"], function (_index, _item) {
                        var _html = '\
                            <div class="mk-im-company-item">\
                                <div class="mk-im-item-name mk-im-company" data-value="'+ _item.id + '"  data-deep="0" >\
                                    <i class="fa fa-angle-right"></i>'+ _item.name + '\
                                </div>\
                            </div>';
                        $list.append(_html);

                    });
                    // 获取部门列表
                    Minke.clientdata.getAllAsync('department', {
                        callback: function (data) {
                            $.each(data, function (_id, _item) {
                                _item.id = _id;
                                if (_item.parentId == "0") {
                                    departmentMap[_item.companyId] = departmentMap[_item.companyId] || [];
                                    departmentMap[_item.companyId].push(_item);
                                }
                                else {
                                    departmentMap[_item.parentId] = departmentMap[_item.parentId] || [];
                                    departmentMap[_item.parentId].push(_item);
                                }
                            });
                            // 获取人员数据
                            Minke.clientdata.getAllAsync('user', {
                                callback: function (data) {
                                    $.each(data, function (_id, _item) {
                                        _item.id = _id;
                                        if (_item.departmentId) {
                                            userMap[_item.departmentId] = userMap[_item.departmentId] || [];
                                            userMap[_item.departmentId].push(_item);
                                        }
                                        else if (_item.companyId) {
                                            userMap[_item.companyId] = userMap[_item.companyId] || [];
                                            userMap[_item.companyId].push(_item);
                                        }
                                    });
                                    // 获取最近联系人列表
                                    Minke.im.getContacts(function (data) {
                                        var $userList = $('#immsg_userlist .mk-scroll-box');
                                        $.each(data, function (_index, _item) {
                                            var html = '\
                                            <div class="userlist-item '+ (_item.F_IsRead == '1' ? 'imHasMsg' : '') + '" data-value="' + _item.F_OtherUserId + '"  >\
                                                <div class="photo"><img src="'+ top.$.rootUrl + '/Content/images/head/on-boy.jpg" >\
                                                <div class="point"></div></div>\
                                            </div>';
                                            $userList.append(html);
                                            Minke.clientdata.getAsync('user', {
                                                key: _item.F_OtherUserId,
                                                index: _index,
                                                callback: function (data, op) {
                                                    var $item = $userList.find('[data-value="' + op.key + '"]');
                                                    $item.attr('title', data.name);
                                                    data.id = op.key;
                                                    $item.find('img').attr('src', getHeadImg(data));
                                                    if (op.index == 0) {
                                                        $item.trigger('click');
                                                    }
                                                    $item = null;
                                                }
                                            });
                                        });
                                    });
                                }
                            }); 
                        }
                    }); 

                }
            });
        },
        bind: function () {
            // 最近消息 与 联系人之间的切换
            $('.mk-im-title .title-item').on('click', function () {
                var $this = $(this);
                if (!$this.hasClass('active')) {
                    $('.mk-im-body>.active').removeClass('active');
                    $('.mk-im-title>.active').removeClass('active');
                    $this.addClass('active');
                    var v = $this.attr('data-value');
                    $('#' + v).addClass('active');
                }
            });
            // 打开关闭聊天窗
            $('.mk-im-bell').on('click', function () {
                var $this = $(this);
                if ($this.hasClass('open')) {
                    $this.removeClass('open');
                    $('.mk-im-body').removeClass('open');
                }
                else {
                    $this.addClass('open');
                    $('.mk-im-bell .point').hide();
                    $('.mk-im-body').addClass('open');
                }
            });
            // 联系人
            $('#im_content_userlist').mkscroll();
            $('#immsg_userlist').mkscroll();
            $('.mk-im-msgcontent').mkscroll();

            // 联系人列表点击
            $('#im_content_userlist .mk-scroll-box').on('click', function (e) {
                e = e || window.event;
                var et = e.target || e.srcElement;
                var $et = $(et);

                if (et.tagName == 'IMG' || et.tagName == 'I') {
                    $et = $et.parent();
                }

                if ($et.hasClass('mk-im-company')) {// 点击公司项
                    // 判断是否是打开的状态
                    if ($et.hasClass('open')) {
                        $et.removeClass('open');
                        $et.parent().find('.mk-im-user-list').remove();

                    } else {
                        var id = $et.attr('data-value');
                        var deep = parseInt($et.attr('data-deep'));
                        var $list = $('<div class="mk-im-user-list" ></div>');
                        $list.css({ 'padding-left': '10px' });
                        var flag = false;
                        // 加载员工
                        var loginInfo = Minke.clientdata.get(['userinfo']);
                        $.each(userMap[id] || [], function (_index, _item) {
                            if (_item.id != loginInfo.userId) {
                                var _html = '\
                                <div class="mk-im-company-item">\
                                    <div class="mk-im-item-name mk-im-user" data-value="'+ _item.id + '" >\
                                         <img src="'+ getHeadImg(_item) + '" >' + _item.name + '\
                                    </div>\
                                </div>';
                                $list.append(_html);
                                flag = true;
                            }
                        });
                        // 加载部门
                        $.each(departmentMap[id] || [], function (_index, _item) {
                            var _html = '\
                            <div class="mk-im-company-item">\
                                <div class="mk-im-item-name mk-im-department" data-value="'+ _item.id + '"  data-deep="' + (deep + 1) + '" >\
                                    <i class="fa fa-angle-right"></i>'+ _item.name + '\
                                </div>\
                            </div>';
                            $list.append(_html);
                            flag = true;
                        });
                        // 加载下属公司
                        $.each(companyMap[id] || [], function (_index, _item) {
                            var _html = '\
                            <div class="mk-im-company-item">\
                                <div class="mk-im-item-name mk-im-company" data-value="'+ _item.id + '"  data-deep="' + (deep + 1) + '" >\
                                    <i class="fa fa-angle-right"></i>'+ _item.name + '\
                                </div>\
                            </div>';
                            $list.append(_html);
                            flag = true;
                        });



                        if (flag) {
                            $et.parent().append($list);
                        }
                        $et.addClass('open');
                    }
                    return false;
                }
                else if ($et.hasClass('mk-im-department')) {
                    // 判断是否是打开的状态
                    if ($et.hasClass('open')) {
                        $et.removeClass('open');
                        $et.parent().find('.mk-im-user-list').remove();

                    } else {
                        var id = $et.attr('data-value');
                        var deep = parseInt($et.attr('data-deep'));
                        var $list = $('<div class="mk-im-user-list" ></div>');
                        $list.css({ 'padding-left': '10px' });
                        var flag = false;
                        // 加载员工
                        var loginInfo = Minke.clientdata.get(['userinfo']);
                        $.each(userMap[id] || [], function (_index, _item) {
                            if (_item.id != loginInfo.userId) {
                                var _html = '\
                                <div class="mk-im-company-item">\
                                    <div class="mk-im-item-name mk-im-user" data-value="'+ _item.id + '" >\
                                         <img src="'+ getHeadImg(_item) + '" >' + _item.name + '\
                                    </div>\
                                </div>';
                                $list.append(_html);
                                flag = true;
                            }
                        });
                        // 加载部门
                        $.each(departmentMap[id] || [], function (_index, _item) {
                            var _html = '\
                            <div class="mk-im-company-item">\
                                <div class="mk-im-item-name mk-im-department" data-value="'+ _item.id + '"  data-deep="' + (deep + 1) + '" >\
                                    <i class="fa fa-angle-right"></i>'+ _item.name + '\
                                </div>\
                            </div>';
                            $list.append(_html);
                            flag = true;
                        });

                        if (flag) {
                            $et.parent().append($list);
                        }
                        $et.addClass('open');

                    }

                }
                else if ($et.hasClass('mk-im-user')){
                    // 如果是用户列表
                    // 1.打开聊天窗口
                    // 2.添加一条最近联系人数据（如果没有添加的话）
                    // 3.获取最近的20条聊天数据或者最近的聊天信息

                    var id = $et.attr('data-value');
                    var $userList = $('#immsg_userlist .mk-scroll-box');
                    var $userItem = $userList.find('[data-value="' + id + '"]');

                    // 更新下最近的联系人列表数据

                    $('.mk-im-title .title-item').eq(0).trigger('click');

                    $('#im_msglist .mk-im-right').removeClass('mk-im-nouser');

                    imUserId = id;
                    if ($userItem.length > 0) {
                        $userList.prepend($userItem);
                        $userItem.trigger('click');
                    }
                    else {
                        $userList.find('.active').removeClass('active');
                        var imgurl = $et.find('img').attr('src');

                        var _html = '\
                            <div class="userlist-item" data-value="'+ id + '" >\
                                <div class="photo"><img src="'+ imgurl + '" >\
                                <div class="point"></div></div>\
                            </div>';

                        $userList.prepend(_html);

                        // 获取人员数据
                        Minke.clientdata.getAsync('user', {
                            key: id,
                            callback: function (data, op) {
                                $userList.find('[data-value="' + op.key + '"]').attr('title', data.name).addClass('active');
                                $('#im_msglist .mk-im-right .mk-im-touser').text(data.name);
                            }
                        });
                        Minke.im.addContacts(id);
                        $('.mk-im-msgcontent .mk-scroll-box').html('');
                    }
                    $('#im_input').val('');
                    $('#im_input').select();
                }
            });
            // 最近联系人列表点击
            $('#immsg_userlist .mk-scroll-box').on('click', function (e) {
                e = e || window.event;
                var et = e.target || e.srcElement;
                var $et = $(et);

                if (!$et.hasClass('userlist-item')) {
                    $et = $et.parents('.userlist-item');
                }
                if ($et.length > 0) {
                    if (!$et.hasClass('active')) {
                        var name = $et.attr('title');
                        imUserId = $et.attr('data-value');

                        $('#immsg_userlist .mk-scroll-box .active').removeClass('active');
                        $et.addClass('active');
                        $('#im_msglist .mk-im-right .mk-im-touser').text(name);
                        $('#im_msglist .mk-im-right').removeClass('mk-im-nouser');

                        $('#im_input').val('');
                        $('#im_input').select();

                        $('.mk-im-msgcontent .mk-scroll-box').html('');
                        // 获取聊天信息
                        Minke.im.getMsgList(imUserId, function (data) {
                            var len = data.length;
                            if (len > 0) {
                                for (var i = len - 1; i >= 0; i--) {
                                    var _item = data[i];
                                    Minke.clientdata.getAsync('user', {
                                        key: _item.userId,
                                        msg: _item.content,
                                        time: _item.time,
                                        callback: function (data, op) {
                                            data.id = op.key;
                                            var loginInfo = Minke.clientdata.get(['userinfo']);
                                            var _html = '\
                                            <div class="'+ (loginInfo.userId == op.key ? 'im-me' : 'im-other') + '">\
                                                <div class="headimg"><img src="'+ getHeadImg(data) + '"></div>\
                                                <div class="arrow"></div>\
                                                <span class="content">'+ op.msg + '</span>\
                                            </div>';
                                            $('.mk-im-msgcontent .mk-scroll-box').prepend(_html);
                                            $('.mk-im-msgcontent .mk-scroll-box').prepend('<div class="im-time">' + op.time + '</div>');
                                        }
                                    });
                                }
                                $('.mk-im-msgcontent').mkscrollSet('moveBottom');
                            }
                        }, $et.hasClass('imHasMsg'));
                        $et.removeClass('imHasMsg');
                        Minke.im.updateContacts(imUserId);
                    }
                }
            });
            // 联系人搜索
            $('.mk-im-search input').on("keypress", function (e) {
                e = e || window.event;
                if (e.keyCode == "13") {
                    var $this = $(this);
                    var keyword = $this.val();
                    var $list = $('#im_content_userlist .mk-scroll-box');
                    $list.html("");
                    if (keyword) {
                        Minke.clientdata.getAllAsync('user', {
                            callback: function (data) {
                                var loginInfo = Minke.clientdata.get(['userinfo']);
                                $.each(data, function (_index, _item) {
                                    if (_index != loginInfo.userId) {
                                        if (_item.name.indexOf(keyword) != -1) {
                                            _item.id = _index;
                                            var _html = '\
                                            <div class="mk-im-company-item">\
                                                <div class="mk-im-item-name mk-im-user" data-value="'+ _item.id + '" >\
                                                     <img src="'+ getHeadImg(_item) + '" >' + _item.name + '\
                                                </div>\
                                            </div>';
                                            $list.append(_html);
                                        }
                                    }
                                });
                            }
                        }); 
                    }
                    else {
                        $.each(companyMap["0"], function (_index, _item) {
                            var _html = '\
                            <div class="mk-im-company-item">\
                                <div class="mk-im-item-name mk-im-company" data-value="'+ _item.id + '"  data-deep="0" >\
                                    <i class="fa fa-angle-right"></i>'+ _item.name + '\
                                </div>\
                            </div>';
                            $list.append(_html);
                        });
                    }

                }
            });
            // 发送消息
            $('#im_input').on("keypress", function (e) {
                e = e || window.event;
                if (e.keyCode == "13") {
                    var text = $(this).val();
                    $(this).val('');
                    if (text.replace(/(^\s*)|(\s*$)/g, "") != '') {
                        var time = Minke.im.sendMsg(imUserId,text);
                        sendMsg(text, time);
                    }
                    console.log(text);

                    return false;
                }
            });

            // 注册消息接收
            Minke.im.registerRevMsg(function (userId, msg, dateTime) {
                // 判断当前账号是否打开聊天窗口
                if (userId == imUserId) {
                    revMsg(userId, msg, dateTime);
                    Minke.im.updateContacts(userId);
                }
                else {
                    var $userList = $('#immsg_userlist .mk-scroll-box');
                    var $userItem = $userList.find('[data-value="' + userId + '"]');
                    $('#im_msglist .mk-im-right').removeClass('mk-im-nouser');

                    if ($userItem.length > 0) {
                        $userList.prepend($userItem);
                        if (!$userItem.hasClass('imHasMsg')) {
                            $userItem.addClass('imHasMsg');
                        }
                    }
                    else {
                        var html = '\
                            <div class="userlist-item imHasMsg" data-value="'+ userId + '" >\
                                <div class="photo"><img src="'+ top.$.rootUrl + '/Content/images/head/on-boy.jpg" >\
                                <div class="point"></div></div>\
                            </div>';
                        $userList.prepend(html);
                        Minke.clientdata.getAsync('user', {
                            key: userId,
                            callback: function (data, op) {
                                var $item = $userList.find('[data-value="' + op.key + '"]');
                                $item.attr('title', data.name);
                                data.id = op.key;
                                $item.find('img').attr('src', getHeadImg(data));
                                $item = null;
                            }
                        });

                        var _$userItem = $userList.find('.userlist-item');
                        if (_$userItem.length == 1) {
                            _$userItem.trigger('click');
                        }

                    }
                    $('#im_input').val('');
                    $('#im_input').select();
                }
                if (!$('.mk-im-bell').hasClass('open')) {
                    $('.mk-im-bell .point').show();
                }
            });

            // 查看聊天记录
            $('#im_look_msg_btn').on('click', function () {
                Minke.layerForm({
                    id: 'LookMsgIndex',
                    title: '查看聊天记录-' + $('#im_msglist .mk-im-right .mk-im-touser').text(),
                    url: top.$.rootUrl + '/IM/IMMsg/Index?userId=' + imUserId,
                    width: 800,
                    height: 500,
                    maxmin: true,
                    btn: null
                });
            });
        }
    };

    im.init();
};