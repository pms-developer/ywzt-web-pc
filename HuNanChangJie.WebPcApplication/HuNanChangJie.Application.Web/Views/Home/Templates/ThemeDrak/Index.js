﻿
$(function () {
    var special = ["北京", "天津", "上海", "重庆", "香港", "澳门"];
    var defaults = {
        geoCoordMap: {
            '上海': [121.4648, 31.2891],
            '东莞': [113.8953, 22.901],
            '东营': [118.7073, 37.5513],
            '中山': [113.4229, 22.478],
            '临汾': [111.4783, 36.1615],
            '临沂': [118.3118, 35.2936],
            '丹东': [124.541, 40.4242],
            '丽水': [119.5642, 28.1854],
            '乌鲁木齐': [87.9236, 43.5883],
            '佛山': [112.8955, 23.1097],
            '保定': [115.0488, 39.0948],
            '兰州': [103.5901, 36.3043],
            '甘肃': [103.5901, 36.3043],

            '包头': [110.3467, 41.4899],
            '北京': [116.4551, 40.2539],
            '北海': [109.314, 21.6211],
            '南京': [118.8062, 31.9208],
            '江苏': [118.8062, 31.9208],
            '南宁': [108.479, 23.1152],
            '广西': [108.479, 23.1152],

            '南昌': [116.0046, 28.6633],
            '江西': [116.0046, 28.6633],

            '南通': [121.1023, 32.1625],
            '厦门': [118.1689, 24.6478],
            '台州': [121.1353, 28.6688],

            '合肥': [117.29, 32.0581],
            '安徽': [117.29, 32.0581],

            '呼和浩特': [111.4124, 40.4901],
            '咸阳': [108.4131, 34.8706],
            '哈尔滨': [127.9688, 45.368],
            '黑龙江': [127.9688, 45.368],
            '唐山': [118.4766, 39.6826],
            '嘉兴': [120.9155, 30.6354],
            '大同': [113.7854, 39.8035],
            '大连': [122.2229, 39.4409],
            '天津': [117.4219, 39.4189],
            '太原': [112.3352, 37.9413],
            '山西': [112.3352, 37.9413],
            '威海': [121.9482, 37.1393],
            '宁波': [121.5967, 29.6466],
            '宝鸡': [107.1826, 34.3433],
            '宿迁': [118.5535, 33.7775],
            '常州': [119.4543, 31.5582],
            '广州': [113.5107, 23.2196],
            '广东': [113.5107, 23.2196],

            '廊坊': [116.521, 39.0509],
            '延安': [109.1052, 36.4252],
            '张家口': [115.1477, 40.8527],
            '徐州': [117.5208, 34.3268],
            '德州': [116.6858, 37.2107],
            '惠州': [114.6204, 23.1647],
            '成都': [103.9526, 30.7617],
            '四川': [103.9526, 30.7617],

            '扬州': [119.4653, 32.8162],
            '承德': [117.5757, 41.4075],
            '拉萨': [91.1865, 30.1465],
            '无锡': [120.3442, 31.5527],
            '日照': [119.2786, 35.5023],
            '昆明': [102.9199, 25.4663],
            '云南': [102.9199, 25.4663],

            '杭州': [119.5313, 29.8773],
            '浙江': [119.5313, 29.8773],

            '枣庄': [117.323, 34.8926],
            '柳州': [109.3799, 24.9774],
            '株洲': [113.5327, 27.0319],
            '武汉': [114.3896, 30.6628],
            '湖北': [114.3896, 30.6628],

            '汕头': [117.1692, 23.3405],
            '江门': [112.6318, 22.1484],
            '沈阳': [123.1238, 42.1216],
            '辽宁': [123.1238, 42.1216],
            '沧州': [116.8286, 38.2104],
            '河源': [114.917, 23.9722],
            '泉州': [118.3228, 25.1147],
            '泰安': [117.0264, 36.0516],
            '泰州': [120.0586, 32.5525],
            '济南': [117.1582, 36.8701],
            '山东': [117.1582, 36.8701],

            '济宁': [116.8286, 35.3375],
            '海口': [110.3893, 19.8516],
            '海南': [110.3893, 19.8516],

            '淄博': [118.0371, 36.6064],
            '淮安': [118.927, 33.4039],
            '深圳': [114.5435, 22.5439],
            '清远': [112.9175, 24.3292],
            '温州': [120.498, 27.8119],
            '渭南': [109.7864, 35.0299],
            '湖州': [119.8608, 30.7782],
            '湘潭': [112.5439, 27.7075],
            '滨州': [117.8174, 37.4963],
            '潍坊': [119.0918, 36.524],
            '烟台': [120.7397, 37.5128],
            '玉溪': [101.9312, 23.8898],
            '珠海': [113.7305, 22.1155],
            '盐城': [120.2234, 33.5577],
            '盘锦': [121.9482, 41.0449],
            '石家庄': [114.4995, 38.1006],
            '河北': [114.4995, 38.1006],
            '福州': [119.4543, 25.9222],
            '福建': [119.4543, 25.9222],

            '秦皇岛': [119.2126, 40.0232],
            '绍兴': [120.564, 29.7565],
            '聊城': [115.9167, 36.4032],
            '肇庆': [112.1265, 23.5822],
            '舟山': [122.2559, 30.2234],
            '苏州': [120.6519, 31.3989],
            '莱芜': [117.6526, 36.2714],
            '菏泽': [115.6201, 35.2057],
            '营口': [122.4316, 40.4297],
            '葫芦岛': [120.1575, 40.578],
            '衡水': [115.8838, 37.7161],
            '衢州': [118.6853, 28.8666],
            '西宁': [101.4038, 36.8207],
            '青海': [101.4038, 36.8207],

            '西安': [109.1162, 34.2004],
            '陕西': [109.1162, 34.2004],

            '贵阳': [106.6992, 26.7682],
            '贵州': [106.6992, 26.7682],

            '连云港': [119.1248, 34.552],
            '邢台': [114.8071, 37.2821],
            '邯郸': [114.4775, 36.535],
            '郑州': [113.4668, 34.6234],
            '河南': [113.4668, 34.6234],

            '鄂尔多斯': [108.9734, 39.2487],
            '重庆': [107.7539, 30.1904],
            '金华': [120.0037, 29.1028],
            '铜川': [109.0393, 35.1947],
            '银川': [106.3586, 38.1775],
            '宁夏': [106.3586, 38.1775],

            '镇江': [119.4763, 31.9702],
            '长春': [125.8154, 44.2584],
            '吉林': [125.8154, 44.2584],
            '长沙': [113.0823, 28.2568],
            '湖南': [113.0823, 28.2568],

            '长治': [112.8625, 36.4746],
            '阳泉': [113.4778, 38.0951],
            '青岛': [120.4651, 36.3373],
            '韶关': [113.7964, 24.7028]
        },

        provinces: {
            //23个省
            "台湾": "taiwan",
            "河北": "hebei",
            "山西": "shanxi",
            "辽宁": "liaoning",
            "吉林": "jilin",
            "黑龙江": "heilongjiang",
            "江苏": "jiangsu",
            "浙江": "zhejiang",
            "安徽": "anhui",
            "福建": "fujian",
            "江西": "jiangxi",
            "山东": "shandong",
            "河南": "henan",
            "湖北": "hubei",
            "湖南": "hunan",
            "广东": "guangdong",
            "海南": "hainan",
            "四川": "sichuan",
            "贵州": "guizhou",
            "云南": "yunnan",
            "陕西": "shanxi1",
            "甘肃": "gansu",
            "青海": "qinghai",
            //5个自治区
            "新疆": "xinjiang",
            "广西": "guangxi",
            "内蒙古": "neimenggu",
            "宁夏": "ningxia",
            "西藏": "xizang",
            //4个直辖市
            "北京": "beijing",
            "天津": "tianjin",
            "上海": "shanghai",
            "重庆": "chongqing",
            //2个特别行政区
            "香港": "xianggang",
            "澳门": "aomen"
        },
        convertData: function (data) {
            var res = [];
            for (var i = 0; i < data.length; i++) {
                var geoCoord = $.geoCoordMap[data[i].name];
                if (geoCoord) {
                    res.push({
                        name: data[i].name,
                        value: geoCoord.concat(data[i].value)
                    });
                }
            }



            return res;
        },
        convertDataQX: function (data) {
            //var res = [];
            //for (var i = 0; i < data.length; i++) {
            //    var geoCoord = $.geoCoordMap[data[i].name];
            //    if (geoCoord) {
            //        res.push({
            //            name: data[i].name,
            //            value: geoCoord.concat(data[i].value)
            //        });
            //    }
            //}


            var res = [];
            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                var fromCoord = $.geoCoordMap[dataItem[0].name];
                var toCoord = $.geoCoordMap[dataItem[1].name];
                if (fromCoord && toCoord) {
                    res.push({
                        fromName: dataItem[0].name,
                        toName: dataItem[1].name,
                        coords: [fromCoord, toCoord]
                    });
                }
            }
            return res;
        },
        option: {},
        itemColors: [
            ["rgb(188,25,92)", "rgb(252,76,89)"]
            , ["rgb(45,98,215)", "rgb(67,220,252)"]
            , ["rgb(67,69,218)", "rgb(93,112,236)"]
            , ["rgb(109,68,206)", "rgb(153,116,252)"]
            , ["rgb(140,62,174)", "rgb(237,151,255)"]
            , ["rgb(29,155,76)", "rgb(0,226,133)"]
            , ["rgb(255,75,31)", "rgb(255,144,104)"]
            , ["rgb(8,80,120)", "rgb(133,216,206)"]
        ]
    };

    $.extend(defaults);
    function FlowData(dataT) {

        var biCategory = MKFormateGroupData(dataT, "map").category;
        var biSeries = MKFormateGroupData(dataT, "map").series;
        var TopData = [];
        for (var topi in biSeries) {
            var myobject = [];
            myobject[0] = biSeries[topi].name //最外层
            var BJData = [];
            for (var si in biSeries[topi].value) {
                var myboject2 = [];//内层数组
                myboject2[0] = { name: myobject[0] };
                myboject2[1] = { name: biSeries[topi].value[si].name, value: biSeries[topi].value[si].value };
                BJData.push(myboject2);
            }
            myobject[1] = BJData;
            TopData.push(myobject);
        }

        return TopData;
    }

    function MKFormateGroupData(data, type) {

        /*
         *data的格式如上的Result2，type为要渲染的图表类型：可以为line，bar，is_stack表示为是否是堆积图，
         *这种格式的数据多用于展示多条折线图、分组的柱图
         *
         *
         *
         */

        var chart_type = 'line';
        if (type)
            chart_type = type || 'line';
        var xAxis = [];
        var group = [];
        var series = [];
        for (var i = 0; i < data.length; i++) {
            for (var j = 0; j < xAxis.length && xAxis[j] != data[i].name; j++);
            if (j == xAxis.length)
                xAxis.push(data[i].name);
            for (var k = 0; k < group.length && group[k] != data[i].group; k++);
            if (k == group.length)
                group.push(data[i].group);
        }
        for (var i = 0; i < group.length; i++) {
            var temp = [];
            if (type != "warn") {
                for (var j = 0; j < data.length; j++) {
                    if (group[i] == data[j].group) {
                        if (type == "map") {
                            temp.push({ name: data[j].name, value: data[i].value });
                        } else {
                            temp.push(data[j].value);
                        }
                    }
                }
            } else {
                for (var m = 0; m < data.length; m++) {
                    if (group[i] == data[m].group) {
                        if (type == "warn") {
                            temp.push({ name: data[m].name, value: data[i].group });
                        } else {
                            temp.push(data[m].value);
                        }
                    }
                }
            }

            switch (type) {
                case 'bar':
                    var series_temp = { name: group[i], value: temp, type: chart_type };

                    break;
                case 'map':
                    var series_temp = {
                        name: group[i], value: temp, type: chart_type
                    };
                    break;
                case 'line':
                    var series_temp = { name: group[i], value: temp, type: chart_type };

                    break;
                case 'flow':
                    var series_temp = { name: group[i], value: temp, type: chart_type };

                    break;
                case 'warn':
                    var series_temp = { name: group[i], value: temp, type: chart_type };

                    break;
                default:
                    var series_temp = { name: group[i], value: temp, type: chart_type };
            }
            series.push(series_temp);
        }
        return { category: group, xAxis: xAxis, series: series };
    }



    var f = {};
    var d = {};


    function showTargetItem(i) {
        if (i.length > 0) {
            $("#target").mkscroll();
            var mkitemWidth = 210;
            var mktargetwidth = 0;
            var k = $("#target").width() - 10;
            var mkitemWidth = k / i.length;
            if (mkitemWidth < 210) {
                mkitemWidth = 210;
            }
            mktargetwidth = mkitemWidth * i.length;
            var target = $("#target .mk-scroll-box");
            target.css("width", mktargetwidth);
            $.each(i,
                function (m, dttargetObj) {
                    f[dttargetObj.F_Id] = dttargetObj;
                    var mkitem = '<div class="mk-item-20">' +
                        '<div class="task-stat" >' +
                        '<div class="visual">' +
                        '<i class="' + dttargetObj.F_Icon + '"></i>' +
                        '</div>' +
                        '<div class="details">' +
                        '<div class="number" data-value="' + dttargetObj.F_Id + '">' +
                        '</div>' +
                        '<div class="desc">' + dttargetObj.F_Name + "</div>" +
                        "</div>";
                    if (dttargetObj.F_Url) {
                        mkitem += '<a class="more" data-Id="' + dttargetObj.F_Id + '" >                            查看更多 <i class="fa fa-arrow-circle-right"></i>                        </a>'
                    }
                    mkitem += "</div>" +
                        "</div>";
                    target.append(mkitem);
                    top.Minke.httpAsync("GET",
                        top.$.rootUrl + "/Desktop/DTTarget/GetSqlData",
                        {
                            Id: dttargetObj.F_Id
                        },
                        function (o) {
                            if (o) {
                                target.find('[data-value="' + o.Id + '"]').text(o.value);
                            }
                        });
                });
            target.find(".mk-item-20 .more").on("click",
                function () {
                    var dataid = $(this).attr("data-Id");
                    top.Minke.frameTab.open({
                        F_ModuleId: dataid,
                        F_FullName: f[dataid].F_Name,
                        F_UrlAddress: f[dataid].F_Url
                    });
                    return false;
                });
            target.find(".mk-item-20").css("width", mkitemWidth);
            $("#target").resize(function () {
                var m = $("#target").width() - 10;
                var l = m / i.length;
                if (l < 210) {
                    l = 210;
                }
                mktargetwidth = l * i.length;
                target.css("width", mktargetwidth);
                target.find(".mk-item-20").css("width", l);
            });
        }
    }
    function showListItem(h) {
        if (h.length > 0) {
            var g = $(".mk-desktop-panel>.mk-scroll-box");
            $.each(h,
                function (j, k) {
                    d[k.F_Id] = k;
                    var i = '                <div class="col-xs-6" data-Id="' + k.F_Id + '"><div class="mychartcss" >                    <div class="portal-panel-title">                        <i class="' + k.F_Icon + '"></i>&nbsp;&nbsp;&nbsp;&nbsp;' + k.F_Name + '                        <span class="menu" title="更多">                        <span class="point"></span>                        <span class="point"></span>                        <span class="point"></span>                        </span>                    </div>                    <div class="portal-panel-content" style="overflow: hidden; padding-top: 20px; padding-left: 30px; padding-right: 50px;height:225px;" data-value="' + k.F_Id + '" >                    </div>                </div></div>';
                    g.append(i);
                    top.Minke.httpAsync("GET",
                        top.$.rootUrl + "/Desktop/DTList/GetSqlData",
                        {
                            Id: k.F_Id
                        },
                        function (m) {
                            if (m) {
                                var l = g.find('[data-value="' + m.Id + '"]');
                                $.each(m.value,
                                    function (p, q) {
                                        var o =
                                            '                            <div class="mk-msg-line">                                <a href="#" style="text-decoration: none;color:#FFF" >' +
                                            q.f_title +
                                            "</a>                                <label style='color:#FFF'>" +
                                            q.f_time +
                                            "</label>                            </div>";
                                        var n = $(o);
                                        n.find("a")[0].item = q;
                                        l.append(n);
                                    });
                                l.find(".mk-msg-line>a").on("click",
                                    function () {
                                        var n = $(this).parents(".col-xs-6");
                                        var o = n.attr("data-Id");
                                        var p = $(this)[0].item;
                                        if (d[o].F_ItemUrl) {
                                            top.Minke.frameTab.open({
                                                F_ModuleId: "dtlist" + p.f_id,
                                                F_FullName: p.f_title,
                                                F_UrlAddress: d[o].F_ItemUrl + p.f_id
                                            });
                                        } else {
                                            top["dtlist" + p.f_id] = p;
                                            top.Minke.frameTab.open({
                                                F_ModuleId: "dtlist" + p.f_id,
                                                F_FullName: p.f_title,
                                                F_UrlAddress: "/Utility/ListContentIndex?id=" + p.f_id
                                            });
                                        }
                                        return false;
                                    });
                            }
                        });
                });
            g.find(".portal-panel-title>.menu").on("click",
                function () {
                    var i = $(this).parents(".col-xs-6");
                    var j = i.attr("data-Id");
                    top.Minke.frameTab.open({
                        F_ModuleId: j,
                        F_FullName: d[j].F_Name,
                        F_UrlAddress: d[j].F_Url
                    });
                    return false
                });
            if (h.length % 2 > 0) {
                g.find('[data-value="' + h[h.length - 1].F_Id + '"]').css("height", 425)
            }
        }
    }
    //图容器 数组
    var chart = {};
    function showChartItem(h) {
        if (h.length > 0) {
            var g = $(".mk-desktop-panel>.mk-scroll-box");
            $.each(h,
                function (j, dtChart) {
                    //F_Proportion1 为经典样式的栅栏比例

                    var dtChartHtml = '<div class="col-xs-' + (12 / parseInt(dtChart.F_Proportion1)) + '">' +
                        '<div class="mychartcss" ><div class="portal-panel-title">' +
                        '<i class="' + dtChart.F_Icon + '"></i>&nbsp;&nbsp;' + dtChart.F_Name + '</div>' +
                        '<div class="portal-panel-content">' +
                        '<div id="' + dtChart.F_Id + '" class="mk-chart-container" data-type="' + dtChart.F_Type + '"   ></div>' +
                        '</div>' +
                        '</div></div>';
                    g.append(dtChartHtml);
                    //向容器集合中添加 地图容器
                    chart[dtChart.F_Id] = echarts.init(document.getElementById(dtChart.F_Id));
                    top.Minke.httpAsync("GET",
                        top.$.rootUrl + "/Desktop/DTChart/GetSqlData",
                        {
                            Id: dtChart.F_Id
                        },
                        function (data) {
                            if (data) {
                                var chartType = $("#" + data.Id).attr("data-type");

                                var names = [];
                                var values = [];
                                $.each(data.value,
                                    function (q, redata) {
                                        names.push(redata.name);
                                        values.push(redata.value);
                                    });
                                var option = {};

                                switch (chartType) {
                                    case "0": //饼图
                                        option.legend = {
                                            bottom: "bottom",
                                            data: names,
                                            textStyle: {
                                                color: "#FFF"
                                            }
                                        };

                                        var pieDatasItems = [];

                                        for (const i in data.value) {

                                            var element = data.value[i];
                                            var yushu = i % 8;
                                            element.itemStyle = {
                                                color: {
                                                    type: 'linear',
                                                    x: 0,
                                                    y: 1,
                                                    x2: 0,
                                                    y2: 0,
                                                    colorStops: [{
                                                        offset: 0, color: $.itemColors[yushu][0] // 0% 处的颜色
                                                    }, {
                                                        offset: 1, color: $.itemColors[yushu][1]// 100% 处的颜色
                                                    }],
                                                    globalCoord: false // 缺省为 false
                                                }
                                            };

                                            pieDatasItems.push(element);

                                        }
                                        option.series = [
                                            {
                                                name: "占比",
                                                type: "pie",
                                                radius: ['40%', '80%'],//内围心大小，外围
                                                center: ["50%", "50%"],

                                                label: {
                                                    normal: {
                                                        formatter: "{b}:{c}: ({d}%)",
                                                        textStyle: {
                                                            fontWeight: "normal",
                                                            fontSize: 12,
                                                            color: "#FFF"
                                                        }
                                                    }
                                                },
                                                data: pieDatasItems,

                                            }
                                        ];
                                        option.color = [
                                            "#df4d4b", "#304552", "#52bbc8", "rgb(224,134,105)", "#8dd5b4", "#5eb57d",
                                            "#d78d2f"
                                        ];
                                        chart[data.Id].on('click',
                                            function (param) {


                                            });
                                        chart[data.Id].setOption(option);

                                        break;
                                    case "1": //折线图
                                        option.tooltip = {
                                            trigger: "axis"
                                        };
                                        option.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        option.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: names
                                        };
                                        option.yAxis = {
                                            type: "value"
                                        };
                                        option.series = [
                                            {
                                                type: "line",
                                                data: values
                                            }
                                        ];
                                        chart[data.Id].setOption(option);
                                        break;
                                    case "2": //柱形图

                                        option.textStyle =
                                            {
                                                color: "#FFF"
                                            };

                                        option.tooltip = {
                                            trigger: "axis"
                                        };
                                        option.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        option.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: names

                                        };
                                        option.yAxis = {
                                            type: "value"
                                        };
                                        option.series = [
                                            {
                                                type: "bar",
                                                data: values,
                                                itemStyle: {
                                                    normal: {
                                                        color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [{


                                                            // 0% 处的颜色   
                                                            offset: 0, color: 'rgb(188,25,92)'
                                                        },
                                                        {

                                                            // 100% 处的颜色
                                                            offset: 1, color: 'rgb(252,76,89)'
                                                        }], false)
                                                    }
                                                }

                                            }
                                        ];


                                        chart[data.Id].setOption(option);
                                        break;
                                    case "3": //地图显示

                                        $(function () {
                                            //$.getJSON('/Content/json/map_json/china.json', function (mdata) {
                                            //    //注册地图
                                            //    echarts.registerMap('china', mdata);

                                            //});

                                            var mapOption = {

                                                title: {
                                                    text: 'CJ-PMS智能地图',
                                                    subtext: '中国',
                                                    sublink: '',
                                                    x: 'center',
                                                    textStyle: {
                                                        color: '#fff'
                                                    }
                                                },
                                                tooltip: {
                                                    trigger: 'item',
                                                    formatter: function (params) {
                                                        return params.name + ' : ' + params.value[2];
                                                    }
                                                },
                                                legend: {
                                                    orient: 'vertical',
                                                    y: 'bottom',
                                                    x: 'right',

                                                    textStyle: {
                                                        color: '#fff'
                                                    }
                                                },
                                                visualMap: {
                                                    show: false,
                                                    min: 0,
                                                    max: 2500,
                                                    left: 'left',
                                                    top: 'bottom',
                                                    text: ['高', '低'], // 文本，默认为数值文本
                                                    calculable: true,
                                                    seriesIndex: [1],
                                                    textStyle: {
                                                        color: '#fff'
                                                    }
                                                },

                                                geo: {
                                                    // 地理坐标系组件。
                                                    //地理坐标系组件用于地图的绘制，支持在地理坐标系上绘制散点图，线集。
                                                    //是否显示地理坐标系组件。
                                                    show: true,
                                                    /*
                                                     * ECharts 中提供了两种格式的地图数据，
                                                     * 一种是可以直接 script 标签引入的 js 文件，
                                                     * 引入后会自动注册地图名字和数据。
                                                     * 还有一种是 JSON 文件，需要通过 AJAX 异步加载后手动注册。
                                                     */
                                                    map: "china",

                                                    /*
                                                     * 图形上的文本标签，可用于说明图形的一些数据信息，比如值，名称等，
                                                     * label选项在 ECharts 2.x 中放置于itemStyle下，
                                                     * 在 ECharts 3 中为了让整个配置项结构更扁平合理，
                                                     * label 被拿出来跟 itemStyle 平级，
                                                     * 并且跟 itemStyle 一样拥有 emphasis 状态。
                                                     */
                                                    label: {
                                                        normal: {
                                                            show: true,
                                                            color: '#5bd4fa',
                                                        },
                                                        emphasis: {
                                                            color: 'red',
                                                        }
                                                    },
                                                    /*
                                                     * 地图区域的多边形 图形样式。
                                                     */
                                                    itemStyle: {
                                                        normal: {
                                                            //地图区域的颜色。
                                                            areaColor: '#323c48',
                                                            borderColor: 'dodgerblue',
                                                            //图形的描边颜色。支持的颜色格式同 color，不支持回调函数

                                                            //描边线宽。为 0 时无描边。
                                                            borderWidth: 1,
                                                            //图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果。
                                                            shadowBlur: {
                                                                shadowColor: 'rgba(121, 194, 218, 0.5)',//阴影颜色。支持的格式同color。
                                                                shadowBlur: 30,//图形阴影的模糊大小。
                                                                shadowOffsetY: 20,//阴影垂直方向上的偏移距离。
                                                                shadowOffsetX: 20,//阴影水平方向上的偏移距离。
                                                                opacity: 0.6
                                                            }
                                                        },
                                                        /*
                                                         * 高亮状态下的多边形和标签样式。
                                                         */
                                                        emphasis: {
                                                            //地图区域的多边形 图形样式。
                                                            itemStyle: {
                                                                areaColor: '#2B91B7'
                                                            }

                                                        }
                                                    },

                                                    layoutSize: 900,
                                                    roam: true

                                                },
                                                series: [
                                                    {
                                                        name: '',
                                                        type: 'scatter',
                                                        coordinateSystem: 'geo',
                                                        symbol: 'pin',
                                                        symbolSize: function (val) {
                                                            var max = 30000;
                                                            var min = 900; // todo
                                                            var maxSize4Pin = 100;
                                                            var minSize4Pin = 0;
                                                            var a = (maxSize4Pin - minSize4Pin) / (max - min);
                                                            var b = minSize4Pin - a * min;
                                                            b = maxSize4Pin - a * max;
                                                            return a * val[2] + b;
                                                        },
                                                        data: $.convertData(data.value),

                                                        symbolSize: 12,
                                                        label: {
                                                            normal: {
                                                                show: false
                                                            },
                                                            emphasis: {
                                                                show: false
                                                            }
                                                        },
                                                        itemStyle: {
                                                            normal: {
                                                                areaColor: '#323c48',
                                                                borderColor: '#404a59'
                                                            },
                                                            emphasis: {
                                                                areaColor: '#2a333d'
                                                            }
                                                        }
                                                    }
                                                ]
                                            };

                                            chart[data.Id].setOption(mapOption);
                                        });



                                        break;

                                    case "4":


                                        var planePath = 'path:// M878.229514 645.809634c-40.072094-279.906746-298.66716-645.809634-375.45455-645.809634-46.917869 0-327.58588 367.1461-375.462032 645.809634-46.003852 267.897962 168.10054 378.190366 375.462032 378.190366S917.497324 920.091997 878.229514 645.809634zM683.499037 658.740541 572.745883 658.740541l0 110.774975-139.920641 0L432.825243 658.740541 322.072089 658.740541 322.072089 518.841722l110.753154 0L432.825243 408.074228l139.920641 0 0 110.767494L683.499037 518.841722 683.499037 658.740541z';
                                        var color = ['#a6c84c', '#ffa022', '#46bee9'];
                                        var series = [];
                                        FlowData(data.value).forEach(function (item, i) {
                                            series.push({
                                                name: item[0],
                                                type: 'lines',
                                                zlevel: 1,
                                                effect: {
                                                    show: true,
                                                    period: 6,
                                                    trailLength: 0.7,
                                                    color: '#fff',
                                                    symbolSize: 3
                                                },
                                                lineStyle: {
                                                    normal: {
                                                        color: color[i],
                                                        width: 0,
                                                        curveness: 0.2
                                                    }
                                                },
                                                data: $.convertDataQX(item[1])
                                            },
                                                {
                                                    name: item[0],
                                                    type: 'lines',
                                                    zlevel: 2,
                                                    effect: {
                                                        show: true,
                                                        period: 6,
                                                        trailLength: 0,
                                                        symbol: planePath,
                                                        symbolSize: 10
                                                    },
                                                    lineStyle: {
                                                        normal: {
                                                            color: color[i],
                                                            width: 1,
                                                            opacity: 0.4,
                                                            curveness: 0.2
                                                        }
                                                    },
                                                    data: $.convertDataQX(item[1])
                                                },
                                                {
                                                    name: item[0],
                                                    type: 'effectScatter',
                                                    coordinateSystem: 'geo',
                                                    zlevel: 2,
                                                    rippleEffect: {
                                                        brushType: 'stroke'
                                                    },
                                                    label: {
                                                        normal: {
                                                            show: true,
                                                            position: 'right',
                                                            formatter: '{b}'
                                                        }
                                                    },
                                                    symbolSize: function (val) {
                                                        return val[2] / 2000;
                                                    },
                                                    itemStyle: {
                                                        normal: {
                                                            color: color[i]
                                                        }
                                                    },
                                                    data: item[1].map(function (dataItem) {
                                                        var myname = dataItem[1].name;
                                                        var myvalue = dataItem[1].value;

                                                        return {
                                                            name: myname,
                                                            value: $.geoCoordMap[myname].concat([dataItem[1].value])
                                                        };
                                                    })
                                                });
                                        });
                                        option = {
                                            //        backgroundColor: '#404a59',
                                            title: {

                                                left: 'center',
                                                textStyle: {
                                                    color: '#fff'
                                                }
                                            },
                                            tooltip: {
                                                trigger: 'item'
                                            },

                                            geo: {
                                                map: 'china',
                                                label: {
                                                    emphasis: {
                                                        show: false
                                                    }
                                                },
                                                roam: true,
                                                itemStyle: {
                                                    normal: {
                                                        areaColor: '#353761',
                                                        borderColor: '#a38a8a'
                                                    },
                                                    emphasis: {
                                                        areaColor: '#2a333d'
                                                    }
                                                }
                                            },
                                            series: series
                                        };
                                        chart[data.Id].setOption(option);
                                        break;
                                }

                            }
                        });
                });
            window.onresize = function (i) {
                $.each(chart,
                    function (j, k) {
                        k.resize(i);
                    });
            }
        }
    }
    $(".mk-desktop-panel").mkscroll();
    top.Minke.clientdata.getAsync("desktop",
        {
            callback: function (g) {
                showTargetItem(g.target || []);
                showListItem(g.list || []);
                showChartItem(g.chart || []);
            }
        });








});