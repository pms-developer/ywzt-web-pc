﻿(function ($, Changjie) {
    "use strict";
    $.fn.backstageVerify = function (formData, formId,projectId) {
        var isVerify = true;
        if (formId) {
            var param = { formId: formId };
            var data = Changjie.httpget(top.$.rootUrl + '/SystemModule/BaseConfirm/GetList', param);
            debugger;
            for (var _i in data) {
                var item = data[_i];
                var expres = JSON.parse(item.Expression);
                var formvalue;
                if (item.iscustom) {//自定义表单
                    formvalue = formData[expres.customControlId];
                }
                else {
                    formvalue = formData[expres.leftObj];
                }
                var sql = "";

                while (expres.rightObjSqlInfo.sql.indexOf("@projectId@") > -1) {
                    sql = expres.rightObjSqlInfo.sql.replace("@projectId@", "'" + projectId + "'");
                }
               
                while (sql.indexOf("@this.") > -1) {
                    
                    var ag = sql.match(/@(\S*)@/)[1];
                   
                    sql = sql.replace("@" + ag + "@", formvalue);
                }
                var dbId = item.F_DataSourceId;
                var getparam = {
                    dbId: dbId,
                    sql: sql
                }
                var getdata = Changjie.httpget(top.$.rootUrl + '/SystemModule/DatabaseTable/GetTableDataListBySql', getparam)
                if (getdata.data.length > 0) {
                    var info = getdata.data[0];
                    if (!!!info.flag) {
                        var msg = item.InfoMsg + "当前" + expres.rightObjSqlInfo.name + "为:" + info.value
                        if (item.IsForce) {//强制提醒
                            Changjie.alert.warning(msg);
                            isVerify = false;
                        }
                        else {
                            Changjie.layerConfirm(msg+"   是否继续保存？", function (res) {
                                if (res) {
                                    isVerify = true;
                                }
                                else {
                                    isVerify = false;
                                }
                            });
                        }
                    }
                }
            }
        }
        return isVerify;
    }
})(window.jQuery, top.Changjie);