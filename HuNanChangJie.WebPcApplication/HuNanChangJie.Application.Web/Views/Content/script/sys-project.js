﻿(function ($, Changjie) {
    "use strict";
    $(function () {
        var projectId = request("projectId");
        if (!projectId) return;
        var url = top.$.rootUrl + '/ProjectModule/Project/GetFormData?keyValue=' + projectId;
        top.Changjie.httpAsyncGet(url, function (data) {
            if (data.code == 200) {
                $("#ProjectCode").val(data.data.Code);
                $("#ProjectName").val(data.data.ProjectName);
                $("#ProjectMode").mkselectSet(data.data.Mode);
                $("#ProjectID").mkselectSet(data.data.ID);

                //$("#ProjectMode").mkselectSet(data.data.Mode).attr("disabled", "disabled");
                //$("#ProjectID").mkselectSet(data.data.ID).attr("disabled", "disabled");

                if ($("#companyid").length > 0) {
                    $("#companyid").val(data.data.Company_ID);
                }
                if ($("#yewury").length > 0) {
                    $("#yewury").val(data.data.MarketingStaffID);
                }
                if ($("#xmjl").length > 0) {
                    $("#xmjl").val(data.data.ProjectManager);
                }
            }
        });
    });
})(window.jQuery, top.Changjie);
