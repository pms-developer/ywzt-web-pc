﻿top.badge = (function ($, Changjie) {
    "use strict";
    var badge = {
        allCount: 0,
        finishedCount: 0,
        notifyCount: 0,
        waitCount: 0,
        backCount: 0,
        systemMessageCount:0,
        init: function () {
            var flag = false;
            if (!flag) {
                var url = top.$.rootUrl + '/WorkflowModule/Workflow/GetWaitProcessInfo';
                var data = top.Changjie.httpAsyncGet(url, function (data) {
                    data = data.data;
                    top.badge.allCount = data.allCount;
                    top.badge.finishedCount = data.finishedCount;
                    top.badge.notifyCount = data.notifyCount;
                    top.badge.waitCount = data.waitCount;
                    top.badge.backCount = data.backCount;
                    var systemMessageData = top.Changjie.httpGet(top.$.rootUrl + '/SystemModule/BaseMessage/GetUnreadInfo');
                    top.badge.systemMessageCount = systemMessageData.data;
                    top.badge.allCount += top.badge.systemMessageCount;
                    flag = true;
                });
            }
            else {
                setTimeout(function () {
                    this.init();
                }, 100);
            }

        },
        getProcess: function () {
            return top.badge.allCount;
        },
        getWaitTransact: function () {
            return top.badge.waitCount;
        },
        getNotiy: function () {
            return top.badge.notifyCount;
        },
        getFinished: function () {
            return top.badge.finishedCount;
        },
        getBack: function () {
            return top.badge.backCount;
        },
        setProcess: function () {
            if (top.badge.allCount >= 1) {
                top.badge.allCount--;
                $("#getProcess").html(top.badge.allCount);
            }
        },
        setWaitTransact: function () {
            if (top.badge.waitCount >= 1) {
                top.badge.waitCount--;
                this.setProcess();
                $("#getWaitTransact").html(top.badge.waitCount);
            }
        },
        setNotiy: function () {
            if (top.badge.notifyCount >= 1) {
                top.badge.notifyCount--;
                this.setProcess();
                $("#getNotiy").html(top.badge.notifyCount);
            }
        },
        setFinished: function () {
            if (top.badge.finishedCount >= 1) {
                top.badge.finishedCount--;
                this.setProcess();
                $("#getFinished").html(top.badge.finishedCount);
            }
        },
        setBack: function () {
            if (top.badge.backCount >= 1) {
                top.badge.backCount--;
                this.setProcess();
                $("#getBack").html(top.badge.backCount);
            }
        },
        getSystemMessage: function () {
            return top.badge.systemMessageCount;
        },
        setSystemMessage: function () {
            if (top.badge.systemMessageCount >= 1) {
                top.badge.systemMessageCount--;
                this.setProcess();
                $("#getSystemMessage").html(top.badge.systemMessageCount);
            }
        },

    };
    return badge;
})(window.jQuery,top.Changjie);

 