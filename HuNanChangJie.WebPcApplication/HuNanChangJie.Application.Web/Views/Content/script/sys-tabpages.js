﻿(function ($, Changjie) {

    "use strict";

    var loadTabpage = function ($ul, $content, dfop) {

        var tabInfos = [];
        if (dfop.isShowAttachment) {
            tabInfos.push({ name: "附件", id: "tabsub_showAttachment", value: dfop.tabs.attachment });
        }
        if (dfop.isShowWorkflow) {
            tabInfos.push({ name: "流程", id: "tabsub_showWorkflow", value: dfop.tabs.workflow });
        }
        for (var tb in tabInfos) {
            var $li = $("<li></li>");
            var $a = $("<a data-value=\"" + tabInfos[tb].id + "\">" + tabInfos[tb].name + "</a>")
            $li.append($a);
            $ul.append($li);

            if (tabInfos[tb].name == "附件") {
                var tblid = tabInfos[tb].value.tablename;
                var $div = $("<div class=\"mk-form-wrap tab-pane\" id=\"" + tabInfos[tb].id + "\"></div>");
                var $btn = $('<div></div>');

                var btns = tabInfos[tb].value.buttons;
                for (var btn in btns) {
                    var $name = btns[btn].name;
                    var $id = btns[btn].id;
                    var $class = btns[btn].class;
                    var $style = btns[btn].style;
                    var buttons = $('<div class="mk-component-control" style="text-align:right;float:right" ><a id="' + $id + '"   style="' + $style + '" class="' + $class + '">' + $name + '</a></div>');
                    $btn.append(buttons);
                }
                $div.append($btn);

                $div.append("<div id=\"" + tblid + "\"></div>");
                $content.append($div);

                var headData = [];
                var fields = tabInfos[tb].value.fields;
                for (var i in fields) {
                    var fd = fields[i];
                    if (fd.field == "F_Id") continue;

                    var item = {};
                    item.label = fd.name;
                    item.name = fd.field;
                    item.width = fd.width;
                    item.type = fd.type;
                    item.cellStyle = { 'text-align': 'left' };
                    if (fd.field == "operationTabs") {
                        item.formatter = function (value, coldata, rowData, rowItem, op, rowIndex, colname) {
                            if (colname == "operationTabs") {
                                if (!!coldata.F_Id) {
                                    return "<a href='#' onclick=\"downloadFile2('" + coldata.F_Id + "')\"><span style='color:blue'>下载附件</span></a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"previewFile('" + coldata.F_Id + "')\"><span style='color:blue'>在线预览</span></a>";
                                }
                            }
                            else {
                                return value;
                            }
                        }
                    }

                    headData.push(item);
                }
                $("#" + tblid).jfGrid({
                    headData: headData,
                    isEdit: false,
                    mainId: "F_Id",
                    bindTable: tblid,
                });

                $("#btn_uploadfile").on("click", function () {

                    $("#btn_uploadfile").mkUploader({
                        isShowDefaultButton: false,
                        parentId: dfop.keyvalue,
                        moduleType: dfop.moduleType || "Other",
                        moduleId: dfop.moduleId,
                        moduleInfoId: dfop.moduleInfoId,
                        folderName: dfop.folderName,
                        id: "Base_AnnexesFile"
                    });

                });

                $("#btn_delfile").on("click", function () {
                    var id = $("#" + tblid).jfGridValue("F_Id");
                    if (!!id) {
                        Changjie.layerConfirm('是否删除该文件？', function (res) {
                            if (res) {

                                var param = {};
                                param['__RequestVerificationToken'] = $.mkToken;
                                param['fileId'] = id;
                                Changjie.httpAsyncPost(top.$.rootUrl + "/SystemModule/Annexes/DeleteAnnexesFile", param, function (res) {
                                    if (res.code == Changjie.httpCode.success) {
                                        $("#Base_AnnexesFile").jfGridSet("removeRow", id);
                                    }
                                });
                            }
                        });
                    }
                });

                if (dfop.type == "edit") {
                    Changjie.httpAsyncGet(top.$.rootUrl + '/SystemModule/Annexes/GetAnnexesFileList?infoId=' + dfop.keyvalue, function (res) {
                        if (res.code == Changjie.httpCode.success) {
                            for (var item in res.data) {
                                if (!res.data[item].F_DownloadCount) {
                                    res.data[item].F_DownloadCount = 0;
                                }
                                var tokb = parseInt(res.data[item].F_FileSize) / 1024;
                                res.data[item].F_FileSize = (Math.round(tokb * 100) / 100) + "KB";
                                $('#Base_AnnexesFile').jfGridSet('refreshdata', res.data);
                            }
                        }
                    });
                }
            }

            if (tabInfos[tb].name == "流程") {
                var $div = "<div class=\"mk-form-wrap tab-pane\" id=\"" + tabInfos[tb].id + "\">";
                dfop.wfTabname = 'workflowHistory'
                $div += "<div id=\"" + dfop.wfTabname + "\"></div>";
                $div += "</div >";
                $content.append($div);
                $("#" + dfop.wfTabname).jfGrid({
                    url: top.$.rootUrl + '/WorkFlowModule/WorkFlow/GetHistory',
                    headData: [
                        {
                            label: "节点名称", name: "NodeName", width: 160, align: "left",
                        },
                        {
                            label: "审批结果", name: "Result", width: 160, align: "left",
                            formatter: function (cellvalue) {
                                switch (cellvalue) {
                                    case 1:
                                        return '<span class="label label-success" style="cursor: pointer;">审批通过</span>';
                                    case 2:
                                        return '<span class="label label-warning" style="cursor: pointer;">审批未通过</span>';
                                    case 3:
                                        return '<span class="label label-info" style="cursor: pointer;">驳回</span>';;
                                    case 4:
                                        return '<span class="label label-warning" style="cursor: pointer;">加签审批通过</span>';
                                    default:
                                        return '<span class="label label-default" style="cursor: pointer;">审批未通过</span>';
                                }
                            }
                        },
                        { label: "审批时间", name: "AuditTime", width: 160, align: "left" },
                        { label: "审批人", name: "AuditorUserName", width: 120, align: "left" },
                        { label: "总耗", name: "ElapsedTimeDescription", width: 120, align: "left" },
                        { label: "审批意见", name: "Description", width: 120, align: "left" }
                    ],
                    isEdit: false,
                    mainId: "ID",
                    bindTable: 'Sys_WfHistory',
                });
                if (dfop.type == "edit") {
                    if (dfop.isAutoLoadWorkflowData) {
                        $.loadWorkflowData.load(dfop);
                    }
                    else {
                        $a.on("click", function () {
                            if ($.loadWorkflowData.isload == false) {
                                $.loadWorkflowData.load(dfop);
                                $.loadWorkflowData.isload = true;
                            }
                        });

                    }
                }
            }
        }
    }

    var loadExtendTabpage = function ($ul, $content, dfop) {
        var formId = request("formId");
        if (!!formId) {
            Changjie.httpAsyncGet(top.$.rootUrl + '/SystemModule/Base_FormTabs/GetFormTabInfo?formId=' + formId, function (data) {
                if (data.code == Changjie.httpCode.success) {
                    if (data.data) {
                        var data = data.data;
                        debugger;
                        for (var _i in data) {
                            var item = data[_i];
                            var tabId = top.Changjie.newGuid();
                            var tabName = item.tabName;

                            var $li = $("<li></li>");
                            var $a = $("<a data-value=\"" + tabId + "\">" + tabName + "</a>")
                            $li.append($a);
                            $ul.append($li)

                            var $div = "<div class=\"mk-form-wrap tab-pane\" id=\"" + tabId + "\">";
                            dfop.gridname = top.Changjie.newGuid() + _i;
                            $div += "<div id=\"" + dfop.gridname + "\"></div>";
                            $div += "</div >";
                            $content.append($div);
                            $("#" + dfop.gridname).jfGrid({
                                url: top.$.rootUrl + '/SystemModule/DatabaseTable/GetTableDataListBySql',
                                headData: function (heads) {

                                    var colModelData = [];

                                    if (heads) {
                                        for (var index in heads) {
                                            var item = heads[index];

                                            if (item.IsShow == 'false' || item.IsShow == false) continue;

                                            var width = 120;
                                            var reg = /^\+?[1-9][0-9]*$/;
                                            if (item.Width && reg.test(item.Width)) {
                                                width = parseInt(item.Width);
                                            }

                                            if (!!item.isParent) {
                                                var children = [];

                                                var childCount = 0;
                                                for (var _index = 0; _index < headData.length; _index++) {
                                                    var info = headData[_index];
                                                    if (!!info.parentId) {
                                                        if (info.parentId != item.id) continue;

                                                        childCount++;
                                                        children.push({ name: info.Field, label: info.Name, width: width, align: "left" });
                                                    }
                                                }

                                                if (item.IsSeparate == 'true' || item.IsSeparate == true) {
                                                    colModelData.push({
                                                        name: item.Field, label: item.Name, width: 240, align: "center",
                                                        children: children, formatter: function (cellvalue, row) {
                                                            if (!isNaN(cellvalue))
                                                                return cellvalue.toLocaleString();;
                                                            return cellvalue;
                                                        }
                                                    });
                                                }
                                                else {
                                                    colModelData.push({
                                                        name: item.Field, label: item.Name, width: 240, align: "center",
                                                        children: children
                                                    });
                                                }
                                            }
                                            else if (!!item.parentId == false) {

                                                if (item.IsSeparate == 'true' || item.IsSeparate == true) {
                                                    colModelData.push({
                                                        name: item.Field, label: item.Name, width: width, align: "left", statistics: (item.IsSum == 'true' || item.IsSum == true), formatter: function (cellvalue, row) {
                                                            if (!isNaN(cellvalue))
                                                                return cellvalue.toLocaleString();;
                                                            return cellvalue;
                                                        }
                                                    });
                                                }
                                                else {
                                                    colModelData.push({ name: item.Field, label: item.Name, width: width, align: "left", statistics: (item.IsSum == 'true' || item.IsSum == true) });
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        for (var key in data[0]) {
                                            colModelData.push({ name: key, label: key, width: 120, align: "left" });
                                        }
                                    }
                                    return colModelData;
                                }(item.heads)
                            });
                            var param = {
                                dbId: item.dbId,
                                sql: item.sql.toLowerCase().replace("@this.id@", "'" + dfop.keyvalue + "'"),
                            };
                            $('#' + dfop.gridname).jfGridSet('reload', param);
                        }
                    }
                }
            })
            
        }
    }


    $.systemtables = {
        init: function ($self) {
            var dfop = $self[0]._systemtables.dfop;
            var index = 1;
            var tabselectid = "";
            if (dfop.state == "extend") {
                var $ul = $self.find('ul');
                var $content = $self.nextAll(".tab-content");

                $($ul).find('li').each(function (_index, _element) {
                    index += _index;
                    var $a = $(_element).find('a');
                    tabselectid = $a.attr('data-value');
                    tabselectid = tabselectid.replace(index, (index + 1));
                });
                loadTabpage($ul, $content, dfop);
                loadExtendTabpage($ul, $content, dfop);
            }
            else {

                var $ul = $("<ul class='nav nav-tabs'></ul");
                $self.append($ul);

                var $content = $("<div class='tab-content mk-tab-content' id='tab_content_sub'></div>")
                $self.after($content);

                loadTabpage($ul, $content, dfop);
                loadExtendTabpage($ul, $content, dfop);
            }
        },
        click: null,
        callback: null,
    };
    $.loadWorkflowData = {
        load: function (op) {
            op.workflowId = Changjie.workflowApi.workfolwinfo({
                tableName: mainTable,
                keyValue: op.keyvalue
            });
            $('#' + op.wfTabname).jfGridSet('reload', { workflowId: op.workflowId });
          
        },
        isload: false,
    };
    $.fn.systemtables = function (op) {
        var dfop = {
            state: 'extend',//extend(在当前选项卡后面追加tab页),newinstance(新实例)
            tabs: {
                "attachment": Changjie.attachmentSettings(),
                "workflow": Changjie.workflowSettings()
            },
            isShowAttachment: true,
            isShowWorkflow: true,
            isAutoLoadWorkflowData: false,
            keyvalue: "",
            moduleType: "",
            moduleId: "",
            moduleInfoId: "",
            folderName: "",
            type: "edit"
        };
        $.extend(dfop, op || {});
        var $self = $(this);
        dfop.id = $self.attr("id");
        if (!dfop.id) return false;

        if (!!$self[0]._systemtables) {
            return $self;
        }
        $self[0]._systemtables = { dfop: dfop };
        $.systemtables.init($self);
        return $self;
    };

})(window.jQuery, top.Changjie);
function downloadFile2(fileId) {
    window.open(top.$.rootUrl + '/SystemModule/Annexes/DownAnnexesFile2?fileId=' + fileId);
}
function downloadFile(fileId) {
    var data = {
        url: top.$.rootUrl + '/SystemModule/Annexes/DownAnnexesFile',
        param:
        {
            fileId: fileId,
            __RequestVerificationToken: $.mkToken
        },
        method: 'POST'
    };
    top.Changjie.download(data);
}


function previewFile(fileId) {
    $.ajax({
        url: top.$.rootUrl + '/SystemModule/Annexes/PreviewFile',
        type: "POST",
        data: {
            fileId: fileId,
            rnd: Math.random()
        },
        success: function (responseText) {//当保存成功之后的回调          
            if (responseText == "") {
                alert("文件不存在！")
            }
            else {
                window.open("http://" + responseText);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            top.Changjie.log(textStatus);
        }
    });
}
