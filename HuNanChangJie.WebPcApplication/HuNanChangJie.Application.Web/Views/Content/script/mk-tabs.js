﻿/*
 * 创建人：
 * 日 期：2017.03.16
 * 描 述：tab窗口操作方法
 */
(function ($, Changjie) {
    "use strict";
    //初始化菜单和tab页的属性Id
    var iframeIdList = {};

    Changjie.frameTab = {
        iframeId: '',
        init: function () {
            Changjie.frameTab.bind();
        },
        bind: function () {
            $(".mk-frame-tabs-wrap").mkscroll();
        },
        setCurrentIframeId: function (iframeId) {
            Changjie.iframeId = iframeId;
        },
        open: function (module, notAllowClosed) {
            var $tabsUl = $('#frame_tabs_ul');
            var $frameMain = $('#frame_main');

            if (iframeIdList[module.F_ModuleId] == undefined || iframeIdList[module.F_ModuleId] == null) {
                // 隐藏之前的tab和窗口
                if (Changjie.frameTab.iframeId != '') {
                    $tabsUl.find('#tab_' + Changjie.frameTab.iframeId).removeClass('active');
                    $frameMain.find('#iframe_' + Changjie.frameTab.iframeId).removeClass('active');
                    iframeIdList[Changjie.frameTab.iframeId] = 0;
                }
                var parentId = Changjie.frameTab.iframeId;
                Changjie.frameTab.iframeId = module.F_ModuleId;
                iframeIdList[Changjie.frameTab.iframeId] = 1;

                // 打开一个功能模块tab_iframe页面
                var $tabItem = $('<li class="mk-frame-tabItem active"  id="tab_' + module.F_ModuleId + '" parent-id="' + parentId + '"  ><span tabid="' + module.F_ModuleId + '">' + module.F_FullName + '</span></li>');
                if (module.topMenuId) {
                    $tabItem.on("click", function () {
                        var $topMenu = $("#" + module.topMenuId);
                        var projectMenuId = "2082f7c5-d493-43f0-8a9d-00d8fa6c6fb7";//项目菜单ID
                        if (module.topMenuId == projectMenuId)
                        {
                            $("#projectmenu").hide();
                            $("ul[data-value=" + projectMenuId + "]").show();
                            $topMenu.trigger('click');
                        }
                        else {
                            if ($topMenu.attr('class').indexOf("active") == -1) {
                                $topMenu.trigger('click');
                            }
                        }
                    });
                }
                if (module.IsProject) {
                    $tabItem.on("click", function () {
                        var projectMenuId = "2082f7c5-d493-43f0-8a9d-00d8fa6c6fb7";//项目菜单ID
                        $("#" + projectMenuId).trigger('click');
                        $("ul[data-value=" + projectMenuId + "]").hide();

                        var $projectmenu = $("#projectmenu");
                        var projectId = module.F_ModuleId;

                        
                        $projectmenu.find(".mk-menu-item").each(function (index, element) {
                            var $this = $(element);
                            $this.attr("projectId", projectId);
                           
                        });
                        $projectmenu.show();
                    });
                }
                
                // 翻译
                //Changjie.language.get(module.F_FullName, function (text) {
                //    $tabItem.find('span').text(text);
                //});

                if (!notAllowClosed) {
                    $tabItem.append('<span class="reomve" title="关闭窗口"></span>');
                }
                var tabUrl = $.rootUrl + module.F_UrlAddress;
                if (tabUrl.indexOf("?") > -1) {
                    tabUrl += "&moduleId=" + module.F_ModuleId;
                    if (!!module.projectId) {
                        tabUrl += "&projectId=" + module.projectId;
                    }
                }
                else {
                    tabUrl += "?moduleId=" + module.F_ModuleId;
                    if (!!module.projectId) {
                        tabUrl += "&projectId=" + module.projectId;
                    }
                }

                var $iframe = $('<iframe class="mk-frame-iframe active" id="iframe_' + module.F_ModuleId + '" frameborder="0" src="'+tabUrl+ '"></iframe>');
                $tabsUl.append($tabItem);
                $frameMain.append($iframe);

                var w = 0;
                var width = $tabsUl.children().each(function () {
                    w += $(this).outerWidth();
                });
                $tabsUl.css({ 'width': w });
                $tabsUl.parent().css({ 'width': w });


                $(".mk-frame-tabs-wrap").mkscrollSet('moveRight');

             

                //绑定一个点击事件
                $tabItem.on('click', function () {
                    var id = $(this).attr('id').replace('tab_', '');
                    Changjie.frameTab.focus(id);
                });
                $tabItem.find('.reomve').on('click', function () {
                    var id = $(this).parent().attr('id').replace('tab_', '');
                    Changjie.frameTab.close(id);
                    return false;
                });

                if (!!Changjie.frameTab.opencallback) {
                    Changjie.frameTab.opencallback();
                }
                if (!notAllowClosed) {
                    $.ajax({
                        url: top.$.rootUrl + "/Home/VisitModule",
                        data: { moduleName: module.F_FullName, moduleUrl: module.F_UrlAddress },
                        type: "post",
                        dataType: "text"
                    });
                }
            }
            else {
                Changjie.frameTab.focus(module.F_ModuleId);
            }
        },
        focus: function (moduleId) {
            if (iframeIdList[moduleId] == 0) {
                // 定位焦点tab页
                $('#tab_' + Changjie.frameTab.iframeId).removeClass('active');
                $('#iframe_' + Changjie.frameTab.iframeId).removeClass('active');
                iframeIdList[Changjie.frameTab.iframeId] = 0;

                $('#tab_' + moduleId).addClass('active');
                $('#iframe_' + moduleId).addClass('active');
                Changjie.frameTab.iframeId = moduleId;
                iframeIdList[moduleId] = 1;

                if (!!Changjie.frameTab.opencallback) {
                    Changjie.frameTab.opencallback();
                }
            }
        },
        leaveFocus: function () {
            $('#tab_' + Changjie.frameTab.iframeId).removeClass('active');
            $('#iframe_' + Changjie.frameTab.iframeId).removeClass('active');
            iframeIdList[Changjie.frameTab.iframeId] = 0;
            Changjie.frameTab.iframeId = '';
        },
        closeallprojecttab: function () {
            $.each(iframeIdList, function (ii, ee) {
                if (ii != '10000') {
                    //var tabtemp = $('#tab_' + ii);
                    //var iframetemp = $('#iframe_' + ii);
                    //if (tabtemp && tabtemp.length > 0 && tabtemp.get(0).innerText != "全部项目" && $('#iframe_' + ii).attr("src").indexOf('projectId=') != -1)
                    //    Changjie.frameTab.close(ii);

                    var iframetemp = $('#iframe_' + ii);
                    if (iframetemp && iframetemp.attr("src") && iframetemp.attr("src").indexOf('projectId=') != -1)
                        Changjie.frameTab.close(ii);
                }
            })
        },
        close: function (moduleId) {
            delete iframeIdList[moduleId];

            var $this = $('#tab_' + moduleId);
            var $prev = $this.prev();// 获取它的上一个节点数据;
            if ($prev.length < 1) {
                $prev = $this.next();
            }
            $this.remove();
            $('#iframe_' + moduleId).remove();
            if (moduleId == Changjie.frameTab.iframeId && $prev.length > 0) {
                var prevId = $prev.attr('id').replace('tab_', '');

                $prev.addClass('active');
                $('#iframe_' + prevId).addClass('active');
                Changjie.frameTab.iframeId = prevId;
                iframeIdList[prevId] = 1;
            }
            else {
                if ($prev.length < 1) {
                    Changjie.frameTab.iframeId = "";
                }
            }

            var $tabsUl = $('#frame_tabs_ul');
            var w = 0;
            var width = $tabsUl.children().each(function () {
                w += $(this).outerWidth();
            });
            $tabsUl.css({ 'width': w });
            $tabsUl.parent().css({ 'width': w });

            if (!!Changjie.frameTab.closecallback) {
                Changjie.frameTab.closecallback();
            }
        }
        // 获取当前窗口
        ,currentIframe: function () {
            var ifameId = 'iframe_' + Changjie.frameTab.iframeId;
            if (top.frames[ifameId].contentWindow != undefined) {
                return top.frames[ifameId].contentWindow;
            }
            else {
                return top.frames[ifameId];
            }
        }
        ,parentIframe: function () {
            var ifameId = 'iframe_' + top.$('#tab_'+Changjie.frameTab.iframeId).attr('parent-id');
            if (top.frames[ifameId].contentWindow != undefined) {
                return top.frames[ifameId].contentWindow;
            }
            else {
                return top.frames[ifameId];
            }
        }


        , opencallback: false
        , closecallback: false
    };

    Changjie.frameTab.init();
})(window.jQuery, top.Changjie);
