﻿(function ($, Changjie) {
    "use strict";
    $.fn.calculateGet = function (type, rows, column, currentColname) {
        
        if (JSON.stringify(rows) == "{}") return 0;
        var $input = $("input[autocalculate='" + type + "@" + column + "']");
        var result = 0;
        switch (type) {
            case "sum":
                for (var row in rows) {
                    var cellValue = top.Changjie.clearNumSeparator(rows[row][currentColname]);
                    var value = parseFloat(cellValue);
                    var isnan = isNaN(value);
                    if (!isnan) {
                        result += value;
                    }
                }
                break;
            case "average":
                for (var row in rows) {
                    var cellValue = top.Changjie.clearNumSeparator(rows[row][currentColname]);
                    var value = parseFloat(cellValue);
                    var isnan = isNaN(value);
                    if (!isnan) {
                        result += value;
                    }
                }
                result= result / rows.length;
                break;
            case "max":
                var max = 0;
                var cellValue = top.Changjie.clearNumSeparator(rows[0][currentColname]);
                var val = isNaN(parseFloat(cellValue));
                if (!val) {
                    max = parseFloat(cellValue);
                }
                for (var i = 1; i < rows.length; i++) {
                    var cellValue = top.Changjie.clearNumSeparator(rows[i][currentColname]);
                    var value = isNaN(parseFloat(cellValue)) ? 0 : parseFloat(cellValue);
                    if (max >= value) {
                        max = max;
                    }
                    else {
                        max = value;
                    }
                }
                result = max;
                break;
            case "mix":
                var mix = 0;
                var cellValue = top.Changjie.clearNumSeparator(rows[0][currentColname]);
                var val = isNaN(parseFloat(cellValue));
                if (!val) {
                    mix = parseFloat(cellValue);
                }
                for (var i = 1; i < rows.length; i++) {
                    var cellValue = top.Changjie.clearNumSeparator(rows[i][currentColname]);
                    var value = isNaN(parseFloat(cellValue)) ? 0 : parseFloat(cellValue);
                    if (mix <= value) {
                        mix = mix;
                    }
                    else {
                        mix = value;
                    }
                }
                result = mix;
                break;
            case "sumcols":
                result = rows.length;
                break;
        }

        result= top.Changjie.numberFormat(result, top.Changjie.sysGlobalSettings.pointGlobal);
        $input.val(result);
    };

})(jQuery,top.Changjie)
