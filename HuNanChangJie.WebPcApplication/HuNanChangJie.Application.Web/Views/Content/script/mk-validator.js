﻿/*
 * 日 期：2017.03.16
 * 描 述：表单数据验证完整性
 */
(function ($, Changjie) {
    "use strict";
    
    $.mkValidformMessage = function ($this, errormsg) {
        /*错误处理*/
        $this.addClass('mk-field-error');
        $this.parent().append('<div class="mk-field-error-info" title="' + errormsg + '！"><i class="fa fa-info-circle"></i></div>');
        var validatemsg = $this.parent().find('.mk-form-item-title').text().replace('*','') +  errormsg;
        Changjie.alert.error('表单信息输入有误,请检查！</br>' + validatemsg);
        if ($this.attr('type') == 'mkselect') {
            $this.on('change', function () {
                removeErrorMessage($(this));
            })
        }
        else if ($this.attr('type') == 'formselect') {
            $this.on('change', function () {
                removeErrorMessage($(this));
            });
        }
        else if ($this.attr("class").indexOf("checkbox")>-1) {
            $this.on('change', function () {
                removeErrorMessage($(this));
            });
        }
        else if ($this.hasClass('mk-input-wdatepicker')) {
            $this.on('blur', function () {
                //alert(2);
                var $input = $(this);
                //alert(3);
                if ($input.val()) {
                    //alert(4);
                    removeErrorMessage($input);
                }
            });
        }
        else {
            $this.on('input propertychange', function () {
                var $input = $(this);
                if ($input.val()) {
                    removeErrorMessage($input);
                }
            });
        }
    };

    $.fn.mkRemoveValidMessage = function () {
        removeErrorMessage($(this));
    }

    $.fn.mkValidform = function () {
        var validateflag = true;
        var validHelper = Changjie.validator;
        var formdata = $(this).mkGetFormData();
        
        $(this).find("[isvalid=yes]").each(function () {
            var $this = $(this);

            if ($this.parent().find('.mk-field-error-info').length > 0) {
                validateflag = false;
                return true;
            }

            var checkexpession = $(this).attr("checkexpession");
            
            if (checkexpession == "checkbox") {
                var count = 0;
                $(".checkbox").find("input[type='checkbox']").each(function () {
                    var $this = $(this);
                    if ($this.prop("checked"))
                        count++;
                });
                $(".checkbox").find("input[type='radio']").each(function () {
                    var $this = $(this);
                    if ($this.prop("checked"))
                        count++;
                });
                if (count == 0) {
                    validateflag = false;
                    $.mkValidformMessage($this, "请至少选择一项");
                }

 
            }
            else {
                var checkfn = validHelper['is' + checkexpession];
                if (!checkexpession || !checkfn) { return false; }
                var errormsg = $(this).attr("errormsg") || "";

                var id = $this.attr('id');
                var value = formdata[id];

                //switch (checkexpession) {
                //    case "Num":
                //        value = top.Changjie.numberFormat(value, 0);
                //        break;
                //    case "Float":
                //        value = top.Changjie.numberFormat(value, 2);
                //        break;
                //}
                //var type = $this.attr('type');
                //if (type == 'mkselect') {
                //    value = $this.mkselectGet();
                //}
                //else if (type == 'formselect') {
                //    value = $this.mkformselectGet();
                //}
                //else {
                //    value = $this.val();
                //}
                var r = { code: true, msg: '' };
                if (checkexpession == 'LenNum' || checkexpession == 'LenNumOrNull' || checkexpession == 'LenStr' || checkexpession == 'LenStrOrNull') {
                    var len = $this.attr("length");
                    r = checkfn(value, len);
                } else {
                    r = checkfn(value);
                }
                if (!r.code) {
                    validateflag = false;
                    $.mkValidformMessage($this, errormsg + r.msg);
                }
            }
        });
        return validateflag;
    }

    function removeErrorMessage($obj) {
        $obj.removeClass('mk-field-error');
        $obj.parent().find('.mk-field-error-info').remove();
    }

})(window.jQuery, top.Changjie);
