﻿$.LoadReport = function (options) {
    var defaults = {
        geoCoordMap: {
            '上海': [121.4648, 31.2891],
            '东莞': [113.8953, 22.901],
            '东营': [118.7073, 37.5513],
            '中山': [113.4229, 22.478],
            '临汾': [111.4783, 36.1615],
            '临沂': [118.3118, 35.2936],
            '丹东': [124.541, 40.4242],
            '丽水': [119.5642, 28.1854],
            '乌鲁木齐': [87.9236, 43.5883],
            '佛山': [112.8955, 23.1097],
            '保定': [115.0488, 39.0948],
            '兰州': [103.5901, 36.3043],
            '甘肃': [103.5901, 36.3043],
            '包头': [110.3467, 41.4899],
            '北京': [116.4551, 40.2539],
            '北海': [109.314, 21.6211],
            '南京': [118.8062, 31.9208],
            '江苏': [118.8062, 31.9208],
            '南宁': [108.479, 23.1152],
            '广西': [108.479, 23.1152],
            '南昌': [116.0046, 28.6633],
            '江西': [116.0046, 28.6633],
            '南通': [121.1023, 32.1625],
            '厦门': [118.1689, 24.6478],
            '台州': [121.1353, 28.6688],
            '合肥': [117.29, 32.0581],
            '安徽': [117.29, 32.0581],
            '呼和浩特': [111.4124, 40.4901],
            '咸阳': [108.4131, 34.8706],
            '哈尔滨': [127.9688, 45.368],
            '黑龙江': [127.9688, 45.368],
            '唐山': [118.4766, 39.6826],
            '嘉兴': [120.9155, 30.6354],
            '大同': [113.7854, 39.8035],
            '大连': [122.2229, 39.4409],
            '天津': [117.4219, 39.4189],
            '太原': [112.3352, 37.9413],
            '山西': [112.3352, 37.9413],
            '威海': [121.9482, 37.1393],
            '宁波': [121.5967, 29.6466],
            '宝鸡': [107.1826, 34.3433],
            '宿迁': [118.5535, 33.7775],
            '常州': [119.4543, 31.5582],
            '广州': [113.5107, 23.2196],
            '广东': [113.5107, 23.2196],
            '廊坊': [116.521, 39.0509],
            '延安': [109.1052, 36.4252],
            '张家口': [115.1477, 40.8527],
            '徐州': [117.5208, 34.3268],
            '德州': [116.6858, 37.2107],
            '惠州': [114.6204, 23.1647],
            '成都': [103.9526, 30.7617],
            '四川': [103.9526, 30.7617],
            '扬州': [119.4653, 32.8162],
            '承德': [117.5757, 41.4075],
            '拉萨': [91.1865, 30.1465],
            '无锡': [120.3442, 31.5527],
            '日照': [119.2786, 35.5023],
            '昆明': [102.9199, 25.4663],
            '云南': [102.9199, 25.4663],
            '杭州': [119.5313, 29.8773],
            '浙江': [119.5313, 29.8773],
            '枣庄': [117.323, 34.8926],
            '柳州': [109.3799, 24.9774],
            '株洲': [113.5327, 27.0319],
            '武汉': [114.3896, 30.6628],
            '湖北': [114.3896, 30.6628],
            '汕头': [117.1692, 23.3405],
            '江门': [112.6318, 22.1484],
            '沈阳': [123.1238, 42.1216],
            '辽宁': [123.1238, 42.1216],
            '沧州': [116.8286, 38.2104],
            '河源': [114.917, 23.9722],
            '泉州': [118.3228, 25.1147],
            '泰安': [117.0264, 36.0516],
            '泰州': [120.0586, 32.5525],
            '济南': [117.1582, 36.8701],
            '山东': [117.1582, 36.8701],
            '济宁': [116.8286, 35.3375],
            '海口': [110.3893, 19.8516],
            '海南': [110.3893, 19.8516],
            '淄博': [118.0371, 36.6064],
            '淮安': [118.927, 33.4039],
            '深圳': [114.5435, 22.5439],
            '清远': [112.9175, 24.3292],
            '温州': [120.498, 27.8119],
            '渭南': [109.7864, 35.0299],
            '湖州': [119.8608, 30.7782],
            '湘潭': [112.5439, 27.7075],
            '滨州': [117.8174, 37.4963],
            '潍坊': [119.0918, 36.524],
            '烟台': [120.7397, 37.5128],
            '玉溪': [101.9312, 23.8898],
            '珠海': [113.7305, 22.1155],
            '盐城': [120.2234, 33.5577],
            '盘锦': [121.9482, 41.0449],
            '石家庄': [114.4995, 38.1006],
            '河北': [114.4995, 38.1006],
            '福州': [119.4543, 25.9222],
            '福建': [119.4543, 25.9222],
            '秦皇岛': [119.2126, 40.0232],
            '绍兴': [120.564, 29.7565],
            '聊城': [115.9167, 36.4032],
            '肇庆': [112.1265, 23.5822],
            '舟山': [122.2559, 30.2234],
            '苏州': [120.6519, 31.3989],
            '莱芜': [117.6526, 36.2714],
            '菏泽': [115.6201, 35.2057],
            '营口': [122.4316, 40.4297],
            '葫芦岛': [120.1575, 40.578],
            '衡水': [115.8838, 37.7161],
            '衢州': [118.6853, 28.8666],
            '西宁': [101.4038, 36.8207],
            '青海': [101.4038, 36.8207],
            '西安': [109.1162, 34.2004],
            '陕西': [109.1162, 34.2004],
            '贵阳': [106.6992, 26.7682],
            '贵州': [106.6992, 26.7682],
            '连云港': [119.1248, 34.552],
            '邢台': [114.8071, 37.2821],
            '邯郸': [114.4775, 36.535],
            '郑州': [113.4668, 34.6234],
            '河南': [113.4668, 34.6234],
            '鄂尔多斯': [108.9734, 39.2487],
            '重庆': [107.7539, 30.1904],
            '金华': [120.0037, 29.1028],
            '铜川': [109.0393, 35.1947],
            '银川': [106.3586, 38.1775],
            '宁夏': [106.3586, 38.1775],
            '镇江': [119.4763, 31.9702],
            '长春': [125.8154, 44.2584],
            '吉林': [125.8154, 44.2584],
            '长沙': [113.0823, 28.2568],
            '湖南': [113.0823, 28.2568],
            '长治': [112.8625, 36.4746],
            '阳泉': [113.4778, 38.0951],
            '青岛': [120.4651, 36.3373],
            '韶关': [113.7964, 24.7028]
        },

        provinces: {
            //23个省
            "台湾": "taiwan",
            "河北": "hebei",
            "山西": "shanxi",
            "辽宁": "liaoning",
            "吉林": "jilin",
            "黑龙江": "heilongjiang",
            "江苏": "jiangsu",
            "浙江": "zhejiang",
            "安徽": "anhui",
            "福建": "fujian",
            "江西": "jiangxi",
            "山东": "shandong",
            "河南": "henan",
            "湖北": "hubei",
            "湖南": "hunan",
            "广东": "guangdong",
            "海南": "hainan",
            "四川": "sichuan",
            "贵州": "guizhou",
            "云南": "yunnan",
            "陕西": "shanxi1",
            "甘肃": "gansu",
            "青海": "qinghai",
            //5个自治区
            "新疆": "xinjiang",
            "广西": "guangxi",
            "内蒙古": "neimenggu",
            "宁夏": "ningxia",
            "西藏": "xizang",
            //4个直辖市
            "北京": "beijing",
            "天津": "tianjin",
            "上海": "shanghai",
            "重庆": "chongqing",
            //2个特别行政区
            "香港": "xianggang",
            "澳门": "aomen"
        },
        convertData: function (data) {
            var res = [];
            for (var i = 0; i < data.length; i++) {
                var geoCoord = $.geoCoordMap[data[i].name];
                if (geoCoord) {
                    res.push({
                        name: data[i].name,
                        value: geoCoord.concat(data[i].value)
                    });
                }
            }



            return res;
        },
        convertDataFlow: function (data) {


            var res = [];
            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                var fromCoord = $.geoCoordMap[dataItem[0].name];
                var toCoord = $.geoCoordMap[dataItem[1].name];
                if (fromCoord && toCoord) {
                    res.push({
                        fromName: dataItem[0].name,
                        toName: dataItem[1].name,
                        coords: [fromCoord, toCoord]
                    });
                }
            }
            return res;
        },
        option: {}
    };

    $.extend(defaults);
    $.ajax({
        url: options.url,
        cache: false,
        async: false,
        dataType: 'json',
        success: function (data) {
            var $echart, $list;
            if (data.queryData) {
                var $search = $('<div id="searchArea" class="no-print"></div>');
                options.element.append($search);
                DrawSearchArea($search, data);
                console.log(data, 1234)
            }
            if (data.tempStyle == 1) {
                if (data.listData.length > 0) {
                    $list = $('<div id="gridtable" class="mk-layout-body"></div>');
                    options.element.append($list);

                    DrawList(data.listData, $list, data.headData, data.treeSettings);
                }
            } else if (data.tempStyle == 2) {
                if (data.chartData.length > 0) {
                    $echart = $('<div id="echart" style="width: 100%; height: 350px;"></div>');
                    options.element.append($echart);
                    switch (data.chartType) {
                        case 'pie':
                            DrawPie(data.chartData, $echart[0]);
                            break;
                        case 'bar':
                            DrawBar(data.chartData, $echart[0]);
                            break;
                        case 'line':
                            DrawLine(data.chartData, $echart[0]);
                            break;
                        case 'map':
                            DrawMap(data.chartData, $echart[0], "/Content/json/map_json/china.json", 'china');
                            break;
                        case 'flowmap':
                            DrawFlowMap(data.chartData, $echart[0], "/Content/json/map_json/china.json", 'china');
                            break;
                        default:
                    }
                }
            } else {
                if (data.chartData.length > 0) {
                    $echart = $('<div id="echart" style="width: 100%; height: 350px;"></div>');
                    options.element.append($echart);
                    switch (data.chartType) {
                        case 'pie':
                            DrawPie(data.chartData, $echart[0]);
                            break;
                        case 'bar':
                            DrawBar(data.chartData, $echart[0]);
                            break;
                        case 'line':
                            DrawLine(data.chartData, $echart[0]);
                            break;
                        default:
                    }

                }
                if (data.listData.length > 0) {
                    $list = $('<div id="gridtable" class="mk-layout-body"></div>');
                    options.element.append($list);
                    DrawList(data.listData, $list, data.headData, data.treeSettings);
                }
            }
        }
    });
    function DrawPie(data, echartElement) {
        var myChart = echarts.init(echartElement);
        var option = ECharts.ChartOptionTemplates.Pie(data);
        myChart.setOption(option);
    }
    function DrawBar(data, echartElement) {
        var myChart = echarts.init(echartElement);
        var option = ECharts.ChartOptionTemplates.Bars(data, 'bar', false);
        myChart.setOption(option);
    }
    function DrawLine(data, echartElement) {
        var myChart = echarts.init(echartElement);
        var option = ECharts.ChartOptionTemplates.Lines(data, 'line', true);
        myChart.setOption(option);
    }
    function DrawSearchArea(element, data) {
        var queryData = data.queryData
        var count = 0;

        for (var i = 0; i < queryData.length; i++) {
            var row = queryData[i];
            var $cell = $('<div class="col-xs-3 mk-form-item"></div>')
            element.append($cell);

            var $title = $('<div class="mk-form-item-title language">' + row.Name + '</div>');
            $cell.append($title);
            switch (row.ControlType) {
                case "input":
                    count++;
                    $cell.append('<input id="' + row.Field + '" DbType="' + row.DbType + '"  CompareType="' + row.CompareType + '" issearchbox="true" searchcontroltype="' + row.ControlType + '" type="text" class="form-control"/>');
                    break;
                case "dropDownList":
                    count++;
                    var $control = $('<div id="' + row.Field + '" DbType="' + row.DbType + '"  CompareType="' + row.CompareType + '" issearchbox="true" searchcontroltype="' + row.ControlType + '"></div>')
                    $cell.append($control);
                    InitSelect($control, row);
                    break;

                case "dateSection":
                    count++;
                    var $control = $('<div id="' + row.Field + '" DbType="' + row.DbType + '"  CompareType="' + row.CompareType + '" issearchbox="true" searchcontroltype="' + row.ControlType + '"  />')
                    $cell.append($control);
                    $control.mkdate({
                        dfdata: [
                            { name: '今天', begin: function () { return top.Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return top.Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                            { name: '近7天', begin: function () { return top.Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return top.Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                            { name: '近1个月', begin: function () { return top.Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return top.Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                            { name: '近3个月', begin: function () { return top.Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return top.Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                            { name: '一年内', begin: function () { return top.Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return top.Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                        ],
                        // 月
                        mShow: true,
                        premShow: false,
                        // 季度
                        jShow: false,
                        prejShow: false,
                        // 年
                        ysShow: false,
                        yxShow: false,
                        preyShow: false,
                        yShow: false,
                        // 默认
                        dfvalue: 'currentM',
                        selectfn: function (begin, end) {
                            console.log(begin, end, "mk-chartshelper.js")
                        }
                    });;
                    break;
                case "dateBox":
                    count++;
                    var $control = $('<input id="' + row.Field + '" DbType="' + row.DbType + '"  CompareType="' + row.CompareType + '" issearchbox="true" searchcontroltype="' + row.ControlType + '"  type="text" class="form-control mk-input-wdatepicker"  />')
                    $cell.append($control);
                    $control.on("focus", function () {
                        var $this = $(this);
                        WdatePicker({
                            dateFmt: 'yyyy-MM-dd',
                            onpicked: function () {
                                $this.trigger('change');
                            }
                        })
                    });
                    break;
                case "hidden":
                    var $input = $('<input id="' + row.Field + '" defalutvalue type="hidden"  />');
                    $cell.append($input);
                    $cell.hide();
                    if (row.ValueType == "System" && row.DefaultValue == "CurrentProject") {
                        $input.val(request("projectId"));
                    }
                    break;
            }
        }
        if (count > 0) {
            var $cell = $('<div class="col-xs-3 mk-form-item"></div>')
            element.append($cell);
            $search = $('<a id="btn_querySearch" class="btn btn-primary  btn-sm" style="margin-top:-12px">&nbsp;查&nbsp;询</a>');
            $cell.append($search);
            count++;
            //打印 暂时屏蔽 字段太多 实际打印显示不全 引发一系列的问题


            $excel = $('<a id="btn_excel" class="btn btn-info  btn-sm" style="margin-top:-12px;margin-left:4px">&nbsp;导&nbsp;出</a>');
            $excel.on("click", function () {
                var csvhead = '', csvbody = '';
                var fileds = [];
                if (data.headData && Array.isArray(data.listData)) {
                    data.headData.forEach(function (currentValue, index, arr) {
                        if (currentValue.IsShow && currentValue.IsShow != "false") {
                            if (csvhead != '')
                                csvhead += ','
                            csvhead += currentValue.Name;

                            fileds.push(currentValue.Field);
                        }
                    })
                }

                //报表导出为查询条件全量数据
                var param = {};
                param.queryJson = [];
                if ($("input[defalutvalue]").length > 0) {
                    $input = $("input[defalutvalue]");
                    param.queryJson.push({ Field: $input.attr("id"), Value: $input.val(), CompareType: "Equal", DbType: "String" });
                }
                $("[issearchbox='true']").each(function () {
                    var $this = $(this);
                    var info = {};
                    info.Field = $this.attr("id");
                    var value = "";
                    var controlType = $this.attr("searchcontroltype");
                    switch (controlType) {
                        case "input":
                        case "dateBox":
                            value = $this.val();
                            break;
                        case "dropDownList":
                            value = $this.mkselectGet();
                            break;
                        case "dateSection":
                            value = $this.mkdateget();
                            break;
                    }
                    console.log(value, controlType)
                    if (value) {
                        info.CompareType = $this.attr("CompareType");
                        info.Value = value;
                        info.DbType = $this.attr("DbType");
                        param.queryJson.push(info);
                    }
                });

                var queryJson = JSON.stringify(param.queryJson);

                var query = {
                    reportId: request('reportId'),
                    queryJson: queryJson,
                    projectId: request('projectId')
                }

                //alert(queryJson);
                //alert(11);

                $.ajax({
                    url: top.$.rootUrl + "/ReportModule/ReportManage/GetReportDateList?reportId=" + request('reportId') + "&queryJson=" + queryJson + "&projectId=" + request('projectId'),
                    cache: false,
                    type:"post",
                    async: false,
                    dataType: 'json',
                    data: query,
                    success: function (exceldata) {
                        //alert(1);
                        var listData = exceldata.rows;// $("#gridtable").jfGridGet("rowdatas");
                        if (listData && Array.isArray(listData)) {
                            //alert(2);
                            listData.forEach(function (currentValue, index, arr) {
                                var csv = '\n';
                                fileds.forEach(function (currentValueI, indexI, arrI) {
                                    if (csv != '' && csv != '\n')
                                        csv += ','
                                    //console.log(currentValue[currentValueI], typeof currentValue[currentValueI])
                                    if (currentValue[currentValueI] != null && currentValue[currentValueI] != "null") {
                                        var value = "\t" + currentValue[currentValueI] + "\t";
                                        value= value.replace(/,/g, ':')
                                        //alert(value.replace(/,/g, ':'));
                                        csv += value;
                                    }
                                })

                                csvbody += csv;
                            })
                        }

                        var date = new Date();
                        saveCSV(csvhead + csvbody, data.reportName + date.getFullYear().toString() + (date.getMonth() + 1).toString() + date.getDate().toString() + '.csv')
                    }
                });
            })
            $cell.append($excel);
            count++;

            $print = $('<a id="btn_print" class="btn btn-info  btn-sm" style="margin-top:-12px;margin-left:4px">&nbsp;打&nbsp;印</a>');
            $print.on("click", function () {
                Print('#gridtable', {
                    onStart: function () {
                        console.log('onStart', new Date())
                    },
                    onEnd: function () {
                        console.log('onEnd', new Date())
                    }
                })

            })
            $cell.append($print);
            count++;

            var rows = Math.ceil(count / 4);
            var height = rows * 40;
            element.css("width", "100%").css("background-color", "white").css("height", "" + height + "px");
            element.css("line-height", "35px").css("vertical-align", "middle");

            $search.on("click", function () {
                search();
            });

        }
    }
    function InitSelect(control, row) {
       
        switch (row.DropDownListType) {
            case "ShopList":
                if (row.isTextParameter)
                {
                    control.mkDataSourceSelect({ type: row.isMultiSelect ? 'multiple' : 'default', code: 'SHOPS', value: 'shopname', text: 'shopname' });
                }
                else
                {
                    control.mkDataSourceSelect({ type: row.isMultiSelect ? 'multiple' : 'default', code: 'SHOPS', value: 'id', text: 'shopname' });
                }
                break;
            case "ContractList":
                control.mkDataSourceSelect({ code: 'PurchaseContractList', value: 'id', text: 'name' });
                break;
            case "SubContractList":
                control.mkDataSourceSelect({ code: 'FBHTLB', value: 'id', text: 'name' });
                break;
            case "CompanyList":
                control.mkCompanySelect({});
                break;
            case "DepartmentList":
                control.mkDepartmentSelect({});
                break;
            case "UserList":
                control.mkUserSelect({});
                break;
            case "CustomerList":
                control.mkDataSourceSelect({ code: 'KHLB', value: 'id', text: 'fullname' });
                break;
            case "SupplierList":
                control.mkDataSourceSelect({ code: 'SupplierList', value: 'id', text: 'name' });
                break;
            case "SubcontractingList":
                control.mkDataSourceSelect({ code: 'FBDW', value: 'id', text: 'name' });
                break;
        }
    }
    function DrawList(data, listElement, headData, treeSettings) {
        var reportId = request("reportId");
        var projectId = request("projectId");
        listElement.jfGrid({
            url: top.$.rootUrl + '/ReportModule/ReportManage/GetReportDateListByPager?reportId=' + reportId + "&projectId=" + request('projectId'),
            headData: function () {

                var colModelData = [];
                if (headData) {
                    for (var index in headData) {
                        var item = headData[index];

                        if (item.IsShow == 'false' || item.IsShow == false) continue;

                        var width = 120;
                        var reg = /^\+?[1-9][0-9]*$/;
                        if (item.Width && reg.test(item.Width)) {
                            width = parseInt(item.Width);
                        }

                        if (!!item.isParent) {
                            var children = [];

                            var childCount = 0;
                            for (var _index = 0; _index < headData.length; _index++) {
                                var info = headData[_index];
                                if (!!info.parentId) {
                                    if (info.parentId != item.id) continue;

                                    childCount++;
                                    children.push({ name: info.Field, label: info.Name, width: width, align: "left" });
                                }
                            }

                            if (item.IsSeparate == 'true' || item.IsSeparate == true) {
                                colModelData.push({
                                    name: item.Field, label: item.Name, width: 240, align: "center",
                                    children: children, formatter: function (cellvalue, row) {
                                        if (!isNaN(cellvalue))
                                            return cellvalue.toLocaleString();;
                                        return cellvalue;
                                    }
                                });
                            }
                            else {
                                colModelData.push({
                                    name: item.Field, label: item.Name, width: 240, align: "center",
                                    children: children
                                });
                            }
                        }
                        else if (!!item.parentId == false) {

                            if (item.IsSeparate == 'true' || item.IsSeparate == true) {
                                colModelData.push({
                                    name: item.Field, label: item.Name, width: width, align: "left", statistics: (item.IsSum == 'true' || item.IsSum == true), formatter: function (cellvalue, row) {
                                        if (!isNaN(cellvalue))
                                            return cellvalue.toLocaleString();;
                                        return cellvalue;
                                    }
                                });
                            }
                            else {
                                var cmdp = { name: item.Field, label: item.Name, width: width, align: "left", statistics: (item.IsSum == 'true' || item.IsSum == true) };
                                if (item.ThroughPage && JSON.parse(item.ThroughPage).jumpUrl) {
                                    var f = JSON.parse(item.ThroughPage);
                                    cmdp.formatter = function (value, row) {
                                        //console.log(row, row[f.jumpParam.toLowerCase()]);

                                        var _name = f.targetTitle == undefined ? "" : f.targetTitle;
                                        var paramObj = [];
                                        if (f.pName) {
                                            var pp = f.pName.split(",");
                                            for (var i = 0; i < pp.length; i++) {
                                                var ppobj = new Object();
                                                ppobj[pp[i]] = row[pp[i].toLowerCase()]
                                                paramObj.push(ppobj);
                                            }
                                        }
                                        return "<a onclick='jumpThrough(\"" + JSON.stringify(paramObj).replace(new RegExp(/(\")/g),"\\\"") + "\",\"" + _name + "\",\"" + f.jumpUrl + "\",this,\"" + value + "\",\"" + f.reportId + "\",\"" + f.queryJson + "\",\"" + f.moduleId + "\",\"" + f.formId + "\",\"" + f.jumpParam + "\",\"" + row[f.jumpParam.toLowerCase()] + "\")'>" + value + "</a>";
                                    };
                                }
                                colModelData.push(cmdp);
                            }
                        }
                    }
                }
                else {
                    for (var key in data[0]) {
                        colModelData.push({ name: key, label: key, width: 120, align: "left" });
                    }
                }
                return colModelData;
            }(),
            isTree: treeSettings.IsTree,
            parentId: treeSettings.ParentField,
            isPage: true,
            //rowdatas: data,
            isAutoHeight: true
        });

        search();
    }
    function search() {
        var param = {};
        param.queryJson = [];
        if ($("input[defalutvalue]").length > 0) {
            $input = $("input[defalutvalue]");
            param.queryJson.push({ Field: $input.attr("id"), Value: $input.val(), CompareType: "Equal", DbType: "String" });
        }
        $("[issearchbox='true']").each(function () {
            var $this = $(this);
            var info = {};
            info.Field = $this.attr("id");
            var value = "";
            var controlType = $this.attr("searchcontroltype");
            switch (controlType) {
                case "input":
                case "dateBox":
                    value = $this.val();
                    break;
                case "dropDownList":
                    value = $this.mkselectGet();
                    break;
                case "dateSection":
                    value = $this.mkdateget();
                    break;
            }
            console.log(value, controlType)
            if (value) {
                info.CompareType = $this.attr("CompareType");
                info.Value = value;
                info.DbType = $this.attr("DbType");
                param.queryJson.push(info);
            }


        });
        debugger;
        $("#gridtable").jfGridSet('reload', { queryJson: JSON.stringify(param.queryJson) });
    }
    //画迁徙地图
    function DrawFlowMap(data, echartElement, mapPath, mapName) {
        var myChart = echarts.init(echartElement);
        $.get(mapPath, function (getJSON) {
            echarts.registerMap(mapName, getJSON);

            //地图流向图-迁徙图
            var flow_mapdatas = ECharts.ChartDataFormate.FormateGroupData(data, 'flowmap').series;

            var planePath = 'path:// M878.229514 645.809634c-40.072094-279.906746-298.66716-645.809634-375.45455-645.809634-46.917869 0-327.58588 367.1461-375.462032 645.809634-46.003852 267.897962 168.10054 378.190366 375.462032 378.190366S917.497324 920.091997 878.229514 645.809634zM683.499037 658.740541 572.745883 658.740541l0 110.774975-139.920641 0L432.825243 658.740541 322.072089 658.740541 322.072089 518.841722l110.753154 0L432.825243 408.074228l139.920641 0 0 110.767494L683.499037 518.841722 683.499037 658.740541z';
            var color = ['#a6c84c', '#ffa022', '#46bee9'];
            var TopData = [];
            for (var topi in flow_mapdatas) {
                var myobject = [];
                myobject[0] = flow_mapdatas[topi].name //最外层
                var BJData = [];
                for (var si in flow_mapdatas[topi].value) {
                    var myboject2 = [];//内层数组
                    myboject2[0] = { name: myobject[0] };
                    myboject2[1] = { name: flow_mapdatas[topi].value[si].name, value: flow_mapdatas[topi].value[si].value };
                    BJData.push(myboject2);
                }
                myobject[1] = BJData;
                TopData.push(myobject);
            }
            var series = [];
            TopData.forEach(function (item, i) {
                series.push({
                    name: item[0],
                    type: 'lines',
                    zlevel: 1,
                    effect: {
                        show: true,
                        period: 6,
                        trailLength: 0.7,
                        color: '#fff',
                        symbolSize: 3
                    },
                    lineStyle: {
                        normal: {
                            color: color[i],
                            width: 0,
                            curveness: 0.2
                        }
                    },
                    data: $.convertDataFlow(item[1])
                },
                    {
                        name: item[0],
                        type: 'lines',
                        zlevel: 2,
                        effect: {
                            show: true,
                            period: 6,
                            trailLength: 0,
                            symbol: planePath,
                            symbolSize: 10
                        },
                        lineStyle: {
                            normal: {
                                color: color[i],
                                width: 1,
                                opacity: 0.4,
                                curveness: 0.2
                            }
                        },
                        data: $.convertDataFlow(item[1])
                    },
                    {
                        name: item[0],
                        type: 'effectScatter',
                        coordinateSystem: 'geo',
                        zlevel: 2,
                        rippleEffect: {
                            brushType: 'stroke'
                        },
                        label: {
                            normal: {
                                show: true,
                                position: 'right',
                                formatter: '{b}'
                            }
                        },
                        symbolSize: function (val) {
                            return val[2] / 2000;
                        },
                        itemStyle: {
                            normal: {
                                color: color[i]
                            }
                        },
                        data: item[1].map(function (dataItem) {
                            var myname = dataItem[1].name;
                            var myvalue = dataItem[1].value;


                            return {
                                name: myname,
                                value: $.geoCoordMap[myname].concat([myvalue])
                            };
                        })
                    });
            });



            option = {
                //   backgroundColor: '#404a59',
                title: {

                    left: 'center',
                    textStyle: {
                        color: '#fff'
                    }
                },
                tooltip: {
                    trigger: 'item'
                },

                geo: {
                    map: 'china',
                    label: {
                        emphasis: {
                            show: false
                        }
                    },
                    roam: true,
                    itemStyle: {
                        normal: {
                            areaColor: '#353761',
                            borderColor: '#a38a8a'
                        },
                        emphasis: {
                            areaColor: '#2a333d'
                        }
                    }
                },
                series: series
            };

            myChart.setOption(option);
        });
    }

    function DrawMap(drawdata, echartElement, mapPath, mapName) {
        var myChart = echarts.init(echartElement);
        $.extend({ mapChart: myChart });
        var option = {

            title: {
                text: 'CJ-PMS智能地图',
                subtext: '中国',
                sublink: '',
                x: 'center',
                textStyle: {
                    color: '#fff'
                }
            },
            tooltip: {
                trigger: 'item',
                formatter: function (params) {
                    return params.name + ' : ' + params.value[2];
                }
            },
            legend: {
                orient: 'vertical',
                y: 'bottom',
                x: 'right',

                textStyle: {
                    color: '#fff'
                }
            },
            visualMap: {
                show: false,
                min: 0,
                max: 2500,
                left: 'left',
                top: 'bottom',
                text: ['高', '低'], // 文本，默认为数值文本
                calculable: true,
                seriesIndex: [1],
                textStyle: {
                    color: '#fff'
                }
            },

            geo: {
                // 地理坐标系组件。
                //地理坐标系组件用于地图的绘制，支持在地理坐标系上绘制散点图，线集。
                //是否显示地理坐标系组件。
                show: true,
                /*
                 * ECharts 中提供了两种格式的地图数据，
                 * 一种是可以直接 script 标签引入的 js 文件，
                 * 引入后会自动注册地图名字和数据。
                 * 还有一种是 JSON 文件，需要通过 AJAX 异步加载后手动注册。
                 */
                map: "china",

                /*
                 * 图形上的文本标签，可用于说明图形的一些数据信息，比如值，名称等，
                 * label选项在 ECharts 2.x 中放置于itemStyle下，
                 * 在 ECharts 3 中为了让整个配置项结构更扁平合理，
                 * label 被拿出来跟 itemStyle 平级，
                 * 并且跟 itemStyle 一样拥有 emphasis 状态。
                 */
                label: {
                    normal: {
                        show: true,
                        color: '#5bd4fa',
                    },
                    emphasis: {
                        color: 'red',
                    }
                },
                /*
                 * 地图区域的多边形 图形样式。
                 */
                itemStyle: {
                    normal: {
                        //地图区域的颜色。
                        areaColor: 'rgba(85, 213, 252, 0.4)',
                        //图形的描边颜色。支持的颜色格式同 color，不支持回调函数
                        borderColor: '#5bd4fa',
                        //描边线宽。为 0 时无描边。
                        borderWidth: 1,
                        //图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果。
                        shadowBlur: {
                            shadowColor: 'rgba(121, 194, 218, 0.5)',//阴影颜色。支持的格式同color。
                            shadowBlur: 30,//图形阴影的模糊大小。
                            shadowOffsetY: 20,//阴影垂直方向上的偏移距离。
                            shadowOffsetX: 20,//阴影水平方向上的偏移距离。
                            opacity: 0.6
                        }
                    },
                    /*
                     * 高亮状态下的多边形和标签样式。
                     */
                    emphasis: {
                        //地图区域的多边形 图形样式。
                        itemStyle: {
                            areaColor: '#2B91B7'
                        }

                    }
                },
                layoutCenter: ['50%', '50%'],
                layoutSize: 900,
                roam: true

            },
            series: [
                {
                    name: '',
                    type: 'scatter',
                    coordinateSystem: 'geo',
                    symbol: 'pin',
                    symbolSize: function (val) {
                        var max = 30000;
                        var min = 900; // todo
                        var maxSize4Pin = 100;
                        var minSize4Pin = 0;
                        var a = (maxSize4Pin - minSize4Pin) / (max - min);
                        var b = minSize4Pin - a * min;
                        b = maxSize4Pin - a * max;
                        return a * val[2] + b;
                    },
                    data: $.convertData(drawdata),

                    symbolSize: 12,
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
                    },
                    itemStyle: {
                        emphasis: {
                            borderColor: '#fff',
                            borderWidth: 1
                        }
                    }
                }
            ]
        };
        $.get(mapPath, function (getJSON) {
            echarts.registerMap(mapName, getJSON);
            myChart.setOption(option, true);

        });


    }

    function saveCSV(csv, saveName) {
        var blob = new Blob(['\ufeff' + csv], { type: 'text/csv,charset=UTF-8' });
        openDownloadDialog(blob, saveName);
    }

    function openDownloadDialog(url, saveName) {
        if (typeof url == 'object' && url instanceof Blob) {
            url = URL.createObjectURL(url); // 创建blob地址
        }
        var aLink = document.createElement('a');
        aLink.href = url;
        aLink.download = saveName || ''; // HTML5新增的属性，指定保存文件名，可以不要后缀，注意，file:///模式下不会生效
        var event;
        if (window.MouseEvent) event = new MouseEvent('click');
        else {
            event = document.createEvent('MouseEvents');
            event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        }
        aLink.dispatchEvent(event);
    }
}
function jumpThrough(pps, name, url, self, cellValue, reportId, queryJson, moduleId, formId, pKey, pValue) {
    parent.$(".mk-second-menu-list").hide();
    var $menu = parent.$("#projectmenu");
    $menu.find(".mk-menu-item").each(function (index, element) {
        var $this = $(element);
        $this.attr(pKey, pValue);
    });
    $menu.show();
    //var url = "/ProjectModule/Project/Main?projectId=" + id + "&name=" + name;
    if (url.indexOf("?") > -1)
        url += "&";
    else
        url += "?";
    if (queryJson)
        queryJson = "";
    url += "queryJson=" + queryJson;
    if (pps) {
        var jpps = JSON.parse(pps);
        for (var i = 0; i < jpps.length; i++) {
            for (var key in jpps[i]) {
                url += "&" + key + "=" + jpps[i][key];
            }
        }
    }
    if (reportId)
        url += "&reportId=" + reportId;
    url += "&" + $(self).parent().attr("colname") + "=" + cellValue;
    if (pKey && pValue)
        url += "&" + pKey + "=" + pValue;
    if (formId)
        url += "&formId=" + formId;

    //top.Changjie.frameTab.closeallprojecttab();
    top.Changjie.frameTab.open({
        F_ModuleId: moduleId,
        F_Icon: 'fa fa-file-text-o',
        F_FullName: name,
        F_UrlAddress: url,
        IsProject: true
    });
}