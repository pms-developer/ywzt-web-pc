﻿(function ($, b) {
    $.fn.mkLayout = function (h) {
        var mklayoutleft = {
            blocks: [{
                target: ".mk-layout-left",
                type: "right",
                size: 203
            }]
        };
        $.extend(mklayoutleft, h || {});
        var c = $(this);
        if (c.length <= 0) {
            return false
        }
        c[0]._mkLayout = {
            dfop: mklayoutleft
        };
        mklayoutleft.id = "mklayout" + new Date().getTime();
        for (var f = 0,
            g = mklayoutleft.blocks.length; f < g; f++) {
            var d = mklayoutleft.blocks[f];
            c.children(d.target).append('<div class="mk-layout-move mk-layout-move-' + d.type + ' " path="' + f + '"  ></div>')
        }
        c.on("mousedown",
            function (m) {
                var n = m.target || m.srcElement;
                var i = $(n);
                var j = $(this);
                var l = j[0]._mkLayout.dfop;
                if (i.hasClass("mk-layout-move")) {
                    var k = i.attr("path");
                    l._currentBlock = l.blocks[k];
                    l._ismove = true;
                    l._pageX = m.pageX;
                }
            });
        c.mousemove(function (l) {
            var j = $(this);
            var k = j[0]._mkLayout.dfop;
            if (!!k._ismove) {
                var i = j.children(k._currentBlock.target);
                i.css("width", k._currentBlock.size + (l.pageX - k._pageX));
                j.css("padding-left", k._currentBlock.size + (l.pageX - k._pageX));
            }
        });
        c.on("click",
            function(k) {
                var i = $(this);
                var j = i[0]._mkLayout.dfop;
                if (!!j._ismove) {
                    j._currentBlock.size += (k.pageX - j._pageX);
                    j._ismove = false;
                }
            });
    }
})(jQuery, top.Changjie); 
