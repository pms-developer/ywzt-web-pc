﻿/*
 * 日 期：2017.03.16
 * 描 述：操作类	
 */

top.Changjie = (function ($) {
    "use strict";
    var changjie = {
        myProjectData: [],
        // 是否是调试模式
        isDebug: true,
        sysGlobalSettings: {
            pointGlobalEnabled: false,
            pointGlobal: 4,
            pointGlobal2: 2,
        },
        nofind: function () {
            var img = event.srcElement;
            img.src = "/content/usermoren.png";
            img.onerror = null;
        },
        tranAuditStatus(auditstatus) {
            switch (auditstatus) {
                case '0':
                    return '<span class="label label-default" style="cursor: pointer;">未审核</span>';
                case '1':
                    return '<span class="label label-info" style="cursor: pointer;">审核中</span>';
                case '2':
                    return '<span class="label label-success" style="cursor: pointer;">已通过</span>';
                case '3':
                case '4':
                case '5':
                    return '<span class="label label-warning" style="cursor: pointer;">未通过</span>';
                default:
                    return '<span class="label label-default" style="cursor: pointer;">未审核</span>';
            }
        },
        tranAuditStatusUseWorkFlow(auditstatus) {
            switch (auditstatus) {
                case 0:
                    return '<span class="label label-default" style="cursor: pointer;">未审核</span>';
                case 1:
                    return '<span class="label label-info" style="cursor: pointer;">审核中</span>';
                case 2:
                    return '<span class="label label-success" style="cursor: pointer;">已通过</span>';
                case 3:
                case 4:
                case 5:
                    return '<span class="label label-warning" style="cursor: pointer;">未通过</span>';
                default:
                    return '<span class="label label-default" style="cursor: pointer;">未审核</span>';
            }
        },
        log: function () {
            if (Changjie.isDebug) {
                console.log('=====>' + new Date().getTime() + '<=====');
                var len = arguments.length;
                for (var i = 0; i < len; i++) {
                    console.log(arguments[i]);
                }
            }
        },
        // 创建一个GUID
        newGuid: function () {
            var guid = "";
            for (var i = 1; i <= 32; i++) {
                var n = Math.floor(Math.random() * 16.0).toString(16);
                guid += n;
                if ((i == 8) || (i == 12) || (i == 16) || (i == 20)) guid += "-";
            }
            return guid;
        },
        // 加载提示
        loading: function (isShow, _text) {//加载动画显示与否
            var $loading = top.$("#loading_bar");
            if (!!_text) {
                //top.Changjie.language.get(_text, function (text) {
                //    top.$("#loading_bar_message").html(text);
                //});

            } else {
                //top.Changjie.language.get("正在拼了命为您加载…", function (text) {
                //    top.$("#loading_bar_message").html(text);
                //});
                //多语言翻译
                _text = "正在拼了命为您加载…";
            }
            //多语言翻译
            data.forEach(function (itemData) {
                if (_text === itemData.key) {
                    _text = itemData.value;
                }
            });
            top.$("#loading_bar_message").html(_text);

            if (isShow) {
                $loading.show();
            } else {
                $loading.hide();
            }
        },
        // 动态加载css文件
        loadStyles: function (url) {
            var link = document.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.href = url;
            link.back = "backdl";
            document.getElementsByTagName("head")[0].appendChild(link);
        },
        // 获取iframe窗口
        iframe: function (Id, _frames) {
            if (_frames[Id] != undefined) {
                if (_frames[Id].contentWindow != undefined) {
                    return _frames[Id].contentWindow;
                }
                else {
                    return _frames[Id];
                }
            }
            else {
                return null;
            }
        },
        // 改变url参数值
        changeUrlParam: function (url, key, value) {
            var newUrl = "";
            var reg = new RegExp("(^|)" + key + "=([^&]*)(|$)");
            var tmp = key + "=" + value;
            if (url.match(reg) != null) {
                newUrl = url.replace(eval(reg), tmp);
            } else {
                if (url.match("[\?]")) {
                    newUrl = url + "&" + tmp;
                }
                else {
                    newUrl = url + "?" + tmp;
                }
            }
            return newUrl;
        },
        // 转化成十进制
        toDecimal: function (num, decimals) {
            if (!num) {
                num = "0";
            }
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            var sign = (num == (num = Math.abs(num)));
            var numlen = Math.floor(num * (decimals || 10000) + 0.50000000001).toString().length;
            var zeroLen = 0;
            num = Math.floor(num * (decimals || 10000) + 0.50000000001);
            var cents = num % (decimals || 10000);
            num = Math.floor(num / (decimals || 10000)).toString();

            if (num < decimals) {
                //num原值小于1
                zeroLen = decimals.toString().length - cents.toString().length;
                for (var i = 1; i < zeroLen; i++) {
                    cents = "0" + cents;
                }
            }
            else if (cents != 0 && num.toString().length + cents.toString().length < numlen) {
                zeroLen = numlen - num.toString().length - cents.toString().length;
                for (var i = 0; i < zeroLen; i++) {
                    cents = "0" + cents;
                }
            }
            //if (cents < 10)
            //    cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + '' +
                    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        },
        // 文件大小转换
        countFileSize: function (size) {
            if (size < 1024.00)
                return Changjie.toDecimal(size) + " 字节";
            else if (size >= 1024.00 && size < 1048576)
                return Changjie.toDecimal(size / 1024.00) + " KB";
            else if (size >= 1048576 && size < 1073741824)
                return Changjie.toDecimal(size / 1024.00 / 1024.00) + " MB";
            else if (size >= 1073741824)
                return Changjie.toDecimal(size / 1024.00 / 1024.00 / 1024.00) + " GB";
        },
        // 数组复制
        arrayCopy: function (data) {
            return $.map(data, function (obj) {
                return $.extend(true, {}, obj);
            });
        },
        // 检测数据是否选中
        checkrow: function (id) {
            var isOK = true;
            if (id == undefined || id == "" || id == 'null' || id == 'undefined') {
                isOK = false;
                //top.Changjie.language.get('您没有选中任何数据项,请选中后再操作！', function (text) {
                //    Changjie.alert.warning(text);
                //});
                var text = '您没有选中任何数据项,请选中后再操作！'
                data.forEach(function (itemData) {
                    if (text === itemData.key) {
                        text = itemData.value;
                    }
                });
                Changjie.alert.warning(text);
            }
            return isOK;
        },
        // 提示消息栏
        alert: {
            success: function (msg) {
                //top.Changjie.language.get(msg, function (text) {
                //    toastr.success(text);
                //});
                //多语言翻译
                data.forEach(function (itemData) {
                    if (msg === itemData.key) {
                        msg = itemData.value;
                    }
                });
                toastr.success(msg);
            },
            info: function (msg) {
                //top.Changjie.language.get(msg, function (text) {
                //    toastr.info(text);
                //});
                //多语言翻译
                data.forEach(function (itemData) {
                    if (msg === itemData.key) {
                        msg = itemData.value;
                    }
                });
                toastr.info(msg);
            },
            warning: function (msg) {
                //top.Changjie.language.get(msg, function (text) {
                //    toastr.warning(text);
                //});
                //多语言翻译
                data.forEach(function (itemData) {
                    if (msg === itemData.key) {
                        msg = itemData.value;
                    }
                });
                toastr.warning(msg);
            },
            error: function (msg) {
                //top.Changjie.language.get(msg, function (text) {
                //    toastr.warning(msg);
                //});
                //多语言翻译
                data.forEach(function (itemData) {
                    if (msg === itemData.key) {
                        msg = itemData.value;
                    }
                });
                toastr.warning(msg);
            }
        },
        //下载文件（she写的扩展）
        download: function (options) {
            var defaults = {
                method: "GET",
                url: "",
                param: []
            };
            var options = $.extend(defaults, options);
            if (options.url && options.param) {

                var $form = $('<form target="_blank" action="' + options.url + '" method="' + (options.method || 'post') + '"></form>');
                for (var key in options.param) {
                    var $input = $('<input type="hidden" />').attr('name', key).val(options.param[key]);
                    $form.append($input);
                }
                $form.appendTo('body').submit().remove();
            };
        },
        // 数字格式转换成千分位
        commafy: function (num) {
            if (num == null) {
                num = "0";
            }
            if (isNaN(num)) {
                return "0";
            }
            num = num + "";
            if (/^.*\..*$/.test(num)) {
                varpointIndex = num.lastIndexOf(".");
                varintPart = num.substring(0, pointIndex);
                varpointPart = num.substring(pointIndex + 1, num.length);
                intPart = intPart + "";
                var re = /(-?\d+)(\d{3})/
                while (re.test(intPart)) {
                    intPart = intPart.replace(re, "$1,$2")
                }
                num = intPart + "." + pointPart;
            } else {
                num = num + "";
                var re = /(-?\d+)(\d{3})/
                while (re.test(num)) {
                    num = num.replace(re, "$1,$2")
                }
            }
            return num;
        },
        //是否整数
        isInt: function (obj) {
            if (!!obj) {
                var num = obj.toString().replace(/\$|\,/g, '');
                num = Number(num);
                return (num | 0) === num
            }
            else {
                return false;
            }
        },

        isFloat: function (obj) {
            if (!!obj) {
                var num = obj.toString().replace(/\$|\,/g, '');
                var rge = /^(-?\d+)(\.\d+)?$/;
                var result = rge.test(num);
                return result;
            }
            else {
                return false;
            }
        },
        // 检测图片是否存在
        isExistImg: function (pathImg) {
            if (!!pathImg) {
                var ImgObj = new Image();
                ImgObj.src = pathImg;
                if (ImgObj.fileSize > 0 || (ImgObj.width > 0 && ImgObj.height > 0)) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                return false;
            }
        },
        numberFormat: function (number, decimals, decPoint, thousandsSep) {
            /*
    * 参数说明：
    * number：要格式化的数字
    * decimals：保留几位小数
    * dec_point：小数点符号
    * thousands_sep：千分位符号
    * */
            number = (number + '').replace(/\$|\,/g, '');
            number = (number + '').replace(/[^0-9+-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep,
                dec = (typeof decPoint === 'undefined') ? '.' : decPoint,
                s = '',
                toFixedFix = function (n, prec) {
                    //var k = Math.pow(10, prec);
                    //return '' + Math.ceil(n * k) / k;
                    return n.toFixed(prec);
                };

            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            var re = /(-?\d+)(\d{3})/;
            while (re.test(s[0])) {
                s[0] = s[0].replace(re, "$1" + sep + "$2");
            }

            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        },

        setRowEditState: function (data, row, colname, oldValue) {
            if (row.rowState == undefined) {
                row.rowState = 0;
                row.EditType = 0;
            }
            if (row.rowState == 0)//0编辑 1新增 -1删除
            {
                var newValue = row[colname];
                if (newValue == oldValue) {
                    row[colname + "_Cell_Edit_State"] = 0;//未编辑
                    row.EditType = 0;
                }
                else {
                    row[colname + "_Cell_Edit_State"] = 1;//已编辑
                    row.EditType = 2;
                }
                var state = 0;
                for (var i = 0; i < data.length; i++) {
                    var value = 0;
                    var colValue = row[data[i].data.name + "_Cell_Edit_State"];
                    if (!!colValue) {
                        value = parseInt(colValue);
                    }
                    state += value;
                }
                row.isEdit = state > 0 ? true : false;
            }
        },
        attachmentSettings: function () {
            var settings = {
                "tabname": "附件",
                "buttons": [{
                    "id": "btn_delfile",
                    "name": "删除",
                    "style": "",
                    "class": "btn btn-danger mkUploader-input-btn"
                }, {
                    "id": "btn_uploadfile",
                    "name": "上传",
                    "style": "",
                    "class": "btn btn-success mkUploader-input-btn"
                }
                ],
                "isEdit": false,
                "tablename": "Base_AnnexesFile",
                "fields": [{
                    "id": "",
                    "name": "文件名",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "F_FileName",
                    "dataSource": "0"
                }, {
                    "id": "",
                    "name": "创建者",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "CreationName",
                    "dataSource": "0"
                }, {
                    "id": "",
                    "name": "上传日期 ",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "CreationDate",
                    "dataSource": "0"
                }, {
                    "id": "",
                    "name": "文件大小 ",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "F_FileSize",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "文件类型 ",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "F_FileType",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "下载次数 ",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "F_DownloadCount",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "操作 ",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "operationTabs",
                    "dataSource": "0"
                }, {
                    "id": "",
                    "name": "主键",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "guid",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "F_Id",
                    "dataSource": "0"
                }]
            }
            return settings;
        },

        workflowSettings: function () {
            var settings = {
                "tabname": "流程",
                "isEdit": false,
                "tablename": "SysAttachment11",
                "fields": [{
                    "id": "",
                    "name": "节点名称",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 200,
                    "align": "left",
                    "parentId": "",
                    "field": "123",
                    "dataSource": "0"
                }, {
                    "id": "",
                    "name": "类型",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "456",
                    "dataSource": "0"
                }, {
                    "id": "",
                    "name": "节点状态",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "789",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "审批人",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "987",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "办理人",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "876",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "接收时间",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "654",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "办理时间",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "543",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "办理意见",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "432",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "附件",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "label",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "321",
                    "dataSource": "0"
                },
                {
                    "id": "",
                    "name": "主键",
                    "aggtype": "normal",
                    "aggcolumn": "",
                    "type": "guid",
                    "width": 100,
                    "align": "left",
                    "parentId": "",
                    "field": "ID",
                    "dataSource": "0"
                }]
            }
            return settings;
        },
        //去除数字千号
        clearNumSeparator: function (num, type) {
            if (!!num == false) return 0;
            if (num.toString() == "0") return num;
            if (!!type) {
                var result = num;
                switch (type) {
                    case "int":
                        num = (num + '').replace(/\$|\,/g, '');
                        return parseInt(num);
                    case "float":
                        result = (result + '').replace(/\$|\,/g, '');
                        break;
                }
                return result;
            }
            else {
                if (!!num) {
                    var result = parseFloat((num + '').replace(/[^\d\.-]/g, ""));
                    if (result == NaN) return num;
                    return result;
                }
            }
        },
        getFloatValue: function (value) {
            var val = value || 0;
            return parseFloat(this.clearNumSeparator(val), "float");
        },
        delcommafy(num) {//去除千分位中的','
            if (num && num != 'undefined' && num != 'null') {
                let numS = num;
                numS = numS.toString();
                numS = numS.replace(/,/gi, '');
                return numS;
            } else {
                return num;
            }
        },
        setProjectInfo: function (formname, projectId) {
            var $form = $("#" + formname);
            var dttable = $form.attr("data-table");
            var namehtml = "<div class='col-xs-12 mk-form-item' style='display:none'  data-table='" + dttable + "' >" +
                +"<div class='mk-form-item-title' >项目名称</div >"
                + " <input readonly id='ProjectName' type='text' class='form-control'  />"
                + "</div >";
            $form.append(namehtml);

            var codehtml = "<div class='col-xs-12 mk-form-item' style='display:none'  data-table='" + dttable + "' >" +
                +"<div class='mk-form-item-title' >项目ID</div >"
                + " <input readonly id='Project_ID' type='text' value=\"" + projectId + "\" class='form-control'  />"
                + "</div >";
            $form.append(codehtml);

            var para = {
                keyValue: projectId
            };
            top.Changjie.httpAsync("GET", top.$.rootUrl + '/SystemModule/Project/GetEntity', para, function (data) {
                if (!!data) {
                    $("#ProjectName").val(data.ProjectName);
                }
            });
        },

        //公式取值
        mathExpress: function (codeList, item, rowItem, rowData) {
            if (!!!item) return;
            if (!!!item.formula) return;
            var str = item.formula.replace(/\s+/g, "");
            if (!!str) {
                var rows = rowItem;
                while (str.indexOf("[") > -1) {
                    for (var _index in codeList) {
                        var colvalue;
                        if (!!rowData[codeList[_index]]) {
                            colvalue = rowData[codeList[_index]];

                        } else {
                            colvalue = rows[codeList[_index]].value;
                        }
                        var code = "[" + codeList[_index] + "]";
                        if (str.indexOf(code) > -1) {
                            var value = this.clearNumSeparator(colvalue || 0);
                            str = str.replace(code, value);
                        }
                    }
                }
                var result = this.toDecimal(eval(str));
                return result;
            }
        },

        //加减乘除简单数学计算
        simpleMath: function (num1, num2, type, points) {
            var reg = /^((\d+\.?\d*)|(\d*\.\d+))\%$/;
            var n1 = 0;
            var decimals = 10000;
            if (!!points) {
                points = parseInt(points);
                switch (points) {
                    case 0:
                        decimals = 0;
                        break;
                    case 1:
                        decimals = 10;
                        break;
                    case 2:
                        decimals = 100;
                        break;
                    case 3:
                        decimals = 1000;
                        break;
                    case 4:
                        decimals = 10000;
                        break;
                    case 5:
                        decimals = 100000;
                        break;
                }
            }
            if (reg.test(num1)) {

                n1 = this.toDecimal(num1.replace("%", "")) / 100;
            }
            else {
                n1 = this.toDecimal(num1, decimals);
            }

            var n2 = 0;
            if (reg.test(num2)) {
                n2 = this.toDecimal(num2.replace("%", "")) / 100;
            }
            else {
                n2 = this.toDecimal(num2, decimals);
            }
            var result = 0;
            switch (type) {
                case "add":
                    result = parseFloat(n1) + parseFloat(n2);
                    break;
                case "sub":
                    result = parseFloat(n1) - parseFloat(n2);
                    break;
                case "mul":
                    result = n1 * n2;
                    break;
                case "div":
                    if (n2 != 0) {
                        result = n1 / n2;
                    }
                    break;
            }

            result = this.format45(result, decimals);
            if (!!points) {
                return this.numberFormat(result, points);
            }
            if (this.sysGlobalSettings.pointGlobalEnabled) {
                return this.numberFormat(result, this.sysGlobalSettings.pointGlobal);
            }

            return this.numberFormat(result, this.sysGlobalSettings.pointGlobal);

        },
        format45: function (value, param) {
            if (isNaN(value) || value == undefined || value == null) { return null; }
            switch (param) {
                case 0:
                    param = 1;
                    break;
                case 1:
                    param = 10;
                    break;
                case 2:
                    param = 100;
                    break;
                case 3:
                    param = 1000;
                    break;
                case 4:
                    param = 10000;
                    break;
                case 5:
                    param = 100000;
                    break;
            }

            return Math.round(value * param) / param;
        },
        formatWan: function (value) {
            var numvalue = parseFloat(value);
            if (!!numvalue) {//数字
                if (numvalue > 10000) {
                    var v = this.format45(eval(numvalue / 10000), 0);
                    v = this.numberFormat(v);
                    return v+"万";
                }
            }
            return value;
        },
        gridInlineSimpleMath: function (headData, row, index) {
            if (!!headData.inlineOperation) {
                var info = headData.inlineOperation;
                var v1 = 0;
                var v2 = 0;
                var result = 0;
                if (info.isFirstTarget) {
                    v1 = row[info.target1];
                    v2 = row[info.target2];
                }
                else {
                    var v1 = row[info.target2];
                    var v2 = row[info.target1];
                }
                result = this.simpleMath(v1, v2, info.type);
                row[info.toTarget] = result;
                $("div[colname='" + info.toTarget + "'][rowindex='" + index + "']").html(result);
            }
        },
        formatSql: function (sql,projectId) {
            while (sql.indexOf("@projectId@") > -1) {
                sql = sql.replace("@projectId@", "'" + projectId + "'");
            }
            return sql;
        },
        filterSysParams: function (sql) {
            var value = "";
            if (sql.indexOf("@projectId@") >= 0) {
                if (sql.indexOf(" ") >= 0) {
                    var list = sql.split(' ');
                    for (var i = 0; i < list.length; i++) {
                        var item = list[i];
                        if (item.indexOf("@projectId@") == -1) {
                            value += item + " ";
                        }
                        else {
                            value += " 1=1 ";
                        }
                    }
                }
            }
            else {
                return sql;
            }
            return value;
        },
        //提取税率 格式为：增值税专用发票(13.0%) 的税率 提取结果为：13
        extractTaxTateNum1: function (value) {
            if (value) {
                if (value.indexOf("(") > -1) {
                    value = value.split("(")[1];
                    if (value.indexOf("%") > -1) {
                        value = value.split("%")[0];
                        return this.extractTaxTateNum2(value);
                    }
                }
            }
            return value;
        },
        //提取税率 格式为：13.0% 的税率 提取结果为：13
        extractTaxTateNum2: function (value) {
            value = value || 0;
            value = value.toString().replace("%", "");
            value = parseInt(value);
            if (value == NaN)
                return 0;
            return value;
        },
        getUrlParam: function (url,name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = url.split('?')[1].match(reg);
            if (r != null) return unescape(decodeURI(r[2])); return "";
        },
    };

    return changjie;
})(window.jQuery);