﻿/*
 * 日 期：2017.03.16
 * 描 述：表单处理方法
 */
(function ($, Changjie) {
    "use strict";

    var workflowId = "";
    /*获取和设置表单数据*/
    $.fn.mkGetFormData = function (keyValue) {// 获取表单数据
        var resdata = {};
        $(this).find('input,select,textarea,.mk-select,.mk-formselect,.mkUploader-wrap,.mk-radio,.mk-checkbox,.edui-default').each(function (r) {
            var id = $(this).attr('id');
            if (!!id) {
                var type = $(this).attr('type');
                switch (type) {
                    case "radio":
                        if ($("#" + id).is(":checked")) {
                            var _name = $("#" + id).attr('name');
                            resdata[_name] = $("#" + id).val();
                        }
                        break;
                    case "checkbox":
                        if ($("#" + id).is(":checked")) {
                            resdata[id] = 1;
                        } else {
                            resdata[id] = 0;
                        }
                        break;
                    case "mkselect":
                        resdata[id] = $(this).mkselectGet();
                        break;
                    case "formselect":
                        resdata[id] = $(this).mkformselectGet();
                        break;
                    case "mkGirdSelect":
                        resdata[id] = $(this).mkGirdSelectGet();
                        break;
                    case "mk-Uploader":
                        resdata[id] = $(this).mkUploaderGet();
                        break;
                    case "mk-radio":
                        resdata[id] = $(this).find('input:checked').val();
                        break;
                    case "mk-checkbox":
                        var _idlist = [];
                        $(this).find('input:checked').each(function () {
                            _idlist.push($(this).val());
                        });
                        resdata[id] = String(_idlist);
                        break;
                    default:
                        if ($("#" + id).hasClass('currentInfo')) {
                            var value = $("#" + id)[0].mkvalue;
                            resdata[id] = $.trim(value);
                        }
                        else if ($(this).hasClass('edui-default')) {
                            if ($(this)[0].ue) {
                                resdata[id] = $(this)[0].ue.getContent(null, null, true);
                            }
                        }
                        else {

                            var value = $("#" + id).val();
                            resdata[id] = $.trim(value);
                        }

                        break;
                }
                resdata[id] += '';
                if (resdata[id] == '') {
                    resdata[id] = '&nbsp;';
                }
                if (resdata[id] == '&nbsp;' && !keyValue) {
                    resdata[id] = '';
                }
            }
        });
        return resdata;
    };
    $.fn.mkSetFormData = function (data) {// 设置表单数据
        var $this = $(this);
        for (var id in data) {
            var value = data[id];
            if (id == "Workflow_ID") {
                workflowId = value;
            }
            var $obj = $this.find('#' + id);
            if ($obj.length == 0 && value != null) {
                $obj = $this.find('[name="' + id + '"][value="' + value + '"]');
                if ($obj.length > 0) {
                    if (!$obj.is(":checked")) {
                        $obj.trigger('click');
                    }
                }
            }
            else {
                var type = $obj.attr('type');
                if ($obj.hasClass("mk-input-wdatepicker")) {
                    type = "datepicker";
                }
                switch (type) {
                    case "checkbox":
                        var isck = 0;
                        if ($obj.is(":checked")) {
                            isck = 1;
                        } else {
                            isck = 0;
                        }
                        if (isck != parseInt(value)) {
                            $obj.trigger('click');
                        }
                        break;
                    case "mkselect":
                        $obj.mkselectSet(value);
                        break;
                    case "formselect":
                        $obj.mkformselectSet(value);
                        break;
                    case "mkGirdSelect":
                        $obj.mkGirdSelectSet(value);
                        break;
                    case "datepicker":
                        $obj.val(Changjie.formatDate(value, 'yyyy-MM-dd'));
                        break;
                    case "mk-Uploader":
                        $obj.mkUploaderSet(value);
                        break;
                    case "mk-radio":
                        if (!$obj.find('input[value="' + value + '"]').is(":checked")) {
                            $obj.find('input[value="' + value + '"]').trigger('click');
                        }
                        break;
                    default:
                        if ($obj.hasClass('currentInfo')) {
                            $obj[0].mkvalue = value;
                            if ($obj.hasClass('currentInfo-user')) {
                                $obj.val('');
                                Changjie.clientdata.getAsync('user', {
                                    key: value,
                                    callback: function (item, op) {
                                        op.obj.val(item.name);
                                    },
                                    obj: $obj
                                });
                            }
                            else if ($obj.hasClass('currentInfo-company')) {
                                $obj.val('');
                                Changjie.clientdata.getAsync('company', {
                                    key: value,
                                    callback: function (_data, op) {
                                        op.obj.val(_data.name);
                                    },
                                    obj: $obj
                                });
                            }
                            else if ($obj.hasClass('currentInfo-department')) {
                                $obj.val('');
                                Changjie.clientdata.getAsync('department', {
                                    key: value,
                                    callback: function (_data, op) {
                                        op.obj.val(_data.name);
                                    },
                                    obj: $obj
                                });
                            }
                            else {
                                $obj.val(value);
                            }

                        }
                        else if ($obj.hasClass('edui-default')) {
                            var ue = $obj[0].ue;
                            setUe(ue, value);
                        }
                        else {
                            $obj.val(value);
                        }


                        break;
                }
            }
        }
    };

    function setUe(ue, value) {
        ue.ready(function () {
            ue.setContent(value);
        });
    }

    $.fn.getWorkflowValue = function () {
        return workflowId;
    };

    /*表单数据操作*/
    $.mkSetForm = function (url, callback) {
        Changjie.loading(true, '正在获取数据');
        Changjie.httpAsyncGet(url, function (res) {
            Changjie.loading(false);
            if (res.code == Changjie.httpCode.success) {
                callback(res.data);
            }
            else {
                //Changjie.layerClose(window.name);
                Changjie.alert.error('表单数据获取失败,请重新获取！');
                Changjie.httpErrorLog(res.info);
            }
        });
    };
    $.mkSaveForm = function (url, param, callback, isNotClosed) {
        param['__RequestVerificationToken'] = $.mkToken;
        Changjie.loading(true, '正在保存数据');
        Changjie.httpAsyncPost(url, param, function (res) {
            Changjie.loading(false);
            if (res.code == Changjie.httpCode.success) {
                if (!!callback) {
                    callback(res);
                }
                Changjie.alert.success(res.info);
                if (!isNotClosed) {
                    Changjie.layerClose(window.name);
                }
            }
            else {
                Changjie.alert.error(res.info);
                Changjie.httpErrorLog(res.info);
            }
        });
    };
    $.mkPostForm = function (url, param) {
        param['__RequestVerificationToken'] = $.mkToken;
        Changjie.loading(true, '正在提交数据');
        Changjie.httpAsyncPost(url, param, function (res) {
            Changjie.loading(false);
            if (res.code == Changjie.httpCode.success) {
                Changjie.alert.success(res.info);
            }
            else {
                Changjie.alert.error(res.info);
                Changjie.httpErrorLog(res.info);
            }
        });
    };

    /*tab页切换*/
    $.fn.mkFormTab = function (paddingtop) {
        var $this = $(this);
        if (!!paddingtop) {
            $this.parent().css({ 'padding-top': '' + paddingtop + 'px' });
        }
        else {
            $this.parent().css({ 'padding-top': '44px' });
        }
        $this.mkscroll();

        $this.on('DOMNodeInserted', function (e) {
            var $this = $(this);
            var w = 0;
            $this.find('li').each(function () {
                w += $(this).outerWidth();
            });
            $this.find('.mk-scroll-box').css({ 'width': w });
        });

        $this.delegate('li', 'click', { $ul: $this }, function (e) {
            var $li = $(this);
            if (!$li.hasClass('active')) {
                var $parent = $li.parent();
                var $content = e.data.$ul.next();

                var id = $li.find('a').attr('data-value');
                $parent.find('li.active').removeClass('active');
                $li.addClass('active');
                $content.find('.tab-pane.active').removeClass('active');
                $content.find('#' + id).addClass('active');
            }
        });
    }
    $.fn.mkFormTabEx = function (callback) {
        var $this = $(this);
        $this.delegate('li', 'click', { $ul: $this }, function (e) {
            var $li = $(this);
            if (!$li.hasClass('active')) {
                var $parent = $li.parent();
                var $content = e.data.$ul.next();

                var id = $li.find('a').attr('data-value');
                $parent.find('li.active').removeClass('active');
                $li.addClass('active');
                $content.find('.tab-pane.active').removeClass('active');
                $content.find('#' + id).addClass('active');

                if (!!callback) {
                    callback(id);
                }
            }
        });
    }

    /*检测字段是否重复*/
    $.mkExistField = function (keyValue, controlId, url, param) {
        var $control = $("#" + controlId);
        if (!$control.val()) {
            return false;
        }
        var data = {
            keyValue: keyValue
        };
        data[controlId] = $control.val();
        $.extend(data, param);
        Changjie.httpAsync('GET', url, data, function (data) {
            if (data == false) {
                $.mkValidformMessage($control, '已存在,请重新输入');
            }
        });
    };

    /*固定下拉框的一些封装：数据字典，组织机构，省市区级联*/
    // 数据字典下拉框
    $.fn.mkDataItemSelect = function (op) { 
        // op:code 码,parentId 父级id,maxHeight 200,allowSearch， childId 级联下级框id
        var dfop = {
            // 是否允许搜索
            allowSearch: false,
            // 访问数据接口地址
            //url: top.$.rootUrl + '/SystemModule/DataItem/GetDetailListByParentId',
            // 访问数据接口参数
            param: { itemCode: '', parentId: '0' },
            // 级联下级框
        }
        op = op || {};
        if (!op.code) {
            return $(this);
        }
        dfop.param.itemCode = op.code;
        dfop.param.parentId = op.parentId || '0';
        dfop.allowSearch = op.allowSearch;

        var list = [];

        if (!!op.childId) {
            var list2 = [];
            $('#' + op.childId).mkselect({
                // 是否允许搜索
                allowSearch: dfop.allowSearch
            });
            dfop.select = function (item) {
                if (!item) {
                    $('#' + op.childId).mkselectRefresh({
                        data: []
                    });
                }
                else {
                    list2 = [];
                    Changjie.clientdata.getAllAsync('dataItem', {
                        code: dfop.param.itemCode,
                        callback: function (dataes) {
                            $.each(dataes, function (_index, _item) {
                                if (_item.parentId == item.k) {
                                    list2.push({ id: _item.text, text: _item.value, title: _item.text, k: _index });
                                }
                            });
                            $('#' + op.childId).mkselectRefresh({
                                data: list2
                            });
                        }
                    });
                }
            };
        }
        var $select = $(this).mkselect(dfop);
        Changjie.clientdata.getAllAsync('dataItem', {
            code: dfop.param.itemCode,
            callback: function (dataes) {
                $.each(dataes, function (_index, _item) {
                    if (_item.parentId == dfop.param.parentId) {
                        list.push({ id: _item.value, text: _item.text, title: _item.text, k: _index });
                    }
                });
                $select.mkselectRefresh({
                    data: list
                });
            }
        });
        return $select;
    };  
    // 数据源下拉框
    $.fn.mkDataSourceSelect = function (op) {
        op = op || {};
        
        var dfop = {
            // 是否允许搜索
            type: (!!op.type?op.type:'default'),
            allowSearch: true,
            select: op.select,
         }
 
        if (!op.code) {
            return $(this);
        }
        var $select = $(this).mkselect(dfop);
        var strWhere = op.strWhere || "";
        Changjie.clientdata.getAllAsync('sourceData', {
            code: op.code,
            strWhere: strWhere,
            callback: function (dataes) {
                $select.mkselectRefresh({
                    value: op.value,
                    text: op.text,
                    title: op.text,
                    data: dataes
                });
                if (!!op.default) {
                    $select.mkselectSet(op.default);
                }
            }
        });
        return $select;
    } 


    //字典分类下拉框
    $.fn.mkDataTypeSelect = function (op) {
        // op:parentId 父级id,maxHeight 200,
        var dfop = {
            value: "value",
            text: "text",
            type: 'tree',
            // 是否允许搜索
            allowSearch: true,
            // 访问数据接口地址
            url: top.$.rootUrl + '/SystemModule/DataItem/GetClassifyTree',
            // 访问数据接口参数
            param: { companyId: '', parentId: '0' },
        }
        op = op || {};
        dfop.param.itemCode = op.value;
        dfop.param.parentId = op.parentId || '0';

        return $(this).mkselect(dfop);;
    }; 

    //所有数据源下拉框
    $.fn.mkDataSourceAllSelect = function (op) { 
        // op:parentId 父级id,maxHeight 200,
        var dfop = {
            // 字段
            value: "F_Code",
            text: "F_Name",
            title: "F_Name",
            sqlText:"F_Sql",
            // 是否允许搜索
            allowSearch: true,
            // 访问数据接口地址
            url: top.$.rootUrl + '/SystemModule/DataSource/GetList',
            // 访问数据接口参数
            param: { parentId: '' },
        }
        op = op || {};
        dfop.param.itemCode = op.value;
        dfop.param.parentId = op.parentId || '0';

        return $(this).mkselect(dfop);;
    };
     
    $.fn.mkProvinceSelect = function (op) {
        op = op || {};
        var dfop = {
            allowSearch: true,
            select: op.select,
        }
        var $select = $(this).mkselect(dfop);
        Changjie.clientdata.getAllAsync('Province', {
            code: "Province",
            callback: function (dataes) {
                $select.mkselectRefresh({
                    value: op.value,
                    text: op.text,
                    title: op.text,
                    data: dataes
                });
            }
        });
        return $select;
    }

    $.fn.mkSubAreaSelect = function (op) {
        op = op || {};
        var dfop = {
            allowSearch: true,
            select: op.select,
        }
        var $select = $(this).mkselect(dfop);

        Changjie.clientdata.getAllAsync('SubArea', {
            parentId: op.parentId,
            callback: function (dataes) {
                $select.mkselectRefresh({
                    value: op.value,
                    text: op.text,
                    title: op.text,
                    data: dataes
                });
            }
        });
        return $select;
    }

    // 公司信息下拉框
    $.fn.mkCompanySelect = function (op) {
        // op:parentId 父级id,maxHeight 200,
        var dfop = {
            type: 'tree',
            // 是否允许搜索
            allowSearch: true,
            // 访问数据接口地址
            url: top.$.rootUrl + '/OrganizationModule/Company/GetTree',
            // 访问数据接口参数
            param: { parentId: '0' },
        };
        op = op || {};
        dfop.param.parentId = op.parentId || '0';

        if (!!op.isLocal) {
            dfop.url = '';
        }
        var $select = $(this).mkselect(dfop);
        if (!!op.isLocal) {
            Changjie.clientdata.getAllAsync('company', {
                callback: function (dataes) {
                    var mapdata = {};
                    var resdata = [];
                    $.each(dataes, function (_index, _item) {
                        mapdata[_item.parentId] = mapdata[_item.parentId] || [];
                        _item.id = _index;
                        mapdata[_item.parentId].push(_item);
                    });
                    _fn(resdata, dfop.param.parentId);
                    function _fn(_data, vparentId) {
                        var pdata = mapdata[vparentId] || [];
                        for (var j = 0, l = pdata.length; j < l; j++) {
                            var _item = pdata[j];
                            var _point = {
                                id: _item.id,
                                text: _item.name,
                                value: _item.id,
                                showcheck: false,
                                checkstate: false,
                                hasChildren: false,
                                isexpand: false,
                                complete: true,
                                ChildNodes: []
                            };
                            if (_fn(_point.ChildNodes, _item.id)) {
                                _point.hasChildren = true;
                                _point.isexpand = true;
                            }
                            _data.push(_point);
                        }
                        return _data.length > 0;
                    }
                    $select.mkselectRefresh({
                        data: resdata
                    });
                }
            });
        }

        return $select;

    };
    // 部门信息下拉框
    $.fn.mkDepartmentSelect = function (op) {
        // op:parentId 父级id,maxHeight 200,
        var dfop = {
            type: 'tree',
            // 是否允许搜索
            allowSearch: true,
            // 访问数据接口地址
            url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
            // 访问数据接口参数
            param: { companyId: '', parentId: '0' },
        }
        op = op || {};
        dfop.param.companyId = op.companyId;
        dfop.param.parentId = op.parentId;

        return $(this).mkselect(dfop);;
    }; 
    // 人员下拉框
    $.fn.mkUserSelect = function (type) {//0单选1多选
        if (type == 0) {
            $(this).mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
        }
        else {
            $(this).mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectForm',
                layerUrlW: 800,
                layerUrlH: 520,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
        }
    }

    // 省市区级联
    $.fn.mkAreaSelect = function (op) {
        // op:parentId 父级id,maxHeight 200,
        var dfop = {
            // 字段
            value: "F_AreaCode",
            text: "F_AreaName",
            title: "F_AreaName",
            // 是否允许搜索
            allowSearch: true,
            // 访问数据接口地址
            url: top.$.rootUrl + '/SystemModule/Area/Getlist',
            // 访问数据接口参数
            param: { parentId: '' },
        }
        op = op || {};
        if (!!op.parentId) {
            dfop.param.parentId = op.parentId;
        }
        var _obj = [], i = 0;
        var $this = $(this);
        $(this).find('div').each(function () {
            var $div = $('<div></div>');
            var $obj = $(this);
            dfop.placeholder = $obj.attr('placeholder');
            $div.addClass($obj.attr('class'));
            $obj.removeAttr('class');
            $obj.removeAttr('placeholder');
            $div.append($obj);
            $this.append($div);
            if (i == 0) {
                $obj.mkselect(dfop);
            }
            else {
                dfop.url = "";
                dfop.parentId = "";
                $obj.mkselect(dfop);
                _obj[i - 1].on('change', function () {
                    var _value = $(this).mkselectGet();
                    if (_value == "") {
                        $obj.mkselectRefresh({
                            url: '',
                            param: { parentId: _value },
                            data: []
                        });
                    }
                    else {
                        $obj.mkselectRefresh({
                            url: top.$.rootUrl + '/SystemModule/Area/Getlist',
                            param: { parentId: _value },
                        });
                    }

                });
            }
            i++;
            _obj.push($obj);
        });
    };
    // 数据库选择
    $.fn.mkDbSelect = function (op) {
        // op:maxHeight 200,
        var dfop = {
            type: 'tree',
            // 是否允许搜索
            allowSearch: true,
            // 访问数据接口地址
            url: top.$.rootUrl + '/SystemModule/DatabaseLink/GetTreeList'
        }
        op = op || {};

        return $(this).mkselect(dfop);
    };

    // 动态获取和设置radio，checkbox
    $.fn.mkRadioCheckbox = function (op) {
        var dfop = {
            type: 'radio',        // checkbox
            default: '',
            dataType: 'dataItem', // 默认是数据字典 dataSource（数据源）
            code: '',
            text: 'F_ItemName',
            value: 'F_ItemValue'
        };
      
        $.extend(dfop, op || {});
        var $this = $(this);
        $this.addClass(dfop.type);
        $this.addClass('mk-' + dfop.type);
        $this.attr('type', 'mk-' + dfop.type);
        var thisId = $this.attr('id');
        if (dfop.dataType == 'dataItem') {
            Changjie.clientdata.getAllAsync('dataItem', {
                code: dfop.code,
                callback: function (dataes) {
                    $.each(dataes, function (id, item) {
                        var $point = $('<label><input name="' + thisId + '" value="' + item.value + '"' + ' type="' + dfop.type + '">' + item.text + '</label>');
                        $this.append($point);
                    });
                    if (dfop.default == '1') {
                        $this.find('input').eq(1).trigger('click');
                    } else {
                        $this.find('input').eq(0).trigger('click');
                    }
                }
            });
        }
        else {
            Changjie.clientdata.getAllAsync('sourceData', {
                code: dfop.code,
                callback: function (dataes) {
                    $.each(dataes, function (id, item) {
                        var $point = $('<label><input name="' + thisId + '" value="' + item[dfop.value] + '"' + '" type="' + dfop.type + '">' + item[dfop.text] + '</label>');
                        $this.append($point);
                    });
                    $this.find('input').eq(0).trigger('click');
                }
            });
        }
    };
    // 多条件查询框
    $.fn.mkMultipleQuery = function (search, height, width) {
        var $this = $(this);
        var contentHtml = $this.html();
        $this.addClass('mk-query-wrap');


        var _html = '';
        _html += '<div class="mk-query-btn"><i class="fa fa-search"></i>&nbsp;多条件查询</div>';
        _html += '<div class="mk-query-content">';
        //_html += '<div class="mk-query-formcontent">';
        _html += contentHtml;
        //_html += '</div>';
        _html += '<div class="mk-query-arrow"><div class="mk-query-inside"></div></div>';
        _html += '<div class="mk-query-content-bottom">';
        _html += '<a id="btn_queryReset" class="btn btn-default">&nbsp;重&nbsp;&nbsp;置</a>';
        _html += '<a id="btn_querySearch" class="btn btn-primary">&nbsp;查&nbsp;&nbsp;询</a>';
        _html += '</div>';
        _html += '</div>';
        $this.html(_html);
        $this.find('.mk-query-formcontent').show();

        $this.find('.mk-query-content').css({ 'width': width || 400, 'height': height || 300 });

        $this.find('.mk-query-btn').on('click', function () {
            var $content = $this.find('.mk-query-content');
            if ($content.hasClass('active')) {
                $content.removeClass('active');
            }
            else {
                $content.addClass('active');
            }
        });

        $this.find('#btn_querySearch').on('click', function () {
            var $content = $this.find('.mk-query-content');
            var query = $content.mkGetFormData();
            $content.removeClass('active');
            search(query);
        });

        $this.find('#btn_queryReset').on('click', function () {
            var $content = $this.find('.mk-query-content');
            var query = $content.mkGetFormData();
            for (var id in query) {
                query[id] = "";
            }
            $content.mkSetFormData(query);
        });

        $(document).click(function (e) {
            var et = e.target || e.srcElement;
            var $et = $(et);
            if (!$et.hasClass('mk-query-wrap') && $et.parents('.mk-query-wrap').length <= 0) {

                $('.mk-query-content').removeClass('active');
            }
        });
    };

})(jQuery, top.Changjie);
