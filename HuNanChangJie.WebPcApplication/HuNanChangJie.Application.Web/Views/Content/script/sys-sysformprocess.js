﻿// 设置权限
var setAuthorize;
// 验证数据是否填写完整
var validForm;

var save;
var setFormData;

setAuthorize = function (data) {
    $("#btn_uploadfile").attr('disabled', true);
    $("#btn_delfile").attr("disabled", true);
    for (var item in data) {
        var info = data[item];
        var inputId = info.field;
        var $input = $("#" + inputId);

        if (info.isWrite == 1) {
            $input.removeAttr("disabled");
        }
        else {
            if (info.isRead != 1) {
                $input.parent().remove();
            }
            else {
                $input.attr("disabled", "disabled");
            }
        }
    }
};


// 验证数据是否填写完整
validForm = function () {

    if (!$('body').mkValidform()) {
        return false;
    }
    return true;
};

setFormData = function (processId) {
    $("#btn_uploadfile").attr("disabled", "disabled");
    $("#btn_delfile").attr("disabled", "disabled");
    $(".fa.fa-plus").css("display", "none");
    $(".fa.fa-minus").css("display", "none");
    $(".fa.fa-file-excel-o").css("display", "none");
};

save = function (processId, callBack, i) {
    var keyValue = request("keyValue");
    var data = getFormData();
    $.mkSaveForm(processCommitUrl + "?keyValue=" + keyValue + "&type=edit", data, function (res) {
        if (!!callBack) {
            callBack(res, data, i);
        }
    });
}


