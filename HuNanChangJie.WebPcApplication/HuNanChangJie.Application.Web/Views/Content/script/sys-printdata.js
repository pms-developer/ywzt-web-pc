﻿window.PrintData = (function ($) {
    "use strict";
    var printdata = {
        getValue: function (tablename, relevanceField, keyValue, targetField) {
            if (!tablename || !relevanceField || !keyValue) return;
            var param = {
                tableName: tablename,
                pkFiled: relevanceField,
                keyValue: keyValue
            };
            var data = top.Changjie.httpGet(top.$.rootUrl + "/SystemModule/CustmerQuery/GetDataTable", param).data;
            if (data) {
               return data[0][targetField];
            }
        },
        getExtendCellValues: function () { }
    };
    return printdata;
})(window.jQuery);