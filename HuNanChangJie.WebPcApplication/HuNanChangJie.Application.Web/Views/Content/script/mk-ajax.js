﻿/*
 * 日 期：2017.03.16
 * 描 述：ajax操作方法
 */
(function ($, Changjie) {
    "use strict";
    var httpCode = {
        success: 200,
        fail: 400,
        exception: 500
    };
    var exres = { code: httpCode.exception, info: '通信异常，请联系管理员！' }
    $.extend(Changjie, {
        // http 通信异常的时候调用此方法
        httpErrorLog: function (msg) {
            Changjie.log(msg);
        },
        // http请求返回数据码
        httpCode: httpCode,
        // get请求方法（异步）:url地址,callback回调函数
        httpAsyncGet: function (url, callback) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    if (data.code == Changjie.httpCode.exception) {
                        Changjie.httpErrorLog(data.info);
                        data.info = '系统异常，请联系管理员！';
                    }
                    callback(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    Changjie.httpErrorLog(textStatus);
                    callback(exres);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        },
        // get请求方法（同步）:url地址,param参数
        httpGet: function (url, param,callback) {
            var res = {};
            $.ajax({
                url: url,
                data: param,
                type: "GET",
                dataType: "json",
                async: false,
                cache: false,
                success: function (data) {
                    if (data.code == Changjie.httpCode.exception) {
                        Changjie.httpErrorLog(data.info);
                        data.info = '系统异常，请联系管理员！';
                    }
                    res = data;
                    if (!!callback) {
                        callback(data);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    Changjie.httpErrorLog(textStatus);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
            return res;
        },
        // post请求方法（异步）:url地址,param参数,callback回调函数
        httpAsyncPost: function (url, param, callback) {
            $.ajax({
                url: url,
                data: param,
                type: "POST",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    if (data.code == Changjie.httpCode.exception) {
                        Changjie.httpErrorLog(data.info);
                        data.info = '系统异常，请联系管理员！';
                    }
                    callback(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    Changjie.httpErrorLog(textStatus);
                    callback(exres);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        },
        // post请求方法（同步步）:url地址,param参数,callback回调函数
        httpPost: function (url, param, callback) {
            $.ajax({
                url: url,
                data: param,
                type: "POST",
                dataType: "json",
                async: false,
                cache: false,
                success: function (data) {
                    if (data.code == Changjie.httpCode.exception) {
                        Changjie.httpErrorLog(data.info);
                        data.info = '系统异常，请联系管理员！';
                    }
                    callback(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    Changjie.httpErrorLog(textStatus);
                    callback(exres);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        },
        // ajax 异步封装
        httpAsync: function (type, url, param, callback) {
            $.ajax({
                url: url,
                data: param,
                type: type,
                dataType: "json",
                async: true,
                cache: false,
                success: function (res) {
                    if (res.code == Changjie.httpCode.success) {
                        callback(res.data);
                    }
                    else {
                        Changjie.httpErrorLog(res.info);
                        callback(null);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    Changjie.httpErrorLog(textStatus);
                    callback(null);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        },

        deleteForm:function (url, param, callback) {
            Changjie.loading(true, '正在删除数据');
            Changjie.httpAsyncPost(url, param, function (res) {
                Changjie.loading(false);
                if (res.code == Changjie.httpCode.success) {
                    if (!!callback) {
                        callback(res);
                    }
                    Changjie.alert.success(res.info);
                }
                else {
                    Changjie.alert.error(res.info);
                    Changjie.httpErrorLog(res.info);
                }
                layer.close(layer.index);
            });
        },
        postForm:function (url, param, callback) {
            Changjie.loading(true, '正在提交数据');
            Changjie.httpAsyncPost(url, param, function (res) {
                Changjie.loading(false);
                if (res.code == Changjie.httpCode.success) {
                    if (!!callback) {
                        callback(res);
                    }
                    Changjie.alert.success(res.info);
                }
                else {
                    Changjie.alert.error(res.info);
                    Changjie.httpErrorLog(res.info);
                }
                layer.close(layer.index);
            });
        },
        httpPostFile: function (url, param, callback) {
            $.ajax({
                url: url,
                data: param,
                type: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.code == Changjie.httpCode.exception) {
                        Changjie.httpErrorLog(data.info);
                        data.info = '系统异常，请联系管理员！';
                    }
                    callback(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    Changjie.httpErrorLog(textStatus);
                    callback(exres);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        }
    });

})(window.jQuery, top.Changjie);
