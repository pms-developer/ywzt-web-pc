﻿(function ($, Changjie) {

    $.fn.analysisFormula = function (formula) {
      
        if (!!formula) {
            if (formula.indexOf("|") != -1) {
                var paras = formula.split("|");
                var value = "";
                for (var item in paras) {
                   
                    value = analysisFormula(paras[item], value);
                }
                $(this).val(value);
            }
            else {
                var value = analysisFormula(formula);
                $(this).val(value);
            }
           
        } 
    };

    //解析公式
    var analysisFormula = function (formula, value) {
        if (formula.indexOf(".") != -1) {
            var infoList = formula.split(".");
            var methodName = infoList[0];
            var property = infoList[1];
            return  getFormulaValue(methodName, property,value);
        }
    }

    //计算公式
    var getFormulaValue = function (methodName, property,value) {
        var url = "";
        var param = {};
        switch (methodName) {
            case "project":
                var infoId = request("projectId");
                if (!!infoId) {
                    url = top.$.rootUrl + "/ProjectModule/Project/GetFormData";
                    param.keyValue = infoId;
                    var data = Changjie.httpGet(url, param);
                    if (data.code == Changjie.httpCode.success) {
                        return data.data[property];
                    }
                }
                break;
            case "user":
                var infoId = value;
                if (!!infoId) {
                    url = top.$.rootUrl + "/OrganizationModule/User/GetUserEntity";
                    param.userId = infoId;
                    var data = Changjie.httpGet(url, param);
                    if (data.code == Changjie.httpCode.success) {
                        return data.data[property];
                    }
                }
        }
    };

})(jQuery, top.Changjie);
