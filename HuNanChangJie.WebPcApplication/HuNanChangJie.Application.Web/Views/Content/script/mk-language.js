﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 日 期：2018.05.10
 * 描 述：客户端语言包加载（菜单，tab条）
 */
(function (a, Changjie) {
    var changjielanguage = "english";/*"chinese";*/
    var e = {};
    var miancode = null;
    var langobject = {};
    var currlang = {};
    var lang = {
        get: function (i) {
            if (localStorage) {
                return JSON.parse(localStorage.getItem("mklt_" + i)) || {}
            } else {
                return currlang[i] || {}
            }
        }, set: function (j, i) {
            if (localStorage) {
                localStorage.setItem("mklt_" + j, JSON.stringify(i));
            } else {
                currlang[j] = i;
            }
        }
    };
    Changjie.language = {
        getMainCode: function () {
            return miancode;
        }, get: function (l, i) {
            if (changjielanguage != miancode) {
                if (langobject[changjielanguage] && langobject[miancode]) {
                    var k = lang.get(miancode);
                    var j = lang.get(changjielanguage);
                    i(j.data[k.data[l]] || l);
                } else {
                    setTimeout(function () {
                        Changjie.language.get(l, i);
                    },
                        200);
                }
            } else {
                i(l);
            }
        }, getSyn: function (k) {
            //if (changjielanguage != miancode) {
            //    var j = lang.get(miancode);
            //    var i = lang.get(changjielanguage);
            //    return i.data[j.data[k]] || k;
            //} else {
            //    return k;
            //}
        }
    };
    a(function () {
        //changjielanguage = top.$.cookie("Changjie_ADMS_V2_Language") || "no";
        var i = a("#lg_setting");
        //if (changjielanguage == "no") {
        //    i.find("span").text("简体中文");
        //}
       
        Changjie.httpAsyncGet(top.$.rootUrl + "/LGManager/LGType/GetList",
            function (m) {
                if (m.code == 200) {
                    var j = i.find("ul");
                    a.each(m.data,
                        function (n, o) {
                            e[o.F_Code] = o.F_Name;
                            if (o.F_IsMain == 1) {
                                miancode = o.F_Code;
                                if (changjielanguage == "no") {
                                    changjielanguage = miancode;
                                }
                            }
                            langobject[o.F_Code] = false;
                            var p = '<li><a href="javascript:void(0);" data-value="' +
                                o.F_Code +
                                '" >' +
                                o.F_Name +
                                "</a></li>";
                            j.append(p)
                        });
               
                }
            });
    });
})(window.jQuery, top.Changjie);
