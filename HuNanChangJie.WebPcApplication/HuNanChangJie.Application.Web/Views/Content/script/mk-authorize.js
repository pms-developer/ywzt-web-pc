﻿/*
 * 日 期：2017.03.16
 * 描 述：权限验证模块
 */
(function ($, Changjie) {
    "use strict";

    $.fn.mkAuthorizeJfGrid = function (op) {
        var _headData = [];
        $.each(op.headData, function (id, item) {
            if (!!mkModuleColumnList[item.name.toLowerCase()]) {

                //多语言初始化列表，表头
                data.forEach(function (itemData) {
                    if (item.label == itemData.key) {
                        item.label = itemData.value;
                    }
                })

                _headData.push(item);
            }
        });
        if (!!op.enableWorkflow) {
            var stateColumn = {};
            stateColumn.label = "审核状态";
            stateColumn.name = "auditstatus";
            stateColumn.width = 150;
            stateColumn.align = "center";
            stateColumn.formatter = function (cellvalue, row) {
                switch (cellvalue) {
                    case "0":
                        return '<span class=\"label label-default\" style=\"cursor: pointer;\">未审核</span>';
                    case "1":
                        return '<span class=\"label label-info\" style=\"cursor: pointer;\">审核中</span>';
                    case "2":
                        return '<span class=\"label label-success\" style=\"cursor: pointer;\">已通过</span>';
                    case "3":
                    case "4":
                        return '<span class=\"label label-warning\" style=\"cursor: pointer;\">未通过</span>';
                    default:
                        return '<span class=\"label label-default\" style=\"cursor: pointer;\">未审核</span>';
                }
            };
            _headData.push(stateColumn);
        }
        op.headData = _headData;
        $(this).jfGrid(op);
    }

    $.fn.mkAuthorizeAgGrid = function (op) {
        var _headData = [];

        $.each(op.headData, function (id, item) {
            if (!!item.label) {
                item.headerName = item.label;

                $(item).removeProp("label");
            }
            if (!!item.name) {
                item.field = item.name;
                $(item).removeProp("name");
            }
            if (!!mkModuleColumnList[item.field.toLowerCase()]) {
                _headData.push(item);
            }
        });
        op.headData = _headData;
        $(this).AgGrid(op);
    }


    $(function () {
        function btnAuthorize() {
            if (!!mkModuleButtonList) {
                var $container = $('[changjie-authorize="yes"]');

                $container.find('[id]').each(function () {
                    var $this = $(this);
                    var id = $this.attr('id');
                    if (!mkModuleButtonList[id]) {
                        $this.remove();
                    }
                });
                $container.find('.dropdown-menu').each(function () {
                    var $this = $(this);
                    if ($this.find('li').length == 0) {
                        $this.remove();
                    }
                });
                $container.css({ 'display': 'inline-block' });
            }
            else {
                setTimeout(btnAuthorize, 100);
            }
        }
        btnAuthorize();
    });

})(window.jQuery, top.Changjie);
