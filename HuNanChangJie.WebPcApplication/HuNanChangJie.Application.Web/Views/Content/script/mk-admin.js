﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 创建人：前端开发组
 * 日 期：2017.03.16
 * 描 述：admin顶层页面操作方法
 */

var loaddfimg;
(function ($, Changjie) {
    "use strict";
    
    var page = {
        init: function () {
            /*判断当前浏览器是否是IE浏览器*/
            if ($('body').hasClass('IE') || $('body').hasClass('InternetExplorer')) {
                $('#loadbg').append('<img data-img="imgdl" src="' + top.$.rootUrl + '/Content/images/ie-loader.gif" style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;margin: auto;vertical-align: middle;">');
                Pace.stop();
            }
            else {
                Pace.on('done', function () {
                    $('#loadbg').fadeOut();
                    Pace.options.target = '#changjiepacenone';
                });
            }
            // 通知栏插件初始化设置
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            // 打开首页模板
           
            Changjie.frameTab.open({ F_ModuleId: '10000', F_Icon: 'fa fa-desktop', F_FullName: '我的桌面', F_UrlAddress: '/Home/AdminDesktop' }, true);
            //Changjie.frameTab.open({ F_ModuleId: '20000', F_Icon: 'fa fa-desktop', F_FullName: '热门功能', F_UrlAddress: '/Home/DesktopIndex' }, true);

            Changjie.clientdata.init(function () {
                page.userInit();
                // 初始页面特例
                bootstrap($, Changjie);
                if ($('body').hasClass('IE') || $('body').hasClass('InternetExplorer')) {
                    $('#loadbg').fadeOut();
                }
            });

            // 加载数据进度
            page.loadbarInit();
            // 全屏按钮
            page.fullScreenInit();
            // 主题选择初始化
            page.uitheme();
           
        },
       
        // 登录头像和个人设置
        userInit: function () {
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            var _html = '<div class="mk-frame-personCenter"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">';
            _html += '<img id="userhead"src="' + top.$.rootUrl + '/OrganizationModule/User/GetImg?userId=' + loginInfo.userId + '" onerror="top.Changjie.nofind()" >';
            _html += '<span>' + loginInfo.realName + '</span>';
            _html += '</a>';
            _html += '<ul class="dropdown-menu pull-right">';
            _html += '<li><a href="javascript:void(0);" id="userinfo_btn"><i class="fa fa-user"></i>个人信息</a></li>';
            _html += '<li><a href="javascript:void(0);" id="schedule_btn"><i class="fa fa-calendar"></i>我的日程</a></li>';
          /*  _html += '<li><a href="javascript:void(0);" id="WmsChangeWarehousebtn"><i class="fa fa-bank"></i>切换仓库</a></li>';*/
            if (loginInfo.isSystem) {
                _html += '<li><a href="javascript:void(0);" id="clearredis_btn"><i class="fa fa-refresh"></i>清空缓存</a></li>';
            }
            _html += '<li><a href="javascript:void(0);" id="loginout_btn"><i class="fa fa-power-off"></i>安全退出</a></li>';
            _html += '</ul></div>';
            $('body').append(_html);

            $('#loginout_btn').on('click', page.loginout);
            $('#userinfo_btn').on('click', page.openUserCenter);
            $('#clearredis_btn').on('click', page.clearredis);
            $('#WmsChangeWarehousebtn').on('click', page.changeWarehouse);
        },
        loginout: function () { // 安全退出
            Changjie.layerConfirm("注：您确定要安全退出本次登录吗？", function (r) {
                if (r) {
                    Changjie.loading(true, '退出系统中...');
                    Changjie.httpAsyncPost($.rootUrl + '/Login/OutLogin', {}, function (data) {
                        window.location.href = $.rootUrl + "/Login/Index";
                    });
                }
            });
        },
        changeWarehouse: function () {
            Changjie.layerForm({
                id: 'wmsChangeForm',
                title: '切换仓库',
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouse/ChangeForm',
                width: 550,
                height: 690,
                btn: null
            });
        },
        clearredis: function () {
            Changjie.layerConfirm("注：您确定要清空全部后台缓存数据吗？", function (r) {
                if (r) {
                    Changjie.loading(true, '清理缓存数据中...');
                    Changjie.httpAsyncPost($.rootUrl + '/Home/ClearRedis', {}, function (data) {
                        window.location.href = $.rootUrl + "/Login/Index";
                    });
                }
            });
        },
        openUserCenter: function () {
            // 打开个人中心
            Changjie.frameTab.open({ F_ModuleId: '1', F_Icon: 'fa fa-user', F_FullName: '个人中心', F_UrlAddress: '/UserCenter/Index' });
        },

        // 全屏按钮
        fullScreenInit: function () {
            var _html = '<div class="frame_fullscreen"><a href="javascript:void(0);" id="fullscreen_btn" title="全屏"><i class="fa fa-arrows-alt"></i></a></div>';
            $('body').append(_html);
            $('#fullscreen_btn').on('click', function () {
                if (!$(this).attr('fullscreen')) {
                    $(this).attr('fullscreen', 'true');
                    page.requestFullScreen();
                } else {
                    $(this).removeAttr('fullscreen');
                    page.exitFullscreen();
                }
            });
        },
        requestFullScreen: function () {
            var de = document.documentElement;
            if (de.requestFullscreen) {
                de.requestFullscreen();
            } else if (de.mozRequestFullScreen) {
                de.mozRequestFullScreen();
            } else if (de.webkitRequestFullScreen) {
                de.webkitRequestFullScreen();
            }
        },
        exitFullscreen: function () {
            var de = document;
            if (de.exitFullscreen) {
                de.exitFullscreen();
            } else if (de.mozCancelFullScreen) {
                de.mozCancelFullScreen();
            } else if (de.webkitCancelFullScreen) {
                de.webkitCancelFullScreen();
            }
        },

        // 加载数据进度
        loadbarInit: function () {
            var _html = '<div class="mk-loading-bar" id="loading_bar" >';
            _html += '<div class="mk-loading-bar-bg"></div>';
            _html += '<div class="mk-loading-bar-message" id="loading_bar_message"></div>';
            _html += '</div>';
            $('body').append(_html);
        },

        // 皮肤主题设置
        uitheme: function () {
           
        },
    };

    $(function () {
        page.init();
    });
})(window.jQuery, top.Changjie);
