﻿/*
 * 日期 2020-04-01
 * 描述 公式设计器
 */
(function ($, Changjie) {
    "use strict";
    $.formulaDesigner = {
        init: function ($self, op) {
            var dfop = {
            };
            $.extend(dfop, op || {});
            dfop.id = $self.attr('id');
            $self[0]._formulaDesigner = { dfop: dfop };
            
            var _html = '';
            _html += '<div class="mk-custmerform-designer-layout-left"  id="custmerform_compont_list_' + dfop.id + '"></div>';

            _html += '<div class="mk-custmerform-designer-layout-center">';
            _html += '<div class="mk-custmerform-designer-layout-header">';
            _html += '<div class="mk-custmerform-designer-tabs" id="custmerform_designer_tabs_' + dfop.id + '">';
            _html += '</div>';
            _html += '</div>';

            _html += '<div class="mk-custmerform-designer-layout-area" id="custmerform_designer_layout_area_' + dfop.id + '" ></div>';


            _html += '</div>';

            $self.html(_html);

            $.formulaDesigner.bind($self);
            $.formulaDesigner.formulainit($self);
            $.formulaDesigner.formulabind($self);
            $.formulaDesigner.renderFormulas($self);
        },
        bind: function ($self) {
            var dfop = $self[0]._formulaDesigner.dfop;
            $('#custmerform_compont_list_' + dfop.id).mkscroll();
            $('#custmerform_designer_tabs_' + dfop.id).mkscroll();
            $('#custmerform_designer_layout_area_' + dfop.id).mkscroll();
            $('#custmerform_designer_layout_area_' + dfop.id + ' .mk-scroll-box')[0].dfop = dfop;
        },
        formulainit: function ($self) {
            var dfop = $self[0]._formulaDesigner.dfop;
            var $formulaList = $self.find('#custmerform_compont_list_' + dfop.id + ' .mk-scroll-box');
            //$formulaList.append('<div class="mk-custmerform-component" data-type="label" id="TaxType" name="计税方式" ><i  class="fa fa-random"></i>计税方式</div>');
            //$formulaList.append('<div class="mk-custmerform-component" data-type="label" id="ybjs" name="一般计税" ><i  class="fa fa-long-arrow-right"></i>一般计税</div>');
            //$formulaList.append('<div class="mk-custmerform-component" data-type="label" id="jyjs" name="简易计税"><i  class="fa fa-long-arrow-left"></i>简易计税</div>');
            var id = "custmerform_designer_layout_area_" + dfop.id;
            var ue = UE.getEditor(id, {
                toolbars: [
                    ['fullscreen',"source", 'undo', 'redo']
                ],
                initialFrameHeight: 450,
                initialFrameWidth: 525,
                autoHeightEnabled: true,
                autoFloatEnabled: true,
                elementPathEnabled: false
            });
            
            //$("#TaxType,#ybjs,#jyjs").on("click", function () {
            //    var $this = $(this);
            //    ue.focus();
            //    ue.execCommand('inserthtml', '<a href="' + $this.attr("id") + '">' + $this.attr("name")+ '</a>');
            //});
            if (dfop.formulas && dfop.formulas.length > 0) {
                $.each(dfop.formulas, function (i,item) {
                    var $html = $('<div class="mk-custmerform-component" style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;" title="'+item.Name+'" data-type="label" id="' + item.ID +'" ><i  class="fa fa-cogs"></i>'+item.Name+'</div>');
                    $formulaList.append($html);
                    var $element = $("#" + item.ID);
                    $element.on("click", function () {
                        ue.focus();
                        ue.execCommand('inserthtml', '<a href="javascript:void(0)" title="'+item.ID+'">' + item.Name + '</a>');
                    });
                });
            }
            ue.ready(function () {
                if (dfop.value) {
                    ue.setContent(dfop.value);
                }
            });
            
        },
        formulabind: function ($self) {
        },
        renderFormulas: function ($self) {
            var dfop = $self[0]._formulaDesigner.dfop;
            var compontsLayout = $('#custmerform_designer_layout_area_' + dfop.id + ' .mk-scroll-box');
            compontsLayout.html('');
             
            $('.mk-custmerform-designer-layout-right').animate({ right: '-240px', speed: 2000 });
            $('.mk-custmerform-designer-layout').animate({ 'padding-right': '0px', speed: 2000 });

        },
    };
    $.fn.formulaDesigner = function (op) {
        var $this = $(this);
        if (!$this.attr('id')) return false;
        $.formulaDesigner.init($this, op);
    };
})(jQuery, top.Changjie)
