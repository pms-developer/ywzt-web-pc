﻿

(function ($, Changjie) {
    "use strict";

    var loadSate = {
        no: -1,  // 还未加载
        yes: 1,  // 已经加载成功
        ing: 0,  // 正在加载中
        fail: 2  // 加载失败
    };

    var clientDataFn = {};
    var clientAsyncData = {};

    var clientData = {};


    function initLoad(callback) {
        var res = loadSate.yes;
        for (var id in clientDataFn) {
            var _fn = clientDataFn[id];
            if (_fn.state == loadSate.fail) {
                res = loadSate.fail;
                break;
            }
            else if (_fn.state == loadSate.no) {
                res = loadSate.ing;
                _fn.init();
            }
            else if (_fn.state == loadSate.ing) {
                res = loadSate.ing;
            }
        }
        if (res == loadSate.yes) {
            callback(true);
        } else if (res == loadSate.fail) {
            callback(false);
        }
        else {
            setTimeout(function () {
                initLoad(callback);
            }, 100);
        }
    }
    function get(key, data) {
        var res = "";
        var len = data.length;
        if (len == undefined) {
            res = data[key];
        }
        else {
            for (var i = 0; i < len; i++) {
                if (key(data[i])) {
                    res = data[i];
                    break;
                }
            }
        }
        return res;
    }

    Changjie.clientdata = {
        init: function (callback) {
            initLoad(function (res) {
                callback(res);
                if (res) {// 开始异步加载数据
                    clientAsyncData.company.init();
                }
            });
        },
        get: function (nameArray) {//[key,function (v) { return v.key == value }]
            var res = "";
            if (!nameArray) {
                return res;
            }
            var len = nameArray.length;
            var data = clientData;
            for (var i = 0; i < len; i++) {
                res = get(nameArray[i], data);
                if (res != "" && res != undefined) {
                    data = res;
                }
                else {
                    break;
                }
            }
            res = res || "";
            return res;
        },
        getSync: function (name, op) {//
            return clientAsyncData[name].getSync(op);
        },
        getAsync: function (name, op) {//
            return clientAsyncData[name].get(op);
        },
        getAllAsync: function (name, op) {//
            return clientAsyncData[name].getAll(op);
        },
        getsAsync: function (name, op) {//
            return clientAsyncData[name].gets(op);
        },
        update: function (name) {
            clientAsyncData[name].update && clientAsyncData[name].update();
        },
        refresh: function (name,op) {
            clientAsyncData[name].refresh && clientAsyncData[name].refresh(op);
        }
    };


    /*******************登录后数据***********************/
    // 注册数据的加载方法
    // 功能模块数据
    clientDataFn.modules = {
        state: loadSate.no,
        init: function () {
            //初始化加载数据
            clientDataFn.modules.state = loadSate.ing;
            Changjie.httpAsyncGet($.rootUrl + '/SystemModule/Module/GetModuleList', function (res) {

                if (res.code == Changjie.httpCode.success) {
                    clientData.modules = res.data;
                    clientDataFn.modules.toMap();
                    clientDataFn.modules.state = loadSate.yes;
                }
                else {
                    clientData.modules = [];
                    clientDataFn.modules.toMap();
                    clientDataFn.modules.state = loadSate.fail;
                }
            });
        },
        toMap: function () {
            //转化成树结构 和 转化成字典结构
            var modulesTree = {};
            var modulesMap = {};
            var _len = clientData.modules.length;
            for (var i = 0; i < _len; i++) {
                var _item = clientData.modules[i];
                if (_item.F_EnabledMark == 1) {
                    modulesTree[_item.F_ParentId] = modulesTree[_item.F_ParentId] || [];
                    modulesTree[_item.F_ParentId].push(_item);
                    modulesMap[_item.F_ModuleId] = _item;
                }
            }
            clientData.modulesTree = modulesTree;
            clientData.modulesMap = modulesMap;
        }
    };
    // 登录用户信息
    clientDataFn.userinfo = {
        state: loadSate.no,
        init: function () {
            //初始化加载数据
            clientDataFn.userinfo.state = loadSate.ing;
            Changjie.httpAsyncGet($.rootUrl + '/Login/GetUserInfo', function (res) {
                if (res.code == Changjie.httpCode.success) {
                    clientData.userinfo = res.data;
                    clientDataFn.userinfo.state = loadSate.yes;
                }
                else {
                    clientDataFn.userinfo.state = loadSate.fail;
                }
            });
        }
    };

    /*******************使用时异步获取*******************/
    var storage = {
        get: function (name) {
            if (localStorage) {
                return JSON.parse(localStorage.getItem(name)) || {};
            }
            else {
                return clientData[name] || {};
            }
        },
        set: function (name, data) {
            if (localStorage) {
                localStorage.setItem(name, JSON.stringify(data));
                // localStorage.clear();
            }
            else {
                clientData[name] = data;
            }
        }
    };
    // 公司信息
    clientAsyncData.company = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.company.states == loadSate.no) {
                clientAsyncData.company.states = loadSate.ing;
                var ver = storage.get("companyData").ver || "";
                Changjie.httpAsync('GET', top.$.rootUrl + '/OrganizationModule/Company/GetMap', { ver: ver }, function (data) {
                    if (!data) {
                        clientAsyncData.company.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("companyData", data);
                        }
                        clientAsyncData.company.states = loadSate.yes;
                        clientAsyncData.department.init();
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.company.init();
            if (clientAsyncData.company.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.company.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("companyData").data || {};
                op.callback(data[op.key] || {}, op);
            }
        },
        getAll: function (op) {
            clientAsyncData.company.init();
            if (clientAsyncData.company.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.company.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("companyData").data || {};
                op.callback(data, op);
            }
        }
    };
    // 部门信息
    clientAsyncData.department = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.department.states == loadSate.no) {
                clientAsyncData.department.states = loadSate.ing;
                var ver = storage.get("departmentData").ver || "";
                Changjie.httpAsync('GET', top.$.rootUrl + '/OrganizationModule/Department/GetMap', { ver: ver }, function (data) {
                    if (!data) {
                        clientAsyncData.department.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("departmentData", data);
                        }
                        clientAsyncData.department.states = loadSate.yes;
                        clientAsyncData.user.init();
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.department.init();
            if (clientAsyncData.department.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.department.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("departmentData").data || {};
                op.callback(data[op.key] || {}, op);
            }
        },
        getSync: function (op) {
            clientAsyncData.department.init();
            if (clientAsyncData.department.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.department.get(op);
                }, 100);// 如果还在加载100ms后再检测
                return null;
            }
            else {
                var data = storage.get("departmentData").data || {};
                return data[op.key] || {}
            }
        },
        getAll: function (op) {
            clientAsyncData.department.init();
            if (clientAsyncData.department.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.department.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("departmentData").data || {};
                op.callback(data, op);
            }
        }
    };
    // 人员信息
    clientAsyncData.user = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.user.states == loadSate.no) {
                clientAsyncData.user.states = loadSate.ing;
                var ver = storage.get("userData").ver || "";
                Changjie.httpAsync('GET', top.$.rootUrl + '/OrganizationModule/User/GetMap', { ver: ver }, function (data) {
                    if (!data) {
                        clientAsyncData.user.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("userData", data);
                        }
                        clientAsyncData.user.states = loadSate.yes;
                        clientAsyncData.dataItem.init();
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.user.init();
            if (clientAsyncData.user.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.user.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("userData").data || {};
                op.callback(data[op.key] || {}, op);
            }
        },
        getAll: function (op) {
            clientAsyncData.user.init();
            if (clientAsyncData.user.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.user.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("userData").data || {};
                op.callback(data, op);
            }
        }
    };
    // 数据字典
    clientAsyncData.dataItem = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.dataItem.states == loadSate.no) {
                clientAsyncData.dataItem.states = loadSate.ing;
                var ver = storage.get("dataItemData").ver || "";

                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DataItem/GetMap', { ver: ver }, function (data) {
                    if (!data) {

                        clientAsyncData.dataItem.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("dataItemData", data);
                        }
                        clientAsyncData.dataItem.states = loadSate.yes;
                        clientAsyncData.db.init();
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.dataItem.init();
            if (clientAsyncData.dataItem.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.dataItem.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dataItemData").data || {};

                // 数据字典翻译
                var _item = clientAsyncData.dataItem.find(op.key, data[op.code] || {});
                if (_item) {
                    //top.Changjie.language.get(_item.text, function (text) {
                    //    _item.text = text;
                        op.callback(_item, op);
                    //});
                }
                else {
                    op.callback({}, op);
                }
            }
        },
        getSync: function (op) {
            clientAsyncData.dataItem.init();
            if (clientAsyncData.dataItem.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.dataItem.getSync(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dataItemData").data || {};


                // 数据字典翻译
                var _item = clientAsyncData.dataItem.find(op.key, data[op.code] || {});
                if (_item) {
                    //top.Changjie.language.get(_item.text, function (text) {
                    //    _item.text = text;
                        if (!!op.callback) {
                            op.callback(_item, op);
                        }


                    /*});*/
                }
                else {
                    op.callback({}, op);
                }
                if (!!_item) {
                    return _item.text || {}
                }

            }
        },
        getAll: function (op) {
            clientAsyncData.dataItem.init();
            if (clientAsyncData.dataItem.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.dataItem.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dataItemData").data || {};
                var res = {};
                $.each(data[op.code] || {}, function (_index, _item) {
                    //_item.text = top.Changjie.language.getSyn(_item.text);
                    res[_index] = _item;
                });
                op.callback(res, op);
            }
        },
        gets: function (op) {
            clientAsyncData.dataItem.init();
            if (clientAsyncData.dataItem.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.dataItem.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dataItemData").data || {};

                var keyList = op.key.split(',');
                var _text = []
                $.each(keyList, function (_index, _item) {
                    var _item = clientAsyncData.dataItem.find(_item, data[op.code] || {});
                    //top.Changjie.language.get(_item.text, function (text) {
                    //    _text.push(text);
                    //});
                    _text.push(_item.text);
                });
                op.callback(String(_text), op);
            }
        },
        find: function (key, data) {
            var res = {};
            for (var id in data) {
                if (data[id].value == key) {
                    res = data[id];
                    break;
                }
            }
            return res;
        },
        update: function () {
            clientAsyncData.dataItem.states = loadSate.no;
            clientAsyncData.dataItem.init();
        }
    };
    // 数据库连接数据
    clientAsyncData.db = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.db.states == loadSate.no) {
                clientAsyncData.db.states = loadSate.ing;
                var ver = storage.get("dbData").ver || "";
                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseLink/GetMap', { ver: ver }, function (data) {
                    if (!data) {
                        clientAsyncData.db.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("dbData", data);
                        }
                        clientAsyncData.db.states = loadSate.yes;
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.db.init();
            if (clientAsyncData.db.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.db.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dbData").data || {};
                op.callback(data[op.key] || {}, op);
            }
        },
        getAll: function (op) {
            clientAsyncData.db.init();
            if (clientAsyncData.db.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.db.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dbData").data || {};
                op.callback(data, op);
            }
        }
    };
    // 数据源数据
    clientAsyncData.sourceData = {
        states: {},
        get: function (op) {
            var strWhere = op.strWhere || "";
            //var cacheKey = op.code + strWhere;
            //if (clientAsyncData.sourceData.states[cacheKey] == undefined || clientAsyncData.sourceData.states[cacheKey] == loadSate.no) {
            //    clientAsyncData.sourceData.states[cacheKey] = loadSate.ing;
            //    clientAsyncData.sourceData.load(op);
            //}

            //if (clientAsyncData.sourceData.states[cacheKey] == loadSate.ing) {
            //    setTimeout(function () {
            //        clientAsyncData.sourceData.get(op);
            //    }, 100);// 如果还在加载100ms后再检测
            //}
            //else {
            //    var data = storage.get("sourceData_" + cacheKey).data || [];
            //    if (!!data) {
            //        op.callback(clientAsyncData.sourceData.find(op.key, op.keyId, data) || {}, op);
            //    } else {
            //        op.callback({}, op);
            //    }
            //}
            if (op && op.code) {
                Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetMapUseCache', { code: op.code, strWhere: strWhere }, function (res) {
                    if (res.code == 200) {
                        op.callback(clientAsyncData.sourceData.find(op.key, op.keyId, res.data) || {}, op);
                    } else {
                        op.callback({}, op);
                    }
                });
            }
            else {
                op.callback({}, op);
            }
        },
        getAll: function (op) {
            var strWhere = op.strWhere || "";
            if (op && op.code) {
                Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetMapUseCache', { code: op.code, strWhere: strWhere }, function (res) {
                    if (res.code == 200) {
                        op.callback(res.data, op);
                    } else {
                        op.callback({}, op);
                    }
                });
            }
            else {
                op.callback({}, op);
            }

            //var strWhere = op.strWhere || "";
            //var cacheKey = op.code + strWhere;
            //var isLoadCacheDate = op.isLoadCacheDate || false;
            //if (isLoadCacheDate) {
            //    if (clientAsyncData.sourceData.states[cacheKey] == undefined || clientAsyncData.sourceData.states[cacheKey] == loadSate.no) {
            //        clientAsyncData.sourceData.states[cacheKey] = loadSate.ing;
            //        clientAsyncData.sourceData.load(op);
            //    }
            //}
            //else {
            //    if (clientAsyncData.sourceData.states[cacheKey] != loadSate.yes) {
            //        //clientAsyncData.sourceData.states[cacheKey] = loadSate.ing;
            //        clientAsyncData.sourceData.load(op);
            //    }
            //}

            ////if (clientAsyncData.sourceData.states[cacheKey] == loadSate.ing) {
            //if (clientAsyncData.sourceData.states[cacheKey] != loadSate.yes) {
            //    setTimeout(function () {
            //        clientAsyncData.sourceData.getAll(op);
            //    }, 100);// 如果还在加载100ms后再检测
            //}
            //else if (clientAsyncData.sourceData.states[cacheKey] == loadSate.yes) {
            //    var data = storage.get("sourceData_" + cacheKey).data || [];

            //    if (!!data) {
            //        op.callback(data, op);
            //    } else {
            //        op.callback({}, op);
            //    }
            //}
        },
        load: function (op) {
            //var strWhere = op.strWhere || "";
            //var cacheKey = op.code + strWhere;
            //var ver = storage.get("sourceData_" + cacheKey).ver || "";

            //Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DataSource/GetMap', { code: op.code, ver: op.ver, strWhere: strWhere }, function (data) {
            //    if (!data) {
            //        clientAsyncData.sourceData.states[cacheKey] = loadSate.fail;
            //    } else {
            //        if (data.ver) { 
            //            storage.set("sourceData_" + cacheKey, data);  
            //        }
            //        clientAsyncData.sourceData.states[cacheKey] = loadSate.yes;
            //    }
            //});
        },
        find: function (key, keyId, data) {
            var res = {};
            for (var i = 0, l = data.length; i < l; i++) {
                if (data[i][keyId] == key) {
                    res = data[i];
                    break;
                }
            }
            return res;
        },
        refresh: function (op) {
            clientAsyncData.sourceData.load(op);
        }
    };
    // 获取自定义数据 url key valueId
    clientAsyncData.custmerData = {
        states: {},
        get: function (op) {
            if (clientAsyncData.custmerData.states[op.url] == undefined || clientAsyncData.custmerData.states[op.url] == loadSate.no) {
                clientAsyncData.custmerData.states[op.url] = loadSate.ing;
                clientAsyncData.custmerData.load(op.url);
            }
            if (clientAsyncData.custmerData.states[op.url] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.custmerData.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = clientData[op.url] || [];
                if (!!data) {
                    op.callback(clientAsyncData.custmerData.find(op.key, op.keyId, data) || {}, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        gets: function (op) {
            if (clientAsyncData.custmerData.states[op.url] == undefined || clientAsyncData.custmerData.states[op.url] == loadSate.no) {
                clientAsyncData.custmerData.states[op.url] = loadSate.ing;
                clientAsyncData.custmerData.load(op.url);
            }
            if (clientAsyncData.custmerData.states[op.url] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.custmerData.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = clientData[op.url] || [];
                if (!!data) {
                    var keyList = op.key.split(',');
                    var _text = []
                    $.each(keyList, function (_index, _item) {
                        var _item = clientAsyncData.custmerData.find(op.key, op.keyId, data) || {};
                        if (_item[op.textId]) {
                            _text.push(_item[op.textId]);
                        }

                    });
                    op.callback(String(_text), op);
                } else {
                    op.callback('', op);
                }
            }
        },
        getAll: function (op) {
            if (clientAsyncData.custmerData.states[op.url] == undefined || clientAsyncData.custmerData.states[op.url] == loadSate.no) {
                clientAsyncData.custmerData.states[op.url] = loadSate.ing;
                clientAsyncData.custmerData.load(op.url);
            }
            if (clientAsyncData.custmerData.states[op.url] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.custmerData.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = clientData[op.url] || [];
                if (!!data) {
                    op.callback(data, op);
                } else {
                    op.callback([], op);
                }
            }
        },
        load: function (url) {
            Changjie.httpAsync('GET', top.$.rootUrl + url, {}, function (data) {
                if (!!data) {
                    clientData[url] = data;
                }
                clientAsyncData.custmerData.states[url] = loadSate.yes;
            });
        },
        find: function (key, keyId, data) {
            var res = {};
            for (var i = 0, l = data.length; i < l; i++) {
                if (data[i][keyId] == key) {
                    res = data[i];
                    break;
                }
            }
            return res;
        },
        refresh: function (op) {
            clientAsyncData.custmerData.states[op.url] = loadSate.no;
            clientAsyncData.custmerData.load(op.url);
        }
    };
    //桌面
    clientAsyncData.desktop = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.desktop.states == loadSate.no) {
                clientAsyncData.desktop.states = loadSate.ing;
                var j = storage.get("desktopData").ver || "";
                Changjie.httpAsync("GET",
                    top.$.rootUrl + "/Desktop/DTTarget/GetMap",
                    {
                        ver: j
                    },
                    function (k) {
                        if (!k) {
                            clientAsyncData.desktop.states = loadSate.fail;
                        } else {
                            if (k.ver) {
                                storage.set("desktopData", k);
                            }
                            clientAsyncData.desktop.states = loadSate.yes;
                            clientAsyncData.department.init();
                        }
                    });
            }
        }, get: function (k) {
            clientAsyncData.desktop.init();
            if (clientAsyncData.desktop.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.desktop.get(k);
                }, 100)
            } else {
                var j = storage.get("desktopData").data || {};
                k.callback(j || {}, k);
            }
        }
    };

    clientAsyncData.Province = {
        states: {},
        get: function (op) {
            var state = clientAsyncData.Province.states[op.code];
            if (state == undefined || state == loadSate.no) {
                state = loadSate.ing;
                clientAsyncData.Province.load(op.code);
            }
            if (state == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.Province.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("Province").data || [];
                if (!!data) {
                    op.callback(clientAsyncData.Province.find(op.key, op.keyId, data) || {}, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        getAll: function (op) {
            if (clientAsyncData.Province.states[op.code] == undefined || clientAsyncData.Province.states[op.code] == loadSate.no) {
                clientAsyncData.Province.states[op.code] = loadSate.ing;
                clientAsyncData.Province.load(op.code);
            }

            if (clientAsyncData.Province.states[op.code] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.Province.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else if (clientAsyncData.Province.states[op.code] == loadSate.yes) {
                var data = storage.get("Province").data || [];
                if (!!data) {
                    op.callback(data, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        load: function (code) {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/Area/GetProvince', {}, function (data) {
                if (!data) {
                    clientAsyncData.Province.states[code] = loadSate.fail;
                } else {
                    storage.set("Province", data);
                    clientAsyncData.Province.states[code] = loadSate.yes;
                }
            });
        },
        find: function (key, keyId, data) {
            var res = {};
            for (var i = 0, l = data.length; i < l; i++) {
                if (data[i][keyId] == key) {
                    res = data[i];
                    break;
                }
            }
            return res;
        }
    };

    //子地区
    clientAsyncData.SubArea = {
        states: {},
        get: function (op) {
            var state = clientAsyncData.SubArea.states[op.parentId];
            if (state == undefined || state == loadSate.no) {
                state = loadSate.ing;
                clientAsyncData.SubArea.load(op.parentId);
            }
            if (state == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.SubArea.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("SubArea_" + op.parentId).data || [];
                if (!!data) {
                    op.callback(clientAsyncData.SubArea.find(op.key, op.keyId, data) || {}, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        getAll: function (op) {
            if (clientAsyncData.SubArea.states[op.parentId] == undefined || clientAsyncData.SubArea.states[op.parentId] == loadSate.no) {
                clientAsyncData.SubArea.states[op.parentId] = loadSate.ing;
                clientAsyncData.SubArea.load(op.parentId);
            }

            if (clientAsyncData.SubArea.states[op.parentId] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.SubArea.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else if (clientAsyncData.SubArea.states[op.parentId] == loadSate.yes) {
                var data = storage.get("SubArea_" + op.parentId).data || [];
                if (!!data) {
                    op.callback(data, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        load: function (parentId) {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/Area/GetSubArea', { parentId: parentId}, function (data) {
                if (!data) {
                    clientAsyncData.SubArea.states[parentId] = loadSate.fail;
                } else {
                    storage.set("SubArea_" + parentId, data);
                    clientAsyncData.SubArea.states[parentId] = loadSate.yes;
                }
            });
        },
        find: function (key, keyId, data) {
            var res = {};
            for (var i = 0, l = data.length; i < l; i++) {
                if (data[i][keyId] == key) {
                    res = data[i];
                    break;
                }
            }
            return res;
        }
    };

    //musen扩展开始
    clientAsyncData.sourceDataItem = {
        states: {},
        get: function (op) { 
            var state = clientAsyncData.sourceDataItem.states[op.code];
            if (state == undefined || state == loadSate.no) {
                state = loadSate.ing;
                clientAsyncData.sourceDataItem.load(op);
            }
            if (state == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.sourceDataItem.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("sourceData_" + op.code).data || [];
                if (!!data) {
                    op.callback(clientAsyncData.sourceDataItem.find(op.key, op.keyId, data) || {}, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        getAll: function (op) { 
            var state = clientAsyncData.sourceDataItem.states[op.code];
            if (state == undefined || state == loadSate.no) {
                state = loadSate.ing;
                clientAsyncData.sourceDataItem.load(op);
            }

            if (state == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.sourceDataItem.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else if (state == loadSate.yes) {
                var data = storage.get("sourceData_" + op.code).data || [];
                if (!!data) {
                    op.callback(data, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        load: function (op) { 
            if (op && op.code) {
                var strWhere = op.strWhere || "";
                Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetMapUseCache', { code: op.code, strWhere: strWhere }, function (res) {
                    if (!res) {
                        clientAsyncData.sourceDataItem.states[op.code] = loadSate.fail;
                    } else {
                        storage.set("sourceData_" + op.code, res);
                        clientAsyncData.sourceDataItem.states[op.code] = loadSate.yes;
                    }
                });
            } 
        },
         
        find: function (key, keyId, data) { 
            var res = {};
            for (var i = 0, l = data.length; i < l; i++) {
                if (data[i][keyId] == key) {
                    res = data[i];
                    break;
                }
            }
            return res;
        },
        refresh: function (op) {
            clientAsyncData.sourceDataItem.load(op);
        }
    };


    //musen扩展结束

})(window.jQuery, top.Changjie);
