﻿/*
 * 日 期：2017.03.16
 * 描 述：excel 导入导出
 */
(function ($, Changjie) {
    "use strict";
    $(function () {
        function excelInit() {
            if (!!mkModule) {
                var openDailog = function (id, projectId, mainTableId, excelImportType) {
                    var url = top.$.rootUrl + '/SystemModule/ExcelImport/ImportForm?id=' + id;
                    if (projectId) {
                        url += "&projectId=" + projectId;
                    }
                    if (mainTableId) {
                        url += "&mainTableId=" + mainTableId;
                    }
                    if (excelImportType) {
                        url += "&excelImportType=" + excelImportType;
                    }
                    var text = $(this).text();
                    Changjie.layerForm({
                        id: 'ImportForm',
                        title: text,
                        url: url,
                        width: 600,
                        height: 400,
                        maxmin: true,
                        btn: null,
                        end: function () {
                            if (refreshGirdData) {
                                refreshGirdData();
                            }
                        }
                    });
                };
                // 导入
                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/ExcelImport/GetList', { moduleId: mkModule.F_ModuleId }, function (data) {

                    if (!!data && data.length > 0) {
                        var $layouttool = $('.mk-layout-tool-right');
                        var $btnGroup = $('<div class=" btn-group btn-group-sm"></div>');
                        var hasBtn = false;
                        $.each(data, function (id, item) {
                            if (!!mkModuleButtonList[item.F_ModuleBtnId]) {
                                hasBtn = true;
                                var $btn = $('<a id="' + item.F_ModuleBtnId + '" data-value="' + item.F_Id + '"  class="btn btn-default"><i class="fa fa-sign-in"></i>&nbsp;' + item.F_Name + '</a>')
                                if ($('a[data-value="' + item.F_Id + '"]').length > 0) return true;
                                $btn.on('click', function () {
                                    var id = $(this).attr('data-value');
                                    var projectId = request("projectId");
                                    var mainTableId = "";
                                    var excelImportType = item.ExcelImportType;
                                    if (typeof mainId == "undefined") {
                                        mainTableId = request('keyValue')
                                    }
                                    else {
                                        mainTableId = request('keyValue') || mainId;
                                    }
                                    if (!mainTableId) {
                                        mainTableId = $("#gridtable").jfGridKeyValue();
                                    }
                                    if (!mainTableId) {
                                        Changjie.layerConfirm('无导入主体目标，是否继续导入', function (res) {
                                            if (res) {
                                                openDailog(id, projectId, mainTableId, excelImportType);
                                            }
                                        });
                                    }
                                    else {
                                        openDailog(id, projectId, mainTableId, excelImportType);
                                    }

                                });
                                $btnGroup.append($btn);
                            }
                        });

                        $layouttool.append($btnGroup);
                    }
                });
                // 导出
                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/ExcelExport/GetList', { moduleId: mkModule.F_ModuleId }, function (data) {
                    if (!!data && data.length > 0) {
                        var $layouttool = $('.mk-layout-tool-right');
                        var $btnGroup = $('<div class=" btn-group btn-group-sm"></div>');
                        var hasBtn = false;

                        $.each(data, function (id, item) {
                            if (!!mkModuleButtonList[item.F_ModuleBtnId]) {
                                hasBtn = true;
                                var $btn = $('<a id="' + item.F_ModuleBtnId + '" class="btn btn-default"><i class="fa fa-sign-out"></i>&nbsp;' + item.F_Name + '</a>')
                                $btn[0].dfop = item;
                                $btn.on('click', function () {
                                    item = $btn[0].dfop;
                                    if (item.ExportType == 2) {
                                        //自定义导出
                                        var op = Changjie.frameTab.currentIframe().$('#' + item.F_GridId).jfGridGet('settingInfo');
                                        if (op && op.url) {
                                            Changjie.httpAsync('GET', op.url, { queryJson: op.param.queryJson, pagination: JSON.stringify({ "rows": 100000, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                                                if (data == null) {
                                                    //if (data.length <= 0 || data.rows.length <= 0) {
                                                    top.Changjie.alert.info("导出失败;当前无数据可导出");
                                                    return;
                                                    //}
                                                }
                                                var rowJson;
                                                var queryIds = '';
                                                if (data.rows != undefined) {
                                                    rowJson = JSON.stringify(data.rows);
                                                    $.each(data.rows, function (index, row) {
                                                        console.log(item.PrimaryTableID);
                                                        queryIds += ',' + row[item.PrimaryTableID];
                                                    });
                                                } else {
                                                    rowJson = JSON.stringify(data);
                                                    console.log("data" + rowJson);
                                                    $.each(data, function (index, itemdata) {
                                                        queryIds += ',' + itemdata;
                                                    });
                                                }
                                                if (queryIds.length > 0) {
                                                    queryIds = queryIds.substring(1);
                                                }

                                                //if (fildSetdata != "" && fildSetdata != null && fildSetdata != undefined) {
                                                //    var fildConfig = JSON.parse(fildSetdata);//后台获取当前页面配置数据，返回json数组
                                                //    //如果导出按钮有配置取数规则 
                                                //    var isExistSet = false;
                                                //    var fieldSetList = [];
                                                //    $.each(fildConfig, function (index, setItem) {
                                                //        var sfieldCode = setItem.fieldCode;//导出设置字段code
                                                //        var sfieldText = setItem.fieldText;//导出设置的字段名称 
                                                //        var newFieldName = sfieldCode + "_Name"; //添加一个对应的Name列
                                                //        //判断所选列中是否包含定义字段
                                                //        for (var i = 0; i < exportField.length; i++) {
                                                //            if (sfieldCode == exportField[i]) {
                                                //                //exportField.splice(i, 1);//移除当前列
                                                //                //exportField.push(newFieldName);
                                                //                //表头对应增加名称列
                                                //                //var sHead = JSON.parse("{\"label\":\"" + sfieldText + " \",\"name\":\"" + newFieldName + "\",\"width\":250,\"align\":\"left\",\"height\":28}");
                                                //                //strHeadData.push(sHead);
                                                //                exportField[i] = newFieldName;
                                                //                for (var h = 0; h < newHeadData.length; h++) {
                                                //                    if (sfieldCode == newHeadData[h].name) {
                                                //                        newHeadData[h].name = newFieldName;
                                                //                        break;
                                                //                    }
                                                //                }
                                                //                isExistSet = true;
                                                //                fieldSetList.push(setItem);
                                                //                break;
                                                //            }
                                                //        }
                                                //    });

                                                //    if (isExistSet) {
                                                //        //如果存在字段取值设置，给data添加对应主键的文本字段值
                                                //        var newRowJson = JSON.parse(rowJson);
                                                //        $.each(newRowJson, function (j, item) {
                                                //            $.each(fieldSetList, function (index, setItem) {
                                                //                var sfieldCode = setItem.fieldCode;//导出设置字段code
                                                //                var sdataType = setItem.dataType;//数据来源类型
                                                //                var sdataSourceCode = setItem.dataSourceCode;//数据源
                                                //                var sdataItemCode = setItem.dataItemCode;//数据字典  
                                                //                var sfieldKey = setItem.FieldKey;//查询字段名称  
                                                //                var sfieldValue = setItem.FieldValue;//绑定值字段 
                                                //                var newFieldName = sfieldCode + "_Name";
                                                //                var sKey = item[sfieldCode];
                                                //                switch (sdataType) {
                                                //                    case "sjy":
                                                //                        Changjie.clientdata.getAsync('sourceDataItem', {
                                                //                            code: sdataSourceCode,
                                                //                            key: sKey,
                                                //                            keyId: sfieldKey,
                                                //                            callback: function (_data) {
                                                //                                var dataValue = _data[sfieldValue];
                                                //                                newRowJson[j][newFieldName] = dataValue != undefined && dataValue != null ? dataValue : "";
                                                //                            }
                                                //                        });
                                                //                        break;
                                                //                    case "sjzd":
                                                //                        Changjie.clientdata.getAsync('dataItem', {
                                                //                            code: sdataItemCode,//字典分类编码
                                                //                            key: sKey,
                                                //                            callback: function (_data) {
                                                //                                var dataValue = _data.text;
                                                //                                newRowJson[j][newFieldName] = dataValue != undefined && dataValue != null ? dataValue : "";
                                                //                            }
                                                //                        });
                                                //                        break;
                                                //                }
                                                //            });
                                                //        });
                                                //        rowJson = JSON.stringify(newRowJson);
                                                //        columnJson = JSON.stringify(newHeadData);
                                                //    }
                                                //}

                                                //console.log(rowJson, data, 89)

                                                Changjie.download({
                                                    method: "POST",
                                                    url: '/ExcelExport/CustomExportExcel',
                                                    param: {
                                                        fileName: encodeURI(item.F_Name),
                                                        moduleId: mkModule.F_ModuleId,
                                                        queryIds: queryIds
                                                    }
                                                });
                                            })
                                        }
                                    } else if (item.ExportType == 3) {
                                       
                                        var selectedRow = Changjie.frameTab.currentIframe().$('#' + item.F_GridId).jfGridGet('rowdata');
                                        var excelExportName = Changjie.frameTab.currentIframe().$('#excelExportName').val();
                                        console.log("filename:" + excelExportName);
                                        var ids = "";
                                        selectedRow.forEach(function (row) {
                                            ids += "," + row.ID;
                                        });
                                        if (ids.length > 0) {
                                            ids = ids.substring(1);
                                        }
                                        //按模板导出
                                        if (ids.length > 0) {
                                            Changjie.download({
                                                method: "POST",
                                                url: '/ExcelExport/TemplateExportExcel',
                                                param: {
                                                    moduleId: mkModule.F_ModuleId,
                                                    queryIds: ids,
                                                    btnId: item.F_ModuleBtnId,
                                                    fileName: excelExportName
                                                }
                                            });
                                        } else {
                                            var op = Changjie.frameTab.currentIframe().$('#' + item.F_GridId).jfGridGet('settingInfo');
                                            if (op && op.url) {
                                                Changjie.httpAsync('GET', op.url, { queryJson: op.param.queryJson, pagination: JSON.stringify({ "rows": 100000, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                                                    if (data == null) {
                                                        //if (data.length <= 0 || data.rows.length <= 0) {
                                                        top.Changjie.alert.info("导出失败;当前无数据可导出");
                                                        return;
                                                        //}
                                                    }
                                                    var rowJson;
                                                    var queryIds = '';
                                                    if (data.rows != undefined) {
                                                        rowJson = JSON.stringify(data.rows);
                                                        $.each(data.rows, function (index, row) {
                                                            console.log(item.PrimaryTableID);
                                                            queryIds += ',' + row["ID"];
                                                        });
                                                    } else {
                                                        rowJson = JSON.stringify(data);
                                                        console.log("data" + rowJson);
                                                        $.each(data, function (index, itemdata) {
                                                            queryIds += ',' + itemdata;
                                                        });
                                                    }
                                                    if (queryIds.length > 0) {
                                                        queryIds = queryIds.substring(1);
                                                    }
                                                    Changjie.download({
                                                        method: "POST",
                                                        url: '/ExcelExport/TemplateExportExcel',
                                                        param: {
                                                            moduleId: mkModule.F_ModuleId,
                                                            queryIds: queryIds,
                                                            btnId: item.F_ModuleBtnId,
                                                            fileName: excelExportName
                                                        }
                                                    });
                                                });
                                            }
                                        }
                                    }
                                    else {
                                        Changjie.layerForm({
                                            id: "ExcelExportForm",
                                            title: '导出Excel数据',
                                            url: top.$.rootUrl + '/Utility/ExcelExportForm?gridId=' + item.F_GridId + '&filename=' + encodeURI(item.F_Name),
                                            width: 500,
                                            height: 380,
                                            callBack: function (id) {
                                                return top[id].acceptClick();
                                            },
                                            btn: ['导出Excel', '关闭']
                                        });
                                    }
                                });
                                $btnGroup.append($btn);
                            }
                        });
                        $layouttool.append($btnGroup);
                    }
                });
            }
            else {
                setTimeout(excelInit, 100);
            }
        }
        excelInit();
    });

})(window.jQuery, top.Changjie);
