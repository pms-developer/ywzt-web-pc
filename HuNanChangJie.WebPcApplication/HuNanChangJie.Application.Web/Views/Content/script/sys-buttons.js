﻿(function ($, Changjie) {
    "use strict";

    // 表单页面对象集合
    var formIframes = [];
    var formIframesData = {};
    var formIframesHave = {};

    $(function () {
        function generateButtons() {
            var buttons = $('div[changjie-authorize="yes"]');
            //税费核算
            buttons.append('<a id="piao" class="btn btn-default"><i class="fa fa-cc-paypal"></i>&nbsp;录入发票</a>');


            buttons.append('<a id="add" class="btn btn-default"><i class="fa fa-plus"></i>&nbsp;<span class="language">新增</span></a>');
            buttons.append('<a id="edit" class="btn btn-default"><i class="fa fa-pencil-square-o"></i>&nbsp;<span class="language">编辑</span></a>');
            buttons.append('<a id="myAudit" class="btn btn-default"><i class="fa fa-check-circle"></i>&nbsp;<span class="language">审核</span></a>');
            buttons.append('<a id="dyAccountConfig" class="btn btn-default"><i class="fa fa-pencil-square-o"></i>&nbsp;抖音账户设置</a>');
            buttons.append('<a id="delete" class="btn btn-default"><i class="fa fa-trash-o"></i>&nbsp;<span class="language">删除</span></a>');
            ///保证金支付等
            buttons.append('<a id="pay" class="btn btn-default"><i class="fa fa-cc-paypal"></i>&nbsp;付款</a>');

            ///供应商
            buttons.append('<a id="review" class="btn btn-default"><i class="fa fa-sticky-note-o"></i>&nbsp;评审</a>');

            ///证照管理
            buttons.append('<a id="yazheng" class="btn btn-default"><i class="fa fa-exchange"></i>&nbsp;押证</a>');
            buttons.append('<a id="jiezheng" class="btn btn-default"><i class="fa fa-exchange"></i>&nbsp;解证</a>');
            buttons.append('<a id="zuofei" class="btn btn-default"><i class="fa fa-trash"></i>&nbsp;作废</a>');
            buttons.append('<a id="huifu" class="btn btn-default"><i class="fa fa-reply"></i>&nbsp;恢复</a>');

            //证照借用管理
            buttons.append('<a id="revert" class="btn btn-default"><i class="fa fa-cc-paypal"></i>&nbsp;归还</a>');
           

            buttons.append('<a id="audit" class="btn btn-default"><i class="fa fa-check-circle"></i>&nbsp;审核</a>');
            buttons.append('<a id="unaudit" class="btn btn-default"><i class="fa fa-ban"></i>&nbsp;反审核</a>');
            buttons.append('<a id="send" class="btn btn-default"><i class="fa fa-group"></i>&nbsp;发送</a>');
            /*buttons.after('<div class=" btn-group btn-group-sm"><a id="revocation" class="btn btn-default"><i class="fa fa-reply"></i>&nbsp;撤回</a></div>')*/
            buttons.append('<a id="print" class="btn btn-default"><i class="fa fa-print"></i>&nbsp;打印</a>');

            $("#send").on("click", function () {

                var auditstatus = "";
                if (typeof auditStatus == "undefined") {
                    auditstatus = $("#gridtable").jfGridValue("auditstatus") || $("#gridtable").jfGridValue("AuditStatus");
                }
                else {
                    auditstatus = auditStatus;
                }
                var formId = request("formId");
                var formType = request("formType");

                if (!!auditstatus) {
                    if (auditstatus == "1") {
                        Changjie.alert.warning('当前记录正在审核中......');
                        return;
                    }
                    if (auditstatus == "2") {
                        Changjie.alert.warning('当前记录已经审核完毕。');
                        return;
                    }
                }
                var workflowId = "";
                if ($("#Workflow_ID").length == 1) {
                    workflowId = $("#Workflow_ID").val();
                }
                else {
                    workflowId = $("#gridtable").jfGridValue("Workflow_ID") || $("#gridtable").jfGridValue("workflow_id");
                }
                if (!!formId) {
                    var infoId = "";
                    if (typeof pageType == "undefined") {
                        infoId = $("#gridtable").jfGridKeyValue();
                    }
                    else {
                        infoId = keyValue;
                    }

                    if (!!infoId) {
                        var projectId = request("projectId");
                        var moduleId = request("moduleId");

                        var checkDataState = true;//页面数据检测
                        if (typeof checkDataEvent !== "undefined") {
                            checkDataState = checkDataEvent(infoId);
                        }
                        if (!checkDataState) return;

                        Changjie.layerForm({
                            id: formId,
                            title: '发起流程',
                            url: top.$.rootUrl + '/WorkFlowModule/WorkFlow/StartForm?formId=' + formId + '&projectId=' + projectId + '&moduleId=' + moduleId,
                            width: 650,
                            height: 450,
                            callBack: function (id) {
                                return top[id].acceptClick(function (formData, auditers) {
                                    createProcess();
                                    function createProcess() {
                                        // 发起流程
                                        var isNew = true;
                                        if (workflowId) {
                                            isNew = false;
                                        }
                                        Changjie.workflowApi.start({
                                            schemeId: formData.workflowInfo,
                                            workflowName: formData.workflowName,
                                            level: formData.Level,
                                            description: formData.description,
                                            formId: formId,
                                            infoId: infoId,
                                            projectId: projectId || formData.projectId,
                                            workflowId: workflowId || top.Changjie.newGuid(),
                                            isNew: isNew,
                                            callback: function (res, data) {
                                                if (res) {
                                                    if (typeof refreshGirdData != "undefined") {
                                                        refreshGirdData();
                                                    }
                                                    //Changjie.frameTab.close(formId);
                                                }
                                            }
                                        });
                                    }
                                });
                                if (typeof pageType == "undefined") {
                                    $("#gridtable").jfGridSet("refreshdata");
                                }
                                else {
                                    refreshGirdData();
                                }
                            }
                        });
                    }
                    //var table = $("#gridtable").jfGridBindTable();
                }
            });

            //审核按钮点击事件
            $("#audit").on("click", function () {

                //page.init();

                //Modified by Wangwei 09/22/2020
                $('#save').unbind("click");
                var formId = request("formId");
                var formType = request("formType");
                var infoId = "";
                var auditstatus = "";
                var abstract22 = "";
                if (typeof pageType == "undefined") {
                    infoId = $("#gridtable").jfGridKeyValue();
                }
                else {
                    infoId = keyValue;
                }

                if (typeof auditStatus == "undefined") {
                    auditstatus = $("#gridtable").jfGridValue("auditstatus") || $("#gridtable").jfGridValue("AuditStatus");

                    auditstatus = auditstatus || "";
                    auditstatus = auditstatus.replace(/\s/g, "");
                }
                else {
                    auditstatus = auditStatus;
                }
                if (!!infoId) {
                    if (auditstatus == "0" || auditstatus == "3" || auditstatus == "4" || auditstatus == null || auditstatus == "null" || auditstatus == "") {
                        Changjie.layerConfirm('是否直接审核该记录！', function (res) {
                            if (res) {
                                if (typeof auditPassEventBat !== "undefined") {
                                    auditPassEventBat(infoId);
                                }
                                else {
                                    var url = top.$.rootUrl + '/WorkflowModule/WfEngine/DirectlyAudit';
                                    var param = {
                                        formId: formId,
                                        infoId: infoId,
                                        formType: formType,
                                        auditType: "Audit",
                                    };




                                    //Modified by wangwei 09/24/2020 招标立项改成异步调用   此处有点问题 先注释 2020-09-25 
                                    //var data = top.Changjie.httpAsyncPost(url, param, RefreshBidForm);

                                    //审核完毕，刷新页面，也即刷新表单
                                    //refreshGirdData();
                                    //if (typeof auditPassEvent !== "undefined") {
                                    //    auditPassEvent(infoId);
                                    //}

                                    var data = top.Changjie.httpGet(url, param);
                                    refreshGirdData();
                                    if (typeof auditPassEvent !== "undefined") {
                                        auditPassEvent(infoId);
                                    }
                                }
                            }
                        });
                    }
                    else {
                        if (auditstatus == "1") {
                            Changjie.alert.warning('当前记录正在审核中......');
                        }
                        else {
                            Changjie.alert.warning('当前记录已经审核完毕.');
                        }
                    }
                }
            });


            //Modified by Wangwei 09/23/2020 解决审核和反审核时出现项目-投标管理-投标立项表单页面下拉框拉不上的bug   此处有点问题 先注释 2020-09-25 
            //function RefreshBidForm() {

            //    var url = window.location.href;
            //    //投标立项表单页面Url
            //    var projectBidUrl = "/ProjectModule/ProjectBid/Form";
            //    if (null != url && "undefined" != url && url.indexOf(projectBidUrl)) {

            //        window.location.reload();
            //    }
            //    refreshGirdData();
            //    if (typeof auditPassEvent !== "undefined") {
            //       auditPassEvent(infoId);
            //    }
            //    if (typeof unauditPassEvent !== "undefined") {
            //       unauditPassEvent(infoId);
            //                        } 



            //}

            //反审核按钮点击事件
            $("#unaudit").on("click", function () {
                //Modified by Wangwei 09/22/2020
                $('#save').unbind("click");
                var formId = request("formId");
                //alert(formId);

                var formType = request("formType");
                var infoId = "";
                var auditstatus = "";
                if (typeof pageType == "undefined") {
                    infoId = $("#gridtable").jfGridKeyValue();
                }
                else {
                    infoId = keyValue;
                }
                if (typeof auditStatus == "undefined") {
                    auditstatus = $("#gridtable").jfGridValue("auditstatus") || $("#gridtable").jfGridValue("AuditStatus");
                }
                else {
                    auditstatus = auditStatus;
                }
                if (!!infoId) {

                    if (auditstatus == "2") {
                        Changjie.layerConfirm('是否反审核该记录！', function (res) {
                            if (res) {
                                if (typeof unauditPassEventBat !== "undefined") {
                                    unauditPassEventBat(infoId);
                                }
                                else {
                                    var url = top.$.rootUrl + '/WorkflowModule/WfEngine/DirectlyAudit';
                                    var param = {
                                        formId: formId,
                                        infoId: infoId,
                                        formType: formType,
                                        auditType: "Reverse",
                                    };





                                    //Modified by wangwei 09/24/2020 招标立项改成异步调用   此处有点问题 先注释 2020-09-25 
                                    //var data = top.Changjie.httpAsyncPost(url, param, RefreshBidForm);
                                    //refreshGirdData();
                                    //if (typeof unauditPassEvent !== "undefined") {
                                    //    unauditPassEvent(infoId);
                                    //}                              

                                    var data = top.Changjie.httpGet(url, param);
                                    refreshGirdData();

                                    if (typeof unauditPassEvent !== "undefined") {
                                        unauditPassEvent(infoId);
                                    }
                                }
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning('当前记录未审核完成,无法进行此操作.');
                    }
                }

            });
            $("#revocation").on("click", function () {
                var auditstatus = "";
                if (typeof auditStatus == "undefined") {
                    auditstatus = $("#gridtable").jfGridValue("auditstatus") || $("#gridtable").jfGridValue("AuditStatus");
                    auditstatus = auditstatus || "";
                    auditstatus = auditstatus.replace(/\s/g, "");
                }
                else {
                    auditstatus = auditStatus;
                }
                var workflowId = "";
                if ($("#Workflow_ID").length == 1) {
                    workflowId = $("#Workflow_ID").val();
                }
                else {
                    workflowId = $("#gridtable").jfGridValue("Workflow_ID") || $("#gridtable").jfGridValue("workflow_id");
                }
                if (!!workflowId) {
                    if (auditstatus == "1") {
                        Changjie.layerConfirm('是否撤销当前审核该记录！', function (res) {
                            if (res) {
                                {
                                    var url = top.$.rootUrl + '/WorkflowModule/Workflow/Revocation';
                                    var param = {
                                        workflowId: workflowId
                                    };

                                    top.Changjie.httpAsyncPost(url, param, function (res) {
                                        if (res.code == 200) {
                                            refreshGirdData();
                                        }
                                    });
                                }

                            }
                        });
                    }
                    else {
                        Changjie.alert.warning('当前记录已经审核完毕，或未发起流程');
                    }
                }
                else {
                    Changjie.alert.warning('流程未发起');
                }

            });
            $("#delete").on("click", function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length > 0) {
                    deleteMsg();
                } else {
                    var auditstatus = $("#gridtable").jfGridValue("auditstatus") || $("#gridtable").jfGridValue("AuditStatus");
                    if (!!auditstatus) {
                        //auditstatus = auditstatus.replace(/\s/g, "");
                    }
                    if (auditstatus == "0" || auditstatus == "3" || auditstatus == "4" || auditstatus == null || auditstatus == "null" || auditstatus == "") {
                        deleteMsg();
                    }
                    else {
                        var infoId = $("#gridtable").jfGridKeyValue();
                        if (!!infoId) {
                            Changjie.alert.warning("审核中或已审核中的记录,不可删除！");
                        }
                        else {
                            Changjie.alert.warning("您没有选中任何数据项,请选中后再操作！");
                        }

                    }
                }
            });

            $("#edit").on("click", function () {
                var auditstatus = $("#gridtable").jfGridValue("auditstatus") || $("#gridtable").jfGridValue("AuditStatus");
                var title = "编辑";
                var viewState = 0;
                var isShowConfirmBtn = true;
                switch (auditstatus) {
                    case "0":
                    case "3":
                    case "4":
                        viewState = 0;
                        title = "编辑";
                        break;
                    case "1":
                    case "2":
                        viewState = 1;
                        title = "查看";
                        isShowConfirmBtn = false;
                        break;
                }
                editMsg(isShowConfirmBtn, title, viewState);
            });
            $("#print").on("click", function () {
                /*hiprint.init();
               var  hiprintTemplate = new hiprint.PrintTemplate({
                    paperFooter:340,paperHeader:10
               });
                var panel = hiprintTemplate.addPrintPanel();
                panel.paperNumberDisabled = true;
                panel.addPrintHtml({ options: { width: 180, height: 30, top: 20, left: 19, content: document.body.outerHTML } })

                hiprintTemplate.print();*/
                // window.print();

                var formId = request("formId");

                if (formId) {
                    var keyValue = $("#gridtable").jfGridKeyValue();
                    if (keyValue) {
                        var op = $("#gridtable").jfGridGet("settingInfo");

                        if (op && op.bindTable) {
                            Changjie.httpAsyncGet(top.$.rootUrl + "/PrintModule/Print/StatisticsPrintCount?table=" + op.bindTable + "&infoid=" + keyValue, function (res) {
                            });
                        }

                        Changjie.layerForm({
                            id: "printForm",
                            title: '打印',
                            url: top.$.rootUrl + '/PrintModule/Print/Preview?formId=' + formId + '&keyValue=' + keyValue,
                            width: 1000,
                            height: 800,
                           callback: null
                        });

                    }
                    else {
                        Changjie.alert.warning("请选择需要打印信息！");
                    }
                }
                else {
                    Changjie.alert.warning("未检测表单ID,请与系统管理员联系！");
                }
            });
        };


        generateButtons();
    });
})(window.jQuery, top.Changjie);
