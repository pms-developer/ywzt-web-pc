﻿(function (a, b) {
    a.mkworkflow = {
        render: function (f) {
            var k = f[0].dfop;
            f.addClass("mk-workflow");
            if (!k.isPreview) {
                var g = a('<div class="mk-workflow-tool" ></div>');
                g[0].dfop = k;
                g.append('<a  type="cursor" class="mk-workflow-btndown" id="' + k.id + '_btn_cursor" title="' + k.nodeRemarks.cursor + '" ><b class="ico_cursor" /></a>');
                g.append('<a  type="direct" class="mk-workflow-btn" id="' + k.id + '_btn_direct" title="' + k.nodeRemarks.direct + '" ><b class="ico_direct"/></a>');
                var m = k.toolBtns.length;
                if (m > 0) {
                    g.append("<span></span>");
                    for (var l = 0; l < m; ++l) {
                        var j = k.toolBtns[l];
                        g.append('<a  type="' + j + '" id="' + k.id + "_btn_" + j + '" class="mk-workflow-btn" title="' + k.nodeRemarks[j] + '" ><b class="ico_' + j + '"/></a>')
                    }
                }
                k.currentBtn = "cursor";
                g.on("click",
                    function (o) {
                        var i = a(this);
                        var n = i[0].dfop;
                        o = o || window.event;
                        var p;
                        switch (o.target.tagName) {
                            case "SPAN":
                                return false;
                            case "DIV":
                                return false;
                            case "B":
                                p = o.target.parentNode;
                                break;
                            case "A":
                                p = o.target
                        }
                        var q = a(p).attr("type");
                        a.mkworkflow.switchToolBtn(n, q);
                        return false
                    });
                f.append(g)
            } else {
                f.addClass("mk-workflow-preview")
            }
            f.append('<div class="mk-workflow-work"></div>');
            var h = a("<div class='mk-workflow-workinner' style='width:5000px;height:5000px'></div>").attr({
                unselectable: "on",
                onselectstart: "return false",
                onselect: "document.selection.empty()"
            });
            f.children(".mk-workflow-work").append(h);
            h[0].dfop = k;
            a.mkworkflow.initDraw(h, k);
            h.on("click", a.mkworkflow.clickWorkArea);
            a.mkworkflow.initNodeEvent(h);
            a.mkworkflow.initLineEvent(h);
            var c = a("<div class='mk-workflow-rsghost'></div>").attr({
                unselectable: "on",
                onselectstart: "return false",
                onselect: "document.selection.empty()"
            });
            f.append(c);
            var d = a('<div class="mk-workflow-linemover" style="display:none" ></div>');
            h.append(d);
            d.on("mousedown", {
                $workArea: h
            },
                function (n) {
                    if (n.button == 2) {
                        return false
                    }
                    var r = a(this);
                    r.css({
                        "background-color": "#000"
                    });
                    var i = n.data.$workArea;
                    var o = a.mkworkflow.mousePosition(n),
                        u = a.mkworkflow.getElCoordinate(i[0]);
                    var x, y;
                    x = o.x - u.left;
                    y = o.y - u.top;
                    var s = r.position();
                    var v = x - s.left,
                        w = y - s.top;
                    var q = false;
                    document.onmousemove = function (p) {
                        if (!p) {
                            p = window.event
                        }
                        var t = a.mkworkflow.mousePosition(p);
                        var z = r.position();
                        x = t.x - u.left;
                        y = t.y - u.top;
                        if (r.data("type") == "lr") {
                            x = x - v;
                            if (x < 0) {
                                x = 0
                            } else {
                                if (x > 5000) {
                                    x = 5000
                                }
                            }
                            r.css({
                                left: x + "px"
                            })
                        } else {
                            if (r.data("type") == "tb") {
                                y = y - w;
                                if (y < 0) {
                                    y = 0
                                } else {
                                    if (y > 5000) {
                                        y = 5000
                                    }
                                }
                                r.css({
                                    top: y + "px"
                                })
                            }
                        }
                        q = true
                    };
                    document.onmouseup = function (z) {
                        var A = r.data("tid");
                        var t = a("#" + A)[0].dfop;
                        if (q) {
                            var B = r.position();
                            if (r.data("type") == "lr") {
                                a.mkworkflow.setLineM(A, B.left + 3)
                            } else {
                                if (r.data("type") == "tb") {
                                    a.mkworkflow.setLineM(A, B.top + 3)
                                }
                            }
                        }
                        r.css({
                            "background-color": "transparent"
                        });
                        if (t.focusId == r.data("tid")) {
                            a.mkworkflow.focusItem(r.data("tid"))
                        }
                        document.onmousemove = null;
                        document.onmouseup = null
                    }
                });
            var e = a("<div class='mk-workflow-lineoper' style='display:none'><b class='lr'></b><b class='tb'></b><b class='sl'></b><b class='x'></b></div>");
            h.append(e);
            e.on("click",
                function (i) {
                    if (!i) {
                        i = window.event
                    }
                    if (i.target.tagName != "A" && i.target.tagName != "B") {
                        return
                    }
                    var n = a(this).data("tid");
                    var o = a(i.target).attr("class");
                    if (o == "x") {
                        a.mkworkflow.delLine(n);
                        this.style.display = "none"
                    } else {
                        a.mkworkflow.setLineType(n, o)
                    }
                })
        },
        switchToolBtn: function (e, f) {
            var d = a("#" + e.id + "_btn_" + e.currentBtn);
            var c = a("#" + e.id + "_btn_" + f);
            d.removeClass("mk-workflow-btndown");
            d.addClass("mk-workflow-btn");
            c.removeClass("mk-workflow-btn");
            c.addClass("mk-workflow-btndown");
            e.currentBtn = f
        },
        initDraw: function (d, f) {
            var c;
            var g;
            c = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            d.prepend(c);
            var e = document.createElementNS("http://www.w3.org/2000/svg", "defs");
            c.appendChild(e);
            e.appendChild(a.mkworkflow.getSvgMarker("arrow1", "gray"));
            e.appendChild(a.mkworkflow.getSvgMarker("arrow2", "#ff3300"));
            e.appendChild(a.mkworkflow.getSvgMarker("arrow3", "#225ee1"));
            c.id = "draw_" + f.id;
            c.style.width = "5000px";
            c.style.height = "5000px"
        },
        getSvgMarker: function (d, c) {
            var e = document.createElementNS("http://www.w3.org/2000/svg", "marker");
            e.setAttribute("id", d);
            e.setAttribute("viewBox", "0 0 6 6");
            e.setAttribute("refX", 5);
            e.setAttribute("refY", 3);
            e.setAttribute("markerUnits", "strokeWidth");
            e.setAttribute("markerWidth", 6);
            e.setAttribute("markerHeight", 6);
            e.setAttribute("orient", "auto");
            var f = document.createElementNS("http://www.w3.org/2000/svg", "path");
            f.setAttribute("d", "M 0 0 L 6 3 L 0 6 z");
            f.setAttribute("fill", c);
            f.setAttribute("stroke-width", 0);
            e.appendChild(f);
            return e
        },
        clickWorkArea: function (f) {
            var c = a(this);
            var d = c[0].dfop;
            if (!d.isPreview) {
                f = f || window.event;
                var l = d.currentBtn;
                if (l == "cursor") {
                    var k = a(f.target);
                    var i = k.prop("tagName");
                    if (i == "svg" || (i == "DIV" && k.prop("class").indexOf("mk-workflow-workinner") > -1)) {
                        a.mkworkflow.blurItem(d)
                    }
                    return
                } else {
                    if (l == "direct") {
                        return
                    }
                }
                var m, o;
                var g = a.mkworkflow.mousePosition(f),
                    k = a.mkworkflow.getElCoordinate(this);
                m = g.x - k.left;
                o = g.y - k.top;
                var j = d.nodeRemarks[l];
                var h = true;
                if (l == "startround") {
                    j = "开始";
                    if (d.hasStartround) {
                        b.alert.error("只能有一个开始节点");
                        return false
                    }
                }
                if (l == "endround") {
                    j = "结束";
                    if (d.hasEndround) {
                        b.alert.error("只能有一个结束节点");
                        return false
                    }
                }
                a.mkworkflow.addNode(c, d, {
                    id: b.newGuid(),
                    name: j,
                    left: m,
                    top: o,
                    type: l
                })
            }
        },
        blurItem: function (f) {
            if (f.focusId != "") {
                var c = a("#" + f.focusId);
                if (c.prop("tagName") == "DIV") {
                    c.removeClass("mk-workflow-nodefocus");
                    c.find(".mk-workflow-nodeclose").hide();
                    c.removeClass("mk-workflow-nodemark")
                } else {
                    var g = a.mkworkflow.getLine(f, f.focusId);
                    if (!g.marked) {
                        if (g.color == "2") {
                            c[0].childNodes[1].setAttribute("stroke", "#ff3300");
                            c[0].childNodes[1].setAttribute("marker-end", "url(#arrow2)")
                        } else {
                            c[0].childNodes[1].setAttribute("stroke", "gray");
                            c[0].childNodes[1].setAttribute("marker-end", "url(#arrow1)")
                        }
                    }
                    var e = a(".mk-workflow-lineoper");
                    var d = a(".mk-workflow-linemover");
                    d.hide().removeData("type").removeData("tid");
                    e.hide().removeData("tid")
                }
            }
            f.focusId = "";
            return true
        },
        focusItem: function (h) {
            var c = a("#" + h);
            if (c.length == 0) {
                return
            }
            c.removeClass("mk-workflow-nodemark");
            var f = c[0].dfop;
            if (!this.blurItem(f)) {
                return
            }
            if (c.prop("tagName") == "DIV") {
                c.addClass("mk-workflow-nodefocus");
                c.find(".mk-workflow-nodeclose").show()
            } else {
                c[0].childNodes[1].setAttribute("stroke", "#225ee1");
                c[0].childNodes[1].setAttribute("marker-end", "url(#arrow3)");
                var k, l, g, j;
                g = c.attr("from").split(",");
                j = c.attr("to").split(",");
                g[0] = parseInt(g[0], 10);
                g[1] = parseInt(g[1], 10);
                j[0] = parseInt(j[0], 10);
                j[1] = parseInt(j[1], 10);
                var i = a.mkworkflow.getLine(f, h);
                if (i.type == "lr") {
                    g[0] = i.M;
                    j[0] = g[0];
                    var d = a(".mk-workflow-linemover");
                    d.css({
                        width: "5px",
                        height: (j[1] - g[1]) * (j[1] > g[1] ? 1 : -1) + "px",
                        left: g[0] - 3 + "px",
                        top: (j[1] > g[1] ? g[1] : j[1]) + 1 + "px",
                        cursor: "e-resize",
                        display: "block"
                    }).data({
                        type: "lr",
                        tid: h
                    })
                } else {
                    if (i.type == "tb") {
                        g[1] = i.M;
                        j[1] = g[1];
                        var d = a(".mk-workflow-linemover");
                        d.css({
                            width: (j[0] - g[0]) * (j[0] > g[0] ? 1 : -1) + "px",
                            height: "5px",
                            left: (j[0] > g[0] ? g[0] : j[0]) + 1 + "px",
                            top: g[1] - 3 + "px",
                            cursor: "s-resize",
                            display: "block"
                        }).data({
                            type: "tb",
                            tid: h
                        })
                    }
                }
                k = (g[0] + j[0]) / 2 - 35;
                l = (g[1] + j[1]) / 2 + 6;
                var e = a(".mk-workflow-lineoper");
                e.css({
                    display: "block",
                    left: k + "px",
                    top: l + "px"
                }).data("tid", h)
            }
            f.focusId = h;
            a.mkworkflow.switchToolBtn(f, "cursor")
        },
        getElCoordinate: function (c) {
            var e = c.offsetTop;
            var d = c.offsetLeft;
            c = c.offsetParent;
            while (c) {
                e += c.offsetTop;
                d += c.offsetLeft;
                c = c.offsetParent
            }
            return {
                top: e,
                left: d
            }
        },
        mousePosition: function (c) {
            if (!c) {
                c = window.event
            }
            if (c.pageX || c.pageY) {
                return {
                    x: c.pageX,
                    y: c.pageY
                }
            }
            return {
                x: c.clientX + document.documentElement.scrollLeft - document.body.clientLeft,
                y: c.clientY + document.documentElement.scrollTop - document.body.clientTop
            }
        },
        addNode: function (d, e, h, f) {
            var g = h.type;
            var c;
            if (!h.width || h.width < 150) {
                h.width = 150
            }
            if (!h.height || h.height < 65) {
                h.height = 65
            }
            if (!h.top || h.top < 0) {
                h.top = 0
            }
            if (!h.left || h.left < 0) {
                h.left = 0
            }
            if (g == "conditionnode") {
                h.width = 160;
                h.height = 90;
                c = a('<div class="mk-workflow-node item-conditionnode" id="' + h.id + '" ><div class="mk-workflow-nodeico"></div><b class="ico_' + h.type + 'div"></b><div class="mk-workflow-nodetext">' + h.name + '</div><div class="mk-workflow-nodeassemble" ></div></div>')
            } else {
                if (g != "startround" && g != "endround") {
                    c = a('<div class="mk-workflow-node" id="' + h.id + '" ><div class="mk-workflow-nodeico"><b class="ico_' + h.type + '"></b></div><div class="mk-workflow-nodetext">' + h.name + '</div><div class="mk-workflow-nodeassemble" ></div></div>')
                } else {
                    h.width = 52;
                    h.height = 52;
                    if (g == "startround") {
                        h.name = "开始";
                        e.hasStartround = true
                    } else {
                        if (g == "endround") {
                            h.name = "结束";
                            e.hasEndround = true
                        }
                    }
                    c = a('<div class="mk-workflow-node item-' + g + '" id="' + h.id + '" ><div class="mk-workflow-nodeico"></div><div class="mk-workflow-nodetext">' + h.name + '</div><div class="mk-workflow-nodeassemble" ></div></div>')
                }
            }
            c.find(".mk-workflow-nodeassemble").append('<div class="mk-workflow-nodeclose"></div>');
            c.find(".mk-workflow-nodeassemble").append('<div class="mk-workflow-nodespot left"><div class="mk-workflow-nodespotc"></div></div>');
            c.find(".mk-workflow-nodeassemble").append('<div class="mk-workflow-nodespot top"><div class="mk-workflow-nodespotc"></div></div>');
            c.find(".mk-workflow-nodeassemble").append('<div class="mk-workflow-nodespot right"><div class="mk-workflow-nodespotc"></div></div>');
            c.find(".mk-workflow-nodeassemble").append('<div class="mk-workflow-nodespot bottom"><div class="mk-workflow-nodespotc"></div></div>');
            c.css({
                top: h.top + "px",
                left: h.left + "px",
                width: h.width + "px",
                height: h.height + "px"
            });
            if (h.state != undefined && (h.type == "startround" || h.type == "auditornode" || h.type == "stepnode" || h.type == "confluencenode" || h.type == "childwfnode")) {
                c.css({
                    "padding-left": "0",
                    color: "#fff"
                }).find(".mk-workflow-nodeico").remove();
                switch (h.state) {
                    case "0":
                        c.css({
                            background:
                                "#5bc0de",
                            border: "0"
                        });
                        break;
                    case "1":
                        c.css({
                            background:
                                "#5cb85c",
                            border: "0"
                        });
                        break;
                    case "2":
                        c.css({
                            background:
                                "#d9534f",
                            border: "0"
                        });
                        break;
                    case "3":
                        c.css({
                            background:
                                "#999",
                            border: "0"
                        });
                        break;
                    case "4":
                        c.css({
                            background:
                                "#f0ad4e",
                            border: "0"
                        });
                        break
                }
            }
            if (!f) {
                switch (h.type) {
                    case "startround":
                        h.wfForms = [];
                        break;
                    case "stepnode":
                        h.auditors = [];
                        h.wfForms = [];
                        h.btnlist = [];
                        break;
                    case "auditornode":
                        h.auditors = [];
                        h.wfForms = [];
                        break;
                    case "confluencenode":
                        break;
                    case "conditionnode":
                        h.conditions = [];
                        break;
                    case "childwfnode":
                        h.auditors = [];
                        break
                }
            }
            c[0].wfdata = h;
            c[0].dfop = e;
            d.append(c);
            e.node.push(h)
        },
        delNode: function (c, f) {
            var h = [];
            for (var d = 0,
                e = c.line.length; d < e; d++) {
                var g = c.line[d];
                if (g.from != f.id && g.to != f.id) {
                    h.push(g)
                } else {
                    a("#" + g.id).remove()
                }
            }
            a("#" + f.id).remove();
            c.line = h;
            c.node.splice(c.node.indexOf(f), 1);
            if (f.type == "startround") {
                c.hasStartround = false
            } else {
                if (f.type == "endround") {
                    c.hasEndround = false
                }
            }
            c.focusId = ""
        },
        moveNode: function (e, f, h) {
            if (f < 0) {
                f = 0
            }
            if (h < 0) {
                h = 0
            }
            var c = a("#" + e);
            c.css({
                left: f + "px",
                top: h + "px"
            });
            var g = c[0].wfdata;
            var d = c[0].dfop;
            g.left = f;
            g.top = h;
            this.resetLines(e, d)
        },
        updateNodeName: function (d, f) {
            var c = d.find("#" + f);
            var e = c[0].wfdata;
            c.find(".mk-workflow-nodetext").html(e.name)
        },
        initNodeEvent: function (c) {
            var d = c[0].dfop;
            c.delegate(".mk-workflow-node", "dblclick", {
                $workArea: c
            },
                function (i) {
                    var g = i.data.$workArea;
                    var h = g[0].dfop;
                    var f = a(this);
                    var j = f[0].wfdata;
                    h.openNode(j, h.node)
                });
            if (!d.isPreview) {
                c.delegate(".mk-workflow-node", "click",
                    function (f) {
                        a.mkworkflow.focusItem(this.id);
                        return false
                    });
                c.delegate(".mk-workflow-node", "contextmenu",
                    function (f) { });
                c.delegate(".mk-workflow-nodeico", "mousedown", {
                    $workArea: c
                },
                    function (j) {
                        var g = a(this).parents(".mk-workflow-node");
                        var i = g[0].dfop;
                        var o = g[0].wfdata;
                        j = j || window.event;
                        if (i.$nowType == "direct") {
                            return
                        }
                        var m = g.attr("id");
                        var h = j.data.$workArea;
                        a.mkworkflow.focusItem(m);
                        var k = a.mkworkflow.mousePosition(j),
                            p = a.mkworkflow.getElCoordinate(h[0]);
                        var f = a("#" + i.id).find(".mk-workflow-rsghost");
                        if (o.type == "endround" || o.type == "startround" || o.type == "conditionnode") {
                            f.css({
                                "padding-left": "0px"
                            })
                        } else {
                            f.css({
                                "padding-left": "48px"
                            })
                        }
                        g.children().clone().prependTo(f);
                        if (o.type == "conditionnode") {
                            f.find("b").css({
                                width: "100%",
                                height: "100%",
                                position: "absolute",
                                "z-index": "-1"
                            })
                        }
                        f.find(".mk-workflow-nodeclose").remove();
                        var s, u;
                        s = k.x - p.left;
                        u = k.y - p.top;
                        var q = s - o.left,
                            r = u - o.top;
                        var n = false;
                        var l = 1;
                        if (navigator.userAgent.indexOf("8.0") != -1) {
                            l = 0
                        }
                        document.onmousemove = function (t) {
                            if (!t) {
                                t = window.event
                            }
                            var v = a.mkworkflow.mousePosition(t);
                            if (s == v.x - q && u == v.y - r) {
                                return false
                            }
                            s = v.x - q;
                            u = v.y - r - 47;
                            if (n && f.css("display") == "none") {
                                f.css({
                                    display: "table",
                                    width: a("#" + m).css("width"),
                                    height: a("#" + m).css("height"),
                                    top: o.top + "px",
                                    left: o.left + p.left + "px",
                                    cursor: "move"
                                })
                            }
                            if (s < 60) {
                                s = 60
                            } else {
                                if (s + o.width > p.left + h.width()) {
                                    s = p.left + h.width() - o.width
                                }
                            }
                            if (u < 0) {
                                u = 0
                            } else {
                                if (u + o.height > p.top + h.height() - 47) {
                                    u = h.height() - o.height + p.top - 47
                                }
                            }
                            f.css({
                                left: s + "px",
                                top: u + "px"
                            });
                            n = true
                        };
                        document.onmouseup = function (t) {
                            if (n) {
                                a.mkworkflow.moveNode(m, s - p.left, u + 47 - p.top)
                            }
                            f.empty().hide();
                            document.onmousemove = null;
                            document.onmouseup = null
                        };
                        return false
                    });
                c.delegate(".mk-workflow-node", "mouseenter", {
                    $workArea: c
                },
                    function (g) {
                        var f = g.data.$workArea[0].dfop;
                        if (f.currentBtn != "direct") {
                            return
                        }
                        a(this).addClass("mk-workflow-nodemark")
                    });
                c.delegate(".mk-workflow-node", "mouseleave", {
                    $workArea: c
                },
                    function (g) {
                        var f = g.data.$workArea[0].dfop;
                        if (f.currentBtn != "direct") {
                            return
                        }
                        a(this).removeClass("mk-workflow-nodemark")
                    });
                c.delegate(".mk-workflow-nodespot", "mouseenter", {
                    $workArea: c
                },
                    function (g) {
                        var f = g.data.$workArea[0].dfop;
                        if (f.currentBtn != "direct") {
                            return
                        }
                        a(this).addClass("mk-workflow-nodespotmark")
                    });
                c.delegate(".mk-workflow-nodespot", "mouseleave", {
                    $workArea: c
                },
                    function (g) {
                        var f = g.data.$workArea[0].dfop;
                        if (f.currentBtn != "direct") {
                            return
                        }
                        a(this).removeClass("mk-workflow-nodespotmark")
                    });
                c.delegate(".mk-workflow-nodespot", "mousedown", {
                    $workArea: c
                },
                    function (j) {
                        var i = j.data.$workArea[0].dfop;
                        if (i.currentBtn != "direct") {
                            return
                        }
                        var h = a(this);
                        var g = h.parents(".mk-workflow-node");
                        var l = g[0].wfdata;
                        var n, o;
                        n = l.left;
                        o = l.top;
                        var m = "left";
                        if (h.hasClass("left")) {
                            m = "left";
                            o += l.height / 2
                        } else {
                            if (h.hasClass("top")) {
                                m = "top";
                                n += l.width / 2
                            } else {
                                if (h.hasClass("right")) {
                                    m = "right";
                                    n += l.width;
                                    o += l.height / 2
                                } else {
                                    if (h.hasClass("bottom")) {
                                        m = "bottom";
                                        n += l.width / 2;
                                        o += l.height
                                    }
                                }
                            }
                        }
                        j.data.$workArea.data("lineStart", {
                            x: n,
                            y: o,
                            id: l.id,
                            position: m
                        }).css("cursor", "crosshair");
                        var k = a.mkworkflow.drawLine("1", "workflow_tmp_line", [n, o], [n, o], true, true);
                        var f = a("#" + i.id).find("svg");
                        f.append(k)
                    });
                c.mousemove(function (h) {
                    var f = a(this);
                    var g = f[0].dfop;
                    if (g.currentBtn != "direct") {
                        return
                    }
                    var k = f.data("lineStart");
                    if (!k) {
                        return
                    }
                    var i = a.mkworkflow.mousePosition(h),
                        l = a.mkworkflow.getElCoordinate(this);
                    var m, n;
                    m = i.x - l.left;
                    n = i.y - l.top;
                    var j = document.getElementById("workflow_tmp_line");
                    j.childNodes[0].setAttribute("d", "M " + k.x + " " + k.y + " L " + m + " " + n);
                    j.childNodes[1].setAttribute("d", "M " + k.x + " " + k.y + " L " + m + " " + n);
                    if (j.childNodes[1].getAttribute("marker-end") == 'url("#arrow2")') {
                        j.childNodes[1].setAttribute("marker-end", "url(#arrow3)")
                    } else {
                        j.childNodes[1].setAttribute("marker-end", "url(#arrow3)")
                    }
                });
                c.mouseup(function (h) {
                    var f = a(this);
                    var g = f[0].dfop;
                    if (g.currentBtn != "direct") {
                        return
                    }
                    a(this).css("cursor", "auto").removeData("lineStart");
                    a("#workflow_tmp_line").remove()
                });
                c.delegate(".mk-workflow-nodespot", "mouseup", {
                    $workArea: c
                },
                    function (j) {
                        var h = j.data.$workArea;
                        var i = h[0].dfop;
                        if (i.currentBtn != "direct") {
                            return
                        }
                        var g = a(this);
                        var f = g.parents(".mk-workflow-node");
                        var l = f[0].wfdata;
                        var k = h.data("lineStart");
                        var m = "left";
                        if (g.hasClass("left")) {
                            m = "left"
                        } else {
                            if (g.hasClass("top")) {
                                m = "top"
                            } else {
                                if (g.hasClass("right")) {
                                    m = "right"
                                } else {
                                    if (g.hasClass("bottom")) {
                                        m = "bottom"
                                    }
                                }
                            }
                        }
                        if (k) {
                            a.mkworkflow.addLine(i, {
                                id: b.newGuid(),
                                from: k.id,
                                to: l.id,
                                sp: k.position,
                                ep: m,
                                name: ""
                            })
                        }
                    });
                c.delegate(".mk-workflow-nodeclose", "click",
                    function () {
                        var e = a(this).parents(".mk-workflow-node");
                        var g = e[0].wfdata;
                        var f = e[0].dfop;
                        a.mkworkflow.delNode(f, g);
                        return false
                    })
            }
        },
        initLineEvent: function (c) {
            var d = c[0].dfop;
            if (!d.isPreview) {
                c.delegate("g", "click",
                    function (f) {
                        a.mkworkflow.focusItem(this.id)
                    });
                c.delegate("g", "dblclick", {
                    $workArea: c
                },
                    function (h) {
                        var f = h.data.$workArea;
                        var g = f[0].dfop;
                        var i = a.mkworkflow.getLine(g, this.id);
                        var j = null;
                        a.each(g.node,
                            function (e, k) {
                                if (k.id == i.from) {
                                    j = k;
                                    return false
                                }
                            });
                        g.openLine(i, j)
                    })
            }
        },
        getLine: function (c, f) {
            for (var d = 0,
                e = c.line.length; d < e; d++) {
                if (f == c.line[d].id) {
                    return c.line[d]
                }
            }
        },
        getLineSpotXY: function (g, c, h) {
            var f;
            for (var d = 0,
                e = c.node.length; d < e; d++) {
                if (g == c.node[d].id) {
                    f = c.node[d];
                    break
                }
            }
            var j, k;
            j = f.left;
            k = f.top;
            switch (h) {
                case "left":
                    k += f.height / 2;
                    break;
                case "top":
                    j += f.width / 2;
                    break;
                case "right":
                    j += f.width;
                    k += f.height / 2;
                    break;
                case "bottom":
                    j += f.width / 2;
                    k += f.height;
                    break
            }
            return [j, k]
        },
        drawLine: function (c, h, l, f, j, e, d) {
            var i;
            i = document.createElementNS("http://www.w3.org/2000/svg", "g");
            var g = document.createElementNS("http://www.w3.org/2000/svg", "path");
            var k = document.createElementNS("http://www.w3.org/2000/svg", "path");
            if (h != "") {
                i.setAttribute("id", h)
            }
            i.setAttribute("from", l[0] + "," + l[1]);
            i.setAttribute("to", f[0] + "," + f[1]);
            g.setAttribute("visibility", "hidden");
            g.setAttribute("stroke-width", 9);
            g.setAttribute("fill", "none");
            g.setAttribute("stroke", "white");
            g.setAttribute("d", "M " + l[0] + " " + l[1] + " L " + f[0] + " " + f[1]);
            g.setAttribute("pointer-events", "stroke");
            k.setAttribute("d", "M " + l[0] + " " + l[1] + " L " + f[0] + " " + f[1]);
            k.setAttribute("stroke-width", 2);
            k.setAttribute("stroke-linecap", "round");
            k.setAttribute("fill", "none");
            if (e) {
                k.setAttribute("style", "stroke-dasharray:6,5")
            }
            if (j) {
                k.setAttribute("stroke", "#3498DB");
                k.setAttribute("marker-end", "url(#arrow3)")
            } else {
                if (c == "2") {
                    k.setAttribute("stroke", "#ff3300");
                    k.setAttribute("marker-end", "url(#arrow2)")
                } else {
                    k.setAttribute("stroke", "gray");
                    k.setAttribute("marker-end", "url(#arrow1)")
                }
            }
            i.appendChild(g);
            i.appendChild(k);
            i.style.cursor = "crosshair";
            if (h != "" && h != "workflow_tmp_line") {
                var m = document.createElementNS("http://www.w3.org/2000/svg", "text");
                i.appendChild(m);
                var n = (f[0] + l[0]) / 2;
                var o = (f[1] + l[1]) / 2;
                m.setAttribute("text-anchor", "middle");
                m.setAttribute("x", n);
                m.setAttribute("y", o - 5);
                i.style.cursor = "pointer";
                m.style.cursor = "text"
            }
            return i
        },
        drawPoly: function (c, f, l, g, h, d, i) {
            var k, m;
            k = document.createElementNS("http://www.w3.org/2000/svg", "g");
            var e = document.createElementNS("http://www.w3.org/2000/svg", "path");
            var j = document.createElementNS("http://www.w3.org/2000/svg", "path");
            if (f != "") {
                k.setAttribute("id", f)
            }
            k.setAttribute("from", l[0] + "," + l[1]);
            k.setAttribute("to", d[0] + "," + d[1]);
            e.setAttribute("visibility", "hidden");
            e.setAttribute("stroke-width", 9);
            e.setAttribute("fill", "none");
            e.setAttribute("stroke", "white");
            m = "M " + l[0] + " " + l[1];
            if (g[0] != l[0] || g[1] != l[1]) {
                m += " L " + g[0] + " " + g[1]
            }
            if (h[0] != d[0] || h[1] != d[1]) {
                m += " L " + h[0] + " " + h[1]
            }
            m += " L " + d[0] + " " + d[1];
            e.setAttribute("d", m);
            e.setAttribute("pointer-events", "stroke");
            j.setAttribute("d", m);
            j.setAttribute("stroke-width", 2);
            j.setAttribute("stroke-linecap", "round");
            j.setAttribute("fill", "none");
            if (i) {
                j.setAttribute("stroke", "#3498DB");
                j.setAttribute("marker-end", "url(#arrow3)")
            } else {
                if (c == "2") {
                    j.setAttribute("stroke", "#ff3300");
                    j.setAttribute("marker-end", "url(#arrow2)")
                } else {
                    j.setAttribute("stroke", "gray");
                    j.setAttribute("marker-end", "url(#arrow1)")
                }
            }
            k.appendChild(e);
            k.appendChild(j);
            var n = document.createElementNS("http://www.w3.org/2000/svg", "text");
            k.appendChild(n);
            var o = (h[0] + g[0]) / 2;
            var p = (h[1] + g[1]) / 2;
            n.setAttribute("text-anchor", "middle");
            n.setAttribute("x", o);
            n.setAttribute("y", p - 5);
            n.style.cursor = "text";
            k.style.cursor = "pointer";
            return k
        },
        calcPolyPoints: function (h, c, i, e) {
            var f = [],
                g = [],
                d;
            if (i == "lr") {
                var d = e || (h[0] + c[0]) / 2;
                f = [d, h[1]];
                g = [d, c[1]]
            } else {
                if (i == "tb") {
                    var d = e || (h[1] + c[1]) / 2;
                    f = [h[0], d];
                    g = [c[0], d]
                }
            }
            return {
                start: h,
                m1: f,
                m2: g,
                end: c
            }
        },
        addLine: function (f, k) {
            var e = false;
            a.each(f.node,
                function (i, l) {
                    if (l.id == k.from) {
                        if (l.type == "endround" || l.type == "auditornode") {
                            e = true
                        }
                        return false
                    }
                });
            if (e) {
                return
            }
            var d;
            if (k.from == k.to) {
                return
            }
            for (var h = 0,
                j = f.line.length; h < j; h++) {
                if ((k.from == f.line[h].from && k.to == f.line[h].to)) {
                    return
                }
            }
            var n = a.mkworkflow.getLineSpotXY(k.from, f, k.sp);
            var g = a.mkworkflow.getLineSpotXY(k.to, f, k.ep);
            k.name = k.name || "";
            k.color = k.color || "1";
            f.line.push(k);
            if (k.type && k.type != "sl") {
                var m = a.mkworkflow.calcPolyPoints(n, g, k.type, k.M);
                d = a.mkworkflow.drawPoly(k.color, k.id, m.start, m.m1, m.m2, m.end, k.mark)
            } else {
                k.type = "sl";
                d = a.mkworkflow.drawLine(k.color, k.id, n, g, k.mark)
            }
            var c = a("#" + f.id).find("svg");
            a(d)[0].dfop = f;
            if (k.name != "") {
                a(d).find("text").html(k.name)
            }
            c.append(d)
        },
        resetLines: function (n, e) {
            var d;
            for (var g = 0,
                h = e.line.length; g < h; g++) {
                var p = [];
                var f = [];
                var j = e.line[g];
                if (j.from == n || j.to == n) {
                    p = a.mkworkflow.getLineSpotXY(j.from, e, j.sp);
                    f = a.mkworkflow.getLineSpotXY(j.to, e, j.ep);
                    a("#" + j.id).remove();
                    if (j.type == "sl") {
                        d = a.mkworkflow.drawLine(j.color, j.id, p, f, j.mark)
                    } else {
                        var o = a.mkworkflow.calcPolyPoints(p, f, j.type, j.M);
                        d = a.mkworkflow.drawPoly(j.color, j.id, o.start, o.m1, o.m2, o.end, j.mark)
                    }
                    var c = a("#" + e.id).find("svg");
                    a(d)[0].dfop = e;
                    c.append(d);
                    var m = a(d).attr("id");
                    var k = a.mkworkflow.getLine(e, m);
                    a(d).find("text").html(k.name)
                }
            }
        },
        setLineType: function (i, k) {
            var d = a("#" + i);
            var f = d[0].dfop;
            var j = a.mkworkflow.getLine(f, i);
            if (!k || k == null || k == "" || k == j.type) {
                return false
            }
            var h = j.from;
            var n = j.to;
            j.type = k;
            var m = a.mkworkflow.getLineSpotXY(h, f, j.sp);
            var g = a.mkworkflow.getLineSpotXY(n, f, j.ep);
            var l;
            if (k != "sl") {
                var l = a.mkworkflow.calcPolyPoints(m, g, j.type, j.M);
                a.mkworkflow.setLineM(i, a.mkworkflow.getMValue(m, g, k), true)
            } else {
                delete j.M;
                var e = a(".mk-workflow-linemover");
                e.hide().removeData("type").removeData("tid");
                d.remove();
                d = a.mkworkflow.drawLine(j.color, j.id, m, g, j.mark);
                var c = a("#" + f.id).find("svg");
                a(d)[0].dfop = f;
                c.append(d);
                var j = a.mkworkflow.getLine(f, i);
                a(d).find("text").html(j.name)
            }
            if (f.focusId == i) {
                a.mkworkflow.focusItem(i)
            }
        },
        setLineM: function (h, j, k) {
            var e = a("#" + h)[0].dfop;
            var i = a.mkworkflow.getLine(e, h);
            if (!i || j < 0 || !i.type || i.type == "sl") {
                return false
            }
            var g = i.from;
            var n = i.to;
            i.M = j;
            var m = a.mkworkflow.getLineSpotXY(g, e, i.sp);
            var f = a.mkworkflow.getLineSpotXY(n, e, i.ep);
            var l = a.mkworkflow.calcPolyPoints(m, f, i.type, i.M);
            a("#" + h).remove();
            var d = a.mkworkflow.drawPoly(i.color, h, l.start, l.m1, l.m2, l.end, i.marked || e.focusId == h);
            var c = a("#" + e.id).find("svg");
            a(d)[0].dfop = e;
            c.append(d);
            a(d).find("text").html(i.name)
        },
        getMValue: function (e, c, d) {
            if (d == "lr") {
                return (e[0] + c[0]) / 2
            } else {
                if (d == "tb") {
                    return (e[1] + c[1]) / 2
                }
            }
        },
        delLine: function (g) {
            var c = a("#" + g);
            var d = c[0].dfop;
            for (var e = 0,
                f = d.line.length; e < f; e++) {
                if (g == d.line[e].id) {
                    d.line.splice(e, 1);
                    break
                }
            }
            d.focusId = "";
            c.remove()
        },
        updateLineName: function (d, g) {
            var c = a("#" + g);
            var e = d[0].dfop;
            var f = a.mkworkflow.getLine(e, g);
            c.find("text").html(f.name);
            if (f.color == "2") {
                c[0].childNodes[1].setAttribute("stroke", "#ff3300");
                c[0].childNodes[1].setAttribute("marker-end", "url(#arrow2)")
            } else {
                c[0].childNodes[1].setAttribute("stroke", "gray");
                c[0].childNodes[1].setAttribute("marker-end", "url(#arrow1)")
            }
        }
    };
    a.fn.mkworkflow = function (e) {
        if (document.createElementNS == undefined) {
            return
        }
        var d = {
            openNode: function () { },
            openLine: function () { },
            toolBtns: ["startround", "endround", "stepnode", "confluencenode", "conditionnode", "auditornode", "childwfnode"],
            nodeRemarks: {
                cursor: "选择指针",
                direct: "步骤连线",
                startround: "开始节点",
                endround: "结束节点",
                stepnode: "普通节点",
                confluencenode: "会签节点",
                conditionnode: "条件判断节点",
                auditornode: "传阅节点",
                childwfnode: "子流程节点"
            },
            node: [],
            line: [],
            hasStartround: false,
            hasEndround: false,
            focusId: ""
        };
        a.extend(d, e);
        var c = a(this);
        d.id = c.attr("id");
        c[0].dfop = d;
        a.mkworkflow.render(c)
    };
    a.fn.mkworkflowGet = function () {
        if (document.createElementNS == undefined) {
            return
        }
        var c = a(this);
        var d = c.find(".mk-workflow-workinner");
        var f = d[0].dfop;
        var e = {
            nodes: f.node,
            lines: f.line
        };
        return e
    };
    a.fn.mkworkflowSet = function (j, m) {
        if (document.createElementNS == undefined) {
            return
        }
        var c = a(this);
        var d = c.find(".mk-workflow-workinner");
        switch (j) {
            case "updateNodeName":
                a.mkworkflow.updateNodeName(d, m.nodeId);
                break;
            case "updateLineName":
                a.mkworkflow.updateLineName(d, m.lineId);
                break;
            case "set":
                var e = d[0].dfop;
                for (var f = 0,
                    g = m.data.nodes.length; f < g; f++) {
                    var k = m.data.nodes[f];
                    a.mkworkflow.addNode(d, e, k, true)
                }
                for (var f = 0,
                    g = m.data.lines.length; f < g; f++) {
                    var h = m.data.lines[f];
                    a.mkworkflow.addLine(e, h)
                }
                break
        }
    }
})(jQuery, top.Changjie);
