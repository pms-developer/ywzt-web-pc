﻿/*
 * 
 *   
 * 创建人：
 * 日 期：2017.05.24
 * 描 述：mk-uploader 表单附件选择插件
 */
(function ($, Changjie) {
    "use strict";

    $.mkUploader = {
        init: function ($self) {
            
            var dfop = $self[0]._mkUploader.dfop;
            $.mkUploader.initRender($self, dfop);
        },
        initRender: function ($self, dfop) {
            $self.attr('type', 'mk-Uploader').addClass('mkUploader-wrap');
            var $wrap = $('<div class="mkUploader-input" ></div>');

            var $btnGroup = $('<div class="mkUploader-btn-group"></div>');
            var $uploadBtn = $('<a id="mkUploader_uploadBtn_' + dfop.id + '" class="btn btn-success mkUploader-input-btn">上传</a>');
            var $downBtn = $('<a id="mkUploader_downBtn_' + dfop.id + '" class="btn btn-danger mkUploader-input-btn">下载</a>');

            $self.append($wrap);

            $btnGroup.append($uploadBtn);
            $btnGroup.append($downBtn);
            $self.append($btnGroup);

            $uploadBtn.on('click', $.mkUploader.openUploadForm);
            $downBtn.on('click', $.mkUploader.openDownForm);
        },
        openUploadFormDialog: function (dfop) {
            var moduleType = dfop.moduleType || "Other";
            var moduleId = dfop.moduleId;
            var moduleInfoId = dfop.moduleInfoId;
            var folderName = dfop.folderName || "";
            Changjie.layerForm({
                id: dfop.id,
                title: dfop.placeholder,
                url: top.$.rootUrl + '/SystemModule/Annexes/UploadForm?keyVaule=' + dfop.value + "&extensions=" + dfop.extensions + '&moduleType=' + moduleType + '&moduleId=' + moduleId + '&moduleInfoId=' + moduleInfoId + '&folderName=' + folderName,
                width: 600,
                height: 400,
                maxmin: true,
                btn: null,
                end: function () {

                    Changjie.httpAsyncGet(top.$.rootUrl + '/SystemModule/Annexes/GetAnnexesFileList?infoId=' + dfop.value, function (res) {

                        if (res.code == Changjie.httpCode.success) {
                            for (var item in res.data) {
                                if (!res.data[item].F_DownloadCount) {
                                    res.data[item].F_DownloadCount = 0;
                                }
                                var tokb = parseInt(res.data[item].F_FileSize) / 1024;
                                res.data[item].F_FileSize = (Math.round(tokb * 100) / 100) + "KB";
                            }
                            $('#' + dfop.id).jfGridSet('refreshdata', res.data);
                        }
                    });
                    
                }
            });
        },
        openCustomDialog:function(dfop) {
            Changjie.layerForm({
                id: dfop.id,
                title: dfop.placeholder,
                url: top.$.rootUrl + '/ProjectModule/ProjectBidDocument/UploadDialog?projectId=' + dfop.value + "&extensions=" + dfop.extensions,
                width: 600,
                height: 400,
                maxmin: true,
                btn: null,
                end: function () {
                    Changjie.httpAsyncGet(top.$.rootUrl + '/ProjectModule/ProjectBidDocument/GetPageList?projectId=' + dfop.value, function (res) {

                        if (res.code == Changjie.httpCode.success) {
                            for (var item in res.data) {
                                if (!res.data[item].F_DownloadCount) {
                                    res.data[item].F_DownloadCount = 0;
                                }
                                var tokb = parseInt(res.data[item].F_FileSize) / 1024;
                                res.data[item].F_FileSize = (Math.round(tokb * 100) / 100) + "KB";
                                
                            }
                            $('#' + dfop.id).jfGridSet('refreshdata', res.data);
                        }
                    });

                }
            });
        },
        openUploadForm: function () {
            var $btn = $(this);
            var $self = $btn.parents('.mkUploader-wrap');
            var dfop = $self[0]._mkUploader.dfop;
            Changjie.layerForm({
                id: dfop.id,
                title: dfop.placeholder,
                url: top.$.rootUrl + '/SystemModule/Annexes/UploadForm?keyVaule=' + dfop.value + "&extensions=" + dfop.extensions,
                width: 600,
                height: 400,
                maxmin: true,
                btn: null,
                end: function () {
                    Changjie.httpAsyncGet(top.$.rootUrl + '/SystemModule/Annexes/GetFileNames?folderId=' + dfop.value, function (res) {
                        if (res.code == Changjie.httpCode.success) {
                            $('#' + dfop.id).find('.mkUploader-input').text(res.info);
                        }
                    });
                }
            });
        },
        openDownForm: function () {
            var $btn = $(this);
            var $self = $btn.parents('.mkUploader-wrap');
            var dfop = $self[0]._mkUploader.dfop;
            Changjie.layerForm({
                id: dfop.id,
                title: dfop.placeholder,
                url: top.$.rootUrl + '/SystemModule/Annexes/DownForm?keyVaule=' + dfop.value,
                width: 600,
                height: 400,
                maxmin: true,
                btn: null
            });
        }
    };

    $.fn.mkUploader = function (op) {

        var dfop = {
            placeholder: '上传附件',
            isUpload: true,
            isDown: true,
            extensions: ''
        }
        $.extend(dfop, op || {});
        if (!!op) {
            if (typeof op.isShowDefaultButton !== "undefined") {
                if (op.isShowDefaultButton == false) {
                    dfop.value = op.parentId;
                    if (op.isCustom) {
                        $.mkUploader.openCustomDialog(dfop);
                    }
                    else {
                        $.mkUploader.openUploadFormDialog(dfop);
                    }
                    return;
                }
            }
        }
        dfop.value = Changjie.newGuid();
        var $this = $(this);
        dfop.id = $this.attr('id');
        if (!!$this[0]._mkUploader) {
            return $this;
        }
        $this[0]._mkUploader = { dfop: dfop };
        $.mkUploader.init($this);
    };

    $.fn.mkUploaderSet = function (value) {
        var $self = $(this);
        var dfop = $self[0]._mkUploader.dfop;
        dfop.value = value;
        Changjie.httpAsyncGet(top.$.rootUrl + '/SystemModule/Annexes/GetFileNames?folderId=' + dfop.value, function (res) {
            if (res.code == Changjie.httpCode.success) {
                $('#' + dfop.id).find('.mkUploader-input').text(res.info);
            }
        });
    }

    $.fn.mkUploaderGet = function () {
        var $this = $(this);
        var dfop = $this[0]._mkUploader.dfop;
        return dfop.value;
    }
})(jQuery, top.Changjie);
