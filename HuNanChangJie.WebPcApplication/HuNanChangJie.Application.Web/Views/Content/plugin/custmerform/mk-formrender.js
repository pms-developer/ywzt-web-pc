﻿/*
 * 日 期：2017.03.22
 * 描 述：自定义表单渲染
 */



(function ($, Changjie) {
    "use strict";

    function getFontHtml(verify) {
        var res = "";
        switch (verify) {
            case "NotNull":
            case "Num":
            case "Float":
            case "Email":
            case "EnglishStr":
            case "Phone":
            case "Fax":
            case "Mobile":
            case "MobileOrPhone":
            case "Uri":
                res = '<font face="宋体">*</font>';
                break;
        }
        return res;
    }
    function getTdValidatorHtml(verify) {
        var res = "";
        if (verify != "") {
            res = 'isvalid="yes" checkexpession="' + verify + '"';
        }
        return res;

    }

    $.fn.mkCustmerFormRender = function (data, mainId, type, fileop) {
        var $this = $(this);
        var compontsMap = {};//字段映射信息
      

        var girdCompontMap = {};//子表映射信息
        var iLen = data.length;
 
        var $ul;
        var $container;
        if (iLen > 1) {
            var html = '<div class="mk-form-tabs" id="form_tabs">';
            html += '<ul class="nav nav-tabs"></ul></div>';
            html += '<div class="tab-content mk-tab-content" id="tab_content">';
            html += '</div>';
            $this.append(html);
            $('#form_tabs').mkFormTab(); //tab页切换
            $ul = $('#form_tabs ul');
            $container = $('#tab_content');
        }
        else {
            $container = $this;
        }

        $this[0].compontsMap = compontsMap;

        var calculate = [];//统计计算
         
        for (var i = 0; i < iLen; i++) {
            for (var j = 0, jLen = data[i].componts.length; j < jLen; j++) {
                var compont = data[i].componts[j];
                if (compont.type == "tabpage") {
                    var tabs = compont.tabs;
                    for (var tab  in tabs)
                    {
                        var fields = tabs[tab].fields;
                        for (var field in fields) {
                            if (fields[field].aggtype != 'normal' && !!fields[field].aggcolumn) {
                                calculate.push({ "aggtype": fields[field].aggtype, "aggcolumn": fields[field].aggcolumn });
                            }
                        }
                    }
                }
            }
        }


        for (var i = 0; i < iLen; i++) {
            var $content = $('<div class="mk-form-wrap"></div>');
            $container.append($content);

            var tabIndex = -1;
            

            for (var j = 0, jLen = data[i].componts.length; j < jLen; j++) {//渲染自定义元素
                var compont = data[i].componts[j];
                if (!!compont.table && !!compont.field) {//描述、文本信息
                    compontsMap[compont.table + compont.field.toLowerCase()] = compont.id;
                }
                var $row = $('<div class="col-xs-' + (12 / parseInt(compont.proportion)) + ' mk-form-item" ></div>');//每行显示元素个数
                var $title = $(' <div class="mk-form-item-title language">' + compont.title + getFontHtml(compont.verify) + '</div>');//元素数据验证类型
                if (compont.title != '') {
                    $row.append($title);
                }
                $content.append($row);
                var $compont;
                if (compont.type == "text")
                {
                    $compont = $.mkFormComponents[compont.type].renderTable(compont, $row, calculate, mainId,type);//渲染元素
                }
                else if (compont.type == "tabpage")
                {
                    $compont = $.mkFormComponents[compont.type].renderTable(compont, $row, mainId, type, fileop)
                }
                else {
                    
                     $compont = $.mkFormComponents[compont.type].renderTable(compont, $row,mainId,type);//渲染元素
                }
               
                if (!!$compont && !!compont.verify && compont.verify != "") {
                    
                    $compont.attr('isvalid', 'yes').attr('checkexpession', compont.verify);//添加验证信息
                    switch(compont.verify)
                    {
                        case "Num":
                        case "NumOrNull":
                            $compont.attr('controltype', 'int');
                            $compont.attr("onblur", "numToStr(this.value, this, 0)");
                            $compont.on("input propertychange", function (e) {
                                var val = $(this).val();
                                var setLastSele = e.target.selectionEnd;
                                var leveEnd = oldVal.length - targeSeleStat;
                                var val = val.numToChange();
                                $(this).val('').val(val);
                                if (val.length != setLastSele) {
                                    e.target.selectionStart = e.target.selectionEnd = val.length - leveEnd;
                                }
                            }) .blur(function (e) {
                                targeSeleStat = 0;
                                }).click(function (e) {
                                    targeSeleStat = e.target.selectionStart;
                                    oldVal = $(this).val();
                                });

                            break;
                        case "Float":
                        case "FloatOrNull":
                            $compont.attr('controltype', 'float');
                            $compont.attr("onblur", "numToStr(this.value, this, 2)");
                            $compont.on("input propertychange", function (e) {
                                var val = $(this).val();
                                var setLastSele = e.target.selectionEnd;
                               
                                var leveEnd = oldVal.length - targeSeleStat;
                                var val = val.numToChange();
                                $(this).val('').val(val);
                                if (val.length != setLastSele) {
                                    e.target.selectionStart = e.target.selectionEnd = val.length - leveEnd;
                                }
                            }).blur(function (e) {
                                targeSeleStat = 0;
                            }).click(function (e) {
                                targeSeleStat = e.target.selectionStart;
                                oldVal = $(this).val();
                            });

                            break;

                    }
                }
                
                if (compont.type == 'girdtable') {
                    girdCompontMap[compont.table] = compont;
                }
                if (compont.type == 'tabpage') {
                    for (var tab in compont.tabs) {
                        var tabitem = compont.tabs[tab];
                        girdCompontMap[tabitem.tablename] = tabitem.fields;
                    }
                }
            }
            
            if (iLen > 1) {// 如果大于一个选项卡，需要添加选项卡，否则不需要
                $ul.append('<li><a data-value="' + data[i].id + '">' + data[i].text + '</a></li>');
                $content.addClass('tab-pane').attr('id', data[i].id);
                if (i == 0) {
                    $ul.find('li').trigger('click');
                }
            }
        }

        $('.mk-form-wrap').mkscroll();//优化滚动条
      
        return girdCompontMap;
    };

    // 验证自定义表单数据
    $.mkValidCustmerform = function () {
        var validateflag = true;
       
        var validHelper = Changjie.validator;
        $('body').find("[isvalid=yes]").each(function () {
            var $this = $(this);
            if ($this.parent().find('.mk-field-error-info').length > 0) {
                validateflag = false;
                return true;
            }

            var checkexpession = $(this).attr("checkexpession");
            var checkfn = validHelper['is' + checkexpession];
           
            if (!checkexpession || !checkfn) { return false; }
            var errormsg = $(this).attr("errormsg") || "";
            var value;
            var type = $this.attr('type');
            if (type == 'mkselect') {
                value = $this.mkselectGet();
            }
            else if (type == 'formselect') {
                value = $this.mkformselectGet();
            }
            else {

                value = $this.val();
                if (value != null && value != undefined && value.length != 0 && value != "") {
                    var controltype = $this.attr("controltype") || "";
                    value = top.Changjie.clearNumSeparator(value, controltype);
                }

            }
            var r = { code: true, msg: '' };
            if (checkexpession == 'LenNum' || checkexpession == 'LenNumOrNull' || checkexpession == 'LenStr' || checkexpession == 'LenStrOrNull') {
                var len = $this.attr("length");
                
                r = checkfn(value, len);
            } else {
                r = checkfn(value);
            }
            if (!r.code) {
                validateflag = false;
                $.mkValidformMessage($this, errormsg + r.msg);
            }
        });
        return validateflag;
    }

    // 获取自定义表单数据
    $.fn.mkGetCustmerformData = function () {
        var resdata = {};
        $(this).find('input,select,textarea,.mk-select,.mk-formselect,.mkUploader-wrap,.jfgrid-layout,.edui-default').each(function (r) {
            var $self = $(this);
            var id = $self.attr('id') || $self.attr('name');
            if (!!id) {
                var type = $self.attr('type');
                switch (type) {
                    case "checkbox":
                        if ($self.is(":checked")) {
                            if (resdata[id] != undefined && resdata[id] != '') {
                                resdata[id] += ',';
                            }
                            else {
                                resdata[id] = '';
                            }
                            resdata[id] += $self.val();
                        }
                        break;
                    case "radio":
                        if ($self.is(":checked")) {
                            resdata[id] = $self.val();
                        }
                        break;
                    case "mkselect":
                        resdata[id] = $self.mkselectGet();
                        break;
                    case "formselect":
                        resdata[id] = $self.mkformselectGet();
                        break;
                    case "mk-Uploader":
                        resdata[id] = $self.mkUploaderGet();
                        break;
                    default:
                        
                        if ($self.hasClass('mk-currentInfo')) {
                            resdata[id] = $self[0].mkvalue;
                        }
                        else if ($self.hasClass('jfgrid-layout')) {
                            var _resdata = [];
                            var _resdataTmp = $self.jfGridGet('rowdatas');
                            for (var i = 0, l = _resdataTmp.length; i < l; i++) {
                                _resdata.push(_resdataTmp[i]);
                            }
                            resdata[id] = JSON.stringify(_resdata);
                        }
                        else if ($self.hasClass('edui-default')) {
                            if ($self[0].ue) {
                                resdata[id] = $self[0].ue.getContent(null, null, true);
                            }
                        }
                        else {
                           
                            var value = $self.val();
                            var controltype = $self.attr("controltype") || "";
                            if (!!controltype) {
                                value = top.Changjie.clearNumSeparator(value, controltype);
                            }
                            resdata[id] = $.trim(value);
                        }
                        break;
                }
            }
        });
        return resdata;
    }
    // 设置自定义表单数据
    $.fn.mkSetCustmerformData = function (data, tablename) {// 设置表单数据
        var compontsMap = $(this)[0].compontsMap;
        for (var field in data) {
            var value = data[field];
            var id = compontsMap[tablename + field];
            var $obj = $('#' + id);
            if (!$obj.length || $obj.length == 0) {
                var vs = (value + "").split(',');
                for (var i = 0, l = vs.length; i < l; i++) {
                    _setvalue(vs[i]);
                }

                function _setvalue(_value) {
                    var _$obj = $('input[name="' + id + '"][value="' + _value + '"]');
                    if (!!_$obj.length && _$obj.length > 0) {
                        if (!_$obj.is(":checked")) {
                            _$obj.trigger('click');
                        }
                    }
                    else {
                        setTimeout(function () {
                            _setvalue(_value);
                        }, 100);
                    }
                }
            }
            else {
                var type = $obj.attr('type');
                if ($obj.hasClass("mk-input-wdatepicker")) {
                    type = "datepicker";
                }
                switch (type) {
                    case "mkselect":
                        $obj.mkselectSet(value);
                        break;
                    case "formselect":
                        $obj.mkformselectSet(value);
                        break;
                    case "datepicker":
                        $obj.val(Changjie.formatDate(value, 'yyyy-MM-dd hh:mm:ss'));
                        break;
                    case "mk-Uploader":
                        $obj.mkUploaderSet(value);
                        break;
                    default:
                        if ($obj.hasClass('mk-currentInfo-user')) {
                            $obj[0].mkvalue = value;
                            $obj.val('');
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (item, op) {
                                    op.obj.val(item.name);
                                },
                                obj: $obj
                            });
                        }
                        else if ($obj.hasClass('mk-currentInfo-company')) {
                            $obj[0].mkvalue = value;
                            $obj.val('');
                            Changjie.clientdata.getAsync('company', {
                                key: value,
                                callback: function (_data, op) {
                                    op.obj.val(_data.name);
                                },
                                obj: $obj
                            });
                        }
                        else if ($obj.hasClass('mk-currentInfo-department')) {
                            $obj[0].mkvalue = value;
                            $obj.val('');
                            Changjie.clientdata.getAsync('department', {
                                key: value,
                                callback: function (item, op) {
                                    op.obj.val(item.name);
                                },
                                obj: $obj
                            });
                        }
                        else if ($obj.hasClass('mk-currentInfo-guid')) {
                            $obj[0].mkvalue = value;
                            $obj.val(value);
                        }
                        else if ($obj.hasClass('edui-default')) {
                            ueSet($obj[0].ue, value);
                            //$obj[0].ue.setContent(value);
                        }
                        else {
                            var controltype = $obj.attr("controltype") || "";
                           
                            if (controltype == "float" || controltype == "int") {
                                var decimalponit = 0;
                                decimalponit = 2;
                                if (value) {
                                    value = value.toString().numToString(decimalponit);
                                }
                            }
                            $obj.val(value);
                        }
                        break;
                }
            }
        }
    };

    function ueSet(ue, content) {
        ue.ready(function() {
            ue.setContent(content);
        });
    }

})(jQuery, top.Changjie);

String.prototype.thousandsToNumber = function (str) {
    return str.split(",").join("");
}


String.prototype.numToStr = function (num) {

    if (this.length == 0) return

    num = num ? num : 2
    var val = this.thousandsToNumber(this.toString())
    var IntegerPat, decimalPat
    decimalPat = '.' + parseFloat(val).toFixed(num).split('.')[1]
    IntegerPat = this.dealIntegerPat(val)

    return IntegerPat + decimalPat
}

String.prototype.numToStr2 = function (num) {

    if (this.length == 0) return

    num = num ? num : 2
    var val = this.thousandsToNumber(this.toString());
    return this.dealIntegerPat(val)

}



String.prototype.numToChange = function () {

    var val = this.thousandsToNumber(this.toString())
    var IntegerPat, decimalPat
    decimalPat = val.indexOf('.') != -1 ? '.' + val.split('.')[1] : ''
    IntegerPat = this.dealIntegerPat(val)

    return IntegerPat + decimalPat
}

String.prototype.numToString = function (num) {
    if (this.length == 0) return
    if (num == 0) {
        if (this.length == 0) return
        num = num ? num : 2
        var val = this.thousandsToNumber(this.toString());
        return this.dealIntegerPat(val)
    }
    else {
        num = num ? num : 2
        var val = this.thousandsToNumber(this.toString())
        var IntegerPat, decimalPat
        decimalPat = '.' + parseFloat(val).toFixed(num).split('.')[1]
        IntegerPat = this.dealIntegerPat(val)
        return IntegerPat + decimalPat
    }
}


String.prototype.dealIntegerPat = function (ret) {
    return (ret.indexOf('.') != -1 ? ret.split('.')[0] : ret).replace(/(\d{1,3})(?=(\d{3})+(?:$|\.))/g, '$1,')
}

function numToStr(val, ele, num) {
    if (num == "0") {
        $(ele).val(val.numToStr2(num))
    }
    else {
        $(ele).val(val.numToStr(num))
    }
}
var targeSeleStat, oldVal;
