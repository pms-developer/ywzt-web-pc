﻿/*
 * 日 期：2017.03.22
 * 描 述：自定义表单组件
 */

var $$tabNames = [];
(function ($, Changjie) {
    "use strict";

    var ruleCode;
    var mainFields = [{ "table": "", "fields": [] }];

    Date.prototype.DateAdd = function (strInterval, Number) {
        var dtTmp = this;
        switch (strInterval) {
            case 's': return new Date(Date.parse(dtTmp) + (1000 * Number));// 秒
            case 'n': return new Date(Date.parse(dtTmp) + (60000 * Number));// 分
            case 'h': return new Date(Date.parse(dtTmp) + (3600000 * Number));// 小时
            case 'd': return new Date(Date.parse(dtTmp) + (86400000 * Number));// 天
            case 'w': return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));// 星期
            case 'q': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());// 季度
            case 'm': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());// 月
            case 'y': return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());// 年
        }
    }
    Date.prototype.DateDiff = function (strInterval, dtEnd) {
        var dtStart = this;
        if (typeof dtEnd == 'string')//如果是字符串转换为日期型  
        {
            dtEnd = Changjie.parseDate(dtEnd);
        }
        switch (strInterval) {
            case 's': return parseInt((dtEnd - dtStart) / 1000);
            case 'n': return parseInt((dtEnd - dtStart) / 60000);
            case 'h': return parseInt((dtEnd - dtStart) / 3600000);
            case 'd': return parseInt((dtEnd - dtStart) / 86400000);
            case 'w': return parseInt((dtEnd - dtStart) / (86400000 * 7));
            case 'm': return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtStart.getFullYear()) * 12) - (dtStart.getMonth() + 1);
            case 'y': return dtEnd.getFullYear() - dtStart.getFullYear();
        }
    }

    var fieldMap = {};

    $.mkFormComponents = {
        label: {//文本标题
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="label" ><i  class="fa fa-font"></i>标 题</div>');
                return $html;
            },
            render: function ($component) {

                $component[0].dfop = $component[0].dfop || {
                    title: '标 题',
                    type: "label",
                    proportion: '1',
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: "标 题" }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;

                var $html = $(".mk-custmerform-designer-layout-right .mk-scroll-box");
                var _html = '';
                _html += '<div class="mk-component-title">组件标题</div>';
                _html += '<div class="mk-component-control"><input id="component_title" type="text" class="form-control" placeholder="必填项"/></div>';
                _html += '<div class="mk-component-control"><div id="component_verify"></div></div>';
                _html += '<div class="mk-component-title">所占行比例</div>';
                _html += '<div class="mk-component-control"><div id="component_proportion"></div></div>';
                $html.html(_html);

                // 标题设置
                $html.find('#component_title').val(dfop.title);
                $html.find('#component_title').on('input propertychange', function () {
                    var value = $(this).val();
                    dfop.title = value;
                    $component.find('.mk-compont-title span').text(value);
                });
                // 所在行占比
                $('#component_proportion').mkselect({
                    data: [
                        {
                            id: '1', text: '1'
                        },
                        {
                            id: '2', text: '1/2'
                        },
                        {
                            id: '3', text: '1/3'
                        },
                        {
                            id: '4', text: '1/4'
                        },
                        {
                            id: '5', text: '1/5'
                        },
                        {
                            id: '6', text: '1/6'
                        }
                    ],
                    placeholder: false,
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.proportion = item.id;
                        }
                        else {
                            dfop.proportion = '1';
                        }
                        $component.css({ 'width': 100 / parseInt(dfop.proportion) + '%' });
                    }
                });
                $('#component_proportion').mkselectSet(dfop.proportion);



            },
            renderTable: function (compont, $row) {
                $row.find('.mk-form-item-title').remove();
                $row.append("<span>" + compont.title + "<span>");
                $row.css({ 'padding': '0', 'line-height': '38px', 'text-align': 'center', 'font-size': '20px', 'font-weight': 'bold', 'color': '#000' });
            }
        },
        html: {//文本标题
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="html" ><i  class="fa fa-file-o"></i>富文本</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '',
                    type: "html",
                    proportion: '1',
                };
                $component.html(getComponentRowHtml({ name: "富文本", text: "富文本" }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;

                var $html = $(".mk-custmerform-designer-layout-right .mk-scroll-box");
                var _html = '';
                _html += '<div class="mk-component-title">文本内容</div>';
                _html += '<div class="mk-component-control"><textarea id="component_title" type="text" style="height:300px;" class="form-control" placeholder="必填项"></textarea></div>';
                _html += '<div class="mk-component-control"><div id="component_verify"></div></div>';
                _html += '<div class="mk-component-title">所占行比例</div>';
                _html += '<div class="mk-component-control"><div id="component_proportion"></div></div>';
                $html.html(_html);


                // 标题设置
                $html.find('#component_title').val(htmlDecode(dfop.title));
                $html.find('#component_title').on('input propertychange', function () {
                    var value = $(this).val();
                    dfop.title = htmlEncode(value);
                });
                // 所在行占比
                $('#component_proportion').mkselect({
                    data: [
                        {
                            id: '1', text: '1'
                        },
                        {
                            id: '2', text: '1/2'
                        },
                        {
                            id: '3', text: '1/3'
                        },
                        {
                            id: '4', text: '1/4'
                        },
                        {
                            id: '5', text: '1/5'
                        },
                        {
                            id: '6', text: '1/6'
                        }
                    ],
                    placeholder: false,
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.proportion = item.id;
                        }
                        else {
                            dfop.proportion = '1';
                        }
                        $component.css({ 'width': 100 / parseInt(dfop.proportion) + '%' });
                    }
                });
                $('#component_proportion').mkselectSet(dfop.proportion);
            },
            renderTable: function (compont, $row) {
                $row.find('.mk-form-item-title').remove();
                $row.html(htmlDecode(compont.title));
            }
        },
        text: {//文本框
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="text" ><i  class="fa fa-italic"></i>文本框</div>');
                return $html;
            },
            render: function ($component) {

                $component[0].dfop = $component[0].dfop || {
                    title: '文本框',
                    type: "text",
                    table: '',
                    field: "",
                    proportion: '1',
                    verify: '',
                    isReadOnly: '1',//是否可编辑
                    isHide: '0',  // 是否隐藏
                    dfvalue: '', // 默认值
                    formula: ''//取值公式
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: "文本框" }));
            },
            property: function ($component) {

                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component, verifyDatalist);
                var _html = '';
                _html += '<div class="mk-component-title">是否隐藏</div>';
                _html += '<div class="mk-component-control"><div id="component_isHide"></div></div>';
                _html += '<div class="mk-component-title">默认值<i title="仅在添加数据时默认填入" class="help fa fa-question-circle"></i></div>';
                _html += '<div class="mk-component-control"><input id="component_dfvalue" type="text" class="form-control" placeholder="无则不填"/></div>';

                _html += '<div class="mk-component-title">是否可编辑</div>';
                _html += '<div class="mk-component-control"><div id="component_isReadOnly"></div></div>';
                _html += '<div class="mk-component-title">取值公式</div>';
                _html += '<div class="mk-component-control"><input id="component_formula" type="text" class="form-control" placeholder="无则不填" /></div>';
                $html.append(_html);

                $('#component_isReadOnly').mkselect({
                    placeholder: false,
                    data: [{ id: '0', text: '否' }, { id: '1', text: '是' }],
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.isReadOnly = item.id;
                        }
                        else {
                            dfop.isReadOnly = '1';
                        }
                    }
                });
                $('#component_isReadOnly').mkselectSet(dfop.isReadOnly);
                $('#component_isHide').mkselect({
                    placeholder: false,
                    data: [{ id: '1', text: '否' }, { id: '0', text: '是' }],
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.isHide = item.id;
                        }
                        else {
                            dfop.isHide = '1';
                        }
                    }
                });
                $('#component_isHide').mkselectSet(dfop.isHide);

                $html.find('#component_dfvalue').val(dfop.dfvalue);
                $html.find('#component_dfvalue').on('input propertychange', function () {
                    var value = $(this).val();
                    dfop.dfvalue = value;
                });
                $html.find("#component_formula").val(dfop.formula);
                $html.find("#component_formula").on("input propertychange", function () {
                    dfop.formula = $(this).val();
                });
            },
            renderTable: function (compont, $row, calculate) {
                //if (compont.field == "Phone") {
                //}

                var calculateAtt = ""
                for (var item in calculate) {
                    if (calculate[item].aggcolumn == compont.field) {
                        calculateAtt = calculate[item].aggtype + "@" + calculate[item].aggcolumn;
                        break;
                    }
                }
                var $compont;
                if (calculateAtt == "") {
                    $compont = $('<input id="' + compont.id + '"  ' + (compont.isReadOnly == '0' ? "readonly" : "") + '  type="text" class="form-control" />');
                }
                else {
                    $compont = $('<input id="' + compont.id + '"  ' + (compont.isReadOnly == '0' ? "readonly" : "") + ' autocalculate="' + calculateAtt + '"  type="text" class="form-control" />');
                }
                if (!!compont.formula) {
                    $compont.attr("getvalueformula", compont.formula);
                    $compont.analysisFormula(compont.formula);
                }
                else {
                    $compont.val(compont.dfvalue);
                }
                $row.append($compont);
                if (compont.isHide == '1') {
                    $row.hide();
                }
                return $compont;
            },
            renderQuery: function (compont, $row) {
                var $compont = $('<input id="' + compont.id + '" type="text" class="form-control" />');
                $row.append($compont);
                return $compont;
            }
        },
        textarea: {//文本区
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="textarea" ><i class="fa fa-align-justify"></i>文本区</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '文本区',
                    type: "textarea",
                    table: '',
                    field: "",
                    proportion: '1',
                    verify: '',

                    height: '100',
                    dfvalue: '' // 默认值
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: "文本区" }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component, verifyDatalist2);
                var _html = '';
                _html += '<div class="mk-component-title">区域高度</div>';
                _html += '<div class="mk-component-control"><input id="component_height" type="text" class="form-control" /></div>';
                _html += '<div class="mk-component-title">默认值<i title="仅在添加数据时默认填入" class="help fa fa-question-circle"></i></div>';
                _html += '<div class="mk-component-control"><input id="component_dfvalue" type="text" class="form-control" placeholder="无则不填"/></div>';

                $html.append(_html);

                $html.find('#component_height').val(dfop.height);
                $html.find('#component_height').on('input propertychange', function () {
                    var value = $(this).val();
                    dfop.height = value;
                });

                $html.find('#component_dfvalue').val(dfop.dfvalue);
                $html.find('#component_dfvalue').on('input propertychange', function () {
                    var value = $(this).val();
                    dfop.dfvalue = value;
                });
            },
            renderTable: function (compont, $row) {//使用表单的时候渲染成table
                var $compont = $('<textarea id="' + compont.id + '"  class="form-control" ' + 'style="height: ' + compont.height + 'px;" />');
                $compont.val(compont.dfvalue);

                $row.append($compont);
                return $compont;
            },
            renderQuery: function (compont, $row) {
                var $compont = $('<input id="' + compont.id + '" type="text" class="form-control" />');
                $row.append($compont);
                return $compont;
            }
        },
        texteditor: {//编辑器
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="texteditor" ><i class="fa fa-edit"></i>编辑器</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '编辑器',
                    type: "texteditor",
                    table: '',
                    field: "",
                    proportion: '1',
                    verify: '',

                    height: '200',
                    dfvalue: '' // 默认值
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: "编辑器" }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component, verifyDatalist2);
                var _html = '';
                _html += '<div class="mk-component-title">区域高度</div>';
                _html += '<div class="mk-component-control"><input id="component_height" type="text" class="form-control" /></div>';
                _html += '<div class="mk-component-title">默认值<i title="仅在添加数据时默认填入" class="help fa fa-question-circle"></i></div>';
                _html += '<div class="mk-component-control"><input id="component_dfvalue" type="text" class="form-control" placeholder="无则不填"/></div>';

                $html.append(_html);

                $html.find('#component_height').val(dfop.height);
                $html.find('#component_height').on('input propertychange', function () {
                    var value = $(this).val();
                    dfop.height = value;
                });

                $html.find('#component_dfvalue').val(dfop.dfvalue);
                $html.find('#component_dfvalue').on('input propertychange', function () {
                    var value = $(this).val();
                    dfop.dfvalue = value;
                });
            },
            renderTable: function (compont, $row) {//使用表单的时候渲染成table
                var $compont = $('<div id="' + compont.id + '" type="text/plain" style="height: ' + compont.height + 'px;"></div>');
                $row.append($compont);
                $compont[0].ue = UE.getEditor(compont.id);
                if (compont.dfvalue != '') {
                    $compont[0].ue.ready(function () {
                        $compont[0].ue.setContent(compont.dfvalue);
                    });
                }
                return $compont;
            },
            renderQuery: function (compont, $row) {
                var $compont = $('<input id="' + compont.id + '" type="text" class="form-control" />');
                $row.append($compont);
                return $compont;
            }
        },
        radio: {//单选框
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="radio" ><i class="fa fa-circle-thin"></i>单选框</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '单选项',
                    type: "radio",
                    table: '',
                    field: "",
                    proportion: '1',

                    dataSource: '0',  // 0数据字典1数据源
                    dataSourceId: '',
                    itemCode: '',
                    dfvalue: ''       // 默认值
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: "单选项" }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component);
                var _html = '';
                _html += '<div class="mk-component-title">数据来源</div>';
                _html += '<div class="mk-component-control"><div id="component_dataSource"></div></div>';
                _html += '<div class="mk-component-title">数据选项</div>';
                _html += '<div class="mk-component-control" id="component_dataItemCode1"><div id="component_dataItemCode"></div></div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;" ><div id="component_dataSourceId"></div></div>';
                _html += '<div class="mk-component-title dataSourceId" style="display:none;">显示字段</div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;" ><div id="component_dataSourceId1"></div></div>';
                _html += '<div class="mk-component-title dataSourceId" style="display:none;">保存字段</div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;" ><div id="component_dataSourceId2"></div></div>';


                _html += '<div class="mk-component-title">默认值<i title="仅在添加数据时默认填入" class="help fa fa-question-circle"></i></div>';
                _html += '<div class="mk-component-control"><div id="component_dfvalue"></div></div>';

                $html.append(_html);
                setDatasource(dfop);
            },
            renderTable: function (compont, $row) {//使用表单的时候渲染成table
                var $compont = $('<div class="radio"></div>');
                /*获取数据字典或者数据源数据*/
                if (compont.dataSource == '0') {
                    Changjie.clientdata.getAllAsync('dataItem', {
                        code: compont.itemCode,
                        callback: function (dataes) {
                            $.each(dataes, function (id, item) {
                                var $point = $('<label><input name="' + compont.id + '" value="' + item.value + '"' + (compont.dfvalue == item.value ? "checked" : "") + ' type="radio">' + item.text + '</label>');
                                $compont.append($point);
                            });
                        }
                    });
                }
                else {
                    var vlist = compont.dataSourceId.split(',');
                    Changjie.clientdata.getAllAsync('sourceData', {
                        code: vlist[0],
                        callback: function (data) {
                            $.each(data, function (id, item) {
                                var $point = $('<label><input name="' + compont.id + '" value="' + item[vlist[2]] + '"' + (compont.dfvalue == item[vlist[2]] ? "checked" : "") + ' type="radio">' + item[vlist[1]] + '</label>');
                                $compont.append($point);
                            });
                        }
                    });
                }
                $row.append($compont);
                return $compont;
            },
            renderQuery: function (compont, $row) {
                var $compont = $('<div class="radio"></div>');
                /*获取数据字典或者数据源数据*/
                if (compont.dataSource == '0') {
                    Changjie.clientdata.getAllAsync('dataItem', {
                        code: compont.itemCode,
                        callback: function (dataes) {
                            $.each(dataes, function (id, item) {
                                var $point = $('<label><input name="' + compont.id + '" value="' + item.value + '"' + (compont.dfvalue == item.value ? "checked" : "") + ' type="radio">' + item.text + '</label>');
                                $compont.append($point);
                            });
                        }
                    });
                }
                else {
                    var vlist = compont.dataSourceId.split(',');
                    Changjie.clientdata.getAllAsync('sourceData', {
                        code: vlist[0],
                        callback: function (data) {
                            $.each(data, function (id, item) {
                                var $point = $('<label><input name="' + compont.id + '" value="' + item[vlist[2]] + '"' + (compont.dfvalue == item[vlist[2]] ? "checked" : "") + ' type="radio">' + item[vlist[1]] + '</label>');
                                $compont.append($point);
                            });
                        }
                    });
                }

                $row.append($compont);
                return $compont;
            }
        },
        checkbox: {//多选框
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="checkbox" ><i class="fa fa-square-o"></i>多选框</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '多选项',
                    type: "checkbox",
                    table: '',
                    field: "",
                    proportion: '1',

                    dataSource: '0',  // 0数据字典1数据源
                    dataSourceId: '',
                    itemCode: '',
                    dfvalue: ''       // 默认值
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: "多选项" }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component);
                var _html = '';
                _html += '<div class="mk-component-title">数据来源</div>';
                _html += '<div class="mk-component-control"><div id="component_dataSource"></div></div>';
                _html += '<div class="mk-component-title">数据选项</div>';
                _html += '<div class="mk-component-control" id="component_dataItemCode1"><div id="component_dataItemCode"></div></div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;" ><div id="component_dataSourceId"></div></div>';
                _html += '<div class="mk-component-title dataSourceId" style="display:none;">显示字段</div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;" ><div id="component_dataSourceId1"></div></div>';
                _html += '<div class="mk-component-title dataSourceId" style="display:none;">保存字段</div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;" ><div id="component_dataSourceId2"></div></div>';

                _html += '<div class="mk-component-title">默认值<i title="仅在添加数据时默认填入" class="help fa fa-question-circle"></i></div>';
                _html += '<div class="mk-component-control"><div id="component_dfvalue"></div></div>';

                $html.append(_html);
                setDatasource(dfop);
            },
            renderTable: function (compont, $row) {//使用表单的时候渲染成table
                var $compont = $('<div class="checkbox"></div>');
                /*获取数据字典或者数据源数据*/
                if (compont.dataSource == '0') {
                    Changjie.clientdata.getAllAsync('dataItem', {
                        code: compont.itemCode,
                        callback: function (dataes) {
                            $.each(dataes, function (id, item) {
                                var $point = $('<label><input name="' + compont.id + '" value="' + item.value + '"' + (compont.dfvalue == item.value ? "checked" : "") + ' type="checkbox">' + item.text + '</label>');
                                $compont.append($point);
                            });
                        }
                    });
                }
                else {
                    var vlist = compont.dataSourceId.split(',');
                    Changjie.clientdata.getAllAsync('sourceData', {
                        code: vlist[0],
                        callback: function (data) {
                            $.each(data, function (id, item) {
                                var $point = $('<label><input name="' + compont.id + '" value="' + item[vlist[2]] + '"' + (compont.dfvalue == item[vlist[2]] ? "checked" : "") + ' type="checkbox">' + item[vlist[1]] + '</label>');
                                $compont.append($point);
                            });
                        }
                    });
                }

                $row.append($compont);
                return $compont;
            },
            renderQuery: function (compont, $row) {
                var $compont = $('<div class="checkbox"></div>');
                /*获取数据字典或者数据源数据*/
                if (compont.dataSource == '0') {
                    Changjie.clientdata.getAllAsync('dataItem', {
                        code: compont.itemCode,
                        callback: function (dataes) {
                            $.each(data, function (id, item) {
                                var $point = $('<label><input name="' + compont.id + '" value="' + item.value + '"' + (compont.dfvalue == item.value ? "checked" : "") + ' type="checkbox">' + item.text + '</label>');
                                $compont.append($point);
                            });
                        }
                    });
                }
                else {
                    var vlist = compont.dataSourceId.split(',');
                    Changjie.clientdata.getAllAsync('sourceData', {
                        code: vlist[0],
                        callback: function (data) {
                            $.each(data, function (id, item) {
                                var $point = $('<label><input name="' + compont.id + '" value="' + item[vlist[2]] + '"' + (compont.dfvalue == item[vlist[2]] ? "checked" : "") + ' type="checkbox">' + item[vlist[1]] + '</label>');
                                $compont.append($point);
                            });
                        }
                    });
                }

                $row.append($compont);
                return $compont;
            }
        },
        select: {//下拉框
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="select" ><i class="fa fa-caret-square-o-right"></i>下拉框</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '下拉框',
                    type: "select",
                    table: '',
                    field: "",
                    proportion: '1',
                    verify: '',

                    dataSource: '0',  // 0数据字典1数据源
                    dataSourceId: '',
                    itemCode: '',
                    dfvalue: '',       // 默认值
                    whereSql:''
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: "下拉框" }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component, verifyDatalist2);
                var _html = '';
                _html += '<div class="mk-component-title">数据来源</div>';
                _html += '<div class="mk-component-control"><div id="component_dataSource"></div></div>';
                _html += '<div class="mk-component-title">数据选项</div>';
                _html += '<div class="mk-component-control" id="component_dataItemCode1"><div id="component_dataItemCode"></div></div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;" ><div id="component_dataSourceId"></div></div>';
                _html += '<div class="mk-component-title dataSourceId" style="display:none;">显示字段</div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;" ><div id="component_dataSourceId1"></div></div>';
                _html += '<div class="mk-component-title dataSourceId" style="display:none;">保存字段</div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;" ><div id="component_dataSourceId2"></div></div>';
                _html += '<div class="mk-component-title">默认值<i title="仅在添加数据时默认填入" class="help fa fa-question-circle"></i></div>';
                _html += '<div class="mk-component-control"><div id="component_dfvalue"></div></div>';

                _html += '<div class="mk-component-title dataSourceId" style="display:none;">过渡条件<i title="where后面的查询条件，如需当前项目做为过滤条件，则可填入:projectId=#currentProjectId#" class="help fa fa-question-circle"></i></div>';
                _html += '<div class="mk-component-control dataSourceId" style="display:none;"><input type="text" id="component_condition" class="form-control" placeholder="where后面的查询条件,如需当前项目做为过滤条件，则可填入:projectId=#currentProjectId#" /></div>';

                $html.append(_html);
                setDatasource(dfop);
                $("#component_condition").val(dfop.whereSql);
                $("#component_condition").on("input properchange", function () {
                    dfop.whereSql = $(this).val();
                })
            },
            renderTable: function (compont, $row) {//使用表单的时候渲染成table
                var $compont = $('<div id="' + compont.id + '"></div>');
                
                $row.append($compont);
                /*获取数据字典或者数据源数据*/
                if (compont.dataSource == '0') {
                    $compont.mkDataItemSelect({
                        allowSearch: true,
                        code: compont.itemCode
                    }).mkselectSet(compont.dfvalue);
                }
                else {
                    var vlist = compont.dataSourceId.split(',');
                    var strWhere = compont.whereSql || "";
                    if (strWhere.indexOf("#currentProjectId#") > -1) {
                        strWhere = strWhere.replace("#currentProjectId#", "'" + projectId + "'");
                    }
                   
                    $compont.mkDataSourceSelect({
                        allowSearch: true,
                        code: vlist[0],
                        value: vlist[2],
                        text: vlist[1],
                        strWhere: strWhere
                    }).mkselectSet(compont.dfvalue);
                }
                return $compont;
            },
            renderQuery: function (compont, $row) {
                var $compont = $('<div id="' + compont.id + '"></div>');
                $row.append($compont);

                /*获取数据字典或者数据源数据*/
                if (compont.dataSource == '0') {
                    $compont.mkDataItemSelect({
                        allowSearch: true,
                        code: compont.itemCode
                    }).mkselectSet(compont.dfvalue);
                }
                else {
                    var vlist = compont.dataSourceId.split(',');
                    $compont.mkDataSourceSelect({
                        allowSearch: true,
                        code: vlist[0],
                        value: vlist[2],
                        text: vlist[1]
                    }).mkselectSet(compont.dfvalue);
                }
                return $compont;
            }
        },
        datetime: {//日期框
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="datetime" ><i class="fa fa-calendar"></i>日期框</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '日期框',
                    type: "datetime",
                    table: '',
                    field: "",
                    proportion: '1',
                    verify: '',

                    dateformat: '0',
                    dfvalue: ''       // 默认值
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: '日期框' }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component, verifyDatalist2);
                var _html = '';
                _html += '<div class="mk-component-title">日期格式</div>';
                _html += '<div class="mk-component-control"><div id="component_dateformat"></div></div>';
                _html += '<div class="mk-component-title">默认值<i title="仅在添加数据时默认填入" class="help fa fa-question-circle"></i></div>';
                _html += '<div class="mk-component-control"><div id="component_dfvalue"></div></div>';

                $html.append(_html);
                $('#component_dateformat').mkselect({
                    data: [{ id: '0', text: '仅日期' }, { id: '1', text: '日期和时间' }],
                    value: 'id',
                    text: 'text',
                    placeholder: false,
                    select: function (item) {
                        dfop.dateformat = item.id;
                    }
                }).mkselectSet(dfop.dateformat);
                $('#component_dfvalue').mkselect({
                    data: [{ id: '0', text: '昨天' }, { id: '1', text: '今天' }, { id: '2', text: '明天' }],
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.dfvalue = item.id;
                        }
                        else {
                            dfop.dfvalue = '';
                        }
                    }
                }).mkselectSet(dfop.dfvalue);
            },
            renderTable: function (compont, $row) {//使用表单的时候渲染成table
                var dateformat = compont.dateformat == '0' ? 'yyyy-MM-dd' : 'yyyy-MM-dd HH:mm:ss';
                var datedefault = "";
                var datetime = new Date();
                switch (compont.dfvalue) {
                    case "0":
                        datedefault = datetime.DateAdd('d', -1);
                        break;
                    case "1":
                        datedefault = datetime.DateAdd('d', 0);
                        break;
                    case "2":
                        datedefault = datetime.DateAdd('d', 1);;
                        break;
                }
                datedefault = Changjie.formatDate(datedefault, dateformat.replace(/H/g, 'h'));
                var $compont = $('<input value="' + datedefault + '" id="' + compont.id + '" onClick="WdatePicker({dateFmt:\'' + dateformat + '\',qsEnabled:false,isShowClear:false,isShowOK:false,isShowToday:false,onpicked:function(){$(\'#' + compont.id + '\').trigger(\'change\');}})"  type="text" class="form-control mk-input-wdatepicker" autocomplete="off"/>');
                $row.append($compont);
                return $compont;
            }
        },
        datetimerange: {
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="datetimerange" ><i class="fa fa-calendar"></i>日期区间</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '日期区间',
                    type: "datetimerange",
                    table: '',
                    field: "",
                    proportion: '1',
                    verify: '',

                    startTime: '',
                    endTime: ''
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: '日期区间' }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component, verifyDatalist2);
                var _html = '';
                _html += '<div class="mk-component-title">开始日期</div>';
                _html += '<div class="mk-component-control"><div id="component_startTime"></div></div>';
                _html += '<div class="mk-component-title">结束日期</div>';
                _html += '<div class="mk-component-control"><div id="component_endTime"></div></div>';

                $html.append(_html);

                // 获取日期框的数据
                var datatimes = [];
                var $parent = $component.parent();
                $parent.find('.mk-compont-item').each(function () {
                    var compontItem = $(this)[0].dfop;
                    if (compontItem.type == 'datetime') {
                        var point = { id: compontItem.id, text: compontItem.title };
                        datatimes.push(point);
                    }
                });
                // 开始日期
                $('#component_startTime').mkselect({
                    data: datatimes,
                    select: function (item) {
                        if (!!item) {
                            dfop.startTime = item.id;
                        }
                    }
                }).mkselectSet(dfop.startTime);
                // 结束日期
                $('#component_endTime').mkselect({
                    data: datatimes,
                    select: function (item) {
                        if (!!item) {
                            dfop.endTime = item.id;
                        }
                    }
                }).mkselectSet(dfop.endTime);
            },
            renderTable: function (compont, $row) {
                var $compont = $('<input id="' + compont.id + '" type="text" class="form-control" />');
                function register() {
                    if ($('#' + compont.startTime).length > 0 && $('#' + compont.endTime).length > 0) {
                        $('#' + compont.startTime).on('change', function () {
                            var st = $(this).val();
                            var et = $('#' + compont.endTime).val();
                            if (!!st && !!et) {
                                var diff = Changjie.parseDate(st).DateDiff('d', et) + 1;
                                $compont.val(diff);
                            }
                        });
                        $('#' + compont.endTime).on('change', function () {
                            var st = $('#' + compont.startTime).val();
                            var et = $(this).val();
                            if (!!st && !!et) {
                                var diff = Changjie.parseDate(st).DateDiff('d', et) + 1;
                                $compont.val(diff);
                            }
                        });
                    }
                    else {
                        setTimeout(function () {
                            register();
                        }, 50);
                    }
                }
                if (!!compont.startTime && compont.endTime) {
                    register();
                }

                $row.append($compont);
                return $compont;
            },
            renderQuery: function (compont, $row) {
                var $compont = $('<input id="' + compont.id + '" type="text" class="form-control" />');
                $row.append($compont);
                return $compont;
            }
        },
        encode: {//单据编码
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="encode" ><i class="fa fa-barcode"></i>编 码</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '编 码',
                    type: "encode",
                    table: '',
                    field: "",
                    proportion: '1',

                    rulecode: '',
                    isHide: '0',  // 是否隐藏
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: "编 码" }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component);
                var _html = '';
                _html += '<div class="mk-component-title">单据规则</div>';
                _html += '<div class="mk-component-control"><div id="component_rulecode"></div></div>';

                $html.append(_html);

                $('#component_rulecode').mkselect({
                    value: 'F_EnCode',
                    text: 'F_FullName',
                    title: 'F_FullName',
                    allowSearch: true,
                    select: function (item) {
                        if (!!item) {
                            dfop.rulecode = item.F_EnCode;
                        }
                        else {
                            dfop.rulecode = '';
                        }
                    }
                }).mkselectSet(dfop.rulecode);

                if (!!ruleCode) {
                    $('#component_rulecode').mkselectRefresh({
                        data: ruleCode
                    });
                }
                else {
                    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetList', {}, function (data) {
                        ruleCode = data;
                        $('#component_rulecode').mkselectRefresh({
                            data: ruleCode
                        });
                    });
                }

            },
            renderTable: function (compont, $row) {//使用表单的时候渲染成table
                var $compont = $('<input id="' + compont.id + '" type="text" readonly class="form-control" />');
                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: compont.rulecode }, function (data) {
                    if (!$compont.val()) {
                        $compont.val(data);
                    }
                });

                $row.append($compont);
                return $compont;
            },
            renderQuery: function (compont, $row) {
                var $compont = $('<input id="' + compont.id + '" type="text" class="form-control" />');
                $row.append($compont);
                return $compont;
            }
        },
        organize: {//单位组织
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="organize" ><i  class="fa fa-coffee"></i>单位组织</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '单位组织',
                    type: "organize",
                    table: '',
                    field: "",
                    proportion: '1',

                    relation: "",
                    dataType: 'user'
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: '单位组织' }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component, verifyDatalist2);
                var _html = '';
                _html += '<div class="mk-component-title">类型选择</div>';
                _html += '<div class="mk-component-control"><div id="component_dataType"></div></div>';

                _html += '<div class="mk-component-title mkrelation">单位组织控件级联-上一级</div>';
                _html += '<div class="mk-component-control mkrelation"><div id="component_relation"></div></div>';

                $html.append(_html);

                /*上一级联*/
                var organizes = [];
                var $parent = $component.parent();
                $parent.find('.mk-compont-item').each(function () {
                    var compontItem = $(this)[0].dfop;
                    if (compontItem.type == 'organize' && compontItem.id != dfop.id) {
                        var point = { id: compontItem.id, text: compontItem.title, type: compontItem.dataType };
                        organizes.push(point);
                    }
                });
                $('#component_relation').mkselect({
                    data: organizes,
                    value: 'id',
                    text: 'text',
                    select: function (item, type) {
                        if (!!item) {
                            dfop.relation = item.id;
                        }
                        else if (type !== 'refresh') {
                            dfop.relation = '';
                        }
                    }
                })
                /*类型选择*/
                $('#component_dataType').mkselect({
                    data: [{ id: 'company', text: '公司' }, { id: 'department', text: '部门' }, { id: 'user', text: '人员' }],
                    value: 'id',
                    text: 'text',
                    placeholder: false,
                    select: function (item) {
                        dfop.dataType = item.id;
                        switch (dfop.dataType) {
                            case "user"://用户
                                var tmpData = [];
                                $.each(organizes, function (id, item) {
                                    if (item.type == 'department') {
                                        tmpData.push(item);
                                    }
                                });
                                $('#component_relation').mkselectRefresh({
                                    data: tmpData
                                }).mkselectSet(dfop.relation);
                                $('.mkrelation').show();
                                break;
                            case "department"://部门
                                var tmpData = [];
                                $.each(organizes, function (id, item) {
                                    if (item.type == 'company') {
                                        tmpData.push(item);
                                    }
                                });
                                $('#component_relation').mkselectRefresh({
                                    data: tmpData
                                }).mkselectSet(dfop.relation);

                                $('.mkrelation').show();
                                break;
                            case "company"://公司
                                $('.mkrelation').hide();
                                break;
                        }
                    }
                }).mkselectSet(dfop.dataType);
            },
            renderTable: function (compont, $row) {
                var $compont = $('<div id="' + compont.id + '" ></div>');
                $row.append($compont);
                switch (compont.dataType) {
                    case "user"://用户
                        if (compont.relation != "") {
                            $compont.mkselect({
                                value: 'F_UserId',
                                text: 'F_RealName',
                                title: 'F_RealName',
                                // 是否允许搜索
                                allowSearch: true
                            });
                            function register() {
                                if ($('#' + compont.relation).length > 0) {
                                    $('#' + compont.relation).on('change', function () {
                                        var value = $(this).mkselectGet();
                                        if (value == "") {
                                            $compont.mkselectRefresh({
                                                url: '',
                                                data: []
                                            });
                                        }
                                        else {
                                            $compont.mkselectRefresh({
                                                url: top.$.rootUrl + '/OrganizationModule/User/GetList',
                                                param: { departmentId: value }
                                            });
                                        }
                                    });
                                }
                                else {
                                    setTimeout(function () { register(); }, 100);
                                }
                            }
                            register();
                        }
                        else {
                            $compont.mkformselect({
                                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                                layerUrlW: 400,
                                layerUrlH: 300,
                                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
                            });
                        }
                        break;
                    case "department"://部门
                        $compont.mkselect({
                            type: 'tree',
                            // 是否允许搜索
                            allowSearch: true
                        });
                        if (compont.relation != "") {
                            function register() {
                                if ($('#' + compont.relation).length > 0) {
                                    $('#' + compont.relation).on('change', function () {
                                        var value = $(this).mkselectGet();
                                        $compont.mkselectRefresh({
                                            url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                                            param: { companyId: value }
                                        });
                                    });
                                }
                                else {
                                    setTimeout(function () { register(); }, 100);
                                }
                            }
                            register();
                        }
                        else {
                            $compont.mkselectRefresh({
                                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                                param: {}
                            });
                        }
                        break;
                    case "company"://公司
                        $compont.mkCompanySelect({});
                        break;
                }
                return $compont;
            },
            renderQuery: function (compont, $row) {
                var $compont = $('<div id="' + compont.id + '" ></div>');
                $row.append($compont);
                switch (compont.dataType) {
                    case "user"://用户
                        $compont.mkformselect({
                            layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                            layerUrlW: 400,
                            layerUrlH: 300,
                            dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
                        });
                        break;
                    case "department"://部门
                        $compont.mkselect({
                            type: 'tree',
                            // 是否允许搜索
                            allowSearch: true
                        });

                        $compont.mkselectRefresh({
                            url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                            param: {}
                        });
                        break;
                    case "company"://公司
                        $compont.mkCompanySelect({});
                        break;
                }
                return $compont;
            }
        },
        currentInfo: {//固定信息
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="currentInfo" ><i  class="fa fa-book"></i>当前信息</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '当前信息',
                    type: "currentInfo",
                    table: '',
                    field: "",
                    proportion: '1',

                    isHide: '0',  // 是否隐藏
                    dataType: 'user'
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: '当前信息' }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component);
                var _html = '';
                _html += '<div class="mk-component-title">是否隐藏</div>';
                _html += '<div class="mk-component-control"><div id="component_isHide"></div></div>';
                _html += '<div class="mk-component-title">类型选择</div>';
                _html += '<div class="mk-component-control"><div id="component_dataType"></div></div>';

                $html.append(_html);

                $('#component_isHide').mkselect({
                    placeholder: false,
                    data: [{ id: '0', text: '否' }, { id: '1', text: '是' }],
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.isHide = item.id;
                        }
                        else {
                            dfop.isHide = '0';
                        }
                    }
                });
                $('#component_isHide').mkselectSet(dfop.isHide);
                $('#component_dataType').mkselect({
                    data: [
                        { id: 'company', text: '当前公司' },
                        { id: 'department', text: '当前部门' },
                        { id: 'user', text: '当前用户' },
                        { id: 'time', text: '当前时间' },
                        { id: 'projectName', text: '当前项目' }
                    ],
                    value: 'id',
                    text: 'text',
                    placeholder: false,
                    select: function (item) {
                        dfop.dataType = item.id;
                    }
                }).mkselectSet(dfop.dataType);
            },
            renderTable: function (compont, $row) {
                var $compont = $('<input id="' + compont.id + '" readonly type="text"  class="form-control mk-currentInfo mk-currentInfo-' + compont.dataType + '" />');
                var loginInfo = Changjie.clientdata.get(['userinfo']);
                switch (compont.dataType) {
                    case 'company':
                        $compont[0].mkvalue = loginInfo.companyId;
                        Changjie.clientdata.getAsync('company', {
                            key: loginInfo.companyId,
                            callback: function (_data) {
                                $compont.val(_data.name);
                            }
                        });
                        break;
                    case 'department':
                        $compont[0].mkvalue = loginInfo.departmentId;
                        Changjie.clientdata.getAsync('department', {
                            key: loginInfo.departmentId,
                            callback: function (_data) {
                                $compont.val(_data.name);
                            }
                        });
                        break;
                    case 'user':
                        $compont.val(loginInfo.realName);
                        $compont[0].mkvalue = loginInfo.userId;
                        break;
                    case 'time':
                        $compont[0].mkvalue = Changjie.formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss');
                        $compont.val($compont[0].mkvalue);
                        break;
                    case 'projectName':
                        var projectId = request("projectId");
                        if (projectId) {
                            var url = top.$.rootUrl + '/ProjectModule/Project/GetFormData?keyValue=' + projectId;
                            top.Changjie.httpAsyncGet(url, function (data) {
                                if (data.code == 200) {
                                    $compont[0].mkvalue = data.data.ProjectName;
                                    $compont.val(data.data.ProjectName);
                                }
                            });
                        }
                        break;
                }
                if (compont.isHide == '1') {
                    $row.hide();
                }

                $row.append($compont);
                return $compont;
            }
        },
        guid: {//固定信息
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="guid" ><i  class="fa fa-info"></i>GUID/隐藏域</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: 'GUID/隐藏域',
                    type: "guid",
                    table: '',
                    field: "",
                    proportion: '1',
                    isAutoGuid: '1',
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: 'GUID/隐藏域', readonly: $component[0].dfop.readonly }));
            },
            property: function ($component) {
                var dfop = $component[0].dfop;
                var $html = setComponentPropertyHtml($component);
                var _html = '<div class="mk-component-title">是否自动填充GUID</div>';
                _html += '<div class="mk-component-control"><div id="component_autoguid" ' + (dfop.readonly == true ? "readonly" : "") + '></div></div>';
                $html.append(_html);
                $('#component_autoguid').mkselect({
                    data: [
                        {
                            id: '0', text: '否'
                        },
                        {
                            id: '1', text: '是'
                        }
                    ],
                    placeholder: false,
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.isAutoGuid = item.id;
                        }
                        else {
                            dfop.isAutoGuid = '1';
                        }
                    }
                });
                $('#component_autoguid').mkselectSet(dfop.isAutoGuid);
            },
            renderTable: function (compont, $row) {
                var $compont = $('<input id="' + compont.id + '" readonly type="text"  class="form-control mk-currentInfo mk-currentInfo-guid" />');
                if (compont.isAutoGuid == "1") {
                    $compont[0].mkvalue = Changjie.newGuid();
                }
                else {
                    $compont[0].mkvalue = "";
                }
                $compont.val($compont[0].mkvalue);
                $row.hide();
                $row.append($compont);
                return $compont;
            }
        },
        upload: {//附件框
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="upload" ><i class="fa fa-paperclip"></i>附件上传</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '附件上传',
                    type: "upload",
                    table: '',
                    field: "",
                    proportion: '1',
                    verify: ''
                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: '附件上传' }));
            },
            property: function ($component) {
                setComponentPropertyHtml($component);
            },
            renderTable: function (compont, $row) {
                var $compont = $('<div id="' + compont.id + '"></div>');
                $row.append($compont);
                $compont.mkUploader();

                return $compont;
            }
        },
        girdtable: {
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="girdtable" ><i class="fa fa-table"></i>编辑表格</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '编辑表格',
                    table: '',
                    type: "girdtable",
                    proportion: '1',
                    verify: '',

                    minheight: 200,  // 表格默认的行数
                    fieldsData: [],  // 设置的字段数据

                    preloadDb: '',
                    preloadTable: '',
                    IsShowTabPage: '1',//是否以Tab页显示
                };

                $component.html(getComponentRowHtml({ name: '编辑表格', text: '编辑表格' }));

            },
            property: function ($component) {

                var designerDfop = $component.parent()[0].dfop;
                var dfop = $component[0].dfop;


                var tables = [];
                for (var item in designerDfop.dbTable) {
                    if (designerDfop.dbTable[item].isMain != "1") {
                        tables.push(designerDfop.dbTable[item]);
                    }
                }
                var $html = $(".mk-custmerform-designer-layout-right .mk-scroll-box");
                var _html = '<div class="mk-component-title">绑定表1</div>';
                _html += '<div class="mk-component-control"><div id="component_table"></div></div>';
                _html += '<div class="mk-component-title">组件标题</div>';
                _html += '<div class="mk-component-control"><input id="component_title" type="text" class="form-control" placeholder="必填项"/></div>';
                _html += '<div class="mk-component-title">字段验证</div>';
                _html += '<div class="mk-component-control"><div id="component_verify"></div></div>';
                _html += '<div class="mk-component-title">所占行比例</div>';
                _html += '<div class="mk-component-control"><div id="component_proportion"></div></div>';
                _html += '<div class="mk-component-title">表格高度</div>';
                _html += '<div class="mk-component-control"><input id="component_minheight" type="text" class="form-control" placeholder="必填项"/></div>';

                _html += '<div class="mk-component-title">是否在Tab页中展示</div>';
                _html += '<div class="mk-component-control"><div id="show_tabpage"></div></div>';


                _html += '<div class="mk-component-title">预加载数据库</div>';
                _html += '<div class="mk-component-control"><div id="component_ydb"></div></div>';
                _html += '<div class="mk-component-title">预加载数据表</div>';
                _html += '<div class="mk-component-control"><div id="component_ytable"></div></div>';

                _html += '<div class="mk-component-control" style="text-align: center;padding-top: 10px;" ><a id="fieldsData_set" class="btn btn-primary btn-block">设置表格字段</a></div>';

                $html.html(_html);
                $('#component_table').mkselect({
                    data: tables,
                    value: 'name',
                    text: 'name',
                    allowSearch: true,
                    select: function (item) {
                        if (!!item) {
                            dfop.table = item.name;
                        }
                        else {
                            dfop.table = '';
                        }
                    }
                });
                if (!!tables && tables.length > 0) {
                    $('#component_table').mkselectSet(dfop.table || tables[0].name);
                }
                // 标题设置
                $html.find('#component_title').val(dfop.title);
                $html.find('#component_title').on('input propertychange', function () {
                    var value = $(this).val();
                    $component.find('.mk-compont-title span').text(value);
                    dfop.title = value;
                });
                // 表格高度

                $html.find('#component_minheight').val(dfop.minheight);
                $html.find('#component_minheight').on('input propertychange', function () {
                    var value = $(this).val();
                    dfop.minheight = value;
                });
                // 字段验证
                $('#component_verify').mkselect({
                    data: verifyDatalist2,
                    value: 'id',
                    text: 'text',
                    allowSearch: true,
                    select: function (item) {
                        if (!!item) {
                            dfop.verify = item.id;
                        }
                        else {
                            dfop.verify = "";
                        }
                    }
                });

                $('#show_tabpage').mkselect({
                    data: [
                        {
                            id: '1', text: '是'
                        },
                        {
                            id: '0', text: '否'
                        }
                    ],
                    placeholder: false,
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.IsShowTabPage = item.id;
                        } else {
                            dfop.IsShowTabPage = '1';
                        }
                    }
                });
                $('#show_tabpage').mkselectSet(dfop.IsShowTabPage);

                $('#component_verify').mkselectSet(dfop.verify);
                // 所在行占比
                $('#component_proportion').mkselect({
                    data: [
                        {
                            id: '1', text: '1'
                        },
                        {
                            id: '2', text: '1/2'
                        },
                        {
                            id: '3', text: '1/3'
                        },
                        {
                            id: '4', text: '1/4'
                        },
                        {
                            id: '5', text: '1/5'
                        },
                        {
                            id: '6', text: '1/6'
                        }
                    ],
                    placeholder: false,
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.proportion = item.id;
                        }
                        else {
                            dfop.proportion = '1';
                        }

                        $component.css({ 'width': 100 / parseInt(dfop.proportion) + '%' });
                    }
                });
                $('#component_proportion').mkselectSet(dfop.proportion);
                // 编辑表格字段
                $('#fieldsData_set').on('click', function () {
                    top.formGridSetting = {
                        fields: dfop.fieldsData,
                        tableName: dfop.table,
                        dbId: designerDfop.dbId,
                        fieldMap: fieldMap
                    };
                    Changjie.layerForm({
                        id: 'custmerForm_editgrid_index',
                        title: '编辑表格选项',
                        url: top.$.rootUrl + '/Utility/EditGridForm?keyValue=formGridSetting',
                        width: 1000,
                        height: 650,
                        maxmin: true,
                        btn: null,
                        end: function () {
                            top.formGridSetting = null;
                            //$.mkCustmerFormDesigner.renderTabs($self);
                        }
                    });
                });

                // 预加载数据
                // 数据库表选择
                $('#component_ytable').mkselect({
                    value: 'name',
                    text: 'name',
                    title: 'tdescription',
                    allowSearch: true,
                    select: function (item) {
                        if (!!item) {
                            dfop.preloadTable = item.name;
                        }
                        else {
                            dfop.preloadTable = '';
                        }
                    }
                }).mkselectSet(dfop.preloadTable);
                // 数据库表选择
                $('#component_ydb').mkselect({
                    url: top.$.rootUrl + '/SystemModule/DatabaseLink/GetTreeList',
                    type: 'tree',
                    placeholder: '请选择数据库',
                    allowSearch: true,
                    select: function (item) {
                        if (!item || item.hasChildren || item.id == -1) {
                            dfop.preloadDb = '';
                            $('#component_ytable').mkselectRefresh({
                                url: '',
                                data: [],
                            });
                        }
                        else {
                            dfop.preloadDb = item.id;
                            $('#component_ytable').mkselectRefresh({
                                url: top.$.rootUrl + '/SystemModule/DatabaseTable/GetList',
                                param: { databaseLinkId: dfop.preloadDb },
                                data: null,
                            });
                        }
                    }
                }).mkselectSet(dfop.preloadDb);
            },
            renderTable: function (compont, $row) {
                $row.find('.mk-form-item-title').remove();
                $row.addClass('mk-form-item-grid');
                var $compont = $('<div id="' + compont.id + '"></div>');
                $row.append($compont);
                compont.hideData = [];
                var headData = toGirdheadData(compont.fieldsData, compont.hideData, $compont);

                if (!!compont.preloadDb && !!compont.preloadTable) {

                    $compont.jfGrid({
                        headData: headData,
                        isAutoHeight: true
                    });
                    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetTableDataAllList', { databaseLinkId: compont.preloadDb, tableName: compont.preloadTable }, function (data) {
                        if ($compont.jfGridGet('rowdatas').length < 1) {
                            var fieldMap = {};
                            $.each(compont.fieldsData, function (id, girdFiled) {
                                if (!!girdFiled.field) {
                                    fieldMap[girdFiled.field.toLowerCase()] = girdFiled.field;
                                }
                            });
                            var rowDatas = [];
                            //$ul.append('<li><a data-value="'++'"');
                            for (var i = 0, l = data.length; i < l; i++) {
                                var _point = {};
                                for (var _field in data[i]) {
                                    _point[fieldMap[_field]] = data[i][_field];
                                }
                                rowDatas.push(_point);
                            }
                            $compont.jfGridSet('refreshdata', { rowdatas: rowDatas });

                        }
                    });
                }
                else {

                    $compont.jfGrid({
                        headData: headData,
                        isEdit: true,
                        bindTable: compont.table,
                        mainId: "ID",
                        rowdatas: [
                            {
                                "SortCode": 1,
                                "rowState": 1,
                                'ID': Changjie.newGuid()
                            }],
                        isAutoHeight: (compont.minheight == 0 ? true : false),
                        height: compont.minheight,
                        onAddRow: function (row, rows) {
                            row.SortCode = rows.length;
                            row.rowState = 1;
                            row.ID = Changjie.newGuid();
                        },

                    });
                }

            }
        },
        tabpage: {
            init: function () {
                var $html = $('<div class="mk-custmerform-component" data-type="tabpage" ><i class="fa fa-tablet"></i>选项卡</div>');
                return $html;
            },
            render: function ($component) {
                $component[0].dfop = $component[0].dfop || {
                    title: '选项卡',
                    type: "tabpage",
                    tabs: [],
                    field: '',
                    proportion: '1',
                    verify: '',
                    isShowAttachment: '1',
                    isShowWorkflow: '1',
                };

                for (var rw in $component[0].dfop.tabs) {
                    $$tabNames.push($component[0].dfop.tabs[rw].tabname);
                }
                var $text = "Tab项";
                if (JSON.stringify($$tabNames) != "{}") {
                    $text = $$tabNames;
                }

                $component.html(getComponentRowHtml({ name: '选项卡', text: $text }));
            },
            property: function ($component) {
                setTabpage($component);
            },
            renderTable: function (compont, $row, mainIdValue, type, fileop) {
                $row.find('.mk-form-item-title').remove();
                $row.addClass('mk-form-item-grid');
                var tabhtml = '<div id="form_tabs_sub">';
                tabhtml += '<ul class="nav nav-tabs"></ul></div>';
                tabhtml += '<div class="tab-content mk-tab-content" id="tab_content_sub">';
                tabhtml += '</div>';
                $row.append(tabhtml);
                $("#form_tabs_sub").mkFormTab();
                var $ul = $('#form_tabs_sub ul');
                var $container = $('#tab_content_sub');
                var tabs = compont.tabs;
                if (compont.isShowAttachment == "1") {
                    var attachmentIsExist = false;
                    for (var tab in tabs) {
                        if (tabs[tab].tabname == "附件") {
                            attachmentIsExist = true;
                            break;
                        }
                    }
                    if (attachmentIsExist == false) {
                        tabs.push(Changjie.attachmentSettings());
                    }

                }

                for (var tab in tabs) {
                    var $content = $('<div class="mk-form-wrap" id="tab_' + tab + '"></div>');
                    var $btn = $('<div></div>');
                    if (!!tabs[tab].buttons) {
                        for (var button in tabs[tab].buttons) {
                            var $name = tabs[tab].buttons[button].name;
                            var $id = tabs[tab].buttons[button].id;
                            var $class = tabs[tab].buttons[button].class;
                            var $style = tabs[tab].buttons[button].style;
                            var buttons = $('<div class="mk-component-control" style="text-align:right;float:right" ><a id="' + $id + '"  style="' + $style + '" class="btn btn-primary">' + $name + '</a></div>');
                            $btn.append(buttons);
                        }
                        $content.prepend($btn);

                    }
                    //var $content = $('<div class="mk-form-wrap" id="tab_' + tab+'"></div>');
                    $container.append($content);
                    var $compont = $('<div id="' + tabs[tab].tablename + '"></div>');
                    $content.append($compont);

                    compont.hideData = [];

                    var headData = toGirdheadData(tabs[tab].fields, compont.hideData, $compont, tabs[tab].tabname);
                    if (!!compont.preloadDb && !!compont.preloadTable) {
                        $compont.jfGrid({
                            headData: headData,
                            isAutoHeight: true
                        });
                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetTableDataAllList', { databaseLinkId: compont.preloadDb, tableName: compont.preloadTable }, function (data) {
                            if ($compont.jfGridGet('rowdatas').length < 1) {
                                var fieldMap = {};
                                $.each(compont.fieldsData, function (id, girdFiled) {
                                    if (!!girdFiled.field) {
                                        fieldMap[girdFiled.field.toLowerCase()] = girdFiled.field;
                                    }
                                });
                                var rowDatas = [];
                                //$ul.append('<li><a data-value="'++'"');
                                for (var i = 0, l = data.length; i < l; i++) {
                                    var _point = {};
                                    for (var _field in data[i]) {
                                        _point[fieldMap[_field]] = data[i][_field];
                                    }
                                    rowDatas.push(_point);
                                }
                                $compont.jfGridSet('refreshdata', { rowdatas: rowDatas });
                            }
                        });
                    }
                    else {
                        var mainId = "ID";
                        var tname = tabs[tab].tablename;

                        if (tname == "Base_AnnexesFile") {
                            mainId = "F_Id";
                        }

                        $compont.jfGrid({
                            headData: headData,
                            isEdit: (tabs[tab].isEdit == false ? false : true),
                            rowdatas: [
                                {
                                    "SortCode": 1,
                                    "rowState": 1,
                                    'ID': Changjie.newGuid()
                                }],
                            isAutoHeight: (compont.minheight == 0 ? true : false),
                            height: compont.minheight,
                            headData: headData,
                            bindTable: tabs[tab].tablename,
                            mainId: mainId,
                            onAddRow: function (row, rows) {
                                row.SortCode = rows.length;
                                row.rowState = 1;
                                row.ID = Changjie.newGuid();
                            },
                            onMinusRow: function (row, rows, headData) {
                            }
                        });
                    }

                    $ul.append('<li><a data-value="tab_' + tabs[tab].tablename + '">' + tabs[tab].tabname + '</a></li>');
                    $content.addClass('tab-pane').attr('id', "tab_" + tabs[tab].tablename);
                }
                if (compont.isShowWorkflow == "1") {

                    var $flowcontent = $('<div class="mk-form-wrap tab-pane" id="tab_auditflowinfo"><div id="auditflowinfo"></div></div>');
                    $container.append($flowcontent);
                    $ul.append('<li><a data-value="tab_auditflowinfo">流程</a></li>');

                    $("#auditflowinfo").jfGrid({
                        url: top.$.rootUrl + '/WorkFlowModule/WorkFlow/GetHistory',
                        headData: [
                            {
                                label: "节点名称", name: "NodeName", width: 160, align: "left",
                            },
                            {
                                label: "审批结果", name: "Result", width: 160, align: "left",
                                formatter: function (cellvalue) {
                                    switch (cellvalue) {
                                        case 1:
                                            return '<span class="label label-success" style="cursor: pointer;">审批通过</span>';
                                        case 2:
                                            return '<span class="label label-warning" style="cursor: pointer;">审批未通过</span>';
                                        case 3:
                                            return '<span class="label label-info" style="cursor: pointer;">驳回</span>';;
                                        case 4:
                                            return '<span class="label label-warning" style="cursor: pointer;">加签审批通过</span>';
                                        default:
                                            return '<span class="label label-default" style="cursor: pointer;">审批未通过</span>';
                                    }
                                }
                            },
                            { label: "审批时间", name: "AuditTime", width: 160, align: "left" },
                            { label: "审批人", name: "AuditorUserName", width: 120, align: "left" },
                            { label: "总耗", name: "ElapsedTimeDescription", width: 120, align: "left" },
                            { label: "审批意见", name: "Description", width: 120, align: "left" }
                        ],
                        isEdit: false,
                        mainId: "ID",
                        bindTable: 'Sys_WfHistory',
                    });
                    //tabs.push(GetWorkflowSettings());

                }

                $("#btn_uploadfile").on("click", function () {

                    $("#btn_uploadfile").mkUploader({
                        isShowDefaultButton: false,
                        parentId: mainIdValue,
                        moduleType: fileop.moduleType || "",
                        moduleId: fileop.moduleId || "",
                        moduleInfoId: fileop.moduleInfoId || "",
                        folderName: fileop.folderName || "",
                        id: "Base_AnnexesFile"
                    });

                });

                $("#btn_delfile").on("click", function () {
                    var id = $("#Base_AnnexesFile").jfGridValue("F_Id");
                    if (!!id) {
                        Changjie.layerConfirm('是否删除该文件？', function (res) {
                            if (res) {

                                var param = {};
                                param['__RequestVerificationToken'] = $.mkToken;
                                param['fileId'] = id;
                                Changjie.httpAsyncPost(top.$.rootUrl + "/SystemModule/Annexes/DeleteAnnexesFile", param, function (res) {
                                    if (res.code == Changjie.httpCode.success) {
                                        $("#Base_AnnexesFile").jfGridSet("removeRow", id);
                                    }
                                });
                            }
                        });
                    }
                });

                if (type == "edit") {

                    Changjie.httpAsyncGet(top.$.rootUrl + '/SystemModule/Annexes/GetAnnexesFileList?infoId=' + mainIdValue, function (res) {

                        if (res.code == Changjie.httpCode.success) {
                            for (var item in res.data) {
                                if (!res.data[item].F_DownloadCount) {
                                    res.data[item].F_DownloadCount = 0;
                                }
                                var tokb = parseInt(res.data[item].F_FileSize) / 1024;
                                res.data[item].F_FileSize = (Math.round(tokb * 100) / 100) + "KB";
                                $('#Base_AnnexesFile').jfGridSet('refreshdata', res.data);
                            }
                        }
                    });

                    var shcemeformid = request('keyValue');
                    var shcemeinfoid = request('id');
                    if (shcemeformid && shcemeinfoid) {
                        Changjie.httpAsyncGet(top.$.rootUrl + '/FormModule/Custmerform/GetProcessinfoBySchemeForm?schemeCode=' + shcemeinfoid + '&formKeyValue=' + shcemeformid, function (res) {
                            if (res.code == Changjie.httpCode.success) {
                                if (res.data.res) {
                                    $('#auditflowinfo').jfGridSet('reload', { workflowId: res.data.data });
                                }
                            }
                        });
                    }
                }

                $ul.find('li').click(function () {
                    $("#tab_content_sub .tab-pane").hide();
                    $('#' + $("a", $(this)).data("value")).show();
                });

                $ul.find('li:eq(0)').trigger('click');


            }
        },
        calculation: {
            init: function () {

                var $html = $('<div class="mk-custmerform-component" data-type="calculation" ><i  class="fa fa-keyboard-o"></i>计算框</div>');
                return $html;
            },
            render: function ($component) {

                $component[0].dfop = $component[0].dfop || {
                    title: '计算框',
                    type: 'calculation',
                    isReadOnly: '1',//是否可编辑
                    points: 2,//小数点保留位数
                    target1: '',//目标1
                    target2: '',//目标2
                    toservicetarget1: '',
                    toservicetarget2: '',
                    calculationType: "mul"

                };
                $component.html(getComponentRowHtml({ name: $component[0].dfop.title, text: "计算框" }));
            },
            property: function ($component) {

                var dfop = $component[0].dfop;

                var $html = setComponentPropertyHtml($component, verifyDatalist);

                var _html = '';
                _html += '<div class="mk-component-title">是否可编辑</div>';
                _html += '<div class="mk-component-control"><div id="component_isReadOnly"></div></div>';

                _html += '<div class="">小数点保留位数</div>';
                _html += '<div class="mk-component-control"><div id="component_points"></div></div>';

                _html += '<div class="">目标1</div>';
                _html += '<div class="mk-component-control"><div id="component_target1"></div></div>';

                _html += '<div class="">目标2</div>';
                _html += '<div class="mk-component-control"><div id="component_target2"></div></div>';

                _html += '<div class="">计算方式</div>';
                _html += '<div class="mk-component-control"><div id="component_calculationType"></div></div>';


                $html.append(_html);

                $('#component_isReadOnly').mkselect({
                    placeholder: false,
                    data: [{ id: '0', text: '否' }, { id: '1', text: '是' }],
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.isReadOnly = item.id;
                        }
                        else {
                            dfop.isReadOnly = '1';
                        }
                    }
                });
                $('#component_isReadOnly').mkselectSet(dfop.isReadOnly);

                $('#component_points').mkselect({
                    placeholder: false,
                    data: [
                        { id: '0', text: '不保留' },
                        { id: '1', text: '保留1位' },
                        { id: '2', text: '保留2位' },
                        { id: '3', text: '保留3位' },
                        { id: '4', text: '保留4位' }
                    ],
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.points = item.id;
                        }
                        else {
                            dfop.points = '2';
                        }
                    }
                });
                $('#component_points').mkselectSet(dfop.points);

                var $fieldlist = [];//字段集合

                var $this = $component.parents("div.step-pane.active");

                var scheme = $($this).mkCustmerFormDesigner('get');

                for (var item in scheme.data) {
                    var cmpts = scheme.data[item].componts;
                    for (var index in cmpts) {
                        if (cmpts[index].type != "text" && cmpts[index].type != "select") continue;
                        $fieldlist.push({
                            "id": cmpts[index].id,
                            "text": cmpts[index].field + "(" + cmpts[index].title + ")"
                        });

                    }
                }

                $('#component_target1').mkselect({
                    placeholder: false,
                    data: $fieldlist,
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.target1 = item.id;
                            dfop.toservicetarget1 = item.text;
                        }
                    }
                });
                $('#component_target1').mkselectSet(dfop.target1);

                $('#component_target2').mkselect({
                    placeholder: false,
                    data: $fieldlist,
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.target2 = item.id;
                            dfop.toservicetarget2 = item.text;
                        }
                    }
                });
                $('#component_target2').mkselectSet(dfop.target2);

                $('#component_calculationType').mkselect({
                    placeholder: false,
                    data: [
                        { id: 'add', text: '加' },
                        { id: 'sub', text: '减' },
                        { id: 'mul', text: '乘' },
                        { id: 'div', text: '除' },
                    ],
                    value: 'id',
                    text: 'text',
                    select: function (item) {
                        if (!!item) {
                            dfop.calculationType = item.id;
                        }
                        else {
                            dfop.calculationType = 'mul';
                        }
                    }
                });
                $('#component_calculationType').mkselectSet(dfop.calculationType);

            },
            renderTable: function (compont, $row) {

                var $compont;

                $compont = $('<input id="' + compont.id + '" points="' + compont.points + '"  ' + (compont.isReadOnly == '0' ? "readonly" : "") + ' type="text" class="form-control" />');

                var value1 = 0;
                var value2 = 0;

                var t1type = $("#" + compont.target1).attr("type");
                var t2type = $("#" + compont.target2).attr("type");

                if (t1type == "text" && t2type == "text") {
                    $("#" + compont.target1).on("input propertychange", function () {
                        value1 = $(this).val();
                        value2 = $("#" + compont.target2).val();
                        $("#" + compont.id).val(top.Changjie.simpleMath(value1, value2, compont.calculationType, compont.points));
                    });
                    $("#" + compont.target2).on("input propertychange", function () {
                        value1 = $("#" + compont.target1).val();
                        value2 = $(this).val();
                        $("#" + compont.id).val(top.Changjie.simpleMath(value1, value2, compont.calculationType, compont.points));
                    });
                }
                else if (t1type == "text" && t2type == "mkselect") {
                    $("#" + compont.target1).on("input propertychange", function () {
                        value1 = $(this).val();
                        value2 = $("#" + compont.target2).mkselectGetText();
                        $("#" + compont.id).val(top.Changjie.simpleMath(value1, value2, compont.calculationType, compont.points));
                    });

                    $("#" + compont.target2).mkselect().on("change", function () {
                        value1 = $("#" + compont.target1).val();
                        value2 = $(this).mkselectGetText();
                        $("#" + compont.id).val(top.Changjie.simpleMath(value1, value2, compont.calculationType, compont.points));
                    });
                }
                else if (t1type == "mkselect" && t2type == "text") {
                    $("#" + compont.target1).mkselect().on("change", function () {
                        value1 = $(this).mkselectGetText();
                        value2 = $("#" + compont.target2).val();
                        $("#" + compont.id).val(top.Changjie.simpleMath(value1, value2, compont.calculationType, compont.points));
                    });

                    $("#" + compont.target2).on("input propertychange", function () {
                        value1 = $("#" + compont.target1).mkselectGetText();
                        value2 = $(this).val();
                        $("#" + compont.id).val(top.Changjie.simpleMath(value1, value2, compont.calculationType, compont.points));
                    });
                }


                $row.append($compont);

                return $compont;
            },
        },
    }

    function setTabpage($component, verifyData) {
        var designerDfop = $component.parent()[0].dfop;
        var dfop = $component[0].dfop;
        var $html = $(".mk-custmerform-designer-layout-right .mk-scroll-box");
        var _html = '';
        _html += '<div class="mk-component-title">是否显示附件Tab页</div>';
        _html += '<div class="mk-component-control"><div id="component_isShowAttachment"></div></div>';
        _html += '<div class="mk-component-title">是否显示流程Tab页</div>';
        _html += '<div class="mk-component-control"><div id="component_isShowWorkflow"></div></div>';
        _html += '<div class="mk-component-title">绑定表</div>';
        _html += '<div class="mk-component-control"><div id="component_table"></div></div>';
        _html += '<div class="mk-component-title">组件标题</div>';
        _html += '<div class="mk-component-control"><input id="component_title" type="text" class="form-control" placeholder="必填项"/></div>';
        _html += '<div class="mk-component-control" style="text-align:center;padding-top:10px;" ><a id="addtable" class="btn btn-primary btn-block">添加表</a></div>';
        _html += '<div class="mk-component-control" style="text-align:center;padding-top:10px;" ><a id="addfields" class="btn btn-primary btn-block">字段编辑</a></div>';
        _html += '<div class="mk-layout-body" id="tabgridtable"></div>';
        $html.html(_html);

        $('#component_isShowAttachment').mkselect({
            data: [
                { "id": "0", "text": "否" },
                { "id": "1", "text": "是" }
            ],
            value: 'id',
            text: 'text',
            placeholder: false,
            select: function (item) {
                if (!!item) {
                    dfop.isShowAttachment = item.id;
                }
                else {
                    dfop.isShowAttachment = '1';
                }
            }
        });
        $('#component_isShowAttachment').mkselectSet(dfop.isShowAttachment || "1");
        $('#component_isShowWorkflow').mkselect({
            data: [
                { "id": "0", "text": "否" },
                { "id": "1", "text": "是" }
            ],
            value: 'id',
            text: 'text',
            placeholder: false,
            select: function (item) {
                if (!!item) {
                    dfop.isShowWorkflow = item.id;
                }
                else {
                    dfop.isShowWorkflow = '1';
                }
            }
        });
        $('#component_isShowWorkflow').mkselectSet(dfop.isShowWorkflow || "1");

        var tables = [];//非主表集合
        var $fields = [];//主表字段
        var mainTableName = "";
        for (var item in designerDfop.dbTable) {
            if (designerDfop.dbTable[item].isMain != "1") {
                tables.push(designerDfop.dbTable[item]);
            }
            else {
                mainTableName = designerDfop.dbTable[item].name;
            }
        }
        var $this = $component.parents("div.step-pane.active");
        var scheme = $($this).mkCustmerFormDesigner('get');

        for (var item in scheme.data) {
            for (var index in scheme.data[item].componts) {
                if (scheme.data[item].componts[index].table == mainTableName) {
                    $fields.push({
                        "id": scheme.data[item].componts[index].field,
                        "text": scheme.data[item].componts[index].field + "(" + scheme.data[item].componts[index].title + ")"
                    });
                }
            }
        }

        $('#tabgridtable').jfGrid({
            headData: [
                {
                    label: "Tab名称", name: "tabname", width: 80, align: "left",
                    edit: {
                        type: 'input',
                        change: function (row, rowIndex) {
                            $$tabNames[rowIndex] = row.tabname;
                            $component.find('.mk-compont-value').text($$tabNames);
                        },
                        blur: function (row, index, oldValue, columnName, obj) {
                            if (!!!row.tabname) {
                                Changjie.alert.warning("请输入Tab名称");
                                obj.focus();
                            }
                        }
                    }
                },
                { label: "关联表", name: "tablename", width: 80, align: "left", height: 30 },
                {
                    label: "操作", name: "operation", width: 30, align: "center", height: 30,
                    formatter: function (a, b, c, d, e, index) {
                        return '<div class="btn-group btn-group-sm" ><a id="deleterow" onclick="delrow(' + index + ')" style="height:27px;margin-top:-3px" class="btn btn-default deleterow"><i class="fa fa-trash-o"></i></a></div>';
                    }
                }
            ],
        });
        var _field = dfop.field;

        $('#tabgridtable').jfGridSet("refreshdata", dfop.tabs);

        $("#addtable").on("click", function () {
            var $selectValue = $('#component_table').mkselectGet();
            var $tabRows = $("#tabgridtable").jfGridGet("rowdatas");
            var $isExist = false;

            var $tabname = $("#component_title").val();
            if (!!$tabname) {
                for (var item in $tabRows) {
                    if ($tabRows[item].tablename == $selectValue) {
                        $isExist = true;
                        break;
                    }
                }

                if (!$isExist) {
                    var row = { "tabname": $tabname, "tablename": $selectValue, "fields": [] };
                    $('#tabgridtable').jfGridSet("addRow", row);
                    dfop.tabs = $('#tabgridtable').jfGridGet("rowdatas");

                    $$tabNames.push($tabname);
                    $component.find('.mk-compont-value').text($$tabNames);

                    $("#addfields").show();
                }
                else {
                    Changjie.alert.warning("表已存在。");
                }
            }
            else {
                Changjie.alert.warning("请输入tab名称。");
            }
        });

        $("#addfields").on("click", function () {
            var currentRow = $("#tabgridtable").jfGridGet("rowdata");

            if (Object.keys(currentRow).length == 0) {
                Changjie.alert.warning("请先从下列表格中选择数据!");
                return;
            }

            var $rowindex;
            for (var i in $$tabNames) {
                if ($$tabNames[i] == currentRow.tabname) {
                    $rowindex = i;
                    break;
                }
            }

            top.formGridSetting = {
                fields: dfop.tabs[$rowindex].fields,
                tableName: dfop.tabs[$rowindex].tablename,
                dbId: designerDfop.dbId,
                fieldMap: fieldMap,
                mainTableFields: $fields
            };
            Changjie.layerForm({
                id: 'custmerForm_editgrid_index',
                title: '编辑表格选项',
                url: top.$.rootUrl + '/Utility/EditGridForm?keyValue=formGridSetting',
                width: 1000,
                height: 650,
                maxmin: true,
                btn: null,
                end: function () {
                    top.formGridSetting = null;
                }
            });
        });

        // 绑定字段
        $('#component_table').mkselect({
            data: tables || [],
            value: 'name',
            text: 'name',
            allowSearch: true,
            select: function (item) {
                if (!!item) {
                    dfop.table = item.name;
                }
                else {
                    dfop.table = '';
                }
            }
        });
        if (!!tables && tables.length > 0) {
            $('#component_table').mkselectSet(dfop.table || tables[0].name);
        }
        // 标题设置
        return $html;
    };

    // 表字段
    var verifyDatalist = [
        { id: "NotNull", text: "不能为空！" },
        { id: "Num", text: "必须为数字(整数)！" },
        { id: "NumOrNull", text: "数字(整数)或空！" },
        { id: "Float", text: "必须为数字(浮点数)！" },
        { id: "FloatOrNull", text: "数字(浮点数)或空！" },
        { id: "Email", text: "必须为E-mail格式！" },
        { id: "EmailOrNull", text: "E-mail格式或空！" },
        { id: "EnglishStr", text: "必须为字符串！" },
        { id: "EnglishStrOrNull", text: "字符串或空！" },
        { id: "Phone", text: "必须电话格式！" },
        { id: "PhoneOrNull", text: "电话格式或者空！" },
        { id: "Fax", text: "必须为传真格式！" },
        { id: "Mobile", text: "必须为手机格式！" },
        { id: "MobileOrNull", text: "手机格式或者空！" },
        { id: "MobileOrPhone", text: "电话格式或手机格式！" },
        { id: "MobileOrPhoneOrNull", text: "电话格式或手机格式或空！" },
        { id: "Uri", text: "必须为网址格式！" },
        { id: "UriOrNull", text: "网址格式或空！" }
    ];
    var verifyDatalist2 = [
        { id: "NotNull", text: "不能为空！" },
        { id: "Num", text: "必须为数字(整数)！" },
        { id: "NumOrNull", text: "数字(整数)或空！" },
        { id: "Float", text: "必须为数字(浮点数)！" },
        { id: "FloatOrNull", text: "数字(浮点数)或空！" },
        { id: "Email", text: "必须为E-mail格式！" },
        { id: "EmailOrNull", text: "E-mail格式或空！" },
        { id: "EnglishStr", text: "必须为字符串！" },
        { id: "EnglishStrOrNull", text: "字符串或空！" },
        { id: "Phone", text: "必须电话格式！" },
        { id: "PhoneOrNull", text: "电话格式或者空！" },
        { id: "Fax", text: "必须为传真格式！" },
        { id: "Mobile", text: "必须为手机格式！" },
        { id: "MobileOrNull", text: "手机格式或者空！" },
        { id: "MobileOrPhone", text: "电话格式或手机格式！" },
        { id: "MobileOrPhoneOrNull", text: "电话格式或手机格式或空！" },
        { id: "Uri", text: "必须为网址格式！" },
        { id: "UriOrNull", text: "网址格式或空！" }
    ];

    // 获取组件行显示html
    function getComponentRowHtml(data) {
        var _html = "";
        if (data.readonly) {
            _html = '<div class="mk-compont-title"><span>' + data.name + '</span><div class="mk-compont-remove"></div></div><div class="mk-compont-value">系统字段不可编辑,不在页面呈现</div>';
        }
        else {
            _html = '<div class="mk-compont-title"><span>' + data.name + '</span><div class="mk-compont-remove"><i  title="移除组件" class="del fa fa-close"></i></div></div><div class="mk-compont-value">' + data.text + '</div>';
        }
        return _html;
    };
    // 设置组件公共属性设置html
    function setComponentPropertyHtml($component, verifyData) {
        var designerDfop = $component.parent()[0].dfop;
        var dfop = $component[0].dfop;

        var $html = $(".mk-custmerform-designer-layout-right .mk-scroll-box");
        var _html = '';
        _html += '<div class="mk-component-title">绑定表</div>';
        _html += '<div class="mk-component-control"><div id="component_table" ' + (dfop.readonly == true ? "readonly" : "") + '></div></div>';
        _html += '<div class="mk-component-title">绑定字段</div>';
        _html += '<div class="mk-component-control"><div id="component_field" ' + (dfop.readonly == true ? "readonly" : "") + '></div></div>';
        _html += '<div class="mk-component-title">组件标题</div>';
        _html += '<div class="mk-component-control"><input id="component_title" ' + (dfop.readonly == true ? "readonly" : "") + ' type="text" class="form-control" placeholder="必填项"/></div>';
        if (!!verifyData) {
            _html += '<div class="mk-component-title">字段验证</div>';
            _html += '<div class="mk-component-control"><div id="component_verify"></div></div>';
        }
        _html += '<div class="mk-component-title">所占行比例</div>';
        _html += '<div class="mk-component-control"><div id="component_proportion" ' + (dfop.readonly == true ? "readonly" : "") + '></div></div>';

        $html.html(_html);

        var _field = dfop.field;

        // 绑定字段
        $('#component_field').mkselect({
            value: 'f_column',
            text: 'f_column',
            title: 'f_remark',
            allowSearch: true,
            select: function (item) {
                if (!!item) {
                    dfop.field = item.f_column;
                }
                else {
                    dfop.field = '';
                }
                if (dfop.title == "文本框" && item != null) {

                    dfop.title = item.f_remark;
                    $("#component_title").val(item.f_remark);
                    $component.find('.mk-compont-title span').text(item.f_remark);

                }
                if (item != null && item.f_remark != dfop.title) {
                    dfop.title = item.f_remark;
                    $("#component_title").val(item.f_remark);
                    $component.find('.mk-compont-title span').text(item.f_remark);
                }
            }
        }).mkselectSet(_field);
        $('#component_table').mkselect({
            data: designerDfop.dbTable || [],
            value: 'name',
            text: 'name',
            allowSearch: true,
            select: function (item) {
                if (!!item) {
                    dfop.table = item.name;
                    if (!!fieldMap[designerDfop.dbId + item.name]) {
                        $('#component_field').mkselectRefresh({
                            data: fieldMap[designerDfop.dbId + item.name]
                        });
                    }
                    else {
                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldList', { databaseLinkId: designerDfop.dbId, tableName: item.name }, function (data) {
                            fieldMap[designerDfop.dbId + item.name] = data;
                            $('#component_field').mkselectRefresh({
                                data: fieldMap[designerDfop.dbId + item.name]
                            });

                        });
                    }
                }
                else {
                    $('#component_field').mkselectRefresh({
                        data: []
                    });
                    dfop.table = '';
                }
            }
        });
        if (!!designerDfop.dbTable && designerDfop.dbTable.length > 0) {
            $('#component_table').mkselectSet(dfop.table || designerDfop.dbTable[0].name);
        }
        // 标题设置
        $html.find('#component_title').val(dfop.title);
        $html.find('#component_title').on('input propertychange', function () {
            var value = $(this).val();
            $component.find('.mk-compont-title span').text(value);
            dfop.title = value;
        });
        if (!!verifyData) {
            // 字段验证
            $('#component_verify').mkselect({
                data: verifyData,
                value: 'id',
                text: 'text',
                allowSearch: true,
                select: function (item) {
                    if (!!item) {
                        dfop.verify = item.id;
                    }
                    else {
                        dfop.verify = "";
                    }
                }
            });
            $('#component_verify').mkselectSet(dfop.verify);
        }
        // 所在行占比
        $('#component_proportion').mkselect({
            data: [
                {
                    id: '1', text: '1'
                },
                {
                    id: '2', text: '1/2'
                },
                {
                    id: '3', text: '1/3'
                },
                {
                    id: '4', text: '1/4'
                },
                {
                    id: '6', text: '1/6'
                }
            ],
            placeholder: false,
            value: 'id',
            text: 'text',
            select: function (item) {
                if (!!item) {
                    dfop.proportion = item.id;
                }
                else {
                    dfop.proportion = '1';
                }

                $component.css({ 'width': 100 / parseInt(dfop.proportion) + '%' });
            }
        });
        $('#component_proportion').mkselectSet(dfop.proportion);

        return $html;
    };
    // 数据字典选择初始化、数据源选择初始化
    function setDatasource(dfop) {
        $('#component_dfvalue').mkselect({
            allowSearch: true,
            select: function (item) {
                if (!!item) {
                    if (dfop.dataSource == '0') {
                        dfop.dfvalue = item.id;
                    }
                    else {
                        var vlist = dfop.dataSourceId.split(',');
                        dfop.dfvalue = item[vlist[2]];
                    }
                }
                else {
                    dfop.dfvalue = '';
                }
            }
        }).mkselectSet(dfop.dfvalue);
        $('#component_dataItemCode').mkselect({
            allowSearch: true,
            url: top.$.rootUrl + '/SystemModule/DataItem/GetClassifyTree',
            type: 'tree',
            value: 'value',
            select: function (item) {
                if (!!item) {
                    if (dfop.dataSourceId != item.id) {
                        dfop.dfvalue = '';
                        //dfop.dataSourceId = item.id;
                        dfop.itemCode = item.value;
                    }
                    Changjie.clientdata.getAllAsync('dataItem', {
                        code: item.value,
                        callback: function (dataes) {
                            var list = [];
                            $.each(dataes, function (_index, _item) {
                                if (_item.parentId == "0") {
                                    list.push({ id: _item.value, text: _item.text, title: _item.text, k: _index });
                                }
                            });
                            $('#component_dfvalue').mkselectRefresh({
                                value: "id",
                                text: "text",
                                title: "title",
                                data: list,
                                url: ''
                            });
                        }
                    });
                }
                else {
                    //dfop.dataSourceId = '';
                    dfop.itemCode = '';
                    $('#component_dfvalue').mkselectRefresh({ url: '', data: [] });
                }
            }
        });

        $('#component_dataSourceId1').mkselect({// 显示字段
            title: 'text',
            text: 'text',
            value: 'value',
            allowSearch: true,
            select: function (item) {
                if (item) {
                    var vlist = dfop.dataSourceId.split(',');
                    vlist[1] = item.value;
                    dfop.dataSourceId = String(vlist);
                    if (vlist[2] != '') {

                        Changjie.clientdata.getAllAsync('sourceData', {
                            code: vlist[0],
                            callback: function (dataes) {
                                var v = dfop.dfvalue;
                                $('#component_dfvalue').mkselectRefresh({
                                    value: vlist[2],
                                    text: vlist[1],
                                    title: vlist[1],
                                    url: '',
                                    data: dataes
                                });
                                $('#component_dfvalue').mkselectSet(v);
                            }
                        });
                    }
                }
            }
        });
        $('#component_dataSourceId2').mkselect({// 保存字段
            title: 'text',
            text: 'text',
            value: 'value',
            allowSearch: true,
            select: function (item) {
                if (item) {
                    var vlist = dfop.dataSourceId.split(',');
                    vlist[2] = item.value;
                    dfop.dataSourceId = String(vlist);
                    if (vlist[1] != '') {
                        Changjie.clientdata.getAllAsync('sourceData', {
                            code: vlist[0],
                            callback: function (dataes) {
                                var v = dfop.dfvalue;
                                $('#component_dfvalue').mkselectRefresh({
                                    value: vlist[2],
                                    text: vlist[1],
                                    title: vlist[1],
                                    url: '',
                                    data: dataes
                                });
                                $('#component_dfvalue').mkselectSet(v);
                            }
                        });
                    }
                }
            }
        });

        $('#component_dataSourceId').mkselect({
            allowSearch: true,
            url: top.$.rootUrl + '/SystemModule/DataSource/GetList',
            value: 'F_Code',
            text: 'F_Name',
            title: 'F_Name',
            select: function (item) {
                if (item) {
                    if (dfop.dataSourceId != '') {
                        var vlist = dfop.dataSourceId.split(',');
                        if (vlist.length < 3 || vlist[0] != item.F_Code) {
                            vlist[1] = '';
                            vlist[2] = '';
                            dfop.dataSourceId = item.F_Code + ',,';
                            dfop.dfvalue = '';
                        }
                    }
                    else {
                        dfop.dataSourceId = item.F_Code + ',,';
                        dfop.dfvalue = '';
                    }
                    //$('#component_dfvalue').mkselectRefresh({ url: '', data: [] });


                    //获取字段数据
                    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DataSource/GetDataColName', { code: item.F_Code }, function (data) {
                        var fieldData = [];
                        for (var i = 0, l = data.length; i < l; i++) {
                            var id = data[i];
                            var selectpoint = { value: id, text: id };
                            fieldData.push(selectpoint);
                        }
                        var vlist = dfop.dataSourceId.split(',');
                        $('#component_dataSourceId1').mkselectRefresh({
                            data: fieldData,
                            placeholder: false
                        });
                        $('#component_dataSourceId1').mkselectSet(vlist[1] || fieldData[0].value);
                        $('#component_dataSourceId2').mkselectRefresh({
                            data: fieldData,
                            placeholder: false
                        });
                        $('#component_dataSourceId2').mkselectSet(vlist[2] || fieldData[0].value);
                    });
                }
                else {
                    dfop.dfvalue = '';
                    dfop.dataSourceId = '';
                    $('#component_dataSourceId1').mkselectRefresh({
                        data: [],
                        placeholder: '请选择'
                    });
                    $('#component_dataSourceId2').mkselectRefresh({
                        data: [],
                        placeholder: '请选择'
                    });
                    $('#component_dfvalue').mkselectRefresh({ url: '', data: [] });
                }
            }
        });



        if (dfop.dataSource == "0") {
            $('#component_dataItemCode').mkselectSet(dfop.itemCode);
        }
        else {
            var valuelist = dfop.dataSourceId.split(',');
            $('#component_dataSourceId').mkselectSet(valuelist[0]);
        }

        $('#component_dataSource').mkselect({
            data: [{ id: '0', text: '数据字典' }, { id: '1', text: '数据源' }],
            value: 'id',
            text: 'text',
            placeholder: false,
            select: function (item) {
                if (dfop.dataSource != item.id) {
                    dfop.dfvalue = '';
                    dfop.dataSourceId = '';
                    dfop.itemCode = '';
                    $('#component_dataItemCode').mkselectSet('');
                    //$('#component_dataSourceId').mkselectSet('');
                    $('#component_dfvalue').mkselectRefresh({ url: '', data: [] });
                }
                dfop.dataSource = item.id;
                if (dfop.dataSource == '0') {
                    $('.dataSourceId').hide();
                    $('#component_dataItemCode1').show();
                }
                else {
                    $('#component_dataItemCode1').hide();
                    $('.dataSourceId').show();
                }
            }
        }).mkselectSet(dfop.dataSource);
    };


    // 转化成树形数据
    function toGirdheadData(data, hideData, $compont, tabname) {
        // 只适合小数据计算
        var resdata = [];
        var mapdata = {};
        var mIds = {};
        var pIds = {};
        var maprowdatas = {};
        data = data || [];
        var codeList = [];//公式涉及字段

        //var isExistInline = false;
        var inlineList = [];
        //$.each(data, function (_index, item) {
        //    var inlineOperation = {};
        //    if (item.inlinetype != "none") {
        //        if (item.inlineTarget1 != "none" && item.inlineTarget2 != "none") {
        //            inlineOperation.type = item.inlinetype;

        //            inlineOperation.target1 = item.inlineTarget1;
        //            inlineOperation.target2 = item.inlineTarget2;
        //            inlineOperation.toTarget = item.field;

        //            inlineList.push(inlineOperation);
        //            isExistInline = true;
        //        }
        //    }
        //});
        $.each(data, function (_index, item) {
            var inlineOperation = {};
            if (item.inlinetype != "none") {
                if (item.inlineTarget1 != "none" && item.inlineTarget2 != "none") {
                    inlineOperation.toTargets = [];
                    var item = { t1: item.inlineTarget1, t2: item.inlineTarget2, to: item.field, type: item.inlinetype }
                    inlineOperation.toTargets.push(item);
                    inlineList.push(inlineOperation);
                }

                if (item.inlinetype == "formula") {
                    inlineOperation.toTargets = [];
                    var item = { to: item.field, type: item.inlinetype, formula: item.formula }
                    inlineOperation.toTargets.push(item);
                    inlineList.push(inlineOperation);
                }
            }
        });

        for (var i = 0; i < inlineList.length; i++) {
            for (var j = i + 1; j < inlineList.length; j++) {
                var current = inlineList[i].toTargets[0];
                var next = inlineList[j].toTargets[0];
                if (current.t1 == next.t1) {
                    inlineList[i].toTargets.push({ t1: current.t1, t2: next.t2, to: next.to, type: next.type, formula: current.formulaStr });
                }
                else if (current.t1 == next.t2) {
                    inlineList[i].toTargets.push({ t1: next.t2, t2: current.t1, to: next.to, type: next.type, formula: next.formulaStr });
                }

            }
        }

        $.each(data, function (_index, item) {
            var isinline = false;
            var inlineInfo = {};
            for (var i = 0; i < inlineList.length; i++) {
                var info = inlineList[i].toTargets[0];
                if (!!!info) continue;
                if (item.inlinetype == "formula") {
                    inlineInfo.to = inlineList[i].toTargets;
                    isinline = true;
                    break;
                }
            }

            if (!!inlineInfo.to) {
                if (inlineInfo.to[0].type == "formula") {
                    //解析计算字段
                    var stringValue = inlineInfo.to[0].formula;
                    var positionsLeft = new Array();
                    var posleft = stringValue.indexOf("[");
                    while (posleft > -1) {
                        positionsLeft.push(posleft);
                        posleft = stringValue.indexOf("[", posleft + 1);
                    }

                    var positionsRight = new Array();
                    var posright = stringValue.indexOf("]");
                    while (posright > -1) {
                        positionsRight.push(posright);
                        posright = stringValue.indexOf("]", posright + 1);
                    }

                    for (var i = 0; i < positionsLeft.length; i++) {
                        codeList[i] = stringValue.toString().substring(positionsLeft[i] + 1, positionsRight[i]);
                    }
                }
            }

        });


        $.each(data, function (_index, item) {
            var isAdd = true;
            var isinline = false;
            var inlineInfo = {};
            for (var i = 0; i < inlineList.length; i++) {
                var info = inlineList[i].toTargets[0];
                if (!!!info) continue;
                if (item.field == info.t1) {
                    inlineInfo.to = inlineList[i].toTargets;
                    inlineInfo.isFirst = true;
                    isinline = true;
                    break;
                } else if (item.field == info.t2) {
                    inlineInfo.to = inlineList[i].toTargets;
                    inlineInfo.isFirst = false;
                    isinline = true;
                    break;
                }
                else if (item.inlinetype == "formula") {
                    inlineInfo.to = inlineList[i].toTargets;
                    inlineInfo.isFirst = false;
                    isinline = true;
                    break;
                }
                else {
                    for (var t = 0; t < codeList.length; t++) {
                        if (codeList[t] == item.field) {
                            inlineInfo.to = inlineList[i].toTargets;
                            inlineInfo.codeList = codeList;
                            isinline = true;
                            break;
                        }
                    }
                    break;
                }
            }
            var point = { id: item.id, label: item.name, name: item.field || Changjie.newGuid(), width: parseInt(item.width || 100), align: item.align || "left", aggtype: item.aggtype, aggcolumn: item.aggcolumn, required: item.required, datatype: item.datatype, tabname: tabname };
            if (isinline) {
                point.inlineOperation = inlineInfo;
            }

            switch (item.type) {
                case 'input':          // 输入框 文本,数字,密码
                    point.edit = {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    };
                    break;
                case 'label':
                    point.formatter = function (value, coldata, rowData, rowItem, op, rowIndex, colname) {
                        if (colname == "operationTabs") {
                            if (!!coldata.F_Id) {
                                return "<a href='#' onclick=\"downloadFile2('" + coldata.F_Id + "')\"><span style='color:blue'>下载附件</span></a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"previewFile('" + coldata.F_Id + "')\"><span style='color:blue'>在线预览</span></a>";
                            }
                        }
                        else {
                            return value;
                        }
                    };
                    break;
                case 'select':         // 下拉框选择
                    point.edit = {
                        type: 'select',
                        change: function (row, index, item, oldValue, colname, headData) {
                        }
                    };
                    if (item.dataSource == "0") {// 数据字典
                        point.edit.datatype = 'dataItem';
                        point.edit.code = item.itemCode;
                    } else {// 数据源
                        point.edit.code = item.dataSourceId;
                        point.edit.datatype = 'dataSource';
                        var strWhere = item.whereSql || "";
                        if (strWhere.indexOf("#currentProjectId#") > -1) {
                            strWhere = strWhere.replace("#currentProjectId#", "'" + projectId + "'");
                        }
                        point.edit.strWhere = strWhere || "";
                        point.edit.op = {
                            value: item.saveField,
                            text: item.showField,
                            title: item.showField,
                            strWhere: point.edit.strWhere
                        }
                    }
                    break;
                case 'radio':          // 单选
                    point.edit = {
                        type: 'radio',
                        dfvalue: item.dfvalue,
                        change: function (row, index, oldValue, colname, rows, headData) {

                        }
                    };
                    if (item.dataSource == "0") {
                        point.edit.datatype = 'dataItem';
                        point.edit.code = item.itemCode;
                    } else {
                        point.edit.datatype = 'dataSource';
                        point.edit.code = item.dataSourceId;
                        point.edit.text = item.showField;
                        point.edit.value = item.saveField;
                    }

                    break;
                case 'checkbox':       // 多选
                    point.edit = {
                        type: 'checkbox',
                        dfvalue: item.dfvalue,
                        change: function (row, index, oldValue, colname, headData) {

                        }
                    };
                    if (item.dataSource == "0") {
                        point.edit.datatype = 'dataItem';
                        point.edit.code = item.itemCode;
                    } else {
                        point.edit.datatype = 'dataSource';
                        point.edit.code = item.dataSourceId;
                        point.edit.text = item.showField;
                        point.edit.value = item.saveField;
                    }
                    break;
                case 'datetime':       // 时间
                    point.edit = {
                        type: 'datatime',
                        dateformat: (item.datetime == "date" ? '0' : '1'),
                        change: function (row, index, item, oldValue, colname, headData) {

                        }
                    };
                    break;
                case 'layer':          // 弹层
                    var layerData = [];
                    $.each(item.layerData, function (_index, _item) {
                        var _point = { label: _item.label, name: _item.name, width: parseInt(_item.width || 100), align: _item.align };
                        layerData.push(_point);
                    });

                    point.edit = {
                        type: 'layer',
                        change: function (data, rownum, selectData) {
                            $.each(item.layerData, function (_index, _item) {
                                data[_item.value] = selectData[_item.name];
                            });
                            $compont.jfGridSet('updateRow', rownum);
                        },
                        op: {
                            width: item.layerW,
                            height: item.layerH,
                            colData: layerData
                        }
                    };
                    if (item.dataSource == "0") {
                        point.edit.op.url = top.$.rootUrl + '/SystemModule/DataItem/GetDetailList';
                        point.edit.op.param = { itemCode: item.itemCode };
                    } else {
                        point.edit.op.url = top.$.rootUrl + '/SystemModule/DataSource/GetDataTable';
                        point.edit.op.param = { itemCode: item.dataSourceId };
                    };

                    break;
                case 'guid':
                    isAdd = false;
                    break;
            }
            if (isAdd) {
                mIds[item['id']] = 1;
                var parentId = item['parentId'] || '0';
                mapdata[parentId] = mapdata[parentId] || [];
                mapdata[parentId].push(point);
                if (mIds[parentId] == 1) {
                    delete pIds[parentId];
                }
                else {
                    pIds[parentId] = 1;
                }
                if (pIds[item['id']] == 1) {
                    delete pIds[item['id']];
                }
            }

        });
        for (var id in pIds) {
            _fn(resdata, id);
        }
        function _fn(_data, vparentId) {
            var pdata = mapdata[vparentId] || [];
            for (var j = 0, l = pdata.length; j < l; j++) {
                var _item = pdata[j];
                _item.children = [];
                _fn(_item.children, _item['id']);
                if (_item.children.length == 0) {
                    delete _item['children'];
                }

                _data.push(_item);
                maprowdatas[_item['id']] = _item;
            }
        }
        return resdata;
    }
    // html编码
    function htmlEncode(text) {
        text = text || "";
        text = text.replace(/%/g, "{@bai@}");
        text = text.replace(/ /g, "{@kong@}");
        text = text.replace(/</g, "{@zuojian@}");
        text = text.replace(/>/g, "{@youjian@}");
        text = text.replace(/&/g, "{@and@}");
        text = text.replace(/\"/g, "{@shuang@}");
        text = text.replace(/\'/g, "{@dan@}");
        text = text.replace(/\t/g, "{@tab@}");
        text = text.replace(/\+/g, "{@jia@}");

        return text;
    }
    // html解码
    function htmlDecode(text) {
        text = text || "";
        text = text.replace(/{@bai@}/g, "%");
        text = text.replace(/{@dan@}/g, "'");
        text = text.replace(/{@shuang@}/g, "\"");
        text = text.replace(/{@kong@}/g, " ");
        text = text.replace(/{@zuojian@}/g, "<");
        text = text.replace(/{@youjian@}/g, ">");
        text = text.replace(/{@and@}/g, "&");
        text = text.replace(/{@tab@}/g, "\t");
        text = text.replace(/{@jia@}/g, "+");

        return text;
    }



})(jQuery, top.Changjie);
function downloadFile2(fileId) {
    window.open(top.$.rootUrl + '/SystemModule/Annexes/DownAnnexesFile2?fileId=' + fileId);
}
function downloadFile(fileId) {
    var data = {
        url: top.$.rootUrl + '/SystemModule/Annexes/DownAnnexesFile',
        param:
        {
            fileId: fileId,
            __RequestVerificationToken: $.mkToken
        },
        method: 'POST'
    };
    top.Changjie.download(data);
}
function previewFile(fileId) {
    $.ajax({
        url: top.$.rootUrl + '/SystemModule/Annexes/PreviewFile',
        type: "POST",
        data: {
            fileId: fileId,
            rnd: Math.random()
        },
        success: function (responseText) {//当保存成功之后的回调          
            if (responseText == "") {
                alert("文件不存在！")
            }
            else {
                window.open("http://" + responseText);
            }
        },
        error: function () {
            $.ligerDialog.closeWaitting();
            $.ligerDialog.error('失败！');
        }
    });
}


function delrow(index) {
    var $datarows = $('#tabgridtable').jfGridGet("rowdatas");
    if (index >= 0) {
        $datarows.splice(index, 1);
        $$tabNames.splice(index, 1);
        $("div.ui-draggable-handle").find('.mk-compont-value').text($$tabNames);
        $(" div.mk-compont-item.active").find('.mk-compont-value').text($$tabNames);

        if ($datarows.length <= 0) {
            $("#addfields").hide();
        }
    }
    $('#tabgridtable').jfGridSet("refreshdata");
}




