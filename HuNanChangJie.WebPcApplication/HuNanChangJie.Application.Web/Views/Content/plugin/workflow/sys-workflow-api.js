﻿(function ($, Changjie) {
    "use strict";
    var api = {
        start: top.$.rootUrl + '/WorkFlowModule/WorkFlow/Start',
        taskinfo: top.$.rootUrl + '/WorkFlowModule/WorkFlow/GetTaskInfo',
        audit: top.$.rootUrl + '/WorkFlowModule/WorkFlow/Audit',
        history: top.$.rootUrl + '/WorkFlowModule/WorkFlow/GetHistory',
        workfolwinfo: top.$.rootUrl + '/WorkFlowModule/WfEngine/GetWorkFlowId',
    };

    var httpGet = function (url, param, callback, loadmsg) {
        Changjie.loading(true, loadmsg || '正在获取数据');
        Changjie.httpAsync('GET', url, param, function (res) {
            Changjie.loading(false);
            callback(res);
        });
    };

    var httpPost = function (url, param, callback, loadmsg) {
        Changjie.loading(true, loadmsg || '正在获取数据');
        Changjie.httpAsync('Post', url, param, function (data) {
            Changjie.loading(false);
            callback(data);
        });
    };

    Changjie.workflowApi = {
        start: function (op) {
            var dfop = {
                projectId: '',
                infoId: '',
                schemeId: '',
                formId: '',
                workflowName: '',
                level:''
            };
            $.extend(dfop, op);

            var param = {
                projectId: dfop.projectId,
                infoId: dfop.infoId,
                schemeId: dfop.schemeId,
                formId: dfop.formId,
                workflowName: dfop.workflowName,
                level: dfop.level,
                description: dfop.description,
                workflowId: dfop.workflowId,
                isNew: dfop.isNew
            }

            httpPost(api.start, param, function (res) {
                if (res != null) {
                    if (res.status == 1) {
                        op.callback(true);
                    }
                    else {
                        Changjie.alert.error(res.desc);
                        op.callback(false);
                    }
                }
                else {
                    Changjie.alert.error('创建流程失败!');
                    op.callback(false);
                }
            },'正在创建流程实例...');
        },
        taskinfo: function (op) {
            var dfop = {
                workflowId:"",
                taskId:""
            };
            $.extend(dfop, op);
            var param = {
                workflowId:dfop.workflowId,
                taskId: dfop.taskId,
                isLook: dfop.isLook
            }
            httpGet(api.taskinfo, param, function (res) {
                if (res != null) {
                    if (res.status == 1) {
                        op.callback(true, res.data);
                    }
                    else {
                        Changjie.alert.error(res.desc);
                        op.callback(false);
                    }
                }
                else {
                    Changjie.alert.error('获取流程信息失败!');
                    op.callback(false);
                }
            }, '正在获取流程信息...');
        },
        audit: function (op) {
            var dfop = {
                taskId: "",
                description: "",
                verifyType: "",
                nopassType: "",
                addUserId:"",
            };
            $.extend(dfop, op);
            var param = {
                taskId: dfop.taskId,
                description: dfop.description,
                verifyType: dfop.verifyType,
                nopassType: dfop.nopassType,
                toNode: dfop.toNode,
                addUserId: dfop.addUserId
            };
            httpPost(api.audit, param, function (res) {
                if (res != null) {
                    if (res.IsPass == true) {
                        op.finishCallBack();
                    }
                    if (res.status == 1) {
                        op.callback(true);
                    }
                    else {
                        Changjie.alert.error(res.desc);
                        op.callback(false);
                    }
                }
                else {
                    Changjie.alert.error('流程审核失败!');
                    op.callback(false);
                }
            }, '正在审核流程实例...');
        },
        history: function (op) {
            var dfop = {
                workflowId: '',
            }
            $.extend(dfop, op);
            var req = {
                workflowId: dfop.workflowId,
            };
            httpGet(api.history, req, function (res) {
                if (res != null) {
                    if (res.status == 1) {
                        op.callback(true, res.data);
                    }
                    else {
                        Changjie.alert.error(res.desc);
                        op.callback(false);
                    }
                }
                else {
                    op.callback(false);
                }
            }, '正在获取流程信息...');
            
        },
        workfolwinfo: function (op) {
            var dfop = {
                tableName: "",
                keyValue: ""
            };
            $.extend(dfop, op);
            var req = {
                tableName: dfop.tableName,
                keyValue: dfop.keyValue
            };
            var data = Changjie.httpGet(api.workfolwinfo, req).info;
            return data;
        },
    };

})(jQuery, top.Changjie);