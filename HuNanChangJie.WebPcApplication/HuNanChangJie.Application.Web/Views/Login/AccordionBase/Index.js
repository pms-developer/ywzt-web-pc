﻿/*
 * 日 期：2018.08.08
 * 描 述：登录页面前端脚本
 */
(function($) {
    "use strict";

    var mkPage = {
        init: function() {
            //$('.mk-login-body').css('background',
            //    'url(' + $.rootUrl + '/Content/images/Login/back.png) no-repeat center');
            $('#psw_change').css({
                'background': 'url(' + $.rootUrl + '/Content/images/Login/psw0.png) no-repeat center center'
            });

            var error = request('error');
            if (error == "ip") {
                mkPage.tip("登录IP限制");
            } else if (error == "time") {
                mkPage.tip("登录时间限制");
            }

            if (window.location.href != top.window.location.href) {
                top.window.location.href = window.location.href;
            }
            var isIE = !!window.ActiveXObject;
            var isIE6 = isIE && !window.XMLHttpRequest;
            if (isIE6) {
                window.location.href = $.rootUrl + "/Error/ErrorBrowser";
            }
            mkPage.bind();
        },
        bind: function() {
            // 回车键
            document.onkeydown = function(e) {
                e = e || window.event;
                if ((e.keyCode || e.which) == 13) {
                    $('#login_btn').trigger('click');
                }
            }

            // 点击切换验证码
            $("#verifycode_img").click(function() {
                $("#verifycode_input").val('');
                $("#verifycode_img").attr("src", $.rootUrl + "/Login/VerifyCode?time=" + Math.random());
            });
            var errornum = $('#errornum').val();
            if (errornum >= 3) {

                $('.mk-login-bypsw').removeClass('noreg');
                $("#verifycode_img").trigger('click');
            }

            //点击密码icon  显示／隐藏
            $('#psw_change').click(function(event) {
                var event = event || window.event;
                event.stopPropagation();
                var $this = $(this);
                $this.toggleClass('psw_show');
                //如果当前隐藏  变显示
                if ($this.hasClass('psw_show')) {
                    $this.css({
                        'background': 'url(' + $.rootUrl + '/Content/images/Login/psw1.png) no-repeat center center'
                    });
                    $this.prev().attr('type', 'text');
                } else {
                    $this.css(
                        'background',
                        'url(' + $.rootUrl + '/Content/images/Login/psw0.png) no-repeat center center'
                    );
                    $this.prev().attr('type', 'password');
                }
            });

            //登录方式点击
            $('.mk-login-toCode').click(function() {
                var _this = $(this);
                if (_this.attr('login-access') == 'psw') {
                    $('.mk-login-bycode').show();
                    $('.mk-login-bypsw').hide();

                } else {
                    $('.mk-login-bypsw').show();
                    $('.mk-login-bycode').hide();

                }
            })

            // 登录按钮事件
            $("#login_btn").on('click',
                function() {
                    mkPage.login();
                });
        },
        login: function() {
            mkPage.tip();

            var $username = $("#username"), $password = $("#password"), $verifycode = $("#verifycode_input");
            var username = $.trim($username.val()),
                password = $.trim($password.val()),
                verifycode = $.trim($verifycode.val());

            if (username == "") {
                mkPage.tip('请输入账户。');
                $username.focus();
                return false;
            }
            if (password == "") {
                mkPage.tip('请输入密码。');
                $password.focus();
                return false;
            }

            if ($("#verifycode_input").is(":visible") && verifycode == "") {
                mkPage.tip('请输入验证码。');
                $verifycode.focus();
                return false;
            }
            password = $.md5(password);
            mkPage.logining(true);
            $.ajax({
                url: $.rootUrl + "/Login/CheckLogin",
                headers: { __RequestVerificationToken: $.mkToken },
                data: { username: username, password: password, verifycode: verifycode },
                type: "post",
                dataType: "json",
                success: function(res) {
                    if (res.code == 200) {
                        window.location.href = $.rootUrl + '/Home/Index';
                    } else if (res.code == 400) {
                        mkPage.logining(false);
                        mkPage.tip(res.info, true);
                        $('#errornum').val(res.data);
                        if (res.data >= 3) {
                            $('#verifycode_input').parent().show();
                            $("#verifycode_img").trigger('click');
                        }
                    } else if (res.code == 500) {
                        console.error(res.info);
                        mkPage.logining(false);
                        mkPage.tip('服务端异常，请联系管理员', true);
                    }
                }
            });
        },
        logining: function(isShow) {
            if (isShow) {
                $('input').attr('disabled', 'disabled');
                $("#login_btn").addClass('active').attr('disabled', 'disabled').find('span').hide();
                $("#login_btn").css('background',
                    '#eeecec url(/Content/images/Login/loading.gif) no-repeat center 10px');

            } else {
                $('input').removeAttr('disabled');
                $("#login_btn").removeClass('active').removeAttr('disabled').find('span').show();
                $("#login_btn").css('background', '#156EDB');

            }
        },
        tip: function(msg) {
            var $tip = $('.mk-login-warning');
            $tip.hide();
            if (!!msg) {
                $tip.find('span').html(msg);
                $('.mk-login-head-sub').hide();
                $tip.show();
            }
        }
    };
    $(function() {
        mkPage.init();
    });
})(window.jQuery);

 