﻿/*
 * 日 期：2017.04.11
 * 描 述：excel 数据导出	
 */
var gridId = request('gridId');
var filename = decodeURI(request('filename'));
var fildSetdata = "";
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            var columnModel = Changjie.frameTab.currentIframe().$('#' + gridId).jfGridGet('settingInfo').headData;
            var $ul = $('.sys_spec_text');
            $.each(columnModel, function (i, item) {
                var label = item.label;
                var name = item.name;
                if (!!label) {
                    $(".sys_spec_text").append("<li data-value='" + name + "' title='" + label + "'><a>" + label + "</a><i></i></li>");
                }
            });
            $(".sys_spec_text li").addClass("active");
            $(".sys_spec_text li").click(function () {
                if (!!$(this).hasClass("active")) {
                    $(this).removeClass("active");
                } else {
                    $(this).addClass("active").siblings("li");
                }
            });
            var mkModule = Changjie.frameTab.currentIframe().mkCurrentModuleId;
            //根据模块id获取当前导出按钮配置的取数规则
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/ExcelExport/GetList', { moduleId: mkModule }, function (dataExc) {
                if (!!dataExc && dataExc.length > 0) {
                    fildSetdata = dataExc[0].F_FieldSettings;
                }
            });
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        var exportField = [];
        $('.sys_spec_text ').find('li.active').each(function () {
            var value = $(this).attr('data-value');
            exportField.push(value);
        });
        var strHeadData = Changjie.frameTab.currentIframe().$('#' + gridId).jfGridGet('settingInfo').headData;        
        var columnJson = JSON.stringify(strHeadData);
        var newHeadData = JSON.parse(columnJson);
        var op = Changjie.frameTab.currentIframe().$('#' + gridId).jfGridGet('settingInfo');

        if (op && op.url) {
            Changjie.httpAsync('GET', op.url, { queryJson: op.param.queryJson, pagination: JSON.stringify({ "rows": 100000, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                if (data == null) {
                    //if (data.length <= 0 || data.rows.length <= 0) {
                    top.Changjie.alert.info("导出失败;当前无数据可导出");
                    return;
                    //}
                }
                var rowJson;
                if (data.rows != undefined) {
                    rowJson = JSON.stringify(data.rows);
                } else {
                    rowJson = JSON.stringify(data);
                }

                if (fildSetdata != "" && fildSetdata != null && fildSetdata != undefined) {
                    var fildConfig = JSON.parse(fildSetdata);//后台获取当前页面配置数据，返回json数组
                    //如果导出按钮有配置取数规则 
                    var isExistSet = false;
                    var fieldSetList = [];
                    $.each(fildConfig, function (index, setItem) {
                        var sfieldCode = setItem.fieldCode;//导出设置字段code
                        var sfieldText = setItem.fieldText;//导出设置的字段名称 
                        var newFieldName = sfieldCode + "_Name"; //添加一个对应的Name列
                        //判断所选列中是否包含定义字段
                        for (var i = 0; i < exportField.length; i++) {
                            if (sfieldCode == exportField[i]) {
                                //exportField.splice(i, 1);//移除当前列
                                //exportField.push(newFieldName);
                                //表头对应增加名称列
                                //var sHead = JSON.parse("{\"label\":\"" + sfieldText + " \",\"name\":\"" + newFieldName + "\",\"width\":250,\"align\":\"left\",\"height\":28}");
                                //strHeadData.push(sHead);
                                exportField[i] = newFieldName;
                                for (var h = 0; h < newHeadData.length; h++) {
                                    if (sfieldCode == newHeadData[h].name) {
                                        newHeadData[h].name = newFieldName;
                                        break;
                                    }
                                }
                                isExistSet = true;
                                fieldSetList.push(setItem);
                                break;
                            }
                        }
                    });

                    if (isExistSet) {
                        //如果存在字段取值设置，给data添加对应主键的文本字段值
                        var newRowJson = JSON.parse(rowJson);
                        $.each(newRowJson, function (j, item) {
                            $.each(fieldSetList, function (index, setItem) {
                                var sfieldCode = setItem.fieldCode;//导出设置字段code
                                var sdataType = setItem.dataType;//数据来源类型
                                var sdataSourceCode = setItem.dataSourceCode;//数据源
                                var sdataItemCode = setItem.dataItemCode;//数据字典  
                                var sfieldKey = setItem.FieldKey;//查询字段名称  
                                var sfieldValue = setItem.FieldValue;//绑定值字段 
                                var newFieldName = sfieldCode + "_Name";
                                var sKey = item[sfieldCode];
                                switch (sdataType) {
                                    case "sjy":
                                        Changjie.clientdata.getAsync('sourceDataItem', {
                                            code: sdataSourceCode,
                                            key: sKey,
                                            keyId: sfieldKey,
                                            callback: function (_data) {
                                                var dataValue = _data[sfieldValue];
                                                newRowJson[j][newFieldName] = dataValue != undefined && dataValue != null ? dataValue : "";
                                            }
                                        });
                                        break;
                                    case "sjzd":
                                        Changjie.clientdata.getAsync('dataItem', {
                                            code: sdataItemCode,//字典分类编码
                                            key: sKey,
                                            callback: function (_data) {
                                                var dataValue = _data.text;
                                                newRowJson[j][newFieldName] = dataValue != undefined && dataValue != null ? dataValue : "";
                                            }
                                        });
                                        break;
                                }
                            });
                        });
                        rowJson = JSON.stringify(newRowJson);
                        columnJson = JSON.stringify(newHeadData);
                    }
                }

                //console.log(rowJson, data, 89)

                Changjie.download({
                    method: "POST",
                    url: '/Utility/ExportExcel',
                    param: {
                        fileName: filename,
                        columnJson: columnJson,
                        dataJson: rowJson,
                        exportField: String(exportField)
                    }
                });
            })
        }

    };
    page.init();
}