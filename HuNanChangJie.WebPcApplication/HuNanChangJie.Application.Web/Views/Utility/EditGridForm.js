﻿/*
 * 日 期：2018.04.28
 * 描 述：表格字段编辑	
 */
var keyValue = request('keyValue');
 
var gridSetting = top[keyValue];

var bootstrap = function ($, Changjie) {
    "use strict";

    var currentRow = null;
    var setFlag = false;
    function initData(data) {
        var colDatas = [];
        var map = {};
        $.each(gridSetting.fields, function (_index, _item) {
            map[_item.field] = _item;
        });
        $.each(data, function (_index, _item) {
            var point = {
                isHide: 0,
                field: _item.f_column,
                name: _item.f_remark,
                width: 80,
                align: 'left',
                type: 'input'
            };
            if (map[_item.field]) {
                point = map[_item.field];
            }
            colDatas.push(point);
        });
        gridSetting.fields = colDatas;

    }

    function setRowOp() {
        setFlag = true;
        var res = [];
        var parentId = currentRow.parentId;
        $('.field').show();
        $.each(gridSetting.fields, function (_index, _item) {
            if (_item.field === '' && _item.name !== '' && _item.id !== currentRow.id) {
                res.push({ 'id': _item.id, 'text': _item.name });
            }
            if (currentRow.id === _item.parentId) {
                $('.field').hide();
            }
        });
        $('#parentId').mkselectRefresh({
            data: res
        });
        
        //$('#colop').mkSetFormData(currentRow);
        $('#name').val(currentRow.name);
        $('#parentId').mkselectSet(parentId);
        $('#field').mkselectSet(currentRow.field);
        $('#align').mkselectSet(currentRow.align);
        $('#width').val(currentRow.width);
        $('#formula').val(currentRow.formula);
        $("#aggtype").mkselectSet(currentRow.aggtype);
        $("#aggcolumn").mkselectSet(currentRow.aggcolumn);
        $("#required").mkselectSet(currentRow.required);
        $("#datatype").mkselectSet(currentRow.datatype);
        $("#dfcondition").val(currentRow.whereSql);
        $("#inlinetype").mkselectSet(currentRow.inlinetype);
        
        $("#inlineTarget1").mkselectSet(currentRow.inlineTarget1);
        
        $("#inlineTarget2").mkselectSet(currentRow.inlineTarget2);
 
        $('#type').mkselectSet(currentRow.type);
      
        if (currentRow.aggtype =="normal") {
            $(".aggcolumn").hide();
        }
        else {
            $(".aggcolumn").show();
        }

        //if (currentRow.datatype != "string") {
        //    if (!!currentRow.datatype) {
        //        $(".inlineOperation").show();
        //    }
        //    else {
        //        $(".inlineOperation").hide();
        //    }
        //}
        //else {
        //    $(".inlineOperation").hide();
        //}

        switch (currentRow.type) {
            case 'label':
            case 'input':
            case 'guid':
                break;
            case 'radio':
            case 'checkbox':
            case 'select':
                $('#dataSource').mkselectSet(currentRow.dataSource);
                $('#dfvalue').mkselectSet(currentRow.dfvalue);
                if (currentRow.dataSource === '0') {
                    $('#dataItemId').mkselectSet(currentRow.itemCode);
                }
                else {
                    $('#dataSourceId').mkselectSet(currentRow.dataSourceId);
                    $('#showField').mkselectSet(currentRow.showField);
                    $('#saveField').mkselectSet(currentRow.saveField);
                }
                break;
            case 'layer':
                $('#layerW').val(currentRow.layerW || '');
                $('#layerH').val(currentRow.layerH || '');
                break;
            case 'datetime':
                $('#datetime').mkselectSet(currentRow.datetime);
                break;
        }

        setFlag = false;
    }

    // 设置弹层显示数据
    function setLayerGridData(data) {
        currentRow.layerData = data || [
            { label: '项目名', name: 'F_ItemName', value: '', width: '100', align: 'left', hide: 0 },
            { label: '项目值', name: 'F_ItemValue', value: '', width: '100', align: 'left', hide: 0 }
        ];
        $('#layerGrid').jfGridSet('refreshdata', currentRow.layerData);
    }


    var page = {
        init: function () {
            page.bind();
            page.refreshInlineTarget();
        },
        bind: function () {
            $('#colopWrap').mkscroll();
            // 显示名称
            $('#name').on("input propertychange", function (e) {
                var value = $(this).val();
                currentRow['name'] = value;
                $('#edit_grid').jfGridSet('updateRow');
            });
            $("#formula").on("input propertychange", function (e) {
                var value = $(this).val();
                currentRow.formula = value;
                $('#edit_grid').jfGridSet('updateRow');
            });
           
            // 字段绑定
            $('#field').mkselect({
                value: 'f_column',
                text: 'f_column',
                title: 'f_remark',
                allowSearch: true,
                select: function (item) {
                    if (item) {
                        currentRow.field = item.f_column;
                        if (currentRow.name === '') {
                            currentRow.name = item.f_remark;
                            $('#name').val(currentRow.name);
                        }
                        $('.lastcol').show();
                    }
                    else {
                        if (currentRow) {
                            currentRow.field = '';
                        }
                        $('.lastcol').hide();
                    }

                    $('#edit_grid').jfGridSet('updateRow');
                }
            });

            // 对齐方式
            $('#align').mkselect({
                placeholder: false,
                data: [
                    { 'id': 'left', 'text': '靠左' },
                    { 'id': 'center', 'text': '居中' },
                    { 'id': 'right', 'text': '靠右' }
                ],
                select: function (item) {
                    currentRow.align = item.id;
                    $('#edit_grid').jfGridSet('updateRow');
                }
            });

            // 字段宽度
            $('#width').on("input propertychange", function (e) {
                var value = $(this).val();
                currentRow['width'] = value;
            });

            $("#aggtype").mkselect({
                placeholder: false,
                data: [
                    { 'id': 'normal', 'text': '无' },
                    { 'id': 'sum', 'text': '求和' },
                    { 'id': 'average', 'text': '平均' },
                    { 'id': 'max', 'text': '最大值' },
                    { 'id': 'mix', 'text': '最小值' },
                    { 'id': 'sumcols', 'text': '合计行数' },
                ],
                select: function (item) {
                    if (item) {
                        currentRow.aggtype = item.id;

                        if (item.id == "normal") {
                            $("#aggcolumn").mkselectSet("");
                            $(".aggcolumn").hide();
                        }
                        else {
                            $(".aggcolumn").show();
                        }

                        $('#layerop').hide();
                        $('#layerop').parent().removeClass('layerop');
                        $('.layer').hide();

                    }
                }
            });

            $('#aggcolumn').mkselect({
                placeholder: false,
                data: gridSetting.mainTableFields,
                select: function (item) {
                    if (item) {
                        currentRow.aggcolumn = item.id;
                    }
                }
            });

            $("#required").mkselect({
                placeholder: false,
                data: [
                    { 'id': '0', 'text': '否' },
                    { 'id': '1', 'text': '是' }
                ],
                select: function (item) {
                    if (!item) {
                        currentRow.required = '0';
                    }
                    else {
                        currentRow.required = item.id;
                    }
                }
            });

            $("#datatype").mkselect({
                placeholder: false,
                data: [
                    {'id':'string','text':'字符串'},
                    { 'id': 'int', 'text': '整数' },
                    { 'id': 'float', 'text': '浮点数' },
                ],
                select: function (item) {
                    if (!item) {
                        currentRow.datatype = 'string';
                    }
                    else {
                        currentRow.datatype = item.id;
                    }
                   
                    if (currentRow.datatype == "int" || currentRow.datatype == "float") {
                        $(".numtype").show();
                        if (currentRow.inlinetype != "none") {
                            $(".inlineOperationTarget").show();
                        }
                        else {
                            $(".inlineOperationTarget").hide();
                        }
                    }
                    else {
                        $(".numtype").hide();
                    }
                }
            });

            $("#inlinetype").mkselect({
                placeholder: false,
                data: [
                    { 'id': 'none', 'text': '不计算' },
                    { 'id': 'add', 'text': '加' },
                    { 'id': 'sub', 'text': '减' },
                    { 'id': 'mul', 'text': '乘' },
                    { 'id': 'div', 'text': '除' },
                    { 'id': 'formula','text':'公式运算'},
                ],
                select: function (item) {
                    if (!item) {
                        currentRow.inlinetype = 'none';
                        $(".inlineOperationTarget").hide();
                    }
                    else {
                        if (item.id == "none") {
                            $("#inlineTarget1").mkselectSet("none");
                            $("#inlineTarget2").mkselectSet("none");
                            $("#formula").val("");
                            $(".inlineOperationTarget").hide();
                        }
                        else if (item.id == "formula") {
                            $("#inlineTarget1").mkselectSet("none");
                            $("#inlineTarget2").mkselectSet("none");
                            $(".inlineOperationTarget").hide();
                            $("#formula-div1").show();
                            $("#formula-div2").show();
                        }
                        else {
                            $(".inlineOperationTarget").show();
                            $("#formula-div1").hide();
                            $("#formula-div2").hide();
                        }
                        currentRow.inlinetype = item.id;
                    }
                    //if (currentRow.inlinetype != 'none') {
                    //    $(".inlineOperationTarget").show();
                    //}
                    //else {
                    //    $(".inlineOperationTarget").hide();
                    //}
                }
            });

            
            $("#inlineTarget1").mkselect({
                placeholder: false,
                data: page.getCurrentRows(),
                select: function (item) {
                    if (!!currentRow) {
                        if (!item) {
                            currentRow.inlineTarget1 = 'none';
                        }
                        else {
                            currentRow.inlineTarget1 = item.id;
                        }
                    }
                }
            });

            $("#inlineTarget2").mkselect({
                placeholder: false,
                data: page.getCurrentRows(),
                select: function (item) {
                    if (!!currentRow) {
                        if (!item) {
                            currentRow.inlineTarget2 = 'none';
                        }
                        else {
                            currentRow.inlineTarget2 = item.id;
                        }
                    }
                }
            });

            // 字段类型
            $('#type').mkselect({
                placeholder: false,
                data: [
                    { 'id': 'label', 'text': '文本' },
                    { 'id': 'input', 'text': '输入框' },
                    { 'id': 'select', 'text': '下拉框' },
                    { 'id': 'radio', 'text': '单选框' },
                    { 'id': 'checkbox', 'text': '多选框' },
                    { 'id': 'layer', 'text': '弹层选择框' },
                    { 'id': 'datetime', 'text': '日期' },
                    { 'id': 'guid', 'text': 'GUID' }
                ],
                select: function (item) {
                    
                    currentRow.type = item.id;
                    $('#layerop').hide();
                    $('#layerop').parent().removeClass('layerop');
                    $('.layer').hide();

                    switch (item.id) {
                        case 'label':
                        case 'input':
                        case 'guid':
                            $('.datasource').hide();
                            $('.datasource1').hide();
                            $('.datetime').hide();
                            break;
                        case 'radio':
                        case 'checkbox':
                        case 'select':
                            $('.datetime').hide();
                            $('.datasource').show();
                            $('#dataSource').mkselectSet(currentRow.dataSource);
                            if (currentRow.dataSource === '0') {
                                $('#dataItemId').mkselectSet(currentRow.itemCode || currentRow.dataItemCode);
                            }
                            else {
                                $('#dataSourceId').mkselectSet(currentRow.dataSourceId);
                                $('#showField').mkselectSet(currentRow.showField);
                                $('#saveField').mkselectSet(currentRow.saveField);
                            }
                            break;
                        case 'layer':
                            $('.datetime').hide();
                            $('.datasource1').hide();
                            $('.datasource').show();

                            $('.dfvalue').hide();
                            $('#dataSource').mkselectSet(currentRow.dataSource);
                            if (currentRow.dataSource === '0') {
                                $('#dataItemId').mkselectSet(currentRow.itemCode || currentRow.dataItemCode);
                            }
                            else {
                                $('#dataSourceId').mkselectSet(currentRow.dataSourceId);
                            }
                            $('.layer').show();
                            $('#layerop').show();
                            $('#layerop').parent().addClass('layerop');


                            break;
                        case 'datetime':
                            $('.datetime').show();
                            $('.datasource').hide();
                            $('.datasource1').hide();
                            break;
                    }
                    $('#edit_grid').jfGridSet('updateRow');
                }
            });

            // 上级列头
            $('#parentId').mkselect({
                select: function (item) {
                    if (!item) {
                        currentRow.parentId = '';
                    }
                    else {
                        if (currentRow.parentId !== item.id) {
                            currentRow.parentId = item.id;
                        }
                    }

                    if (!setFlag) {
                        $('#edit_grid').jfGridSet('refreshdata', gridSetting.fields);
                    }
                }
            });

            // 数据来源
            $('#dfvalue').mkselect({
                allowSearch: true,
                select: function (item) {
                    if (item) {
                        if (currentRow.dataSource === '0') {
                            currentRow.dfvalue = item.id;
                        }
                        else {
                            currentRow.dfvalue = item[currentRow.saveField];
                        }
                    }
                    else {
                        currentRow.dfvalue = '';
                    }
                }
            });

            // 数据字典选项
            $('#dataItemId').mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/SystemModule/DataItem/GetClassifyTree',
                type: 'tree',
                value: 'value',
                select: function (item) {
                    if (item) {
                        if (currentRow.dataSourceId !== item.id) {
                            currentRow.dfvalue = '';
                            currentRow.itemCode = item.value;
                        }

                        if (currentRow.type === 'radio' || currentRow.type === 'checkbox' || currentRow.type === 'select') {

                            Changjie.clientdata.getAllAsync('dataItem', {
                                code: item.value,
                                callback: function (dataes) {
                                    var list = [];
                                    $.each(dataes, function (_index, _item) {
                                        if (_item.parentId === "0") {
                                            list.push({ id: _item.value, text: _item.text, title: _item.text, k: _index });
                                        }
                                    });
                                    var _value = currentRow.dfvalue;
                                    $('#dfvalue').mkselectRefresh({
                                        value: "id",
                                        text: "text",
                                        title: "title",
                                        data: list,
                                        url: ''
                                    });
                                    if (_value !== '' && _value !== undefined && _value !== null) {
                                        $('#dfvalue').mkselectSet(_value);
                                    }

                                }
                            });
                        }
                    }
                    else {
                        currentRow.itemCode = '';
                        currentRow.dataItemCode && (currentRow.dataItemCode = '');
                        if (currentRow.type === 'radio' || currentRow.type === 'checkbox' || currentRow.type === 'select') {
                            currentRow.dfvalue = '';
                            $('#dfvalue').mkselectRefresh({ url: '', data: [] });
                        }
                    }
                }
            });
            $("#dfcondition").on("input propertychange", function () {
                currentRow.whereSql = $(this).val();
            });
            // 数据源
            $('#showField').mkselect({
                title: 'text',
                text: 'text',
                value: 'value',
                allowSearch: true,
                select: function (item) {
                    if (item) {
                        currentRow.showField = item.value;
                        if (currentRow.saveField !== '') {
                            Changjie.clientdata.getAllAsync('sourceData', {
                                code: currentRow.dataSourceId,
                                callback: function (dataes) {
                                    var _value = currentRow.dfvalue;
                                    $('#dfvalue').mkselectRefresh({
                                        value: currentRow.saveField,
                                        text: currentRow.showField,
                                        title: currentRow.showField,
                                        url: '',
                                        data: dataes
                                    });

                                    if (_value !== '' && _value !== undefined && _value !== null) {
                                        $('#dfvalue').mkselectSet(_value);
                                    }
                                }
                            });
                        }
                    }
                }
            });
            $('#saveField').mkselect({
                title: 'text',
                text: 'text',
                value: 'value',
                allowSearch: true,
                select: function (item) {
                    if (item) {
                        currentRow.saveField = item.value;
                        if (currentRow.showField !== '') {
                            Changjie.clientdata.getAllAsync('sourceData', {
                                code: currentRow.dataSourceId,
                                callback: function (dataes) {
                                    var _value = currentRow.dfvalue;
                                    $('#dfvalue').mkselectRefresh({
                                        value: currentRow.saveField,
                                        text: currentRow.showField,
                                        title: currentRow.showField,
                                        url: '',
                                        data: dataes
                                    });

                                    if (_value !== '' && _value !== undefined && _value !== null) {
                                        $('#dfvalue').mkselectSet(_value);
                                    }
                                }
                            });
                        }
                    }
                }
            });
            $('#dataSourceId').mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/SystemModule/DataSource/GetList',
                value: 'F_Code',
                text: 'F_Name',
                title: 'F_Name',
                select: function (item) {
                    var flag = true;
                    if (item) {
                        if (currentRow.dataSourceId !== item.F_Code) {
                            currentRow.showField = '';
                            currentRow.saveField = '';
                            currentRow.dfvalue = '';
                            flag = false;
                        }

                        currentRow.dataSourceId = item.F_Code;
                        if (currentRow.type === 'radio' || currentRow.type === 'checkbox' || currentRow.type === 'select') {
                            var _value = currentRow.dfvalue;
                            $('#dfvalue').mkselectRefresh({ url: '', data: [] });
                            currentRow.dfvalue = _value;
                            //获取字段数据
                            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DataSource/GetDataColName', { code: item.F_Code }, function (data) {
                                var fieldData = [];
                                for (var i = 0, l = data.length; i < l; i++) {
                                    var id = data[i];
                                    var selectpoint = { value: id, text: id };
                                    fieldData.push(selectpoint);
                                }
                                $('#showField').mkselectRefresh({
                                    data: fieldData,
                                    placeholder: false
                                });
                                $('#showField').mkselectSet(currentRow.showField || fieldData[0].value);
                                $('#saveField').mkselectRefresh({
                                    data: fieldData,
                                    placeholder: false
                                });
                                $('#saveField').mkselectSet(currentRow.saveField || fieldData[0].value);
                            });
                        }
                        else if (currentRow.type === 'layer') {
                            if (!flag || currentRow.layerData.length === 0) {
                                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DataSource/GetDataColName', { code: item.F_Code }, function (data) {
                                    var fieldData = [];
                                    $.each(data, function (_index, _item) {
                                        var selectpoint = { label: '', name: _item, value: '', width: '100', align: 'left', hide: 0 };
                                        fieldData.push(selectpoint);
                                    });
                                    setLayerGridData(fieldData);
                                });
                            }
                            else {
                                setLayerGridData(currentRow.layerData);
                            }
                        }
                    }
                    else {
                        currentRow.dataSourceId = '';
                        if (currentRow.type === 'radio' || currentRow.type === 'checkbox' || currentRow.type === 'select') {
                            currentRow.showField = '';
                            currentRow.saveField = '';
                            currentRow.dfvalue = '';

                            $('#showField').mkselectRefresh({
                                data: [],
                                placeholder: '请选择'
                            });
                            $('#saveField').mkselectRefresh({
                                data: [],
                                placeholder: '请选择'
                            });
                            $('#dfvalue').mkselectRefresh({ url: '', data: [] });
                        } else if (currentRow.type === 'layer') {
                            setLayerGridData([]);
                        }
                    }
                }
            });
            $('#dataSource').mkselect({
                data: [{ id: '0', text: '数据字典' }, { id: '1', text: '数据源' }],
                value: 'id',
                text: 'text',
                placeholder: false,
                select: function (item) {
                    if (currentRow) {
                        if (currentRow.dataSource === '0' && item.id === '1') {
                            currentRow.layerData = [];
                        }
                        else if (currentRow.dataSource === '1' && item.id === '0') {
                            currentRow.layerData = [
                                { label: '项目名', name: 'F_ItemName', value: '', width: '100', align: 'left', hide: 0 },
                                { label: '项目值', name: 'F_ItemValue', value: '', width: '100', align: 'left', hide: 0 }
                            ];
                        }

                        currentRow.dataSource = item.id;
                    }
                    if (item.id === '0') {
                        $('#dataSourceId').hide();
                        $('#dataItemId').show();
                        $('.datasource1').hide();

                        if (currentRow.type === 'layer') {
                            setLayerGridData(currentRow.layerData);
                        }
                    }
                    else {
                        $('#dataItemId').hide();
                        $('#dataSourceId').show();

                        if (currentRow.type === 'radio' || currentRow.type === 'checkbox' || currentRow.type === 'select') {
                            $('.datasource1').show();
                        }
                        else {
                            $('.datasource1').hide();
                        }

                        if (currentRow.type === 'layer') {
                            setLayerGridData(currentRow.layerData || []);
                        }
                    }
                }
            });

            // 弹层数据设置
            $('#layerGrid').jfGrid({
                headData: [
                    {
                        label: "字段名", name: "label", width: 150, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "字段ID", name: "name", width: 150, align: "left" },
                    {
                        label: "绑定字段", name: "value", width: 150, align: "left",
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                                $self.mkselectRefresh({
                                    data: gridSetting.fieldMap[gridSetting.dbId + gridSetting.tableName]
                                });
                            },
                            op: {
                                value: 'f_column',
                                text: 'f_column',
                                title: 'f_remark',
                                allowSearch: true
                            }
                        }
                    },
                    {
                        label: "宽度", name: "width", width: 70, align: "center",
                        edit: {
                            type: 'input'
                        }
                    },
                    {
                        label: "对齐", name: "align", width: 70, align: "center",
                        edit: {
                            type: 'select',
                            op: {
                                placeholder: false,
                                data: [
                                    { 'id': 'left', 'text': '靠左' },
                                    { 'id': 'center', 'text': '居中' },
                                    { 'id': 'right', 'text': '靠右' }
                                ]
                            }
                        }
                    },
                    {
                        label: "隐藏", name: "hide", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var v = $cell.find('span').text();
                                if (v === '是') {
                                    row.hide = 0;
                                    $cell.html('<span class=\"label label-success \" style=\"cursor: pointer;\">否</span>');
                                }
                                else {
                                    row.hide = 1;
                                    $cell.html('<span class=\"label label-default \" style=\"cursor: pointer;\">是</span>');
                                }

                                return false;
                            });


                            if (value === 1) {
                                return '<span class=\"label label-default \" style=\"cursor: pointer;\">是</span>';
                            } else if (value === 0) {
                                return '<span class=\"label label-success \" style=\"cursor: pointer;\">否</span>';
                            }
                        }
                    },// 1 隐藏 0 显示
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#layerGrid').jfGridSet('moveUp', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#layerGrid').jfGridSet('moveDown', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-warning\" style=\"cursor: pointer;\">下移</span>';
                        }
                    }

                ],
                isShowNum: false
            });
            // 弹层宽
            $('#layerW').on("input propertychange", function (e) {
                var value = $(this).val();
                currentRow['layerW'] = value;
            });
            // 弹层高
            $('#layerH').on("input propertychange", function (e) {
                var value = $(this).val();
                currentRow['layerH'] = value;
            });

            // 日期格式
            $('#datetime').mkselect({
                placeholder: false,
                data: [
                    { 'id': 'date', 'text': '仅日期' },
                    { 'id': 'datetime', 'text': '日期和时间' }
                ],
                select: function (item) {
                    if (currentRow) {
                        if (item) {
                            currentRow.datetime = item.id;
                        }
                        else {
                            currentRow.datetime = 'date';
                        }
                    }
                }
            }).mkselectSet('date');


            $('#edit_grid').jfGrid({
                headData: [
                    {
                        label: "显示名称", name: "name", width: 200, align: "left"
                    },
                    {
                        label: "绑定字段", name: "field", width: 200, align: "left"
                    },
                    {
                        label: "类型", name: "type", width: 100, align: "center",
                        formatter: function (value, row, op, $cell) {
                            switch (value) {
                                case 'label':
                                    return '文本';
                                case 'input':          // 输入框 文本,数字,密码
                                    return '输入框';
                                case 'select':         // 下拉框选择
                                    return '下拉框';
                                case 'radio':          // 单选
                                    return '单选框';
                                case 'checkbox':       // 多选
                                    return '多选框';
                                case 'datetime':       // 时间
                                    return '日期';
                                case 'layer':          // 弹层
                                    return '弹层选择框';
                                case 'guid':
                                    return 'GUID';
                            }
                        }
                    },
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#edit_grid').jfGridSet('moveUp', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#edit_grid').jfGridSet('moveDown', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-warning\" style=\"cursor: pointer;\">下移</span>';
                        }
                    },
                    {
                        label: "", name: "", width: 5, align: "center"
                    }
                ],
                isEdit: true,
                isShowNum: false,
                mainId: 'id',
                isTree: true,
                onSelectRow: function (rowdata) {
                    $('#colopWrap').show();
                    $('#colopWrap').parent().addClass('editopen');
                    currentRow = rowdata;
                    setRowOp();

                     
                        if (rowdata.isnew == "1") {
                            page.refreshInlineTarget();
                            rowdata.isnew = "0";
                        }
                    

                },
                onAddRow: function (row, rows) {
                    row.id = Changjie.newGuid();
                    row.name = '';
                    row.aggtype = 'normal';
                    row.required = '0';
                    row.datatype = "string";
                    row.aggcolumn = '';
                    row.type = 'label';
                    row.inlinetype = "none";
                    row.inlineTarget1 = "none";
                    row.inlineTarget2 = "none";
                    row.width = 100;
                    row.align = 'left';
                    row.parentId = '';
                    row.field = '';
                    row.dataSource = '0';
                    row.isnew = 1;
                },
                onMinusRow: function (row, rows) {
                    var currentField = row.field;
               
                    var curT1 = $("#inlineTarget1").mkselectGet();
                    var curT2 = $("#inlineTarget2").mkselectGet();
                    if (currentField == curT1) {
                        $("#inlineTarget1").mkselectSet("none");
                    }
                    if (currentField == curT2) {
                        $("#inlineTarget2").mkselectSet("none");
                    }
                    page.refreshInlineTarget();
                }
            });
            $('#edit_grid').jfGridSet('refreshdata', gridSetting.fields);
            if (!gridSetting.fieldMap[gridSetting.dbId + gridSetting.tableName]) {
                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldList', { databaseLinkId: gridSetting.dbId, tableName: gridSetting.tableName }, function (data) {
                    gridSetting.fieldMap[gridSetting.dbId + gridSetting.tableName] = data;
                    $('#field').mkselectRefresh({
                        data: data
                    });
                });
            }
            else {
                $('#field').mkselectRefresh({
                    data: gridSetting.fieldMap[gridSetting.dbId + gridSetting.tableName]
                });
            }
        },
        refreshInlineTarget: function () {
            var data = page.getCurrentRows();
            if (data.length > 1) {
                $("#inlineTarget1").mkselectRefresh({
                    data: data,
                });

                $("#inlineTarget2").mkselectRefresh({
                    data: data,
                });
            }
        },
        getCurrentRows: function () {
            var fields = [{id:"none",text:"无"}];
            var $rows = $("#edit_grid").jfGridGet("rowdatas");
            for (var i in $rows) {
                if (!$rows[i].field) continue;
                fields.push({ id: $rows[i].field, text: $rows[i].field + "(" + $rows[i].name + ")" });
            }
            return fields;
        },

    };
    page.init();
};