﻿/*
 * 日 期：2017.04.11
 * 描 述：数据列表选择	
 */
var acceptClick;
var op = top.mkGirdLayerEdit;
var bootstrap = function ($, Changjie) {
    "use strict";
    var selectItem;
    var griddata = null;
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $('#btn_Search').on('click', function () {
                if (griddata != null) {
                    var data = [];
                    var keyword = $('#txt_Keyword').val();
                    if (!!keyword) {
                        var searchList = op.edit.op.listFieldInfo || op.edit.op.colData;
                        for (var i = 0, l = griddata.length; i < l; i++) {
                            var item = griddata[i];
                            for (var j = 0, jl = searchList.length; j < jl; j++) {
                                var name = item[searchList[j].name];
                                if (isString(name)) {
                                    if (name && name.indexOf(keyword) != -1) {
                                        data.push(item);
                                        break;
                                    }
                                }
                            }
                        }
                        $('#gridtable').jfGridSet('refreshdata', data);
                    }
                    else {
                        $('#gridtable').jfGridSet('refreshdata', griddata);
                    }

                }
            });
            var param = {};
            if (op.edit.op.param.itemCode) {
                param.code = op.edit.op.param.itemCode;
            }
            else {
                param = op.edit.op.param;
            }
            $('#gridtable').jfGrid({
                headData: op.edit.op.listFieldInfo || op.edit.op.colData,
                url: op.edit.op.url,
                param: param, //op.edit.op.param,
                onRenderComplete: function (data) {
                    griddata = data;
                },
                dblclick: function (row) {
                    top.mkGirdLayerEditCallBack(row);
                    Changjie.layerClose(window.name);
                },
                onSelectRow: function (row) {
                    selectItem = row;
                }
            });

            $('#gridtable').jfGridSet('reload');
        }
    };


    // 保存数据
    acceptClick = function (callBack) {
        callBack(selectItem);
        return true;
    };
    page.init();
}


function isString(str) {
    return (typeof str == 'string') && str.constructor == String;
}

