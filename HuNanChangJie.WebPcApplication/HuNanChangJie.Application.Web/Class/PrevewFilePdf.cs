﻿using PreviewFile;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace HuNanChangJie.Application.Web
{
    public class PrevewFilePdf
    {
        public string PreviewFile(string filepath, string fileName)
        {
            string result = "";
            ConvertToPDF convertToPDF = new ConvertToPDF();
            bool flag = false;
            int port = HttpContext.Current.Request.Url.Port;
            string text = HttpContext.Current.Request.Url.Host;
            if (port > 0)
            {
                text = text + ":" + port;
            }

            string text2 = Guid.NewGuid().ToString();
            string text3 = text2 + "_" + fileName.Replace(".", "-") + ".pdf";
            string text4 = HttpContext.Current.Request.MapPath("~/PDF/");
            string text5 = text4 + text3;
            string text6 = "/PDF/" + text3;
            string text7 = filepath.Replace("/", "\\");
            if (!Directory.Exists(text4))
            {
                Directory.CreateDirectory(text4);
            }

            string extension = Path.GetExtension(HttpContext.Current.Server.UrlDecode(fileName));
            switch (extension.ToLower())
            {
                case ".doc":
                    flag = convertToPDF.DOC2PDF(text7, text5);
                    text5 = text + text6;
                    break;
                case ".docx":
                    {
                        WebClient webClient = new WebClient();
                        string text8 = Path.ChangeExtension(text7, "doc");
                        webClient.DownloadFile(text7, text8);
                        flag = convertToPDF.DOC2PDF(text8, text5);
                        text5 = text + text6;
                        break;
                    }
                case ".xls":
                case ".xlsx":
                    flag = convertToPDF.XLS2PDF(text7, text5);
                    text5 = text + text6;
                    break;
                case ".ppt":
                case ".pptx":
                    flag = convertToPDF.PPT2PDF(text7, text5);
                    text5 = text + text6;
                    break;
                case ".txt":
                    flag = true;
                    text4 = text4 + text2 + "_" + fileName;
                    text5 = convertToPDF.WebClient(filepath, text4, text, fileName, text2);
                    break;
                case ".pdf":
                    flag = true;
                    text4 = text4 + text2 + "_" + fileName;
                    text5 = convertToPDF.WebClient(filepath, text4, text, fileName, text2);
                    break;
                case ".jpg":
                case ".jpeg":
                case ".bmp":
                case ".gif":
                case ".png":
                    flag = true;
                    text4 = text4 + text2 + "_" + fileName;
                    text5 = convertToPDF.WebClient(filepath, text4, text, fileName, text2);
                    break;
                default:
                    flag = false;
                    break;
            }

            if (flag)
            {
                result = text5;
            }

            return result;
        }
    }
}