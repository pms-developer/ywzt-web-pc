﻿using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.PowerPoint;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace HuNanChangJie.Application.Web
{
    public class ConvertToPDF
    {
        public bool DOC2PDF(string sourcePath, string targetPath)
        {
            bool result = false;
            WdExportFormat wdExportFormat = WdExportFormat.wdExportFormatPDF;
            object ConfirmConversions = Type.Missing;
            Microsoft.Office.Interop.Word.Application applicationClass = new Microsoft.Office.Interop.Word.Application();
            Document document = null;
            try
            {
                object FileName = sourcePath;
                WdExportFormat exportFormat = wdExportFormat;
                bool openAfterExport = false;
                WdExportOptimizeFor optimizeFor = WdExportOptimizeFor.wdExportOptimizeForPrint;
                WdExportRange range = WdExportRange.wdExportAllDocument;
                int from = 0;
                int to = 0;
                WdExportItem item = WdExportItem.wdExportDocumentContent;
                bool includeDocProps = true;
                bool keepIRM = true;
                WdExportCreateBookmarks createBookmarks = WdExportCreateBookmarks.wdExportCreateWordBookmarks;
                bool docStructureTags = true;
                bool bitmapMissingFonts = true;
                bool useISO19005_ = false;
                document = applicationClass.Documents.Open(ref FileName, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions);
                document?.ExportAsFixedFormat(targetPath, exportFormat, openAfterExport, optimizeFor, range, from, to, item, includeDocProps, keepIRM, createBookmarks, docStructureTags, bitmapMissingFonts, useISO19005_, ref ConfirmConversions);
                result = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (document != null)
                {
                    document.Close(ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions);
                    document = null;
                }

                if (applicationClass != null)
                {
                    applicationClass.Quit(ref ConfirmConversions, ref ConfirmConversions, ref ConfirmConversions);
                    applicationClass = null;
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

            return result;
        }

        public bool XLS2PDF(string sourcePath, string targetPath)
        {
            bool result = false;
            XlFixedFormatType xlFixedFormatType = XlFixedFormatType.xlTypePDF;
            object missing = Type.Missing;
            Microsoft.Office.Interop.Excel.Application applicationClass = null;
            Workbook workbook = null;
            try
            {
                applicationClass = new Microsoft.Office.Interop.Excel.Application();
                object obj = xlFixedFormatType;
                workbook = applicationClass.Workbooks.Open(sourcePath, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
                workbook.ExportAsFixedFormat(xlFixedFormatType, targetPath, XlFixedFormatQuality.xlQualityStandard, true, false, missing, missing, missing, missing);
                result = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close(true, missing, missing);
                    workbook = null;
                }

                if (applicationClass != null)
                {
                    applicationClass.Quit();
                    applicationClass = null;
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

            return result;
        }

        public bool PPT2PDF(string sourcePath, string targetPath)
        {
            PpSaveAsFileType fileFormat = PpSaveAsFileType.ppSaveAsPDF;
            object missing = Type.Missing;
            Microsoft.Office.Interop.PowerPoint.Application applicationClass = null;
            Presentation presentation = null;
            try
            {
                applicationClass = new Microsoft.Office.Interop.PowerPoint.Application();
                presentation = applicationClass.Presentations.Open(sourcePath, MsoTriState.msoTrue, MsoTriState.msoFalse, MsoTriState.msoFalse);
                presentation.SaveAs(targetPath, fileFormat, MsoTriState.msoTrue);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (presentation != null)
                {
                    presentation.Close();
                    presentation = null;
                }

                if (applicationClass != null)
                {
                    applicationClass.Quit();
                    applicationClass = null;
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public string WebClient(string url, string url2, string hostPort, string fileName, string guid)
        {
            WebClient webClient = new WebClient();
            webClient.DownloadFile(url, url2);
            return hostPort + "/PDF/" + guid + "_" + fileName;
        }
    }
}