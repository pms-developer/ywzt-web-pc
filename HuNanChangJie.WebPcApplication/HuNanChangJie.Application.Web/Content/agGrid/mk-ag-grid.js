﻿/*
*名称：AgGrid
*版本号: V1.0.1 
*日期:2018年12月11日17:11:40
*作者: 
*描述：世界上最好用的表格Ag-Grid!
*修改人:
*描述：
*/

gridOptions =
    {
        localeText: {
            // for filter panel
            page: '当前页',
            more: '更多',
            to: '-',
            of: '共',
            next: '下一页',
            last: '尾页',
            first: '首页',
            previous: '上一页',
            loadingOoo: '数据即将呈现...',
            rowCount: '总行数',
            // for set filter
            selectAll: '选择全部',
            searchOoo: '查询...',
            blanks: '空白',
            andId: '并且',
            orId: '或',
            // for number filter and text filter
            filterOoo: '筛选...',
            applyFilter: '应用筛选...',
            equals: '等于',
            notEquals: '不等于',
            notEqual: '不等于',
            // for number filter
            lessThan: '小于',
            greaterThan: '大于',
            lessThanOrEqual: '小于等于',
            greaterThanOrEqual: '大于等于',
            inRange: '范围',

            // for text filter
            contains: '包含',
            notContains: '不包含',
            startsWith: '以...开始',
            endsWith: '以...结束',

            // the header of the default group column
            group: '组',

            // tool panel
            columns: '列',
            filters: '筛选',
            rowGroupColumns: '列分组',
            rowGroupColumnsEmptyMessage: '拖动此处可设置行组',
            valueColumns: '所有列的值',
            pivotMode: '模式',
            groups: '行组',
            values: '值',
            pivots: '列标签',
            valueColumnsEmptyMessage: '拖动到这里聚合',
            pivotColumnsEmptyMessage: '拖动此处可设置列标签',
            toolPanelButton: '工具按钮',

            // other
            noRowsToShow: '无数据呈现',

            // enterprise menu
            pinColumn: '冻结列',
            valueAggregation: '聚集值',
            autosizeThiscolumn: '本列自动尺寸',
            autosizeAllColumns: '全部列自动尺寸',
            groupBy: '分组依据',
            ungroupBy: '取消分组依据',
            resetColumns: '重置列',
            expandAll: '展开全部',
            collapseAll: '折叠全部',
            toolPanel: '工具面板',
            export: '导出',
            csvExport: '导出CSV (*.cvs)',
            excelExport: '导出Excel (*.xlsx)',

            // enterprise menu pinning
            pinLeft: '左冻结',
            pinRight: '右冻结',
            noPin: '默认',

            // enterprise menu aggregation and status bar
            sum: '合计值',
            min: '最小值',
            max: '最大值',
            none: '空',
            count: '数量',
            average: '平均值',
            selectedRowCount: '选中行数',
            filteredRowCount: '筛选行数',
            // standard menu
            copy: '复制',
            copyWithHeaders: '复制列头',
            ctrlC: 'Ctrl + C',
            paste: '粘贴',
            ctrlV: 'Ctrl + V',
            excelXMLExport: '导出Xml表格(*.xml)'
        },
        statusBar: {
            statusPanels: [
                { statusPanel: 'agTotalRowCountComponent', align: 'left' },
                { statusPanel: 'agFilteredRowCountComponent' },
                { statusPanel: 'agSelectedRowCountComponent' },
                { statusPanel: 'agAggregationComponent' }
            ]
        },
        enableColResize: true,
        debug: true
    }


//数据源
function ServerSideDatasource(server) {
    this.server = server;
}

ServerSideDatasource.prototype.getRows = function (params) {
    // invoke your server with request params supplied by grid
    var response = this.server.getResponse(params.request);

    if (response.success) {
        // call the success callback
        params.successCallback(response.rows, response.lastRow);
    } else {
        // inform the grid the request failed
        params.failCallback();
    }
};

function FakeServer(url, dfop) {
    return {

        getResponse(request) {

            // 从数据库行中取一个切片()
            var trows = request.endRow - request.startRow;
            var cpage = request.endRow / trows;
            dfop.pageparam.rows = trows;
            dfop.pageparam.page = cpage;
            if (!!dfop.sidx) {
                dfop.pageparam.sidx = dfop.sidx;
            }
            
            //string pagination, string keyword
            var req = {
                pagination: JSON.stringify(dfop.pageparam),
                keyword: "",
                filterModel: JSON.stringify(request.filterModel),
                groupKeys: "",
                pivotCols: "",
                sortModel: ""
            };
            $.extend(req, dfop.param);

            if (!!dfop.keyword) {
                req.keyword = dfop.keyword;
            }
            if (!!request.sortModel) {
                req.sortModel = JSON.stringify(request.sortModel);
            }

            var data = top.Changjie.httpGet(url, req);
           
            var rowsThisPage = data.data.rows;
            // 如果在最后一页或之后，计算出最后一行。

            var lastRow = data.data.records <= request.endRow ? data.data.records : -1;

            return {
                success: true,
                rows: rowsThisPage,
                lastRow: lastRow
            };
        }
    }
}
function findParent(data, filed, flag) {
    var returnElement = null;
    $.each(data, function (index, element) {
        if (element[filed] == flag) {
            returnElement = element;
            return false;
        }
    })
    return returnElement;
}
function compare(val1, val2) {
    return val1.treeNames.length - val2.treeNames.length;
};

function listTotree(data, op) {
    $.each(data, function (index, element) {
        if (!element.treeNames)
            element.treeNames = [];
        element.treeNames.push(element[op.displayName]);

        var parent = findParent(data, op.mainId, element[op.parentId]);
        while (parent) {
            element.treeNames.unshift(parent[op.displayName]);
            parent = findParent(data, op.mainId, parent[op.parentId]);
        }
    });

    data.sort(compare);
    return data;
    
    //var resdata = [];
    //var mapdata = {};
    //var mIds = {};

    //var pIds = {};
    //data = data || [];
    //for (var i = 0, l = data.length; i < l; i++) {
    //    var item = data[i];

    //    item.treeNames = new Array();
    //    mIds[item[op.mainId]] = 1;
    //    mapdata[item[op.parentId]] = mapdata[item[op.parentId]] || [];
    //    mapdata[item[op.parentId]].push(item);
    //    if (mIds[item[op.parentId]] == 1) {
    //        delete pIds[item[op.parentId]];
    //    }
    //    else {
    //        pIds[item[op.parentId]] = 1;
    //    }
    //    if (pIds[item[op.mainId]] == 1) {
    //        delete pIds[item[op.mainId]];
    //    }
    //}
    //var trueData = new Array();
    //for (var id in pIds) {
    //    var treeNames = new Array();
    //    _fn(resdata, id, treeNames, trueData);

    //}

    //function _fn(_data, vparentId, treeNames, trueData) {
    //    var pdata = mapdata[vparentId] || [];
    //    for (var j = 0, l = pdata.length; j < l; j++) {
    //        treeNames.push(pdata[j][op.displayName]);
    //        pdata[j].treeNames = treeNames;
    //        let arr1 = treeNames.slice()
    //        trueData.push(pdata[j]);
    //        var _row = {
    //            data: pdata[j],
    //            childRows: []
    //        }
    //        _fn(_row.childRows, pdata[j][op.mainId], arr1, trueData);
    //        var firstName = treeNames[0];
    //        treeNames = new Array();
    //        if (vparentId != 0) {
    //            treeNames.push(firstName);
    //        }

    //        // _data.push(_row);
    //    }
    //}
    //return trueData;
}


(function ($) {
    "use strict";



    $.AgGrid = $.AgGrid || {};

    $.extend($.AgGrid, {
        //显示表格数据
        render: function ($self) {
            
            var dfop = $self[0].dfop;

            $self.html('');
            $self.addClass('ag-theme-balham');
            var mygridOptions = {};
            if (!!dfop.gridOptions) {
                $.extend(mygridOptions, dfop.gridOptions);
            }
            //处理headerName
            for (var head in dfop.headData) {
                // label: "公式", name:
                //  headerName: "操作时间", field: "F_OperateTime",
                if (!!head.label) {
                    head.headerName = head.label;
                    $(head).removeProp("label");
                }
                if (!!head.name) {
                    head.field = head.name;
                    $(head).removeProp("name");
                }

            }


            if (dfop.isPage) {


                //默认为单行选中
                mygridOptions.rowSelection = 'single';
                //虚拟分页
                //因此要启用服务器端行模型，请rowModelType按如下方式定义：
                mygridOptions.rowModelType = 'serverSide';
                // fetch 100 rows per at a time
                mygridOptions.cacheBlockSize = 50;

                // only keep 10 blocks of rows
                mygridOptions.maxBlocksInCache = 10;

                //enableColResize: true,
                mygridOptions.animateRows = true;
                //debug: true
                ////pagination: true,
                //paginationAutoPageSize: true,


                
                $self[0].GridOptions = mygridOptions;
                $.extend($self.GridOptions, mygridOptions);
                $.extend(mygridOptions, gridOptions);
                mygridOptions.columnDefs = dfop.headData;
                mygridOptions.rowData = dfop.rowData;
                var eGridDiv = document.querySelector($self.selector);
                var grid = new agGrid.Grid(eGridDiv, mygridOptions);
                var server = new FakeServer(dfop.url, dfop);
                var datasource = new ServerSideDatasource(server);
                mygridOptions.api.setServerSideDatasource(datasource);
            } else {
                //不分页的情况

                if (!!dfop.isTree) {

                    mygridOptions = {
                        rowSelection: 'single',
                        statusBar: {
                            statusPanels: [
                                { statusPanel: 'agTotalRowCountComponent', align: 'left' },
                                { statusPanel: 'agFilteredRowCountComponent' },
                                { statusPanel: 'agSelectedRowCountComponent' },
                                { statusPanel: 'agAggregationComponent' }
                            ]
                        },
                        columnDefs: dfop.headData,
                        treeData: true, // enable Tree Data mode
                        animateRows: true,
                        groupDefaultExpanded: -1, // expand all groups by default
                        getDataPath: function (data) {
                            return data.treeNames;
                        },
                        onGridReady: function (params) {
                            params.api.sizeColumnsToFit();
                        },
                        autoGroupColumnDef: {
                            headerName: dfop.TreeGroupName,
                            cellRendererParams: {
                                suppressCount: true
                            }
                        }
                    };


                    mygridOptions.groupDefaultExpanded = -1; // expand all groups by default

                    $self[0].GridOptions = mygridOptions;
                    $.extend($self.GridOptions, mygridOptions);
                    $.extend(mygridOptions, gridOptions);
                    var neGridDiv = document.querySelector($self.selector);
                    var ngrid = new agGrid.Grid(neGridDiv, mygridOptions);
                    var ndata = top.Changjie.httpGet(dfop.url, dfop.param);
                    if (ndata.code == 200) {


                        mygridOptions.api.setRowData(listTotree(ndata.data, dfop));


                    }
                } else {
                    mygridOptions =
                        {
                            rowSelection: 'single',
                            statusBar: {
                                statusPanels: [
                                    { statusPanel: 'agTotalRowCountComponent', align: 'left' },
                                    { statusPanel: 'agFilteredRowCountComponent' },
                                    { statusPanel: 'agSelectedRowCountComponent' },
                                    { statusPanel: 'agAggregationComponent' }
                                ]
                            }
                        };
                    $self[0].GridOptions = mygridOptions;
                    $.extend($self.GridOptions, mygridOptions);
                    $.extend(mygridOptions, gridOptions);
                    mygridOptions.columnDefs = dfop.headData;
                    mygridOptions.rowData = dfop.rowData;
                    var neGridDiv = document.querySelector($self.selector);
                    var ngrid = new agGrid.Grid(neGridDiv, mygridOptions);
                    if (!!dfop.url) {
                        var ndata = top.Changjie.httpGet(dfop.url, dfop.param);
                        if (ndata.code == 200) {
                            if (!!ndata.data) {
                                if (!!ndata.data.rows) {
                                    mygridOptions.api.setRowData(ndata.data.rows);
                                } else {
                                    mygridOptions.api.setRowData(ndata.data);
                                }

                            }

                        }
                    } else {
                        // mygridOptions.rowData = [];
                    }

                }
            }
            mygridOptions.api.sizeColumnsToFit();
            mygridOptions.columnApi.autoSizeColumns();
        }
        ,
        reload: function ($self) {
            var dfop = $self[0].dfop;
            dfop.rowdatas = [];

            dfop.pageparam.page = 1; //查询默认为第一页
            dfop.pageparam.records = 0;
            dfop.pageparam.total = 0;
            dfop.param['pagination'] = JSON.stringify(dfop.pageparam);
            $.AgGrid.render($self);
        }
    });
    //扩展方法初始化Ag-Grid
    $.fn.AgGrid = function (op) {
        
        var $self = $(this);
        var id = $self.attr('id');
        if (id == null || id == undefined || id == '') {
            id = "AgGrid" + new Date().getTime();
            $self.attr('id', id);
        }
        var dfop = {
            url: '',                      // 数据服务地址
            param: {},                    // 请求参数
            rowdatas: [],                 // 列表数据            // 数据类型
            headData: [],                // 列数据
            pageparam: {
                rows: 50,                 // 每页行数      
                page: 1,                  // 当前页
                sidx: '',                 // 排序列
                sord: '',                 // 排序类型
                records: 0,               // 总记录数
                total: 0
            }                  //分页
        };
        if (!!op) {
            $.extend(dfop, op);
        }
        dfop.id = id;
        $self[0].dfop = dfop;
        $.AgGrid.render($self);
        dfop = null;
        return $self;
    }

    //获取所有选中的行
    $.fn.AgGridGetSelectedRows = function () {
        var $self = $(this);
        var id = $self.attr('id');
        if (id == null || id == undefined || id == '') {
            return null;
        }
        var dfop = $self[0].dfop;
        var gridOptions = $self[0].GridOptions;
        if (!dfop) {
            return null;
        }
        return gridOptions.api.getSelectedRows();
    }

    //设置AgGrid的属性
    $.fn.AgGridSet = function (name, op) {
        var $self = $(this);
        var id = $self.attr('id');
        if (id == null || id == undefined || id == '') {
            return null;
        }
        var dfop = $self[0].dfop;
        if (!dfop) {
            return null;
        }
        if (!!op) {
            $.extend(dfop, op);
            $.extend(dfop.param, op);
        }
        switch (name) {
            //查询方法重载
            case 'reload':
                $.AgGrid.reload($self);
                break;
            case 'refreshdata':
                if (!!op) {
                    dfop.rowdatas = op.rowdatas || op;
                }
                $self[0].GridOptions.api.setRowData(dfop.rowdatas);
                break;
            default:
        }
        return null;
    }
    //获取数据 包括 当前行 当前选中的所有行 等。
    $.fn.AgGridGet = function (name) {
        var $self = $(this);
        var op = $self[0].dfop;
        if (!op) {
            return null;
        }
        var gridOptions = $self[0].GridOptions;

        switch (name) {

            case 'rowdata':
                return gridOptions.api.getSelectedRows()[0];
                break;

        }
        return null;
    }
    //获取当前行的主键
    $.fn.AgGridValue = function (rowId) {
        var $self = $(this);
        var id = $self.attr('id')
        if (id == null || id == undefined || id == '') {
            return null;
        }
        var dfop = $self[0].dfop;
        var gridOptions = $self[0].GridOptions;
        if (!dfop) {
            return null;
        }
        var _rowdata = gridOptions.api.getSelectedRows();
        if (!!_rowdata) {
            _rowdata = _rowdata[0];
            var res = "";
            try {
                if (_rowdata.length > 0) {
                    $.each(_rowdata, function (id, item) {
                        if (res != "") {
                            res += ',';
                        }
                        res += item[rowId] || '';
                    });
                    return res;
                }
                else {
                    return _rowdata[rowId] || '';
                }
            } catch (e) {
                return '';
            } 
           
        }
        else {
            return '';
        }
    };


})(window.jQuery)