/// 创建人：
/// 日 期：2018.11.17
/// 描 述： echarts 封装类
var defaults = {
    geoCoordMap: {
        '上海': [121.4648, 31.2891],
        '东莞': [113.8953, 22.901],
        '东营': [118.7073, 37.5513],
        '中山': [113.4229, 22.478],
        '临汾': [111.4783, 36.1615],
        '临沂': [118.3118, 35.2936],
        '丹东': [124.541, 40.4242],
        '丽水': [119.5642, 28.1854],
        '乌鲁木齐': [87.9236, 43.5883],
        '佛山': [112.8955, 23.1097],
        '保定': [115.0488, 39.0948],
        '兰州': [103.5901, 36.3043],
        '甘肃': [103.5901, 36.3043],
        '包头': [110.3467, 41.4899],
        '北京': [116.4551, 40.2539],
        '北海': [109.314, 21.6211],
        '南京': [118.8062, 31.9208],
        '江苏': [118.8062, 31.9208],
        '南宁': [108.479, 23.1152],
        '广西': [108.479, 23.1152],
        '南昌': [116.0046, 28.6633],
        '江西': [116.0046, 28.6633],
        '南通': [121.1023, 32.1625],
        '厦门': [118.1689, 24.6478],
        '台州': [121.1353, 28.6688],
        '合肥': [117.29, 32.0581],
        '安徽': [117.29, 32.0581],
        '呼和浩特': [111.4124, 40.4901],
        '咸阳': [108.4131, 34.8706],
        '哈尔滨': [127.9688, 45.368],
        '黑龙江': [127.9688, 45.368],
        '唐山': [118.4766, 39.6826],
        '嘉兴': [120.9155, 30.6354],
        '大同': [113.7854, 39.8035],
        '大连': [122.2229, 39.4409],
        '天津': [117.4219, 39.4189],
        '太原': [112.3352, 37.9413],
        '山西': [112.3352, 37.9413],
        '威海': [121.9482, 37.1393],
        '宁波': [121.5967, 29.6466],
        '宝鸡': [107.1826, 34.3433],
        '宿迁': [118.5535, 33.7775],
        '常州': [119.4543, 31.5582],
        '广州': [113.5107, 23.2196],
        '广东': [113.5107, 23.2196],
        '廊坊': [116.521, 39.0509],
        '延安': [109.1052, 36.4252],
        '张家口': [115.1477, 40.8527],
        '徐州': [117.5208, 34.3268],
        '德州': [116.6858, 37.2107],
        '惠州': [114.6204, 23.1647],
        '成都': [103.9526, 30.7617],
        '四川': [103.9526, 30.7617],
        '扬州': [119.4653, 32.8162],
        '承德': [117.5757, 41.4075],
        '拉萨': [91.1865, 30.1465],
        '无锡': [120.3442, 31.5527],
        '日照': [119.2786, 35.5023],
        '昆明': [102.9199, 25.4663],
        '云南': [102.9199, 25.4663],
        '杭州': [119.5313, 29.8773],
        '浙江': [119.5313, 29.8773],
        '枣庄': [117.323, 34.8926],
        '柳州': [109.3799, 24.9774],
        '株洲': [113.5327, 27.0319],
        '武汉': [114.3896, 30.6628],
        '湖北': [114.3896, 30.6628],
        '汕头': [117.1692, 23.3405],
        '江门': [112.6318, 22.1484],
        '沈阳': [123.1238, 42.1216],
        '辽宁': [123.1238, 42.1216],
        '沧州': [116.8286, 38.2104],
        '河源': [114.917, 23.9722],
        '泉州': [118.3228, 25.1147],
        '泰安': [117.0264, 36.0516],
        '泰州': [120.0586, 32.5525],
        '济南': [117.1582, 36.8701],
        '山东': [117.1582, 36.8701],
        '济宁': [116.8286, 35.3375],
        '海口': [110.3893, 19.8516],
        '海南': [110.3893, 19.8516],
        '淄博': [118.0371, 36.6064],
        '淮安': [118.927, 33.4039],
        '深圳': [114.5435, 22.5439],
        '清远': [112.9175, 24.3292],
        '温州': [120.498, 27.8119],
        '渭南': [109.7864, 35.0299],
        '湖州': [119.8608, 30.7782],
        '湘潭': [112.5439, 27.7075],
        '滨州': [117.8174, 37.4963],
        '潍坊': [119.0918, 36.524],
        '烟台': [120.7397, 37.5128],
        '玉溪': [101.9312, 23.8898],
        '珠海': [113.7305, 22.1155],
        '盐城': [120.2234, 33.5577],
        '盘锦': [121.9482, 41.0449],
        '石家庄': [114.4995, 38.1006],
        '河北': [114.4995, 38.1006],
        '福州': [119.4543, 25.9222],
        '福建': [119.4543, 25.9222],
        '秦皇岛': [119.2126, 40.0232],
        '绍兴': [120.564, 29.7565],
        '聊城': [115.9167, 36.4032],
        '肇庆': [112.1265, 23.5822],
        '舟山': [122.2559, 30.2234],
        '苏州': [120.6519, 31.3989],
        '莱芜': [117.6526, 36.2714],
        '菏泽': [115.6201, 35.2057],
        '营口': [122.4316, 40.4297],
        '葫芦岛': [120.1575, 40.578],
        '衡水': [115.8838, 37.7161],
        '衢州': [118.6853, 28.8666],
        '西宁': [101.4038, 36.8207],
        '青海': [101.4038, 36.8207],
        '西安': [109.1162, 34.2004],
        '陕西': [109.1162, 34.2004],
        '贵阳': [106.6992, 26.7682],
        '贵州': [106.6992, 26.7682],
        '连云港': [119.1248, 34.552],
        '邢台': [114.8071, 37.2821],
        '邯郸': [114.4775, 36.535],
        '郑州': [113.4668, 34.6234],
        '河南': [113.4668, 34.6234],
        '鄂尔多斯': [108.9734, 39.2487],
        '重庆': [107.7539, 30.1904],
        '金华': [120.0037, 29.1028],
        '铜川': [109.0393, 35.1947],
        '银川': [106.3586, 38.1775],
        '宁夏': [106.3586, 38.1775],
        '镇江': [119.4763, 31.9702],
        '长春': [125.8154, 44.2584],
        '吉林': [125.8154, 44.2584],
        '长沙': [113.0823, 28.2568],
        '湖南': [113.0823, 28.2568],
        '长治': [112.8625, 36.4746],
        '阳泉': [113.4778, 38.0951],
        '青岛': [120.4651, 36.3373],
        '韶关': [113.7964, 24.7028]
    },

    provinces: {
        //23个省
        "台湾": "taiwan",
        "河北": "hebei",
        "山西": "shanxi",
        "辽宁": "liaoning",
        "吉林": "jilin",
        "黑龙江": "heilongjiang",
        "江苏": "jiangsu",
        "浙江": "zhejiang",
        "安徽": "anhui",
        "福建": "fujian",
        "江西": "jiangxi",
        "山东": "shandong",
        "河南": "henan",
        "湖北": "hubei",
        "湖南": "hunan",
        "广东": "guangdong",
        "海南": "hainan",
        "四川": "sichuan",
        "贵州": "guizhou",
        "云南": "yunnan",
        "陕西": "shanxi1",
        "甘肃": "gansu",
        "青海": "qinghai",
        //5个自治区
        "新疆": "xinjiang",
        "广西": "guangxi",
        "内蒙古": "neimenggu",
        "宁夏": "ningxia",
        "西藏": "xizang",
        //4个直辖市
        "北京": "beijing",
        "天津": "tianjin",
        "上海": "shanghai",
        "重庆": "chongqing",
        //2个特别行政区
        "香港": "xianggang",
        "澳门": "aomen"
    },
    convertData: function (data) {
        var res = [];
        for (var i = 0; i < data.length; i++) {
            var geoCoord = $.geoCoordMap[data[i].name];
            if (geoCoord) {
                res.push({
                    name: data[i].name,
                    value: geoCoord.concat(data[i].value)
                });
            }
        }



        return res;
    },
    convertDataFlow: function (data) {


        var res = [];
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            var fromCoord = $.geoCoordMap[dataItem[0].name];
            var toCoord = $.geoCoordMap[dataItem[1].name];
            if (fromCoord && toCoord) {
                res.push({
                    fromName: dataItem[0].name,
                    toName: dataItem[1].name,
                    coords: [fromCoord, toCoord]
                });
            }
        }
        return res;
    },
    option: {},
    itemColors: [
       
          ["rgb(45,98,215)", "rgb(67,220,252)"]
        , ["rgb(188,25,92)", "rgb(252,76,89)"] 
        , ["rgb(67,69,218)", "rgb(93,112,236)"]
        , ["rgb(8,80,120)", "rgb(133,216,206)"]
        , ["rgb(109,68,206)", "rgb(153,116,252)"]
        , ["rgb(140,62,174)", "rgb(237,151,255)"]
        , ["rgb(29,155,76)", "rgb(0,226,133)"]
        , ["rgb(255,75,31)", "rgb(255,144,104)"]
        , ["rgb(185,29,115)", "rgb(249,83,198)"]
        , ["rgb(71,118,230)", "rgb(168,113,253)"]
    ]
};

$.extend(defaults);



var ECharts = {
    ChartDataFormate: {
        FormateNOGroupData: function (data) {
            //data的格式如上的Result1，这种格式的数据，多用于饼图、单一的柱形图的数据源
            var categories = [];
            var datas = [];
            for (var i = 0; i < data.length; i++) {
                categories.push(data[i].name || "");
                datas.push({ name: data[i].name, value: data[i].value || 0 });
            }
            return { category: categories, data: datas };
        },
        FormateGroupData: function (data, type, is_stack) {
            //data[{name:"",value:"",group:""},...]
            //data的格式如上的Result2，type为要渲染的图表类型：可以为line，bar，is_stack表示为是否是堆积图，这种格式的数据多用于展示多条折线图、分组的柱图
            var chart_type = 'line';
            if (type)
                chart_type = type || 'line';
            var xAxis = [];
            var group = [];
            var series = [];
            
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < xAxis.length && xAxis[j] != data[i].name; j++);
                if (j == xAxis.length)
                    xAxis.push(data[i].name);
                for (var k = 0; k < group.length && group[k] != data[i].group; k++);
                if (k == group.length)
                    group.push(data[i].group);
            }
            for (var i = 0; i < group.length; i++) {
                var temp = [];
                for (var j = 0; j < data.length; j++) {
                    if (group[i] == data[j].group) {
                        if (type == "map" || type == 'flowmap') {
                            temp.push({ name: data[j].name, value: data[i].value });
                        } else {
                            temp.push(data[j].value);
                        }
                    }
                }

                switch (type) {
                    case 'bar':
                        var series_temp = { 
                            name: group[i],
                             data: temp, 
                             type: chart_type,
                           
                             itemStyle:{
                               
                             } };
                        if (is_stack)
                            series_temp = $.extend({}, { stack: 'stack' }, series_temp);
                        series_temp = $.extend({}, {
                            markPoint: {
                                data: [
                                    { type: 'max', name: '最大值' },
                                    { type: 'min', name: '最小值' }
                                ]
                            },
                            
                            markLine: {
                                data: [
                                    { type: 'average', name: '平均值' }
                                ]
                            }
                        }, series_temp);


                        break;

                    case 'map':
                        var series_temp = {
                            name: group[i], type: chart_type, mapType: 'china', selectedMode: 'multiple',
                            itemStyle: {
                                normal: { label: { show: true } },
                                emphasis: { label: { show: true } }
                            },
                            data: temp
                        };
                        break;

                    case 'flowmap':
                        var series_temp = { name: group[i], value: temp, type: chart_type };
                        break;
                    case 'line':
                        var series_temp = { name: group[i], data: temp, type: chart_type };
                        if (is_stack)
                            series_temp = $.extend({}, { stack: 'stack' }, series_temp);
                        series_temp = $.extend({}, {
                            markPoint: {
                                data: [
                                    { type: 'max', name: '最大值' },
                                    { type: 'min', name: '最小值' }
                                ]
                            },
                            areaStyle: {},
                            smooth: true,
                            markLine: {
                                data: [
                                    { type: 'average', name: '平均值' }
                                ]
                            }
                        }, series_temp);
                        break;

                    default:
                        var series_temp = { name: group[i], data: temp, type: chart_type };
                }
                series.push(series_temp);
            }
            return { category: group, xAxis: xAxis, series: series };
        }
    }
    ,
    ChartOptionTemplates: {
        CommonOption: {
            title: {
                text: '',
                left: "40%"
            },
            //通用的图表基本配置 
            tooltip: {
                trigger: 'item'//tooltip触发方式:axis以X轴线触发,item以每一个数据项触发 
            },
            toolbox: {
                show: true,
                orient: 'vertical',
                left: 'left',
                top: 'top',
                feature: {
                    mark: { show: true },
                    dataView: { show: true, readOnly: false },
                    restore: { show: true },
                    saveAsImage: { show: true }
                }
            }
        },
        CommonLineOption: {//通用的折线图表的基本配置 
            title: {
                text: 'CJ-PMS智能图表',
                left: "40%"
            },
            tooltip: {
                trigger: 'axis'
            },
            calculable: true,
            toolbox: {
                show: true,
                //orient : 'vertical',
                left: 'right',
                top: 'top',
                feature: {
                    mark: { show: true },
                    dataView: { show: true, readOnly: false },
                    restore: { show: true },
                    magicType: ['line', 'bar'],//支持柱形图和折线图的切换 
                    saveAsImage: { show: true }
                }
            }

        },
        Pie: function (data, name) {
            //data:数据格式：{name：xxx,value:xxx}...
            var pie_datas = ECharts.ChartDataFormate.FormateNOGroupData(data);
            var pie_datas_items = [];

            for (const i in pie_datas.data) {

                var element = pie_datas.data[i];
                 var yushu = i % 8;
                 element.itemStyle = {
                     color: {
                         type: 'linear',
                         x: 0,
                         y: 1,
                         x2: 0,
                         y2: 0,
                         colorStops: [{
                             offset: 0, color: $.itemColors[yushu][0] // 0% 处的颜色
                         }, {
                             offset: 1, color: $.itemColors[yushu][1]// 100% 处的颜色
                         }],
                         globalCoord: false // 缺省为 false
                     }
                 };
                pie_datas_items.push(element);

            }


            var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: '{b} : {c} ({d}/%)',
                    show: true
                },
                calculable : true,
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: pie_datas.category
                },
                calculable: true,
                toolbox: {
                    show: true,
                    feature: {
                        mark: { show: true },
                        dataView: { show: true, readOnly: true },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                series: [
                    {
                        name: name || "",
                        radius: ['40%', '80%'],//内围心大小，外围
                        type: 'pie',
                        // radius: '65%',//实心
                        center: ['50%', '50%'],
                        data: pie_datas_items
                    }
                ]
            };
            return $.extend({}, ECharts.ChartOptionTemplates.CommonOption, option);
        },
        Lines: function (data, name, is_stack) {
            //data:数据格式：{name：xxx,group:xxx,value:xxx}... 

            var option = {};
            if (is_stack) {
                var stackline_datas = ECharts.ChartDataFormate.FormateGroupData(data, 'line', is_stack);
                option = {
                    legend: {
                        data: stackline_datas.xAxis
                    },
                    xAxis: [{
                        type: 'category', //X轴均为category，Y轴均为value 
                        data: stackline_datas.xAxis,
                        boundaryGap: false,//数值轴两端的空白策略 ,
                        
                    }],
                    yAxis: [{
                        type: 'value',
                        splitArea: { show: true }
                    }],
                    series: stackline_datas.series
                }
            } else {
                var stackline_datas = ECharts.ChartDataFormate.FormateNOGroupData(data, 'line', is_stack);
                option = {

                    xAxis: [{
                        type: 'category', //X轴均为category，Y轴均为value 
                        data: stackline_datas.xAxis,
                        boundaryGap: false//数值轴两端的空白策略 
                    }],
                    yAxis: [{
                        type: 'value',
                        splitArea: { show: true }
                    }],
                    series: stackline_datas.series
                }
            }
            return $.extend({}, ECharts.ChartOptionTemplates.CommonLineOption, option);
        },
        Bars: function (data, name, is_stack) {
            //data:数据格式：{name：xxx,group:xxx,value:xxx}...
            var bars_dates = ECharts.ChartDataFormate.FormateGroupData(data, 'bar', is_stack);
            var bars_datas_items = [];

            // for (const i in bars_dates.series) {

            //     const element = bars_dates.series[i];
               
            //     var yushu = i % 8;
            //     element.itemStyle = {
            //         color: {
            //             type: 'linear',
            //             x: 0,
            //             y: 1,
            //             x2: 0,
            //             y2: 0,
            //             colorStops: [{
            //                 offset: 0, color: $.itemColors[yushu][0] // 0% 处的颜色
            //             }, {
            //                 offset: 1, color: $.itemColors[yushu][1]// 100% 处的颜色
            //             }],
            //             globalCoord: false // 缺省为 false
            //         }
            //     };
            //     bars_datas_items.push(element);

            // }


            var option = {
                legend: { data: bars_dates.category },
                xAxis: [{
                    type: 'category',
                    data: bars_dates.xAxis
                }],
                yAxis: [{
                    type: 'value'
                }],
                toolbox: bars_dates.toolbox,
                series:bars_dates.series
             //   series: bars_datas_items
            };
            return $.extend({}, ECharts.ChartOptionTemplates.CommonLineOption, option);
        },
        //普通地图
        Maps: function (data, name, is_stack) {
            //data:数据格式：{name：xxx,group:xxx,value:xxx}...
            var maps_dates = ECharts.ChartDataFormate.FormateGroupData(data, 'map', is_stack);
            var option = {
                tooltip: {
                    trigger: 'item'
                },

                visualMap: {
                    min: 0,
                    max: 500,
                    show: false,
                    splitNumber: 5,
                    inRange: {
                        color: ['#d94e5d', '#eac736', '#50a3ba'].reverse()
                    },
                    textStyle: {
                        color: '#fff'
                    }
                }, geo: {
                    map: name,
                    label: {
                        normal: {
                            show: true,
                            color: '#fff'
                        },
                        emphasis: {
                            show: true,
                            color: '#fff'
                        }
                    },
                    roam: false,
                    itemStyle: {
                        normal: {
                            areaColor: '#40458e',
                            borderColor: '#6367ad',
                            borderWidth: 1.5
                        },
                        emphasis: {
                            areaColor: '#40458e'
                        }
                    },
                    "left": 0,
                    "right": 0,
                    "top": 0,
                    "bottom": 0
                },
                series: maps_dates.series
            };
            return $.extend({}, ECharts.ChartOptionTemplates.CommonOption, option);
        },
        //模拟迁徙图
        FlowMap: function (data, name) {
            //地图流向图-迁徙图
            var flow_mapdatas = ECharts.ChartDataFormate.FormateGroupData(data, 'flowmap').series;

            var planePath = 'path:// M878.229514 645.809634c-40.072094-279.906746-298.66716-645.809634-375.45455-645.809634-46.917869 0-327.58588 367.1461-375.462032 645.809634-46.003852 267.897962 168.10054 378.190366 375.462032 378.190366S917.497324 920.091997 878.229514 645.809634zM683.499037 658.740541 572.745883 658.740541l0 110.774975-139.920641 0L432.825243 658.740541 322.072089 658.740541 322.072089 518.841722l110.753154 0L432.825243 408.074228l139.920641 0 0 110.767494L683.499037 518.841722 683.499037 658.740541z';
            var color = ['#a6c84c', '#ffa022', '#46bee9'];
            var TopData = [];
            for (var topi in flow_mapdatas) {
                var myobject = [];
                myobject[0] = flow_mapdatas[topi].name //最外层
                var BJData = [];
                for (var si in flow_mapdatas[topi].value) {
                    var myboject2 = [];//内层数组
                    myboject2[0] = { name: myobject[0] };
                    myboject2[1] = { name: flow_mapdatas[topi].value[si].name, value: flow_mapdatas[topi].value[si].value };
                    BJData.push(myboject2);
                }
                myobject[1] = BJData;
                TopData.push(myobject);
            }
            var series = [];
            TopData.forEach(function (item, i) {
                series.push({
                    name: item[0],
                    type: 'lines',
                    zlevel: 1,
                    effect: {
                        show: true,
                        period: 6,
                        trailLength: 0.7,
                        color: '#fff',
                        symbolSize: 3
                    },
                    lineStyle: {
                        normal: {
                            color: color[i],
                            width: 0,
                            curveness: 0.2
                        }
                    },
                    data: $.convertDataFlow(item[1])
                },
                    {
                        name: item[0],
                        type: 'lines',
                        zlevel: 2,
                        effect: {
                            show: true,
                            period: 6,
                            trailLength: 0,
                            symbol: planePath,
                            symbolSize: 10
                        },
                        lineStyle: {
                            normal: {
                                color: color[i],
                                width: 1,
                                opacity: 0.4,
                                curveness: 0.2
                            }
                        },
                        data: $.convertDataFlow(item[1])
                    },
                    {
                        name: item[0],
                        type: 'effectScatter',
                        coordinateSystem: 'geo',
                        zlevel: 2,
                        rippleEffect: {
                            brushType: 'stroke'
                        },
                        label: {
                            normal: {
                                show: true,
                                position: 'right',
                                formatter: '{b}'
                            }
                        },
                        symbolSize: function (val) {
                            return val[2] / 2000;
                        },
                        itemStyle: {
                            normal: {
                                color: color[i]
                            }
                        },
                        data: item[1].map(function (dataItem) {
                            var myname = dataItem[1].name;
                            var myvalue = dataItem[1].value;


                            return {
                                name: myname,
                                value: $.geoCoordMap[myname].concat([myvalue])
                            };
                        })
                    });
            });



            option = {

                title: {

                    left: 'center',
                     
                },
                tooltip: {
                    trigger: 'item'
                },

                geo: {
                    map: 'china',
                    label: {
                        emphasis: {
                            show: false
                        }
                    },
                    roam: true
                },
                series: series
            };

            return $.extend({}, ECharts.ChartOptionTemplates.CommonOption, option);
        },
        //散点地图
        ScatterMap: function (data, name) {

            var series = [{
                name: '',
                type: 'scatter',
                coordinateSystem: 'geo',
                data: $.convertData(data),
                symbolSize: function (val) {
                    return val[2] / 2;
                },
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: false
                    },
                    emphasis: {
                        show: true
                    }
                } 
            },
            {
                name: '排名前五',
                type: 'effectScatter',
                coordinateSystem: 'geo',
                data: $.convertData(data.sort(function (a, b) {
                    return b.value - a.value;
                }).slice(0, 10)),
                symbolSize: function (val) {
                    return val[2] / 2;
                },
                showEffectOn: 'render',
                rippleEffect: {
                    brushType: 'stroke'
                },
                hoverAnimation: true,
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#f4e925',
                        shadowBlur: 10,
                        shadowColor: '#000'
                    }
                },
                zlevel: 1
            }];
            option = {
                
                title: {
                    text: '',
                    subtext: '',
                    sublink: '',
                    left: 'center',
                    
                },
                tooltip: {
                    trigger: 'item'
                },
                geo: {
                    map: 'china',
                    label: {
                        emphasis: {
                            show: false
                        }
                    },
                    roam: true,
                    
                },
                series: series
            };
            return $.extend({}, ECharts.ChartOptionTemplates.CommonOption, option);
        }
    }


};

