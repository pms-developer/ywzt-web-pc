/*
*百度 Echarts 帮助类
*2018年11月19日09:06:28
*作者：
*描述：
*/

$.LoadReport = function (options) {
    var defaults = {
        url: "",
        element: "",
         data:""
    };
    var options = $.extend(defaults, options);
    $.ajax({
        url: options.url,
        data: options.data,
        type:"POST",
        cache: false,
        async: false,
        dataType: 'json',
        success: function (apidata) {

           var data = apidata.data;
            var $echart, $list;
            if (data.tempStyle == 1) {
                if (data.listData.length > 0) {
                    $list = $('<div id="gridtable" class="mk-layout-body"></div>');
                    options.element.append($list);
                    DrawList(data.listData, $list);
                }
            } else if (data.tempStyle == 2) {
                if (data.chartData.length > 0) {
                    $echart = $('<div id="echart" style="width: 100%; height: 400px;"></div>');
                    options.element.append($echart);
                    switch (data.chartType) {
                        case 'pie'://饼图
                            DrawPie(data.chartData, $echart[0]);
                            break;
                        case 'bar'://柱形图
                            DrawBar(data.chartData, $echart[0]);
                            break;
                        case 'line'://折线图
                            DrawLine(data.chartData, $echart[0]);
                            break;
                        case 'map'://普通地图
                            DrawMap(data.chartData, $echart[0]);
                            break;
                        case 'flowmap'://地理信息迁徙图
                            DrawFlowMap(data.chartData, $echart[0]);
                            break;
                        case 'geomap'://地理信息地图


                            break;
                        case 'scattermap'://气泡地图
                            DrawScatterMap(data.chartData, $echart[0]);

                            break;
                        default:
                    }

                }
            } else {
                if (data.chartData.length > 0) {
                    $echart = $('<div id="echart" style="width: 100%; height: 400px;"></div>');
                    options.element.append($echart);
                    switch (data.chartType) {
                        case 'pie'://饼图
                            DrawPie(data.chartData, $echart[0]);
                            break;
                        case 'bar'://柱形图
                            DrawBar(data.chartData, $echart[0]);
                            break;
                        case 'line'://折线图
                            DrawLine(data.chartData, $echart[0]);
                            break;
                        case 'map'://普通地图
                            DrawMap(data.chartData, $echart[0]);
                            break;
                        case 'flowmap'://地理信息迁徙图
                            DrawFlowMap(data.chartData, $echart[0]);
                            break;
                        case 'geomap'://地理信息地图


                            break;
                        case 'scattermap'://气泡地图
                            DrawScatterMap(data.chartData, $echart[0]);

                            break;
                        default:
                    }


                }
                if (data.listData.length > 0) {
                    $list = $('<div id="gridtable" class="mk-layout-body"></div>');
                    options.element.append($list);
                    DrawList(data.listData, $list);
                }
            }
        }
    });
    function DrawPie(data, echartElement) {
        var myChart = echarts.init(echartElement,'halloween');
        myChart.echartElement={echartElement:echartElement};
        myChart.on('click', function(param) {
          
         
        })
        var option = ECharts.ChartOptionTemplates.Pie(data);
        myChart.setOption(option,true);
    }
    function DrawBar(data, echartElement) {
        var myChart = echarts.init(echartElement, 'halloween');
       
        myChart.clear();
        var option = ECharts.ChartOptionTemplates.Bars(data, 'bar', true);
        myChart.setOption(option,true);
    }
    function DrawLine(data, echartElement) {
        var myChart = echarts.init(echartElement,'halloween');
        var option = ECharts.ChartOptionTemplates.Lines(data, 'line', true);
        myChart.setOption(option,true);
    }
    function DrawMap(data, echartElement) {
        var myChart = echarts.init(echartElement,'halloween');
        $.get('/Content/json/map_json/china.json', function (getJSON) {
            echarts.registerMap('china', getJSON);

            var option = ECharts.ChartOptionTemplates.Maps(data, 'map', true);

            myChart.setOption(option,true);

        });


    }

    //画散点地图
    function DrawScatterMap(data, echartElement) {
        var myChart = echarts.init(echartElement,'halloween');
        $.get('/Content/json/map_json/china.json', function (getJSON) {
            echarts.registerMap('china', getJSON);
            var option = ECharts.ChartOptionTemplates.ScatterMap(data, '');
            myChart.setOption(option,true);
        });
    }

    //画地图迁徙图
    function DrawFlowMap(data, echartElement) {
        var myChart = echarts.init(echartElement,'halloween');
        $.get('/Content/json/map_json/china.json', function (getJSON) {
            echarts.registerMap('china', getJSON);
            var option = ECharts.ChartOptionTemplates.FlowMap(data, '');
            myChart.setOption(option,true);
        });



    }
    function DrawList(data, listElement) {
        listElement.jfGrid({
            headData: function () {
                var colModelData = [];
                for (key in data[0]) {
                    colModelData.push({ name: key, label: key, width: 120, align: "left" });
                }
                return colModelData;
            }(),
            rowdatas: data,
            isAutoHeight: true
        });
    }
}
