﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace HuNanChangJie.Application.Web.XmlConfig
{
    public class XmlGlobalinfo
    {

        /// <summary>
        /// 系统参数列表
        /// </summary>
        public static List<SystemConfigParam> SystemConfigs { get; }

        public static XmlDocument doc = new XmlDocument();

        public static System.IO.DirectoryInfo path_exe = new System.IO.DirectoryInfo(HttpRuntime.AppDomainAppPath.ToString());

        /// <summary>
        /// GetXML  
        /// </summary>
        public static List<SystemCommon.SystemConfigParam> GetXmlValue()
        {
            try
            {
                List<SystemCommon.SystemConfigParam> ParamInfoList = new List<SystemCommon.SystemConfigParam>();
                doc.Load(path_exe.FullName + @"/XmlConfig/SystemConfiguration.xml");
                System.IO.Path.GetFullPath(path_exe.Parent.Parent.FullName + @"/XmlConfig/SystemConfiguration.xml");
                XmlNode root = doc.SelectSingleNode("SystemConfiguration");
                foreach (XmlNode item in root.ChildNodes[0].ChildNodes)
                {
                    foreach (XmlNode itemg in root.ChildNodes)
                    {
                        if (itemg.Name == "Configurations")
                        {
                            SystemCommon.SystemConfigParam ParamInfo = new SystemCommon.SystemConfigParam();
                            ParamInfo.Name = item.Attributes["Name"].Value;
                            ParamInfo.ValueType = item.Attributes["ValueType"].Value;
                            dynamic value;
                            switch (ParamInfo.ValueTypeEnum)
                            {
                                case SystemCommon.ValueType.Bool:
                                    value = item.Attributes["Value"].Value.ToBool();
                                    break;
                                case SystemCommon.ValueType.Int:
                                    value = item.Attributes["Value"].Value.ToInt();
                                    break;
                                case SystemCommon.ValueType.Decimal:
                                    value = item.Attributes["Value"].Value.ToDecimal();
                                    break;
                                default:
                                    value= item.Attributes["Value"].Value;
                                    break;
                            }
                            ParamInfo.Value = value;
                            ParamInfo.Description = item.Attributes["Description"].Value;
                            ParamInfoList.Add(ParamInfo);
                        }
                    }
                    
                }
                return ParamInfoList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



       
        /// <summary>
        /// InsertXml
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static int InsertXml(string Key, string Value)
        {
            try
            {
                int IsExistS = 0;
                doc.Load(path_exe.FullName + @"/XmlConfig/SystemConfiguration.xml");
                XmlNode root = doc.SelectSingleNode("SystemConfiguration");
                XmlNode root1 = root.SelectSingleNode("Configurations");
                XmlNode IsExist = root1.SelectSingleNode(Key);
                if (IsExist == null)
                {
                    XmlNode Leader = doc.CreateElement(Key);
                    Leader.InnerText = Value;
                    root1.AppendChild(Leader);
                    doc.Save(path_exe.FullName + @"/XmlConfig/SystemConfiguration.xml");
                    IsExistS++;
                }
                return IsExistS;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public static void SetCache(string CacheKey, object objObject)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            objCache.Insert(CacheKey, objObject);
        }

        /// <summary>
        /// 获取数据缓存
        /// </summary>
        /// <param name="CacheKey">键</param>
        public static object GetCache(string CacheKey)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            return objCache[CacheKey];
        }

        public static object GetXml() {
            var obj =XmlGlobalinfo.GetCache("XmlListString");
            if (obj==null)
            {
                List<SystemCommon.SystemConfigParam> ListString = XmlGlobalinfo.GetXmlValue();//待用
                XmlGlobalinfo.SetCache("XmlListString", ListString);
            }
            return obj;
        }
        /// <summary>
        /// 移除全部缓存
        /// </summary>
        public static void RemoveAllCache()
        {
            System.Web.Caching.Cache _cache = HttpRuntime.Cache;
            IDictionaryEnumerator CacheEnum = _cache.GetEnumerator();
            while (CacheEnum.MoveNext())
            {
                _cache.Remove(CacheEnum.Key.ToString());
            }
        }

        /// <summary>
        /// Excel导入成Datable
        /// </summary>
        /// <param name="file">导入路径(包含文件名与扩展名)</param>
        /// <returns></returns>
        public static DataTable ExcelToTable(string file)
        {
            DataTable dt = new DataTable();
            IWorkbook workbook;
            string fileExt = Path.GetExtension(file).ToLower();
            using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                //XSSFWorkbook 适用XLSX格式，HSSFWorkbook 适用XLS格式
                if (fileExt == ".xlsx") { workbook = new XSSFWorkbook(fs); } else if (fileExt == ".xls") { workbook = new HSSFWorkbook(fs); } else { workbook = null; }
                if (workbook == null) { return null; }
                ISheet sheet = workbook.GetSheetAt(0);

                //表头  
                IRow header = sheet.GetRow(sheet.FirstRowNum);
                List<int> columns = new List<int>();
                for (int i = 0; i < header.LastCellNum; i++)
                {
                    object obj = GetValueType(header.GetCell(i));
                    if (obj == null || obj.ToString() == string.Empty)
                    {
                        dt.Columns.Add(new DataColumn("Columns" + i.ToString()));
                    }
                    else
                        dt.Columns.Add(new DataColumn(obj.ToString()));
                    columns.Add(i);
                }
                //数据  
                for (int i = sheet.FirstRowNum + 1; i <= sheet.LastRowNum; i++)
                {
                    DataRow dr = dt.NewRow();
                    bool hasValue = false;
                    foreach (int j in columns)
                    {
                        dr[j] = GetValueType(sheet.GetRow(i).GetCell(j));
                        if (dr[j] != null && dr[j].ToString() != string.Empty)
                        {
                            hasValue = true;
                        }
                    }
                    if (hasValue)
                    {
                        dt.Rows.Add(dr);
                    }
                }
            }
            return dt;
        }

        /// <summary>
        /// 获取单元格类型
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private static object GetValueType(ICell cell)
        {
            if (cell == null)
                return null;
            switch (cell.CellType)
            {
                case CellType.Blank: //BLANK:  
                    return null;
                case CellType.Boolean: //BOOLEAN:  
                    return cell.BooleanCellValue;
                case CellType.Numeric: //NUMERIC:  
                    return cell.NumericCellValue;
                case CellType.String: //STRING:  
                    return cell.StringCellValue;
                case CellType.Error: //ERROR:  
                    return cell.ErrorCellValue;
                case CellType.Formula: //FORMULA:  
                default:
                    return "=" + cell.CellFormula;
            }
        }
    }
}