﻿using HuNanChangJie.Loger;
using HuNanChangJie.Util;
using HuNanChangJie.Util.Operat;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace HuNanChangJie.Application.Web
{
    /// <summary>
    /// 日 期：2017.03.08
    /// 描 述：基础控制器
    /// </summary>
    [HandlerLogin(FilterMode.Enforce)]
    public abstract class MvcControllerBase : Controller
    {
        #region  日志操作
        /// <summary>
        /// 日志对象实体
        /// </summary>
        private Log _logger;
        /// <summary>
        /// 日志操作
        /// </summary>
        public Log Logger
        {
            get { return _logger ?? (_logger = LogFactory.GetLogger(this.GetType().ToString())); }
        }
        #endregion

        #region  请求响应
        /// <summary>
        /// 返回成功消息
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected virtual ActionResult ToJsonResult(object data)
        {
            return Content(data.ToJson());
        }
        /// <summary>
        /// 返回成功消息
        /// </summary>
        /// <param name="info">消息</param>
        /// <returns></returns>
        protected virtual ActionResult Success(string info)
        {
            return Content(new ResParameter { code = ResponseCode.success, info = info, data = new object { } }.ToJson());
        }

        protected virtual ActionResult Success(object data)
        {
            return Content(new ResParameter { code = ResponseCode.success, info = "响应成功", data = data }.ToJson());
        }
        /// <summary>
        /// 返回成功消息
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected virtual ActionResult SuccessString(string data)
        {
            return Content(new ResParameter { code = ResponseCode.success, info = "响应成功", data = data }.ToJson());
        }
        /// <summary>
        /// 返回成功数据
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected virtual ActionResult JsonResult(object data)
        {
            return Content(new ResParameter { code = ResponseCode.success, info = "响应成功", data = data }.ToJson());
        }
        /// <summary>
        /// 返回成功消息
        /// </summary>
        /// <param name="info">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected virtual ActionResult Success(string info, object data)
        {
            return Content(new ResParameter { code = ResponseCode.success, info = info, data = data }.ToJson());
        }

        /// <summary>
        /// 带操作日志
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected virtual ActionResult Success(string info, string title, OperationType type, string keyValue, string content)
        {
            OperateLogModel operateLogModel = new OperateLogModel();
            operateLogModel.title = title;
            operateLogModel.type = type;
            operateLogModel.url = (string)WebHelper.GetHttpItems("currentUrl");
            operateLogModel.sourceObjectId = keyValue;
            operateLogModel.sourceContentJson = content;

            OperatorHelper.Instance.WriteOperateLog(operateLogModel);

            return Content(new ResParameter { code = ResponseCode.success, info = info, data = new object { } }.ToJson());
        }

        /// <summary>
        /// 返回失败消息
        /// </summary>
        /// <param name="info">消息</param>
        /// <returns></returns>
        protected virtual ActionResult Fail(string info)
        {
            return Content(new ResParameter { code = ResponseCode.fail, info = info }.ToJson());
        }
        /// <summary>
        /// 返回失败消息
        /// </summary>
        /// <param name="info">消息</param>
        /// <param name="data">消息</param>
        /// <returns></returns>
        protected virtual ActionResult Fail(string info, object data)
        {
            return Content(new ResParameter { code = ResponseCode.fail, info = info, data = data }.ToJson());
        }
        #endregion


        /// <summary>
        ///     压缩html代码
        /// </summary>
        /// <param name="text">html代码</param>
        /// <returns></returns>
        private static string Compress(string text)
        {
            var reg = new Regex(@"\s*(</?[^\s/>]+[^>]*>)\s+(</?[^\s/>]+[^>]*>)\s*");
            text = reg.Replace(text, m => m.Groups[1].Value + m.Groups[2].Value);

            //移除html标签之间的空格 
            text = new Regex(@"(?<=>)[\s|\n|\t]*(?=<)").Replace(text, string.Empty);

            //移除多余空格和换行符
            text = new Regex(@"\n+\s+").Replace(text, string.Empty);

            return text;
        }

        /// <summary>
        ///     在执行Action的时候，就把需要的Writer存起来
        /// </summary>
        /// <param name="filterContext">上下文</param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            //if (filterContext.HttpContext.Response.ContentType == "text/html")
            //{
            //    _builder = new StringBuilder();
            //    _writer = new StringWriter(_builder);
            //    _htmlWriter = new HtmlTextWriter(_writer);
            //    _output = (HttpWriter)filterContext.RequestContext.HttpContext.Response.Output;
            //    filterContext.RequestContext.HttpContext.Response.Output = _htmlWriter;
            //}



            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        ///     在执行完成后，处理得到的HTML，并将他输出到前台
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            //if (filterContext.HttpContext.Response.ContentType == "text/html")
            //{
            //    var hc = Config.GetValue("HtmlCompressor");
            //    if (hc == "true")
            //    {
            //        var response = Compress(_builder.ToString());
            //        _output.Write(response);
            //    }
            //    else
            //    {
            //        _output.Write(_builder.ToString());
            //    }
            //}


            base.OnResultExecuted(filterContext);

        }

        #region Private

        /// <summary>
        ///     HtmlTextWriter
        /// </summary>
        private HtmlTextWriter _htmlWriter;

        /// <summary>
        ///     StringWriter
        /// </summary>
        private StringWriter _writer;

        /// <summary>
        ///     StringBuilder
        /// </summary>
        private StringBuilder _builder;

        /// <summary>
        ///     HttpWriter
        /// </summary>
        private HttpWriter _output;

        #endregion




    }
}