﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-22 15:55
 * 描  述：合同类型
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Base_ContractType";
var processCommitUrl = top.$.rootUrl + '/SystemModule/BaseContractType/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $("#BaseSubjectId").mkselect();
            $("input[name='Property']").on("change", function () {
                var value = $("input[name='Property']:checked").val();
                $("#Property").val(value);
                var paymentType = "";
                if (value == "Project") {
                    paymentType = "in";
                }
                else {
                    paymentType = "out";
                }
                $("#BaseSubjectId").mkselectRefresh({
                    url: top.$.rootUrl + "/ProjectModule/Subject/GetSubjectList?type=" + paymentType,
                    value: "ID",
                    title: "FullName",
                    text:"FullName"
                });

            });
            
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/BaseContractType/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            
                            if (id == "Base_ContractType") {
                                $("#Property-" + data[id].Property).trigger("click")
                            }
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/BaseContractType/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
