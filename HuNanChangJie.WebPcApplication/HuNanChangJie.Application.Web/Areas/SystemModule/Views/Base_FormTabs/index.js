﻿/* * Copyright (c)  
 * 创建人：超级管理员
 */
var refreshGirdData;
var formId = request("formId");
var moduleId;
var selectedRow;
var bootstrap = function ($, Changjie) {
    "use strict";
    var formId;
    var page = {
        init: function () {
            page.inittree();
            page.initGird();
            page.bind();
        },
        bind: function () {
            $("#add,#edit,#delete").attr("disabled", "disabled");
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                selectedRow = null;
                Changjie.layerForm({
                    id: 'form',
                    title: '新增Tab页签',
                    url: top.$.rootUrl + '/SystemModule/Base_FormTabs/Form?formId=' + formId,
                    width: 1000,
                    height: 800,
                    btn: null,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        inittree: function () {
            $('#module_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/Module/GetModuleTree',
                nodeClick: page.treeNodeClick
            });
        },
        treeNodeClick: function (item) {
            function getUrlParam(url, name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
                var r = url.substr(1).match(reg);  //匹配目标参数
                if (r != null) return unescape(r[2]); return null; //返回参数值
            }
            if (item.url) {
                formId = getUrlParam(item.url, "formId");
            }
            $('#titleinfo').text(item.text);
            if (!!formId == false ||formId=="null") {
                $("#add,#edit,#delete").attr("disabled", "disabled");
                formId = 'null';
            }
            else {
                $("#add,#edit,#delete").removeAttr("disabled");
            }
            page.search({ formId: formId });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/Base_FormTabs/GetPageList',
                headData: [
                    { label: "Tab名称", name: "TabName", width: 250, align: "left" },
                    { label: "排序号", name: "SortCode", width: 250, align: "left" },
                    {
                        label: "启用状态", name: "Enabled", width: 350, align: "center",
                        formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case true:
                                    return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                                default:
                                    return '<span class="label label-default" style="cursor: pointer;">未启用</span>';
                            }
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/Base_FormTabs/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    selectedRow = $('#gridtable').jfGridGet('rowdata');;
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title+"编辑FormTab",
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/SystemModule/Base_FormTabs/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 1000,
            height: 800,
            btn: null,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
