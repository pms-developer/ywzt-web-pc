﻿
var keyValue = request('keyValue');
var columnData = null;
var parentColumnData = [];
var formId = request("formId");
var type = "add";
var bootstrap = function ($, Changjie) {
    "use strict";
    var selectedRow = Changjie.frameTab.currentIframe().selectedRow;
    if (!!selectedRow) {
        columnData = JSON.parse(selectedRow.HeadInfo);
    }
    var page = {
        init: function () {
            if (!!keyValue) {
                type = "edit";
            }
            else {
                keyValue = top.Changjie.newGuid();
                type = "add";
            }
            page.bind();
            page.initData();
        },
        bind: function () {

            // 加载导向
            $('#wizard').wizard().on('change', function (e, data) {
                var $finish = $("#btn_finish");
                var $next = $("#btn_next");
                if (data.direction == "next") {
                    if (data.step == 1) {
                        if (!$('#step-1').mkValidform()) {
                            return false;
                        }
                       if (!!columnData == false) {
                            var isloadsuccess = page.loadColumn();
                            if (!isloadsuccess)
                                return false;
                        }
                        $finish.removeAttr('disabled');
                        $next.attr('disabled', 'disabled')
                    }
                } else {
                    $finish.attr('disabled', 'disabled');
                    $next.removeAttr('disabled');
                }
            });

            $("#refresh").on("click", function () {
                page.loadColumn();
            });
            $("#columngrid").jfGrid({
                headData: [
                    {
                        label: "显示名称", name: "Name", width: 140, align: "left",
                        edit: {
                            type: 'input',
                            change: function (row, index, item, oldValue, colname, headData) {
                                if (!!row.isParent) {
                                    for (var i in parentColumnData) {
                                        var info = parentColumnData[i];
                                        if (info.id != row.id) continue;
                                        info.Name = row.Name;
                                        break;
                                    }
                                }
                            },
                        }
                    },
                    { label: "字段名", name: "Field", width: 140, align: "left" },
                    {
                        label: "是否显示", name: "IsShow", width: 80, align: "center",
                        formatter: function (cellvalue) {

                            var val = JSON.parse(cellvalue || "false");

                            return val == true ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: true// 默认选中项
                        }
                    },
                    {
                        label: "是否汇总", name: "IsSum", width: 80, align: "center",
                        formatter: function (cellvalue) {

                            var val = JSON.parse(cellvalue || "false");

                            return val == true ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: false// 默认选中项
                        }
                    }, {
                        label: "千位分隔", name: "IsSeparate", width: 80, align: "center",
                        formatter: function (cellvalue) {

                            var val = JSON.parse(cellvalue || "false");

                            return val == true ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: false// 默认选中项
                        }
                    },
                    {
                        label: '上级列名', name: 'ParentColumn', width: 100
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {

                                row.parentId = row.ParentColumn;

                                $('#columngrid').jfGridSet('refreshdata');

                            },
                            click: function ($this) {
                                $this.mkselectRefresh({ data: parentColumnData });
                            },
                            op: {
                                value: "id",
                                text: "Name",
                                data: parentColumnData
                            }
                        }
                    }, {
                        label: "显示宽度", name: "Width", width: 80, align: "center",
                        edit: {
                            type: 'input',
                            change: function (row, index, item, oldValue, colname, headData) {
                                var r = /^\+?[1-9][0-9]*$/;　　//正整数

                            },
                        }
                    },
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#columngrid').jfGridSet('moveUp', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#columngrid').jfGridSet('moveDown', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-warning\" style=\"cursor: pointer;\">下移</span>';
                        }
                    },
                ],
                height: 400,
                isTree: true,
                mainId: 'id',
                isEdit: true,
                onAddRow: function (row, rows) {
                    row.id = Changjie.newGuid();
                    row.isParent = true;
                    row.parentId = '';
                    parentColumnData.push({ id: row.id });
                },
                onMinusRow: function (row, rows) {
                }
            });
 
            //状态
            $("#Enabled").mkDataItemSelect({ code: 'BESF' }).mkselectSet(true);

            //数据库
            $('#F_DataSourceId').mkselect({
                url: top.$.rootUrl + '/SystemModule/DatabaseLink/GetTreeList',
                type: 'tree',
                placeholder: '请选择数据库',
            });
            //完成事件
            $("#btn_finish").click(function () {
                if (!$('#wizard-steps').mkValidform()) {
                    return false;
                }
                var postData = $('#wizard-steps').mkGetFormData(keyValue);
                var griddata = $("#columngrid").jfGridGet("rowdatas");
                postData.HeadInfo = JSON.stringify(griddata);
                if (!!formId) {
                    postData.BaseSystemFormID = formId;
                }
                debugger;
                $.mkSaveForm(top.$.rootUrl + '/SystemModule/Base_FormTabs/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                    Changjie.frameTab.currentIframe().refreshGirdData();
                });
            })
        },
        loadColumn: function () {
            var param = {};
            var sql = $("#SqlInfo").val().toLowerCase();
            while (sql.indexOf("@this.") > -1) {
                var tempList = sql.split("@this.");
                sql = tempList[0];
                var tmpList = tempList[1].split("@");
                sql +=" null " + tmpList[1]
            }

            param.sqlString=sql
            console.log(param.sqlString);
            var isloadsuccess = true;
            Changjie.httpPost(top.$.rootUrl + "/ReportModule/ReportManage/GetColumnsNoTry", param, function (res) {
                if (res.code == 200) {
                    $('#columngrid').jfGridSet('refreshdata', res.data);
                    page.refreshSelect(res.data);
                }
                else {
                    isloadsuccess = false;
                    Changjie.alert.error(res.info);
                }
            });
            return isloadsuccess;
        },
        refreshSelect: function (data) {
            var op = {
                value: "Field",
                text: "Field",
                title: "Field",
                data: data
            };
            $("#ParentField").mkselectRefresh(op);
        },
        initData: function () {
            if (!!selectedRow) {
                $('#wizard-steps').mkSetFormData(selectedRow);
                $('#columngrid').jfGridSet('refreshdata', columnData);
                page.refreshSelect(columnData);
            }
        }
    };
    page.init();
}


