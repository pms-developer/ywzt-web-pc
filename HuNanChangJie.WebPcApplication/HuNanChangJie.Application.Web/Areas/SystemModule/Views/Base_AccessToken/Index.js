﻿/* *  
 * 创建人：超级管理员
 * 日  期：2021-01-12 19:56
 * 描  述：Base_AccessToken
 */
var selectedRow;
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                selectedRow = null;
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/SystemModule/Base_AccessToken/Form',
                    width: 700,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/Base_AccessToken/GetPageList',
                headData: [
                        { headerName: 'id', field: 'id', width: 200,  cellStyle: { 'text-align': 'left' },
                        { headerName: 'AppType', field: 'AppType', width: 200,  cellStyle: { 'text-align': 'left' },
                        { headerName: 'AppKey', field: 'AppKey', width: 200,  cellStyle: { 'text-align': 'left' },
                        { headerName: 'AccessToken', field: 'AccessToken', width: 200,  cellStyle: { 'text-align': 'left' },
                ],
                mainId:'id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function (){
                var keyValue = $('#gridtable').jfGridValue('id');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/Base_AccessToken/DeleteForm', { keyValue: keyValue}, function () {
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('id');
                selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/SystemModule/Base_AccessToken/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 700,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            }
