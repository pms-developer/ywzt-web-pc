﻿/* *  
 * 创建人：超级管理员
 * 日  期：2021-01-12 19:56
 * 描  述：Base_AccessToken
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    "use strict";
    var selectedRow = Changjie.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.initData();
        },
        bind: function () {
        },
        initData: function () {
            if (!!selectedRow) {
                $('#form').mkSetFormData(selectedRow);
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData();
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/Base_AccessToken/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
