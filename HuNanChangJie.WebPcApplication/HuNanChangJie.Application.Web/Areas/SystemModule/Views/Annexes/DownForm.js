﻿var keyVaule = request('keyVaule');
var bootstrap = function ($, Changjie) {
    "use strict";

    $.mkSetForm(top.$.rootUrl + '/SystemModule/Annexes/GetAnnexesFileList?infoId=' + keyVaule, function (data) {
        for (var i = 0, l = data.length; i < l; i++) {
            $('#form_file_queue .mk-form-file-queue-bg').hide();
            var item = data[i];
            var $item = $('<div class="mk-form-file-queue-item" id="filequeue_' + item.F_Id + '" ></div>');
            $item.append('<div class="mk-file-image"><img src="' + top.$.rootUrl + '/Content/images/filetype/' + item.F_FileType + '.png"></div>');
            $item.append('<span class="mk-file-name">' + item.F_FileName + '(' + Changjie.countFileSize(item.F_FileSize) + ')</span>');
            $item.append('<div class="mk-tool-bar"><i class="fa fa-cloud-download" title="下载"  data-value="' + item.F_Id + '" ></i></div>');

            $item.find('.mk-tool-bar .fa-cloud-download').on('click', function () {
                var fileId = $(this).attr('data-value');
                DownFile(fileId);
            });

            $('#form_file_queue_list').append($item);
        }
    });
    // 下载文件
    var DownFile = function (fileId) {
        Changjie.download({ url: top.$.rootUrl + '/SystemModule/Annexes/DownAnnexesFile', param: { fileId: fileId, __RequestVerificationToken: $.mkToken }, method: 'POST' });
    }

   

    $('#form_file_queue').mkscroll();
}
