﻿/*
 * 日 期：2017.04.17
 * 描 述：新建表
 */
var databaseLinkId = request('databaseLinkId');
var draftId = '';

var add = 1;
var del = 2;
var rename = 4;
var resetDataType = 8;
var resetIsNull = 16;
var resetDeiscrption = 32;

var editColumns = [];

var originalData = [];

function setFieldState(row, index, columnName, type, newValue, oldValue) {
  
    if (!!!row.FieldState) {
        row.FieldState = 0;
    }
    if (row.FieldState == add) return;
    var item = { "columnName": columnName, "index": index, "FieldState": type };
    if (newValue != oldValue) {
        var isExist = false;
        if (editColumns.length <= 0) {
            editColumns.push(item);
            row.FieldState = row.FieldState + type;
        }
        else {
            for (var info in editColumns) {
                if (editColumns[info].columnName == columnName && editColumns[info].index == index) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                row.FieldState = row.FieldState + type;
                editColumns.push(item);
            }
        }
    }
    else {//编辑的值还原为原始值时去除修改状态(还原为编辑前的状态)
        var idx = -1;
        for (var i = 0; i < editColumns.length; i++) {
            if (editColumns[i].columnName == columnName && editColumns[i].index == index) {
                idx = i;
                break;
            }
        }
        if (idx >= 0) {
            row.FieldState = row.FieldState - type;
            editColumns.splice(idx, 1);//移除元素
        }
    }
    
}

var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            page.initForm();
            page.bind();
            page.initGrid();
            page.initData();
            
        },
        initForm: function () {
            var tableName = request("tableName");
            if (!!tableName) {
                $("#tableName").val(tableName);
                $("#remark").val(request("des"));
            }
        },
        bind: function () {
            /*复制表*/
            $('#copytable').on('click', function () {
                Changjie.layerForm({
                    id: 'copyform',
                    title: '复制表',
                    url: top.$.rootUrl + '/SystemModule/DatabaseTable/CopyTableForm',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            $('#gridtable').jfGridSet('refreshdata', []);
                            Changjie.httpAsyncGet(top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldList?databaseLinkId=' + data.databaseLinkId + '&tableName=' + data.tableName, function (res) {
                                Changjie.clientdata.getAllAsync('dataItem', {
                                    code: 'DbFieldType',
                                    callback: function (dataes) {
                                        var map = {};
                                        $.each(dataes, function (_index, _item) {
                                            map[_item.value] = _item.text;
                                        });
                                        $.each(res.data, function (_index, _item) {
                                            _item.f_datatypename = map[_item.f_datatype];
                                            _item.f_isnullable = _item.f_isnullable || '0';
                                            _item.f_key = _item.f_key || '0';
                                        });
                                        $('#gridtable').jfGridSet('refreshdata', res.data);
                                    }
                                });
                            });
                        });
                    }
                });
            });

            /*复制行*/
            $('#copyrow').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length > 0) {
                    $.each(selectedRow, function (_index, _item) {
                        $('#gridtable').jfGridSet('addRow', JSON.parse(JSON.stringify(_item)));
                    });
                }
                else {
                    top.Changjie.alert.warning('请选择需要复制的行!');
                }
            });

            /*常用字段*/
            $('#commonfield').on('click', function () {
                Changjie.layerForm({
                    id: 'commonfieldform',
                    title: '常用字段选择',
                    url: top.$.rootUrl + '/SystemModule/DbField/SelectForm',
                    width: 600,
                    height: 500,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (data) {
                                Changjie.clientdata.getAllAsync('dataItem', {
                                    code: 'DbFieldType',
                                    callback: function (dataes) {
                                        var map = {};
                                        $.each(dataes, function (_index, _item) {
                                            map[_item.value] = _item.text;
                                        });
                                        
                                        if (data.length > 0) {
                                            $.each(data, function (_index, _item) {
                                                var _point = {
                                                    f_column: _item.F_Name,
                                                    f_remark: _item.F_Remark,
                                                    f_datatype: _item.F_DataType,
                                                    f_datatypename: map[_item.F_DataType],
                                                    f_length: _item.F_Length,
                                                    f_key: '0',
                                                    f_isnullable: '1'
                                                }
                                                $('#gridtable').jfGridSet('addRow', _point);
                                            });
                                        }
                                        else {
                                            var _point = {
                                                f_column: data.F_Name,
                                                f_remark: data.F_Remark,
                                                f_datatype: data.F_DataType,
                                                f_datatypename: map[data.F_DataType],
                                                f_length: data.F_Length,
                                                f_key: '0',
                                                f_isnullable: '1'
                                            }
                                            $('#gridtable').jfGridSet('addRow', _point);
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            });

            /*导入草稿*/
            $('#draft').on('click', function () {
                Changjie.layerForm({
                    id: 'draftform',
                    title: '导入草稿',
                    url: top.$.rootUrl + '/SystemModule/DatabaseTable/DraftForm',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (data) {
                                draftId = data.F_Id;
                                $('#form').mkSetFormData({ tableName: data.F_Name, remark: data.F_Remark});
                                $('#gridtable').jfGridSet('refreshdata', JSON.parse(data.F_Content));
                            }
                        });
                    }
                });
            });


            /*保存草稿*/
            $('#savedraft').on('click', function () {
                if (!$('#form').mkValidform()) {
                    return false;
                }
                var formData = $('#form').mkGetFormData();
                var grid = $('#gridtable').jfGridGet('rowdatas');

                var postData = {
                    F_Name: formData.tableName,
                    F_Remark: formData.remark,
                    F_Content: JSON.stringify(grid),
                    F_DbLinkId: databaseLinkId
                };
                $.mkSaveForm(top.$.rootUrl + '/SystemModule/DatabaseTable/SaveDraft?keyValue=' + draftId, postData, function (res) {
                    // 保存成功后才回调
                  
                    if (res.code == 200) {
                        draftId = res.data;
                    }
                }, true);
            });

            /*发布*/
            $('#release').on('click', function () {
                if (!$('#form').mkValidform()) {
                    return false;
                }
                var formData = $('#form').mkGetFormData();
                var grid = $('#gridtable').jfGridGet('rowdatas');
                //判断字段是否合法
                if (!page.checkGrid(grid)) {
                    return false;
                }

                var tableEditInfo = [];
                var tableName = request("tableName");
                if (!!tableName) {
                    var rename = 1;
                    var resetDes = 2;
                    var editState = 0;

                    var newName = formData.tableName;
                    if (newName!= tableName)
                        editState += rename;
                    var des = formData.remark;
                    if (des != request("des"))
                        editState += resetDes;
                    var item = { "OldName": tableName, "NewName": newName, "Description": des, "EditState": editState };
                    tableEditInfo.push(item);
                }

                var postData = {
                    databaseLinkId: databaseLinkId,
                    draftId: draftId,
                    tableName: formData.tableName,
                    tableRemark: formData.remark,
                    strColList: JSON.stringify(grid),
                    deleteList: $('#gridtable').jfGridDelKeys().toString(),
                    tableEditInfo: JSON.stringify(tableEditInfo)
                };

                if (!!tableName) {//编辑
                    var url = top.$.rootUrl + '/SystemModule/DatabaseTable/EditTable';
                    $.mkSaveForm(url, postData, function (res) {
                        if (res.code == 200) {
                            draftId = '';
                            $('#gridtable').jfGridSet('refreshdata');
                        }
                    }, true);
                }
                else {//新增
                    $.mkSaveForm(top.$.rootUrl + '/SystemModule/DatabaseTable/SaveTable', postData, function (res) {
                        // 新建成功后刷新页面
                        if (res.code == 200) {
                            draftId = '';
                            $('#form').mkSetFormData({ tableName: '', remark: '' });
                            $('#gridtable').jfGridSet('refreshdata', [{}]);
                        }
                    }, true);
                }
            });
        },
        //检查列名是否为空或重复
        checkGrid: function (data) {
            // 1.列名不能为空;2.字串类型长度不能为0或不填3.数据类型不能为空4.列名不能重复
            var map = {};
            var flag = true;

            $.each(data, function (_index, _item) {

                if (_item.f_column === undefined || _item.f_column === '') {
                    Changjie.alert.error('【第' + _index + '列】列名为空');
                    flag = false;
                    return false;
                }
                else if (_item.f_datatype === undefined || _item.f_datatype === '') {
                    Changjie.alert.error('【第' + _index + '列】数据类型为空');
                    flag = false;
                    return false;
                }
                else if (_item.f_datatype == 'varchar' && (_item.f_length == 0 || _item.f_length == undefined)) {
                    Changjie.alert.error('【第' + _index + '列】字串长度设置错误');
                    flag = false;
                    return false;
                }
                else if (map[_item.f_column] != undefined ){
                    Changjie.alert.error('【第' + _index + '列与第' + map[_item.f_column] + '列】列名重复');
                    flag = false;
                    return false;
                }
                map[_item.f_column] = _index;
            });
            return flag;
        },
        /*初始化表格*/
        initGrid: function () {
            // 订单产品信息
            $('#gridtable').jfGrid({
                headData: [
                    {
                        label: "列名", name: "f_column", width: 180, align: "left",
                        formatter: function (cellvalue, row) {
                            if (row.IsReadOnly) {
                                return "<span style='color:red'>" + cellvalue + "</span>";
                            }
                            return cellvalue;
                        },
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, columnName) {
                                setFieldState(row, index, columnName, rename, row.f_column, oldValue);
                                row.OldValue = oldValue;
                            }
                        }
                    },
                    {
                        label: "数据类型", name: "f_datatype", width: 110, align: "center",
                        formatter: function (cellvalue, row) {
                            var res="";
                            Changjie.clientdata.getAllAsync('dataItem', {
                                code: 'DbFieldType',
                                callback: function (dataes) {
                                    $.each(dataes, function (_index, _item) {
                                        if (_item.value == cellvalue) {
                                            res = _item.text;
                                            return false;
                                        }
                                    });
                                }
                            });
                            return res;
                        },
                        edit: {
                            type: 'select',
                            op: {
                                value: 'F_ItemValue',
                                text: 'F_ItemName',
                                title: 'F_ItemName',
                                url: top.$.rootUrl + '/SystemModule/DataItem/GetDetailList',
                                param: { itemCode: 'DbFieldType' },
                            },
                            change: function (row, index, selectdata, oldValue,columnName) {// 行数据和行号
                               
                                if (!!selectdata) {
                                    
                                    row.f_datatypename = selectdata.F_ItemName;
                                    row.f_datatype = selectdata.F_ItemValue;
                                    if (selectdata.F_ItemValue == 'varchar') {
                                        row.f_length = 50;
                                    }
                                    else {
                                        row.f_length = 0;
                                    }
                                }
                                else {
                                    row.f_length = 0;
                                    row.f_datatype = '';
                                    row.f_datatypename = '';
                                }
                                setFieldState(row, index, columnName, resetDataType, row.f_datatype, oldValue);
                                $('#gridtable').jfGridSet('updateRow', index);
                            }
                        }
                    },
                    {
                        label: "长度", name: "f_length", width: 57, align: "center",
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, columnName) {
                                setFieldState(row, index, "f_datatype", resetDataType, row.f_length, oldValue);//修改数据长度等同于修改数据类型  
                            }
                        }
                    },
                    {
                        label: "允许空", name: "f_isnullable", width: 80, align: "center",
                        formatter: function (cellvalue) {
                             return cellvalue == 1 ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            change: function (row, index, oldValue, columnName) {
                                setFieldState(row, index, columnName, resetIsNull, row.f_isnullable, oldValue);//修改数据长度等同于修改数据类型
                            },
                            data: [
                                { 'id': '0', 'text': '否' },
                                { 'id': '1', 'text': '是' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }
                    },
                    {
                        label: "主键", name: "f_key", width: 80, align: "center",
                        formatter: function (cellvalue) {
                            return cellvalue == 1 ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            change: function (data, num) {// 行数据和行号
                                if (data.f_key == "1") {
                                    data.f_isnullable = '0';
                                    $('#gridtable').jfGridSet('updateRow', num);
                                }
                            },
                            data: [
                                { 'id': '0', 'text': '否' },
                                { 'id': '1', 'text': '是' }
                            ],
                            dfvalue: '0'// 默认选中项
                        }
                    },
                    {
                        label: "说明", name: "f_remark", width: 100, align: "left",
                        formatter: function (cellValue, row) {
                            if (row.IsReadOnly == true)
                                return cellValue + "-系统默认字段";
                            return cellValue;
                        },
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, columnName) {
                                setFieldState(row, index, columnName, resetDeiscrption, row.f_remark, oldValue);//修改数据长度等同于修改数据类型
                            },
                        }
                    }
                ],
                onAddRow: function (row, rows) {
                    row.f_key = '0';
                    row.f_isnullable = '1';
                    row.FieldState = add;
                },
                isEdit: true,
                isMultiselect: true,
                mainId:"f_column",
            });
        },
        initData: function () {
            var tableName = request('tableName');
            var databaseLinkId = request('databaseLinkId');
            if (!!tableName) {//编辑
                var url = top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldList?databaseLinkId=' + databaseLinkId + '&tableName=' + tableName;
                $.mkSetForm(url, function (data) {
                    originalData = data;
                    $('#gridtable').jfGridSet('refreshdata', data, "edit");
                });
            } 
            else {//新增
                var url = top.$.rootUrl + '/SystemModule/DatabaseTable/GetSysDefaultFieldList';
                $.mkSetForm(url, function (data) {
                    $('#gridtable').jfGridSet('refreshdata', data);
                });
            }
        }
    };
    page.init();
}

