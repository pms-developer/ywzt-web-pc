﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-11-27 21:29
 * 描  述：公司公告
 */
var refreshGirdData;
var formId = request("formId");
var checkJson = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 400);
            $('#LeiXing').mkDataItemSelect({ code: 'GGFL' });
            $('#ZhongYao').mkDataItemSelect({ code: 'GGFL' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/SystemModule/BASE_CJ_GongGao/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/SystemModule/BASE_CJ_GongGao/Form?keyValue=' + keyValue,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/SystemModule/BASE_CJ_GongGao/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
           
            //  发布
            $('#fabu').on('click', function () {
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/BASE_CJ_GongGao/GetPageList',
                headData: [
                    { label: "标题", name: "BiaoTi", width: 100, align: "left"},
                    { label: "公告类型", name: "LeiXing", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'GGFL',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "重要程度", name: "ZhongYao", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'GGFL',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "发布范围", name: "FanWeiBuMeng", width: 100, align: "left"},
                    { label: "关键字", name: "GuanJianZi", width: 100, align: "left"},
                    { label: "内容", name: "NeiRong", width: 100, align: "left"},
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left"},
                    { label: "创建者名称", name: "CreationName", width: 100, align: "left"},
                    { label: "发布状态", name: "FaBuZhuangTai", width: 100, align: "left"},
                    { label: "范围 个人", name: "FanWeiGeRen", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
}
