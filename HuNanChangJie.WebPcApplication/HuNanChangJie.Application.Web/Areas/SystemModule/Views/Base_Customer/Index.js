﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-25 15:59
 * 描  述：客户管理
 */
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 240, 400);
            $('#CStatus').mkDataItemSelect({ code: 'KHZT' });
            $('#Grade').mkDataItemSelect({ code: 'KHDJ' });
            $('#CustomerType').mkDataItemSelect({ code: 'HYLB' });
            $('#ChargeUserId').mkUserSelect(0);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增客户',
                    url: top.$.rootUrl + '/SystemModule/Base_Customer/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
           
        },
        // 初始化列表
        initGird: function () {

            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/Base_Customer/GetPageList',
                headData: [
                    { label: "客户编码", name: "Code", width: 120, align: "left"},
                    { label: "客户性质", name: "Property", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'DWXZ',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "客户名称", name: "FullName", width: 200, align: "left"},
                    {
                        label: "客户来源", name: "Source", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'KHLY',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    {
                        label: "客户等级", name: "Grade", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'KHDJ',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "客户状态", name: "CStatus", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'KHZT',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "客户类型", name: "CustomerType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'HYLB',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    { label: "联系人", name: "Contacts", width: 100, align: "left" },
                    { label: "联系电话", name: "Phone", width: 100, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "所在地省", name: "ProvinceId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'Province',
                                 key: value,
                                 keyId: 'f_areaid',
                                 callback: function (_data) {
                                     callback(_data['f_areaname']);
                                 }
                             });
                        }},
                    { label: "所在地市", name: "CityId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'CityOrDistrict',
                                 key: value,
                                 keyId: 'f_areaid',
                                 callback: function (_data) {
                                     callback(_data['f_areaname']);
                                 }
                             });
                        }},
                    { label: "详细地址", name: "Address", width: 100, align: "left"},
                    { label: "发票抬头", name: "InvoiceTitle", width: 100, align: "left"},
                    { label: "税务登记号", name: "TaxID", width: 100, align: "left"},
                    { label: "开户银行", name: "BankName", width: 100, align: "left"},
                    { label: "银行账号", name: "BankAccount", width: 100, align: "left"},
                    { label: "地址", name: "InvoiceAddress", width: 100, align: "left"},
                    { label: "业务负责人", name: "ChargeUserId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "负责部门", name: "ChargeDepartmentId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('department', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "签约金额", name: "ContractedAmount", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};

            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        $("#btn_querySearch").click()
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/Base_Customer/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + "客户",
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/SystemModule/Base_Customer/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
