﻿/* *  
 * 创建人：超级管理员
 * 日  期：2019-11-29 14:14
 * 描  述：印章管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTalbe="";
var projectId = request('projectId');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (type == 'add') {
                page.loadProjectInfo();
            }
        },
        bind: function () {
            $('#LeiXing').mkDataItemSelect({ code: 'XZLB' });
            $('#BaoGuanRen').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#Enabled').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/Base_CJ_YinZhang/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
        loadProjectInfo: function () {
             if (!!projectId) {
                 top.Changjie.setProjectInfo('form_tabs');
             }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
        }
        if (type == "add") {
            keyValue = mainId;
        }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/Base_CJ_YinZhang/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
