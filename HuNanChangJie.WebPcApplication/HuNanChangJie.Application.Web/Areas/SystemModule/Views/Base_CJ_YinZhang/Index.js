﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-11-29 14:14
 * 描  述：印章管理
 */
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 初始化左侧树形数据
            $('#dataTree').mktree({
                url: top.$.rootUrl + '/SystemModule/Base_CJ_YinZhang/GetTree',
                nodeClick: function (item) {
                    console.log(item)
                    if (item.text == "所有") {
                        page.search();
                        $("#LeiXing").mkselectSet("")
                    }
                    else {
                        page.search({ LeiXing: item.text });
                        $("#LeiXing").mkselectSet(item.text)
                    }
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#LeiXing').mkDataItemSelect({ code: 'XZLB' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/SystemModule/Base_CJ_YinZhang/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            }); 
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/Base_CJ_YinZhang/GetPageList',
                headData: [
                    { label: "印章类别", name: "LeiXing", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'XZLB',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "印章名称", name: "MingChen", width: 400, align: "left"},
                    { label: "保管人", name: "BaoGuanRen", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "是否启用", name: "Enabled", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'SZSF',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {

        page.search();
    };
    page.init();
}
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/Base_CJ_YinZhang/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: '编辑',
            url: top.$.rootUrl + '/SystemModule/Base_CJ_YinZhang/Form?keyValue=' + keyValue + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};

