﻿/* *   
 * 创建人：超级管理员 
 * 日  期：2021-01-28 16:43 
 * 描  述：系统弹窗配置 
 */
var selectedRow;
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.inittree();
            page.initGird();
            page.bind();
        },
        bind: function () {
            $("#add,#edit,#delete,#start").attr("disabled", "disabled");
            // 刷新 
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增 
            $('#add').on('click', function () {
                selectedRow = null;
                Changjie.layerForm({
                    id: 'form',
                    title: '弹出窗设计',
                    url: top.$.rootUrl + '/SystemModule/BaseConfirm/Form?formId=' + formId,
                    width: 800,
                    height: 550,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        inittree: function () {
            $('#module_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/Module/GetModuleTree',
                nodeClick: page.treeNodeClick
            });
        },
        treeNodeClick: function (item) {
            function getUrlParam(url, name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
                var r = url.substr(1).match(reg);  //匹配目标参数
                if (r != null) return unescape(r[2]); return null; //返回参数值
            }

            if (item.url) {
                formId = getUrlParam(item.url, "formId");

            } else {
                formId = 'null';
            }

            $('#titleinfo').text(item.text);
            if (!!formId == false || formId == "null") {
                $("#add,#edit,#delete,#start").attr("disabled", "disabled");
                formId = 'null';
            }
            else {
                $("#add,#edit,#delete,#start").removeAttr("disabled");
            }
            page.search({ formId: formId });
        },
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/BaseConfirm/GetPageList',
                headData: [
                    { label: "名称", name: "Name", width: 150, align: "left" },
                    {
                        label: "创建时间", name: "CreationDate", width: 120, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: '作用表单', name: 'BaseSystemFormName', width: 200, },
                    { label: '提示内容', name: 'InfoMsg', width: 200, },
                    {
                        label: '是否强制提醒', name: 'IsForce', width: 200, align: "center",
                        formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case true:
                                    return '<span class="label label-success" style="cursor: pointer;">是</span>';
                                default:
                                    return '<span class="label label-default" style="cursor: pointer;">否</span>';
                            }
                        }
                    },
                    { label: '条件表达式', name: 'Expression', width: 200, },
                    {
                        label: '是否启用', name: 'Enabled', width: 200,align: "center",
                        formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case true:
                                    return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                                default:
                                    return '<span class="label label-default" style="cursor: pointer;">未启用</span>';
                            }
                        }
                    },
                    { label: '排序号', name: 'SortCode', width: 200, }
                ],
                mainId: 'ID',
                isPage: true,
                onSelectRow: function (row) {
                    if (row.ID) {
                        $("#add,#edit,#delete,#start").removeAttr("disabled");
                    }
                }
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除 
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/BaseConfirm/DeleteForm', { keyValue: keyValue }, function () {
                });
            }
        });
    }
};
// 编辑 
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    selectedRow = $('#gridtable').jfGridGet('rowdata');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'editConfirmSetting',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/SystemModule/BaseConfirm/Form?keyValue=' + keyValue + '&viewState=' + viewState,
            width: 800,
            height: 550,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
} 
