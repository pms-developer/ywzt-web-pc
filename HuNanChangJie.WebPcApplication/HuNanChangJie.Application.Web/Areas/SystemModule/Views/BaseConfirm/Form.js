﻿
/* *   
 * 创建人：系统弹窗配置
 * 日  期：2021-01-28 16:43 
 * 描  述：顶替 
 */
var acceptClick;
var keyValue = request('keyValue');
var formId = request("formId");
var type = "add";
var mainfields;//主表字段
var custmerformlist = null;
var iscustom = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var selectedRow = Changjie.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            if (!!keyValue) {
                type = "edit";
            }
            else {
                keyValue = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            
        },
        bind: function () {
            //数据库
            $('#F_DataSourceId').mkselect({
                url: top.$.rootUrl + '/SystemModule/DatabaseLink/GetTreeList',
                type: 'tree',
                placeholder: '请选择数据库',
            });

            Changjie.httpAsync('GET', top.$.rootUrl + '/FormModule/Custmerform/GetFormList', {}, function (data) {
                custmerformlist = data;
                $('#BaseSystemFormId').mkselect({
                    data: data,
                    value: "F_Id",
                    text: "F_Name",
                    allowSearch: true
                }).on("change", function () {
                    var item = $(this).mkselectGetEx();
                    var param = {};
                    param.maintable = item.MainTable;
                    $("#MainTable").val(item.MainTable);
                    param.subs = item.SubTables;
                    param.type = item.Type;
                    param.schemeId = item.F_Id;
                    page.loadFormField(param);
                });
                if (formId) {
                    $('#BaseSystemFormId').attr("readonly", "readonly");
                    $('#BaseSystemFormId').mkselectSet(formId);
                }
                page.initData();
            });

            $('#IsForce').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $("#leftObj").mkselect({
                value: "f_column",
                text: "text",
                allowSearch: true,
                data: []
            });
            $("#operators").mkselect({
                placeholder: "运行符",
                data: [
                    { id: "==", text: "等于" },
                    { id: ">", text: "大于" },
                    { id: ">=", text: "大于等于" },
                    { id: "<", text: "小于" },
                    { id: "<=", text: "小于等于" },
                ]
            }).mkselectSet('==');
        },
        loadFormField: function (param) {
            
            if (param) {
                var customerFields = [];
                if (param.type == "自定义") {
                    iscustom = true;
                    var data = Changjie.httpGet(top.$.rootUrl + '/FormModule/Custmerform/GetFormSettingData', { schemeId: param.schemeId }).data;
                    customerFields = JSON.parse(data.SettingData).data[0];
                    $("#customControlId").show();
                    $("#customControlId").val("");
                    //$("#leftObj").hide();
                }
                else {
                    iscustom = false;
                    $("#customControlId").hide();
                    //$("#leftObj").show();
                    //$("#leftObj").mkselectSet("");
                }
                if (param.maintable) {
                    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetTableFieldList', { tableName: param.maintable }, function (data) {
                        if (data) {
                            if (param.type == "自定义") {
                                for (var _i in data) {
                                    var item = data[_i];
                                    for (var _j in customerFields.componts) {
                                        var info = customerFields.componts[_j];
                                        if (item.id.toLowerCase() != info.field.toLowerCase()) continue;
                                        item.id = info.id;
                                    }
                                }
                            }
                            for (var _i in data) {
                                var item = data[_i];
                                item.text += "(" + item.f_column + ")";
                            }
                            mainfields = data
                            $("#leftObj").mkselectRefresh({ data:mainfields });
                        }
                    });
                }
            }
        },
        initData: function () {
            setTimeout(function () {
                if (!!selectedRow) {
                    $('#form').mkSetFormData(selectedRow);
                    var expres = JSON.parse(selectedRow.Expression);
                    if (expres.iscustom) {
                        $("#customControlId").show();
                        $("#customControlId").val(expres.customControlId);
                    }
                    else {
                        $("#customControlId").hide();
                    }
                    $("#leftObj").mkselectSet(expres.leftObj);
                    $("#operators").mkselectSet(expres.operators);

                    $("#rightObjSqlInfoName").val(expres.rightObjSqlInfo.name);
                    $("#rightObjSqlInfoSql").val(expres.rightObjSqlInfo.sql);
                }
            }, 500);
        }
    };
    // 保存数据 
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData();
        var Expression = {};
        Expression.leftObj = $("#leftObj").mkselectGet();
        Expression.iscustom = iscustom;
        Expression.customControlId = $("#customControlId").val();
        Expression.operators = $("#operators").mkselectGet();
        Expression.rightObjSqlInfo = {
            name: $("#rightObjSqlInfoName").val(),
            sql: $("#rightObjSqlInfoSql").val()
        };
        postData.Expression = JSON.stringify(Expression);

        $.mkSaveForm(top.$.rootUrl + '/SystemModule/BaseConfirm/SaveForm?keyValue=' + keyValue+ '&type=' + type, postData, function (res) {
            // 保存成功后才回调 
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
} 
