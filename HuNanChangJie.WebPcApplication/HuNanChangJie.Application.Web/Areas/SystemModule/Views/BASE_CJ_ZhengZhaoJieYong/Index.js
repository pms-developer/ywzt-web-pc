﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-11-19 16:03
 * 描  述：证书借用
 */
var refreshGirdData;
var formId = request("formId");
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            //$('#datesearch').mkdate({
            //    dfdata: [
            //        { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
            //        { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
            //        { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
            //        { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
            //    ],
            //    // 月
            //    mShow: false,
            //    premShow: false,
            //    // 季度
            //    jShow: false,
            //    prejShow: false,
            //    // 年
            //    ysShow: false,
            //    yxShow: false,
            //    preyShow: false,
            //    yShow: false,
            //    // 默认
            //    dfvalue: false,
            //    selectfn: function (begin, end) {
            //        startTime = begin;
            //        endTime = end;
            //        page.search();
            //    }
            //});
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            //$('#ShenQinRen').mkUserSelect(0);
            //$('#Project_ID').mkDataSourceSelect({ code: 'BASE_XMLB',value: 'project_id',text: 'projectname' });
            //$('#ShenQinBuMeng').mkDepartmentSelect();
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
             
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增证照借用',
                    url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

           

            $("#revert").on("click", function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: "selectZhengZhao",
                        width: 800,
                        height: 400,
                        title: "选择证照",
                        url: top.$.rootUrl + "/SystemModule/BASE_CJ_ZhengZhao/Dialog3?id=" + keyValue,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var temp = { ids: [] };
                                    $.each(data, function (i, e) {
                                        temp.ids.push(e.ID)
                                    })

                                    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/GuiHuan', { keyValue: keyValue, ids: JSON.stringify(temp) }, function (data) {
                                        if (data.code == 200)
                                            refreshGirdData();
                                        else
                                            top.Changjie.alert.error(data.info);
                                    });
                                }
                            });
                        }
                    });
                }
                else {
                    top.Changjie.alert.info("请选择要归还证照的申请单！");
                }
            })
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/GetPageList',
                headData: [
                    { label: "借用编号", name: "BianHao", width: 200, align: "center"},
                    //{ label: "申请部门", name: "ShenQinBuMeng", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('department', {
                    //             key: value,
                    //             callback: function (_data) {
                    //                 callback(_data.name);
                    //             }
                    //         });
                    //    }},
                   
                    //{ label: "项目名称", name: "Project_ID", width: 400, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('sourceData', {
                    //             code:  'BASE_XMLB',
                    //             key: value,
                    //             keyId: 'project_id',
                    //             callback: function (_data) {
                    //                 callback(_data['projectname']);
                    //             }
                    //         });
                    //    }},
                    {
                        label: "借用日期", name: "JieYongRiQi", width: 100, align: "center", formatter: function (cellvalue, row) {
                            return Changjie.formatDate(cellvalue, "yyyy-MM-dd");
                        } },
                    {
                        label: "预计归还日期", name: "GuiHuanRiQi", width: 100, align: "center", formatter: function (cellvalue, row) {
                            return Changjie.formatDate(cellvalue, "yyyy-MM-dd");
                        }
                    },
                    { label: "借用证照数量", name: "ZhengZhaoCount", width: 150, align: "center" },
                    { label: "归还证照数量", name: "ZhengZhaoBackCount", width: 150, align: "center" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },{
                        label: "申请人", name: "ShenQinRen", width: 90, align: "center"
                    }, {
                        label: "备注", name: "Remark", width: 200, align: "left"
                    }
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            //if (startTime)
            //    param.StartTime = startTime;
            //if (endTime)
            //param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
// 删除
var deleteMsg = function () {

    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: '编辑证照借用',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};