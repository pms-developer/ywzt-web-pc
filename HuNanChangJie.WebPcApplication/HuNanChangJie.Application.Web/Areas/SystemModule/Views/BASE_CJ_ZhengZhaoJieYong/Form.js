﻿/* *  
 * 创建人：超级管理员
 * 日  期：2019-11-19 16:03
 * 描  述：证书借用
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type = "add";
var tables = [];
var subGrid = [];
var mainTable = "Base_CJ_ZhenZhaoJieYong";
var viewState = request('viewState');
var processCommitUrl = top.$.rootUrl + '/SystemModule/Base_CJ_ZhengZhaoJieYong/SaveForm';
var projectId = request('projectId');

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Base_CJ_ZhenZhaoJieYongMingXi', "gridId": 'Base_CJ_ZhenZhaoJieYongMingXi' });
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
        
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0002' }, function (data) {
                if (!$('#BianHao').val()) {
                    $('#BianHao').val(data);
                }
            });
            //$('#ShenQinBuMeng').mkselect({
            //    type: 'tree',
            //    allowSearch: true,
            //    url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
            //    param: {} 
            //});
            //$('#ShenQinRen').mkformselect({
            //    layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
            //    layerUrlW: 400,
            //    layerUrlH: 300,
            //    dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            //});
            $('#Project_ID').mkDataSourceSelect({ code: 'BASE_XMLB',value: 'project_id',text: 'projectname' });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Base_CJ_ZhenZhaoJieYongMingXi').jfGrid({
                headData: [
                    { label: '证件名称', name: 'zzmc_name', width: 200 },
                    { label: '证件号', name: 'zz_bianhao', width: 200 },
                    { label: '持证人', name: 'zz_xinming', width: 100 },
                    { label: '身份证号', name: 'zz_shenfengzheng', width: 100 },
                    {
                        label: '行备注', name: 'Remark', width: 160, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '借用证书列表'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId:"ID",
                bindTable:"Base_CJ_ZhenZhaoJieYongMingXi",
                height: 300,
                isEdit: true, toolbarposition: "top", showchoose: true, showadd: false,
                onChooseEvent: function () {

                    var projectid = $('#Project_ID').mkselectGet();
                    if (projectid) {
                        Changjie.layerForm({
                            id: "selectZhengZhao",
                            width: 800,
                            height: 455,
                            title: "选择证照",
                            url: top.$.rootUrl + "/SystemModule/BASE_CJ_ZhengZhao/Dialog2?projectid=" + projectid,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.fk_ZhenZhaoId = data[i].ID;
                                            row.zzmc_name = data[i].zzmc_name;
                                            row.zz_bianhao = data[i].zz_bianhao;
                                            row.zz_xinming = data[i].zz_xinming;
                                            row.zz_shenfengzheng = data[i].zz_shenfengzheng;
                                            row.fk_JieYongId = mainId;
                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }

                                        $("#Base_CJ_ZhenZhaoJieYongMingXi").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.info("请选择关联项目");
                    }
                }
            });

            //$("#addZhengZhao").on("click", function () {
            //    Changjie.layerForm({
            //        id: "selectZhengZhao",
            //        width: 800,
            //        height: 400,
            //        title: "选择证照",
            //        url: top.$.rootUrl + "/SystemModule/BASE_CJ_ZhengZhao/Dialog2",
            //        callBack: function (id) {
            //            return top[id].acceptClick(function (data) {
            //                if (!!data) {
            //                    var rows = [];
            //                    for (var i = 0; i < data.length; i++) {
            //                        var row = {};
            //                        row.fk_ZhenZhaoId = data[i].ID;
            //                        row.zzmc_name = data[i].zzmc_name;
            //                        row.zz_bianhao = data[i].zz_bianhao;
            //                        row.zz_xinming = data[i].zz_xinming;
            //                        row.zz_shenfengzheng = data[i].zz_shenfengzheng;
            //                        row.fk_JieYongId = mainId;
            //                        row.rowState = 1;
            //                        row.ID = Changjie.newGuid();
            //                        row.EditType = 1;
            //                        rows.push(row);
            //                    }
            //                    console.log(rows)
            //                    $("#Base_CJ_ZhenZhaoJieYongMingXi").jfGridSet("addRows", rows);
            //                }
            //            });
            //        }
            //    });
            //});
            //$("#deleteZhengZhao").on("click", function () {
            //    var rowIndex = $("#Base_CJ_ZhenZhaoJieYongMingXi").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Base_CJ_ZhenZhaoJieYongMingXi").jfGridSet("removeRow");
            //    }
            //});

            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
                state:"extend",
               isShowAttachment:true,
                isShowWorkflow: true
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoJieYong/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {

        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }

    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Base_CJ_ZhenZhaoJieYong"]').mkGetFormData());
    postData.strbase_CJ_ZhenZhaoJieYongMingXiList = JSON.stringify($('#Base_CJ_ZhenZhaoJieYongMingXi').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}




