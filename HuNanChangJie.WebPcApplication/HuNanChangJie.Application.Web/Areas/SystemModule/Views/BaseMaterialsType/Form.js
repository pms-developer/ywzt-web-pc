﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-05-18 14:43
 * 描  述：材料分类
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="Base_MaterialsType";
var processCommitUrl=top.$.rootUrl + '/SystemModule/BaseMaterialsType/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var isRename = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $("#ParentId").mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/SystemModule/BaseMaterialsType/GetTreeList',
            });
            $('#BuyerId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#IsEnable').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $("#Code").on("input propertychange", function () {
                Changjie.httpAsyncGet(top.$.rootUrl + "/SystemModule/BaseMaterialsType/CheckRename?value=" + $(this).val() + "&type=code", function (res) {
                    if (res.data) {
                        isRename = true;
                        Changjie.alert.warning("编码不能重复");
                    }
                    else {
                        isRename = false;
                    }
                });
            });
            $("#Name").on("input propertychange", function () {
                var data = $("#ParentId").mkselectGetEx();
                var fullName = data.FullName + "/" + $(this).val();
                $("#FullName").val(fullName);

                Changjie.httpAsyncGet(top.$.rootUrl + "/SystemModule/BaseMaterialsType/CheckRename?value=" + $(this).val() +"&type=name", function (res) {
                    if (res.data) {
                        isRename = true;
                        Changjie.alert.warning("名称不能重复");
                    }
                    else {
                        isRename = false;
                    }
                });
            });
            $("#ParentId").on("change", function () {
                var data = $("#ParentId").mkselectGetEx();
                var fullName = data.FullName + "/" + $("#Name").val();
                $("#FullName").val(fullName);
            })
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/BaseMaterialsType/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        if (isRename) {
            Changjie.alert.warning("编码或名称不能重复");
            return false;
        }
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/BaseMaterialsType/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
     return postData;
}
