﻿/*
 * 日 期：2017.04.17
 * 描 述：Excel导入配置	
 */
var refreshGirdData; // 更新数据
var selectedRow;
var bootstrap = function ($, Changjie) {
    "use strict";
    var moduleId = "";
    var page = {
        init: function () {
            page.initGrid();
            page.bind();
        },
        bind: function () {
            $('#module_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/Module/GetModuleTree',
                nodeClick: function (item) {
                    if (item.hasChildren) {
                        moduleId = '';
                    }
                    else {
                        moduleId = item.id;
                        page.search();
                    }
                    $('#titleinfo').text(item.text);
                }
            });

            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                if (!moduleId) {
                    Changjie.alert.warning('请选择功能！');
                    return false;
                }
                selectedRow = null;
                //Changjie.layerForm({
                //    id: 'Form',
                //    title: '添加快速导出',
                //    url: top.$.rootUrl + '/SystemModule/ExcelExport/Form?moduleId=' + moduleId,
                //    width: 500,
                //    height: 500,
                //    callBack: function (id) {
                //        return top[id].acceptClick(refreshGirdData);
                //    }
                //});
                Changjie.layerForm({
                    id: 'Form',
                    title: '选择导出类型',
                    url: top.$.rootUrl + '/SystemModule/ExcelExport/SelectExportTypeForm?moduleId=' + moduleId,
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
                
            });
            // 编辑
            $('#edit').on('click', function () {
                selectedRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                var ExportType = $('#gridtable').jfGridValue('ExportType');
                if (Changjie.checkrow(keyValue)) {
                    if (ExportType == 1) {
                        Changjie.layerForm({
                            id: 'Form',
                            title: '编辑快速导出',
                            url: top.$.rootUrl + '/SystemModule/ExcelExport/Form?moduleId=' + moduleId,
                            width: 500,
                            height: 500,
                            callBack: function (id) {
                                return top[id].acceptClick(refreshGirdData);
                            }
                        });
                    } else if (ExportType == 2) {
                        Changjie.layerForm({
                            id: 'Form',
                            title: '编辑自定义导出',
                            url: top.$.rootUrl + '/SystemModule/ExcelExport/FormNew?moduleId=' + moduleId + '&keyValue=' + keyValue,
                            width: 1100,
                            height: 700,
                            callBack: function (id) {
                                return top[id].acceptClick(refreshGirdData);
                            }
                        });
                    } else if (ExportType == 3) {
                        Changjie.layerForm({
                            id: 'Form',
                            title: '编辑按模板导出',
                            url: top.$.rootUrl + '/SystemModule/ExcelExport/TemplateExportForm?moduleId=' + moduleId,
                            width: 600,
                            height: 600,
                            callBack: function (id) {
                                return top[id].acceptClick(refreshGirdData);
                            }
                        });
                    }
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/SystemModule/ExcelExport/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 启用
            $('#enable').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认要【启用】！', function (res) {
                        if (res) {
                            Changjie.postForm(top.$.rootUrl + '/SystemModule/ExcelExport/UpdateState', { keyValue: keyValue, state: 1 }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 禁用
            $('#disable').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认要【停用】！', function (res) {
                        if (res) {
                            Changjie.postForm(top.$.rootUrl + '/SystemModule/ExcelExport/UpdateState', { keyValue: keyValue, state: 0 }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/ExcelExport/GetPageList',
                headData: [
                    { label: "导出名称", name: "F_Name", width: 160, align: "left" },
                    {
                         label: "绑定功能", name: "F_ModuleId", width: 160, align: "left",
                         formatter: function (cellvalue) {
                             var data = Changjie.clientdata.get(['modulesMap']);
                             return data[cellvalue].F_FullName;
                         }
                     },
                    { label: "绑定按钮", name: "F_BtnName", width: 160, align: "left" },
                    {
                        label: "导出类型", name: "ExportType", width: 160, align: "left",
                        formatter: function (cellvalue) {
                            if (cellvalue == 1)
                                return '按表格导出';
                            else if (cellvalue == 2)
                                return '自定义导出';
                            else if (cellvalue == 3)
                                return '按模板导出';
                            else
                                return '未知';
                        }
                    },
                    //{
                    //    label: "导出模板", name: "ExportTemplate", width: 160, align: "left",
                    //    formatter: function (value, row) {
                    //        if (value != "" && value != null) {
                    //            return "<a style='color:blue' href=" + value + ">下载</a>&nbsp;&nbsp;&nbsp;<a style='color:blue' onclick=\"uploadExportTemplate('" + row['F_Id'] + "')\">更新</a>&nbsp;&nbsp;&nbsp;<a style='color:blue' onclick=\"deleteExportTemplate('" + row['F_Id'] + "')\">删除</a>"
                    //        } else
                    //            return "<a style='color:blue' onclick=\"uploadExportTemplate('" + row['F_Id'] + "')\">上传</a>"
                    //    }
                    //},
                    {
                        label: "状态", name: "F_EnabledMark", width: 50, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue == 1) {
                                return '<span class=\"label label-success\" style=\"cursor: pointer;\">启用</span>';
                            } else if (cellvalue == 0) {
                                return '<span class=\"label label-default\" style=\"cursor: pointer;\">停用</span>';
                            }
                        }
                    },
                    {
                        label: '创建人', name: 'CreationName', width: 130, align: 'left'

                    },
                    {
                        label: '创建时间', name: 'CreationDate', width: 130, align: 'left',
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    }
                ],
                mainId: 'F_CustmerQueryId',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.moduleId = moduleId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };

    // 保存数据后回调刷新
    refreshGirdData = function () {
        page.search();
    }

    page.init();
}

var uploadExportTemplate = function (id) {
    if (top.Changjie.checkrow(id)) {
        top.Changjie.layerForm({
            id: 'SelectExportTypeForm',
            title: '上传导出模板',
            url: top.$.rootUrl + '/SystemModule/ExcelExport/SelectExportTypeForm?moduleId=' + id+'&exportType=3',
            width: 600,
            height: 400,
            maxmin: true,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
}

var deleteExportTemplate = function (id) {
    if (top.Changjie.checkrow(id)) {
        top.Changjie.layerConfirm('是否确认要删除导出模板！', function (res) {
            if (res) {
                top.Changjie.postForm(top.$.rootUrl + '/SystemModule/ExcelExport/DeleteCustomTemplate?keyValue=' + id, null, function () {
                    refreshGirdData();
                });
            }
        });
    }
}

