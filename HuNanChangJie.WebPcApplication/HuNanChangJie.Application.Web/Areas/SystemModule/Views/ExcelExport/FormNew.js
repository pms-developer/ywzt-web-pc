﻿/*
 * 日 期：2024.10.12
 * 描 述：导出配置
 */
var keyValue = request('keyValue');
var moduleId = request('moduleId');
var exportType = request('exportType');
var currentData;

var acceptClick;
var deleteTabSublist;

var add = 1;
var del = 2;
var rename = 4;
var resetDataType = 8;
var resetIsNull = 16;
var resetDeiscrption = 32;
var editColumns = [];

var originalData = [];
function setFieldState(row, index, columnName, type, newValue, oldValue) {

    if (!!!row.FieldState) {
        row.FieldState = 0;
    }
    if (row.FieldState == add) return;
    var item = { "columnName": columnName, "index": index, "FieldState": type };
    if (newValue != oldValue) {
        var isExist = false;
        if (editColumns.length <= 0) {
            editColumns.push(item);
            row.FieldState = row.FieldState + type;
        }
        else {
            for (var info in editColumns) {
                if (editColumns[info].columnName == columnName && editColumns[info].index == index) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                row.FieldState = row.FieldState + type;
                editColumns.push(item);
            }
        }
    }
    else {//编辑的值还原为原始值时去除修改状态(还原为编辑前的状态)
        var idx = -1;
        for (var i = 0; i < editColumns.length; i++) {
            if (editColumns[i].columnName == columnName && editColumns[i].index == index) {
                idx = i;
                break;
            }
        }
        if (idx >= 0) {
            row.FieldState = row.FieldState - type;
            editColumns.splice(idx, 1);//移除元素
        }
    }

}
var bootstrap = function ($, Changjie) {
    "use strict";
    var cols = [];
    var dbTable = '';
    var dbId = '';
    var btnName = '';
    var tab = 1;
    var colsPrimary = [];
    var colsSublist = [];
    var colsSublistSelect = [];
    var tableSetList = [];
    var TableName = "";


    function setDes(row, data) {
        var type = Number(data.F_RelationType);
        switch (type) {
            case 0://无关联
                row.F_Description = '无关联';
                break;
            case 1://GUID
                row.F_Description = '系统产生GUID';
                break;
            case 2://数据字典
                row.F_Description = '关联数据字典';
                break;
            case 3://数据表
                row.F_Description = '关联数据表';
                break;
            case 4://固定值
                row.F_Description = '固定数值/' + data.F_Value;
                break;
            case 5://操作人ID
                row.F_Description = '登录者ID';
                break;
            case 6://操作人名字
                row.F_Description = '登录者名字';
                break;
            case 7://操作时间
                row.F_Description = '导入时间';
                break;
            case 8://操作时间
                row.F_Description = '当前项目ID';
                break;
            case 9://操作时间
                row.F_Description = '当前主表信息ID';
                break;
            case 10://操作时间
                row.F_Description = '单据编码';
                break;
            case 11://上传文件url
                row.F_Description = '上传文件url';
                break;
            case 12://主表ID
                row.F_Description = '主表ID';
                break;
        }

    }

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');

            $('#F_ModuleBtnId').mkselect({
                url: top.$.rootUrl + '/SystemModule/Module/GetButtonListNoAuthorize',
                param: {
                    moduleId: moduleId
                },
                value: 'F_EnCode',
                text: 'F_FullName',
                select: function (item) {
                    if (!!item) {
                        btnName = item.F_FullName
                    }
                    else {
                        btnName = '';
                    }

                }
            });
           
            $('#filedtree').mktree({
                nodeCheck: function (item) {
                    if (item.checkstate == '1') {

                        if (tab == 1) {
                            var point = {
                                fieldCode: item.value,
                                fieldText: item.title,
                                dataType: '',
                                dataSourceCode: '',
                                dataSourceText: '',
                                FieldKey: '',
                                FieldValue: '',
                                dataItemCode: '',
                                dataItemText: '',
                                TableName: TableName
                            };
                            colsPrimary.push(point);
                        } else {
                            var point = {
                                fieldCode: item.value,
                                fieldText: item.title,
                                dataType: '',
                                dataSourceCode: '',
                                dataSourceText: '',
                                FieldKey: '',
                                FieldValue: '',
                                dataItemCode: '',
                                dataItemText: '',
                                TableName: TableName
                            };
                            colsSublist.push(point);
                        }

                    }
                    else {
                        if (tab == 1) {
                            for (var i = 0, l = colsPrimary.length; i < l; i++) {
                                if (colsPrimary[i].fieldCode == item.value) {
                                    colsPrimary.splice(i, 1);
                                    break;
                                }
                            }
                        }
                        else {
                            for (var i = 0, l = colsSublist.length; i < l; i++) {
                                if (colsSublist[i].fieldCode == item.value && colsSublist[i].TableName == TableName) {
                                    colsSublist.splice(i, 1);
                                    break;
                                }
                            }
                        }

                    }

                    if (tab == 1) {
                        $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                    } else {
                        var resultColsSublist = $.grep(colsSublist, function (item) {
                            return item.TableName == TableName;
                        });

                        $('#gridtableSublist').jfGridSet('refreshdata', resultColsSublist);
                        //console.log(resultColsSublist);
                        //$('#sublistQueryField').mkselectRefresh({ data: resultColsSublist });
                    }

                }
            });
            $('#dbtree').mktree({
                url: top.$.rootUrl + '/SystemModule/DatabaseTable/GetTreeList',
                nodeClick: function (item) {
                    if (!item.hasChildren) {
                        dbId = item.value;
                        TableName = item.text;
                        var resultLast = $.grep(tableSetList, function (item) {
                            return item.SortCode == tab;
                        });
                        if (resultLast.length == 0) {
                            $('#form_tabs ul').append('<li><a data-value="tabSublist" id="tab' + (tab + 1) + '">子表</a></li>');
                        }

                        tableSetList = $.grep(tableSetList, function (item) {
                            return item.SortCode !== tab;
                        });
                        if (tab == 1) {
                            $("#tab" + tab).html("主表：" + TableName);
                            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: item.value, tableName: TableName }, function (res) {
                                colsPrimary.length = 0;
                                $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                                $('#filedtree').mktreeSet('refresh', { data: res });
                                $('#filedtree').mktreeSet('allCheck');
                                $('#primaryQueryField').mkselectRefresh({
                                    data: res
                                });
                            });

                            var point = {
                                ImportId: keyValue,
                                DataReadMode:0,// $('#DataReadMode').mkselectGet(),
                                StartCell: $('#StartCell').val(),
                                EndCell: 0,//$('#EndCell').val(),
                                AttributesNumber: $('#AttributesNumber').val(),
                                StartRow: 0,
                                EndRow: 0,
                                TableName: TableName,
                                IsPrimary: true,
                                SortCode: tab,
                                QueryField: ''//$('#primaryQueryField').mkselectGet()
                            };
                            tableSetList.push(point);

                        } else {
                            $("#tab" + tab).html("子表：" + TableName + " <a style='color:blue' onclick='deleteTabSublist()'>删除</a>");
                            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: item.value, tableName: TableName }, function (res) {
                                //colsSublist.length = 0;
                                colsSublist = $.grep(colsSublist, function (itemColsSublist) {
                                    return itemColsSublist.TableName !== TableName;
                                });
                                colsSublistSelect.length = 0;
                                $('#gridtableSublist').jfGridSet('refreshdata', colsSublistSelect);
                                $('#filedtree').mktreeSet('refresh', { data: res });
                                $('#filedtree').mktreeSet('allCheck');
                                $('#sublistQueryField').mkselectRefresh({ data: res });
                            });

                            var point = {
                                ImportId: keyValue,
                                DataReadMode: 0,
                                StartCell: 0,
                                EndCell: 0,
                                AttributesNumber: 0,
                                StartRow: $("#StartRow").val(),
                                EndRow:0, //$("#EndRow").val(),
                                TableName: item.text,
                                IsPrimary: false,
                                SortCode: tab,
                                QueryField: ''//$("#sublistQueryField").mkselectGet()
                            };
                            tableSetList.push(point);
                        }

                    }
                    else {
                        dbTable = '';
                        dbId = '';
                        $('#filedtree').mktreeSet('refresh', { data: [] });
                    }
                }
            });


            $('#gridtablePrimary').jfGrid({
                headData: [
                    { label: "字段", name: "fieldCode", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "fieldText", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtablePrimary').jfGridSet('moveUp', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtablePrimary').jfGridSet('moveDown', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-success\" style=\"cursor: pointer;\">下移</span>';
                        }
                    },
                ]
            });
            $('#gridtableSublist').jfGrid({
                headData: [
                    { label: "字段", name: "fieldCode", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "fieldText", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtableSublist').jfGridSet('moveUp', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtableSublist').jfGridSet('moveDown', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-success\" style=\"cursor: pointer;\">下移</span>';
                        }
                    },
                ]
            });

            $('#sublistQueryField').mkselect({
                value: 'value',
                text: 'value',
                title: 'value',
                maxHeight: 99,
                placeholder: false,
                select: function (item) {
                }
            });

            $('#primaryQueryField').mkselect({
                value: 'value',
                text: 'value',
                title: 'value',
                maxHeight: 99,
                placeholder: false,
                select: function (item) {
                }
            });

            $('#form_tabs').delegate('li', 'click', function () {
                console.log("上一个tab:" + tab);
                var resultLast = $.grep(tableSetList, function (item) {
                    return item.SortCode == tab;
                });
                if (resultLast.length > 0) {
                    tableSetList = $.map(tableSetList, function (item) {
                        if (item.SortCode === tab) {
                            if (tab == 1) {
                                item.StartCell = $("#StartCell").val();
                                item.EndCell = 0; //$("#EndCell").val();
                                item.DataReadMode = 0; //$("#DataReadMode").mkselectGet();
                                item.AttributesNumber = $("#AttributesNumber").val();
                                item.QueryField = $("#primaryQueryField").mkselectGet();
                            } else {
                                item.StartRow = $("#StartRow").val();
                                item.EndRow =0, //$("#EndRow").val();
                                item.QueryField = $("#sublistQueryField").mkselectGet();
                            }
                        }
                        return item;
                    });
                }


                tab = $(this).index() + 1;

                console.log("当前tab:" + tab);
                console.log("当前tableSetList:" + tableSetList);

                var result = $.grep(tableSetList, function (item) {
                    return item.SortCode == tab;
                });

                if (result.length > 0) {
                    TableName = result[0].TableName;
                    console.log("TableName:" + TableName);
                    colsSublistSelect = $.grep(colsSublist, function (item) {
                        return item.TableName == TableName;
                    });
                    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: TableName }, function (res) {
                       
                        var map = {};
                        if (tab == 1) {
                            $.each(colsPrimary, function (id, item) {
                                map[item.fieldCode] = "1";
                                setDes(item, item);
                            });
                            $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                            $('#primaryQueryField').mkselectRefresh({
                                data: res
                            });
                        } else {
                            $.each(colsSublistSelect, function (id, item) {
                                map[item.fieldCode] = "1";
                                setDes(item, item);
                            });
                            $('#gridtableSublist').jfGridSet('refreshdata', colsSublistSelect);
                            $("#StartRow").val(result[0].StartRow);
                            //$("#EndRow").val(result[0].EndRow);
                            console.log(colsSublistSelect);
                            $('#sublistQueryField').mkselectRefresh({ data: res });
                           
                            if (result[0].QueryField.length > 0) {
                                $('#sublistQueryField').mkselectSet(result[0].QueryField);
                            }
                        }
                        $.each(res, function (id, item) {
                            if (!!map[item.value]) {
                                item.checkstate = '1';
                            }
                        });
                        $('#filedtree').mktreeSet('refresh', { data: res });
                    });
                } else {
                    TableName = "";
                    if (tab == 1) {
                        colsPrimary.length = 0;
                        $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                        $('#primaryQueryField').mkselectRefresh({
                            data: []
                        });
                    } else {
                        colsSublistSelect.length = 0;
                        $('#gridtableSublist').jfGridSet('refreshdata', colsSublistSelect);
                        console.log(colsSublistSelect);
                        $('#sublistQueryField').mkselectRefresh({ data: [] });
                    }
                    $('#filedtree').mktreeSet('refresh', { data: [] });
                }
            });
        },
        initData: function () {
            $('#F_ModuleId').val(moduleId);
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/ExcelExport/GetFormDataNew?keyValue=' + keyValue, function (data) {//
                    $('#F_ModuleBtnId').mkselectRefresh({
                        param: {
                            moduleId: data.entity.F_ModuleId
                        }
                    });
                    $('.mk-form-wrap').mkSetFormData(data.entity);
                    //dbTable = data.entity.F_DbTable;
                    //dbId = data.entity.F_DbId;
                    dbId = 'b3aaa000-1223-40d2-8ae2-28f980bc8f9c';

                    colsPrimary = data.primaryList;
                    colsSublist = data.sublistList;
                    tableSetList = data.tableSetList;
                    TableName = data.primaryList[0].TableName;

                    $("#tab1").html("主表：" + TableName);
                    var tabIndex = 2;
                    var primaryQueryField = "";
                    $.each(tableSetList, function (id, item) {
                        if (item.TableName != TableName) {
                            $("#tab" + tabIndex).html("子表：" + item.TableName + " <a style='color:blue' onclick='deleteTabSublist()'>删除</a>");
                            tabIndex++;
                        } else {
                            //$("#DataReadMode").mkselectSet(item.DataReadMode);
                            $("#StartCell").val(item.StartCell);
                            //$("#EndCell").val(item.EndCell);
                            $("#AttributesNumber").val(item.AttributesNumber);
                            $("#primaryQueryField").mkselectSet(item.QueryField);
                            primaryQueryField = item.QueryField;
                        }
                        $('#form_tabs ul').append('<li><a data-value="tabSublist" id="tab' + tabIndex + '">子表</a></li>');
                    });
                   

                    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: TableName }, function (res) {
                        console.log("init1");
                        var map = {};
                        $.each(colsPrimary, function (id, item) {
                            map[item.fieldCode] = "1";
                            setDes(item, item);
                        });
                        $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                        $.each(res, function (id, item) {
                            if (!!map[item.value]) {
                                item.checkstate = '1';
                            }
                        });
                        $('#filedtree').mktreeSet('refresh', { data: res });
                        $('#primaryQueryField').mkselectRefresh({
                            data: res
                        }).mkselectSet(primaryQueryField);
                    });

                });
            }
        }
    };

    acceptClick = function (callBack,name) {
        /* if (!$('.mk-form-layout-header').mkValidform()) {*/
        if (!$('.mk-form-wrap').mkValidform()) {
            return false;
        }

        if (colsPrimary.length == 0) {
            Changjie.alert.error('请添加设置字段');
            return false;
        }


        var resultLast = $.grep(tableSetList, function (item) {
            return item.SortCode == tab;
        });
        if (resultLast.length > 0) {
            tableSetList = $.map(tableSetList, function (item) {
                if (item.SortCode === tab) {
                    if (tab == 1) {
                        item.StartCell = $("#StartCell").val();
                        item.EndCell = 0; //$("#EndCell").val();
                        item.DataReadMode = 0; //$("#DataReadMode").mkselectGet();
                        item.AttributesNumber = $("#AttributesNumber").val();
                        item.QueryField = $("#primaryQueryField").mkselectGet();
                    } else {
                        item.StartRow = $("#StartRow").val();
                        //item.EndRow = $("#EndRow").val();
                        item.QueryField = $("#sublistQueryField").mkselectGet();
                    }
                }
                return item;
            });
        }

        /*var formData = $('.mk-form-layout-header').mkGetFormData(keyValue);*/
        var formData = $('.mk-form-wrap').mkGetFormData(keyValue);
        //formData.F_DbId = dbId;
        //formData.F_DbTable = dbTable;
        formData.F_BtnName = btnName;

        $.each(colsPrimary, function (id, item) {
            if (item.IsNull == "1") {
                item.IsNull = true;
            } else {
                item.IsNull = false;
            }
        });

        $.each(colsSublist, function (id, item) {
            if (item.IsNull == "1") {
                item.IsNull = true;
            } else {
                item.IsNull = false;
            }
        });

        var postData = {
            keyValue: keyValue,
            strEntity: JSON.stringify(formData),
            //strList: JSON.stringify(cols),
            strPrimaryList: JSON.stringify(colsPrimary),
            strSublistList: JSON.stringify(colsSublist),
            strTableSetList: JSON.stringify(tableSetList),
        };
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/ExcelExport/SaveFormNew', postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                Changjie.layerClose(name);
                callBack();
            }
        });
    }
    page.init();


    deleteTabSublist = function () {
        Changjie.layerConfirm('是否确认要删除子表！', function (res) {
            if (res) {
                tableSetList = $.grep(tableSetList, function (item) {
                    return item.SortCode !== tab;
                });

                $.each(tableSetList, function (index, item) {
                    item.SortCode = index + 1;
                });

                colsSublist = $.grep(colsSublist, function (item) {
                    return item.TableName !== TableName;
                });

                colsSublistSelect.length = 0;
                TableName = "";
                $('#form_tabs ul li').eq(tab - 1).remove();
                $('#form_tabs ul li').each(function (index, element) {
                    $(this).find('a').attr('id', 'tab' + (index+1));
                });
                tab = 0;
                $('#tab1').click();
            }
        });

    }
}
