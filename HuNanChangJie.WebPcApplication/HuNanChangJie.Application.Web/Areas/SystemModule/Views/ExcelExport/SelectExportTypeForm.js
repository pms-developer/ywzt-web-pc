﻿var acceptClick;
var moduleId = request('moduleId');
var exportType1 = request('exportType');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $("#ExportType").mkRadioCheckbox({
                type: 'radio',
                code: 'ExportType',
                default: 0
            })

            //$("#ExportTemplateShow").hide();

            //if (exportType1 != null) {
            //    $('input[name="ExportType"][value=\"' + exportType1 + '\"]').prop('checked', true);
            //    if (exportType1 == "3") {
            //        $("#ExportTemplateShow").show();
            //    } else {
            //        $("#ExportTemplateShow").hide();
            //    }
            //}
            //$("#ExportType").on('click', function () {
            //    var exportType = $('body').mkGetFormData().ExportType;
            //    if (exportType == "3") {
            //        $("#ExportTemplateShow").show();
            //    } else {
            //        $("#ExportTemplateShow").hide();
            //    }
            //});

           
        }
    };
    acceptClick = function (callBack) {
        var exportType = $('body').mkGetFormData().ExportType;
        if (exportType == 1) {
            Changjie.layerForm({
                id: 'Form1',
                title: '添加快速导出',
                url: top.$.rootUrl + '/SystemModule/ExcelExport/Form?moduleId=' + moduleId + '&exportType=' + exportType,
                width: 500,
                height: 500,
                callBack: function (id) {
                    //return top[id].acceptClick(refreshGirdData);
                    return top[id].acceptClick(callBack, window.name);
                }
            });
        } else if (exportType == 2) {
            Changjie.layerForm({
                id: 'Form1',
                title: '添加自定义导出',
                url: top.$.rootUrl + '/SystemModule/ExcelExport/FormNew?moduleId=' + moduleId + '&exportType=' + exportType,
                width: 1100,
                height: 700,
                callBack: function (id) {
                    //return top[id].acceptClick(refreshGirdData);
                    return top[id].acceptClick(callBack, window.name);
                }
            });
        } else if (exportType == 3) {
            //var e = document.getElementById("uploadFile").files[0];
            //if (!!e) {
            //    Changjie.loading(true, '正在上传...');
            //    const form = new FormData();
            //    form.append("file", e)
            //    form.append("keyValue", moduleId)
            //    Changjie.httpPostFile(top.$.rootUrl + '/SystemModule/ExcelExport/UpLoadFile', form, function (data) {
            //        Changjie.loading(false);
            //        let obj = JSON.parse(data)
            //        if (obj.code == 200) {
            //            // 保存成功后才回调
            //            if (!!callBack) {
            //                callBack();
            //            }
            //            Changjie.layerClose(window.name);
            //        } else {
            //            top.Changjie.alert.error(obj.info);
            //        }
            //    });
            //}
            Changjie.layerForm({
                id: 'Form1',
                title: '添加模板导出',
                url: top.$.rootUrl + '/SystemModule/ExcelExport/TemplateExportForm?moduleId=' + moduleId + '&exportType=' + exportType,
                width: 600,
                height: 600,
                callBack: function (id) {
                    //return top[id].acceptClick(refreshGirdData);
                    return top[id].acceptClick(callBack, window.name);
                }
            });
        }
    };
    page.init();
};