﻿/*
 * 日 期：2017.04.11
 * 描 述：区域管理	
 */
var keyValue = '';
var acceptClick;
var moduleId = request('moduleId');
var currentColRow = null;

var bootstrap = function ($, Changjie) {
    "use strict";
    var selectedRow = Changjie.frameTab.currentIframe().selectedRow;
    var btnName = '';
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        getColumnCode: function () {
            var columnCode = "";
            //获取列中所有字段code
            var sRow = $('#gridtable').jfGridGet('rowdatas');
            if (sRow != null && sRow != "" && sRow != undefined) {
                $.each(sRow, function (id, item) {
                    if (item) {
                        columnCode += item.fieldCode;
                        if (sRow.length - 1 != id) {
                            columnCode += ","
                        }
                    }
                });
            }
            return columnCode;
        },
        bind: function () {
            $('#F_ModuleBtnId').mkselect({
                url: top.$.rootUrl + '/SystemModule/Module/GetButtonListNoAuthorize',
                param: {
                    moduleId: moduleId
                },
                value: 'F_EnCode',
                text: 'F_FullName',
                select: function (item) {
                    if (!!item) {
                        btnName = item.F_FullName
                    }
                    else {
                        btnName = '';
                    }

                },
                maxHeight: 170
            });

            //添加
            $('#add_format').on('click', function () { 
                var sColumnCode = page.getColumnCode();
                currentColRow = null;
                Changjie.layerForm({
                    id: 'FormatForm',
                    title: '添加',
                    url: top.$.rootUrl + '/SystemModule/ExcelExport/FormatForm?isAdd=1&ModuleId=' + moduleId + "&ColumnCode=" + sColumnCode,
                    width: 500,
                    height: 310,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            $('#gridtable').jfGridSet('addRow', data);
                            $("#F_FieldSettings").val(JSON.stringify($('#gridtable').jfGridGet('rowdatas')));
                        });
                    }
                });
            });
            //修改
            $('#edit_format').on('click', function () {
                currentColRow = $('#gridtable').jfGridGet('rowdata');
                var _id = currentColRow ? currentColRow.fieldCode : '';
                if (Changjie.checkrow(_id)) {
                    Changjie.layerForm({
                        id: 'FormatForm',
                        title: '修改',
                        url: top.$.rootUrl + '/SystemModule/ExcelExport/FormatForm?isAdd=2&ModuleId=' + moduleId,
                        width: 500,
                        height: 310,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                $.extend(currentColRow, data);
                                $('#gridtable').jfGridSet('updateRow');
                                $("#F_FieldSettings").val(JSON.stringify($('#gridtable').jfGridGet('rowdatas')));
                            });
                        }
                    });
                }

            });
            //移除列
            $('#delete_format').on('click', function () {
                currentColRow = null;
                var row = $('#gridtable').jfGridGet('rowdata');
                var _id = row ? row.fieldCode : '';
                if (Changjie.checkrow(_id)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res, index) {
                        if (res) {
                            $('#gridtable').jfGridSet('removeRow');
                            var sFieldSettings = $('#gridtable').jfGridGet('rowdatas');
                            if (sFieldSettings.length > 0) {
                                $("#F_FieldSettings").val(JSON.stringify(sFieldSettings));
                            }
                            else {
                                $("#F_FieldSettings").val("");
                            }
                            top.layer.close(index); //再执行关闭  
                        }
                    });
                }
            });

            //添加表头
            $('#gridtable').jfGrid({
                headData: [
                    { label: "导出字段", name: "fieldText", align: "left" },
                    {
                        label: "数据类型", name: "dataType", align: "left",
                        formatter: function (cellvalue) {
                            if (cellvalue == "sjy") {
                                return '<span class=\"label label-info\" >数据源</span>';
                            } else if (cellvalue == "sjzd") {
                                return '<span class=\"label label-warning\" >数据字典</span>';
                            }
                        }
                    },
                    { label: "字典分类", name: "dataItemText", align: "left" },
                    { label: "数据来源", name: "dataSourceText", align: "left" },
                    { label: "关联主键", name: "FieldKey", align: "left" },
                    { label: "关联值字段", name: "FieldValue", align: "left" }
                  
                ],
                mainId: 'fieldCode'
            });
        },
        initData: function () {
            $('#F_ModuleId').val(moduleId);
            if (!!selectedRow) {
                $('#F_ModuleBtnId').mkselectRefresh({
                    param: {
                        moduleId: selectedRow.F_ModuleId
                    }
                });
                keyValue = selectedRow.F_Id;
                $('#form1').mkSetFormData(selectedRow);
                var formatdata = JSON.parse(selectedRow.F_FieldSettings);
                $('#gridtable').jfGridSet('refreshdata', formatdata);

            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form1').mkValidform()) {
            return false;
        } 
        var postData = $('#form1').mkGetFormData(keyValue);
        postData.F_BtnName = btnName;
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/ExcelExport/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}