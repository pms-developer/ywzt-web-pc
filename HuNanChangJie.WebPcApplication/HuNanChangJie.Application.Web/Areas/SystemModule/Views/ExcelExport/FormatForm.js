﻿
/*
 * 日 期：2017.04.18
 * 描 述：单据编号规则	
 */
var acceptClick;
var currentColRow = top.layer_Form.currentColRow;
var moduleId = request('ModuleId');
var sColumnCode = request('ColumnCode');
var iIsAdd = request('isAdd');

var classify_itemCode = "";

var bootstrap = function ($, Changjie) {
    "use strict";


    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () { 
            $('#dataSourceCode').mkDataSourceAllSelect();//绑数据来源下拉值 
            $('#dataItemCode').mkDataTypeSelect(); //绑数据字典下拉值
            $("#FieldKey").mkselect({});
            $("#FieldValue").mkselect({});
            if (iIsAdd == "2") {
                $("#fieldCode").attr({ "disabled": "disabled" });
            }
            $("#dataItem").hide();//首次加载隐藏数据字典
            //模块ID不为空，根据id查询列信息
            $.mkSetForm(top.$.rootUrl + '/SystemModule/Module/GetFormData?keyValue=' + moduleId, function (data) {
                var columnData = data.moduleColumns;
                if (sColumnCode != undefined && sColumnCode != "" && sColumnCode != null) {
                    sColumnCode = sColumnCode.split(",");
                    if (columnData.length > 0) {
                        //过滤已选数据 
                        for (var i = 0; i < sColumnCode.length; i++) {
                            for (var j = 0; j < columnData.length; j++) {
                                if (columnData[j].F_EnCode == sColumnCode[i]) {
                                    columnData.splice(j, 1);
                                }
                            }
                        }
                    }
                }
                var columnCode = currentColRow != "" && currentColRow != null ? currentColRow.fieldCode : "";
                //给【导出字段】绑定下拉取值
                $("#fieldCode").mkselect({
                    value: "F_EnCode",
                    text: "F_FullName",
                    data: columnData,
                    select: function (item) {
                        if (item != null && item != undefined) {
                            $("#fieldCode").val(item.F_EnCode);
                            $("#fieldText").val(item.F_FullName);
                        }
                        else {
                            $("#fieldText").val("");
                        }
                    }
                }).mkselectSet(columnCode);
                var columnName = currentColRow != "" && currentColRow != null ? currentColRow.fieldText : "";
                $("#fieldText").val(columnName);
            });
            //数据类型,设置固定值 
            $("#dataType").mkselect({
                value: 'value',
                text: 'text',
                data: [{ "value": "sjy", "text": "数据源" }, { "value": "sjzd", "text": "数据字典" }],
                select: function (item) {
                    var value = item.value;
                    if (value == "sjy") {
                        page.ShowDataSource();
                    }
                    else if (value == "sjzd") {
                        page.ShowDataItem();
                    }
                }
            }).mkselectSet("sjy");
            //数据字典
            $('#dataItemCode').on('change', function () {
                var selectData = $("#dataItemCode").mkselectGetEx();
                if (selectData) {
                    if (selectData.parentId == "-1" || selectData.parentId == "0") {
                        alert("请选择子级数据");
                        $("#dataItemCode").mkselectSet("");
                        $("#dataItemText").val("");
                    }
                    else {

                        $("#dataItemCode").val(selectData.value);
                        $("#dataItemText").val(selectData.text);
                    }
                }
                else {
                    $("#dataItemText").val("");
                }
            });
            //源数据值改变
            $("#dataSourceCode").on('change', function () {
                var selectData = $("#dataSourceCode").mkselectGetEx();
                var strSQL = "";
                if (selectData) {
                    $("#dataSourceCode").val(selectData.F_Code);
                    $("#dataSourceText").val(selectData.F_Name);
                    strSQL = selectData.F_Sql; //绑定主键和值字段      
                }
                else {
                    $("#dataSourceText").val("");
                }
                page.BinddataSourceField(strSQL);//绑定查询字段名称 
            });
        },
        ShowDataItem: function () {
            $("#dataItem").show();//显示数据字典
            $('#dataItemCode').attr({ 'isvalid': 'yes', "checkexpession": "NotNull" });

            $("#dataSource").hide();
            $('#dataSourceCode').removeAttr('isvalid checkexpession');
            $("#dataSourceCode").mkselectSet("");
            $("#dataSourceText").val("");

            $(".dataSourceField").hide();
            $('#FieldKey').removeAttr('isvalid checkexpession');
            $('#FieldValue').removeAttr('isvalid checkexpession');
            $("#FieldKey").mkselectSet("");
            $("#FieldValue").mkselectSet("");

        },
        ShowDataSource: function () {
            $("#dataSource").show();//显示源数据
            $('#dataSourceCode').attr({ 'isvalid': 'yes', "checkexpession": "NotNull" });
            $(".dataSourceField").show();
            $('#FieldKey').attr({ 'isvalid': 'yes', "checkexpession": "NotNull" });
            $('#FieldValue').attr({ 'isvalid': 'yes', "checkexpession": "NotNull" });

            $("#dataItem").hide();
            $('#dataItemCode').removeAttr('isvalid checkexpession');
            $("#dataItemCode").mkselectSet("");
            $("#dataItemText").val("");
        },
        BinddataSourceField: function (strSQL) {
            var newfield = [];
            if (strSQL != "" && strSQL != undefined && strSQL != null) {
                strSQL = strSQL.toLowerCase();
                var fieldInfo = strSQL.replace("select", "");  //去掉SQL内的select              
                var fromLen = fieldInfo.indexOf("from");  //获取from开始位置                               
                var fromToEnd = fieldInfo.substring(fromLen);//获取from后面的脚本               
                fieldInfo = fieldInfo.replace(fromToEnd, ""); //替换from后面的脚本内容               
                var arryfield = fieldInfo.split(','); //解析字段
                //处理字段名称
                if (arryfield != null) {
                    $.each(arryfield, function (index, fieldItem) {
                        var strFieldName = page.ParseSQLFields(fieldItem);
                        var selectItem = JSON.parse("{ \"value\": \"" + strFieldName + "\", \"text\":\"" + strFieldName + "\" }");
                        newfield.push(selectItem);
                    });
                }
            }
            //绑定主键和值字段的下拉取数 
            $('#FieldKey').mkselectRefresh({
                data: newfield,
                value: 'value',
                text: 'text',
                title: 'text'
            });
            $('#FieldValue').mkselectRefresh({
                data: newfield,
                value: 'value',
                text: 'text',
                title: 'text'
            });

        },
        //解析字段
        ParseSQLFields: function (fieldItem) { 
            var strFieldName = "";
            //字段的命名规则，1、表名.；2、字段 as 别名；3、字段 别名；4、字段名
            fieldItem = fieldItem.trim();
            var fiedByDot = page.ParseSQLFieldsByDot(fieldItem);
            var fiedByAs = page.ParseSQLFieldsByAs(fiedByDot);
            var fiedByEmpty = page.ParseSQLFieldsByEmpty(fiedByAs);
            strFieldName = fiedByEmpty; 
            return strFieldName.replace(/\s*/g, "");//去掉所有空格 
        },
        ParseSQLFieldsByEmpty: function (fieldItem) {            
            var strFieldName = "";
            if (fieldItem != "") {
                var emptyAlias = fieldItem.split(' ');//3、字段 别名
                var emptyAliasLen = emptyAlias.length;
                if (emptyAliasLen > 1) {
                    var emptyDotAlias = page.ParseSQLFieldsByDot(emptyAlias[emptyAliasLen - 1]);
                    var emptyAsAlias = page.ParseSQLFieldsByAs(emptyDotAlias);
                    strFieldName = emptyAsAlias;
                } else {
                    strFieldName = fieldItem;
                }
            }
            return strFieldName;
        },
        ParseSQLFieldsByAs: function (fieldItem) {
            var strFieldName = "";             
            if (fieldItem != "") {
                var asAlias = fieldItem.split('as');//2、字段 as 别名
                var asAliasLen = asAlias.length;
                if (asAliasLen > 1) {
                    var asDotAlias = page.ParseSQLFieldsByDot(asAlias[asAliasLen - 1]);
                    var asEmptyAlias = page.ParseSQLFieldsByEmpty(asDotAlias);
                    strFieldName = asEmptyAlias;
                } else {
                    strFieldName = fieldItem;
                }
            }
            return strFieldName;
        },
        ParseSQLFieldsByDot: function (fieldItem) {            
            var strFieldName = "";
            if (fieldItem != "") {
                var dotAlias = fieldItem.split('.');//1、表名.
                var dotAliasLen = dotAlias.length;
                if (dotAliasLen > 1) {
                    var dotAsAlias = page.ParseSQLFieldsByAs(dotAlias[dotAliasLen - 1]);
                    var dotEmptyAlias = page.ParseSQLFieldsByEmpty(dotAsAlias);
                    strFieldName = dotEmptyAlias;
                } else {
                    strFieldName = fieldItem;
                }
            }
            return strFieldName;
        },
        initData: function () {
            if (!!currentColRow) {
                $('#form').mkSetFormData(currentColRow);
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {        
        if (!$('#form').mkValidform()) {
            return false;
        }
        var data = $('#form').mkGetFormData();
        if (!!callBack) { callBack(data); }
        return true;
    };
    page.init();
}
