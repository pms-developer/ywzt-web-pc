﻿/*
 * 日 期：2017.04.11
 * 描 述：区域管理	
 */
var keyValue = '';
var acceptClick;
var moduleId = request('moduleId');
var currentColRow = null;

var bootstrap = function ($, Changjie) {
    "use strict";
    var selectedRow = Changjie.frameTab.currentIframe().selectedRow;
    var btnName = '';
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#F_ModuleBtnId').mkselect({
                url: top.$.rootUrl + '/SystemModule/Module/GetButtonListNoAuthorize',
                param: {
                    moduleId: moduleId
                },
                value: 'F_EnCode',
                text: 'F_FullName',
                select: function (item) {
                    if (!!item) {
                        btnName = item.F_FullName
                    }
                    else {
                        btnName = '';
                    }

                },
                maxHeight: 170
            });
        },
        initData: function () {
            //$('#F_ModuleId').val(moduleId);
            if (!!selectedRow) {
                $('#F_ModuleBtnId').mkselectRefresh({
                    param: {
                        moduleId: selectedRow.F_ModuleId
                    }
                });
                keyValue = selectedRow.F_Id;
                //$('#form1').mkSetFormData(selectedRow);
                //var formatdata = JSON.parse(selectedRow.F_FieldSettings);
                //$('#gridtable').jfGridSet('refreshdata', formatdata);
                $.mkSetForm(top.$.rootUrl + '/SystemModule/ExcelExport/GetEntity?keyValue=' + keyValue, function (data) {
                    //for (var id in data) {
                    //    if (!!data[id].length && data[id].length > 0) {
                    //        $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                    //        subGrid.push({ "tableName": id, "gridId": id });
                    //    }
                    //    else {
                    //        tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                    //    }
                    //}

                    $('#F_ModuleId').val(data.F_ModuleId);
                    $('#F_ModuleBtnId').mkselectSet(data.F_ModuleBtnId);
                    $('#F_Name').val(data.F_Name);
                    $('#F_GridId').val(data.F_GridId);
                    var strs = data.ExportTemplate.split("\\");
                    $('#ExportTemplate').attr('href', '/Areas/SystemModule/Views/ExcelExport/Template/' + strs[strs.length - 1]);
                    $('#ExportTemplateSQL').val(data.ExportTemplateSQL);
                });
            }
           
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form1').mkValidform()) {
            return false;
        }

        var e = document.getElementById("uploadFile").files[0];
        if (!!e) {
            Changjie.loading(true, '正在上传...');
            const form = new FormData();
            form.append("file", e)
            form.append("keyValue", moduleId)
            Changjie.httpPostFile(top.$.rootUrl + '/SystemModule/ExcelExport/UpLoadFile', form, function (data) {
                Changjie.loading(false);
                let obj = JSON.parse(data)
                if (obj.code == 200) {
                    var postData = $('#form1').mkGetFormData(keyValue);
                    postData.F_BtnName = btnName;
                    postData.ExportTemplate = obj.info;
                    postData.F_ModuleId = moduleId;
                    postData.ExportType = 3;
                    $.mkSaveForm(top.$.rootUrl + '/SystemModule/ExcelExport/SaveForm?keyValue=' + keyValue, postData, function (res) {
                        // 保存成功后才回调
                        if (!!callBack) {
                            callBack();
                        }
                    });
                } else {
                    top.Changjie.alert.error(obj.info);
                }
            });
        }

        
    };
    page.init();
}