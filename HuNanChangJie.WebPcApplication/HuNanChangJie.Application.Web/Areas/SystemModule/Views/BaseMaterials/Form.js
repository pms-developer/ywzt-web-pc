﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-05-19 11:20
 * 描  述：材料档案
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Base_Materials";
var processCommitUrl = top.$.rootUrl + '/SystemModule/BaseMaterials/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CL_Code' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            //系统管理-数据数据字典配置

            $('#TypeId').mkDataSourceSelect({ code: 'MaterialsType', value: 'id', text: 'title' });
            $('#BrandId').mkDataSourceSelect({ code: 'MaterialsBrand', value: 'id', text: 'cnname' });
            $('#Origin').mkDataItemSelect({ code: 'PlaceOfOrigin' });
            $('#UnitId').mkDataSourceSelect({ code: 'MaterialsUnit', value: 'id', text: 'name' });
            $('#WarrantyType').mkDataItemSelect({ code: 'DateType' });
            $('#State').mkRadioCheckbox({
                type: 'radio',
                code: 'MaterialStatus',
            });
            $('#IsProject').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
                default: '1'
            });

            $("#IsProject").on("change", function () {
                if ($("input[name='IsProject']:checked").val() == "true") {
                    $('#ProjectId').attr('disabled', false);
                    $('#ProjectId').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
                }
                else {
                    $('#ProjectId').attr('disabled', true);
                    $('#ProjectId').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
                }
            });

            //系统管理-数据源配置
            $('#ProjectId').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
            $('#SubjectCost').mkDataSourceSelect({ code: 'Base_CBKM', value: 'id', text: 'fullname' });

            $('#IsBei').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#IsChange').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#DeliveryCycleType').mkDataItemSelect({ code: 'DateType' });
            $('#ProcurementCycleType').mkDataItemSelect({ code: 'DateType' });
            $('#Buyer').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#IsOutTheFront').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/BaseMaterials/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") keyValue = mainId;
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/BaseMaterials/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
