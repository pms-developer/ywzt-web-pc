﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-05-19 11:20
 * 描  述：材料档案
 */
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var typeId = "";
    var typeName = "";

    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            var param = {};
            //$("#revocation").hide()

            //$('#dataTree').mktree({
            //    url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/GetTree?deep=0',
            //    nodeClick: function (item) {
            //        if (item._deep == 0)
            //            page.search();
            //        else
            //            page.search({ fk_zhengzhaomingcheng: item.value, deep: item._deep });
            //    }
            //});

            // 分类
            $('#dataTree').mktree({
                nodeClick: function (item) {
                    console.log(item);

                    typeId = item.id;
                    typeName = item.text;
                    param.TypeId = typeId;
                    param.TypeName = typeName;
                    page.search(param);
                }
            });

            //分类
            $('#dataTree').mktreeSet('refresh', {
                url: top.$.rootUrl + '/SystemModule/BaseMaterialsType/GetTreeList',
            });

            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 100, 950);

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增材料档案',
                    url: top.$.rootUrl + '/SystemModule/BaseMaterials/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/BaseMaterials/GetPageList',
                headData: [
                    { label: "材料编码", name: "Code", width: 140, align: "left"},
                    { label: "材料名称", name: "Name", width: 100, align: "left" },
                    { label: "材料类别", name: "TypeName", width: 100, align: "left" },
                    { label: "规格型号", name: "Model", width: 100, align: "left"},
                    { label: "品牌", name: "BrandName", width: 100, align: "left"},
                    { label: "产地", name: "Origin", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'PlaceOfOrigin',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    {
                        label: "计量单位", name: "UnitName", width: 100, align: "left",
                    },
                    { label: "项目", name: "ProjectId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'BASE_XMLB',
                                 key: value,
                                 keyId: 'project_id',
                                 callback: function (_data) {
                                     callback(_data['projectname']);
                                 }
                             });
                        }},
                    { label: "采购价", name: "Price", width: 100, align: "left"},
                    { label: "预算成本价", name: "BugetCost", width: 100, align: "left"},
                    { label: "人工费", name: "DirectLabor", width: 100, align: "left"},
                    { label: "机械费", name: "MachineryPrice", width: 100, align: "left"},
                    { label: "成本科目", name: "SubjectCost", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'SubjectCost',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.TypeId = typeId;
            param.TypeName = typeName;

            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/BaseMaterials/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '材料档案',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/SystemModule/BaseMaterials/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
