﻿/*
 * 日 期：2017.04.18
 * 描 述：零星采购
 */

var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");
var acceptClick;
var selectdata = [];
var bootstrap = function ($, Changjie) {
    "use strict";
    var typeId = "";
    var typeName = "";

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },

        bind: function () {
            var param = {};
            // 分类
            $('#type_tree').mktree({
                nodeClick: function (item) {
                    typeId = item.id;
                    typeName = item.text;
                    param.ProjectId = projectId;
                    param.TypeId = typeId;
                    param.TypeName = typeName;
                    page.search(param);

                }
            });

            $("#btn_Select").click(function () {
                var data = $("#gridtable").jfGridGet("rowdata");
                
                for (var i = 0; i < data.length; i++) {
                    var ishas = false;
                    $.each(selectdata, function (ii, ee) {
                        if (ee.ID == data[i].ID) {
                            ishas = true;
                            return false;
                        }
                    })

                    if (ishas) {
                        continue;
                    }

                    selectdata.push(data[i])
                }

                $("#gridtableselect").jfGridSet("refreshdata", selectdata);
            })

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/SystemModule/BaseMaterials/GetMaterialsList',
                headData: [
                    { label: "材料编码", name: "Code", width: 140, align: "left" },
                    { label: "材料名称", name: "Name", width: 100, align: "left" },
                    { label: "材料类别", name: "TypeName", width: 100, align: "left" },
                    { label: "规格型号", name: "Model", width: 100, align: "left" },
                    { label: "品牌", name: "BrandName", width: 100, align: "left" },
                    {
                        label: "产地", name: "Origin", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'PlaceOfOrigin',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    //{
                    //    label: "计量单位", name: "UnitName", width: 100, align: "left",
                    //},

                    {
                        label: '计量单位', name: 'UnitId', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    {
                        label: "项目", name: "ProjectId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },
                    { label: "采购价", name: "Price", width: 100, align: "left" },
                    { label: "预算成本价", name: "BugetCost", width: 100, align: "left" },
                    { label: "人工费", name: "DirectLabor", width: 100, align: "left" },
                    { label: "机械费", name: "MachineryPrice", width: 100, align: "left" },
                    {
                        label: "成本科目", name: "SubjectCost", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'SubjectCost',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                ],
                isPage: true,
                mainId: "ID",
                height: 360,
                height: 290,
                isMultiselect: true,
                param: { queryJson: JSON.stringify(param) }
            });
            $('#gridtableselect').jfGrid({
                data: selectdata,
                //url: top.$.rootUrl + '/SystemModule/BaseMaterials/GetMaterialsList',
                headData: [
                    { label: "材料编码", name: "Code", width: 140, align: "left" },
                    { label: "材料名称", name: "Name", width: 100, align: "left" },
                    { label: "材料类别", name: "TypeName", width: 100, align: "left" },
                    { label: "规格型号", name: "Model", width: 100, align: "left" },
                    { label: "品牌", name: "BrandName", width: 100, align: "left" },
                    {
                        label: "产地", name: "Origin", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'PlaceOfOrigin',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    //{
                    //    label: "计量单位", name: "UnitName", width: 100, align: "left",
                    //},
                    {
                        label: '计量单位', name: 'UnitId', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    {
                        label: "项目", name: "ProjectId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },
                    { label: "采购价", name: "Price", width: 100, align: "left" },
                    { label: "预算成本价", name: "BugetCost", width: 100, align: "left" },
                    { label: "人工费", name: "DirectLabor", width: 100, align: "left" },
                    { label: "机械费", name: "MachineryPrice", width: 100, align: "left" },
                    {
                        label: "成本科目", name: "SubjectCost", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'SubjectCost',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                ],
                mainId: "ID",
                height: 360,
                height: 290,
                isEdit: true, toolbarposition: "top", showadd: false,
                onMinusRow: function (row, rows, headData) {
                    selectdata = rows
                }
            });
            //分类
            $('#type_tree').mktreeSet('refresh', {
                url: top.$.rootUrl + '/SystemModule/BaseMaterialsType/GetTreeList',
            });

            // 滚动条
            $('#user_list_warp').mkscroll();
        },

        initData: function () {

            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                param.ProjectId = projectId;
                param.TypeId = typeId;
                param.TypeName = typeName;

                page.search(param);
            });
        },

        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    }

    // 保存数据
    acceptClick = function (callBack) {
        if (!!selectdata && Array.isArray(selectdata) && selectdata.length >0) {
            callBack(selectdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
            return false
        }
    };


    refreshGirdData = function () {
        page.search();
    };
    page.init();
};


