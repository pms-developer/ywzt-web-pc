﻿/*
 * 创建人：
 * 日 期：2017.04.17
 * 描 述：单据编码	
 */
var refreshGirdData; // 更新数据
var formId = request("formId");
var selectedRow;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGrid();
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                selectedRow = null;
                Changjie.layerForm({
                    id: 'Form',
                    title: '添加单据编码',
                    url: top.$.rootUrl + '/SystemModule/CodeRule/Form',
                    width: 700,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                var keyValue = $('#gridtable').AgGridValue('F_RuleId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'Form',
                        title: '编辑单据编码',
                        url: top.$.rootUrl + '/SystemModule/CodeRule/Form',
                        width: 700,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RuleId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/SystemModule/CodeRule/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/SystemModule/CodeRule/GetPageList',
                headData: [
                    {headerName: '对象编号',field: 'F_EnCode', width: 150},
                    {headerName: '对象名称',field: 'F_FullName', width: 200 },
                    {
                       headerName: '当前流水号',field: 'F_CurrentNumber', width: 150,
                        formatter: function (cellvalue) {
                            if (cellvalue) {
                                return cellvalue;
                            } else {
                                return "<span class=\"label label-danger\">未使用</span>";
                            }
                        }
                    },
                    {headerName: '创建用户',field: 'CreationName', width: 100 },
                    {
                       headerName: '创建时间',field: 'CreationDate', width: 130,
                       cellRenderer: function (cellvalue) {
                            return Changjie.formatDate(cellvalue.value, 'yyyy-MM-dd hh:mm');
                        }
                    },
                    { headerName: "说明", field: "F_Description", width: 200, align: "left" }

                ],
                mainId: 'F_RuleId',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            $('#gridtable').AgGridSet('reload', param);
        }
    };

    // 保存数据后回调刷新
    refreshGirdData = function () {
        page.search();
    }

    page.init();
}


