﻿/*
 * 日 期：2017.03.22
 * 描 述：数据字典管理	
 */
var refreshGird;
var bootstrap = function ($, Changjie) {
   $("#sjzd").on("input", function () {
        let $this= $(this).val()
       var add = $("#left_tree")["context"].links;
      // add.forEach(t => console.log(t))
        for (var i = 0; i < add.length; i++) {
            if (add[i].text.search($this) != -1)
                add[i].parentNode.classList.remove("hidden");
            else
                add[i].parentNode.classList.add("hidden");
        }

    })
    "use strict";
    var classify_itemCode = '';
    var page = {
        init: function () {
            page.initTree();
            page.initGrid();
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                if (!classify_itemCode) {
                    Changjie.alert.warning('请选择字典分类！');
                    return false;
                }
                top.selectedDataItemRow = null;
                var parentId = $('#gridtable').AgGridValue('F_ItemDetailId') || '0';
                Changjie.layerForm({
                    id: 'form',
                    title: '添加字典',
                    url: top.$.rootUrl + '/SystemModule/DataItem/Form?parentId=' + parentId + '&itemCode=' + classify_itemCode,
                    width: 500,
                    height: 370,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGird);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_ItemDetailId');
                top.selectedDataItemRow = $('#gridtable').AgGridGet('rowdata');


               

                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑字典',
                        url: top.$.rootUrl + '/SystemModule/DataItem/Form?itemCode=' + classify_itemCode,
                        width: 500,
                        height: 370,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGird);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_ItemDetailId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/SystemModule/DataItem/DeleteDetailForm', { keyValue: keyValue }, function () {
                                refreshGird();
                            });
                        }
                    });
                }
            });
            /*分类管理*/
            $('#category').on('click', function () {
                Changjie.layerForm({
                    id: 'ClassifyIndex',
                    title: '分类管理',
                    url: top.$.rootUrl + '/SystemModule/DataItem/ClassifyIndex',
                    width: 800,
                    height: 500,
                    maxmin: true,
                    btn: null,
                    end: function () {
                        location.reload();
                    }
                });
            });
        },
        initTree: function () {
            $('#left_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/DataItem/GetClassifyTree',
                nodeClick: function (item) {
                    classify_itemCode = item.value;
                    $('#titleinfo').text(item.text + '(' + classify_itemCode + ')');
                    page.search();
                }
            });
        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/SystemModule/DataItem/GetDetailList',
                headData: [
                    { headerName: '项目名', field: 'F_ItemName', width: 200,hide:true},
                    { headerName: '项目值', field: 'F_ItemValue', width: 200 },
                    { headerName: '简拼', field: 'F_SimpleSpelling', width: 150 },
                    { headerName: '排序', field: 'F_SortCode', width: 80 },
                    {
                        headerName: "有效", field: "F_EnabledMark", width: 100, 
                        cellRenderer: function (cellvalue) {
                            return cellvalue.value == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                        }
                    },
                    { headerName: "备注", field: "F_Description", width: 400 }
                ],

                isTree: true,
                mainId: 'F_ItemDetailId',
                parentId: 'F_ParentId',
                TreeGroupName: "项目名",
                displayName: "F_ItemName"
            });
        },
        search: function (param) {
            param = param || {};
            param.itemCode = classify_itemCode;
            $('#gridtable').AgGridSet('reload', param);
        }
    };

    refreshGird = function () {
        page.search();
    };

    page.init();
}


    