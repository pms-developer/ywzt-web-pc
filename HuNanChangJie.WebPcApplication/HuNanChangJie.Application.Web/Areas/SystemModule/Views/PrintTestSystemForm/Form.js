﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-08-03 17:49
 * 描  述：PrintTestSystemForm
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="PrintTestMian";
var processCommitUrl=top.$.rootUrl + '/SystemModule/PrintTestSystemForm/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'PrintTestSubA',"gridId":'PrintTestSubA'});
               subGrid.push({"tableName":'PrintTestSubB',"gridId":'PrintTestSubB'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#Sex').mkRadioCheckbox({
                type: 'radio',
                code: 'usersex',
            });
            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"extend",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#PrintTestSubA').jfGrid({
                headData: [
                    {
                        label: '毕业学校名称', name: 'SchoolName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'教育履历' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '开始日期', name: 'StartDate', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'教育履历' 
                        ,edit:{
                            type:'datatime',
                            change: function (row, index, oldValue, colname,headData) {
                            },
                            dateformat: '1'
                        }
                    },
                    {
                        label: '结束日期', name: 'EndDate', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'教育履历' 
                        ,edit:{
                            type:'datatime',
                            change: function (row, index, oldValue, colname,headData) {
                            },
                            dateformat: '1'
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'教育履历' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '主键', name: 'ID', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'教育履历'                     },
                ],
                mainId:"ID",
                bindTable:"PrintTestSubA",
                isEdit: true,
                height: 300,
                rowdatas:[
                {
                     "SortCode": 1,
                     "rowState": 1,
                     "ID": Changjie.newGuid(),
                     "EditType": 1,
                }],
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
            $('#PrintTestSubB').jfGrid({
                headData: [
                    {
                        label: '公司名称', name: 'Company', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'工作履历' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '职位', name: 'Position', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'工作履历' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '开始时间', name: 'StartDate', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'工作履历' 
                        ,edit:{
                            type:'datatime',
                            change: function (row, index, oldValue, colname,headData) {
                            },
                            dateformat: '1'
                        }
                    },
                    {
                        label: '结束时间', name: 'EndDate', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'工作履历' 
                        ,edit:{
                            type:'datatime',
                            change: function (row, index, oldValue, colname,headData) {
                            },
                            dateformat: '1'
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'工作履历' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '主键', name: 'ID', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'工作履历'                     },
                ],
                mainId:"ID",
                bindTable:"PrintTestSubB",
                isEdit: true,
                height: 300,
                rowdatas:[
                {
                     "SortCode": 1,
                     "rowState": 1,
                     "ID": Changjie.newGuid(),
                     "EditType": 1,
                }],
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/PrintTestSystemForm/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/PrintTestSystemForm/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="PrintTestMian"]').mkGetFormData());
        postData.strprintTestSubAList = JSON.stringify($('#PrintTestSubA').jfGridGet('rowdatas'));
        postData.strprintTestSubBList = JSON.stringify($('#PrintTestSubB').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
