﻿/*
 * 日 期：2017.04.17
 * 描 述：Excel导入配置	
 */
var refreshGirdData; // 更新数据
var downloadSystemDefaultTemplate;
var uploadCustomTemplate;
var deleteCustomTemplate;
var bootstrap = function ($, Changjie) {
    "use strict";
    var moduleId = "";
    var page = {
        init: function () {
            page.initGrid();
            page.bind();
        },
        bind: function () {
            $('#module_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/Module/GetModuleTree',
                nodeClick: function (item) {
                    if (item.hasChildren) {
                        moduleId = '';
                    }
                    else {
                        moduleId = item.id;
                        page.search();
                    }
                    $('#titleinfo').text(item.text);
                }
            });

            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                if (!moduleId) {
                    Changjie.alert.warning('请选择功能！');
                    return false;
                }
                Changjie.layerForm({
                    id: 'Form',
                    title: '添加快速导入',
                    url: top.$.rootUrl + '/SystemModule/ExcelImport/Form2?moduleId=' + moduleId,
                    width: 1100,
                    height: 700,
                    maxmin: true,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'Form',
                        title: '编辑快速导入',
                        url: top.$.rootUrl + '/SystemModule/ExcelImport/Form2?keyValue=' + keyValue,
                        width: 1100,
                        height: 700,
                        maxmin: true,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/SystemModule/ExcelImport/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 启用
            $('#enable').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认要【启用】！', function (res) {
                        if (res) {
                            Changjie.postForm(top.$.rootUrl + '/SystemModule/ExcelImport/UpdateForm?keyValue=' + keyValue, { F_EnabledMark: 1 }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 禁用
            $('#disable').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认要【停用】！', function (res) {
                        if (res) {
                            Changjie.postForm(top.$.rootUrl + '/SystemModule/ExcelImport/UpdateForm?keyValue=' + keyValue, { F_EnabledMark: 0 }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });

        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/ExcelImport/GetPageList',
                headData: [
                    { label: "模板名称", name: "F_Name", width: 160, align: "left" },
                    {
                        label: "绑定功能", name: "F_ModuleId", width: 160, align: "left",
                        formatter: function (cellvalue) {
                            var data = Changjie.clientdata.get(['modulesMap']);
                            if (data && data[cellvalue])
                                return data[cellvalue].F_FullName;
                            return "";
                        }
                    },
                    { label: "绑定按钮", name: "F_BtnName", width: 160, align: "left" },
                    {
                        label: "系统默认模板", name: "SystemDefaultTemplate", width: 160, align: "left",
                        formatter: function (value, row) {
                            if (row["F_Id"] != "" && row["F_Id"] != null) {
                                return "<a style='color:blue' onclick=\"downloadSystemDefaultTemplate('" + row["F_Id"] + "')\">下载</a>"
                            }
                            else
                                return ""
                        }
                    },
                    {
                        label: "自定义模板", name: "CustomTemplate", width: 160, align: "left",
                        formatter: function (value, row) {
                            if (value != "" && value != null) {
                                return "<a style='color:blue' href=" + value + ">下载</a>&nbsp;&nbsp;&nbsp;<a style='color:blue' onclick=\"uploadCustomTemplate('" + row['F_Id'] + "')\">更新</a>&nbsp;&nbsp;&nbsp;<a style='color:blue' onclick=\"deleteCustomTemplate('" + row['F_Id'] + "')\">删除</a>"
                            } else
                                return "<a style='color:blue' onclick=\"uploadCustomTemplate('" + row['F_Id'] + "')\">上传</a>"
                        }
                    },
                    {
                        label: "状态", name: "F_EnabledMark", width: 60, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue == 1) {
                                return '<span class=\"label label-success\" style=\"cursor: pointer;\">启用</span>';
                            } else if (cellvalue == 0) {
                                return '<span class=\"label label-default\" style=\"cursor: pointer;\">停用</span>';
                            }
                        }
                    },
                    {
                        label: '创建人', name: 'CreationName', width: 140, align: 'left'

                    },
                    {
                        label: '创建时间', name: 'CreationDate', width: 140, align: 'left',
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    }
                ],
                mainId: 'F_CustmerQueryId',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.moduleId = moduleId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };

    // 保存数据后回调刷新
    refreshGirdData = function () {
        page.search();
    }

    page.init();

    /*模板下载*/
    downloadSystemDefaultTemplate = function (id) {
        //Changjie.download({ url: top.$.rootUrl + '/SystemModule/ExcelImport/DownSchemeFile', param: { keyValue: id, __RequestVerificationToken: $.mkToken }, method: 'POST' });
        Changjie.download({ url: top.$.rootUrl + '/SystemModule/ExcelImport/DownSchemeFile1', param: { keyValue: id, __RequestVerificationToken: $.mkToken }, method: 'POST' });
    }

    uploadCustomTemplate = function (id) {
        if (Changjie.checkrow(id)) {
            Changjie.layerForm({
                id: 'UploadCustomTemplateForm',
                title: '上传自定义导入模板',
                url: top.$.rootUrl + '/SystemModule/ExcelImport/UploadCustomTemplateForm?keyValue=' + id,
                width: 300,
                height: 200,
                maxmin: true,
                callBack: function (id) {
                    return top[id].acceptClick(refreshGirdData);
                }
            });
        }
    }

    deleteCustomTemplate = function (id) {
        if (Changjie.checkrow(id)) {
            Changjie.layerConfirm('是否确认要删除自定义上传模板！', function (res) {
                if (res) {
                    Changjie.postForm(top.$.rootUrl + '/SystemModule/ExcelImport/DeleteCustomTemplate?keyValue=' + id, null, function () {
                        refreshGirdData();
                    });
                }
            });
        }
    }
}
