﻿/*
 * 日 期：2017.04.17
 * 描 述：导入配置
 */
var keyValue = request('keyValue');
var moduleId = request('moduleId');
var currentData;

var acceptClick;
var deleteTabSublist;

var add = 1;
var del = 2;
var rename = 4;
var resetDataType = 8;
var resetIsNull = 16;
var resetDeiscrption = 32;
var editColumns = [];

var originalData = [];
function setFieldState(row, index, columnName, type, newValue, oldValue) {

    if (!!!row.FieldState) {
        row.FieldState = 0;
    }
    if (row.FieldState == add) return;
    var item = { "columnName": columnName, "index": index, "FieldState": type };
    if (newValue != oldValue) {
        var isExist = false;
        if (editColumns.length <= 0) {
            editColumns.push(item);
            row.FieldState = row.FieldState + type;
        }
        else {
            for (var info in editColumns) {
                if (editColumns[info].columnName == columnName && editColumns[info].index == index) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                row.FieldState = row.FieldState + type;
                editColumns.push(item);
            }
        }
    }
    else {//编辑的值还原为原始值时去除修改状态(还原为编辑前的状态)
        var idx = -1;
        for (var i = 0; i < editColumns.length; i++) {
            if (editColumns[i].columnName == columnName && editColumns[i].index == index) {
                idx = i;
                break;
            }
        }
        if (idx >= 0) {
            row.FieldState = row.FieldState - type;
            editColumns.splice(idx, 1);//移除元素
        }
    }

}
var bootstrap = function ($, Changjie) {
    "use strict";
    var cols = [];
    var dbTable = '';
    var dbId = '';
    var btnName = '';
    var tab = 1;
    var colsPrimary = [];
    var colsSublist = [];
    var colsSublistSelect = [];
    var tableSetList = [];
    var TableName = "";


    function setDes(row, data) {
        var type = Number(data.F_RelationType);
        switch (type) {
            case 0://无关联
                row.F_Description = '无关联';
                break;
            case 1://GUID
                row.F_Description = '系统产生GUID';
                break;
            case 2://数据字典
                row.F_Description = '关联数据字典';
                break;
            case 3://数据表
                row.F_Description = '关联数据表';
                break;
            case 4://固定值
                row.F_Description = '固定数值/' + data.F_Value;
                break;
            case 5://操作人ID
                row.F_Description = '登录者ID';
                break;
            case 6://操作人名字
                row.F_Description = '登录者名字';
                break;
            case 7://操作时间
                row.F_Description = '导入时间';
                break;
            case 8://操作时间
                row.F_Description = '当前项目ID';
                break;
            case 9://操作时间
                row.F_Description = '当前主表信息ID';
                break;
            case 10://操作时间
                row.F_Description = '单据编码';
                break;
            case 11://上传文件url
                row.F_Description = '上传文件url';
                break;
            case 12://主表ID
                row.F_Description = '主表ID';
                break;
        }

    }

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#F_ModuleBtnId').mkselect({
                url: top.$.rootUrl + '/SystemModule/Module/GetButtonListNoAuthorize',
                param: {
                    moduleId: moduleId
                },
                value: 'F_EnCode',
                text: 'F_FullName',
                select: function (item) {
                    if (!!item) {
                        btnName = item.F_FullName
                    }
                    else {
                        btnName = '';
                    }

                }
            });
            $('#F_ErrorType').mkselect({ placeholder: false }).mkselectSet('1');
            $('#CallBackUrl').mkselect({
                url: top.$.rootUrl + '/Module/GetIframeModuleTree',
                value: 'F_ModuleId',
                text: 'F_UrlAddress',
                title:'F_FullName'
            });

            $('#gridtable').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "允许空", name: "IsNull", width: 80, align: "center",
                        formatter: function (cellvalue) {
                            return cellvalue? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            change: function (row, index, oldValue, columnName) {
                                //setFieldState(row, index, columnName, resetIsNull, row.IsNull, oldValue);//修改数据长度等同于修改数据类型
                                setCellValue("IsNull", row, index, "gridtable", row.IsNull);
                            },
                            data: [
                                { 'id': false, 'text': '否' },
                                { 'id': true, 'text': '是' }
                            ],
                            dfvalue: true// 默认选中项
                        }

                    },
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtable').jfGridSet('refreshdata', cols);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#filedtree').mktree({
                nodeCheck: function (item) {
                    if (item.checkstate == '1') {
                        if ($("#ExcelImportType").mkselectGet() == 1) {
                            var point = {
                                F_Name: item.value,
                                F_ColName: item.title,
                                F_OnlyOne: 0,
                                F_SortCode: cols.length,
                                F_RelationType: 0,
                                F_DataItemCode: '',
                                F_DataSourceId: '',
                                F_Value: '',
                                F_Description: '无关联',
                                TableName: '',
                                IsNull: true
                            };
                            cols.push(point);
                        } else {

                            if (tab == 1) {
                                var point = {
                                    F_Name: item.value,
                                    F_ColName: item.title,
                                    F_OnlyOne: 0,
                                    F_SortCode: colsPrimary.length,
                                    F_RelationType: 0,
                                    F_DataItemCode: '',
                                    F_DataSourceId: '',
                                    F_Value: '',
                                    F_Description: '无关联',
                                    TableName: TableName,
                                    IsNull: true
                                };
                                colsPrimary.push(point);
                            } else {
                                var point = {
                                    F_Name: item.value,
                                    F_ColName: item.title,
                                    F_OnlyOne: 0,
                                    F_SortCode: colsSublist.length,
                                    F_RelationType: 0,
                                    F_DataItemCode: '',
                                    F_DataSourceId: '',
                                    F_Value: '',
                                    F_Description: '无关联',
                                    TableName: TableName,
                                    IsNull: true
                                };
                                colsSublist.push(point);
                            }
                        }
                    }
                    else {
                        if ($("#ExcelImportType").mkselectGet() == 1) {
                            for (var i = 0, l = cols.length; i < l; i++) {
                                if (cols[i].F_Name == item.value) {
                                    cols.splice(i, 1);
                                    break;
                                }
                            }
                        } else {
                            if (tab == 1) {
                                for (var i = 0, l = colsPrimary.length; i < l; i++) {
                                    if (colsPrimary[i].F_Name == item.value) {
                                        colsPrimary.splice(i, 1);
                                        break;
                                    }
                                }
                            }
                            else {
                                for (var i = 0, l = colsSublist.length; i < l; i++) {
                                    if (colsSublist[i].F_Name == item.value && colsSublist[i].TableName == TableName) {
                                        colsSublist.splice(i, 1);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if ($("#ExcelImportType").mkselectGet() == 1) {
                        $('#gridtable').jfGridSet('refreshdata', cols);
                    } else {
                        if (tab == 1) {
                            $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                            $('#gridtablePrimary1').jfGridSet('refreshdata', colsPrimary);
                        } else {
                            var resultColsSublist = $.grep(colsSublist, function (item) {
                                return item.TableName == TableName;
                            });

                            $('#gridtableSublist').jfGridSet('refreshdata', resultColsSublist);
                        }
                    }
                }
            });
            $('#dbtree').mktree({
                url: top.$.rootUrl + '/SystemModule/DatabaseTable/GetTreeList',
                nodeClick: function (item) {
                    if (!item.hasChildren) {
                        dbId = item.value;
                        if ($("#ExcelImportType").mkselectGet() == 1) {
                            dbTable = item.text;
                            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: item.value, tableName: item.text }, function (res) {
                                console.log("dbtree");
                                cols.length = 0;
                                $('#gridtable').jfGridSet('refreshdata', cols);
                                $('#filedtree').mktreeSet('refresh', { data: res });
                                $('#filedtree').mktreeSet('allCheck');
                            });


                        } else {
                            TableName = item.text;
                            var resultLast = $.grep(tableSetList, function (item) {
                                return item.SortCode == tab;
                            });
                            if (resultLast.length == 0) {
                                $('#form_tabs ul').append('<li><a data-value="tabSublist" id="tab' + (tab + 1) + '">子表</a></li>');
                            }
                            tableSetList = $.grep(tableSetList, function (item) {
                                return item.SortCode !== tab;
                            });
                            if (tab == 1) {
                                $("#tab" + tab).html("主表：" + TableName);
                                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: item.value, tableName: TableName }, function (res) {
                                    console.log("dbtree0")
                                    colsPrimary.length = 0;
                                    $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                                    $('#gridtablePrimary1').jfGridSet('refreshdata', colsPrimary);
                                    $('#filedtree').mktreeSet('refresh', { data: res });
                                    $('#filedtree').mktreeSet('allCheck');
                                });

                                var point = {
                                    ImportId: keyValue,
                                    //DataReadMode: $('#DataReadMode').mkselectGet(),
                                    StartColumn: $('#StartColumn').val(),
                                    StartCell: $('#StartCell').val(),
                                    EndCell: $('#EndCell').val(),
                                    AttributesNumber: $('#AttributesNumber').val(),
                                    StartRow: 0,
                                    EndRow: 0,
                                    TableName: TableName,
                                    IsPrimary: true,
                                    SortCode: tab
                                };
                                tableSetList.push(point);

                            } else {
                                $("#tab" + tab).html("子表：" + TableName + " <a style='color:blue' onclick='deleteTabSublist()'>删除</a>");
                                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: item.value, tableName: TableName }, function (res) {
                                    console.log("dbtree1")
                                    //colsSublist.length = 0;
                                    colsSublist = $.grep(colsSublist, function (itemColsSublist) {
                                        return itemColsSublist.TableName !== TableName;
                                    });
                                    colsSublistSelect.length = 0;
                                    $('#gridtableSublist').jfGridSet('refreshdata', colsSublistSelect);
                                    $('#filedtree').mktreeSet('refresh', { data: res });
                                    $('#filedtree').mktreeSet('allCheck');
                                });

                                var point = {
                                    ImportId: keyValue,
                                    //DataReadMode: 0,
                                    StartColumn:0,
                                    StartCell: 0,
                                    EndCell: 0,
                                    AttributesNumber: 0,
                                    StartRow: $("#StartRow").val(),
                                    EndRow: $("#EndRow").val(),
                                    TableName: item.text,
                                    IsPrimary: false,
                                    SortCode: tab
                                };
                                tableSetList.push(point);
                            }
                        }
                    }
                    else {
                        dbTable = '';
                        dbId = '';
                        $('#filedtree').mktreeSet('refresh', { data: [] });
                    }
                }
            });


            $('#ExcelImportType').mkselect({
                data: [{ value: 1, text: '单表导入' }, { value: 2, text: '主从表一单导入' }, { value: 3, text: '主从表多单导入' }],
                value: 'value',
                text: 'text',
                title: 'text',
                maxHeight: 99,
                placeholder: false,
                select: function (item) {
                    if (item.value == 1) {
                        $("#gridtable").show();
                        $("#PrimaryAndSublistPlan").hide();
                        if (dbId.length > 0 && dbTable.length > 0) {
                            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: dbTable }, function (res) {
                                console.log("excelimporttype");
                                var map = {};
                                $.each(cols, function (id, item) {
                                    map[item.F_Name] = "1";
                                    setDes(item, item);
                                });
                                $('#gridtable').jfGridSet('refreshdata', cols);
                                $.each(res, function (id, item) {
                                    if (!!map[item.value]) {
                                        item.checkstate = '1';
                                    }
                                });
                                $('#filedtree').mktreeSet('refresh', { data: res });
                            });
                        }
                    }
                    else {
                        $('#form_tabs').mkFormTab();
                        $('#form_tabs ul li').eq(0).trigger('click');

                        $("#gridtable").hide();
                        $("#PrimaryAndSublistPlan").show();
                    }
                }
            }).mkselectSet('1');
            //$('#DataReadMode').mkselect({
            //    data: [{ value: 1, text: '按列' }, { value: 2, text: '按行' }],
            //    value: 'value',
            //    text: 'text',
            //    title: 'text',
            //    maxHeight: 99,
            //    placeholder: false,
            //    select: function (item) {
            //        if (item.value == 1) {
            //            $("#AttributesNumber").val("");
            //            $("#lableAttributesNumber").hide();
            //            $("#gridtablePrimary").show();
            //            $("#gridtablePrimary1").hide();
            //        }
            //        else if (item.value == 2) {
            //            $("#lableAttributesNumber").show();
            //            $("#gridtablePrimary").hide();
            //            $("#gridtablePrimary1").show();
            //        }
            //    }
            //}).mkselectSet('1');

            $('#gridtablePrimary').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "允许空", name: "IsNull", width: 80, align: "center",
                        formatter: function (cellvalue) {
                            return cellvalue==1 ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            change: function (row, index, oldValue, columnName) {
                                setFieldState(row, index, columnName, resetIsNull, row.IsNull, oldValue);//修改数据长度等同于修改数据类型
                            },
                            data: [
                                { 'id': '0', 'text': '否' },
                                { 'id': '1', 'text': '是' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }

                    },
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#gridtablePrimary1').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "允许空", name: "IsNull", width: 80, align: "center",
                        formatter: function (cellvalue) {
                            return cellvalue==1 ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            change: function (row, index, oldValue, columnName) {
                                setFieldState(row, index, columnName, resetIsNull, row.IsNull, oldValue);//修改数据长度等同于修改数据类型
                            },
                            data: [
                                { 'id': '0', 'text': '否' },
                                { 'id': '1', 'text': '是' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }

                    },
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtablePrimary1').jfGridSet('moveUp', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtablePrimary1').jfGridSet('moveDown', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-success\" style=\"cursor: pointer;\">下移</span>';
                        }
                    },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtablePrimary1').jfGridSet('refreshdata', colsPrimary);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#gridtableSublist').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "允许空", name: "IsNull", width: 80, align: "center",
                        formatter: function (cellvalue) {
                            return cellvalue==1 ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            change: function (row, index, oldValue, columnName) {
                                setFieldState(row, index, columnName, resetIsNull, row.IsNull, oldValue);//修改数据长度等同于修改数据类型
                            },
                            data: [
                                { 'id': '0', 'text': '否' },
                                { 'id': '1', 'text': '是' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }

                    },
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtableSublist').jfGridSet('refreshdata', colsSublist);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });

            $('#form_tabs').delegate('li', 'click', function () {
                var resultLast = $.grep(tableSetList, function (item) {
                    return item.SortCode == tab;
                });
                if (resultLast.length > 0) {
                    tableSetList = $.map(tableSetList, function (item) {
                        if (item.SortCode === tab) {
                            if (tab == 1) {
                                item.StartCell = $("#StartCell").val();
                                item.EndCell = $("#EndCell").val();
                                //item.DataReadMode = $("#DataReadMode").mkselectGet();
                                item.StartColumn = $("#StartColumn").val();
                                item.AttributesNumber = $("#AttributesNumber").val();
                            } else {
                                item.StartRow = $("#StartRow").val();
                                item.EndRow = $("#EndRow").val();
                            }
                        }
                        return item;
                    });
                }


                tab = $(this).index() + 1;

                var result = $.grep(tableSetList, function (item) {
                    return item.SortCode == tab;
                });

                if (result.length > 0) {
                    TableName = result[0].TableName;
                    console.log("TableName:" + TableName);
                    colsSublistSelect = $.grep(colsSublist, function (item) {
                        return item.TableName == TableName;
                    });
                    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: TableName }, function (res) {
                        console.log("click");
                        var map = {};
                        if (tab == 1) {
                            $.each(colsPrimary, function (id, item) {
                                map[item.F_Name] = "1";
                                setDes(item, item);
                            });
                            $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                            $('#gridtablePrimary1').jfGridSet('refreshdata', colsPrimary);
                        } else {
                            $.each(colsSublistSelect, function (id, item) {
                                map[item.F_Name] = "1";
                                setDes(item, item);
                            });
                            $('#gridtableSublist').jfGridSet('refreshdata', colsSublistSelect);
                            $("#StartRow").val(result[0].StartRow);
                            $("#EndRow").val(result[0].EndRow);

                        }
                        $.each(res, function (id, item) {
                            if (!!map[item.value]) {
                                item.checkstate = '1';
                            }
                        });
                        $('#filedtree').mktreeSet('refresh', { data: res });
                    });
                } else {
                    TableName = "";
                    if (tab == 1) {
                        colsPrimary.length = 0;
                        $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                        $('#gridtablePrimary1').jfGridSet('refreshdata', colsPrimary);
                    } else {
                        colsSublistSelect.length = 0;
                        $('#gridtableSublist').jfGridSet('refreshdata', colsSublistSelect);
                    }
                    $('#filedtree').mktreeSet('refresh', { data: [] });
                }
            });
        },
        initData: function () {
            $('#F_ModuleId').val(moduleId);
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/ExcelImport/GetFormData?keyValue=' + keyValue, function (data) {//
                    $('#F_ModuleBtnId').mkselectRefresh({
                        param: {
                            moduleId: data.entity.F_ModuleId
                        }
                    });
                    $('.mk-form-wrap').mkSetFormData(data.entity);
                    dbTable = data.entity.F_DbTable;
                    dbId = data.entity.F_DbId;
                    if (data.entity.ExcelImportType == 1) {
                        cols = data.list;
                        console.log("dbId:" + dbId + "&&dbTable:" + dbTable);
                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: dbTable }, function (res) {
                            console.log("init");
                            var map = {};
                            $.each(cols, function (id, item) {
                                map[item.F_Name] = "1";
                                setDes(item, item);
                            });
                            $('#gridtable').jfGridSet('refreshdata', cols);
                            $.each(res, function (id, item) {
                                if (!!map[item.value]) {
                                    item.checkstate = '1';
                                }
                            });
                            $('#filedtree').mktreeSet('refresh', { data: res });
                        });
                    } else {
                        colsPrimary = data.primaryList;
                        colsSublist = data.sublistList;
                        tableSetList = data.tableSetList;
                        TableName = data.primaryList[0].TableName;

                        $("#tab1").html("主表：" + TableName);
                        var tabIndex = 2;
                        $.each(tableSetList, function (id, item) {
                            if (item.TableName != TableName) {
                                $("#tab" + tabIndex).html("子表：" + item.TableName + " <a style='color:blue' onclick='deleteTabSublist()'>删除</a>");
                                tabIndex++;
                            } else {
                                //$("#DataReadMode").mkselectSet(item.DataReadMode);
                                $("#StartColumn").val(item.StartColumn);
                                $("#StartCell").val(item.StartCell);
                                $("#EndCell").val(item.EndCell);
                                $("#AttributesNumber").val(item.AttributesNumber);
                                $("#SkipRow").val(item.SkipRow);
                            }
                            $('#form_tabs ul').append('<li><a data-value="tabSublist" id="tab' + tabIndex + '">子表</a></li>');
                        });

                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: TableName }, function (res) {
                            console.log("init1");
                            var map = {};
                            $.each(colsPrimary, function (id, item) {
                                map[item.F_Name] = "1";
                                setDes(item, item);
                            });
                            $('#gridtablePrimary').jfGridSet('refreshdata', colsPrimary);
                            $('#gridtablePrimary1').jfGridSet('refreshdata', colsPrimary);
                            $.each(res, function (id, item) {
                                if (!!map[item.value]) {
                                    item.checkstate = '1';
                                }
                            });
                            $('#filedtree').mktreeSet('refresh', { data: res });
                        });
                    }
                });
            }
        }
    };

    acceptClick = function (callBack) {
        /* if (!$('.mk-form-layout-header').mkValidform()) {*/
        if (!$('.mk-form-wrap').mkValidform()) {
            return false;
        }
        if ($("#ExcelImportType").mkselectGet() == 1) {
            if (cols.length == 0) {
                Changjie.alert.error('请添加设置字段');
                return false;
            }
        } else {
            if (colsPrimary.length == 0) {
                Changjie.alert.error('请添加设置字段');
                return false;
            }
        }

        var resultLast = $.grep(tableSetList, function (item) {
            return item.SortCode == tab;
        });
        if (resultLast.length > 0) {
            tableSetList = $.map(tableSetList, function (item) {
                if (item.SortCode === tab) {
                    if (tab == 1) {
                        item.StartCell = $("#StartCell").val();
                        item.EndCell = $("#EndCell").val();
                        //item.DataReadMode = $("#DataReadMode").mkselectGet();
                        item.StartColumn = $("#StartColumn").val();
                        item.AttributesNumber = $("#AttributesNumber").val();
                        item.SkipRow = $("#SkipRow").val();
                    } else {
                        item.StartRow = $("#StartRow").val();
                        item.EndRow = $("#EndRow").val();
                    }
                }
                return item;
            });
        }

        /*var formData = $('.mk-form-layout-header').mkGetFormData(keyValue);*/
        var formData = $('.mk-form-wrap').mkGetFormData(keyValue);
        formData.F_DbId = dbId;
        formData.F_DbTable = dbTable;
        formData.F_BtnName = btnName;

        $.each(colsPrimary, function (id, item) {
            if (item.IsNull == "1") {
                item.IsNull = true;
            } else {
                item.IsNull = false;
            }
        });

        $.each(colsSublist, function (id, item) {
            if (item.IsNull == "1") {
                item.IsNull = true;
            } else {
                item.IsNull = false;
            }
        });

        var postData = {
            keyValue: keyValue,
            strEntity: JSON.stringify(formData),
            strList: JSON.stringify(cols),
            strPrimaryList: JSON.stringify(colsPrimary),
            strSublistList: JSON.stringify(colsSublist),
            strTableSetList: JSON.stringify(tableSetList),
        };
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/ExcelImport/SaveForm', postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    }
    page.init();


    deleteTabSublist = function () {
        Changjie.layerConfirm('是否确认要删除子表！', function (res) {
            if (res) {
                tableSetList = $.grep(tableSetList, function (item) {
                    return item.SortCode !== tab;
                });

                $.each(tableSetList, function (index, item) {
                    item.SortCode = index + 1;
                });

                colsSublist = $.grep(colsSublist, function (item) {
                    return item.TableName !== TableName;
                });

                colsSublistSelect.length = 0;
                TableName = "";
                $('#form_tabs ul li').eq(tab - 1).remove();
                $('#form_tabs ul li').each(function (index, element) {
                    $(this).find('a').attr('id', 'tab' + (index + 1));
                });
                tab = 0;
                $('#tab1').click();
            }
        });

    }
}
