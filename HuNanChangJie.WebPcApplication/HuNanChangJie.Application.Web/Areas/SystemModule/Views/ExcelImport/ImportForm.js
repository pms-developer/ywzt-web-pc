﻿/*
 * 日 期：2017.04.11
 * 描 述：导入Excel	
 */
var id = request('id');
var mainTableId = request("mainTableId");
var projectId = request("projectId");
var keyVaule = '';
var excelImportType = request('excelImportType');
//var excelImportData = request('excelImportData');

var bootstrap = function ($, Changjie) {
    "use strict";

    var page1 = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#add_format').on('click', function () {
                currentColRow = null;
                Changjie.layerForm({
                    id: 'FormatForm',
                    title: '添加',
                    url: top.$.rootUrl + '/SystemModule/CodeRule/FormatForm',
                    width: 450,
                    height: 310,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            $('#gridtable').jfGridSet('addRow', { row: data });
                        });
                    }
                });
            });
            $('#edit_format').on('click', function () {
                currentColRow = $('#gridtable').jfGridGet('rowdata');
                var _id = currentColRow ? currentColRow.itemTypeName : '';
                if (Changjie.checkrow(_id)) {
                    Changjie.layerForm({
                        id: 'FormatForm',
                        title: '修改',
                        url: top.$.rootUrl + '/SystemModule/CodeRule/FormatForm',
                        width: 450,
                        height: 310,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                $('#gridtable').jfGridSet('updateRow', { row: data });
                            });
                        }
                    });
                }

            });
            $('#delete_format').on('click', function () {
                currentColRow = null;
                var row = $('#gridtable').jfGridGet('rowdata');
                var _id = row ? row.itemTypeName : '';
                if (Changjie.checkrow(_id)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res, index) {
                        if (res) {
                            $('#gridtable').jfGridSet('removeRow');
                            top.layer.close(index); //再执行关闭  
                        }
                    });
                }
            });

            $('#gridtable').jfGrid({
                headData: [
                    { label: "前缀", name: "itemTypeName", width: 120, align: "left" },
                    { label: "格式", name: "formatStr", width: 120, align: "left" },
                    { label: "步长", name: "stepValue", width: 100, align: "left" },
                    { label: "初始值", name: "initValue", width: 120, align: "left" },
                    { label: "说明", name: "description", width: 180, align: "left" }
                ]
            });

            /*检测重复项*/
            $('#F_EnCode').on('blur', function () {
                $.mkExistField(keyValue, 'F_EnCode', top.$.rootUrl + '/SystemModule/CodeRule/ExistEnCode');
            });
            $('#F_FullName').on('blur', function () {
                $.mkExistField(keyValue, 'F_FullName', top.$.rootUrl + '/SystemModule/CodeRule/ExistFullName');
            });
        },
        initData: function () {
            if (!!selectedRow) {
                keyValue = selectedRow.F_RuleId;
                $('#form').mkSetFormData(selectedRow);
                var formatdata = JSON.parse(selectedRow.F_RuleFormatJson);
                $('#gridtable').jfGridSet('refreshdata', formatdata);
            }
        }
    };

    var fileInfo = {};

    // 触发合并文件碎片
    var mergeFileChunks = function (file) {

        var param = {};
        param['__RequestVerificationToken'] = $.mkToken;
        param['fileId'] = fileInfo[file.id].fileGuid;
        param['chunks'] = fileInfo[file.id].chunks;
        param['ext'] = file.ext;
        param['templateId'] = id;
        param.projectId = projectId;
        param.mainTableId = mainTableId;
        //param.excelImportData = excelImportData;

        Changjie.httpAsyncPost(
        /* top.$.rootUrl + "/SystemModule/ExcelImport/ExecuteImportExcel", param, function (res) {*/
            top.$.rootUrl + "/SystemModule/ExcelImport/ExecuteImportExcelNew", param, function (res) {
            var $fileItem = $('#form_file_queue_list').find('#filequeue_' + file.id);
            $fileItem.find('.mk-uploader-progress').remove();
            debugger;
            if (res.code == Changjie.httpCode.success) {
                if (res.data.Success != '0') {
                    Changjie.alert.success('导入成功' + res.data.Success + '条');
                    //跳转到指定页面  BatchID（导入批次ID）
                    if (res.data.CallBackUrl != "") {
                        Changjie.layerClose(window.name);//关闭导入页面
                        //跳转页面
                        var moduledata = Changjie.httpGet(top.$.rootUrl + "/Module/GetModuleInfo?keyValue=" + res.data.CallBackUrl).data;
                        if (moduledata != null) {
                            var moduleurl ="";
                            if (moduledata.F_UrlAddress.indexOf('?') > -1)
                                moduleurl = moduledata.F_UrlAddress + "&BatchID=" + res.data.BatchID;
                            else
                                moduleurl = moduledata.F_UrlAddress + "?BatchID=" + res.data.BatchID;
                            Changjie.frameTab.open({ F_ModuleId: res.data.CallBackUrl, F_Icon: moduledata.F_Icon, F_FullName: moduledata.F_FullName, F_UrlAddress: moduleurl });
                        }
                        
                    }
                }
                // 文件保存成功后
                $fileItem.append('<div class="mk-msg2"><span>' + res.data.Success + '</span><span>/</span><span style="color:#b94a48;" >' + res.data.Fail + '</span></div>');
                // 如果有失败
                if (res.data.Fail != '0')
                {
                 /*   Changjie.download({ url: top.$.rootUrl + '/SystemModule/ExcelImport/DownImportErrorFile', param: { fileId: fileInfo[file.id].fileGuid, fileName: fileInfo[file.id].name, __RequestVerificationToken: $.mkToken }, method: 'POST' });*/
                    Changjie.download({ url: top.$.rootUrl + '/SystemModule/ExcelImport/DownImportErrorFileNew', param: { fileId: fileInfo[file.id].fileGuid, fileName: fileInfo[file.id].name, excelImportType: excelImportType, __RequestVerificationToken: $.mkToken }, method: 'POST' });
                    //Changjie.download({ url: $.rootUrldownloadError + '/SystemModule/ExcelImport/DownImportErrorFile', param: { fileId: fileInfo[file.id].fileGuid, fileName: fileInfo[file.id].name, __RequestVerificationToken: $.mkToken }, method: 'POST' });
                   
                }
            }
            else {
                $fileItem.append('<div class="mk-msg"><i class="fa fa-exclamation-circle"></i></div>');
            }
        });
    }
    // 触发清楚文件碎片
    var reomveFileChunks = function (file) {
        var param = {};
        param['__RequestVerificationToken'] = $.mkToken;
        param['fileGuid'] = fileInfo[file.id].fileGuid;
        param['chunks'] = fileInfo[file.id].chunks;
        Changjie.httpAsyncPost(top.$.rootUrl + "/SystemModule/Annexes/MergeAnnexesFile", param, function (res) { });
        var $fileItem = $('#form_file_queue_list').find('#filequeue_' + file.id);
        $fileItem.find('.mk-uploader-progress').remove();
        $fileItem.append('<div class="mk-msg"><i class="fa fa-exclamation-circle"></i></div>');
    }

    var page = {
        uploader: null,
        init: function () {
            /*模板下载*/
            //$('#down_file_btn').on('click', function () {
            //    Changjie.download({ url: top.$.rootUrl + '/SystemModule/ExcelImport/DownSchemeFile', param: { keyValue: id, __RequestVerificationToken: $.mkToken }, method: 'POST' });
            //});

            $('#down_file_btn').on('click', function () {
                Changjie.download({ url: top.$.rootUrl + '/SystemModule/ExcelImport/DownSchemeFileNew', param: { keyValue: id, __RequestVerificationToken: $.mkToken }, method: 'POST' });
            });


            if (!WebUploader.Uploader.support()) {
                alert('Web Uploader 不支持您的浏览器！如果你使用的是IE浏览器，请尝试升级 flash 播放器');
                throw new Error('WebUploader does not support the browser you are using.');
            }

            page.uploader = WebUploader.create({
                auto: true,
                swf: top.$.rootUrl + '/Content/webuploader/Uploader.swf',
                // 文件接收服务端。
                server: top.$.rootUrl + "/SystemModule/Annexes/UploadAnnexesFileChunk",
                // 选择文件的按钮。可选。
                // 内部根据当前运行是创建，可能是input元素，也可能是flash.
                pick: '#add_file_btn',
                dnd: '#form_file_queue',
                paste: 'document.body',
                disableGlobalDnd: true,
                accept: {
                    extensions: "xls,xlsx,zip"
                },
                multiple: true,
                // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
                resize: false,
                // 文件分片上传
                chunked: true,
                chunkRetry: 3,
                prepareNextFile: true,
                chunkSize: '1048576',
                // 上传参数
                formData: {
                    __RequestVerificationToken: $.mkToken
                }
            });
            page.uploader.on('fileQueued', page.fileQueued);
            page.uploader.on('uploadStart', page.uploadStart);
            page.uploader.on('uploadBeforeSend', page.uploadBeforeSend);
            page.uploader.on('uploadProgress', page.uploadProgress);
            page.uploader.on('uploadSuccess', page.uploadSuccess);
            page.uploader.on('uploadError', page.uploadError);
            page.uploader.on('uploadComplete', page.uploadComplete);
            page.uploader.on('error', page.error);

            $('#form_file_queue').mkscroll();

        },
        fileQueued: function (file) {// 文件加载到队列
            fileInfo[file.id] = { name: file.name };
            $('#form_file_queue .mk-form-file-queue-bg').hide();
            // 添加一条文件记录
            var $item = $('<div class="mk-form-file-queue-item" id="filequeue_' + file.id + '" ></div>');
            $item.append('<div class="mk-file-image"><img src="' + top.$.rootUrl + '/Content/images/filetype/' + file.ext + '.png"></div>');
            $item.append('<span class="mk-file-name">' + file.name + '(' + Changjie.countFileSize(file.size) + ')</span>');

            $('#form_file_queue_list').append($item);
        },
        uploadStart: function (file) {
            var $fileItem = $('#form_file_queue_list').find('#filequeue_' + file.id);
            $fileItem.append('<div class="mk-uploader-progress"><div class="mk-uploader-progress-bar" style="width:0%;"></div></div>');
        },
        uploadBeforeSend: function (object, data, headers) {
            data.chunk = data.chunk || 0;
            data.chunks = data.chunks || 1;
            fileInfo[data.id].fileGuid = fileInfo[data.id].fileGuid || WebUploader.Base.guid();
            data.fileGuid = fileInfo[data.id].fileGuid;
            fileInfo[data.id].chunks = data.chunks;
        },
        uploadProgress: function (file, percentage) {
            var $fileItem = $('#form_file_queue_list').find('#filequeue_' + file.id);
            $fileItem.find('.mk-uploader-progress-bar').css('width', (percentage * 100 + '%'));
        },
        uploadSuccess: function (file, res) {
            if (res.code == 200) {// 上传成功
                mergeFileChunks(file);
            }
            else {// 上传失败
                reomveFileChunks(file);
            }
        },
        uploadError: function (file, code) {
            reomveFileChunks(file);
        },
        uploadComplete: function (file) {
        },
        error: function (type) {
            switch (type) {
                case 'Q_TYPE_DENIED':
                    Changjie.alert.error('当前文件类型不允许上传');
                    break;
            };
        }
    };
    page.init();

}