﻿/*
 * 日 期：2017.04.11
 * 描 述：区域管理	
 */
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var currentData = top.layer_Form.currentData;
    var dataItemCode = '';
    var dataSourceId = '';
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            // 数据来源设置
            $('#F_DataItemCode').mkselect({
                allowSearch: true,
                maxHeight: 130,
                url: top.$.rootUrl + '/SystemModule/DataItem/GetClassifyTree',
                type: 'tree',
                select: function (item) {

                    if (!!item && item.id != -1) {
                        dataItemCode = item.value;
                        dataSourceId = item.id;
                    }
                    else {
                        dataItemCode = '';
                        dataSourceId = '';
                    }

                }
            });

            $('#F_DataSourceId').mkformselect({
                placeholder: '请选择数据源项',
                layerUrl: top.$.rootUrl + '/SystemModule/DataSource/SelectForm',
                layerUrlH: 500,
                layerUrlW: 800,
                dataUrl: top.$.rootUrl + '/SystemModule/DataSource/GetNameByCode'
            });

            Changjie.httpGet(top.$.rootUrl + '/SystemModule/CodeRule/GetPageList', { queryJson: '{}', pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                //console.log(data,9999)
                $('#F_CodeRuleId').mkselect({
                    allowSearch: true,
                    data: data.data.rows,
                    text: 'F_FullName',
                    value: 'F_RuleId',
                });
            }); 
           

            $('#F_RelationType').mkselect({
                data: [
                    { "id": 0, "text": "无关联" },
                    { "id": 1, "text": "GUID" },
                    { "id": 2, "text": "数据字典" },
                    { "id": 3, "text": "数据来源" },
                    { "id": 4, "text": "固定数据" },
                    { "id": 5, "text": "登录者ID" },
                    { "id": 6, "text": "登录者名字" },
                    { "id": 7, "text": "导入时间" },
                    { "id": 8, "text": "当前项目ID" },
                    { "id": 9, "text": "当前主表信息ID" },
                    { "id": 10, "text": "单据编码" },
                    { "id": 11, "text": "上传文件url" },
                    { "id": 12, "text": "主表ID" },
                    { "id": 13, "text": "导入批次ID" },
                    { "id": 14, "text": "导入参数" },
                ],
                placeholder: false,
                maxHeight: 190,
                select: function (item) {
                    $('#F_DataItemCode').parent().hide();
                    $('#F_DataSourceId').parent().hide();
                    $('#F_CodeRuleId').parent().hide();
                    
                    $('#F_Value').parent().hide();
                    if (item.id == 2) {
                        $('#F_DataItemCode').parent().show();
                    }
                    else if (item.id == 3) {
                        $('#F_DataSourceId').parent().show();
                    }
                    else if (item.id == 10) {
                        $('#F_CodeRuleId').parent().show();
                    }
                    else if (item.id == 4) {
                        $('#F_Value').parent().show();
                    }
                }
            });
        },
        initData: function () {
            if (!!currentData) {
                if (currentData.F_RelationType == 2) {
                    dataItemCode = currentData.F_DataItemCode;
                    dataSourceId = currentData.F_DataSourceId;
                    $('#F_DataItemCode').mkselectSet(dataSourceId);
                    currentData.F_DataSourceId = '';
                    currentData.F_DataItemCode = '';
                    $('#form').mkSetFormData(currentData);
                    currentData.F_DataItemCode = dataItemCode;
                    currentData.F_DataSourceId = dataSourceId;
                }
                else if (currentData.F_RelationType == 10) {
                    currentData.F_CodeRuleId = currentData.F_DataItemCode;
                   
                    //$('#F_CodeRuleId').mkselectSet(currentData.F_CodeRuleId);
                    $('#form').mkSetFormData(currentData);
                }
                else {
                    //
                    $('#form').mkSetFormData(currentData);
                }
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        var formData = $('#form').mkGetFormData();
        if (!!callBack) {
            if (formData.F_RelationType == 2) {
                formData.F_DataSourceId = dataSourceId;
                formData.F_DataItemCode = dataItemCode;
            }
            else if (formData.F_RelationType == 10) {
                var codeRuleId = $("#F_CodeRuleId").mkselectGet();
                //console.log(codeRuleId)
                formData.F_DataItemCode = codeRuleId;
            }
            callBack(formData);
        }
        return true;
    };
    page.init();
}