﻿/*
 * 日 期：2017.04.17
 * 描 述：导入配置
 */
var keyValue = request('keyValue');
var moduleId = request('moduleId');
var currentData;

var acceptClick;
var deleteTabSublist;
var bootstrap = function ($, Changjie) {
    "use strict";
    var cols = [];
    var dbTable = '';
    var dbId = '';
    var btnName = '';

    var tab = 1;
    var cols0 = [];
    var cols1 = [];
    var cols2 = [];
    var cols3 = [];
    var cols4 = [];
    var cols5 = [];
    var primaryTableName = "";
    var sublistTableName1 = "";
    var sublistTableName2 = "";
    var sublistTableName3 = "";
    var sublistTableName4 = "";
    var sublistTableName5 = "";

    var tableSetList = [];

    function setDes(row, data) {
        var type = Number(data.F_RelationType);
        switch (type) {
            case 0://无关联
                row.F_Description = '无关联';
                break;
            case 1://GUID
                row.F_Description = '系统产生GUID';
                break;
            case 2://数据字典
                row.F_Description = '关联数据字典';
                break;
            case 3://数据表
                row.F_Description = '关联数据表';
                break;
            case 4://固定值
                row.F_Description = '固定数值/' + data.F_Value;
                break;
            case 5://操作人ID
                row.F_Description = '登录者ID';
                break;
            case 6://操作人名字
                row.F_Description = '登录者名字';
                break;
            case 7://操作时间
                row.F_Description = '导入时间';
                break;
            case 8://操作时间
                row.F_Description = '当前项目ID';
                break;
            case 9://操作时间
                row.F_Description = '当前主表信息ID';
                break;
            case 10://操作时间
                row.F_Description = '单据编码';
                break;
            case 11://上传文件url
                row.F_Description = '上传文件url';
                break;
            case 12://主表ID
                row.F_Description = '主表ID';
                break;
        }

    }

    var page = {
        init: function () {
            $('#gridtablePrimary').show();
            $('#gridtablePrimary1').hide();
            $('#gridtableSublist1').hide();
            $('#gridtableSublist2').hide();
            $('#gridtableSublist3').hide();
            $('#gridtableSublist4').hide();
            $('#gridtableSublist5').hide();

            $('#headerPrimary').show();
            $('#headerSublist').hide();

           /* $('#tabSublist1').hide();*/
            $('#tabSublist2').hide();
            $('#tabSublist3').hide();
            $('#tabSublist4').hide();
            $('#tabSublist5').hide();
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#F_ModuleBtnId').mkselect({
                url: top.$.rootUrl + '/SystemModule/Module/GetButtonListNoAuthorize',
                param: {
                    moduleId: moduleId
                },
                value: 'F_EnCode',
                text: 'F_FullName',
                select: function (item) {
                    if (!!item) {
                        btnName = item.F_FullName
                    }
                    else {
                        btnName = '';
                    }

                }
            });
            $('#F_ErrorType').mkselect({ placeholder: false }).mkselectSet('1');
            $('#gridtable').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "是否为空", name: "IsNull", width: 60, align: "center",
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象
                            },
                            change: function (data, num) {// 行数据和行号

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }
                    }, 
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtable').jfGridSet('refreshdata', cols);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#filedtree').mktree({
                nodeCheck: function (item) {
                    if (tab == 1) {
                        if (item.checkstate == '1') {
                            var point = {
                                F_Name: item.value,
                                F_ColName: item.title,
                                F_OnlyOne: 0,
                                F_SortCode: cols.length,
                                F_RelationType: 0,
                                F_DataItemCode: '',
                                F_DataSourceId: '',
                                F_Value: '',
                                F_Description: '无关联',
                                TableName: primaryTableName,
                            };
                            if ($("#ExcelImportType").mkselectGet() == 1) {
                                cols.push(point);
                            } else {
                                cols0.push(point);
                            }
                        }
                        else {
                            if ($("#ExcelImportType").mkselectGet() == 1) {
                                for (var i = 0, l = cols.length; i < l; i++) {
                                    if (cols[i].F_Name == item.value) {
                                        cols.splice(i, 1);
                                        break;
                                    }
                                }
                            }
                            else {
                                for (var i = 0, l = cols0.length; i < l; i++) {
                                    if (cols0[i].F_Name == item.value) {
                                        cols0.splice(i, 1);
                                        break;
                                    }
                                }
                            }
                        }
                        $('#gridtable').jfGridSet('refreshdata', cols);

                        $('#gridtablePrimary').jfGridSet('refreshdata', cols0);
                        $('#gridtablePrimary1').jfGridSet('refreshdata', cols0);
                    } else if (tab == 2) {
                        if (item.checkstate == '1') {
                            var point = {
                                F_Name: item.value,
                                F_ColName: item.title,
                                F_OnlyOne: 0,
                                F_SortCode: cols1.length,
                                F_RelationType: 0,
                                F_DataItemCode: '',
                                F_DataSourceId: '',
                                F_Value: '',
                                F_Description: '无关联',
                                TableName: sublistTableName1,
                            };
                            cols1.push(point);
                        }
                        else {
                            for (var i = 0, l = cols1.length; i < l; i++) {
                                if (cols1[i].F_Name == item.value) {
                                    cols1.splice(i, 1);
                                    break;
                                }
                            }
                        }

                        $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols1);
                    } else if (tab == 3) {
                        if (item.checkstate == '1') {
                            var point = {
                                F_Name: item.value,
                                F_ColName: item.title,
                                F_OnlyOne: 0,
                                F_SortCode: cols2.length,
                                F_RelationType: 0,
                                F_DataItemCode: '',
                                F_DataSourceId: '',
                                F_Value: '',
                                F_Description: '无关联',
                                TableName: sublistTableName2,
                            };
                            cols2.push(point);
                        }
                        else {
                            for (var i = 0, l = cols1.length; i < l; i++) {
                                if (cols2[i].F_Name == item.value) {
                                    cols2.splice(i, 1);
                                    break;
                                }
                            }
                        }

                        $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols2);
                    } else if (tab == 4) {
                        if (item.checkstate == '1') {
                            var point = {
                                F_Name: item.value,
                                F_ColName: item.title,
                                F_OnlyOne: 0,
                                F_SortCode: cols3.length,
                                F_RelationType: 0,
                                F_DataItemCode: '',
                                F_DataSourceId: '',
                                F_Value: '',
                                F_Description: '无关联',
                                TableName: sublistTableName3,
                            };
                            cols3.push(point);
                        }
                        else {
                            for (var i = 0, l = cols1.length; i < l; i++) {
                                if (cols3[i].F_Name == item.value) {
                                    cols3.splice(i, 1);
                                    break;
                                }
                            }
                        }

                        $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols3);
                    } else if (tab == 5) {
                        if (item.checkstate == '1') {
                            var point = {
                                F_Name: item.value,
                                F_ColName: item.title,
                                F_OnlyOne: 0,
                                F_SortCode: cols4.length,
                                F_RelationType: 0,
                                F_DataItemCode: '',
                                F_DataSourceId: '',
                                F_Value: '',
                                F_Description: '无关联',
                                TableName: sublistTableName4,
                            };
                            cols4.push(point);
                        }
                        else {
                            for (var i = 0, l = cols1.length; i < l; i++) {
                                if (cols4[i].F_Name == item.value) {
                                    cols4.splice(i, 1);
                                    break;
                                }
                            }
                        }

                        $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols4);
                    } else if (tab == 6) {
                        if (item.checkstate == '1') {
                            var point = {
                                F_Name: item.value,
                                F_ColName: item.title,
                                F_OnlyOne: 0,
                                F_SortCode: cols5.length,
                                F_RelationType: 0,
                                F_DataItemCode: '',
                                F_DataSourceId: '',
                                F_Value: '',
                                F_Description: '无关联',
                                TableName: sublistTableName5,
                            };
                            cols5.push(point);
                        }
                        else {
                            for (var i = 0, l = cols1.length; i < l; i++) {
                                if (cols5[i].F_Name == item.value) {
                                    cols5.splice(i, 1);
                                    break;
                                }
                            }
                        }

                        $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols5);
                    }
                }
            });
            $('#dbtree').mktree({
                url: top.$.rootUrl + '/SystemModule/DatabaseTable/GetTreeList',
                nodeClick: function (item) {
                    if (!item.hasChildren) {
                        if ($("#ExcelImportType").mkselectGet() == 1) {
                            dbTable = item.text;
                        } else {
                            dbTable = "";
                        }
                        dbId = item.value;
                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: item.value, tableName: item.text }, function (res) {
                            //cols.length = 0;
                            //cols0.length = 0;
                            //cols1.length = 0;
                            //cols2.length = 0;
                            //cols3.length = 0;
                            //cols4.length = 0;
                            //cols5.length = 0;

                            if (tab == 1) {
                                if ($("#ExcelImportType").mkselectGet() == 1) {
                                    $('#gridtable').jfGridSet('refreshdata', cols);
                                } else {
                                    $('#gridtablePrimary').jfGridSet('refreshdata', cols0);
                                    $('#gridtablePrimary1').jfGridSet('refreshdata', cols0);

                                    $('#tabPrimary').html("主表：" + item.text);
                                    primaryTableName = item.text;

                                    $('#tabSublist1').html("子表1");
                                    $('#tabSublist1').show();
                                }
                            } else if (tab == 2) {
                                $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols1);

                                $('#tabSublist' + (tab - 1)).html("子表：" + item.text);
                                sublistTableName1 = item.text;
                                $('#tabSublist2').html("子表2");
                                $('#tabSublist2').show();
                                $('#deleteTabSublist').show();
                            } else if (tab == 3) {
                                $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols2);

                                $('#tabSublist' + (tab - 1)).html("子表：" + item.text);
                                sublistTableName2 = item.text;
                                $('#tabSublist3').html("子表3");
                                $('#tabSublist3').show();
                            } else if (tab == 4) {
                                $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols3);

                                $('#tabSublist' + (tab - 1)).html("子表：" + item.text);
                                sublistTableName3 = item.text;
                                $('#tabSublist4').html("子表4");
                                $('#tabSublist4').show();
                            } else if (tab == 5) {
                                $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols4);

                                $('#tabSublist' + (tab - 1)).html("子表：" + item.text);
                                sublistTableName4 = item.text;
                                $('#tabSublist5').html("子表5");
                                $('#tabSublist5').show();
                            } else if (tab == 6) {
                                $('#gridtableSublist' + (tab - 1)).jfGridSet('refreshdata', cols5);

                                $('#tabSublist' + (tab - 1)).html("子表：" + item.text);
                                sublistTableName5 = item.text;
                            }
                            $('#filedtree').mktreeSet('refresh', { data: res });
                            $('#filedtree').mktreeSet('allCheck');
                        });
                    }
                    else {
                        dbTable = '';
                        dbId = '';
                        $('#filedtree').mktreeSet('refresh', { data: [] });
                    }
                }
            });


            $('#ExcelImportType').mkselect({
                data: [{ value: 1, text: '单表导入' }, { value: 2, text: '主从表一单导入' }, { value: 3, text: '主从表多单导入' }],
                value: 'value',
                text: 'text',
                title: 'text',
                maxHeight: 99,
                placeholder: false,
                select: function (item) {
                    $(".mk-form-layout-header").show();
                    if (item.value == 1) {
                        $("#gridtable").show();
                        $("#gridtablePrimary").hide();
                        $("#gridtablePrimary1").hide();
                        $("#gridtableSublist1").hide();
                        $("#gridtableSublist2").hide();
                        $("#gridtableSublist3").hide();
                        $("#gridtableSublist4").hide();
                        $("#gridtableSublist5").hide();

                        $(".mk-form-layout-header").hide();
                        if (dbTable.length > 0) {
                            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: dbTable }, function (res) {
                                if (tab == 1) {
                                    var map = {};
                                    $.each(cols, function (id, item) {
                                        map[item.F_Name] = "1";
                                        setDes(item, item);
                                    });
                                    $('#gridtable').jfGridSet('refreshdata', cols);

                                    $.each(res, function (id, item) {
                                        if (!!map[item.value]) {
                                            item.checkstate = '1';
                                        }
                                    });
                                    $('#filedtree').mktreeSet('refresh', { data: res });
                                }
                            });
                        } else {
                            $('#filedtree').mktreeSet('refresh', { data: [] });
                        }
                    }
                    else {
                        $("#gridtable").hide();
                        tab = 1;
                        if ($('#DataReadMode').mkselectGet() == 1) {
                            $("#AttributesNumber").val("");
                            $("#lableAttributesNumber").hide();
                            $("#gridtablePrimary").show();
                            $("#gridtablePrimary1").hide();
                        } else if ($('#DataReadMode').mkselectGet() == 2) {
                            $("#lableAttributesNumber").show();
                            $("#gridtablePrimary").hide();
                            $("#gridtablePrimary1").show();
                        }
                        $("#gridtableSublist1").hide();
                        $("#gridtableSublist2").hide();
                        $("#gridtableSublist3").hide();
                        $("#gridtableSublist4").hide();
                        $("#gridtableSublist5").hide();

                        $("#tabPrimary").click();
                        if (primaryTableName.length > 0) {
                            
                        } else {
                            $('#filedtree').mktreeSet('refresh', { data: [] });
                        }
                    }
                }
            }).mkselectSet('1');
            $('#DataReadMode').mkselect({
                data: [{ value: 1, text: '按列' }, { value: 2, text: '按行' }],
                value: 'value',
                text: 'text',
                title: 'text',
                maxHeight: 99,
                placeholder: false,
                select: function (item) {
                    if (item.value == 1) {
                        $("#AttributesNumber").val("");
                        $("#lableAttributesNumber").hide();
                        $("#gridtablePrimary").show();
                        $("#gridtablePrimary1").hide();
                    }
                    else if (item.value == 2) {
                        $("#lableAttributesNumber").show();
                        $("#gridtablePrimary").hide();
                        $("#gridtablePrimary1").show();
                    }
                }
            }).mkselectSet('1');

            $('#gridtablePrimary').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "是否为空", name: "IsNull", width: 60, align: "center",
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象
                            },
                            change: function (data, num) {// 行数据和行号

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }

                    }, 
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtablePrimary').jfGridSet('refreshdata', cols0);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ],
                isEdit: true,
            });
            $('#gridtablePrimary1').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "是否为空", name: "IsNull", width: 60, align: "center",
                        //edit: {
                        //    type: 'checkbox'
                        //}
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象
                            },
                            change: function (data, num) {// 行数据和行号

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }
                    }, 
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtablePrimary1').jfGridSet('moveUp', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtablePrimary1').jfGridSet('moveDown', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-success\" style=\"cursor: pointer;\">下移</span>';
                        }
                    },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtablePrimary1').jfGridSet('refreshdata', cols0);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ],
                isEdit: true,
            });
            $('#gridtableSublist1').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "是否为空", name: "IsNull", width: 60, align: "center",
                        //edit: {
                        //    type: 'checkbox'
                        //}
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象
                            },
                            change: function (data, num) {// 行数据和行号

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }
                    }, 
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtableSublist').jfGridSet('refreshdata', cols1);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#gridtableSublist2').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "是否为空", name: "IsNull", width: 60, align: "center",
                        //edit: {
                        //    type: 'checkbox'
                        //}
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象
                            },
                            change: function (data, num) {// 行数据和行号

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }
                    }, 
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtableSublist').jfGridSet('refreshdata', cols2);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#gridtableSublist3').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "是否为空", name: "IsNull", width: 60, align: "center",
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象
                            },
                            change: function (data, num) {// 行数据和行号

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }
                    }, 
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtableSublist').jfGridSet('refreshdata', cols3);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#gridtableSublist4').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "是否为空", name: "IsNull", width: 60, align: "center",
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象
                            },
                            change: function (data, num) {// 行数据和行号

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }
                    }, 
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtableSublist').jfGridSet('refreshdata', cols4);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#gridtableSublist5').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    {
                        label: "是否为空", name: "IsNull", width: 60, align: "center",
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象
                            },
                            change: function (data, num) {// 行数据和行号

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: '1'// 默认选中项
                        }
                    }, 
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtableSublist').jfGridSet('refreshdata', cols5);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#tabPrimary').on('click', function () {
                if (tab != 1) {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var tableName = "";
                    if (tab == 2)
                        tableName = sublistTableName1;
                    else if (tab == 3)
                        tableName = sublistTableName2;
                    else if (tab == 4)
                        tableName = sublistTableName3;
                    else if (tab == 5)
                        tableName = sublistTableName4;
                    else if (tab == 6)
                        tableName = sublistTableName5;

                    var point = {
                        ImportId: keyValue,
                        DataReadMode: 0,
                        StartCell: 0,
                        EndCell: 0,
                        AttributesNumber: 0,
                        StartRow: $("#StartRow").val(),
                        EndRow: $("#EndRow").val(),
                        TableName: tableName,
                        IsPrimary: false,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                } else {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });

                    var point = {
                        ImportId: keyValue,
                        DataReadMode: $('#DataReadMode').mkselectGet(),
                        StartCell: $('#StartCell').val(),
                        EndCell: $('#EndCell').val(),
                        AttributesNumber: $('#AttributesNumber').val(),
                        StartRow: 0,
                        EndRow: 0,
                        TableName: primaryTableName,
                        IsPrimary: true,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                }

                tab = 1;
                $('#headerPrimary').show();
                $('#headerSublist').hide();

                var dataReadModeVal = $('#DataReadMode').mkselectGet();
                if (dataReadModeVal == 1) {
                    $("#gridtablePrimary").show();
                    $("#gridtablePrimary1").hide();
                }
                else if (dataReadModeVal == 2) {
                    $("#gridtablePrimary").hide();
                    $("#gridtablePrimary1").show();
                }
                $('#gridtableSublist1').hide();
                $('#gridtableSublist2').hide();
                $('#gridtableSublist3').hide();
                $('#gridtableSublist4').hide();
                $('#gridtableSublist5').hide();

                $('#filedtree').mktreeSet('refresh', { data: [] });

                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: primaryTableName }, function (res) {
                    if (tab == 1) {
                        var map = {};
                        $.each(cols0, function (id, item) {
                            map[item.F_Name] = "1";
                            setDes(item, item);
                        });
                        //$('#gridtable').jfGridSet('refreshdata', cols);
                        $('#gridtablePrimary').jfGridSet('refreshdata', cols0);
                        $('#gridtablePrimary1').jfGridSet('refreshdata', cols0);

                        $.each(res, function (id, item) {
                            if (!!map[item.value]) {
                                item.checkstate = '1';
                            }
                        });
                        $('#filedtree').mktreeSet('refresh', { data: res });
                    }
                });

                $("#tabPrimary").css("border", "5px solid Red");
                for (var i = 1; i <= 5; i++) {
                    $("#tabSublist" + i).css("border", "1px solid black");
                }
            });

            $('#tabSublist1').on('click', function () {
                if (tab != 1) {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var tableName = "";
                    if (tab == 2)
                        tableName = sublistTableName1;
                    else if (tab == 3)
                        tableName = sublistTableName2;
                    else if (tab == 4)
                        tableName = sublistTableName3;
                    else if (tab == 5)
                        tableName = sublistTableName4;
                    else if (tab == 6)
                        tableName = sublistTableName5;
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: 0,
                        StartCell: 0,
                        EndCell: 0,
                        AttributesNumber: 0,
                        StartRow: $("#StartRow").val(),
                        EndRow: $("#EndRow").val(),
                        TableName: tableName,
                        IsPrimary: false,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                } else {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: $('#DataReadMode').mkselectGet(),
                        StartCell: $('#StartCell').val(),
                        EndCell: $('#EndCell').val(),
                        AttributesNumber: $('#AttributesNumber').val(),
                        StartRow: 0,
                        EndRow: 0,
                        TableName: primaryTableName,
                        IsPrimary: true,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                }

                tab = 2;
                $('#headerPrimary').hide();
                $('#headerSublist').show();

                $('#gridtablePrimary').hide();
                $('#gridtablePrimary1').hide();
                $('#gridtableSublist1').show();
                $('#gridtableSublist2').hide();
                $('#gridtableSublist3').hide();
                $('#gridtableSublist4').hide();
                $('#gridtableSublist5').hide();

                $('#filedtree').mktreeSet('refresh', { data: [] });

                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: sublistTableName1 }, function (res) {
                    if (tab == 2) {
                        var map = {};
                        $.each(cols1, function (id, item) {
                            map[item.F_Name] = "1";
                            setDes(item, item);
                        });
                        //$('#gridtable').jfGridSet('refreshdata', cols);
                        $('#gridtableSublist1').jfGridSet('refreshdata', cols1);

                        $.each(res, function (id, item) {
                            if (!!map[item.value]) {
                                item.checkstate = '1';
                            }
                        });
                        $('#filedtree').mktreeSet('refresh', { data: res });
                    }
                });

                var result = $.grep(tableSetList, function (item) {
                    return item.SortCode == tab;
                });
                if (result.length > 0) {
                    $("#StartRow").val(result[0].StartRow);
                    $("#EndRow").val(result[0].EndRow);
                }

                $("#tabPrimary").css("border", "1px solid black");
                for (var i = 1; i <= 5; i++) {
                    $("#tabSublist" + i).css("border", "1px solid black");
                }
                $("#tabSublist1").css("border", "5px solid Red");
            });

            $('#tabSublist2').on('click', function () {
                if (tab != 1) {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var tableName = "";
                    if (tab == 2)
                        tableName = sublistTableName1;
                    else if (tab == 3)
                        tableName = sublistTableName2;
                    else if (tab == 4)
                        tableName = sublistTableName3;
                    else if (tab == 5)
                        tableName = sublistTableName4;
                    else if (tab == 6)
                        tableName = sublistTableName5;
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: 0,
                        StartCell: 0,
                        EndCell: 0,
                        AttributesNumber: 0,
                        StartRow: $("#StartRow").val(),
                        EndRow: $("#EndRow").val(),
                        TableName: tableName,
                        IsPrimary: false,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                } else {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: $('#DataReadMode').mkselectGet(),
                        StartCell: $('#StartCell').val(),
                        EndCell: $('#EndCell').val(),
                        AttributesNumber: $('#AttributesNumber').val(),
                        StartRow: 0,
                        EndRow: 0,
                        TableName: primaryTableName,
                        IsPrimary: true,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                }

                tab = 3;
                $('#headerPrimary').hide();
                $('#headerSublist').show();

                $('#gridtablePrimary').hide();
                $('#gridtablePrimary1').hide();
                $('#gridtableSublist2').show();
                $('#gridtableSublist1').hide();
                $('#gridtableSublist3').hide();
                $('#gridtableSublist4').hide();
                $('#gridtableSublist5').hide();

                $('#filedtree').mktreeSet('refresh', { data: [] });

                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: sublistTableName2 }, function (res) {
                    if (tab == 3) {
                        var map = {};
                        $.each(cols2, function (id, item) {
                            map[item.F_Name] = "1";
                            setDes(item, item);
                        });
                        //$('#gridtable').jfGridSet('refreshdata', cols);
                        $('#gridtableSublist2').jfGridSet('refreshdata', cols2);

                        $.each(res, function (id, item) {
                            if (!!map[item.value]) {
                                item.checkstate = '1';
                            }
                        });
                        $('#filedtree').mktreeSet('refresh', { data: res });
                    }
                });

                var result = $.grep(tableSetList, function (item) {
                    return item.SortCode == tab;
                });
                if (result.length > 0) {
                    $("#StartRow").val(result[0].StartRow);
                    $("#EndRow").val(result[0].EndRow);
                }

                $("#tabPrimary").css("border", "1px solid black");
                for (var i = 1; i <= 5; i++) {
                    $("#tabSublist" + i).css("border", "1px solid black");
                }
                $("#tabSublist2").css("border", "5px solid Red");
            });

            $('#tabSublist3').on('click', function () {
                if (tab != 1) {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var tableName = "";
                    if (tab == 2)
                        tableName = sublistTableName1;
                    else if (tab == 3)
                        tableName = sublistTableName2;
                    else if (tab == 4)
                        tableName = sublistTableName3;
                    else if (tab == 5)
                        tableName = sublistTableName4;
                    else if (tab == 6)
                        tableName = sublistTableName5;
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: 0,
                        StartCell: 0,
                        EndCell: 0,
                        AttributesNumber: 0,
                        StartRow: $("#StartRow").val(),
                        EndRow: $("#EndRow").val(),
                        TableName: tableName,
                        IsPrimary: false,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                } else {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: $('#DataReadMode').mkselectGet(),
                        StartCell: $('#StartCell').val(),
                        EndCell: $('#EndCell').val(),
                        AttributesNumber: $('#AttributesNumber').val(),
                        StartRow: 0,
                        EndRow: 0,
                        TableName: primaryTableName,
                        IsPrimary: true,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                }

                tab = 4;
                $('#headerPrimary').hide();
                $('#headerSublist').show();

                $('#gridtablePrimary').hide();
                $('#gridtablePrimary1').hide();
                $('#gridtableSublist3').show();
                $('#gridtableSublist1').hide();
                $('#gridtableSublist2').hide();
                $('#gridtableSublist4').hide();
                $('#gridtableSublist5').hide();

                $('#filedtree').mktreeSet('refresh', { data: [] });

                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: sublistTableName3 }, function (res) {
                    if (tab == 4) {
                        var map = {};
                        $.each(cols3, function (id, item) {
                            map[item.F_Name] = "1";
                            setDes(item, item);
                        });
                        //$('#gridtable').jfGridSet('refreshdata', cols);
                        $('#gridtableSublist3').jfGridSet('refreshdata', cols3);

                        $.each(res, function (id, item) {
                            if (!!map[item.value]) {
                                item.checkstate = '1';
                            }
                        });
                        $('#filedtree').mktreeSet('refresh', { data: res });
                    }
                });

                var result = $.grep(tableSetList, function (item) {
                    return item.SortCode == tab;
                });
                if (result.length > 0) {
                    $("#StartRow").val(result[0].StartRow);
                    $("#EndRow").val(result[0].EndRow);
                }

                $("#tabPrimary").css("border", "1px solid black");
                for (var i = 1; i <= 5; i++) {
                    $("#tabSublist" + i).css("border", "1px solid black");
                }
                $("#tabSublist3").css("border", "5px solid Red");
            });

            $('#tabSublist4').on('click', function () {
                if (tab != 1) {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var tableName = "";
                    if (tab == 2)
                        tableName = sublistTableName1;
                    else if (tab == 3)
                        tableName = sublistTableName2;
                    else if (tab == 4)
                        tableName = sublistTableName3;
                    else if (tab == 5)
                        tableName = sublistTableName4;
                    else if (tab == 6)
                        tableName = sublistTableName5;
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: 0,
                        StartCell: 0,
                        EndCell: 0,
                        AttributesNumber: 0,
                        StartRow: $("#StartRow").val(),
                        EndRow: $("#EndRow").val(),
                        TableName: tableName,
                        IsPrimary: false,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                } else {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: $('#DataReadMode').mkselectGet(),
                        StartCell: $('#StartCell').val(),
                        EndCell: $('#EndCell').val(),
                        AttributesNumber: $('#AttributesNumber').val(),
                        StartRow: 0,
                        EndRow: 0,
                        TableName: primaryTableName,
                        IsPrimary: true,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                }

                tab = 5;
                $('#headerPrimary').hide();
                $('#headerSublist').show();

                $('#gridtablePrimary').hide();
                $('#gridtablePrimary1').hide();
                $('#gridtableSublist4').show();
                $('#gridtableSublist1').hide();
                $('#gridtableSublist2').hide();
                $('#gridtableSublist3').hide();
                $('#gridtableSublist5').hide();

                $('#filedtree').mktreeSet('refresh', { data: [] });

                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: sublistTableName4 }, function (res) {
                    if (tab == 5) {
                        var map = {};
                        $.each(cols4, function (id, item) {
                            map[item.F_Name] = "1";
                            setDes(item, item);
                        });
                        //$('#gridtable').jfGridSet('refreshdata', cols);
                        $('#gridtableSublist4').jfGridSet('refreshdata', cols4);

                        $.each(res, function (id, item) {
                            if (!!map[item.value]) {
                                item.checkstate = '1';
                            }
                        });
                        $('#filedtree').mktreeSet('refresh', { data: res });
                    }
                });

                var result = $.grep(tableSetList, function (item) {
                    return item.SortCode == tab;
                });
                if (result.length > 0) {
                    $("#StartRow").val(result[0].StartRow);
                    $("#EndRow").val(result[0].EndRow);
                }

                $("#tabPrimary").css("border", "1px solid black");
                for (var i = 1; i <= 5; i++) {
                    $("#tabSublist" + i).css("border", "1px solid black");
                }
                $("#tabSublist4").css("border", "5px solid Red");
            });

            $('#tabSublist5').on('click', function () {
                if (tab != 1) {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var tableName = "";
                    if (tab == 2)
                        tableName = sublistTableName1;
                    else if (tab == 3)
                        tableName = sublistTableName2;
                    else if (tab == 4)
                        tableName = sublistTableName3;
                    else if (tab == 5)
                        tableName = sublistTableName4;
                    else if (tab == 6)
                        tableName = sublistTableName5;
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: 0,
                        StartCell: 0,
                        EndCell: 0,
                        AttributesNumber: 0,
                        StartRow: $("#StartRow").val(),
                        EndRow: $("#EndRow").val(),
                        TableName: tableName,
                        IsPrimary: false,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                } else {
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== tab;
                    });
                    var point = {
                        ImportId: keyValue,
                        DataReadMode: $('#DataReadMode').mkselectGet(),
                        StartCell: $('#StartCell').val(),
                        EndCell: $('#EndCell').val(),
                        AttributesNumber: $('#AttributesNumber').val(),
                        StartRow: 0,
                        EndRow: 0,
                        TableName: primaryTableName,
                        IsPrimary: true,
                        SortCode: tab
                    };
                    tableSetList.push(point);
                }

                tab = 6;
                $('#headerPrimary').hide();
                $('#headerSublist').show();

                $('#gridtablePrimary').hide();
                $('#gridtablePrimary1').hide();
                $('#gridtableSublist5').show();
                $('#gridtableSublist1').hide();
                $('#gridtableSublist2').hide();
                $('#gridtableSublist3').hide();
                $('#gridtableSublist4').hide();

                $('#filedtree').mktreeSet('refresh', { data: [] });

                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: sublistTableName5 }, function (res) {
                    if (tab == 6) {
                        var map = {};
                        $.each(cols5, function (id, item) {
                            map[item.F_Name] = "1";
                            setDes(item, item);
                        });
                        //$('#gridtable').jfGridSet('refreshdata', cols);
                        $('#gridtableSublist5').jfGridSet('refreshdata', cols5);

                        $.each(res, function (id, item) {
                            if (!!map[item.value]) {
                                item.checkstate = '1';
                            }
                        });
                        $('#filedtree').mktreeSet('refresh', { data: res });
                    }
                });

                var result = $.grep(tableSetList, function (item) {
                    return item.SortCode == tab;
                });
                if (result.length > 0) {
                    $("#StartRow").val(result[0].StartRow);
                    $("#EndRow").val(result[0].EndRow);
                }

                $("#tabPrimary").css("border", "1px solid black");
                for (var i = 1; i <= 5; i++) {
                    $("#tabSublist" + i).css("border", "1px solid black");
                }
                $("#tabSublist5").css("border", "5px solid Red");
            });
        },
        initData: function () {
            $('#F_ModuleId').val(moduleId);
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/ExcelImport/GetFormData?keyValue=' + keyValue, function (data) {//
                    $('#F_ModuleBtnId').mkselectRefresh({
                        param: {
                            moduleId: data.entity.F_ModuleId
                        }
                    });
                    //$('.mk-form-layout-header').mkSetFormData(data.entity);
                    $('.mk-form-wrap').mkSetFormData(data.entity);
                    dbTable = data.entity.F_DbTable;
                    dbId = data.entity.F_DbId;

                    if (data.entity.ExcelImportType == 1) {
                        cols = data.list;
                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: dbTable }, function (res) {
                            if (tab == 1) {
                                var map = {};
                                $.each(cols, function (id, item) {
                                    map[item.F_Name] = "1";
                                    setDes(item, item);
                                });
                                $('#gridtable').jfGridSet('refreshdata', cols);

                                $.each(res, function (id, item) {
                                    if (!!map[item.value]) {
                                        item.checkstate = '1';
                                    }
                                });
                                $('#filedtree').mktreeSet('refresh', { data: res });
                            }
                        });
                    }
                    else {
                        cols0 = data.primaryList;
                        cols1 = data.sublistList1;
                        cols2 = data.sublistList2;
                        cols3 = data.sublistList3;
                        cols4 = data.sublistList4;
                        cols5 = data.sublistList5;
                        tableSetList = data.tableSetList;

                        $('#gridtablePrimary').jfGridSet('refreshdata', data.primaryList);
                        $('#gridtablePrimary1').jfGridSet('refreshdata', data.primaryList);
                        $('#gridtableSublist1').jfGridSet('refreshdata', data.sublistList1);
                        $('#gridtableSublist2').jfGridSet('refreshdata', data.sublistList2);
                        $('#gridtableSublist3').jfGridSet('refreshdata', data.sublistList3);
                        $('#gridtableSublist4').jfGridSet('refreshdata', data.sublistList4);
                        $('#gridtableSublist5').jfGridSet('refreshdata', data.sublistList5);

                        if (data.primaryEntity != null) {
                            $("#DataReadMode").mkselectSet(data.primaryEntity.DataReadMode);
                            $("#StartCell").val(data.primaryEntity.StartCell);
                            $("#EndCell").val(data.primaryEntity.EndCell);
                            $("#AttributesNumber").val(data.primaryEntity.AttributesNumber);

                            primaryTableName = data.primaryEntity.TableName;
                            $('#tabPrimary').html("主表：" + primaryTableName);
                            $('#tabSublist1').html("子表1");
                            $('#tabSublist1').show();
                        }

                        if (data.sublistEntity1 != null) {
                            sublistTableName1 = data.sublistEntity1.TableName;
                            $('#tabSublist1').html("子表：" + sublistTableName1);
                            $('#tabSublist2').html("子表2");
                            $('#tabSublist2').show();
                            $('#deleteTabSublist').show();
                        }
                        if (data.sublistEntity2 != null) {
                            sublistTableName2 = data.sublistEntity2.TableName;
                            $('#tabSublist2').html("子表：" + sublistTableName2);
                            $('#tabSublist3').html("子表3");
                            $('#tabSublist3').show();
                        }
                        if (data.sublistEntity3 != null) {
                            sublistTableName3 = data.sublistEntity3.TableName;
                            $('#tabSublist3').html("子表：" + sublistTableName3);
                            $('#tabSublist4').html("子表4");
                            $('#tabSublist4').show();
                        }
                        if (data.sublistEntity4 != null) {
                            sublistTableName4 = data.sublistEntity4.TableName;
                            $('#tabSublist4').html("子表：" + sublistTableName4);
                            $('#tabSublist5').html("子表5");
                            $('#tabSublist5').show();
                        }
                        if (data.sublistEntity5 != null) {
                            sublistTableName5 = data.sublistEntity5.TableName;
                            $('#tabSublist5').html("子表：" + sublistTableName5);
                        }

                        //Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: primaryTableName }, function (res) {
                        //    if (tab == 1) {
                        //        var map = {};
                        //        $.each(cols0, function (id, item) {
                        //            map[item.F_Name] = "1";
                        //            setDes(item, item);
                        //        });
                        //        //$('#gridtable').jfGridSet('refreshdata', cols);
                        //        $('#gridtablePrimary').jfGridSet('refreshdata', cols0);
                        //        $('#gridtablePrimary1').jfGridSet('refreshdata', cols0);

                        //        $.each(res, function (id, item) {
                        //            if (!!map[item.value]) {
                        //                item.checkstate = '1';
                        //            }
                        //        });
                        //        $('#filedtree').mktreeSet('refresh', { data: res });

                        //        $("#tabPrimary").css("border", "5px solid Red");
                        //    }
                        //});
                    }
                });
            }
        }
    };

    acceptClick = function (callBack) {
        /*if (!$('.mk-form-layout-header').mkValidform()) {*/
        if (!$('.mk-form-wrap').mkValidform()) {
            return false;
        }

        if ($("#ExcelImportType").mkselectGet() == 1) {
            if (cols.length == 0) {
                Changjie.alert.error('请添加设置字段');
                return false;
            }
        } else {
            if (cols0.length == 0) {
                Changjie.alert.error('请添加设置字段');
                return false;
            }
        }
        /*var formData = $('.mk-form-layout-header').mkGetFormData(keyValue);*/
        var formData = $('.mk-form-wrap').mkGetFormData(keyValue);
        formData.F_DbId = dbId;
        formData.F_DbTable = dbTable;
        formData.F_BtnName = btnName;

        var postData = {
            keyValue: keyValue,
            strEntity: JSON.stringify(formData),
            strList: JSON.stringify(cols),
            strPrimaryList: JSON.stringify(cols0),
            strSublistList1: JSON.stringify(cols1),
            strSublistList2: JSON.stringify(cols2),
            strSublistList3: JSON.stringify(cols3),
            strSublistList4: JSON.stringify(cols4),
            strSublistList5: JSON.stringify(cols5),
            strTableSetList: JSON.stringify(tableSetList),
        };
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/ExcelImport/SaveForm', postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    }
    page.init();


    deleteTabSublist = function () {
        Changjie.layerConfirm('是否确认要删除子表！', function (res) {
            if (res) {
                if (cols5.length > 0) {
                    cols5.length = 0;
                    sublistTableName5 = "";
                    $('#gridtableSublist5').jfGridSet('refreshdata', cols5);
                    tableSetList = $.grep(tableSetList, function (item) {
                        return item.SortCode !== 6;
                    });
                    $('#tabSublist5').html("子表5");
                    $("#tabSublist5").show();
                    $('#tabSublist4').click();
                } else {
                    if (cols4.length > 0) {
                        cols4.length = 0;
                        sublistTableName4 = "";
                        $('#gridtableSublist4').jfGridSet('refreshdata', cols4);
                        tableSetList = $.grep(tableSetList, function (item) {
                            return item.SortCode !== 5;
                        });
                        $('#tabSublist4').html("子表4");
                        $("#tabSublist4").show();
                        $("#tabSublist5").hide();
                        $('#tabSublist3').click();
                    } else {
                        if (cols3.length > 0) {
                            cols3.length = 0;
                            sublistTableName3 = "";
                            $('#gridtableSublist3').jfGridSet('refreshdata', cols3);
                            tableSetList = $.grep(tableSetList, function (item) {
                                return item.SortCode !== 4;
                            });
                            $('#tabSublist3').html("子表3");
                            $("#tabSublist3").show();
                            $("#tabSublist4").hide();
                            $('#tabSublist2').click();
                        }
                        else {
                            if (cols2.length > 0) {
                                cols2.length = 0;
                                sublistTableName2 = "";
                                $('#gridtableSublist2').jfGridSet('refreshdata', cols2);
                                tableSetList = $.grep(tableSetList, function (item) {
                                    return item.SortCode !== 3;
                                });

                                $('#tabSublist2').html("子表2");
                                $("#tabSublist2").show();
                                $("#tabSublist3").hide();
                                $('#tabSublist1').click();
                            } else {
                                if (cols1.length > 0) {
                                    cols1.length = 0;
                                    sublistTableName1 = "";
                                    $('#gridtableSublist1').jfGridSet('refreshdata', cols1);
                                    tableSetList = $.grep(tableSetList, function (item) {
                                        return item.SortCode !== 2;
                                    });

                                    $('#deleteTabSublist').hide();
                                    $('#tabSublist1').html("子表1");
                                    $("#tabSublist1").show();
                                    $("#tabSublist2").hide();
                                    $('#tabPrimary').click();
                                } else {
                                    
                                }
                            }
                        }
                    }
                }
            }
        });

    }
}
