﻿/*
 * 日 期：2017.04.17
 * 描 述：导入配置
 */
var keyValue = request('keyValue');
var moduleId = request('moduleId');
var currentData;

var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var cols = [];
    var dbTable = '';
    var dbId = '';
    var btnName = '';
    var tab = 1;
    var primaryTable = '';
    var sublistTable = '';
    var primaryCols = [];
    var sublistCols = [];
    var dfop = [];

    function setDes(row, data) {
        var type = Number(data.F_RelationType);
        switch (type) {
            case 0://无关联
                row.F_Description = '无关联';
                break;
            case 1://GUID
                row.F_Description = '系统产生GUID';
                break;
            case 2://数据字典
                row.F_Description = '关联数据字典';
                break;
            case 3://数据表
                row.F_Description = '关联数据表';
                break;
            case 4://固定值
                row.F_Description = '固定数值/' + data.F_Value;
                break;
            case 5://操作人ID
                row.F_Description = '登录者ID';
                break;
            case 6://操作人名字
                row.F_Description = '登录者名字';
                break;
            case 7://操作时间
                row.F_Description = '导入时间';
                break;
            case 8://操作时间
                row.F_Description = '当前项目ID';
                break;
            case 9://操作时间
                row.F_Description = '当前主表信息ID';
                break;
            case 10://操作时间
                row.F_Description = '单据编码';
                break;
            case 11://上传文件url
                row.F_Description = '上传文件url';
                break;
            case 12://主表ID
                row.F_Description = '主表ID';
                break;
        }

    }

    var page = {
        init: function () {
            dfop = {
                dbId: '',      // 数据主键
                dbTable: [], // 对应的表数据
                data: [{// 选项卡数据
                    id: '1',
                    text: '主表信息',
                    componts: []
                }]
            }
            //$.extend(dfop, op || {});
            //dfop.id = $self.attr('id');
            //$self[0]._mkCustmerFormDesigner = { dfop: dfop };
            //$self.addClass('mk-custmerform-designer-layout');
            //var _html = '';
            //_html += '<div class="mk-custmerform-designer-layout-left"  id="custmerform_compont_list_' + dfop.id + '"></div>';

            //_html += '<div class="mk-custmerform-designer-layout-center">';
            //_html += '<div class="mk-custmerform-designer-layout-header">';
            //_html += '<div class="mk-custmerform-designer-tabs" id="custmerform_designer_tabs_' + dfop.id + '">';
            //_html += '</div>';
            //_html += '</div>';

            //_html += '<div class="mk-custmerform-designer-layout-area" id="custmerform_designer_layout_area_' + dfop.id + '" ></div>';
            //_html += '<div class="mk-custmerform-designer-layout-footer">';
            //_html += '<div class="mk-custmerform-designer-layout-footer-item" id="custmerform_tabsEdit_btn_' + dfop.id + '"><i class="fa fa-pencil-square-o"></i><span>选项卡编辑</span></div>';
            //_html += '<div class="mk-custmerform-designer-layout-footer-item" id="custmerform_preview_btn_' + dfop.id + '"><i class="fa fa-eye"></i><span>预览表单</span></div>';
            //_html += '</div>';
            //_html += '<div class="mk-custmerform-designer-layout-center-bg"><img src="' + top.$.rootUrl + '/Content/images/tableform.png" /></div>';
            //_html += '<div class="mk-custmerform-designer-layout-tabedit" id="custmerform_designer_layout_tabedit_' + dfop.id + '" ></div>';

            //_html += '</div>';
            //_html += '<div class="mk-custmerform-designer-layout-right" id="custmerform_compont_property_' + dfop.id + '"></div>';

            //$self.html(_html);


            //$('#multilist').hide();
            //$('#gridtable').hide();
            $('#gridtablePrimary').show();
            $('#gridtablePrimary1').hide();
            $('#gridtableSublist').hide();
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            $('#custmerform_designer_layout_area_1').show();
            $('#custmerform_designer_layout_area_2').hide();
            page.bind();
            page.initData();

            page.tabbind();
        },
        bind: function () {
            // 优化滚动条
            //$('#custmerform_designer_tabs_').mkscroll();

            // 编辑选项卡
            $('#custmerform_designer_layout_tabedit_').jfGrid({
                headData: [
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#custmerform_designer_layout_tabedit_').jfGridSet('moveUp', rowindex);
                                if (res) {
                                    page.renderTabs();
                                }
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#custmerform_designer_layout_tabedit_').jfGridSet('moveDown', rowindex);
                                if (res) {
                                    page.renderTabs();
                                }
                                return false;
                            });
                            return '<span class=\"label label-success\" style=\"cursor: pointer;\">下移</span>';
                        }
                    },
                    {
                        label: "选项卡名称", name: "text", width: 200, align: "left",
                        edit: {
                            type: 'input',
                            change: function (row, rowIndex) {
                                $('#custmerform_designer_tabs_.mk-scroll-box').find('div').eq(rowIndex).html(row.text || '&nbsp;');
                            }
                        }
                    }
                ],
                mainId: 'id',
                isEdit: true,
                isMultiselect: true,
                rowdatas: dfop.data,
                onAddRow: function (row, rows) {
                    row.id = Changjie.newGuid();
                    row.text = '子表信息';
                    row.componts = [];

                    page.renderTabs();
                },
                onMinusRow: function (row, rows) {
                    page.renderTabs();
                },
                beforeMinusRow: function (row) {
                    if (row.id == '1') {
                        return false;
                    }
                    return true;
                },

            });

            $('#custmerform_tabsEdit_btn_').on('click', function () {
                var $edit = $('#custmerform_designer_layout_tabedit_');
                if ($edit.hasClass('open')) {
                    $edit.animate({ 'bottom': '-269px', speed: 2000 });
                    $edit.removeClass('open');
                }
                else {
                    $edit.animate({ 'bottom': '31px', speed: 2000 });
                    $edit.addClass('open');
                }

                $edit = null;
                return false;
            });
            $('#custmerform_designer_layout_tabedit_').on('click', function () {
                return false;
            });
            $('#multilist').on('click', function () {
                var $edit = $('#custmerform_designer_layout_tabedit_');
                $edit.animate({ 'bottom': '-269px', speed: 2000 });
                $edit.removeClass('open');
                $edit = null;
            });


            $('#F_ModuleBtnId').mkselect({
                url: top.$.rootUrl + '/SystemModule/Module/GetButtonListNoAuthorize',
                param: {
                    moduleId: moduleId
                },
                value: 'F_EnCode',
                text: 'F_FullName',
                select: function (item) {
                    if (!!item) {
                        btnName = item.F_FullName
                    }
                    else {
                        btnName = '';
                    }

                }
            });
            $('#F_ErrorType').mkselect({ placeholder: false }).mkselectSet('1');
            $('#ExcelImportType').mkselect({
                data: [{ value: 1, text: '单表导入' }, { value: 2, text: '主从表一单导入' }, { value: 3, text: '主从表多单导入' }],
                value: 'value',
                text: 'text',
                title: 'text',
                maxHeight: 99,
                placeholder: false,
                select: function (item) {
                }
            }).mkselectSet('1');
            $('#DataReadMode').mkselect({
                data: [{ value: 1, text: '按列' }, { value: 2, text: '按行' }],
                value: 'value',
                text: 'text',
                title: 'text',
                maxHeight: 99,
                placeholder: false,
                select: function (item) {
                    if (item.value == 1) {
                        $("#AttributesNumber").val("");
                        $("#lableAttributesNumber").hide();
                        $("#gridtablePrimary").show();
                        $("#gridtablePrimary1").hide();
                    }
                    else if (item.value == 2) {
                        $("#lableAttributesNumber").show();
                        $("#gridtablePrimary").hide();
                        $("#gridtablePrimary1").show();
                    }
                }
            }).mkselectSet('1');
            $('#gridtable').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtable').jfGridSet('refreshdata', cols);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#filedtree').mktree({
                nodeCheck: function (item) {
                    if (item.checkstate == '1') {
                        var point = {
                            F_Name: item.value,
                            F_ColName: item.title,
                            F_OnlyOne: 0,
                            F_SortCode: cols.length,
                            F_RelationType: 0,
                            F_DataItemCode: '',
                            F_DataSourceId: '',
                            F_Value: '',
                            F_Description: '无关联',

                        };
                        cols.push(point);
                    }
                    else {
                        for (var i = 0, l = cols.length; i < l; i++) {
                            if (cols[i].F_Name == item.value) {
                                cols.splice(i, 1);
                                break;
                            }
                        }
                    }
                    if (tab == 1) {
                        $('#gridtablePrimary').jfGridSet('refreshdata', cols);
                        $('#gridtablePrimary1').jfGridSet('refreshdata', cols);
                    } else {
                        $('#gridtableSublist').jfGridSet('refreshdata', cols);
                    }

                }
            });
            $('#dbtree').mktree({
                url: top.$.rootUrl + '/SystemModule/DatabaseTable/GetTreeList',
                nodeClick: function (item) {
                    if (!item.hasChildren) {
                        dbTable = item.text;
                        dbId = item.value;
                        if (tab == 1) {
                            $("#PrimaryTableName").val(item.text);
                            primaryTable = item.text;
                        } else if (tab == 2) {
                            $("#SublistTableName").val(item.text);
                            sublistTable = item.text;
                        }
                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: item.value, tableName: item.text }, function (res) {
                            cols.length = 0;
                            if (tab == 1) {
                                $('#gridtablePrimary').jfGridSet('refreshdata', cols);
                                $('#gridtablePrimary1').jfGridSet('refreshdata', cols);
                            } else {
                                $('#gridtableSublist').jfGridSet('refreshdata', cols);
                            }

                            $('#filedtree').mktreeSet('refresh', { data: res });
                            $('#filedtree').mktreeSet('allCheck');
                        });
                    }
                    else {
                        dbTable = '';
                        dbId = '';
                        $('#filedtree').mktreeSet('refresh', { data: [] });
                    }
                }
            });

            //$("#form_tabs").systemtables({
            //    type: type,
            //    keyvalue: mainId,
            //    state: "extend",
            //    isShowAttachment: false,
            //    isShowWorkflow: false,
            //});
            //$('#form_tabs').mkFormTab();
            //$('#form_tabs ul li').eq(0).trigger('click');

            $('#gridtablePrimary').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtablePrimary').jfGridSet('refreshdata', cols);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#gridtablePrimary1').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtablePrimary1').jfGridSet('moveUp', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#gridtablePrimary1').jfGridSet('moveDown', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-success\" style=\"cursor: pointer;\">下移</span>';
                        }
                    },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtablePrimary1').jfGridSet('refreshdata', cols);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            $('#gridtableSublist').jfGrid({
                headData: [
                    { label: "字段", name: "F_Name", width: 170, align: "left", editType: 'label' },
                    {
                        label: "Excel列名", name: "F_ColName", width: 170, align: "left",
                        edit: {
                            type: 'input'
                        }
                    },
                    { label: "唯一性", name: "F_OnlyOne", width: 60, align: "center", editType: 'checkbox' },
                    { label: "描述", name: "F_Description", width: 180, align: "left", editType: 'label' },
                    {
                        label: "", name: "F_Op", width: 50, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                currentData = row;
                                Changjie.layerForm({
                                    id: 'SetFieldForm',
                                    title: '设置字段属性【' + row.F_Name + '】',
                                    url: top.$.rootUrl + '/SystemModule/ExcelImport/SetFieldForm',
                                    width: 500,
                                    height: 360,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            row.F_RelationType = data.F_RelationType;
                                            row.F_Value = data.F_Value;
                                            row.F_DataItemCode = data.F_DataItemCode;
                                            row.F_DataSourceId = data.F_DataSourceId;
                                            setDes(row, data);
                                            $('#gridtableSublist').jfGridSet('refreshdata', cols);
                                        });
                                    }
                                });
                            });
                            return '<span class=\"label label-success \" style=\"cursor: pointer;\">设置</span>';
                        }
                    }
                ]
            });
            //$('#tabPrimary').on('click', function () {
            //    tab = 1;

            //    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: primaryTable }, function (res) {
            //        var map = {};
            //        $.each(primaryCols, function (id, item) {
            //            map[item.F_Name] = "1";
            //            setDes(item, item);
            //        });
            //        $('#gridtablePrimary').jfGridSet('refreshdata', primaryCols);
            //        $('#gridtablePrimary1').jfGridSet('refreshdata', primaryCols);
            //        $.each(res, function (id, item) {
            //            if (!!map[item.value]) {
            //                item.checkstate = '1';
            //            }
            //        });
            //        $('#filedtree').mktreeSet('refresh', { data: res });
            //    });
            //});
            //$('#tabSublist').on('click', function () {
            //    tab = 2;

            //    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: sublistTable }, function (res) {
            //        var map = {};
            //        $.each(sublistCols, function (id, item) {
            //            map[item.F_Name] = "1";
            //            setDes(item, item);
            //        });
            //        $('#gridtableSublist').jfGridSet('refreshdata', sublistCols);
            //        $.each(res, function (id, item) {
            //            if (!!map[item.value]) {
            //                item.checkstate = '1';
            //            }
            //        });
            //        $('#filedtree').mktreeSet('refresh', { data: res });
            //    });
            //});
        },
        initData: function () {
            $('#F_ModuleId').val(moduleId);
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/ExcelImport/GetFormData?keyValue=' + keyValue, function (data) {//
                    $('#F_ModuleBtnId').mkselectRefresh({
                        param: {
                            moduleId: data.entity.F_ModuleId
                        }
                    });
                    //$('.mk-form-layout-header').mkSetFormData(data.entity);
                    $('.mk-form-wrap').mkSetFormData(data.entity);
                    dbTable = data.entity.F_DbTable;
                    dbId = data.entity.F_DbId;
                    if (data.entity.ExcelImportType == 1) {
                        cols = data.list;
                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: dbTable }, function (res) {
                            var map = {};
                            $.each(cols, function (id, item) {
                                map[item.F_Name] = "1";
                                setDes(item, item);
                            });
                            $('#gridtable').jfGridSet('refreshdata', cols);
                            $.each(res, function (id, item) {
                                if (!!map[item.value]) {
                                    item.checkstate = '1';
                                }
                            });
                            $('#filedtree').mktreeSet('refresh', { data: res });
                        });
                    }
                    else if (data.entity.ExcelImportType == 2) {
                        if (data.tableSetEntity != null) {
                            primaryTable = data.tableSetEntity.PrimaryTableName;
                            sublistTable = data.tableSetEntity.SublistTableName;
                            primaryCols = data.tableSetEntity.primaryList;
                            sublistCols = data.tableSetEntity.sublistList;

                            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldTreeList', { databaseLinkId: dbId, tableName: primaryTable }, function (res) {
                                var map = {};
                                $.each(primaryCols, function (id, item) {
                                    map[item.F_Name] = "1";
                                    setDes(item, item);
                                });
                                $('#gridtablePrimary').jfGridSet('refreshdata', primaryCols);
                                $('#gridtablePrimary1').jfGridSet('refreshdata', primaryCols);
                                $.each(res, function (id, item) {
                                    if (!!map[item.value]) {
                                        item.checkstate = '1';
                                    }
                                });
                                $('#filedtree').mktreeSet('refresh', { data: res });
                            });
                        }
                    }


                });
            }
        },
        // 选项卡事件绑定
        tabbind: function () {
            $('#custmerform_designer_tabs_').delegate('div', 'click', function () {
                var $this = $(this);
                if (!$this.hasClass('active')) {
                    var $parent = $this.parent();
                    //var $self = $this.parents('.mk-form-layout');
                    //var _dfop = $self[0]._mkCustmerFormDesigner.dfop;

                    $parent.find('.active').removeClass('active');
                    $this.addClass('active');

                    if ($this.attr('data-value') == 1) {
                        $('#custmerform_designer_layout_area_1').show();
                        $('#custmerform_designer_layout_area_2').hide();

                        var dataReadModeVal= $('#DataReadMode').mkselectGet();
                        if (dataReadModeVal == 1) {
                            $("#gridtablePrimary").show();
                            $("#gridtablePrimary1").hide();
                        }
                        else if (dataReadModeVal == 2) {
                            $("#gridtablePrimary").hide();
                            $("#gridtablePrimary1").show();
                        }

                        $("#gridtableSublist").hide();
                    } else {
                        $('#custmerform_designer_layout_area_1').hide();
                        $('#custmerform_designer_layout_area_2').show();

                        var dataReadModeVal = $('#DataReadMode').mkselectGet();
                        if (dataReadModeVal == 1) {
                            $("#gridtablePrimary").show();
                            $("#gridtablePrimary1").hide();
                        }
                        else if (dataReadModeVal == 2) {
                            $("#gridtablePrimary").hide();
                            $("#gridtablePrimary1").show();
                        }

                        $("#gridtableSublist").show();
                    }
                    // 保存当前选项卡组件数据
                    //$.mkCustmerFormDesigner.saveComponts($self);
                    // 切换到新的选项卡数据
                    //_dfop._currentTabId = $this.attr('data-value');
                    //for (var i = 0; i < _dfop.data.length; i++) {
                    //var tabItem = _dfop.data[i];
                    //if (_dfop._currentTabId == tabItem.id) {
                    // _dfop._currentComponts = _dfop.data[i].componts;
                    //}
                    //}
                    // _dfop._isRenderComponts = true;
                    //$.mkCustmerFormDesigner.renderComponts($self);
                }
            });
        },
        // 渲染选项卡
        renderTabs: function () {// 渲染选项卡
            var $tabs = $('#custmerform_designer_tabs_ .mk-scroll-box');
            var tabsLength = dfop.data.length;
            var index = 0;
            $tabs.html("");
            for (var i = 0; i < tabsLength; i++) {
                var tabItem = dfop.data[i];

                $tabs.append('<div data-value="' + tabItem.id + '">' + tabItem.text || '&nbsp;' + '</div>');
                if (dfop._currentTabId == tabItem.id) {
                    index = i;
                }
            }

            if (tabsLength <= 1) {
                $('.mk-custmerform-designer-layout-center').removeClass('hasTab');
               /* $('.mk-form-layout').removeClass('hasTab');*/
            }
            else {
                 $('.mk-custmerform-designer-layout-center').addClass('hasTab');
              /*  $('.mk-form-layout').addClass('hasTab');*/
                $tabs.find('div').eq(index).addClass('active');

                var w = 0;
                var width = $tabs.children().each(function () {
                    w += $(this).outerWidth();
                });
                $tabs.css({ 'width': w });
            }
        },
        // 渲染数据
        renderData: function () {
            var $tabs = $('#custmerform_designer_tabs_.mk-scroll-box');
            var tabsLength = dfop.data.length;
            $tabs.html("");
            $('#custmerform_designer_layout_tabedit_').jfGridSet("refreshdata", dfop.data);
            for (var i = 0; i < tabsLength; i++) {
                var tabItem = dfop.data[i];
                $tabs.append('<div data-value="' + tabItem.id + '">' + tabItem.text || '&nbsp;' + '</div>');

            }
            if (tabsLength <= 1) {
                $('.mk-custmerform-designer-layout-center').removeClass('hasTab');
                //$('.mk-form-layout').removeClass('hasTab');
            }
            else {
                $('.mk-custmerform-designer-layout-center').addClass('hasTab');
                //$('.mk-form-layout').addClass('hasTab')
                $tabs.find('div').eq(0).addClass('active');
                var w = 0;
                var width = $tabs.children().each(function () {
                    w += $(this).outerWidth();
                });
                $tabs.css({ 'width': w });
            }
        },
    };

    acceptClick = function (callBack) {
        //if (!$('.mk-form-layout-header').mkValidform()) {
        //    return false;
        //}
        if (!$('.mk-form-wrap').mkValidform()) {
            return false;
        }
        var type = $('#ExcelImportType').mkselectGet();
        if (type == 1) {
            if (cols.length == 0) {
                Changjie.alert.error('请添加设置字段');
                return false;
            }
        } else if (type == 2) {
            if (primaryCols.length == 0) {
                Changjie.alert.error('请添加主表设置字段');
                return false;
            }
            if (sublistCols.length == 0) {
                Changjie.alert.error('请添加从表设置字段');
                return false;
            }
        }

        //var formData = $('.mk-form-layout-header').mkGetFormData(keyValue);
        var formData = $('.mk-form-wrap').mkGetFormData(keyValue);
        formData.F_DbId = dbId;
        formData.F_DbTable = dbTable;
        formData.F_BtnName = btnName;

        var postData = {
            keyValue: keyValue,
            strEntity: JSON.stringify(formData),
            strList: JSON.stringify(cols)
        };
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/ExcelImport/SaveForm', postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    }
    page.init();



}
