﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-11-13 20:34
 * 描  述：证照管理
 */
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 初始化左侧树形数据
            $('#dataTree').mktree({
                url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/GetTree?deep=0',
                nodeClick: function (item) {
                    if (item._deep == 0)
                        page.search();
                    else
                        page.search({ fk_zhengzhaomingcheng: item.value, deep: item._deep });
                }
            });

            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            $('#yazheng').on('click', function () {
               //location.reload();
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    page.updatestatus(keyValue,"押证")
                }
                else {
                    top.Changjie.alert.info("请选择要操作的行!");
                }
            });

            $('#jiezheng').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    page.updatestatus(keyValue, "解证")
                }
                else {
                    top.Changjie.alert.info("请选择要操作的行!");
                }
            });

            $('#zuofei').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    page.updatestatus(keyValue, "作废")
                }
                else {
                    top.Changjie.alert.info("请选择要操作的行!");
                }
            });

            $('#huifu').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    page.updatestatus(keyValue, "正常")
                }
                else {
                    top.Changjie.alert.info("请选择要操作的行!");
                }
            });

            // 新增
            $('#add').on('click', function () {
                var selectvalue = "";
                var currentItem = $('#dataTree').mktreeSet("currentItem");

                if (currentItem != null && typeof currentItem != "undefined") {
                    selectvalue = currentItem.value;
                }
                if (selectvalue == null || typeof selectvalue == "undefined") {
                    selectvalue = "";
                }
                Changjie.layerForm({
                    id: 'form',
                    title: '新增证照',
                    url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/Form?selectvalue=' + selectvalue,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            }); 
            
            
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/GetPageList',
                headData: [
                    { label: "证书名称", name: "fk_zhengzhaomingcheng", width: 180, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'BASE_ZZMC',
                                 key: value,
                                 keyId: 'zzmc_id',
                                 callback: function (_data) {
                                     callback(_data['zzmc_name']);
                                 }
                             });
                        }},
                    { label: "证书编号", name: "zz_bianhao", width: 200, align: "left" },
                    //{ label: "存档编号", name: "zz_cunfangweizhi", width: 160, align: "left" },
                    { label: "一证多押", name: "zz_yizhengduoya", width: 70, align: "center",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'SZSF',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    {
                        label: "关联类型", name: "zz_leixing", width: 70, align: "center",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ZZGLLX',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    //{ label: "关联机构", name: "zz_guanlianjigou", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('department', {
                    //             key: value,
                    //             callback: function (_data) {
                    //                 callback(_data.name);
                    //             }
                    //         });
                    //    }},
                    { label: "姓名", name: "zz_xinming", width: 60, align: "center"},
                    //{ label: "性别", name: "zz_xingbie", width: 40, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('dataItem', {
                    //             key: value,
                    //             code: 'XB',
                    //             callback: function (_data) {
                    //                 callback(_data.text);
                    //             }
                    //         });
                    //    }},
                    { label: "身份证号码", name: "zz_shenfengzheng", width: 140, align: "center"},
                    {
                        label: "证照有效期止", name: "zz_youxiaoqi2", width: 90, align: "center", formatter: function (cellvalue, row) {
                            var ct = "";
                            if (cellvalue) {
                                var currentTime = new Date();
                                var customTime = cellvalue;
                                customTime = customTime.replace("-", "/");
                                customTime = new Date(Date.parse(customTime));

                                if (currentTime > customTime) {
                                    ct = "<span style='font-size:13px;font-weight:bold;color:#f0ad4e'>" + Changjie.formatDate(cellvalue, 'yyyy-MM-dd') + "</span>";
                                }
                                else {
                                    ct = Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                                }
                            }
                            return ct;
                        }
                    },
                    {
                        label: "身份证有效期止", name: "zz_shenfengzhengguoqiriqi", width: 100, align: "center", formatter: function (cellvalue, row) {
                            var ct = "";
                            if (cellvalue) {
                                var currentTime = new Date();
                                var customTime = cellvalue;
                                customTime = customTime.replace("-", "/");
                                customTime = new Date(Date.parse(customTime));

                                if (currentTime > customTime) {
                                    ct = "<span style='font-size:13px;font-weight:bold;color:red'>" + Changjie.formatDate(cellvalue, 'yyyy-MM-dd') + "</span>";
                                }
                                else {
                                    ct = Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                                }
                            }
                            return ct;
                        }
                    },
                    {
                        label: "借出状态", name: "zz_jiechuzhuangtai", width: 70, align: "center", formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case '借出':
                                    return '<span class="label label-warning" style="cursor: pointer;">借出</span>';
                                default:
                                    return '<span class="label label-success" style="cursor: pointer;">正常</span>';
                            }
                        } },
                    {
                        label: "证照状态", name: "zz_zhuangtai", width: 70, align: "center", formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case '押证':
                                    return '<span class="label label-default" style="cursor: pointer;">押证</span>';
                                case '解证':
                                    return '<span class="label label-info" style="cursor: pointer;">解证</span>';
                                case '作废':
                                    return '<span class="label label-warning" style="cursor: pointer;">作废</span>';
                                default:
                                    return '<span class="label label-success" style="cursor: pointer;">正常</span>';
                            }
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "使用项目", name: "shiyongxiangmu", width: 200, align: "left" },
                    { label: "借出项目", name: "jiechuxiangmu", width: 200, align: "left" },
                    { label: "证件去向", name: "CredentialsWhereabouts", width: 150, align: "left" },
                    { label: "备注", name: "zz_beizhu", width: 100, align: "left" },
                    
                ],
                mainId:'ID',
                isPage: true
            });

            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        },
        updatestatus: function (id, status) {
            top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/UpdateStatus', { id: id, status:status }, function (data) {
                if (data.code == 200) {
                    top.Changjie.alert.success("操作成功!");
                    refreshGirdData();
                }
                else
                    top.Changjie.alert.error(data.info);
            });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: '编辑证照',
            url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
