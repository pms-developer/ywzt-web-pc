﻿var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $('#dosearch').on("click", function () {
                param = {};
                param.sword = $("#sword").val();
                page.search(param);
            })
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/GetZhengZhaoList',
                headData: [
                    { label: "证书名称", name: "zzmc_name", width: 100, align: "left" },
                    { label: "证书编号", name: "zz_bianhao", width: 100, align: "left" },
                    {label: "支持一证多押", name: "zz_yizhengduoya", width: 100, align: "left",},
                    {label: "姓名", name: "zz_xinming", width: 100, align: "left"},
                    { label: "身份证号码", name: "zz_shenfengzheng", width: 100, align: "left" },
                    {
                        label: "有效期止", name: "zz_youxiaoqi2", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {label: "备注", name: "zz_beizhu", width: 100, align: "left"}
                ],
                mainId: 'ID',
                isPage: true,
                height: 362,
                isMultiselect:true
            });
            page.search();
            
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};