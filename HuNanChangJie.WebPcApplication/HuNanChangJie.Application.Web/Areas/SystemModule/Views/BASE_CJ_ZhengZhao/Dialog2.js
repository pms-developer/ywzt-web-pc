﻿var acceptClick;
var projectId = request('projectid');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $('#dosearch').on("click", function () {
                page.search();
            })
            
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/GetZhengZhaoList2',
                headData: [
                    { label: "证书名称", name: "zzmc_name", width: 130, align: "left" },
                    { label: "证书编号", name: "zz_bianhao", width: 160, align: "left" },
                   
                    {label: "姓名", name: "zz_xinming", width: 100, align: "left"},
                    { label: "身份证号码", name: "zz_shenfengzheng", width: 120, align: "left" },
                    //{
                    //    label: "有效期止", name: "zz_youxiaoqi2", width: 100, align: "left",
                    //    formatter: function (cellvalue) {
                    //        return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                    //    }
                    //}, 
                    {
                        label: "一证多押", name: "zz_yizhengduoya", width: 80, align: "center", formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case 1:
                                    return '<span class="label label-warning" style="cursor: pointer;">是</span>';
                                default:
                                    return '<span class="label label-success" style="cursor: pointer;">否</span>';
                            }
                        }
                    },
                    {
                        label: "借出状态", name: "zz_jiechuzhuangtai", width: 80, align: "center", formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case '借出':
                                    return '<span class="label label-warning" style="cursor: pointer;">借出</span>';
                                default:
                                    return '<span class="label label-success" style="cursor: pointer;">正常</span>';
                            }
                        }
                    },
                    {
                        label: "证照状态", name: "zz_zhuangtai", width: 80, align: "center", formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case '押证':
                                    return '<span class="label label-default" style="cursor: pointer;">押证</span>';
                                case '解证':
                                    return '<span class="label label-info" style="cursor: pointer;">解证</span>';
                                case '作废':
                                    return '<span class="label label-warning" style="cursor: pointer;">作废</span>';
                                default:
                                    return '<span class="label label-success" style="cursor: pointer;">正常</span>';
                            }
                        }
                    },
                ],
                mainId: 'ID',
                isPage: true,
                height: 362,
                isMultiselect:true
            });
            page.search();
            
        },
        search: function (param) {
            param = param || {};
            param.projectid = projectId;
            param.sword = $("#sword").val();
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};