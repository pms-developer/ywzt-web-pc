﻿/* *  
 * 创建人：超级管理员
 * 日  期：2019-11-13 20:34
 * 描  述：证照管理
 */
var acceptClick;
var keyValue = request('keyValue');
var selectvalue = request('selectvalue');
var viewState = request('viewState');
var mainId="";
var type="add";
var subGrid = [];
var tables = [];
var mainTable = "Base_CJ_ZhengZhao";
var processCommitUrl = top.$.rootUrl + '/SystemModule/Base_CJ_ZhengZhao/SaveForm';
var projectId = request('projectId');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (type == 'add') {
                page.loadProjectInfo();

                $('#zz_leixing input[type]:eq(1)').trigger("click");
            }
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);

                $("#fk_zhengzhaomingcheng").attr("disabled", "disabled")
                $("#zz_bianhao").prop("disabled", "disabled")

                $("#zz_xinming").prop("disabled", "disabled")
                $("#zz_shenfengzheng").prop("disabled", "disabled")

                //$("input[name='zz_leixing']").prop("disabled", "disabled")
            }
        },
        bind: function () {
            $(".xiangmu,.renyuan,.jigou").hide();

            $('#fk_zhengzhaomingcheng').mkDataSourceSelect({ code: 'BASE_ZZMC', value: 'zzmc_id', text: 'zzmc_name' });

            if (type == "add" && selectvalue) {
                $('#fk_zhengzhaomingcheng').mkselectSet(selectvalue);
            }
            $('#zz_yizhengduoya').mkRadioCheckbox({
                type: 'radio',
                code: 'SZSF',
            });
            $('#zz_leixing').mkRadioCheckbox({
                type: 'radio',
                code: 'ZZGLLX',
            });
            $('#zz_leixing input[type]').removeProp("checked");

            $('#zz_leixing input[type]:eq(0)').on("click", function () {
                $(".xiangmu,.renyuan,.jigou").hide();
                $(".jigou").show();
            });

            $('#zz_leixing input[type]:eq(1)').on("click", function () {
                $(".xiangmu,.renyuan,.jigou").hide();
                $(".renyuan").show();
            });

            $('#zz_leixing input[type]:eq(2)').on("click", function () {
                $(".xiangmu,.renyuan,.jigou").hide();
                $(".xiangmu").show();
            });;

            $('#Project_ID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });

            $('#ProjectName').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });

            $('#zz_guanlianjigou').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
            });
            $('#zz_gongsiyuangong').mkRadioCheckbox({
                type: 'radio',
                code: 'SZSF',
            });
            $('#zz_xingbie').mkRadioCheckbox({
                type: 'radio',
                code: 'XB',
            });

            $('#is_master').mkRadioCheckbox({
                type: 'radio',
                code: 'SZSF',
            });

            $('#zz_zhucezhuantai').mkRadioCheckbox({
                type: 'radio',
                code: 'ZZZCZT',
            });

            //$('#WhetherToWorkOrNot').mkRadioCheckbox({
            //    type: 'radio',
            //    code: 'SZSF',
            //});
            //$('#WhetherToWorkOrNot').mkRadioCheckbox({
            //    type: 'radio',
            //    code: 'ZJQX',
            //});
          
            $('#zz_shebaoqingkuan').mkRadioCheckbox({
                type: 'radio',
                code: 'SZSF',
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "1",
                isShowAttachment: true,
                isShowWorkflow: false,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

           
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);     $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
        loadProjectInfo: function () {
             if (!!projectId) {
                 top.Changjie.setProjectInfo('form_tabs');
             }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;

        $.mkSaveForm(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhao/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }

    var cbrenyuan = $('#zz_leixing input[type]:eq(1)').get(0);
    if (cbrenyuan && cbrenyuan.checked) {

        if ($('#zz_shenfengzheng').val() == "") {
            top.Changjie.alert.error("表单信息输入有误,请检查!<br/>身份证号码不能为空!");
            return false;
        }
        if ($('#zz_xinming').val() == "") {
            top.Changjie.alert.error("表单信息输入有误,请检查!<br/>姓名不能为空!");
            return false;
        }
        //if ($('#zz_zhuanye').val() == "") {
        //    Changjie.alert.error("表单信息输入有误,请检查!<br/>专业不能为空!");
        //    return false;
        //}
        //if ($('#zz_shenfengzhengguoqiriqi1').val() == "") {
        //    Changjie.alert.error("表单信息输入有误,请检查!<br/>身份证有效期 起不能为空!");
        //    return false;
        //}
        //if ($('#zz_shenfengzhengguoqiriqi').val() == "") {
        //    Changjie.alert.error("表单信息输入有误,请检查!<br/>身份证有效期 止不能为空!");
        //    return false;
        //}
    }

    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
