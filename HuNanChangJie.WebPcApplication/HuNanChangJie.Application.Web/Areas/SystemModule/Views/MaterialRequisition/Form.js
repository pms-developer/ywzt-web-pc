﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-10-21 18:46
 * 描  述：领料单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MaterialRequisition";
var processCommitUrl = top.$.rootUrl + '/SystemModule/MaterialRequisition/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/MaterialRequisition/Audit', { keyValue: keyValue }, function (data) {
       
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/MaterialRequisition/UnAudit', { keyValue: keyValue }, function (data) {
      
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'MaterialRequisitionDetails', "gridId": 'MaterialRequisitionDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }

            setTimeout(function () {
                $('#Project_SubcontractId').unbind("change").on("change", function () {
                    var subcontractEx = $('#Project_SubcontractId').mkselectGetEx()

                    if (subcontractEx && subcontractEx.Yi) {
                        $("#SubTeam").mkselectSet(subcontractEx.Yi)
                    }

                    var rows = $('#MaterialRequisitionDetails').jfGridGet("rowdatas");
                    if (rows && rows.length > 0) {
                        top.Changjie.alert.info('变更分包合同将清除领料明细');
                        $("#MaterialRequisitionDetails").jfGridSet("refreshdata", []);
                    }
                })
            }, 3000)
        },
        bind: function () {

            var loginInfo = Changjie.clientdata.get(["userinfo"]);

            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'MaterialRequisitionCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });

            $('#CreateUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);



            $('#SubTeam').mkDataSourceSelect({ code: 'FBDW', value: 'id', text: 'name' });
            $('#Base_WarehouseId').mkDataSourceSelect({ code: 'Warehouse', value: 'id', text: 'title' });
            $('#Project_SubcontractId').mkselect({});
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });


            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);

                $('#Gcbw').attr('disabled', false);
                var sql = " ProjectID ='" + projectId + "' ";
                $('#Gcbw').mkDataSourceSelect({ code: 'GCBW', value: 'id', text: 'engineeringpartsname', strWhere: sql });

                var params = { "ProjectId": projectId, "AuditStatus": "2" };
                //if (type == "add") {
                //    params.IsFinishInStorage = 0;
                //}
                Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectSubcontract/GetPageList', { queryJson: JSON.stringify(params), pagination: JSON.stringify({ "rows": 500, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                    $("#Project_SubcontractId").mkselectRefresh({
                        value: "ID",
                        text: "FullName",
                        title: "FullName",
                        data: data.rows
                    })
                });
            }

            //$('#Gcbw').mkDataSourceSelect({ code: 'GCBW', value: 'id', text: 'engineeringpartsname' });
            //$('#Gcbw').attr('disabled', true);

            $('#Gcbw').mkselect({});

            $('#ProjectID').on("change", function () {
                var projectEx = $('#ProjectID').mkselectGetEx();
                if (projectEx) {

                    $('#Gcbw').attr('disabled', false);
                    var sql = " ProjectID ='" + projectEx.project_id + "' ";
                    $('#Gcbw').mkDataSourceSelect({ code: 'GCBW', value: 'id', text: 'engineeringpartsname', strWhere: sql });


                    var params = { "ProjectId": projectEx.project_id, "AuditStatus": "2" };
                    //if (type == "add") {
                    //    params.IsFinishInStorage = 0;
                    //}
                    Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectSubcontract/GetPageList', { queryJson: JSON.stringify(params), pagination: JSON.stringify({ "rows": 500, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                        $("#Project_SubcontractId").mkselectRefresh({
                            value: "ID",
                            text: "FullName",
                            title: "FullName",
                            data: data.rows
                        })
                    });
                }
            })


            $('#Base_WarehouseId').on("change", function () {
                var warehouseIdEx = $('#Base_WarehouseId').mkselectGetEx();
                var warehouseId = ''
                if (warehouseIdEx != null) {
                    warehouseId = warehouseIdEx.id
                    var rows = $('#MaterialRequisitionDetails').jfGridGet("rowdatas");
                    $.each(rows, function (ii, ee) {
                        //if (!ee.Base_WarehouseId) {
                        ee.Base_WarehouseId = warehouseId;
                        //}
                    })

                    $("#MaterialRequisitionDetails").jfGridSet("refreshdata", rows);
                }
            })

            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });



            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#MaterialRequisitionDetails').jfGrid({
                headData: [{
                    label: '材料编码', name: 'Code', width: 140, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '领料明细'
                },
                {
                    label: '材料名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '领料明细'
                },

                {
                    label: '规格型号', name: 'Model', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '领料明细'
                },
                {
                    label: '品牌', name: 'Brand', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '领料明细'
                },
                {
                    label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                    edit: {
                        type: 'select',
                        init: function (row, $self) {// 选中单元格后执行
                        },
                        change: function (row, index, item, oldValue, colname, headData) {
                        },
                        datatype: 'dataSource',
                        code: 'MaterialsUnit',
                        op: {
                            value: 'id',
                            text: 'name',
                            title: 'name'
                        },
                        readonly: true
                    }
                },

                {
                    label: '发出仓库', name: 'Base_WarehouseId', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    datatype: 'string', tabname: '领料明细'
                    , edit: {
                        type: 'select',
                        change: function (row, index, item, oldValue, colname, headData) {
                        },
                        datatype: 'dataSource',
                        code: 'Warehouse',
                        op: {
                            value: 'id',
                            text: 'title',
                            title: 'title'
                        }
                    }
                },
                {
                    label: '单价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'float', tabname: '领料明细', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }, { "t1": "Price", "t2": "Quantity", "to": "OutMoney", "type": "mul", "toTargets": null }], isFirst: true }
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },

                {
                    label: '实时库存', name: 'Inventory', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'float', tabname: '领料明细'
                },


                {
                    label: '领料上限', name: 'MaxQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'float', tabname: '领料明细'
                },

                {
                    label: '领料数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    datatype: 'float', tabname: '领料明细', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }, { "t1": "Price", "t2": "Quantity", "to": "OutMoney", "type": "mul", "toTargets": null }], isFirst: false }
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '领料总价', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'OutMoney', required: '0',
                    datatype: 'float', tabname: '领料明细'
                },
                //{
                //    label: '出库金额', name: 'OutMoney', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                //    datatype:'float', tabname:'领料明细'                     },
                {
                    label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '领料明细',
                    edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },

                ],
                mainId: "ID",
                bindTable: "MaterialRequisitionDetails",
                isEdit: true,
                height: 300,
                toolbarposition: "top", showadd: false, showchoose: true,
                onChooseEvent: function () {
                    var subcontractIdE = $('#Project_SubcontractId').mkselectGetEx();
                    var subcontractId = ''
                    if (subcontractIdE != null) {
                        subcontractId = subcontractIdE.ID
                    }

                    var warehouseIdE = $('#Base_WarehouseId').mkselectGetEx();
                    var warehouseId = ''
                    if (warehouseIdE != null) {
                        warehouseId = warehouseIdE.id
                    }
                    Changjie.layerForm({
                        id: "selectHeTong",
                        width: 960,
                        height: 500,
                        title: "选择材料明细(只支持材料库材料)",
                        url: top.$.rootUrl + "/SystemModule/MaterialRequisition/MaterialsListDialog?subcontractId=" + subcontractId + "&warehouseId=" + warehouseId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    var total = 0;

                                    //var cvv = $('#MaterialRequisitionDetails').jfGridGet("rowdatas");

                                    for (var i = 0; i < data.length; i++) {
                                        //var ishas = false;
                                        //$.each(cvv, function (ii, ee) {
                                        //    if (ee.Base_MaterialsId == data[i].ID) {
                                        //        ishas = true;
                                        //        return false;
                                        //    }
                                        //})

                                        //if (ishas) {
                                        //    continue;
                                        //}
                                        var row = {};

                                        row.Base_WarehouseId = warehouseId;
                                        row.Base_MaterialsId = data[i].ID;
                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Model = data[i].Model;
                                        row.Brand = data[i].BrandId;
                                        row.Price = data[i].Price;
                                        row.TotalPrice = data[i].TotalPrice;
                                        row.Inventory = data[i].Quantity;
                                        row.MaxQuantity = data[i].MaxQuantity;
                                        row.Unit = data[i].UnitId;
                                        row.Base_WarehouseId = data[i].Base_Warehouse_ID;

                                        //row.Project_SubcontractId = data[i].Project_SubcontractId;
                                        row.OutMoney = data[i].OutMoney;
                                        row.Quantity = 0;
                                        //row.ProjectId = data[i].ProjectID;
                                        var TotalPrice = parseFloat(row.Price) * row.Quantity.toFixed(Changjie.sysGlobalSettings.pointGlobal);
                                        row.TotalPrice = TotalPrice;

                                        if (row.TotalPrice)
                                            total += row.TotalPrice;

                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;

                                        rows.push(row);
                                    }

                                    //if ($("#OutMoney").val() != "")
                                    $("#OutMoney").val(total);
                                    $("#MaterialRequisitionDetails").jfGridSet("addRows", rows);

                                }
                            });
                        }
                    });
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/MaterialRequisition/GetformInfoListConnMaterials?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        $("#CreateUserName").val($("#CreateUserId").mkformselectGetText());
        $("#OperatorName").val($("#OperatorId").mkformselectGetText());

        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/MaterialRequisition/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="MaterialRequisition"]').mkGetFormData());
    postData.strmaterialRequisitionDetailsList = JSON.stringify($('#MaterialRequisitionDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
