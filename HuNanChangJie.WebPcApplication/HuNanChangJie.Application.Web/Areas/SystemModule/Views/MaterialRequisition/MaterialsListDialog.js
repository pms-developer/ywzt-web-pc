﻿

var subcontractId = request("subcontractId");
var formId = request("formId");
var warehouseId = request("warehouseId");
var refreshGirdData;

var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },

        bind: function () {

            $("#btn_Search").on("click", function () {
                page.search();
            })
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("")
                page.search();
            })

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
        },

        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/SystemModule/MaterialRequisition/GetMaterialsInfoList',
                headData: [
                    { label: "材料编码", name: "Code", width: 140, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "规格型号", name: "Model", width: 100, align: "left" },
                    {
                        label: "品牌", name: "BrandId", width: 100, align: "left",

                    },
                    //{
                    //    label: "产地", name: "Origin", width: 100, align: "left",

                    //},
                    //{
                    //    label: "计量单位", name: "UnitId", width: 100, align: "left",

                    //},
                    {
                        label: "计量单位", name: "UnitId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'MaterialsUnit',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    //{
                    //    label: "项目", name: "ProjectId", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('sourceData', {
                    //            code:  'BASE_XMLB',
                    //            key: value,
                    //            keyId: 'project_id',
                    //            callback: function (_data) {
                    //                callback(_data['projectname']);
                    //            }
                    //        });
                    //    }
                    //}, Base_Warehouse_ID
                    { label: "单价", name: "Price", width: 100, align: "left" },

                   // { label: "所属仓库", name: "Base_Warehouse_Name", width: 100, align: "left" },


                    {
                        label: "所属仓库", name: "Base_Warehouse_ID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'Warehouse',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },


                    { label: "实时库存", name: "Quantity", width: 100, datatype: 'float', align: "left" },
                    { label: "领料上限", name: "MaxQuantity", width: 100, datatype: 'float',  align: "left" },
                    { label: "限制来源", name: "LimitDesc", width: 120, align: "left" },
                    //{ label: "预算成本价", name: "BugetCost", width: 100, align: "left" },
                    //{
                    //    label: "成本科目", name: "SubjectCost", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('dataItem', {
                    //            key: value,
                    //            code: 'SubjectCost',
                    //            callback: function (_data) {
                    //                callback(_data.text);
                    //            }
                    //        });
                    //    }
                    //},

                ],
                mainId: 'ID',
                height: 362,
                isPage: true,
                isMultiselect: true
            });
            page.search();
        },


        search: function (param) {
            param = param || {};
            //if (subcontractId)
            //    param.subcontractId = subcontractId;

            if ($("#SearchValue").val() != "") {
                param.name = $("#SearchValue").val()
            }

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param), subcontractId: subcontractId, warehouseId: warehouseId });
        }
    };

    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };

    page.init();
};