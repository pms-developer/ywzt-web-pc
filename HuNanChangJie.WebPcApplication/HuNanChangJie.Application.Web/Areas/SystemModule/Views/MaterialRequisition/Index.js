﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-10-21 18:46
 * 描  述：领料单
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/MaterialRequisition/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData()
        }
        else {
            top.Changjie.alert.error(data.info);
        }
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/MaterialRequisition/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData()
        }
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            Changjie.clientdata.refresh("sourceData", { code: 'FBHTLB' })
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 950);
            $('#Project_SubcontractId').mkDataSourceSelect({ code: 'FBHTLB', value: 'id', text: 'name' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增领料单',
                    url: top.$.rootUrl + '/SystemModule/MaterialRequisition/Form?projectId=' + projectId,
                    width: 1000,
                    height: 700,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 打印
            //$('#print').on('click', function () {
            //    $('#gridtable').jqprintTable();
            //});
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/MaterialRequisition/GetPageList',
                headData: [
                    { label: "编号", name: "Code", width: 140, align: "left" },
                    {
                        label: "分包合同", name: "Project_SubcontractId", width: 180, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'FBHTLB',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    { label: "领料时间", name: "OutTIme", width: 140, align: "center" },
                    //{ label: "领料金额", name: "OutMoney", width: 100, align: "left" },
                    //{ label: "领料期间", name: "Period", width: 100, align: "left"},
                    { label: "领料人", name: "Llr", width: 80, align: "center" },
                    {
                        label: "发料人", name: "OperatorId", width: 80, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },

                    {
                        label: "工程部位", name: "Gcbw", width: 120, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'GCBW',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['engineeringpartsname']);
                                }
                            });
                        }
                    },

                    { label: "领料用途", name: "Llyt", width: 150, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }
                    ,
                    { label: "备注", name: "Remark", width: 100, align: "left" },
                ],
                mainId: 'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/MaterialRequisition/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '领料单',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/SystemModule/MaterialRequisition/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 1000,
            height: 700,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
