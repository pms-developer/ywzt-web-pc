﻿/*
 * 日 期：2017.03.22
 * 描 述：日志管理	
 */
var bootstrap = function ($, Changjie) {
    "use strict";
    var categoryId = '1';
    var logbegin = '';
    var logend = '';

    var refreshGirdData = function () {
        page.search();
    }
    var page = {
        init: function () {
            page.initleft();
            page.initGrid();
            page.bind();
        },
        bind: function () {
            $('.datetime').each(function () {
                $(this).mkdate({
                    dfdata: [
                        { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                        { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                        { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                        { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    ],
                    // 月
                    mShow: false,
                    premShow: false,
                    // 季度
                    jShow: false,
                    prejShow: false,
                    // 年
                    ysShow: false,
                    yxShow: false,
                    preyShow: false,
                    yShow: false,
                    // 默认
                    dfvalue: '1',
                    selectfn: function (begin, end) {
                        logbegin = begin;
                        logend = end;
                        page.search();
                    }
                });
            });
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 清空
            $('#removelog').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '清空',
                    url: top.$.rootUrl + '/SystemModule/Log/Form?categoryId=' + categoryId,
                    height: 200,
                    width: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 导出
            $('#export').on('click', function () {
                Changjie.layerForm({
                    id: "ExcelExportForm",
                    title: '导出Excel数据',
                    url: top.$.rootUrl + '/Utility/ExcelExportForm?gridId=gridtable&filename=log',
                    width: 500,
                    height: 380,
                    callBack: function (id) {
                        return top[id].acceptClick();
                    },
                    btn: ['导出Excel', '关闭']
                });
            });
        },
        initleft: function () {
            $('#left_list li').on('click', function () {
                var $this = $(this);
                var $parent = $this.parent();
                $parent.find('.active').removeClass('active');
                $this.addClass('active');

                categoryId = $this.attr('data-value');
                page.search();
            });
        },
        initGrid: function () {
            $('#gridtable').AgGrid({
                url: top.$.rootUrl + '/SystemModule/Log/GetPageList',
                headData: [
                    {
                        headerName: "操作时间", field: "F_OperateTime", width: 135, 
                        cellRenderer: function (cellvalue) {
                            return Changjie.formatDate(cellvalue.value, 'yyyy-MM-dd hh:mm:ss');
                        }
                    },
                    { headerName: "操作用户", field: "F_OperateAccount", width: 140 },
                    { headerName: "IP地址", field: "F_IPAddress", width: 100  },
                    { headerName: "系统功能", field: "F_Module", width: 150  },
                    { headerName: "操作类型", field: "F_OperateType", width: 100  },
                    {
                        headerName: "执行结果", field: "F_ExecuteResult", width: 100 ,
                        cellRenderer: function (cellvalue) {
                            if (cellvalue.value == '1') {
                                return "<span class=\"label label-success\">成功</span>";
                            } else {
                                return "<span class=\"label label-danger\">失败</span>";
                            }
                        }
                    },
                    { headerName: "执行结果描述", field: "F_ExecuteResultJson", width: 500}

                ],

                mainId: 'F_ItemDetailId',
                isPage: true,
                sidx: 'F_OperateTime'
            });
        },
        search: function (param) {
            param = param || {};
            param.CategoryId = categoryId;
            param.StartTime = logbegin;
            param.EndTime = logend;

            $('#gridtable').AgGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };



    page.init();
}


