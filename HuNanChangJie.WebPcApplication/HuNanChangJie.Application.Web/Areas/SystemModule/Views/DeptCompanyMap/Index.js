﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2022-01-05 16:41
 * 描  述：钉钉部门与公司的mapping关系
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/SystemModule/DeptCompanyMap/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/DeptCompanyMap/GetPageList',
                headData: [
                    { label: "DingDeptId", name: "DingDeptId", width: 100, align: "left"},
                    { label: "钉钉公司名称", name: "DingDeptName", width: 100, align: "left"},
                    { label: "NCDeptId", name: "NCDeptId", width: 100, align: "left"},
                    { label: "NC公司名称", name: "NCDeptName", width: 100, align: "left"},
                    { label: "公司Id", name: "F_CompanyId", width: 100, align: "left"},
                    { label: "公司名称", name: "F_CompanyName", width: 100, align: "left"},
                ],
                mainId:'F_Deparment_Company_MappingId',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('F_Deparment_Company_MappingId');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/DeptCompanyMap/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('F_Deparment_Company_MappingId');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/SystemModule/DeptCompanyMap/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
