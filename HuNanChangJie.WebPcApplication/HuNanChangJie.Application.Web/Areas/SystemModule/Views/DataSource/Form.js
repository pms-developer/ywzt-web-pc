﻿/*
 * 日 期：2017.04.11
 * 描 述：数据源管理	
 */
var keyValue = "";
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var selectedRow = Changjie.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#F_DbId').mkDbSelect();
        },
        initData: function () {
            if (!!selectedRow) {
                keyValue = selectedRow.F_Id;
                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DataSource/GetEntityByCode', { keyValue: selectedRow.F_Code }, function (data) {
                    $('#form').mkSetFormData(data);
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData(keyValue);
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/DataSource/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}