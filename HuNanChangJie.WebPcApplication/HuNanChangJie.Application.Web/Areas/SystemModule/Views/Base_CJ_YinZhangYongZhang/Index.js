﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-12-02 21:00
 * 描  述：用章申请
 */
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#ShenQingRen').mkUserSelect(0);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/SystemModule/Base_CJ_YinZhangYongZhang/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            //// 编辑
            //$('#edit').on('click', function () {
            //    debugger;
            //    var keyValue = $('#gridtable').jfGridValue('ID');
            //    if (Changjie.checkrow(keyValue)) {
            //        Changjie.layerForm({
            //            id: 'form',
            //            title: '编辑',
            //            url: top.$.rootUrl + '/SystemModule/Base_CJ_YinZhangYongZhang/Form?keyValue=' + keyValue,
            //            width: 800,
            //            height: 600,
            //            callBack: function (id) {
            //                return top[id].acceptClick(refreshGirdData);
            //            }
            //        });
            //    }
            //});
            //// 删除
            //$('#delete').on('click', function () {
            //    debugger;
            //    var keyValue = $('#gridtable').jfGridValue('ID');
            //    if (Changjie.checkrow(keyValue)) {
            //        Changjie.layerConfirm('是否确认删除该项！', function (res) {
            //            if (res) {
            //                Changjie.deleteForm(top.$.rootUrl + '/SystemModule/Base_CJ_YinZhangYongZhang/DeleteForm', { keyValue: keyValue}, function () {
            //                    refreshGirdData();
            //                });
            //            }
            //        });
            //    }
            //});
            
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/Base_CJ_YinZhangYongZhang/GetPageList',
                headData: [
                    { label: "申请编号", name: "BianHao", width: 100, align: "left"},
                    { label: "申请人", name: "ShenQingRen", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "申请部门", name: "ShenQingBuMeng", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('department', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "关联项目", name: "Project_ID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'BASE_XMLB',
                                 key: value,
                                 keyId: 'project_id',
                                 callback: function (_data) {
                                     callback(_data['projectname']);
                                 }
                             });
                        }},
                    { label: "申请公章", name: "JieYongZhang", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getsAsync('custmerData', {
                                 code:  'BASE_YinZhang',
                                 key: value,
                                 keyId: 'id',
                                 textId: 'mingchen',
                                 callback: function (text) {
                                     callback(text);
                                 }
                             });
                        }},
                    { label: "文件份数", name: "WenJianShu", width: 100, align: "left"},
                    { label: "用章事由", name: "ShiYou", width: 100, align: "left"},
                    { label: "备注", name: "BeiZhu", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}

// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/Base_CJ_YinZhangYongZhang/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/SystemModule/Base_CJ_YinZhangYongZhang/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId,
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};

