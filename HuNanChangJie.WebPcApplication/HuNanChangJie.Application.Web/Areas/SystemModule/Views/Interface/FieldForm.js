﻿/*
 * 日 期：2017.04.18
 * 描 述：单据编号规则	
 */
var acceptClick;
var currentColRow = top.layer_InterfaceForm.currentColRow;
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#fieldType').mkDataItemSelect({ code: 'FieldType',maxHeight:100 });
        },
        initData: function () {
            if (!!currentColRow) {
                $('#form').mkSetFormData(currentColRow);
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var data = $('#form').mkGetFormData();
        if (!!callBack) { callBack(data); }
        return true;
    };
    page.init();
}