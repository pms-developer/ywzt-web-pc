﻿/*
 * 日 期：2017.03.22
 * 描 述：功能模块	
 */
var refreshGirdData; // 更新数据
var bootstrap = function ($, Changjie) {
    "use strict";
    var moduleId = '0';
    var page = {
        init: function () {
            page.inittree();
            page.initGrid();
            page.bindEvent();
        },
        bindEvent: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ parentId: moduleId, keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '添加功能',
                    url: top.$.rootUrl + '/SystemModule/Module/Form?moduleId=' + moduleId,
                    height: 430,
                    width: 700,
                    btn: null
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_ModuleId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑功能',
                        url: top.$.rootUrl + '/SystemModule/Module/Form?keyValue=' + keyValue,
                        height: 430,
                        width: 700,
                        btn: null
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_ModuleId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/SystemModule/Module/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        inittree: function () {
            $('#module_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/Module/GetModuleTree',
                nodeClick: page.treeNodeClick
            });
        },
        treeNodeClick: function (item) {
            moduleId = item.id;
            $('#titleinfo').text(item.text);
            page.search({ parentId: moduleId });
        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/SystemModule/Module/GetModuleListByParentId',
               
                headData: [
                    
                    { headerName: "主键", field: "F_ModuleId", width: 150, hide:true},

                    { headerName: "编号", field: "F_EnCode", width: 150 },
                    { headerName: "名称", field: "F_FullName", width: 150 },
                    { headerName: "地址", field: "F_UrlAddress", width: 350 },
                    { headerName: "目标", field: "F_Target", width: 80 },
                    {
                        headerName: "菜单", field: "F_IsMenu", width: 100, 
                        cellRenderer: function (cellvalue) {
                            return cellvalue.value == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                        }
                    },
                    {
                        headerName: "展开", field: "F_AllowExpand", width: 100, 
                        cellRenderer: function (cellvalue) {
                            return cellvalue.value == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                        }
                    },
                    //{
                    //    label: "公共", name: "F_IsPublic", width: 50, align: "center",
                    //    formatter: function (cellvalue) {
                    //        return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                    //    }
                    //},
                    {
                        headerName: "有效", field: "F_EnabledMark", width: 100,
                        cellRenderer: function (cellvalue) {
                            return cellvalue.value == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                        }
                    },
                    {
                        headerName: "是否在APP中展示", field: "IsShowApp", width: 100,
                        cellRenderer: function (cellvalue) {
                            return cellvalue.value == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                        }
                    },
                    { headerName: "描述", field: "F_Description", width: 300}
                ]
            });
            page.search({ parentId: moduleId });
        },
        search: function (param) {
            $('#gridtable').AgGridSet('reload', param);
        }
    };
    // 保存数据后回调刷新
    refreshGirdData = function () {
        page.search({ parentId: moduleId });
        //$('#module_tree').mktreeSet('refresh');
    }

    page.init();
}