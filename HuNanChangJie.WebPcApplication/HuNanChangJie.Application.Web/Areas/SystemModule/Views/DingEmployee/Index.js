﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2022-01-06 18:49
 * 描  述：钉钉员工信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/SystemModule/DingEmployee/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/DingEmployee/GetPageList',
                headData: [
                    { label: "钉钉Userid", name: "userid", width: 100, align: "left"},
                    { label: "钉钉Name", name: "name", width: 100, align: "left"},
                    { label: "手机号码。", name: "mobile", width: 100, align: "left"},
                    { label: "职位", name: "title", width: 100, align: "left"},
                    { label: "员工邮箱", name: "email", width: 100, align: "left"},
                    { label: "查询本组织专属帐号时可获得昵称", name: "nickname", width: 100, align: "left"},
                    { label: "是否是部门的主管", name: "leader", width: 100, align: "left"},
                    { label: "是否为企业的老板", name: "boss", width: 100, align: "left"},
                    { label: "所属部门ID列表", name: "dept_id_list", width: 100, align: "left"},
                    { label: "员工工号", name: "job_number", width: 100, align: "left"},
                ],
                mainId:'userid',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('userid');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/DingEmployee/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('userid');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/SystemModule/DingEmployee/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
