﻿var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            page.bind();
            page.initData();
        },
        bind: function () {
            $("#uploadfile").on("click", function () {
                $("#fujian").trigger("click");
            });
            $("#fujian").change(function () {
                var reader = new FileReader();
                var AllowImgFileSize = 23000000; //上传图片最大值(单位字节)（ 2 M = 2097152 B ）超过20M上传失败
                var file = $("#fujian")[0].files[0];
                var imgUrlBase64;
                if (file) {
                    //将文件以Data URL形式读入页面
                    imgUrlBase64 = reader.readAsDataURL(file);
                    console.log(imgUrlBase64);
                    reader.onload = function (e) {
                        if (AllowImgFileSize != 0 && AllowImgFileSize < reader.result.length) {
                            Changjie.alert.error('上传失败，请上传不大于20M的图片！');
                            return;
                        } else {
                            //执行上传操作
                            
                            var imgbase64 = reader.result.split(',')[1];
                            var param = { imgbase64: imgbase64 };
                            Changjie.httpAsync("POST",top.$.rootUrl + '/SystemModule/Ocr/GetInvoiceOcrDate', param, function (result) {
                                if (result) {
                                    if (result.error_code) {
                                        Changjie.alert.error('识别失败,请选择正确的发票。错误：' + result.error_code + "  " + result.error_msg);
                                    }
                                    else {
                                        var data = result.result;
                                        $('[data-table="OcrModel"]').mkSetFormData(data);
                                        $('#gridtable').jfGridSet('refreshdata', data.item_list);
                                        $('#form_tabs_sub ul li').eq(0).trigger('click');
                                    }
                                }
                            });

                            $("#previewimg").attr("src", reader.result);
                            $('#form_tabs_sub ul li').eq(1).trigger('click');
                        }
                    }
                }
            });

            $('#gridtable').jfGrid({
                headData: [
                    {
                        label: '货物或应税劳务、服务名称', name: 'name', width: 200
                    },
                    {
                        label: '规格型号', name: 'specification', width: 100
                    },
                    {
                        label: '单位', name: 'unit', width: 100
                    },
                    {
                        label: '数量', name: 'quantity', width: 100
                    },
                    {
                        label: '单价', name: 'unit_price', width: 100
                    },
                    {
                        label: '金额', name: 'amount', width: 100
                    },
                    {
                        label: '税率', name: 'tax_rate', width: 100
                    },
                    {
                        label: '税额', name: 'tax', width: 100
                    },
                ],
                mainId: "ID",
                bindTable: "OcrModel",
                isEdit: false,
                height: 300, 
            });
        },
        initData: function () {
        },

    };
    // 保存数据
    acceptClick = function (callback) {
        var backdata = {
            maindata: $('body').mkGetFormData(),
            subdata: $('#gridtable').jfGridGet("rowdatas")
        };
        if (callback) {
            return callback(backdata);
        }
    };
    page.init();
}