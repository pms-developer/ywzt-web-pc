﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-10-22 19:48
 * 描  述：退料单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "StoreCredit";
var processCommitUrl = top.$.rootUrl + '/SystemModule/StoreCredit/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/StoreCredit/Audit', { keyValue: keyValue }, function (data) {
       
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/StoreCredit/UnAudit', { keyValue: keyValue }, function (data) {
     
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'StoreCreditDetails', "gridId": 'StoreCreditDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }

            setTimeout(function () {
                $('#MaterialRequisitionId').unbind("change").on("change", function () {
                    var rows = $('#StoreCreditDetails').jfGridGet("rowdatas");
                    if (rows && rows.length > 0) {
                        top.Changjie.alert.info('变更领料单将清除退料明细');
                        $("#StoreCreditDetails").jfGridSet("refreshdata", []);
                    }
                })
            }, 3000)
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'StoreCreditCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#MaterialRequisitionId').mkselect({});
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });

            var params = { "AuditStatus": "2" };

            if (projectId) {
                params = { "AuditStatus": "2", "ProjectID": projectId };
            }
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/MaterialRequisition/GetPageList', { queryJson: JSON.stringify(params), pagination: JSON.stringify({ "rows": 500, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                $("#MaterialRequisitionId").mkselectRefresh({
                    value: "ID",
                    text: "Code",
                    title: "Code",
                    data: data.rows
                })
            });

            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#StoreCreditDetails').jfGrid({
                headData: [
                    {
                        label: '材料编码', name: 'Code', width: 140, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '退料明细'
                    },
                    {
                        label: '材料名称', name: 'Name', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '退料明细'
                    },
                    {
                        label: '规格型号', name: 'Model', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '退料明细'
                    },
                    {
                        label: '品牌', name: 'Brand', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '退料明细'
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    {
                        label: '退料仓库', name: 'Base_WarehouseId', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '退料明细'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'Warehouse',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            }
                        }
                    },
                    {
                        label: '单价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '退料明细', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    //{
                    //    label: '领料数量', name: 'MaxQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'string', tabname: '退料明细'
                    //},
                    {
                        label: '退料数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '退料明细', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                if (row.Quantity > row.MaxQuantity) {
                                    row.Quantity = row.MaxQuantity;
                                    row.TotalPrice = row.MaxQuantity * row.Price
                                    top.Changjie.alert.error("退料数量不能大于领料数量,退料上限为【" + row.MaxQuantity + "】");


                                    $('#StoreCreditDetails').jfGridSet("refreshdata")
                                }
                                //console.log(row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData)
                                //MaxQuantity
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '退料总价', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'BackMoney', required: '0',
                        datatype: 'float', tabname: '退料明细'
                    },
                    {
                        label: '备注', name: 'Remark', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '退料明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "StoreCreditDetails",
                isEdit: true,
                height: 300,
                toolbarposition: "top", showadd: false, showchoose: true,
                onChooseEvent: function () {
                    var materialRequisitionIdE = $('#MaterialRequisitionId').mkselectGetEx();
                    if (materialRequisitionIdE == null) {
                        top.Changjie.alert.info("请选择领料单");
                        return
                    }
                    Changjie.layerForm({
                        id: "selectHeTong",
                        width: 960,
                        height: 500,
                        title: "选择退料明细",
                        url: top.$.rootUrl + "/SystemModule/StoreCredit/MaterialsListDialog?materialRequisitionId=" + materialRequisitionIdE.ID,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                console.log(data)
                                if (!!data) {
                                    var rows = [];
                                    var total = 0;

                                    //var cvv = $('#MaterialRequisitionDetails').jfGridGet("rowdatas");

                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.MaterialRequisitionDetailsId = data[i].Warranty
                                        row.Base_MaterialsId = data[i].ID;
                                        row.Base_WarehouseId = data[i].CostType;
                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Model = data[i].Model;
                                        row.Brand = data[i].BrandId;
                                        row.MaxQuantity = data[i].MaxQuantity;
                                        row.Price = data[i].Price;
                                        row.Unit = data[i].UnitId;

                                        row.TotalPrice = data[i].TotalPrice;
                                        row.Quantity = data[i].Quantity;
                                        row.Remark = '退料上限' + data[i].MaxQuantity;
                                        if (row.TotalPrice)
                                            total += row.TotalPrice;
                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;

                                        rows.push(row);
                                    }

                                    $("#StoreCreditDetails").jfGridSet("addRows", rows);
                                }
                            });
                        }
                    });
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/StoreCredit/GetformInfoListConnMaterials?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/StoreCredit/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;
    row.EditType = 2;
    $cell.html(value);
    $edit.val(value);
};
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="StoreCredit"]').mkGetFormData());
    postData.strstoreCreditDetailsList = JSON.stringify($('#StoreCreditDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
