﻿var materialRequisitionId = request("materialRequisitionId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
            
        },
        bind: function () {           
                $('#gridtable').jfGrid({
                    url: top.$.rootUrl + '/SystemModule/StoreCredit/GetMaterialsInfoList',
                    headData: [
                        { label: "材料编码", name: "Code", width: 140, align: "left" },
                        { label: "材料名称", name: "Name", width: 150, align: "left" },
                        { label: "规格型号", name: "Model", width: 100, align: "left" },
                        {
                            label: "品牌", name: "BrandId", width: 100, align: "left",
                           
                        },
                        //{
                        //    label: "产地", name: "Origin", width: 100, align: "left",
                            
                        //},
                        //{
                        //    label: "计量单位", name: "UnitId", width: 100, align: "left",
                            
                        //},


                        {
                            label: '计量单位', name: 'UnitId', width: 100, cellStyle: { 'text-align': 'left' },
                            edit: {
                                type: 'select',
                                init: function (row, $self) {// 选中单元格后执行
                                },
                                change: function (row, index, item, oldValue, colname, headData) {
                                },
                                datatype: 'dataSource',
                                code: 'MaterialsUnit',
                                op: {
                                    value: 'id',
                                    text: 'name',
                                    title: 'name'
                                },
                                readonly: true
                            }
                        },

                        //{
                        //    label: "项目", name: "ProjectId", width: 100, align: "left",
                        //    formatterAsync: function (callback, value, row, op, $cell) {
                        //        Changjie.clientdata.getAsync('sourceData', {
                        //            code:  'BASE_XMLB',
                        //            key: value,
                        //            keyId: 'project_id',
                        //            callback: function (_data) {
                        //                callback(_data['projectname']);
                        //            }
                        //        });
                        //    }
                        //},
                        { label: "单价", name: "Price", width: 100, align: "left" },
                        { label: "退料上限", name: "MaxQuantity", width: 100, align: "left" },
                        { label: "限制来源", name: "LimitDesc", width: 120, align: "left" },
                        //{ label: "预算成本价", name: "BugetCost", width: 100, align: "left" },
                        //{
                        //    label: "成本科目", name: "SubjectCost", width: 100, align: "left",
                        //    formatterAsync: function (callback, value, row, op, $cell) {
                        //        Changjie.clientdata.getAsync('dataItem', {
                        //            key: value,
                        //            code: 'SubjectCost',
                        //            callback: function (_data) {
                        //                callback(_data.text);
                        //            }
                        //        });
                        //    }
                        //},

                    ],
                    mainId: 'ID',
                    height: 432,
                    isPage:true,
                    isMultiselect:true
                });
                page.search();
        },
        search: function (param) {
            param = param || {};
            //if (subcontractId)
            //    param.subcontractId = subcontractId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param), materialRequisitionId: materialRequisitionId });
        }
    };
    page.init();
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };
};