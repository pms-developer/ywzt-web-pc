﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-10-22 19:48
 * 描  述：退料单
 */
var refreshGirdData;
var formId = request('formId');
var projectId = request('projectId');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/StoreCredit/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData()
        }
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/SystemModule/StoreCredit/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData()
        }
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            Changjie.clientdata.refresh("sourceData", { code: 'MaterialRequisitionList' })


            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#MaterialRequisitionId').mkDataItemSelect({ code: '' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增退料单',
                    url: top.$.rootUrl + '/SystemModule/StoreCredit/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });

            $('#MaterialRequisitionId').mkselect({});
            var params = { "AuditStatus": "2" };
            if (projectId) {
                params = { "AuditStatus": "2", "ProjectID": projectId };
            } 

            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/MaterialRequisition/GetPageList', { queryJson: JSON.stringify(params), pagination: JSON.stringify({ "rows": 500, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                $("#MaterialRequisitionId").mkselectRefresh({
                    value: "ID",
                    text: "Code",
                    title: "Code",
                    data: data.rows
                })
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/StoreCredit/GetPageList',
                headData: [
                    { label: "单号", name: "Code", width: 150, align: "left" },
                    {
                        label: "领料单", name: "MaterialRequisitionId", width: 180, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {

                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'MaterialRequisitionList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['code']);
                                }
                            });
                        }
                    },
                    { label: "退料时间", name: "BackTime", width: 140, align: "left" },
                    { label: "退料金额", name: "BackMoney", width: 140, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 90, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "备注", name: "Remark", width: 200, align: "left" },

                ],
                mainId: 'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/StoreCredit/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '退料单',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/SystemModule/StoreCredit/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
