﻿/* *  
 * 创建人：超级管理员
 * 日  期：2019-10-28 17:03
 * 描  述：证照名称管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var selectvalue = request('selectvalue');
var type = "add";
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData(); 
        },
        bind: function () {
            $('#fk_dataitemdetail').mkDataSourceSelect({ code: 'BASE_ZZSX', value: 'f_itemdetailid', text: 'f_itemname' });

            if (type == "add" && selectvalue) {
                $('#fk_dataitemdetail').mkselectSet(selectvalue);
            }

            $('#zzmc_isjieyong').mkRadioCheckbox({
                type: 'radio',
                code: 'ZZRXJYZT',
            });
            $('#zzmc_baoguanren').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#zzmc_oper')[0].mkvalue = Changjie.clientdata.get(['userinfo']).userId;
            $('#zzmc_oper').val(Changjie.clientdata.get(['userinfo']).realName);
        },
        initData: function () {
           
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoMingCheng/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id]);
                        }
                        else {
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        if (type == "add") {
            keyValue = mainId;
        }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData())
        };
        $.mkSaveForm(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoMingCheng/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
