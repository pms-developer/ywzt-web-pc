﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-10-28 17:03
 * 描  述：证照名称管理
 */
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 初始化左侧树形数据
            $('#dataTree').mktree({
                url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoMingCheng/GetTree',
                nodeClick: function (item) {
                   
                    page.search({ fk_dataitemdetail: item.value });
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                var selectvalue = "";
                var currentItem = $('#dataTree').mktreeSet("currentItem");

                if (currentItem != null && typeof currentItem != "undefined") {
                    selectvalue = currentItem.value;
                }
                if (selectvalue == null || typeof selectvalue == "undefined") {
                    selectvalue = "";
                }

                Changjie.layerForm({
                    id: 'form',
                    title: '新增证照名称',
                    url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoMingCheng/Form?selectvalue=' + selectvalue,
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
             
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoMingCheng/GetPageList',
                headData: [
                    { label: "证照名称", name: "zzmc_name", width: 400, align: "left"},
                  
                    { label: "保管人", name: "zzmc_baoguanren", width: 160, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }
                    }, {
                        label: "是否允许借用", name: "zzmc_isjieyong", width: 160, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ZZRXJYZT',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "排序", name: "zzmc_sort", width: 100, align: "left" },
                ],
                mainId:'zzmc_id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
           
            param = param || {};
            
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}

// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('zzmc_id');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoMingCheng/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('zzmc_id');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: '编辑证照名称',
            url: top.$.rootUrl + '/SystemModule/BASE_CJ_ZhengZhaoMingCheng/Form?keyValue=' + keyValue + "&formId=" + formId, 
            width: 600,
            height: 400,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};

