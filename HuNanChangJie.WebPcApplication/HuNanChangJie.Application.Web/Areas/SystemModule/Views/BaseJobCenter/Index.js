﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-05-07 10:01
 * 描  述：计划任务中心
 */
var refreshGirdData;
var formId = request("formId");
var jobInfo = null;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $("#Run").attr("disabled", "disabled");
            $("#Remove").attr("disabled", "disabled");
            $("#Pause").attr("disabled", "disabled");
            $("#Resume").attr("disabled", "disabled");
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/SystemModule/BaseJobCenter/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            //  启动
            $('#Run').on('click', function () {
                var url = top.$.rootUrl + '/SystemModule/BaseJobCenter/Run';
                page.runApi(url);
            });
            //  停止 
            $('#Remove').on('click', function () {
                var url = top.$.rootUrl + '/SystemModule/BaseJobCenter/Remove';
                page.runApi(url);
            });
            //  暂停
            $('#Pause').on('click', function () {
                var url = top.$.rootUrl + '/SystemModule/BaseJobCenter/Pause';
                page.runApi(url);
            });
            //  恢复
            $('#Resume').on('click', function () {
                var url = top.$.rootUrl + '/SystemModule/BaseJobCenter/Resume';
                page.runApi(url);
            });
        },
        runApi: function (url) {
            if (jobInfo) {
                Changjie.httpAsyncPost(url, jobInfo, function (result) {
                    if (result.data.Msg) {
                        Changjie.alert.error(result.data.Msg);
                    }
                    else {//成功
                        refreshGirdData();
                    }
                });
            }
            else {
                Changjie.alert.warning("请选择数据行");
            }
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/BaseJobCenter/GetPageList',
                headData: [
                    { label: "调试器编码", name: "SchedCode", width: 100, align: "left"},
                    { label: "调试器名称", name: "SchedName", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ProjectType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "作业名称", name: "JobName", width: 100, align: "left"},
                    { label: "作业组", name: "JobGroup", width: 100, align: "left"},
                    { label: "时间表达式", name: "Cron", width: 100, align: "left"},
                    { label: "间隔时间（秒）", name: "Second", width: 100, align: "left"},
                    { label: "项目组", name: "ProjectTeam", width: 100, align: "left" },
                    { label: "任务状态", name: "RunState", width: 100, align: "left" },
                    { label: "作业程序集物理路径", name: "AssemblyPath", width: 100, align: "left"},
                    { label: "作业完全限定名", name: "ClassType", width: 100, align: "left" }
                    
                ],
                mainId:'ID',
                isPage: true,
                onSelectRow: function (row) {
                    jobInfo = row;
                    var state = row.TRIGGER_STATE
                    switch (state) {
                        case "PAUSED"://暂停
                            $("#Run").attr("disabled", "disabled");
                            $("#Pause").attr("disabled", "disabled");
                            $("#delete").attr("disabled", "disabled");
                            $("#edit").attr("disabled", "disabled");
                            $("#Remove").removeAttr("disabled");
                            $("#Resume").removeAttr("disabled");
                            break;
                        case "BLOCKED"://阻塞
                        case "WAITING"://等待 
                        case "ERROR":
                            $("#Run").attr("disabled", "disabled");
                            $("#delete").attr("disabled", "disabled");
                            $("#edit").attr("disabled", "disabled");
                            $("#Remove").attr("disabled", "disabled");
                            $("#Resume").attr("disabled", "disabled");

                            $("#Pause").removeAttr("disabled");
                            break;
                        case "ACQUIRED"://正常执行
                            $("#Run").attr("disabled", "disabled");
                            $("#Pause").removeAttr("disabled");
                            $("#delete").attr("disabled", "disabled");
                            $("#edit").attr("disabled", "disabled");
                            $("#Remove").attr("disabled", "disabled");
                            $("#Resume").attr("disabled", "disabled");
                            break;
                        default:
                            $("#Run").removeAttr("disabled");
                            $("#Remove").attr("disabled", "disabled");
                            $("#Pause").attr("disabled", "disabled");
                            $("#Resume").attr("disabled", "disabled");
                            $("#delete").removeAttr("disabled");
                            $("#edit").removeAttr("disabled");
                            break;
                    }
                }
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/BaseJobCenter/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/SystemModule/BaseJobCenter/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
