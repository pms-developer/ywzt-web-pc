﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-02 14:53
 * 描  述：系统消息
 */
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/BaseMessage/GetPageList',
                headData: [
                    { label: "标题", name: "Title", width: 350, align: "left"},
                    {
                        label: "消息类型", name: "MessageType", width: 100, align: "left",
                        formatter: function (cellValue) {
                            switch (cellValue) {
                                case "Problems":
                                    return "问题反馈";
                                default:
                                    return "系统消息"
                            }
                        }
                    },
                    { label: "发送时间", name: "MessageDate", width: 200, align: "left" },
                    {
                        label: "收信人", name: "UserId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "是否已阅读", name: "IsRead", width: 100, align: "left",
                        formatter: function (cellValue) {
                            var value = cellValue || "";
                            if (value.toString() == "true") {
                                return '<span class="label label-success" style="cursor: pointer;">已读</span>';
                            }
                            return '<span class="label label-default" style="cursor: pointer;">未读</span>';
                        }
                    },
                    { label: "阅读时间", name: "ReadDate", width: 200, align: "left"},
                ],
                mainId:'ID',
                isPage: true,
                dblclick: function (row) {
                    var params = "?keyValue=" + row["InfoId"]+"&viewState=1";
                    if (row["MessageType"] == "Problems") {
                        params+="&isReply=1"
                    }
                    Changjie.layerForm({
                        id: 'viewform',
                        title: '查看',
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + "/" + row["Url"] + params,
                        width: 800,
                        height: 650 
                    });
                    Changjie.httpAsyncPost(top.$.rootUrl + "/SystemModule/BaseMessage/Readed", { keyValue: row["ID"] }, function (data) {
                        if (data.code == Changjie.httpCode.success) {
                            top.badge.setSystemMessage();
                            refreshGirdData();
                        }
                    });
                    
                }
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/SystemModule/BaseMessage/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/SystemModule/BaseMessage/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
