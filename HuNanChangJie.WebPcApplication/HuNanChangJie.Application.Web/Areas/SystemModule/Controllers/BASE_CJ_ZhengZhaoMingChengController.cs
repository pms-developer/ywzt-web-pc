﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-10-28 17:03
    /// 描 述：证照名称管理
    /// </summary>
    public class BASE_CJ_ZhengZhaoMingChengController : MvcControllerBase
    {
        private BASE_CJ_ZhengZhaoMingChengIBLL bASE_CJ_ZhengZhaoMingChengIBLL = new BASE_CJ_ZhengZhaoMingChengBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = bASE_CJ_ZhengZhaoMingChengIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var Base_CJ_ZhenZhaoMingChengData = bASE_CJ_ZhengZhaoMingChengIBLL.GetBase_CJ_ZhenZhaoMingChengEntity( keyValue );
            var jsonData = new {
                Base_CJ_ZhenZhaoMingCheng = Base_CJ_ZhenZhaoMingChengData,
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取左侧树形数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTree()
        {
            var data = bASE_CJ_ZhengZhaoMingChengIBLL.GetTree();
            return Success(data);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            bASE_CJ_ZhengZhaoMingChengIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string deleteList)
        {
            Base_CJ_ZhenZhaoMingChengEntity entity = strEntity.ToObject<Base_CJ_ZhenZhaoMingChengEntity>();
            bASE_CJ_ZhengZhaoMingChengIBLL.SaveEntity(keyValue,entity, deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
