﻿using HuNanChangJie.Application.Excel;
using System.Collections.Generic;
using System.Web.Mvc;
using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.Base.SystemModule;
using System;
using System.Drawing;
using System.Web;
using Aliyun.OSS;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Web.UI.WebControls;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using Newtonsoft.Json.Linq;
using NPOI.SS.Formula.Functions;
using MySql.Data.MySqlClient.Memcached;
using System.Net;
using System.Text;
using System.Security.Policy;
using ClosedXML.Excel;
using Spire.Xls;
using System.EnterpriseServices.Internal;
using DocumentFormat.OpenXml.Office.CustomUI;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.04.01
    /// 描 述：Excel导入管理
    /// </summary>
    public class ExcelImportController : MvcControllerBase
    {
        private ExcelImportIBLL excelImportIBLL = new ExcelImportBLL();
        private AnnexesFileIBLL annexesFileIBLL = new AnnexesFileBLL();
        #region  视图功能
        /// <summary>
        /// 导入模板管理页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 导入模板管理表单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult NewForm()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Form1()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Form2()
        {
            return View();
        }
        /// <summary>
        /// 设置字段属性
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SetFieldForm()
        {
            return View();
        }

        /// <summary>
        /// 导入页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ImportForm()
        {
            return View();
        }
        /// <summary>
        /// 上传自定义导入模板页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UploadCustomTemplateForm()
        {
            return View();
        }

        #endregion

        #region  获取数据
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = excelImportIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="moduleId">功能模块主键</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList(string moduleId)
        {
            var data = excelImportIBLL.GetList(moduleId);
            return JsonResult(data);
        }
        /// <summary>
        /// 获取表单数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            ExcelImportEntity entity = excelImportIBLL.GetEntity(keyValue);
            IEnumerable<ExcelImportFieldEntity> list = excelImportIBLL.GetFieldList(keyValue);
            if (entity.ExcelImportType == 1)
            {
                var data = new
                {
                    entity = entity,
                    list = list,

                };
                return JsonResult(data);
            }
            else
            {

                IEnumerable<ExcelImportTableSetEntity> tableSetList = excelImportIBLL.GetTableSetList(keyValue);
                var PrimaryTableName = tableSetList.FirstOrDefault(p => p.IsPrimary).TableName;
                var SublistTableName = tableSetList.Where(p => !p.IsPrimary).Select(o => o.TableName);
                #region Form1 data
                //var data = new
                //{
                //    entity = entity,
                //    primaryList = list.Where(p => p.TableName == PrimaryTableName),
                //    sublistList1 = Sublist.Count > 0 ? list.Where(p => p.TableName == Sublist[0].TableName) : list.Where(p => false),
                //    sublistList2 = Sublist.Count > 1 ? list.Where(p => p.TableName == Sublist[1].TableName) : list.Where(p => false),
                //    sublistList3 = Sublist.Count > 2 ? list.Where(p => p.TableName == Sublist[2].TableName) : list.Where(p => false),
                //    sublistList4 = Sublist.Count > 3 ? list.Where(p => p.TableName == Sublist[3].TableName) : list.Where(p => false),
                //    sublistList5 = Sublist.Count > 4 ? list.Where(p => p.TableName == Sublist[4].TableName) : list.Where(p => false),

                //    tableSetList = tableSetList,
                //    primaryEntity = tableSetList.FirstOrDefault(p => p.TableName == PrimaryTableName),
                //    sublistEntity1 = Sublist.Count > 0 ? tableSetList.FirstOrDefault(p => p.TableName == Sublist[0].TableName) : null,
                //    sublistEntity2 = Sublist.Count > 1 ? tableSetList.FirstOrDefault(p => p.TableName == Sublist[1].TableName) : null,
                //    sublistEntity3 = Sublist.Count > 2 ? tableSetList.FirstOrDefault(p => p.TableName == Sublist[2].TableName) : null,
                //    sublistEntity4 = Sublist.Count > 3 ? tableSetList.FirstOrDefault(p => p.TableName == Sublist[3].TableName) : null,
                //    sublistEntity5 = Sublist.Count > 4 ? tableSetList.FirstOrDefault(p => p.TableName == Sublist[4].TableName) : null,
                //};
                #endregion

                var data = new
                {
                    entity = entity,
                    tableSetList = tableSetList,
                    primaryList = list.Where(p => p.TableName == PrimaryTableName),
                    sublistList = list.Where(p => SublistTableName.Contains(p.TableName))
                };
                return JsonResult(data);
            }

        }
        #endregion

        #region  提交数据
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[AjaxOnly]
        //public ActionResult SaveForm(string keyValue, string strEntity, string strList)
        //{
        //    ExcelImportEntity entity = strEntity.ToObject<ExcelImportEntity>();
        //    List<ExcelImportFieldEntity> filedList = strList.ToObject<List<ExcelImportFieldEntity>>();
        //    excelImportIBLL.SaveEntity(keyValue, entity, filedList);
        //    return Success("保存成功！");
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[AjaxOnly]
        //public ActionResult SaveForm(string keyValue, string strEntity, string strTableSetList, string strList,string strPrimaryList, string strSublistList1, string strSublistList2, string strSublistList3, string strSublistList4, string strSublistList5)
        //{
        //    ExcelImportEntity entity = strEntity.ToObject<ExcelImportEntity>();
        //    List<ExcelImportTableSetEntity> tableSetList = new List<ExcelImportTableSetEntity>();
        //    List<ExcelImportFieldEntity> filedList = new List<ExcelImportFieldEntity>();
        //    if (entity.ExcelImportType == 1)
        //    {
        //        if (!string.IsNullOrEmpty(strList))
        //        {
        //            filedList = strList.ToObject<List<ExcelImportFieldEntity>>();
        //        }
        //    }
        //    else
        //    {
        //        entity.F_DbTable = "";
        //        tableSetList = strTableSetList.ToObject<List<ExcelImportTableSetEntity>>();
        //        if (!string.IsNullOrEmpty(strPrimaryList))
        //        {
        //            List<ExcelImportFieldEntity> filedPrimaryList = strPrimaryList.ToObject<List<ExcelImportFieldEntity>>();
        //            filedList.AddRange(filedPrimaryList);
        //        }
        //        if (!string.IsNullOrEmpty(strSublistList1))
        //        {
        //            List<ExcelImportFieldEntity> filedList1 = strSublistList1.ToObject<List<ExcelImportFieldEntity>>();
        //            filedList.AddRange(filedList1);
        //        }
        //        if (!string.IsNullOrEmpty(strSublistList2))
        //        {
        //            List<ExcelImportFieldEntity> filedList2 = strSublistList2.ToObject<List<ExcelImportFieldEntity>>();
        //            filedList.AddRange(filedList2);
        //        }
        //        if (!string.IsNullOrEmpty(strSublistList3))
        //        {
        //            List<ExcelImportFieldEntity> filedList3 = strSublistList3.ToObject<List<ExcelImportFieldEntity>>();
        //            filedList.AddRange(filedList3);
        //        }
        //        if (!string.IsNullOrEmpty(strSublistList4))
        //        {
        //            List<ExcelImportFieldEntity> filedList4 = strSublistList4.ToObject<List<ExcelImportFieldEntity>>();
        //            filedList.AddRange(filedList4);
        //        }
        //        if (!string.IsNullOrEmpty(strSublistList5))
        //        {
        //            List<ExcelImportFieldEntity> filedList5 = strSublistList5.ToObject<List<ExcelImportFieldEntity>>();
        //            filedList.AddRange(filedList5);
        //        }
        //    }
        //    excelImportIBLL.SaveEntity(keyValue, entity, tableSetList, filedList);
        //    return Success("保存成功！");
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity, string strTableSetList, string strList, string strPrimaryList, string strSublistList)
        {
            ExcelImportEntity entity = strEntity.ToObject<ExcelImportEntity>();
            List<ExcelImportTableSetEntity> tableSetList = new List<ExcelImportTableSetEntity>();
            List<ExcelImportFieldEntity> filedList = new List<ExcelImportFieldEntity>();
            if (entity.ExcelImportType == 1)
            {
                if (!string.IsNullOrEmpty(strList))
                {
                    filedList = strList.ToObject<List<ExcelImportFieldEntity>>();
                }
            }
            else
            {
                entity.F_DbTable = "";
                tableSetList = strTableSetList.ToObject<List<ExcelImportTableSetEntity>>();
                if (!string.IsNullOrEmpty(strPrimaryList))
                {
                    List<ExcelImportFieldEntity> filedPrimaryList = strPrimaryList.ToObject<List<ExcelImportFieldEntity>>();
                    filedList.AddRange(filedPrimaryList);
                }
                if (!string.IsNullOrEmpty(strSublistList))
                {
                    List<ExcelImportFieldEntity> filedSublistList = strSublistList.ToObject<List<ExcelImportFieldEntity>>();
                    filedList.AddRange(filedSublistList);
                }
            }
            excelImportIBLL.SaveEntity(keyValue, entity, tableSetList, filedList);
            return Success("保存成功！");
        }
        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            excelImportIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 更新表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="entity">实体数据</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult UpdateForm(string keyValue, ExcelImportEntity entity)
        {
            excelImportIBLL.UpdateEntity(keyValue, entity);
            return Success("操作成功！");
        }
        #endregion

        #region  扩展方法
        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="fileId">文件id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DownSchemeFile(string keyValue)
        {
            ExcelImportEntity templateInfo = excelImportIBLL.GetEntity(keyValue);
            IEnumerable<ExcelImportFieldEntity> fileds = excelImportIBLL.GetFieldList(keyValue);

            //设置导出格式
            ExcelConfig excelconfig = new ExcelConfig();
            excelconfig.FileName = Server.UrlDecode(templateInfo.F_Name) + ".xls";
            excelconfig.IsAllSizeColumn = true;
            excelconfig.ColumnEntity = new List<ColumnModel>();
            //表头
            DataTable dt = new DataTable();
            foreach (var col in fileds)
            {
                if (col.F_RelationType != 1 && col.F_RelationType != 4 && col.F_RelationType != 5 && col.F_RelationType != 6 && col.F_RelationType != 7 && col.F_RelationType != 8 && col.F_RelationType != 9 && col.F_RelationType != 10)
                {
                    excelconfig.ColumnEntity.Add(new ColumnModel()
                    {
                        Column = col.F_Name,
                        ExcelColumn = col.F_ColName,
                        Alignment = "center",
                    });
                    dt.Columns.Add(col.F_Name, typeof(string));
                }
            }
            ExcelHelper.ExcelDownload(dt, excelconfig);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DownSchemeFile1(string keyValue)
        {
            ExcelImportEntity templateInfo = excelImportIBLL.GetEntity(keyValue);
            IEnumerable<ExcelImportFieldEntity> fileds = excelImportIBLL.GetFieldList(keyValue);

            if (templateInfo.ExcelImportType == 1)
            {
                //设置导出格式
                ExcelConfig excelconfig = new ExcelConfig();
                excelconfig.FileName = Server.UrlDecode(templateInfo.F_Name) + ".xls";
                excelconfig.IsAllSizeColumn = true;
                excelconfig.ColumnEntity = new List<ColumnModel>();
                //表头
                DataTable dt = new DataTable();
                foreach (var col in fileds)
                {
                    if (col.F_RelationType != 1 && col.F_RelationType != 4 && col.F_RelationType != 5 && col.F_RelationType != 6 && col.F_RelationType != 7 && col.F_RelationType != 8 && col.F_RelationType != 9 && col.F_RelationType != 10 && col.F_RelationType != 12)
                    {
                        excelconfig.ColumnEntity.Add(new ColumnModel()
                        {
                            Column = col.F_Name,
                            ExcelColumn = col.F_ColName,
                            Alignment = "center",
                        });
                        dt.Columns.Add(col.F_Name, typeof(string));
                    }
                }
                ExcelHelper.ExcelDownload(dt, excelconfig);
            }
            else
            {
                //设置导出格式
                ExcelConfigNew excelconfig = new ExcelConfigNew();
                excelconfig.FileName = Server.UrlDecode(templateInfo.F_Name) + ".xls";
                excelconfig.IsAllSizeColumn = true;
                excelconfig.ColumnEntity = new List<ColumnModelNew>();

                IEnumerable<ExcelImportTableSetEntity> tableSets = excelImportIBLL.GetTableSetList(keyValue);
                Dictionary<string, string> primaryList = new Dictionary<string, string>();
                Dictionary<string, DataTable> dictionarySubList = new Dictionary<string, DataTable>();
                foreach (var tableSet in tableSets)
                {
                    if (tableSet.IsPrimary)
                    {
                        foreach (var col in fileds.Where(p => p.TableName == tableSet.TableName))
                        {
                            if (col.F_RelationType != 1 && col.F_RelationType != 4 && col.F_RelationType != 5 && col.F_RelationType != 6 && col.F_RelationType != 7 && col.F_RelationType != 8 && col.F_RelationType != 9 && col.F_RelationType != 10 && col.F_RelationType != 12)
                            {
                                excelconfig.ColumnEntity.Add(new ColumnModelNew()
                                {
                                    Column = col.F_Name,
                                    ExcelColumn = col.F_ColName,
                                    Alignment = "center",
                                    TableName = tableSet.TableName,
                                });
                                primaryList.Add(col.F_Name, "");
                            }
                        }
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        foreach (var col in fileds.Where(p => p.TableName == tableSet.TableName))
                        {
                            if (col.F_RelationType != 1 && col.F_RelationType != 4 && col.F_RelationType != 5 && col.F_RelationType != 6 && col.F_RelationType != 7 && col.F_RelationType != 8 && col.F_RelationType != 9 && col.F_RelationType != 10 && col.F_RelationType != 12)
                            {
                                excelconfig.ColumnEntity.Add(new ColumnModelNew()
                                {
                                    Column = col.F_Name,
                                    ExcelColumn = col.F_ColName,
                                    Alignment = "center",
                                    TableName = tableSet.TableName,
                                });
                                dt.Columns.Add(col.F_Name, typeof(string));
                            }
                        }
                        dictionarySubList.Add(tableSet.TableName, dt);
                    }
                }

                ExcelHelperNew.ExcelDownload(primaryList, dictionarySubList, tableSets.ToList(), excelconfig, null);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DownSchemeFileNew(string keyValue)
        {
            ExcelImportEntity templateInfo = excelImportIBLL.GetEntity(keyValue);
            if (!string.IsNullOrEmpty(templateInfo.CustomTemplate))
            {
                //以字符流的形式下载文件
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(templateInfo.CustomTemplate);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                List<byte> btlst = new List<byte>();
                int b = responseStream.ReadByte();
                while (b > -1)
                {
                    btlst.Add((byte)b);
                    b = responseStream.ReadByte();
                }
                byte[] bytes = btlst.ToArray();

                Response.ContentType = "application/ms-excel";
                Response.ContentEncoding = Encoding.UTF8;
                Response.Charset = "";
                Response.AppendHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(templateInfo.F_Name + ".xls", Encoding.UTF8));
                Response.BinaryWrite(bytes);
                Response.End();
            }
            else
            {
                DownSchemeFile1(keyValue);
            }
        }

        /// <summary>
        /// excel文件导入（通用）
        /// </summary>
        /// <param name="templateId">模板Id</param>
        /// <param name="fileId">文件主键</param>
        /// <param name="chunks">分片数</param>
        /// <param name="ext">文件扩展名</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExecuteImportExcel(string templateId, string fileId, int chunks, string ext, string projectId, string mainTableId)
        {
            CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
            UserInfo userInfo = LoginUserInfo.Get();
            string path = annexesFileIBLL.SaveAnnexes(fileId, fileId + "." + ext, chunks, userInfo);
            if (!string.IsNullOrEmpty(path))
            {
                DataTable dt = new DataTable();
                if (ext == "xls" || ext == "xlsx")
                {
                    dt = ExcelHelper.ExcelImport(path);
                }
                else if (ext == "zip")
                {
                    #region 解压缩
                    var extractPath = path.Replace(".zip", "");
                    Unzip(path, extractPath);
                    #endregion
                    // 获取文件夹内所有文件的路径
                    string[] filePaths = Directory.GetFiles(extractPath);

                    foreach (string filePath in filePaths)
                    {
                        FileInfo fileInfo = new FileInfo(filePath);
                        #region 获取excel数据
                        if (fileInfo.Name.IndexOf(".xls") > -1 || fileInfo.Name.IndexOf(".xlsx") > -1)
                        {
                            dt = ExcelHelper.ExcelImport(fileInfo.FullName);
                            break;
                        }

                        #endregion

                    }

                    if (dt != null)
                    {
                        string columnName = "";

                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            if (dt.Columns[i].ColumnName.ToUpper().IndexOf("URL") > -1)
                            {
                                columnName = dt.Columns[i].ColumnName;
                            }
                        }

                        if (!string.IsNullOrEmpty(columnName))
                        {
                            foreach (string filePath in filePaths)
                            {
                                FileInfo fileInfo = new FileInfo(filePath);
                                var queryRow = dt.Select($" {columnName}='{fileInfo.Name}'").FirstOrDefault();
                                if (queryRow != null)
                                {
                                    #region 上传商品图片
                                    var extension = System.IO.Path.GetExtension(fileInfo.Name);

                                    string fullFileName = Guid.NewGuid().ToString() + extension;
                                    // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
                                    var endpoint = "https://oss-cn-shenzhen.aliyuncs.com";
                                    // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
                                    var accessKeyId = "LTAI5tQnAwRcEBzn3gkTFR7u";
                                    var accessKeySecret = "4jgxSlJNgCo6kdSL5t4bMSYPkATui0";
                                    // 填写Bucket名称，例如examplebucket。
                                    var bucketName = "meio-wms";
                                    // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
                                    string date = DateTime.Now.ToString("yyyy-MM-dd");
                                    var objectName = "erp/" + date + "/" + fullFileName;
                                    // 创建OssClient实例。
                                    var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
                                    try
                                    {
                                        // 上传文件。
                                        FileStream fileStream = new FileStream(fileInfo.FullName, FileMode.OpenOrCreate);
                                        client.PutObject(bucketName, objectName, fileStream);
                                        // return Success("https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName);
                                        fullFileName = "https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName;

                                        //return Success(fullFileName);
                                        dt.Rows[dt.Rows.IndexOf(queryRow)][columnName] = fullFileName;
                                    }
                                    catch (Exception e)
                                    {
                                        //return Fail("上传文件异常：" + e.Message);
                                    }
                                    #endregion


                                }

                            }
                        }
                    }

                    //ExcelImportEntity templateInfo = excelImportIBLL.GetEntity(templateId);
                    //string tableName = templateInfo.F_DbTable;
                    //switch (tableName)
                    //{
                    //    case "renyuan_xinxi"://人员信息导入
                    //        string datacode = codeRuleIBLL.GetBillCode("renshi_renyuanbianhao");
                    //        break;
                    //    case "Base_Supplier"://供应商导入
                    //        string coderule = "CJ_BASE_0017";
                    //        for (var i = 0; i < dt.Rows.Count; i++)
                    //        {
                    //            string code = codeRuleIBLL.GetBillCode(coderule);
                    //            dt.Rows[i][0] = code;
                    //            new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed(coderule);
                    //        }
                    //        break;
                    //}

                }

                string res = excelImportIBLL.ImportTable(templateId, fileId, dt, projectId, mainTableId);
                var data = new
                {
                    Success = res.Split('|')[0],
                    Fail = res.Split('|')[1]
                };
                return JsonResult(data);
            }
            else
            {
                return Fail("导入数据失败!");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExecuteImportExcelNew(string templateId, string fileId, int chunks, string ext, string projectId, string mainTableId,string excelImportData)
        {
            CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
            UserInfo userInfo = LoginUserInfo.Get();
            string path = annexesFileIBLL.SaveAnnexes(fileId, fileId + "." + ext, chunks, userInfo);
            if (!string.IsNullOrEmpty(path))
            {
                var excelImportEntity = excelImportIBLL.GetEntity(templateId);
                var tableSetList = excelImportIBLL.GetTableSetList(templateId);

                if (ext == "xls" || ext == "xlsx")
                {
                    #region 去掉excel中公式  获取excel中图片
                    //创建Workbook实例
                    Workbook workbook = new Workbook();
                    //加载Excel文档
                    workbook.LoadFromFile(path);

                    //遍历文档中所有工作表，找到其中所有包含公式的单元格，获取单元格中公式的数据，然后清除单元格中的内容，最后将数据再次赋值给单元格
                    foreach (Worksheet sheet in workbook.Worksheets)
                    {
                        foreach (CellRange cell in sheet.Range)
                        {
                            if (cell.HasFormula)
                            {
                                Object value = cell.FormulaValue;
                                cell.Clear(ExcelClearOptions.ClearContent);
                                cell.Value2 = value;
                            }

                            if (cell.HasPictures)
                            {
                                for (int i = 0; i < sheet.Pictures.Count; i++)
                                {
                                    ExcelPicture picture = sheet.Pictures[i];
                                    if (picture.TopRow == cell.LastRow && picture.LeftColumn == cell.LastColumn)
                                    {
                                        //保存图片
                                        var imageName = Guid.NewGuid() + ".png";
                                        var path1 = Server.MapPath("~/ExcelImportImage/" + imageName);
                                        picture.Picture.Save(path1);
                                        //System.Diagnostics.Process.Start(path1);
                                        cell.Clear(ExcelClearOptions.ClearContent);
                                        cell.Value2 = path1;
                                        break;
                                    }
                                }

                            }
                        }


                    }

                    //保存文档
                    workbook.SaveToFile(path);//, ExcelVersion.Version2013
                    workbook.Dispose();
                    #endregion

                    if (excelImportEntity.ExcelImportType == 1)
                    {
                        DataTable dt = ExcelHelperNew.ExcelImport(path);
                        string res = excelImportIBLL.ImportTable(templateId, fileId, dt, projectId, mainTableId);
                        var data = new
                        {
                            Success = res.Split('|')[0],
                            Fail = res.Split('|')[1]
                        };
                        return JsonResult(data);
                    }
                    else
                    {
                        Dictionary<string, DataTable> dtList = new Dictionary<string, DataTable>();
                        Dictionary<string, string> dictionary = new Dictionary<string, string>();

                        int startRow = 0;//子表开始读取行
                        foreach (var tableSet in tableSetList)
                        {
                            if (tableSet.IsPrimary)
                            {
                                var startcell = tableSet.StartCell.Value > 0 ? tableSet.StartCell.Value - 1 : 0;
                                var endcell = tableSet.EndCell.Value > 0 ? tableSet.EndCell.Value - 1 : 0;
                                var attributesNumber = tableSet.AttributesNumber.HasValue ? tableSet.AttributesNumber.Value : 1;
                                var startcolumn = tableSet.StartColumn.Value > 0 ? tableSet.StartColumn.Value - 1 : 0;
                                var skiprow = tableSet.SkipRow;
                                dictionary = ExcelHelperNew.ExcelImport(path, tableSet.DataReadMode.Value, startcell, endcell, attributesNumber, startcolumn,skiprow);
                                startRow += endcell;
                            }
                            else
                            {
                                startRow += tableSet.StartRow.Value;
                                DataTable dt = ExcelHelperNew.ExcelImport(path, startRow + 1, tableSet.EndRow.Value);
                                dtList.Add(tableSet.TableName, dt);
                                startRow += dt.Rows.Count;
                            }
                        }
                        var primaryTableName = tableSetList.FirstOrDefault(p => p.IsPrimary).TableName;
                        var batchId=Guid.NewGuid().ToString();
                        var importData = excelImportData.ToObject<Dictionary<string, string>>();
                        string res = excelImportIBLL.ImportTableNew(templateId, fileId, dtList, dictionary, primaryTableName, tableSetList.ToList(), projectId, mainTableId, batchId);
                        var data = new
                        {
                            Success = res.Split('|')[0],
                            Fail = res.Split('|')[1],
                            BatchID=batchId,
                            CallBackUrl= excelImportEntity.CallBackUrl
                        };
                        return JsonResult(data);
                    }
                }
                else
                {
                    return Fail("导入失败，导入文件不是excel");
                }
            }
            else
            {
                return Fail("导入数据失败!");
            }
        }

        /// <summary>
        /// 下载文件(导入文件未被导入的数据)
        /// </summary>
        /// <param name="fileId">文件id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DownImportErrorFile(string fileId, string fileName)
        {
            //设置导出格式
            ExcelConfig excelconfig = new ExcelConfig();
            excelconfig.FileName = Server.UrlDecode("未导入错误数据【" + fileName + "】") + ".xls";
            excelconfig.IsAllSizeColumn = true;
            excelconfig.ColumnEntity = new List<ColumnModel>();
            //表头
            DataTable dt = excelImportIBLL.GetImportError(fileId);
            foreach (DataColumn col in dt.Columns)
            {
                if (col.ColumnName == "导入错误")
                {
                    excelconfig.ColumnEntity.Add(new ColumnModel()
                    {
                        Column = col.ColumnName,
                        ExcelColumn = col.ColumnName,
                        Alignment = "center",
                        Background = Color.Red
                    });
                }
                else
                {
                    excelconfig.ColumnEntity.Add(new ColumnModel()
                    {
                        Column = col.ColumnName,
                        ExcelColumn = col.ColumnName,
                        Alignment = "center",
                    });
                }
            }
            ExcelHelper.ExcelDownload(dt, excelconfig);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DownImportErrorFileNew(string fileId, string fileName, int excelImportType = 1)
        {

            if (excelImportType == 2)
            {
                //设置导出格式
                ExcelConfigNew excelconfig = new ExcelConfigNew();
                excelconfig.FileName = Server.UrlDecode("未导入错误数据【" + fileName + "】") + ".xls";
                excelconfig.IsAllSizeColumn = true;
                excelconfig.ColumnEntity = new List<ColumnModelNew>();

                var importerror = excelImportIBLL.GetImportErrorNew(fileId);
                var primaryError = (Dictionary<string, string>)importerror["PrimaryError"];
                var primaryList = (Dictionary<string, string>)importerror["PrimaryList"];
                var subList = (Dictionary<string, DataTable>)importerror["SubList"];
                var tableSetList = (List<ExcelImportTableSetEntity>)importerror["TableSetList"];

                foreach (var primary in primaryList)
                {
                    if (primaryError.Any(p => p.Key == primary.Key))
                    {
                        excelconfig.ColumnEntity.Add(new ColumnModelNew()
                        {
                            Column = primary.Key,
                            ExcelColumn = primary.Key,
                            Alignment = "center",
                            Background = Color.Red,
                            TableName = tableSetList.FirstOrDefault(p => p.IsPrimary).TableName
                        });
                    }
                    else
                    {
                        excelconfig.ColumnEntity.Add(new ColumnModelNew()
                        {
                            Column = primary.Key,
                            ExcelColumn = primary.Key,
                            Alignment = "center",
                            TableName = tableSetList.FirstOrDefault(p => p.IsPrimary).TableName
                        });
                    }
                }

                foreach (var sub in subList)
                {
                    foreach (DataColumn col in sub.Value.Columns)
                    {
                        if (col.ColumnName == "导入错误")
                        {
                            excelconfig.ColumnEntity.Add(new ColumnModelNew()
                            {
                                Column = col.ColumnName,
                                ExcelColumn = col.ColumnName,
                                Alignment = "center",
                                Background = Color.Red,
                                TableName = sub.Key
                            });
                        }
                        else
                        {
                            excelconfig.ColumnEntity.Add(new ColumnModelNew()
                            {
                                Column = col.ColumnName,
                                ExcelColumn = col.ColumnName,
                                Alignment = "center",
                                TableName = sub.Key
                            });
                        }
                    }
                }
                ExcelHelperNew.ExcelDownload(primaryList, subList, tableSetList, excelconfig, primaryError);
            }
            else
            {
                //设置导出格式
                ExcelConfig excelconfig = new ExcelConfig();
                excelconfig.FileName = Server.UrlDecode("未导入错误数据【" + fileName + "】") + ".xls";
                excelconfig.IsAllSizeColumn = true;
                excelconfig.ColumnEntity = new List<ColumnModel>();
                //表头
                DataTable dt = excelImportIBLL.GetImportError(fileId);
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName == "导入错误")
                    {
                        excelconfig.ColumnEntity.Add(new ColumnModel()
                        {
                            Column = col.ColumnName,
                            ExcelColumn = col.ColumnName,
                            Alignment = "center",
                            Background = Color.Red
                        });
                    }
                    else
                    {
                        excelconfig.ColumnEntity.Add(new ColumnModel()
                        {
                            Column = col.ColumnName,
                            ExcelColumn = col.ColumnName,
                            Alignment = "center",
                        });
                    }
                }
                ExcelHelper.ExcelDownload(dt, excelconfig);
            }
        }
        /// <summary>
        /// 解压缩文件
        /// </summary>
        /// <param name="zipFilePath"></param>
        /// <param name="extractPath"></param>
        public void Unzip(string zipFilePath, string extractPath)
        {
            using (var zipInputStream = new ZipInputStream(System.IO.File.OpenRead(zipFilePath)))
            {
                ZipEntry zipEntry;
                while ((zipEntry = zipInputStream.GetNextEntry()) != null)
                {
                    if (zipEntry.IsFile)
                    {
                        // 创建完整的文件路径
                        string fullPath = Path.Combine(extractPath, zipEntry.Name);
                        string directoryName = Path.GetDirectoryName(fullPath);
                        if (directoryName.Length > 0)
                        {
                            Directory.CreateDirectory(directoryName);
                        }

                        using (var zipStreamWriter = System.IO.File.Create(fullPath))
                        {
                            int size;
                            byte[] buffer = new byte[4096];
                            while ((size = zipInputStream.Read(buffer, 0, buffer.Length)) != 0)
                            {
                                zipStreamWriter.Write(buffer, 0, size);
                            }
                        }
                    }
                }
            }
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UploadFile(HttpPostedFileBase file, string keyValue)
        {
            #region 上传自定义导入模板
            // 获取上传文件的扩展名
            if (file == null)
            {
                return Success("");
            }
            var extension = System.IO.Path.GetExtension(file.FileName);
            if (extension.Contains("xls"))
            {

            }
            else
            {
                return Fail("不支持该格式");
            }
            long timestamp = ((DateTimeOffset)System.DateTime.Now).ToUnixTimeSeconds();
            string fullFileName = timestamp + extension;
            // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
            var endpoint = "https://oss-cn-shenzhen.aliyuncs.com";
            // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
            var accessKeyId = "LTAI5tQnAwRcEBzn3gkTFR7u";
            var accessKeySecret = "4jgxSlJNgCo6kdSL5t4bMSYPkATui0";
            // 填写Bucket名称，例如examplebucket。
            var bucketName = "meio-wms";
            // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
            string dt = System.DateTime.Now.ToString("yyyy-MM-dd");
            var objectName = "erp/" + dt + "/" + fullFileName;
            // 创建OssClient实例。
            var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
            try
            {
                // 上传文件。
                client.PutObject(bucketName, objectName, file.InputStream);
                // return Success("https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName);
                fullFileName = "https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName;

                excelImportIBLL.UpdateCustomTemplate(keyValue, fullFileName);
                return Success("上传成功");

            }
            catch (Exception e)
            {
                return Fail("上传文件异常：" + e.Message);
            }
            #endregion
        }

        [HttpPost]
        public ActionResult DeleteCustomTemplate(string keyValue)
        {
            try
            {
                excelImportIBLL.UpdateCustomTemplate(keyValue, "");
                return Success("删除成功");
            }
            catch (Exception e)
            {
                return Fail("删除失败，异常：" + e.Message);
            }
        }
        #endregion
    }
}