﻿using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Util;
using System;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.04.01
    /// 描 述：自定义查询
    /// </summary>
    public class CustmerQueryController : MvcControllerBase
    {
        CustmerQueryIBLL custmerQueryBLL = new CustmerQueryBLL();

        #region  视图功能
        /// <summary>
        /// 自定义查询公共界面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 自定义查询公共界面(表单)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        /// <summary>
        /// 条件添加和编辑
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult QueryForm() {
            return View();
        }
        #endregion

        #region  获取数据
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">关键字</param>
        /// <returns></returns>
        public ActionResult GetPageList(string pagination, string keyword)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = custmerQueryBLL.GetPageList(paginationobj, keyword);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }

        /// <summary>
        /// 获取DataTable
        /// </summary>
        /// <param name="tableName">数据库表名</param>
        /// <param name="pkFiled">主键名</param>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpGet,AjaxOnly]
        public ActionResult GetDataTable(string tableName, string pkFiled, string keyValue)
        {
            var data = custmerQueryBLL.GetDataTable(tableName, pkFiled, keyValue);
            return JsonResult(data);
        }

        public ActionResult GetDataTableBySql(string sql) {
            var data = custmerQueryBLL.GetDataTableBySql(sql);
            return JsonResult(data);
        }
        #endregion

        #region  提交数据
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, CustmerQueryEntity entity)
        {
            custmerQueryBLL.SaveEntity(keyValue, entity);
            return Success("保存成功！");
        }

        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            custmerQueryBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        #endregion
    }
}