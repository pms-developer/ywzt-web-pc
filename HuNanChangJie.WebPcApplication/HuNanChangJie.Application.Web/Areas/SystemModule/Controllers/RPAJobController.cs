﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;
using FastJSON;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 14:27
    /// 描 述：RPA作业
    /// </summary>
    public class RPAJobController : MvcControllerBase
    {
        private RPAJobIBLL rPAJobIBLL = new RPAJobBLL();

        private RpaDyPromotedaccountsIBLL rpaDyAaccountsIBLL = new RpaDyPromotedaccountsBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        /// <summary>
        /// 抖音账户页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult dyAccountForm()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = rPAJobIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var rpa_jobsData = rPAJobIBLL.Getrpa_jobsEntity( keyValue );
            var rpa_joblogData = rPAJobIBLL.Getrpa_joblogList( rpa_jobsData.ID );
            var jsonData = new {
                rpa_jobs = rpa_jobsData,
                rpa_joblog = rpa_joblogData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            rPAJobIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strrpa_joblogList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<rpa_jobsEntity>();
            strrpa_joblogList = "[]";
            deleteList= "[]";
            var rpa_joblogList = strrpa_joblogList.ToObject<List<rpa_joblogEntity>>();
            rPAJobIBLL.SaveEntity(keyValue,mainInfo,rpa_joblogList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

        #region musen 获取抖音账户
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetDyAccountList(string keyValue)
        {
            var rpa_jobsData = rPAJobIBLL.Getrpa_jobsEntity(keyValue);
            var queryJson = "{'base_shopid':'" + rpa_jobsData.Base_ShopInfoID + "'}";
            var rpa_dy_accountsData = rpaDyAaccountsIBLL.GetList(queryJson);            
            
            return Success(rpa_dy_accountsData);
        }
        #endregion

    }
}
