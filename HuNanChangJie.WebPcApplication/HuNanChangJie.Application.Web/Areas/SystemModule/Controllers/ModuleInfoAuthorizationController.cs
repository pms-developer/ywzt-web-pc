﻿using HuNanChangJie.Application.Base.ModuleInfoAuthorization;
using HuNanChangJie.Application.Base.SystemModule.ModuleInfoAuthorization;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    public class ModuleInfoAuthorizationController : MvcControllerBase
    {
        private IModuleInfoAuthorizationBLL bll = new ModuleInfoAuthorizationBLL();
        // GET: SystemModule/ModuleInfoAuthorization
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList(string moduleInfoId,string moduleType)
        {
            var type = (ModuleType)Enum.Parse(typeof(ModuleType), moduleType);
            var data = bll.GetList(moduleInfoId, type);
            return JsonResult(data);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult Save(string moduleInfoId, string moduleType, string formData)
        {
            var type = (ModuleType)Enum.Parse(typeof(ModuleType), moduleType);
            var list = formData.ToObject<List<ModuleInfoAuthorizationEntity>>();
            bll.Save(moduleInfoId, type, list);
            return Success("保存成功");
        }
    }
}