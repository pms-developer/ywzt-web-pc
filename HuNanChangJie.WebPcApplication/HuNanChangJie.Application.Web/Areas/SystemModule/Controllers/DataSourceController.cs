﻿using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.04.01
    /// 描 述：数据源管理
    /// </summary>
    public class DataSourceController : MvcControllerBase
    {
        DataSourceIBLL dataSourceIBLL = new DataSourceBLL();

        private ICache cache = CacheFactory.CaChe();
        #region  获取视图
        /// <summary>
        /// 管理页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        /// <summary>
        /// 测试页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TestForm()
        {
            return View();
        }
        /// <summary>
        /// 选择页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SelectForm()
        {
            return View();
        }
        #endregion

        #region  获取数据
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">关键字</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string keyword)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = dataSourceIBLL.GetPageList(paginationobj, keyword);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
        /// <summary>
        /// 获取所有数据源数据列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList()
        {
            var data = dataSourceIBLL.GetList();
            return JsonResult(data);
        }
        /// <summary>
        /// 获取所有数据源实体根据编号
        /// </summary>
        /// <param name="keyValue">编号</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetEntityByCode(string keyValue)
        {
            if (string.IsNullOrEmpty(keyValue))
            {
                return Success("");
            }
            else
            {
                var data = dataSourceIBLL.GetEntityByCode(keyValue.Split(',')[0]);
                return JsonResult(data);
            }

        }
        /// <summary>
        /// 获取所有数据源实体根据编号
        /// </summary>
        /// <param name="keyValue">编号</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetNameByCode(string keyValue)
        {
            if (string.IsNullOrEmpty(keyValue))
            {
                return SuccessString("");
            }
            else
            {
                var data = dataSourceIBLL.GetEntityByCode(keyValue.Split(',')[0]);
                return SuccessString(data.F_Name);
            }

        }
        #endregion

        #region  提交数据
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, DataSourceEntity entity)
        {
            bool res = dataSourceIBLL.SaveEntity(keyValue, entity);
            if (res)
            {
                return Success("保存成功！");
            }
            else
            {
                return Fail("保存失败,编码重复！");
            }
        }
        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            dataSourceIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        #endregion

        #region  扩展方法
        /// <summary>
        /// 获取数据源数据
        /// </summary>
        /// <param name="code">数据源编号</param>
        /// <param name="strWhere">sql查询条件语句</param>
        /// <param name="queryJson">数据源请求条件字串</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetDataTable(string code,string strWhere, string queryJson)
        {
            var data = dataSourceIBLL.GetDataTable(code, strWhere, queryJson);
            return JsonResult(data);
        }
        /// <summary>
        /// 获取数据源数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="code">数据源编号</param>
        /// <param name="strWhere">sql查询条件语句</param>
        /// <param name="queryJson">数据源请求条件字串</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetDataTablePage(string pagination, string code, string strWhere, string queryJson)
        {

            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = dataSourceIBLL.GetDataTable(code, paginationobj, strWhere, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
         /// <summary>
        /// 获取数据源列名
        /// </summary>
        /// <param name="code">数据源编码</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetDataColName(string code)
        {
            var data = dataSourceIBLL.GetDataColName(code);
            return JsonResult(data);
        }

        /// <summary>
        /// 获取数据源数据
        /// </summary>
        /// <param name="code">数据源编号</param>
        /// <param name="strWhere">sql查询条件语句</param>
        /// <param name="queryJson">数据源请求条件字串</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMap(string code, string ver,string strWhere)
        {
            var where = "";
            if (!string.IsNullOrEmpty(strWhere))
            {
                where = strWhere;
            }
            var data = dataSourceIBLL.GetDataTable(code, where);

            string md5 = Md5Helper.Encrypt(data.ToJson(), 32);
            if (md5 == ver)
            {
                return Success("no update");
            }
            else
            {
                var jsondata = new
                {
                    data = data,
                    ver = md5
                };
                return JsonResult(jsondata);
            }
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMapUseCache(string code, string strWhere = "")
        {
            ///只是初步修改 后续要做大的调整
            string cacheKey = "";
            Dictionary<string, string> sourceDataCacheKeyList = cache.Read<Dictionary<string, string>>("SourceDataCacheKeyList");
            if (sourceDataCacheKeyList != null && sourceDataCacheKeyList.Count > 0)
            {
                var cacheKeyQuery = sourceDataCacheKeyList.FirstOrDefault(m => m.Value.Contains(code + strWhere));
                cacheKey = cacheKeyQuery.Key;
            }

            if (!string.IsNullOrEmpty(cacheKey))
            {
                string cacheData = cache.Read<string>(cacheKey);
                if (!string.IsNullOrEmpty(cacheData))
                    return Content("{\"code\":200,\"data\":" + cacheData + "}");
                else
                    sourceDataCacheKeyList.Remove(cacheKey);
            }

            var data = dataSourceIBLL.GetDataTable(code, strWhere);
            string cacheNewData = data.ToJson();
            string cacheNewKey = Guid.NewGuid().ToString().ToLower();

            if (sourceDataCacheKeyList == null)
                sourceDataCacheKeyList = new Dictionary<string, string>();

            sourceDataCacheKeyList.Add(cacheNewKey, code + strWhere);
            cache.Write<Dictionary<string, string>>("SourceDataCacheKeyList", sourceDataCacheKeyList);
            cache.Write<string>(cacheNewKey, cacheNewData,DateTime.Now.AddMinutes(1));

            return JsonResult(data);
        }
        
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTree(string code, string parentId, string Id, string showId)
        {
            var data = dataSourceIBLL.GetTree(code, parentId, Id, showId);
            return JsonResult(data);
        }
        #endregion
    }
}