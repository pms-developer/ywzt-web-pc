﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.ScheduleJobModel;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-07 10:01
    /// 描 述：计划任务中心
    /// </summary>
    public class BaseJobCenterController : MvcControllerBase
    {
        private BaseJobCenterIBLL baseJobCenterIBLL = new BaseJobCenterBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseJobCenterIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var JobInfoData = baseJobCenterIBLL.GetJobInfo(keyValue);
            var jsonData = new
            {
                Base_JobInfo = JobInfoData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            baseJobCenterIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string deleteList)
        {
            var mainInfo = strEntity.ToObject<JobInfo>();
            baseJobCenterIBLL.SaveEntity(keyValue, mainInfo, deleteList, type);
            return Success("保存成功！");
        }

        /// <summary>
        /// 启动
        /// </summary>
        /// <param name="jobInfo"></param>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        public ActionResult Run(JobInfo jobInfo)
        {
            var result = new JobLogic.JobLogic().Run(jobInfo);
            return GetApiInfo(result);
            #endregion
        }

        /// <summary>
        /// 暂停
        /// </summary>
        /// <param name="jobInfo"></param>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        public ActionResult Pause(JobInfo jobInfo)
        {
            var result = new JobLogic.JobLogic().Pause(jobInfo);
            return GetApiInfo(result);
        }

        /// <summary>
        /// 恢复
        /// </summary>
        /// <param name="jobInfo"></param>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        public ActionResult Resume(JobInfo jobInfo)
        {
            var result = new JobLogic.JobLogic().Resume(jobInfo);
            return GetApiInfo(result);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="jobInfo"></param>
        /// <returns></returns>
        [HttpPost,AjaxOnly]
        public ActionResult UpdateJob(JobInfo jobInfo)
        {
            var result = new JobLogic.JobLogic().Update(jobInfo);
            return GetApiInfo(result);
        }

        /// <summary>
        /// 停止
        /// </summary>
        /// <param name="jobInfo"></param>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        public ActionResult Remove(JobInfo jobInfo)
        {
            var result = new JobLogic.JobLogic().Remove(jobInfo);
            return GetApiInfo(result);
        }
        private ActionResult GetApiInfo(JobResult result)
        {
            if (result.Code == 400)
            {
                return Fail("操作失败", result);
                
            }
            else
            {
                return Success(result);
            }
        }
    }
}
