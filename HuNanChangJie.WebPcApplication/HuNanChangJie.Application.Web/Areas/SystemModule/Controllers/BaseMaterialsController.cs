﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-19 11:20
    /// 描 述：材料档案
    /// </summary>
    public class BaseMaterialsController : MvcControllerBase
    {
        private BaseMaterialsIBLL baseMaterialsIBLL = new BaseMaterialsBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }

        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MaterialsDialog()
        {
            return View();
        }

        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseMaterialsIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        public ActionResult CheckIsSameName(string name)
        {
            var data = baseMaterialsIBLL.CheckIsSameName(name);
            return Success(data);
        }


        /// <summary>
        /// 获取材料档案清单
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaterials(string pagination, string projectId,string queryJson )
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseMaterialsIBLL.GetMaterials(paginationobj, projectId, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }



        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaterialsList(string pagination, string typeId, string projectId, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseMaterialsIBLL.GetMaterialsList(paginationobj,typeId, projectId, queryJson);

            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };

            //return JsonResult(data);

            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Base_MaterialsData = baseMaterialsIBLL.GetBase_MaterialsEntity( keyValue );
            var jsonData = new {
                Base_Materials = Base_MaterialsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            return Fail("系统异常");
            /*baseMaterialsIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");*/
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            return Fail("系统异常");
            /*var mainInfo = strEntity.ToObject<Base_MaterialsEntity>();
            baseMaterialsIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");*/
        }
        #endregion

    }
}
