﻿using HuNanChangJie.Application.Excel;
using System.Collections.Generic;
using System.Web.Mvc;
using HuNanChangJie.Util;
using System.Linq;
using System.Data;
using System.Web;
using HuNanChangJie.Application.Base.SystemModule;
using Aliyun.OSS;
using System;
using System.IO;
using System.EnterpriseServices.Internal;
using DocumentFormat.OpenXml.Office2010.Excel;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.04.01
    /// 描 述：Excel导出管理
    /// </summary>
    public class ExcelExportController : MvcControllerBase
    {
        private ExcelExportIBLL excelExportIBLL = new ExcelExportBLL();
        private DataItemIBLL dataItemIBLL = new DataItemBLL();
        private DataSourceIBLL dataSourceIBLL = new DataSourceBLL();

        #region  视图功能
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult FormatForm()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult FormNew()
        {
            return View();
        }
        [HttpGet]
        public ActionResult SelectExportTypeForm()
        {
            return View();
        }
        [HttpGet]
        public ActionResult TemplateExportForm()
        {
            return View();
        }
        #endregion

        #region  获取数据
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = excelExportIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表数据
        /// </summary>
        /// <param name="moduleId">功能模块主键</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList(string moduleId)
        {
            var data = excelExportIBLL.GetList(moduleId);
            return JsonResult(data);
        }
        /// <summary>
        /// 获取表单数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetEntity(string keyValue)
        {
            var data = excelExportIBLL.GetEntity(keyValue);
            return JsonResult(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormDataNew(string keyValue)
        {
            var entity = excelExportIBLL.GetEntity(keyValue);
            var tableSetList = excelExportIBLL.GetTableSetList(keyValue);
            var fieldList = entity.F_FieldSettings.ToObject<List<ExcelExportFieldEntity>>();
            var primaryTable = tableSetList.FirstOrDefault(p => p.IsPrimary);
            var data = new
            {
                entity,
                primaryList = fieldList.Where(p => p.TableName == primaryTable.TableName),
                sublistList = fieldList.Where(p => p.TableName != primaryTable.TableName),
                tableSetList
            };
            return JsonResult(data);
        }
        #endregion

        #region  提交数据
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, ExcelExportEntity entity)
        {
            excelExportIBLL.SaveEntity(keyValue, entity);
            return Success("保存成功！");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveFormNew(string keyValue, string strEntity, string strTableSetList, string strPrimaryList, string strSublistList)
        {
            ExcelExportEntity entity = strEntity.ToObject<ExcelExportEntity>();
            List<ExcelExportFieldEntity> filedList = new List<ExcelExportFieldEntity>();
            List<ExcelExportTableSetEntity> tableSetList = strTableSetList.ToObject<List<ExcelExportTableSetEntity>>();
            if (!string.IsNullOrEmpty(strPrimaryList))
            {
                List<ExcelExportFieldEntity> filedPrimaryList = strPrimaryList.ToObject<List<ExcelExportFieldEntity>>();
                filedList.AddRange(filedPrimaryList);
            }
            if (!string.IsNullOrEmpty(strSublistList))
            {
                List<ExcelExportFieldEntity> filedSublistList = strSublistList.ToObject<List<ExcelExportFieldEntity>>();
                filedList.AddRange(filedSublistList);
            }
            if (filedList.Count > 0)
                entity.F_FieldSettings = filedList.ToJson();
            excelExportIBLL.SaveEntityNew(keyValue, entity, tableSetList);
            return Success("保存成功！");
        }
        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            excelExportIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 更新表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="entity">实体数据</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult UpdateState(string keyValue, int state)
        {
            ExcelExportEntity entity = new ExcelExportEntity()
            {
                F_EnabledMark = state
            };
            excelExportIBLL.SaveEntity(keyValue, entity);
            return Success("操作成功！");
        }
        #endregion
        [HttpPost]
        public void CustomExportExcel(string fileName, string moduleId, string queryIds)
        {
            //设置导出格式
            ExcelConfigNew excelconfig = new ExcelConfigNew();
            excelconfig.FileName = Server.UrlDecode(fileName) + ".xls";
            excelconfig.IsAllSizeColumn = true;
            excelconfig.ColumnEntity = new List<ColumnModelNew>();

            var primaryList = new Dictionary<string, Dictionary<string, string>>();
            var subList = new Dictionary<string, DataTable>();

            var excelExportList = excelExportIBLL.GetList(moduleId).Where(p => p.ExportType == 2);
            if (excelExportList.Count() > 0)
            {
                var excelExportEntity = excelExportList.First();
                List<ExcelExportFieldEntity> excelExportFieldList = excelExportEntity.F_FieldSettings.ToObject<List<ExcelExportFieldEntity>>();
                var tableSetList = excelExportIBLL.GetTableSetList(excelExportEntity.F_Id).ToList();
                if (tableSetList.Count() > 0 && !string.IsNullOrEmpty(queryIds))
                {
                    var ids = queryIds.Split(',').ToList();
                    foreach (var tableSet in tableSetList)
                    {
                        var excelExportFieldListQuery = excelExportFieldList.Where(p => p.TableName == tableSet.TableName);
                        var fieldSql = string.Join(",", excelExportFieldListQuery.Select(o => o.fieldCode));
                        if (tableSet.IsPrimary)
                        {
                            var dtPrimary = excelExportIBLL.GetExportDataTable(tableSet.TableName, fieldSql, tableSet.QueryField, ids);
                            foreach (DataRow row in dtPrimary.Rows)
                            {
                                Dictionary<string, string> itemPrimary = new Dictionary<string, string>();
                                foreach (DataColumn column in dtPrimary.Columns)
                                {
                                    var fieldColumn = excelExportFieldListQuery.FirstOrDefault(o => o.fieldCode.ToLower() == column.ColumnName.ToLower());
                                    if (fieldColumn != null)
                                    {
                                        if (fieldColumn.dataType == "sjzd")
                                        {
                                            var dataItemList = dataItemIBLL.GetDetailList(fieldColumn.dataItemCode);
                                            if (dataItemList != null)
                                            {
                                                var dataItemEntity = dataItemList.FirstOrDefault(p => p.F_ItemValue == row[column.ColumnName].ToString());
                                                if (dataItemEntity != null)
                                                {
                                                    itemPrimary.Add(column.ColumnName, dataItemEntity.F_ItemName);
                                                }
                                                else
                                                {
                                                    itemPrimary.Add(column.ColumnName, row[column.ColumnName].ToString());
                                                }
                                            }
                                            else
                                            {
                                                itemPrimary.Add(column.ColumnName, row[column.ColumnName].ToString());
                                            }

                                        }
                                        else if (fieldColumn.dataType == "sjy")
                                        {
                                            var dt = dataSourceIBLL.GetDataTable(fieldColumn.dataSourceCode, " id='" + row[column.ColumnName].ToString() + "'");
                                            if (dt != null)
                                            {
                                                itemPrimary.Add(column.ColumnName, dt.Rows[0][1].ToString());
                                            }
                                            else
                                            {
                                                itemPrimary.Add(column.ColumnName, row[column.ColumnName].ToString());
                                            }
                                        }
                                        else
                                        {
                                            itemPrimary.Add(column.ColumnName, row[column.ColumnName].ToString());
                                        }
                                    }
                                    else
                                    {
                                        itemPrimary.Add(column.ColumnName, row[column.ColumnName].ToString());
                                    }
                                }
                                primaryList.Add(row[tableSet.QueryField].ToString(), itemPrimary);
                            }
                        }
                        else
                        {
                            var dtSublist = excelExportIBLL.GetExportDataTable(tableSet.TableName, fieldSql, tableSet.QueryField, ids);
                            subList.Add(tableSet.TableName, dtSublist);
                        }

                        foreach (var excelExportField in excelExportFieldListQuery)
                        {
                            if (excelExportField.fieldCode.ToLower() != tableSet.QueryField.ToLower())
                            {
                                excelconfig.ColumnEntity.Add(new ColumnModelNew()
                                {
                                    Column = excelExportField.fieldCode,
                                    ExcelColumn = excelExportField.fieldText,
                                    Alignment = "center",
                                    TableName = excelExportField.TableName
                                });
                            }
                        }

                    }

                }

                if (primaryList.Count > 0)
                {
                    ExcelHelperNew.ExcelDownload(primaryList, subList, tableSetList, excelconfig);
                }
            }
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UploadFile(HttpPostedFileBase file, string keyValue)
        {
            #region 上传自定义导出模板
            // 获取上传文件的扩展名
            if (file == null)
            {
                return Success("");
            }
            var extension = System.IO.Path.GetExtension(file.FileName);
            if (extension.Contains("xls"))
            {

            }
            else
            {
                return Fail("不支持该格式");
            }
            #region 上传SSO
            //long timestamp = ((DateTimeOffset)System.DateTime.Now).ToUnixTimeSeconds();
            //string fullFileName = timestamp + extension;
            //// yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
            //var endpoint = "https://oss-cn-shenzhen.aliyuncs.com";
            //// 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
            //var accessKeyId = "LTAI5tQnAwRcEBzn3gkTFR7u";
            //var accessKeySecret = "4jgxSlJNgCo6kdSL5t4bMSYPkATui0";
            //// 填写Bucket名称，例如examplebucket。
            //var bucketName = "meio-wms";
            //// 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
            //string dt = System.DateTime.Now.ToString("yyyy-MM-dd");
            //var objectName = "erp/" + dt + "/" + fullFileName;
            //// 创建OssClient实例。
            //var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
            //try
            //{
            //    // 上传文件。
            //    client.PutObject(bucketName, objectName, file.InputStream);
            //    // return Success("https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName);
            //    fullFileName = "https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName;

            //    excelExportIBLL.UpdateCustomTemplate(keyValue, fullFileName);
            //    return Success("上传成功");

            //}
            //catch (Exception e)
            //{
            //    return Fail("上传文件异常：" + e.Message);
            //}
            #endregion
            try
            {
                long timestamp = ((DateTimeOffset)System.DateTime.Now).ToUnixTimeSeconds();
                string fullFileName = timestamp + extension;
                string path = Server.MapPath("~/Areas/SystemModule/Views/ExcelExport/Template/"); // 确保这个文件夹存在
                string filepath = Path.Combine(path, fullFileName);

                // 保存文件到服务器
                file.SaveAs(filepath);

                //excelExportIBLL.UpdateCustomTemplate(keyValue, filepath);
                return Success(filepath);
            }
            catch (Exception e)
            {
                return Fail("上传文件异常：" + e.Message);
            }
            #endregion
        }


        [HttpPost]
        public ActionResult DeleteCustomTemplate(string keyValue)
        {
            try
            {
                excelExportIBLL.UpdateCustomTemplate(keyValue, "");
                return Success("删除成功");
            }
            catch (Exception e)
            {
                return Fail("删除失败，异常：" + e.Message);
            }
        }

        [HttpPost]
        public ActionResult TemplateExportExcel(string moduleId, string queryIds,string btnId,string fileName)
        {
            var excelExportList = excelExportIBLL.GetList(moduleId).Where(p => p.ExportType == 3);
            if (excelExportList.Count() > 0)
            {
                var excelExportEntity = excelExportList.First(p=>p.F_ModuleBtnId==btnId);
                var ids = queryIds.Split(',').ToList();
                var dt = excelExportIBLL.GetExportDataTable(excelExportEntity.ExportTemplateSQL, ids);
                if (dt.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(fileName))
                        fileName = excelExportEntity.F_Name;
                    ExcelHelperNew.ExcelDownload(excelExportEntity.ExportTemplate, dt, fileName);
                    return Success("导出成功");
                }
                else
                {
                    return Fail("导出失败，没有可导出的数据");
                }
            }
            else
            {
                return Fail("导出失败，请检查导出配置");
            }
        }
    }
}