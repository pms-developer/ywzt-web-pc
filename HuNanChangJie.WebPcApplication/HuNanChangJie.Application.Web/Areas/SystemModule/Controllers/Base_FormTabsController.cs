﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule.Base_FormTabs;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    public class Base_FormTabsController : MvcControllerBase
    {
        private Base_FormTabsIBLL bll = new Base_FormTabsBLL();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">关键词</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = bll.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }

        /// <summary>
        /// 获取实体数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetEntity(string keyValue)
        {
            var data = bll.GetBase_FormTabsEntity(keyValue);
            return JsonResult(data);
        }

        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken, AjaxOnly]
        public ActionResult SaveForm(string keyValue, Base_FormTabsEntity entity,string type)
        {
            bll.SaveEntity(keyValue, entity,type);
            return Success("保存成功！");
        }
        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            bll.DeleteEntity(keyValue);
            return Success("删除成功！");
        }

        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormTabInfo(string formId)
        {
            var data = bll.GetFormTabInfo(formId);
            return Success(data);
        }
    }
}