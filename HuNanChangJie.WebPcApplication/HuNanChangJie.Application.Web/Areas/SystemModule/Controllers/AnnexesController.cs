﻿using DocumentFormat.OpenXml.Drawing.Diagrams;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Util;
using PreviewFile;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.03.08
    /// 描 述：附件管理
    /// </summary>
    public class AnnexesController : MvcControllerBase
    {
        private AnnexesFileIBLL annexesFileIBLL = new AnnexesFileBLL();

        #region  视图功能
        /// <summary>
        /// 上传列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UploadForm()
        {
            return View();
        }
        /// <summary>
        /// 下载列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DownForm()
        {
            return View();
        }

        /// <summary>
        /// 在线预览
        /// </summary>
        /// <param name="fileId">文件id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PreviewFile(string fileId)
        {
            string htmlUrl = "";
            try
            {
                PrevewFilePdf prevewFilePdf = new PrevewFilePdf();

                var data = annexesFileIBLL.GetEntity(fileId);
                string filename = data.F_FileName;//文件名称
                string filepath = data.F_FilePath;//文件路径
                if (System.IO.File.Exists(filepath))
                {
                    htmlUrl = prevewFilePdf.PreviewFile(filepath, filename);
                }
            }
            catch (Exception ex)
            {
                htmlUrl = ex.Message;
            }
            int port = System.Web.HttpContext.Current.Request.Url.Port;
            string text = System.Web.HttpContext.Current.Request.Url.Host;
            if (port > 0)
            {
                text = text + ":" + port;
            }
            ViewBag.htmlUrl = htmlUrl.Replace(text, "");
            return View();
        }


        #endregion

        #region  提交数据
        /// <summary>
        /// 上传附件分片数据
        /// </summary>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="chunk">分片序号</param>
        /// <param name="Filedata">文件数据</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadAnnexesFileChunk(string fileGuid, int chunk, int chunks, HttpPostedFileBase Filedata,string name)
        {
            //没有文件上传，直接返回
            if (Filedata == null || string.IsNullOrEmpty(Filedata.FileName) || Filedata.ContentLength == 0)
            {
                if (Request.Files.Count > 0)
                {
                    Filedata = Request.Files[0];
                }
                else
                {
                    return HttpNotFound();
                }
            }

            //if (bool.Parse(Config.GetValue("EnableEmployeeUpload")))
            //{
            //    UserInfo userInfo = LoginUserInfo.Get();              
            //    string fileName = Path.GetFileNameWithoutExtension(name) + userInfo.mobile;

            //    annexesFileIBLL.SaveChunkAnnexes(fileName, chunk, Filedata.InputStream);
            //}
            //else
            //{
                annexesFileIBLL.SaveChunkAnnexes(fileGuid, chunk, Filedata.InputStream);
            //}
            return Success("保存成功");
        }
        /// <summary>
        /// 移除附件分片数据
        /// </summary>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="chunks">总分片数</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveAnnexesFileChunk(string fileGuid, int chunks)
        {
            annexesFileIBLL.RemoveChunkAnnexes(fileGuid, chunks);
            return Success("移除成功");
        }

        /// <summary>
        /// 合并上传附件的分片数据
        /// </summary>
        /// <param name="folderId">附件夹主键</param>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="fileName">文件名</param>
        /// <param name="chunks">文件总分片数</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MergeAnnexesFile(string folderId, string fileGuid, string fileName, int chunks)
        {
            UserInfo userInfo = LoginUserInfo.Get();
            bool res = annexesFileIBLL.SaveAnnexes(folderId, fileGuid, fileName, chunks, userInfo);

        
            if (res)
            {
                return Success("保存文件成功");

            }
            else
            {
                return Fail("保存文件失败");
            }
        }

        /// <summary>
        /// 合并上传附件的分片数据
        /// </summary>
        /// <param name="infoId">信息ID</param>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="fileName">文件名</param>
        /// <param name="chunks">文件总分片数</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewMergeAnnexesFile(string infoId, string fileGuid, string fileName, int chunks, string moduleType, string moduleId, string moduleInfoId, string folderName)
        {
            UserInfo userInfo = LoginUserInfo.Get();
            bool res = false;

            if (bool.Parse(Config.GetValue("EnableEmployeeUpload")))
            {

                string fileNameTarget = Path.GetFileNameWithoutExtension(fileName) + userInfo.mobile;

                res = annexesFileIBLL.SaveAnnexes(infoId, fileNameTarget, fileName, chunks, userInfo, moduleType, moduleId, moduleInfoId, folderName);
            }
            else
            {
                 res = annexesFileIBLL.SaveAnnexes(infoId, fileGuid, fileName, chunks, userInfo, moduleType, moduleId, moduleInfoId, folderName);

            }

            if (res)
            {
                return Success("保存文件成功");

            }
            else
            {
                return Fail("保存文件失败");
            }
        }
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="fileId">文件主键</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAnnexesFile(string fileId)
        {
            AnnexesFileEntity fileInfoEntity = annexesFileIBLL.GetEntity(fileId);
            annexesFileIBLL.DeleteEntity(fileId);
            //删除文件
            if (System.IO.File.Exists(fileInfoEntity.F_FilePath))
            {
                System.IO.File.Delete(fileInfoEntity.F_FilePath);
            }
            return Success("删除附件成功");
        }

        /// <summary>
        /// MVC上传文件
        /// </summary>
        /// <param name="basefile"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadFileMvc(HttpPostedFileBase basefile)
        {
            if (basefile != null)
            {
                if (!Directory.Exists(Server.MapPath("~/Attachment/Files")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Attachment/Files"));
                }
                //没有上传报告，需要新建
                var guidstr = Guid.NewGuid().ToString();
                var pathforsaving = Server.MapPath("~/Attachment/Files");
                string serverfilename = guidstr.ToString() + basefile.FileName;
                string filePath = Path.Combine(pathforsaving, serverfilename);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                basefile.SaveAs(filePath);
                return Success("保存文件成功", filePath);
            }
            return Fail("保存文件失败");
        }
        #endregion

        #region  获取数据
        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="fileId">文件id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public string DownAnnexesFile(string fileId)
        {
            var data = annexesFileIBLL.GetEntity(fileId);
            string filename = Server.UrlDecode(data.F_FileName);//返回客户端文件名称
            string filepath = data.F_FilePath;
            if (FileDownHelper.FileExists(filepath))
            {
                FileDownHelper.DownLoadold(filepath, filename);
                annexesFileIBLL.FileDownCounter(fileId);
                return "";
            }
            return "文件不存在";
        }

        public void DownAnnexesFile2(string fileId)
        {
            var data = annexesFileIBLL.GetEntity(fileId);
            string filename = Server.UrlDecode(data.F_FileName);
            string filepath = data.F_FilePath;
            if (FileDownHelper.FileExists(filepath))
            {
                annexesFileIBLL.FileDownCounter(fileId);
                FileInfo fi = new FileInfo(filepath);
                Response.Clear();
                Response.ClearHeaders();
                Response.Buffer = false;
                Response.AppendHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(filename, System.Text.Encoding.UTF8));// HttpUtility.UrlEncode(name, System.Text.Encoding.UTF8));// ;
                Response.AppendHeader("Content-Length", fi.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(filepath);
                Response.Flush();
                Response.End();
            }
        }
        /// <summary>
        /// 获取附件列表
        /// </summary>
        /// <param name="folderId">附件夹主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAnnexesFileList(string infoId)
        {
            var data = annexesFileIBLL.GetList(infoId);
            return JsonResult(data);
        }

        /// <summary>
        /// 获取附件列表
        /// </summary>
        /// <param name="folderId">附件夹主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAnnexesFilesList(string infoIds)
        {
            var data = annexesFileIBLL.GetLists(infoIds);
            return JsonResult(data);
        }

        /// <summary>
        /// 获取附件主图
        /// </summary>
        /// <param name="folderId">附件夹主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAnnexesImgOne(string infoId)
        {
            var fileList = annexesFileIBLL.GetList(infoId);
            if (fileList.Count() > 0)
            {
                var imgList = fileList.ToList();
                string baseImgId = imgList[0].F_Id;
                string htmlUrl = "";
                PrevewFilePdf prevewFilePdf = new PrevewFilePdf();

                var dataFile = annexesFileIBLL.GetEntity(baseImgId);
                string filename = dataFile.F_FileName;//文件名称
                string filepath = dataFile.F_FilePath;//文件路径
                if (System.IO.File.Exists(filepath))
                {
                    htmlUrl = prevewFilePdf.PreviewFile(filepath, filename);
                }
                int port = System.Web.HttpContext.Current.Request.Url.Port;
                string text = System.Web.HttpContext.Current.Request.Url.Host;
                if (port > 0)
                {
                    text = text + ":" + port;
                }
                string baseImg = htmlUrl.Replace(text, "");
                return JsonResult(new
                {
                    baseImgId = baseImgId,
                    baseImg = baseImg,
                });
            }
            return JsonResult("");
        }

        /// <summary>
        /// 获取项目文档 文件列表
        /// </summary>
        /// <param name="moduleId">功能模块ID</param>
        /// <param name="moduleInfoId">项目ID</param>
        /// <returns></returns>
        public ActionResult GetProjectFileList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var queryParam = queryJson.ToJObject();
            var moduleId = "";
            var moduleInfoId = "";
            var keyword = "";
            if (!queryParam["moduleId"].IsEmpty())
                moduleId = queryParam["moduleId"].ToString();

            if (!queryParam["moduleInfoId"].IsEmpty())
                moduleInfoId = queryParam["moduleInfoId"].ToString();

            if (!queryParam["keyword"].IsEmpty())
                keyword = queryParam["keyword"].ToString();
            var data = annexesFileIBLL.GetProjectFileList(paginationobj, moduleId, moduleInfoId, keyword);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取项目文档 文件列表
        /// </summary>
        /// <param name="moduleId">功能模块ID</param>
        /// <param name="moduleInfoId">项目ID</param>
        /// <returns></returns>
        public ActionResult GetProjectFileListNew(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var queryParam = queryJson.ToJObject();
            var moduleId = "";
            var moduleInfoId = "";
           
            var keyword = "";
            if (!queryParam["moduleId"].IsEmpty())
                moduleId = queryParam["moduleId"].ToString();
           
            if (!queryParam["moduleInfoId"].IsEmpty())
                moduleInfoId = queryParam["moduleInfoId"].ToString();

            if (!queryParam["keyword"].IsEmpty())
                keyword = queryParam["keyword"].ToString();

            var data = annexesFileIBLL.GetProjectFileListNew(paginationobj, moduleId, keyword, moduleInfoId);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取附件夹信息
        /// </summary>
        /// <param name="folderId">附件夹主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetFileNames(string folderId)
        {
            var data = annexesFileIBLL.GetFileNames(folderId);
            return Success(data);
        }

        

        #endregion
    }
}