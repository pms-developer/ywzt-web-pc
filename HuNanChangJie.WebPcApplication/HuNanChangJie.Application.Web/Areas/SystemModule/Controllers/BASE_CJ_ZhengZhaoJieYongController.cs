﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-19 16:03
    /// 描 述：证书借用
    /// </summary>
    public class BASE_CJ_ZhengZhaoJieYongController : MvcControllerBase
    {
        private BASE_CJ_ZhengZhaoJieYongIBLL bASE_CJ_ZhengZhaoJieYongIBLL = new BASE_CJ_ZhengZhaoJieYongBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = bASE_CJ_ZhengZhaoJieYongIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = bASE_CJ_ZhengZhaoJieYongIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = bASE_CJ_ZhengZhaoJieYongIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Base_CJ_ZhenZhaoJieYongData = bASE_CJ_ZhengZhaoJieYongIBLL.GetBase_CJ_ZhenZhaoJieYongEntity( keyValue );
            var Base_CJ_ZhenZhaoJieYongMingXiData = bASE_CJ_ZhengZhaoJieYongIBLL.GetBase_CJ_ZhenZhaoJieYongMingXiList( Base_CJ_ZhenZhaoJieYongData.ID );
            var jsonData = new {
                Base_CJ_ZhenZhaoJieYong = Base_CJ_ZhenZhaoJieYongData,
                Base_CJ_ZhenZhaoJieYongMingXi = Base_CJ_ZhenZhaoJieYongMingXiData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            bASE_CJ_ZhengZhaoJieYongIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        [HttpPost]
        [AjaxOnly]
        public ActionResult GuiHuan(string keyValue,string ids)
        {
            var param = ids.ToJObject();
            if (param["ids"].IsEmpty())
            {
                return Fail("参数错误");
            }
            List<string> idsList = new List<string>();
            foreach (var item in param["ids"])
            {
                idsList.Add(item.ToString());
            }

            var result = bASE_CJ_ZhengZhaoJieYongIBLL.GuiHuan(keyValue, idsList);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strbase_CJ_ZhenZhaoJieYongMingXiList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Base_CJ_ZhenZhaoJieYongEntity>();
            var base_CJ_ZhenZhaoJieYongMingXiList = strbase_CJ_ZhenZhaoJieYongMingXiList.ToObject<List<Base_CJ_ZhenZhaoJieYongMingXiEntity>>();
            bASE_CJ_ZhengZhaoJieYongIBLL.SaveEntity(keyValue,mainInfo,base_CJ_ZhenZhaoJieYongMingXiList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
