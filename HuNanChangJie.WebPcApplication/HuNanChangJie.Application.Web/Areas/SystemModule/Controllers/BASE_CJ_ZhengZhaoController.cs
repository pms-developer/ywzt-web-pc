﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-13 20:34
    /// 描 述：证照管理
    /// </summary>
    public class BASE_CJ_ZhengZhaoController : MvcControllerBase
    {
        private BASE_CJ_ZhengZhaoIBLL bASE_CJ_ZhengZhaoIBLL = new BASE_CJ_ZhengZhaoBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }

        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Dialog2()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Dialog3()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = bASE_CJ_ZhengZhaoIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetZhengZhaoList(string queryJson = "")
        {
            var data = bASE_CJ_ZhengZhaoIBLL.GetZhengZhaoList();
            var edata = data;
            if (!string.IsNullOrEmpty(queryJson))
            {
                var queryParam = queryJson.ToJObject();

                if (!queryParam["sword"].IsEmpty())
                {
                    string sword = queryParam["sword"].ToString();
                    edata = edata.Where(zz => (!string.IsNullOrEmpty(zz.zzmc_name) && zz.zzmc_name.Contains(sword)) || (!string.IsNullOrEmpty(zz.zz_bianhao) && zz.zz_bianhao.Contains(sword)) || (!string.IsNullOrEmpty(zz.zz_zhuanye) && zz.zz_zhuanye.Contains(sword)) || (!string.IsNullOrEmpty(zz.zz_shenfengzheng) && zz.zz_shenfengzheng.Contains(sword)) || (!string.IsNullOrEmpty(zz.zz_xinming) && zz.zz_xinming.Contains(sword)));
                }
            }
            var jsonData = new { rows = edata };
            return Success(jsonData);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetZhengZhaoList2(string queryJson = "")
        {
            var data = bASE_CJ_ZhengZhaoIBLL.GetZhengZhaoList2(queryJson);
            
            var jsonData = new { rows = data };
            return Success(jsonData);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetZhengZhaoList3(string id="")
        {
            if(string.IsNullOrEmpty(id))
                return Fail("参数错误");

            var data = bASE_CJ_ZhengZhaoIBLL.GetZhengZhaoListByJieYongId(id);
            var jsonData = new { rows = data }; 
            return Success(jsonData);
        }


        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Base_CJ_ZhengZhaoData = bASE_CJ_ZhengZhaoIBLL.GetBase_CJ_ZhengZhaoEntity( keyValue );
            var jsonData = new {
                Base_CJ_ZhengZhao = Base_CJ_ZhengZhaoData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取左侧树形数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTree(int deep = 0)
        {
            var data = bASE_CJ_ZhengZhaoIBLL.GetTree(deep);
            return Success(data);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            bASE_CJ_ZhengZhaoIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        [HttpPost]
        [AjaxOnly]
        public ActionResult UpdateStatus(string id,string status)
        {
            bASE_CJ_ZhengZhaoIBLL.UpdateStatus(id, status);
            return Success("操作成功！");
        }
        
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Base_CJ_ZhengZhaoEntity>();
            bASE_CJ_ZhengZhaoIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
