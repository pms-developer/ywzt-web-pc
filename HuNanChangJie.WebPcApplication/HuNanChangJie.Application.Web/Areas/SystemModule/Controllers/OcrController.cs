﻿using HuNanChangeJie.Plugin.ORC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    public class OcrController : MvcControllerBase
    {
        // GET: SystemModule/Ocr
        public ActionResult InvoiceOcr()
        {
            return View();
        }

        [AjaxOnly,HttpPost]
        public ActionResult GetInvoiceOcrDate(string imgbase64)
        {
            var data = HWORC.DiscernInvoice(imgbase64,true);
            return Success(data);
        }
    }
}