﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-08-03 17:49
    /// 描 述：PrintTestSystemForm
    /// </summary>
    public class PrintTestSystemFormController : MvcControllerBase
    {
        private PrintTestSystemFormIBLL printTestSystemFormIBLL = new PrintTestSystemFormBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = printTestSystemFormIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var PrintTestMianData = printTestSystemFormIBLL.GetPrintTestMianEntity( keyValue );
            var PrintTestSubAData = printTestSystemFormIBLL.GetPrintTestSubAList( PrintTestMianData.ID );
            var PrintTestSubBData = printTestSystemFormIBLL.GetPrintTestSubBList( PrintTestMianData.ID );
            var jsonData = new {
                PrintTestMian = PrintTestMianData,
                PrintTestSubA = PrintTestSubAData,
                PrintTestSubB = PrintTestSubBData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            printTestSystemFormIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strprintTestSubAList, string strprintTestSubBList,string deleteList)
        {
            strEntity = strEntity.Replace("\"Sex\":\"1\"", "\"Sex\":\"true\"");
            strEntity = strEntity.Replace("\"Sex\":\"0\"", "\"Sex\":\"false\"");
            var mainInfo = strEntity.ToObject<PrintTestMianEntity>();
            var printTestSubAList = strprintTestSubAList.ToObject<List<PrintTestSubAEntity>>();
            var printTestSubBList = strprintTestSubBList.ToObject<List<PrintTestSubBEntity>>();
            printTestSystemFormIBLL.SaveEntity(keyValue,mainInfo,printTestSubAList,printTestSubBList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
