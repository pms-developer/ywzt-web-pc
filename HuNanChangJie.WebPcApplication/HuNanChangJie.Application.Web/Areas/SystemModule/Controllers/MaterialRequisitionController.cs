﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-21 18:46
    /// 描 述：领料单
    /// </summary>
    public class MaterialRequisitionController : MvcControllerBase
    {
        private MaterialRequisitionIBLL materialRequisitionIBLL = new MaterialRequisitionBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult MaterialsListDialog()
        {
            return View();
        }

        #endregion

        #region  获取数据
        /// <summary>
        /// 获取材料档案
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaterialsInfoList(string pagination, string queryJson, string subcontractId, string warehouseId)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = materialRequisitionIBLL.GetMaterialsInfoList(paginationobj, queryJson, subcontractId, warehouseId);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = materialRequisitionIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MaterialRequisitionData = materialRequisitionIBLL.GetMaterialRequisitionEntity(keyValue);
            var MaterialRequisitionDetailsData = materialRequisitionIBLL.GetMaterialRequisitionDetailsList(MaterialRequisitionData.ID);
            var jsonData = new
            {
                MaterialRequisition = MaterialRequisitionData,
                MaterialRequisitionDetails = MaterialRequisitionDetailsData,
            };
            return Success(jsonData);
        }


        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoListConnMaterials(string keyValue)
        {
            var MaterialRequisitionData = materialRequisitionIBLL.GetMaterialRequisitionEntity(keyValue);
            var MaterialRequisitionDetailsData = materialRequisitionIBLL.GetMaterialRequisitionDetailsListConnectionMaterials(MaterialRequisitionData.ID);
            var jsonData = new
            {
                MaterialRequisition = MaterialRequisitionData,
                MaterialRequisitionDetails = MaterialRequisitionDetailsData,
            };
            return Success(jsonData);
        }

        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = materialRequisitionIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = materialRequisitionIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            materialRequisitionIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strmaterialRequisitionDetailsList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<MaterialRequisitionEntity>();
            var materialRequisitionDetailsList = strmaterialRequisitionDetailsList.ToObject<List<MaterialRequisitionDetailsEntity>>();
            materialRequisitionIBLL.SaveEntity(keyValue, mainInfo, materialRequisitionDetailsList, deleteList, type);
            return Success("保存成功！");
        }
        #endregion

    }
}
