﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.SystemModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-25 15:59
    /// 描 述：客户管理
    /// </summary>
    public class Base_CustomerController : MvcControllerBase
    {
        private Base_CustomerIBLL base_CustomerIBLL = new Base_CustomerBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = base_CustomerIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Base_CustomerData = base_CustomerIBLL.GetBase_CustomerEntity( keyValue );
            var jsonData = new {
                Base_Customer = Base_CustomerData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            base_CustomerIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Base_CustomerEntity>();
            base_CustomerIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
