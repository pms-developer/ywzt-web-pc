﻿using System.Web.Mvc;
namespace HuNanChangJie.Application.Web.Areas.CaiWu
{
    public class CaiWuAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CaiWu"; 
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CaiWu_default", 
                "CaiWu/{controller}/{action}/{id}", 
                             new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
