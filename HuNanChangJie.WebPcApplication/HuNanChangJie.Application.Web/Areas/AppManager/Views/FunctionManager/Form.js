﻿
/*
 * 日 期：2018.03.16
 * 描 述：功能模块	
 */
var keyValue = request('keyValue');
var type = request('type');
var isProjectModule = request("isProjectModule");
var formId = request("formId");
var isSystem = request("isSystem");
var titlemsg = "与系统模块中的项目管理菜单中同，此处不可修改";
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";

    var mkcomponts = [];

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        /*绑定事件和初始化控件*/
        bind: function () {

            if (isProjectModule == "1") {
               
                $("#F_Name").attr("readonly","readonly");
                $("#F_Name").attr("title", titlemsg);
                $("#F_Type").attr("readonly", "readonly");
                $("#F_Type").attr("title", titlemsg);
                $("#F_SortCode").attr("readonly", "readonly");
                $("#F_SortCode").attr("title", titlemsg);
                
               
                if (isSystem == "2") {//自定义表单
                    $("#F_FormId").attr("readonly", "readonly");
                    $("#F_FormId").attr("title", titlemsg);
                }
            }
            // 列表显示
            $('#listTitle').mkselect({
                type: 'multiple'
            });
            $('#listContent1').mkselect({ maxHeight: 160 });
            $('#listContent2').mkselect({ maxHeight: 160 });
            $('#listContent3').mkselect({ maxHeight: 160 });


            // 选择图标
            $('#selectIcon').on('click', function () {
                Changjie.layerForm({
                    id: 'iconForm',
                    title: '选择图标',
                    url: top.$.rootUrl + '/Utility/AppIcon',
                    height: 700,
                    width: 1000,
                    btn: null,
                    maxmin: true,
                    end: function () {
                        if (top._changjieSelectIcon != '') {
                            $('#F_Icon').val(top._changjieSelectIcon);
                        }
                    }
                });
            });
            // 分类
            $('#F_Type').mkselect({
                allowSearch: true,
                title: 'F_ItemName',
                text: 'F_ItemName',
                value: 'F_ItemValue',
                url: top.$.rootUrl + '/AppManager/FunctionManager/GetAppModule',
                showtop: true
            });
            $('#F_Type').mkselectSet(type);
            // 选在表单
            $('#F_FormId').mkselect({
                text: 'F_Name',
                value: 'F_Id',
                url: top.$.rootUrl + '/FormModule/Custmerform/GetSchemeInfoList',
                maxHeight: 180,
                allowSearch: true,
                select: function (item) {
                    if (item) {
                        mkcomponts = [];
                        $.mkSetForm(top.$.rootUrl + '/FormModule/Custmerform/GetFormData?keyValue=' + item.F_Id, function (data) {
                            var scheme = JSON.parse(data.schemeEntity.F_Scheme);
                            for (var i = 0, l = scheme.data.length; i < l; i++) {
                                var componts = scheme.data[i].componts;
                                for (var j = 0, jl = componts.length; j < jl; j++) {
                                    var compont = componts[j];
                                    if (compont.type == 'gridtable') {
                                    }
                                    else {
                                        var point = { text: compont.title, id: compont.id };
                                        mkcomponts.push(point);
                                    }
                                }
                            }

                            $('#listTitle').mkselectRefresh({
                                data: mkcomponts
                            });
                            $('#listContent1').mkselectRefresh({
                                data: mkcomponts
                            });
                            $('#listContent2').mkselectRefresh({
                                data: mkcomponts
                            });
                            $('#listContent3').mkselectRefresh({
                                data: mkcomponts
                            });
                        });
                    } else {
                        mkcomponts = [];
                        $('#listTitle').mkselectRefresh({
                            data: mkcomponts
                        });
                        $('#listContent1').mkselectRefresh({
                            data: mkcomponts
                        });
                        $('#listContent2').mkselectRefresh({
                            data: mkcomponts
                        });
                        $('#listContent3').mkselectRefresh({
                            data: mkcomponts
                        });
                    }
                }
            });

            $("#SystemFormId").mkselect({
                text: "FormName",
                value: 'ID',
                url: top.$.rootUrl + '/SystemForm/SystemForm/GetSystemForms',
                maxHeight: 180,
                allowSearch: true,
                select: function (item) {
                    if (item) {
                        $("#F_Name").val(item.FormName);
                        $("#F_Url").val(item.AppUrl);
                    }
                }
            });
            $('#preview').on('click', function () {
                var formId = $('#F_FormId').mkselectGet();
                if (!!formId) {
                    Changjie.layerForm({
                        id: 'custmerForm_PreviewForm',
                        title: '预览当前表单',
                        url: top.$.rootUrl + '/FormModule/Custmerform/PreviewForm?schemeInfoId=' + formId,
                        width: 800,
                        height: 600,
                        maxmin: true,
                        btn: null
                    });
                }
                else {
                    Changjie.alert.warning('请选择表单！');
                }
            });

            // 功能类型
            $('[name="F_IsSystem"]').on('click', function () {
                var value = $(this).val();
                if (value == 1) {
                    $('.system').show();
                    $('.nosystem').hide();
                } else {
                    $('.system').hide();
                    $('.nosystem').show();
                }
            });

        },
        /*初始化数据*/
        initData: function () {
            if (!!keyValue) {
                if (isProjectModule == "0") {
                    $.mkSetForm(top.$.rootUrl + '/AppManager/FunctionManager/GetForm?keyValue=' + keyValue, function (data) {//
                        $('#form').mkSetFormData(data.entity);
                        if (data.entity.F_IsSystem != 1 && data.schemeEntity) {
                            var scheme = JSON.parse(data.schemeEntity.F_Scheme);
                            $('#listTitle').mkselectSet(scheme.title);
                            $('#listContent1').mkselectSet(scheme.content[0]);
                            $('#listContent2').mkselectSet(scheme.content[1]);
                            $('#listContent3').mkselectSet(scheme.content[2]);
                        }
                        if (data.entity.F_IsSystem == 1) {
                            $("#SystemFormId").mkselectSet(data.entity.F_FormId);
                        }
                    });
                }
                else if (isProjectModule == "1") {
                    Changjie.httpAsyncGet(top.$.rootUrl + "/SystemModule/Module/GetModuleInfo?keyValue=" + keyValue, function (data) {
                        var info = data.data;
                        if (info) {

                            $("#F_Name").val(info.F_FullName);
                            $("#F_Icon").val(info.APP_Icon);
                            $("#F_Type").mkselectSet(type);
                            $("#F_SortCode").val(info.F_SortCode);
                          
                            if ($('input[name="F_IsSystem"][value="' + isSystem + '"]').is(":checked") == false) {
                                $('input[name="F_IsSystem"][value="' + isSystem + '"]').trigger('click');
                            }
                            if (info.F_IsSystem == 1) {//系统表单
                                $("#SystemFormId").mkselectSet(info.FormId);
                                $("#F_Url").val(info.App_Url);
                            }
                            else {
                                $("#F_FormId").mkselectSet(formId);
                                var scheme = JSON.parse(info.AppCustomScheme);
                                if (scheme) {
                                    $('#listTitle').mkselectSet(scheme.title);
                                    $('#listContent1').mkselectSet(scheme.content[0]);
                                    $('#listContent2').mkselectSet(scheme.content[1]);
                                    $('#listContent3').mkselectSet(scheme.content[2]);
                                }
                            }
                        }
                        $("input[name='F_IsSystem']").each(function () {
                            var $this = $(this)
                            if ($this.val() == isSystem) {
                                $this.trigger('click');
                            }
                            $this.attr("disabled", true);
                            $this.attr("title", titlemsg);
                        });
                    });
                }

            }
        }
    };


    // 保存数据
    acceptClick = function (callBack) {
        if (isProjectModule == "1") {
            projectModuleSave(callBack);
        }
        else {
            defaultSave(callBack);
        }
            
    };
    var projectModuleSave = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var formData = $('#form').mkGetFormData(keyValue);
        formData.F_IsSystem = $('[name="F_IsSystem"]:checked').val();
        var entity = {
            F_IsSystem: formData.F_IsSystem,
            APP_Icon: formData.F_Icon,
            App_Url: formData.F_Url,
            FormId: formData.F_FormId,
             
        };
        if (formData.F_IsSystem == 1) {
            if ($.trim(formData.F_Url || '') == '') {
                Changjie.alert.error('请填写功能地址!');
                return false;
            }
            entity.FormId = $("#SystemFormId").mkselectGet();
        }
        else {
            if ($.trim(formData.listTitle || '') == '') {
                Changjie.alert.error('请设置列表展示!');
                return false;
            }
            var AppCustomScheme = {
                title: formData.listTitle,
                content: ['', '', '']
            };
            AppCustomScheme.content[0] = formData.listContent1;
            AppCustomScheme.content[1] = formData.listContent2;
            AppCustomScheme.content[2] = formData.listContent3;
            entity.AppCustomScheme = JSON.stringify(AppCustomScheme);
        }

        // 提交数据
        var postData = {
            strEntity: JSON.stringify(entity),
        };


        $.mkSaveForm(top.$.rootUrl + '/SystemModule/Module/UpdateEntity?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    var defaultSave = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var formData = $('#form').mkGetFormData(keyValue);
        formData.F_IsSystem = $('[name="F_IsSystem"]:checked').val();
        var entity = {
            F_Name: formData.F_Name,
            F_Icon: formData.F_Icon,
            F_Type: formData.F_Type,
            F_SortCode: formData.F_SortCode,
            F_FormId: formData.F_FormId,
            F_SchemeId: formData.F_SchemeId,
            F_Url: formData.F_Url,
            F_IsSystem: formData.F_IsSystem,
        };
        if (formData.F_IsSystem == 1) {
            if ($.trim(formData.F_Url || '') == '') {
                Changjie.alert.error('请填写功能地址!');
                return false;
            }
            entity.F_FormId = $("#SystemFormId").mkselectGet();
        }
        else {
            if ($.trim(formData.listTitle || '') == '') {
                Changjie.alert.error('请设置列表展示!');
                return false;
            }
        }

        var scheme = {
            title: formData.listTitle,
            content: ['', '', '']
        };
        scheme.content[0] = formData.listContent1;
        scheme.content[1] = formData.listContent2;
        scheme.content[2] = formData.listContent3;

        var schemeEntity = {
            F_Scheme: JSON.stringify(scheme)
        }


        // 提交数据
        var postData = {
            strEntity: JSON.stringify(entity),
            strSchemeEntity: JSON.stringify(schemeEntity),
            isProjectModule: isProjectModule
        };


        $.mkSaveForm(top.$.rootUrl + '/AppManager/FunctionManager/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };

    page.init();
}