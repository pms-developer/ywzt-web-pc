﻿/*
 * 日 期：2017.04.17
 * 描 述：流程模板管理	
 */
var refreshGirdData; // 更新数据
var projectFunctionList = {};//项目功能集合
var bootstrap = function ($, Changjie) {
    "use strict";
    var type = '';
    var isProjectModule="0"
    var page = {
        init: function () {
            page.loadProjectFunction();
            page.initGrid();
            page.bind();
        },
        bind: function () {
            // 左侧数据加载
            $('#left_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/DataItem/GetDetailTree',
                param: { itemCode: 'function' },
                nodeClick: function (item) {
                    type = item.value;
                    if (type == "projectCenter") {
                        $('#add').attr("disabled", true);
                        $('#delete').attr("disabled", true);
                        $('#disabled').attr("disabled", true);
                        $('#enable').attr("disabled", true);
                        isProjectModule = "1";
                    }
                    else {
                        $('#add').attr("disabled", false);
                        $('#delete').attr("disabled", false);
                        $('#disabled').attr("disabled", false);
                        $('#enable').attr("disabled", false);
                        isProjectModule = "0";
                    }
                    if (item.parent) {
                        if (item.parent.id == "projectCenter") {
                            type = item.id;
                            $('#add').attr("disabled", true);
                            $('#delete').attr("disabled", true);
                            $('#disabled').attr("disabled", true);
                            $('#enable').attr("disabled", true);
                            isProjectModule = "1";
                        } else {
                            $('#add').attr("disabled", false);
                            $('#delete').attr("disabled", false);
                            $('#disabled').attr("disabled", false);
                            $('#enable').attr("disabled", false);
                            isProjectModule = "0";
                        }
                    }
                    $('#titleinfo').text(item.text);
                    page.search();
                }
            });
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'Form',
                    title: '新增移动功能',
                    url: top.$.rootUrl + '/AppManager/FunctionManager/Form?type=' + type + "&isProjectModule=" + isProjectModule,
                    width: 600,
                    height: 450,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id'); 
                var formId = $('#gridtable').jfGridValue('FormId'); 
                var isSystem = $("#gridtable").jfGridValue("F_IsSystem");
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'Form',
                        title: '编辑移动功能',
                        url: top.$.rootUrl + '/AppManager/FunctionManager/Form?type=' + type + '&keyValue=' + keyValue + "&isProjectModule=" + isProjectModule + "&formId=" + formId + "&isSystem=" + isSystem,
                        width: 600,
                        height: 450,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该功能！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/AppManager/FunctionManager/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            
            // 启用
            $('#enable').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                var enabledMark = $('#gridtable').jfGridValue('F_EnabledMark');
                if (Changjie.checkrow(keyValue)) {
                    if (enabledMark != 1) {
                        Changjie.layerConfirm('是否启用该功能！', function (res) {
                            if (res) {
                                Changjie.postForm(top.$.rootUrl + '/AppManager/FunctionManager/UpDateSate', { keyValue: keyValue, state: 1 }, function () {
                                    refreshGirdData();
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning('该功能已启用!');
                    }
                }
            });
            // 禁用
            $('#disabled').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_Id');
                var enabledMark = $('#gridtable').jfGridValue('F_EnabledMark');

                if (Changjie.checkrow(keyValue)) {
                    if (enabledMark == 1) {
                        Changjie.layerConfirm('是否禁用该功能！', function (res) {
                            if (res) {
                                Changjie.postForm(top.$.rootUrl + '/AppManager/FunctionManager/UpDateSate', { keyValue: keyValue, state: 0 }, function () {
                                    refreshGirdData();
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning('该功能已禁用!');
                    }
                }
            });
           
            /*分类管理*/
            $('#category').on('click', function () {
                Changjie.layerForm({
                    id: 'ClassifyIndex',
                    title: '分类管理',
                    url: top.$.rootUrl + '/SystemModule/DataItem/DetailIndex?itemCode=function',
                    width: 800,
                    height: 500,
                    maxmin: true,
                    btn: null,
                    end: function () {
                        Changjie.clientdata.update('dataItem');
                        location.reload();
                    }
                });
            });
        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/AppManager/FunctionManager/GetPageList',
                headData: [
                    { label: "功能名称", name: "F_Name", width: 150, align: "left" },
                    {
                        label: "分类", name: "F_Type", width: 120, align: "left",
                        formatterAsync: function (callback, value, row) {
                            if (!row.IsProjectModule) {
                                Changjie.clientdata.getAsync('dataItem', {
                                    key: value,
                                    code: 'function',
                                    callback: function (_data) {
                                        callback(_data.text);
                                    }
                                });
                            }
                            else {
                                callback(row.TypeName);
                            }
                        }
                    },
                    {
                        label: "类型", name: "F_IsSystem", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            if (cellvalue == 1) {
                                return '<span class=\"label label-info\" style=\"cursor: pointer;\">代码开发</span>';
                            } else {
                                return '<span class=\"label label-warning\" style=\"cursor: pointer;\">自定义表单</span>';
                            }
                        }
                    },
                    {
                        label: "状态", name: "F_EnabledMark", width: 60, align: "center",
                        formatter: function (cellvalue, row) {
                            if (cellvalue == 1) {
                                return '<span class=\"label label-success\" style=\"cursor: pointer;\">正常</span>';
                            } else if (cellvalue == 0) {
                                return '<span class=\"label label-default\" style=\"cursor: pointer;\">禁用</span>';
                            }
                        }
                    },
                    { label: "编辑人", name: "CreationName", width: 120, align: "left" },
                    {
                        label: "编辑时间", name: "CreationDate", width: 150, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        },
                        sort: true
                    }
                ],
                mainId: 'F_Id',
                sidx:'F_SortCode',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.type = type;
            if (isProjectModule == "0") {
                $('#gridtable').jfGridSet('reload', param);
            }
            else {
                $("#gridtable").jfGridSet("refreshdata", projectFunctionList[type]);
            }
        },
        loadProjectFunction: function () {
            var url = top.$.rootUrl + '/SystemModule/Module/GetProjectModuleListToApp';
            var data = Changjie.httpGet(url).data;
            var menuList = data.menuList;
            var functionList = data.functionList;
            $.each(menuList, function (index, menu) {
                projectFunctionList[menu.F_ModuleId] = [];
                for (var _i in functionList) {
                    var func = functionList[_i];
                    if (func.F_ParentId != menu.F_ModuleId) continue;
                    
                    var info = {
                        F_Id: func.F_ModuleId,
                        F_Name: func.F_FullName,
                        F_EnabledMark: 1,
                        F_Type: menu.F_ModuleId,
                        TypeName:menu.F_FullName,
                        IsProjectModule:1
                    };
                    if (func.F_UrlAddress && func.F_UrlAddress.indexOf("FormModule/FormRelation/PreviewIndex") > -1) {
                        info.F_IsSystem = 2;

                        info.FormId = Changjie.getUrlParam(func.F_UrlAddress, "formId");
                    }
                    else {
                        info.F_IsSystem = 1;
                    }
                    projectFunctionList[menu.F_ModuleId].push(info);
                }
            });
        }
    };

    // 保存数据后回调刷新
    refreshGirdData = function () {
        page.search();
    }

    page.init();
}


