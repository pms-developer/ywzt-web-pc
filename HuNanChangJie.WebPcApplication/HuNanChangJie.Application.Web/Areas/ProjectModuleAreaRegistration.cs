﻿using System.Web.Mvc;
namespace HuNanChangJie.Application.Web.Areas.ProjectModule
{
    public class ProjectModuleAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ProjectModule"; 
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ProjectModule_default", 
                "ProjectModule/{controller}/{action}/{id}", 
                             new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
