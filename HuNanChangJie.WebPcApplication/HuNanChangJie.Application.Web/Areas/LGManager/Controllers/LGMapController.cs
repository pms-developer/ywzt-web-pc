﻿using HuNanChangJie.Application.TwoDevelopment.LGManager;
using HuNanChangJie.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.LGManager.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-09-29 14:51
    /// 描 述：语言映射
    /// </summary>
    public class LGMapController : MvcControllerBase
    {
        private LGMapIBLL lGMapIBLL = new LGMapBLL();
        private LGTypeIBLL lGTypeIBLL = new LGTypeBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        public ActionResult AddForm(string keyValue)
        {
            return View();
        }

        /// <summary>
        /// 字典翻译
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult DataItemLG()
        {
            return View();
        }

        /// <summary>
        /// Systems the module lg. 系统模块翻译
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult SystemModuleLG()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson, string typeList)
        {
            //节后再说吧
            //这里很复杂的写法呢
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = lGMapIBLL.GetPageList(paginationobj, queryJson, typeList);
            //var mkltMapEntities = data as LR_Lg_MapEntity[] ?? data.ToArray();
            //var datagroup = mkltMapEntities.GroupBy(p => p.F_TypeCode);
            //List<object> facade= new List<object>();

            //foreach (IGrouping<string, LR_Lg_MapEntity> lgMapEntities in datagroup)
            //{
            //  var newlist=  typeList.Split(',');
            //  Dictionary<string,string> newobject = new Dictionary<string, string>();
            //    foreach (var s in newlist)
            //    {
            //        newobject.Add(s, lgMapEntities.First(p => p.F_TypeCode==s).F_Name); 
            //    }
            //    newobject.Add("f_code", lgMapEntities.Key);
            //    newobject.Add("rownum", lgMapEntities.Key);


            //}
            if (paginationobj == null)
            {
                var jsonData = new
                {
                    rows = data
                };
                return Success(jsonData);
            }
            else
            {
                var jsonData = new
                {
                    rows = data,
                    total = paginationobj.total,
                    page = paginationobj.page,
                    records = paginationobj.records
                };
                return Success(jsonData);
            }
            //   return null;
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var LR_Lg_MapData = lGMapIBLL.GetLR_Lg_MapEntity(keyValue);
            var jsonData = new
            {
                LR_Lg_Map = LR_Lg_MapData,
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetListByNameAndType(string keyValue, string typeCode)
        {
            var lgType = lGTypeIBLL.GetList((new LgTypeEntity { F_Code= typeCode, F_IsMain = 1 }).ToJson());
            if (lgType.Count() > 0)
            {
                var lgMapData = lGMapIBLL.GetList((new LgMapEntity { F_TypeCode = typeCode, F_Name = keyValue }).ToJson());
                return Success(lgMapData);
            }
            else
            {
                return Success(null);
            }
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetLgMapListByMainName(string keyValue)
        {
            var lgType = lGTypeIBLL.GetList((new LgTypeEntity { F_IsMain = 1 }).ToJson()).FirstOrDefault();
            if (lgType != null)
            {
                var lgMap = lGMapIBLL.GetList((new LgMapEntity { F_Name = keyValue, F_TypeCode = lgType.F_Code }).ToJson()).FirstOrDefault();
                if (lgMap != null)
                {
                    var lgMapList = lGMapIBLL.GetList((new LgMapEntity { F_Code = lgMap.F_Code }).ToJson());
                    return Success(lgMapList);
                }
                else
                {
                    return Success(null);
                }
            }
            else
            {
                return Success(null);
            }
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            lGMapIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>


        #endregion

        #region    扩展

        public ActionResult GetLanguageByCode(string typeCode, bool isMain, string ver)
        {
            var qu = new
            {
                F_TypeCode = typeCode,
                F_isMain = isMain
            };

            var tcList = lGMapIBLL.GetList(qu.ToJson());

            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            foreach (var item in tcList)
            {
                try
                {
                    if (isMain)
                    {
                        keyValuePairs.Add(item.F_Name, item.F_Code);

                    }
                    else
                    {
                        keyValuePairs.Add(item.F_Code, item.F_Name);
                    }
                }
                catch (Exception)
                {

                    //throw;
                }


            }
            var resData = new { data = keyValuePairs };
            return Success(resData);
        }

        public ActionResult GetList(string queryJson)
        {
            IEnumerable<LgMapEntity> modellist = lGMapIBLL.GetList(queryJson);
            return Success(modellist);
        }


        /// <summary>
        /// Saves the muti lg form.
        /// </summary>
        /// <param name="nameList">The name list.</param>
        /// <param name="newNameList">The new name list.</param>
        /// <param name="code">The code.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string nameList, string newNameList, string code)
        {
            try
            {
                if (string.IsNullOrEmpty(code))
                {
                    string mycode = Guid.NewGuid().ToString();
                    var mydict = newNameList.ToObject<Dictionary<string, string>>();
                    //mydict.Remove("f_code");
                    foreach (var kvalue in mydict)
                    {

                        LgMapEntity mkltMap = new LgMapEntity();
                        mkltMap.F_TypeCode = kvalue.Key;
                        mkltMap.F_Code = mycode;
                        mkltMap.F_Name = kvalue.Value;
                        lGMapIBLL.SaveEntity("", mkltMap);
                    }
                }
                else
                {
                    var list = lGMapIBLL.GetList(new { F_Code = code }.ToJson());
                    var mydict = newNameList.ToObject<Dictionary<string, string>>();
                    //mydict.Remove("f_code");
                    if (mydict.Count != list.Count())
                    {
                        foreach (var mydictKey in mydict.Keys)
                        {
                            if (list.Count(p => p.F_TypeCode.ToLower() == mydictKey.ToLower()) == 0)
                            {
                                LgMapEntity lge = new LgMapEntity();
                                lge.F_TypeCode = mydictKey;
                                lge.F_Code = code;
                                lge.F_Name = mydict[mydictKey];
                                lGMapIBLL.SaveEntity("", lge);

                            }
                        }
                    }
                    foreach (LgMapEntity mapEntity in list)
                    {
                        var objNewName = newNameList.ToObject<JObject>();

                        if (objNewName.ContainsKey(mapEntity.F_TypeCode))
                        {
                            mapEntity.F_Name = objNewName[mapEntity.F_TypeCode].ToString();
                            lGMapIBLL.SaveEntity(mapEntity.F_Id, mapEntity);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Fail("保存异常！");

            }
            return Success("保存成功");
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GenerateJS()
        {
            try
            {
                StringBuilder strFileText = new StringBuilder();
                strFileText.Append(@"var languageType = 'chinese';
if (getCookie('languagetype') != null && getCookie('languagetype') != '') {
    languageType = getCookie('languagetype');
}

");

                strFileText.Append("var languagedata = ");
                var lgType = lGTypeIBLL.GetList((new LgTypeEntity { F_IsMain=1}).ToJson()).FirstOrDefault();
                var list = lGMapIBLL.GetList((new { }).ToJson());
                if (list.Count() > 0 && lgType!=null)
                {
                    var listMain = list.Where(p => p.F_TypeCode == lgType.F_Code);

                    var data = list.Select(o => new { type = o.F_TypeCode, key = listMain.FirstOrDefault(p=>p.F_Code==o.F_Code).F_Name, value = o.F_Name });
                    strFileText.Append(data.ToJson());
                }
                strFileText.Append(@" 

var data = [];


function InitLanguage() {
    data = $.grep(languagedata, function (item) {
        return item.type == languageType;
    });

    //初始化标签，按钮多语言显示
    var elements = $('.language');

    elements.each(function (index, element) {
        //console.log($(element).html()); // 打印每个元素的HTML内容
        data.forEach(function (itemData) {
            var text = $(element).html();
            if (text.indexOf(itemData.key) > -1) {
                text=text.replace(itemData.key, itemData.value);
                $(element).html(text);
            }
        })

    });

    //多语言初始化列表，表头
    var gridheads = $('.jfgrid-head').find(""span"");
    gridheads.each(function (index, gridhead) {
        //console.log($(gridhead).html());
        data.forEach(function (itemData) {
            if ($(gridhead).html() == itemData.key) {
                $(gridhead).html(itemData.value);
            }
        })
    });

    //多语言初始化菜单
    var menuTitles = $('.mk-frame-menu').find(""li"");
    var menus = $('.mk-menu-item-text');
    menus.each(function (index, menu) {
        console.log($(menu).html());
        data.forEach(function (itemData) {
            if ($(menu).html() == itemData.key) {
                $(menu).html(itemData.value);
            }
        })
    });

    menuTitles.each(function (index, menuTitle) {
        console.log($(menuTitle).attr('title'));
        data.forEach(function (itemData) {
            if ($(menuTitle).attr('title') == itemData.key) {
                $(menuTitle).attr('title',itemData.value);
            }
        })
    });

    //多语言初始化title
    var title = $('title').text();
    data.forEach(function (itemData) {
        if (title == itemData.key) {
            $('title').text(itemData.value);
        }
    });
}

function getLanguageValue(key) {
    var value = key;
    data.forEach(function (itemData) {
        if (key == itemData.key) {
            value=itemData.value;
        }
    })
    return value;
}
function setCookie(name, value, days) {
    var expires = """";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = ""; expires="" + date.toUTCString();
    }
    document.cookie = name + ""="" + (value || """") + expires + ""; path=/"";
}

function getCookie(name) {
    var nameEQ = name + ""="";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}");

                string filePath = AppDomain.CurrentDomain.BaseDirectory + "//Content//language//language.js";

                //string filePath = Directory.GetCurrentDirectory() + "//Content//language//language.js";
                
                // 创建文件
                FileStream fileStream = System.IO.File.Create(filePath);

                // 写入数据
                using (StreamWriter writer = new StreamWriter(fileStream))
                {
                    writer.Write(strFileText.ToString());
                }
                return Success("生成JS文件成功");
            }
            catch (Exception e)
            {
                return Fail("生成JS文件失败，错误：" + e.Message);
            }
        }

        public ActionResult InitLG()
        {
            var allfiles = System.IO.Directory.GetFiles("D:\\dataitem\\", "*.txt");
            foreach (string file in allfiles)
            {
                string jsonData = System.IO.File.ReadAllText(file, Encoding.Default);

                var mydata = jsonData.ToObject<ReqParameter<FacadeNameListForm>>();

                foreach (var d in mydata.data.rows)
                {
                    try
                    {
                        lGMapIBLL.SaveEntity("", new LgMapEntity()
                        {
                            F_Code = d.f_code,
                            F_Id = d.f_id,
                            F_TypeCode = d.f_typecode,
                            F_Name = d.f_name
                        });
                    }
                    catch (Exception e)
                    {


                    }

                }

            }

            return Success("成了啊");
        }

        public class FacadeNameListForm
        {
            public List<MyRow> rows { get; set; }
        }

        public class MyRow
        {
            public string f_code { get; set; }
            public string f_id { get; set; }
            public string f_name { get; set; }
            public string f_typecode { get; set; }

        }
    }

    #endregion
}


