﻿var selectedRow;
var refreshGirdData;
var formHeight;
var bootstrap = function (a, b) {
    var c = {
        init: function () {
            c.initGrid();
            c.bind()
        },
        bind: function () {
            a("#btn_Search").on("click",
                function () {
                    var d = a("#txt_Keyword").val();
                    c.search({
                        F_Name: d,
                    })
                });
            a("#refresh").on("click",
                function () {
                    location.reload()
                });
            a("#add").on("click",
                function () {
                    selectedRow = null;
                    b.layerForm({
                        id: "form",
                        title: "新增",
                        url: top.$.rootUrl + "/LGManager/LGMap/Form",
                        width: 400,
                        height: formHeight,
                        callBack: function (d) {
                            return top[d].acceptClick(c.search)
                        }
                    })
                });
            a("#edit").on("click",
                function () {
                    selectedRow = a("#gridtable").AgGridGet("rowdata");
                    if (b.checkrow(selectedRow)) {
                        b.layerForm({
                            id: "form",
                            title: "编辑",
                            url: top.$.rootUrl + "/LGManager/LGMap/Form?keyValue=" + selectedRow.f_code,
                            width: 400,
                            height: formHeight,
                            callBack: function (d) {
                                return top[d].acceptClick(c.search)
                            }
                        })
                    }
                });
            a("#delete").on("click",
                function () {
                    selectedRow = a("#gridtable").AgGridGet("rowdata");
                    if (b.checkrow(selectedRow)) {
                        b.layerConfirm("是否确认删除该项！",
                            function (d) {
                                if (d) {
                                    b.deleteForm(top.$.rootUrl + "/LGManager/LGMap/DeleteForm",
                                        {
                                            keyValue: selectedRow.f_code
                                        },
                                        function () {
                                            c.search();
                                        });
                                }
                            });
                    }
                });
            a("#generateJS").on("click", function () {
                //$('script[src="~/Content/language/language.js"]').remove();
                b.httpAsyncGet(top.$.rootUrl + "/LGManager/LGMap/GenerateJS", function (g) {
                    if (g.code == 200) {
                        b.alert.info(g.info);
                        //top.b.alert.info(g.info);
                    } else {
                        b.alert.error(g.info);
                        //top.b.alert.info(g.info);
                    }
                })
            })
        },
        initGrid: function () {
            var d = [];
            //var f = {
            //    headerName: 'Key',
            //    field: 'f_code',
            //    width: 200,

            //};
            //d.push(f);

            b.httpAsyncGet(top.$.rootUrl + "/LGManager/LGType/GetList",
                function (g) {
                    if (g.data) {
                        var h = [];
                        for (var e = 0; e < g.data.length; e++) {
                            var f = {
                                headerName: g.data[e].F_Name,
                                field: g.data[e].F_Code.toLowerCase(),
                                width: 200,

                            };
                            d.push(f);
                            h.push(g.data[e].F_Code)
                        }
                        a("#gridtable").AgGrid({
                            url: top.$.rootUrl + "/LGManager/LGMap/GetPageList?typeList=" + h,
                            headData: d,
                            isPage: false,
                        });
                        c.search();
                        if (g.data.length <= 3) {
                            formHeight = 230;
                        } else {
                            formHeight = 230 + (g.data.length - 3) * 40;
                        }
                    }
                });
        },
        search: function (d) {
            d = d || {};
            a("#gridtable").AgGridSet("reload",
                {
                    queryJson: JSON.stringify(d)
                });
        }
    };
    refreshGirdData = function () {
        c.search();
    };
    c.init();
};