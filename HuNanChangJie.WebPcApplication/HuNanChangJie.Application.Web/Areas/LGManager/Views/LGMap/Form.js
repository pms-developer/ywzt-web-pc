﻿var acceptClick;
var mainType;
var keyValue = request("keyValue");
var bootstrap = function (a, b) {
    var d = b.frameTab.currentIframe().selectedRow;

    var c = {
        init: function () {
            c.bind()
        },
        bind: function () {
            var lgMapList = [];
            if (!!keyValue) {
                b.httpAsyncGet(top.$.rootUrl + "/LGManager/LGMap/GetLgMapListByMainName?keyValue=" + keyValue, function (rest) {
                    if (rest.data) {
                        lgMapList = rest.data;
                    }
                });

            }
            var e = [];
            b.httpAsyncGet(top.$.rootUrl + "/LGManager/LGType/GetList",
                function (h) {
                    if (h.data) {
                        for (var g = 0; g < h.data.length; g++) {
                            if (lgMapList.length > 0) {
                                var isendlt = false;
                                for (var i = 0; i < lgMapList.length; i++) {
                                    if (h.data[g].F_Code.toLowerCase() == lgMapList[i].F_TypeCode.toLowerCase()) {
                                        isendlt = true;
                                        var f = '<div class="col-xs-12 mk-form-item"> <div class="mk-form-item-title">' + h.data[g].F_Name + '<font face="宋体">*</font></div> <input id="' + h.data[g].F_Code.toLowerCase() + '" type="text" class="form-control" isvalid="yes" checkexpession="NotNull" value="' + lgMapList[i].F_Name + '" /> </div>';
                                        e.push(f)
                                    }
                                }
                                if (!isendlt) {
                                    var f = '<div class="col-xs-12 mk-form-item"> <div class="mk-form-item-title">' + h.data[g].F_Name + '<font face="宋体">*</font></div> <input id="' + h.data[g].F_Code.toLowerCase() + '" type="text" class="form-control" isvalid="yes" checkexpession="NotNull" /> </div>';
                                    e.push(f)
                                }
                            } else {
                                if (!!keyValue) {
                                    if (h.data[g].F_IsMain == 1) {
                                        var f = '<div class="col-xs-12 mk-form-item"> <div class="mk-form-item-title">' + h.data[g].F_Name + '<font face="宋体">*</font></div> <input id="' + h.data[g].F_Code.toLowerCase() + '" type="text" class="form-control" isvalid="yes" checkexpession="NotNull" value="' + keyValue + '" /> </div>';
                                        e.push(f)
                                    } else {
                                        var f = '<div class="col-xs-12 mk-form-item"> <div class="mk-form-item-title">' + h.data[g].F_Name + '<font face="宋体">*</font></div> <input id="' + h.data[g].F_Code.toLowerCase() + '" type="text" class="form-control" isvalid="yes" checkexpession="NotNull" /> </div>';
                                        e.push(f)
                                    }
                                } else {

                                    var f = '<div class="col-xs-12 mk-form-item"> <div class="mk-form-item-title">' + h.data[g].F_Name + '<font face="宋体">*</font></div> <input id="' + h.data[g].F_Code.toLowerCase() + '" type="text" class="form-control" isvalid="yes" checkexpession="NotNull" /> </div>';
                                    e.push(f)
                                }
                            }
                        }
                        mainType = h.data[0].F_Code.toLowerCase();
                        a("#form .mk-form-item:last").parent().append(e);
                        a("#form .mk-form-item:first").remove()
                    }
                    if (!!d) {
                        a("#form").mkSetFormData(d)
                    } else {
                        d = a("#form").mkGetFormData();
                        if (lgMapList.length > 0)
                            d.f_code = lgMapList[0].F_Code;
                    }
                })

        }
    };
    acceptClick = function (e) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var h = "";
        var f = "";
        if (!!d) {
            f = d.f_code;
            delete d.f_code;
            h = JSON.stringify(d)
        }
        var g = a("#form").mkGetFormData();
        var i = JSON.stringify(g);
        if (!keyValue) {
            b.httpAsyncGet(top.$.rootUrl + "/LGManager/LGMap/GetListByNameAndType?keyValue=" + g[mainType] + "&typeCode=" + mainType,
                function (j) {
                    if (j.data.length != 0) {
                        b.alert.warning("主语言项不能重复！");
                        return false
                    } else {
                        a.mkSaveForm(top.$.rootUrl + "/LGManager/LGMap/SaveForm?nameList=" + h + "&newNameList=" + i + "&code=" + f, {},
                            function (k) {
                                if (!!e) {
                                    e()
                                }
                            })
                    }
                })
        } else {
            if (g[mainType] != d[mainType]) {
                b.httpAsyncGet(top.$.rootUrl + "/LGManager/LGMap/GetListByNameAndType?keyValue=" + g[mainType] + "&typeCode=" + mainType,
                    function (j) {
                        if (j.data.length != 0) {
                            b.alert.warning("主语言项不能重复！");
                            return false
                        } else {
                            a.mkSaveForm(top.$.rootUrl + "/LGManager/LGMap/SaveForm?nameList=" + h + "&newNameList=" + i + "&code=" + f, {},
                                function (k) {
                                    if (!!e) {
                                        e()
                                    }
                                })
                        }
                    })
            } else {
                a.mkSaveForm(top.$.rootUrl + "/LGManager/LGMap/SaveForm?nameList=" + h + "&newNameList=" + i + "&code=" + f, {},
                    function (j) {
                        if (!!e) {
                            e()
                        }
                    })
            }
        }
    };
    c.init()
};