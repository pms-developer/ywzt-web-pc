﻿var acceptClick;
var keyValue = request("keyValue");
var bootstrap = function (a, b) {
    var d = b.frameTab.currentIframe().selectedRow;
    var c = {
        init: function () {
            c.bind();
        },
        bind: function () {
            var e = [];
            b.httpAsyncGet(top.$.rootUrl + "/LGManager/LGType/GetList",
                function(h) {
                    if (h.data) {
                        for (var g = 0; g < h.data.length; g++) {
                            var f = '<div class="col-xs-12 mk-form-item"> <div class="mk-form-item-title language">' +
                                h.data[g].F_Name +
                                '<font face="宋体">*</font></div> <input id="' +
                                h.data[g].F_Code +
                                '" type="text" class="form-control" isvalid="yes" checkexpession="NotNull" /> </div>';
                            e.push(f);
                        }
                        a("#form .mk-form-item:last").parent().append(e);
                        a("#form .mk-form-item:first").remove();
                    }
                    if (!!d) {
                        a("#form").mkSetFormData(d);
                    }
                    a("#" + keyValue).attr("disabled", "disabled");
                });
        }
    };
    acceptClick = function(e) {
        if (!a("#form").mkValidform()) {
            return false;
        }
        var g = "";
        var f = "";
        if (!!d) {
            f = d.F_Code;
            delete d.F_Code;
            g = JSON.stringify(d);
        }
        var h = JSON.stringify(a("#form").mkGetFormData());
        a.mkSaveForm(top.$.rootUrl + "/LGManager/LGMap/SaveForm?nameList=" + g + "&newNameList=" + h + "&code=" + f,
            {},
            function(i) {
                if (!!e) {
                    e();
                }
            });
    };
    c.init();
};