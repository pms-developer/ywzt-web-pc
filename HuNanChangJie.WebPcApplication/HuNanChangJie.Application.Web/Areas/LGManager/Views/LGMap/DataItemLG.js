﻿var refreshGirdData;
var selectedRow;
var formHeight;
var keyValue;
var bootstrap = function ($, Changjie) {
    var classify_itemCode = "";
    var page = {
        init: function () {
            page.inittree();
            page.initGrid();
            page.bind();
        }, bind: function () {
            $("#btn_Search").on("click", function () {
                var keyword = $("#txt_Keyword").val();
                page.search({
                    keyword: keyword
                });
            });
            $("#refresh").on("click", function () {
                location.reload();
            });
            $("#edit").on("click",
                function () {
                    selectedRow = $("#gridtable").AgGridGet("rowdata");
                    if (Changjie.checkrow(selectedRow)) {
                        Changjie.layerForm({
                            id: "form",
                            title: "编辑",
                            url: top.$.rootUrl + "/LGManager/LGMap/AddForm?keyValue=" + keyValue,
                            width: 400,
                            height: formHeight,
                            callBack: function (e) {
                                return top[e].acceptClick(page.search);
                            }
                        });
                    }
                });
        },
        inittree: function () {


            $("#left_tree").mktree({
                url: top.$.rootUrl + "/SystemModule/DataItem/GetClassifyTree",
                nodeClick: function (e) {
                    classify_itemCode = e.value;
                    $("#titleinfo").text(e.text + "(" + classify_itemCode + ")");
                    page.search();
                }
            });




        }, initGrid: function () {
            var headers = [];
            Changjie.httpAsyncGet(top.$.rootUrl + "/LGManager/LGType/GetList",
                function (data) {
                    if (data.data) {
                        headers.push({
                            headerName: "项目值",
                            field: data.data[0].F_Code,
                            width: 200
                           
                        });
                        keyValue = data.data[0].F_Code;
                        for (var f = 1; f < data.data.length; f++) {
                            var g = {
                                headerName: data.data[f].F_Name,
                                field: data.data[f].F_Code,
                                width: 200
                              
                            };
                            headers.push(g);
                        }
                        $("#gridtable").AgGrid({
                            headData: headers,
                            rowData:[]
                        });
                        page.search();
                        if (data.data.length <= 3) {
                            formHeight = 230;
                        } else {
                            formHeight = 230 + (data.data.length - 3) * 40;
                        }
                    }
                });
        }, search: function (f) {
            var g = [];
            var e = {};
            Changjie.httpAsyncGet(top.$.rootUrl + "/SystemModule/DataItem/GetDetailList?itemCode=" + classify_itemCode,
                function (h) {
                    Changjie.httpAsyncGet(top.$.rootUrl + "/LGManager/LGMap/GetList",
                        function (n) {
                            if (h.data && n.data) {
                                for (var k = 0; k < h.data.length; k++) {
                                    var o = n.data.find(function (i) {
                                        return i.F_Name == h.data[k].F_ItemName;
                                    });
                                    if (typeof o != "undefined") {
                                        var m = n.data.filter(function (i) {
                                            return i.F_Code == o.F_Code;
                                        });
                                        for (var l = 0; l < m.length; l++) {
                                            e[m[l].F_TypeCode] = m[l].F_Name;
                                            e.F_Code = m[l].F_Code;
                                        }
                                    } else {
                                        e[keyValue] = h.data[k].F_ItemName;
                                        e.F_Code = "";
                                    }
                                    g.push(e);
                                    e = {}
                                }
                                $("#gridtable").AgGridSet("refreshdata", g);
                                g = [];
                            }
                        });
                });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};