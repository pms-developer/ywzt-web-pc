﻿/* 
 * 创建人：超级管理员
 * 日  期：2018-09-29 15:01
 * 描  述：多语言映射
 */
var refreshGirdData;
var selectedRow;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {


            $("#btn_Search").on("click", function () {
                var keyword = a("#txt_Keyword").val();
                page.search({
                    keyword: keyword
                });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                selectedRow = null;
                Changjie.layerForm({
                    id: "form",
                    title: "新增",
                    url: top.$.rootUrl + "/LGManager/LGType/Form",
                    width: 300,
                    height: 180,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: "form",
                        title: "编辑",
                        url: top.$.rootUrl + "/LGManager/LGType/Form?keyValue=" + keyValue,
                        width: 300,
                        height: 180,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    selectedRow = $("#gridtable").AgGridGet("rowdata");
                    if (selectedRow.F_IsMain === 1) {
                        Changjie.alert.warning("主语言不能删除！");
                        return false;
                    }
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/LGManager/LGType/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/LGManager/LGType/GetPageList',
                headData: [{
                    label: "名称",
                    name: "F_Name",
                    width: 200,
                  
                }, {
                    label: "编码",
                    name: "F_Code",
                    width: 300,
                   
                }, {
                    label: "主语言",
                    name: "F_IsMain",
                    width: 80,
                   
                    cellRenderer: function (param) {
                        if (param.value == 1) {
                            return '<span class="label label-info" style="cursor: pointer;">是</span>';
                        } else {
                            return '<span class="label label-danger" style="cursor: pointer;">否</span>';
                        }
                    }
                }],
                mainId:'F_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
