﻿/* 
 * 创建人：超级管理员
 * 日  期：2018-09-29 15:01
 * 描  述：多语言映射
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
        },
        bind: function () {
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/LGManager/LGType/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id]);
                        }
                        else {
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData())
        };
        $.mkSaveForm(top.$.rootUrl + '/LGManager/LGType/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
