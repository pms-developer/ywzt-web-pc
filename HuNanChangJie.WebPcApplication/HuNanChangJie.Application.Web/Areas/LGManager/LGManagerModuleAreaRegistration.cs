﻿using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.LGManager
{
    public class LGManagerModuleAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LGManager";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LGManagerModule_default",
                "LGManager/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}