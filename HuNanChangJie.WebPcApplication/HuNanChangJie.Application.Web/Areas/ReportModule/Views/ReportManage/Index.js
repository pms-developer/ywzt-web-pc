﻿/*
 * 日 期：2017.11.11
 * 描 述：报表管理	
 */
var refreshGirdData; // 更新数据
var formId = request("formId");
var selectedRow;
var bootstrap = function ($, Changjie) {
    "use strict";
    var mygridOptions = {

        rowSelection: 'single',
        pagination: true,
        paginationAutoPageSize: true
    };
    $.extend(mygridOptions, gridOptions);
    var page = {
        init: function () {
            //page.initGrid();
            page.initAgGrid();
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                selectedRow = null;
                Changjie.layerForm({
                    id: 'form',
                    title: '添加报表',
                    url: top.$.rootUrl + '/ReportModule/ReportManage/Form',
                    width: 900,
                    height: 700,
                    btn: null
                });
            });
            // 编辑
            $('#edit').on('click', function () {

                var selectedrows = $('#gridtable').AgGridGetSelectedRows();
                if (selectedrows.length == 0) {

                    top.Changjie.alert.warning("您未选中任何行");
                    return;
                }
                //selectedRow= $('#gridtable').jfGridGet('rowdata');
                selectedRow = $('#gridtable').AgGridGet('rowdata');;

                var keyValue = $('#gridtable').AgGridValue('F_TempId');

                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑报表',
                        url: top.$.rootUrl + '/ReportModule/ReportManage/Form',
                        width: 900,
                        height: 700,
                        btn: null
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_TempId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/ReportModule/ReportManage/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            //预览
            $('#preview').on('click', function () {
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                var keyValue = $('#gridtable').AgGridValue('F_TempId');
               
                var strSql = JSON.parse(selectedRow.F_ParamJson).F_ChartSqlString;
                
                if (strSql.indexOf('@') > 0) {
                    try {
                     
                        var testargs = $('#gridtable').AgGridValue('F_ArgsTestJson');

                        var obj = JSON.parse(testargs.trim());

                        if (typeof obj == 'object' && obj) {
                            Changjie.frameTab.open({ F_ModuleId: 'preview_' + keyValue, F_Icon: 'fa fa fa-eye', F_FullName: '预览报表', F_UrlAddress: '/ReportModule/ReportManage/Preview?reportId=' + keyValue + '&type=preview&queryJson=' + encodeURIComponent(testargs, 'utf-8') + '' });
                        } else {
                            Changjie.alert.error("格式不正确");
                        }
                    } catch (e) {
                        Changjie.alert.error("格式不正确");
                    }
                } else {
                    var keyValue = $('#gridtable').AgGridValue('F_TempId');
                    Changjie.frameTab.open({ F_ModuleId: 'preview_' + keyValue, F_Icon: 'fa fa fa-eye', F_FullName: '预览报表', F_UrlAddress: '/ReportModule/ReportManage/Preview?reportId=' + keyValue + '&type=preview' });
                }



            });
        },

        initAgGrid: function () {
            // specify the columns
            $('#gridtable').AgGrid({
                url: top.$.rootUrl + '/ReportModule/ReportManage/GetAGPageList',
                isPage: true,
                headData: [
                    { headerName: "主键", field: "F_TempId", hide: true },
                    {
                        headerName: "报表编号", field: "F_EnCode",
                        filter: "agTextColumnFilter",
                        filterParams: { apply: true }
                    },
                    {
                        headerName: "报表名称", field: "F_FullName",
                        filter: "agTextColumnFilter",
                        filterParams: { apply: true }
                    },
                    { headerName: "报表分类", field: "F_TempCategory" },
                    {
                        headerName: "报表风格", field: "F_TempStyle", width: 100,

                        cellStyle: { 'text-align': 'center' },
                        filter: "agNumberColumnFilter",
                        filterParams: { apply: true },
                        cellRenderer: function (params) {
 
                            return Changjie.clientdata.getSync('dataItem', {
                                key: params.value,
                                code: 'MyTempStyle'
                            });




                        }
                    },
                    {
                        headerName: "报表状态", field: "F_Status", width: 100,
                        cellStyle: { 'text-align': 'center' },
                        cellRenderer: function (params) {

                            if (params.value == 0) {
                                return '<span class="label label-default">待定</span>';
                            } else if (params.value == 1) {
                                return '<span class="label label-primary">草稿</span>';
                            } else if (params.value == 2) {
                                return '<span class="label label-info">已发布</span>';
                            }

                            return params.value;
                        }
                    },
                    {
                        headerName: "接口状态", field: "F_IsImpl", width: 100, cellStyle: { 'text-align': 'center' },
                        cellRenderer: function (params) {

                            if (params.value == 0) {
                                return '<span class="label label-default">无接口</span>';
                            } else if (params.value == 1) {
                                return '<span class="label label-primary">未开发</span>';
                            } else if (params.value == 2) {
                                return '<span class="label label-success">已开发</span>';
                            } else if (params.value == 3) {
                                return '<span class="label abel-danger">已发布</span>';
                            }

                            return params.value;
                        }
                    },
                    {
                        headerName: "创建时间", width: 120, field: "CreationDate",
                        filter: "agDateColumnFilter",
                        filterParams: { apply: true }
                    },
                    { headerName: "测试条件", width: 400, field: "F_ArgsTestJson" },
                    { headerName: "报表介绍", width: 400, field: "F_Description" }
                ],
                gridOptions:
                {
                    enableFilter: true,
                    onFilterChanged: function () {  },
                    onFilterModified: function () {  },
                    enableSorting: true,
                    multiSortKey: 'ctrl'
                }
            });

        },
        search: function (param) {
            $('#gridtable').AgGridSet('reload', param);
        }
    };
    // 保存数据后回调刷新
    refreshGirdData = function () {
        page.search();
    }
    page.init();
}


