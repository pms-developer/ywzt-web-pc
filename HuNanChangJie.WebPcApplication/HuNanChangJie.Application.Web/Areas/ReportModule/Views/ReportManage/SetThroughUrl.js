﻿var acceptClick;
var keyValue = request('keyValue');
var gridSetting = top[keyValue];

var bootstrap = function ($, Changjie) {
    //console.log(gridSetting);
    //初始化数据
    if (gridSetting.throughSettings) {
        if (gridSetting.throughSettings.jumpParam == "keyValue") {
            $("input[value='jumpType']").attr("checked", "checked");
            $("input[value='projectId']").removeAttr("checked");
        }
        else {
            $("input[value='projectId']").attr("checked", "checked");
            $("input[value='jumpType']").removeAttr("checked");
        }
        $("#jumpUrl").val(gridSetting.throughSettings.jumpUrl);
        $("#pName").val(gridSetting.throughSettings.pName);
        $("#targetTitle").val(gridSetting.throughSettings.targetTitle);
        $("#reportId").val(gridSetting.throughSettings.reportId);
        $("#queryJson").val(gridSetting.throughSettings.queryJson);
        $("#moduleId").val(gridSetting.throughSettings.moduleId);
        $("#formId").val(gridSetting.throughSettings.formId);
    }
    // 保存数据
    acceptClick = function (callBack) {
        var retValue = {};
        retValue.jumpParam = $("input[name='jumpType']:checked").val();
        retValue.jumpUrl = $("#jumpUrl").val();
        retValue.pName = $("#pName").val();
        retValue.targetTitle = $("#targetTitle").val();
        retValue.reportId = $("#reportId").val();
        retValue.queryJson = $("#queryJson").val();
        retValue.moduleId = $("#moduleId").val();
        retValue.formId = $("#formId").val();
        callBack(JSON.stringify(retValue));
        return true;
    };
}