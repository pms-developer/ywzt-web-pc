﻿var acceptClick;
var keyValue = request('keyValue');
var gridSetting = top[keyValue];
var bootstrap = function ($, Changjie) {
    "use strict";
    var row = gridSetting;
    var page = {
        init: function () {

            page.bind();
            page.initData();
        },
        bind: function () {
            $("#ControlType").mkDataItemSelect({ code: "ControlType" });
            $("#DropDownListType").mkselect({
                value: "type",
                text: "name",
                data: [
                    { type: "ShopList", name: "店铺列表" },
                    { type: "ContractList", name: "采购合同列表" },
                    { type: "SubContractList", name: "分包合同列表" },
                    { type: "CompanyList", name: "公司列表" },
                    { type: "DepartmentList", name: "部门列表" },
                    { type: "UserList", name: "用户列表" },
                    { type: "CustomerList", name: "客户列表" },
                    { type: "SupplierList", name: "供应商列表" },
                    { type: "SubcontractingList", name: "分包商列表" },
                ]
            });
            $("#DbType").mkselect({
                value: "type",
                text: "name",
                data: [
                    { type: "Date", name: "日期" },
                    { type: "Int32", name: "整型" },
                    { type: "Decimal", name: "十进制(Decimal)" },
                    { type: "String", name: "字符串" }
                ],
                dfvalue:"String"
            })
            $("#CompareType").mkDataItemSelect({ code: "CompareType" });
            $("#ValueType").mkselect({
                value: "type",
                text: "name",
                data: [
                    { type: "Input", name: "手动输入" },
                    { type: "System", name: "系统参数" },
                ],
                dfvalue:"Input"
            });
            $("#DefaultValue").mkselect({
                value: "id",
                text: "name",
                data: [{ id: "CurrentProject", name: "当前项目" }]
            });

            $("#ControlType").on("change", function () {
                var value = $(this).mkselectGet();
                if (value == "dropDownList") {
                    $("div[dropDownList]").show();
                    //显示是否多选选择框
                    //
                    $("div[multiSelect]").show();
                    $("div[textParameter]").show();
                }
                else {
                    $("div[dropDownList]").hide();
                    //隐藏是否多选选择框
                    //
                    $("div[multiSelect]").hide();
                    $("div[textParameter]").hide();
                    $("#DropDownListType").mkselectSet("");
                }
            })

            $("#ValueType").on("change", function () {
                var value = $(this).mkselectGet();
                if (value == "System") {
                    $("div[System]").show();
                }
                else {
                    $("div[System]").hide();
                    $("#DefaultValue").mkselectSet("");
                }
            });

        },
        initData: function () {
            if (row.querySettings) {
                var item = row.querySettings;
                $("#Name").val(item.name || row.name);
                $("#Field").val(item.field || row.field);
                if (item.ControlType) {
                    $("#ControlType").mkselectSet(item.ControlType);
                    if (item.ControlType == "dropDownList") {
                        $("div[dropDownList]").show();
                        $("div[multiSelect]").show();
                        $("div[textParameter]").show();
                    }
                    else {
                        $("div[dropDownList]").hide();
                        $("div[multiSelect]").hide();
                        $("div[textParameter]").hide();
                    }
                }
                else {
                    $("div[dropDownList]").hide();
                    $("div[multiSelect]").hide();
                    $("div[textParameter]").hide();
                }

                if (item.DropDownListType) {
                    $("#DropDownListType").mkselectSet(item.DropDownListType);
                }

                //设置多选性质
                //
                if (item.isMultiSelect==1) {
                    //alert(1);
                    $("#isMultiSelect").attr('checked', true);
                } else {
                    $("#isMultiSelect").attr('checked', false);
                }
                //后天文本传参
                if (item.isTextParameter==1) {
                    //alert(2);
                    $("#isTextParameter").attr('checked', true);
                } else {
                    $("#isTextParameter").attr('checked', false);
                }
                if (item.CompareType) {
                    $("#CompareType").mkselectSet(item.CompareType);
                }

                if (item.ValueType) {
                    $("#ValueType").mkselectSet(item.ValueType);
                    if (item.ValueType == "System") {
                        $("div[System]").show();
                    }
                    else {
                        $("div[System]").hide();
                    }
                }
                else {
                    $("div[System]").hide();
                }

                if (item.DefaultValue) {
                    $("#DefaultValue").mkselectSet(item.DefaultValue);
                }
               
            }
            else {
                $("#Name").val(row.name);
                $("#Field").val(row.field);
            }
        }
    };
    page.init();

    // 保存数据
    acceptClick = function (callBack) {
        var data = getFormData();
        
        callBack(data);
        return true;
    };
};

var getFormData = function () {

    if (!$('body').mkValidform()) {
        return false;
    }

    var postData = {};
    postData = $('[data-table="QuerySettings"]').mkGetFormData();
    return JSON.stringify(postData);
}