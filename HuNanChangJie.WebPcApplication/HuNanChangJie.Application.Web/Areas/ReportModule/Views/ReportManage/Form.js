﻿/*
 * 创建人：
 * 日 期：2017.11.11
 * 描 述：报表管理
 */
var keyValue = '';
var columnData = null;
var parentColumnData = [];
var bootstrap = function ($, Changjie) {
    "use strict";
    var selectedRow = Changjie.frameTab.currentIframe().selectedRow;
    if (!!selectedRow) {
        columnData = JSON.parse(selectedRow.ColumnNamesJson);
    }
    var page = {
        init: function () {
            page.bind();
            page.initData();
            page.isTree();
        },
        isTree: function () {
            if ($("#IsTree").prop('checked')) {
                $("div[Parent]").show();
            }
            else {
                $("div[Parent]").hide();
            }
        },
        bind: function () {

            $("#IsTree").on("change", function () {
                page.isTree();
            });
            // 加载导向
            $('#wizard').wizard().on('change', function (e, data) {
                var $finish = $("#btn_finish");
                var $next = $("#btn_next");
                if (data.direction == "next") {

                    if (data.step == 1) {
                        if (!$('#step-1').mkValidform()) {
                            return false;
                        }
                        //$finish.removeAttr('disabled');
                        // $next.attr('disabled', 'disabled');
                    }
                    else if (data.step == 2) {
                        var csql = $("#F_DataSourceId").mkselectGetEx()
                        if (csql == null || csql.id == -1) {
                            Changjie.alert.error("请选择数据库");
                            return false;
                        }

                        if ($("#F_ChartSqlString").val() == "" && $("#F_ListSqlString").val() == "") {
                            Changjie.alert.error("请输入SQL语句");
                            return false;
                        }

                        if (!!columnData == false) {
                            var isloadsuccess = page.loadColumn();
                            if (!isloadsuccess)
                                return false;
                        }

                        $finish.removeAttr('disabled');
                        $next.attr('disabled', 'disabled');
                    }
                } else {
                    $finish.attr('disabled', 'disabled');
                    $next.removeAttr('disabled');
                }
            });

            $("#refresh").on("click", function () {
                page.loadColumn();
            });
            $("#columngrid").jfGrid({
                headData: [
                    {
                        label: "显示名称", name: "Name", width: 140, align: "left",
                        edit: {
                            type: 'input',
                            change: function (row, index, item, oldValue, colname, headData) {
                                if (!!row.isParent) {
                                    for (var i in parentColumnData) {
                                        var info = parentColumnData[i];
                                        if (info.id != row.id) continue;
                                        info.Name = row.Name;
                                        break;
                                    }
                                }
                            },
                        }
                    },
                    { label: "字段名", name: "Field", width: 140, align: "left" },
                    {
                        label: "是否显示", name: "IsShow", width: 80, align: "center",
                        formatter: function (cellvalue) {

                            var val = JSON.parse(cellvalue || "false");

                            return val == true ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: true// 默认选中项
                        }
                    },
                    {
                        label: "是否汇总", name: "IsSum", width: 80, align: "center",
                        formatter: function (cellvalue) {

                            var val = JSON.parse(cellvalue || "false");

                            return val == true ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: false// 默认选中项
                        }
                    }, {
                        label: "千位分隔", name: "IsSeparate", width: 80, align: "center",
                        formatter: function (cellvalue) {

                            var val = JSON.parse(cellvalue || "false");

                            return val == true ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {// 在点击单元格的时候触发，可以用来初始化输入控件，行数据和控件对象

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: false// 默认选中项
                        }
                    },
                    {
                        label: "配置查询条件", name: "QueryConditionJson", width: 100, align: "left",
                        edit: {
                            type: "layer",
                            init: function (row, $obj, rownum) {
                                top.reportQuerySettings = {
                                    field: row.Field,
                                    name: row.Name,
                                    querySettings: JSON.parse((row.QueryConditionJson || "{}"))
                                };
                                return "?keyValue=reportQuerySettings";
                            },
                            change: function (data, rownum, selectData) {
                                data.QueryConditionJson = selectData;
                                $('#columngrid').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 600,
                                height: 400,
                                title: '查询设计器',
                                customurl: top.$.rootUrl + '/ReportModule/ReportManage/SetQueryCondition'
                            }
                        }
                    },
                    {
                        label: "", name: "btn3", width: 80, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                row.QueryConditionJson = "";
                                var rowindex = parseInt($cell.attr('rowindex'));
                                $('#columngrid').jfGridSet('updateRow', rowindex);
                            });
                            return '<span class=\"label label-success\" style=\"cursor: pointer;\">清空条件</span>';
                        }
                    },
                    {
                        label: "穿透地址", name: "ThroughPage", width: 100, align: "left",
                        edit: {
                            type: "layer",
                            init: function (row, $obj, rownum) {
                                top.reportThroughSettings = {
                                    field: row.Field,
                                    name: row.Name,
                                    throughSettings: JSON.parse((row.ThroughPage || "{}")),
                                    rowId: row.id
                                };
                                return "?keyValue=reportThroughSettings";
                            },
                            change: function (data, rownum, selectData) {
                                data.ThroughPage = selectData;
                                $('#columngrid').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 700,
                                height: 400,
                                title: '查询设计器',
                                customurl: top.$.rootUrl + '/ReportModule/ReportManage/SetThroughCondition'
                            }
                        }
                    },
                    {
                        label: '上级列名', name: 'ParentColumn', width: 100
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {

                                row.parentId = row.ParentColumn;

                                $('#columngrid').jfGridSet('refreshdata');

                            },
                            click: function ($this) {
                                $this.mkselectRefresh({ data: parentColumnData });
                            },
                            op: {
                                value: "id",
                                text: "Name",
                                data: parentColumnData
                            }
                        }
                    }, {
                        label: "显示宽度", name: "Width", width: 80, align: "center",
                        edit: {
                            type: 'input',
                            change: function (row, index, item, oldValue, colname, headData) {
                                var r = /^\+?[1-9][0-9]*$/;　　//正整数

                            },
                        }
                    },
                    {
                        label: "", name: "btn1", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#columngrid').jfGridSet('moveUp', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-info\" style=\"cursor: pointer;\">上移</span>';
                        }
                    },
                    {
                        label: "", name: "btn2", width: 50, align: "center",
                        formatter: function (value, row, op, $cell) {
                            $cell.on('click', function () {
                                var rowindex = parseInt($cell.attr('rowindex'));
                                var res = $('#columngrid').jfGridSet('moveDown', rowindex);
                                return false;
                            });
                            return '<span class=\"label label-warning\" style=\"cursor: pointer;\">下移</span>';
                        }
                    },
                ],
                height: 400,
                isTree: true,
                mainId: 'id',
                isEdit: true,
                onAddRow: function (row, rows) {
                    row.id = Changjie.newGuid();
                    row.isParent = true;
                    row.parentId = '';
                    parentColumnData.push({ id: row.id });
                },
                onMinusRow: function (row, rows) {
                }
            });
            //报表分类
            $('#F_TempCategory').mkDataItemSelect({ code: 'ReportSort', maxHeight: 160 });
            //报表状态
            $('#F_Status').mkDataItemSelect({ code: 'RStatus', maxHeight: 160 });
            //接口状态
            $('#F_IsImpl').mkDataItemSelect({ code: 'RIsImpl', maxHeight: 160 });

            $("#ParentField").mkselect();
            //报表风格
            $("[name='F_TempStyle']").click(function () {
                if ($(this).val() == 1) {
                    $("div.F_TempType").hide();
                    $("div.F_TempType").find("#F_TempType").attr("isvalid", "no");
                    $("div.list-pane").show(); $("div.chart-pane").hide();
                } else if ($(this).val() == 2) {
                    $("div.F_TempType").show();
                    $("div.F_TempType").find("#F_TempType").attr("isvalid", "yes");
                    $("div.chart-pane").show(); $("div.list-pane").hide();
                } else {
                    $("div.F_TempType").show();
                    $("div.F_TempType").find("#F_TempType").attr("isvalid", "yes");
                    $("div.chart-pane,div.list-pane").show();
                }
            })
            //图表类型
            $('#F_TempType').mkselect();
            //数据库
            $('#F_DataSourceId').mkselect({
                url: top.$.rootUrl + '/SystemModule/DatabaseLink/GetTreeList',
                type: 'tree',
                placeholder: '请选择数据库',
            });

            //完成事件
            $("#btn_finish").click(function () {
                if (!$('#wizard-steps').mkValidform()) {
                    return false;
                }
                var postData = $('#wizard-steps').mkGetFormData(keyValue);
                var griddata = $("#columngrid").jfGridGet("rowdatas");
                postData.ColumnNamesJson = JSON.stringify(griddata);
                var queryInfo = [];
                for (var i = 0; i < griddata.length; i++) {
                    var row = griddata[i];
                    if (row.QueryConditionJson) {
                        try {
                            var item = $.parseJSON(row.QueryConditionJson);
                            queryInfo.push(item);
                        }
                        catch { }
                    }
                }
                postData.QueryConditionJson = JSON.stringify(queryInfo);

                var throughInfo = [];
                for (var i = 0; i < griddata.length; i++) {
                    var row = griddata[i];
                    if (row.ThroughConditionJson) {
                        try {
                            var item = $.parseJSON(row.ThroughConditionJson);
                            throughInfo.push(item);
                        }
                        catch { }
                    }
                }
                postData.ThroughConditionJson = JSON.stringify(throughInfo);

                postData["F_TempStyle"] = $("[name='F_TempStyle']:checked").val();
                postData["F_ParamJson"] = function () {
                    var paramJson = {
                        F_DataSourceId: $("#F_DataSourceId").mkselectGet(),
                        F_ChartSqlString: $("#F_ChartSqlString").val(),
                        F_ListSqlString: $("#F_ListSqlString").val()
                    }
                    return JSON.stringify(paramJson);
                }();
                postData["IsTree"] = $("#IsTree").prop("checked");
                $.mkSaveForm(top.$.rootUrl + '/ReportModule/ReportManage/SaveForm?keyValue=' + keyValue, postData, function (res) {
                    Changjie.frameTab.currentIframe().refreshGirdData();
                });
            })
        },
        loadColumn: function () {
            var param = {};
            param.sqlString = $("#F_ListSqlString").val();
            param.dbResourceId = $("#F_DataSourceId").mkselectGet();
            var isloadsuccess = true;
            Changjie.httpPost(top.$.rootUrl + "/ReportModule/ReportManage/GetColumnsByDb", param, function (res) {
                //console.log(res)
                //debugger
                if (res.code == 200) {
                    $('#columngrid').jfGridSet('refreshdata', res.data);
                    page.refreshSelect(res.data);

                }
                else {
                    isloadsuccess = false;
                    Changjie.alert.error(res.info);
                }
            });
            return isloadsuccess;
        },
        refreshSelect: function (data) {
            var op = {
                value: "Field",
                text: "Field",
                title: "Field",
                data: data
            };
            $("#ParentField").mkselectRefresh(op);
        },
        initData: function () {
            if (!!selectedRow) {
                keyValue = selectedRow.F_TempId;
                $('#wizard-steps').mkSetFormData(selectedRow);
                $("[name='F_TempStyle'][value=" + selectedRow.F_TempStyle + "]").attr("checked", "checked").trigger("click");
                $('#wizard-steps').mkSetFormData(JSON.parse(selectedRow.F_ParamJson));
                $('#columngrid').jfGridSet('refreshdata', columnData);
                if (selectedRow.IsTree) {
                    $("#IsTree").attr("checked", true);
                }
                else {
                    $("#IsTree").attr("checked", false);
                }
                page.refreshSelect(columnData);
            }
        }
    };
    page.init();
}


