﻿using HuNanChangJie.Application.Report;
using HuNanChangJie.SystemCommon.Report;
using HuNanChangJie.Util;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.ReportModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2017-07-12 09:57
    /// 描 述：报表管理
    /// </summary>
    public class ReportManageController : MvcControllerBase
    {
        private ReportTempIBLL reportTempIBLL = new ReportTempBLL();

        #region  视图功能
        /// <summary>
        /// 管理页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        /// <summary>
        /// 设置查询条件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SetQueryCondition()
        {
            return View();
        }

        /// <summary>
        /// 设置穿透条件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SetThroughCondition()
        {
            return View();
        }

        /// <summary>
        /// 浏览页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Preview()
        {

            return View();
        }
        #endregion

        #region  获取数据
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">关键词</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetPageList(string pagination, string keyword, string filterModel)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = reportTempIBLL.GetPageList(paginationobj, keyword);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
        /// <summary>
        /// 获取实体数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetEntity(string keyValue)
        {
            var data = reportTempIBLL.GetEntity(keyValue);
            return JsonResult(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetColumns(string sqlString)
        {
            var data = reportTempIBLL.GetColumns(sqlString);
            return Success(data);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult GetColumnsNoTry(string sqlString)
        {
            try
            {
                var data = reportTempIBLL.GetColumnsNoTry(sqlString);
                return Success(data);
            }
            catch (System.Exception ex)
            {
                return Fail(ex.Message);
            }
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult GetColumnsByDb(string sqlString,string dbResourceId)
        {
            try
            {
                var data = reportTempIBLL.GetColumnsNoTry(sqlString, dbResourceId);
                return Success(data);
            }
            catch (System.Exception ex)
            {
                return Fail(ex.Message);
            }
        }
        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <param name="reportId">报表主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetReportData(string reportId, string queryJson, string projectId = "")
        {
            ReportTempEntity reportEntity = reportTempIBLL.GetEntity(reportId);
            dynamic paramJson = reportEntity.F_ParamJson.ToJson();

            var queryList = queryJson.ToList<QueryConditionEntity>();


            if (!string.IsNullOrEmpty(paramJson.F_ListSqlString.ToString()))
            {
                paramJson.F_ListSqlString = paramJson.F_ListSqlString.ToString().ToLower();

                if (!string.IsNullOrEmpty(projectId))
                {
                    paramJson.F_ListSqlString = paramJson.F_ListSqlString.ToString().Replace($"@projectid@", projectId);
                }

                //if (paramJson.F_ListSqlString.ToString().StartsWith("exec"))
                //{
                if (queryList != null)
                {
                    foreach (var item in queryList)
                    {
                        if (item.Value.Contains("#"))
                        {
                            string[] ps = item.Value.Split(new char[] { '#' });
                            if (ps.Length > 0)
                                paramJson.F_ListSqlString = paramJson.F_ListSqlString.ToString().Replace($"@{item.Field.ToLower()}1@", item.Value);
                            if (ps.Length > 1)
                                paramJson.F_ListSqlString = paramJson.F_ListSqlString.ToString().Replace($"@{item.Field.ToLower()}2@", item.Value);
                        }
                        else
                        {
                            paramJson.F_ListSqlString = paramJson.F_ListSqlString.ToString().Replace($"@{item.Field.ToLower()}@", item.Value);
                        }
                    }
                }

                Regex regParam = new Regex(@"@.*?@");
                paramJson.F_ListSqlString = regParam.Replace(paramJson.F_ListSqlString.ToString(), "");
                //}

                Regex regWhere = new Regex(@"#.*?#");
                Match match = regWhere.Match(paramJson.F_ListSqlString.ToString());
                if (match.Success)
                    paramJson.F_ListSqlString = paramJson.F_ListSqlString.ToString().Replace($"{match.Value}", match.Value.Replace("#", ""));
            }

            DataTable dataTable2 = new DataTable();
            if (string.IsNullOrEmpty(queryJson))
            {
                
                dataTable2.Rows.Add(dataTable2.NewRow());

                var data = new
                {
                    reportName = reportEntity.F_FullName,
                    tempStyle = reportEntity.F_TempStyle,
                    chartType = reportEntity.F_TempType,
                    treeSettings = new { reportEntity.IsTree, reportEntity.ParentField },
                    chartData = reportTempIBLL.GetReportData(paramJson.F_DataSourceId.ToString(), paramJson.F_ChartSqlString.ToString()),
                    listData = dataTable2,// reportTempIBLL.GetReportDataOrEmptyRow(paramJson.F_DataSourceId.ToString(), paramJson.F_ListSqlString.ToString()),
                    queryData = reportEntity.QueryConditionJson.ToJson(),
                    headData = reportEntity.ColumnNamesJson.ToJson()
                };
                return Content(data.ToJson());
            }
            else
            {
                var data = new
                {
                    reportName = reportEntity.F_FullName,
                    tempStyle = reportEntity.F_TempStyle,
                    chartType = reportEntity.F_TempType,
                    treeSettings = new { reportEntity.IsTree, reportEntity.ParentField },
                    chartData = reportTempIBLL.GetReportData(paramJson.F_DataSourceId.ToString(), paramJson.F_ChartSqlString.ToString(), queryJson),
                    listData = dataTable2,//reportTempIBLL.GetReportDataOrEmptyRow(paramJson.F_DataSourceId.ToString(), paramJson.F_ListSqlString.ToString(), queryJson),
                    queryData = reportEntity.QueryConditionJson.ToJson(),
                    headData = reportEntity.ColumnNamesJson.ToJson()
                };
                return Content(data.ToJson());
            }

        }

        /// <summary>
        /// 报表导出
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="queryJson"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        //[HttpPost, AjaxOnly]
        public ActionResult GetReportDateList(string reportId, string queryJson, string projectId = "")
        {
            var data = reportTempIBLL.GetReportDateList(reportId, queryJson);
            var jsonData = new
            {
                 rows = data
            };
            return Content(jsonData.ToJson());
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetReportDateListByPager(string reportId, string pagination, string queryJson, string projectId = "")
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = reportTempIBLL.GetReportDateListByPager(reportId, paginationobj,queryJson, projectId);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken, AjaxOnly]
        public ActionResult SaveForm(string keyValue, ReportTempEntity entity)
        {
            reportTempIBLL.SaveEntity(keyValue, entity);
            return Success("保存成功！");
        }
        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            reportTempIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        #endregion



        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">关键词</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAGPageList(string pagination, string filterModel, string groupKey, string pivotCols, string sortModel)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = reportTempIBLL.GetAgPageList(paginationobj, filterModel, groupKey, pivotCols: pivotCols, sortModel: sortModel);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
    }
}