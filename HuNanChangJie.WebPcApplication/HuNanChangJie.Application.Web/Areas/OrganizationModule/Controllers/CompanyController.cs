﻿using HuNanChangJie.Application.Organization;
using HuNanChangJie.Application.Organization._3Rd;
using HuNanChangJie.Application.Organization._3Rd.DingDing;
using HuNanChangJie.Application.Organization._3Rd.DingDing.model;
using HuNanChangJie.Application.Organization.Organization;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using HuNanChangJie.Util.Operat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.OrganizationModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.03.09
    /// 描 述：公司管理
    /// </summary>
    public class CompanyController : MvcControllerBase
    {
        private CompanyIBLL companyIBLL = new CompanyBLL();
        private DepartmentIBLL departmentIBLL = new DepartmentBLL();
        private IOranizationBll oranBll = new OrganizationBll();
        private DingCompanyIBLL dingCompanyIBLL = new DingCompanyBLL();
        private DingDeptmentIBLL dingDepartmentIBLL = new DingDeptmentBLL();
        private DingEmployeeIBLL dingEmployeeBLL = new DingEmployeeBLL();
        UserBLL userBLL = new UserBLL();

        private DeptCompanyMapBLL deptCompanyMapBLL = new DeptCompanyMapBLL();

        #region  获取视图
        /// <summary>
        /// 主页
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ChooseOrganization()
        {
            return View();
        }

        /// <summary>
        /// 表单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region  获取数据
        public ActionResult GetOranizationList(string isloadUser)
        {
            bool isload;
            bool.TryParse(isloadUser, out isload);
            var list = oranBll.GetOrganizationList(isload);
            return JsonResult(list);
        }

        /// <summary>
        /// 获取公司列表信息
        /// </summary>
        /// <param name="keyword">查询关键字</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList(string keyword)
        {
            var data = companyIBLL.GetList(keyword);
            return JsonResult(data);
        }

        /// <summary>
        /// 获取公司信息实体
        /// </summary>
        /// <param name="keyword">查询关键字</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetEntity(string keyword)
        {
            var data = companyIBLL.GetEntity(keyword);
            return JsonResult(data);
        }


        /// <summary>
        /// 获取树形数据
        /// </summary>
        /// <param name="parentId">父级id</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTree(string parentId)
        {
            var data = companyIBLL.GetTree(parentId);
            return JsonResult(data);
        }
        /// <summary>
        /// 获取映射数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMap(string ver)
        {
            var data = companyIBLL.GetModelMap();
            string md5 = Md5Helper.Encrypt(data.ToJson(), 32);
            if (md5 == ver)
            {
                return Success("no update");
            }
            else
            {
                var jsondata = new
                {
                    data = data,
                    ver = md5
                };
                return JsonResult(jsondata);
            }
        }
        #endregion

        #region  提交数据
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="entity">实体数据</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, CompanyEntity entity)
        {
            companyIBLL.SaveEntity(keyValue, entity);
            return Success("保存成功！", "公司信息", string.IsNullOrEmpty(keyValue) ? OperationType.Create : OperationType.Update, entity.F_CompanyId, entity.ToJson());
        }
        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            companyIBLL.VirtualDelete(keyValue);
            return Success("删除成功！", "公司信息", OperationType.Delete, keyValue, "");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult DingDingImport(string company)
        {
            if (string.IsNullOrEmpty(company))
                return Fail("参数错误");

            I3rdHelp help = new DingDingHelp(company);
            try
            {
               long xiziDepartmentIdForCompany = long.Parse(Config.GetValue("XiZiCompanyStructureDingDingId"));

                //dingCompanyIBLL.RemoveAll();

                //SaveDingCompany(help,xiziDepartmentIdForCompany);

                long xiziDepartmentId = long.Parse(Config.GetValue("XiZiDepartmentDingDingId"));
                dingDepartmentIBLL.RemoveAll();

                SaveDingDepartment(help, xiziDepartmentId);

                SaveDepartment(xiziDepartmentId.ToString());

                dingEmployeeBLL.RemoveAll();
                SaveDingEmployee(help);

                SaveEmployee();
                return Success("导入成功！");
            }
            catch (Exception ex)
            {
                return Fail(ex.Message);
            }
        }

        void SaveDingCompany(I3rdHelp help, long departmentId)
        {

            var result = help.GetSubDepmentList(departmentId);

            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    Ding_CompanyEntity entity = new Ding_CompanyEntity
                    {
                        Dept_id = item.dept_id.ToString(),
                        Name = item.name,
                        Parent_id = item.parent_id.ToString(),
                        CreationDate = DateTime.Now
                    };
                    dingCompanyIBLL.SaveEntity(entity.Dept_id, entity, null, "Add");
                    SaveDingCompany(help, item.dept_id);
                }
            }
        }

        void SaveDepartment(string parentDepartmentId)
        {

            var dingDepartmentList = dingDepartmentIBLL.GetPageList(new XqPagination { rows = int.MaxValue, page = 1 }, null);

            var deptcompanyList = deptCompanyMapBLL.GetPageList(new XqPagination { rows = int.MaxValue, page = 1 }, null);

            SaveDepartmentLoop(parentDepartmentId, dingDepartmentList, deptcompanyList);

        }

        void SaveDepartmentLoop(string parentDepartmentId, IEnumerable<Ding_departmentEntity> dingDepartmentList, IEnumerable<base_3rd_deparment_company_mappingEntity> deptcompanyList)
        {
            var deptList = departmentIBLL.GetAllList();

            foreach (var item in dingDepartmentList.Where(x => x.Parent_id.Equals(parentDepartmentId)))
            {
                var deptCompanyMap = deptcompanyList.FirstOrDefault(x => x.DingDeptId.Equals(item.Parent_id));

                var deptParentCompany = deptList.Find(x => x.DingDeptId.Equals(item.Parent_id));
                var department = deptList.Find(x => x.DingDeptId.Equals(item.Dept_id));

                string companyId = string.Empty;
                if (deptCompanyMap != null)
                {

                    companyId = deptCompanyMap.F_CompanyId;
                }
                else if (deptParentCompany != null)
                {
                    companyId = deptParentCompany.F_CompanyId;
                }
                var deptcompanies = deptcompanyList.Select(x => x.DingDeptId);

                if (!deptcompanies.Contains(item.Dept_id))
                {
                    
                    if (department == null)
                    {
                        DepartmentEntity entity = new DepartmentEntity()
                        {
                            F_CompanyId = companyId,
                            F_ParentId = deptParentCompany?.F_DepartmentId?? "0",
                            F_FullName = item.Name,
                            CreationDate = DateTime.Now,
                            DingDeptId = item.Dept_id
                        };
                        departmentIBLL.SaveEntity("", entity);
                    }
                    else
                    {
                        department.F_FullName = item.Name;
                        department.F_CompanyId = companyId;
                        department.F_ParentId = deptParentCompany?.F_DepartmentId ?? "0";
                        department.ModificationDate=DateTime.Now;

                        departmentIBLL.SaveEntity(department.F_DepartmentId, department);
                    }
                }

                SaveDepartmentLoop(item.Dept_id, dingDepartmentList, deptcompanyList);
            }
        }

        void SaveDingDepartment(I3rdHelp help, long departmentId)
        {

            var result = help.GetSubDepmentList(departmentId);

            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    Ding_departmentEntity entity = new Ding_departmentEntity
                    {
                        Dept_id = item.dept_id.ToString(),
                        Name = item.name,
                        Parent_id = item.parent_id.ToString(),
                        CreationDate = DateTime.Now
                    };
                    dingDepartmentIBLL.SaveEntity(entity.Dept_id, entity, null, "Add");
                    SaveDingDepartment(help, item.dept_id);
                }
            }
        }

        void SaveDingEmployee(I3rdHelp help)
        {
            var departmentList = dingDepartmentIBLL.GetPageList(new XqPagination { rows=int.MaxValue,page=1 },null);

            foreach(var item in departmentList)
            {
                var employeeList = help.GetUserListByDeptId(long.Parse(item.Dept_id));

                if (employeeList != null && employeeList.Count > 0)
                {
                    foreach (var dingEmployee in employeeList)
                    {
                       
                        ding_employeeEntity entity = new ding_employeeEntity
                        {
                            userid = dingEmployee.userid,
                            unionid=dingEmployee.unionid,
                            name = dingEmployee.name,
                            admin = dingEmployee.admin.ToString(),
                            avatar = dingEmployee.avatar,
                            mobile = dingEmployee.mobile,
                            title = dingEmployee.title,
                            email = dingEmployee.email,
                            work_place = dingEmployee.work_place,
                            job_number = dingEmployee.job_number,
                            hired_date = dingEmployee.hired_date.ToString(),
                            active = dingEmployee.active.ToString(),
                            nickname = dingEmployee.nickname,
                            dept_id_list = string.Join(",",dingEmployee.dept_id_list),
                            extension=dingEmployee.extension
                        };
                        try
                        {
                            dingEmployeeBLL.SaveEntity(entity.userid, entity, null, "Add");
                        }
                        catch(Exception ex)
                        {
                            continue;
                        }
                    }
                }
            }
        }

        void SaveEmployee()
        {
            var dingEmployeeList = dingEmployeeBLL.GetPageList(new XqPagination { rows = int.MaxValue, page = 1 }, null);

            var departments = departmentIBLL.GetAllList();

            var userList = userBLL.GetAllList();

            foreach (var item in dingEmployeeList)
            {
                string deptId = item.dept_id_list.Split(',')[0];
               

                var employee = userList.FirstOrDefault(x => x.DingUserId!=null&& x.DingUserId.Equals(item.userid));
             
                var parentCompany= deptCompanyMapBLL.GetPageList(new XqPagination { rows=int.MaxValue,page=1},null).FirstOrDefault(x => x.DingDeptId.Equals(deptId));
                var department = departments.FirstOrDefault(x => x.DingDeptId.Equals(item.dept_id_list.Split(',')[0]));
                string companyId = parentCompany?.F_CompanyId ?? department?.F_CompanyId;
                if (employee != null)
                {                   
                    employee.F_CompanyId = companyId;
                    employee.F_DepartmentId = department?.F_DepartmentId;
                    employee.F_Mobile = item.mobile;
                    employee.F_RealName = item.name;
                    employee.F_EnCode = item.job_number;
                    employee.F_Email = item.email;
                    employee.F_Description = $"{item.extension}";
                    userBLL.SaveEntity(employee.F_UserId, employee);
                }
                else
                {
                 
                    UserEntity userEntity = new UserEntity()
                    {
                        F_Gender = 1,
                        F_UserId = Guid.NewGuid().ToString().ToLower(),
                        F_CompanyId = department?.F_CompanyId,
                        F_DepartmentId= department?.F_DepartmentId,
                        DingUserId = item.userid,
                        F_Mobile=item.mobile,
                        DingUnionId = item.unionid,
                        F_RealName = item.name,
                        F_EnCode = item.job_number,
                        F_Email = item.email,
                        F_Description = $"{item.extension}",
                        F_Password = "123456"
                    };

                    if (!string.IsNullOrEmpty(item.mobile))
                        userEntity.F_Account = item.mobile;
                    else
                        userEntity.F_Account = item.name;

                    userBLL.SaveEntity("", userEntity);
                }
            }

        }



        [HttpPost]
        [AjaxOnly]
        public ActionResult DingDingExport(string company)
        {
            if (string.IsNullOrEmpty(company))
                return Fail("参数错误");

            I3rdHelp help = new DingDingHelp(company);
            try
            {
                help.ExportDept();
                help.ExportUser();
                return Success("导出成功！");
            }
            catch (Exception ex)
            {
                return Fail(ex.Message);
            }
        }
        #endregion
    }
}