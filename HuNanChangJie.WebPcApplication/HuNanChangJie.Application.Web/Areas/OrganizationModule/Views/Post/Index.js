﻿/*
 * 日 期：2017.03.22
 * 描 述：部门管理	
 */
var selectedRow;
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var companyId = '';
    var departmentId = '';
    var page = {
        init: function () {
            page.inittree();
            page.initGrid();
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 部门选择
            $('#department_select').mkselect({
                type: 'tree',
                placeholder: '请选择部门',
                // 展开最大高度
                maxHeight: 300,
                // 是否允许搜索
                allowSearch: true,
                select: function (item) {

                    if (!item || item.value == '-1') {
                        departmentId = '';
                    }
                    else {
                        departmentId = item.value;
                    }
                    page.search();
                }
            });

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                if (!companyId) {
                    Changjie.alert.warning('请选择公司！');
                    return false;
                }
                selectedRow = null;
                Changjie.layerForm({
                    id: 'form',
                    title: '添加岗位',
                    url: top.$.rootUrl + '/OrganizationModule/Post/Form?companyId=' + companyId,
                    width: 500,
                    height: 379,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_PostId');
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑岗位',
                        url: top.$.rootUrl + '/OrganizationModule/Post/Form?companyId=' + companyId,
                        width: 500,
                        height: 379,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_PostId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/OrganizationModule/Post/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 添加岗位成员
            $('#memberadd').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_PostId');
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '添加岗位成员',
                        url: top.$.rootUrl + '/AuthorizeModule/UserRelation/SelectForm?objectId=' + keyValue + '&companyId=' + companyId + '&departmentId=' + selectedRow.F_DepartmentId + '&category=2',
                        width: 800,
                        height: 520,
                        callBack: function (id) {
                            return top[id].acceptClick();
                        }
                    });
                }
            });
            // 产看成员
            $('#memberlook').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_PostId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '查看岗位成员',
                        url: top.$.rootUrl + '/AuthorizeModule/UserRelation/LookForm?objectId=' + keyValue,
                        width: 800,
                        height: 520,
                        btn: null
                    });
                }
            });
        },
        inittree: function () {
            $('#companyTree').mktree({
                url: top.$.rootUrl + '/OrganizationModule/Company/GetTree',
                param: { parentId: '0' },
                nodeClick: page.treeNodeClick
            });
            $('#companyTree').mktreeSet('setValue', '53298b7a-404c-4337-aa7f-80b2a4ca6681');
        },
        treeNodeClick: function (item) {
            companyId = item.id;
            $('#titleinfo').text(item.text);

            $('#department_select').mkselectRefresh({
                // 访问数据接口地址
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                // 访问数据接口参数
                param: { companyId: companyId, parentId: '0' },
            });
            departmentId = '';
            page.search();
        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/OrganizationModule/Post/GetList',
                headData: [

                    { label: "岗位编号", name: "F_EnCode", width: 100 },
                    {
                        label: "所属部门", name: "F_DepartmentId", width: 120,
                        cellRenderer: function (value) {

                            return Changjie.clientdata.getSync('department', {
                                key: value.value,
                                companyId: companyId
                            }).name;

                        }
                    },
                    { label: "备注", name: "F_Description", width: 200 },
                    { label: "创建人", name: "CreationName", width: 100 },
                    {
                        label: "创建时间", name: "CreationDate", width: 100,
                        cellRenderer: function (cellvalue) {
                            return Changjie.formatDate(cellvalue.value, 'yyyy-MM-dd');
                        }
                    }
                ],

                isTree: true,
                TreeGroupName: "岗位名称",
                displayName: "F_Name",
                mainId: 'F_PostId',
                parentId: 'F_ParentId',
            });
        },
        search: function (param) {
            param = param || {};
            param.companyId = companyId;
            param.departmentId = departmentId;

            $('#gridtable').AgGridSet('reload', param);
        }
    };

    refreshGirdData = function () {
        page.search();
    };

    page.init();
}


