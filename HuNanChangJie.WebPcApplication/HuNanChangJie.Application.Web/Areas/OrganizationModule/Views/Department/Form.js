﻿/*
 * 日 期：2017.04.18
 * 描 述：部门管理	
 */
var companyId = request('companyId');


var acceptClick;
var keyValue = '';
var bootstrap = function ($, Changjie) {
    "use strict";
    var selectedRow = Changjie.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            // 部门性质
            $('#F_Nature').mkDataItemSelect({ code: 'DepartmentNature', maxHeight: 230 });
            // 上级部门
            $('#F_ParentId').mkDepartmentSelect({ companyId: companyId, maxHeight: 160 });
        },
        initData: function () {
            if (!!selectedRow) {
                keyValue = selectedRow.F_DepartmentId;
                $('#form').mkSetFormData(selectedRow);
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData(keyValue);
        if (postData["F_ParentId"] == '' || postData["F_ParentId"] == '&nbsp;') {
            postData["F_ParentId"] = '0';
        }
        postData["F_CompanyId"] = companyId;
        $.mkSaveForm(top.$.rootUrl + '/OrganizationModule/Department/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}