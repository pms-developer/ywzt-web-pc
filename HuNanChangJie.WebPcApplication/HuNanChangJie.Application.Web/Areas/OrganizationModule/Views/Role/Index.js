﻿/*
 * 日 期：2017.03.22
 * 描 述：部门管理	
 */
var selectedRow;
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGrid();
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                selectedRow = null;
                Changjie.layerForm({
                    id: 'form',
                    title: '添加角色',
                    url: top.$.rootUrl + '/OrganizationModule/Role/Form',
                    width: 500,
                    height: 340,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RoleId');
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑角色',
                        url: top.$.rootUrl + '/OrganizationModule/Role/Form',
                        width: 500,
                        height: 340,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RoleId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/OrganizationModule/Role/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 添加角色成员
            $('#memberadd').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RoleId');
                var loginInfo = Changjie.clientdata.get(['userinfo']);
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '添加角色成员',
                        url: top.$.rootUrl + '/AuthorizeModule/UserRelation/SelectForm?objectId=' + keyValue + '&companyId=' + loginInfo.F_CompanyId + '&departmentId=' + loginInfo.F_DepartmentId + '&category=1',
                        width: 800,
                        height: 520,
                        callBack: function (id) {
                            return top[id].acceptClick();
                        }
                    });
                }
            });
            // 查看成员
            $('#memberlook').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RoleId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '查看角色成员',
                        url: top.$.rootUrl + '/AuthorizeModule/UserRelation/LookForm?objectId=' + keyValue,
                        width: 800,
                        height: 520,
                        btn: null
                    });
                }
            });
            // 功能授权
            $('#authorize').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RoleId');
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'authorizeForm',
                        title: '功能授权 - ' + selectedRow.F_FullName,
                        url: top.$.rootUrl + '/AuthorizeModule/Authorize/Form?objectId=' + keyValue + '&objectType=1',
                        width: 550,
                        height: 690,
                        btn: null
                    });
                }
            });
            // 数据授权
            $('#dataauthorize').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RoleId');
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'dataAuthorizeForm',
                        title: '数据授权 - ' + selectedRow.F_FullName,
                        url: top.$.rootUrl + '/AuthorizeModule/DataAuthorize/Index?objectId=' + keyValue + '&objectType=1',
                        width: 1100,
                        height: 700,
                        maxmin: true,
                        btn: null
                    });
                }
            });
            // 移动功能授权
            $('#appauthorize').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RoleId');
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'appAuthorizeForm',
                        title: '移动功能授权 - ' + selectedRow.F_FullName,
                        url: top.$.rootUrl + '/AuthorizeModule/Authorize/AppForm?objectId=' + keyValue + '&objectType=1',
                        width: 550,
                        height: 690,
                        callBack: function (id) {
                            return top[id].acceptClick();
                        }
                    });
                }
            });
            // 设置Ip过滤
            $('#ipfilter').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RoleId');
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'filterIPIndex',
                        title: 'TCP/IP 地址访问限制 - ' + selectedRow.F_FullName,
                        url: top.$.rootUrl + '/AuthorizeModule/FilterIP/Index?objectId=' + keyValue + '&objectType=Role',
                        width: 600,
                        height: 400,
                        btn: null,
                        callBack: function (id) { }
                    });
                }
            });
            // 设置时间段过滤
            $('#timefilter').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_RoleId');
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'filterTimeForm',
                        title: '时段访问过滤 - ' + selectedRow.F_FullName,
                        url: top.$.rootUrl + '/AuthorizeModule/FilterTime/Form?objectId=' + keyValue + '&objectType=Role',
                        width: 610,
                        height: 470,
                        callBack: function (id) {
                            return top[id].acceptClick();
                        }
                    });
                }
            });
        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/OrganizationModule/Role/GetPageList',
                headData: [
                    { label: '角色编号', name: 'F_EnCode', width: 100   },
                    { label: '角色名称', name: 'F_FullName', width: 200 },
                    {
                        label: '创建时间', name: 'CreationDate', width: 130
                    },
                    {
                        label: '创建人', name: 'CreationName', width: 130
                    },
                    {
                        label: "有效", name: "F_EnabledMark", width: 50,  
                        cellRenderer: function (cellvalue) {
                            return cellvalue.value == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                        }
                    },
                    { label: "角色描述", name: "F_Description", index: "F_Description", width: 300 }
                ],
                isPage: true,
                reloadSelected: true,
                mainId: 'F_RoleId'
            });

            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload', param);
        }
    };

    refreshGirdData = function () {
        page.search();
    };

    page.init();
}


