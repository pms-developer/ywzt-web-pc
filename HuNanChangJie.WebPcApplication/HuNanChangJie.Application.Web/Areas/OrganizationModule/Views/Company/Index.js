﻿/*
 * 日 期：2017.04.17
 * 描 述：公司管理	
 */
var refreshGirdData; // 更新数据
var selectedRow;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGrid();
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                selectedRow = null;
                Changjie.layerForm({
                    id: 'Form',
                    title: '添加公司',
                    url: top.$.rootUrl + '/OrganizationModule/Company/Form',
                    width: 750,
                    height: 500,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                var keyValue = $('#gridtable').AgGridValue('F_CompanyId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'Form',
                        title: '编辑公司',
                        url: top.$.rootUrl + '/OrganizationModule/Company/Form',
                        width: 750,
                        height: 500,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });

            $('#dingimport').on('click', function () {
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                var keyValue = $('#gridtable').AgGridValue('F_CompanyId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.httpPost(top.$.rootUrl + '/OrganizationModule/Company/DingDingImport', { company: keyValue }, function (data) {
                        if (data.code == 200)
                            Changjie.alert.success("导入成功");
                        else
                           Changjie.alert.error(data.info);
                    });
                }
            });

            $('#dingexport').on('click', function () {
                selectedRow = $('#gridtable').AgGridGet('rowdata');
                var keyValue = $('#gridtable').AgGridValue('F_CompanyId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.httpPost(top.$.rootUrl + '/OrganizationModule/Company/DingDingExport', { company: keyValue }, function (data) {
                        if (data.code == 200)
                            Changjie.alert.success("导出成功");
                        else
                            Changjie.alert.error(data.info);
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_CompanyId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/OrganizationModule/Company/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/OrganizationModule/Company/GetList',
                headData: [
                    { headerName: "公司名称", field: "F_FullName", width: 260,hide:true},
                    { headerName: "公司编码", field: "F_EnCode", width: 150  },
                    { headerName: "公司简称", field: "F_ShortName", width: 150 },
                    { headerName: "公司性质", field: "F_Nature", width: 80  },
                    {
                        headerName: "成立时间", field: "F_FoundedTime", width: 80 ,
                        cellRenderer: function (param) {
                            return Changjie.formatDate(param.value, 'yyyy-MM-dd');
                        }
                    },
                    { headerName: "负责人", field: "F_Manager", width: 100 },
                    { headerName: "经营范围", field: "F_Fax", width: 200  },
                    { headerName: "备注", field: "F_Description", width: 200  }
                ],
                isTree: true,
                TreeGroupName: "公司名称",
                displayName:"F_FullName",
                mainId: 'F_CompanyId',
                parentId: 'F_ParentId'
            });
            page.search();
        },
        search: function (param) {
            $('#gridtable').AgGridSet('reload', param);
        }
    };

    // 保存数据后回调刷新
    refreshGirdData = function () {
        page.search();
    }

    page.init();
}


