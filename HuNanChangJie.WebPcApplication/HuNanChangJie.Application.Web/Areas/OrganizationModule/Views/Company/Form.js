﻿/*
 * 日 期：2017.04.18
 * 描 述：公司管理	
 */

var acceptClick;
var keyValue = '';
var bootstrap = function ($, Changjie) {
    "use strict";
    var selectedRow = Changjie.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            // 公司性质
            $('#F_Nature').mkDataItemSelect({ code: 'CompanyNature' });
            // 上级公司
            $('#F_ParentId').mkCompanySelect();
            // 省市区
            $('#area').mkAreaSelect();
        },
        initData: function () {
            if (!!selectedRow) {
                keyValue = selectedRow.F_CompanyId;
                $('#form').mkSetFormData(selectedRow);
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData(keyValue);
        if (postData["F_ParentId"] == '' || postData["F_ParentId"] == '&nbsp;') {
            postData["F_ParentId"] = '0';
        }
        $.mkSaveForm(top.$.rootUrl + '/OrganizationModule/Company/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}