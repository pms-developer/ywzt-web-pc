﻿var acceptClick;
var projectId = request("projectId");
var infoId = "";
var infoType = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            page.inittree();
        },
        bind: function () {
            $('#form_tabs_sub').mkFormTab(5);
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            // 搜索框初始化
            $('#txt_keyword').on("change", function (e) {
                var keyword = $(this).val();
                $('#companyTree').mktreeSet('search', { keyword: keyword });
            });

        },
        initData: function () {
            $.mkSetForm(top.$.rootUrl + '/OrganizationModule/Role/GetRoleList', function (data) {
                if (!!data) {
                    for (var n in data) {
                        var item = data[n];
                        var $select = $("#selectRole");
                        var value = page.createSelectOptionValue(item.F_RoleId, "Role", item.F_FullName);
                        var $poption = $("<option  id='" + item.F_RoleId + "' datatext='" + item.F_FullName + " (R)' value=\'" + value + "\'>" + item.F_FullName + " (R)</option>");
                        $select.append($poption);
                        $poption.on("dblclick", function () {

                            var $this = $(this);
                            var pid = $this.attr("id");
                            var ptext = $this.attr("datatext");;

                            var $select = $("#selectlist");
                            var isexist = false;
                            $("#selectlist option").each(function () {
                                var $this = $(this);
                                var id = $this.attr("id");
                                if (id == pid) {
                                    isexist = true;
                                    return false;
                                }
                            });
                            if (!isexist) {
                                var value = page.createSelectOptionValue(pid, "Role");
                                var $option = $("<option  id='" + pid + "' datatext='" + ptext + "' value=\'" + value + "\'>" + ptext + "</option>");
                                $select.append($option);
                                $option.on("dblclick", function () {
                                    var $this = $(this);
                                    var id = $this.attr("id");
                                    $("#selectlist option[id='" + id + "']").remove();
                                });
                            }
                        });
                    }
                }
            });
        },
        inittree: function () {
            $('#companyTree').mkscroll();

            $('#companyTree').mktree({
                url: top.$.rootUrl + '/OrganizationModule/Company/GetOranizationList?isloadUser=true',
                param: { parentId: '0' },
                nodeClick: page.treeNodeClick,
                nodeDbClick: page.treeNodeDbClick,
                allowSearch: true
            });
            $('#companyTree').mktreeSet('setValue', '53298b7a-404c-4337-aa7f-80b2a4ca6681');




        },
        treeNodeClick: function (item) {
            if (!!item.type) {
                infoId = item.id;
                infoType = item.type;
            }
        },
        treeNodeDbClick: function (item) {
            var $select = $("#selectlist");

            var isexist = false;
            $("#selectlist option").each(function () {
                var $this = $(this);
                var id = $this.attr("id");
                if (id == item.id) {
                    isexist = true;
                    return false;
                }
            });
            if (!isexist) {
                var value = page.createSelectOptionValue(item.id, item.type, item.text);
                var $option = $("<option  id='" + item.id + "'  datatext='" + item.text + "'  value=\'" + value + "\'>" + item.text + "</option>");
                $select.append($option);
                $option.on("dblclick", function () {
                    var $this = $(this);
                    var id = $this.attr("id");
                    $("#selectlist option[id='" + id + "']").remove();
                });
            }
        },
        createSelectOptionValue: function (id, type, name) {
            var value = { ObjectId: id, Type: type, Name: name };
            return JSON.stringify(value);
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var formData = [];
        $("#selectlist option").each(function () {
            var $this = $(this);
            var value = $this.val();
            var text = $this.attr("datatext");
            value = JSON.parse(value);
            value.Name = text;
            formData.push(value);
        });
        if (formData.length > 0) {
            callBack(formData);
            //var win = top.window.filter(t => t.mkCurrentUrl == "/ProjectModule/Project/Authorization")
            for (var i = 0; i < top.length; i++) {
                if (top.window[i].mkCurrentUrl == "/ProjectModule/Project/Authorization") {
                    var url = top.$.rootUrl + '/SystemModule/ModuleInfoAuthorization/Save';
                    var postData = {
                        moduleInfoId: top.window[i].projectId,
                        moduleType: "Project",
                        formData: JSON.stringify(top.window[i].$("#gridtable").jfGridGet("rowdatas"))
                    };
                    $.mkSaveForm(url, postData, function (res) {
                    });

                }
            }
            return true;
        }
        else {
            Changjie.alert.warning("您没有选择任何授权对象。");
        }
    };

    page.init();
};
