﻿/*
 * 日 期：2017.04.18
 * 描 述：人员选择	
 */
var acceptClick;
var dfopid = request('dfopid');
var userName = '';
var userItem = null;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $('#userId').mkselect({
                value: 'F_UserId',
                text: 'F_RealName',
                title: 'F_RealName',
                // 展开最大高度
                maxHeight: 110,
                // 是否允许搜索
                allowSearch: true,
                select: function (item) {
                    userItem = item
                    if (!!item) {
                        userName = item.F_RealName;
                    }
                    else {
                        userName = '';
                    }
                }
            });
            $('#department').mkDepartmentSelect({
                maxHeight: 150
            }).on('change', function () {
                var value = $(this).mkselectGet();
                $('#userId').mkselectRefresh({
                    url: top.$.rootUrl + '/OrganizationModule/User/GetList',
                    param: { departmentId: value }
                });
            });
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var formData = $('#form').mkGetFormData();
        var department = $("#department").children(":first").text();
        //console.log(userItem)
        var postitem = { value: formData.userId, text: userName, department: department, useritem: userItem };
       
        callBack(postitem, dfopid);
        return true;
    };
    page.init();
}