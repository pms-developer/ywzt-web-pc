﻿/*
 * Copyright (c) 2013-2017 
 * 创建人：
 * 日 期：2018.06.05
 * 描 述：写邮件	
 */
var keyValue = request('keyValue');
var type = request('type');
var data = request('data');

var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
        },
        initData: function () {
            if (!!keyValue) {
                if (type == '2') {
                    $.mkSetForm(top.$.rootUrl + '/OAModule/Email/GetReceiveEntity?keyValue=' + keyValue, function (data) {//
                        $('#F_frame').attr("srcdoc", data.F_BodyText);
                        $('#form').mkSetFormData(data);
                    });
                }
                else {
                    $.mkSetForm(top.$.rootUrl + '/OAModule/Email/GetSendEntity?keyValue=' + keyValue, function (data) {//
                        $('#F_frame').attr("srcdoc", data.F_BodyText);
                        $('#form').mkSetFormData(data);
                    });
                }
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        return true;
    };
    page.init();
}