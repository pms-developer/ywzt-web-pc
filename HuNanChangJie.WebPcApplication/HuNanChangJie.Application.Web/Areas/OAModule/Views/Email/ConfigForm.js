﻿/*
 * Copyright (c) 2013-2017 
 * 创建人：
 * 日 期：2018.06.05
 * 描 述：邮件配置	
 */

var acceptClick;
var keyValue = '';
var Id = '';
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {

        },
        initData: function () {
            var loginInfo = Changjie.clientdata.get(['userinfo']); //登陆者信息
            $.mkSetForm(top.$.rootUrl + '/OAModule/Email/GetConfigEntity?keyValue=' + loginInfo.userId, function (data) {//
                Id = data.F_Id;
                $('#form').mkSetFormData(data);
            });
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData(keyValue);
        $.mkSaveForm(top.$.rootUrl + '/OAModule/Email/SaveConfigEntity?keyValue=' + Id, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}