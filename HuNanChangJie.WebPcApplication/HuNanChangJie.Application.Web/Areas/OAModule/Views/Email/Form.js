﻿/*
 * Copyright (c) 2013-2017 
 * 创建人：
 * 日 期：2018.06.05
 * 描 述：写邮件	
 */
var keyValue = request('keyValue');
var data = request('data');

var acceptClick;
var ue;
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            //内容编辑器
            ue = UE.getEditor('F_BodyText');
            $('#F_Attachment').mkUploader({ isUpload: false });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/OAModule/Email/GetReceiveEntity?keyValue=' + keyValue, function (data) {//
                    $('#form').mkSetFormData(data);
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData(keyValue);
        postData['F_BodyText'] = ue.getContentTxt(null, null, true);
        $.mkSaveForm(top.$.rootUrl + '/OAModule/Email/SendMail', postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}