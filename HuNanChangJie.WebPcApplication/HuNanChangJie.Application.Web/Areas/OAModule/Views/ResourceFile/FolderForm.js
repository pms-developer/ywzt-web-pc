﻿/*
 * Copyright (c) 2013-2017 
 * 创建人：
 * 日 期：2017.06.20
 * 描 述：文件管理	
 */
var keyValue = request('keyValue');
var parentId = request('parentId');
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var Extension = "";
    var page = {
        init: function () {
            page.initControl();
        },
        //初始化控件
        initControl: function () {
            //获取表单
            //if (!!keyValue) {
            //    $.mkSetForm(top.$.rootUrl + "/OAModule/ResourceFile/GetFolderFormJson", { keyValue: keyValue }, function (data) {//
            //        $('#form').mkSetFormData(data);
            //    });
            //} else {
            //    $("#F_ParentId").val(parentId);
            //}
        }
    };
    //保存表单
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $("#form").mkGetFormData(keyValue);
        if (!!parentId) {
            postData["F_ParentId"] = parentId;
        }
        $.mkSaveForm(top.$.rootUrl + '/OAModule/ResourceFile/SaveFolderForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    }
    page.init();
}


