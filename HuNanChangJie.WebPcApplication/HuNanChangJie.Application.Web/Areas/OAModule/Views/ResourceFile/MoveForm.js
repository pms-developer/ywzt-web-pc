﻿/*
 * Copyright (c) 2013-2017 
 * 创建人：
 * 日 期：2017.06.20
 * 描 述：文件管理	
 */
var keyValue = request('keyValue');
var fileType = request('fileType');
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";

    var moveFolderId = "";
    var page = {
        init: function () {
            page.GetTree();
        },
        //初始化控件
        GetTree: function () {
            $('#ItemsTree').mktree({
                url: top.$.rootUrl + "/OAModule/ResourceFile/GetTreeJson",
                nodeClick: function (item) {
                    moveFolderId = item.id;
                }
            });
            $("#ItemsTree_" + keyValue.replace(/-/g, '_')).parent('li').remove();
        }
    };
    //保存表单
    acceptClick = function (callBack) {
        if (moveFolderId == "") {
            Changjie.alert.error('请选择要移动到的位置');
            return false;
        }
        var postData = $("#form").mkGetFormData(keyValue);
        postData["keyValue"] = keyValue;
        postData["moveFolderId"] = moveFolderId;
        postData["fileType"] = fileType;
        $.mkSaveForm(top.$.rootUrl + '/OAModule/ResourceFile/SaveMoveForm', postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });


    }
    page.init();
}