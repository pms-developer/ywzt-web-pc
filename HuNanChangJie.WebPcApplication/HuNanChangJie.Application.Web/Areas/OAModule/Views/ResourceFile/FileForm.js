﻿/*
 * Copyright (c) 2013-2017 
 * 创建人：
 * 日 期：2017.06.20
 * 描 述：文件管理	
 */
var keyValue = request('keyValue');

var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";

    var Extension = "";
    var page = {
        init: function () {
            page.initControl();
        },
        //初始化控件
        initControl: function () {
            //获取表单
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + "/OAModule/ResourceFile/GetFileFormJson?keyValue=" + keyValue, function (data) {//
                    $('#form').mkSetFormData(data);
                    Extension = data.F_FileExtensions;
                    var FileName = data.F_FileName.replace(Extension, '');
                    $("#FileName").val(F_FileName).focus().select();
                });
            }
        }
    };
    //保存表单
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $("#form").mkGetFormData(keyValue);
        postData["keyValue"] = keyValue;
        postData["F_FileName"] = $("#F_FileName").val() + Extension;
        $.mkSaveForm(top.$.rootUrl + '/OAModule/ResourceFile/SaveFileForm', postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    }
    page.init();
}


