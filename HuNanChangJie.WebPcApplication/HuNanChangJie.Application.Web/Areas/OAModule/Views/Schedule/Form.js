﻿/*
 * 日 期：2017.11.11
 * 描 述：日程管理
 */
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            //开始时间
            $('#F_StartTime').mkselect({
                maxHeight: 150
            });
            //结束时间
            $('#F_EndTime').mkselect({
                maxHeight: 150
            });
        },
        initData: function () {
            $("#F_StartDate").val(request('startDate'));
            $("#F_StartTime").mkselectSet(request('startTime'));
        }
    };
    acceptClick = function () {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData("");
        $.mkSaveForm(top.$.rootUrl + '/OAModule/Schedule/SaveForm?keyValue=', postData, function (res) {
            Changjie.frameTab.currentIframe().location.reload();
        });
    }
    page.init();
}


