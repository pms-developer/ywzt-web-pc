﻿/*
 * Copyright (c) 2013-2017 
 * 创建人：
 * 日 期：2018.06.20
 * 描 述：添加项目
 */
var keyValue = request('keyValue');

var acceptClick;
var ue;
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#status').mkselect();
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/OAModule/ProjectGantt/GetEntity?keyValue=' + keyValue, function (data) {//
                    //$('#form').mkSetFormData(data);
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData(keyValue);
        postData.progressByWorklog = false;
        postData.progressByWorklog = false;
        postData.relevance = 0;
        postData.type = "";
        postData.typeId = "";
        postData.level = 0;
        postData.depends = "";
        postData.canWrite = true;
        postData.collapsed = false;
        postData.hasChild = true;
        postData.assigs = "[]";
        postData.start = new Date(postData.start).getTime();
        postData.end = new Date(postData.end).getTime();
        postData.startIsMilestone = postData.startIsMilestone == 1 ? true : false;
        postData.endIsMilestone = postData.endIsMilestone == 1 ? true : false;
        $.mkSaveForm(top.$.rootUrl + '/OAModule/ProjectGantt/SaveEntity?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}