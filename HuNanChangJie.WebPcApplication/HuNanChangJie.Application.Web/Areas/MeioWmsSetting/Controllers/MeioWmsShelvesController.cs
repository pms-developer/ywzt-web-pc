﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment;

namespace HuNanChangJie.Application.Web.Areas.MeioWmsSetting.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-18 09:14
    /// 描 述：仓库配置-货架
    /// </summary>
    public class MeioWmsShelvesController : MvcControllerBase
    {
        private MeioWmsShelvesIBLL meioWmsShelvesIBLL = new MeioWmsShelvesBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioWmsShelvesIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }


        /// <summary>
        /// 获取下拉列表数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSelectList()
        {
            var data = meioWmsShelvesIBLL.GetSelectList();
            return Success(data);
        }



        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var meioWmsShelvesData = meioWmsShelvesIBLL.GetmeioWmsShelvesEntity( keyValue );
            var jsonData = new {
                meioWmsShelves = meioWmsShelvesData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioWmsShelvesIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<meioWmsShelvesEntity>();

            if (type == "add")
            {
                int num = meioWmsShelvesIBLL.checkNoCount(mainInfo.Code);
                if (num != 0)
                {
                    //CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    //mainInfo.Code = codeRuleIBLL.GetBillCode("CL_Code");
                    return Fail("Code重复！");
                }
            }

            meioWmsShelvesIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion


        #region  验证数据
        /// <summary>
        /// 账号不能重复
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="F_Account">账号</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult ExistCode(string keyValue, string Code)
        {
            bool res = WarehouseUtil.ExistCode("MeioWmsShelves", Code, keyValue);
            return JsonResult(res);
        }
        #endregion

    }
}
