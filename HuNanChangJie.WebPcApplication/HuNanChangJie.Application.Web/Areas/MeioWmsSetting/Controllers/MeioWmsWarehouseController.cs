﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.Organization;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment;

namespace HuNanChangJie.Application.Web.Areas.MeioWmsSetting.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-19 10:50
    /// 描 述：仓库
    /// </summary>
    public class MeioWmsWarehouseController : MvcControllerBase
    {
        private MeioWmsWarehouseIBLL meioWmsWarehouseIBLL = new MeioWmsWarehouseBLL();

        private MeioWmsShipperIBLL meioWmsShipperIBLL = new MeioWmsShipperBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UserForm()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ChangeForm()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioWmsWarehouseIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var meioWmsWarehouseData = meioWmsWarehouseIBLL.GetmeioWmsWarehouseEntity( keyValue );
            var jsonData = new {
                meioWmsWarehouse = meioWmsWarehouseData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取仓库授权数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetUserAuthList(string objectId)
        {
            var wList = meioWmsWarehouseIBLL.getUserWarehouse(objectId);
            var sList = meioWmsWarehouseIBLL.getUserShipper(objectId);
            var json = new
            {
                warehouse = wList.Select(x=>x.WarehouseID).ToList(),
                shipper = sList.Select(x=>x.ShipperID).ToList(),
            };
            return Success(json);
        }

        /// <summary>
        /// 获取仓库授权数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetCheckTreeByType(int type,string WarehouseID="")
        {
            if (type == 1)
            {
                //仓库授权
                var list = meioWmsWarehouseIBLL.GetSelectList();
                return Success(list);
            }
            if (type == 2)
            {
                //货主授权
                var list = meioWmsShipperIBLL.GetSelectList(WarehouseID);
                return Success(list);
            }
            return Success(new { });
        }


        /// <summary>
        /// 获取仓库授权数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetUserWarehouseList()
        {
            UserInfo userInfo = LoginUserInfo.Get();
            var wList = meioWmsWarehouseIBLL.getUserWarehouse(userInfo.userId);
            var ids = wList.Select(x=>x.WarehouseID).ToArray();
            string strIds = "";
            foreach (var item in ids)
            {
                strIds += "'" + item + "',";
            }
            strIds=strIds.TrimEnd(',');
            var list = meioWmsWarehouseIBLL.GetSelectList(" and t.id in ("+ strIds +") ");
            return Success(list);
        }



        #endregion

        #region  提交数据


        /// <summary>
        /// 仓库切换
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult userWarehouseChange(string id)
        {
            meioWmsWarehouseIBLL.userWarehouseChange(id);
            return Success("切换成功！");
        }


        /// <summary>
        /// 仓库初始化
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult userWarehouseInit()
        {
            meioWmsWarehouseIBLL.userWarehouseInit();
            return Success("初始化成功！");
        }

        /// <summary>
        /// 提交用户仓库授权数据
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveUserAuth(string objectId, string strWarehouseId, string strShipperId)
        {
            var wList = strWarehouseId.Split(',').ToList();
            var sList = strShipperId.Split(',').ToList();
            meioWmsWarehouseIBLL.setUserWarehouse(objectId, wList);
            meioWmsWarehouseIBLL.setUserShipper(objectId, sList);
            return Success("操作成功");
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioWmsWarehouseIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<meioWmsWarehouseEntity>();
            if (type == "add")
            {
                int num = meioWmsWarehouseIBLL.checkNoCount(mainInfo.Code);
                if (num != 0)
                {
                    //CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    //mainInfo.Code = codeRuleIBLL.GetBillCode("CL_Code");
                    return Fail("Code重复！");
                }
            }
            meioWmsWarehouseIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

        #region  验证数据
        /// <summary>
        /// 账号不能重复
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="F_Account">账号</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult ExistCode(string keyValue, string Code)
        {
            bool res = WarehouseUtil.ExistCode("MeioWmsWarehouse", Code, keyValue);
            return JsonResult(res);
        }
        #endregion

    }
}
