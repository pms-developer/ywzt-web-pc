﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.MeioWmsSetting.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-26 14:52
    /// 描 述：入库单
    /// </summary>
    public class MeioWmsWarehouseReceiptController : MvcControllerBase
    {
        private MeioWmsWarehouseReceiptIBLL meioWmsWarehouseReceiptIBLL = new MeioWmsWarehouseReceiptBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        /// <summary>
        /// 明细表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DetailForm()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioWmsWarehouseReceiptIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var meioWmsWarehouseReceiptData = meioWmsWarehouseReceiptIBLL.GetmeioWmsWarehouseReceiptEntity( keyValue );
            var meioWmsWarehouseReceiptDetailData = meioWmsWarehouseReceiptIBLL.GetmeioWmsWarehouseReceiptDetailList( meioWmsWarehouseReceiptData.ID );
            var meioWmsWarehouseReceiptReceivingDetailData = meioWmsWarehouseReceiptIBLL.GetmeioWmsWarehouseReceiptReceivingDetailList( meioWmsWarehouseReceiptData.ID );
            var meioWmsWarehouseReceiptItemsData = meioWmsWarehouseReceiptIBLL.GetmeioWmsWarehouseReceiptItemsList(meioWmsWarehouseReceiptData.ID);
            var jsonData = new {
                meioWmsWarehouseReceipt = meioWmsWarehouseReceiptData,
                meioWmsWarehouseReceiptDetail = meioWmsWarehouseReceiptDetailData,
                meioWmsWarehouseReceiptReceivingDetail = meioWmsWarehouseReceiptReceivingDetailData,
                meioWmsWarehouseReceiptItems = meioWmsWarehouseReceiptItemsData
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioWmsWarehouseReceiptIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strmeioWmsWarehouseReceiptDetailList, string strmeioWmsWarehouseReceiptReceivingDetailList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<meioWmsWarehouseReceiptEntity>();

            if (type == "add")
            {
                int num = meioWmsWarehouseReceiptIBLL.checkNoCount(mainInfo.No);
                if (num != 0)
                {
                    //CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    //mainInfo.Code = codeRuleIBLL.GetBillCode("CL_Code");
                    return Fail("入库单号重复！");
                }
            }

            var meioWmsWarehouseReceiptDetailList = strmeioWmsWarehouseReceiptDetailList.ToObject<List<meioWmsWarehouseReceiptDetailEntity>>();
            var meioWmsWarehouseReceiptReceivingDetailList = strmeioWmsWarehouseReceiptReceivingDetailList.ToObject<List<meioWmsWarehouseReceiptReceivingDetailEntity>>();
            meioWmsWarehouseReceiptIBLL.SaveEntity(keyValue,mainInfo,meioWmsWarehouseReceiptDetailList,meioWmsWarehouseReceiptReceivingDetailList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
