﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.MeioWmsSetting.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-30 16:19
    /// 描 述：工作单
    /// </summary>
    public class MeioWmsWarehouseWorkOrderController : MvcControllerBase
    {
        private MeioWmsWarehouseWorkOrderIBLL meioWmsWarehouseWorkOrderIBLL = new MeioWmsWarehouseWorkOrderBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioWmsWarehouseWorkOrderIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var meioWmsWarehouseWorkOrderData = meioWmsWarehouseWorkOrderIBLL.GetmeioWmsWarehouseWorkOrderEntity( keyValue );
            var meioWmsWarehouseWorkOrderDetailData = meioWmsWarehouseWorkOrderIBLL.GetmeioWmsWarehouseWorkOrderDetailList( meioWmsWarehouseWorkOrderData.ID );
            var jsonData = new {
                meioWmsWarehouseWorkOrder = meioWmsWarehouseWorkOrderData,
                meioWmsWarehouseWorkOrderDetail = meioWmsWarehouseWorkOrderDetailData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioWmsWarehouseWorkOrderIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strmeioWmsWarehouseWorkOrderDetailList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<meioWmsWarehouseWorkOrderEntity>();
            var meioWmsWarehouseWorkOrderDetailList = strmeioWmsWarehouseWorkOrderDetailList.ToObject<List<meioWmsWarehouseWorkOrderDetailEntity>>();
            meioWmsWarehouseWorkOrderIBLL.SaveEntity(keyValue,mainInfo,meioWmsWarehouseWorkOrderDetailList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
