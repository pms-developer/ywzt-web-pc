﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.MeioWmsSetting.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 09:47
    /// 描 述：定位货位范围
    /// </summary>
    public class MeioWmsWarehousePointStorageLocationRangeController : MvcControllerBase
    {
        private MeioWmsWarehousePointStorageLocationRangeIBLL meioWmsWarehousePointStorageLocationRangeIBLL = new MeioWmsWarehousePointStorageLocationRangeBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioWmsWarehousePointStorageLocationRangeIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var meioWmsWarehousePointStorageLocationRangeData = meioWmsWarehousePointStorageLocationRangeIBLL.GetmeioWmsWarehousePointStorageLocationRangeEntity( keyValue );
            var meioWmsWarehousePointStorageLocationRangeItemData = meioWmsWarehousePointStorageLocationRangeIBLL.GetmeioWmsWarehousePointStorageLocationRangeItemList( meioWmsWarehousePointStorageLocationRangeData.ID );
            var jsonData = new {
                meioWmsWarehousePointStorageLocationRange = meioWmsWarehousePointStorageLocationRangeData,
                meioWmsWarehousePointStorageLocationRangeItem = meioWmsWarehousePointStorageLocationRangeItemData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioWmsWarehousePointStorageLocationRangeIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strmeioWmsWarehousePointStorageLocationRangeItemList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<meioWmsWarehousePointStorageLocationRangeEntity>();
            var meioWmsWarehousePointStorageLocationRangeItemList = strmeioWmsWarehousePointStorageLocationRangeItemList.ToObject<List<meioWmsWarehousePointStorageLocationRangeItemEntity>>();
            meioWmsWarehousePointStorageLocationRangeIBLL.SaveEntity(keyValue,mainInfo,meioWmsWarehousePointStorageLocationRangeItemList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
