﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-04-26 14:52
 * 描  述：入库单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="meioWmsWarehouseReceipt";
var processCommitUrl=top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseReceipt/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var baseDel = [];
var receivingDetailDel = [];
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'meioWmsWarehouseReceiptDetail',"gridId":'meioWmsWarehouseReceiptDetail'});
               subGrid.push({"tableName":'meioWmsWarehouseReceiptReceivingDetail',"gridId":'meioWmsWarehouseReceiptReceivingDetail'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#Type').mkDataItemSelect({ code: 'meioWmsReceiptType' });
            $('#StartState').mkDataItemSelect({ code: 'meioWmsReceiptGoodsState' });
            $('#EndState').mkDataItemSelect({ code: 'meioWmsReceiptGoodsState' });
            $("#ShipperID").mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsShipper/GetUserShipperList',
                value: "ID",
                text: "Name"
            });


            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            $("#baseTab").on("click", function () {
                $('#form_tabs_sub ul li').eq(1).trigger('click');
                //$('#form_tabs_sub ul li').eq(0).trigger('click');
                setTimeout(() => {
                    $('#form_tabs_sub ul li').eq(0).trigger('click');
                })
            })

            $("#detailAdd").on("click", function () {
                Changjie.layerForm({
                    id: 'detailAddForm',
                    title: '明细新增',
                    url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseReceipt/DetailForm?no=' + $("#No").val(),
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick((d) => {
                            var meioWmsWarehouseReceiptDetailList = $('#meioWmsWarehouseReceiptDetail').jfGridGet('rowdatas');
                            meioWmsWarehouseReceiptDetailList.push(d);
                            $('#meioWmsWarehouseReceiptDetail').jfGridSet('refreshdata');
                        });
                    }
                });
            });

            $("#detailDel").on("click", function () {
                var one = $('#meioWmsWarehouseReceiptDetail').jfGridGet('rowdata');
                if (!one.ID) {
                    return;
                }
                var meioWmsWarehouseReceiptDetailList = $('#meioWmsWarehouseReceiptDetail').jfGridGet('rowdatas');
                baseDel.push(one.ID);
                var i = meioWmsWarehouseReceiptDetailList.findIndex(x => x == one);
                meioWmsWarehouseReceiptDetailList.splice(i, 1);
                $('#meioWmsWarehouseReceiptDetail').jfGridSet('refreshdata');
                
            });

            $("#detailClose").on("click", function () {
                var loginInfo = Changjie.clientdata.get(['userinfo']);
                var one = $('#meioWmsWarehouseReceiptDetail').jfGridGet('rowdata');
                one.CloseUserName = loginInfo.realName;
                one.CloseUserID = loginInfo.userId;
                one.CloseDate = Changjie.formatDate(new Date(), "yyyy-MM-dd hh:mm:ss") ;
                one.EditType = 2;
                $('#meioWmsWarehouseReceiptDetail').jfGridSet('refreshdata');
            });

            $("#detailUnClose").on("click", function () {
                var one = $('#meioWmsWarehouseReceiptDetail').jfGridGet('rowdata');
                one.CloseUserName="";
                one.CloseUserID="";
                one.EditType = 2;
                $('#meioWmsWarehouseReceiptDetail').jfGridSet('refreshdata');
            });

            $("#receivingDetailDel").on("click", function () {
                var one = $('#meioWmsWarehouseReceiptReceivingDetail').jfGridGet('rowdata');
                if (!one.ID) {
                    return;
                }
                var meioWmsWarehouseReceiptReceivingDetailList = $('#meioWmsWarehouseReceiptReceivingDetail').jfGridGet('rowdatas');
                receivingDetailDel.push(one.ID);
                var i = meioWmsWarehouseReceiptReceivingDetailList.findIndex(x => x == one);
                meioWmsWarehouseReceiptReceivingDetailList.splice(i, 1);
                $('#meioWmsWarehouseReceiptReceivingDetail').jfGridSet('refreshdata');
            })


            $('#meioWmsWarehouseReceiptDetail').jfGrid({
                headData: [
                    {
                        label: '货品编码', name: 'Code', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'入库单明细'                     },
                    {
                        label: '货品名称', name: 'Name', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'入库单明细'                     },
                    {
                        label: '单位', name: 'Unit', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'入库单明细'                     },
                    {
                        label: '单位数量', name: 'UnitNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'入库单明细'                     },
                    {
                        label: '总数量', name: 'TotalNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'入库单明细'                     },
                    {
                        label: '待收数量', name: 'CollectedNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'入库单明细'                     },
                    {
                        label: '已收数量', name: 'ReceivedNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'入库单明细'                     },
                    {
                        label: '关闭用户', name: 'CloseUserName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'入库单明细'                     },
                    {
                        label: '关闭时间', name: 'CloseDate', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype: 'string', tabname: '入库单明细', formatter: function (value, row) {
                            if (row.CloseUserName) {
                                return value;
                            }
                            return '';
                        }                     },
                ],
                mainId:"ID",
                bindTable:"meioWmsWarehouseReceiptDetail",
                //isEdit: true,
                height: 300,
                rowdatas:[],
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
            $('#meioWmsWarehouseReceiptReceivingDetail').jfGrid({
                headData: [
                    {
                        label: '仓库', name: 'ShipperID', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                    {
                        label: '货品编码', name: 'Code', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                    {
                        label: '货品名称', name: 'Name', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                    {
                        label: '状态', name: 'State', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                    {
                        label: '单位', name: 'Unit', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                    {
                        label: '单位数量', name: 'UnitNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                    {
                        label: '数量', name: 'Num', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                    {
                        label: '质量状态', name: 'QualityState', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                    {
                        label: '定位规则', name: 'PositioningRule', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                    {
                        label: '目标货位', name: 'TargetStorageLocation', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'收货明细'                     },
                ],
                mainId:"ID",
                bindTable:"meioWmsWarehouseReceiptReceivingDetail",
                //isEdit: true,
                height: 300,
                rowdatas:[],
/*                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },*/
                onMinusRow: function(row,rows,headData){
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseReceipt/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (id == 'meioWmsWarehouseReceiptItems') {
                            data[id].forEach(x => {
                                $("#tab2").append('<div class="col-xs-6 mk-form-item" data-table="meioWmsWarehouseReceipt"><div class= "mk-form-item-title" >' +
                                    x.ItemName + '</div><input type="checkbox" checked /></div>');
                            })
                        }
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                            
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseReceipt/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
               deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
               var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
               if (!info.isPass) {
                  isgridpass = false;
                  errorInfos.push(info.errorCells);
               }
    }
    if (baseDel.length > 0) {
        deleteList.push({ "TableName": "meioWmsWarehouseReceiptDetail", "idList": baseDel.join(',') })
    }
    if (receivingDetailDel.length > 0) {
        deleteList.push({ "TableName": "meioWmsWarehouseReceiptReceivingDetail", "idList": receivingDetailDel.join(',') })
    }
    
    
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="meioWmsWarehouseReceipt"]').mkGetFormData());
    postData.strmeioWmsWarehouseReceiptDetailList = JSON.stringify($('#meioWmsWarehouseReceiptDetail').jfGridGet('rowdatas'));
        postData.strmeioWmsWarehouseReceiptReceivingDetailList = JSON.stringify($('#meioWmsWarehouseReceiptReceivingDetail').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
