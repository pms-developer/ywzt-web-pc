﻿
var acceptClick;

var no = request('no');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $("#ShipperID").mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsShipper/GetUserShipperList',
                value: "ID",
                text: "Name"
            });
            $('#Unit').mkDataItemSelect({ code: 'meioWmsReceiptGoodsUnit' });
            $('#State').mkDataItemSelect({ code: 'meioWmsReceiptGoodsQualityState' });
            $("#No").val(no)

        },
        initData: function () {

        }
    }
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        callBack(postData);
        return true;
    };
    page.init();
}


var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    var postData = $('[data-table="meioWmsWarehouseReceiptDetail"]').mkGetFormData();
    postData.EditType = 1;
    postData.ID = top.Changjie.newGuid();
    return postData;
}