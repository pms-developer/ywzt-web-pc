﻿var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
        },
        initGird: function () {
            Changjie.httpAsyncGet(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouse/GetUserWarehouseList', function (res) {
                console.log(res);
                var str = "";
                res.data.forEach(function (x) {
                    if (x.ID == $("#nowWarehouse").val()) {
                        str += "<li class='whOne' refid='" + x.ID +"' ><a>" + x.Name + "[" + x.Code +"]" +"--当前</a></li>";
                    } else {
                        str += "<li class='whOne' refid='" + x.ID +"' ><a>" + x.Name + "[" + x.Code + "]" + "</a></li>";
                    }
                })

                $("#whPickList").append(str);
                $(".whOne").on("click", function () {

                    var postData = {
                        id: $(this).attr("refid"),
                    };
                    $.mkSaveForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouse/userWarehouseChange', postData, function (res) {
                        if (res.code == 200) {

                        }
                    });
                })
            })
        }
    }
    page.init();
}