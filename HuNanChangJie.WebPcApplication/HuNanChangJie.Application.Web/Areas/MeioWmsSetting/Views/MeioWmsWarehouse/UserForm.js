﻿/*
 * 日 期：2017.04.05
 * 描 述：功能模块	
 */
var objectId = request('objectId');
var objectType = request('objectType');
var g_currentstep = 1;
var bootstrap = function ($, Changjie) {
    "use strict";

    var selectData;

    var treeLoadDataFinish = [];
    var checkModuleIds = [];

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        loadTree: function (type,str="") {
            Changjie.httpAsyncGet(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouse/GetCheckTreeByType?type=' + type +'&WarehouseID='+str, function (res) {
                if (res.code == 200) {
                    $.each(res.data, function (ii, ee) {
                        ee.isexpand = false;
                        ee.showcheck = true;
                        ee.id = ee.ID;
                        ee.text = ee.Name + "[" + ee.Code + "]";
                        if (type == 2) {
                            ee.text += "--" + ee.WarehouseName;
                        }
                        if (ee.ChildNodes) {
                            $.each(ee.ChildNodes, function (iii, eee) {
                                eee.isexpand = false;
                            })
                        }
                    })

                    $('#step-' + type).mktree({
                        data: res.data
                    });

                    if (!!selectData) {
                        if (type == 1) {
                            $('#step-1').mktreeSet('setCheck', selectData.warehouse);
                        }
                        else if (type == 2) {
                            $('#step-2').mktreeSet('setCheck', selectData.shipper);
                        }
                    }

                    treeLoadDataFinish[type - 1] = true;
                }
            })
        },
        /*绑定事件和初始化控件*/
        bind: function () {

            // 加载导向
            $('#wizard').wizard().on('change', function (e, data) {
                g_currentstep = data.currentStep;
                console.log(data);
                var $finish = $("#btn_finish");
                var $next = $("#btn_next");
                if (data.currentStep == 2) {
                    $next.attr('disabled', 'disabled');
                    $finish.removeAttr('disabled');
                    var wIds = $('#step-1').mktreeSet('getCheckNodeIds');
                    page.loadTree(2, wIds.join(','));
                    
                } else {
                    $finish.attr('disabled', 'disabled');
                    $next.removeAttr('disabled');
                }

                $("#checkall").unbind('click').removeProp("checked").on('click', function () {
                    page.bindallcheck(this)
                });
            });
            // 保存数据按钮
            $("#btn_finish").on('click', page.save);
            $("#checkall").on('click', function () {
                page.bindallcheck(this)
            });
        },
        /*初始化数据*/
        initData: function () {
            if (!!objectId) {
                $.mkSetForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouse/GetUserAuthList?objectId=' + objectId, function (data) {//
                    selectData = data;
                    page.loadTree(1);

                    setTimeout(function () {
                        page.loadTree(2);
                    }, 50)

                   /* setTimeout(function () {
                        page.loadTree(3);
                    }, 100)

                    setTimeout(function () {
                        page.loadTree(4);
                    }, 150)*/
                });
            }
        },
        bindallcheck: function (ee) {
            if (ee.checked) {
                if (g_currentstep == 2) {
                    $('#step-2').mktreeSet('newAllCheck');
                }
                else if (g_currentstep == 3) {
                    $('#step-3').mktreeSet('newAllCheck');
                }
                else if (g_currentstep == 4) {
                    $('#step-4').mktreeSet('newAllCheck');
                }
                else {
                    $('#step-1').mktreeSet('newAllCheck');
                }
            }
            else {
                if (g_currentstep == 2) {
                    $('#step-2').mktreeSet('allNoCheck');
                }
                else if (g_currentstep == 3) {
                    $('#step-3').mktreeSet('allNoCheck');
                }
                else if (g_currentstep == 4) {
                    $('#step-4').mktreeSet('allNoCheck');
                }
                else
                    $('#step-1').mktreeSet('allNoCheck');
            }
        },
        /*保存数据*/
        save: function () {
            var checkModuleIds = [], buttonList = []
            var checkColumnIds = $('#step-1').mktreeSet('getCheckNodeIds');
            var checkButtonIds = $('#step-2').mktreeSet('getCheckNodeIds');
            


            $.each(checkColumnIds, function (id, item) {
                if (item.indexOf('_changjie_moduleId') == -1) {
                    checkModuleIds.push(item);
                }
            });
            $.each(checkButtonIds, function (id, item) {
                if (item.indexOf('_changjie_moduleId') == -1) {
                    buttonList.push(item);
                }
            });


            var postData = {
                objectId: objectId,
                objectType: objectType,
                strWarehouseId: String(checkModuleIds),
                strShipperId: String(buttonList)
            };
            $.mkSaveForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouse/SaveUserAuth', postData, function (res) {
                if (res.code == 200) {

                }
            });
        }
    };

    page.init();
}