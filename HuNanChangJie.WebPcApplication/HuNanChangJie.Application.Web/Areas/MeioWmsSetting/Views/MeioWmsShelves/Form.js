﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-04-18 09:14
 * 描  述：仓库配置-货架
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="meioWmsShelves";
var processCommitUrl=top.$.rootUrl + '/MeioWmsSetting/MeioWmsShelves/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
                type = "edit";
                $('#Code').attr('readonly', 'readonly');
                $('#Code').attr('unselectable', 'on');
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
            }
            page.bind();
            page.initData();
            var ctltype11=$("#Length").attr("type");;
            var ctltype12 = $("#Width").attr("type");;
            var ctltype13 = $("#Height").attr("type");;
            if (ctltype11 == 'text' && ctltype12 == 'text') {
                 $('#Length').on('input propertychange', function () {
                     var value1 = $(this).val();
                     var value2 = $('#Width').val();
                     var value3 = $('#Height').val();
                     var r = (value1 * value2 * value3).toFixed(2);
                     $('#MaximumCapacity').val(r);
                 });
                 $('#Width').on('input propertychange', function () {
                     var value1 = $('#Length').val();
                     var value2 = $(this).val();
                     var value3 = $('#Height').val();
                     var r = (value1 * value2 * value3).toFixed(2);
                     $('#MaximumCapacity').val(r);
                 });
                $('#Height').on('input propertychange', function () {
                    var value1 = $('#Length').val();
                    var value2 = $('#Width').val();
                    var value3 = $(this).val();
                    var r = (value1 * value2 * value3).toFixed(2);
                    $('#MaximumCapacity').val(r);
                });
            }
            else if (ctltype11 == 'text' && ctltype12 == 'mkselect') {
                 $('#Length').on('input propertychange', function () {
                     var value1 = $(this).val();
                     var value2 = $('#Width').mkselectGetText();
                     $('#MaximumCapacity').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
                 $('#Width').mkselect().on('change', function () {
                     var value1 = $('#Length').val();
                     var value2 = $(this).mkselectGetText();
                     $('#MaximumCapacity').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
            }
            else if (ctltype11 == 'mkselect' && ctltype12 == 'text') {
                 $('#Length').mkselect().on('change', function () {
                     var value1 = $(this).mkselectGetText();
                     var value2 = $('#Width').val();
                     $('#MaximumCapacity').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
                 $('#Width').on('input propertychange', function () {
                     var value1 = $('#Length').mkselectGetText();
                     var value2 = $(this).val();
                     $('#MaximumCapacity').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
            }
            
        },
        bind: function () {
            $('#CapacityUnit').mkDataItemSelect({ code: 'StorageCapacityUnit' });
            $('#Code').on('blur', function () {
                $.mkExistField(keyValue, 'Code', top.$.rootUrl + '/MeioWmsSetting/MeioWmsShelves/ExistCode');
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsShelves/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsShelves/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {l
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
     return postData;
}
