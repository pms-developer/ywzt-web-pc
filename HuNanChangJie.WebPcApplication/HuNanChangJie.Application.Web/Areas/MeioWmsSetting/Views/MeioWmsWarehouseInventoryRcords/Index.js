﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-05-06 11:14
 * 描  述：库存交易记录
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseInventoryRcords/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseInventoryRcords/GetPageList',
                headData: [
                    { label: "变动类型", name: "Type", width: 100, align: "left"},
                    { label: "单号", name: "No", width: 100, align: "left"},
                    { label: "货主", name: "ShipperID", width: 100, align: "left"},
                    { label: "货品编码", name: "GoodCode", width: 100, align: "left"},
                    { label: "货品名称", name: "GoodName", width: 100, align: "left"},
                    { label: "质量状态", name: "QualityState", width: 100, align: "left"},
                    { label: "尺码", name: "Size", width: 100, align: "left"},
                    { label: "颜色", name: "Color", width: 100, align: "left"},
                    { label: "属性1", name: "Attr1", width: 100, align: "left"},
                    { label: "操作人", name: "OperUser", width: 100, align: "left"},
                    { label: "货位", name: "StorageLocation", width: 100, align: "left"},
                    { label: "进/出", name: "InOut", width: 100, align: "left"},
                    { label: "数量", name: "Num", width: 100, align: "left"},
                    { label: "之前数量", name: "OldNum", width: 100, align: "left"},
                    { label: "之后数量", name: "NewNum", width: 100, align: "left"},
                    { label: "操作时间", name: "OperDate", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseInventoryRcords/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseInventoryRcords/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
