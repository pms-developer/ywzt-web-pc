﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-04-18 09:54
 * 描  述：仓库配置-货位
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);

            $("#Area").mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsArea/GetSelectList',
                value: "ID",
                text: "name"
            });

            //$('#Area').mkDataSourceSelect({ code: 'meioWmsArea',value: 'id',text: 'title' });
            //$('#Type').mkDataSourceSelect({ code: 'meioWmsShelves', value: 'id', text: 'title' });

            $("#Type").mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsShelves/GetSelectList',
                value: "ID",
                text: "Name"
            });

            $('#Cate').mkDataItemSelect({ code: 'meioWmsLocalClass' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsStorageLocation/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
            //  导入
            $('#import').on('click', function () {
            });
            //  导出
            $('#export').on('click', function () {
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsStorageLocation/GetPageList',
                headData: [
                    { label: "编码", name: "Code", width: 100, align: "left"},
                    {
                        label: "区域", name: "AraeName", width: 100, align: "left"
                    },
                    {
                        label: "货位类型", name: "TypeName", width: 100, align: "left",
                    },
                    { label: "货位种类", name: "Cate", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'meioWmsLocalClass',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "最大货品种类", name: "MaxClassType", width: 100, align: "left"},
                    { label: "最大批次数", name: "MaxPi", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsStorageLocation/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsStorageLocation/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
