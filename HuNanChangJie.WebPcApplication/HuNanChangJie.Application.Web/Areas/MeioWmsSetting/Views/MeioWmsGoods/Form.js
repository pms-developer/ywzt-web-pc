﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-04-22 17:49
 * 描  述：货品列表
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="meioWmsGoods";
var processCommitUrl=top.$.rootUrl + '/MeioWmsSetting/MeioWmsGoods/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
                type = "edit";
                $('#Code').attr('readonly', 'readonly');
                $('#Code').attr('unselectable', 'on');
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
            var ctltype11 = $("#Lenght").attr("type");;
            var ctltype12 = $("#Width").attr("type");;
            var ctltype13 = $("#Height").attr("type");;
            if (ctltype11 == 'text' && ctltype12 == 'text') {
                $('#Lenght').on('input propertychange', function () {
                    var value1 = $(this).val();
                    var value2 = $('#Width').val();
                    var value3 = $('#Height').val();
                    var r = (value1 * value2 * value3).toFixed(2);
                    $('#Volume').val(r);
                });
                $('#Width').on('input propertychange', function () {
                    var value1 = $('#Lenght').val();
                    var value2 = $(this).val();
                    var value3 = $('#Height').val();
                    var r = (value1 * value2 * value3).toFixed(2);
                    $('#Volume').val(r);
                });
                $('#Height').on('input propertychange', function () {
                    var value1 = $('#Lenght').val();
                    var value2 = $('#Width').val();
                    var value3 = $(this).val();
                    var r = (value1 * value2 * value3).toFixed(2);
                    $('#Volume').val(r);
                });
            }
        },
        bind: function () {
            $('#Type').mkDataItemSelect({ code: 'meio_product_type' });
            $('#Enabled').mkDataItemSelect({ code: 'BESF' });
            $('#Enabled').mkselectSet('true')
            $("#ShipperID").mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsShipper/GetUserShipperList',
                value: "ID",
                text: "Name"
            });
            $('#Code').on('blur', function () {
                $.mkExistField(keyValue, 'Code', top.$.rootUrl + '/MeioWmsSetting/MeioWmsGoods/ExistCode');
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsGoods/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsGoods/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
     return postData;
}
