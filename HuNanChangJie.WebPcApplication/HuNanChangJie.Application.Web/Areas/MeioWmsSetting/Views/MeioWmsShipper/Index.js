﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-04-18 10:46
 * 描  述：货主信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#Enabled').mkDataItemSelect({ code: 'BESF' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsShipper/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsShipper/GetPageList',
                headData: [
                    {
                        label: "编码", name: "Code", width: 100, align: "left",
                        /*formatter: function (value, row) {
                            return "<a style='color:blue' onclick=\"openWin('" + row.ID + "','" + row.Name + "')\" >" + value + "</a>";
                        }*/
                    },
                    {
                        label: "名称", name: "Name", width: 100, align: "left",
                        /*formatter: function (value, row) {
                            return "<a style='color:blue' onclick=\"openWin('" + row.ID + "','" + row.Name + "')\" >" + value + "</a>";
                        }*/
                    },
                    { label: "是否启用", name: "Enabled", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            console.log(value);
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: ''+value,
                                 code: 'BESF',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left" },
                    { label: "创建用户", name: "CreationName", width: 100, align: "left" },
                    { label: "更新时间", name: "ModificationDate", width: 100, align: "left" },
                    { label: "更新用户", name: "ModificationName", width: 100, align: "left" },
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsShipper/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsShipper/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
};

var openWin = function (id, name) {
    parent.$(".mk-second-menu-list").hide();
    var $menu = parent.$("#projectmenu");
    $menu.find(".mk-menu-item").each(function (index, element) {
        var $this = $(element);
        $this.attr("projectId", id);
    });
    $menu.show();
    var url = "/MeioWmsSetting/MeioWmsShipper/Main?projectId=" + id + "&name=" + name;
    top.Changjie.frameTab.closeallprojecttab();
    top.Changjie.frameTab.open({
        F_ModuleId: id,
        F_Icon: 'fa fa-file-text-o',
        F_FullName: name,
        F_UrlAddress: url,
        IsProject: true
    });
}
