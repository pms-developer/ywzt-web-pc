﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-06 09:47
 * 描  述：定位货位范围
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="meioWmsWarehousePointStorageLocationRange";
var processCommitUrl=top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointStorageLocationRange/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');

var dbBase = "";
var dbBaseText = "";

var baseDel = [];

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'meioWmsWarehousePointStorageLocationRangeItem',"gridId":'meioWmsWarehousePointStorageLocationRangeItem'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');



            $('#baseDbTable').mkDataItemSelect({ code: 'meioWmsStorageLocationRangeDict' });

            $('#baseDbTable').on("change", function () {
                //获取表字段并写入到控件中
                dbBase = $(this).mkselectGet();
                dbBaseText = $(this).mkselectGetText();
                
                Changjie.httpGet("/SystemModule/DatabaseTable/GetFieldList?databaseLinkId=b3aaa000-1223-40d2-8ae2-28f980bc8f9c&tableName=" + $(this).mkselectGet(), {}, function (res) {
                    $("#Field").html('');
                    let str = '';
                    res.data.forEach(function (x) {
                        str += "<option value='" + x.id + "' >" + x.text + "</option>";
                                              
                    })
                    $("#Field").append(str);
                })

            })

            $("#btnRangeUp").on("click", function () {
                var meioWmsWarehousePointStorageLocationRangeItemList = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdatas');
                var one = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdata');
                if (!one.ID) {
                    return;
                }
                var i = meioWmsWarehousePointStorageLocationRangeItemList.findIndex(x => x == one);
                if (i > 0) {
                    meioWmsWarehousePointStorageLocationRangeItemList[i - 1].SortCode = one.SortCode;
                    meioWmsWarehousePointStorageLocationRangeItemList[i - 1].EditType = 2;
                    one.SortCode--;
                    one.EditType = 2;
                }
                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('moveUp', i);
                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('refreshdata');
            })

            $("#btnRangeDown").on("click", function () {
                var meioWmsWarehousePointStorageLocationRangeItemList = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdatas');
                var one = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdata');
                if (!one.ID) {
                    return;
                }
                var i = meioWmsWarehousePointStorageLocationRangeItemList.findIndex(x => x == one);
                if (i < meioWmsWarehousePointStorageLocationRangeItemList.length) {

                    meioWmsWarehousePointStorageLocationRangeItemList[i + 1].SortCode = one.SortCode;
                    meioWmsWarehousePointStorageLocationRangeItemList[i + 1].EditType = 2;
                    one.SortCode++;
                    one.EditType = 2;
                }
                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('moveDown', i);
                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('refreshdata');
            })

            $("#btnRangeDel").on("click", function () {
                var one = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdata');
                if (!one.ID) {
                    return;
                }
                var meioWmsWarehouseReceiptDetailList = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdatas');
                baseDel.push(one.ID);
                var i = meioWmsWarehouseReceiptDetailList.findIndex(x => x == one);
                meioWmsWarehouseReceiptDetailList.splice(i, 1);
                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('refreshdata');
            })


            $("#btnRangeLeftKuo").on("click", function () {
                var one = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdata');
                if (!one.ID) {
                    return;
                }
                one.EditType = 2;
                one.LogicText = "(" + one.LogicText;
                one.Logic = "(" + one.Logic;
                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('refreshdata');
            })

            $("#btnRangeRightKuo").on("click", function () {
                var one = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdata');
                if (!one.ID) {
                    return;
                }
                one.EditType = 2;
                one.LogicText = one.LogicText+")";
                one.Logic = one.Logic + ")";
                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('refreshdata');
            })

            $("#btnRangeDelKuo").on("click", function () {
                var one = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdata');
                if (!one.ID) {
                    return;
                }
                one.EditType = 2;
                if (one.LogicText[0] == "(") {
                    one.LogicText = one.LogicText.slice(0, 0) + one.LogicText.slice(0 + 1);
                }
                if (one.LogicText[one.LogicText.length - 1] == ")") {
                    one.LogicText = one.LogicText.slice(0, one.LogicText.length - 1) + one.LogicText.slice((one.LogicText.length - 1) + 1);
                }
                if (one.Logic[0] == "(") {
                    one.Logic = one.Logic.slice(0, 0) + one.Logic.slice(0 + 1);
                }
                if (one.Logic[one.Logic.length - 1] == ")") {
                    one.Logic = one.Logic.slice(0, one.Logic.length - 1) + one.Logic.slice((one.Logic.length - 1) + 1);
                }
                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('refreshdata');
            })


            $("#btnRangeChange").on("click", function () {
                var one = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdata');
                if (!one.ID) {
                    return;
                }
                let andOr = $("#andOr").val();
                let Field = $("#Field").val();
                let FieldText = $("#Field").find("option:selected").text();
                let oper = $("#oper").val();
                let key = $("#key").val();
                let isStr = $("#isStr").is(":checked");

                if (!andOr || !Field || !oper || !key) {
                    return;
                }


                var str = '';
                var lg = '';
                if (isStr) {
                    str = andOr + " " + dbBaseText + "." + FieldText + " " + oper + " '" + key + "'";
                    lg = andOr + " " + dbBase + "." + Field + " " + oper + " '" + key + "'";
                } else {
                    str = andOr + " " + dbBaseText + "." + FieldText + " " + oper + " " + key
                    if (key[0] == ":") {
                        key = "@" + key;
                    }
                    lg = andOr + " " + dbBase + "." + Field + " " + oper + " " + key;
                }



                one.EditType = 2;
                one.LogicText = str;
                one.Logic = lg;


                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('refreshdata');


                $("#andOr").val("");
                $("#Field").val("");
                $("#oper").val("");
                $("#key").val("");
                $("#isStr").prop("checked", false);

            })



            $("#btnRangeAdd").on("click", function () {
                let andOr = $("#andOr").val();
                let Field = $("#Field").val();
                let FieldText = $("#Field").find("option:selected").text();
                let oper = $("#oper").val();
                let key = $("#key").val();
                let isStr = $("#isStr").is(":checked");

                if (!andOr || !Field || !oper || !key) {
                    return;
                }

                var meioWmsWarehousePointStorageLocationRangeItemList = $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdatas');
                var str = '';
                var lg = '';
                if (isStr) {
                    str = andOr + " " + dbBaseText + "." + FieldText + " " + oper + " '" + key + "'";
                    lg = andOr + " " + dbBase + "." + Field + " " + oper + " '" + key + "'";
                } else {
                    str = andOr + " " + dbBaseText + "." + FieldText + " " + oper + " " + key
                    if (key[0] == ":") {
                        key = "@" + key;
                    }
                    lg = andOr + " " + dbBase + "." + Field + " " + oper + " " + key;
                }
                
                meioWmsWarehousePointStorageLocationRangeItemList.push({
                    "ID": Changjie.newGuid(),
                    "EditType":1,
                    "LogicText": str,
                    "Logic": lg,
                    "SortCode": meioWmsWarehousePointStorageLocationRangeItemList.length+1
                });
                $('#meioWmsWarehousePointStorageLocationRangeItem').jfGridSet('refreshdata');



                $("#andOr").val("");
                $("#Field").val("");
                $("#oper").val("");
                $("#key").val("");
                $("#isStr").prop("checked", false);

            })



            $('#meioWmsWarehousePointStorageLocationRangeItem').jfGrid({
                headData: [
                    {
                        label: '展示处理逻辑', name: 'LogicText', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '条件'
                    },
                    {
                        label: '实际处理逻辑', name: 'Logic', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '条件'
                    }
                ],
                mainId:"ID",
                bindTable:"meioWmsWarehousePointStorageLocationRangeItem",
                //isEdit: true,
                height: 300,
                rowdatas:[
                /*{
                     "SortCode": 1,
                     "rowState": 1,
                     "ID": Changjie.newGuid(),
                     "EditType": 1,
                    }*/
                ],
                onAddRow: function (row, rows) {
                     /*row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;*/
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointStorageLocationRange/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointStorageLocationRange/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
            deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
            var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
            if (!info.isPass) {
                isgridpass = false;
                errorInfos.push(info.errorCells);
            }
        }

        if (baseDel.length > 0) {
            deleteList.push({ "TableName": "meioWmsWarehousePointStorageLocationRangeItem", "idList": baseDel.join(',') })
        }

         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="meioWmsWarehousePointStorageLocationRange"]').mkGetFormData());
        postData.strmeioWmsWarehousePointStorageLocationRangeItemList = JSON.stringify($('#meioWmsWarehousePointStorageLocationRangeItem').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
