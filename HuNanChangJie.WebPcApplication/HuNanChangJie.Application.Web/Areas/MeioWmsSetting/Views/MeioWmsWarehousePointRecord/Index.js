﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-05-06 11:50
 * 描  述：上架定位
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointRecord/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointRecord/GetPageList',
                headData: [
                    { label: "状态", name: "State", width: 100, align: "left"},
                    { label: "货品编码", name: "Code", width: 100, align: "left"},
                    { label: "货品名称", name: "Name", width: 100, align: "left"},
                    { label: "单位", name: "Unit", width: 100, align: "left"},
                    { label: "单位规格", name: "UnitSpec", width: 100, align: "left"},
                    { label: "单位数量", name: "UnitNum", width: 100, align: "left"},
                    { label: "数量", name: "Num", width: 100, align: "left"},
                    { label: "定位规则", name: "ListingRule", width: 100, align: "left"},
                    { label: "上架货位", name: "StorageLocation", width: 100, align: "left"},
                    { label: "库存状态", name: "InventoryState", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointRecord/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointRecord/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
