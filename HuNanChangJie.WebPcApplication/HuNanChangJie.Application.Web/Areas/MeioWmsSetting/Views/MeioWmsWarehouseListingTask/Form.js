﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-06 10:16
 * 描  述：上架任务
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="meioWmsWarehouseListingTask";
var processCommitUrl=top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseListingTask/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'meioWmsWarehouseListingTaskItem',"gridId":'meioWmsWarehouseListingTaskItem'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#meioWmsWarehouseListingTaskItem').jfGrid({
                headData: [
                    {
                        label: '货主', name: 'ShipperID', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '货品编码', name: 'GoodCode', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '货品名称', name: 'GoodName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '单位', name: 'Unit', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '单位规格', name: 'UnitSpec', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '单位数量', name: 'UnitNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '总数量', name: 'TotalNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '从货位', name: 'FromStorageLocation', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '到货位', name: 'ToStorageLocation', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '从数量', name: 'FromNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '到数量', name: 'ToNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '质量状态', name: 'QualityState', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                    {
                        label: '状态', name: 'State', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'上架任务'                     },
                ],
                mainId:"ID",
                bindTable:"meioWmsWarehouseListingTaskItem",
                isEdit: true,
                height: 300,
                rowdatas:[
                {
                     "SortCode": 1,
                     "rowState": 1,
                     "ID": Changjie.newGuid(),
                     "EditType": 1,
                }],
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseListingTask/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehouseListingTask/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="meioWmsWarehouseListingTask"]').mkGetFormData());
        postData.strmeioWmsWarehouseListingTaskItemList = JSON.stringify($('#meioWmsWarehouseListingTaskItem').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
