﻿
var acceptClick;

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $('#Unit').mkDataItemSelect({ code: 'StorageCapacityUnit' });
            $('#BeSplit').mkDataItemSelect({ code: 'WZSF' });
            $("#PointStorageLocationRange").mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsArea/GetSelectList',
                value: "ID",
                text: "name"
            });
        }
    }
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        callBack(postData);
        return true;
    };
    page.init();
}

var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var postData = $('[data-table="meioWmsWarehousePointListingRuleDetail"]').mkGetFormData();
    postData.EditType = 1;
    postData.ID = top.Changjie.newGuid();
    return postData;
}