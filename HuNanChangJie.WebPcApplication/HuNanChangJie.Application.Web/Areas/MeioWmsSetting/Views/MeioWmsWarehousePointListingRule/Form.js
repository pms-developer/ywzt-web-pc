﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-06 09:58
 * 描  述：上架定位规则
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="meioWmsWarehousePointListingRule";
var processCommitUrl=top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointListingRule/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'meioWmsWarehousePointListingRuleItem',"gridId":'meioWmsWarehousePointListingRuleItem'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');


            $("#btnDetailAdd").on("click", function () {
                Changjie.layerForm({
                    id: 'detailAddForm',
                    title: '明细新增',
                    url: top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointListingRule/DetailForm',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick((d) => {
                            console.log(d);
                            /*var meioWmsWarehouseReceiptDetailList = $('#meioWmsWarehouseReceiptDetail').jfGridGet('rowdatas');
                            meioWmsWarehouseReceiptDetailList.push(d);
                            $('#meioWmsWarehouseReceiptDetail').jfGridSet('refreshdata');*/
                        });
                    }
                });
            })

            $("#btnDetailDel").on("click", function () { })

            $('#meioWmsWarehousePointListingRuleItem').jfGrid({
                headData: [
                    {
                        label: '定位范围', name: 'PointStorageLocationRange', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'货位选择'                     },
                    {
                        label: '单位', name: 'Unit', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'货位选择'                     },
                    {
                        label: '是否拆分', name: 'BeSplit', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'货位选择'                     },
                    {
                        label: '序号', name: 'Sort', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'货位选择'                     },
                ],
                mainId:"ID",
                bindTable:"meioWmsWarehousePointListingRuleItem",
                //isEdit: true,
                height: 300,
                rowdatas:[
                /*{
                     "SortCode": 1,
                     "rowState": 1,
                     "ID": Changjie.newGuid(),
                     "EditType": 1,
                    }*/
                ],
                onAddRow: function(row,rows){
                    /* row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;*/
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointListingRule/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/MeioWmsSetting/MeioWmsWarehousePointListingRule/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="meioWmsWarehousePointListingRule"]').mkGetFormData());
        postData.strmeioWmsWarehousePointListingRuleItemList = JSON.stringify($('#meioWmsWarehousePointListingRuleItem').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
