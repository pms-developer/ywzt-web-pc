﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-10-19 14:19
 * 描  述：采购入库
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Purchase_InStorage";
var processCommitUrl = top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) { }
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) { }
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Purchase_InStorageDetails', "gridId": 'Purchase_InStorageDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'PurchaseInStorageCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#fk_PurchaseOrderId').mkselect({})
            $('#Base_WarehouseId').mkselect({})

            // var sqlWhere = "Project_ID='" + projectId + "'";

            // $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname', strWhere: sqlWhere });
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);
                var params = { "ProjectID": projectId, "AuditStatus": "2" };
                if (type == "add") {
                    params.IsFinishInStorage = 0;
                }

                //仓库
                var parama = { "ProjectId": projectId };
                Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/Warehouse/GetPageList', { queryJson: JSON.stringify(parama), pagination: JSON.stringify({ "rows": 500, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {

                    $("#Base_WarehouseId").mkselectRefresh({
                        value: "ID",
                        text: "Name",
                        title: "Name",
                        data: data.rows
                    })
                });

                //采购订单
                Changjie.httpAsync('GET', top.$.rootUrl + '/PurchaseModule/PurchaseOrder/GetPageList', { queryJson: JSON.stringify(params), pagination: JSON.stringify({ "rows": 500, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                    $("#fk_PurchaseOrderId").mkselectRefresh({
                        value: "ID",
                        text: "Abstract",
                        title: "Abstract",
                        data: data.rows
                    })
                });
            }

            $('#ProjectID').on("change", function () {
                var projectEx = $('#ProjectID').mkselectGetEx();
                if (projectEx) {
                    var params = { "ProjectID": projectEx.project_id, "AuditStatus": "2" };
                    if (type == "add") {
                        params.IsFinishInStorage = 0;
                    }

                    //仓库
                    var parama = { "ProjectId": projectEx.project_id };
                    Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/Warehouse/GetPageList', { queryJson: JSON.stringify(parama), pagination: JSON.stringify({ "rows": 500, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {

                        $("#Base_WarehouseId").mkselectRefresh({
                            value: "ID",
                            text: "Name",
                            title: "Name",
                            data: data.rows
                        })
                    });

                    //采购订单
                    Changjie.httpAsync('GET', top.$.rootUrl + '/PurchaseModule/PurchaseOrder/GetPageList', { queryJson: JSON.stringify(params), pagination: JSON.stringify({ "rows": 500, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                        $("#fk_PurchaseOrderId").mkselectRefresh({
                            value: "ID",
                            text: "Abstract",
                            title: "Abstract",
                            data: data.rows
                        })
                    });
                }
            })
            //  $('#Base_WarehouseId').mkDataSourceSelect({ code: 'Warehouse', value: 'id', text: 'title' });


            //$('#OperatorId')[0].mkvalue = Changjie.clientdata.get(['userinfo']).userId;
            //$('#OperatorId').val(Changjie.clientdata.get(['userinfo']).realName);

            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(Changjie.clientdata.get(['userinfo']).userId);


            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });


            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Purchase_InStorageDetails').jfGrid({
                headData: [
                    {
                        label: '材料编码', name: 'Code', width: 140, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '入库单明细'
                    },
                    {
                        label: '材料名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '入库单明细'
                    },
                    {
                        label: '规格型号', name: 'Model', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '入库单明细'
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '入库单明细'
                    },

                    //{
                    //    label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'string', tabname: '入库单明细'
                    //},

                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },
                    {
                        label: '税率(如13%输13)', name: 'TaxRate', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '入库单明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "Purchase_InStorageDetails";
                                //row.TaxRate = row.InvoiceType;
                                //修改//
                                //setCellValue("TaxRate", row, index, gridName, row.InvoiceType);

                                //console.log(row.SettlementPrice, Changjie.sysGlobalSettings.pointGlobal )

                                var TaxRates = 1 + Number(row.TaxRate) / 100;
                                row.Price = (row.ContainTaxPrice / TaxRates).toFixed(4);
                                if (!isNaN(row.Price))
                                    setCellValue("Price", row, index, gridName, row.Price);

                                row.TotalPrice = (row.Price * row.Quantity).toFixed(4);
                                if (!isNaN(row.TotalPrice))
                                    setCellValue("TotalPrice", row, index, gridName, row.TotalPrice);


                                calcinmoney();
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },

                    {
                        label: '入库单价(含税)', name: 'ContainTaxPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '入库单明细',
                        inlineOperation: { to: [{ "t1": "ContainTaxPrice", "t2": "Quantity", "to": "ContainTaxTotalPrice", "type": "mul", "toTargets": null }], isFirst: true },
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "Purchase_InStorageDetails";
                                //row.TaxRate = row.InvoiceType;
                                //setCellValue("TaxRate", row, index, gridName, row.InvoiceType);

                                //console.log(row.SettlementPrice, Changjie.sysGlobalSettings.pointGlobal )

                                var TaxRates = 1 + Number(row.TaxRate) / 100;
                                row.Price = (row.ContainTaxPrice / TaxRates).toFixed(4);
                                if (!isNaN(row.Price))
                                    setCellValue("Price", row, index, gridName, row.Price);

                                row.TotalPrice = (row.Price * row.Quantity).toFixed(4);
                                if (!isNaN(row.TotalPrice))
                                    setCellValue("TotalPrice", row, index, gridName, row.TotalPrice);

                                calcinmoney();
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '入库数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '入库单明细',
                        //inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false },
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "Purchase_InStorageDetails";
                                //row.TaxRate = row.InvoiceType;
                                //setCellValue("TaxRate", row, index, gridName, row.InvoiceType);

                                //console.log(row.SettlementPrice, Changjie.sysGlobalSettings.pointGlobal )

                                //var TaxRates = 1 + Number(row.TaxRate) / 100;
                                //row.Price = (row.NoContainTaxPrice / TaxRates).toFixed(4);
                                //if (!isNaN(row.Price))
                                //    setCellValue("Price", row, index, gridName, row.Price);

                                row.TotalPrice = (row.Price * row.Quantity).toFixed(4);
                                if (!isNaN(row.TotalPrice))
                                    setCellValue("TotalPrice", row, index, gridName, row.TotalPrice);

                                row.ContainTaxTotalPrice = (row.ContainTaxPrice * row.Quantity).toFixed(4);
                                if (!isNaN(row.ContainTaxTotalPrice))
                                    setCellValue("ContainTaxTotalPrice", row, index, gridName, row.ContainTaxTotalPrice);

                                calcinmoney();
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '入库总价(含税)', name: 'ContainTaxTotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '入库单明细'
                    },

                    {
                        label: '入库单价(不含税)', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '入库单明细'

                    }, {
                        label: '入库总价(不含税)', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: '', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '入库单明细'

                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '入库单明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    }
                ],
                mainId: "ID",
                bindTable: "Purchase_InStorageDetails",
                isEdit: true,
                toolbarposition: "top",
                showadd: false,
                showchoose: true,
                onChooseEvent: function () {
                    var purchaseOrder = $('#fk_PurchaseOrderId').mkselectGetEx();
                    if (purchaseOrder == null) {
                        top.Changjie.alert.info("请选择采购订单");
                        return
                    }
                    Changjie.layerForm({
                        id: "selectHeTong",
                        width: 960,
                        height: 500,
                        title: "选择材料明细(只支持材料库材料)",
                        url: top.$.rootUrl + "/PurchaseModule/PurchaseInStorage/DetailDialog?orderId=" + purchaseOrder.ID,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    var total = 0;

                                    var cvv = $('#Purchase_InStorageDetails').jfGridGet("rowdatas");

                                    for (var i = 0; i < data.length; i++) {
                                        var ishas = false;
                                        $.each(cvv, function (ii, ee) {
                                            //console.log(ee.PaymentApplyId)
                                            //console.log(data[i].ApplyId)

                                            if (ee.Base_MaterialsId == data[i].Fk_BaseMaterialsId || ee.Base_MaterialsId == data[i].MaterialsId) {
                                                ishas = true;
                                                return false;
                                            }
                                        })
                                        if (ishas) {
                                            continue;
                                        }
                                        var row = {};
                                        row.Purchase_OrderDetailsId = data[i].ID;
                                        if (data[i].MaterialsId)
                                            row.Base_MaterialsId = data[i].MaterialsId;
                                        else
                                            row.Base_MaterialsId = data[i].Fk_BaseMaterialsId;

                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Model = data[i].ModelNumber;
                                        row.Brand = data[i].Brand;
                                        row.Quantity = data[i].Quantity;
                                        row.ContainTaxPrice = data[i].Price;
                                        row.ContainTaxTotalPrice = data[i].TotalPrice;
                                        row.TaxRate = data[i].TaxRage;
                                        row.Unit = data[i].Unit;

                                        var TaxRates = 1 + Number(row.TaxRate) / 100;
                                        row.Price = (row.ContainTaxPrice / TaxRates)
                                        row.TotalPrice = (row.Price * row.Quantity);
                                        //row.Price = row.Price.toFixed(4);
                                        //console.log(TaxRates,row.Price , row.Quantity)
                                        row.Purchase_ContractId = data[i].PurchaseContractId;
                                        row.Purchase_OrderId = data[i].PurchaseOrderId;

                                        row.Base_SupplierId = data[i].SupplierId;
                                        row.ProjectId = data[i].ProjectID;
                                        total += parseFloat(row.TotalPrice);
                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;

                                        rows.push(row);
                                    }

                                    $("#Purchase_InStorageDetails").jfGridSet("addRows", rows);


                                    calcinmoney()
                                }
                            });
                        }
                    });
                },
                height: 300,
                onMinusRow: function (row, rows, headData) {
                    calcinmoney()
                }
            });
        },

        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/GetformInfoListConnectionMaterials?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        var strEntity = $("#Purchase_InStorageDetails").jfGridGet('rowdatas');
        var istaxRate = true;
        console.log(strEntity);

        for (var i = 0; i < strEntity.length; i++) {
            var taxRate = strEntity[i].TaxRate;
            if (taxRate <= 0) {
                istaxRate = false;
                break;
            }
        }

        if (!istaxRate) {
            top.Changjie.layerConfirm('是否税率为0！', function (res) {
                if (res) {
                    $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                        // 保存成功后才回调
                        if (!!callBack) {
                            callBack();
                        }
                    });
                }
            });
        } else {
            $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                // 保存成功后才回调
                if (!!callBack) {
                    callBack();
                }
            });
        }
    };
    page.init();
}
var calcinmoney = function () {
    var rows = $('#Purchase_InStorageDetails').jfGridGet("rowdatas");

    var total = 0.0;
    console.log(typeof total, rows)
    $.each(rows, function (ii, ee) {

        if (ee.TotalPrice && !isNaN(ee.TotalPrice)) {
            total += parseFloat(ee.TotalPrice);
        }
    })
    //console.log(typeof total)
    $("#InMoney").val(total.toFixed(4))
}
var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;
    //row.EditType = 2;
    $cell.html(value);
    $edit.val(value);
};
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Purchase_InStorage"]').mkGetFormData());
    postData.strpurchase_InStorageDetailsList = JSON.stringify($('#Purchase_InStorageDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
