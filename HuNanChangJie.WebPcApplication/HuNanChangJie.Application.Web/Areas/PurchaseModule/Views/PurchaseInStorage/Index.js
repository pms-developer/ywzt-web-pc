﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-10-19 14:19
 * 描  述：采购入库
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 950);
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
            $('#Base_WarehouseId').mkDataSourceSelect({ code: 'Warehouse', value: 'id', text: 'title' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增采购入库',
                    url: top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/Form?projectId=' + projectId,
                    width: 800,
                    height: 660,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 打印
            //$('#print').on('click', function () {

            // $('#gridtable').jqprintTable();
            //});
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/GetPageList',
                headData: [
                    { label: "入库单号", name: "Code", width: 120, align: "left" },
                    {
                        label: "项目", name: "ProjectID", width: 220, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },
                    {
                        label: "仓库", name: "Base_WarehouseId", width: 180, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'Warehouse',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    //{ label: "入库时间", name: "InTime", width: 130, align: "left"},
                    { label: "入库期间", name: "Period", width: 130, align: "left" },
                    { label: "入库金额", name: "InMoney", width: 120, align: "left" },
                    {
                        label: "经办人", name: "OperatorId", width: 90, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "备注", name: "Remark", width: 300, align: "left" },
                ],
                mainId: 'ID',
                isPage: true
            });
            page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '采购入库',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 660,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
