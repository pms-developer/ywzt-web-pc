﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-15 17:08
 * 描  述：采购订单变更
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Purchase_OrderChange";
var processCommitUrl = top.$.rootUrl + '/PurchaseModule/PurchaseOrderChange/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Purchase_OrderChangeDetails', "gridId": 'Purchase_OrderChangeDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'PurchaseOrderChangeCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#SupplierId').mkDataSourceSelect({ code: 'SupplierList', value: 'id', text: 'name' });
            $("#SupplierId").on("change", function () {
                if ($("#Purchase_OrderChangeDetails").jfGridGet("rowdatas").length > 0) {
                    Changjie.layerConfirm("切换项目后，采购明细将清空。是否继续?", function (flag) {
                        if (flag) {
                            $("#Purchase_OrderChangeDetails").jfGridSet("clearallrow");
                        }
                    });
                }

                var supId = $(this).mkselectGet();
                if (supId) {
                    $("#PurchaseOrderId").removeAttr("readonly");
                    var orderDate = Changjie.httpGet(top.$.rootUrl + "/PurchaseModule/PurchaseOrder/GetOrderList?supplierId=" + supId).data;
                    $('#PurchaseOrderId').mkselect({
                        value: "ID",
                        text: "Name",
                        data: orderDate
                    });
                }
                else {
                    $("#PurchaseOrderId").attr("readonly", "readonly");
                    $("#PurchaseOrderId").mkselectSet("");

                }

            });
            $('#PurchaseOrderId').mkDataSourceSelect({ code: 'PurchaseOrderList', value: 'id', text: 'name' });
            $('#PurchaseOrderId').on("change", function () {

                if ($("#Purchase_OrderChangeDetails").jfGridGet("rowdatas").length > 0) {
                    Changjie.layerConfirm("切换项目后，采购明细将清空。是否继续?", function (flag) {
                        if (flag) {
                            $("#Purchase_OrderChangeDetails").jfGridSet("clearallrow");
                        }
                    });
                }
            });
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            //$("#addMaterials").on("click", function () {

            //});

            //$("#deleteMaterials").on("click", function () {
            //    var rowIndex = $("#Purchase_OrderChangeDetails").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Purchase_OrderChangeDetails").jfGridSet("removeRow");
            //    }
            //});
            $('#Purchase_OrderChangeDetails').jfGrid({
                headData: [
                    { label: "项目名称", name: "ProjectName", width: 150, align: "left" },
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                    //{ label: "单位", name: "Unit", width: 100, align: "left" },

                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    { label: "预算单价", name: "BudgetPrice", width: 100, align: "left" },
                    {
                        label: '订单', name: 'l484d6a7f3f344224a1c1c8b046d84541', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [{
                            label: '数量', name: 'OrderQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '变更明细'
                        },
                        {
                            label: '单价', name: 'OrderPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '变更明细'
                        },
                        {
                            label: '待入库数量', name: 'WaitInStorage', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '变更明细'
                        }
                        ]
                    },
                    {
                        label: '变更', name: 'l484d6a7f3f344224a1c1c8b046d84546', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [{
                            label: '数量', name: 'BeforeQuantity', width: 100, headColor: 'blue', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '变更明细'
                            , edit: {
                                type: 'input',
                                change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    var value = parseFloat(Changjie.clearNumSeparator(row["OrderQuantity"] || 0)) - parseFloat(Changjie.clearNumSeparator(row["BeforeQuantity"] || 0));
                                    setCellValue("AfterQuantity", row, index, "Purchase_OrderChangeDetails", value);
                                },
                                blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                }
                            }
                        },
                        {
                            label: '单价', name: 'BeforPrice', width: 100, headColor: 'blue', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '变更明细'
                            , edit: {
                                type: 'input',
                                change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    setCellValue("AfterPrice", row, index, "Purchase_OrderChangeDetails", row["BeforPrice"]);
                                },
                                blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                }
                            }
                        },
                        ]
                    },
                    {
                        label: '变更后', name: 'ld5c2f0cdcc574bf9ad55c22bd98f910e', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [{
                            label: '数量', name: 'AfterQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '变更明细'
                            , edit: {
                                type: 'input',
                                change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                },
                                blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                }
                            }
                        },
                        {
                            label: '单价', name: 'AfterPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '变更明细'
                            , edit: {
                                type: 'input',
                                change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                },
                                blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                }
                            }
                        },
                        ]
                    }
                ],
                mainId: "ID",
                bindTable: "Purchase_OrderChangeDetails",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    var orderId = $('#PurchaseOrderId').mkselectGet();
                    if (!!orderId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择采购清单",
                            url: top.$.rootUrl + "/PurchaseModule/PurchaseOrder/DetailDialog?orderId=" + orderId + "&projectId=" + projectId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.PurchaseOrderDetailsId = data[i].ID;
                                            row.ProjectConstructionBudgetMaterialsId = data[i].ProjectConstructionBudgetMaterialsId;
                                            row.ProjectName = data[i].ProjectName;
                                            $("#ProjectID").val(data[i].ProjectID);
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].Brand;
                                            row.ModelNumber = data[i].ModelNumber;
                                            row.Unit = data[i].Unit;
                                            row.BudgetPrice = data[i].BudgetPrice;

                                            row.OrderPrice = data[i].Price;
                                            row.OrderQuantity = data[i].Quantity;

                                            row.AfterPrice = data[i].Price;
                                            row.BeforPrice = data[i].Price;

                                            var result = parseFloat(Changjie.clearNumSeparator(data[i].Quantity)) - parseFloat(Changjie.clearNumSeparator(data[i].InStorage));
                                            row.WaitInStorage = result;

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }
                                        $("#Purchase_OrderChangeDetails").jfGridSet("addRows", rows);

                                        var freight = parseFloat($("#Freight").val() || 0);
                                        var amount = parseFloat($("#Purchase_OrderChangeDetails").jfGridGet("footerdata")["TotalPrice"] || 0);
                                        $("#OrderAmount").val(freight + amount);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择采购订单");
                    }
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/PurchaseOrderChange/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    var setCellValue = function (cellName, row, index, gridName, value) {
        var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
        var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
        row[cellName] = value;
        if (row.EditType != 1)
            row.EditType = 2;
        $cell.html(value);
        $edit.val(value);
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/PurchaseOrderChange/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Purchase_OrderChange"]').mkGetFormData());
    postData.strpurchase_OrderChangeDetailsList = JSON.stringify($('#Purchase_OrderChangeDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
