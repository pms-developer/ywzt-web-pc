﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-17 12:04
 * 描  述：采购合同增补
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Purchase_ContractSupplement";
var processCommitUrl = top.$.rootUrl + '/PurchaseModule/PurchaseContractSupplement/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var contractDetails = null;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Purchase_ContractSupplementDetails', "gridId": 'Purchase_ContractSupplementDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            //$("#addMaterials").on("click", function () {

            //});

            //$("#deleteMaterials").on("click", function () {
            //    var rowIndex = $("#Purchase_ContractSupplementDetails").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Purchase_ContractSupplementDetails").jfGridSet("removeRow");
            //    }
            //});
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'PurchaseContractSupplementCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $("#ContractName").on("click", function () {
                Changjie.layerForm({
                    id: "selectContract",
                    title: "选择合同",
                    url: top.$.rootUrl + '/PurchaseModule/PurchaseContract/Dialog',
                    width: 800,
                    hegiht: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                page.bindContractInfo(data[0]);
                            }
                        });
                    }
                });
            });
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Purchase_ContractSupplementDetails').jfGrid({
                headData: [
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                    //{ label: "单位", name: "Unit", width: 100, align: "left" },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    { label: "预算单价", name: "BudgetPrice", width: 100, align: "left" },
                    { label: "合同单价", name: "ContractPrice", width: 100, align: "left" },
                    { label: "原合同数量", name: "ContractQuantity", width: 100, align: "left" },
                    {
                        label: '增补数量', name: 'SupplementQuantity', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购明细', inlineOperation: { to: [{ "t1": "ContractPrice", "t2": "SupplementQuantity", "to": "SupplementPrice", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                setFooterValue(rows, headData);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '增补金额', name: 'SupplementPrice', statistics: true, width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'SupplementAmount', required: '0',
                        datatype: 'float', tabname: '采购明细'
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, headColor: "blue", cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Purchase_ContractSupplementDetails",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    var projectId = $('#ProjectID').val();
                    if (!!projectId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择采购任务",
                            url: top.$.rootUrl + "/PurchaseModule/BuyingTask/Dialog?projectId=" + projectId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectPurchaseApplyId = data[i].projectpurchaseapplyid;
                                            row.ProjectPurchaseApplyDetailsId = data[i].projectpurchaseapplydetailsid;
                                            row.ProjectConstructionBudgetMaterialsId = data[i].projectconstructionbudgetmaterialsid;
                                            row.ProjectId = projectId;

                                            row.BudgetPrice = data[i].budgetprice;

                                            row.Code = data[i].code;
                                            row.Name = data[i].name;
                                            row.Brand = data[i].brand;
                                            row.ModelNumber = data[i].modelnumber;
                                            row.Unit = data[i].unit;
                                            row.CurrentAllowQuantity = data[i].currentallowquantity;
                                            var isExist = false;
                                            var contractPrice = 0;
                                            var contractQuantity = 0;
                                            if (!!contractDetails) {
                                                for (var j = 0; j < contractDetails.rows.length; j++) {
                                                    var details = contractDetails.rows[j];
                                                    if (details.ProjectConstructionBudgetMaterialsId != row.ProjectConstructionBudgetMaterialsId) continue;
                                                    isExist = true;
                                                    contractPrice = details.ContractPrice;
                                                    contractQuantity = details.ContractQuantity;
                                                    break;
                                                }
                                            }
                                            if (isExist) {
                                                row.ContractPrice = contractPrice;
                                                row.cellEdit = true;
                                                row.ContractQuantity = contractQuantity;
                                            }
                                            else {
                                                row.ContractPrice = data[i].budgetPrice;
                                                row.ContractQuantity = 0;
                                            }

                                            var amount = parseFloat((row.ContractPrice || 0)) * parseFloat((row.ContractQuantity || 0));
                                            row.Amount = amount;

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }

                                        $("#Purchase_ContractSupplementDetails").jfGridSet("addRows", rows);

                                        var supplementAmount = parseFloat($("#Purchase_ContractSupplementDetails").jfGridGet("footerdata")["SupplementPrice"] || 0);
                                        $("#SupplementPrice").val(supplementAmount);

                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先合同项目");
                    }
                }
            });
        },
        bindContractInfo: function (data) {
            $("#ContractName").val(data.Name);
            $("#PurchaseContractId").val(data.ID);
            $("#F_FullName").val(data.F_FullName);
            $("#Company_ID").val(data.Jia);
            $("#SupplierName").val(data.SupplierName);
            $("#ProjectName").val(data.ProjName);
            $("#ProjectID").val(data.ProjectID);
            $("#ContractType").val(data.ContractTypeName);
            contractDetails = Changjie.httpGet(top.$.rootUrl + "/PurchaseModule/PurchaseContract/GetDetailList?contractId=" + data.ID).data;
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/PurchaseContractSupplement/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    var setFooterValue = function (rows, runparam) {
        var totalPrice = 0;
        for (var _index in rows) {
            var row = rows[_index];
            totalPrice += parseFloat(Changjie.clearNumSeparator((row["SupplementPrice"] || 0)));
        }
        runparam.op.running.statisticData["SupplementPrice"] = totalPrice;
        $("div.jfGird-statistic-cell[name='SupplementPrice']").html(Changjie.toDecimal(totalPrice));
    };

    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/PurchaseContractSupplement/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Purchase_ContractSupplement"]').mkGetFormData());
    postData.strpurchase_ContractSupplementDetailsList = JSON.stringify($('#Purchase_ContractSupplementDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
