﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-16 15:02
 * 描  述：其它采购合同
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Purchase_OtherContract";
var processCommitUrl = top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'PurchaseContractCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $("#IsBudget").on("change", function () {
                if ($("input[name='IsBudget']:checked").val() == "true") {
                    $("div[ProjectSubject]").show();
                    $("#ProjectSubjectId").attr("isvalid", "yes").attr("checkexpession", "NotNull");
                }
                else {
                    $("div[ProjectSubject]").hide();
                    $("#ProjectSubjectId").removeAttr("isvalid").removeAttr("checkexpession");
                }
            });
            $('#Jia').mkCompanySelect({});
            $('#Yi').mkDataSourceSelect({ code: 'SupplierList', value: 'id', text: 'name' });
            $('#ContractType').mkDataItemSelect({ code: 'PurchaseOtherContractType' });
            $('#IsContract').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
            $("#ProjectID").on("change", function () {
                var value = $(this).mkselectGet();
                if (value) {
                    var subjectData = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectSubject/GetSubjectList?projectId=" + value).data;

                    var op = {
                        value: "ID",
                        text: "FullName",
                        title: "FullName",
                        data: subjectData
                    };
                    $("#ProjectSubjectId").mkselectRefresh(op);
                }
                else {
                    $('#ProjectSubjectId').mkselectRefresh({ data: {} });
                    $('#ProjectSubjectId').mkselectSet("");
                }
            });
            $("#ProjectSubjectId").mkselect({});
            $('#IsBudget').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "newinstance",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);
            }
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                            if ($("input[name='IsBudget']:checked").val() == "true") {
                                $("div[ProjectSubject]").show();
                                $("#ProjectSubjectId").attr("isvalid", "yes").attr("checkexpession", "NotNull");
                            }
                            else {
                                $("div[ProjectSubject]").hide();
                                $("#ProjectSubjectId").removeAttr("isvalid").removeAttr("checkexpession");
                            }

                            if (data.IsContract) {
                                $("#IsContract").attr("checked", true);
                            }
                            else {
                                $("#IsContract").attr("checked", false);
                            }

                            if (data.IsBudget) {
                                $("#IsBudget").attr("checked", true);
                            }
                            else {
                                $("#IsBudget").attr("checked", false);
                            }


                            if ($("#YiDesc").val() != "") {
                                $("#addsupper").trigger("click")
                            }
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (checkBudget() == false) {
            Changjie.alert.warning("签定金额不能超过预算！");
            return false;
        }
        $("#ProjectName").val($("#ProjectID").mkselectGet());
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    var checkBudget = function () {
        if ($("input[name='IsBudget']:checked").val() == "true") {
            var subjectId = $("#ProjectSubjectId").mkselectGet();
            var amount = $("#SignAmount").val();
            var projectId = $("#ProjectID").mkselectGet();
            var param = { subjectId: subjectId, projectId: projectId, amount: amount };
            var data = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/CheckIsExceedBudget", param).data;
            if (data) { return false; }
            else { return true; }
        }
        else {
            return true;
        }

    };
    page.init();
}
var displaysupperadd = function (ee) {
    if ($(ee).hasClass("fa-plus")) {
        $("#YiDesc,#YiCode").show()
        $(ee).removeClass("fa-plus").addClass("fa-minus")
    }
    else {
        $("#YiDesc,#YiCode").hide()
        $(ee).removeClass("fa-minus").addClass("fa-plus")
    }
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }

    if (($('#Yi').mkselectGet() == null || $('#Yi').mkselectGet() == "") && ($('#YiDesc').val() == "" || $('#YiCode').val() == "")) {
        top.Changjie.alert.error("请选择供应商或者输入供应商名称和证件号码");
        return false;
    }

    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
};
