﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-16 15:02
 * 描  述：其它采购合同
 */
var projectId = request("projectId");
var formId = request("formId");
var refreshGirdData;
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增其它采购合同',
                    url: top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/GetPageList',
                headData: [
                    { label: "合同编号", name: "Code", width: 160, align: "left"},
                    { label: "合同名称", name: "Name", width: 200, align: "left"},
                    
                    { label: "甲方", name: "Jia", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('company', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "乙方", name: "Yi", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            if (row.YiDesc) {
                                callback(row.YiDesc);
                            }
                            else {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code: 'SupplierList',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['name']);
                                 }
                             });
                            }
                        }},
                    { label: "合同类型", name: "ContractType", width: 80, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'PurchaseOtherContractType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    }, {
                        label: "签订日期", name: "SignDate", width: 90, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "签证金额", name: "SignAmount", width: 100, align: "left" },
                    {
                        label: "项目", name: "ProjectID", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code:  'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "是否有合同", name: "IsContract", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'BESF',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                   
                    { label: "是否预算控制", name: "IsBudget", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'BESF',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    {
                        label: "费用科目", name: "ProjectSubjectId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'Base_CBKM',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['fullname']);
                                }
                            });
                        }},
                ],
                mainId:'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title +'其它采购合同',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/PurchaseModule/PurchaseOtherContract/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
