﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-24 10:56
 * 描  述：采购任务
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            if (!!projectId) {
                page.bind();
            }
        },
        bind: function () {
            $("#refresh").on("click", function () {
                refreshGirdData();
            });

            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.MaterialsName = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/PurchaseModule/BuyingTask/GetPageList',
                headData: [
                    { label: "材料编码", name: "code", width: 150, align: "left" },
                    { label: "项目名称", name: "projectname", width: 150, align: "left" },
                    { label: "客户名称", name: "customername", width: 150, align: "left" },
                    { label: "申请单号", name: "applycode", width: 150, align: "left" },
                    { label: "材料名称", name: "name", width: 150, align: "left" },
                    { label: "品牌", name: "brand", width: 100, align: "left" },
                    { label: "规格型号", name: "modelnumber", width: 100, align: "left" },
                    //{ label: "单位", name: "unit", width: 100, align: "left" },

                    {
                        label: '计量单位', name: 'unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    { label: "申请数量", name: "applyquantity", width: 100, align: "left" },
                    {
                        label: "需求日期", name: "needdate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "清单编号", name: "listcode", width: 150, align: "left" },
                    { label: "预算数量", name: "quantity", width: 100, align: "left" },
                    { label: "已采购数量", name: "purchasedquantity", width: 100, align: "left" },
                    { label: "待采购数量", name: "waitquantity", width: 100, align: "left" },
                    { label: "采购员", name: "f_realname", width: 100, align: "left" }
                ],
                mainId: 'id',
                isMultiselect:true,
                isPage: true,
                height: 362,
                rows: 300,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.ProjectID = projectId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
