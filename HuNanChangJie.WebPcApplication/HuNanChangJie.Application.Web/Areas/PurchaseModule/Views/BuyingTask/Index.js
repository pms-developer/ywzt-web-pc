﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-24 10:56
 * 描  述：采购任务
 */
var refreshGirdData;
var formId = request("formId");
var checkJson = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 400);

            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });

            $("#refresh").on("click", function () {
                refreshGirdData();
            });
            $("#generateOrder").on("click", function () {

            });
            $("#generateContract").on("click", function () {

            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/PurchaseModule/BuyingTask/GetPageList',
                headData: [
                    { label: "材料编码", name: "code", width: 150, align: "left" },
                    { label: "项目名称", name: "projectname", width: 150, align: "left" },
                    { label: "客户名称", name: "customername", width: 150, align: "left" },
                    { label: "申请单号", name: "applycode", width: 150, align: "left" },
                    { label: "材料名称", name: "name", width: 150, align: "left" },
                    { label: "品牌", name: "brand", width: 100, align: "left" },
                    { label: "规格型号", name: "modelnumber", width: 100, align: "left" },
                    {
                        label: "单位", name: "unit", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'MaterialsUnit',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        } },
                    { label: "申请数量", name: "applyquantity", width: 100, align: "left" },
                    {
                        label: "需求日期", name: "needdate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "清单编号", name: "listcode", width: 150, align: "left" },
                    { label: "预算数量", name: "quantity", width: 100, align: "left" },
                    { label: "已采购数量", name: "purchasedquantity", width: 100, align: "left" },
                    { label: "待采购数量", name: "waitquantity", width: 100, align: "left" },
                    { label: "采购员", name: "f_realname", width: 100, align: "left" }
                ],
                mainId:'id',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;

            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };


    page.init();
};
