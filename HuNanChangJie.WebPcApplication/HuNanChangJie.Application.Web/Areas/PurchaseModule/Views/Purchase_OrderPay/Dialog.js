﻿var acceptClick;
var supplier = request('supplier');
var projectId = request("projectId");
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
            page.search();
        },
        bind: function () {
            $('#gridtable').jfGrid({
                        url: top.$.rootUrl + '/PurchaseModule/PurchaseContract/GetPageList',
                        headData: [
                            { label: "合同编码", name: "Code", width: 160, align: "center" },
                            { label: "合同名称", name: "Name", width: 180, align: "left" },
                            { label: "项目名称", name: "ProjectName", width: 180, align: "left" },
                            { label: "合同金额", name: "SignAmount", width: 100, align: "left", },
                            { label: "结算金额", name: "SettlementAmount", width: 100, align: "left", },
                            { label: "已付金额", name: "AccountPaid", width: 100, align: "left" },
                            {
                                label: '累计罚款金额', name: 'FineExpend', width: 100
                            },
                            {
                                label: '累计支出金额', name: 'OtherExpend', width: 100
                            },
                            {
                                label: '累计收入金额', name: 'OtherIncome', width: 100
                            },
                            {
                                label: '实际付款比例(%)', name: 'Sjfkbl', width: 100
                            },
                            {
                                label: '约定付账比例(%)', name: 'YdfkBiLi', width: 100
                            },
                            {
                                label: '约定应付账款', name: 'YdyfKuan', width: 100
                            },
                            {
                                label: "甲方", name: "Jia", width: 100, align: "center",
                                formatterAsync: function (callback, value, row, op, $cell) {
                                    Changjie.clientdata.getAsync('company', {
                                        key: value,
                                        callback: function (_data) {
                                            callback(_data.name);
                                        }
                                    });
                                }},
                            {
                                label: "供应商", name: "Yi", width: 100, align: "center",
                                formatterAsync: function (callback, value, row, op, $cell) {
                                    Changjie.clientdata.getAsync('sourceData', {
                                        code:  'SupplierList',
                                        key: value,
                                        keyId: 'id',
                                        callback: function (_data) {
                                            callback(_data['name']);
                                        }
                                    });
                                }},
                            {
                                label: "签订时间", name: "SignDate", width: 100, align: "center",
                                formatter: function (cellvalue) {
                                    return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                                }
                            }
                        ],
                        mainId: 'ID',
                        isPage: true,
                        height: 362,
                        isMultiselect: true
                    });
        },
        search: function (param) {
            param = param || {};
            param.AuditStatus = 2;
            param.Yi = supplier;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};