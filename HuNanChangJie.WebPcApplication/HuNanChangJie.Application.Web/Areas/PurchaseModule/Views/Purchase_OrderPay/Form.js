﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-04-06 10:09
 * 描  述：采购订单付款申请
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Purchase_OrderPay";
var processCommitUrl = top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var gridverifystate = true;
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Purchase_OrderPayDetails', "gridId": 'Purchase_OrderPayDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $("#PayAmount").on("input propertychange", function () {
                $("#RatifyAmount").val($(this).val());
                var e = jQuery.Event("propertychange");
                $("#RatifyAmount").trigger(e);
            });
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0028' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });

            //var sql = " b.ProjectID='" + projectId + "' ";
            //Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'ProjectSupplierList', strWhere: sql }, function (res) {
            //    console.log(res)
            //})

            $('#BaesSupplierId').mkDataSourceSelect({ code: 'SupplierBankList', value: 'id', text: 'name' });

            $('#BaesSupplierId').on('change', function () {
                var data = $('#BaesSupplierId').mkselectGetEx();
                if (data) {
                    $("#AccountName").val(data.accountname);
                    $("#BankName").val(data.bankname);
                    $("#BankAccount").val(data.banknumber);

                } else {
                    $("#AccountName").val("");
                    $("#BankName").val("");
                    $("#BankAccount").val("");
                }
            });

            $('#ProjectMode').mkDataItemSelect({ code: 'ProjectMode' });
            $('#PayModel').mkDataItemSelect({ code: 'FKMS' });
            var loginInfo = Changjie.clientdata.get(['userinfo']);

            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            $('#OperatorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            }).mkselectSet(loginInfo.departmentId);

            $('#pinyin').mkDataItemSelect({ code: '' });
            $('#PayCompanyId').mkCompanySelect({});
            $('#HasInvoice').mkRadioCheckbox({
                type: 'radio',
                code: 'WZSF',
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Purchase_OrderPayDetails').jfGrid({
                headData: [{
                    label: '采购合同编号', name: 'PurchaseContractCode', width: 160, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '采购付款申请合同明细'
                },
                {
                    label: '采购合同名称', name: 'PurchaseContractName', width: 160, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '采购付款申请合同明细'
                },
                { label: "项目名称", name: "ProjectName", width: 180, align: "left" },
                {
                    label: '甲方', name: 'Jia', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '采购付款申请合同明细',
                    formatterAsync: function (callback, value, row, op, $cell) {
                        Changjie.clientdata.getAsync('company', {
                            key: value,
                            callback: function (_data) {
                                callback(_data.name);
                            }
                        });
                    }
                }, {
                    label: '供应商', name: 'Yi', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '采购付款申请合同明细',
                    formatterAsync: function (callback, value, row, op, $cell) {
                        Changjie.clientdata.getAsync('sourceData', {
                            code: 'SupplierList',
                            key: value,
                            keyId: 'id',
                            callback: function (_data) {
                                callback(_data['name']);
                            }
                        });
                    }
                },
                {
                    label: '合同金额', name: 'SignAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '采购付款申请合同明细'
                },
                {
                    label: '本次付款金额', name: 'PayAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'PayAmount', required: '1',
                    datatype: 'float', tabname: '采购付款申请合同明细'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {
                           // //console.log(row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, "aaaa")

                            //2-23 时代消防系统要求去除验证
                            //var signAmount = parseFloat(row["SignAmount"] || 0);
                            //var accountPaid = parseFloat(row["AccountPaid"] || 0);

                            //if (signAmount - accountPaid < row.PayAmount) {
                            //    top.Changjie.alert.warning("本次付款金额不能大于剩余待付金额");
                            //    $(obj).css("color", "red");
                            //    $(obj).attr("color", "red");
                            //    gridverifystate = false;
                            //} else {
                            //    $(obj).css("color", "black");
                            //    $(obj).attr("color", "black");
                            //    gridverifystate = true;
                            //}
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                { label: "已付金额", name: "AccountPaid", width: 100, align: "left" },
                { label: "结算金额", name: "SettlementAmount", width: 100, align: "left" },
                {
                    label: '累计罚款金额', name: 'FineExpend', width: 100
                },
                {
                    label: '累计支出金额', name: 'OtherExpend', width: 100
                },
                {
                    label: '累计收入金额', name: 'OtherIncome', width: 100
                },
                { label: "实际付款比例(%)", name: "Sjfkbl", width: 100, align: "left" },
                { label: "约定付账比例(%)", name: "YdfkBiLi", width: 100, align: "left" },
                { label: "约定应付账款", name: "YdyfKuan", width: 100, align: "left" },

                {
                    label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    datatype: 'string', tabname: '采购付款申请合同明细'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                ],
                mainId: "ID",
                bindTable: "Purchase_OrderPayDetails",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    var supplier = $("#BaesSupplierId").mkselectGet();
                    if (!supplier) {
                        top.Changjie.alert.warning("请选择供应商");
                        return false;
                    }

                    Changjie.layerForm({
                        id: "selectHeTong",
                        width: 900,
                        height: 400,
                        title: "选择采购合同",
                        url: top.$.rootUrl + "/PurchaseModule/Purchase_OrderPay/Dialog?supplier=" + supplier + "&projectId=" + projectId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    //console.log(data,"abc")
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.PurchaseContractCode = data[i].Code;
                                        row.PurchaseContractId = data[i].ID;
                                        row.PurchaseContractName = data[i].Name;
                                        row.ProjectName = data[i].ProjectName;

                                        $("#ProjectID").val(data[i].ProjectID);
                                        $("#ProjectMode").mkselectSet(data[i].ProjectMode);

                                        row.SettlementAmount = data[i].SettlementAmount;
                                        row.Sjfkbl = data[i].Sjfkbl;
                                        row.YdfkBiLi = data[i].YdfkBiLi;
                                        row.YdyfKuan = data[i].YdyfKuan;
                                        row.FineExpend = data[i].FineExpend;
                                        row.OtherExpend = data[i].OtherExpend;
                                        row.OtherIncome = data[i].OtherIncome;

                                        row.Jia = data[i].Jia;
                                        row.Yi = data[i].Yi;
                                        row.SignAmount = data[i].SignAmount;
                                        row.AccountPaid = data[i].AccountPaid;
                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        rows.push(row);
                                    }
                                    $("#Purchase_OrderPayDetails").jfGridSet("addRows", rows);
                                }
                            });
                        }
                    });
                },
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#AuditStatus').mkDataItemSelect({ code: 'AuditStatus' });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };

    // 保存数据
    acceptClick = function (callBack) {
        if (gridverifystate == false) {
            top.Changjie.alert.warning("数据验证失败。");
            return false;
        }

        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}

var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName);
    row[cellName] = value;
    $cell.html(value);
    $edit.val(value);
};
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var ratifyAmount = top.Changjie.clearNumSeparator($("#RatifyAmount").val(), "float");
    if (ratifyAmount <= 0) {
        top.Changjie.alert.warning("批准金额必须大于0");
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Purchase_OrderPay"]').mkGetFormData());
    postData.strpurchase_OrderPayDetailsList = JSON.stringify($('#Purchase_OrderPayDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
