﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-04-06 10:09
 * 描  述：采购订单付款申请
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                
                page.search(queryJson);
            }, 250, 800);
            $('#BaesSupplierId').mkDataSourceSelect({ code: 'SupplierList',value: 'id',text: 'name' });
            // 刷新

            $('#PayModel').mkDataItemSelect({ code: 'FKMS' });
            $('#PayCompanyId').mkCompanySelect({});
            $('#AuditStatus').mkselect({
                type: 'default',
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ]
            }) ;

            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增采购订单付款申请',
                    url: top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/GetPageList',
                headData: [
                    { label: "编码", name: "Code", width: 170, align: "center"},
                    {
                        label: "供应商", name: "SupplierName", width: 200, align: "left",
                        },
                    {
                        label: "经办人", name: "OperatorName", width: 100, align: "left",
                        },
                    {
                        label: "经办部门", name: "OperatorDepartmentName", width: 120, align: "left",
                        },
                    {
                        label: "申请日期", name: "ApplyDate", width: 80, align: "center", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }

                    },
                    {
                        label: "计划付款日期", name: "PlanPayDate", width: 100, align: "center", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }},
                    { label: "付款金额", name: "PayAmount", width: 100, align: "left"},
                    {
                        label: "付款单位", name: "PayCompanyName", width: 130, align: "left"
                    },
                    {
                        label: "是否有发票", name: "HasInvoice", width: 80, align: "center",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'WZSF',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    {
                        label: "付款模式", name: "PayModel", width: 100, align: "center",
                    },
                    {
                        label: "是否打印", name: "printcount", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            if (cellvalue > 0) {
                                return '<span class="label label-success" style="cursor: pointer;">已打印</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">未打印</span>';
                            }
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }, { label: "开户名称", name: "AccountName", width: 200, align: "left" },
                    { label: "开户银行", name: "BankName", width: 200, align: "left" },
                    { label: "银行账号", name: "BankAccount", width: 200, align: "left" },
                    { label: "说明", name: "Remark", width: 300, align: "left" },
                ],
                bindTable:"Purchase_OrderPay",
                mainId:'ID',
                isPage: true
            });

            page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '采购订单付款申请',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/PurchaseModule/Purchase_OrderPay/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&projectId=" + projectId + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
