﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-19 09:55
 * 描  述：现场收货单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Storage_LocaleMatesReceipt";
var processCommitUrl = top.$.rootUrl + '/PurchaseModule/StorageLocaleMatesReceipt/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var gridverifystate = true;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Storage_LocalematesReceiptDetails', "gridId": 'Storage_LocalematesReceiptDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {

            var loginInfo = Changjie.clientdata.get(["userinfo"]);

            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'StorageLocaleMatesReceiptCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });

            $("#addMaterials").on("click", function () {

            });

            $("#deleteMaterials").on("click", function () {
                var rowIndex = $("#Storage_LocalematesReceiptDetails").jfGridGet("rowIndex");
                if (rowIndex != -1) {
                    $("#Storage_LocalematesReceiptDetails").jfGridSet("removeRow");
                }
            });

            $('#ReceiptUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);


            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname', default: projectId });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Storage_LocalematesReceiptDetails').jfGrid({
                headData: [
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },


                    //{ label: "单位", name: "Unit", width: 100, align: "left" },

                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },


                    { label: "采购数量", name: "OrderQuantity", width: 100, align: "left" },
                    { label: "收货数量", name: "InStorage", width: 100, align: "left" },
                    {
                        label: '本次收货数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '现场收货单明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {
                                var quantity = parseFloat(Changjie.clearNumSeparator(row["Quantity"] || 0));
                                var waitInStorage = parseFloat(Changjie.clearNumSeparator(row["OrderQuantity"] || 0)) - parseFloat(Changjie.clearNumSeparator(row["InStorage"] || 0));
                                if (quantity > waitInStorage) {

                                    Changjie.alert.warning("【入库数量】不能超过【待入库数量】");
                                    $(obj).css("color", "red");
                                    $(obj).attr("color", "red");
                                    gridverifystate = false;
                                }
                                else {
                                    $(obj).css("color", "black");
                                    $(obj).attr("color", "black");
                                    gridverifystate = true;
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '现场收货单明细'
                        , edit: {
                            type: 'input'
                        }
                    },
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left" },
                    { label: "供应商名称", name: "SupplierName", width: 100, align: "left" },
                    { label: "订单编号", name: "OrderCode", width: 100, align: "left" }
                ],
                mainId: "ID",
                bindTable: "Storage_LocalematesReceiptDetails",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    var projectId = $('#ProjectID').mkselectGet();
                    if (!!projectId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择采购明细",
                            url: top.$.rootUrl + "/PurchaseModule/PurchaseOrder/ReceivingDialog?projectId=" + projectId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};

                                            row.PurchaseContractId = data[i].PurchaseContractId;
                                            row.PurchaseOrderId = data[i].PurchaseOrderId;
                                            row.PurchaseOrderDetailsID = data[i].ID;
                                            row.PurchaseContractDetailsId = data[i].PurchaseContractDetailsId;
                                            row.ProjectConstructionBudgetMaterialsId = data[i].ProjectConstructionBudgetMaterialsId;
                                            row.OrderQuantity = data[i].Quantity;
                                            row.InStorage = data[i].InStorage;
                                            row.Quantity = parseFloat(Changjie.clearNumSeparator(row.OrderQuantity || 0)) - parseFloat(Changjie.clearNumSeparator(row.InStorage || 0));
                                            row.OrderCode = data[i].OrderCode;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].Brand;
                                            row.ModelNumber = data[i].ModelNumber;
                                            row.Unit = data[i].Unit;
                                            row.Fk_BaseMaterialsId = data[i].Fk_BaseMaterialsId;
                                            row.ProjectID = projectId
                                            row.ProjectName = data[i].ProjectName;
                                            row.SupplierName = data[i].SupplierName;

                                            console.log(data[i]);
                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }
                                        $("#Storage_LocalematesReceiptDetails").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择项目");
                    }
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/StorageLocaleMatesReceipt/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!gridverifystate) return false;
        $("#ProjectName").val($("#ProjectID").mkselectGet());
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/StorageLocaleMatesReceipt/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Storage_LocaleMatesReceipt"]').mkGetFormData());
    postData.strstorage_LocalematesReceiptDetailsList = JSON.stringify($('#Storage_LocalematesReceiptDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
