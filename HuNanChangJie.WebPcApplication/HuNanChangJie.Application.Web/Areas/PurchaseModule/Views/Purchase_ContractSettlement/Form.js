﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-04-02 20:25
 * 描  述：采购合同结算
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="Purchase_ContractSettlement";
var processCommitUrl=top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'Purchase_ContractSettlementDetails',"gridId":'Purchase_ContractSettlementDetails'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0027' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#PurchaseContractId').mkDataSourceSelect({ code: 'PurchaseContractList', value: 'id', text: 'name' });
            $('#PurchaseContractId').on("change", function () {
                var contract = $('#PurchaseContractId').mkselectGetEx();
                console.log(contract);
                if (contract) {
                    $("#Yi").val(contract.yiname)
                    $("#a").val(contract.code)
                    $("#b").val(contract.signamount)
                    $("#ProjectID").val(contract.projectid)
                    $("#ProjectName").val(contract.projectname)
                }
                else {
                    $("#a").val("")
                    $("#b").val("")
                    $("#ProjectID").val("")
                    $("#ProjectName").val("")
                }
            });
            
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            $('#OperatorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            }).mkselectSet(loginInfo.departmentId);
            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"extend",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Purchase_ContractSettlementDetails').jfGrid({
                headData: [
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                    {
                        label: "单位", name: "Unit", width: 100, align: "left", formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'MaterialsUnit',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }

                    },
                    { label: "采购数量", name: "Quantity", width: 100, align: "left",datatype: 'float' },
                    { label: "采购单价", name: "Price", width: 100, align: "left", datatype: 'float'  },
                    { label: "采购金额合计", name: "TotalPrice", width: 120, align: "left", datatype: 'float'  },
                    {
                        label: '税率', name: 'TaxRate', width: 140, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '合同清单'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                                var gridName = "Purchase_ContractSettlementDetails";
                                //row.TaxRate = row.InvoiceType;
                                //setCellValue("TaxRate", row, index, gridName, row.InvoiceType);

                                //console.log(row.SettlementPrice, Changjie.sysGlobalSettings.pointGlobal )
                                //console.log(row.TaxRate,9999)
                                var TaxRates = 1 + Number(row.TaxRate);
                                row.SettlementPriceNoTax = (row.SettlementPrice / TaxRates).toFixed(4);
                                if (!isNaN(row.SettlementPriceNoTax))
                                    setCellValue("SettlementPriceNoTax", row, index, gridName, row.SettlementPriceNoTax);

                                row.SettlementAmountNoTax = (row.SettlementPriceNoTax * row.SettlementQuantity).toFixed(4);
                                if (!isNaN(row.SettlementAmountNoTax))
                                    setCellValue("SettlementAmountNoTax", row, index, gridName, row.SettlementAmountNoTax);
                            },
                            datatype: 'dataItem',
                            code: 'FPLX'
                        }
                    },
                    {
                        label: '结算单价(含税)', name: 'SettlementPrice', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'float', tabname:'合同清单' ,inlineOperation:{to:[{"t1":"SettlementPrice","t2":"SettlementQuantity","to":"SettlementAmount","type":"mul","toTargets":null}],isFirst:true}
                        ,edit:{
                            type:'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "Purchase_ContractSettlementDetails";
                                //row.TaxRate = row.InvoiceType;
                                //setCellValue("TaxRate", row, index, gridName, row.InvoiceType);

                                //console.log(row.SettlementPrice, Changjie.sysGlobalSettings.pointGlobal )
                                if (typeof row.TaxRate != "undifined") {
                                    var TaxRates = 1 + Number(row.TaxRate);
                                    row.SettlementPriceNoTax = (row.SettlementPrice / TaxRates).toFixed(4);
                                    if (!isNaN(row.SettlementPriceNoTax))
                                        setCellValue("SettlementPriceNoTax", row, index, gridName, row.SettlementPriceNoTax);

                                    row.SettlementAmountNoTax = (row.SettlementPriceNoTax * row.SettlementQuantity).toFixed(4);
                                    if (!isNaN(row.SettlementAmountNoTax))
                                        setCellValue("SettlementAmountNoTax", row, index, gridName, row.SettlementAmountNoTax);
                                }

                            },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    
                {
                        label: '结算数量', name: 'SettlementQuantity', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'float', tabname:'合同清单' ,inlineOperation:{to:[{"t1":"SettlementPrice","t2":"SettlementQuantity","to":"SettlementAmount","type":"mul","toTargets":null}],isFirst:false}
                        ,edit:{
                            type:'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                
                                if (row.SettlementQuantity > row.Quantity) {
                                    top.Changjie.alert.error("结算数量不能大于采购数量!");
                                    row.SettlementQuantity = row.Quantity
                                    $("#Purchase_ContractSettlementDetails").jfGridSetCellValue(index, "SettlementQuantity", row.Quantity)
                                }

                                var gridName = "Purchase_ContractSettlementDetails";
                                //row.TaxRate = row.InvoiceType;
                                //setCellValue("TaxRate", row, index, gridName, row.InvoiceType);

                                //console.log(row.SettlementPrice, Changjie.sysGlobalSettings.pointGlobal )
                                if (typeof row.TaxRate != "undifined") {
                                    var TaxRates = 1 + Number(row.TaxRate);
                                    row.SettlementPriceNoTax = (row.SettlementPrice / TaxRates).toFixed(4);
                                    if (!isNaN(row.SettlementPriceNoTax))
                                        setCellValue("SettlementPriceNoTax", row, index, gridName, row.SettlementPriceNoTax);

                                    row.SettlementAmountNoTax = (row.SettlementPriceNoTax * row.SettlementQuantity).toFixed(4);
                                    if (!isNaN(row.SettlementAmountNoTax))
                                        setCellValue("SettlementAmountNoTax", row, index, gridName, row.SettlementAmountNoTax);
                                }
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    { label: "结算金额(含税)", name: "SettlementAmount", width: 120, align: "left", aggtype: 'sum', aggcolumn: 'SettlementAmount' },
                    { label: "结算单价(不含税)", name: "SettlementPriceNoTax", width: 120, align: "left" },
                    { label: "结算金额(不含税)", name: "SettlementAmountNoTax", width: 120, align: "left" },
                {
                        label: '备注', name: 'Remark', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'合同清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    }
                ],
                mainId:"ID",
                bindTable:"Purchase_ContractSettlementDetails",
                isEdit: true,
                height: 300, toolbarposition: "top",
                showadd: false, showchoose: true, onChooseEvent: function () {
                    var contract = $('#PurchaseContractId').mkselectGet();
                    if (contract) {
                        Changjie.layerForm({
                            id: "selectHeTong",
                            width: 900,
                            height: 400,
                            title: "选择采购订单",
                            url: top.$.rootUrl + "/PurchaseModule/Purchase_ContractSettlement/Dialog?contractid=" + contract,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    console.log(data);
                                    if (!!data) {
                                        var orderid = data.ID;
                                        Changjie.httpGet(top.$.rootUrl + '/PurchaseModule/PurchaseOrder/GetOrderDetailListByOrderId', { orderId: orderid}, function (res) {
                                            $.each(res.data, function (iii, eee) {
                                                eee.rowState = 1;
                                                eee.ID = Changjie.newGuid();
                                                eee.EditType = 1;
                                                eee.SettlementPrice = eee.Price
                                                eee.SettlementQuantity = eee.Quantity
                                                
                                                eee.SettlementAmount = top.Changjie.format45(eee.Price * eee.Quantity, top.Changjie.sysGlobalSettings.pointGlobal);
                                            })

                                            if (res.code == 200)
                                                $("#Purchase_ContractSettlementDetails").jfGridSet("addRows", res.data);
                                            else
                                                $("#Purchase_ContractSettlementDetails").jfGridSet("clearallrow", res.data);
                                        });

                                        //for (var i = 0; i < data.length; i++) {
                                        //    var row = {};
                                        //    row.PurchaseContractCode = data[i].Code;
                                        //    row.PurchaseContractId = data[i].ID;
                                        //    row.PurchaseContractName = data[i].Name;
                                        //    row.Jia = data[i].Jia;
                                        //    row.Yi = data[i].Yi;
                                        //    row.SignAmount = data[i].SignAmount;

                                        //    row.rowState = 1;
                                        //    row.ID = Changjie.newGuid();
                                        //    row.EditType = 1;
                                        //    rows.push(row);
                                        //}

                                        //$("#Purchase_OrderPayDetails").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        top.Changjie.alert.info("请先选购采购合同!");
                    }
                },
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
            $('#AuditStatus').mkRadioCheckbox({
                type: 'checkbox',
                code: 'AuditStatus',
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;
    if (row.EditType != 1)
        row.EditType = 2;
    $cell.html(value);
    $edit.val(value);
};

var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
    }

    //$.each(res.data, function (iii, eee) {
    //    eee.rowState = 1;
    //    eee.ID = Changjie.newGuid();
    //    eee.EditType = 1;
    //})


    //var cvv = $('#Purchase_ContractSettlementDetails').jfGridGet("rowdatas");
    //$.each(cvv, function (ii, ee) {
    //    //console.log(ee.PaymentApplyId)
    //    //console.log(data[i].ApplyId)

    //    if (ee.PaymentApplyId == data[i].ApplyId) {
    //        ishas = true;
    //        return false;
    //    }
    //})

    //var msg = "选项卡[" + tabname + "],第" + rowNum + "行中的[" + datatypes[item].title + "]列的数据类型不正确,请输入数字。";
    //errorCells.push({ 'cellName': datatypes[item].title, "rowindex": rowNum, 'tabname': tabname, 'Msg': msg });

         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="Purchase_ContractSettlement"]').mkGetFormData());
        postData.strpurchase_ContractSettlementDetailsList = JSON.stringify($('#Purchase_ContractSettlementDetails').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
