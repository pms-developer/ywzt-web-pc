﻿var acceptClick;
var contractid = request('contractid');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            Changjie.httpGet(top.$.rootUrl + '/PurchaseModule/PurchaseOrder/GetPageList', { queryJson: JSON.stringify({ AuditStatus: 2, PurchaseContractId:contractid}), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (res) {
                if (res.code == 200) {
                    $('#gridtable').jfGrid({
                        rowdatas: res.data.rows,
                        headData: [
                            { label: "订单编码", name: "Code", width: 160, align: "center" },
                            { label: "订单名称", name: "Name", width: 200, align: "left" },
                            { label: "订单金额", name: "OrderAmount", width: 100, align: "left"},
                            {
                                label: "采购合同", name: "PurchaseContractId", width: 300, align: "center",
                                formatterAsync: function (callback, value, row, op, $cell) {
                                    Changjie.clientdata.getAsync('sourceData', {
                                        code:  'PurchaseContractList',
                                        key: value,
                                        keyId: 'id',
                                        callback: function (_data) {
                                            callback(_data['name']);
                                        }
                                    });
                                }},
                            {
                                label: "订单时间", name: "OrderDate", width: 100, align: "center",
                                formatter: function (cellvalue) {
                                    return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                                }
                            }
                        ],
                        mainId: 'ID',
                        isPage: true,
                        height: 362,
                    });
                }
            }); 
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        console.log(formdata);

        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};