﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-04-02 20:25
 * 描  述：采购合同结算
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            //$("#AuditStatus").mkselect({
            //    placeholder: '流程状态',
            //    text: 'name',
            //    value: 'id',
            //    data: [{ id: '', name: '全部' }, { id: '0', name: '审核中' }, { id: '1', name: '已通过' }, { id: '2', name: '未通过' }, { id: '3', name: '未审核' }]
            //})
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 800);

            $('#AuditStatus').mkselect({
                type: 'default',
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ]
            });

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增采购合同结算',
                    url: top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/GetPageList',
                headData: [
                    { label: "结算单号", name: "Code", width: 160, align: "left" },
                    {
                        label: "项目", name: "ProName", width: 200, align: "left",
                        
                    },
                    {
                        label: "采购合同", name: "PurchaseContractName", width: 220, align: "left",
                        },
                    {
                        label: "经办人", name: "OperatorName", width: 70, align: "center",
                        },
                    //{
                    //    label: "经办部门", name: "OperatorDepartmentId", width: 100, align: "center",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('department', {
                    //             key: value,
                    //             callback: function (_data) {
                    //                 callback(_data.name);
                    //             }
                    //         });
                    //    }},
                    {
                        label: "结算日期", name: "SettlementDate", width: 100, align: "center", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }},
                   
                    { label: "结算金额", name: "SettlementAmount", width: 100, align: "center"},
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                       formatter: function (cellvalue, row) {
                           return top.Changjie.tranAuditStatus(cellvalue);
                    }
                    }, { label: "结算说明", name: "Remark", width: 300, align: "left" },
                ],
                mainId:'ID',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            
            if (projectId) {
                param.ProjectID = projectId;
            }
          
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '采购合同结算',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/PurchaseModule/Purchase_ContractSettlement/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
