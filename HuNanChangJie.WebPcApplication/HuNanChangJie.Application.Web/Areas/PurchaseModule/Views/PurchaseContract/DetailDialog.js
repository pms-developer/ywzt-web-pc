﻿var refreshGirdData;
var formId = request("formId");
var contractId = request("contractId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            if (!!contractId) {
                page.bind();
            }
        },
        bind: function () {
            $("#refresh").on("click", function () {
                refreshGirdData();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/PurchaseModule/PurchaseContract/GetDetailList?contractId=' + contractId,
                headData: [
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                    {
                        label: "单位", name: "Unit", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                        Changjie.clientdata.getAsync('sourceData', {
                            code: 'MaterialsUnit',
                            key: value,
                            keyId: 'id',
                            callback: function (_data) {
                                callback(_data['name']);
                            }
                        });
                        }
                    },
                    { label: "合同单价", name: "ContractPrice", width: 100, align: "left" },
                    { label: "合同数量", name: "ContractQuantity", width: 100, align: "left" },
                    { label: "预算单价", name: "BudgetPrice", width: 100, align: "left" },
                    { label: "累计购货数", name: "TotalOrderQuantity", width: 100, align: "left" }
                ],
                mainId: 'id',
                isMultiselect: true,
                isPage: true,
                height: 362,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
