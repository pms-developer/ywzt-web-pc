﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-07 10:21
 * 描  述：采购核销
 */
var acceptClick;
var keyValue = request('purchaseContractid');
var finance_OtherPaymentCode = request('finance_OtherPaymentCode');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "VerificationTable";
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {

            if (finance_OtherPaymentCode != "null") {
                mainId = keyValue;
                type = "edit";
            }

            $('.mk-form-wrap').mkscroll();

            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {

            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);
                var pj = $("#ProjectID").mkselectGetEx();
                if (pj) {
                    $("#ProjectName").val(pj.projectname);
                }
            }

            $("#ProjectID").on("change", function () {
                var pj = $("#ProjectID").mkselectGetEx();
                if (pj) {
                    $("#ProjectName").val(pj.projectname);
                }
                else {
                    $("#ProjectName").val("");
                }
                if ($("#VerificationTable").jfGridGet("rowdatas").length > 0) {
                    Changjie.layerConfirm("切换项目后，付款订单将清空。是否继续?", function (flag) {
                        if (flag) {
                            $("#VerificationTable").jfGridSet("clearallrow");
                        }
                    });
                }
            });

            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });

            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            $('#VerificationTable').jfGrid({
                headData: [
                    {
                        label: '付款单号', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '对方单位', name: 'CustomerId', width: 100, cellStyle: { 'text-align': 'left' },
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'FKDWLB',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '付款日期', name: 'PaymentDate', width: 100, cellStyle: { 'text-align': 'left' }, formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '款项类型', name: 'FundType', width: 100, cellStyle: { 'text-align': 'left' },
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FKKXLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '付款金额', name: 'Verification_Amount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                    },
                    {
                        label: "资金账户", name: "BankId", width: 180, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'ZJZH',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    {
                        label: "支出科目", name: "ProjectSubjectId", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'ZCKM',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['fullname']);
                                }
                            });
                        }
                    }
                    //, {
                    //    label: "经办人", name: "OperatorId", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('user', {
                    //            key: value,
                    //            callback: function (_data) {
                    //                callback(_data.name);
                    //            }
                    //        });
                    //    }
                    //}

                ],
                mainId: "ID",
                bindTable: "VerificationTable",
                isEdit: true,
                height: 300,
                toolbarposition: "top",
                rowdatas: [],
                showchoose: true,
                onChooseEvent: function () {
                    var bj = $("#ProjectID").mkselectGetEx();
                    if (bj == null) {
                        bj = "";
                    }

                    // if (bj) {
                    Changjie.layerForm({
                        id: "selectQuantities",
                        width: 800,
                        height: 400,
                        title: "选择其他付款",
                        url: top.$.rootUrl + "/CaiWu/Finance_OtherPayment/Dialog?projectId=" + bj.project_id,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.Finance_OtherPaymentID = data[i].ID;
                                        row.Code = data[i].Code;
                                        row.CustomerId = data[i].CustomerId;
                                        row.PaymentDate = data[i].PaymentDate;
                                        row.FundType = data[i].FundType;
                                        row.Verification_Amount = data[i].PaymentAmount;
                                        row.BankId = data[i].BankId;
                                        row.ProjectSubjectId = data[i].ProjectSubjectId;
                                        row.Purchase_ContractID = keyValue;
                                        // row.OperatorId = data[i].OperatorId;
                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        rows.push(row);
                                    }
                                    $("#VerificationTable").jfGridSet("addRows", rows);

                                    publicSetRate();
                                }
                            });
                        }
                    });
                    // } else {
                    //     Changjie.alert.warning("请您先选择项目");
                    //  }
                },

                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });

        },
        initData: function () {
            if (!!keyValue) {
              
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/PurchaseContract/GetformInfos?purchase_ContractID=' + keyValue, function (data) {

                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }

                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();

        if (postData == false) return false;

        //var tempprojectid = $("#ProjectID").mkselectGet();
        //if ($('input[name="BaoXiaoType"]:checked').val() == "项目费用" && !tempprojectid) {
        //    top.Changjie.alert.error("报销类型为项目费用,请选择项目名称");
        //    return false;
        //}

        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/PurchaseContract/VerificationSaveFormList?purchaseContractid=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}

var publicSetRate = function () {
    var rowsQuantities = $('#VerificationTable').jfGridGet('rowdatas');

    var sum_Verification_Amount = 0;

    for (var i = 0; i < rowsQuantities.length; i++) {
        if (!isNaN(rowsQuantities[i].Verification_Amount))
            sum_Verification_Amount += Number(rowsQuantities[i].Verification_Amount);
    }

    $("#Verification_Amount").val(parseFloat(sum_Verification_Amount).toFixed(top.Changjie.sysGlobalSettings.pointGlobal));
    $("#Ticket_Amount").val(parseFloat(sum_Verification_Amount).toFixed(top.Changjie.sysGlobalSettings.pointGlobal));
};



var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    //if (type == "add") {
    //    keyValue = mainId;
    //}
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Purchase_Contract"]').mkGetFormData());
    postData.strVerificationTableList = JSON.stringify($('#VerificationTable').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
