﻿var refreshGirdData;
var formId = request("formId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            //$("#refresh").on("click", function () {
            //    refreshGirdData();
            //});

            //$('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
            //    page.search(queryJson);
            //}, 60, 400);

            $("#btn_Search").on("click", function () {
                page.search();
            })
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("")
                page.search();
            })


            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/PurchaseModule/PurchaseContract/GetContractList',
                headData: [
                    { label: "合同编码", name: "Code", width: 100, align: "left" },
                    { label: "合同名称", name: "Name", width: 100, align: "left" },
                    {
                        label: "项目名称", name: "ProjName", width: 100, align: "left"
                    },
                    {
                        label: "签定日期", name: "SignDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "甲方", name: "F_FullName", width: 100, align: "left"
                    },
                    {
                        label: "乙方", name: "SupplierName", width: 100, align: "left"
                    },
                    {
                        label: "计价方式", name: "PriceType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ProjectType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "采购方式", name: "PurchaseType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ProjectType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "合同签定金额", name: "SignAmount", width: 100, align: "left" },
                    {
                        label: "发票类型", name: "InvoiceType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FPLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                ],
                mainId: 'ID',
                isPage: true,
                height: 362,
                isMultiselect: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            if ($("#SearchValue").val() != "") {
                param.word = $("#SearchValue").val()
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
