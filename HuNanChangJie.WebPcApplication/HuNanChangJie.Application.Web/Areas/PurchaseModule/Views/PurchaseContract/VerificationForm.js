﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-07 10:21
 * 描  述：采购核销
 */
var acceptClick;
var keyValue = request('purchaseContractid');
var finance_OtherPaymentID = request('finance_OtherPaymentID');
var mainId = "";
var type = "";
var subGrid = [];
var tables = [];
var mainTable = "VerificationTable";
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (finance_OtherPaymentID != "null") {
                mainId = keyValue;
                type = "edit";
            }

            $('.mk-form-wrap').mkscroll();

            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {


            $('#Finance_OtherPaymentID').on('change', function () {
                var data = $('#Finance_OtherPaymentID').mkselectGetEx();

                if (data) {
                    $("#Verification_Amount").val(data.paymentamount);
                    $("#Ticket_Amount").val(data.paymentamount);
                }
            });

            var sqlWhere = "";

            if (finance_OtherPaymentID) {

                sqlWhere += " (already_verification=0 or already_verification is null or code='" + finance_OtherPaymentID + "')";

            } else {
                sqlWhere = " (already_verification=0 or already_verification is null)";
            }

            if (projectId) {
                sqlWhere += " and projectid='" + projectId + "'";
            }

            $('#Finance_OtherPaymentID').mkDataSourceSelect({ code: 'Finance_OtherPayment', value: 'id', text: 'code', strWhere: sqlWhere });


            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });

            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/PurchaseContract/GetformInfo?purchase_ContractID=' + keyValue, function (data) {

                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }

                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();

        if (postData == false) return false;

        //var tempprojectid = $("#ProjectID").mkselectGet();
        //if ($('input[name="BaoXiaoType"]:checked').val() == "项目费用" && !tempprojectid) {
        //    top.Changjie.alert.error("报销类型为项目费用,请选择项目名称");
        //    return false;
        //}

        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/PurchaseContract/VerificationSaveForm?purchaseContractid=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="VerificationTable"]').mkGetFormData());
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
