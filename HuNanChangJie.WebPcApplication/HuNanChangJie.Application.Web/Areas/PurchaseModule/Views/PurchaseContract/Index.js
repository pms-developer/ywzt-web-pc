﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-11 19:12
 * 描  述：采购合同
 */
var refreshGirdData;
var formId = request("formId");
var auditPassEvent;
var unauditPassEvent;
var formId = request("formId");
var projectId = request("projectId");
var checkJson = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            //Changjie.clientdata.refresh('sourceData', {
            //    code: 'SupplierList'
            //});
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '4',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 800);
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
            $('#AuditStatus').mkselect({
                type: 'default',
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ]
            });
            $('#Yi').mkDataSourceSelect({ code: 'SupplierList', value: 'id', text: 'name' });

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增采购合同',
                    url: top.$.rootUrl + '/PurchaseModule/PurchaseContract/Form?projectId=' + projectId,
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

            // 核销
            $('#verification').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                var finance_OtherPaymentID = $('#gridtable').jfGridValue('Finance_OtherPaymentCode');
                
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '核销',
                        url: top.$.rootUrl + '/PurchaseModule/PurchaseContract/VerificationForm?finance_OtherPaymentID=' + finance_OtherPaymentID + '&projectId=' + projectId + '&purchaseContractid=' + keyValue,
                        // url: top.$.rootUrl + '/PurchaseModule/PurchaseContract/VerificationFormList?finance_OtherPaymentCode=' + finance_OtherPaymentID + '&projectId=' + projectId + '&purchaseContractid=' + keyValue,
                        width: 1000,
                        height: 500,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PurchaseModule/PurchaseContract/GetPageList',
                headData: [
                    { label: "外部合同编码", name: "ExternalCode", width: 150, align: "left" },
                    { label: "系统编码", name: "Code", width: 150, align: "left" },
                    { label: "合同名称", name: "Name", width: 300, align: "left" },
                    { label: "收票金额", name: "Ticket_Amount", width: 100, align: "left" },
                    { label: "合同签定金额", name: "SignAmount", width: 100, align: "left" },
                    { label: "已付款金额", name: "AccountPaid", width: 100, align: "left" },
                    { label: "应付款金额", name: "AccountPayable", width: 100, align: "left" }, 
                    {
                        label: "项目名称", name: "ProName", width: 200, align: "left"
                    },

                    {
                        label: "项目经理", name: "ProjectManagerName", width: 180, align: "left"
                    },
                    {
                        label: "项目模式", name: "ProjectMode", width: 70, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ProjectMode',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "甲方", name: "JiaCompanyName", width: 180, align: "left"
                    },
                    {
                        label: "乙方", name: "SupplierName", width: 180, align: "left"
                    },
                    {
                        label: "签定日期", name: "SignDate", width: 80, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },

                    {
                        label: "核销单号", name: "Finance_OtherPaymentCode", width: 200, align: "left"
                    },

                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    {
                        label: "计价方式", name: "PriceTypeName", width: 100, align: "left"
                    },
                    {
                        label: "采购方式", name: "PurchaseType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'PurchaseType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                   
                    {
                        label: "发票类型", name: "InvoiceType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FPLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "优惠金额", name: "DiscountsAmount", width: 100, align: "left" },
                    { label: "优惠合同金额", name: "DiscountsContractAmount", width: 100, align: "left" },

                ],
                mainId: 'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        Changjie.clientdata.refresh('custmerData', {
            code: 'SupplierList'
        });
        page.search(checkJson);
    };

    auditPassEvent = function (keyValue) {
        //判断数量
        Changjie.httpGet(top.$.rootUrl + '/PurchaseModule/PurchaseContract/GetIsExceed', { keyValue: keyValue }, function (data) {
            if (data.data.exceedNum > 0) {
                top.Changjie.layerConfirm('该单据内有材料超出申请数量，确认要审核通过吗！', function (res) {
                    if (res) {
                        Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseContract/Audit', { keyValue: keyValue }, function (data) {
                        });
                        $('#gridtable').jfGridSet("updateRow", { ID: keyValue });
                    }
                    else {
                        //改回审核状态
                        Changjie.httpAsync('POST', top.$.rootUrl + '/PurchaseModule/PurchaseContract/EditStatus', { keyValue: keyValue }, function (data) {
                        });
                        refreshGirdData();
                        $('#gridtable').jfGridSet("updateRow", { ID: keyValue });
                    }
                });
            }
            else
                Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseContract/Audit', { keyValue: keyValue }, function (data) {
                });
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/PurchaseModule/PurchaseContract/UnAudit', { keyValue: keyValue }, function (data) {
        });
    };

    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/PurchaseModule/PurchaseContract/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '采购合同',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/PurchaseModule/PurchaseContract/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId,
            width: 1000,
            height: 800,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
