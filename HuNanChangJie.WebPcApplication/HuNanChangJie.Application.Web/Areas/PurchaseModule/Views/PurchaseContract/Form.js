﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-11 19:12
 * 描  述：采购合同
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var count = 0;
var subGrid = [];
var tables = [];
var mainTable = "Purchase_Contract";
var processCommitUrl = top.$.rootUrl + '/PurchaseModule/PurchaseContract/SaveForm';
var viewState = request('viewState');
var projectId = request('projectId');

var ContractPrice = 0;
var gridverifystate = true;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Purchase_ContractDetails', "gridId": 'Purchase_ContractDetails' });
                subGrid.push({ "tableName": 'Project_PaymentAgreement', "gridId": 'Project_PaymentAgreement' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.bindEvent();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }

        },
        bind: function () {
            //$("#addMaterials").on("click", function () {

            //});

            //$("#deleteMaterials").on("click", function () {
            //    var rowIndex = $("#Purchase_ContractDetails").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Purchase_ContractDetails").jfGridSet("removeRow");
            //    }
            //});
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'PurchaseContractCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });

            $('#ProjectMode').mkDataItemSelect({ code: 'ProjectMode' });


            //Changjie.httpGet(top.$.rootUrl + '/ProjectModule/Project/GetPageList', { queryJson: JSON.stringify({ AuditStatus: 2 }), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "Code", "sord": "Desc", "records": 0, "total": 0 }) }, function (res) {
            //    console.log(res)
            //})

            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            })

            if (!keyValue) {

                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'BASE_XMLB', strWhere: " project_id ='" + projectId + "'" }, function (data) {
                    var marketingstaffid = "";
                    if (data.length > 0) {
                        marketingstaffid = data[0].marketingstaffid;
                    }
                    $('#ProjectManager').mkformselectSet(marketingstaffid);
                });
            }

            $('#Jia').mkCompanySelect({});
            $('#Yi').mkDataSourceSelect({ code: 'SupplierList', value: 'id', text: 'name' });
            $('#PriceType').mkRadioCheckbox({
                type: 'radio',
                code: 'PriceType',
            });
            $('#ContractType').mkselect({
                url: top.$.rootUrl + "/SystemModule/BaseContractType/GetListByProperty?property=PurchaseContract",
                value: 'ID',
                text: "Name",
                title: "Name"
            });
            $('#CopeType').mkRadioCheckbox({
                type: 'radio',
                code: 'CopeType',
            });
            $('#PurchaseType').mkDataItemSelect({ code: 'PurchaseType' });
            $('#InvoiceType').mkDataItemSelect({ code: 'FPLX' });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Purchase_ContractDetails').jfGrid({
                headData: [
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                    //{ label: "单位", name: "Unit", width: 100, align: "left" },

                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            }
                            //,readonly: true
                        }
                    },

                    { label: "预算单价", name: "BudgetPrice", width: 100, align: "left" },
                    { label: "待采购数量", name: "CurrentAllowQuantity", width: 100, align: "left" },
                    {
                        label: '合同单价(优惠后)', name: 'ContractPrice', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购明细', inlineOperation: { to: [{ "t1": "ContractPrice", "t2": "ContractQuantity", "to": "Amount", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {

                                setFooterValue(rows, headData);

                                //if (row.DataState != 1) {
                                //    var contractPrice = parseFloat(row["ContractPrice"] || 0);
                                //    var BudgetPrice = parseFloat(row["BudgetPrice"] || 0);
                                //    if (contractPrice > BudgetPrice) {
                                //        Changjie.alert.warning("【合同单价】不能超过【预算单价】");
                                //        $(obj).css("color", "red");
                                //        $(obj).attr("color", "red");
                                //        gridverifystate = false;
                                //    }
                                //    else {
                                //        $(obj).css("color", "black");
                                //        $(obj).attr("color", "black");
                                //        gridverifystate = true;
                                //    }
                                //}
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合同数量', name: 'ContractQuantity', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购明细', inlineOperation: { to: [{ "t1": "ContractPrice", "t2": "ContractQuantity", "to": "Amount", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {
                                setFooterValue(rows, headData);
                                var ContractQuantity = parseFloat(row["ContractQuantity"] || 0);
                                var CurrentAllowQuantity = parseFloat(row["CurrentAllowQuantity"]);
                                var DiscountsPrice = parseFloat(row["DiscountsPrice"] || 0);
                                var result = top.Changjie.simpleMath(ContractQuantity, DiscountsPrice, "mul");
                                row.AfterDiscountPrice = result;
                                setCellValue("AfterDiscountPrice", row, index, "Purchase_ContractDetails", result);
                                if (!!CurrentAllowQuantity && ContractQuantity > CurrentAllowQuantity) {

                                    Changjie.alert.warning("【合同数量】不能超过【待采购数量】");
                                    $(obj).css("color", "red");
                                    $(obj).attr("color", "red");
                                    gridverifystate = false;
                                }
                                else {
                                    $(obj).css("color", "black");
                                    $(obj).attr("color", "black");
                                    gridverifystate = true;
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合计金额', name: 'Amount', statistics: true, width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'SignAmount', required: '0',
                        datatype: 'float', tabname: '采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {

                                var rage = $("#TaxRage").val();
                                var price = $("#SignAmount").val() || 0 + $("#Freight").val();
                                // var result = (price * rage / 100).toFixed(Changjie.sysGlobalSettings.pointGlobal)
                                var rageconversion = rage / 100;

                                //（税额公式为: 金额 / (1 + 税率) * 税率）
                                var result = (price / (1 + rageconversion) * rageconversion).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                                $("#TaxAmount").val(result || "");

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    //{
                    //    label: '优惠后单价', name: 'DiscountsPrice', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'float', tabname: '采购明细', inlineOperation: { to: [{ "t1": "ContractQuantity", "t2": "DiscountsPrice", "to": "AfterDiscountPrice", "type": "mul", "toTargets": null }], isFirst: false }
                    //    , edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //            setFooterValue(rows, headData);

                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},
                    //{
                    //    label: '优惠后金额', statistics: true, name: 'AfterDiscountPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'DiscountsContractAmount', required: '0',
                    //    datatype: 'float', tabname: '采购明细'
                    //    , edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},
                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Purchase_ContractDetails",
                isEdit: true,
                height: 300,
                toolbarposition: "top",

                //showchoose: true,
                //onChooseEvent: function () {
                //    var projectId = $('#ProjectID').mkselectGet();
                //    if (!!projectId) {
                //        Changjie.layerForm({
                //            id: "selectHeTong",
                //            width: 900,
                //            height: 400,
                //            title: "选择采购申请",
                //            url: top.$.rootUrl + "/ProjectModule/PurchaseApply/Dialog?projectId=" + projectId,
                //            callBack: function (id) {
                //                return top[id].acceptClick(function (data) {
                //                    if (!!data) {
                //                        var applyid = data.ID;
                //                        Changjie.httpGet(top.$.rootUrl + '/ProjectModule/PurchaseApply/GetApplyDetailListByApplyId', { keyValue: applyid }, function (res) {
                //                            $.each(res.data, function (iii, eee) {
                //                                eee.rowState = 1;
                //                                eee.ID = Changjie.newGuid();
                //                                eee.EditType = 1;
                //                                eee.MaterialsSource = 1;
                //                            })

                //                            if (res.code == 200)
                //                                $("#Purchase_ContractDetails").jfGridSet("addRows", res.data);
                //                            else
                //                                $("#Purchase_ContractDetails").jfGridSet("clearallrow", res.data);
                //                        });

                //                        //for (var i = 0; i < data.length; i++) {
                //                        //    var row = {};
                //                        //    row.PurchaseContractCode = data[i].Code;
                //                        //    row.PurchaseContractId = data[i].ID;
                //                        //    row.PurchaseContractName = data[i].Name;
                //                        //    row.Jia = data[i].Jia;
                //                        //    row.Yi = data[i].Yi;
                //                        //    row.SignAmount = data[i].SignAmount;

                //                        //    row.rowState = 1;
                //                        //    row.ID = Changjie.newGuid();
                //                        //    row.EditType = 1;
                //                        //    rows.push(row);
                //                        //}

                //                        //$("#Purchase_OrderPayDetails").jfGridSet("addRows", rows);
                //                    }
                //                });
                //            }
                //        });
                //    }
                //    else {
                //        Changjie.alert.warning("请您先选择项目");
                //    }
                //},


                showchoose: true,
                onChooseEvent: function () {
                    var projectId = $('#ProjectID').mkselectGet();
                    if (!!projectId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择采购任务",
                            url: top.$.rootUrl + "/PurchaseModule/BuyingTask/Dialog?projectId=" + projectId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectPurchaseApplyId = data[i].projectpurchaseapplyid;
                                            row.ProjectPurchaseApplyDetailsId = data[i].projectpurchaseapplydetailsid;
                                            row.ProjectConstructionBudgetMaterialsId = data[i].projectconstructionbudgetmaterialsid;
                                            row.BudgetPrice = data[i].budgetprice;
                                            row.ProjectId = projectId;
                                            row.Code = data[i].code;
                                            row.Name = data[i].name;
                                            row.Brand = data[i].brand;
                                            row.ModelNumber = data[i].modelnumber;
                                            row.Unit = data[i].unit;
                                            row.CurrentAllowQuantity = data[i].currentallowquantity;
                                            row.ContractPrice = data[i].budgetprice;
                                            row.ContractQuantity = data[i].currentallowquantity;
                                            var amount = parseFloat((row.ContractPrice || 0)) * parseFloat((row.ContractQuantity || 0));
                                            row.Amount = amount;
                                            //优惠单价及优惠后金额默认取合同单价及金额
                                            row.DiscountsPrice = row.ContractPrice;
                                            row.AfterDiscountPrice = amount;
                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.MaterialsSource = 1;
                                            rows.push(row);
                                        }
                                        $("#Purchase_ContractDetails").jfGridSet("addRows", rows);

                                        var freight = parseFloat($("#Freight").val() || 0);
                                        var signAmount = parseFloat($("#Purchase_ContractDetails").jfGridGet("footerdata")["Amount"] || 0);
                                        $("#SignAmount").val(freight + signAmount);
                                        var e = jQuery.Event("propertychange");
                                        $("#SignAmount").trigger(e);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择项目");
                    }
                },

                onAddRowEvent: function () {
                    var projectId = $('#ProjectID').mkselectGet();
                    if (!!projectId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 1100,
                            height: 1000,
                            title: "选择材料档案清单",
                            // url: top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/MaterialsListDialog?projectId=" + projectId,
                            url: top.$.rootUrl + "/SystemModule/BaseMaterials/MaterialsDialog?projectId=" + projectId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.BudgetPrice = data[i].Price;
                                            row.ProjectId = projectId;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].BrandId;
                                            row.ModelNumber = data[i].Model;
                                            row.Unit = data[i].UnitId;
                                            row.Fk_BaseMaterialsId = data[i].ID;
                                            row.MaterialsSource = 1;
                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.DataState = 1;
                                            rows.push(row);
                                        }
                                        $("#Purchase_ContractDetails").jfGridSet("addRows", rows);

                                        var freight = parseFloat($("#Freight").val() || 0);
                                        var signAmount = parseFloat($("#Purchase_ContractDetails").jfGridGet("footerdata")["Amount"] || 0);
                                        $("#SignAmount").val(freight + signAmount);
                                        var e = jQuery.Event("propertychange");
                                        $("#SignAmount").trigger(e);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择项目");
                    }
                }
            });
            $('#Project_PaymentAgreement').jfGrid({
                headData: [
                    {
                        label: '款项性质', name: 'FundType', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '付款比率', name: 'PaymentRate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '付款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var price = parseFloat(Changjie.clearNumSeparator($("#SignAmount").val() || 0));
                                var rate = parseFloat(Changjie.clearNumSeparator(row["PaymentRate"] || 0));
                                var result = price * rate / 100;
                                if (!!result == false) {
                                    result = "";
                                }
                                setCellValue("PaymentAmount", row, index, "Project_PaymentAgreement", result);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '付款金额', name: 'PaymentAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '付款协议'
                    },
                    {
                        label: '预计付款日期', name: 'PrepayPaymentDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款协议'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '付款责任人', name: 'OperatorId', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款协议'
                        , edit: {
                            type: 'layer',
                            change: function (data, rownum, selectData) {
                                data.OperatorId = selectData.f_realname;
                                //data.Name = selectData.f_realname;
                                $('#Project_PaymentAgreement').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 500,
                                height: 300,
                                listFieldInfo: [
                                    { label: "所在部门", name: "f_fullname", width: 100, align: "left" },
                                    { label: "人员姓名", name: "f_realname", width: 100, align: "left" },
                                ],
                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: { code: 'User' }
                            }
                        }
                    },
                    {
                        label: '排序', name: 'SortCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'int', tabname: '付款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_PaymentAgreement",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                }
            });
        },
        bindEvent: function () {
            $("#ProjectID").on("change", function () {
                debugger;
                var pj = $("#ProjectID").mkselectGetEx();
                if (pj) {
                    $("#ProjectName").val(pj.projectname);
                    $("#ProjectMode").mkselectSet(pj.mode);
                    $("#ProjectManager").mkformselectSet(pj.marketingstaffid);
                }
                else {
                    $("#ProjectName").val("");
                    $("#ProjectMode").mkselectSet("");
                    $("#ProjectManager").mkformselectSet("");
                }
                if ($("#Purchase_ContractDetails").jfGridGet("rowdatas").length > 0) {
                    Changjie.layerConfirm("切换项目后，采购明细将清空。是否继续?", function (flag) {
                        if (flag) {
                            $("#Purchase_ContractDetails").jfGridSet("clearallrow");
                        }
                    });
                }
            });
            if ($("input[name='PriceType']:checked").val() == "UnitPrice") {
                $("#SignAmount").attr("readonly", "readonly");
            }
            else {
                $("#SignAmount").removeAttr("readonly");
            }
            $("input[name='PriceType']").on("change", function () {
                var priceType = $(this).val();
                var $title = $("#contractprice");
                if (priceType == "UnitPrice") {
                    $title.html("合同签定金额<font face='宋体'>*</font>");
                    $("#SignAmount").val("");
                    $("#SignAmount").attr("readonly", "readonly");
                }
                else {
                    $title.html("暂估金额<font face='宋体'>*</font>");
                    $("#SignAmount").val("");
                    $("#SignAmount").removeAttr("readonly");
                }
            });
            $("#InvoiceType").on('change', function () {
                $("#TaxRage").val(eval($(this).mkselectGet() * 100));
                $("#TaxRage").trigger("propertychange");



            });
            $("#SignAmount").on("input propertychange", function () {
                var price = $(this).val() || 0;
                var lastprice = price.charAt(price.length - 1)
                if (lastprice != ".") {
                    ContractPrice = parseFloat(Changjie.clearNumSeparator(price));
                    var Freights = parseFloat($("#Freight").val()) || 0;
                    var SAmount = parseFloat(Changjie.clearNumSeparator($("#SignAmount").val())) || 0;
                    price = eval(SAmount + Freights);
                    $("#SignAmount").val(eval(SAmount + Freights));
                    var rage = $("#TaxRage").val() || 0;

                    var rageconversion = rage / 100;
                    //税额 （税额公式为: 金额 / (1 + 税率) * 税率）
                    var result = (price / (1 + rageconversion) * rageconversion).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                    //  var result = (price * rage / 100).toFixed(Changjie.sysGlobalSettings.pointGlobal);

                    $("#TaxAmount").val(result || "");

                    var depositRage = $("#DepositRage").val() || 0;

                    var v = eval(price * depositRage / 100);
                    $("#DepositAmount").val(v || "");
                }
            });
            $("#TaxRage").on("input propertychange", function () {
                //税率
                var rage = $(this).val() || 0;
                //合同签订金额 
                var price = $("#SignAmount").val() || 0;
                // var result = (price * rage / 100).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                var rageconversion = rage / 100;

                //税额 （税额公式为: 金额 / (1 + 税率) * 税率）
                var result = (price / (1 + rageconversion) * rageconversion).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                $("#TaxAmount").val(result || "");
            });
            $("#DepositRage").on("input propertychange", function () {
                var price = $("#SignAmount").val() || 0;
                var depositRage = $(this).val() || 0;
                var result = eval(price * depositRage / 100);
                $("#DepositAmount").val(result || "");
            });

            $("#Freight").on("input propertychange", function () {

                var value = parseFloat($(this).val() || 0);

                ContractPrice = parseFloat(Changjie.clearNumSeparator($("#SignAmount").val()));
                $("#SignAmount").val(eval(ContractPrice - count + value));

                count = value;

                //税率
                var rage = $("#TaxRage").val() || 0;
                var rageconversion = rage / 100;

                //合同签订金额 
                var price = $("#SignAmount").val() || 0;

                //  var result = (price * rage / 100).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                //税额 （税额公式为: 金额 / (1 + 税率) * 税率）
                var result = (price / (1 + rageconversion) * rageconversion).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                $("#TaxAmount").val(result || "");

                //$("#SignAmount").trigger("propertychange");
            });
            $("#DiscountsContractAmount").on("input propertychange", function () {
                var v1 = parseFloat(Changjie.clearNumSeparator($("#SignAmount").val() || 0));
                var v2 = parseFloat(Changjie.clearNumSeparator($(this).val() || 0));

                $("#DiscountsAmount").val((v1 - v2).toFixed(Changjie.sysGlobalSettings.pointGlobal));
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/PurchaseContract/GetformInfoList?keyValue=' + keyValue, function (data) {
                    var isIsBudget = false;
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);

                            if (!isIsBudget) {
                                if (data[id].IsBudget) {
                                    $("#IsBudget").attr("checked", true);
                                    isIsBudget = true;
                                }
                                else {
                                    $("#IsBudget").attr("checked", false);
                                }
                            }
                        }
                    }
                    if ($("input[name='PriceType']:checked").val() == "UnitPrice") {
                        $("#SignAmount").attr("readonly", "readonly");
                    }
                    else {
                        $("#SignAmount").removeAttr("readonly");
                    }

                    if ($("#YiDesc").val() != "") {
                        $("#addsupper").trigger("click")
                    }

                });
            }
        },
    };
    var setCellValue = function (cellName, row, index, gridName, value) {
        var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
        var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
        row[cellName] = value;
        var type = parseInt(row.EditType || 0);
        if (type != 1) {
            row.EditType = 2;
        }
        $cell.html(value);
        $edit.val(value);
    };
    var setFooterValue = function (rows, runparam) {
        var amountTotal = 0;
        var afterDiscountPriceTotal = 0;
        for (var _index in rows) {
            var row = rows[_index];
            amountTotal += parseFloat(Changjie.clearNumSeparator((row["Amount"] || 0)));
            afterDiscountPriceTotal += parseFloat(Changjie.clearNumSeparator((row["AfterDiscountPrice"] || 0)));
        }
        runparam.op.running.statisticData["Amount"] = amountTotal;
        runparam.op.running.statisticData["AfterDiscountPrice"] = afterDiscountPriceTotal;
        $("div.jfGird-statistic-cell[name='Amount']").html(Changjie.toDecimal(amountTotal));
        $("div.jfGird-statistic-cell[name='AfterDiscountPrice']").html(Changjie.toDecimal(afterDiscountPriceTotal));
    };

    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();

        if (checkBudget() == false) {
            Changjie.alert.warning("签定金额不能超过预算！");
            return false;
        }
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/PurchaseContract/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };

    var checkBudget = function () {
        if ($("#IsBudget").prop("checked")) {
            var amount = $("#SignAmount").val();
            var projectId = $('#ProjectID').mkselectGet();
            var typeid = $('#ContractType').mkselectGet();
            var id = mainId;
            var param = { projectId: projectId, amount: amount, id: id, typeid: typeid };
            var data = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/CheckIsExceedBudgetByProjectId", param).data;
            if (data) { return true; }
            else { return false; }
        }
        else {
            return true;
        }
    };
    page.init();
}
var displaysupperadd = function (ee) {
    if ($(ee).hasClass("fa-plus")) {
        $("#YiDesc,#YiCode,#AccountName,#BankName,#BankNumber").show()
        $(ee).removeClass("fa-plus").addClass("fa-minus")
    }
    else {
        $("#YiDesc,#YiCode,#AccountName,#BankName,#BankNumber").hide()
        $(ee).removeClass("fa-minus").addClass("fa-plus")
    }
}

var auditPassEvent = function (keyValue) {

    ////判断数量
    top.Changjie.httpGet(top.$.rootUrl + '/PurchaseModule/PurchaseContract/GetIsExceed', { keyValue: keyValue }, function (data) {
        if (data.data.exceedNum > 0) {
            top.Changjie.layerConfirm('该单据内有材料超出申请数量，确认要审核通过吗！', function (res) {
                if (res) {
                    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseContract/Audit', { keyValue: keyValue }, function (data) {
                    });
                }
                else {
                    top.Changjie.httpAsync('POST', top.$.rootUrl + '/PurchaseModule/PurchaseContract/EditStatus', { keyValue: keyValue }, function (data) {
                    });
                }
            });
        }
        else
            top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseContract/Audit', { keyValue: keyValue }, function (data) {
            });
    });
};

var unauditPassEvent = function (keyValue) {
    top.Changjie.httpAsync('POST', top.$.rootUrl + '/PurchaseModule/PurchaseContract/UnAudit', { keyValue: keyValue }, function (data) {
    });
};
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }

    if (($('#Yi').mkselectGet() == null || $('#Yi').mkselectGet() == "") && ($('#YiDesc').val() == "" || $('#YiCode').val() == "")) {
        top.Changjie.alert.error("请选择供应商或者输入供应商名称和证件号码");
        return false;
    }

    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Purchase_Contract"]').mkGetFormData());
    postData.strpurchase_ContractDetailsList = JSON.stringify($('#Purchase_ContractDetails').jfGridGet('rowdatas'));
    postData.strproject_PaymentAgreementList = JSON.stringify($('#Project_PaymentAgreement').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
