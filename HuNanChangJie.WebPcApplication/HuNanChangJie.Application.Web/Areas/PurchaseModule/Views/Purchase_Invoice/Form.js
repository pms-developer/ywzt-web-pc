﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-04-03 15:51
 * 描  述：采购发票
 */
var acceptClick;
var ocrCallBack;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Purchase_Invoice";
var processCommitUrl = top.$.rootUrl + '/PurchaseModule/Purchase_Invoice/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var gridverifystate = true;
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_Invoice/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/Purchase_Invoice/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Purchase_InvoiceDetails', "gridId": 'Purchase_InvoiceDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            if (projectId) {
                $("#ProjectID").val(projectId);
            }
            
            $('#ProjectMode').mkDataItemSelect({ code: 'ProjectMode' });
            $("#invoiceocr").on("click", function () {
                Changjie.layerForm({
                    id: 'autoinvoiceocr',
                    title: '发票识别',
                    url: top.$.rootUrl + '/SystemModule/Ocr/InvoiceOcr',
                    width: 1000,
                    height: 700,
                    callBack: function (id) {
                        return top[id].acceptClick(ocrCallBack);
                    }
                });
            });

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#BaesSupplierId').mkDataSourceSelect({ code: 'SupplierList', value: 'id', text: 'name' });
            $('#BaesSupplierId').on("change", function () {
                var sup = $('#BaesSupplierId').mkselectGet();
                if (sup) {
                    var sql = " yi='" + sup + "' ";
                    if (projectId) {
                        sql += " and projectid = '" + projectId + "'"
                    }
                    Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'PurchaseContractList', strWhere: sql }, function (res) {
                        console.log(res)
                        $('#PurchaseContractId').mkselectRefresh({ data: res.data, value: 'id', text: 'title', title: 'title' });
                    })
                }
                else {
                    $('#PurchaseContractId').mkselectRefresh({ data: {} })
                }
            })
            $('#PurchaseContractId').mkselect({});

            //$('#PurchaseContractId').mkDataSourceSelect({ code: 'PurchaseContractList',value: 'id',text: 'name' });
            $('#InvoiceType').mkDataItemSelect({ code: 'FPLX' }).on("change", function () {
                // $("#TaxRate").val(eval($(this).mkselectGet() * 100));
                var taxRate = $(this).mkselectGet();
                $("#TaxRate").val(eval($(this).mkselectGet() * 100));
                var tempamount = $("#InvoiceAmount").val() || 0;

                if (taxRate && tempamount) {
                    var rate = parseFloat(taxRate);
                    //税额 （税额公式为: 金额 / (1 + 税率) * 税率）
                    var taxAmount = (tempamount / (1 + rate) * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                    $("#TaxAmount").val(taxAmount || 0);
                }
                else {
                    $("#TaxAmount").val(0)
                }
            });

            $("#InvoiceAmount").on("input propertychange", function (e) {
                var rate = parseFloat($("#InvoiceType").mkselectGet());
                var baseValue = $(this).val();
                //税额 （税额公式为: 金额 / (1 + 税率) * 税率）
                var result = (baseValue / (1 + rate) * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                $("#TaxAmount").val(result || "");
            });


            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            $('#CollectCompanyId').mkDataItemSelect({ code: 'SPDW' });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Purchase_InvoiceDetails').jfGrid({
                headData: [
                    {
                        label: '订单编号', name: 'OrderCode', width: 100
                    },
                    {
                        label: '合同名称', name: 'ContractName', width: 100
                    },
                    {
                        label: '材料编码', name: 'MaterialsCode', width: 100
                    },
                    {
                        label: '材料名称', name: 'MaterialsName', width: 100
                    },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                    //{ label: "单位", name: "Unit", width: 100, align: "left" },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    { label: "订单数量", name: "OrderQuantity", width: 100, align: "left" },
                    { label: "已收票数量", name: "ReceivedInvoiceQuantity", width: 100, align: "left" },
                    { label: "待收票数量", name: "UnreceivedInvoiceQuantity", width: 100, align: "left" },
                    {
                        label: '本次收票数量', name: 'InvoiceQuantity', width: 100, headColor: 'blue', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购订单明细', inlineOperation: { to: [{ "t1": "InvoiceQuantity", "t2": "AfterTaxPrice", "to": "AfterTaxAmount", "type": "mul", "toTargets": null }, { "t1": "InvoiceQuantity", "t2": "TaxInclusivePrice", "to": "TaxInclusiveAmount", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {
                                var InvoiceQuantity = parseFloat(row["InvoiceQuantity"] || 0);
                                var UnreceivedInvoiceQuantity = parseFloat(row["UnreceivedInvoiceQuantity"] || 0);
                                if (InvoiceQuantity > UnreceivedInvoiceQuantity) {
                                    Changjie.alert.warning("【本次收标数量】不能超过【待收票数量】");
                                    $(obj).css("color", "red");
                                    $(obj).attr("color", "red");
                                    gridverifystate = false;
                                }
                                else {
                                    $(obj).css("color", "black");
                                    $(obj).attr("color", "black");
                                    gridverifystate = true;
                                }
                                page.setCellValues(row, index);

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    { label: "税率(%)", name: "TaxRate", width: 100, align: "left" },
                    {
                        label: '税额', name: 'TaxAmount', width: 100, headColor: 'blue', cellStyle: { 'text-align': 'left' }, statistics: true, aggtype: 'sum', aggcolumn: 'pinyin', required: '0',
                        datatype: 'float', tabname: '采购订单明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    { label: "预算单价", name: "BudgetPrice", width: 100, align: "left" },
                    {
                        label: '不含税', name: 'l484d6a7f3f344224a1c1c8b046d84546', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'AfterTaxPrice', width: 100, headColor: 'blue', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '采购订单明细', inlineOperation: { to: [{ "t1": "InvoiceQuantity", "t2": "AfterTaxPrice", "to": "AfterTaxAmount", "type": "mul", "toTargets": null }, { "t1": "InvoiceQuantity", "t2": "TaxInclusivePrice", "to": "TaxInclusiveAmount", "type": "mul", "toTargets": null }], isFirst: false }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        page.setCellValues(row,index);
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '金额', name: 'AfterTaxAmount', width: 100, headColor: 'blue', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '采购订单明细'
                            }
                        ]
                    },
                    {
                        label: '含税', name: '2484d6a7f3f344224a1c1c8b046d84546', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'TaxInclusivePrice', width: 100, headColor: 'blue', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '采购订单明细', inlineOperation: { to: [{ "t1": "InvoiceQuantity", "t2": "TaxInclusivePrice", "to": "TaxInclusiveAmount", "type": "mul", "toTargets": null }], isFirst: false }
                                //, edit: {
                                //    type: 'input',
                                //    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                //    },
                                //    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                //    }
                                //}
                            },
                            {
                                label: '金额', name: 'TaxInclusiveAmount', width: 100, headColor: 'blue', cellStyle: { 'text-align': 'left' }, statistics: true, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '采购订单明细'
                            }
                        ]
                    },
                    { label: "计划到货日期", name: "PlanDate", width: 100, align: "left" },
                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '采购订单明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Purchase_InvoiceDetails",
                isEdit: true,
                height: 300,
                toolbarposition: "top",
                showchoose: true,
                showadd: false,
                onChooseEvent: function () {
                    page.loadDialog();
                },
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });

            $("#PurchaseContractId").on("change", function () {
                var data = $(this).mkselectGetEx();
                
                if (!!data) {
                    $('#ProjectID').val(data.projectid)
                    $("#TaxRate").val(data.taxrage);
                    $("#InvoiceType").mkselectSet(data.invoicetype);
                    $('#ProjectMode').mkselectSet(data.pmode)
                }
                else {
                    $("#TaxRage").val("");
                    $("#InvoiceType").val("");
                }
            });
            $('#AuditStatus').mkRadioCheckbox({
                type: 'checkbox',
                code: 'AuditStatus',
            });
        },
        setCellValues: function (row, index) {
            var AfterTaxPrice = parseFloat((row.AfterTaxPrice || 0));
            var taxRate = parseFloat(1 + (row.TaxRate || 0) / 100);
            var price = AfterTaxPrice * taxRate;
            row.TaxInclusivePrice = Changjie.format45(price, Changjie.sysGlobalSettings.pointGlobal);
            var afterAmount = AfterTaxPrice * parseFloat(row.InvoiceQuantity || 0);
            row.AfterTaxAmount = Changjie.format45(afterAmount, Changjie.sysGlobalSettings.pointGlobal);

            var amount = parseFloat(row.TaxInclusivePrice || 0) * parseFloat(row.InvoiceQuantity || 0);
            row.TaxInclusiveAmount = Changjie.format45(amount, Changjie.sysGlobalSettings.pointGlobal);
            var taxAmount = parseFloat(row.TaxInclusiveAmount || 0) - parseFloat(row.AfterTaxAmount || 0);
            row.TaxAmount = Changjie.format45(taxAmount, Changjie.sysGlobalSettings.pointGlobal);
             
            setCellValue("TaxInclusivePrice", row, index, "Purchase_InvoiceDetails", row.TaxInclusivePrice);
            setCellValue("AfterTaxAmount", row, index, "Purchase_InvoiceDetails", row.AfterTaxAmount);
            setCellValue("TaxInclusiveAmount", row, index, "Purchase_InvoiceDetails", row.TaxInclusiveAmount);
            setCellValue("TaxAmount", row, index, "Purchase_InvoiceDetails", row.TaxAmount);
        },
        loadDialog: function () {
            var contractId = $('#PurchaseContractId').mkselectGet();
            if (!!contractId) {
                Changjie.layerForm({
                    id: "selectQuantities",
                    width: 900,
                    height: 470,
                    title: "选择采购订单明细",
                    url: top.$.rootUrl + "/PurchaseModule/PurchaseOrder/OrderDetailsDialog?contractId=" + contractId,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                var rows = [];
                                for (var i = 0; i < data.length; i++) {
                                    var row = {};
                                    row.OrderCode = data[i].OrderCode;
                                    row.PurchaseOrderDetailsId = data[i].ID;
                                    row.PurchaseOrderId = data[i].PurchaseOrderId;
                                    row.PurchaseContractId = data[i].PurchaseContractId;
                                    row.PurchaseContractDetailsId = data[i].PurchaseContractDetailsId;
                                    row.ContractName = data[i].ContractName;
                                    row.MaterialsCode = data[i].Code;
                                    row.MaterialsName = data[i].Name;
                                    row.Brand = data[i].Brand;
                                    row.ModelNumber = data[i].ModelNumber;
                                    row.Unit = data[i].Unit;
                                    row.OrderQuantity = data[i].Quantity;
                                    row.ReceivedInvoiceQuantity = data[i].ReceivedInvoiceQuantity;
                                   
                                    row.UnreceivedInvoiceQuantity = parseFloat((data[i].Quantity || 0)) - parseFloat((data[i].ReceivedInvoiceQuantity || 0));
                                    row.InvoiceQuantity = row.UnreceivedInvoiceQuantity;

                                    row.TaxInclusivePrice = data[i].Price;
                                    row.TaxInclusiveAmount = parseFloat((data[i].Price || 0)) * parseFloat((row.InvoiceQuantity || 0));

                                    row.TaxRate = data[i].TaxRage;

                                    var taxRate = parseFloat((row.TaxRate || 0)) / 100;

                                    var taxAmount = parseFloat((row.TaxInclusiveAmount || 0)) / (1 + taxRate) * taxRate;

                                    row.TaxAmount = Changjie.format45(taxAmount, Changjie.sysGlobalSettings.pointGlobal);

                                    row.AfterTaxAmount = parseFloat((row.TaxInclusiveAmount || 0)) - parseFloat((row.TaxAmount || 0))

                                    var afterTaxPrice = parseFloat((row.AfterTaxAmount || 0)) / parseFloat((row.InvoiceQuantity || 0));
                                    row.AfterTaxPrice = Changjie.format45(afterTaxPrice, Changjie.sysGlobalSettings.pointGlobal);
                                   
                                    row.BudgetPrice = data[i].BudgetPrice;
                                    row.PlanDate = data[i].PlanDate;


                                    row.rowState = 1;
                                    row.ID = Changjie.newGuid();
                                    row.EditType = 1;
                                    rows.push(row);
                                }
                                $("#Purchase_InvoiceDetails").jfGridSet("addRows", rows);

                                var signAmount = parseFloat($("#Purchase_InvoiceDetails").jfGridGet("footerdata")["TaxInclusiveAmount"] || 0);
                                $("#InvoiceAmount").val(signAmount);
                                var e = jQuery.Event("propertychange");
                                $("#InvoiceAmount").trigger(e);

                                var taxAmount = parseFloat($("#Purchase_InvoiceDetails").jfGridGet("footerdata")["TaxAmount"] || 0);




                                $("#TaxAmount").val(taxAmount);

                                e = jQuery.Event("propertychange");


                                $("#TaxAmount").trigger(e);
                            }
                        });
                    }
                });
            }
            else {
                Changjie.alert.warning("请您先选择采购合同");
            }
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/Purchase_Invoice/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (!gridverifystate) {
            Changjie.alert.warning("无法保存，表格信息验证不通过");
            return false;
        }
        
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/Purchase_Invoice/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };

    var setCellValue = function (cellName, row, index, gridName, value) {
        var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
        var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
        row[cellName] = value;
        var type = parseInt(row.EditType || 0);
        if (type != 1) {
            row.EditType = 2;
        }
        $cell.html(value);
        $edit.val(value);
    };
    ocrCallBack = function (data) {
        if (data) {
            var maindata = data.maindata;
            var subdata = data.subdata;
            var rate = 0;
            if (subdata) {
                rate = top.Changjie.extractTaxTateNum2(subdata[0].tax_rate);
                $("#TaxRate").val(rate);
            }
            if (maindata) {

                $("#InvoiceCode").val(maindata.number);
                var orcinvoiceType = maindata.invoice_type;
                if (orcinvoiceType) {
                    var invoiceTypeDatas = $("#InvoiceType").mkselectGetAll();
                    var invoiceTypeId = "";
                    if (invoiceTypeDatas) {
                        for (var _i in invoiceTypeDatas) {
                            var item = invoiceTypeDatas[_i];
                            var text = item.text;
                            if (text.indexOf("电子") > -1) {//增值税电子普通发票
                                var itemRate = top.Changjie.extractTaxTateNum1(text);
                                if (itemRate == rate) {
                                    invoiceTypeId = item.id;
                                    break;
                                }
                            }
                            else if (text.indexOf("普通") > -1) {//增值税普通发票
                                var itemRate = top.Changjie.extractTaxTateNum1(text);
                                if (itemRate == rate) {
                                    invoiceTypeId = item.id;
                                    break;
                                }
                            }
                            else if (text.indexOf("专用") > -1) {//增值税专用发票
                                var itemRate = top.Changjie.extractTaxTateNum1(text);
                                if (itemRate == rate) {
                                    invoiceTypeId = item.id;
                                    break;
                                }
                            }
                        }
                    }
                    if (!!invoiceTypeId == false) {
                        Changjie.alert.error('发票类型及税率匹配失败，请人工选择');
                    }
                    else {
                        $("#InvoiceType").mkselectSet(invoiceTypeId);
                    }
                }
                function SetCompany(selectid, value, typename) {
                    if (value) {
                        var $this = $("#" + selectid);
                        var datas = $this.mkselectGetAll();
                        var ismapping = false;
                        if (datas) {
                            for (var _i in datas) {
                                var item = datas[_i];
                                if (item.text == value) {
                                    $this.mkselectSet(item.id);
                                    ismapping = true;
                                    break;
                                }
                            }
                        }
                        if (!ismapping) {
                            Changjie.alert.error(typename + '匹配失败，请人工选择');
                        }
                    }
                    else {
                        Changjie.alert.error(typename + '匹配失败，请人工选择');
                    }
                }

                $("#InvoiceAmount").val(maindata.total);

               $("#issue_date").val(maindata.issue_date);


                SetCompany("BaesSupplierId", maindata.seller_name, "供应商");
                $("#TaxAmount").val(maindata.subtotal_tax);
                
                SetCompany("CollectCompanyId", maindata.seller_name, "收票公司");
                 
            }
        }
        return true;
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Purchase_Invoice"]').mkGetFormData());
    postData.strpurchase_InvoiceDetailsList = JSON.stringify($('#Purchase_InvoiceDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
