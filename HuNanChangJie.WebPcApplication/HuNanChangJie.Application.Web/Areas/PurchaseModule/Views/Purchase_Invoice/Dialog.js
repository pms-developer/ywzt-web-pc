﻿/* * Copyright (c) 2013-2019
 * 创建人：超级管理员
 * 日  期：2020-02-24 10:56
 * 描  述：采购发票
 */

var acceptClick;
var sumInvoiceAmount = 0;
var projectId = request("projectId");
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.initGird();
            page.bind();

          //  $("<label id='sumAmount' style='color: red'>0</label>").insertAfter("#jfgrid_page_bar_gridtable");

        },
        bind: function () {
            $("#refresh").on("click", function () {
                refreshGirdData();
            });

            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.InvoiceCode = findName;
                    param.ProjectID = projectId;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });
        },

        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/PurchaseModule/Purchase_Invoice/GetPageList',
                headData: [
                    { label: "发票编码", name: "InvoiceCode", width: 90, align: "left" },
                    {
                        label: "采购合同", name: "PurchaseContractId", width: 220, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'PurchaseContractList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },

                    { label: "发票金额", name: "InvoiceAmount", width: 100, align: "left" },

                    { label: "已抵金额", name: "AccountingInvoiceAmount", width: 100, align: "left" },

                    { label: "发票余额", name: "balance", width: 100, align: "left" },

                    {
                        label: "发票类型", name: "InvoiceType", width: 150, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FPLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "供应商", name: "BaesSupplierId", width: 220, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'SupplierList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true,
                height: 394,
                isMultiselect: true,
                onSelectRow: function (row, isCheck) {
                    if (isCheck) {
                        sumInvoiceAmount = Number(sumInvoiceAmount) + Number(row.InvoiceAmount - row.AccountingInvoiceAmount);
                        $("#sumAmount").html(sumInvoiceAmount +"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;发票编号");
                    } else {
                        sumInvoiceAmount = Number(sumInvoiceAmount) - Number(row.InvoiceAmount - row.AccountingInvoiceAmount);
                        $("#sumAmount").html(sumInvoiceAmount + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;发票编号");
                    }
                },
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.AuditStatus = 2;
            if (projectId) {
                param.ProjectID = projectId;
            }
            param.Amount = 1;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};