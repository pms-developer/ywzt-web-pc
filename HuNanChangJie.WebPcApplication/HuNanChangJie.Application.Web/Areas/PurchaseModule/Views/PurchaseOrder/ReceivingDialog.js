﻿var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            if (!!projectId) {
                page.bind();
            }
        },
        bind: function () {
            $("#refresh").on("click", function () {
                refreshGirdData();
            });
            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/PurchaseModule/PurchaseOrder/GetDetailListByProjectId',
                headData: [
                    { label: "项目名称", name: "ProjectName", width: 150, align: "left" },
                    { label: "订单编号", name: "OrderCode", width: 150, align: "left" },
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },

                    {
                        label: "单位", name: "Unit", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'MaterialsUnit',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },

                    //{ label: "单位", name: "Unit", width: 100, align: "left" },

                    { label: "采购数量", name: "Quantity", width: 100, align: "left" },
                    { label: "已收货数量", name: "InStorage", width: 100, align: "left" },
                ],
                mainId: 'id',
                isMultiselect: true,
                isPage: true,
                height: 280,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.projectId = projectId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
