﻿var refreshGirdData;
var formId = request("formId");
var orderId = request("orderId");
var projectId = request('projectId');
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            if (!!orderId) {
                page.bind();
            }
        },
        bind: function () {
            $("#refresh").on("click", function () {
                refreshGirdData();
            });

            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/PurchaseModule/PurchaseOrder/GetDetailList',
                headData: [
                    { label: "项目名称", name: "ProjectName", width: 150, align: "left" },
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                    //{
                    //    label: "单位", name: "Unit", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('sourceData', {
                    //            code: 'MaterialsUnit',
                    //            key: value,
                    //            keyId: 'id',
                    //            callback: function (_data) {
                    //                callback(_data['name']);
                    //            }
                    //        });
                    //    } },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    { label: "合同单价", name: "Price", width: 100, align: "left" },
                    { label: "合同数量", name: "Quantity", width: 100, align: "left" },
                    { label: "预算单价", name: "BudgetPrice", width: 100, align: "left" },
                    { label: "待入库数量", name: "WaitInStorage", width: 100, align: "left" }

                ],
                mainId: 'id',
                isMultiselect: true,
                isPage: true,
                height: 280,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.keyValue = orderId;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
