﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-13 11:09
 * 描  述：采购订单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Purchase_Order";
var processCommitUrl = top.$.rootUrl + '/PurchaseModule/PurchaseOrder/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var OrderAmount = 0;
var Freight = 0;
var gridverifystate = true;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Purchase_OrderDetails', "gridId": 'Purchase_OrderDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'PurchaseOrderCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#BuyerId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $("#OrderAmount").on("input propertychange", function () {
                var price = $(this).val() || 0;
                OrderAmount = parseFloat(Changjie.clearNumSeparator(price));
                $(this).val(OrderAmount + Freight);
            });

            $('#SupplierId').mkDataSourceSelect({ code: 'SupplierList', value: 'id', text: 'name' });
            $('#PurchaseContractId').mkDataSourceSelect({ code: 'PurchaseContractList', value: 'id', text: 'name' });
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });


            //$("#PurchaseContractId").on("change", function () {
            //    var data = $(this).mkselectGetEx();
            //    if (!!data) {

            //        $("#TaxRage").val(data.taxrage);
            //        $("#InvoiceType").val(data.f_itemname);
            //        var e = jQuery.Event("propertychange");
            //        $("#InvoiceType").trigger(e);
            //       // $("#ProjectID").mkselectSet(data.projectid);
            //        $("#Jia").mkselectSet(data.jia);
            //        $("#SupplierId").mkselectSet(data.yi);
            //        $("#SupplierLinkman").val(data.linkman);
            //    }
            //    else {
            //        $("#TaxRage").val("");
            //        $("#InvoiceType").val("");
            //        $("#ProjectID").mkselectSet("");
            //        $("#Jia").mkselectSet("");
            //        $("#SupplierId").mkselectSet("");
            //        $("#SupplierLinkman").val("");
            //    }
            //});



            $("#ProjectID").on("change", function () {

                var data = $(this).mkselectGetEx();
                if (!!data) {

                    var purchaseContractId = $("#PurchaseContractId").mkselectGet();
                    $('#PurchaseContractId').removeAttr("readonly");

                    var sql = " projectid='" + data.project_id + "' ";
                    $('#PurchaseContractId').mkDataSourceSelect({ code: 'PurchaseContractList', value: 'id', text: 'name', strWhere: sql });

                    $("#PurchaseContractId").mkselectSet(purchaseContractId);

                    $("#PurchaseContractId").on("change", function () {

                        var data = $(this).mkselectGetEx();
                        if (!!data) {
                            $("#TaxRage").val(data.taxrage);
                            $("#InvoiceType").val(data.f_itemname);
                            var e = jQuery.Event("propertychange");
                            $("#InvoiceType").trigger(e);


                            $("#Jia").mkselectSet(data.jia);
                            $("#SupplierId").mkselectSet(data.yi);
                            $("#SupplierLinkman").val(data.linkman);
                        }
                        else {
                            $("#TaxRage").val("");
                            $("#InvoiceType").val("");
                            //$("#ProjectID").mkselectSet("");
                            $("#Jia").mkselectSet("");
                            $("#SupplierId").mkselectSet("");
                            $("#SupplierLinkman").val("");
                        }
                    });
                }
            });


            $("#Freight").on("input propertychange", function () {
                Freight = parseFloat(Changjie.clearNumSeparator($(this).val() || 0));
                var price = Freight + OrderAmount;
                $("#OrderAmount").val(price);
            });

            $('#Jia').mkCompanySelect({});

            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            //$("#addMaterials").on("click", function () {

            //});

            //$("#deleteMaterials").on("click", function () {
            //    var rowIndex = $("#Purchase_OrderDetails").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Purchase_OrderDetails").jfGridSet("removeRow");
            //    }
            //});
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Purchase_OrderDetails').jfGrid({
                headData: [
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                    //{
                    //    label: "单位", name: "Unit", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('sourceData', {
                    //            code: 'MaterialsUnit',
                    //            key: value,
                    //            keyId: 'id',
                    //            callback: function (_data) {
                    //                callback(_data['name']);
                    //            }
                    //        });
                    //    }
                    //},

                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            }
                            // ,readonly: true
                        }
                    },

                    { label: "预算单价", name: "BudgetPrice", width: 100, align: "left" },
                    { label: "合同数量", name: "ContractQuantity", width: 100, align: "left" },
                    { label: "待采购数量", name: "WaitBuyQuantity", width: 100, align: "left" },
                    { label: "已订数量", name: "TotalOrderQuantity", width: 100, align: "left" },
                    {
                        label: '采购数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '采购订单明细',
                        inlineOperation: {
                            to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }]
                        }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {
                                setFooterValue(rows, headData);
                                if (row.ContractQuantity) {
                                    var Quantity = parseFloat(row["Quantity"] || 0);
                                    var WaitBuyQuantity = parseFloat(row["WaitBuyQuantity"] || 0);
                                    if (Quantity > WaitBuyQuantity) {
                                        Changjie.alert.warning("【采购数量】不能超过【待采购数量】");
                                        $(obj).css("color", "red");
                                        $(obj).attr("color", "red");
                                        gridverifystate = false;
                                    }
                                    else {
                                        $(obj).css("color", "black");
                                        $(obj).attr("color", "black");
                                        gridverifystate = true;
                                    }
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合同单价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0', datatype: 'float', tabname: '采购订单明细',
                        inlineOperation: {
                            to: [{ "t1": "Quantity", "t2": "Price", "to": "TotalPrice", "type": "mul", "toTargets": null }]
                        }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {
                                setFooterValue(rows, headData);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合计金额', name: 'TotalPrice', statistics: true, width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'OrderAmount', required: '0',
                        datatype: 'float', tabname: '采购订单明细'
                    },
                    {
                        label: '计划到货日期', name: 'PlanDate', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '采购订单明细'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '要求交货周期', name: 'Period', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '采购订单明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '交货地点', name: 'Address', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '采购订单明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Purchase_OrderDetails",
                isEdit: true, showchooses: true,
                onChooseEvents: function () {
                    var contractId = $('#PurchaseContractId').mkselectGet();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 1100,
                            height: 1000,
                            title: "选择材料档案清单",
                            url: top.$.rootUrl + "/SystemModule/BaseMaterials/MaterialsDialog?projectId=" + projectId,
                            // url: top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/MaterialsDialog?projectId=" + projectId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            console.log(data[i], i)
                                            row.Fk_BaseMaterialsId = data[i].ID;
                                            row.PurchaseContractId = contractId;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].BrandName;
                                            row.ModelNumber = data[i].Model;
                                            //row.Unit = data[i].UnitName;
                                            row.Unit = data[i].UnitId;

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }
                                        $("#Purchase_OrderDetails").jfGridSet("addRows", rows);

                                        var freight = parseFloat($("#Freight").val() || 0);
                                        var amount = parseFloat($("#Purchase_OrderDetails").jfGridGet("footerdata")["TotalPrice"] || 0);
                                        $("#OrderAmount").val(freight + amount);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择采购合同");
                    }
                },
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    var contractId = $('#PurchaseContractId').mkselectGet();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择采购任务",
                            url: top.$.rootUrl + "/PurchaseModule/PurchaseContract/DetailDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            console.log(data[i]);
                                            row.PurchaseContractDetailsId = data[i].ID;
                                            row.ProjectConstructionBudgetMaterialsId = data[i].ProjectConstructionBudgetMaterialsId;
                                            row.PurchaseContractId = contractId;
                                            row.Fk_BaseMaterialsId = data[i].Fk_BaseMaterialsId;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].Brand;
                                            row.ModelNumber = data[i].ModelNumber;
                                            row.Unit = data[i].Unit;
                                            row.BudgetPrice = data[i].BudgetPrice;
                                            row.ContractQuantity = data[i].ContractQuantity || 0;
                                            row.TotalOrderQuantity = data[i].TotalOrderQuantity || 0;
                                            var result = parseFloat(Changjie.clearNumSeparator(row.ContractQuantity)) - parseFloat(Changjie.clearNumSeparator(row.TotalOrderQuantity));
                                            row.WaitBuyQuantity = result;
                                            row.Price = data[i].ContractPrice;
                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }
                                        $("#Purchase_OrderDetails").jfGridSet("addRows", rows);

                                        var freight = parseFloat($("#Freight").val() || 0);
                                        var amount = parseFloat($("#Purchase_OrderDetails").jfGridGet("footerdata")["TotalPrice"] || 0);
                                        $("#OrderAmount").val(freight + amount);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择采购合同");
                    }
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PurchaseModule/PurchaseOrder/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {

                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        $("#ProjectName").val($("#ProjectID").mkselectGetText());
        if (!gridverifystate) {
            Changjie.alert.warning("无法保存，表格信息验证不通过");
            return false;
        }
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/PurchaseModule/PurchaseOrder/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };

    var setFooterValue = function (rows, runparam) {
        var amountTotal = 0;
        for (var _index in rows) {
            var row = rows[_index];
            amountTotal += parseFloat(Changjie.clearNumSeparator((row["TotalPrice"] || 0)));
        }
        runparam.op.running.statisticData["TotalPrice"] = amountTotal;
        $("div.jfGird-statistic-cell[name='TotalPrice']").html(Changjie.toDecimal(amountTotal));
    };

    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Purchase_Order"]').mkGetFormData());
    postData.strpurchase_OrderDetailsList = JSON.stringify($('#Purchase_OrderDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
