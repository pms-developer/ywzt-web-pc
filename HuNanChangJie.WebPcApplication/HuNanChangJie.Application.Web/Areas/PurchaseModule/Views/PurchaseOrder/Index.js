﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-13 11:09
 * 描  述：采购订单
 */
var refreshGirdData;
var formId = request("formId");
var auditPassEvent;
var unauditPassEvent;
var projectId = request("projectId");
var checkJson = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            //Changjie.clientdata.refresh('sourceData', {
            //    code: 'SupplierList'
            //});
            //Changjie.clientdata.refresh('sourceData', {
            //    code: 'PurchaseContractList'
            //});
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 240, 400);
            $('#SupplierId').mkDataSourceSelect({ code: 'SupplierId', value: 'id', text: 'name' });
            $('#PurchaseContractId').mkDataSourceSelect({ code: 'PurchaseContractList', value: 'id', text: 'name' });
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增采购订单',
                    url: top.$.rootUrl + '/PurchaseModule/PurchaseOrder/Form?projectId=' + projectId,
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PurchaseModule/PurchaseOrder/GetPageList',
                headData: [
                    {
                        label: "项目", name: "ProjectID", width: 220, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },
                    {
                        label: "采购合同", name: "PurchaseContractId", width: 250, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'PurchaseContractList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    { label: "订单名称", name: "Name", width: 220, align: "left" },
                    { label: "订单编号", name: "Code", width: 120, align: "left" },
                    { label: "订单金额", name: "OrderAmount", width: 120, align: "left" },
                    {
                        label: "订单日期", name: "OrderDate", width: 80, align: "center",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    {
                        label: "采购员", name: "BuyerId", width: 90, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "供应商", name: "SupplierId", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'SupplierList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        Changjie.clientdata.refresh('custmerData', {
            code: 'SupplierList'
        });
        Changjie.clientdata.refresh('custmerData', {
            code: 'PurchaseContractList'
        });
        page.search(checkJson);
    };
    auditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/PurchaseModule/PurchaseOrder/Audit', { keyValue: keyValue }, function (data) {
        });
        //添加Base_MaterialsBuyRecord数据
        Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseOrder/SaveMaterialsBuyRecord', { keyValue: keyValue, type: "audit" }, function (data) {
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/PurchaseModule/PurchaseOrder/UnAudit', { keyValue: keyValue }, function (data) {
        });
        //删除Base_MaterialsBuyRecord数据
        Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseOrder/SaveMaterialsBuyRecord', { keyValue: keyValue, type: "unaudit" }, function (data) {
        });
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/PurchaseModule/PurchaseOrder/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {

    var keyValue = $('#gridtable').jfGridValue('ID');
    var projectID = $('#gridtable').jfGridValue('ProjectID');

    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '采购订单',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/PurchaseModule/PurchaseOrder/Form?keyValue=' + keyValue + '&viewState=' + viewState + '&projectId=' + projectID + "&formId=" + formId, 
            width: 1000,
            height: 800,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
