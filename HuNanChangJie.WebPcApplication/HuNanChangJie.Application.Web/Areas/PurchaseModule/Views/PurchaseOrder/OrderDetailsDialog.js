﻿var refreshGirdData;
var formId = request("formId");
var contractId = request("contractId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/PurchaseModule/PurchaseOrder/GetOrderDetailList',
                headData: [
                    { label: "订单编号", name: "OrderCode", width: 150, align: "left" },
                    { label: "合同名称", name: "ContractName", width: 150, align: "left" },
                    { label: "材料编码", name: "Code", width: 150, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "品牌", name: "Brand", width: 100, align: "left" },
                    { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                    {
                        label: "单位", name: "Unit", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'MaterialsUnit',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }},
                    { label: "订单数量", name: "Quantity", width: 100, align: "left" },
                    { label: "合同数量", name: "ContractQuantity", width: 100, align: "left" },
                    { label: "税率(%)", name: "TaxRage", width: 100, align: "left" },
                    { label: "税额", name: "TaxAmount", width: 100, align: "left" },
                    {
                        label: "不含税", name: "S46456465", width: 200, align: "left",
                        children: [
                            { label: "单价", name: "NotTaxUnit", width: 100, align: "left" },
                            { label: "金额", name: "NotTaxAmount", width: 100, align: "left" },
                        ]
                    },
                    {
                        label: "含税", name: "SS6456465", width: 200, align: "left",
                        children: [
                            { label: "单价", name: "Price", width: 100, align: "left" },
                            { label: "金额", name: "TotalPrice", width: 100, align: "left" },
                        ]
                    },
                    { label: "已收票数量", name: "ReceivedInvoiceQuantity", width: 100, align: "left" },
                    { label: "收货未收票数量", name: "SHWSP", width: 100, align: "left" },
                    { label: "计划到货日期", name: "PlanDate", width: 100, align: "left" },
                ],
                mainId: 'id',
                isMultiselect: true,
                isPage: true,
                height: 362,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.contractId = contractId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
