﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.PurchaseModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 19:12
    /// 描 述：采购合同
    /// </summary>
    public class PurchaseContractController : MvcControllerBase
    {
        private PurchaseContractIBLL purchaseContractIBLL = new PurchaseContractBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult DetailDialog()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        [HttpGet]
        public ActionResult VerificationForm()
        {
            return View();
        }

        [HttpGet]
        public ActionResult VerificationFormList()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchaseContractIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList(string projectId)
        {
            var data = purchaseContractIBLL.GetList(projectId);

            return Success(data);
        }

        public ActionResult GetDetailList(string contractId)
        {
            var data = purchaseContractIBLL.GetDetailList(contractId);
            var jsondata = new { rows = data };
            return Success(jsondata);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Purchase_ContractData = purchaseContractIBLL.GetPurchase_ContractEntity(keyValue);
            var Purchase_ContractDetailsData = purchaseContractIBLL.GetPurchase_ContractDetailsList(Purchase_ContractData.ID);
            var Project_PaymentAgreementData = purchaseContractIBLL.GetProject_PaymentAgreementList(Purchase_ContractData.ID);
            var jsonData = new
            {
                Purchase_Contract = Purchase_ContractData,
                Purchase_ContractDetails = Purchase_ContractDetailsData,
                Project_PaymentAgreement = Project_PaymentAgreementData,
            };
            return Success(jsonData);
        }




        [HttpGet]
        [AjaxOnly]
        public ActionResult GetContractList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchaseContractIBLL.GetContractList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetIsExceed(string keyValue)
        {
            var num = purchaseContractIBLL.GetIsExceed(keyValue);
            var jsonData = new
            {
                exceedNum = num
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            purchaseContractIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strpurchase_ContractDetailsList, string strproject_PaymentAgreementList, string deleteList)
        {
            strEntity = strEntity.Replace("\"IsBudget\":\"1\"", "\"IsBudget\":\"true\"");
            strEntity = strEntity.Replace("\"IsBudget\":\"0\"", "\"IsBudget\":\"false\"");
            var mainInfo = strEntity.ToObject<Purchase_ContractEntity>();


            var purchase_ContractDetailsList = strpurchase_ContractDetailsList.ToObject<List<Purchase_ContractDetailsEntity>>();
            var project_PaymentAgreementList = strproject_PaymentAgreementList.ToObject<List<Project_PaymentAgreementEntity>>();
            purchaseContractIBLL.SaveEntity(keyValue, mainInfo, purchase_ContractDetailsList, project_PaymentAgreementList, deleteList, type);
            return Success("保存成功！");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult VerificationSaveForm(string purchaseContractid, string type, string strEntity, string deleteList)
        {
            var mainInfo = strEntity.ToObject<VerificationTableEntity>();

            purchaseContractIBLL.VerificationSaveEntity(purchaseContractid, mainInfo, deleteList, type);
            return Success("保存成功！");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult VerificationSaveFormList(string strEntity, string strVerificationTableList, string purchaseContractid, string type, string deleteList)
        {
            var mainInfo = strEntity.ToObject<Purchase_ContractEntity>();
            var verificationTableList = strVerificationTableList.ToObject<List<VerificationTableEntity>>();
            purchaseContractIBLL.VerificationSaveListEntity(purchaseContractid, mainInfo, verificationTableList, deleteList, type);
            return Success("保存成功！");
        }



        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfo(string purchase_ContractID)
        {
            var VerificationTableData = purchaseContractIBLL.GetVerificationTableEntity(purchase_ContractID);

            var jsonData = new
            {
                VerificationTable = VerificationTableData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfos(string purchase_ContractID)
        {
            var Purchase_ContractData = purchaseContractIBLL.GetPurchase_ContractEntity(purchase_ContractID);
            var VerificationTablesData = purchaseContractIBLL.GetPageList(purchase_ContractID);
            var jsonData = new
            {
                Purchase_Contract = Purchase_ContractData,
                VerificationTable = VerificationTablesData,
            };
            return Success(jsonData);
        }


        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            purchaseContractIBLL.Audit(keyValue);
            return Success("操作成功");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            purchaseContractIBLL.UnAudit(keyValue);
            return Success("操作成功");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult EditStatus(string keyValue)
        {
            purchaseContractIBLL.EditStatus(keyValue);
            return Success("操作成功");
        }
        #endregion

    }
}
