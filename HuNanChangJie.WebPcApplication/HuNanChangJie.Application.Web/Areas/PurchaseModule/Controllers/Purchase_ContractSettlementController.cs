﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.PurchaseModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 20:25
    /// 描 述：采购合同结算
    /// </summary>
    public class Purchase_ContractSettlementController : MvcControllerBase
    {
        private Purchase_ContractSettlementIBLL purchase_ContractSettlementIBLL = new Purchase_ContractSettlementBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchase_ContractSettlementIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Purchase_ContractSettlementData = purchase_ContractSettlementIBLL.GetPurchase_ContractSettlementEntity(keyValue);
            var Purchase_ContractSettlementDetailsData = purchase_ContractSettlementIBLL.GetPurchase_ContractSettlementDetailsList(Purchase_ContractSettlementData.ID);

            var jsonData = new {
                Purchase_ContractSettlement = Purchase_ContractSettlementData,
                Purchase_ContractSettlementDetails = Purchase_ContractSettlementDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = purchase_ContractSettlementIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = purchase_ContractSettlementIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            purchase_ContractSettlementIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strpurchase_ContractSettlementDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Purchase_ContractSettlementEntity>();
            var purchase_ContractSettlementDetailsList = strpurchase_ContractSettlementDetailsList.ToObject<List<Purchase_ContractSettlementDetailsEntity>>();
            purchase_ContractSettlementIBLL.SaveEntity(keyValue,mainInfo,purchase_ContractSettlementDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
