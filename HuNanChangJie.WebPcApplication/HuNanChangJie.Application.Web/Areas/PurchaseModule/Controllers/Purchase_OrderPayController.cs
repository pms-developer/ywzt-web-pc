﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.PurchaseModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-06 10:09
    /// 描 述：采购订单付款申请
    /// </summary>
    public class Purchase_OrderPayController : MvcControllerBase
    {
        private Purchase_OrderPayIBLL purchase_OrderPayIBLL = new Purchase_OrderPayBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchase_OrderPayIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Purchase_OrderPayData = purchase_OrderPayIBLL.GetPurchase_OrderPayEntity( keyValue );
            var Purchase_OrderPayDetailsData = purchase_OrderPayIBLL.GetPurchase_OrderPayDetailsList( Purchase_OrderPayData.ID );
            var jsonData = new {
                Purchase_OrderPay = Purchase_OrderPayData,
                Purchase_OrderPayDetails = Purchase_OrderPayDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = purchase_OrderPayIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = purchase_OrderPayIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
    [AjaxOnly]
    public ActionResult DeleteForm(string keyValue)
    {
        purchase_OrderPayIBLL.DeleteEntity(keyValue);
        return Success("删除成功！");
    }

   
        //[HttpPost]
        //[AjaxOnly]
        //public ActionResult GetHeTongList(string keyWord)
        //{
        //  var data =  purchase_OrderPayIBLL.GetHeTongList(keyWord);
        //    return Success(data);
        //}
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strpurchase_OrderPayDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Purchase_OrderPayEntity>();
            var purchase_OrderPayDetailsList = strpurchase_OrderPayDetailsList.ToObject<List<Purchase_OrderPayDetailsEntity>>();
            purchase_OrderPayIBLL.SaveEntity(keyValue,mainInfo,purchase_OrderPayDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
