﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.PurchaseModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-19 09:55
    /// 描 述：现场收货单
    /// </summary>
    public class StorageLocaleMatesReceiptController : MvcControllerBase
    {
        private StorageLocaleMatesReceiptIBLL storageLocaleMatesReceiptIBLL = new StorageLocaleMatesReceiptBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = storageLocaleMatesReceiptIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Storage_LocaleMatesReceiptData = storageLocaleMatesReceiptIBLL.GetStorage_LocaleMatesReceiptEntity( keyValue );
            var Storage_LocalematesReceiptDetailsData = storageLocaleMatesReceiptIBLL.GetStorage_LocalematesReceiptDetailsList( Storage_LocaleMatesReceiptData.ID );
            var jsonData = new {
                Storage_LocaleMatesReceipt = Storage_LocaleMatesReceiptData,
                Storage_LocalematesReceiptDetails = Storage_LocalematesReceiptDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            storageLocaleMatesReceiptIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strstorage_LocalematesReceiptDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Storage_LocaleMatesReceiptEntity>();
            var storage_LocalematesReceiptDetailsList = strstorage_LocalematesReceiptDetailsList.ToObject<List<Storage_LocalematesReceiptDetailsEntity>>();
            storageLocaleMatesReceiptIBLL.SaveEntity(keyValue,mainInfo,storage_LocalematesReceiptDetailsList,deleteList,type);
            return Success("保存成功！");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            storageLocaleMatesReceiptIBLL.Audit(keyValue);
            return Success("保存成功！");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            storageLocaleMatesReceiptIBLL.UnAudit(keyValue);
            return Success("保存成功！");
        }
        #endregion

    }
}
