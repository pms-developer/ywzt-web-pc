﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.PurchaseModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 14:19
    /// 描 述：采购入库
    /// </summary>
    public class PurchaseInStorageController : MvcControllerBase
    {
        private PurchaseInStorageIBLL purchaseInStorageIBLL = new PurchaseInStorageBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        [HttpGet]
        public ActionResult DetailDialog()
        {
            return View();
        }
        
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchaseInStorageIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Purchase_InStorageData = purchaseInStorageIBLL.GetPurchase_InStorageEntity( keyValue );
            var Purchase_InStorageDetailsData = purchaseInStorageIBLL.GetPurchase_InStorageDetailsList( Purchase_InStorageData.ID );
            var jsonData = new {
                Purchase_InStorage = Purchase_InStorageData,
                Purchase_InStorageDetails = Purchase_InStorageDetailsData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoListConnectionMaterials(string keyValue)
        {
            var Purchase_InStorageData = purchaseInStorageIBLL.GetPurchase_InStorageEntity(keyValue);
            var Purchase_InStorageDetailsData = purchaseInStorageIBLL.GetPurchase_InStorageDetailsListConnectionMaterials(Purchase_InStorageData.ID);
            var jsonData = new
            {
                Purchase_InStorage = Purchase_InStorageData,
                Purchase_InStorageDetails = Purchase_InStorageDetailsData,
            };
            return Success(jsonData);
        }

        #endregion

        #region  提交数据

        [HttpPost]
        [AjaxOnly]
        public ActionResult AuditSporadicPurchase(string keyValue)
        {
            var result = purchaseInStorageIBLL.AuditSporadicPurchase(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }



        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAuditPassSporadicPurchase(string keyValue)
        {
            var result = purchaseInStorageIBLL.UnAuditPassSporadicPurchase(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }



        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = purchaseInStorageIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = purchaseInStorageIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            purchaseInStorageIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strpurchase_InStorageDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Purchase_InStorageEntity>();
            var purchase_InStorageDetailsList = strpurchase_InStorageDetailsList.ToObject<List<Purchase_InStorageDetailsEntity>>();
            purchaseInStorageIBLL.SaveEntity(keyValue,mainInfo,purchase_InStorageDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
