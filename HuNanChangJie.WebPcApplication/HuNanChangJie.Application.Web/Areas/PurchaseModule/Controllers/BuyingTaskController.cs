﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule.BuyingTask;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.PurchaseModule.Controllers
{
    public class BuyingTaskController :  MvcControllerBase
    {
        private BuyingTaskIBLL bll = new BuyingTaskBLL();
        // GET: PurchaseModule/BuyingTask
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dialog()
        {
            return View();
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = bll.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
    }
}