﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;

namespace HuNanChangJie.Application.Web.Areas.PurchaseModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 11:09
    /// 描 述：采购订单
    /// </summary>
    public class PurchaseOrderController : MvcControllerBase
    {
        private PurchaseOrderIBLL purchaseOrderIBLL = new PurchaseOrderBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        [HttpGet]
        public ActionResult DetailDialog()
        {
            return View();
        }

        /// <summary>
        /// 采购订单明细（用于采购发票明细选择）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OrderDetailsDialog()
        {
            return View();
        }

        /// <summary>
        /// 收货对话框
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ReceivingDialog()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchaseOrderIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetOrderDetailList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchaseOrderIBLL.GetOrderDetailList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult Base_SupplierName(string queryJson)
        {
            
            var data = purchaseOrderIBLL.GetBase_SupplierNameList(queryJson);
            var jsonData = new
            {
                rows = data
            };
            return Success(jsonData);
        }


        [HttpGet]
        [AjaxOnly]
        public ActionResult GetOrderList(string supplierId)
        {
            var data = purchaseOrderIBLL.GetOrderList(supplierId);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetDetailList(string pagination,string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchaseOrderIBLL.GetDetailList(paginationobj, queryJson);
            //var jsonData = new
            //{
            //    rows = data
            //};
            //return Success(jsonData);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetDetailListByProjectId(string pagination,string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchaseOrderIBLL.GetDetailListByProjectId(paginationobj, queryJson);
            //var jsonData = new { rows=data};
            //return Success(jsonData);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetOrderDetailListByOrderId(string orderId)
        {
            var purchase_OrderDetailsData = purchaseOrderIBLL.GetPurchase_OrderDetailsList(orderId);
            var contractDetailIdList = purchase_OrderDetailsData.Select(i => i.PurchaseContractDetailsId);
            var contractBll = new PurchaseContractBLL();
            var contractDetailList = contractBll.GetDetailList(contractDetailIdList);
            var list = from a in purchase_OrderDetailsData
                       join b in contractDetailList on a.PurchaseContractDetailsId equals b.ID
                       select new
                       {
                           PurchaseOrderDetailsId = a.ID,
                           PurchaseOrderId = a.PurchaseOrderId,
                           PurchaseContractId = a.PurchaseContractId,
                           PurchaseContractDetailsId = a.PurchaseContractDetailsId,
                           ProjectConstructionBudgetMaterialsId = a.ProjectConstructionBudgetMaterialsId,
                           Quantity = a.Quantity,
                           Price = a.Price,
                           TotalPrice = a.TotalPrice,
                           PlanDate = a.PlanDate,
                           Period = a.Period,
                           Address = a.Address,
                           Remark = a.Remark,
                           SortCode = a.SortCode,
                           CreationDate = a.CreationDate,
                           Code = b.Code,
                           Name = b.Name,
                           Brand = b.Brand,
                           ModelNumber = b.ModelNumber,
                           Unit = b.Unit,
                           BudgetPrice = b.BudgetPrice ?? 0M,
                           ContractQuantity = b.ContractQuantity ?? 0M,
                           TotalOrderQuantity = b.TotalOrderQuantity ?? 0M,
                           WaitBuyQuantity = (b.ContractQuantity ?? 0) - (b.TotalOrderQuantity ?? 0)
                       };

            var vv = list.ToList();


            var baseMaterialsList = new BaseMaterialsBLL().GetMaterials(new XqPagination() { page = 1, rows = 1000000, records = 0, sord = "ASC", sidx = "" }, "", "{}");
           
            var listBaseMaterials = from a in purchase_OrderDetailsData.Where(m => string.IsNullOrEmpty(m.PurchaseContractDetailsId))
                                    join b in baseMaterialsList on a.Fk_BaseMaterialsId equals b.ID
                                    select new
                                    {
                                        PurchaseOrderDetailsId = a.ID,
                                        PurchaseOrderId = a.PurchaseOrderId,
                                        PurchaseContractId = a.PurchaseContractId,
                                        PurchaseContractDetailsId = a.PurchaseContractDetailsId,
                                        ProjectConstructionBudgetMaterialsId = a.ProjectConstructionBudgetMaterialsId,
                                        Quantity = a.Quantity,
                                        Price = a.Price,
                                        TotalPrice = a.TotalPrice,
                                        PlanDate = a.PlanDate,
                                        Period = a.Period,
                                        Address = a.Address,
                                        Remark = a.Remark,
                                        SortCode = a.SortCode,
                                        CreationDate = a.CreationDate,

                                        Code = b.Code,
                                        Name = b.Name,
                                        Brand = b.BrandName,
                                        ModelNumber = b.Model,
                                        //Unit = b.UnitName,
                                        Unit = b.UnitId,
                                        BudgetPrice = 0M,
                                        ContractQuantity = 0M,
                                        TotalOrderQuantity = 0M,
                                        WaitBuyQuantity = 0M
                                    };

            vv.AddRange(listBaseMaterials.ToList());

            return Success(vv);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Purchase_OrderData = purchaseOrderIBLL.GetPurchase_OrderEntity( keyValue );
            var Purchase_OrderDetailsData = purchaseOrderIBLL.GetPurchase_OrderDetailsList( Purchase_OrderData.ID );
            var contractDetailIdList = Purchase_OrderDetailsData.Select(i => i.PurchaseContractDetailsId);
            var contractBll = new PurchaseContractBLL();
            var contractDetailList = contractBll.GetDetailList(contractDetailIdList);
            var baseMaterialsList = new BaseMaterialsBLL().GetMaterials(new XqPagination() { page=1, rows=1000000, records=0, sord="ASC", sidx="" }, "", "{}");
            var listBudgetMaterials = from a in Purchase_OrderDetailsData.Where(m=>!string.IsNullOrEmpty(m.PurchaseContractDetailsId))
                       join b in contractDetailList on a.PurchaseContractDetailsId equals b.ID
                       select new {
                           ID = a.ID,
                           PurchaseOrderId = a.PurchaseOrderId,
                           PurchaseContractId = a.PurchaseContractId,
                           PurchaseContractDetailsId = a.PurchaseContractDetailsId,
                           ProjectConstructionBudgetMaterialsId = a.ProjectConstructionBudgetMaterialsId,
                           Quantity = a.Quantity,
                           Price = a.Price,
                           TotalPrice = a.TotalPrice,
                           PlanDate = a.PlanDate,
                           Period = a.Period,
                           Address = a.Address,
                           Remark = a.Remark,
                           SortCode = a.SortCode,
                           CreationDate = a.CreationDate,

                           Code = b.Code,
                           Name = b.Name,
                           Brand = b.Brand,
                           ModelNumber = b.ModelNumber,
                           Unit = b.Unit,
                           BudgetPrice = b.BudgetPrice??0M,
                           ContractQuantity = b.ContractQuantity ?? 0M,
                           TotalOrderQuantity = b.TotalOrderQuantity ?? 0M,
                           WaitBuyQuantity = (b.ContractQuantity??0)-(b.TotalOrderQuantity??0)
                       };

            var vv = listBudgetMaterials.ToList();

            var cccc = Purchase_OrderDetailsData.Where(m => !string.IsNullOrEmpty(m.PurchaseContractDetailsId)).ToList();
            var ccc = Purchase_OrderDetailsData.Where(m => string.IsNullOrEmpty(m.PurchaseContractDetailsId)).ToList();


            var listBaseMaterials = from a in Purchase_OrderDetailsData.Where(m => string.IsNullOrEmpty(m.PurchaseContractDetailsId))
                       join b in baseMaterialsList on a.Fk_BaseMaterialsId equals b.ID
                       select new
                       {
                           ID = a.ID,
                           PurchaseOrderId = a.PurchaseOrderId,
                           PurchaseContractId = a.PurchaseContractId,
                           PurchaseContractDetailsId = a.PurchaseContractDetailsId,
                           ProjectConstructionBudgetMaterialsId = a.ProjectConstructionBudgetMaterialsId,
                           Quantity = a.Quantity,
                           Price = a.Price,
                           TotalPrice = a.TotalPrice,
                           PlanDate = a.PlanDate,
                           Period = a.Period,
                           Address = a.Address,
                           Remark = a.Remark,
                           SortCode = a.SortCode,
                           CreationDate = a.CreationDate,

                           Code = b.Code,
                           Name = b.Name,
                           Brand = b.BrandName,
                           ModelNumber = b.Model,
                           //Unit = b.UnitName,
                           Unit = b.UnitId,
                           BudgetPrice = 0M,
                           ContractQuantity = 0M,
                           TotalOrderQuantity = 0M,
                           WaitBuyQuantity = 0M
                       };

            vv.AddRange(listBaseMaterials.ToList());

            var jsonData = new {
                Purchase_Order = Purchase_OrderData,
                Purchase_OrderDetails = vv,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            purchaseOrderIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strpurchase_OrderDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Purchase_OrderEntity>();
            var purchase_OrderDetailsList = strpurchase_OrderDetailsList.ToObject<List<Purchase_OrderDetailsEntity>>();
            purchaseOrderIBLL.SaveEntity(keyValue,mainInfo,purchase_OrderDetailsList,deleteList,type);
            return Success("保存成功！");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            purchaseOrderIBLL.Audit(keyValue);
            return Success("保存成功！");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            purchaseOrderIBLL.UnAudit(keyValue);
            return Success("保存成功！");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult SaveMaterialsBuyRecord(string keyValue,string type)
        {
            purchaseOrderIBLL.SaveMaterialsBuyRecord(keyValue,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
