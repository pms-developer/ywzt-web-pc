﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.PurchaseModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-15 17:08
    /// 描 述：采购订单变更
    /// </summary>
    public class PurchaseOrderChangeController : MvcControllerBase
    {
        private PurchaseOrderChangeIBLL purchaseOrderChangeIBLL = new PurchaseOrderChangeBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchaseOrderChangeIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Purchase_OrderChangeData = purchaseOrderChangeIBLL.GetPurchase_OrderChangeEntity( keyValue );
            var Purchase_OrderChangeDetailsData = purchaseOrderChangeIBLL.GetPurchase_OrderChangeDetailsList( Purchase_OrderChangeData.ID );
            var jsonData = new {
                Purchase_OrderChange = Purchase_OrderChangeData,
                Purchase_OrderChangeDetails = Purchase_OrderChangeDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            purchaseOrderChangeIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strpurchase_OrderChangeDetailsList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<Purchase_OrderChangeEntity>();
            var purchase_OrderChangeDetailsList = strpurchase_OrderChangeDetailsList.ToObject<List<Purchase_OrderChangeDetailsEntity>>();
            purchaseOrderChangeIBLL.SaveEntity(keyValue, mainInfo, purchase_OrderChangeDetailsList, deleteList, type);
            return Success("保存成功！");
        }

        [AjaxOnly]
        [HttpPost]
        public ActionResult Audit(string keyValue)
        {
            purchaseOrderChangeIBLL.Audit(keyValue);
            return Success("保存成功！");
        }

        [AjaxOnly]
        [HttpPost]
        public ActionResult UnAudit(string keyValue)
        {
            purchaseOrderChangeIBLL.UnAudit(keyValue);
            return Success("保存成功！");
        }
        #endregion

    }
}
