﻿var dbAllTable = [];
var databaseLinkId = "";
var dbTable = "";
var dbTablePk = "";
var mapField = {};
var queryAllComponts = [];
var queryAllCompontMap = {};
var bootstrap = function (a, b) {
    var e = a("#rootDirectory").val();
    var d = {};
    var f = function () {
        var g = true;
        a.each(dbTable,
            function (h, i) {
                if (!mapField[databaseLinkId + i.name]) {
                    g = false;
                    return false
                }
            });
        if (g) {
            tableFieldTree.length = 0;
            a.each(dbTable,
                function (h, i) {
                    var o = {
                        id: i.name,
                        text: i.name,
                        value: i.name,
                        hasChildren: true,
                        isexpand: true,
                        complete: true,
                        ChildNodes: []
                    };
                    for (var l = 0,
                        m = mapField[databaseLinkId + i.name].length; l < m; l++) {
                        var k = mapField[databaseLinkId + i.name][l];
                        var n = {
                            id: o.text + k.f_column,
                            text: k.f_column,
                            value: k.f_column,
                            title: k.f_remark,
                            hasChildren: false,
                            isexpand: false,
                            complete: true,
                            showcheck: true
                        };
                        o.ChildNodes.push(n)
                    }
                    tableFieldTree.push(o)
                })
        } else {
            setTimeout(function () {
                f()
            },
                100)
        }
    };
    var c = {
        init: function () {
            c.bind()
        },
        bind: function () {
            a("#refresh").on("click",
                function () {
                    location.reload()
                });
            a("#wizard").wizard().on("change",
                function (w, v) {
                    var h = a("#btn_finish");
                    var k = a("#btn_next");
                    if (v.direction == "next") {
                        if (v.step == 1) {
                            dbTable = a("#dbtablegird").jfGridValue("name");
                            dbTablePk = a("#dbtablegird").jfGridValue("pk");
                            if (dbTable == "") {
                                b.alert.error("请选择数据表");
                                return false
                            }
                            a("#step-2").mkCustmerFormDesigner("updatedb", {
                                dbId: databaseLinkId,
                                dbTable: [{
                                    name: dbTable
                                }]
                            });
                            if (mapField[databaseLinkId + dbTable]) {
                                a("#queryDatetime").mkselectRefresh({
                                    data: mapField[databaseLinkId + dbTable]
                                })
                            } else {
                                if (!mapField[databaseLinkId + dbTable]) {
                                    b.httpAsync("GET", top.$.rootUrl + "/SystemModule/DatabaseTable/GetFieldList", {
                                        databaseLinkId: databaseLinkId,
                                        tableName: dbTable
                                    },
                                        function (i) {
                                            mapField[databaseLinkId + dbTable] = i;
                                            a("#queryDatetime").mkselectRefresh({
                                                data: i
                                            })
                                        })
                                }
                            }
                        } else {
                            if (v.step == 2) {
                                if (!a("#step-2").mkCustmerFormDesigner("valid")) {
                                    return false
                                }
                                var E = a("#step-2").mkCustmerFormDesigner("get");
                                queryAllComponts = [];
                                for (var x = 0,
                                    C = E.data.length; x < C; x++) {
                                    var u = E.data[x].componts;
                                    for (var A = 0,
                                        B = u.length; A < B; A++) {
                                        var z = u[A];
                                        if (z.type != "gridtable" && z.table != "" && z.field != "") {
                                            queryAllComponts.push(z);
                                            queryAllCompontMap[z.table + z.field] = z
                                        }
                                    }
                                }
                                if (queryAllComponts.length == 0) {
                                    b.alert.error("请设置表单字段！");
                                    return false
                                }
                                a("#treefieldRe").mkselectRefresh({
                                    data: queryAllComponts
                                });
                                var t = [];
                                var D = a("#col_gridtable").jfGridGet("rowdatas");
                                if (D.length > 0) {
                                    a.each(D,
                                        function (i, j) {
                                            if (queryAllCompontMap[j.id]) {
                                                t.push(j)
                                            }
                                        });
                                    a("#col_gridtable").jfGridSet("refreshdata", t)
                                }
                                if (t.length == 0) {
                                    a.each(queryAllComponts,
                                        function (i, j) {
                                            var l = {
                                                id: j.table + j.field,
                                                field: j.field,
                                                align: "left",
                                                width: 100
                                            };
                                            t.push(l)
                                        });
                                    a("#col_gridtable").jfGridSet("refreshdata", t)
                                }
                            } else {
                                if (v.step == 3) { } else {
                                    if (v.step == 4) {
                                        var y = a('[name="isViewTree"]:checked').val();
                                        if (y == "1") {
                                            var J = a("#treeDataSource").mkselectGet();
                                            if (J == "1") {
                                                var K = a("#treeDataSourceId").mkselectGet();
                                                if (K == "") {
                                                    b.alert.error("请选择数据源！");
                                                    return false
                                                }
                                            } else {
                                                var L = a("#treesql").val();
                                                if (L == "") {
                                                    b.alert.error("请填写sql语句！");
                                                    return false
                                                }
                                            }
                                            var F = a("#treefieldId").mkselectGet();
                                            if (F == "") {
                                                b.alert.error("请选择字段ID！");
                                                return false
                                            }
                                            var I = a("#treefieldParentId").mkselectGet();
                                            if (I == "") {
                                                b.alert.error("请选择父级字段！");
                                                return false
                                            }
                                            var H = a("#treefieldShow").mkselectGet();
                                            if (H == "") {
                                                b.alert.error("请选择显示字段！");
                                                return false
                                            }
                                            var G = a("#treefieldRe").mkselectGet();
                                            if (G == "") {
                                                b.alert.error("请选择关联字段！");
                                                return false
                                            }
                                        }
                                    } else {
                                        if (v.step == 5) {
                                            if (!a("#step-5").mkValidform()) {
                                                return false
                                            }
                                            d = {};
                                            d.databaseLinkId = databaseLinkId;
                                            d.dbTable = dbTable;
                                            d.dbTablePk = dbTablePk;
                                            var E = a("#step-2").mkCustmerFormDesigner("get");
                                            d.formData = JSON.stringify(E.data);
                                            var n = a("#query_girdtable").jfGridGet("rowdatas");
                                            var o = [];
                                            a.each(n,
                                                function (i, j) {
                                                    if (j.id) {
                                                        o.push(j)
                                                    }
                                                });
                                            var p = {
                                                width: a("#queryWidth").val(),
                                                height: a("#queryHeight").val(),
                                                isDate: a('[name="queryDatetime"]:checked').val(),
                                                DateField: a("#queryDatetime").mkselectGet(),
                                                fields: o
                                            };
                                            d.queryData = JSON.stringify(p);
                                            var s = [];
                                            a("#btnlist .lbtn.active").each(function () {
                                                var i = a(this).attr("data-value");
                                                s.push(i)
                                            });
                                            var r = [];
                                            a("#btnlistex .lbtn.active").each(function () {
                                                var j = a(this).text();
                                                var i = a(this).attr("data-value");
                                                r.push({
                                                    id: i,
                                                    name: j
                                                })
                                            });
                                            var m = {
                                                isPage: a('[name="isPage"]:checked').val(),
                                                fields: a("#col_gridtable").jfGridGet("rowdatas"),
                                                btns: s,
                                                btnexs: r,
                                                isTree: a('[name="isViewTree"]:checked').val(),
                                                treeSource: a("#treeDataSource").mkselectGet(),
                                            };
                                            if (m.isTree == "1") {
                                                if (m.treeSource == "1") {
                                                    m.treeSourceId = a("#treeDataSourceId").mkselectGet()
                                                } else {
                                                    m.treeSql = a("#treesql").val()
                                                }
                                                m.treefieldId = a("#treefieldId").mkselectGet();
                                                m.treeParentId = a("#treefieldParentId").mkselectGet();
                                                m.treefieldShow = a("#treefieldShow").mkselectGet();
                                                m.treefieldRe = a("#treefieldRe").mkselectGet()
                                            }
                                            d.colData = JSON.stringify(m);
                                            var q = a("#step-5").mkGetFormData();
                                            d.baseInfo = JSON.stringify(q);
                                            b.httpAsyncPost(top.$.rootUrl + "/CodeGeneratorModule/TemplatePC/LookGridEditCode", d,
                                                function (i) {
                                                    if (i.code == 200) {
                                                        a.each(i.data,
                                                            function (j, l) {
                                                                a("#" + j).html('<textarea name="SyntaxHighlighter" class="brush: c-sharp;"></textarea>');
                                                                a("#" + j + ' [name="SyntaxHighlighter"]').text(l)
                                                            });
                                                        SyntaxHighlighter.highlight()
                                                    }
                                                })
                                        } else {
                                            if (v.step == 6) {
                                                h.removeAttr("disabled");
                                                k.attr("disabled", "disabled")
                                            } else {
                                                h.attr("disabled", "disabled")
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        h.attr("disabled", "disabled");
                        k.removeAttr("disabled")
                    }
                });
            a("#dbId").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库",
                allowSearch: true,
                select: function (h) {
                    if (h.hasChildren) {
                        databaseLinkId = "";
                        a("#dbtablegird").jfGridSet("refreshdata", [])
                    } else {
                        if (dbId != h.id) {
                            databaseLinkId = h.id;
                            c.dbTableSearch()
                        }
                    }
                }
            });
            a("#btn_Search").on("click",
                function () {
                    var h = a("#txt_Keyword").val();
                    c.dbTableSearch({
                        tableName: h
                    })
                });
            a("#dbtablegird").jfGrid({
                url: top.$.rootUrl + "/SystemModule/DatabaseTable/GetList",
                headData: [{
                    label: "表名",
                    name: "name",
                    width: 300,
                    align: "left"
                },
                {
                    label: "记录数",
                    name: "sumrows",
                    width: 100,
                    align: "center",
                    formatter: function (h) {
                        return h + "条"
                    }
                },
                {
                    label: "使用大小",
                    name: "reserved",
                    width: 100,
                    align: "center"
                },
                {
                    label: "索引使用大小",
                    name: "index_size",
                    width: 120,
                    align: "center"
                },
                {
                    label: "说明",
                    name: "tdescription",
                    width: 350,
                    align: "left"
                }],
                mainId: "name",
                isSubGrid: true,
                subGridExpanded: function (i, h) {
                    a("#" + i).jfGrid({
                        url: top.$.rootUrl + "/SystemModule/DatabaseTable/GetFieldList",
                        headData: [{
                            label: "列名",
                            name: "f_column",
                            width: 300,
                            align: "left"
                        },
                        {
                            label: "数据类型",
                            name: "f_datatype",
                            width: 80,
                            align: "center"
                        },
                        {
                            label: "长度",
                            name: "f_length",
                            width: 57,
                            align: "center"
                        },
                        {
                            label: "允许空",
                            name: "f_isnullable",
                            width: 50,
                            align: "center",
                            formatter: function (j) {
                                return j == 1 ? '<i class="fa fa-toggle-on"></i>' : '<i class="fa fa-toggle-off"></i>'
                            }
                        },
                        {
                            label: "标识",
                            name: "f_identity",
                            width: 50,
                            align: "center",
                            formatter: function (j) {
                                return j == 1 ? '<i class="fa fa-toggle-on"></i>' : '<i class="fa fa-toggle-off"></i>'
                            }
                        },
                        {
                            label: "主键",
                            name: "f_key",
                            width: 50,
                            align: "center",
                            formatter: function (j) {
                                return j == 1 ? '<i class="fa fa-toggle-on"></i>' : '<i class="fa fa-toggle-off"></i>'
                            }
                        },
                        {
                            label: "说明",
                            name: "f_remark",
                            width: 200,
                            align: "left"
                        }]
                    });
                    a("#" + i).jfGridSet("reload", {
                        databaseLinkId: databaseLinkId,
                        tableName: h.name
                    })
                }
            });
            a("#step-2").mkCustmerFormDesigner("init", {
                components: ["text", "radio", "checkbox", "select", "datetime","tabpage"]
            });
            a("#queryDatetime").mkselect({
                value: "f_column",
                text: "f_column",
                title: "f_remark",
                allowSearch: true
            });
            a("#query_girdtable").jfGrid({
                headData: [{
                    label: "",
                    name: "btn1",
                    width: 50,
                    align: "center",
                    formatter: function (k, j, i, h) {
                        h.on("click",
                            function () {
                                var m = parseInt(h.attr("rowindex"));
                                var l = a("#query_girdtable").jfGridSet("moveUp", m);
                                return false
                            });
                        return '<span class="label label-info" style="cursor: pointer;">上移</span>'
                    }
                },
                {
                    label: "",
                    name: "btn2",
                    width: 50,
                    align: "center",
                    formatter: function (k, j, i, h) {
                        h.on("click",
                            function () {
                                var m = parseInt(h.attr("rowindex"));
                                var l = a("#query_girdtable").jfGridSet("moveDown", m);
                                return false
                            });
                        return '<span class="label label-success" style="cursor: pointer;">下移</span>'
                    }
                },
                {
                    label: "字段项名称",
                    name: "compontId",
                    width: 300,
                    align: "left",
                    formatter: function (k, j, i, h) {
                        if (queryAllCompontMap[j.id]) {
                            return queryAllCompontMap[j.id].title
                        } else {
                            return ""
                        }
                    },
                    edit: {
                        type: "select",
                        init: function (i, h) {
                            h.mkselectRefresh({
                                data: queryAllComponts
                            })
                        },
                        op: {
                            value: "id",
                            text: "title",
                            title: "title",
                            allowSearch: true
                        },
                        change: function (i, j, h) {
                            if (h != null) {
                                i.id = h.table + h.field
                            } else {
                                i.id = ""
                            }
                        }
                    }
                },
                {
                    label: "所占行比例",
                    name: "portion",
                    width: 150,
                    align: "left",
                    edit: {
                        type: "select",
                        op: {
                            placeholder: false,
                            data: [{
                                id: "1",
                                text: "1/1"
                            },
                            {
                                id: "2",
                                text: "1/2"
                            },
                            {
                                id: "3",
                                text: "1/3"
                            },
                            {
                                id: "4",
                                text: "1/4"
                            },
                            {
                                id: "6",
                                text: "1/6"
                            }]
                        }
                    },
                    formatter: function (k, j, i, h) {
                        if (!!k) {
                            return "1/" + k
                        } else {
                            return ""
                        }
                    }
                }],
                onAddRow: function (h, i) {
                    h.portion = "1"
                },
                mainId: "id",
                isEdit: true,
                isMultiselect: true
            });
            a("#treesetting").mkscroll();
            a("#btnlist>div").on("click",
                function () {
                    var h = a(this);
                    if (h.hasClass("active")) {
                        h.removeClass("active")
                    } else {
                        h.addClass("active")
                    }
                    h = null
                });
            a("#btnlistex").delegate("div", "click",
                function () {
                    a(this).remove()
                });
            a("#btnlistex_add").on("click",
                function () {
                    b.layerForm({
                        id: "AddBtnForm",
                        title: "添加按钮",
                        url: top.$.rootUrl + "/CodeGeneratorModule/TemplatePC/AddBtnForm",
                        height: 230,
                        width: 400,
                        callBack: function (h) {
                            return top[h].acceptClick(function (i) {
                                a("#btnlistex").append('<div class="lbtn active" data-value="' + i.btnId + '" ><i class="fa fa-plus"></i>&nbsp;' + i.btnName + "</div>")
                            })
                        }
                    })
                });
            a("#treefieldId").mkselect({
                title: "text",
                text: "text",
                value: "value",
                allowSearch: true,
                select: function (h) {
                    if (h) { }
                }
            });
            a("#treefieldParentId").mkselect({
                title: "text",
                text: "text",
                value: "value",
                allowSearch: true,
                select: function (h) {
                    if (h) { }
                }
            });
            a("#treefieldShow").mkselect({
                title: "text",
                text: "text",
                value: "value",
                allowSearch: true,
                select: function (h) {
                    if (h) { }
                }
            });
            a("#treefieldRe").mkselect({
                title: "title",
                text: "title",
                value: "field",
                allowSearch: true
            });
            a("#treeDataSource").mkselect({
                data: [{
                    id: "1",
                    text: "数据源"
                },
                {
                    id: "2",
                    text: "sql语句"
                }],
                placeholder: false,
                select: function (h) {
                    if (h) {
                        if (h.id == "1") {
                            a(".DataSourceType1").hide();
                            a(".DataSourceType2").show()
                        } else {
                            a(".DataSourceType1").show();
                            a(".DataSourceType2").hide()
                        }
                        a("#treefieldId").mkselectRefresh({
                            data: []
                        });
                        a("#treefieldParentId").mkselectRefresh({
                            data: []
                        });
                        a("#treefieldShow").mkselectRefresh({
                            data: []
                        })
                    }
                }
            });
            a("#treeDataSourceId").mkselect({
                allowSearch: true,
                url: top.$.rootUrl + "/SystemModule/DataSource/GetList",
                value: "F_Code",
                text: "F_Name",
                title: "F_Name",
                select: function (h) {
                    if (!!h) {
                        b.httpAsync("GET", top.$.rootUrl + "/SystemModule/DataSource/GetDataColName", {
                            code: h.F_Code
                        },
                            function (j) {
                                var k = [];
                                for (var m = 0,
                                    o = j.length; m < o; m++) {
                                    var n = j[m];
                                    var p = {
                                        value: n,
                                        text: n
                                    };
                                    k.push(p)
                                }
                                a("#treefieldId").mkselectRefresh({
                                    data: k
                                });
                                a("#treefieldParentId").mkselectRefresh({
                                    data: k
                                });
                                a("#treefieldShow").mkselectRefresh({
                                    data: k
                                })
                            })
                    } else { }
                }
            });
            a('[name="isViewTree"]').on("click",
                function () {
                    var h = a(this).val();
                    if (h == 1) {
                        a(".treesetting").show();
                        a("#treeDataSource").mkselectSet("2")
                    } else {
                        a(".treesetting").hide();
                        a("#treeDataSource").mkselectSet("")
                    }
                });
            a("#treesql_set").on("click",
                function () {
                    a("#treefieldId").mkselectRefresh({
                        data: []
                    });
                    a("#treefieldParentId").mkselectRefresh({
                        data: []
                    });
                    a("#treefieldShow").mkselectRefresh({
                        data: []
                    });
                    var h = a("#treesql").val();
                    b.httpAsync("GET", top.$.rootUrl + "/SystemModule/DatabaseTable/GetSqlColName", {
                        databaseLinkId: databaseLinkId,
                        strSql: h
                    },
                        function (j) {
                            var k = [];
                            for (var m = 0,
                                o = j.length; m < o; m++) {
                                var n = j[m];
                                var p = {
                                    value: n,
                                    text: n
                                };
                                k.push(p)
                            }
                            a("#treefieldId").mkselectRefresh({
                                data: k
                            });
                            a("#treefieldParentId").mkselectRefresh({
                                data: k
                            });
                            a("#treefieldShow").mkselectRefresh({
                                data: k
                            })
                        })
                });
            a("#col_gridtable").jfGrid({
                headData: [{
                    label: "",
                    name: "btn1",
                    width: 50,
                    align: "center",
                    formatter: function (k, j, i, h) {
                        h.on("click",
                            function () {
                                var m = parseInt(h.attr("rowindex"));
                                var l = a("#col_gridtable").jfGridSet("moveUp", m);
                                return false
                            });
                        return '<span class="label label-info" style="cursor: pointer;">上移</span>'
                    }
                },
                {
                    label: "",
                    name: "btn2",
                    width: 50,
                    align: "center",
                    formatter: function (k, j, i, h) {
                        h.on("click",
                            function () {
                                var m = parseInt(h.attr("rowindex"));
                                var l = a("#col_gridtable").jfGridSet("moveDown", m);
                                return false
                            });
                        return '<span class="label label-success" style="cursor: pointer;">下移</span>'
                    }
                },
                {
                    label: "列名",
                    name: "field",
                    width: 300,
                    align: "left",
                    formatter: function (k, j, i, h) {
                        if (queryAllCompontMap[j.id]) {
                            j.fieldName = queryAllCompontMap[j.id].title;
                            return queryAllCompontMap[j.id].title
                        } else {
                            return ""
                        }
                    },
                    edit: {
                        type: "select",
                        init: function (i, h) {
                            h.mkselectRefresh({
                                data: queryAllComponts
                            })
                        },
                        op: {
                            value: "field",
                            text: "title",
                            title: "title",
                            allowSearch: true
                        },
                        change: function (i, j, h) {
                            if (h != null) {
                                i.id = h.table + h.field
                            } else {
                                i.id = ""
                            }
                        }
                    }
                },
                {
                    label: "对齐",
                    name: "align",
                    width: 80,
                    align: "left",
                    edit: {
                        type: "select",
                        op: {
                            placeholder: false,
                            data: [{
                                id: "left",
                                text: "靠左"
                            },
                            {
                                id: "center",
                                text: "居中"
                            },
                            {
                                id: "right",
                                text: "靠右"
                            }]
                        }
                    }
                },
                {
                    label: "宽度",
                    name: "width",
                    width: 80,
                    align: "left",
                    edit: {
                        type: "input"
                    }
                }],
                isEdit: true,
                isMultiselect: true,
                onAddRow: function (h, i) {
                    h.align = "left";
                    h.width = 100
                },
            });
            var g = b.clientdata.get(["userinfo"]);
            a("#createUser").val(g.realName);
            a("#outputArea").mkDataItemSelect({
                code: "outputArea"
            });
            a("#mappingDirectory").val(e + a("#_mappingDirectory").val());
            a("#serviceDirectory").val(e + a("#_serviceDirectory").val());
            a("#webDirectory").val(e + a("#_webDirectory").val());
            a("#nav_tabs").mkFormTabEx();
            a("#F_ParentId").mkselect({
                url: top.$.rootUrl + "/SystemModule/Module/GetExpendModuleTree",
                type: "tree",
                maxHeight: 280,
                allowSearch: true
            });
            a("#selectIcon").on("click",
                function () {
                    b.layerForm({
                        id: "iconForm",
                        title: "选择图标",
                        url: top.$.rootUrl + "/Utility/Icon",
                        height: 700,
                        width: 1000,
                        btn: null,
                        maxmin: true,
                        end: function () {
                            if (top._changjieSelectIcon != "") {
                                a("#F_Icon").val(top._changjieSelectIcon)
                            }
                        }
                    })
                });
            a("#btn_finish").on("click", c.save)
        },
        dbTableSearch: function (g) {
            g = g || {};
            g.databaseLinkId = databaseLinkId;
            a("#dbtablegird").jfGridSet("reload", g)
        },
        save: function () {
            if (!a("#step-7").mkValidform()) {
                return false
            }
            var g = a("#step-7").mkGetFormData();
            g.F_EnabledMark = 1;
            d.moduleEntityJson = JSON.stringify(g);
            a.mkSaveForm(top.$.rootUrl + "/CodeGeneratorModule/TemplatePC/CreateGridEditCode", d,
                function (h) { },
                true)
        }
    };
    c.init()
};