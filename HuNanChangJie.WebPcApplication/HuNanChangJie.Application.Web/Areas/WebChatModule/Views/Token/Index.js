﻿/*
 * 日 期：2017.04.11
 * 描 述：微信企业号设置	
 */
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            $('#btn_RevisePassword').on('click', function () {
                if (!$('#form').mkValidform()) {
                    return false;
                }
                var postData = $('#form').mkGetFormData();
                $.mkSaveForm(top.$.rootUrl + '/WebChatModule/Token/SaveForm', postData, null, true);
            });
        }
    };
    page.init();
}