﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.NewWorkFlow.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 14:46
    /// 描 述：222
    /// </summary>
    public class NWFProcessController : MvcControllerBase
    {
        private NWFProcessIBLL nWFProcessIBLL = new NWFProcessBLL();
        private NWFTaskIBLL wfTaskIBLL = new NWFTaskBLL();
        private NWFSchemeIBLL wfSchemeIBLL = new NWFSchemeBLL();
        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }

        /// <summary>
        /// 创建流程
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ReleaseForm()
        {
            return View();
        }

        public ActionResult CreateForm()
        {
            return View();
        }
        /// <summary>
        /// 流程处理页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NWFContainerForm()
        {
            return View();
        }

        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = nWFProcessIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var NWF_ProcessData = nWFProcessIBLL.GetNWF_ProcessEntity( keyValue );
            var jsonData = new {
                NWF_Process = NWF_ProcessData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            nWFProcessIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            NWF_ProcessEntity entity = strEntity.ToObject<NWF_ProcessEntity>();
            nWFProcessIBLL.SaveEntity(keyValue,entity);
            return Success("保存成功！");
        }
        #endregion


        #region 扩展获取数据功能
        /// <summary>
        /// 获取我的流程信息列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询条件</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTaskPageList(string pagination, string queryJson, string categoryId)
        {

            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            IEnumerable<NWF_ProcessEntity> list = new List<NWF_ProcessEntity>();

            UserInfo userInfo = LoginUserInfo.Get();
            switch (categoryId)
            {
                case "1":
                    list = nWFProcessIBLL.GetMyPageList(userInfo.userId, paginationobj, queryJson);
                    break;
                case "2":
                    list = wfTaskIBLL.GetActiveList(userInfo, paginationobj, queryJson);
                    break;
                case "3":
                    list = wfTaskIBLL.GetHasList(userInfo.userId, paginationobj, queryJson);
                    break;
            }

            var jsonData = new
            {
                rows = list,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
            return null;
        }



        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSchemeByCode( string code)
        {
          var wfSchemeInfoEntity=  wfSchemeIBLL.GetWfSchemeInfoEntityByCode(code);
           var  schemeEntity= wfSchemeIBLL.GetNWF_SchemeEntity(wfSchemeInfoEntity.F_SchemeId);

            return Success(schemeEntity);

        }

        #endregion


        #region 发起流程

        /// <summary>
        /// 获取自定义流程列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMyInfoList()
        {
            UserInfo userInfo = LoginUserInfo.Get();
            var data = wfSchemeIBLL.GetMyInfoList(userInfo);
            return JsonResult(data);
        }

        #endregion
    }
}
