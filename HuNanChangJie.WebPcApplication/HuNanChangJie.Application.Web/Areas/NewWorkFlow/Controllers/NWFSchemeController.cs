﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.NewWorkFlow.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:54
    /// 描 述：NWFScheme
    /// </summary>
    public class NWFSchemeController : MvcControllerBase
    {
        private NWFSchemeIBLL wfSchemeIBLL = new NWFSchemeBLL();
         
        #region  视图功能
        /// <summary>
        /// 流程模板管理
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 流程模板设计
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        /// <summary>
        /// 流程模板设计历史记录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult HistoryForm()
        {
            return View();
        }

        /// <summary>
        /// 预览流程模板
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PreviewForm()
        {
            return View();
        }

        /// <summary>
        /// 节点信息设置
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NodeForm()
        {
            return View();
        }



        #region  审核人员添加
        /// <summary>
        /// 添加岗位
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PostForm()
        {
            return View();
        }
        /// <summary>
        /// 添加角色
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RoleForm()
        {
            return View();
        }
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UserForm()
        {
            return View();
        }

        public ActionResult AuditorNodeForm(string layerId)
        {
            return View();
        }
        public ActionResult AuditorFieldForm(string layerId)
        {
            return View();
        }


        public ActionResult ButtonForm(string id)
        {
            return View();
        }

        public ActionResult LevelForm(string id)
        {
            return View();
        }

        #endregion

        #region  表单添加
        /// <summary>
        /// 表单添加
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WorkformForm()
        {
            return View();
        }
        #endregion

        #region  表单权限设置
        /// <summary>
        /// 权限添加
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AuthorizeForm()
        {
            return View();
        }
        #endregion

        #region  条件字段
        /// <summary>
        /// 条件字段添加
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ConditionFieldForm()
        {
            return View();
        }
        /// <summary>
        /// 字段选择
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FieldSelectForm()
        {
            return View();
        }

        #endregion


        /// <summary>
        /// 线段信息设置
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LineForm()
        {
            return View();
        }


        #endregion


        #region  获取数据

        public ActionResult GetScheme(string schemeId)
        {
            var data = wfSchemeIBLL.GetNWF_SchemeEntity(schemeId);
            return JsonResult(data);
        }


        /// <summary>
        /// 获取模板分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="schemeInfoId"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSchemePageList(string pagination, string schemeInfoId)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            IEnumerable<NWF_SchemeEntity> data = wfSchemeIBLL.GetSchemePageList(paginationobj, schemeInfoId);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }

        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">关键字</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSchemeInfoPageList(string pagination, string keyword, string category)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = wfSchemeIBLL.GetSchemeInfoPageList(paginationobj, keyword, category);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }




        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = wfSchemeIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string code)
        {
            NWF_SchemeInfoEntity schemeInfoEntity = wfSchemeIBLL.GetWfSchemeInfoEntityByCode(code);
            if (schemeInfoEntity == null)
            {
                return JsonResult(new { });
            }

            NWF_SchemeEntity schemeEntity = wfSchemeIBLL.GetNWF_SchemeEntity(schemeInfoEntity.F_SchemeId);
            var wfSchemeAuthorizeList = wfSchemeIBLL.GetWfSchemeAuthorizeList(schemeInfoEntity.F_Id);
            var jsonData = new
            {
                info = schemeInfoEntity,
                scheme = schemeEntity,
                authList = wfSchemeAuthorizeList
            };
            return JsonResult(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            wfSchemeIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string schemeInfo, string shcemeAuthorize, string scheme, int type)
        {
            NWF_SchemeInfoEntity schemeInfoEntity = schemeInfo.ToObject<NWF_SchemeInfoEntity>();
            List<NWF_SchemeAuthEntity> wfSchemeAuthorizeList = shcemeAuthorize.ToObject<List<NWF_SchemeAuthEntity>>();
            NWF_SchemeEntity schemeEntity = new NWF_SchemeEntity();
            schemeEntity.F_Content = scheme;
            schemeEntity.F_Type = type;

            wfSchemeIBLL.SaveEntity(keyValue, schemeInfoEntity, schemeEntity, wfSchemeAuthorizeList);
            return Success("保存成功！");
        }
        #endregion




        /// <summary>
        /// 启用/停用表单
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="state">状态1启用0禁用</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult UpDateSate(string keyValue, int state)
        {
            wfSchemeIBLL.UpdateState(keyValue, state);
            return Success((state == 1 ? "启用" : "禁用") + "成功！");
        }
        /// <summary>
        /// 更新表单模板版本
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="state">状态1启用0禁用</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult UpdateScheme(string schemeInfoId, string schemeId)
        {
            wfSchemeIBLL.UpdateScheme(schemeInfoId, schemeId);
            return Success("更新成功！");
        }
    }
}
