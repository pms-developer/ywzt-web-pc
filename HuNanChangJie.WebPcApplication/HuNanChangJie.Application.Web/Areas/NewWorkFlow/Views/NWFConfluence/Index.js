﻿/* 
 * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-03-16 14:41
 * 描  述：流程会签统计
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/NewWorkFlow/NWFConfluence/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').GridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/NewWorkFlow/NWFConfluence/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/NewWorkFlow/NWFConfluence/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/NewWorkFlow/NWFConfluence/GetPageList',
                headData: [
                    { headerName: "流程进程主键", field: "F_ProcessId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "会签节点ID", field: "F_NodeId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "上一节点ID ", field: "F_FormNodeId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "状态1同意0不同意", field: "F_State", width: 100, cellStyle: { 'text-align': 'left' } },
                ],
                mainId:'F_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
