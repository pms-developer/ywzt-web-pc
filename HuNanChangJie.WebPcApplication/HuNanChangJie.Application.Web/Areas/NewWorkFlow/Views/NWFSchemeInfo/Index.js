﻿/* 
 * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-03-16 15:57
 * 描  述：NWFSchemeInfo
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/NewWorkFlow/NWFSchemeInfo/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').GridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/NewWorkFlow/NWFSchemeInfo/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/NewWorkFlow/NWFSchemeInfo/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/NewWorkFlow/NWFSchemeInfo/GetPageList',
                headData: [
                    { headerName: "流程编码", field: "F_Code", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程模板名称", field: "F_Name", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程分类", field: "F_Category", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程模板ID", field: "F_SchemeId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "是否有效", field: "F_EnabledMark", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "是否在我的任务允许发起 1允许 2不允许", field: "F_Mark", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "是否在App上允许发起 1允许 2不允许", field: "F_IsInApp", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "备注", field: "F_Description", width: 100, cellStyle: { 'text-align': 'left' } },
                ],
                mainId:'F_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
