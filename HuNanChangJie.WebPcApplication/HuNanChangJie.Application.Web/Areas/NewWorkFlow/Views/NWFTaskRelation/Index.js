﻿/* 
 * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-03-16 16:05
 * 描  述：NWFTaskRelation
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/NewWorkFlow/NWFTaskRelation/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').GridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/NewWorkFlow/NWFTaskRelation/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/NewWorkFlow/NWFTaskRelation/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/NewWorkFlow/NWFTaskRelation/GetPageList',
                headData: [
                    { headerName: "任务主键", field: "F_TaskId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务执行人员主键", field: "F_UserId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "标记0需要处理1暂时不需要处理", field: "F_Mark", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "处理结果1同意2不同意", field: "F_Result", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "F_Sort", field: "F_Sort", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "F_Time", field: "F_Time", width: 100, cellStyle: { 'text-align': 'left' } },
                ],
                mainId:'F_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
