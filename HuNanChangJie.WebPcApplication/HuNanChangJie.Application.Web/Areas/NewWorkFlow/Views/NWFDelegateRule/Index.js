﻿/* 
 * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-03-16 14:43
 * 描  述：111
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/NewWorkFlow/NWFDelegateRule/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').GridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/NewWorkFlow/NWFDelegateRule/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/NewWorkFlow/NWFDelegateRule/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/NewWorkFlow/NWFDelegateRule/GetPageList',
                headData: [
                    { headerName: "被委托人Id", field: "F_ToUserId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "被委托人名称", field: "F_ToUserName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "委托开始时间", field: "F_BeginDate", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "委托结束时间", field: "F_EndDate", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "委托人Id", field: "Creation_Id", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "委托人名称", field: "CreationName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "备注", field: "F_Description", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "有效标志1有效 0 无效", field: "F_EnabledMark", width: 100, cellStyle: { 'text-align': 'left' } },
                ],
                mainId:'F_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
