﻿var id = request("id");
var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $("#next").mkselect({
                placeholder: false,
                data: [{
                        id: "1",
                        text: "不能手动设置"
                    },
                    {
                        id: "2",
                        text: "能手动设置"
                    }]
            }).mkselectSet("1");
            $("#isHide").mkselect({
                placeholder: false,
                data: [
                    {
                        id: "1",
                        text: "是"
                    },
                    {
                        id: "2",
                        text: "否"
                    }
                ]
            }).mkselectSet("2");
        },
        initData: function () {
            if (id) {
                var d = top.layer_NodeForm.btnList;
                $.each(d,
                    function(e, f) {
                        if (f.id == id) {
                            $("#form").mkSetFormData(f);
                            if (id == "1" || id == "2") {
                                $("#name").attr("readonly", "readonly");
                                $("#code").attr("readonly", "readonly");
                            }
                            return false;
                        }
                    });
            }
        }
    };
    acceptClick = function (d) {
        if (!$("#form").mkValidform()) {
            return false;
        }
        var e = $("#form").mkGetFormData();
        e.id = id || Changjie.newGuid();
        d(e);
        return true;
    };
    page.init();
};