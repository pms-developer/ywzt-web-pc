﻿/*
 * 日 期：2017.04.05
 * 描 述：工作流模板预览	
 */
var schemeId = request('schemeId');

var currentNode;
var currentLine;
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            // 设计页面初始化
            $('#flow').mkworkflow({
                isPreview:true,
                openNode: function (node) {
                    currentNode = node;
                    if (node.type != 'endround') {
                        Changjie.layerForm({
                            id: 'NodeForm',
                            title: '节点信息设置【' + node.name + '】',
                            url: top.$.rootUrl + '/NewWorkFlow/NWfScheme/NodeForm?layerId=layer_PreviewForm&isPreview=1',
                            width: 700,
                            height: 500,
                            btn: null
                        });
                    }
                }
            });
            
            if (!!schemeId) {
                $.mkSetForm(top.$.rootUrl + '/NewWorkFlow/NWfScheme/GetScheme?schemeId=' + schemeId, function (res) {
                    
                    var shceme = JSON.parse(res.F_Content);

                    $('#flow').mkworkflowSet('set', { data: shceme });
                });
            }
        }
    };

    page.init();
}