﻿var acceptClick;
var auditorName = "";
var bootstrap = function (a, b) {
    var c = {
        init: function () {
            c.bind()
        },
        bind: function () {
            a("#auditorId").mkselect({
                data: [{
                        id: "1",
                        text: "上一级"
                    },
                    {
                        id: "2",
                        text: "上二级"
                    },
                    {
                        id: "3",
                        text: "上三级"
                    },
                    {
                        id: "4",
                        text: "上四级"
                    },
                    {
                        id: "5",
                        text: "上五级"
                    },
                    {
                        id: "6",
                        text: "下一级"
                    },
                    {
                        id: "7",
                        text: "下二级"
                    },
                    {
                        id: "8",
                        text: "下三级"
                    },
                    {
                        id: "9",
                        text: "下四级"
                    },
                    {
                        id: "10",
                        text: "下五级"
                    }],
                select: function (d) {
                    if (d) {
                        auditorName = d.text
                    } else {
                        auditorName = ""
                    }
                },
            })
        }
    };
    acceptClick = function (d) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var e = a("#form").mkGetFormData();
        e.auditorName = auditorName;
        e.type = "4";
        d(e);
        return true
    };
    c.init()
};