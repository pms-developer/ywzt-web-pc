﻿
/*
 * 日 期：2019.03.05
 * 描 述：工作流模板设计	
 */
var keyValue = request("keyValue");
var categoryId = request("categoryId");
var shcemeCode = request("shcemeCode");
var currentNode;  // 当前设置节点
var nodeList;
var fromNode;
var currentLine; // 当前设置线条
var schemeAuthorizes = []; // 模板权限人员
var authorizeType = 1;// 模板权限类型


var bootstrap = function ($, Changjie) {
    "use strict";
    function b(id) {
        var res = false;
        for (var i = 0, l = schemeAuthorizes.length; i < l; i++) {
            if (schemeAuthorizes[i].F_ObjectId == id) {
                Changjie.alert.warning("重复添加信息");
                res = true;
                break;
            }
        }
        return res;
    }

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        /*绑定事件和初始化控件*/
        bind: function () {
            // 加载导向
            $("#wizard").wizard().on("change",
                function (e, data) {
                    var $finish = $("#btn_finish");
                    var $next = $("#btn_next");
                    if (data.direction == "next") {
                        if (data.step == 1) {
                            if (!$("#step-1").mkValidform()) {
                                return false;
                            }
                        } else {
                            if (data.step == 2) {
                                if (authorizeType != 1) {
                                    if (schemeAuthorizes.length == 0) {
                                        Changjie.alert.error("请添加权限人员信息");
                                        return false;
                                    }
                                }
                                $finish.removeAttr("disabled");
                                $next.attr("disabled", "disabled");
                            } else {
                                $finish.attr("disabled", "disabled");
                            }
                        }
                    } else {
                        $finish.attr("disabled", "disabled");
                        $next.removeAttr("disabled");
                    }
                });
            $("#F_Category").mkDataItemSelect({
                code: "FlowSort"
            });
            $("#F_Category").mkselectSet(categoryId);

            $('input[name="closeDoType"]').on("click",
                function () {
                    var $closeDoType = $(this).val();
                    $(".operationDiv").hide();
                    $("#" + $closeDoType).show();
                });
            $("#F_CloseDoDbId").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库",
                allowSearch: true
            });
            //权限设置
            $('input[name="authorizeType"]').on("click",
                function () {
                    var e = $(this);
                    var f = e.val();
                    authorizeType = f;
                    if (f == "1") {
                        $("#shcemeAuthorizeBg").show();
                    } else {
                        $("#shcemeAuthorizeBg").hide();
                    }
                });
            $("#authorize_girdtable").jfGrid({
                headData: [
                    {
                        label: "类型",
                        name: "F_ObjType",
                        width: 100,
                        align: "center",
                        formatter: function (e) {
                            switch (parseInt(e)) {
                                case 1:
                                    return "岗位";
                                    break;
                                case 2:
                                    return "角色";
                                    break;
                                case 3:
                                    return "用户";
                                    break;
                            }
                        }
                    },
                    {
                        label: "名称",
                        name: "F_ObjName",
                        width: 700,
                        align: "left"
                    }
                ]
            });
            $("#post_authorize").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuthorizePostForm",
                        title: "添加岗位",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/PostForm?flag=1",
                        width: 400,
                        height: 300,
                        callBack: function (e) {
                            return top[e].acceptClick(function (g) {
                                if (!b(g.auditorId)) {
                                    var $authorpost = {};
                                    $authorpost.F_Id = Changjie.newGuid();
                                    $authorpost.F_ObjName = g.auditorName;
                                    $authorpost.F_ObjId = g.auditorId;
                                    $authorpost.F_ObjType = g.type;
                                    schemeAuthorizes.push($authorpost);
                                    $("#authorize_girdtable").jfGridSet("refreshdata",
                                        {
                                            rowdatas: schemeAuthorizes
                                        });
                                }
                            });
                        }
                    });
                });
            $("#role_authorize").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuthorizeRoleForm",
                        title: "添加角色",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/RoleForm?flag=1",
                        width: 400,
                        height: 300,
                        callBack: function (e) {
                            return top[e].acceptClick(function (g) {
                                if (!b(g.auditorId)) {
                                    var $authorRole = {};
                                    $authorRole.F_Id = Changjie.newGuid();
                                    $authorRole.F_ObjName = g.auditorName;
                                    $authorRole.F_ObjId = g.auditorId;
                                    $authorRole.F_ObjType = g.type;
                                    schemeAuthorizes.push($authorRole);
                                    $("#authorize_girdtable").jfGridSet("refreshdata",
                                        {
                                            rowdatas: schemeAuthorizes
                                        });
                                }
                            });
                        }
                    });
                });
            $("#user_authorize").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuthorizeUserForm",
                        title: "添加人员",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/UserForm",
                        width: 400,
                        height: 300,
                        callBack: function (e) {
                            return top[e].acceptClick(function (g) {
                                if (!b(g.auditorId)) {
                                    var $authorUser = {};
                                    $authorUser.F_Id = Changjie.newGuid();
                                    $authorUser.F_ObjName = g.auditorName;
                                    $authorUser.F_ObjId = g.auditorId;
                                    $authorUser.F_ObjType = g.type;
                                    schemeAuthorizes.push($authorUser);
                                    $("#authorize_girdtable").jfGridSet("refreshdata",
                                        {
                                            rowdatas: schemeAuthorizes
                                        });
                                }
                            });
                        }
                    });
                });
            $("#delete_authorize").on("click",
                function () {
                    var e = $("#authorize_girdtable").jfGridValue("F_Id");
                    if (Changjie.checkrow(e)) {
                        Changjie.layerConfirm("是否确认删除该项！",
                            function (j, g) {
                                if (j) {
                                    for (var f = 0, h = schemeAuthorizes.length; f < h; f++) {
                                        if (schemeAuthorizes[f].F_Id == e) {
                                            schemeAuthorizes.splice(f, 1);
                                            $("#authorize_girdtable").jfGridSet("refreshdata",
                                                {
                                                    rowdatas: schemeAuthorizes
                                                });
                                            break;
                                        }
                                    }
                                    top.layer.close(g);
                                }
                            });
                    }
                });
            $("#step-3").mkworkflow({
                openNode: function ($this, nodelist) {
                    currentNode = $this;
                    nodeList = nodelist;
                    if ($this.type != "endround") {
                        Changjie.layerForm({
                            id: "NodeForm",
                            title: "节点信息设置【" + $this.name + "】",
                            url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/NodeForm?layerId=layer_Form",
                            width: 700,
                            height: 550,
                            callBack: function (g) {
                                return top[g].acceptClick(function () {
                                    $("#step-3").mkworkflowSet("updateNodeName",
                                        {
                                            nodeId: currentNode.id
                                        });
                                });
                            }
                        });
                    }
                },
                openLine: function ($this, formnode) {
                    currentLine = $this;
                    fromNode = formnode;
                    Changjie.layerForm({
                        id: "LineForm",
                        title: "线条信息设置",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/LineForm?layerId=layer_Form",
                        width: 700,
                        height: 550,
                        callBack: function (g) {
                            return top[g].acceptClick(function () {
                                $("#step-3").mkworkflowSet("updateLineName",
                                    {
                                        lineId: currentLine.id
                                    });
                            });
                        }
                    });
                }
            });
            $("#btn_draft").on("click", page.draftsave);
            $("#btn_finish").on("click", page.save);
        },
        initData: function () {
            if (!!shcemeCode) {
                $.mkSetForm(top.$.rootUrl + "/NewWorkFlow/NWFScheme/GetFormData?code=" + shcemeCode,
                    function (data) {
                        $("#step-1").mkSetFormData(data.info);
                        var $fcontent = JSON.parse(data.scheme.F_Content);
                        $fcontent.closeDo = $fcontent.closeDo || {};
                        $("#step-1").mkSetFormData($fcontent.closeDo);
                        $("#step-3").mkworkflowSet("set",
                            {
                                data: $fcontent
                            });
                        if (data.authList.length > 0 && data.authList[0].F_ObjType != 4) {
                            $("#authorizeType2").trigger("click");
                            schemeAuthorizes = data.authList;
                            $("#authorize_girdtable").jfGridSet("refreshdata",
                                {
                                    rowdatas: schemeAuthorizes
                                });
                            authorizeType = 2;
                        }
                    });
            }
        },
        draftsave: function () {
            var e = $("#step-1").mkGetFormData(keyValue);
            var g = $("#step-3").mkworkflowGet();
            if (e.F_CloseDoSql == "&nbsp;") {
                e.F_CloseDoSql = ""
            }
            if (e.F_CloseDoIocName == "&nbsp;") {
                e.F_CloseDoIocName = ""
            }
            if (e.F_CloseDoInterface == "&nbsp;") {
                e.F_CloseDoInterface = ""
            }
            g.closeDo = {
                F_CloseDoType: e.F_CloseDoType,
                F_CloseDoDbId: e.F_CloseDoDbId,
                F_CloseDoSql: e.F_CloseDoSql,
                F_CloseDoIocName: e.F_CloseDoIocName,
                F_CloseDoInterface: e.F_CloseDoInterface,
            };
            if (authorizeType == 1) {
                schemeAuthorizes = [];
            }
            var postData = {
                schemeInfo: JSON.stringify(e),
                scheme: JSON.stringify(g),
                shcemeAuth: JSON.stringify(schemeAuthorizes),
                type: 2
            };
            $.mkSaveForm(top.$.rootUrl + "/NewWorkFlow/NWFScheme/SaveForm?keyValue=" + keyValue,
                postData,
                function (h) {
                    Changjie.frameTab.currentIframe().refreshGirdData();
                });
        },
        save: function () {
            if (!$("#step-1").mkValidform()) {
                return false;
            }
            var e = $("#step-1").mkGetFormData(keyValue);
            var g = $("#step-3").mkworkflowGet();
            if (authorizeType == 1) {
                schemeAuthorizes = [];
                schemeAuthorizes.push({
                    F_Id: Changjie.newGuid(),
                    F_ObjType: 4
                });
            }
            if (e.F_CloseDoSql == "&nbsp;") {
                e.F_CloseDoSql = "";
            }
            if (e.F_CloseDoIocName == "&nbsp;") {
                e.F_CloseDoIocName = "";
            }
            if (e.F_CloseDoInterface == "&nbsp;") {
                e.F_CloseDoInterface = "";
            }
            g.closeDo = {
                F_CloseDoType: e.F_CloseDoType,
                F_CloseDoDbId: e.F_CloseDoDbId,
                F_CloseDoSql: e.F_CloseDoSql,
                F_CloseDoIocName: e.F_CloseDoIocName,
                F_CloseDoInterface: e.F_CloseDoInterface,
            };
            var postData = {
                schemeInfo: JSON.stringify(e),
                scheme: JSON.stringify(g),
                shcemeAuth: JSON.stringify(schemeAuthorizes),
                type: 1
            };
            $.mkSaveForm(top.$.rootUrl + "/NewWorkFlow/NWFScheme/SaveForm?keyValue=" + keyValue,
                postData,
                function (h) {
                    Changjie.frameTab.currentIframe().refreshGirdData();
                });
        }
    };
    page.init();
};