﻿var layerId = request("layerId");
var acceptClick;
var auditorName = "";
var bootstrap = function (a, b) {
    var c = top[layerId].nodeList;
    var d = {
        init: function () {
            d.bind()
        },
        bind: function () {
            a("#auditorId").mkselect({
                text: "name",
                value: "id",
                data: c,
                select: function (e) {
                    if (e) {
                        auditorName = e.name
                    } else {
                        auditorName = ""
                    }
                },
            })
        }
    };
    acceptClick = function (e) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var f = a("#form").mkGetFormData();
        f.auditorName = auditorName;
        f.type = "6";
        e(f);
        return true
    };
    d.init()
};