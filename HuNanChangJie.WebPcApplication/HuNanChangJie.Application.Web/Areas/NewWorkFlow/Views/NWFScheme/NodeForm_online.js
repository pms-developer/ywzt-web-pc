﻿var layerId = request("layerId");
var isPreview = request("isPreview");
var acceptClick;
var auditors = [];
var workforms = [];
var workformMap = {};
var btnList = [{
    id: "1",
    name: "同意",
    code: "agree",
    file: "1",
    next: "1"
},
{
    id: "2",
    name: "不同意",
    code: "disagree",
    file: "1",
    next: "1"
}];
var conditions = [];
var bootstrap = function ($, Changjie) {
    var $currentNode = top[layerId].currentNode;
    var c = {};
    function isRepeat(id) {
        var res = false;
        for (var i = 0,
            j = auditors.length; i < j; i++) {
            if (auditors[i].auditorId == id) {
                Changjie.alert.warning("重复添加审核人员信息");
                res = true;
                break;
            }
        }
        return res;
    }
    var page = {
        init: function () {
            page.nodeInit();
            page.bind();
            page.initData();
            if (isPreview) {
                $("input,textarea").attr("readonly", "readonly");
                $(".mk-form-jfgrid-btns").remove();
            }
        },
        nodeInit: function () {
            switch ($currentNode.type) {
                case "startround":
                    $('#form_tabs li a[data-value="auditor"]').parent().remove();
                    $('#form_tabs li a[data-value="timeout"]').parent().remove();
                    $('#form_tabs li a[data-value="btnSetting"]').parent().remove();
                    $('#form_tabs li a[data-value="conditionField"]').parent().remove();
                    $('#form_tabs li a[data-value="conditionSqlDiv"]').parent().remove();
                    $('#form_tabs li a[data-value="operation"]').parent().remove();
                    $(".div_notice").show();
                    $(".div_next").show();
                    break;
                case "stepnode":
                    $('#form_tabs li a[data-value="conditionField"]').parent().remove();
                    $('#form_tabs li a[data-value="conditionSqlDiv"]').parent().remove();
                    $('#form_tabs li a[data-value="operation"]').parent().remove();
                    $(".div_notice").show();
                    $(".div_auditor").show();
                    $(".div_sign").show();
                    $(".div_batchAudit").show();
                    $("#name").removeAttr("readonly");
                    break;
                case "auditornode":
                    $('#form_tabs li a[data-value="timeout"]').parent().remove();
                    $('#form_tabs li a[data-value="btnSetting"]').parent().remove();
                    $('#form_tabs li a[data-value="conditionField"]').parent().remove();
                    $('#form_tabs li a[data-value="conditionSqlDiv"]').parent().remove();
                    $('#form_tabs li a[data-value="operation"]').parent().remove();
                    $(".div_batchAudit").show();
                    $(".div_notice").show();
                    $("#name").removeAttr("readonly");
                    break;
                case "conditionnode":
                    $('#form_tabs li a[data-value="workform"]').parent().remove();
                    $('#form_tabs li a[data-value="auditor"]').parent().remove();
                    $('#form_tabs li a[data-value="timeout"]').parent().remove();
                    $('#form_tabs li a[data-value="btnSetting"]').parent().remove();
                    $('#form_tabs li a[data-value="operation"]').parent().remove();
                    $("#name").removeAttr("readonly");
                    break;
                case "confluencenode":
                    $('#form_tabs li a[data-value="workform"]').parent().remove();
                    $('#form_tabs li a[data-value="auditor"]').parent().remove();
                    $('#form_tabs li a[data-value="timeout"]').parent().remove();
                    $('#form_tabs li a[data-value="btnSetting"]').parent().remove();
                    $('#form_tabs li a[data-value="conditionField"]').parent().remove();
                    $('#form_tabs li a[data-value="conditionSqlDiv"]').parent().remove();
                    $('#form_tabs li a[data-value="operation"]').parent().remove();
                    $(".div_confluence").show();
                    $("#name").removeAttr("readonly");
                    break;
                case "childwfnode":
                    $('#form_tabs li a[data-value="workform"]').parent().remove();
                    $('#form_tabs li a[data-value="timeout"]').parent().remove();
                    $('#form_tabs li a[data-value="btnSetting"]').parent().remove();
                    $('#form_tabs li a[data-value="conditionField"]').parent().remove();
                    $('#form_tabs li a[data-value="conditionSqlDiv"]').parent().remove();
                    $(".div_notice").show();
                    $(".div_child").show();
                    $("#name").removeAttr("readonly");
                    break;
            }
        },
        bind: function () {
            $("#form_tabs").mkFormTab();
            $("#notice").mkselect({
                text: "F_StrategyName",
                value: "F_StrategyCode",
                url: top.$.rootUrl + "/Message/StrategyInfo/GetList"
            });
            $('input[name="isAllAuditor"]').on("click",
                function () {
                    var g = $(this).val();
                    if (g == "2") {
                        if ($currentNode.type == "stepnode") {
                            $(".div_auditor_type").show();
                        }
                    } else {
                        $(".div_auditor_type").hide();
                    }
                });
            $("#childFlow").mkselect({
                text: "F_Name",
                value: "F_Code",
                url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/GetList",
                allowSearch: true
            });
            $("#confluenceType").mkselect({
                placeholder: false,
                data: [{
                    id: "1",
                    text: "所有步骤通过"
                },
                {
                    id: "2",
                    text: "一个步骤通过即可"
                },
                {
                    id: "3",
                    text: "按百分比计算"
                }]
            }).mkselectSet("1");
            $("#auditor_girdtable").jfGrid({
                headData: [{
                    label: "类型",
                    name: "type",
                    width: 100,
                    align: "center",
                    formatter: function (g) {
                        switch (g) {
                            case "1":
                                return "岗位";
                                break;
                            case "2":
                                return "角色";
                                break;
                            case "3":
                                return "用户";
                                break;
                            case "4":
                                return "上下级";
                                break;
                            case "5":
                                return "表字段";
                                break;
                            case "6":
                                return "某节点执行人";
                                break;
                        }
                    }
                },
                {
                    label: "名称",
                    name: "auditorName",
                    width: 260,
                    align: "left"
                },
                {
                    label: "附加条件",
                    name: "condition",
                    width: 150,
                    align: "left",
                    formatter: function (g) {
                        switch (g) {
                            case "1":
                                return "同一个部门";
                                break;
                            case "2":
                                return "同一个公司";
                                break;
                        }
                    }
                }]
            });
            $("#post_auditor").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuditorPostForm",
                        title: "添加审核岗位",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/PostForm",
                        width: 400,
                        height: 300,
                        callBack: function(id) {
                            return top[id].acceptClick(function(data) {
                                if (!isRepeat(data.auditorId)) {
                                    data.id = Changjie.newGuid();
                                    auditors.push(data);
                                    $("#auditor_girdtable").jfGridSet("refreshdata", auditors)
                                }
                            });
                        }
                    });
                });
            $("#role_auditor").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuditorRoleForm",
                        title: "添加审核角色",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/RoleForm",
                        width: 400,
                        height: 300,
                        callBack: function(id) {
                            return top[id].acceptClick(function(data) {
                                if (!isRepeat(data.auditorId)) {
                                    data.id = Changjie.newGuid();
                                    auditors.push(data);
                                    $("#auditor_girdtable").jfGridSet("refreshdata", auditors)
                                }
                            });
                        }
                    });
                });
            $("#user_auditor").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuditorUserForm",
                        title: "添加审核人员",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/UserForm",
                        width: 400,
                        height: 300,
                        callBack: function(id) {
                            return top[id].acceptClick(function(data) {
                                if (!isRepeat(data.auditorId)) {
                                    data.id = Changjie.newGuid();
                                    auditors.push(data);
                                    $("#auditor_girdtable").jfGridSet("refreshdata", auditors);
                                }
                            });
                        }
                    });
                });
            // 审核人员移除
            $("#delete_auditor").on("click",
                function () {
                    var _id = $("#auditor_girdtable").jfGridValue("id");
                    if (Changjie.checkrow(_id)) {
                        Changjie.layerConfirm("是否确认删除该审核人员！",
                            function(res, index) {
                                if (res) {
                                    for (var i = 0, k = auditors.length; i < k; i++) {
                                        if (auditors[i].id == _id) {
                                            auditors.splice(i, 1);
                                            $("#auditor_girdtable").jfGridSet("refreshdata", auditors);
                                            break;
                                        }
                                    }
                                    top.layer.close(index);
                                }
                            });
                    }
                });


            $("#level_auditor").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuditorLevelForm",
                        title: "添加上下级",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/LevelForm",
                        width: 400,
                        height: 300,
                        callBack: function(id) {
                            return top[id].acceptClick(function(h) {
                                if (!isRepeat(h.auditorId)) {
                                    h.id = Changjie.newGuid();
                                    auditors.push(h);
                                    $("#auditor_girdtable").jfGridSet("refreshdata", auditors);
                                }
                            });
                        }
                    });
                });
            $("#node_auditor").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuditorNodeForm",
                        title: "添加某节点执行人",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/AuditorNodeForm?layerId=" + layerId,
                        width: 400,
                        height: 300,
                        callBack: function(id) {
                            return top[id].acceptClick(function(data) {
                                if (!isRepeat(data.auditorId)) {
                                    data.id = Changjie.newGuid();
                                    auditors.push(data);
                                    $("#auditor_girdtable").jfGridSet("refreshdata", auditors);
                                }
                            });
                        }
                    });
                });
            $("#form_auditor").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuditorFieldForm",
                        title: "添加表字段",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/AuditorFieldForm?layerId=" + layerId,
                        width: 400,
                        height: 300,
                        callBack: function(id) {
                            return top[id].acceptClick(function(data) {
                                if (!isRepeat(data.auditorId)) {
                                    data.id = Changjie.newGuid();
                                    auditors.push(data);
                                    $("#auditor_girdtable").jfGridSet("refreshdata", auditors);
                                }
                            });
                        }
                    });
                });
            $("#workform_girdtable").jfGrid({
                headData: [{
                    label: "名称",
                    name: "name",
                    width: 160,
                    align: "left"
                },
                {
                    label: "类型",
                    name: "type",
                    width: 85,
                    align: "left",
                    formatter: function (cellvalue, row) {
                        if (cellvalue == 1) {
                            return '<span class="label label-success " style="cursor: pointer;">自定义表单</span>'
                        } else {
                            if (cellvalue == 0) {
                                return '<span class="label label-warning " style="cursor: pointer;">系统表单</span>'
                            }
                        }
                    }
                },
                {
                    label: "地址",
                    name: "url",
                    width: 200,
                    align: "left"
                }],
                isSubGrid: true,
                subGridExpanded: function (id, gruid) {
                    $("#" + id).jfGrid({
                        headData: [{
                            label: "字段名称",
                            name: "fieldName",
                            width: 240,
                            align: "left",
                            edit: {
                                type: gruid.type == "0" ? "input" : "label",
                                change: function (k, l) {
                                    workformMap[gruid.id].authorize[k.id] = k;
                                }
                            }
                        },
                        {
                            label: "字段ID",
                            name: "fieldId",
                            width: 240,
                            align: "left",
                            edit: {
                                type: gruid.type == "0" ? "input" : "label",
                                change: function (k, l) {
                                    workformMap[gruid.id].authorize[k.id] = k;
                                }
                            }
                        },
                        {
                            label: "查看",
                            name: "isLook",
                            width: 70,
                            align: "center",
                            formatter: function (cellvalue, row, dfop, $dcell) {
                                $dcell.on("click",
                                    function () {
                                        if (row.isLook == 1) {
                                            if (dfop.isEdit) {
                                                workformMap[gruid.id].authorize[row.id].isLook = 0;
                                            } else {
                                                var o = workformMap[row.formId].authorize;
                                                o[row.fieldId] = o[row.fieldId] || {
                                                    isLook: 1,
                                                    isEdit: 1
                                                };
                                                o[row.fieldId].isLook = 0;
                                            }
                                            row.isLook = 0;
                                            $(this).html('<span class="label label-default " style="cursor: pointer;">否</span>')
                                        } else {
                                            if (dfop.isEdit) {
                                                workformMap[gruid.id].authorize[row.id].isLook = 1
                                            } else {
                                                var o = workformMap[row.formId].authorize;
                                                o[row.fieldId] = o[row.fieldId] || {
                                                    isLook: 1,
                                                    isEdit: 1
                                                };
                                                o[row.fieldId].isLook = 1
                                            }
                                            row.isLook = 1;
                                            $(this).html('<span class="label label-success " style="cursor: pointer;">是</span>')
                                        }
                                    });
                                if (cellvalue == 1) {
                                    return '<span class="label label-success " style="cursor: pointer;">是</span>'
                                } else {
                                    if (cellvalue == 0) {
                                        return '<span class="label label-default " style="cursor: pointer;">否</span>'
                                    }
                                }
                            }
                        },
                        {
                            label: "编辑",
                            name: "isEdit",
                            width: 70,
                            align: "center",
                            formatter: function (cellvalue, row, dfop, $dcell) {
                                $dcell.on("click",
                                    function () {
                                        if (row.isEdit == 1) {
                                            if (dfop.isEdit) {
                                                workformMap[gruid.id].authorize[row.id].isEdit = 0
                                            } else {
                                                var o = workformMap[row.formId].authorize;
                                                o[row.fieldId] = o[row.fieldId] || {
                                                    isLook: 1,
                                                    isEdit: 1
                                                };
                                                o[row.fieldId].isEdit = 0
                                            }
                                            row.isEdit = 0;
                                            $(this).html('<span class="label label-default " style="cursor: pointer;">否</span>')
                                        } else {
                                            if (dfop.isEdit) {
                                                workformMap[gruid.id].authorize[row.id].isEdit = 1
                                            } else {
                                                var o = workformMap[row.formId].authorize;
                                                o[row.fieldId] = o[row.fieldId] || {
                                                    isLook: 1,
                                                    isEdit: 1
                                                };
                                                o[row.fieldId].isEdit = 1
                                            }
                                            row.isEdit = 1;
                                            $(this).html('<span class="label label-success " style="cursor: pointer;">是</span>')
                                        }
                                    });
                                if (cellvalue == 1) {
                                    return '<span class="label label-success " style="cursor: pointer;">是</span>'
                                } else {
                                    if (cellvalue == 0) {
                                        return '<span class="label label-default " style="cursor: pointer;">否</span>'
                                    }
                                }
                            }
                        }],
                        onAddRow: function (k, l) {
                            k.isLook = 0;
                            k.isEdit = 0;
                            k.id = Changjie.newGuid();
                            workformMap[gruid.id].authorize[k.id] = k
                        },
                        onMinusRow: function (k, l) {
                            if (workformMap[gruid.id].authorize[k.jfgridRowData.id]) {
                                delete workformMap[gruid.id].authorize[k.jfgridRowData.id]
                            }
                        },
                        isEdit: gruid.type == "0" ? true : false
                    });
                    if (gruid.type == "1") {
                        var $authorize = workformMap[gruid.formId].authorize;
                        $.mkSetForm(top.$.rootUrl + "/FormModule/Custmerform/GetFormData?keyValue=" + gruid.formId,
                            function (o) {
                                var u = JSON.parse(o.schemeEntity.F_Scheme);
                                var k = [];
                                for (var p = 0,
                                    s = u.data.length; p < s; p++) {
                                    var n = u.data[p].componts;
                                    for (var q = 0,
                                        r = n.length; q < r; q++) {
                                        var m = n[q];
                                        if (m.type == "gridtable" || m.type == "girdtable") {
                                            $.each(m.fieldsData,
                                                function (l, v) {
                                                    if (v.type != "guid") {
                                                        var w = {
                                                            formId: gruid.formId,
                                                            fieldName: m.title + "-" + v.name,
                                                            fieldId: m.id + "|" + v.id,
                                                            isLook: "1",
                                                            isEdit: "1"
                                                        };
                                                        if ($authorize[w.fieldId]) {
                                                            w.isLook = $authorize[w.fieldId].isLook;
                                                            w.isEdit = $authorize[w.fieldId].isEdit
                                                        }
                                                        k.push(w)
                                                    }
                                                })
                                        } else {
                                            var t = {
                                                formId: gruid.formId,
                                                fieldName: m.title,
                                                fieldId: m.id,
                                                isLook: "1",
                                                isEdit: "1"
                                            };
                                            if ($authorize[t.fieldId]) {
                                                t.isLook = $authorize[t.fieldId].isLook;
                                                t.isEdit = $authorize[t.fieldId].isEdit;
                                            }
                                            k.push(t)
                                        }
                                    }
                                }
                                $("#" + id).jfGridSet("refreshdata", k);
                            })
                    } else {
                        var g = [];
                        $.each(workformMap[gruid.id].authorize,
                            function (k, l) {
                                g.push(l)
                            });
                        $("#" + id).jfGridSet("refreshdata", g)
                    }
                }
            });
            $("#add_workform").on("click",
                function () {
                    Changjie.layerForm({
                        id: "WorkformForm",
                        title: "添加表单",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/WorkformForm",
                        width: 400,
                        height: 320,
                        callBack: function (g) {
                            return top[g].acceptClick(function (h) {
                                for (var j = 0,
                                    k = workforms.length; j < k; j++) {
                                    if (h.formId != "") {
                                        if (h.formId == workforms[j].formId) {
                                            Changjie.alert.error("重复添加表单");
                                            return false
                                        }
                                    } else {
                                        if (h.url == workforms[j].url) {
                                            Changjie.alert.error("重复添加表单");
                                            return false
                                        }
                                    }
                                }
                                h.id = Changjie.newGuid();
                                workforms.push(h);
                                h.authorize = {};
                                if (h.type == "0") {
                                    workformMap[h.id] = h
                                } else {
                                    workformMap[h.formId] = h
                                }
                                $("#workform_girdtable").jfGridSet("refreshdata", workforms)
                            })
                        }
                    })
                });
            $("#edit_workform").on("click",
                function () {
                    var g = $("#workform_girdtable").jfGridValue("id");
                    if (Changjie.checkrow(g)) {
                        Changjie.layerForm({
                            id: "WorkformForm",
                            title: "编辑表单",
                            url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/WorkformForm?id=" + g,
                            width: 400,
                            height: 320,
                            callBack: function (h) {
                                return top[h].acceptClick(function (j) {
                                    for (var k = 0,
                                        m = workforms.length; k < m; k++) {
                                        if (workforms[k].id != g) {
                                            if (j.formId != "") {
                                                if (j.formId == workforms[k].formId) {
                                                    Changjie.alert.error("重复添加表单");
                                                    return false
                                                }
                                            } else {
                                                if (j.url == workforms[k].url) {
                                                    Changjie.alert.error("重复添加表单");
                                                    return false
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0,
                                        m = workforms.length; k < m; k++) {
                                        if (workforms[k].id == g) {
                                            if (workforms[k].formId != j.formId) {
                                                delete workformMap[workforms[k].formId];
                                                j.authorize = {};
                                                workformMap[j.formId] = j
                                            }
                                            workforms[k] = j;
                                            $("#workform_girdtable").jfGridSet("refreshdata", workforms);
                                            break
                                        }
                                    }
                                })
                            }
                        })
                    }
                });
            $("#delete_workform").on("click",
                function () {
                    var g = $("#workform_girdtable").jfGridValue("id");
                    if (Changjie.checkrow(g)) {
                        Changjie.layerConfirm("是否确认删除该表单！",
                            function (m, j) {
                                if (m) {
                                    for (var h = 0,
                                        k = workforms.length; h < k; h++) {
                                        if (workforms[h].id == g) {
                                            if (workforms[h].type == "0") {
                                                delete workformMap[workforms[h].id]
                                            } else {
                                                delete workformMap[workforms[h].formId]
                                            }
                                            workforms.splice(h, 1);
                                            $("#workform_girdtable").jfGridSet("refreshdata", workforms);
                                            break
                                        }
                                    }
                                    top.layer.close(j)
                                }
                            })
                    }
                });
            $("#timeoutStrategy").mkselect({
                text: "F_StrategyName",
                value: "F_StrategyCode",
                url: top.$.rootUrl + "/Message/StrategyInfo/GetList"
            });
            $("#btn_girdtable").jfGrid({
                headData: [{
                    label: "名称",
                    name: "name",
                    width: 180,
                    align: "left"
                },
                {
                    label: "编码",
                    name: "code",
                    width: 180,
                    align: "left"
                },
                {
                    label: "是否隐藏",
                    name: "isHide",
                    width: 80,
                    align: "left",
                    formatter: function (g, h) {
                        if (g == "1") {
                            return "是"
                        } else {
                            return "否"
                        }
                    }
                },
                {
                    label: "下一节点审核人",
                    name: "next",
                    width: 100,
                    align: "left",
                    formatter: function (g, h) {
                        if (g == "1") {
                            return "不能手动设置"
                        } else {
                            if (g == "2") {
                                return "能手动设置"
                            }
                        }
                    }
                }]
            });
            $("#btn_girdtable").jfGridSet("refreshdata", btnList);
            $("#add_btns").on("click",
                function () {
                    Changjie.layerForm({
                        id: "ButtonForm",
                        title: "添加按钮",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/ButtonForm",
                        width: 400,
                        height: 320,
                        callBack: function (g) {
                            return top[g].acceptClick(function (i) {
                                var h = true;
                                $.each(btnList,
                                    function (j, k) {
                                        if (k.code == i.code) {
                                            Changjie.alert.error("按钮编码重复！");
                                            h = false;
                                            return false
                                        } else {
                                            if (k.name == i.name) {
                                                Changjie.alert.error("按钮名字重复！");
                                                h = false;
                                                return false
                                            }
                                        }
                                    });
                                if (h) {
                                    btnList.push(i);
                                    $("#btn_girdtable").jfGridSet("refreshdata", btnList)
                                }
                                return h
                            })
                        }
                    })
                });
            $("#edit_btns").on("click",
                function () {
                    var g = $("#btn_girdtable").jfGridValue("id");
                    if (Changjie.checkrow(g)) {
                        Changjie.layerForm({
                            id: "ButtonForm",
                            title: "编辑按钮",
                            url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/ButtonForm?id=" + g,
                            width: 400,
                            height: 320,
                            callBack: function (h) {
                                return top[h].acceptClick(function (j) {
                                    for (var k = 0,
                                        m = btnList.length; k < m; k++) {
                                        if (btnList[k].id != g) {
                                            if (btnList[k].code == j.code) {
                                                Changjie.alert.error("按钮编码重复！");
                                                return false
                                            } else {
                                                if (btnList[k].name == j.name) {
                                                    Changjie.alert.error("按钮名字重复！");
                                                    return false
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0,
                                        m = btnList.length; k < m; k++) {
                                        if (btnList[k].id == g) {
                                            btnList[k] = j;
                                            $("#btn_girdtable").jfGridSet("refreshdata", btnList);
                                            break
                                        }
                                    }
                                })
                            }
                        })
                    }
                });
            $("#delete_btns").on("click",
                function () {
                    var g = $("#btn_girdtable").jfGridValue("id");
                    if (Changjie.checkrow(g)) {
                        if (g == "1" || g == "2") {
                            Changjie.alert.error("同意和不同意按钮不允许删除！");
                            return false
                        }
                        Changjie.layerConfirm("是否确认删除该按钮！",
                            function (m, j) {
                                if (m) {
                                    for (var h = 0,
                                        k = btnList.length; h < k; h++) {
                                        if (btnList[h].id == g) {
                                            btnList.splice(h, 1);
                                            $("#btn_girdtable").jfGridSet("refreshdata", btnList);
                                            break
                                        }
                                    }
                                    top.layer.close(j)
                                }
                            })
                    }
                });
            $("#condition_girdtable").jfGrid({
                headData: [{
                    label: "数据库",
                    name: "dbId",
                    width: 100,
                    align: "left",
                    formatterAsync: function (g, i, h) {
                        if (i == "systemdb") {
                            g("本地数据库")
                        } else {
                            Changjie.clientdata.getAsync("db", {
                                key: i,
                                callback: function (j) {
                                    g(j.alias)
                                }
                            })
                        }
                    }
                },
                {
                    label: "数据表",
                    name: "table",
                    width: 100,
                    align: "left"
                },
                {
                    label: "关联字段",
                    name: "field1",
                    width: 100,
                    align: "left"
                },
                {
                    label: "比较字段",
                    name: "field2",
                    width: 100,
                    align: "left"
                },
                {
                    label: "比较类型",
                    name: "compareType",
                    width: 80,
                    align: "center",
                    formatter: function (g, h) {
                        switch (g) {
                            case "1":
                                return "等于";
                                break;
                            case "2":
                                return "不等于";
                                break;
                            case "3":
                                return "大于";
                                break;
                            case "4":
                                return "大于等于";
                                break;
                            case "5":
                                return "小于";
                                break;
                            case "6":
                                return "小于等于";
                                break;
                            case "7":
                                return "包含";
                                break;
                            case "8":
                                return "不包含";
                                break;
                            case "9":
                                return "包含于";
                                break;
                            case "10":
                                return "不包含于";
                                break
                        }
                    }
                },
                {
                    label: "数据值",
                    name: "value",
                    width: 100,
                    align: "left"
                }]
            });
            $("#add_condition").on("click",
                function () {
                    Changjie.layerForm({
                        id: "AuthorizeForm",
                        title: "添加条件字段",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/ConditionFieldForm",
                        width: 400,
                        height: 400,
                        callBack: function (g) {
                            return top[g].acceptClick(function (h) {
                                conditions.push(h);
                                $("#condition_girdtable").jfGridSet("refreshdata", conditions)
                            })
                        }
                    })
                });
            $("#edit_condition").on("click",
                function () {
                    var g = $("#condition_girdtable").jfGridValue("id");
                    if (Changjie.checkrow(g)) {
                        Changjie.layerForm({
                            id: "AuthorizeForm",
                            title: "编辑条件字段",
                            url: top.$.rootUrl + "/NewWorkFlow/NWFScheme/ConditionFieldForm?id=" + g,
                            width: 400,
                            height: 400,
                            callBack: function (h) {
                                return top[h].acceptClick(function (j) {
                                    for (var k = 0,
                                        m = conditions.length; k < m; k++) {
                                        if (conditions[k].id == g) {
                                            conditions[k] = j;
                                            $("#condition_girdtable").jfGridSet("refreshdata", conditions);
                                            break
                                        }
                                    }
                                })
                            }
                        })
                    }
                });
            $("#delete_condition").on("click",
                function () {
                    var g = $("#condition_girdtable").jfGridValue("id");
                    if (Changjie.checkrow(g)) {
                        Changjie.layerConfirm("是否确认删除该条件字段！",
                            function (m, j) {
                                if (m) {
                                    for (var h = 0,
                                        k = conditions.length; h < k; h++) {
                                        if (conditions[h].id == g) {
                                            conditions.splice(h, 1);
                                            $("#condition_girdtable").jfGridSet("refreshdata", conditions);
                                            break
                                        }
                                    }
                                    top.layer.close(j)
                                }
                            })
                    }
                });
            $("#dbConditionId").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库",
                allowSearch: true
            });
            $('input[name="operationType"]').on("click",
                function () {
                    var g = $(this).val();
                    $(".operationDiv").hide();
                    $("#" + g).show()
                });
            $("#dbId").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库",
                allowSearch: true
            })
        },
        initData: function () {
            $("#baseInfo").mkSetFormData($currentNode);
            $("#timeout").mkSetFormData($currentNode);
            $("#dbConditionId").mkselectSet($currentNode.dbConditionId);
            $("#conditionSql").val($currentNode.conditionSql || "");
            if ($currentNode.wfForms) {
                workforms = $currentNode.wfForms;
                $.each(workforms,
                    function (g, h) {
                        h.authorize = h.authorize || {};
                        if (h.type == "0") {
                            workformMap[h.id] = h
                        } else {
                            workformMap[h.formId] = h
                        }
                    })
            }
            if ($currentNode.auditors) {
                auditors = $currentNode.auditors
            }
            if ($currentNode.btnList && $currentNode.btnList.length > 0) {
                btnList = $currentNode.btnList
            }
            if ($currentNode.conditions) {
                conditions = $currentNode.conditions
            }
            $("#workform_girdtable").jfGridSet("refreshdata", workforms);
            $("#auditor_girdtable").jfGridSet("refreshdata", auditors);
            $("#btn_girdtable").jfGridSet("refreshdata", btnList);
            $("#condition_girdtable").jfGridSet("refreshdata", conditions);
            $("#operationTypeDiv").mkSetFormData($currentNode);
            switch ($currentNode.operationType) {
                case "sql":
                    $("#dbId").mkselectSet($currentNode.dbId);
                    $("#strSql").val($currentNode.strSql);
                    break;
                case "interface":
                    $("#strInterface").val($currentNode.strInterface);
                    break;
                case "ioc":
                    $("#iocName").val($currentNode.iocName);
                    break;
            }
        }
    };
    acceptClick = function (callback) {
        if (!$("#baseInfo").mkValidform()) {
            return false;
        }
        var g = $("#baseInfo").mkGetFormData();
        switch ($currentNode.type) {
            case "startround":
                $currentNode.notice = g.notice;
                $currentNode.isNext = g.isNext;
                $currentNode.isTitle = g.isTitle;
                $currentNode.wfForms = workforms;
                break;
            case "stepnode":
                $currentNode.name = g.name;
                $currentNode.notice = g.notice;
                $currentNode.isAllAuditor = g.isAllAuditor;
                $currentNode.auditorAgainType = g.auditorAgainType;
                $currentNode.auditorType = g.auditorType;
                $currentNode.auditExecutType = g.auditExecutType;
                $currentNode.isSign = g.isSign;
                $currentNode.isBatchAudit = g.isBatchAudit;
                $currentNode.auditors = auditors;
                $currentNode.wfForms = workforms;
                var j = $("#timeout").mkGetFormData();
                $currentNode.timeoutNotice = j.timeoutNotice;
                $currentNode.timeoutInterval = j.timeoutInterval;
                $currentNode.timeoutStrategy = j.timeoutStrategy;
                $currentNode.timeoutAction = j.timeoutAction;
                $currentNode.btnList = btnList;
                break;
            case "auditornode":
                $currentNode.name = g.name;
                $currentNode.notice = g.notice;
                $currentNode.isBatchAudit = g.isBatchAudit;
                $currentNode.auditors = auditors;
                $currentNode.wfForms = workforms;
                break;
            case "confluencenode":
                $currentNode.name = g.name;
                $currentNode.confluenceType = g.confluenceType;
                $currentNode.confluenceRate = g.confluenceRate;
                break;
            case "conditionnode":
                $currentNode.name = g.name;
                $currentNode.conditions = conditions;
                $currentNode.dbConditionId = $("#dbConditionId").mkselectGet();
                $currentNode.conditionSql = $("#conditionSql").val();
                break;
            case "childwfnode":
                $currentNode.name = g.name;
                $currentNode.notice = g.notice;
                $currentNode.childFlow = g.childFlow;
                $currentNode.childType = g.childType;
                $currentNode.auditors = auditors;
                var i = $("#operationTypeDiv").mkGetFormData();
                $currentNode.operationType = i.operationType;
                switch ($currentNode.operationType) {
                    case "sql":
                        $currentNode.dbId = $("#dbId").mkselectGet();
                        $currentNode.strSql = $("#strSql").val();
                        break;
                    case "interface":
                        $currentNode.strInterface = $("#strInterface").val();
                        break;
                    case "ioc":
                        $currentNode.iocName = $("#iocName").val();
                        break;
                }
                break;
        }
        callback();
        return true;
    };
    page.init();
};