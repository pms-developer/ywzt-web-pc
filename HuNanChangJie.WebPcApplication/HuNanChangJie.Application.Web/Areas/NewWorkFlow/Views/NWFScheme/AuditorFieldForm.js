﻿var acceptClick;
var auditorName = "";
var fieldName = "";
var formName = "";
var dbId = "";
var bootstrap = function (a, b) {
    var c = {
        init: function () {
            c.bind()
        },
        bind: function () {
            a("#AuditorFieldId").mkselect({
                value: "f_column",
                text: "f_column",
                title: "f_remark",
                allowSearch: true,
                select: function (d) {
                    if (d) {
                        fieldName = d.f_column
                    } else {
                        fieldName = ""
                    }
                }
            });
            a("#ReFieldId").mkselect({
                value: "f_column",
                text: "f_column",
                title: "f_remark",
                allowSearch: true
            });
            a("#ReFormId").mkselect({
                value: "name",
                text: "name",
                title: "tdescription",
                allowSearch: true,
                select: function (d) {
                    if (d) {
                        b.httpAsync("GET", top.$.rootUrl + "/SystemModule/DatabaseTable/GetFieldList", {
                            databaseLinkId: dbId,
                            tableName: d.name
                        },
                            function (e) {
                                if (e) {
                                    a("#ReFieldId").mkselectRefresh({
                                        data: e
                                    });
                                    a("#AuditorFieldId").mkselectRefresh({
                                        data: e
                                    })
                                }
                            });
                        formName = d.name
                    } else {
                        formName = "";
                        a("#ReFieldId").mkselectRefresh({
                            data: []
                        });
                        a("#AuditorFieldId").mkselectRefresh({
                            data: []
                        })
                    }
                }
            });
            a("#DbId").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库",
                allowSearch: true,
                select: function (d) {
                    if (d && !d.hasChildren) {
                        dbId = d.id;
                        a("#ReFormId").mkselectRefresh({
                            url: top.$.rootUrl + "/SystemModule/DatabaseTable/GetList",
                            param: {
                                databaseLinkId: d.id
                            }
                        })
                    } else {
                        dbId = "";
                        a("#ReFormId").mkselectRefresh({
                            url: false,
                            data: [],
                            param: {}
                        })
                    }
                }
            })
        }
    };
    acceptClick = function (d) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var e = a("#form").mkGetFormData();
        var f = {};
        f.auditorId = e.DbId + "|" + e.ReFormId + "|" + e.ReFieldId + "|" + e.AuditorFieldId;
        f.auditorName = "【" + formName + "】" + fieldName;
        f.type = "5";
        d(f);
        return true
    };
    c.init()
};