﻿/*
 * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-03-16 16:03
 * 描  述：_NWF_TaskLog
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/NewWorkFlow/_NWFTaskLog/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').GridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/NewWorkFlow/_NWFTaskLog/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/NewWorkFlow/_NWFTaskLog/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/NewWorkFlow/_NWFTaskLog/GetPageList',
                headData: [
                    { headerName: "流程进程主键", field: "F_ProcessId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程任务主键", field: "F_TaskId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "操作码", field: "F_OperationCode", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "F_OperationName", field: "F_OperationName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程节点ID", field: "F_NodeId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程节点名称", field: "F_NodeName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程任务类型 ", field: "F_TaskType", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "上一流程节点ID", field: "F_PrevNodeId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "创建时间", field: "CreationDate", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "创建人主键", field: "Creation_Id", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "创建人员名称", field: "CreationName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务人主键", field: "F_TaskUserId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "F_TaskUserName", field: "F_TaskUserName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "备注信息", field: "F_Des", width: 100, cellStyle: { 'text-align': 'left' } },
                ],
                mainId:'F_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
