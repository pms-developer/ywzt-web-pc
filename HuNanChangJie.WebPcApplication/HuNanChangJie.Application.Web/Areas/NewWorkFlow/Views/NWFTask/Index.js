﻿/* 
 * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-03-16 15:59
 * 描  述：NWF_Task
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/NewWorkFlow/NWFTask/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').GridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/NewWorkFlow/NWFTask/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/NewWorkFlow/NWFTask/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/NewWorkFlow/NWFTask/GetPageList',
                headData: [
                    { headerName: "流程实例主键", field: "F_ProcessId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程节点ID", field: "F_NodeId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程节点名称", field: "F_NodeName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "加签情况下最初的审核者", field: "F_FirstUserId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "子流程进程主键", field: "F_ChildProcessId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务类型", field: "F_Type", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "是否完成1完成2关闭0未完成", field: "F_IsFinished", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务超时流转到下一个节点时间", field: "F_TimeoutAction", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务超时提醒消息时间", field: "F_TimeoutNotice", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务超时消息提醒间隔时间", field: "F_TimeoutInterval", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务超时消息发送策略编码", field: "F_TimeoutStrategy", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "上一个任务节点Id", field: "F_PrevNodeId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "上一个节点名称", field: "F_PrevNodeName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务创建时间", field: "CreationDate", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务创建人员", field: "Creation_Id", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务变更时间", field: "ModificationDate", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务变更人员信息", field: "Modification_Id", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务创建人员名称", field: "CreationName", width: 100, cellStyle: { 'text-align': 'left' } },
                ],
                mainId:'F_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
