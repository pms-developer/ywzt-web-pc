﻿
/*
 *
 *  
 * 创建人： 微信：zhi-qiang
 * 日 期：2017.08.04
 * 描 述：流程（我的任务）	
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    var categoryId = "2";
    var logbegin = "";
    var logend = "";
    refreshGirdData = function () {
        $("#gridtable").jfGridSet("reload");
    };
    var page = {
        init: function () {
            page.initleft();
            page.initGrid();
            page.bind();
        },
        bind: function () {
            $("#datesearch").mkdate({
                dfdata: [{
                    name: "今天",
                    begin: function () {
                        return Changjie.getDate("yyyy-MM-dd 00:00:00");
                    },
                    end: function () {
                        return Changjie.getDate("yyyy-MM-dd 23:59:59");
                    }
                },
                {
                    name: "近7天",
                    begin: function () {
                        return Changjie.getDate("yyyy-MM-dd 00:00:00", "d", -6);
                    },
                    end: function () {
                        return Changjie.getDate("yyyy-MM-dd 23:59:59");
                    }
                },
                {
                    name: "近1个月",
                    begin: function () {
                        return Changjie.getDate("yyyy-MM-dd 00:00:00", "m", -1);
                    },
                    end: function () {
                        return Changjie.getDate("yyyy-MM-dd 23:59:59");
                    }
                },
                {
                    name: "近3个月",
                    begin: function () {
                        return Changjie.getDate("yyyy-MM-dd 00:00:00", "m", -3);
                    },
                    end: function () {
                        return Changjie.getDate("yyyy-MM-dd 23:59:59");
                    }
                },
                ],
                mShow: false,
                premShow: false,
                jShow: false,
                prejShow: false,
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                selectfn: function (g, h) {
                    logbegin = g;
                    logend = h;
                    page.search()
                }
            });
            $("#btn_Search").on("click",
                function () {
                    var keyword = $("#txt_Keyword").val();
                    page.search({
                        keyword: keyword
                    });
                });
            $("#lr_refresh").on("click",
                function () {
                    location.reload();
                });
            $("#lr_eye").on("click",
                function () {
                    page.eye();
                });
            $("#lr_release").on("click",
                function () {
                    Changjie.layerForm({
                        id: "form",
                        title: "选择流程模板",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/ReleaseForm",
                        height: 600,
                        width: 825,
                        maxmin: true,
                        callBack: function(id) {
                            return top[id].acceptClick();
                        }
                    });
                });
            $("#lr_verify").on("click",
                function () {
                    page.verify();
                });
            $("#lr_batchAudit").on("click",
                function() {
                    Changjie.layerForm({
                        id: "BatchAuditIndex",
                        title: "批量审核",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/BatchAuditIndex",
                        height: 700,
                        width: 900,
                        maxmin: true,
                        btn: null
                    });
                });
        },
        initleft: function () {
            $("#lr_left_list li").on("click",
                function() {
                    var $this = $(this);
                    var $parent = $this.parent();
                    $parent.find(".active").removeClass("active");
                    $this.addClass("active");
                    categoryId = $this.attr("data-value");
                    if (categoryId == "2") {
                        $("#lr_verify").show();
                        $("#lr_verify span").text("审核");
                    } else {
                        $("#lr_verify").hide();
                    }
                    page.search();
                });
        },
        initGrid: function () {
            $("#gridtable").mkAuthorizeJfGrid({
                url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/GetTaskPageList",
                headData: [{
                    label: "任务",
                    name: "F_TaskName",
                    width: 160,
                    align: "left",
                    formatter: function (cellvalue, row, dfop, $cell) {
                        if (row.F_EnabledMark == 3) {
                            if (categoryId == "1") {
                                return "本人发起";
                            } else {
                               return cellvalue;
                            }
                        }
                        if (row.F_EnabledMark == 2) {
                            $cell.on("click", ".create",
                                function () {
                                    Changjie.frameTab.closeByParam("tabProcessId", row.F_Id);
                                    Changjie.frameTab.open({
                                        F_ModuleId: row.F_Id,
                                        F_Icon: "fa magic",
                                        F_FullName: "创建流程-" + row.F_SchemeName,
                                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?processId=" + row.F_Id + "&tabIframeId=" + row.F_Id + "&type=draftCreate"
                                    });
                                    return false;
                                });
                            $cell.on("click", ".delete",
                                function () {
                                    Changjie.layerConfirm("是否确认删除该草稿？",
                                        function (m) {
                                            if (m) {
                                                Changjie.frameTab.closeByParam("tabProcessId", row.F_Id);
                                                Changjie.deleteForm(
                                                    top.$.rootUrl + "/NewWorkFlow/NWFProcess/DeleteDraft",
                                                    {
                                                        processId: row.F_Id
                                                    },
                                                    function() {
                                                        refreshGirdData();
                                                    });
                                            }
                                        });
                                    return false;
                                });
                            return '<span class="label label-success create" title="编辑创建">编辑创建</span><span class="label label-danger delete" style="margin-left:5px;" title="删除草稿" >删除草稿</span>'
                        }
                        var $IsAgain = false;
                        if (categoryId == "1") {
                            if (row.F_IsAgain == 1) {
                                $IsAgain = true;
                            } else {
                                if (row.F_IsFinished == 0) {
                                    $cell.on("click", ".urge",
                                        function () {
                                            Changjie.layerConfirm("是否确认催办审核？",
                                                function (msg, id) {
                                                    if (msg) {
                                                        Changjie.loading(true, "催办审核...");
                                                        var param = {
                                                            processId: row.F_Id,
                                                        };
                                                        Changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/UrgeFlow", param,
                                                            function (data) {
                                                                Changjie.loading(false);
                                                            });
                                                        top.layer.close(id);
                                                    }
                                                });
                                            return false;
                                        });
                                    $cell.on("click", ".revoke",
                                        function () {
                                            Changjie.layerConfirm("是否确认撤销流程？",
                                                function (msg, id) {
                                                    if (msg) {
                                                        Changjie.loading(true, "撤销流程...");
                                                        var param = {
                                                            processId: row.F_Id,
                                                        };
                                                        Changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/RevokeFlow", param,
                                                            function (data) {
                                                                Changjie.loading(false);
                                                                refreshGirdData();
                                                            });
                                                        top.layer.close(id);
                                                    }
                                                });
                                            return false
                                        });
                                    var $urgeHtml = '<span class="label label-primary urge" title="催办审核" >催办审核</span>';
                                    if (row.F_IsStart == 0) {
                                        $urgeHtml +=
                                            '<span class="label label-warning revoke" style="margin-left:5px;" title="撤销流程" >撤销流程</span>';
                                    }
                                    return $urgeHtml;
                                } else {
                                    return "本人发起";
                                }
                            }
                        }
                        if (row.F_TaskType == 3) {
                            return "【加签】" + cellvalue;
                        } else {
                            if (row.F_TaskType == 5 && categoryId == "2") {
                                $IsAgain = true;
                            } else {
                                if (row.F_TaskType == 5) {
                                    return "重新发起";
                                }
                            }
                        }
                        if ($IsAgain) {
                            $cell.on("click", ".AgainCreate",
                                function () {
                                    var m = "";
                                    if (row.F_SchemeName != row.F_Title && row.F_Title) {
                                        m = row.F_SchemeName + "(" + row.F_Title + ")";
                                    } else {
                                        m = row.F_SchemeName;
                                    }
                                    Changjie.frameTab.open({
                                        F_ModuleId: row.F_Id,
                                        F_Icon: "fa magic",
                                        F_FullName: "重新发起-" + m,
                                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?processId=" + row.F_Id + "&tabIframeId=" + row.F_Id + "&type=againCreate"
                                    });
                                    return false;
                                });
                            return '<span class="label label-danger AgainCreate" title="重新发起">重新发起</span>';
                        }
                        return cellvalue;
                    }
                },
                {
                    label: "标题",
                    name: "F_Title",
                    width: 300,
                    align: "left",
                    formatter: function (cellvalue, row) {
                        if (row.F_SchemeName != row.F_Title && row.F_Title) {
                            return row.F_SchemeName + "(" + row.F_Title + ")";
                        } else {
                            return row.F_SchemeName;
                        }
                    }
                },
                {
                    label: "等级",
                    name: "F_Level",
                    width: 60,
                    align: "center",
                    formatter: function (cellvalue) {
                        switch (cellvalue) {
                            case 0:
                                return "普通";
                                break;
                            case 1:
                                return "重要";
                                break;
                            case 2:
                                return "紧急";
                                break;
                            default:
                                return "普通";
                                break;
                        }
                    }
                },
                {
                    label: "状态",
                    name: "F_EnabledMark",
                    width: 70,
                    align: "center",
                    formatter: function (cellvalue, row) {
                        if (row.F_IsFinished == 0) {
                            if (cellvalue == 1) {
                                if (row.F_IsUrge == "1" && categoryId == "2") {
                                    return '<span class="label label-danger">催办加急</span>';
                                }
                                return '<span class="label label-success">运行中</span>';
                            } else {
                                if (cellvalue == 2) {
                                    return '<span class="label label-primary">草稿</span>';
                                } else {
                                    return '<span class="label label-danger">作废</span>';
                                }
                            }
                        } else {
                            return '<span class="label label-warning">结束</span>';
                        }
                    }
                },
                {
                    label: "发起者",
                    name: "CreationName",
                    width: 70,
                    align: "center"
                },
                {
                    label: "时间",
                    name: "CreationDate",
                    width: 150,
                    align: "left",
                    formatter: function (g) {
                        return Changjie.formatDate(g, "yyyy-MM-dd hh:mm:ss");
                    }
                }],
                mainId: "F_Id",
                isPage: true,
                sidx: "CreationDate",
                onSelectRow: function (g) {
                    if (categoryId == "2") {
                        if (g.F_TaskType == 5) {
                            $("#lr_verify span").text("重新发起");
                        } else {
                            if (g.F_TaskType == 3) {
                                $("#lr_verify span").text("【加签】" + g.F_TaskName);
                            } else {
                                $("#lr_verify span").text(g.F_TaskName);
                            }
                        }
                    }
                },
                dblclick: function () {
                    if (categoryId == "2") {
                        page.verify();
                    } else {
                        page.eye();
                    }
                }
            });
            page.search();
        },
        search: function (g) {
            g = g || {};
            g.StartTime = logbegin;
            g.EndTime = logend;
            $("#gridtable").jfGridSet("reload",
                {
                    queryJson: JSON.stringify(g),
                    categoryId: categoryId
                });
        },
        verify: function () {
            var $id = $("#gridtable").jfGridValue("F_Id");
            var $taskid = $("#gridtable").jfGridValue("F_TaskId");
            var $title = $("#gridtable").jfGridValue("F_Title");
            var $schemename = $("#gridtable").jfGridValue("F_SchemeName");
            var $taskname = $("#gridtable").jfGridValue("F_TaskName");
            var $tasktype = $("#gridtable").jfGridValue("F_TaskType");
            if ($schemename != $title && $title) {
                $title = $schemename + "(" + $title + ")";
            } else {
                $title = $schemename;
            }
            switch ($tasktype) {
                case 1:
                    Changjie.frameTab.open({
                        F_ModuleId:
                            $taskid,
                        F_Icon: "fa magic",
                        F_FullName: "审批-" + $title + "/" + $taskname,
                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?tabIframeId=" + $taskid + "&type=audit&processId=" + $id + "&taskId=" + $taskid
                    });
                    break;
                case 2:
                    Changjie.frameTab.open({
                        F_ModuleId:
                            $taskid,
                        F_Icon: "fa magic",
                        F_FullName: "查阅-" + $title + "/" + $taskname,
                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?tabIframeId=" + $taskid + "&type=refer&processId=" + $id + "&taskId=" + $taskid
                    });
                    break;
                case 3:
                    Changjie.frameTab.open({
                        F_ModuleId:
                            $taskid,
                        F_Icon: "fa magic",
                        F_FullName: "加签审核-" + $title + "/" + $taskname,
                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?tabIframeId=" + $taskid + "&type=signAudit&processId=" + $id + "&taskId=" + $taskid
                    });
                    break;
                case 4:
                    Changjie.frameTab.open({
                        F_ModuleId:
                            $taskid,
                        F_Icon: "fa magic",
                        F_FullName: "子流程-" + $title + "/" + $taskname,
                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?tabIframeId=" + $taskid + "&type=chlid&processId=" + $id + "&taskId=" + $taskid
                    });
                    break;
                case 5:
                    Changjie.frameTab.open({
                        F_ModuleId:
                            $id,
                        F_Icon: "fa magic",
                        F_FullName: "重新发起-" + $title,
                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?processId=" + $id + "&tabIframeId=" + $id + "&type=againCreate"
                    });
                    break;
                case 6:
                    Changjie.frameTab.open({
                        F_ModuleId:
                            $taskid,
                        F_Icon: "fa magic",
                        F_FullName: "子流程-" + $title + "/" + $taskname,
                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?tabIframeId=" + $taskid + "&type=againChild&processId=" + $id + "&taskId=" + $taskid
                    });
                    break;
            }
        },
        eye: function () {
            var $id = $("#gridtable").jfGridValue("F_Id") || "";
            var $taskid = $("#gridtable").jfGridValue("F_TaskId") || "";
            var $title = $("#gridtable").jfGridValue("F_Title");
            var $schemename = $("#gridtable").jfGridValue("F_SchemeName");
            var $tasktype = $("#gridtable").jfGridValue("F_TaskType");
            if ($schemename != $title && $title) {
                $title = $schemename + "(" + $title + ")";
            } else {
                $title = $schemename;
            }
            var g = $("#gridtable").jfGridValue("F_EnabledMark");
            if (g == 2) {
                Changjie.alert.warning("草稿不能查看进度");
                return;
            }
            if (Changjie.checkrow($id)) {
                if ($tasktype == "4" || $tasktype == "6") {
                    Changjie.frameTab.open({
                        F_ModuleId: $id + $taskid,
                        F_Icon: "fa magic",
                        F_FullName: "查看流程进度【" + $title + "】",
                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?tabIframeId=" +
                            $id +
                            $taskid +
                            "&type=childlook&processId=" +
                            $id +
                            "&taskId=" +
                            $taskid
                    });
                } else {
                    Changjie.frameTab.open({
                        F_ModuleId: $id + $taskid,
                        F_Icon: "fa magic",
                        F_FullName: "查看流程进度【" + $title + "】",
                        F_UrlAddress: "/NewWorkFlow/NWFProcess/NWFContainerForm?tabIframeId=" +
                            $id +
                            $taskid +
                            "&type=look&processId=" +
                            $id +
                            "&taskId=" +
                            $taskid
                    });
                }
            }
        }
    };
    page.init();
};