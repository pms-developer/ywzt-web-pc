﻿/*
 * 
 *  
 * 日 期：2019.03.24
 * 描 述：工作流操作界面
 */

var tabProcessId = ""; // 当前窗口ID
var nwflow;
var tabIframeId = request("tabIframeId");
var shcemeCode = request("shcemeCode"); // 流程模板编号
var type = request("type");  // 操作类型  .创建  审批  重新创建  确认阅读  加签 100 流程进度查看
var processId = request("processId"); // 流程实例主键
var taskId = request("taskId");// 任务主键
var wfFormParam = request("wfFormParam"); //表单阐述
var bootstrap = function ($, changjie) {



    var formIframesData = {};
    var formIframes = []; 
    var formIframesHave = {};
    nwflow = {
        schemeObj: null,
        nodeMap: {},
        currentNode: null,
        history: [],
        currentIds: [],
        taskInfo: [],
        processId: "",
        taskId: "",
        init: function () {
            switch (type) {
                case "look":
                    nwflow.initLook();
                    break;
                case "create":
                    nwflow.initCreate();
                    break;
                case "draftCreate":
                    nwflow.initDraftCreate();
                    break;
                case "againCreate":
                    nwflow.initAgainCreate();
                    break;
                case "audit":
                    nwflow.initAudit();
                    break;
                case "signAudit":
                    nwflow.initSignAudit();
                    break;
                case "refer":
                    nwflow.initRefer();
                    break;
                case "chlid":
                    nwflow.initChlid();
                    break;
                case "childlook":
                    nwflow.initChildlook();
                    break;
                case "againChild":
                    nwflow.initAgainChild();
                    break;
            }
        },
        initLook: function () {
            nwflow.processId = processId;
            nwflow.taskId = taskId;
            nwflow.getProcessInfo(processId,
                taskId,
                function(h) {
                    if (h) {
                        var i = h.info;
                        nwflow.taskInfo = h.task;
                        nwflow.schemeObj = JSON.parse(i.Scheme);
                        $.each(nwflow.schemeObj.nodes,
                            function(j, k) {
                                if (k.id == i.CurrentNodeId) {
                                    nwflow.currentNode = k;
                                    return false;
                                }
                            });
                        nwflow.loadForm(nwflow.currentNode.wfForms, true, true);
                        $("#form_list_tabs_warp").mkscroll();
                        nwflow.history = i.TaskLogList;
                        nwflow.currentIds = i.CurrentNodeIds;
                        nwflow.loadFlowInfo();
                        nwflow.loadTimeLine();
                        if (i.parentProcessId) {
                            nwflow.pProcessId = i.parentProcessId;
                            $("#eye").show();
                        }
                    }
                });
        },
        initCreate: function () {
            nwflow.processId = changjie.newGuid();
            nwflow.getSchemeByCode(shcemeCode,
                function (h) {
                    if (h) {
                        nwflow.schemeObj = JSON.parse(h.F_Content);
                        $.each(nwflow.schemeObj.nodes,
                            function (i, j) {
                                if (j.type == "startround") {
                                    nwflow.currentNode = j;
                                    return false;
                                }
                            });
                        nwflow.loadForm(nwflow.currentNode.wfForms);
                        $("#form_list_tabs_warp").mkscroll();
                        nwflow.loadFlowInfo();
                        nwflow.loadTimeLine();
                        $("#release").show();
                        $("#release").on("click",
                            function () {
                                if (!flow.validForm("create")) {
                                    return false
                                }
                                changjie.layerForm({
                                    id: "CreateForm",
                                    title: "创建流程",
                                    url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/CreateForm?shcemeCode=" + shcemeCode,
                                    width: 400,
                                    height: 340,
                                    callBack: function (i) {
                                        return top[i].acceptClick(function (k, j) {
                                            flow.save(nwflow.processId, nwflow.currentNode.wfForms,
                                                function () {
                                                    changjie.loading(true, "创建流程...");
                                                    var l = {
                                                        schemeCode: shcemeCode,
                                                        processId: nwflow.processId,
                                                        title: k.title,
                                                        level: k.level,
                                                        auditors: JSON.stringify(j)
                                                    };
                                                    changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/CreateFlow", l,
                                                        function (m) {
                                                            changjie.loading(false);
                                                            if (m) {
                                                                changjie.frameTab.parentIframe().refreshGirdData();
                                                                changjie.frameTab.close(tabIframeId)
                                                            }
                                                        })
                                                })
                                        })
                                    }
                                })
                            });
                        $("#savedraft").show();
                        $("#savedraft").on("click",
                            function () {
                                tabProcessId = nwflow.processId;
                                flow.save(nwflow.processId, nwflow.currentNode.wfForms,
                                    function () {
                                        changjie.loading(true, "保存流程草稿...");
                                        var i = {
                                            schemeCode: shcemeCode,
                                            processId: nwflow.processId
                                        };
                                        changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/SaveDraft", i,
                                            function (j) {
                                                changjie.loading(false);
                                                if (j) {
                                                    changjie.frameTab.parentIframe().refreshGirdData()
                                                }
                                            })
                                    })
                            })
                    }
                })
        },
        initDraftCreate: function () {
            tabProcessId = processId;
            nwflow.processId = processId;
            nwflow.getSchemeByProcessId(processId,
                function (h) {
                    if (h) {
                        nwflow.schemeObj = JSON.parse(h.F_Content);
                        $.each(nwflow.schemeObj.nodes,
                            function (i, j) {
                                if (j.type == "startround") {
                                    nwflow.currentNode = j;
                                    return false
                                }
                            });
                        nwflow.loadForm(nwflow.currentNode.wfForms, true);
                        $("#form_list_tabs_warp").mkscroll();
                        nwflow.loadFlowInfo();
                        nwflow.loadTimeLine();
                        $("#release").show();
                        $("#release").on("click",
                            function () {
                                if (!flow.validForm("create")) {
                                    return false
                                }
                                changjie.layerForm({
                                    id: "CreateForm",
                                    title: "创建流程",
                                    url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/CreateForm?processId=" + nwflow.processId,
                                    width: 400,
                                    height: 340,
                                    callBack: function (i) {
                                        return top[i].acceptClick(function (k, j) {
                                            flow.save(nwflow.processId, nwflow.currentNode.wfForms,
                                                function () {
                                                    changjie.loading(true, "创建流程...");
                                                    var l = {
                                                        processId: nwflow.processId,
                                                        title: k.title,
                                                        level: k.level,
                                                        auditors: JSON.stringify(j)
                                                    };
                                                    changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/CreateFlow", l,
                                                        function (m) {
                                                            changjie.loading(false);
                                                            if (m) {
                                                                changjie.frameTab.parentIframe().refreshGirdData();
                                                                changjie.frameTab.close(tabIframeId)
                                                            }
                                                        })
                                                })
                                        })
                                    }
                                })
                            });
                        $("#savedraft").show();
                        $("#savedraft").on("click",
                            function () {
                                tabProcessId = nwflow.processId;
                                flow.save(nwflow.processId, nwflow.currentNode.wfForms,
                                    function () {
                                        changjie.loading(true, "保存流程草稿...");
                                        var i = {
                                            schemeCode: shcemeCode,
                                            processId: nwflow.processId
                                        };
                                        changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/SaveDraft", i,
                                            function (j) {
                                                changjie.loading(false);
                                                if (j) {
                                                    changjie.frameTab.parentIframe().refreshGirdData()
                                                }
                                            })
                                    })
                            })
                    }
                })
        },
        initAgainCreate: function () {
            nwflow.processId = processId;
            nwflow.getProcessInfo(processId, "",
                function (h) {
                    if (h) {
                        var i = h.info;
                        nwflow.taskInfo = h.task;
                        nwflow.schemeObj = JSON.parse(i.Scheme);
                        $.each(nwflow.schemeObj.nodes,
                            function (j, k) {
                                if (k.id == i.CurrentNodeId) {
                                    nwflow.currentNode = k;
                                    return false
                                }
                            });
                        nwflow.loadForm(nwflow.currentNode.wfForms, true);
                        $("#form_list_tabs_warp").mkscroll();
                        nwflow.history = i.TaskLogList;
                        nwflow.currentIds = i.CurrentNodeIds;
                        nwflow.loadFlowInfo();
                        nwflow.loadTimeLine();
                        $("#release").show();
                        $("#release").text("重新发起");
                        $("#release").on("click",
                            function () {
                                if (!flow.validForm("againCreate")) {
                                    return false
                                }
                                changjie.layerConfirm("是否重新发起流程！",
                                    function (k, j) {
                                        if (k) {
                                            flow.save(nwflow.processId, nwflow.currentNode.wfForms,
                                                function () {
                                                    changjie.loading(true, "重新发起流程...");
                                                    var l = {
                                                        processId: nwflow.processId
                                                    };
                                                    changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/AgainCreateFlow", l,
                                                        function (m) {
                                                            changjie.loading(false);
                                                            if (m) {
                                                                changjie.frameTab.parentIframe().refreshGirdData();
                                                                changjie.frameTab.close(tabIframeId)
                                                            }
                                                        })
                                                });
                                            top.layer.close(j)
                                        }
                                    })
                            })
                    }
                })
        },
        initAudit: function () {
            nwflow.processId = processId;
            nwflow.taskId = taskId;
            nwflow.getProcessInfo(processId, taskId,
                function (i) {
                    if (i) {
                        var j = i.info;
                        nwflow.taskInfo = i.task;
                        nwflow.schemeObj = JSON.parse(j.Scheme);
                        $.each(nwflow.schemeObj.nodes,
                            function (k, l) {
                                if (l.id == j.CurrentNodeId) {
                                    nwflow.currentNode = l;
                                    return false
                                }
                            });
                        nwflow.loadForm(nwflow.currentNode.wfForms, true);
                        $("#form_list_tabs_warp").mkscroll();
                        nwflow.history = j.TaskLogList;
                        nwflow.currentIds = j.CurrentNodeIds;
                        nwflow.loadFlowInfo();
                        nwflow.loadTimeLine();
                        var h = $("#sign");
                        $.each(nwflow.currentNode.btnList,
                            function (m, n) {
                                if (n.isHide != "1") {
                                    var l = " btn-warning";
                                    if (n.code == "agree") {
                                        l = " btn-success"
                                    } else {
                                        if (n.code == "disagree") {
                                            l = " btn-danger"
                                        }
                                    }
                                    var k = $('<a class="verifybtn btn ' + l + '"  >' + n.name + "</a>");
                                    k[0].mkbtn = n;
                                    h.after(k)
                                }
                            });
                        $(".verifybtn").show();
                        $(".verifybtn").on("click",
                            function () {
                                var k = $(this)[0].mkbtn;
                                if (!flow.validForm(k.code)) {
                                    return false
                                }
                                changjie.layerForm({
                                    id: "AuditFlowForm",
                                    title: "【审批】" + k.name,
                                    url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/AuditFlowForm?processId=" + nwflow.processId + "&taskId=" + nwflow.taskId + "&next=" + k.next + "&operationCode=" + k.code,
                                    width: 500,
                                    height: 400,
                                    callBack: function (l) {
                                        return top[l].acceptClick(function (n, m) {
                                            flow.save(nwflow.processId, nwflow.currentNode.wfForms,
                                                function () {
                                                    changjie.loading(true, "审批流程...");
                                                    var o = {
                                                        operationCode: k.code,
                                                        operationName: k.name,
                                                        processId: nwflow.processId,
                                                        taskId: nwflow.taskId,
                                                        des: n,
                                                        auditors: JSON.stringify(m)
                                                    };
                                                    changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/AuditFlow", o,
                                                        function (p) {
                                                            changjie.loading(false);
                                                            if (p) {
                                                                changjie.frameTab.parentIframe().refreshGirdData();
                                                                changjie.frameTab.close(tabIframeId)
                                                            }
                                                        })
                                                })
                                        })
                                    }
                                })
                            });
                        if (nwflow.currentNode.isSign == "1") {
                            $("#sign").show();
                            $("#sign").on("click",
                                function () {
                                    if (!flow.validForm("sign")) {
                                        return false
                                    }
                                    changjie.layerForm({
                                        id: "SignFlowForm",
                                        title: "加签",
                                        url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/SignFlowForm",
                                        width: 500,
                                        height: 400,
                                        callBack: function (k) {
                                            return top[k].acceptClick(function (l) {
                                                flow.save(nwflow.processId, nwflow.currentNode.wfForms,
                                                    function () {
                                                        changjie.loading(true, "流程加签...");
                                                        var m = {
                                                            des: l.des,
                                                            userId: l.auditorId,
                                                            processId: nwflow.processId,
                                                            taskId: nwflow.taskId
                                                        };
                                                        changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/SignFlow", m,
                                                            function (n) {
                                                                changjie.loading(false);
                                                                if (n) {
                                                                    changjie.frameTab.parentIframe().refreshGirdData();
                                                                    changjie.frameTab.close(tabIframeId)
                                                                }
                                                            })
                                                    })
                                            })
                                        }
                                    })
                                })
                        }
                        if (i.parentProcessId) {
                            nwflow.pProcessId = i.parentProcessId;
                            $("#eye").show()
                        }
                    }
                })
        },
        initSignAudit: function () {
            nwflow.processId = processId;
            nwflow.taskId = taskId;
            nwflow.getProcessInfo(processId, taskId,
                function (k) {
                    if (k) {
                        var l = k.info;
                        nwflow.taskInfo = k.task;
                        nwflow.schemeObj = JSON.parse(l.Scheme);
                        $.each(nwflow.schemeObj.nodes,
                            function (m, n) {
                                if (n.id == l.CurrentNodeId) {
                                    nwflow.currentNode = n;
                                    return false
                                }
                            });
                        nwflow.loadForm(nwflow.currentNode.wfForms, true);
                        $("#form_list_tabs_warp").mkscroll();
                        nwflow.history = l.TaskLogList;
                        nwflow.currentIds = l.CurrentNodeIds;
                        nwflow.loadFlowInfo();
                        nwflow.loadTimeLine();
                        var j = $("#sign");
                        var h = $('<a class="verifybtn btn btn-success"  >同意</a>');
                        h[0].mkbtn = {
                            code: "agree",
                            name: "同意"
                        };
                        j.after(h);
                        var i = $('<a class="verifybtn btn btn-danger"  >不同意</a>');
                        i[0].mkbtn = {
                            code: "disagree",
                            name: "不同意"
                        };
                        j.after(i);
                        $(".verifybtn").show();
                        $(".verifybtn").on("click",
                            function () {
                                var m = $(this)[0].mkbtn;
                                if (!flow.validForm(m.code)) {
                                    return false
                                }
                                changjie.layerForm({
                                    id: "AuditFlowForm",
                                    title: "【加签审批】" + m.name,
                                    url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/AuditFlowForm?operationCode=" + m.code,
                                    width: 500,
                                    height: 400,
                                    callBack: function (n) {
                                        return top[n].acceptClick(function (p, o) {
                                            flow.save(nwflow.processId, nwflow.currentNode.wfForms,
                                                function () {
                                                    changjie.loading(true, "加签审批流程...");
                                                    var q = {
                                                        operationCode: m.code,
                                                        processId: nwflow.processId,
                                                        taskId: nwflow.taskId,
                                                        des: p
                                                    };
                                                    changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/SignAuditFlow", q,
                                                        function (r) {
                                                            changjie.loading(false);
                                                            if (r) {
                                                                changjie.frameTab.parentIframe().refreshGirdData();
                                                                changjie.frameTab.close(tabIframeId)
                                                            }
                                                        })
                                                })
                                        })
                                    }
                                })
                            });
                        j.show();
                        j.on("click",
                            function () {
                                if (!flow.validForm("sign")) {
                                    return false
                                }
                                changjie.layerForm({
                                    id: "SignFlowForm",
                                    title: "加签",
                                    url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/SignFlowForm",
                                    width: 500,
                                    height: 400,
                                    callBack: function (m) {
                                        return top[m].acceptClick(function (n) {
                                            flow.save(nwflow.processId, nwflow.currentNode.wfForms,
                                                function () {
                                                    changjie.loading(true, "流程加签...");
                                                    var o = {
                                                        des: n.des,
                                                        userId: n.auditorId,
                                                        processId: nwflow.processId,
                                                        taskId: nwflow.taskId
                                                    };
                                                    changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/SignFlow", o,
                                                        function (p) {
                                                            changjie.loading(false);
                                                            if (p) {
                                                                changjie.frameTab.parentIframe().refreshGirdData();
                                                                changjie.frameTab.close(tabIframeId)
                                                            }
                                                        })
                                                })
                                        })
                                    }
                                })
                            })
                    }
                })
        },
        initRefer: function () {
            nwflow.processId = processId;
            nwflow.taskId = taskId;
            nwflow.getProcessInfo(processId, taskId,
                function (h) {
                    if (h) {
                        var i = h.info;
                        nwflow.taskInfo = h.task;
                        nwflow.schemeObj = JSON.parse(i.Scheme);
                        $.each(nwflow.schemeObj.nodes,
                            function (j, k) {
                                if (k.id == i.CurrentNodeId) {
                                    nwflow.currentNode = k;
                                    return false
                                }
                            });
                        nwflow.loadForm(nwflow.currentNode.wfForms, true);
                        $("#form_list_tabs_warp").mkscroll();
                        nwflow.history = i.TaskLogList;
                        nwflow.currentIds = i.CurrentNodeIds;
                        nwflow.loadFlowInfo();
                        nwflow.loadTimeLine();
                        $("#confirm").show().on("click",
                            function () {
                                changjie.layerConfirm("是否确认阅读！",
                                    function (l, j) {
                                        if (l) {
                                            changjie.loading(true, "保存数据...");
                                            var k = {
                                                processId: nwflow.processId,
                                                taskId: nwflow.taskId
                                            };
                                            changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/ReferFlow", k,
                                                function (m) {
                                                    changjie.loading(false);
                                                    if (m) {
                                                        changjie.frameTab.parentIframe().refreshGirdData();
                                                        changjie.frameTab.close(tabIframeId)
                                                    }
                                                });
                                            top.layer.close(j)
                                        }
                                    })
                            })
                    }
                })
        },
        initChlid: function () {
            nwflow.taskId = taskId;
            nwflow.getProcessInfo(processId, taskId,
                function (h) {
                    if (h) {
                        var i = h.info;
                        nwflow.taskInfo = h.task;
                        nwflow.schemeObj = JSON.parse(i.Scheme);
                        $.each(nwflow.schemeObj.nodes,
                            function (j, k) {
                                if (k.id == i.CurrentNodeId) {
                                    nwflow.currentNode = k;
                                    return false
                                }
                            });
                        nwflow.processId = i.childProcessId;
                        nwflow.chlidProcessId = i.childProcessId;
                        nwflow.getSchemeByCode(nwflow.currentNode.childFlow,
                            function (j) {
                                if (j) {
                                    nwflow.schemeObj = JSON.parse(j.F_Content);
                                    $.each(nwflow.schemeObj.nodes,
                                        function (k, l) {
                                            if (l.type == "startround") {
                                                nwflow.chlidCurrentNode = l;
                                                return false
                                            }
                                        });
                                    nwflow.loadForm(nwflow.chlidCurrentNode.wfForms, true);
                                    $("#form_list_tabs_warp").mkscroll();
                                    nwflow.history = j.TaskLogList || [];
                                    nwflow.currentIds = j.CurrentNodeIds || "";
                                    nwflow.loadFlowInfo();
                                    nwflow.loadTimeLine();
                                    $("#release").show();
                                    $("#release").on("click",
                                        function () {
                                            if (!flow.validForm("create")) {
                                                return false
                                            }
                                            flow.save(nwflow.chlidProcessId, nwflow.chlidCurrentNode.wfForms,
                                                function () {
                                                    changjie.loading(true, "创建流程...");
                                                    var k = {
                                                        schemeCode: nwflow.currentNode.childFlow,
                                                        processId: nwflow.chlidProcessId,
                                                        parentProcessId: nwflow.pProcessId,
                                                        parentTaskId: nwflow.pTaskId
                                                    };
                                                    changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/CreateChildFlow", k,
                                                        function (l) {
                                                            changjie.loading(false);
                                                            if (l) {
                                                                changjie.frameTab.parentIframe().refreshGirdData();
                                                                changjie.frameTab.close(tabIframeId)
                                                            }
                                                        })
                                                })
                                        });
                                    $("#savedraft").show();
                                    $("#savedraft").on("click",
                                        function () {
                                            flow.save(nwflow.chlidProcessId, nwflow.chlidCurrentNode.wfForms,
                                                function () { })
                                        })
                                }
                            });
                        nwflow.pTaskId = nwflow.taskId;
                        nwflow.pProcessId = processId;
                        $("#eye").show()
                    }
                })
        },
        initChildlook: function () {
            nwflow.taskId = taskId;
            nwflow.getProcessInfo(processId, taskId,
                function (h) {
                    if (h) {
                        var i = h.info;
                        nwflow.taskInfo = h.task;
                        nwflow.schemeObj = JSON.parse(i.Scheme);
                        $.each(nwflow.schemeObj.nodes,
                            function (j, k) {
                                if (k.id == i.CurrentNodeId) {
                                    nwflow.currentNode = k;
                                    return false
                                }
                            });
                        nwflow.processId = i.childProcessId;
                        nwflow.chlidProcessId = i.childProcessId;
                        nwflow.getSchemeByCode(nwflow.currentNode.childFlow,
                            function (j) {
                                if (j) {
                                    nwflow.schemeObj = JSON.parse(j.F_Content);
                                    $.each(nwflow.schemeObj.nodes,
                                        function (k, l) {
                                            if (l.type == "startround") {
                                                nwflow.chlidCurrentNode = l;
                                                return false
                                            }
                                        });
                                    nwflow.loadForm(nwflow.chlidCurrentNode.wfForms, true, true);
                                    $("#form_list_tabs_warp").mkscroll();
                                    nwflow.history = i.TaskLogList || [];
                                    nwflow.currentIds = i.CurrentNodeIds || "";
                                    nwflow.loadFlowInfo();
                                    nwflow.loadTimeLine()
                                }
                            });
                        nwflow.pTaskId = nwflow.taskId;
                        nwflow.pProcessId = processId;
                        $("#eye").show()
                    }
                })
        },
        initAgainChild: function () {
            nwflow.taskId = taskId;
            nwflow.getProcessInfo(processId, taskId,
                function (h) {
                    if (h) {
                        var i = h.info;
                        nwflow.taskInfo = h.task;
                        nwflow.schemeObj = JSON.parse(i.Scheme);
                        $.each(nwflow.schemeObj.nodes,
                            function (j, k) {
                                if (k.id == i.CurrentNodeId) {
                                    nwflow.currentNode = k;
                                    return false
                                }
                            });
                        nwflow.processId = i.childProcessId;
                        nwflow.chlidProcessId = i.childProcessId;
                        nwflow.getSchemeByCode(nwflow.currentNode.childFlow,
                            function (j) {
                                if (j) {
                                    nwflow.schemeObj = JSON.parse(j.F_Content);
                                    $.each(nwflow.schemeObj.nodes,
                                        function (k, l) {
                                            if (l.type == "startround") {
                                                nwflow.chlidCurrentNode = l;
                                                return false
                                            }
                                        });
                                    nwflow.loadForm(nwflow.chlidCurrentNode.wfForms, true);
                                    $("#form_list_tabs_warp").mkscroll();
                                    nwflow.history = i.TaskLogList || [];
                                    nwflow.currentIds = i.CurrentNodeIds || "";
                                    nwflow.loadFlowInfo();
                                    nwflow.loadTimeLine();
                                    $("#release").show();
                                    $("#release").on("click",
                                        function () {
                                            if (!flow.validForm("create")) {
                                                return false
                                            }
                                            flow.save(nwflow.chlidProcessId, nwflow.chlidCurrentNode.wfForms,
                                                function () {
                                                    changjie.loading(true, "创建流程...");
                                                    var k = {
                                                        schemeCode: nwflow.currentNode.childFlow,
                                                        processId: nwflow.chlidProcessId,
                                                        parentProcessId: nwflow.pProcessId,
                                                        parentTaskId: nwflow.pTaskId
                                                    };
                                                    changjie.httpAsync("Post", top.$.rootUrl + "/NewWorkFlow/NWFProcess/CreateChildFlow", k,
                                                        function (l) {
                                                            changjie.loading(false);
                                                            if (l) {
                                                                changjie.frameTab.parentIframe().refreshGirdData();
                                                                changjie.frameTab.close(tabIframeId)
                                                            }
                                                        })
                                                })
                                        });
                                    $("#savedraft").show();
                                    $("#savedraft").on("click",
                                        function () {
                                            flow.save(nwflow.chlidProcessId, nwflow.chlidCurrentNode.wfForms,
                                                function () { })
                                        })
                                }
                            });
                        nwflow.pTaskId = nwflow.taskId;
                        nwflow.pProcessId = processId;
                        $("#eye").show()
                    }
                })
        },
        loadForm: function (j, k, l) {
            var i = $("#form_list_tabs");
            var h = $("#form_list_iframes");
            $.each(j,
                function (m, n) {
                    i.append('<li><a data-value="' + m + '" >' + n.name + "</a></li>");
                    if (n.type == "1") {
                        h.append('<div id="wfFormContainer' + m + '" class="form-list-container" data-value="' + m + '" ></div>');
                        n._index = m;
                        flow.init(n, h.find("#wfFormContainer" + m), k, l)
                    } else {
                        h.append('<iframe id="wfFormIframe' + m + '" class="form-list-iframe" data-value="' + m + '" frameborder="0" ></iframe>');
                        page.iframeLoad("wfFormIframe" + m,
                            n.url,
                            function(p, o) {
                                p.setAuthorize && p.setAuthorize(o.authorize, l);
                                p.setFormData &&
                                    p.setFormData(nwflow.processId,
                                        wfFormParam,
                                        function() {
                                            $(".mk-layout-panel-btn").show();
                                        });
                                if (!p.setFormData) {
                                    $(".mk-layout-panel-btn").show();
                                }
                            },
                            n);
                    }
                    if (m == 0) {
                        i.find("a").trigger("click");
                    }
                });
            if (j.length == 0) {
                $(".mk-layout-panel-btn").show();
            }
        },
        loadFlowInfo: function () {
            changjie.clientdata.getAllAsync("department", {
                callback: function (h) {
                    changjie.clientdata.getAllAsync("user", {
                        callback: function (data) {
                            var i = {};
                            $.each(nwflow.taskInfo,
                                function (l, m) {
                                    var n = [];
                                    $.each(m.nWFUserInfoList,
                                        function (q, curdata) {
                                            if (data[curdata.Id]) {
                                                var userobj = data[curdata.Id].name;
                                                var department = h[data[curdata.Id].departmentId];
                                                if (department) {
                                                    userobj = "【" + department.name + "】" + userobj;
                                                }
                                                n.push(userobj);
                                            }
                                        });
                                    var o = {
                                        namelist: String(n)
                                    };
                                    i[m.F_NodeId] = i[m.F_NodeId] || [];
                                    i[m.F_NodeId].push(o)
                                });
                            $.each(nwflow.history,
                                function (l, m) {
                                    i[m.F_NodeId] = i[m.F_NodeId] || [];
                                    i[m.F_NodeId].push(m)
                                });
                            var j = String(nwflow.currentIds);
                            $.each(nwflow.schemeObj.nodes,
                                function (l, m) {
                                    m.state = "3";
                                    if (i[m.id]) {
                                        m.history = i[m.id];
                                        m.state = "1"
                                    }
                                    if (j.indexOf(m.id) > -1) {
                                        m.state = "0"
                                    }
                                    if (m.isAllAuditor == "2") {
                                        m.name += "<br/>【多人审核:";
                                        if (m.auditorType == "1") {
                                            m.name += "并行】";
                                        } else {
                                            m.name += "串行】";
                                        }
                                    }
                                    nwflow.nodeMap[m.id] = m
                                });
                            $("#flow").mkworkflowSet("set", {
                                data: nwflow.schemeObj
                            })
                        }
                    })
                }
            })
        },
        loadTimeLine: function () {
            var nodelist = [];
            changjie.clientdata.getAllAsync("department",
                {
                    callback: function(i) {
                        changjie.clientdata.getAllAsync("user",
                            {
                                callback: function(s) {
                                    for (var m = 0,
                                        o = nwflow.history.length;
                                        m < o;
                                        m++) {
                                        var n = nwflow.history[m];
                                        var createUserName = (n.CreationName || "系统处理") + "：";
                                        if (n.Creation_Id && s[n.Creation_Id]) {
                                            var deptobj = i[s[n.Creation_Id].departmentId];
                                            if (deptobj) {
                                                createUserName = "【" + deptobj.name + "】" + createUserName;
                                            }
                                        }
                                        var operationName = n.F_OperationName;
                                        if (n.F_Des) {
                                            operationName += "【审批意见】" + n.F_Des;
                                        }
                                        var $nodeName = "";
                                        if (n.F_NodeId && nwflow.nodeMap[n.F_NodeId]) {
                                            $nodeName = nwflow.nodeMap[n.F_NodeId].name;
                                        }
                                        var lookflowFrom = {
                                            title: n.F_NodeName || $nodeName,
                                            people: createUserName,
                                            content: operationName,
                                            time: n.CreationDate
                                        };
                                        if (n.F_OperationCode == "createChild" ||
                                            n.F_OperationCode == "againCreateChild") {
                                            lookflowFrom.content = operationName + '<span class="mk-event" >查看</span>';
                                            lookflowFrom.nodeId = n.F_NodeId;
                                            lookflowFrom.processId = n.F_ProcessId;
                                            lookflowFrom.callback = function(data) {
                                                changjie.layerForm({
                                                    id: "LookFlowForm",
                                                    title: "子流程查看",
                                                    url: top.$.rootUrl +
                                                        "/NewWorkFlow/NWFProcess/LookFlowForm?nodeId=" +
                                                        data.nodeId +
                                                        "&processId=" +
                                                        data.processId +
                                                        "&type=lookChlid",
                                                    width: 1000,
                                                    height: 900,
                                                    maxmin: true,
                                                    btn: null
                                                });
                                            }
                                        }
                                        nodelist.push(lookflowFrom);
                                    }
                                    $("#auditinfo").mktimeline(nodelist);
                                }
                            });
                    }
                });
        },
        getSchemeByCode: function (code, callback) {
            changjie.httpAsync("GET",
                top.$.rootUrl + "/NewWorkFlow/NWFProcess/GetSchemeByCode",
                {
                    code: code
                },
                function(data) {
                    callback && callback(data);
                });
        },
        getSchemeByProcessId: function (processId, callback) {
            changjie.httpAsync("GET",
                top.$.rootUrl + "/NewWorkFlow/NWFProcess/GetSchemeByProcessId",
                {
                    processId: processId
                },
                function(res) {
                    callback && callback(res);
                });
        },
        getProcessInfo: function (processId, taskId, callback) {
            changjie.httpAsync("GET",
                top.$.rootUrl + "/NewWorkFlow/NWFProcess/GetProcessDetails",
                {
                    processId: processId,
                    taskId: taskId
                },
                function(res) {
                    callback && callback(res);
                });
        }
    };
    var flow = {
        init: function (i, h, j, k) {
          
            flow.getScheme(i.formId,
                function (m) {
                    if (m) {
                        var n = JSON.parse(m.schemeEntity.F_Scheme);
                        i.formScheme = n;
                        var o = {};
                        var l = false;
                        $.each(i.authorize || [],
                            function (p, r) {
                                var q = p.split("|");
                                if (q.length > 1) {
                                    if (r.isLook != 1 || r.isEdit != 1) {
                                        o[q[0]] = o[q[0]] || {};
                                        o[q[0]][q[1]] = r;
                                        l = true
                                    }
                                }
                            });
                        if (l) {
                            $.each(n.data,
                                function (p, q) {
                                    $.each(q.componts,
                                        function (t, u) {
                                            if ((u.type == "girdtable" || u.type == "gridtable") && !!o[u.id]) {
                                                var s = o[u.id];
                                                var r = [];
                                                $.each(u.fieldsData,
                                                    function (v, w) {
                                                        if (!s[w.id] || s[w.id].isLook == 1) {
                                                            r.push(w);
                                                            if (s[w.id] && s[w.id].isEdit != 1) {
                                                                w._isEdit = 1
                                                            }
                                                        }
                                                    });
                                                u.fieldsData = r
                                            }
                                        })
                                })
                        }
                        i.girdCompontMap = h.mkCustmerFormRender(n.data);
                        $.each(i.authorize || [],
                            function (p, r) {
                                var q = p.split("|");
                                if (q.length == 1) {
                                    if (r.isLook != 1) {
                                        $("#" + q[0]).parent().remove();
                                        $('[name="' + q[0] + '"]').parents(".mk-form-item").attr("disabled", "disabled")
                                    } else {
                                        if (r.isEdit != 1) {
                                            $("#" + q[0]).attr("disabled", "disabled");
                                            if ($("#" + q[0]).hasClass("lrUploader-wrap")) {
                                                $("#" + q[0]).css({
                                                    "padding-right": "58px"
                                                });
                                                $("#" + q[0]).find(".btn-success").remove()
                                            }
                                        }
                                    }
                                }
                            });
                        if (k) {
                            $(".mkUploader-wrap").css({
                                "padding-right": "58px"
                            }).find(".btn-success").remove()
                        }
                        if (j) {
                            flow.getFormData(i, nwflow.processId, i.field,
                                function (q, p) {
                                    $.each(q,
                                        function (v, w) {
                                            if (p.girdCompontMap[v]) {
                                                var t = {};
                                                $.each(p.girdCompontMap[v].fieldsData,
                                                    function (A, z) {
                                                        if (z.field) {
                                                            t[z.field.toLowerCase()] = z.field
                                                        }
                                                    });
                                                var y = [];
                                                for (var u = 0,
                                                    x = w.length; u < x; u++) {
                                                    var s = {};
                                                    for (var r in w[u]) {
                                                        s[t[r]] = w[u][r]
                                                    }
                                                    y.push(s)
                                                }
                                                if (y.length > 0) {
                                                    p.isUpdate = true
                                                }
                                                $("#" + p.girdCompontMap[v].id).jfGridSet("refreshdata", {
                                                    rowdatas: y
                                                })
                                            } else {
                                                if (w[0]) {
                                                    p.isUpdate = true;
                                                    $("#wfFormContainer" + p._index).mkSetCustmerformData(w[0], v)
                                                }
                                            }
                                        });
                                    $.each(p.authorize || [],
                                        function (r, s) {
                                            if (s.isLook == 1 && s.isEdit != 1) {
                                                $('[name="' + r + '"]').attr("disabled", "disabled")
                                            }
                                        });
                                    $(".mk-layout-panel-btn").show()
                                })
                        } else {
                            $(".mk-layout-panel-btn").show()
                        }
                    }
                })
        },
        getScheme: function (i, h) {
            changjie.httpAsync("GET", top.$.rootUrl + "/FormModule/Custmerform/GetFormData", {
                keyValue: i
            },
                function (j) {
                    h && h(j)
                })
        },
        getFormData: function (i, j, k, h) {
            changjie.httpAsync("GET", top.$.rootUrl + "/FormModule/Custmerform/GetInstanceForm", {
                schemeInfoId: i.formId,
                keyValue: j,
                processIdName: k
            },
                function (l) {
                    h && h(l, i)
                })
        },
        validForm: function (h) {
            if (!$.mkValidCustmerform()) {
                return false
            }
            for (var j = 0,
                k = formIframes.length; j < k; j++) {
                if (formIframes[j].validForm && !formIframes[j].validForm(h)) {
                    return false
                }
            }
            return true
        },
        save: function (p, o, h) {
            for (var k = 0,
                m = formIframes.length; k < m; k++) {
                formIframesHave[k] = null;
                if (formIframesHave[k] != 1) {
                    formIframes[k].save(p,
                        function (l, i) {
                            if (l.code == 200) {
                                formIframes[i].isUpdate = true;
                                formIframesHave[i] = 1
                            } else {
                                formIframesHave[i] = 0
                            }
                        },
                        k)
                }
            }
            var j = [];
            $.each(o,
                function (i, l) {
                    if (l.type == "1") {
                        var r = {
                            schemeInfoId: l.formId,
                            processIdName: l.field
                        };
                        var q = $("#wfFormContainer" + i).mkGetCustmerformData();
                        if (l.isUpdate) {
                            r.keyValue = p
                        }
                        q[l.field] = p;
                        r.formData = JSON.stringify(q);
                        j.push(r);
                        l.isUpdate = true
                    }
                });
            if (j.length > 0) {
                $.mkSaveForm(top.$.rootUrl + "/FormModule/Custmerform/SaveInstanceForms", {
                    data: JSON.stringify(j)
                },
                    function (i) {
                        if (i.code == 200) {
                            n()
                        } else {
                            changjie.alert.error("表单数据保存失败")
                        }
                    })
            } else {
                n()
            }
            function n() {
                var t = 0;
                var q = true;
                for (var r = 0,
                    s = formIframes.length; r < s; r++) {
                    if (formIframesHave[r] == 0) {
                        t++;
                        q = false
                    } else {
                        if (formIframesHave[r] == 1) {
                            t++
                        }
                    }
                }
                if (t == formIframes.length) {
                    if (q) {
                        h()
                    } else {
                        changjie.alert.error("表单数据保存失败")
                    }
                } else {
                    setTimeout(function () {
                        n()
                    },
                        100)
                }
            }
        }
    };
    var page = {
        init: function () {
            page.bind();
            nwflow.init();
        },
        bind: function () {
            $("#tablist").mkFormTabEx(function (h) {
                if (h == "workflowshcemeinfo" || h == "auditinfo") {
                    $("#print").hide()
                } else {
                    $("#print").show()
                }
            });
            $("#form_list_tabs").on("click", "a",
                function () {
                    var i = $(this);
                    if (!i.hasClass("active")) {
                        i.parents("ul").find(".active").removeClass("active");
                        i.parent().addClass("active");
                        var j = i.attr("data-value");
                        var h = $("#form_list_iframes");
                        h.find(".active").removeClass("active");
                        h.find('[data-value="' + j + '"]').addClass("active")
                    }
                });
            $("#flow").mkworkflow({
                isPreview: true,
                openNode: function (h) {
                    top.wflookNode = h;
                    if (h.type == "childwfnode") {
                        changjie.layerForm({
                            id: "LookFlowForm",
                            title: "子流程查看",
                            url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/LookFlowForm?schemeCode=" + h.childFlow + "&nodeId=" + h.id + "&processId=" + nwflow.processId + "&type=lookChlid",
                            width: 1000,
                            height: 900,
                            maxmin: true,
                            btn: null
                        });
                        return
                    }
                    if (h.history) {
                        changjie.layerForm({
                            id: "LookNodeForm",
                            title: "审批记录查看【" + h.name + "】",
                            url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/LookNodeForm",
                            width: 600,
                            height: 400,
                            btn: null
                        })
                    }
                }
            });
            $("#print").on("click",
                function () {
                    var i = $("#form_list_iframes");
                    var j = i.find(".form-list-iframe.active").attr("id");
                    if (j) {
                        var h = changjie.iframe(j, frames);
                        h.$(".mk-form-wrap:visible").jqprint()
                    } else {
                        i.find(".form-list-container.active").find(".mk-form-wrap:visible").jqprint()
                    }
                });
            $("#print").show();
            $("#eye").on("click",
                function () {
                    changjie.layerForm({
                        id: "LookFlowForm",
                        title: "父流程进度查看",
                        url: top.$.rootUrl + "/NewWorkFlow/NWFProcess/LookFlowForm?processId=" + nwflow.pProcessId + "&taskId=" + nwflow.pTaskId + "&type=lookParent",
                        width: 1000,
                        height: 900,
                        maxmin: true,
                        btn: null
                    })
                })
        },
        iframeLoad: function (l, m, j, k) {
            var h = document.getElementById(l);
            var i = function () {
                var n = changjie.iframe(l, frames);
                if (formIframesData[l] != undefined) {
                    formIframes[formIframesData[l]] = n
                } else {
                    formIframesData[l] = formIframes.length;
                    formIframes.push(n)
                }
                if (!!n.$) {
                    j(n, k)
                }
            };
            if (h.attachEvent) {
                h.attachEvent("onload", i)
            } else {
                h.onload = i
            }
            $("#" + l).attr("src", top.$.rootUrl + m)
        }
    };
    page.init();
};