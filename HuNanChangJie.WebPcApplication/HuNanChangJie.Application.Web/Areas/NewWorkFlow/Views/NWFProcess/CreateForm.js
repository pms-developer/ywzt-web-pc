﻿var acceptClick;
var bootstrap = function ($, changjie) {
    var shcemeCode = request("shcemeCode");
    var processId = request("processId");
    var taskId = request("taskId");
    var $currentNode = changjie.frameTab.currentIframe().nwflow.currentNode;
    var $nodes = changjie.frameTab.currentIframe().nwflow.schemeObj.nodes;
    var schemeList = {};
    var page = {
        init: function () {
            if ($currentNode.isTitle != "1") {
                $("#F_Title").parent().remove();
            }
            if ($currentNode.isNext == "1") {
                $.each($nodes,
                    function (k, node) {
                        schemeList[node.id] = node;
                    });
                var j = {
                    code: shcemeCode,
                    processId: processId,
                    taskId: taskId,
                    nodeId: $currentNode.id,
                    operationCode: "agree"
                };
                changjie.httpAsync("GET",
                    top.$.rootUrl + "/NewWorkFlow/NWFProcess/GetNextAuditors",
                    j,
                    function(data) {
                        var k = $("#form .mk-scroll-box");
                        if (data) {
                            $.each(data,
                                function(m, auditor) {
                                    if (auditor.length > 1) {
                                        k.append(
                                            '<div class="col-xs-12 mk-form-item"><div class="mk-form-item-title language" >' +
                                            schemeList[m].name +
                                            '</div><div id="' +
                                            m +
                                            '" class="nodeId" ></div></div >');
                                        $("#" + m).mkselect({
                                            type: "multiple",
                                            data: auditor,
                                            value: "Id",
                                            text: "Name"
                                        });
                                    }
                                });
                        }
                    });
            }
        }
    };
    acceptClick = function (callback) {
        if (!$("#form").mkValidform()) {
            return false;
        }
        var formdata = $("#form").mkGetFormData();
        var jnode = {};
        $("#form").find(".nodeId").each(function () {
            var n = $(this);
            var o = n.attr("id");
            if (formdata[o]) {
                jnode[o] = formdata[o];
            }
        });
        var argeeddata = {
            level: formdata.F_Level
        };
        if ($currentNode.isTitle == "1") {
            argeeddata.title = formdata.F_Title;
        }
        callback(argeeddata, jnode);
        return true;
    };
    page.init();
};