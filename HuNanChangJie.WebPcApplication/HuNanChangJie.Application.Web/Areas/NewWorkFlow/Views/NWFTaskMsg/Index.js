﻿/* 
 * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-03-16 16:04
 * 描  述：NWFTaskMsg
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/NewWorkFlow/NWFTaskMsg/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').GridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/NewWorkFlow/NWFTaskMsg/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/NewWorkFlow/NWFTaskMsg/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/NewWorkFlow/NWFTaskMsg/GetPageList',
                headData: [
                    { headerName: "流程进程主键", field: "F_ProcessId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "流程任务主键", field: "F_TaskId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务发送人主键", field: "F_FromUserId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务发送人账号", field: "F_FromUserAccount", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务发送人名称", field: "F_FromUserName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务接收人主键", field: "F_ToUserId", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务接收人账号", field: "F_ToAccount", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务接收人名称", field: "F_ToName", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务标题", field: "F_Title", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务内容", field: "F_Content", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任务创建时间", field: "CreationDate", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "是否结束1结束0未结束", field: "F_IsFinished", width: 100, cellStyle: { 'text-align': 'left' } },
                ],
                mainId:'F_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
