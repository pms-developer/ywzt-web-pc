﻿using System.Web.Mvc;
namespace HuNanChangJie.Application.Web.Areas.NewWorkFlow
{
    public class NewWorkFlowAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "NewWorkFlow"; 
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "NewWorkFlow_default", 
                "NewWorkFlow/{controller}/{action}/{id}", 
                             new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
