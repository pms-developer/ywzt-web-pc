﻿var keyValue = request('keyValue');
var formId = request("formId");
var hiprintTemplate;
var printData = {};
var serverData = null;
var printGroups = null;
var dataView = [];
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (formId) {
                if (keyValue) {
                    page.bind();
                    page.print();
                    Changjie.layerClose(window.name);
                }
                else {
                    Changjie.alert.warning("未检测需要打印信息！");
                }
            } else {
                Changjie.alert.warning("未检测到表单ID,请与系统管理员联系！");
            }
        },
        bind: function () {
            var param = {
                formId: formId,
                keyValue: keyValue
            };
            var data = Changjie.httpGet(top.$.rootUrl + '/PrintModule/Print/GetPrintTemplateInfo', param).data;
            if (data) {
                var mainData = data.MainInfo[0];
                for (var p in mainData) {
                    printData[p] = mainData[p];
                }
                var subList = data.SubsInfo;
                for (var p in subList) {
                    var paramname = "";
                    if (subList[p].IsCustomSql == true) {
                        paramname = subList[p].Name + "_view";
                    }
                    else {
                        paramname = subList[p].Name;
                    }

                    printData[paramname] = subList[p].Data;
                }
                dataView = JSON.parse(data.ExtendView);
                page.jsonToPrintTempate(data.PrintTemplateJson);
            }
        },
        jsonToPrintTempate: function (data) {
            hiprint.init({
                providers: [new page.loadPrintProvider()]
            });
            hiprintTemplate = new hiprint.PrintTemplate({
                template: JSON.parse(data),
            });
            $('#hiprint-printTemplate').html('');
            hiprintTemplate.design('#hiprint-printTemplate');
        },
        print: function () {
            hiprintTemplate.print(printData);
        },
        loadPrintProvider: (function () {
            return function (options) {
                var data = serverData || Changjie.httpGet("/PrintModule/Print/GetPrintProvider", { formId: formId });
                serverData = data;
                var nodes = [];
                printGroups = data.data.PrintGroups;
                for (var i in printGroups) {
                    var item = printGroups[i];
                    var items = [];
                    for (var j in item.PrintItems) {
                        var info = item.PrintItems[j];

                        items.push(info);
                        if (info.SubTables) {//子表信息
                            for (var sub in info.SubTables) {
                                var subItem = info.SubTables[sub];

                                if (subItem.columns) {
                                    var a = [];
                                    a.push(subItem.columns);
                                    info.columns = a;
                                }
                            }

                            for (var _i in dataView) {
                                var view = dataView[_i];
                                var viewinfo = {
                                    columnAlignEditable: true,
                                    columnDisplayEditable: true,
                                    columnDisplayIndexEditable: true,
                                    columnResizable: true,
                                    columnTitleEditable: true,
                                    custom: false,
                                    customText: null,
                                    data: null,
                                    editable: true,
                                    field: view.tablename + "_view",
                                    options: null,
                                    tid: "configModule." + view.tablename + "_view",
                                    title: view.tablename + "数据视图",
                                    type: "table"
                                };
                                viewinfo.columns = [];
                                viewinfo.columns.push(view.columns);
                                items.push(viewinfo);
                            }
                        }

                    }
                    var group = new hiprint.PrintElementTypeGroup(item.GroupName, items);
                    nodes.push(group);
                }
                var addElementTypes = function (context) {
                    context.addPrintElementTypes(
                        "testModule", nodes
                    );
                };
                return {
                    addElementTypes: addElementTypes
                };
            }
        })()
    };
    page.init();
};