﻿/* 
 * 创建人：超级管理员
 * 日  期：220-07-14
 * 描  述：打印模板
 */
var acceptClick;
var keyValue = request('keyValue');
var formId = request("formId");
var mainId = "";
var previewData = {};
var printGroups = null;
var hiprintTemplate;
var dataView = [];
var serverData = null;
var type = "add";
var bootstrap = function ($, Changjie) {
    "use strict";


    var page = {
        init: function () {
            if (!!keyValue) {
                type = "edit";
            }
            else {
                keyValue = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            $("#PrintElementOptionSetting").mkscroll();
            page.bind();
            page.printInit();
            page.initData();
        },
        bind: function () {

            $("#save").on("click", function () {
                acceptClick();
            });
            $("#a3,#a4,#a5,#b3,#b4,#b5").on("click", function () {
                page.setPaper($(this).html());
            });
            $("#cratetojson").on("click", function () {
                page.getJsonTid();
            });
            $("#jsontotemplate").on("click", function () {
                hiprintTemplate = new hiprint.PrintTemplate({
                    template: JSON.parse($('#textarea').val())
                });
                $('#hiprint-printTemplate').html('');
                hiprintTemplate.design('#hiprint-printTemplate');
            });

            $("#createhtml").on("click", function () {
                page.getHtml();
            });

            $("#jsontopreview").on("click", function () {
                page.jsonPreview();
            });

            $("#preview").on("click", function () {
                page.preview();
            });

            $("#customconfrim").on("click", function () {
                page.setPaper($('#customWidth').val(), $('#customHeight').val());
            });

            $("#print").on("click", function () {
                page.directPrint();
            });

            $("#rotate").on("click", function () {
                page.rotatePaper();
            });

            $("#clear").on("click", function () {
                page.clearTemplate();
            });
        },
        initData: function () {
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            if (type == "edit") {
                $.mkSetForm(top.$.rootUrl + '/PrintModule/Print/GetFormData?keyValue=' + keyValue, function (data) {
                    if (data) {
                        $("#Name").val(data.Name);
                        dataView = JSON.parse(data.ExtendView);
                        if (dataView) {
                            page.refreshLeftMenu();
                            for (var _i in dataView) {
                                var item = dataView[_i];
                                $("#sql" + item.tablename).val(item.sqlview);
                            }
                        }
                        page.jsonToPrintTempate(data.JsonData);
                    }
                });
            }
        },
        printInit: function () {
            //初始化打印插件
            hiprint.init({
                providers: [new page.configElementTypeProvider()]
            });
          
            //hiprint.PrintElementTypeManager.build('.hiprintEpContainer', 'testModule');
            //设置左侧拖拽事件
            hiprint.PrintElementTypeManager.build('.hiprintEpContainer', 'testModule');

            hiprintTemplate = new hiprint.PrintTemplate({
                template: null,
                settingContainer: '#PrintElementOptionSetting',
                paginationContainer: '.hiprint-printPagination'
            });
            //打印设计
            hiprintTemplate.design('#hiprint-printTemplate');
        },
        setPaper: function (paperTypeOrWidth, height) {
            hiprintTemplate.setPaper(paperTypeOrWidth, height);
        },
        jsonToPrintTempate: function (data) {
            
                hiprintTemplate = new hiprint.PrintTemplate({
                    template: JSON.parse(data),
                    settingContainer: '#PrintElementOptionSetting',
                    paginationContainer: '.hiprint-printPagination'
                });
                $('#hiprint-printTemplate').html('');
                hiprintTemplate.design('#hiprint-printTemplate');
        },
        configElementTypeProvider: (function () {
            return function (options) {
                var data = serverData||Changjie.httpGet("/PrintModule/Print/GetPrintProvider", { formId: formId});
                serverData = data;
                var nodes = [];
                printGroups = data.data.PrintGroups;
                for (var i in printGroups) {
                    var item = printGroups[i];
                    if (item.MainIdValue) {
                        mainId = item.MainIdValue;
                    }
                    var items = [];
                    for (var j in item.PrintItems) {
                        var info = item.PrintItems[j];
                        items.push(info);
                        if (info.SubTables) {//子表信息
                            for (var sub in info.SubTables) {
                                var subItem = info.SubTables[sub];
                                if (dataView.length == 0) {//第一加载时执行
                                    page.setDataViewPanel(subItem);
                                }

                                if (subItem.columns) {
                                    var a = [];
                                    a.push(subItem.columns);
                                    info.columns = a;
                                }
                                previewData[info.field] = subItem.TableValues;
                            }
                            for (var _i in dataView) {
                                var view = dataView[_i];
                                if (info.field != view.tablename) continue;
                                var viewinfo = {
                                    columnAlignEditable: true,
                                    columnDisplayEditable: true,
                                    columnDisplayIndexEditable: true,
                                    columnResizable: true,
                                    columnTitleEditable: true,
                                    custom: false,
                                    customText: null,
                                    data: null,
                                    editable: true,
                                    field: view.tablename+"_view",
                                    options: null,
                                    tid: "configModule." + view.tablename + "_view",
                                    title: view.tablename+"数据视图",
                                    type: "table"
                                };
                                viewinfo.columns = [];
                                viewinfo.columns.push(view.columns);
                                previewData[view.tablename + "_view"] = view.data;
                                items.push(viewinfo);
                            }
                            
                        }
                        else {
                            previewData[info.field] = info.data;
                        }
                         
                    }
                    var group = new hiprint.PrintElementTypeGroup(item.GroupName, items);
                    nodes.push(group);
                }
                var addElementTypes = function (context) {
                    context.addPrintElementTypes(
                        "testModule", nodes
                    );
                };
                return {
                    addElementTypes: addElementTypes
                };
            }
        })(),
        setDataViewPanel: function (subItem) {
            $("#ul_tabs").append("<li><a data-value='" + subItem.TableName + "'>" + subItem.TableName + "视图设置</a></li>");
            var $tab = $('<div class="mk-form-wrap tab-pane" id="' + subItem.TableName + '"></div>');
            $("#tab_content_sub").append($tab);
            var $input = $('<textarea id="sql' + subItem.TableName + '" name="' + subItem.TableName + '" class="form-control" style="height: 70px;"></textarea>');
            var $btn = $('<button class="hiprint-option-item-settingBtn hiprint-option-item-submitBtn" totarget="' + subItem.TableName + '" type="button">刷新子表</button>');
            $btn.on("click", function () {
                var inputid = $(this).attr("totarget");
                var $sqlinput = $("#sql" + inputid);
                var sql = $sqlinput.val();
                if (sql.indexOf('@mainId@') > 0) {
                    sql = sql.replace("@mainId@", "'" + mainId + "'");
                }
                if (sql) {
                    var param = {
                        sql: sql
                    };
                    var data = top.Changjie.httpGet(top.$.rootUrl + "/SystemModule/CustmerQuery/GetDataTableBySql", param).data;

                    var isexist = false;
                    for (var i in dataView) {
                        var item = dataView[i];
                        if (item.tablename != inputid) continue;
                        isexist = true;
                        item.columns = page.getDataTableCells(data);
                        item.data = data;
                        item.sqlview = $sqlinput.val();
                        break;
                    }

                    if (isexist == false) {
                        var item = {
                            tablename: inputid,
                            columns: page.getDataTableCells(data),
                            data: data,
                            sqlview: $sqlinput.val()
                        }
                        dataView.push(item);
                    }
                    page.refreshLeftMenu();
                }
            });
            $tab.append($input);
            $tab.append($btn);
        },
        refreshLeftMenu: function () {
            $(".hiprintEpContainer").html("");

            hiprint.init({
                providers: [new page.configElementTypeProvider()]
            });
            //设置左侧拖拽事件
            hiprint.PrintElementTypeManager.build('.hiprintEpContainer', 'testModule');
        },
        getDataTableCells: function (data) {
            var columns = [];
            var cells = data[0];
            for (var p in cells) {
                var column = {
                    title: p,
                    align: "center",
                    field: p,
                    width: 100
                };
                columns.push(column);
            }
            return columns;
        },
        getJsonTid:function () {
            var printJsonData = JSON.stringify(hiprintTemplate.getJsonTid());
            //$("#textarea").val(printJsonData);
            return printJsonData;
        },
        getHtml: function () {
            var printHtmlData = hiprintTemplate.getHtml(previewData)[0].outerHTML;
            //$("#textarea_html").val(printHtmlData);
            return printHtmlData;
        },
        jsonPreview: function () {
            var testTemplate = new hiprint.PrintTemplate({template: JSON.parse($("#textarea").val()) });
            $('#myModal .modal-body .prevViewDiv').html(testTemplate.getHtml(previewData));
            $('#myModal').modal('show');
        },
        preview: function () {
            $('#myModal .modal-body').html(hiprintTemplate.getHtml(previewData));
            $('#myModal').modal('show');
        },
        directPrint: function () {
            hiprintTemplate.print(previewData);
        },
        rotatePaper: function () {
            hiprintTemplate.rotatePaper();
        },
        clearTemplate: function () {
            hiprintTemplate.clear();
        }
    };
    // 保存数据
    acceptClick = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var postJson = {};
        postJson.Name=$('#Name').val();
        postJson.JsonData = page.getJsonTid();
        postJson.HtmlData = page.getHtml();
        postJson.ExtendView = JSON.stringify(dataView);
        postJson.FormId = formId;
        var postData = {
            strEntity: JSON.stringify(postJson)
        };
        $.mkSaveForm(top.$.rootUrl + '/PrintModule/Print/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
        });
    };
    page.init();
}
