﻿/* * Copyright (c)  
 * 创建人：超级管理员
 * 日  期：2020-07-14 14:21
 * 描  述：打印模板
 */
var refreshGirdData;
var moduleId;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var formId;
    var page = {
        init: function () {
            page.inittree();
            page.initGird();
            page.bind();
        },
        bind: function () {
            $("#add,#edit,#delete,#start").attr("disabled", "disabled");
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                top.Changjie.frameTab.open({
                    F_ModuleId: 'setPrintTemplate',
                    F_Icon: 'fa fa-file-text-o',
                    F_FullName: '打印模板设计',
                    F_UrlAddress: '/PrintModule/Print/Form?formId='+ formId
                });
            });

            $("#start").on("click", function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    var url = top.$.rootUrl + "/PrintModule/Print/SetEnableState";
                    Changjie.httpAsyncPost(url, { keyValue: keyValue, formId: formId }, function (data) {
                        if (data.code == Changjie.httpCode.success) {
                            page.search({ formId: formId});
                        }
                        
                    });
                }
            });



        },
        inittree: function () {
            $('#module_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/Module/GetModuleTree',
                nodeClick: page.treeNodeClick
            });
        },
        treeNodeClick: function (item) {
            function getUrlParam(url, name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
                var r = url.substr(1).match(reg);  //匹配目标参数
                if (r != null) return unescape(r[2]); return null; //返回参数值
            }

            if (item.url) {
                formId = getUrlParam(item.url, "formId");
                
            } else {
                formId = 'null';
            }

            $('#titleinfo').text(item.text);
            if (!!formId == false || formId == "null") {
                $("#add,#edit,#delete,#start").attr("disabled", "disabled");
                formId = 'null';
            }
            else {
                $("#add,#edit,#delete,#start").removeAttr("disabled");
            }
            page.search({ formId: formId });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PrintModule/Print/GetPageList',
                headData: [
                    { label: "名称", name: "Name", width: 250, align: "left" },
                    {
                        label: "创建时间", name: "CreationDate", width: 250, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "创建人", name: "CreationName", width: 250, align: "left" },
                    {
                        label: "启用状态", name: "Enabled", width: 350, align: "center",
                        formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case true:
                                    return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                                default:
                                    return '<span class="label label-default" style="cursor: pointer;">未启用</span>';
                            }
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/PrintModule/Print/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        var fid = $('#gridtable').jfGridValue('FormId');
        top.Changjie.frameTab.open({
            F_ModuleId: 'setPrintTemplate',
            F_Icon: 'fa fa-file-text-o',
            F_FullName: '编辑打印模板',
            F_UrlAddress: '/PrintModule/Print/Form?formId=' + fid + "&keyValue=" + keyValue + "&formId=" + formId, 
        });
    }
};
