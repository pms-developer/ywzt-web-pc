﻿using HuNanChangJie.Application.Print;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.PrintModule.Controllers
{
    public class PrintController : MvcControllerBase
    {
        private IPrint printIBll = new PrintBLL();
        // GET: Print/Print
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Preview()
        {
            return View();
        }

        public ActionResult Form()
        {
            return View();
        }

        //[HttpGet]
        //public ActionResult GetPrintProvider()
        //{
        //    var data = printIBll.GetPrintProvider();
        //    return Success(data);
        //}

        [HttpGet,AjaxOnly]
        public ActionResult GetPrintProvider(string formId)
        {
            var data = printIBll.GetPrintProvider(formId);
            return Success(data);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var userInfo = LoginUserInfo.UserInfo;
            var data = printIBll.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var info = printIBll.GetPrintTemplateEntity(keyValue);

            return Success(info);
        }

        [HttpGet, AjaxOnly]
        public ActionResult StatisticsPrintCount(string table,string infoid)
        {
            printIBll.StatisticsPrintCount(table, infoid);
            return Success("");
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetPrintTemplateInfo(string formId, string keyValue)
        {
            var data = printIBll.GetPrintTemplateInfo(formId,keyValue);
            return Success(data);
        }

        [HttpPost,AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            printIBll.Delete(keyValue);
            return Success("删除成功！");
        }

        [HttpPost,ValidateAntiForgeryToken,AjaxOnly,ValidateInput(false)]
        public ActionResult SaveForm(string keyValue, string type, string strEntity)
        {
            var entity = strEntity.ToObject<PrintTemplateEntity>();
            printIBll.SaveEntity(keyValue, entity, type);
            return Success("保存成功！");
        }

        [HttpPost,AjaxOnly]
        public ActionResult SetEnableState(string keyValue, string formId)
        {
            printIBll.SetEnableState(keyValue, formId);
            return Success("保存成功！");
        }

    }
}