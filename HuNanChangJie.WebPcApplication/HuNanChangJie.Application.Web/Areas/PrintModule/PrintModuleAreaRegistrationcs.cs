﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.PrintModule
{
    public class PrintModuleAreaRegistrationcs : AreaRegistration
    {
        public override string AreaName => "PrintModule";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PrintModule_default",
                "PrintModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
                );
        }
    }
}