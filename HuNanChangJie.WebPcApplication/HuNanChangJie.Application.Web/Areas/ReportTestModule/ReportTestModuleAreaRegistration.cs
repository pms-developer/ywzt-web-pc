﻿using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.ReportTestModule
{
    public class ReportTestModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ReportTestModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ReportTestModule_default",
                "ReportTestModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}