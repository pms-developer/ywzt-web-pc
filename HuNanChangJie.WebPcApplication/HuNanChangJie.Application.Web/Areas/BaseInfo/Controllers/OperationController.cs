﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Application.TwoDevelopment;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 19:00
    /// 描 述：作业档案信息
    /// </summary>
    public class OperationController : MvcControllerBase
    {
        private OperationIBLL operationIBLL = new OperationBLL();
        private PriceSheetIBLL priceSheetIBLL = new PriceSheetBLL();

        public OperationController()
        {
            operationIBLL = MyInterceptor.InterCreateObj(operationIBLL);
        }


        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        [HttpGet]
        public ActionResult LogForm()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = operationIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取操作日志信息
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetLog(string itemName)
        {
            XqPagination paginationobj = new XqPagination { rows = 10000, page = 1 };
            var queryJson = (new { ItemName = itemName }).ToJson();
            var data = operationIBLL.GetPageList(paginationobj, queryJson);
            List<LogResult> list = new List<LogResult>();
            if (data.Count() > 0)
            {
                foreach (var itemKeyValue in data)
                {
                    List<LogResult> logList = MyInterceptor.getLogResult("OperationIBLL", itemKeyValue.ID);
                    foreach (var item in logList)
                    {
                        item.keyValue = itemKeyValue.ItemName;
                        if (item.detailList != null)
                        {
                            item.detailList = item.detailList.Where(x => (!string.IsNullOrEmpty(x.updateKey) && x.updateKey.IndexOf("Cell_Edit") < 0) || x.operType != "Update").ToList();
                        }
                    }
                    list.AddRange(logList);
                }
            }

            var deletedata = operationIBLL.GetMeioErpDeleteOperationList(itemName);
            if (deletedata.Count() > 0)
            {
                foreach (var itemKeyValue in deletedata)
                {
                    List<LogResult> logList = MyInterceptor.getLogResult("OperationIBLL", itemKeyValue.DeleteID);
                    foreach (var item in logList)
                    {
                        item.keyValue = itemKeyValue.Code;
                        if (item.detailList != null)
                        {
                            item.detailList = item.detailList.Where(x => (!string.IsNullOrEmpty(x.updateKey) && x.updateKey.IndexOf("Cell_Edit") < 0) || x.operType != "Update").ToList();
                        }
                    }
                    list.AddRange(logList);
                }
            }

            if (list.Count() > 0)
            {
                var result = list.ToList().OrderByDescending(p => p.operTime);
                return Success(result);
            }
            else
            {
                return Fail("没有数据");
            }
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpOperationData = operationIBLL.GetMeioErpOperationEntity( keyValue );
            var MeioErpOperationDetailData = operationIBLL.GetMeioErpOperationDetailList( MeioErpOperationData.ID );
            var jsonData = new {
                MeioErpOperation = MeioErpOperationData,
                MeioErpOperationDetail = MeioErpOperationDetailData,
            };
            return Success(jsonData);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetOperationDetail(string ids)
        {
            var data = operationIBLL.GetMeioErpOperationDetailListByOperationIDs(ids).OrderBy(o=>o.MeioErpOperationID).ThenBy(o=>o.MinValue);
            return Success(data);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var isEdit = priceSheetIBLL.IsEditForm(keyValue);
            if (isEdit)
            {
                return Fail("不能删除该档案信息，报价单已经使用该档案信息");
            }
            var MeioErpOperationData = operationIBLL.GetMeioErpOperationEntity(keyValue);
            var MeioErpOperationDetailData = operationIBLL.GetMeioErpOperationDetailList(keyValue);

            var jsonData = new
            {
                MeioErpOperation= MeioErpOperationData,
                MeioErpOperationDetail= MeioErpOperationDetailData.OrderBy(p=>p.MinValue)
            };
            var deleteList = jsonData.ToJson();
            operationIBLL.DeleteEntity(keyValue,deleteList);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strmeioErpOperationDetailList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpOperationEntity>();
            var meioErpOperationDetailList = strmeioErpOperationDetailList.ToObject<List<MeioErpOperationDetailEntity>>();
            if (type == "edit")
            {
                var isEdit = priceSheetIBLL.IsEditForm(keyValue);
                if (isEdit)
                {
                    return Fail("不能修改该档案信息，报价单已经使用该档案信息");
                }

                var MeioErpOperationData = operationIBLL.GetMeioErpOperationEntity(keyValue);
                var MeioErpOperationDetailData = operationIBLL.GetMeioErpOperationDetailList(keyValue);

                var jsonData = new
                {
                    MeioErpOperation = MeioErpOperationData,
                    MeioErpOperationDetail = MeioErpOperationDetailData.OrderBy(p => p.MinValue)
                };
                deleteList = jsonData.ToJson();
            }
            else
            {
                var jsonData = new
                {
                    MeioErpOperation = mainInfo,
                    MeioErpOperationDetail = meioErpOperationDetailList.OrderBy(p => p.MinValue)
                };
                deleteList = jsonData.ToJson();
            }
           
            operationIBLL.SaveEntity(keyValue,mainInfo,meioErpOperationDetailList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsItemName(string name,string id)
        {
            var data = operationIBLL.CheckIsItemName(name,id);
            return Success(data);
        }
        [HttpPost]
        [AjaxOnly]
        public ActionResult SetIsEnable(string ids, bool isEnable)
        {
            var data = operationIBLL.SetIsEnable(ids, isEnable);
            return Success(data);
        }
    }
}
