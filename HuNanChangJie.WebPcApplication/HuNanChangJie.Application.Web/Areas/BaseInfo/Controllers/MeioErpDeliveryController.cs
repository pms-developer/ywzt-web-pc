﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System;
using System.Xml;
using HuNanChangJie.WebRequestService;
using System.Text;
using System.Linq;
using DocumentFormat.OpenXml.Vml.Office;
using Microsoft.Office.Interop.Word;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-04 14:45
    /// 描 述：发货单
    /// </summary>
    public class MeioErpDeliveryController : MvcControllerBase
    {
        private MeioErpDeliveryIBLL meioErpDeliveryIBLL = new MeioErpDeliveryBLL();
        private MeioErpPurchaseIBLL meioErpPurchaseIBLL=new MeioErpPurchaseBLL();
        private GoodsIBLL goodsIBLL=new GoodsBLL();
        private OwnerIBLL ownerIBLL = new OwnerBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        [HttpGet]
        public ActionResult Dialog() {
            return View();
        }
        [HttpGet]
        public ActionResult Audit() {
            return View();
        }
        [HttpGet]
        public ActionResult DialogPurchase() {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpDeliveryIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpDeliveryData = meioErpDeliveryIBLL.GetMeioErpDeliveryEntity( keyValue );
            var MeioErpDeliveryDetailData = meioErpDeliveryIBLL.GetMeioErpDeliveryDetailList( MeioErpDeliveryData.ID );
            var jsonData = new {
                MeioErpDelivery = MeioErpDeliveryData,
                MeioErpDeliveryDetail = MeioErpDeliveryDetailData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var deliveryEntity = meioErpDeliveryIBLL.GetMeioErpDeliveryEntity(keyValue);
            if (deliveryEntity != null)
            {
                if (deliveryEntity.AuditStatus == "1")
                {
                    return Success("删除失败，发货单已审核通过，不能删除");
                }
                else
                {
                    meioErpDeliveryIBLL.DeleteEntity(keyValue);
                    return Success("删除成功！");
                }
            }
            else
            {
                return Fail("删除失败，发货单不存在或已删除");
            }
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strmeioErpDeliveryDetailList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpDeliveryEntity>();
            var meioErpDeliveryDetailList = strmeioErpDeliveryDetailList.ToObject<List<MeioErpDeliveryDetailEntity>>();

            if (mainInfo != null)
            {
                StringBuilder strError= new StringBuilder();
                //校验发货数量是否大于采购数量
                var meioPurchaseDetailList = meioErpPurchaseIBLL.GetMeioErpPurchaseDetailList(mainInfo.PurchaseID);
                int oldDeliveryQuantity = 0;
                if (!!string.IsNullOrEmpty(keyValue))
                {
                    var meioErpDeliveryDetailQueryList = meioErpDeliveryIBLL.GetMeioErpDeliveryDetailList(keyValue);
                }
                foreach (var item in meioErpDeliveryDetailList)
                {
                    var meioPurchaseDetailQuery = meioPurchaseDetailList.FirstOrDefault(p => p.PurchaseID == item.PurchaseID && p.GoodsCode == item.GoodsCode);
                    if (meioPurchaseDetailQuery != null)
                    {
                        if (item.PurchaseQuantity- meioPurchaseDetailQuery.DeliveryQuantity+oldDeliveryQuantity < item.DeliveryQuantity)
                        {
                            strError.Append($"货品编码：{item.GoodsCode}  采购数量：{item.PurchaseQuantity} 已发货数量：{meioPurchaseDetailQuery.DeliveryQuantity} 本次发货数量不能超过：{item.PurchaseQuantity- meioPurchaseDetailQuery.DeliveryQuantity + oldDeliveryQuantity}  ；");
                        }
                    }
                }
                if (strError.Length > 0)
                {
                    return Fail(strError.ToString());
                }
            }

            meioErpDeliveryIBLL.SaveEntity(keyValue,mainInfo,meioErpDeliveryDetailList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult AuditDelivery(string keyValue, int auditResult, string remark)
        {
            var delivery = meioErpDeliveryIBLL.GetMeioErpDeliveryEntity(keyValue);
            if (string.IsNullOrEmpty(delivery.AuditStatus) || delivery.AuditStatus == "0")
            {
                var meioErpDocumentAuditRecordEntity = new MeioErpDocumentAuditRecordEntity
                {
                    DocumentID = keyValue,
                    DocumentType = "Delivery",
                    AuditDate = DateTime.Now,
                    AuditResult = auditResult,
                    Remark = remark
                };
                meioErpDeliveryIBLL.AuditDelivery(keyValue, meioErpDocumentAuditRecordEntity, delivery);

                return Success("审核成功");
            }
            else
            {
                return Fail("审核失败，发货单已审核");
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult UnAuditDelivery(string keyValue)
        {
            var delivery = meioErpDeliveryIBLL.GetMeioErpDeliveryEntity(keyValue);
            if (!string.IsNullOrEmpty(delivery.AuditStatus) && delivery.AuditStatus != "0")
            {
                var meioErpDocumentAuditRecordEntity = new MeioErpDocumentAuditRecordEntity
                {
                    DocumentID = keyValue,
                    DocumentType = "Delivery",
                    AuditDate = DateTime.Now,
                    AuditResult = 0,
                    Remark = "反审核"
                };
                meioErpDeliveryIBLL.AuditDelivery(keyValue, meioErpDocumentAuditRecordEntity,delivery);
                return Success("审核成功");
            }
            else
            {
                return Fail("反审核失败，发货单待审核");
            }

        }
    }
}
