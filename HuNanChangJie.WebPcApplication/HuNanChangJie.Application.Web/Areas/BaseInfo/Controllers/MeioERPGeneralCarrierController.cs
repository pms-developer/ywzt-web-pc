﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-02 16:39
    /// 描 述：承运商信息
    /// </summary>
    public class MeioERPGeneralCarrierController : MvcControllerBase
    {
        private MeioERPGeneralCarrierIBLL meioERPGeneralCarrierIBLL = new MeioERPGeneralCarrierBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioERPGeneralCarrierIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioERPGeneralCarrierData = meioERPGeneralCarrierIBLL.GetMeioERPGeneralCarrierEntity( keyValue );
            var MeioERPGeneralCarrierHistoricalRecordData = meioERPGeneralCarrierIBLL.GetMeioERPGeneralCarrierHistoricalRecordList( MeioERPGeneralCarrierData.ID );
            var jsonData = new {
                MeioERPGeneralCarrier = MeioERPGeneralCarrierData,
                MeioERPGeneralCarrierHistoricalRecord = MeioERPGeneralCarrierHistoricalRecordData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioERPGeneralCarrierIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioERPGeneralCarrierEntity>();
            meioERPGeneralCarrierIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion
        [HttpPost]
        [AjaxOnly]
        public ActionResult SetIsEnable(string ids, bool isEnable)
        {
            var data = meioERPGeneralCarrierIBLL.SetIsEnable(ids, isEnable);
            return Success(data);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsGeneralCarrierCode(string code, string id)
        {
            var data = meioERPGeneralCarrierIBLL.CheckIsGeneralCarrierCode(code, id);
            return Success(data);
        }
    }
}
