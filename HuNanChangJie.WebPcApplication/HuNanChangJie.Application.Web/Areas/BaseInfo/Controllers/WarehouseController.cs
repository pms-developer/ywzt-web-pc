﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-24 17:47
    /// 描 述：仓库管理
    /// </summary>
    public class WarehouseController : MvcControllerBase
    {
        private WarehouseIBLL warehouseIBLL = new WarehouseBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = warehouseIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpWarehouseData = warehouseIBLL.GetMeioErpWarehouseEntity( keyValue );
            var jsonData = new {
                MeioErpWarehouse = MeioErpWarehouseData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var warehouseEntity= warehouseIBLL.GetMeioErpWarehouseEntity(keyValue);
            if (warehouseEntity != null)
            {
                if (warehouseEntity.IsValid.Value)
                {
                    return Fail("删除失败，仓库已启用不能删除");
                }
                else
                {
                    warehouseIBLL.DeleteEntity(keyValue);
                    return Success("删除成功！");
                }
            }
            else
            {
                return Fail("删除失败，仓库已删除或不存在");
            }
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpWarehouseEntity>();
            warehouseIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion
        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsWarehouseName(string name, string id)
        {
            var result = warehouseIBLL.CheckIsWarehouseName(name, id);
            return Success(result);
        }
        [HttpPost]
        [AjaxOnly]
        public ActionResult SetIsValid(string ids, bool isvalid)
        {
            var data = warehouseIBLL.SetIsValid(ids, isvalid);
            return Success(data);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsCode(string code, string id)
        {
            var data = warehouseIBLL.CheckIsCode(code, id);
            return Success(data);
        }
    }
}
