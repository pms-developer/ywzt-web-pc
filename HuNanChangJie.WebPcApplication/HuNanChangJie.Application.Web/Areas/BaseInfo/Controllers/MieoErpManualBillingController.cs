﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using Aliyun.OSS;
using System.Web;
using System;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-12 14:30
    /// 描 述：手工账单
    /// </summary>
    public class MieoErpManualBillingController : MvcControllerBase
    {
        private MieoErpManualBillingIBLL mieoErpManualBillingIBLL = new MieoErpManualBillingBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = mieoErpManualBillingIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MieoErpManualBillingData = mieoErpManualBillingIBLL.GetMieoErpManualBillingEntity( keyValue );
            var jsonData = new {
                MieoErpManualBilling = MieoErpManualBillingData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            mieoErpManualBillingIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }


        /// <summary>
        /// 审批实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult AuditingForm(string keyValue)
        {
            try
            {
                mieoErpManualBillingIBLL.AuditingEntity(keyValue);
                return Success("审批成功！");
            }
            catch(Exception ex)
            {
                return Fail(ex.Message);
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MieoErpManualBillingEntity>();
            mieoErpManualBillingIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion



        [HttpPost]
        [AjaxOnly]
        public ActionResult UpLoadFile(HttpPostedFileBase file)
        {
            // 获取上传文件的扩展名
            if (file == null)
            {
                return Success("");
            }
            var extension = System.IO.Path.GetExtension(file.FileName);
            string[] extList = new string[] { ".xls", ".xlsx", ".pdf", ".png", ".doc", ".docx", ".zip",".rar" };
            if (!extList.Contains(extension))
            {
                return Fail("不支持该格式");
            }
            string fullFileName = Guid.NewGuid().ToString() + extension;
            // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
            var endpoint = HuNanChangJie.Util.Config.GetValue("ossEndpoint");
            // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
            var accessKeyId = HuNanChangJie.Util.Config.GetValue("ossKey");
            var accessKeySecret = HuNanChangJie.Util.Config.GetValue("ossSecret");
            // 填写Bucket名称，例如examplebucket。
            var bucketName = HuNanChangJie.Util.Config.GetValue("ossBucketName");
            // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
            string dt = DateTime.Now.ToString("yyyy-MM-dd");
            var objectName = "zhongtai/" + dt + "/" + fullFileName;
            // 创建OssClient实例。
            var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
            try
            {
                // 上传文件。
                client.PutObject(bucketName, objectName, file.InputStream);
                // return Success("https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName);
                fullFileName = HuNanChangJie.Util.Config.GetValue("ossSite") + objectName;
            }
            catch (Exception)
            {
                throw new ExceptionEx("上传文件异常", new Exception());
            }

            return Success(fullFileName);
        }

    }
}
