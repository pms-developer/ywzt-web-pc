﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo.Owner;
using Aliyun.OSS;
using HuNanChangJie.Application.Excel;
using System.Web;
using System;
using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-15 15:22
    /// 描 述：货主管理
    /// </summary>
    public class OwnerController : MvcControllerBase
    {
        private OwnerIBLL ownerIBLL = new OwnerBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        [HttpGet]
        public ActionResult AccountBalanceInfo()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ExternalInformation()
        {
            return View();
        }
        [HttpGet]
        public ActionResult TopUpDialog()
        {
            return View();
        }
        [HttpGet]
        public ActionResult UpdateCreditLimit()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Recharge()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = ownerIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetAccountBalancePageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = ownerIBLL.GetAccountBalancePageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
         /// 获货主仓库
         /// <summary>
         /// <returns></returns>
        [AjaxOnly]
        public ActionResult GetOwnerWarehouseList(string keyValue)
        {
            var GetOwnerWarehouseListData = ownerIBLL.GetOwnerWarehouseList(keyValue);

            var data = new
            {
                Warehousegridtable = GetOwnerWarehouseListData
            };
            return Success(data);
        }
        /// <summary>
        /// 获货主仓库聚道信息
        /// <summary>
        /// <returns></returns>
        [AjaxOnly]
        public ActionResult GetOwnerChannelInformationList(string keyValue)
        {
            var GetOwnerChannelInformationData = ownerIBLL.GetOwnerChannelInformationList(keyValue);

            var data = new
            {
                ChannelInformationgridtable = GetOwnerChannelInformationData
            };
            return Success(data);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpOwnerData = ownerIBLL.GetMeioErpOwnerEntity(keyValue);
            var MeioErpContractData = ownerIBLL.GetMeioErpContractEntity(MeioErpOwnerData.ContractID);
            var MeioErpOwnerThirdpartySystemData = ownerIBLL.GetOwnerThirdpartySystemList(keyValue);
            var MeioErpOwnerSalesPlatformData = ownerIBLL.GetOwnerSalesPlatformList(keyValue);
            if (MeioErpContractData != null)
            {
                MeioErpOwnerData.ServiceStartDate = MeioErpContractData.ServiceStartDate;
                MeioErpOwnerData.ServiceEndDate = MeioErpContractData.ServiceEndDate;
            }
            var jsonData = new {
                MeioErpOwner = MeioErpOwnerData,
                //MeioErpContract = MeioErpContractData,
                MeioErpOwnerThirdpartySystem= MeioErpOwnerThirdpartySystemData,
                MeioErpOwnerSalesPlatform= MeioErpOwnerSalesPlatformData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取客户信息
        /// <summary>
        /// <returns></returns>
        [AjaxOnly]
        public ActionResult GetMeioErpOwnerEntity(string keyValue)
        {
            var MeioErpOwnerData = ownerIBLL.GetMeioErpOwnerEntity(keyValue);
            var data = new
            {
                MeioErpOwnerData
            };
            return Success(data);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var ownerEntity = ownerIBLL.GetMeioErpOwnerEntity(keyValue);
            if (ownerEntity != null)
            {
                if (ownerEntity.IsValid.Value)
                {
                    return Fail("删除失败，货主已启用不能删除");
                }
                else
                {
                    ownerIBLL.DeleteEntity(keyValue);
                    return Success("删除成功！");
                }
            }
            else
            {
                return Fail("删除失败，货主已删除或不存在");
            }
            
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strmeioErpOwnerEntity,string strMeioErpOwnerThirdpartySystem,string strMeioErpOwnerSalesPlatform, string deleteList)
        {
            //var mainInfo = strEntity.ToObject<MeioErpContractEntity>();
            var meioErpOwnerEntity = strmeioErpOwnerEntity.ToObject<MeioErpOwnerEntity>();
            var meioErpOwnerThirdpartySystemList = strMeioErpOwnerThirdpartySystem.ToObject<List<MeioErpOwnerThirdpartySystemEntity>>();
            var meioErpOwnerSalesPlatformList = strMeioErpOwnerSalesPlatform.ToObject<List<MeioErpOwnerSalesPlatformEntity>>();
            ownerIBLL.SaveEntity(keyValue,null,meioErpOwnerEntity,meioErpOwnerThirdpartySystemList,meioErpOwnerSalesPlatformList, deleteList,type);
            return Success("保存成功！");
        }
        /// <summary>
        /// 保存实体数据仓库和物流信息（修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [AjaxOnly]
        public ActionResult SaveWarehouseExternal(string keyValue, string type, string strWarehousegridtable, string strChannelInformationgridtable, string deleteList)
        {
            var Warehousegridtable = strWarehousegridtable.ToObject<List<MeioDataCenterWarehouseEntity>>();
            var ChannelInformation = strChannelInformationgridtable.ToObject<List<MeioDataCenterChannelInformationEntity>>();
            ownerIBLL.SaveWarehouseExternal(keyValue, Warehousegridtable, ChannelInformation, deleteList, type);
            return Success("保存成功！");
        }

        #endregion
        [HttpPost]
        [AjaxOnly]
        public ActionResult SetIsValid(string ids, bool isvalid)
        {
            var data = ownerIBLL.SetIsValid(ids, isvalid);
            return Success(data);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsOwnerCode(string code, string id)
        {
            var data=ownerIBLL.CheckIsOwnerCode(code, id);
            return Success(data);
        }
        [HttpPost]
        [AjaxOnly]
        [ValidateAntiForgeryToken]
        public ActionResult TopUp(HttpPostedFileBase file, string keyValue,decimal amount,string remark)
        {
            try
            {
                #region 上传充值凭证
                // 获取上传文件的扩展名
                if (file != null)
                {

                    var extension = System.IO.Path.GetExtension(file.FileName);
                    //if (extension.Contains("jpg"))
                    //{

                    //}
                    //else
                    //{
                    //    return Fail("不支持该格式");
                    //}
                    long timestamp = ((DateTimeOffset)System.DateTime.Now).ToUnixTimeSeconds();
                    string fullFileName = timestamp + extension;
                    // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
                    var endpoint = "https://oss-cn-shenzhen.aliyuncs.com";
                    // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
                    var accessKeyId = "LTAI5tQnAwRcEBzn3gkTFR7u";
                    var accessKeySecret = "4jgxSlJNgCo6kdSL5t4bMSYPkATui0";
                    // 填写Bucket名称，例如examplebucket。
                    var bucketName = "meio-wms";
                    // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
                    string dt = System.DateTime.Now.ToString("yyyy-MM-dd");
                    var objectName = "erp/" + dt + "/" + fullFileName;
                    // 创建OssClient实例。
                    var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
                    // 上传文件。
                    client.PutObject(bucketName, objectName, file.InputStream);
                    // return Success("https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName);
                    fullFileName = "https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName;

                    ownerIBLL.TopUp(keyValue, amount, fullFileName, remark);
                }
                else
                {
                    ownerIBLL.TopUp(keyValue, amount, "", remark);
                }
                return Success("充值成功");
            }
            catch (Exception e)
            {
                return Fail("充值异常：" + e.Message);
            }
            #endregion
        }
        [HttpPost]
        [AjaxOnly]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateCreditLimits(string keyValue, decimal amount)
        {
            try
            {
                ownerIBLL.UpdateCreditLimits(keyValue, amount);
                return Success("修改额度成功");
            }
            catch (Exception e)
            {
                return Fail("修改额度异常：" + e.Message);
            }
        }
        [HttpPost]
        [AjaxOnly]
        [ValidateAntiForgeryToken]
        public ActionResult Withdraw(string keyValue, string remark)
        {
            try
            {
                ownerIBLL.Withdraw(keyValue, remark);
                return Success("提现成功");
            }
            catch (Exception e)
            {
                return Fail("提现异常：" + e.Message);
            }
        }
    }
}
