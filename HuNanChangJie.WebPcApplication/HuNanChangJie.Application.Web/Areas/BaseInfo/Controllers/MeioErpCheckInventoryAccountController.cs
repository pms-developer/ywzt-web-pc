﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using Castle.Core;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-08-14 16:27
    /// 描 述：盘点账单
    /// </summary>
    public class MeioErpCheckInventoryAccountController : MvcControllerBase
    {
        private MeioErpCheckInventoryAccountIBLL meioErpCheckInventoryAccountIBLL = new MeioErpCheckInventoryAccountBLL();
        private MeioErpAccountAdditionalCostIBLL meioErpAccountAdditionalCostIBLL = new MeioErpAccountAdditionalCostBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpCheckInventoryAccountIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpCheckInventoryAccountData = meioErpCheckInventoryAccountIBLL.GetMeioErpCheckInventoryAccountEntity( keyValue );
            var MeioErpCheckInventoryAccountDetailData = meioErpCheckInventoryAccountIBLL.GetMeioErpCheckInventoryAccountDetailList( MeioErpCheckInventoryAccountData.ID );
            var AccountAdditionalCostData=meioErpAccountAdditionalCostIBLL.GetMeioErpAccountAdditionalCostEntityByAccountIDAndType(MeioErpCheckInventoryAccountData.ID, 5);
            var operatingCost = MeioErpCheckInventoryAccountData.OperatingCost;
            if (AccountAdditionalCostData != null)
            {
                operatingCost = operatingCost - AccountAdditionalCostData.AmountSubtotal;
            }
            MeioErpCheckInventoryAccountDetailData.ForEach(f =>
            {
                f.CheckInventoryUnitPrice = operatingCost / MeioErpCheckInventoryAccountData.CheckInventoryNum;
                f.OperatingCost = operatingCost / MeioErpCheckInventoryAccountData.CheckInventoryNum * f.CheckInventoryNum;
            });

            var jsonData = new {
                MeioErpCheckInventoryAccount = MeioErpCheckInventoryAccountData,
                MeioErpCheckInventoryAccountDetail = MeioErpCheckInventoryAccountDetailData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioErpCheckInventoryAccountIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strmeioErpCheckInventoryAccountDetailList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpCheckInventoryAccountEntity>();
            var meioErpCheckInventoryAccountDetailList = strmeioErpCheckInventoryAccountDetailList.ToObject<List<MeioErpCheckInventoryAccountDetailEntity>>();
            meioErpCheckInventoryAccountIBLL.SaveEntity(keyValue,mainInfo,meioErpCheckInventoryAccountDetailList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
