﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web;
using Aliyun.OSS;
using System;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-12 19:45
    /// 描 述：资金流水
    /// </summary>
    public class MeioErpCapitalFlowController : MvcControllerBase
    {
        private MeioErpCapitalFlowIBLL meioErpCapitalFlowIBLL = new MeioErpCapitalFlowBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpCapitalFlowIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpCapitalFlowData = meioErpCapitalFlowIBLL.GetMeioErpCapitalFlowEntity( keyValue );
            var jsonData = new {
                MeioErpCapitalFlow = MeioErpCapitalFlowData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioErpCapitalFlowIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpCapitalFlowEntity>();
            meioErpCapitalFlowIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

        /// <summary>
        /// 充值与结算
        /// </summary>
        /// <param name="keyValue">客户ID</param>
        /// <param name="Currency">充值币种</param>
        /// <param name="amount">交易金额</param>
        /// <param name="fullFileName">凭证</param>
        /// <param name="remark">备注</param>
        /// <param name="type">type=1代表充值，type=2代表结算</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult RechargeAndSettlement(HttpPostedFileBase file,string keyValue, string Currency, decimal amount, string remark, int type)
        {
            if (file != null)
            {

                var extension = System.IO.Path.GetExtension(file.FileName);
                long timestamp = ((DateTimeOffset)System.DateTime.Now).ToUnixTimeSeconds();
                string fullFileName = timestamp + extension;
                // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
                var endpoint = "https://oss-cn-shenzhen.aliyuncs.com";
                // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
                var accessKeyId = "LTAI5tQnAwRcEBzn3gkTFR7u";
                var accessKeySecret = "4jgxSlJNgCo6kdSL5t4bMSYPkATui0";
                // 填写Bucket名称，例如examplebucket。
                var bucketName = "meio-wms";
                // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
                string dt = System.DateTime.Now.ToString("yyyy-MM-dd");
                var objectName = "erp/" + dt + "/" + fullFileName;
                // 创建OssClient实例。
                var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
                // 上传文件。
                client.PutObject(bucketName, objectName, file.InputStream);
                // return Success("https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName);
                fullFileName = "https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName;

                meioErpCapitalFlowIBLL.RechargeAndSettlement(keyValue, Currency, amount, fullFileName, remark, type); 
            }
            else
            {
                meioErpCapitalFlowIBLL.RechargeAndSettlement(keyValue, Currency, amount, "", remark, type);
            }
            return Success("充值成功");
        }
    }
}
