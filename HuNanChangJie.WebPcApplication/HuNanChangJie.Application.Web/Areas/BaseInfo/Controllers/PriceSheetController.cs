﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using Castle.Core;
using System;
using HuNanChangJie.Application.TwoDevelopment;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-10 16:07
    /// 描 述：报价单管理
    /// </summary>
    public class PriceSheetController : MvcControllerBase
    {
        private PriceSheetIBLL priceSheetIBLL = new PriceSheetBLL();
        private WarehouseRentIBLL warehouseRentIBLL = new WarehouseRentBLL();
        private OperationIBLL operationIBLL = new OperationBLL();
        private ValueAddedServicesIBLL valueAddedServicesIBLL = new ValueAddedServicesBLL();
        private ContractIBLL contractIBLL = new ContractBLL();

        public PriceSheetController()
        {
            priceSheetIBLL= MyInterceptor.InterCreateObj(priceSheetIBLL);
        }

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Form1()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Dialog1()
        {
            return View();
        }
        [HttpGet]
        public ActionResult LogForm()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = priceSheetIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取操作日志信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetLog(string name)
        {
            XqPagination paginationobj = new XqPagination { rows = 10000, page = 1 };
            var queryJson = (new { Name = name }).ToJson();
            var data = priceSheetIBLL.GetPageList(paginationobj, queryJson);
            List<LogResult> list = new List<LogResult>();
            if (data.Count() > 0)
            {
                foreach (var itemKeyValue in data)
                {
                    List<LogResult> logList = MyInterceptor.getLogResult("PriceSheetIBLL", itemKeyValue.ID);
                    foreach (var item in logList)
                    {
                        item.keyValue = itemKeyValue.Name;
                        if (item.detailList != null)
                        {
                            item.detailList = item.detailList.Where(x => (!string.IsNullOrEmpty(x.updateKey) && x.updateKey.IndexOf("Cell_Edit") < 0) || x.operType != "Update").ToList();
                        }
                    }
                    list.AddRange(logList);
                }
            }

            var deletedata = priceSheetIBLL.GetMeioErpDeletePriceSheetList(name);
            if (deletedata.Count() > 0)
            {
                foreach (var itemKeyValue in deletedata)
                {
                    List<LogResult> logList = MyInterceptor.getLogResult("PriceSheetIBLL", itemKeyValue.DeleteID);
                    foreach (var item in logList)
                    {
                        item.keyValue = itemKeyValue.Code;
                        if (item.detailList != null)
                        {
                            item.detailList = item.detailList.Where(x => (!string.IsNullOrEmpty(x.updateKey) && x.updateKey.IndexOf("Cell_Edit") < 0) || x.operType != "Update").ToList();
                        }
                    }
                    list.AddRange(logList);
                }
            }

            if (list.Count() > 0)
            {
                var result = list.ToList().OrderByDescending(p => p.operTime);
                return Success(result);
            }
            else
            {
                return Fail("没有数据");
            }
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpPriceSheetData = priceSheetIBLL.GetMeioErpPriceSheetEntity(keyValue);
            var MeioErpPriceSheetDetailData = priceSheetIBLL.GetMeioErpPriceSheetDetailList(MeioErpPriceSheetData.ID);

            string strIds = string.Join(",", MeioErpPriceSheetDetailData.Select(o => o.ItemID));
            string strDetailIds = string.Join(",", MeioErpPriceSheetDetailData.Select(o => o.ItemDetailID));
            var MeioErpWarehouseRentDetail = warehouseRentIBLL.GetMeioErpWarehouseRentDetailList(strDetailIds);
            var MeioErpOperationDetail = operationIBLL.GetMeioErpOperationDetailList(strDetailIds);
            var MeioErpWarehouseRentList = warehouseRentIBLL.GetListByIds(strIds);
            var MeioErpOperationList = operationIBLL.GetListByIds(strDetailIds);
            var MeioErpValueAddedServices = valueAddedServicesIBLL.GetListByIds(strIds);

            foreach (var detail in MeioErpPriceSheetDetailData)
            {
                if (MeioErpWarehouseRentDetail.Any(o => o.ID == detail.ItemDetailID))
                {
                    var item = MeioErpWarehouseRentDetail.FirstOrDefault(p => p.ID == detail.ItemDetailID);
                    detail.ItemDetailName = item.ItemName;
                    detail.ItemDetailMinValue = item.MinValue;
                    detail.ItemDetailMaxValue = item.MaxValue;
                    detail.Unit = MeioErpWarehouseRentList.FirstOrDefault(p => p.ID == detail.ItemID).Unit;
                }
                if (MeioErpOperationDetail.Any(o => o.ID == detail.ItemDetailID))
                {
                    var item = MeioErpOperationDetail.FirstOrDefault(p => p.ID == detail.ItemDetailID);
                    detail.ItemDetailName = item.ItemName;
                    detail.ItemDetailMinValue = item.MinValue;
                    detail.ItemDetailMaxValue = item.MaxValue;
                    detail.Unit = MeioErpOperationList.FirstOrDefault(p => p.ID == detail.ItemID).Unit;
                }
                if (MeioErpValueAddedServices.Any(o => o.ID == detail.ItemID))
                {
                    var item = MeioErpValueAddedServices.FirstOrDefault(p => p.ID == detail.ItemID);
                    detail.ItemName = item.ItemName;
                    detail.ItemDetailName = item.ItemDescription;
                    detail.Unit = item.Unit;
                }
            }


            var jsonData = new
            {
                MeioErpPriceSheet = MeioErpPriceSheetData,
                MeioErpPriceSheetDetailWarehouseRent = MeioErpPriceSheetDetailData.Where(p => p.ItemType == 1).OrderBy(s=>s.ItemDetailMinValue),
                MeioErpPriceSheetDetailOperation = MeioErpPriceSheetDetailData.Where(p => p.ItemType == 2).OrderBy(s=>s.ItemDetailMinValue),
                MeioErpPriceSheetDetailValueAddedServices = MeioErpPriceSheetDetailData.Where(p => p.ItemType == 3),
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList1(string keyValue)
        {
            var MeioErpPriceSheetData = priceSheetIBLL.GetMeioErpPriceSheetEntity(keyValue);
            var MeioErpPriceSheetDetailData = priceSheetIBLL.GetMeioErpPriceSheetDetailList(MeioErpPriceSheetData.ID);

            var jsonData = new
            {
                MeioErpPriceSheet = MeioErpPriceSheetData,
                MeioErpPriceSheetDetail = MeioErpPriceSheetDetailData.OrderBy(o=> o.ItemID).ThenBy(o=>o.ItemDetailMinValue),
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var pricesheet = priceSheetIBLL.GetMeioErpPriceSheetEntity(keyValue);
            var pricesheetdetail = priceSheetIBLL.GetMeioErpPriceSheetDetailList(keyValue);
            if (pricesheet.IsValid.Value)
            {
                return Fail("报价单已启用，不能删除该报价单信息");
            }

            var isEdit = contractIBLL.IsEditForm(keyValue);
            if (isEdit)
            {
                return Fail("不能删除该报价单信息，合同已经使用该报价单信息");
            }

            var jsonData= new
            {
                MeioErpPriceSheet = pricesheet,
                MeioErpPriceSheetDetail = pricesheetdetail.OrderBy(o=>o.ItemID).ThenBy(o=>o.ItemDetailMinValue),
            };
            var deleteList = jsonData.ToJson();
            priceSheetIBLL.DeleteEntity(keyValue,deleteList);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strmeioErpPriceSheetDetailList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpPriceSheetEntity>();
            var meioErpPriceSheetDetailList = strmeioErpPriceSheetDetailList.ToObject<List<MeioErpPriceSheetDetailEntity>>();
            if (type == "edit")
            {
                var isEdit = contractIBLL.IsEditForm(keyValue);
                if (isEdit)
                {
                    return Fail("不能修改该报价单信息，合同已经使用该报价单信息");
                }

                var pricesheet = priceSheetIBLL.GetMeioErpPriceSheetEntity(keyValue);
                var pricesheetdetail = priceSheetIBLL.GetMeioErpPriceSheetDetailList(keyValue);

                var jsonData = new
                {
                    MeioErpPriceSheet = pricesheet,
                    MeioErpPriceSheetDetail = pricesheetdetail.OrderBy(o => o.ItemID).ThenBy(o => o.ItemDetailMinValue),
                };
                deleteList = jsonData.ToJson();
            }
            else
            {
                var jsonData = new
                {
                    MeioErpPriceSheet = mainInfo,
                    MeioErpPriceSheetDetail = meioErpPriceSheetDetailList.OrderBy(o => o.ItemID).ThenBy(o => o.ItemDetailMinValue),
                };
                deleteList = jsonData.ToJson();
            }
               
            priceSheetIBLL.SaveEntity(keyValue, mainInfo, meioErpPriceSheetDetailList, deleteList, type);
            return Success("保存成功！");
        }
        #endregion
        [HttpPost]
        [AjaxOnly]
        public ActionResult SetIsValid(string ids, bool isvalid)
        {
            if (!isvalid)
            {
                var isEdit = contractIBLL.IsEditForm(ids);
                if (isEdit)
                {
                    return Fail("不能禁用选中的报价单信息，合同已经使用选中报价单信息");
                }
            }

            var data = priceSheetIBLL.SetIsEnable(ids,isvalid);
            return Success(data);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPriceSheetDetail(string priceSheetId)
        {
            var MeioErpPriceSheetDetailData = priceSheetIBLL.GetMeioErpPriceSheetDetailList(priceSheetId);

            string strIds = string.Join(",", MeioErpPriceSheetDetailData.Select(o => o.ItemID));
            string strDetailIds = string.Join(",", MeioErpPriceSheetDetailData.Select(o => o.ItemDetailID));
            var MeioErpWarehouseRentDetail = warehouseRentIBLL.GetMeioErpWarehouseRentDetailList(strDetailIds);
            var MeioErpOperationDetail = operationIBLL.GetMeioErpOperationDetailList(strDetailIds);
            var MeioErpWarehouseRentList = warehouseRentIBLL.GetListByIds(strIds);
            var MeioErpOperationList = operationIBLL.GetListByIds(strDetailIds);
            var MeioErpValueAddedServices = valueAddedServicesIBLL.GetListByIds(strIds);

            foreach (var detail in MeioErpPriceSheetDetailData)
            {
                if (MeioErpWarehouseRentDetail.Any(o => o.ID == detail.ItemDetailID))
                {
                    var item = MeioErpWarehouseRentDetail.FirstOrDefault(p => p.ID == detail.ItemDetailID);
                    detail.ItemDetailName = item.ItemName;
                    detail.ItemDetailMinValue = item.MinValue;
                    detail.ItemDetailMaxValue = item.MaxValue;
                    detail.Unit = MeioErpWarehouseRentList.FirstOrDefault(p => p.ID == detail.ItemID).Unit;
                }
                if (MeioErpOperationDetail.Any(o => o.ID == detail.ItemDetailID))
                {
                    var item = MeioErpOperationDetail.FirstOrDefault(p => p.ID == detail.ItemDetailID);
                    detail.ItemDetailName = item.ItemName;
                    detail.ItemDetailMinValue = item.MinValue;
                    detail.ItemDetailMaxValue = item.MaxValue;
                    detail.Unit = MeioErpOperationList.FirstOrDefault(p => p.ID == detail.ItemID).Unit;
                }
                if (MeioErpValueAddedServices.Any(o => o.ID == detail.ItemID))
                {
                    var item = MeioErpValueAddedServices.FirstOrDefault(p => p.ID == detail.ItemID);
                    detail.ItemName = item.ItemName;
                    detail.ItemDetailName = item.ItemDescription;
                    detail.Unit = item.Unit;
                    detail.OperationPoint= item.OperationPoint;
                }
            }

            var jsonData = new
            {
                MeioErpPriceSheetDetailWarehouseRent = MeioErpPriceSheetDetailData.Where(p => p.ItemType == 1),
                MeioErpPriceSheetDetailOperation = MeioErpPriceSheetDetailData.Where(p => p.ItemType == 2),
                MeioErpPriceSheetDetailValueAddedServices = MeioErpPriceSheetDetailData.Where(p => p.ItemType == 3),
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult IsEditForm(string itemId)
        {
            var data= priceSheetIBLL.IsEditForm(itemId);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult IsValidForm(string priceSheetId)
        {
            var pricesheet = priceSheetIBLL.GetMeioErpPriceSheetEntity(priceSheetId);
            if (pricesheet != null)
            {
                return Success(pricesheet.IsValid.Value);
            }
            else
            {
                return Fail("报价单已经被删除");
            }
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsCode(string code, string id)
        {
            var data = priceSheetIBLL.CheckIsCode(code, id);
            return Success(data);
        }
    }
}
