﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;
using NPOI.SS.Formula.Functions;
using System.Text;
using HuNanChangJie.Application.TwoDevelopment;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-08 16:38
    /// 描 述：运费报价
    /// </summary>
    public class MeioErpFreightQuotationController : MvcControllerBase
    {
        private MeioErpFreightQuotationIBLL meioErpFreightQuotationIBLL = new MeioErpFreightQuotationBLL();

        public MeioErpFreightQuotationController()
        {
            meioErpFreightQuotationIBLL = MyInterceptor.InterCreateObj(meioErpFreightQuotationIBLL);
        }

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult DetailForm()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ImportForm()
        {
            return View();
        }
        [HttpGet]
        public ActionResult LogForm() {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpFreightQuotationIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetLog(string ruleName)
        {
            XqPagination paginationobj = new XqPagination { rows=10000,page=1};
            var queryJson = (new { RuleName = ruleName }).ToJson();
            var data = meioErpFreightQuotationIBLL.GetPageList(paginationobj, queryJson);
            List<LogResult> list = new List<LogResult>();
            if (data.Count() > 0)
            {
                foreach (var itemKeyValue in data)
                {
                    List<LogResult> logList = MyInterceptor.getLogResult("MeioErpFreightQuotationIBLL", itemKeyValue.ID);
                    foreach (var item in logList)
                    {
                        item.keyValue = itemKeyValue.RuleName;
                        if (item.detailList != null)
                        {
                            item.detailList = item.detailList.Where(x => (!string.IsNullOrEmpty(x.updateKey) && x.updateKey.IndexOf("Cell_Edit") < 0) || x.operType != "Update").ToList();
                        }
                    }
                    list.AddRange(logList);
                }
            }

            var deletedata = meioErpFreightQuotationIBLL.GetMeioErpDeleteFreightQuotationList(ruleName);
            if (deletedata.Count() > 0)
            {
                foreach (var itemKeyValue in deletedata)
                {
                    List<LogResult> logList = MyInterceptor.getLogResult("meioErpFreightQuotationIBLL", itemKeyValue.DeleteID);
                    foreach (var item in logList)
                    {
                        item.keyValue = itemKeyValue.Code;
                        if (item.detailList != null)
                        {
                            item.detailList = item.detailList.Where(x => (!string.IsNullOrEmpty(x.updateKey) && x.updateKey.IndexOf("Cell_Edit") < 0) || x.operType != "Update").ToList();
                        }
                    }
                    list.AddRange(logList);
                }
            }

            if (list.Count() > 0)
            {
                var result = list.ToList().OrderByDescending(p=>p.operTime);
                return Success(result);
            }
            else
            {
                return Fail("没有数据");
            }
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpFreightQuotationData = meioErpFreightQuotationIBLL.GetMeioErpFreightQuotationEntity(keyValue);
            var MeioErpFreightQuotationDetailData = meioErpFreightQuotationIBLL.GetMeioErpFreightQuotationDetailList(MeioErpFreightQuotationData.ID);

            var jsonData = new
            {
                MeioErpFreightQuotation = MeioErpFreightQuotationData,
                MeioErpFreightQuotationDetail = MeioErpFreightQuotationDetailData.OrderBy(p => p.StartWeight),
                MeioErpFreightQuotationDetailItem = MeioErpFreightQuotationDetailData.GroupBy(g => new { g.Country, g.Area,g.ReferenceAging, g.MinWeight, g.ItemID }).Select(o => new { o.Key.Country, o.Key.Area,o.Key.ReferenceAging, o.Key.MinWeight, o.Key.ItemID })
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            foreach (var id in ids)
            {
                var MeioErpFreightQuotationData = meioErpFreightQuotationIBLL.GetMeioErpFreightQuotationEntity(id);
                var MeioErpFreightQuotationDetailData = meioErpFreightQuotationIBLL.GetMeioErpFreightQuotationDetailList(id);

                var jsonData = new
                {
                    MeioErpFreightQuotation = MeioErpFreightQuotationData,
                    MeioErpFreightQuotationDetail = MeioErpFreightQuotationDetailData.OrderBy(p => p.StartWeight)
                };

                var deleteList = jsonData.ToJson();

                meioErpFreightQuotationIBLL.DeleteEntity(id, deleteList);
            }

            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strmeioErpFreightQuotationDetailList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpFreightQuotationEntity>();
            var meioErpFreightQuotationDetailList = strmeioErpFreightQuotationDetailList.ToObject<List<MeioErpFreightQuotationDetailEntity>>();

            //校验起始重量，结束重量是否连续
            if (meioErpFreightQuotationDetailList.Count > 0)
            {
                var error = new StringBuilder();

                var endWeightList = meioErpFreightQuotationDetailList.GroupBy(g => new { g.Country, g.Area }).Select(o => new { o.Key.Country, o.Key.Area, EndWeight = o.Max(m => m.EndWeight) });
                var weightList = meioErpFreightQuotationDetailList.GroupBy(g => new { g.Country, g.Area }).Select(o => new { o.Key.Country, o.Key.Area, StartWeightSum = o.Sum(s => s.StartWeight), EndWeightSum = o.Sum(s => s.EndWeight) });

                foreach (var weightitem in weightList)
                {
                    var endweightentity = endWeightList.FirstOrDefault(p => p.Country == weightitem.Country && p.Area == weightitem.Area);
                    if (weightitem.StartWeightSum + endweightentity.EndWeight != weightitem.EndWeightSum)
                    {
                        error.Append("；" + weightitem.Country);
                        if (!string.IsNullOrEmpty(weightitem.Area))
                        {
                            error.Append("-" + weightitem.Area);
                        }
                    }
                }

                if (error.Length > 0)
                {
                    return Fail("计费规则重量不连续。国家/区域：" + error.ToString().Substring(1));
                }
                else
                {
                    if (type == "edit")
                    {
                        var MeioErpFreightQuotationData = meioErpFreightQuotationIBLL.GetMeioErpFreightQuotationEntity(keyValue);
                        var MeioErpFreightQuotationDetailData = meioErpFreightQuotationIBLL.GetMeioErpFreightQuotationDetailList(MeioErpFreightQuotationData.ID);

                        var jsonData = new
                        {
                            MeioErpFreightQuotation = MeioErpFreightQuotationData,
                            MeioErpFreightQuotationDetail = MeioErpFreightQuotationDetailData.OrderBy(p => p.StartWeight)
                        };

                        deleteList = jsonData.ToJson();
                    }
                    else
                    {
                        var jsonData = new
                        {
                            MeioErpFreightQuotation = mainInfo,
                            MeioErpFreightQuotationDetail = meioErpFreightQuotationDetailList.OrderBy(p => p.StartWeight)
                        };

                        deleteList = jsonData.ToJson();
                    }

                    meioErpFreightQuotationIBLL.SaveEntity(keyValue, mainInfo, meioErpFreightQuotationDetailList, deleteList, type);
                    return Success("保存成功！");
                }
            }
            else
            {
                return Fail("计费规则不能为空");
            }
        }
        #endregion

        [HttpPost]
        [AjaxOnly]
        public ActionResult SetIsEnable(string ids, bool isEnable)
        {
            var data = meioErpFreightQuotationIBLL.SetIsEnable(ids, isEnable);
            return Success(data);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsRuleName(string name, string id)
        {
            var data = meioErpFreightQuotationIBLL.CheckIsRuleName(name, id);
            return Success(data);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UpdateEffectiveTime(string keyValue, DateTime EffectiveStartTime, DateTime EffectiveEndTime)
        {
            var list = meioErpFreightQuotationIBLL.GetMeioErpFreightQuotationDetailListById(keyValue);
            if (list.Count() > 0)
            {
                meioErpFreightQuotationIBLL.UpdateEffectiveTime(keyValue, EffectiveStartTime, EffectiveEndTime, list.ToList());
                return Success("修改成功");
            }
            else
            {
                return Fail("修改失败");
            }
        }
    }
}
