﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-18 11:47
    /// 描 述：云途账单维护
    /// </summary>
    public class MeioErpYunTuBillingMaintenanceController : MvcControllerBase
    {
        private MeioErpYunTuBillingMaintenanceIBLL meioErpYunTuBillingMaintenanceIBLL = new MeioErpYunTuBillingMaintenanceBLL();
        private MeioErpYunTuBillIBLL meioErpYunTuBillIBLL = new MeioErpYunTuBillBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult BillingMaintenanceErrorForm()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpYunTuBillingMaintenanceIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpYunTuBillingMaintenanceData = meioErpYunTuBillingMaintenanceIBLL.GetMeioErpYunTuBillingMaintenanceEntity(keyValue);
            var jsonData = new
            {
                MeioErpYunTuBillingMaintenance = MeioErpYunTuBillingMaintenanceData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioErpYunTuBillingMaintenanceIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpYunTuBillingMaintenanceEntity>();
            meioErpYunTuBillingMaintenanceIBLL.SaveEntity(keyValue, mainInfo, deleteList, type);
            return Success("保存成功！");
        }
        #endregion

        [HttpPost]
        [AjaxOnly]
        public ActionResult Sync(string keyValue)
        {
           int state= meioErpYunTuBillingMaintenanceIBLL.Sync(keyValue);
            if (state == 1)
            {
                return Success("同步成功");
            }
            else
            {
                return Fail("同步失败");
            }
        }
        [HttpPost]
        [AjaxOnly]
        public ActionResult Compute(string keyValue)
        {
            var entity = meioErpYunTuBillingMaintenanceIBLL.GetMeioErpYunTuBillingMaintenanceEntity(keyValue);
            if (entity.SyncState == 1)
            {
                meioErpYunTuBillingMaintenanceIBLL.Compute(keyValue);
                return Success("重算成功，仅未审核的运单费用会进行重算！");
            }
            else
            {
                return Fail("重算失败，云途账单未同步成功");
            }
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetBillingMaintenanceErrorList(string BillingMaintenanceID, string WaybillNumber)
        {
            var data = meioErpYunTuBillingMaintenanceIBLL.GetBillingMaintenanceErrorList(BillingMaintenanceID, WaybillNumber);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsYunTuBillingMaintenanceCode(string code, string id)
        {
            var data = meioErpYunTuBillingMaintenanceIBLL.CheckIsYunTuBillingMaintenanceCode(code,id);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsYunTuBillingMaintenanceCode1(string code)
        {
            var data = meioErpYunTuBillingMaintenanceIBLL.CheckIsYunTuBillingMaintenanceCode(code);
            return Success(data);
        }
    }
}
