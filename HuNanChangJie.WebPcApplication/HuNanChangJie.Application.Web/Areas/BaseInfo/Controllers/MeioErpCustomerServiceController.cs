﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-19 10:01
    /// 描 述：客户服务信息
    /// </summary>
    public class MeioErpCustomerServiceController : MvcControllerBase
    {
        private MeioErpCustomerServiceIBLL meioErpCustomerServiceIBLL = new MeioErpCustomerServiceBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpCustomerServiceIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpCustomerServiceData = meioErpCustomerServiceIBLL.GetMeioErpCustomerServiceEntity( keyValue );
            var MeioErpCustomerServiceDetailData = meioErpCustomerServiceIBLL.GetMeioErpCustomerServiceDetailList( MeioErpCustomerServiceData.ID );
            var jsonData = new {
                MeioErpCustomerService = MeioErpCustomerServiceData,
                MeioErpCustomerServiceDetail = MeioErpCustomerServiceDetailData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioErpCustomerServiceIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strmeioErpCustomerServiceDetailList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpCustomerServiceEntity>();
            var meioErpCustomerServiceDetailList = strmeioErpCustomerServiceDetailList.ToObject<List<MeioErpCustomerServiceDetailEntity>>();
            meioErpCustomerServiceIBLL.SaveEntity(keyValue,mainInfo,meioErpCustomerServiceDetailList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion
        [HttpPost]
        public ActionResult PushCustomerService(List<string> keyValue)
        {
            var result= meioErpCustomerServiceIBLL.PushCustomerService(keyValue);
            return Success("已推送");
            //if (result)
            //    return Success("推送成功");
            //else
            //    return Fail("推送失败");
        }
    }
}
