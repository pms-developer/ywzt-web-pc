﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.TwoDevelopment;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-19 11:00
    /// 描 述：云途账单
    /// </summary>
    public class MeioErpYunTuBillController : MvcControllerBase
    {
        private MeioErpYunTuBillIBLL meioErpYunTuBillIBLL = new MeioErpYunTuBillBLL();

        public MeioErpYunTuBillController()
        {
            meioErpYunTuBillIBLL = MyInterceptor.InterCreateObj(meioErpYunTuBillIBLL);
        }

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult RemarkForm()
        {
            return View();
        }
        [HttpGet]
        public ActionResult AdjustmentAmountForm()
        {
            return View();
        }
        [HttpGet]
        public ActionResult LogForm()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpYunTuBillIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetLog(string WaybillNumber)
        {
            XqPagination paginationobj = new XqPagination { rows = 10000, page = 1 };
            var queryJson = (new { WaybillNumber = WaybillNumber }).ToJson();
            var data = meioErpYunTuBillIBLL.GetPageList(paginationobj, queryJson);
            if (data.Count() > 0)
            {
                List<LogResult> list = new List<LogResult>();
                foreach (var itemKeyValue in data)
                {
                    List<LogResult> logList = MyInterceptor.getLogResult("meioErpYunTuBillIBLL", itemKeyValue.ID);
                    foreach (var item in logList)
                    {
                        item.keyValue = itemKeyValue.WaybillNumber;
                        item.operType = item.operType.Replace("MeioErpYunTuBillIBLL", "");
                    }
                    list.AddRange(logList);
                }
                var result = list.ToList().OrderByDescending(p => p.operTime);
                return Success(result);
            }
            else
            {
                return Fail("没有数据");
            }
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpYunTuBillData = meioErpYunTuBillIBLL.GetMeioErpYunTuBillEntity(keyValue);
            var jsonData = new
            {
                MeioErpYunTuBill = MeioErpYunTuBillData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioErpYunTuBillIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpYunTuBillEntity>();
            meioErpYunTuBillIBLL.SaveEntity(keyValue, mainInfo, deleteList, type);
            return Success("保存成功！");
        }
        #endregion

        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue, string auditStatus)
        {
            UserInfo userInfo = LoginUserInfo.Get();
            meioErpYunTuBillIBLL.Audit(keyValue, auditStatus, userInfo.realName);
            return Success("审核成功");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult BatchRemark(string keyValue, string remark)
        {
            meioErpYunTuBillIBLL.BatchRemark(keyValue, remark);
            return Success("备注成功");
        }
        [HttpPost]
        [AjaxOnly]
        public ActionResult AdjustmentAmount(string keyValue, decimal amount, string remark,string oldAmount)
        {
            meioErpYunTuBillIBLL.AdjustmentAmount(keyValue,amount, remark,oldAmount);
            return Success("调整金额成功");
        }
    }
}
