﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System;
using Aliyun.OSS;
using System.Threading.Tasks;
using HuNanChangJie.WebRequestService;
using DocumentFormat.OpenXml.EMMA;
using Newtonsoft.Json.Linq;
using Spire.Pdf.Graphics;
using static ClosedXML.Excel.XLPredefinedFormat;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-25 17:29
    /// 描 述：发票信息
    /// </summary>
    public class MeioErpInvoiceController : MvcControllerBase
    {
        private MeioErpInvoiceIBLL meioErpInvoiceIBLL = new MeioErpInvoiceBLL();
        private static readonly string apiKey = Config.GetValue("BaiduAIApiKey");
        private static readonly string secretKey = Config.GetValue("BaiduAISecretKey");
        private OwnerIBLL ownerIBLL = new OwnerBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        [HttpGet]
        public ActionResult DialogInvoice()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpInvoiceIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpInvoiceData = meioErpInvoiceIBLL.GetMeioErpInvoiceEntity(keyValue);
            var jsonData = new
            {
                MeioErpInvoice = MeioErpInvoiceData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioErpInvoiceIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpInvoiceEntity>();
            meioErpInvoiceIBLL.SaveEntity(keyValue, mainInfo, deleteList, type);
            return Success("保存成功！");
        }
        #endregion
        public ActionResult UploadFile(string keyValue)
        {
            #region 上传图片
            HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;
            //没有文件上传，直接返回
            if (files[0].ContentLength == 0 || string.IsNullOrEmpty(files[0].FileName))
            {
                return HttpNotFound();
            }
            string fExtension = Path.GetExtension(files[0].FileName);
            string rootPath = Server.MapPath("/");
            string filename = "invoice_" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + fExtension;
            string filepath = "Content/images/invoice/" + filename;
            files[0].SaveAs(Path.Combine(rootPath, filepath));

            //识别发票信息
            using (FileStream fileStream = new FileStream(Path.Combine(rootPath, filepath), FileMode.Open, FileAccess.Read))
            {
                // 创建一个字节数组来存储图片的字节
                byte[] byteArray = new byte[fileStream.Length];

                // 读取图片文件的内容到字节数组
                fileStream.Read(byteArray, 0, byteArray.Length);

                //string content= BaiduService.vatInvoice(filepath);
                Baidu.Aip.Ocr.Ocr ocr = new Baidu.Aip.Ocr.Ocr(apiKey, secretKey);
                var invoice = ocr.VatInvoice(byteArray);
                if (invoice["error_msg"] == null)
                {
                    var invoiceEntity = meioErpInvoiceIBLL.GetMeioErpInvoiceEntity(keyValue);
                    invoiceEntity.InvoiceType = invoice["words_result"]["InvoiceType"].ToString();
                    invoiceEntity.InvoiceName = invoice["words_result"]["InvoiceTypeOrg"].ToString();
                    invoiceEntity.InvoiceCode = invoice["words_result"]["InvoiceCode"].ToString();
                    invoiceEntity.InvoiceNum = invoice["words_result"]["InvoiceNum"].ToString();
                    invoiceEntity.InvoiceDate = invoice["words_result"]["InvoiceDate"].ToString();
                    invoiceEntity.SellerName = invoice["words_result"]["SellerName"].ToString();
                    invoiceEntity.SellerRegisterNum = invoice["words_result"]["SellerRegisterNum"].ToString();
                    invoiceEntity.SellerAddress = invoice["words_result"]["SellerAddress"].ToString();
                    invoiceEntity.SellerBank = invoice["words_result"]["SellerBank"].ToString();
                    invoiceEntity.TotalAmount = decimal.Parse(invoice["words_result"]["TotalAmount"].ToString());
                    invoiceEntity.TotalTax = decimal.Parse(invoice["words_result"]["TotalTax"].ToString());

                    invoiceEntity.IsUploadElectronicInvoice = true;
                    invoiceEntity.UploadDate = System.DateTime.Now;
                    invoiceEntity.InvoiceImageUrl = filepath;
                    invoiceEntity.Modify(keyValue);
                    invoiceEntity.UploadPeople = invoiceEntity.ModificationName;
                    meioErpInvoiceIBLL.SaveEntity(keyValue, invoiceEntity, "", "edit");
                    return Success("上传发票成功");
                }
                else
                {
                    return Fail("上传发票失败，原因：解析发票失败:" + invoice["error_msg"].ToString());
                }
            }



            #endregion
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UpLoadFile(HttpPostedFileBase file, string keyValue, string code)
        {
            // 获取上传文件的扩展名
            if (file == null)
            {
                return Success("");
            }
            var extension = System.IO.Path.GetExtension(file.FileName);
            if (extension.Contains("jpg"))
            {

            }
            else if (extension.Contains("png"))
            {

            }
            else if (extension.Contains("gif"))
            {

            }
            else if (extension.Contains("pdf"))
            {

            }
            else
            {
                return Fail("不支持该格式");
            }
            long timestamp = ((DateTimeOffset)System.DateTime.Now).ToUnixTimeSeconds();
            string fullFileName = code+"-"+ timestamp + extension;
            // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
            var endpoint = "https://oss-cn-shenzhen.aliyuncs.com";
            // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
            var accessKeyId = "LTAI5tQnAwRcEBzn3gkTFR7u";
            var accessKeySecret = "4jgxSlJNgCo6kdSL5t4bMSYPkATui0";
            // 填写Bucket名称，例如examplebucket。
            var bucketName = "meio-wms";
            // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
            string dt = System.DateTime.Now.ToString("yyyy-MM-dd");
            var objectName = "erp/" + dt + "/" + fullFileName;
            // 创建OssClient实例。
            var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
            try
            {
                // 上传文件。
                client.PutObject(bucketName, objectName, file.InputStream);
                // return Success("https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName);
                fullFileName = "https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName;

                //识别发票信息
                Baidu.Aip.Ocr.Ocr ocr = new Baidu.Aip.Ocr.Ocr(apiKey, secretKey);
                JObject invoice = null;
                if (extension.Contains("pdf"))
                {
                    byte[] pdfData;
                    file.InputStream.Seek(0, SeekOrigin.Begin);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.InputStream.CopyTo(ms);
                        pdfData = ms.ToArray();
                    }
                    invoice = ocr.VatInvoicePdf(pdfData);
                }
                else
                {
                    invoice = ocr.VatInvoiceUrl(fullFileName);
                }
                if (invoice["error_msg"] == null)
                {
                    var invoiceEntity = meioErpInvoiceIBLL.GetMeioErpInvoiceEntity(keyValue);
                    invoiceEntity.InvoiceType = invoice["words_result"]["InvoiceType"].ToString();
                    invoiceEntity.InvoiceName = invoice["words_result"]["InvoiceTypeOrg"].ToString();
                    invoiceEntity.InvoiceCode = invoice["words_result"]["InvoiceCode"].ToString();
                    invoiceEntity.InvoiceNum = invoice["words_result"]["InvoiceNum"].ToString();
                    invoiceEntity.InvoiceDate = invoice["words_result"]["InvoiceDate"].ToString();
                    invoiceEntity.SellerName = invoice["words_result"]["SellerName"].ToString();
                    invoiceEntity.SellerRegisterNum = invoice["words_result"]["SellerRegisterNum"].ToString();
                    invoiceEntity.SellerAddress = invoice["words_result"]["SellerAddress"].ToString();
                    invoiceEntity.SellerBank = invoice["words_result"]["SellerBank"].ToString();
                    invoiceEntity.TotalAmount = decimal.Parse(invoice["words_result"]["TotalAmount"].ToString());
                    invoiceEntity.TotalTax = decimal.Parse(invoice["words_result"]["TotalTax"].ToString());

                    invoiceEntity.IsUploadElectronicInvoice = true;
                    invoiceEntity.UploadDate = System.DateTime.Now;
                    invoiceEntity.InvoiceImageUrl = fullFileName;
                    invoiceEntity.Modify(keyValue);
                    invoiceEntity.UploadPeople = invoiceEntity.ModificationName;
                    meioErpInvoiceIBLL.SaveEntity(keyValue, invoiceEntity, "", "edit");

                    var owner = ownerIBLL.GetMeioErpOwnerEntity(invoiceEntity.OwnerID);
                    if (owner != null)
                    {
                        Task.Run(() =>
                        {
                            var request = new InvoiceRequest
                            {
                                settlementNo = code,
                                fileUrl = fullFileName
                            };
                            var result = OmsService.PushInvoice(request, owner.Code);
                        });
                    }
                    return Success("上传发票成功");
                }
                else
                {
                    return Fail("上传发票失败，原因：解析发票失败:" + invoice["error_msg"].ToString());
                }

            }
            catch (Exception e)
            {
                return Fail("上传文件异常：" + e.Message);
            }
        }
    }
}
