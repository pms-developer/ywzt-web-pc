﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System;
using Aliyun.OSS;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-06-27 19:22
    /// 描 述：货品信息
    /// </summary>
    public class GoodsController : MvcControllerBase
    {
        private GoodsIBLL meioErpGoodsIBLL = new GoodsBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpGoodsIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpGoodsData = meioErpGoodsIBLL.GetMeioErpGoodsEntity(keyValue);
            var jsonData = new
            {
                MeioErpGoods = MeioErpGoodsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var goodsEntity=meioErpGoodsIBLL.GetMeioErpGoodsEntity(keyValue);
            if (goodsEntity != null)
            {
                if (goodsEntity.Enabled.Value)
                {
                    return Fail("删除失败，货品已启用不能删除");
                }
                else
                {
                    meioErpGoodsIBLL.DeleteEntity(keyValue);
                    return Success("删除成功！");
                }
            }
            else
            {
                return Fail("删除失败，货品已删除或不存在");
            }
           
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string deleteList,bool IsNominatedSupplier,string ImageUrl)
        {
            var mainInfo = strEntity.ToObject<MeioErpGoodsEntity>();
            mainInfo.IsNominatedSupplier = IsNominatedSupplier;
            mainInfo.ImageUrl = ImageUrl;
            meioErpGoodsIBLL.SaveEntity(keyValue, mainInfo, deleteList, type);
            return Success("保存成功！");
        }

        public ActionResult UploadFile(string code)
        {
            #region 上传商品图片
            HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;
            //没有文件上传，直接返回
            if (files[0].ContentLength == 0 || string.IsNullOrEmpty(files[0].FileName))
            {
                return HttpNotFound();
            }
            string fExtension = Path.GetExtension(files[0].FileName);
            string rootPath = Server.MapPath("/");
            string filename = "goods_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fExtension;
            string filepath = "Content/images/goods/" + filename;
            files[0].SaveAs(Path.Combine(rootPath, filepath));

            return Success(filepath);
            #endregion
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UpLoadFile(HttpPostedFileBase file,string code)
        {
            // 获取上传文件的扩展名
            if (file == null)
            {
                return Success("");
            }
            var extension = System.IO.Path.GetExtension(file.FileName);
            if (extension.Contains("jpg"))
            {

            }
            else if (extension.Contains("png"))
            {

            }
            else if (extension.Contains("gif"))
            {

            }
            else
            {
                return Fail("不支持该格式");
            }
            string fullFileName = Guid.NewGuid().ToString() + extension;
            // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
            var endpoint = "https://oss-cn-shenzhen.aliyuncs.com";
            // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
            var accessKeyId = "LTAI5tQnAwRcEBzn3gkTFR7u";
            var accessKeySecret = "4jgxSlJNgCo6kdSL5t4bMSYPkATui0";
            // 填写Bucket名称，例如examplebucket。
            var bucketName = "meio-wms";
            // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
            string dt = DateTime.Now.ToString("yyyy-MM-dd");
            var objectName = "erp/" + dt + "/" + fullFileName;
            // 创建OssClient实例。
            var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
            try
            {
                // 上传文件。
                client.PutObject(bucketName, objectName, file.InputStream);
                // return Success("https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName);
                fullFileName = "https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName;

                return Success(fullFileName);
            }
            catch (Exception e)
            {
                return Fail("上传文件异常：" + e.Message);
            }
        }


        #endregion
        [HttpPost]
        [AjaxOnly]
        public ActionResult SetIsEnable(string ids, bool isEnable)
        {
            var data = meioErpGoodsIBLL.SetIsEnable(ids, isEnable);
            return Success(data);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsGoodsCode(string code, string id)
        {
            var data = meioErpGoodsIBLL.CheckIsGoodsCode(code, id);
            return Success(data);
        }
    }
}
