﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-03 10:46
    /// 描 述：采购单
    /// </summary>
    public class MeioErpPurchaseController : MvcControllerBase
    {
        private MeioErpPurchaseIBLL meioErpPurchaseIBLL = new MeioErpPurchaseBLL();
        private MeioErpDeliveryIBLL meioErpDeliveryIBLL = new MeioErpDeliveryBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Audit()
        {
            return View();
        }

        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpPurchaseIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpPurchaseData = meioErpPurchaseIBLL.GetMeioErpPurchaseEntity(keyValue);
            var MeioErpPurchaseDetailData = meioErpPurchaseIBLL.GetMeioErpPurchaseDetailList(MeioErpPurchaseData.ID);
            var jsonData = new
            {
                MeioErpPurchase = MeioErpPurchaseData,
                MeioErpPurchaseDetail = MeioErpPurchaseDetailData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            //var deliveryList = meioErpDeliveryIBLL.GetMeioErpDeliveryListByPurchaseID(keyValue);
            //if (deliveryList.Count() > 0)
            //{
            //    return Fail("删除失败，采购单已生成发货单");
            //}

            var purchaseEntity = meioErpPurchaseIBLL.GetMeioErpPurchaseEntity(keyValue);
            if (purchaseEntity != null)
            {
                if (string.IsNullOrEmpty(purchaseEntity.AuditStatus) || purchaseEntity.AuditStatus == "0")
                {
                    meioErpPurchaseIBLL.DeleteEntity(keyValue);
                    return Success("删除成功！");
                }
                else
                {
                    return Fail("删除失败，采购单已审核");
                }
            }
            else
            {
                return Fail("删除失败，采购单不存在或已删除");
            }

        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strmeioErpPurchaseDetailList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpPurchaseEntity>();
            var meioErpPurchaseDetailList = strmeioErpPurchaseDetailList.ToObject<List<MeioErpPurchaseDetailEntity>>();

            if (!string.IsNullOrEmpty(keyValue))
            {
                var purchaseEntity = meioErpPurchaseIBLL.GetMeioErpPurchaseEntity(keyValue);
                if (purchaseEntity != null)
                {
                    if (!string.IsNullOrEmpty(purchaseEntity.AuditStatus) && purchaseEntity.AuditStatus != "0")
                    {
                        return Fail("不能修改已审核的采购单");
                    }
                }
            }
            meioErpPurchaseIBLL.SaveEntity(keyValue, mainInfo, meioErpPurchaseDetailList, deleteList, type);
            return Success("保存成功！");
        }
        #endregion
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult AuditPurchase(string keyValue, int auditResult, string remark)
        {
            var purchase = meioErpPurchaseIBLL.GetMeioErpPurchaseEntity(keyValue);
            if (string.IsNullOrEmpty(purchase.AuditStatus) || purchase.AuditStatus == "0")
            {
                var meioErpDocumentAuditRecordEntity = new MeioErpDocumentAuditRecordEntity
                {
                    DocumentID = keyValue,
                    DocumentType = "Purchase",
                    AuditDate = DateTime.Now,
                    AuditResult = auditResult,
                    Remark = remark
                };
                meioErpPurchaseIBLL.AuditPurchase(keyValue, meioErpDocumentAuditRecordEntity);
                return Success("审核成功");
            }
            else
            {
                return Fail("审核失败，采购单已审核");
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult UnAuditPurchase(string keyValue)
        {
            var deliveryList= meioErpDeliveryIBLL.GetMeioErpDeliveryListByPurchaseID(keyValue);
            if (deliveryList.Count()> 0)
            {
                return Fail("反审核失败，采购单已生成发货单");
            }
            var purchase = meioErpPurchaseIBLL.GetMeioErpPurchaseEntity(keyValue);
            if (!string.IsNullOrEmpty(purchase.AuditStatus) && purchase.AuditStatus != "0")
            {
                var meioErpDocumentAuditRecordEntity = new MeioErpDocumentAuditRecordEntity
                {
                    DocumentID = keyValue,
                    DocumentType = "Purchase",
                    AuditDate = DateTime.Now,
                    AuditResult = 0,
                    Remark = "反审核"
                };
                meioErpPurchaseIBLL.AuditPurchase(keyValue, meioErpDocumentAuditRecordEntity);
                return Success("审核成功");
            }
            else
            {
                return Fail("反审核失败，采购单待审核");
            }

        }

        [HttpGet]
        public ActionResult GetAwaitDeliveryPurchaseList()
        {
            var data= meioErpPurchaseIBLL.GetAwaitDeliveryPurchaseList().Select(o=>new { o.ID,o.Code }).ToList();
            return Success(data);
        }
    }
}
