﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;
using HuNanChangJie.WebRequestService;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    public class AccountController : MvcControllerBase
    {
        private MeioErpWarehouseRentAccountIBLL warehouseRentAccountIBLL = new MeioErpWarehouseRentAccountBLL();
        private MeioErpLogisticsPackageAccountIBLL logisticsPackageAccountIBLL = new MeioErpLogisticsPackageAccountBLL();
        private MeioErpInStorageAccountIBLL inStorageAccountIBLL = new MeioErpInStorageAccountBLL();
        private MeioErpDeliveryAccountIBLL deliveryAccountIBLL = new MeioErpDeliveryAccountBLL();
        private MeioErpCheckInventoryAccountIBLL checkInventoryAccountIBLL = new MeioErpCheckInventoryAccountBLL();
       
        // GET: BaseInfo/Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        #region  获取数据


        #endregion

        /// <summary>
        /// 推送仓储租金账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PushWarehouseRentAccount(List<string> keyValue)
        {
            if (keyValue == null)
                warehouseRentAccountIBLL.PushWarehouseRentAccount(false);
            else
                warehouseRentAccountIBLL.PushWarehouseRentAccount(keyValue);
            return Success("已推送");
        }
        [HttpGet]
        [HandlerLogin(FilterMode.Ignore)]
        public ActionResult PushWarehouseRentAccount()
        {
            warehouseRentAccountIBLL.PushWarehouseRentAccount(false);
            return Success("已推送");
        }

        /// <summary>
        /// 推送入库账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PushInStorageAccount(List<string> keyValue)
        {
            if (keyValue == null)
                inStorageAccountIBLL.PushInStorageAccount(false);
            else
                inStorageAccountIBLL.PushInStorageAccount(keyValue);
            return Success("已推送");
        }
        [HttpGet]
        [HandlerLogin(FilterMode.Ignore)]
        public ActionResult PushInStorageAccount()
        {
            inStorageAccountIBLL.PushInStorageAccount(false);
            return Success("已推送");
        }

        /// <summary>
        /// 推送出库账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PushDeliveryAccount(List<string> keyValue)
        {
            if (keyValue == null)
                deliveryAccountIBLL.PushDeliveryAccount(false);
            else
                deliveryAccountIBLL.PushDeliveryAccount(keyValue);
            return Success("已推送");
        }

        [HttpGet]
        [HandlerLogin(FilterMode.Ignore)]
        public ActionResult PushDeliveryAccount()
        {
            deliveryAccountIBLL.PushDeliveryAccount(false);
            return Success("已推送");
        }

        /// <summary>
        /// 推送盘点账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PushCheckInventoryAccount(List<string> keyValue)
        {
            if (keyValue == null)
                checkInventoryAccountIBLL.PushCheckInventoryAccount(false);
            else
                checkInventoryAccountIBLL.PushCheckInventoryAccount(keyValue);
            return Success("已推送");
        }

        [HttpGet]
        [HandlerLogin(FilterMode.Ignore)]
        public ActionResult PushCheckInventoryAccount()
        {
            checkInventoryAccountIBLL.PushCheckInventoryAccount(false);
            return Success("已推送");
        }

        /// <summary>
        /// 推送快递账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PushLogisticsPackageAccount(List<string> keyValue)
        {
            if (keyValue == null)
                logisticsPackageAccountIBLL.PushLogisticsPackageAccount(false);
            else
                logisticsPackageAccountIBLL.PushLogisticsPackageAccount(keyValue);
            return Success("已推送");
        }

        [HttpGet]
        [HandlerLogin(FilterMode.Ignore)]
        public ActionResult PushLogisticsPackageAccount()
        {
            logisticsPackageAccountIBLL.PushLogisticsPackageAccount(false);
            return Success("已推送");
        }

        [HttpGet]
        public ActionResult GetHoliday()
        {
            var data = HolidayService.GetHoliday();
            return Success(data);
        }
    }
}