﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-13 16:32
    /// 描 述：合同管理
    /// </summary>
    public class ContractController : MvcControllerBase
    {
        private ContractIBLL contractIBLL = new ContractBLL();
        private WarehouseRentIBLL warehouseRentIBLL = new WarehouseRentBLL();
        private OperationIBLL operationIBLL = new OperationBLL();
        private ValueAddedServicesIBLL valueAddedServicesIBLL = new ValueAddedServicesBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Audit()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Form1()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = contractIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpContractData = contractIBLL.GetMeioErpContractEntity(keyValue);
            var MeioErpContractDetailData = contractIBLL.GetMeioErpContractDetailList(MeioErpContractData.ID);
            var MeioErpContractRecordData = contractIBLL.GetMeioErpContractRecordList(MeioErpContractData.ID);
            var MeioErpDocumentAuditRecordData = contractIBLL.GetDocumentAuditRecordEntityList(MeioErpContractData.ID);

            string strIds = string.Join(",", MeioErpContractDetailData.Select(o => o.ItemID));
            string strDetailIds = string.Join(",", MeioErpContractDetailData.Select(o => o.ItemDetailID));
            var MeioErpWarehouseRentDetail = warehouseRentIBLL.GetMeioErpWarehouseRentDetailList(strDetailIds);
            var MeioErpOperationDetail = operationIBLL.GetMeioErpOperationDetailList(strDetailIds);
            var MeioErpWarehouseRentList = warehouseRentIBLL.GetListByIds(strIds);
            var MeioErpOperationList = operationIBLL.GetListByIds(strDetailIds);
            var valueAddedServicesList= valueAddedServicesIBLL.GetListByIds(strIds);
            var MeioErpValueAddedServices = valueAddedServicesList.Where(p => p.Type == 2);
            var MeioErpValueAddedServicesXiaoBao = valueAddedServicesList.Where(p => p.Type == 1);

            foreach (var detail in MeioErpContractDetailData)
            {
                if (MeioErpWarehouseRentDetail.Any(o => o.ID == detail.ItemDetailID))
                {
                    var item = MeioErpWarehouseRentDetail.FirstOrDefault(p => p.ID == detail.ItemDetailID);
                    detail.ItemDetailName = item.ItemName;
                    detail.ItemDetailMinValue = item.MinValue;
                    detail.ItemDetailMaxValue = item.MaxValue;
                    detail.Unit = MeioErpWarehouseRentList.FirstOrDefault(p => p.ID == detail.ItemID).Unit;
                }
                if (MeioErpOperationDetail.Any(o => o.ID == detail.ItemDetailID))
                {
                    var item = MeioErpOperationDetail.FirstOrDefault(p => p.ID == detail.ItemDetailID);
                    detail.ItemDetailName = item.ItemName;
                    detail.ItemDetailMinValue = item.MinValue;
                    detail.ItemDetailMaxValue = item.MaxValue;
                    detail.Unit = MeioErpOperationList.FirstOrDefault(p => p.ID == detail.ItemID).Unit;
                }
                if (MeioErpValueAddedServices.Any(o => o.ID == detail.ItemID))
                {
                    var item = MeioErpValueAddedServices.FirstOrDefault(p => p.ID == detail.ItemID);
                    detail.ItemName = item.ItemName;
                    detail.ItemDetailName = item.ItemDescription;
                    detail.Unit = item.Unit;
                    detail.OperationPoint = item.OperationPoint;
                }
                if (MeioErpValueAddedServicesXiaoBao.Any(o => o.ID == detail.ItemID))
                {
                    var item = MeioErpValueAddedServicesXiaoBao.FirstOrDefault(p => p.ID == detail.ItemID);
                    detail.ItemName = item.ItemName;
                    detail.ItemDetailName = item.ItemDescription;
                    detail.Unit = item.Unit;
                    detail.OperationPoint = item.OperationPoint;
                }
            }

            var jsonData = new
            {
                MeioErpContract = MeioErpContractData,
                //MeioErpContractDetail = MeioErpContractDetailData,
                MeioErpContractDetailWarehouseRent = MeioErpContractDetailData.Where(p => p.ItemType == 1),
                MeioErpContractDetailOperation = MeioErpContractDetailData.Where(p => p.ItemType == 2),
                MeioErpContractDetailValueAddedServices = MeioErpContractDetailData.Where(p => p.ItemType == 3 && p.Type==2),
                MeioErpContractDetailValueAddedServicesXiaoBao = MeioErpContractDetailData.Where(p => p.ItemType == 3 && p.Type==1),
                MeioErpContractRecord = MeioErpContractRecordData,
                MeioErpDocumentAuditRecord = MeioErpDocumentAuditRecordData
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var contract = contractIBLL.GetMeioErpContractEntity(keyValue);
            if (contract.AuditStatus == 0)
            {
                contractIBLL.DeleteEntity(keyValue);
                return Success("删除成功！");
            }
            else
            {
                return Fail("删除失败，合同已审核不能删除");
            }
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strmeioErpContractDetailList, string strmeioErpContractRecordEntity, string deleteList)
        {
            if (type == "edit")
            {
                var contract = contractIBLL.GetMeioErpContractEntity(keyValue);
                if (contract.AuditStatus != 0)
                {
                    return Fail("合同已审核不能修改");
                }
            }
            var mainInfo = strEntity.ToObject<MeioErpContractEntity>();
            if (type == "add")
            {
                if (mainInfo.ServiceStartDate.Value.Date < DateTime.Now.Date)
                {
                    return Fail("服务起始日期不能小于当前日期");
                }
                if (mainInfo.ServiceEndDate.Value.Date <= mainInfo.ServiceStartDate.Value.Date.AddMonths(3))
                {
                    return Fail("服务截止日期不能小于起始日期3个月，即：截止日期必须大于起始日期+3月");
                }
                //var isClinetContract = contractIBLL.CheckIsClientContract(mainInfo.ClientCode);
                //if (isClinetContract)
                //{
                    var contractEntity=contractIBLL.GetContractByClientCode(mainInfo.ClientCode);
                    if (contractEntity != null)
                    {
                        if (mainInfo.ServiceStartDate <= contractEntity.ServiceEndDate)
                            return Fail($"服务起始时间不能小于:{contractEntity.ServiceEndDate.Value.ToString("yyyy-MM-dd")}");
                    }
                //}
            }


            var meioErpContractDetailList = strmeioErpContractDetailList.ToObject<List<MeioErpContractDetailEntity>>();
            //var meioErpContractRecordEntity = strmeioErpContractRecordEntity.ToObject<MeioErpContractRecordEntity>();
            contractIBLL.SaveEntity(keyValue, mainInfo, meioErpContractDetailList, null, deleteList, type);
            return Success("保存成功！");
        }
        #endregion
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult ContractProcessing(string keyValue, int status, DateTime serviceStartDate, DateTime serviceEndDate)
        {
            var contract = contractIBLL.GetMeioErpContractEntity(keyValue);
            if (contract.Status == 3)
            {
                return Fail("合同已终止，不能处理");
            }
            else
            {
                contractIBLL.ContractProcessing(keyValue, status, serviceStartDate, serviceEndDate);
                return Success("合约处理成功");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult AuditContract(string keyValue, int auditResult, string remark)
        {
            var contract=contractIBLL.GetMeioErpContractEntity(keyValue);
            if (contract.AuditStatus == 0)
            {
                var meioErpDocumentAuditRecordEntity = new MeioErpDocumentAuditRecordEntity
                {
                    DocumentID = keyValue,
                    DocumentType = "Contract",
                    AuditDate = DateTime.Now,
                    AuditResult = auditResult,
                    Remark = remark
                };
                contractIBLL.AuditContract(keyValue, meioErpDocumentAuditRecordEntity);
                return Success("审核成功");
            }
            else
            {
                return Fail("审核失败，合同已审核");
            }
           
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsClientCode(string code, string id)
        {
            var data=contractIBLL.CheckIsClientCode(code, id);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsClientContract(string clientCode)
        {
            var data=contractIBLL.CheckIsClientContract(clientCode);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult IsEditForm(string priceSheetId)
        {
            var data=contractIBLL.IsEditForm(priceSheetId);
            return Success(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult UnAuditContract(string keyValue)
        {
            var contract = contractIBLL.GetMeioErpContractEntity(keyValue);
            if (contract.AuditStatus != 0)
            {
                var meioErpDocumentAuditRecordEntity = new MeioErpDocumentAuditRecordEntity
                {
                    DocumentID = keyValue,
                    DocumentType = "Contract",
                    AuditDate = DateTime.Now,
                    AuditResult = 0,
                    Remark = "反审核"
                };
                contractIBLL.AuditContract(keyValue, meioErpDocumentAuditRecordEntity);
                return Success("审核成功");
            }
            else
            {
                return Fail("反审核失败，合同待审核");
            }

        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsCode(string code, string id)
        {
            var data = contractIBLL.CheckIsCode(code, id);
            return Success(data);
        }
    }
}
