﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;
using Aliyun.OSS;
using System.Web;
using System;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-10 18:06
    /// 描 述：电商平台
    /// </summary>
    public class MeioErpWarehouseCommercePlatformController : MvcControllerBase
    {
        private MeioErpWarehouseCommercePlatformIBLL meioErpWarehouseCommercePlatformIBLL = new MeioErpWarehouseCommercePlatformBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpWarehouseCommercePlatformIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpWarehouseCommercePlatformData = meioErpWarehouseCommercePlatformIBLL.GetMeioErpWarehouseCommercePlatformEntity( keyValue );
            var jsonData = new {
                MeioErpWarehouseCommercePlatform = MeioErpWarehouseCommercePlatformData,
            };
            return Success(jsonData);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UpLoadFile(HttpPostedFileBase file)
        {
            // 获取上传文件的扩展名
            if (file == null)
            {
                return Success("");
            }
            var extension = System.IO.Path.GetExtension(file.FileName);
            if (extension.Contains("jpg") || extension.Contains("jpeg"))
            {

            }
            else if (extension.Contains("png"))
            {

            }
            else if (extension.Contains("gif"))
            {

            }
            else
            {
                return Fail("不支持该格式");
            }
            string fullFileName = Guid.NewGuid().ToString() + extension;
            // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
            var endpoint = HuNanChangJie.Util.Config.GetValue("ossEndpoint");
            // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
            var accessKeyId = HuNanChangJie.Util.Config.GetValue("ossKey");
            var accessKeySecret = HuNanChangJie.Util.Config.GetValue("ossSecret");
            // 填写Bucket名称，例如examplebucket。
            var bucketName = HuNanChangJie.Util.Config.GetValue("ossBucketName");
            // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
            string dt = DateTime.Now.ToString("yyyy-MM-dd");
            var objectName = "zhongtai/" + dt + "/" + fullFileName;
            // 创建OssClient实例。
            var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
            try
            {
                // 上传文件。
                client.PutObject(bucketName, objectName, file.InputStream);
                // return Success("https://meio-wms.oss-cn-shenzhen.aliyuncs.com/" + objectName);
                fullFileName = HuNanChangJie.Util.Config.GetValue("ossSite") + objectName;
            }
            catch (Exception)
            {
                throw new ExceptionEx("上传文件异常", new Exception());
            }

            return Success(fullFileName);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioErpWarehouseCommercePlatformIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpWarehouseCommercePlatformEntity>();
            if (type=="add" && meioErpWarehouseCommercePlatformIBLL.IsExistRate(mainInfo.Code))
            {
                return Fail("编码已经存在！");
            }
            meioErpWarehouseCommercePlatformIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
