﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 09:58
    /// 描 述：账单金额调整
    /// </summary>
    public class MeioErpBillPriceAdjustmentController : MvcControllerBase
    {
        private MeioErpBillPriceAdjustmentIBLL meioErpBillPriceAdjustmentIBLL = new MeioErpBillPriceAdjustmentBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioErpBillPriceAdjustmentIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpBillPriceAdjustmentData = meioErpBillPriceAdjustmentIBLL.GetMeioErpBillPriceAdjustmentEntity( keyValue );
            var jsonData = new {
                MeioErpBillPriceAdjustment = MeioErpBillPriceAdjustmentData,
            };
            return Success(jsonData);
        }


        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoListByBill(string bId, string bAmount)
        {
            var MeioErpBillPriceAdjustmentData = meioErpBillPriceAdjustmentIBLL.GetMeioErpBillPriceAdjustmentEntityByBill(bId, bAmount);
            var jsonData = new
            {
                MeioErpBillPriceAdjustment = MeioErpBillPriceAdjustmentData,
            };
            return Success(jsonData);
        }



        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioErpBillPriceAdjustmentIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioErpBillPriceAdjustmentEntity>();
            meioErpBillPriceAdjustmentIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }



        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveFormByBill(string strEntity)
        {
            var mainInfo = strEntity.ToObject<MeioErpBillPriceAdjustmentEntity>();

            meioErpBillPriceAdjustmentIBLL.SaveEntityByBill(mainInfo);
            return Success("保存成功！");
        }



        #endregion

    }
}
