﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.BaseInfo.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 18:11
    /// 描 述：仓库租金档案信息
    /// </summary>
    public class WarehouseRentController : MvcControllerBase
    {
        private WarehouseRentIBLL warehouseRentIBLL = new WarehouseRentBLL();
        private PriceSheetIBLL priceSheetIBLL=new PriceSheetBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = warehouseRentIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var MeioErpWarehouseRentData = warehouseRentIBLL.GetMeioErpWarehouseRentEntity( keyValue );
            var MeioErpWarehouseRentDetailData = warehouseRentIBLL.GetMeioErpWarehouseRentDetailList( MeioErpWarehouseRentData.ID );
            var jsonData = new {
                MeioErpWarehouseRent = MeioErpWarehouseRentData,
                MeioErpWarehouseRentDetail = MeioErpWarehouseRentDetailData,
            };
            return Success(jsonData);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetWarehouseRentDetail(string ids)
        {
            var data = warehouseRentIBLL.GetMeioErpWarehouseRentDetailListByWarehouseRentIDs(ids);
            return Success(data);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var isEdit= priceSheetIBLL.IsEditForm( keyValue );
            if (isEdit)
            {
                return Fail("不能删除该档案信息，报价单已经使用该档案信息");
            }

            warehouseRentIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strmeioErpWarehouseRentDetailList,string deleteList)
        {
            if (type == "edit")
            {
                var isEdit = priceSheetIBLL.IsEditForm(keyValue);
                if (isEdit)
                {
                    return Fail("不能修改该档案信息，报价单已经使用该档案信息");
                }
            }

            var mainInfo = strEntity.ToObject<MeioErpWarehouseRentEntity>();
            var meioErpWarehouseRentDetailList = strmeioErpWarehouseRentDetailList.ToObject<List<MeioErpWarehouseRentDetailEntity>>();
            warehouseRentIBLL.SaveEntity(keyValue,mainInfo,meioErpWarehouseRentDetailList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion
        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsItemName(string name, string id)
        {
            var data = warehouseRentIBLL.CheckIsItemName(name,id);
            return Success(data);
        }
    }
}
