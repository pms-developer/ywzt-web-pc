﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-07-04 14:45
 * 描  述：发货单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpDelivery";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/MeioErpDelivery/SaveForm';
var purchaseId = request('purchaseId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'MeioErpDeliveryDetail', "gridId": 'MeioErpDeliveryDetail' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: '10002' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#ReceivingWarehouseID').mkDataSourceSelect({ code: 'WarehouseList', value: 'id', text: 'name' });
            $('#PurchaseType').mkDataItemSelect({ code: 'PurchaseContractType' });
            $('#AuditStatus').mkDataItemSelect({ code: 'ContractAuditStatus' });
            $('#MeioErpDeliveryDetail').jfGrid({
                headData: [
                    {
                        label: '采购单号', name: 'PurchaseCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '商品图片Url', name: 'GoodsImageUrl', width: 100, formatterAsync: function (callback, value, row, op, $cell) {
                            callback('<img style="width:100%;height:100%" src="' + value + '">')
                        }
                    },
                    {
                        label: '货品编码', name: 'GoodsCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '货品名称', name: 'GoodsName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '采购数量', name: 'PurchaseQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '已发货数量', name: 'PurchaseDeliveryQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '发货数量', name: 'DeliveryQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'int', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var PurchaseQuantity = Number(row.PurchaseQuantity);
                                var PurchaseDeliveryQuantity = Number(row.PurchaseDeliveryQuantity);
                                if (PurchaseQuantity - PurchaseDeliveryQuantity + oldValue - row.DeliveryQuantity < 0) {
                                    row.DeliveryQuantity = PurchaseQuantity - PurchaseDeliveryQuantity + oldValue;
                                    top.Changjie.alert.error("发货数量不能大于：" + (PurchaseQuantity - PurchaseDeliveryQuantity + oldValue));
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '入库数量', name: 'InStorageQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "MeioErpDeliveryDetail",
                isEdit: false,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });


            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: "selectGoods",
                    width: 800,
                    height: 450,
                    title: "选择货品信息",
                    url: top.$.rootUrl + "/BaseInfo/MeioErpDelivery/Dialog",
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                var rows = $('#MeioErpDeliveryDetail').jfGridGet('rowdatas');
                                for (var i = 0; i < data.length; i++) {
                                    var isEx = false;
                                    rows.forEach(function (row) {
                                        if (row.GoodsCode == data[i].Code) {
                                            isEx = true;
                                        }
                                    });
                                    if (!isEx) {
                                        var row = {};
                                        row.GoodsImageUrl = data[i].ImageUrl;
                                        row.GoodsCode = data[i].Code;
                                        row.GoodsName = data[i].Name;
                                        rows.push(row);
                                    }
                                }
                                $("#MeioErpDeliveryDetail").jfGridSet("refreshdata", rows);

                            }
                        });
                    }
                });
            });
            $('#delete').on('click', function () {
                var selectRow = $('#MeioErpDeliveryDetail').jfGridGet('rowdatas');
                selectRow.forEach(function (row) {
                    $('#MeioErpDeliveryDetail').jfGridSet('removeRow', row.ID);
                });
            });
        },
        initData: function () {
            if (!!purchaseId) {
                $("#PurchaseID").val(purchaseId);
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpPurchase/GetformInfoList?keyValue=' + purchaseId, function (data) {
                    $("#PurchaseType").mkselectSet(data["MeioErpPurchase"].Type);
                    $("#ReceivingWarehouseID").mkselectSet(data["MeioErpPurchase"].WarehouseID);
                    $("#PurchaseCode").val(data["MeioErpPurchase"].Code);
                    var rows = [];
                    data["MeioErpPurchaseDetail"].forEach(function (item) {
                        var row = {}
                        row.PurchaseCode = data["MeioErpPurchase"].Code;
                        row.GoodsImageUrl = item.GoodsImageUrl;
                        row.GoodsCode = item.GoodsCode;
                        row.GoodsName = item.GoodsName;
                        row.PurchaseQuantity = item.PurchaseQuantity;
                        row.PurchaseDeliveryQuantity = item.DeliveryQuantity;
                        rows.push(row);
                    });
                    $("#MeioErpDeliveryDetail").jfGridSet("refreshdata", rows);
                });
            }
            else {
                if (!!keyValue) {
                    $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpDelivery/GetformInfoList?keyValue=' + keyValue, function (data) {
                        for (var id in data) {
                            if (!!data[id].length && data[id].length > 0) {
                                $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                                subGrid.push({ "tableName": id, "gridId": id });
                            }
                            else {
                                tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                            }
                        }
                    });
                }
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack,name) {
        var postData = getFormData();
        if (postData == false) {
            return false;
        }
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpDelivery/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                Changjie.layerClose(name);
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    if ($('#MeioErpDeliveryDetail').jfGridGet('rowdatas').length == 0) {
        top.Changjie.alert.error("发货商品明细不能为空");
        return false;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="MeioErpDelivery"]').mkGetFormData());
    postData.strmeioErpDeliveryDetailList = JSON.stringify($('#MeioErpDeliveryDetail').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
