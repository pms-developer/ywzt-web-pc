﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-06-28 16:38
 * 描  述：供应商信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#Enabled').mkselect({ data: [{ value: true, text: "启用" }, { value: false, text: "禁用" }], value: 'value', text: 'text', title: 'text' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "Code": $("#Code").val(),
                    "Name": $("#Name").val(),
                    "Enabled": $("#Enabled").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/Supplier/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            //  启用
            $('#btn_enable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认启用选中供应商！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Supplier/SetIsEnable', { ids: keyValue, isEnable: true }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            //  禁用
            $('#btn_disable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认禁用选中供应商！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Supplier/SetIsEnable', { ids: keyValue, isEnable: false }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/Supplier/GetPageList',
                headData: [
                    { label: "供应商编码", name: "Code", width: 100, align: "left"},
                    { label: "供应商名称", name: "Name", width: 100, align: "left"},
                    { label: "供应商类型", name: "Type", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'SupplierType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    {
                        label: "启用状态", name: "Enabled", width: 100, align: "left", formatter: function (value, row) {
                            if (value)
                                return "启用";
                            else
                                return "禁用";
                        }
                    },
                    { label: "创建者名称", name: "CreationName", width: 100, align: "left"},
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true,
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form11',
                        title: "供应商信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/Supplier/Form?keyValue=' + row.ID,
                        width: 800,
                        height: 600
                    });
                },
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Supplier/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/Supplier/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
