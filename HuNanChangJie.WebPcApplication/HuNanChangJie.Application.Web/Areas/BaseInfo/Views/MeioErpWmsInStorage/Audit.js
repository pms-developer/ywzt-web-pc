﻿var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $('#AuditResult').mkselect({ data: [{ id: 1, text: "同意" }, { id: 2, text: "拒绝" }],value:'id',text:'text',title:'text' });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $('[data-table="MeioErpDocumentAuditRecord"]').mkGetFormData();

        formdata.keyValue = keyValue;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpPurchase/AuditPurchase',
            formdata,
            function (res) {
                // 保存成功后才回调
                if (!!callBack) {
                    callBack();
                }
            });
    };
    page.init();
};