﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-09-02 16:39
 * 描  述：承运商信息
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="MeioERPGeneralCarrier";
var processCommitUrl=top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'MeioERPGeneralCarrierHistoricalRecord',"gridId":'MeioERPGeneralCarrierHistoricalRecord'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $('#MeioERPGeneralCarrierHistoricalRecord').jfGrid({
                headData: [
                    {
                        label: '生效时间', name: 'EffectiveTime', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '价格', name: 'Price', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                ],
                mainId:"ID",
                bindTable:"MeioERPGeneralCarrierHistoricalRecord",
                isEdit: false,
                height: 400,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function(row,rows,headData){
                 }
            });

            $("#Code").on("blur", function () {
                var value = $(this).val();
                isCode = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/MeioERPGeneralCarrier/CheckIsGeneralCarrierCode?code=" + value + "&id=" + keyValue).data;
                if (isCode) {
                    Changjie.alert.error("承运商编码已存在，请更换");
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="MeioERPGeneralCarrier"]').mkGetFormData());
       /* postData.strmeioERPGeneralCarrierHistoricalRecordList = JSON.stringify($('#MeioERPGeneralCarrierHistoricalRecord').jfGridGet('rowdatas'));*/
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
