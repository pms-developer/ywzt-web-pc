﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-09-02 16:39
 * 描  述：承运商信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#Enabled').mkselect({ data: [{ value: true, text: "启用" }, { value: false, text: "禁用" }], value: 'value', text: 'text', title: 'text' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "Code": $("#Code").val(),
                    "Name": $("#Name").val(),
                    "Enabled": $("#Enabled").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            //  启用
            $('#btn_enable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认启用选中承运商！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/SetIsEnable', { ids: keyValue, isEnable: true }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            //  禁用
            $('#btn_disable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认禁用选中承运商！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/SetIsEnable', { ids: keyValue, isEnable: false }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/GetPageList',
                headData: [
                    { label: "承运商编码", name: "Code", width: 100, align: "left"},
                    { label: "承运商名称", name: "Name", width: 100, align: "left"},
                    { label: "价格", name: "Price", width: 100, align: "left"},
                    { label: "生效时间", name: "EffectiveTime", width: 100, align: "left"},
                    { label: "月结账号", name: "MonthlyAccount", width: 100, align: "left"},
                    { label: "接口账号", name: "InterfaceAccount", width: 100, align: "left"},
                    {
                        label: "启用状态", name: "Enabled", width: 100, align: "left", formatter: function (value, row) {
                            if (value)
                                return "启用";
                            else
                                return "禁用";
                        }
                    },
                    { label: "创建者名称", name: "CreationName", width: 100, align: "left"},
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true,
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form11',
                        title: "承运商信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/Form?keyValue=' + row.ID,
                        width: 800,
                        height: 600
                    });
                },
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MeioERPGeneralCarrier/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
