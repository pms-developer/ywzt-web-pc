﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-07-10 20:01
 * 描  述：快递打包账单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="MeioErpLogisticsPackageAccount";
var processCommitUrl=top.$.rootUrl + '/BaseInfo/MeioErpLogisticsPackageAccount/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'MeioErpLogisticsPackageAccountDetail',"gridId":'MeioErpLogisticsPackageAccountDetail'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#OwnerID').mkDataSourceSelect({ code: 'OwnerList',value: 'id',text: 'title' });
            $('#State').mkDataItemSelect({ code: 'AccountState' });
            $('#MeioErpLogisticsPackageAccountDetail').jfGrid({
                headData: [
                    {
                        label: '订单号', name: 'OrderNo', width:150,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '出库类型', name: 'DeliveryType', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '仓库', name: 'WarehouseID', width: 100,
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'WarehouseList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    {
                        label: '承运商', name: 'Carrier', width: 100,
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'GeneralCarrierList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    {
                        label: '运单号', name: 'WaybillNo', width:150,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '发货时间', name: 'DeliveryTime', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '快递费', name: 'ExpressFee', width: 100,
                    },
                    {
                        label: '服务费', name: 'ServiceCharge', width: 100,
                    },
                    {
                        label: '是否计算快递费', name: 'IsExpressFee', width: 100,
                        formatter: function (value, row) {
                            if (value) {
                                return "是"
                            } else
                                return "否";
                        }
                    }
                    
                ],
                mainId:"ID",
                bindTable: "MeioErpLogisticsPackageAccountDetail",
                isEdit: false,
                height: 370,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                 }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpLogisticsPackageAccount/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpLogisticsPackageAccount/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="MeioErpLogisticsPackageAccount"]').mkGetFormData());
        postData.strmeioErpLogisticsPackageAccountDetailList = JSON.stringify($('#MeioErpLogisticsPackageAccountDetail').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
