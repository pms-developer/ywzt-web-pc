﻿/* *  
 * 创建人：超级管理员
 * 日  期：2025-03-13 09:58
 * 描  述：账单金额调整
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="MeioErpBillPriceAdjustment";
var processCommitUrl=top.$.rootUrl + '/BaseInfo/MeioErpBillPriceAdjustment/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bid = request('bid');
var bamount = request('bamount');
var currency = request('currency');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            /*if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }*/
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            //页面绑定
            $(".currency").text(currency);
            $("#btnAtta").on("click", function () {
                $(".attaUpload").click();
            })
            $(".attaUpload").change(function (event) {
                const file = event.target.files[0]; // 获取用户选择的文件
                if (file) {
                    const form = new FormData();
                    form.append("file", file);
                    Changjie.httpPostFile(top.$.rootUrl + '/BaseInfo/MieoErpManualBilling/UpLoadFile', form, function (data) {
                        let obj = JSON.parse(data);
                        if (obj != null && obj.info != null && obj.info != "") {
                            if (obj.info == "不支持该格式") {
                                Changjie.alert.error("不支持该格式!!");
                                return;
                            }
                            const form1 = new FormData();
                            form1.append("path", obj.info);

                            const reader = new FileReader(); // 创建 FileReader 对象

                            // 文件读取完成后的回调
                            reader.onload = function (e) {
                                $("#linkAtta").attr("href", obj.info); // 设置图片的 src
                                $("#linkAtta").show();
                            };

                            $("#Atta").val(obj.info);
                            reader.readAsDataURL(file);
                        }
                    });
                }
            })
        },
        initData: function () {
            $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpBillPriceAdjustment/GetformInfoListByBill?bId=' + bid + '&bAmount=' + bamount, function (data) {
                for (var id in data) {
                    if (!!data[id].length && data[id].length > 0) {
                        $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                        subGrid.push({ "tableName": id, "gridId": id });
                    }
                    else {
                        tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        if (data[id].Atta) {
                            $("#linkAtta").attr("href", data[id].Atta);
                            $("#linkAtta").show();
                        } else {
                            $("#linkAtta").hide();
                        }
                    }
                }
            });
            //数据初始化
            /*if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpBillPriceAdjustment/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }*/
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpBillPriceAdjustment/SaveFormByBill', postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        /*var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }*/

        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData())
        };
     return postData;
}
