﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2025-03-14 18:07
 * 描  述：账单信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#OwnerID').mkDataSourceSelect({ code: 'OwnerList',value: 'id',text: 'title' });
            $('#WarehouseID').mkDataSourceSelect({ code: 'Warehouse',value: 'id',text: 'title' });
            $('#AuditStatus').mkDataItemSelect({ code: 'YunTuBillAuditStatus' });
            $('#SettlementStatus').mkDataItemSelect({ code: 'ContractStatus' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpFeeAccount/GetPageList',
                headData: [
                    { label: "出/入库单号", name: "AssociatedNumber", width: 100, align: "left"},
                    { label: "仓库ID", name: "WarehouseID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'Warehouse',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['title']);
                                 }
                             });
                        }},
                    { label: "客户", name: "OwnerID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['title']);
                                 }
                             });
                        }},
                    { label: "计费项名称", name: "ItemName", width: 100, align: "left"},
                    { label: "费用项目", name: "ExpenseItem", width: 100, align: "left"},
                    { label: "报价单", name: "PriceSheetID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'PriceSheetListAll',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['code']);
                                 }
                             });
                        }},
                    { label: "币种", name: "Currency", width: 100, align: "left"},
                    { label: "合计金额", name: "TotalAmount", width: 100, align: "left"},
                    { label: "计算金额", name: "CalculatedAmount", width: 100, align: "left"},
                    { label: "调整金额", name: "AdjustmentAmount", width: 100, align: "left"},
                    { label: "结算状态", name: "SettlementStatus", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ContractStatus',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "结算时间", name: "SettlementTime", width: 100, align: "left"},
                    { label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'YunTuBillAuditStatus',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "审核人", name: "AuditorName", width: 100, align: "left"},
                    { label: "审核时间", name: "AuditTime", width: 100, align: "left"},
                    { label: "备注", name: "Remark", width: 100, align: "left"},
                    { label: "附件", name: "AttachmentUrl", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpFeeAccount/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpFeeAccount/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
