﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-07-10 17:11
 * 描  述：库存管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList',value: 'id',text: 'name' });
            $('#OwnerID').mkDataSourceSelect({ code: 'OwnerList',value: 'id',text: 'title' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            $('#query').on('click', function () {
                page.search({
                    "GoodsCode": $("#GoodsCode").val(),
                    "GoodsName": $("#GoodsName").val(),
                    "WarehouseID": $("#WarehouseID").mkselectGet(),
                    "OwnerID":$("#OwnerID").mkselectGet()
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpInventory/GetPageList',
                headData: [
                    { label: "仓库", name: "WarehouseID", width: 150, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'WarehouseList',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['name']);
                                 }
                             });
                        }},
                    { label: "货主", name: "OwnerID", width: 150, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['title']);
                                 }
                             });
                        }},
                    { label: "货品编码", name: "GoodsCode", width: 100, align: "left"},
                    { label: "货品名称", name: "GoodsName", width: 150, align: "left"},
                    {
                        label: "货品类型", name: "GoodsType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'GoodsType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "在库数量", name: "InQuantity", width: 100, align: "left"},
                    { label: "移入数量", name: "MigrationQuantity", width: 100, align: "left"},
                    { label: "冻结数量", name: "FrozenQuantity", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpInventory/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpInventory/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
