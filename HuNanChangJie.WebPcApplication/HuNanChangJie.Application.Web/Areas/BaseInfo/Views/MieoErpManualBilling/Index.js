﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2025-03-12 14:30
 * 描  述：手工账单
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var warehouseID;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    //page.search();
                }
            });
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList', value: 'id', text: 'name' });
            $('#WarehouseID').change(function () {
                warehouseID = $('#WarehouseID').mkselectGet();
            });
           /* $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);*/
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/MieoErpManualBilling/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            $('#myAudit').on("click", function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                var IsAuditing = $('#gridtable').jfGridValue('IsAuditing');
                if (top.Changjie.checkrow(keyValue)) {
                    if (IsAuditing) {
                        top.Changjie.alert.error("已审批请勿重复操作。");
                        return;
                    }
                    openAudio(keyValue);

                }
            })
            $("#query").on("click", function () {
                page.search();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MieoErpManualBilling/GetPageList',
                headData: [
                    { label: "客户编码", name: "OwnerCode", width: 100, align: "left" },
                    { label: "客户名称", name: "OwnerName", width: 100, align: "left" },
                    { label: "仓库", name: "WarehouseName", width: 100, align: "left"},
                    { label: "费用类型", name: "FeeType", width: 100, align: "left"},
                    { label: "费用项目", name: "FeeItem", width: 100, align: "left" },
                    { label: "关联单号", name: "LinkCode", width: 100, align: "left" },
                    { label: "金额", name: "Amount", width: 100, align: "left" },
                    { label: "币种", name: "Currency", width: 100, align: "left"},
                    { label: "成本", name: "Cost", width: 100, align: "left"},
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left" },
                    { label: "创建人", name: "CreationName", width: 100, align: "left" },
                    { label: "备注", name: "Notes", width: 100, align: "left" },
                    {
                        label: "操作", name: "ID", width: 100, align: "left", formatter: function (value, row) {
                            var htmlStr = "<a style='color:blue;' onclick='openEdit(\"" + value + "\")' >编辑</a>"
                                + "&nbsp;<a style='color:blue;' onclick='openDelete(\"" + value + "\")' >删除</a>";
                            if (row["IsAuditing"] != true) {
                                htmlStr += "&nbsp;<a style='color:blue;' onclick='openAudio(\"" + value + "\")' >审核</a>";
                            }
                            return htmlStr;
                        }
                    },
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            param.StartTime = startTime;
            param.EndTime = endTime;
            param.OwnerCode = $("#OwnerCode").val();
            param.OwnerName = $("#OwnerName").val();
            param.WarehouseID = warehouseID;
            param.FeeType = $("#FeeType").val();
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MieoErpManualBilling/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MieoErpManualBilling/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
};


var openDelete = function (id) {
    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
        if (res) {
            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MieoErpManualBilling/DeleteForm', { keyValue: id }, function () {
                refreshGirdData();
            });
        }
    });
}

var openEdit = function (id) {
    top.Changjie.layerForm({
        id: 'form',
        title: "编辑",
        isShowConfirmBtn: true,
        url: top.$.rootUrl + '/BaseInfo/MieoErpManualBilling/Form?keyValue=' + id + '&viewState=1',
        width: 800,
        height: 600,
        callBack: function (id) {
            return top[id].acceptClick(refreshGirdData);
        }
    });
}


var openAudio = function (id) {
    top.Changjie.layerConfirm('是否确认审核该项！', function (res) {
        if (res) {
            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MieoErpManualBilling/AuditingForm', { keyValue: id }, function () {
                refreshGirdData();
            });
        }
    });
}