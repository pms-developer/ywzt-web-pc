﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-07-11 00:00
 * 描  述：账单管理
 */
var refreshGirdData;
var moduleId = request("moduleId");
var isEditAccountAdditionalCost = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);

            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');

            $('#State').mkDataItemSelect({ code: 'AccountState' });
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList', value: 'id', text: 'name' });
            $('#OwnerID').mkDataSourceSelect({ code: 'OwnerList', value: 'id', text: 'title' });
            $('#IsPushOMS').mkselect({ data: [{ value: "false", text: "未审核" }, { value: "true", text: "已审核" }], value: 'value', text: 'text', title: 'text' })
            /*$('#AccountTimeType').mkselect({ data: [{ value: "day", text: "日" }, { value: "month", text: "月" }], value: 'value', text: 'text', title: 'text' });*/

            $('#AccountTimeType').change(function () {
                var type = $('#AccountTimeType').mkselectGet();
                if (type == "day") {
                    $("#AccountTimeDay").show();
                    $("#AccountTimeMonth").hide();
                }
                else if (type == "month") {
                    $("#AccountTimeDay").hide();
                    $("#AccountTimeMonth").show();
                }
            });
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    //page.search();
                }
            });

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            $('#query').on('click', function () {
                console.log($("#IsPushOMS").mkselectGet());
                page.search({
                    "State": $("#State").mkselectGet(),
                    "WarehouseID": $("#WarehouseID").mkselectGet(),
                    "OwnerID": $("#OwnerID").mkselectGet(),
                    "IsPushOMS": $("#IsPushOMS").mkselectGet(),
                    "AssociateNo": $("#OrderNumber").val(),
                    "DeliveryNo": $("#OrderNumber").val(),
                    "CheckInventoryCode": $("#OrderNumber").val(),
                    "InStorageNo": $("#OrderNumber").val()
                });
            });

            $('#pushWarehouseRentAccount').on('click', function () {
                var selectedRow = $('#gridtableWarehouseRentAccount').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要推送的仓租账单");
                    return false;
                }
                Changjie.loading(true, '正在推送...');
                var accountIDList = [];
                selectedRow.forEach(function (row) {
                    accountIDList.push(row.ID)
                });

                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/Account/PushWarehouseRentAccount', { keyValue: accountIDList }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("推送成功");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });

            $('#pushInStorageAccount').on('click', function () {
                var selectedRow = $('#gridtableInStorageAccount').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要推送的入库账单");
                    return false;
                }
                Changjie.loading(true, '正在推送...');
                var accountIDList = [];
                selectedRow.forEach(function (row) {
                    accountIDList.push(row.ID)
                });

                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/Account/PushInStorageAccount', { keyValue: accountIDList }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("推送成功");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });

            $('#pushDeliveryAccount').on('click', function () {
                var selectedRow = $('#gridtableDeliveryAccount').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要推送的出库账单");
                    return false;
                }
                Changjie.loading(true, '正在推送...');
                var accountIDList = [];
                selectedRow.forEach(function (row) {
                    accountIDList.push(row.ID)
                });

                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/Account/PushDeliveryAccount', { keyValue: accountIDList }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("推送成功");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });

            $('#pushCheckInventoryAccount').on('click', function () {
                var selectedRow = $('#gridtableCheckInventoryAccount').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要推送的盘点账单");
                    return false;
                }
                Changjie.loading(true, '正在推送...');
                var accountIDList = [];
                selectedRow.forEach(function (row) {
                    accountIDList.push(row.ID)
                });

                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/Account/PushCheckInventoryAccount', { keyValue: accountIDList }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("推送成功");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });

            $('#pushLogisticsPackageAccount').on('click', function () {
                var selectedRow = $('#gridtableLogisticsPackageAccount').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要推送的快递打包账单");
                    return false;
                }
                Changjie.loading(true, '正在推送...');
                var accountIDList = [];
                selectedRow.forEach(function (row) {
                    accountIDList.push(row.ID)
                });

                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/Account/PushLogisticsPackageAccount', { keyValue: accountIDList }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("推送成功");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });

            $('#holiday').on('click', function () {
                var data = top.Changjie.httpGet(top.$.rootUrl + '/BaseInfo/Account/GetHoliday').data;
                top.Changjie.alert.success(data);
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtableWarehouseRentAccount').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpWarehouseRentAccount/GetPageList',
                headData: [
                    {
                        label: "发货单号", name: "AssociateNo", width: 150, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"openWarehouseRentAccountEdit('" + row['ID'] + "')\" >" + v + "</a>"
                        }
                    },
                    {
                        label: "操作", name: "option", width: 50, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"AccountAdditionalCost('" + row['ID'] + "',1," + row.IsPushOMS + ")\" >附加费</a > "
                        }
                    },
                    { label: "仓库编码", name: "WarehouseCode", width: 100, align: "left" },
                    { label: "仓库名称", name: "WarehouseName", width: 150, align: "left" },
                    {
                        label: "仓库类型", name: "WarehouseType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'WarehouseType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "货主编码", name: "OwnerCode", width: 100, align: "left" },
                    { label: "货主名称", name: "OwnerName", width: 150, align: "left" },
                    { label: "账单日期", name: "AccountDate", width: 150, align: "left" },
                    {
                        label: "操作费", name: "AmountSubtotal", width: 100, align: "left",
                        formatter: function (value, row) {
                            return row.TotalAmount - value;
                        }
                    },
                    { label: "合计金额(CNY)", name: "TotalAmount", width: 100, align: "left" },
                    {
                        label: "状态", name: "State", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'AccountState',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "IsPushOMS", width: 100, align: "left",
                        formatter: function (value) {
                            if (value)
                                return "已审核";
                            else
                                return "未审核";
                        }
                    },
                    {
                        label: "审核人", name: "ModificationName", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS) {
                                if (value == null || value == "")
                                    return "自动审核";
                                else
                                    return value;
                            } else
                                return "";
                        }
                    },
                    {
                        label: "审核时间", name: "ModificationDate", width: 150, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS)
                                return value;
                            else
                                return "";
                        }
                    },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "仓租账单明细信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpWarehouseRentAccount/Form?keyValue=' + row.ID,
                        width: 1000,
                        height: 600
                    });
                },
                mainId: 'ID',
                isPage: true,
                isMultiselect: true,
                height: 570,
            });
            $('#gridtableLogisticsPackageAccount').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpLogisticsPackageAccount/GetPageList',
                headData: [
                    {
                        label: "订单号", name: "OrderNo", width: 150, align: "left", formatter: function (v, row) {
                            if (v.length > 0) {
                                return v.substr(0, v.length-1);
                            } else {
                                return v;
                            }
                        }
                    },
                    {
                        label: "操作", name: "option", width: 50, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"AccountAdditionalCost('" + row['ID'] + "',2," + row.IsPushOMS + ")\" >附加费</a > "
                        }
                    },
                    { label: "账单日期", name: "AccountDate", width: 150, align: "left" },
                    { label: "货主编码", name: "OwnerCode", width: 150, align: "left" },
                    { label: "货主名称", name: "OwnerName", width: 200, align: "left" },
                    {
                        label: "订单数", name: "OrderCount", width: 100, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"openLogisticsPackageAccountEdit('" + row['ID'] + "')\" >" + v + "</a>"
                        }
                    },
                    {
                        label: "操作费", name: "AmountSubtotal", width: 100, align: "left",
                        formatter: function (value, row) {
                            return (row.TotalAmount - value - row.ExpressFee).toFixed(2);
                        }
                    },
                    {
                        label: "物流运费", name: "ExpressFee", width: 100, align: "left"
                    },
                    { label: "合计金额(CNY)", name: "TotalAmount", width: 150, align: "left" },
                    {
                        label: "状态", name: "State", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'AccountState',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "IsPushOMS", width: 100, align: "left",
                        formatter: function (value) {
                            if (value)
                                return "已审核";
                            else
                                return "未审核";
                        }
                    },
                    {
                        label: "审核人", name: "ModificationName", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS) {
                                if (value == null || value == "")
                                    return "自动审核";
                                else
                                    return value;
                            } else
                                return "";
                        }
                    },
                    {
                        label: "审核时间", name: "ModificationDate", width: 150, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS)
                                return value;
                            else
                                return "";
                        }
                    },

                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "快递打包明细信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpLogisticsPackageAccount/Form?keyValue=' + row.ID,
                        width: 1000,
                        height: 600
                    });
                },
                mainId: 'ID',
                isPage: true,
                isMultiselect: true,
                height: 570,
            });
            $('#gridtableInStorageAccount').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpInStorageAccount/GetPageList',
                headData: [
                    {
                        label: "入库单号", name: "InStorageNo", width: 150, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"openInStorageAccountEdit('" + row['ID'] + "')\" >" + v + "</a>"
                        }
                    },
                    {
                        label: "操作", name: "option", width: 50, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"AccountAdditionalCost('" + row['ID'] + "',3," + row.IsPushOMS + ")\" >附加费</a > "
                        }
                    },
                    {
                        label: "订单类型", name: "OrderType", width: 100, align: "left"
                    },
                    { label: "账单日期", name: "AccountDate", width: 100, align: "left" },
                    {
                        label: "仓库", name: "WarehouseID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'WarehouseList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    {
                        label: "货主", name: "OwnerID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['title']);
                                }
                            });
                        }
                    },
                    {
                        label: "操作费", name: "AmountSubtotal", width: 100, align: "left",
                        formatter: function (value, row) {
                            return row.TotalAmount - value;
                        }
                    },
                    { label: "合计金额", name: "TotalAmount", width: 100, align: "left" },
                    {
                        label: "状态", name: "State", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'AccountState',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "IsPushOMS", width: 100, align: "left",
                        formatter: function (value) {
                            if (value)
                                return "已审核";
                            else
                                return "未审核";
                        }
                    },
                    {
                        label: "审核人", name: "ModificationName", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS) {
                                if (value == null || value == "")
                                    return "自动审核";
                                else
                                    return value;
                            } else
                                return "";
                        }
                    },
                    {
                        label: "审核时间", name: "ModificationDate", width: 150, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS)
                                return value;
                            else
                                return "";
                        }
                    },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "入库账单明细信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpInStorageAccount/Form?keyValue=' + row.ID,
                        width: 1000,
                        height: 600
                    });
                },
                mainId: 'ID',
                isPage: true,
                isMultiselect: true,
                height: 570,
            });
            $('#gridtableDeliveryAccount').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpDeliveryAccount/GetPageList',
                headData: [
                    {
                        label: "出库单号", name: "DeliveryNo", width: 150, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"openDeliveryAccountEdit('" + row['ID'] + "')\" >" + v + "</a>"
                        }
                    },
                    {
                        label: "操作", name: "option", width: 50, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"AccountAdditionalCost('" + row['ID'] + "',4," + row.IsPushOMS + ")\" >附加费</a > "
                        }
                    },
                    {
                        label: "订单类型", name: "OrderType", width: 100, align: "left"
                    },
                    { label: "账单日期", name: "AccountDate", width: 100, align: "left" },
                    {
                        label: "仓库", name: "WarehouseID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'WarehouseList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    {
                        label: "货主", name: "OwnerID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['title']);
                                }
                            });
                        }
                    },
                    {
                        label: "操作费", name: "AmountSubtotal", width: 100, align: "left",
                        formatter: function (value, row) {
                            return row.TotalAmount - value;
                        }
                    },
                    { label: "合计金额", name: "TotalAmount", width: 100, align: "left" },
                    {
                        label: "状态", name: "State", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'AccountState',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "IsPushOMS", width: 100, align: "left",
                        formatter: function (value) {
                            if (value)
                                return "已审核";
                            else
                                return "未审核";
                        }
                    },
                    {
                        label: "审核人", name: "ModificationName", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS) {
                                if (value == null || value == "")
                                    return "自动审核";
                                else
                                    return value;
                            } else
                                return "";
                        }
                    },
                    {
                        label: "审核时间", name: "ModificationDate", width: 150, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS)
                                return value;
                            else
                                return "";
                        }
                    },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "出库账单明细信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpDeliveryAccount/Form?keyValue=' + row.ID,
                        width: 1000,
                        height: 600
                    });
                },
                mainId: 'ID',
                isPage: true,
                isMultiselect: true,
                height: 600,
            });
            $('#gridtableCheckInventoryAccount').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpCheckInventoryAccount/GetPageList',
                headData: [
                    {
                        label: "盘点单号", name: "CheckInventoryCode", width: 150, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"openCheckInventoryAccountEdit('" + row['ID'] + "')\" >" + v + "</a>"
                        }
                    },
                    {
                        label: "操作", name: "option", width: 50, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"AccountAdditionalCost('" + row['ID'] + "',5," + row.IsPushOMS + ")\" >附加费</a > "
                        }
                    },
                    { label: "账单日期", name: "AccountDate", width: 100, align: "left" },
                    {
                        label: "货主", name: "OwnerID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['title']);
                                }
                            });
                        }
                    },
                    {
                        label: "仓库", name: "WarehouseID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'WarehouseList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },

                    { label: "盘点数", name: "CheckInventoryNum", width: 100, align: "left" },
                    {
                        label: "操作费", name: "AmountSubtotal", width: 100, align: "left",
                        formatter: function (value, row) {
                            return row.OperatingCost - value;
                        }
                    },
                    { label: "合计金额(CNY)", name: "OperatingCost", width: 100, align: "left" },
                    {
                        label: "状态", name: "State", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'AccountState',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "IsPushOMS", width: 100, align: "left",
                        formatter: function (value) {
                            if (value)
                                return "已审核";
                            else
                                return "未审核";
                        }
                    },
                    {
                        label: "审核人", name: "ModificationName", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS) {
                                if (value == null || value == "")
                                    return "自动审核";
                                else
                                    return value;
                            } else
                                return "";
                        }
                    },
                    {
                        label: "审核时间", name: "ModificationDate", width: 150, align: "left",
                        formatter: function (value, row) {
                            if (row.IsPushOMS)
                                return value;
                            else
                                return "";
                        }
                    },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "盘点账单明细信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpCheckInventoryAccount/Form?keyValue=' + row.ID,
                        width: 1000,
                        height: 600
                    });
                },
                mainId: 'ID',
                isPage: true,
                isMultiselect: true,
                height: 570,
            });
            //page.search();
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            param.FristSearchPage = 0;
            $('#gridtableWarehouseRentAccount').jfGridSet('reload', { queryJson: JSON.stringify(param) });
            $('#gridtableLogisticsPackageAccount').jfGridSet('reload', { queryJson: JSON.stringify(param) });
            $('#gridtableInStorageAccount').jfGridSet('reload', { queryJson: JSON.stringify(param) });
            $('#gridtableDeliveryAccount').jfGridSet('reload', { queryJson: JSON.stringify(param) });
            $('#gridtableCheckInventoryAccount').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};

var openWarehouseRentAccountEdit = function (id) {
    top.Changjie.layerForm({
        id: 'form',
        title: "仓租账单明细信息",
        isShowConfirmBtn: false,
        url: top.$.rootUrl + '/BaseInfo/MeioErpWarehouseRentAccount/Form?keyValue=' + id,
        width: 1000,
        height: 600
    });
};

var openInStorageAccountEdit = function (id) {
    top.Changjie.layerForm({
        id: 'form',
        title: "入库账单明细信息",
        isShowConfirmBtn: false,
        url: top.$.rootUrl + '/BaseInfo/MeioErpInStorageAccount/Form?keyValue=' + id,
        width: 1000,
        height: 600
    });
};

var openDeliveryAccountEdit = function (id) {
    top.Changjie.layerForm({
        id: 'form',
        title: "出库账单明细信息",
        isShowConfirmBtn: false,
        url: top.$.rootUrl + '/BaseInfo/MeioErpDeliveryAccount/Form?keyValue=' + id,
        width: 1000,
        height: 600
    });
};

var openCheckInventoryAccountEdit = function (id) {
    top.Changjie.layerForm({
        id: 'form',
        title: "盘点账单明细信息",
        isShowConfirmBtn: false,
        url: top.$.rootUrl + '/BaseInfo/MeioErpCheckInventoryAccount/Form?keyValue=' + id,
        width: 1000,
        height: 600
    });
};

var openLogisticsPackageAccountEdit = function (id) {
    top.Changjie.layerForm({
        id: 'form',
        title: "快递账单明细信息",
        isShowConfirmBtn: false,
        url: top.$.rootUrl + '/BaseInfo/MeioErpLogisticsPackageAccount/Form?keyValue=' + id,
        width: 1000,
        height: 600
    });
};

var AccountAdditionalCost = function (id, type, IsPushOMS) {
    top.Changjie.layerForm({
        id: 'form11',
        title: "账单附加费",
        isShowConfirmBtn: (IsPushOMS ? false : true),
        url: top.$.rootUrl + '/BaseInfo/MeioErpAccountAdditionalCost/Form?accountID=' + id + "&accountType=" + type,
        width: 800,
        height: 600,
        callBack: function (id) {
            //return top[id].acceptClick(refreshGirdData);
            return top[id].acceptClick();
        }
    });
};

