﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-08-14 16:27
 * 描  述：盘点账单
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList',value: 'id',text: 'name' });
            $('#OwnerID').mkDataSourceSelect({ code: 'OwnerList',value: 'id',text: 'title' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpCheckInventoryAccount/GetPageList',
                headData: [
                    { label: "盘点单号", name: "CheckInventoryCode", width: 100, align: "left"},
                    { label: "仓库", name: "WarehouseID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'WarehouseList',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['name']);
                                 }
                             });
                        }},
                    { label: "货主", name: "OwnerID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['title']);
                                 }
                             });
                        }},
                    { label: "盘点数", name: "CheckInventoryNum", width: 100, align: "left"},
                    { label: "操作费", name: "OperatingCost", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpCheckInventoryAccount/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpCheckInventoryAccount/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
