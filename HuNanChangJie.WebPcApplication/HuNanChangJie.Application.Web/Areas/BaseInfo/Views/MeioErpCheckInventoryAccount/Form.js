﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-08-14 16:27
 * 描  述：盘点账单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="MeioErpCheckInventoryAccount";
var processCommitUrl=top.$.rootUrl + '/BaseInfo/MeioErpCheckInventoryAccount/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'MeioErpCheckInventoryAccountDetail',"gridId":'MeioErpCheckInventoryAccountDetail'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList',value: 'id',text: 'name' });
            $('#OwnerID').mkDataSourceSelect({ code: 'OwnerList',value: 'id',text: 'title' });
            $('#MeioErpCheckInventoryAccountDetail').jfGrid({
                headData: [
                    {
                        label: '货品编码', name: 'GoodsCode', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '货品名称', name: 'GoodsName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '盘点数量', name: 'CheckInventoryNum', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '盘点单价', name: 'CheckInventoryUnitPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    { label: "操作费", name: "OperatingCost", width: 100, align: "left" },
                ],
                mainId:"ID",
                bindTable: "MeioErpCheckInventoryAccountDetail",
                isEdit: false,
                height: 370,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                 }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpCheckInventoryAccount/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpCheckInventoryAccount/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="MeioErpCheckInventoryAccount"]').mkGetFormData());
        postData.strmeioErpCheckInventoryAccountDetailList = JSON.stringify($('#MeioErpCheckInventoryAccountDetail').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
