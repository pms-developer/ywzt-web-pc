﻿var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $('#Owner').mkDataSourceSelect({ code: 'OwnerList', value: 'id', text: 'title' });
            $("#RechargeCurrency").mkselect({
                //type:"multiple",
                allowSearch: true,
                url: top.$.rootUrl + '/BaseInfo/MeioErpMonetaryManagement/GetCurrencyList',
                value: "CurrencyCode",
                text: "CurrencyName"
            });
            $('#Owner').change(function () {
                var OwnerID = $('#Owner').mkselectGet();
                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/Owner/GetMeioErpOwnerEntity',
                    {
                        keyValue: OwnerID
                    }, function (data) {
                        if (data.code == 200) {
                            $('#Currency').val(data.data.MeioErpOwnerData.Currency);
                            $('#CurrentBalance').val(data.data.MeioErpOwnerData.AccountBalance);
                            $('#CreditLimit').val(data.data.MeioErpOwnerData.CreditLimit);
                        }
                        else
                            top.Changjie.alert.error(data.info);
                    });
            });
        }

    };
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }

        var formdata = $('[data-table="MeioErpOwnerAccountBalanceChangeRecord"]').mkGetFormData();

        keyValue = $("#Owner").mkselectGet();
        var e = document.getElementById("uploadFile").files[0];
        Changjie.loading(true, '正在充值...');
        const form = new FormData();
        form.append("__RequestVerificationToken", $.mkToken)
        form.append("file", e)
        form.append("keyValue", $("#Owner").mkselectGet())
        form.append("amount", $("#Amount").val())
        form.append("Currency", $("#RechargeCurrency").mkselectGet())
        form.append("type", 1)
        form.append("remark", $("#Remark").val())
        Changjie.httpPostFile(top.$.rootUrl + '/BaseInfo/MeioErpCapitalFlow/RechargeAndSettlement?keyValue=' + keyValue, form, function (data) {
            Changjie.loading(false);
            let obj = JSON.parse(data);
            if (obj.code == 200) {
                // 保存成功后才回调
                if (!!callBack) {
                    callBack(formdata.Amount);
                }
                Changjie.layerClose(window.name);
            } else {
                top.Changjie.alert.error(obj.info);
            }
        });


    };
    page.init();
};
