﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-05-15 15:22
 * 描  述：货主管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            //$('#IsValid').mkRadioCheckbox({
            //    type: 'radio',
            //    code: 'SZSF',
            //});
            $('#IsValid').mkselect({ data: [{ value: true, text: "启用" }, { value: false, text: "禁用" }], value: 'value', text: 'text', title: 'text' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "Code": $("#Code").val(),
                    "Name": $("#Name").val(),
                    "IsValid": $("#IsValid").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/Owner/Form',
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            //  启用
            $('#btn_enable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认启用选中货主！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Owner/SetIsValid', { ids: keyValue, isvalid: true }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            //  禁用
            $('#btn_disable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认禁用选中货主！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Owner/SetIsValid', { ids: keyValue, isvalid: false }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/Owner/GetPageList',
                headData: [
                    { label: "货主编码", name: "Code", width: 200, align: "left" },
                    { label: "货主名称", name: "Name", width: 300, align: "left" },
                    {
                        label: "账户余额/元", name: "AccountBalance", width: 100, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"openOwnerAccountBalanceInfo('" + row['ID'] + "','" + row['Name'] + "')\" >" + v + "</a>"
                        }
                    },
                    { label: "合约起始时间", name: "ServiceStartDate", width: 100, align: "left" },
                    { label: "合约截止时间", name: "ServiceEndDate", width: 100, align: "left" },
                    {
                        label: "启用状态", name: "IsValid", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'SZSF',
                                callback: function (_data) {
                                    callback(_data.text ? '启用' : '禁用');
                                }
                            });
                        }
                    },
                    { label: "创建用户", name: "CreatedBy", width: 100, align: "left" },
                    { label: "创建时间", name: "CreatedDate", width: 100, align: "left" },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "货主信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/Owner/Form?keyValue=' + row.ID,
                        width: 800,
                        height: 600
                    });
                },
                mainId: 'ID',
                isPage: true,
                //isMultiselect: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Owner/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/BaseInfo/Owner/Form?keyValue=' + keyValue + '&viewState=' + viewState,
            width: 1000,
            height: 800,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
//账户余额
var openOwnerAccountBalanceInfo = function (id,ownerName) {
    top.Changjie.layerForm({
        id: 'form',
        title: "货主【" + ownerName + "】账户余额信息",
        isShowConfirmBtn: false,
        url: top.$.rootUrl + '/BaseInfo/Owner/AccountBalanceInfo?keyValue=' + id,
        width: 1000,
        height: 600,
        callBack: function (id) {
            return top[id].acceptClick(refreshGirdData);
        }
    });
};
