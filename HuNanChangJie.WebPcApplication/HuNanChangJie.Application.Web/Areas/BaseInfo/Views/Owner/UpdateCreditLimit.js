﻿var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {

        }

    };
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }



        Changjie.loading(true, '正在充值...');
        const form = new FormData();
        form.append("__RequestVerificationToken", $.mkToken)
        form.append("keyValue", keyValue)
        form.append("amount", $("#CreditLimit").val())
        Changjie.httpPostFile(top.$.rootUrl + '/BaseInfo/Owner/UpdateCreditLimits?keyValue=' + keyValue, form, function (data) {
            Changjie.loading(false);
            let obj = JSON.parse(data);
            if (obj.code == 200) {
                // 保存成功后才回调
                if (!!callBack) {
                    callBack($("#CreditLimit").val());
                }
                Changjie.layerClose(window.name);
            } else {
                top.Changjie.alert.error(obj.info);
            }
        });


    };
    page.init();
};
