﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-15 15:22
 * 描  述：货主管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var refreshGirdData;
var accountBalance = 0;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
        },
        bind: function () {

            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/Owner/GetAccountBalancePageList',
                headData: [
                    { label: "操作人", name: "OperationBy", width: 100, align: "left" },
                    { label: "操作时间", name: "OperationTime", width: 150, align: "left" },
                    { label: "金额", name: "Amount", width: 80, align: "left" },
                    {
                        label: "操作类型", name: "OperationType", width: 80, align: "left",
                        formatter: function (value) {
                            if (value == 1)
                                return "转入";
                            else if (value == 2)
                                return "转出";
                            else
                                return "未知";
                        }
                    },
                    {
                        label: "操作凭证", name: "OperationCertificate", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            if (value != "" && value != null) {
                                callback('<a style="color: blue" href="' + value + '">下载操作凭证</a>')
                            }
                            else
                                callback('')
                        }
                    },
                    { label: "备注", name: "Remark", width: 100, align: "left" },
                ],
                mainId: 'ID',
                isPage: true,
                height:450
            });

            
            $('#opencredit').on('click', function () {
                Changjie.layerForm({
                    id: 'form12',
                    title: '信用额度',
                    url: top.$.rootUrl + '/BaseInfo/Owner/UpdateCreditLimit?keyValue=' + keyValue,
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        //return top[id].acceptClick(refreshGirdData);
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                $("#CreditLimit").html(parseFloat(data));
                                refreshGirdData();
                            }
                        });

                    }
                });
            });
            //充值
            $('#btn_TopUp').on('click', function () {
                Changjie.layerForm({
                    id: 'form11',
                    title: '充值',
                    url: top.$.rootUrl + '/BaseInfo/Owner/TopUpDialog?keyValue=' + keyValue,
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        //return top[id].acceptClick(refreshGirdData);
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                accountBalance = parseFloat(accountBalance) + parseFloat(data);
                                $("#AccountBalance").html(accountBalance);
                                refreshGirdData();
                            }
                        });

                    }
                });
            });

            //提现
            $('#btn_Withdraw').on('click', function () {
                    top.Changjie.layerConfirm('是否确认提现，提现后余额会清零！', function (res) {
                        if (res) {
                            Changjie.loading(true, '正在提现...');
                            const form = new FormData();
                            form.append("__RequestVerificationToken", $.mkToken)
                            form.append("keyValue", keyValue)
                            form.append("remark", '提现')
                            Changjie.httpPostFile(top.$.rootUrl + '/BaseInfo/Owner/Withdraw?keyValue=' + keyValue, form, function (data) {
                                Changjie.loading(false);
                                let obj = JSON.parse(data);
                                if (obj.code == 200) {
                                    $("#AccountBalance").html("0");
                                    refreshGirdData();
                                } else {
                                    top.Changjie.alert.error(obj.info);
                                }
                            });
                        }
                    });
                
            })
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/Owner/GetformInfoList?keyValue=' + keyValue, function (data) {
                    accountBalance = data["MeioErpOwner"].AccountBalance;
                    $("#AccountBalance").html(accountBalance);
                    $("#CreditLimit").html(data["MeioErpOwner"].CreditLimit);

                });
                page.search();
            }
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            param.OwnerID = keyValue;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        //if (postData == false) return false;
        //$.mkSaveForm(top.$.rootUrl + '/BaseInfo/Owner/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
        //    // 保存成功后才回调
        //    if (!!callBack) {
        //        callBack();
        //    }
        //});
        if (!!callBack) {
            callBack();
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var postData = {};
    postData.strmeioErpOwnerEntity = JSON.stringify($('[data-table="MeioErpOwner"]').mkGetFormData());
    return postData;
}
