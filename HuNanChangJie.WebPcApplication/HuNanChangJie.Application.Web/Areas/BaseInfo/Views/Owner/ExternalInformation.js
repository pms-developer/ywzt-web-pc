﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-15 15:22
 * 描  述：货主管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "edit";
var state = request('state');
var refreshGirdData;
var accountBalance = 0;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
        },
        bind: function () {

            $('#form_tabs').mkFormTab();
            if (state == 1) {
                $('#form_tabs ul li').eq(0).trigger('click');
            } else {
                $('#form_tabs ul li').eq(1).trigger('click');
            }
            var deleteList = "";
            $('#btn_Save').on('click', function () {
                var strWarehousegridtable = JSON.stringify($('#Warehousegridtable').jfGridGet('rowdatas'));
                var strChannelInformationgridtable = JSON.stringify($('#ChannelInformationgridtable').jfGridGet('rowdatas'));
                top.Changjie.httpPost(top.$.rootUrl +  '/BaseInfo/Owner/SaveWarehouseExternal',
                    {
                        keyValue: keyValue, type: type
                        , strWarehousegridtable: strWarehousegridtable, strChannelInformationgridtable: strChannelInformationgridtable, deleteList: deleteList
                    }, function (data) {
                        if (data.code == 200) {
                            top.Changjie.layerClose(window.name);
                        }
                        else
                            top.Changjie.alert.error(data.info);
                    });
            });

            $('#Warehousegridtable').jfGrid({
                headData: [
                    {
                        label: '外部仓库编码', name: 'WhCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['WhCode'] + "</a>")
                        }
                    },
                    {
                        label: '仓库名称', name: 'WhNameCn', width: 135, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['WhNameCn'] + "</a>")
                        }
                    },
                    {
                        label: '国家', name: 'CountryCode', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['CountryCode'] + "</a>")
                        }
                    },
                    {
                        label: '同步时间', name: 'CreationDate', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['CreationDate'] + "</a>")
                        }
                    },
                    {
                        label: '可见状态', name: 'State', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatter: function (value) {
                            if (value == 1)
                                return "可见";
                            else if (value == 2)
                                return "不可见";
                            else
                                return "可见";
                        }
                    },
                    {
                        label: "操作", name: "CreationDate", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            var textbeValue;
                            if (row['State'] === '1') {
                                textbeValue = '设置为不可见';
                            } else if (row['State'] === '2') {
                                textbeValue = '设置为可见';
                            } else {
                                textbeValue = '设置为不可见';
                            }
                            callback("<a style='color:blue' onclick=\"isvisual('0','" + row['ID'] + "')\" ><span id='textbe" + row['ID'] + "'>" + textbeValue + "</span></a>")
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Warehousegridtable",
            });

            $('#ChannelInformationgridtable').jfGrid({
                headData: [
                    {
                        label: '外部仓库编码', name: 'WhCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['WhCode'] + "</a>")
                        }
                    },
                    {
                        label: '渠道编码', name: 'ChannelCode', width: 135, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['ChannelCode'] + "</a>")
                        }
                    },
                    {
                        label: '渠道名称', name: 'ChannelName', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['ChannelName'] + "</a>")
                        }
                    },
                    {
                        label: '承运商编码', name: 'CarrierCode', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['CarrierCode'] + "</a>")
                        }
                    },
                    {
                        label: '承运商名称', name: 'CarrierName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['CarrierName'] + "</a>")
                        }
                    },
                    {
                        label: '渠道的签名类型', name: 'SignatureType', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['SignatureType'] + "</a>")
                        }
                    },
                    {
                        label: '是否支持保险', name: 'InsuranceFlag', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            var InsuranceFlag = "";
                            if (value == 0)
                                InsuranceFlag = "不支持";
                            else if (value == 1)
                                InsuranceFlag = "支持";
                            else
                                InsuranceFlag = "未知";
                            callback("<a class='ahide" + row['ID'] + "' >" + InsuranceFlag + "</a>")
                        }
                    },
                    {
                        label: '渠道类型', name: 'ChannelType', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            var ChannelType = "";
                            if (value == 1)
                                ChannelType= "需获取面单（打单系统）";
                            else if (value == 2)
                                ChannelType= "无需面单";
                            else if (value == 3)
                                ChannelType= "上传物流面单（用户上传）";
                            else
                                ChannelType= "未知";
                            callback("<a class='ahide" + row['ID'] + "' >" + ChannelType + "</a>")
                        }
                    },
                    {
                        label: '同步时间', name: 'CreationDate', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a class='ahide" + row['ID'] +"' >" + row['CreationDate'] + "</a>")
                        }
                    },
                    {
                        label: '可见状态', name: 'State', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            var Statetext = "";
                            if (value == 1) {
                                Statetext = "可见";
                            } else {
                                $(".ahide" + row['ID']).hide();
                                Statetext = "不可见";
                            }
                            callback("<a >" + Statetext + "</a>")
                        }
                    },
                    {
                        label: "操作", name: "CreationDate", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            var textbeValue;
                            if (row['State'] == 1) {
                                $(".ahide" + row['ID']).show();
                                textbeValue = '设置为不可见';
                            } else {
                                $(".ahide" + row['ID']).hide();
                                textbeValue = '设置为可见';
                            }
                            callback("<a style='color:blue' onclick=\"isvisual('1','" + row['ID'] + "')\" ><span id='textbe" + row['ID'] +"'>" + textbeValue + "</span></a>")
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "ChannelInformationgridtable",
            });

        },
        initData: function () {

            if (!!keyValue) {
                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/Owner/GetOwnerWarehouseList', {
                    keyValue: keyValue
                }, function (data) {
                    if (data.code == 200) {
                        $('#Warehousegridtable').jfGridSet('refreshdata', data.data.Warehousegridtable);
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/Owner/GetOwnerChannelInformationList', {
                    keyValue: keyValue
                }, function (data) {
                    if (data.code == 200) {
                        $('#ChannelInformationgridtable').jfGridSet('refreshdata', data.data.ChannelInformationgridtable);
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
                page.search();
            }
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            param.OwnerID = keyValue;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        },
    };
    
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}



var isvisual = function (v,q) {
    if (v==1) {
        $(".ahide" + q).hide();
        $("#textbe" + q).text("设置为可见");
    } else {
        $(".ahide" + q).show();
        $("#textbe" + q).text("设置为不可见");
    }
}
var isvisual = function (v, q) {

    var row = $('#Warehousegridtable').jfGridGet('rowdata', q);
    if (v == 1) {
        row = $('#ChannelInformationgridtable').jfGridGet('rowdata', q);
    }

    if (row.State == 1) {
        $(".ahide" + q).hide();
        $("#textbe" + q).text("设置为可见");
        updateRowState(q, 2); // 更新状态为不可见
    } else {
        $(".ahide" + q).show();
        $("#textbe" + q).text("设置为不可见");
        updateRowState(q, 1); // 更新状态为可见
    }
}

var updateRowState = function (id, newState) {
    var row = $('#Warehousegridtable').jfGridGet('rowdata', id);
    if (row) {
        row['State'] = newState;
        $('#Warehousegridtable').jfGridSet('updateRow', { rowid: id, row: row });
    }
    row = $('#ChannelInformationgridtable').jfGridGet('rowdata', id);
    if (row) {
        row['State'] = newState;
        $('#ChannelInformationgridtable').jfGridSet('updateRow', { rowid: id, row: row });
    }
}
