﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-15 15:22
 * 描  述：货主管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpContract";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/Owner/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'MeioErpOwner', "gridId": 'MeioErpOwner' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
            $('input[name="WarehouseID"]').prop('checked', false);
            $('input[name="Platform"]').prop('checked', false);
            $('input[name="UseSystem"]').prop('checked', false);
        },
        bind: function () {
            $('#WarehouseID').mkRadioCheckbox({
                type: 'checkbox',
                dataType: 'dataSource',
                code: 'WarehouseList',
                value: 'id',
                text: 'code',
            });

            $('#Platform').mkRadioCheckbox({
                type: 'checkbox',
                dataType: 'dataItem',
                code: 'PlatformList'
            });

            $('#UseSystem').mkRadioCheckbox({
                type: 'checkbox',
                dataType: 'dataItem',
                code: 'UseSystemList'
            });

            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $('#IsValid').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#PushAccountType').mkRadioCheckbox({
                type: 'radio',
                code:'PushAccountType'
            });
            $('#PaymentMethod').mkRadioCheckbox({
                type: 'radio',
                code: 'PaymentMethod'
            });
            $("#Currency").mkselect({
                //type:"multiple",
                allowSearch: true,
                url: top.$.rootUrl + '/BaseInfo/MeioErpMonetaryManagement/GetCurrencyList',
                value: "CurrencyCode",
                text: "CurrencyName"
            });

            $('#MeioErpOwnerThirdpartySystem').jfGrid({
                headData: [
                    {
                        label: '系统名称', name: 'SystemName', width: 150, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '第三方系统'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                                //var systemdata = $("#MeioErpOwnerThirdpartySystem").jfGridGet('rowdatas');
                                //console.log("systemdata" + JSON.stringify(systemdata));
                                //var isexist = 0;
                                //var systemname = row.SystemName;
                                //systemdata.forEach(function (systemdataitem) {
                                //    if (systemdataitem.SystemName == row.SystemName) {
                                //        isexist ++;
                                //        row.SystemName = '';
                                //    }
                                //});
                                //if (isexist>1) {
                                //    Changjie.alert.error("系统名称【" + systemname + "】已存在，请重新选择");
                                //    //setCellValue("SystemName", row, index, 'MeioErpOwnerThirdpartySystem', '');
                                //    $('#MeioErpOwnerThirdpartySystem').jfGridSet('refreshdata', systemdata);
                                //}
                            },
                            datatype: 'dataItem',
                            code: 'UseSystemList'
                        }
                    },
                    {
                        label: 'AppKey', name: 'AppKey', width: 150, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '第三方系统', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: 'SecretKey', name: 'SecretKey', width: 150, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '第三方系统', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: 'api地址', name: 'ApiUrl', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '第三方系统'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '客户编码', name: 'CustomerCode', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '第三方系统'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合作仓OMS登陆地址', name: 'OMSloginaddress', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '第三方系统'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: 'OMS账号', name: 'OMSaccount', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '第三方系统'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: 'OMS密码', name: 'OMSpwd', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '第三方系统'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: "操作", name: "CurrencyCode", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a style='color:blue' onclick=\"openView('" + row['ID'] + "')\" >仓库</a>&nbsp;<a style='color:blue' onclick=\"openView1('" + row['ID'] + "')\" >物流聚道</a>")
                        }
                    }
                ],
                mainId: "ID",
                isEdit: true,
                height: 450,
            });

            $('#MeioErpOwnerSalesPlatform').jfGrid({
                headData: [
                    {
                        label: '平台名称', name: 'PlatformName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '销售平台'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                               
                            },
                            datatype: 'dataItem',
                            code: 'PlatformList'
                        }
                    }
                    
                ],
                mainId: "ID",
                isEdit: true,
                height: 450,
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/Owner/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                    var strWarehouseId = data["MeioErpOwner"].WarehouseID;
                    console.log(strWarehouseId);
                    if (strWarehouseId != null && strWarehouseId != "") {
                        var warehouseIds = strWarehouseId.split(",");
                        $.each(warehouseIds, function (i, warehouseId) {
                            $('input[name="WarehouseID"][value="' + warehouseId + '"]').prop('checked', true);
                        });
                    }

                    //var strPlatform = data["MeioErpOwner"].Platform;
                    //console.log(strPlatform);
                    //if (strPlatform != null && strPlatform != "") {
                    //    var Platforms = strPlatform.split(",");
                    //    $.each(Platforms, function (i, Platform) {
                    //        $('input[name="Platform"][value="' + Platform + '"]').prop('checked', true);
                    //    });
                    //}

                    //var strUseSystem = data["MeioErpOwner"].UseSystem;
                    //console.log(strUseSystem);
                    //if (strUseSystem != null && strUseSystem != "") {
                    //    var UseSystems = strUseSystem.split(",");
                    //    $.each(UseSystems, function (i, UseSystem) {
                    //        $('input[name="UseSystem"][value="' + UseSystem + '"]').prop('checked', true);
                    //    });
                    //}
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/Owner/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    //var deleteList = [];
    //var isgridpass = true;
    //var errorInfos = [];
    //for (var item in subGrid) {
    //    deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
    //    var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
    //    if (!info.isPass) {
    //        isgridpass = false;
    //        errorInfos.push(info.errorCells);
    //    }
    //}
    //if (!isgridpass) {
    //    for (var i in errorInfos[0]) {
    //        top.Changjie.alert.error(errorInfos[0][i].Msg);
    //    }
    //    return false;
    //}
    //if (type == "add") {
    //    keyValue = mainId;
    //}
    var postData = {};
    postData.strmeioErpOwnerEntity = JSON.stringify($('[data-table="MeioErpOwner"]').mkGetFormData());
    //postData.strEntity = JSON.stringify($('[data-table="MeioErpContract"]').mkGetFormData());
    //postData.deleteList = JSON.stringify(deleteList);
    var thirdpartySystemRows = $("#MeioErpOwnerThirdpartySystem").jfGridGet('rowdatas');
    var salesPlatformRows = $("#MeioErpOwnerSalesPlatform").jfGridGet('rowdatas');

    var isSystemNameNull = false;
    var isAppKeyNull = false;
    var isSecretKeyNull = false;
    var isApiUrlNull = false;
    thirdpartySystemRows.forEach(function (thirdpartySystemRowsItem) {
        if (thirdpartySystemRowsItem.SystemName == "" || thirdpartySystemRowsItem.SystemName == undefined) {
            isSystemNameNull = true;
        }
        if (thirdpartySystemRowsItem.AppKey == "" || thirdpartySystemRowsItem.AppKey == undefined) {
            isAppKeyNull = true;
        }
        if (thirdpartySystemRowsItem.SecretKey == "" || thirdpartySystemRowsItem.SecretKey == undefined) {
            isSecretKeyNull = true;
        }
        if (thirdpartySystemRowsItem.ApiUrl == "" || thirdpartySystemRowsItem.ApiUrl == undefined) {
            isApiUrlNull = true;
        }
    });

    if (isSystemNameNull) {
        top.Changjie.alert.error("系统名称不能为空");
        return false;
    }
    //if (isAppKeyNull) {
    //    top.Changjie.alert.error("AppKey不能为空");
    //    return false;
    //}
    //if (isSecretKeyNull) {
    //    top.Changjie.alert.error("SecretKey不能为空");
    //    return false;
    //}
    //if (isApiUrlNull) {
    //    top.Changjie.alert.error("api地址不能为空");
    //    return false;
    //}

    var isPlatformNameNull = false;
    salesPlatformRows.forEach(function (salesPlatformRowsItem) {
        if (salesPlatformRowsItem.PlatformName == "" || salesPlatformRowsItem.PlatformName == undefined) {
            isPlatformNameNull = true;
        }
    });

    if (isPlatformNameNull) {
        top.Changjie.alert.error("平台名称不能为空");
        return false;
    }

    postData.strMeioErpOwnerThirdpartySystem = JSON.stringify(thirdpartySystemRows);
    postData.strMeioErpOwnerSalesPlatform = JSON.stringify(salesPlatformRows);
    return postData;
}

var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;

    if (row.EditType == 0) {
        row.EditType = 2;
    }

    $cell.html(value);
    $edit.val(value);
};

var openView = function (v) {
    var keyValue = v;
    var Code = $("#Code").val();
    Code = 'Test';
    if (keyValue) {
        top.Changjie.layerForm({
            id: 'view',
            title: "基础信息",
            isShowConfirmBtn: false,
            url: top.$.rootUrl + '/BaseInfo/Owner/ExternalInformation?keyValue=' + Code +'&&state=1',
            width: 1000,
            height: 800,
            callBack: function (id) {
                //return top[id].acceptClick(refreshGirdData);
            }
        });
    }

}
var openView1 = function (v) {
    var keyValue = v;
    var Code = $("#Code").val();
    Code = 'Test';
    if (keyValue) {
        top.Changjie.layerForm({
            id: 'view',
            title: "基础信息",
            isShowConfirmBtn: false,
            url: top.$.rootUrl + '/BaseInfo/Owner/ExternalInformation?keyValue=' + Code + '&&state=2',
            width: 1000,
            height: 800,
            callBack: function (id) {
               // return top[id].acceptClick(refreshGirdData);
            }
        });
    }

}