﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-09-07 14:39
 * 描  述：账单附加费用明细
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpAccountAdditionalCost";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/MeioErpAccountAdditionalCost/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');

var accountId = request('accountId');
var accountType = request('accountType');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
        },
        initData: function () {
            if (!!accountId && !!accountType ) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpAccountAdditionalCost/GetformInfoListByAccountIdAndType?accountId=' + accountId + '&accountType=' + accountType, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                            keyValue = data[id].ID;
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
       
        if (keyValue == null || keyValue == "")
            type = "add"
        else
            type = "edit"
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpAccountAdditionalCost/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }

    var entity = $('body').mkGetFormData();
    entity.AccountID = accountId;
    entity.AccountType = accountType;
    var postData = {
        strEntity: JSON.stringify(entity),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
