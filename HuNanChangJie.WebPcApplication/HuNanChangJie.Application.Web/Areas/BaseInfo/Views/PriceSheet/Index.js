﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-05-10 16:07
 * 描  述：报价单管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#Type').mkDataItemSelect({ code: 'PriceSheetType' });
            $('#IsValid').mkselect({ data: [{ value: true, text: "启用" }, { value: false, text: "禁用" }], value: 'value', text: 'text', title: 'text' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "Code": $("#Code").val(),
                    "Name": $("#Name").val(),
                    "Type": $("#Type").mkselectGet(),
                    "IsValid": $("#IsValid").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/PriceSheet/Form1',
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            $('#copy').on('click', function () {
                //var selectedRow = $('#gridtable').jfGridGet('rowdata');
                //if (selectedRow.length == 0) {
                //    top.Changjie.alert.error("请选择需要复制的计费项");
                //    return false;
                //}
                //if (selectedRow.length > 1) {
                //    top.Changjie.alert.error("只能选择一个计费项进行复制");
                //    return false;
                //}
                //var keyValue = selectedRow[0].ID;

                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'formcopy',
                        title: '复制',
                        //isShowConfirmBtn: true,
                        url: top.$.rootUrl + '/BaseInfo/PriceSheet/Form1?keyValue=' + keyValue + '&iscopy=1',
                        width: 1000,
                        height: 800,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            //  启用
            $('#btn_enable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                //if (top.Changjie.checkrow(selectRow)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});
                var keyValue = $('#gridtable').jfGridValue('ID');
                    top.Changjie.layerConfirm('是否确认启用选中报价单！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/PriceSheet/SetIsValid', { ids: keyValue, isvalid: true }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                //}
            });
            //  禁用
            $('#btn_disable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                //if (top.Changjie.checkrow(selectRow)) {
                //    var ids = "";
                //    selectRow.forEach(function (row) {
                //        ids += "," + row.ID;
                //    });
                var keyValue = $('#gridtable').jfGridValue('ID');
                    top.Changjie.layerConfirm('是否确认禁用选中报价单！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/PriceSheet/SetIsValid', { ids: keyValue, isvalid: false }, function () {
                                refreshGirdData();
                            });
                        }
                    });
               // }
            });
            //操作日志
            $('#log').on('click', function () {
                Changjie.layerForm({
                    id: 'formLog',
                    title: '操作日志',
                    isShowConfirmBtn: false,
                    url: top.$.rootUrl + '/BaseInfo/PriceSheet/LogForm',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/PriceSheet/GetPageList',
                headData: [
                    { label: "报价单号", name: "Code", width: 100, align: "left" },
                    { label: "报价名称", name: "Name", width: 100, align: "left" },
                    {
                        label: "报价类型", name: "Type", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'PriceSheetType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "生效时间", name: "EffectiveTime", width: 100, align: "left" },
                    { label: "失效时间", name: "ExpirationTime", width: 100, align: "left" },
                    {
                        label: "启用状态", name: "IsValid", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'SZSF',
                                callback: function (_data) {
                                    callback(_data.text ? '启用' : '禁用');
                                }
                            });
                        }
                    },
                    { label: "创建人", name: "CreatedBy", width: 100, align: "left" },
                    { label: "创建时间", name: "CreatedDate", width: 100, align: "left" },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "报价单信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/PriceSheet/Form1?keyValue=' + row.ID,
                        width: 1000,
                        height: 800
                    });
                },
                mainId: 'ID',
                isPage: true,
                //isMultiselect: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        var isValid = top.Changjie.httpGet(top.$.rootUrl + "/BaseInfo/PriceSheet/IsValidForm?priceSheetId=" + keyValue).data;
        if (isValid) {
            top.Changjie.alert.warning("报价单已启用，不能删除该报价单信息");
        } else {
            var isEdit = top.Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Contract/IsEditForm?priceSheetId=" + keyValue).data;
            if (!isEdit) {
                top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                    if (res) {
                        top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/PriceSheet/DeleteForm', { keyValue: keyValue }, function () {
                            refreshGirdData();
                        });
                    }
                });
            } else {
                top.Changjie.alert.warning("不能删除该报价单信息，合同已经使用该报价单信息");
            }
        }
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        var isEdit = top.Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Contract/IsEditForm?priceSheetId=" + keyValue).data;
        if (!isEdit) {
            top.Changjie.layerForm({
                id: 'form',
                title: title,
                isShowConfirmBtn: isShowConfirmBtn,
                url: top.$.rootUrl + '/BaseInfo/PriceSheet/Form1?keyValue=' + keyValue + '&viewState=' + viewState,
                width: 1000,
                height: 800,
                callBack: function (id) {
                    return top[id].acceptClick(refreshGirdData);
                }
            });
        } else {
            top.Changjie.alert.warning("不能编辑该报价单信息，合同已经使用该报价单信息");
        }
    }
};
