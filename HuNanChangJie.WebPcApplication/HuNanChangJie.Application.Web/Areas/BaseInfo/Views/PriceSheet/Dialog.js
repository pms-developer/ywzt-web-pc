﻿var acceptClick;
var archivesType = request('archivesType');
var type = request('type');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $("#btn_Search").on("click", function () {
                page.search();
            })
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("")
                page.search();
            })
            var requestUrl = '/BaseInfo/Operation/GetPageList';
            if (archivesType == "operation")
                requestUrl = '/BaseInfo/Operation/GetPageList'
            else if (archivesType == "warehouserent")
                requestUrl = '/BaseInfo/WarehouseRent/GetPageList'
            else if (archivesType == "valueaddedservices")
                requestUrl ='/BaseInfo/ValueAddedServices/GetPageList'

                $('#gridtable').jfGrid({
                    url: top.$.rootUrl + requestUrl,
                    
                    headData: [
                    { label: "档案名称", name: "ItemName", width: 200, align: "left" },
                    { label: "说明", name: "ItemDescription", width: 300, align: "left" },
                    { label: "单位", name: "Unit", width: 100, align: "left" },
                    { label: "备注", name: "Remark", width: 100, align: "center" }, 

                ],
                mainId: 'ID',
                isPage: true,
                height: 400,
                isMultiselect: true
            });
            page.search();

        },
        search: function (param) {
            param = param || {};
            param.IsValid = true;
            param.Type = type;
            if ($("#SearchValue").val() != "") {
                param.ItemName = $("#SearchValue").val()
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            if (archivesType == "operation" || archivesType == "warehouserent")
            {
                if (formdata.length > 1) {
                    Changjie.alert.warning("只能选择一行档案数据");
                    return false;
                }
            }

            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};