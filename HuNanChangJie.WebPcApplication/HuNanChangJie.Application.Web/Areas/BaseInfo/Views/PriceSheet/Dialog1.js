﻿var acceptClick;
var ExpenseType = request('ExpenseType');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $("#btn_Search").on("click", function () {
                page.search();
            })
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("")
                page.search();
            })

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/Operation/GetPageList',

                headData: [
                    { label: "计费项名称", name: "ItemName", width: 150, align: "left" },
                    { label: "费用类型", name: "ExpenseItem", width: 100, align: "left" },
                    { label: "条件项", name: "Conditions", width: 100, align: "left" },
                    { label: "计费单位", name: "Unit", width: 100, align: "left" },
                    { label: "最小值", name: "ItemDetailMinValue", width: 100, align: "left" },
                    { label: "最大值", name: "ItemDetailMaxValue", width: 100, align: "left" },
                    { label: "备注", name: "Remark", width: 100, align: "center" },

                ],
                mainId: 'ID',
                isPage: true,
                height: 400,
                isMultiselect: true,
                rows: 100,
            });
            page.search();

        },
        search: function (param) {
            param = param || {};
            param.IsValid = true;
            param.ExpenseType = ExpenseType;
            if ($("#SearchValue").val() != "") {
                param.ItemName = $("#SearchValue").val()
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};