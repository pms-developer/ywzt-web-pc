﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-10 16:07
 * 描  述：报价单管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpPriceSheet";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/PriceSheet/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var warehouseRentRows = [];
var operationRows = [];
var valueAddedServicesRows = [];
var priceSheetType = 0;
var isCode = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'MeioErpPriceSheetDetail', "gridId": 'MeioErpPriceSheetDetail' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            //Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'PriceSheetCode' }, function (data) {
            //    if (!$('#Code').val()) {
            //        $('#Code').val(data);
            //    }
            //});

            $("#pricesheet_form_tabs").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: false,
            });
            $('#pricesheet_form_tabs').mkFormTab();
            $('#pricesheet_form_tabs ul li').eq(0).trigger('click');
            $('#Type').mkDataItemSelect({ code: 'PriceSheetType' });
            $('#IsValid').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#pricesheet_form_tabs_sub').mkFormTab();
            $('#pricesheet_form_tabs_sub ul li').eq(0).trigger('click');
            //仓库租金列表
            $('#MeioErpPriceSheetDetailWarehouseRent').jfGrid({
                headData: [
                    {
                        label: '阶梯项', name: 'ItemName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '定价/RMB', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                //bindTable: "MeioErpPriceSheetDetail",
                //isEdit: true,
                height: 300,
                isMultiselect: true,
                toolbarposition: "top", showadd: false, showchoose: true,
                onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectWarehouseRent",
                        width: 800,
                        height: 450,
                        title: "选择档案信息",
                        url: top.$.rootUrl + "/BaseInfo/PriceSheet/Dialog?archivesType=warehouserent&type=" + priceSheetType,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    var strIds = "";
                                    for (var i = 0; i < data.length; i++) {
                                        strIds += "," + data[i].ID;
                                    }

                                    if (strIds.length > 0) {
                                        strIds = strIds.substring(1);
                                        var detailData = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Warehouserent/GetWarehouseRentDetail?ids=" + strIds).data;
                                        if (!!detailData) {
                                            for (var i = 0; i < detailData.length; i++) {
                                                var isEx = false;
                                                warehouseRentRows.forEach(function (row) {
                                                    if (row.ItemID == detailData[i].MeioErpWarehouseRentID && row.ItemDetailID == detailData[i].ID) {
                                                        isEx = true;
                                                    }
                                                });

                                                if (!isEx) {
                                                    var row = {};
                                                    row.ItemID = detailData[i].MeioErpWarehouseRentID;
                                                    row.ItemDetailID = detailData[i].ID;
                                                    row.ItemName = detailData[i].MinValue + "~" + detailData[i].MaxValue;
                                                    row.ItemDetailName = detailData[i].ItemName;
                                                    row.ItemDetailMinValue = detailData[i].MinValue;
                                                    row.ItemDetailMaxValue = detailData[i].MaxValue;

                                                    for (var j = 0; j < data.length; j++) {
                                                        if (data[j].ID == detailData[i].MeioErpWarehouseRentID) {
                                                            row.Unit = data[j].Unit;
                                                            break;
                                                        }
                                                    }
                                                    rows.push(row);
                                                }
                                            }

                                            $("#MeioErpPriceSheetDetailWarehouseRent").jfGridSet("addRows", rows);
                                            warehouseRentRows = $('#MeioErpPriceSheetDetailWarehouseRent').jfGridGet('rowdatas');
                                        }
                                    }
                                }
                            });
                        }
                    });
                },
                //onAddRow: function (row, rows) {
                //    row["ID"] = Changjie.newGuid();
                //    row.rowState = 1;
                //    row.SortCode = rows.length;
                //    row.EditType = 1;
                //},
                onMinusRow: function (row, rows, headData) {
                }
            });
            //常规作业列表
            $('#MeioErpPriceSheetDetailOperation').jfGrid({
                headData: [
                    {
                        label: '日单量', name: 'ItemName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '定价/RMB', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "MeioErpPriceSheetDetail",
                //isEdit: true,
                height: 300,
                isMultiselect: true,

                toolbarposition: "top", showadd: false, showchoose: true,
                onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectOperation",
                        width: 800,
                        height: 450,
                        title: "选择档案信息",
                        url: top.$.rootUrl + "/BaseInfo/PriceSheet/Dialog?archivesType=operation&type=" + priceSheetType,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    var strIds = "";
                                    for (var i = 0; i < data.length; i++) {
                                        strIds += "," + data[i].ID;
                                    }

                                    if (strIds.length > 0) {
                                        strIds = strIds.substring(1);
                                        var detailData = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Operation/GetOperationDetail?ids=" + strIds).data;
                                        if (!!detailData) {
                                            for (var i = 0; i < detailData.length; i++) {
                                                var isEx = false;
                                                operationRows.forEach(function (row) {
                                                    if (row.ItemID == detailData[i].MeioErpOperationID && row.ItemDetailID == detailData[i].ID) {
                                                        isEx = true;
                                                    }
                                                });

                                                if (!isEx) {
                                                    var row = {};
                                                    row.ItemID = detailData[i].MeioErpOperationID;
                                                    row.ItemDetailID = detailData[i].ID;
                                                    row.ItemName = detailData[i].MinValue + "~" + detailData[i].MaxValue;
                                                    row.ItemDetailName = detailData[i].ItemName;
                                                    row.ItemDetailMinValue = detailData[i].MinValue;
                                                    row.ItemDetailMaxValue = detailData[i].MaxValue;
                                                    for (var j = 0; j < data.length; j++) {
                                                        if (data[j].ID == detailData[i].MeioErpOperationID) {
                                                            row.Unit = data[j].Unit;
                                                            break;
                                                        }
                                                    }

                                                    rows.push(row);
                                                }
                                            }

                                            $("#MeioErpPriceSheetDetailOperation").jfGridSet("addRows", rows);
                                            operationRows = $("#MeioErpPriceSheetDetailOperation").jfGridGet("rowdatas");
                                        }

                                    }
                                }
                            });
                        }
                    });
                },

                //onAddRow: function (row, rows) {
                //    row["ID"] = Changjie.newGuid();
                //    row.rowState = 1;
                //    row.SortCode = rows.length;
                //    row.EditType = 1;
                //},
                onMinusRow: function (row, rows, headData) {
                }
            });
            //增值服务列表
            $('#MeioErpPriceSheetDetailValueAddedServices').jfGrid({
                headData: [
                    {
                        label: '服务项', name: 'ItemName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '仓库租金'
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '仓库租金'
                    },
                    {
                        label: "作业点", name: "OperationPoint", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'OperationPoint',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: '定价/RMB', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '仓库租金'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '仓库租金'
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '仓库租金'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "MeioErpPriceSheetDetail",
                //isEdit: true,
                height: 300,
                isMultiselect: true,
                toolbarposition: "top", showadd: false, showchoose: true,
                onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectValueAddedServices",
                        width: 800,
                        height: 450,
                        title: "选择档案信息",
                        url: top.$.rootUrl + "/BaseInfo/PriceSheet/Dialog?archivesType=valueaddedservices&type=" + priceSheetType,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var isEx = false;
                                        valueAddedServicesRows.forEach(function (row) {
                                            if (row.ItemID == data[i].ID) {
                                                isEx = true;
                                            }
                                        });

                                        if (!isEx) {
                                            var row = {};
                                            row.ItemID = data[i].ID;
                                            row.ItemName = data[i].ItemName;
                                            row.ItemDetailName = data[i].ItemDescription;
                                            row.Unit = data[i].Unit;

                                            rows.push(row);
                                        }
                                    }

                                    $("#MeioErpPriceSheetDetailValueAddedServices").jfGridSet("addRows", rows);
                                    valueAddedServicesRows = $("#MeioErpPriceSheetDetailValueAddedServices").jfGridGet("rowdatas");
                                }
                            });
                        }
                    });
                },
                //onAddRow: function (row, rows) {
                //    row["ID"] = Changjie.newGuid();
                //    row.rowState = 1;
                //    row.SortCode = rows.length;
                //    row.EditType = 1;
                //},
                onMinusRow: function (row, rows, headData) {
                }
            });

            $('#Type').change(function () {
                priceSheetType = $('[data-table="MeioErpPriceSheet"]').mkGetFormData().Type;
                //清空  仓库租金列表  常规作业列表 增值服务列表
                $('#MeioErpPriceSheetDetailWarehouseRent').jfGridSet("clearallrow");
                $('#MeioErpPriceSheetDetailOperation').jfGridSet("clearallrow");
                $('#MeioErpPriceSheetDetailValueAddedServices').jfGridSet("clearallrow");
                if (priceSheetType == 2) {
                    $('#tabOperation').attr("style", "display:none");
                } else {
                    $('#tabOperation').removeAttr("style");
                }
            });

            $('#addWarehouseRent').on('click', function () {
                Changjie.layerForm({
                    id: "selectWarehouseRent",
                    width: 800,
                    height: 450,
                    title: "选择档案信息",
                    url: top.$.rootUrl + "/BaseInfo/PriceSheet/Dialog?archivesType=warehouserent&type=" + priceSheetType,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                var rows = [];
                                var strIds = "";
                                for (var i = 0; i < data.length; i++) {
                                    strIds += "," + data[i].ID;
                                }

                                if (strIds.length > 0) {
                                    strIds = strIds.substring(1);
                                    var detailData = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Warehouserent/GetWarehouseRentDetail?ids=" + strIds).data;
                                    if (!!detailData) {
                                        for (var i = 0; i < detailData.length; i++) {
                                            var isEx = false;
                                            warehouseRentRows.forEach(function (row) {
                                                if (row.ItemID == detailData[i].MeioErpWarehouseRentID && row.ItemDetailID == detailData[i].ID) {
                                                    isEx = true;
                                                }
                                            });

                                            if (!isEx) {
                                                var row = {};
                                                row.ID = top.Changjie.newGuid();
                                                row.ItemID = detailData[i].MeioErpWarehouseRentID;
                                                row.ItemDetailID = detailData[i].ID;
                                                row.ItemName = detailData[i].MinValue + "~" + detailData[i].MaxValue;
                                                row.ItemDetailName = detailData[i].ItemName;
                                                row.ItemDetailMinValue = detailData[i].MinValue;
                                                row.ItemDetailMaxValue = detailData[i].MaxValue;

                                                for (var j = 0; j < data.length; j++) {
                                                    if (data[j].ID == detailData[i].MeioErpWarehouseRentID) {
                                                        row.Unit = data[j].Unit;
                                                        break;
                                                    }
                                                }
                                                rows.push(row);
                                            }
                                        }

                                        //$("#MeioErpPriceSheetDetailWarehouseRent").jfGridSet("addRows", rows);
                                        $("#MeioErpPriceSheetDetailWarehouseRent").jfGridSet("refreshdata", rows);
                                        warehouseRentRows = $('#MeioErpPriceSheetDetailWarehouseRent').jfGridGet('rowdatas');
                                    }
                                }
                            }
                        });
                    }
                });
            });
            $('#deleteWarehouseRent').on('click', function () {
                var selectRow = $('#MeioErpPriceSheetDetailWarehouseRent').jfGridGet('rowdatas');
                selectRow.forEach(function (row) {
                    $('#MeioErpPriceSheetDetailWarehouseRent').jfGridSet('removeRow', row.ID);
                });
            });
            $('#addOperation').on('click', function () {
                Changjie.layerForm({
                    id: "selectOperation",
                    width: 800,
                    height: 450,
                    title: "选择档案信息",
                    url: top.$.rootUrl + "/BaseInfo/PriceSheet/Dialog?archivesType=operation&type=" + priceSheetType,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                var rows = [];
                                var strIds = "";
                                for (var i = 0; i < data.length; i++) {
                                    strIds += "," + data[i].ID;
                                }

                                if (strIds.length > 0) {
                                    strIds = strIds.substring(1);
                                    var detailData = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Operation/GetOperationDetail?ids=" + strIds).data;
                                    if (!!detailData) {
                                        for (var i = 0; i < detailData.length; i++) {
                                            var isEx = false;
                                            operationRows.forEach(function (row) {
                                                if (row.ItemID == detailData[i].MeioErpOperationID && row.ItemDetailID == detailData[i].ID) {
                                                    isEx = true;
                                                }
                                            });

                                            if (!isEx) {
                                                var row = {};
                                                row.ID = top.Changjie.newGuid();
                                                row.ItemID = detailData[i].MeioErpOperationID;
                                                row.ItemDetailID = detailData[i].ID;
                                                row.ItemName = detailData[i].MinValue + "~" + detailData[i].MaxValue;
                                                row.ItemDetailName = detailData[i].ItemName;
                                                row.ItemDetailMinValue = detailData[i].MinValue;
                                                row.ItemDetailMaxValue = detailData[i].MaxValue;
                                                for (var j = 0; j < data.length; j++) {
                                                    if (data[j].ID == detailData[i].MeioErpOperationID) {
                                                        row.Unit = data[j].Unit;
                                                        break;
                                                    }
                                                }

                                                rows.push(row);
                                            }
                                        }

                                        //$("#MeioErpPriceSheetDetailOperation").jfGridSet("addRows", rows);
                                        $("#MeioErpPriceSheetDetailOperation").jfGridSet("refreshdata", rows); 
                                        operationRows = $("#MeioErpPriceSheetDetailOperation").jfGridGet("rowdatas");
                                    }

                                }
                            }
                        });
                    }
                });
            });
            $('#deleteOperation').on('click', function () {
                var selectRow = $('#MeioErpPriceSheetDetailOperation').jfGridGet('rowdatas');
                selectRow.forEach(function (row) {
                    $('#MeioErpPriceSheetDetailOperation').jfGridSet('removeRow', row.ID);
                });
            });
            $('#addValueAddedServices').on('click', function () {
                Changjie.layerForm({
                    id: "selectValueAddedServices",
                    width: 800,
                    height: 450,
                    title: "选择档案信息",
                    url: top.$.rootUrl + "/BaseInfo/PriceSheet/Dialog?archivesType=valueaddedservices&type=" + priceSheetType,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                var rows = [];
                                for (var i = 0; i < data.length; i++) {
                                    var isEx = false;
                                    valueAddedServicesRows.forEach(function (row) {
                                        if (row.ItemID == data[i].ID) {
                                            isEx = true;
                                        }
                                    });

                                    if (!isEx) {
                                        var row = {};
                                        row.ID = top.Changjie.newGuid();
                                        row.ItemID = data[i].ID;
                                        row.ItemName = data[i].ItemName;
                                        row.ItemDetailName = data[i].ItemDescription;
                                        row.Unit = data[i].Unit;
                                        row.OperationPoint = data[i].OperationPoint;

                                        rows.push(row);
                                    }
                                }

                                //$("#MeioErpPriceSheetDetailValueAddedServices").jfGridSet("addRows", rows);
                                $("#MeioErpPriceSheetDetailValueAddedServices").jfGridSet("refreshdata", rows);
                                valueAddedServicesRows = $("#MeioErpPriceSheetDetailValueAddedServices").jfGridGet("rowdatas");
                            }
                        });
                    }
                });
            });
            $('#deleteValueAddedServices').on('click', function () {
                var selectRow = $('#MeioErpPriceSheetDetailValueAddedServices').jfGridGet('rowdatas');
                selectRow.forEach(function (row) {
                    $('#MeioErpPriceSheetDetailValueAddedServices').jfGridSet('removeRow', row.ID);
                });
            });
            $("#Code").on("blur", function () {
                if (type == "add") {
                    var value = $(this).val();
                    isCode = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/PriceSheet/CheckIsCode?code=" + value + "&id=" + keyValue).data;
                    if (isCode) {
                        Changjie.alert.error("报价单号已存在");
                    }
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/PriceSheet/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                            if (id == "MeioErpPriceSheetDetailWarehouseRent") {
                                warehouseRentRows = data["MeioErpPriceSheetDetailWarehouseRent"];
                            }
                            if (id == "MeioErpPriceSheetDetailOperation") {
                                operationRows = data["MeioErpPriceSheetDetailOperation"];
                            }
                            if (id == "MeioErpPriceSheetDetailValueAddedServices") {
                                valueAddedServicesRows = data["MeioErpPriceSheetDetailValueAddedServices"];
                            }
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;

        var effectiveTime = $('[data-table="MeioErpPriceSheet"]').mkGetFormData().EffectiveTime;
        var expirationTime = $('[data-table="MeioErpPriceSheet"]').mkGetFormData().ExpirationTime;
        if (effectiveTime >= expirationTime) {
            Changjie.alert.warning("失效时间必须大于生效时间");
            return false;
        }

        var verify = true;

        var warhouseRentData = $('#MeioErpPriceSheetDetailWarehouseRent').jfGridGet('rowdatas');
        var operationData = $('#MeioErpPriceSheetDetailOperation').jfGridGet('rowdatas');
        var valueAddedServicesData = $('#MeioErpPriceSheetDetailValueAddedServices').jfGridGet('rowdatas');
        warhouseRentData.forEach(function (row) {
            if (row.Price<=0) {
                verify = false;
            }
        });
        operationData.forEach(function (row) {
            if (row.Price<=0) {
                verify = false;
            }
        });
        valueAddedServicesData.forEach(function (row) {
            if (row.Price <= 0) {
                verify = false;
            }
        });

        if (!verify) {
            Changjie.alert.warning("定价必须大于0");
            return false;
        }

        if (isCode) {
            Changjie.alert.error("报价单号已存在");
            return false;
        }

        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/PriceSheet/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    //for (var item in subGrid) {
    //    deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
    //    var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
    //    if (!info.isPass) {
    //        isgridpass = false;
    //        errorInfos.push(info.errorCells);
    //    }
    //}
    //if (!isgridpass) {
    //    for (var i in errorInfos[0]) {
    //        top.Changjie.alert.error(errorInfos[0][i].Msg);
    //    }
    //    return false;
    //}
    //if (type == "add") {
    //    keyValue = mainId;
    //}
    
    var priceSheetDetailData = [];
    var warhouseRentData = $('#MeioErpPriceSheetDetailWarehouseRent').jfGridGet('rowdatas');
    var operationData = $('#MeioErpPriceSheetDetailOperation').jfGridGet('rowdatas');
    var valueAddedServicesData = $('#MeioErpPriceSheetDetailValueAddedServices').jfGridGet('rowdatas');
    if (warhouseRentData.length > 0) {
        warhouseRentData.forEach(function (item) {
            item.ItemType = 1;
        });
        priceSheetDetailData=priceSheetDetailData.concat(warhouseRentData);
    }
    if (operationData.length > 0) {
        operationData.forEach(function (item) {
            item.ItemType = 2;
        });
        priceSheetDetailData=priceSheetDetailData.concat(operationData);
    }
    if (valueAddedServicesData.length > 0) {
        valueAddedServicesData.forEach(function (item) {
            item.ItemType = 3;
        });
        priceSheetDetailData=priceSheetDetailData.concat(valueAddedServicesData);
    }
    var postData = {};
    postData.strmeioErpPriceSheetDetailList = JSON.stringify(priceSheetDetailData);
    postData.strEntity = JSON.stringify($('[data-table="MeioErpPriceSheet"]').mkGetFormData());
    postData.deleteList = JSON.stringify(deleteList);
   
    return postData;
}
