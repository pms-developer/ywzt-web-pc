﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-10 16:07
 * 描  述：报价单管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpPriceSheet";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/PriceSheet/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var warehouseRentRows = [];
var operationRows = [];
var valueAddedServicesRows = [];
var priceSheetType = 0;
var isCode = false;
var priceSheetDetailRows = [];
var ExpenseType = "";
var iscopy = request('iscopy');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue && (iscopy == null || iscopy == "")) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'MeioErpPriceSheetDetail', "gridId": 'MeioErpPriceSheetDetail' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $('#Currency').mkDataItemSelect({ code: 'Currency' });

            $('#Currency').change(function () {
                var currency = $('#Currency').mkselectGet();
                var columnTextList = $('[class="jfgrid-head-cell"]').children('span').eq(4);
                columnTextList.text("定价(" + currency + ")");
            })

            $('#WarehouseID').mkselect({
                type: 'multiple'
            });
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList', value: 'id', text: 'name' });
            $('#Type').mkDataItemSelect({ code: 'PriceSheetType' });
            $('#IsValid').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#pricesheet_form_tabs_sub').mkFormTab();
            $('#pricesheet_form_tabs_sub ul li').eq(0).trigger('click');

            $('#MeioErpPriceSheetDetail').jfGrid({
                headData: [
                    {
                        label: '计费项名', name: 'ItemName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '费用项目', name: 'ExpenseItem', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '阶梯项', name: 'ItemDetailValue', width: 150, align: "left",
                        formatter: function (value, row) {
                            return row.ItemDetailMinValue + "<" + row.Conditions+"<=" + row.ItemDetailMaxValue;
                        }
                    },
                    {
                        label: '计费单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '定价(USD)', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '', id:'ColumnPrice'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: "操作", name: "option", width: 50, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"DeleteDetail('" + row['ID'] + "')\" >移除</a > "
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "MeioErpPriceSheetDetail",
                //isEdit: true,
                height: 500,
                toolbarposition: "top", showadd: false, showchoose: true,
                //onChooseEvent: function () {
                //    Changjie.layerForm({
                //        id: "selectPriceSheetDetail",
                //        width: 800,
                //        height: 450,
                //        title: "选择计费项信息",
                //        url: top.$.rootUrl + "/BaseInfo/PriceSheet/Dialog1?ExpenseType=" + ExpenseType,
                //        callBack: function (id) {
                //            return top[id].acceptClick(function (data) {
                //                if (!!data) {
                //                    var rows = [];
                //                    for (var i = 0; i < data.length; i++) {
                //                        var isEx = false;
                //                        priceSheetDetailRows.forEach(function (row) {
                //                            if (row.ItemID == data[i].ID) {
                //                                isEx = true;
                //                            }
                //                        });

                //                        if (!isEx) {
                //                            var row = {};
                //                            row.ItemID = data[i].ID;
                //                            row.ItemName = data[i].ItemName;
                //                            row.ExpenseItem = data[i].ExpenseItem;
                //                            row.Conditions = data[i].Conditions;
                //                            row.ItemDetailMinValue = data[i].ItemDetailMinValue;
                //                            row.ItemDetailMaxValue = data[i].ItemDetailMaxValue;
                //                            row.Unit = data[i].Unit;

                //                            rows.push(row);
                //                        }
                //                    }

                //                    $("#MeioErpPriceSheetDetail").jfGridSet("addRows", rows);
                //                    priceSheetDetailRows = $("#MeioErpPriceSheetDetail").jfGridGet("rowdatas");
                //                }
                //            });
                //        }
                //    });
                //},
                //onAddRow: function (row, rows) {
                //    row["ID"] = Changjie.newGuid();
                //    row.rowState = 1;
                //    row.SortCode = rows.length;
                //    row.EditType = 1;
                //},
                onMinusRow: function (row, rows, headData) {
                }
            });

            $('#Type').change(function () {
                priceSheetType = $('[data-table="MeioErpPriceSheet"]').mkGetFormData().Type;
                //清空  仓库租金列表  常规作业列表 增值服务列表
                $('#MeioErpPriceSheetDetailWarehouseRent').jfGridSet("clearallrow");
                $('#MeioErpPriceSheetDetailOperation').jfGridSet("clearallrow");
                $('#MeioErpPriceSheetDetailValueAddedServices').jfGridSet("clearallrow");
                if (priceSheetType == 2) {
                    $('#tabOperation').attr("style", "display:none");
                } else {
                    $('#tabOperation').removeAttr("style");
                }
            });

            $('#addPriceSheetDetail').on('click', function () {
                Changjie.layerForm({
                    id: "selectPriceSheetDetail",
                    width: 800,
                    height: 450,
                    title: "选择计费项信息",
                    url: top.$.rootUrl + "/BaseInfo/PriceSheet/Dialog1?ExpenseType=" + ExpenseType,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                //var rows = [];
                                var strIds = "";
                                for (var i = 0; i < data.length; i++) {
                                    strIds += "," + data[i].ID;
                                }

                                if (strIds.length > 0) {
                                    strIds = strIds.substring(1);
                                    var detailData = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Operation/GetOperationDetail?ids=" + strIds).data;
                                    if (!!detailData) {
                                        for (var j = 0; j < data.length; j++) {
                                            for (var i = 0; i < detailData.length; i++) {
                                                if (data[j].ID == detailData[i].MeioErpOperationID) {
                                                    priceSheetDetailRows.forEach(function (row) {
                                                        if (row.ItemID == detailData[i].MeioErpOperationID && row.ItemDetailID == detailData[i].ID) {
                                                            //删除数组已有计费项
                                                            priceSheetDetailRows = $.grep(priceSheetDetailRows, function (item) {
                                                                return item.ID !== row.ID;
                                                            });
                                                        }
                                                    });

                                                    var row = {};
                                                    row.ID = top.Changjie.newGuid();
                                                    row.ItemID = detailData[i].MeioErpOperationID;
                                                    row.ItemDetailID = detailData[i].ID;
                                                    row.ItemName = data[j].ItemName;
                                                    row.ExpenseItem = data[j].ExpenseItem;
                                                    row.Conditions = data[j].Conditions;
                                                    row.ItemDetailMinValue = detailData[i].MinValue;
                                                    row.ItemDetailMaxValue = detailData[i].MaxValue;
                                                    row.Unit = data[j].Unit;
                                                    row.ExpenseType = ExpenseType;

                                                    priceSheetDetailRows.push(row);

                                                }
                                            }
                                        }
                                        var rows = $.grep(priceSheetDetailRows, function (item) {
                                            return item.ExpenseType == ExpenseType;
                                        });
                                        $("#MeioErpPriceSheetDetail").jfGridSet("refreshdata", rows);
                                    }
                                }
                            }
                        });
                    }
                });
            });
            $('#deletePriceSheetDetail').on('click', function () {
                var selectRow = $('#MeioErpPriceSheetDetail').jfGridGet('rowdatas');
                selectRow.forEach(function (row) {
                    $('#MeioErpPriceSheetDetail').jfGridSet('removeRow', row.ID);
                });
            });
            $("#Code").on("blur", function () {
                if (type == "add") {
                    var value = $(this).val();
                    isCode = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/PriceSheet/CheckIsCode?code=" + value + "&id=" + keyValue).data;
                    if (isCode) {
                        Changjie.alert.error("报价单号已存在");
                    }
                }
            });

            $("#tabWarehouseRent").on("click", function () {
                ExpenseType = "仓租费";
                var rows = $.grep(priceSheetDetailRows, function (item) {
                    return item.ExpenseType == ExpenseType;
                });
                $("#MeioErpPriceSheetDetail").jfGridSet("refreshdata", rows);
            });
            $("#tabStorageFee").on("click", function () {
                ExpenseType = "入库费";
                var rows = $.grep(priceSheetDetailRows, function (item) {
                    return item.ExpenseType == ExpenseType;
                });
                $("#MeioErpPriceSheetDetail").jfGridSet("refreshdata", rows);
            });
            $("#tabDeliveryCharge").on("click", function () {
                ExpenseType = "出库费";
                var rows = $.grep(priceSheetDetailRows, function (item) {
                    return item.ExpenseType == ExpenseType;
                });
                $("#MeioErpPriceSheetDetail").jfGridSet("refreshdata", rows);
            });
            $("#tabValueAddedServices").on("click", function () {
                ExpenseType = "增值服务费";
                var rows = $.grep(priceSheetDetailRows, function (item) {
                    return item.ExpenseType == ExpenseType;
                });
                $("#MeioErpPriceSheetDetail").jfGridSet("refreshdata", rows);
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/PriceSheet/GetformInfoList1?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });

                            priceSheetDetailRows = data["MeioErpPriceSheetDetail"];

                            ExpenseType = "仓租费";
                            var rows = $.grep(priceSheetDetailRows, function (item) {
                                return item.ExpenseType == ExpenseType;
                            });
                            $("#MeioErpPriceSheetDetail").jfGridSet("refreshdata", rows);
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }

                    if (iscopy == 1) {
                        $("#Code").val($("#Code").val() + "-01");
                        $("#Name").val($("#Name").val() + "-复制");
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;

        var effectiveTime = $('[data-table="MeioErpPriceSheet"]').mkGetFormData().EffectiveTime;
        var expirationTime = $('[data-table="MeioErpPriceSheet"]').mkGetFormData().ExpirationTime;
        if (effectiveTime >= expirationTime) {
            Changjie.alert.warning("失效时间必须大于生效时间");
            return false;
        }

        var verify = true;

        var priceSheetDetailData = $('#MeioErpPriceSheetDetail').jfGridGet('rowdatas');

        priceSheetDetailData.forEach(function (row) {
            if (row.Price <= 0) {
                verify = false;
            }
        });

        if (!verify) {
            Changjie.alert.warning("定价必须大于0");
            return false;
        }

        if (isCode) {
            Changjie.alert.error("报价单号已存在");
            return false;
        }

        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/PriceSheet/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];

    var postData = {};
    //postData.strmeioErpPriceSheetDetailList = JSON.stringify($('#MeioErpPriceSheetDetail').jfGridGet('rowdatas'));
    postData.strmeioErpPriceSheetDetailList = JSON.stringify(priceSheetDetailRows);
    postData.strEntity = JSON.stringify($('[data-table="MeioErpPriceSheet"]').mkGetFormData());
    postData.deleteList = JSON.stringify(deleteList);

    return postData;
}

var DeleteDetail = function (id) {
    priceSheetDetailRows = $.grep(priceSheetDetailRows, function (item) {
        return item.ID !== id;
    });

    var rows = $.grep(priceSheetDetailRows, function (item) {
        return item.ExpenseType == ExpenseType;
    });
    $("#MeioErpPriceSheetDetail").jfGridSet("refreshdata", rows);
};
