﻿/* *  
 * 创建人：超级管理员
 * 日  期：2025-02-08 14:02
 * 描  述：物流渠道维护
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="MeioErpLogisticsChannel";
var processCommitUrl=top.$.rootUrl + '/BaseInfo/MeioErpLogisticsChannel/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var isCode = false;
var isName = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#PrimaryClassification').mkDataItemSelect({ code: 'LogisticsChannelPrimaryClassification' });
            $('#SecondaryClassification').mkDataItemSelect({ code: 'LogisticsChannelSecondaryClassification' });
            $('#GeneralCarrier').mkDataSourceSelect({ code: 'GeneralCarrierList', value: 'id', text: 'name' });
            $("#Code").on("blur", function () {
                var value = $(this).val();
                isCode = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/MeioErpLogisticsChannel/CheckIsLogisticsChannelCode?code=" + value + "&id=" + keyValue).data;
                if (isCode) {
                    Changjie.alert.error("运输代码已存在，请更换");
                }
            });

            $("#Name").on("blur", function () {
                var value = $(this).val();
                isName = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/MeioErpLogisticsChannel/CheckIsLogisticsChannelName?name=" + value + "&id=" + keyValue).data;
                if (isName) {
                    Changjie.alert.error("产品已存在，请更换");
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpLogisticsChannel/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;

        if (isCode) {
            Changjie.alert.error("运输代码已存在，请更换");
            return false;
        }
        if (isName) {
            Changjie.alert.error("产品名称已存在，请更换");
            return false;
        }
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpLogisticsChannel/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
     return postData;
}
