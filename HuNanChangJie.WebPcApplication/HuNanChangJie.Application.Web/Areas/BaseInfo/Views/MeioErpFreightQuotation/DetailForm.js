﻿/* *  
 * 创建人：超级管理员
 * 日  期：2025-02-08 16:38
 * 描  述：运费报价--计费规则
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpFreightQuotation";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var billingMode = request('billingMode');//计费方式
var maxWeight = 0;
var maxIndex = 0;
var freightQuotationDetailData = request('freightQuotationDetailData');
var freightQuotationDetailItemData = request('freightQuotationDetailItemData');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'MeioErpFreightQuotationDetail', "gridId": 'MeioErpFreightQuotationDetail' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }

            if (billingMode == 2) {
                $('#MeioErpFreightQuotationDetail2').show();
                $('#MeioErpFreightQuotationDetail1').hide();
            } else {
                $('#MeioErpFreightQuotationDetail1').show();
                $('#MeioErpFreightQuotationDetail2').hide();
            }
        },
        bind: function () {
            $('#Country').mkselect({
                type: 'multiple'
            });
            $('#Country').mkDataItemSelect({ code: 'Country' });

            $('#MeioErpFreightQuotationDetail1').jfGrid({
                headData: [
                    {
                        label: '起始重量', name: 'StartWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        //edit: {
                        //    type: 'input',
                        //    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        //    },
                        //    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        //    }
                        //}
                    },
                    {
                        label: '结束重量', name: 'EndWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                if (parseFloat(row.EndWeight) <= parseFloat(row.StartWeight)) {
                                    $("#jfgrid_toolbar_MeioErpFreightQuotationDetail1").hide();
                                } else {
                                    $("#jfgrid_toolbar_MeioErpFreightQuotationDetail1").show();
                                    maxWeight = row.EndWeight;
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {

                            }
                        }
                    },
                    {
                        label: '运费(RMB/KG)', name: 'Freight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '挂号费(RMB/票)', name: 'RegistrationFee', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: "操作", name: "option", width: 50, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"DeleteDetail1('" + row['ID'] + "')\" >移除</a > "
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "MeioErpFreightQuotationDetail",
                isEdit: true,
                height: 300,
                showdelete: false,
                //rowdatas: [
                //    {
                //        "SortCode": 1,
                //        "rowState": 1,
                //        "ID": Changjie.newGuid(),
                //        "EditType": 1,
                //    }],
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;

                    row.StartWeight = maxWeight;

                    $("#jfgrid_toolbar_MeioErpFreightQuotationDetail1").hide();
                },
                onMinusRow: function (row, rows, headData) {
                },
                beforeMinusRow: function (row) {
                    return true;
                },
            });
            $('#MeioErpFreightQuotationDetail2').jfGrid({
                headData: [
                    {
                        label: '起始重量', name: 'StartWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        //edit: {
                        //    type: 'input',
                        //    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        //    },
                        //    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        //    }
                        //}
                    },
                    {
                        label: '结束重量', name: 'EndWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                if (parseFloat(row.EndWeight) <= parseFloat(row.StartWeight)) {
                                    $("#jfgrid_toolbar_MeioErpFreightQuotationDetail2").hide();
                                } else {
                                    $("#jfgrid_toolbar_MeioErpFreightQuotationDetail2").show();
                                    maxWeight = row.EndWeight;
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {

                            }
                        }
                    },
                    {
                        label: '进位制(KG)', name: 'Scale', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '首重(RMB)', name: 'FirstWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '续重(RMB)', name: 'ContinueWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    //{
                    //    label: '挂号费(RMB/票)', name: 'RegistrationFee', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    //    datatype: 'float', tabname: '',
                    //    edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},
                    {
                        label: "操作", name: "option", width: 50, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"DeleteDetail2('" + row['ID'] + "')\" >移除</a > "
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "MeioErpFreightQuotationDetail",
                isEdit: true,
                height: 300,
                showdelete: false,
                //rowdatas: [
                //    {
                //        "SortCode": 1,
                //        "rowState": 1,
                //        "ID": Changjie.newGuid(),
                //        "EditType": 1,
                //    }],
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;

                    row.StartWeight = maxWeight;

                    $("#jfgrid_toolbar_MeioErpFreightQuotationDetail2").hide();
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            //if (!!keyValue) {
            //    $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/GetformInfoList?keyValue=' + keyValue, function (data) {
            //        for (var id in data) {
            //            if (!!data[id].length && data[id].length > 0) {
            //                $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
            //                subGrid.push({ "tableName": id, "gridId": id });
            //            }
            //            else {
            //                tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
            //            }
            //        }
            //    });
            //}

            if (!!freightQuotationDetailData) {
                $("#Country").attr("readonly", "readonly");
                $("#Area").attr("readonly", "readonly");

                var freightQuotationDetail = $.parseJSON(freightQuotationDetailData);

                if (freightQuotationDetail.length > 0) {
                    $("#Country").mkselectSet(freightQuotationDetail[0].Country);
                    $("#ReferenceAging").val(freightQuotationDetail[0].ReferenceAging);
                    $("#Area").val(freightQuotationDetail[0].Area);
                    $("#MinWeight").val(freightQuotationDetail[0].MinWeight);

                    if (billingMode == 1) {
                        $("#MeioErpFreightQuotationDetail1").jfGridSet("refreshdata", freightQuotationDetail);
                    } else {
                        $("#MeioErpFreightQuotationDetail2").jfGridSet("refreshdata", freightQuotationDetail);
                    }

                    maxWeight = freightQuotationDetail[freightQuotationDetail.length - 1].EndWeight;
                }
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        //var postData = getFormData();
        //if (postData == false) return false;
        //$.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
        //    // 保存成功后才回调
        //    if (!!callBack) {
        //        callBack();
        //    }
        //});
        //var formdata = $("#gridtable").jfGridGet("rowdata");

        if (!$('body').mkValidform()) {
            return false;
        }

        //var formdata = $('[data-table="MeioErpFreightQuotationDetail"]').mkGetFormData();
        //console.log("Country:" + formdata.Country);
        var formdata = {};
        formdata.Country = $("#Country").mkselectGet();
        formdata.ReferenceAging = $("#ReferenceAging").val();
        formdata.Area = $("#Area").val();
        if ($("#MinWeight").val() == "")
            formdata.MinWeight = 0;
        else
            formdata.MinWeight = $("#MinWeight").val();

        var res = formdata.Country.split(",");

        var errorInfo = "";
        if (!!freightQuotationDetailItemData) {
            $.each(res, function (id, Country) {
                console.log("freightQuotationDetailItemData:" + freightQuotationDetailItemData);
                var freightQuotationDetailItem = $.parseJSON(freightQuotationDetailItemData);
                var seachData = $.grep(freightQuotationDetailItem, function (item) {
                    return Country + formdata.Area === item.Country + item.Area;
                });
                if (seachData.length > 0) {
                    errorInfo += ";" + Country;
                    if (formdata.Area != "" && formdata.Area != null) {
                        errorInfo += "-" + formdata.Area;
                    }

                }
            });
        }

        if (errorInfo.length > 0) {
            Changjie.alert.warning("计费规则中已存在国家/地区：" + errorInfo.substring(1));
            return false;
        }


        var formdataDetail = [];
        if (billingMode == 1)
            formdataDetail = $("#MeioErpFreightQuotationDetail1").jfGridGet("rowdatas");
        else
            formdataDetail = $("#MeioErpFreightQuotationDetail2").jfGridGet("rowdatas");

        if (formdataDetail.length == 0) {
            Changjie.alert.warning("请添加重量段规则");
            return false;
        }

        var seachData = $.grep(formdataDetail, function (item) {
            return parseFloat(item.StartWeight) >= parseFloat(item.EndWeight) || item.EndWeight == null;
        });

        if (seachData.length > 0) {
            Changjie.alert.warning("结束重量必须大于起始重量");
            return false;
        }

        $.each(formdataDetail, function (id, detail) {
            if (detail.Freight == null)
                detail.Freight = 0;
            if (detail.RegistrationFee == null)
                detail.RegistrationFee = 0;
            if (detail.Scale == null)
                detail.Scale = 0;
            if (detail.FirstWeight == null)
                detail.FirstWeight = 0;
            if (detail.ContinueWeight == null)
                detail.ContinueWeight = 0;
        });

        if (!!formdata) {
            /*  console.log("formDetail:" + JSON.stringify(formdataDetail))*/
            console.log("formdata:" + JSON.stringify(formdata))
            callBack(formdata, formdataDetail);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="MeioErpFreightQuotation"]').mkGetFormData());
    postData.strmeioErpFreightQuotationDetailList = JSON.stringify($('#MeioErpFreightQuotationDetail').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}

var DeleteDetail1 = function (id) {
    var data = $("#MeioErpFreightQuotationDetail1").jfGridGet('rowdatas');
    newData = $.grep(data, function (item) {
        return item.ID !== id;
    });

    if (newData == null || newData.length == 0) {
        maxWeight = 0;
        $("#jfgrid_toolbar_MeioErpFreightQuotationDetail1").show();
    } else {
        maxWeight = newData[newData.length - 1].EndWeight;
        if (newData[newData.length - 1].EndWeight > newData[newData.length - 1].StartWeight)
            $("#jfgrid_toolbar_MeioErpFreightQuotationDetail1").show();
    }

    var maxnum = 0;
    $.each(newData, function (id, item) {
        if (item.StartWeight !== maxnum) {
            item.StartWeight = maxnum;
        }

        if (newData.length > 1) {
            maxnum = item.EndWeight;
        } else {
            maxnum = 0;
        }
    });

    $("#MeioErpFreightQuotationDetail1").jfGridSet("refreshdata", newData);
};

var DeleteDetail2 = function (id) {
    var data = $("#MeioErpFreightQuotationDetail2").jfGridGet('rowdatas');
    newData = $.grep(data, function (item) {
        return item.ID !== id;
    });

    if (newData == null || newData.length == 0) {
        maxWeight = 0;
        $("#jfgrid_toolbar_MeioErpFreightQuotationDetail2").show();
    } else {
        maxWeight = newData[newData.length - 1].EndWeight;
        if (newData[newData.length - 1].EndWeight > newData[newData.length - 1].StartWeight)
            $("#jfgrid_toolbar_MeioErpFreightQuotationDetail2").show();
    }

    var maxnum = 0;
    $.each(newData, function (id, item) {
        if (item.StartWeight !== maxnum) {
            item.StartWeight = maxnum;
        }

        if (newData.length > 1) {
            maxnum = item.EndWeight;
        } else {
            maxnum = 0;
        }
    });

    $("#MeioErpFreightQuotationDetail2").jfGridSet("refreshdata", newData);
};
