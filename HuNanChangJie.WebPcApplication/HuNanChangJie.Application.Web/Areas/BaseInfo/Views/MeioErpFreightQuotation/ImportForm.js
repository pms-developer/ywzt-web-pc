﻿var acceptClick;
var keyValue = request('keyValue');
var billingMode = request('billingMode');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
            if (billingMode == 1) {
                $("#importmodel1").show();
                $("#importmodel2").hide();
            } else {
                $("#importmodel1").hide();
                $("#importmodel2").show();
            }
        },
        bind: function () {
        }
    };
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        //var formdata = {};

        var file = $('#fileInput')[0].files[0];
        if (file == null) {
            Changjie.alert.warning("请选择需要导入的excel文件");
            return false;
        }

        var reader = new FileReader();
        reader.onload = function (e) {
            var data = e.target.result;
            var isError = verifyExcelData(data);
            if (!isError) {
                return false;
            }
            var workbook = XLSX.read(data, { type: 'binary' });
            var sheet_name_list = workbook.SheetNames;
            var excelData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
            var formdata = displayExcelData(excelData);
            if (!!formdata) {
                callBack(formdata.freightQuotationDetailItem, formdata.freightQuotationDetail);
                Changjie.layerClose(window.name);
                return true;
            }
            else {
                Changjie.alert.warning("您还没有选择任何数据");
            }
            console.log(excelData);
        };
        reader.readAsBinaryString(file);


    };
    page.init();
};

var verifyExcelData = function (data) {
    var header1 = ["国家/地区", "参考时效", "分区", "重量(KG)", "进位制(KG)", "最低计费重(KG)", "运费(RMB/KG)", "挂号费(RMB/票)"];
    var header2 = ["国家/地区", "参考时效", "分区", "重量(KG)", "进位制(KG)", "最低计费重(KG)", "首重", "续重"];

    var workbook = XLSX.read(data, { type: 'binary' });
    var firstSheetName = workbook.SheetNames[0]; // 获取第一个工作表的名称
    var worksheet = workbook.Sheets[firstSheetName];
    var header = getHeaderRow(worksheet); // 获取列头名称
    console.log('列头名称:', header); // 输出列头名称到控制台
    if (billingMode == 1) {
        if (JSON.stringify(header1) != JSON.stringify(header)) {
            top.Changjie.alert.warning("导入模板错误，请选择重量段导入模板");
            return false;
        }
    } else {
        if (JSON.stringify(header2) != JSON.stringify(header)) {
            top.Changjie.alert.warning("导入模板错误，请选择首重续重导入模板");
            return false;
        }
    }
    return true;
};

var displayExcelData = function (excelData) {
    var freightQuotationDetailItem = [];
    var freightQuotationDetail = [];

    var itemID = "";
    var country = "";
    var area = "";
    var minweight = "0";
    var referenceAging = "";

    $.each(excelData, function (id, dataItem) {
        var isrepeatcountry = false;
        if (dataItem["国家/地区"] == country)
            isrepeatcountry = true;

        if (dataItem["国家/地区"] != null && dataItem["国家/地区"] != "") {
            country = dataItem["国家/地区"];
        }

        if ((dataItem["国家/地区"] != null && dataItem["国家/地区"] != "") || (dataItem["分区"] != null && dataItem["分区"] != "")) {
            var detailItemQuery = [];
            if (dataItem["分区"] != null && dataItem["分区"] != "") {
                detailItemQuery = $.grep(freightQuotationDetailItem, function (item) {
                    return item.Country.replace(" ", "") + item.Area.replace(" ", "") == country + dataItem["分区"].replace(" ", "");
                });
            } else {
                detailItemQuery = $.grep(freightQuotationDetailItem, function (item) {
                    return item.Country.replace(" ", "") == country;
                });
            }
            if (detailItemQuery.length == 0) {
                itemID = top.Changjie.newGuid();

                var detailItem = {};

                if (dataItem["国家/地区"] != null && dataItem["国家/地区"] != "") {
                    detailItem.Country = dataItem["国家/地区"];
                    country = dataItem["国家/地区"];
                } else {
                    detailItem.Country = country;
                }

                if (dataItem["分区"] != null && dataItem["分区"] != "") {
                    detailItem.Area = dataItem["分区"];
                    area = dataItem["分区"];
                } else {
                    if (!isrepeatcountry) {
                        area = "";
                    }
                    detailItem.Area = area;
                }
               
                if (dataItem["最低计费重(KG)"] != null && dataItem["最低计费重(KG)"] != "") {
                    detailItem.MinWeight = dataItem["最低计费重(KG)"];
                    minweight = dataItem["最低计费重(KG)"];
                } else {
                    if (!isrepeatcountry) {
                        minweight = "0";
                    }
                    detailItem.MinWeight = minweight;
                }
                if (dataItem["参考时效"] != null && dataItem["参考时效"] != "") {
                    detailItem.ReferenceAging = dataItem["参考时效"];
                    referenceAging = dataItem["参考时效"];
                } else {
                    if (!isrepeatcountry) {
                        referenceAging = "";
                    }
                    detailItem.ReferenceAging = referenceAging;
                }
                detailItem.ItemID = itemID;
                freightQuotationDetailItem.push(detailItem);
            }
        }

        var itemDetail = {};
        itemDetail.FreightQuotationID = top.Changjie.newGuid();
        itemDetail.Country = country;
        itemDetail.Area = area;
        itemDetail.ReferenceAging = referenceAging;
        itemDetail.MinWeight = minweight;
        if (dataItem["重量(KG)"] != null && dataItem["重量(KG)"] != "") {
            var weightstr = dataItem["重量(KG)"].replace("＜W≤", ",").split(",");
            if (weightstr.length == 1) {
                weightstr = dataItem["重量(KG)"].replace("<W≤", ",").split(",");
            }

            itemDetail.StartWeight = weightstr[0];
            itemDetail.EndWeight = weightstr[1];
        }
        if (dataItem["运费(RMB/KG)"] != null && dataItem["运费(RMB/KG)"] != "") {
            itemDetail.Freight = dataItem["运费(RMB/KG)"];
        }
        if (dataItem["挂号费(RMB/票)"] != null && dataItem["挂号费(RMB/票)"] != "") {
            itemDetail.RegistrationFee = dataItem["挂号费(RMB/票)"];
        }
        if (dataItem["进位制(KG)"] != null && dataItem["进位制(KG)"] != "") {
            itemDetail.Scale = dataItem["进位制(KG)"];
        }
        if (dataItem["首重"] != null && dataItem["首重"] != "") {
            itemDetail.FirstWeight = dataItem["首重"];
        }
        if (dataItem["续重"] != null && dataItem["续重"] != "") {
            itemDetail.ContinueWeight = dataItem["续重"];
        }
        itemDetail.ItemID = itemID;
        itemDetail.ID = top.Changjie.newGuid();
        freightQuotationDetail.push(itemDetail);

    });

    var formdata = {};
    formdata.freightQuotationDetailItem = JSON.stringify(freightQuotationDetailItem);
    formdata.freightQuotationDetail = JSON.stringify(freightQuotationDetail);


    return formdata;
};

var getHeaderRow = function (sheet) {
    var headers = [];
    var range = XLSX.utils.decode_range(sheet['!ref']); // 获取范围信息，例如 A1:F10
    var C;
    var R = range.s.r; /* start in the first row */
    for (C = range.s.c; C <= range.e.c; ++C) { /* walk every column in the range */
        var cell_ref = XLSX.utils.encode_cell({ c: C, r: R }); /* generate cell address */
        var cell = sheet[cell_ref];
        headers.push(cell ? cell.v : ''); // 获取列头名称或空字符串
    }
    return headers;
}
