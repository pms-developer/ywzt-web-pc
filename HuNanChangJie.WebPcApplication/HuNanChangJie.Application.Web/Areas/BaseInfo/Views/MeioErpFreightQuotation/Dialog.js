﻿var acceptClick;
var keyValue = request('keyValue');
var effectiveStartTime = request('effectiveStartTime');
var effectiveEndTime = request('effectiveEndTime');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
        }
    };
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }

        var formdata = $('[data-table="MeioErpFreightQuotation"]').mkGetFormData();

        var dateStart = new Date(formdata.EffectiveStartTime.split(' ')[0]);

        var dateEnd = new Date(formdata.EffectiveEndTime.split(' ')[0]);

        console.log("dateStart:" + dateStart);
        console.log("dateEnd:" + dateEnd);


        if (dateStart >= dateEnd) {
            Changjie.alert.warning("有效结束时间必须大于有效开始时间");
            return false;
        }

        formdata.keyValue = keyValue;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/UpdateEffectiveTime',
            formdata,
            function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
};
