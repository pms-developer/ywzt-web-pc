﻿/* *  
 * 创建人：超级管理员
 * 日  期：2025-02-08 16:38
 * 描  述：运费报价
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpFreightQuotation";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');

var billingMode = 1;

var freightQuotationDetail = [];
var freightQuotationDetailItem = [];
var iscopy = request('iscopy');
var isName = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            console.log("iscopy:" + iscopy);
            if (!!keyValue && (iscopy == null || iscopy == "")) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'MeioErpFreightQuotationDetail', "gridId": 'MeioErpFreightQuotationDetail' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $('#GeneralCarrier').mkDataSourceSelect({ code: 'GeneralCarrierList', value: 'id', text: 'name' });
            $('#LogisticsChannel').mkselect({
                value: 'ID',
                text: 'Title',
                allowSearch: true,
            });
            $('#GeneralCarrier').change(function () {
                var generalcarrier = $('#GeneralCarrier').mkselectGet();
                var url=top.$.rootUrl + '/BaseInfo/MeioErpLogisticsChannel/GetLogisticsChannelListByGeneralCarrier?generalCarrier=' + generalcarrier
                var logisticsChannels = Changjie.httpGet(url).data;
                $('#LogisticsChannel').mkselectRefresh({ data: logisticsChannels });
            });

            //$('#LogisticsChannel').mkDataSourceSelect({ code: 'LogisticsChannelList', value: 'id', text: 'name' });
            //$('#BillingMode').mkRadioCheckbox({
            //    type: 'radio',
            //    code: 'FreightQuotationBillingMode',
            //});
            $('#BillingMode').mkDataItemSelect({ code: 'FreightQuotationBillingMode' });

            $('#BillingMode').change(function () {
                //清空计费规则
                freightQuotationDetail.splice(0, freightQuotationDetail.length);
                freightQuotationDetailItem.splice(0, freightQuotationDetailItem.length);

                $("#detailtable").html("");
            });

            $('#Enabled').mkselect({
                data: [{ value: 'true', text: '启用' }, { value: 'false', text: '禁用' }],
                value: 'value',
                text: 'text',
                title: 'text',
            });

            if (type == "add") {
                $('#EffectiveEndTime').val("2099-12-31");
                $('#Enabled').mkselectSet('true');
            }

            $('#add').on('click', function () {
                //billingMode = $("input[name='BillingMode']:checked").val();
                billingMode = $("#BillingMode").mkselectGet();
                if (billingMode == "" || billingMode == null) {
                    Changjie.alert.error("请选择计费方式后，添加计费规则");
                    return false;
                }

                Changjie.layerForm({
                    id: 'form11',
                    title: '添加计费规则',
                    url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/DetailForm?keyValue=' + keyValue + '&billingMode=' + billingMode + '&freightQuotationDetailItemData=' + encodeURIComponent(JSON.stringify(freightQuotationDetailItem)),
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        //return top[id].acceptClick(refreshGirdData);
                        return top[id].acceptClick(function (data, datadetail) {
                            if (!!data) {
                                var res = data.Country.split(",");

                                $.each(res, function (id, Country) {
                                    var itemID = top.Changjie.newGuid();

                                    var detailItem = {};
                                    detailItem.Country = Country;
                                    detailItem.Area = data.Area;
                                    detailItem.MinWeight = data.MinWeight;
                                    detailItem.ReferenceAging = data.ReferenceAging;
                                    detailItem.ItemID = itemID;
                                    freightQuotationDetailItem.push(detailItem);

                                    for (var i = 0; i < datadetail.length; i++) {
                                        var itemDetail = {};
                                        itemDetail.FreightQuotationID = keyValue;
                                        itemDetail.Country = Country;
                                        itemDetail.Area = data.Area;
                                        itemDetail.ReferenceAging = data.ReferenceAging;
                                        itemDetail.MinWeight = data.MinWeight;
                                        itemDetail.StartWeight = datadetail[i].StartWeight;
                                        itemDetail.EndWeight = datadetail[i].EndWeight;
                                        itemDetail.Freight = datadetail[i].Freight;
                                        itemDetail.RegistrationFee = datadetail[i].RegistrationFee;
                                        itemDetail.Scale = datadetail[i].Scale;
                                        itemDetail.FirstWeight = datadetail[i].FirstWeight;
                                        itemDetail.ContinueWeight = datadetail[i].ContinueWeight;
                                        itemDetail.ItemID = itemID;
                                        itemDetail.ID = datadetail[i].ID;
                                        freightQuotationDetail.push(itemDetail);
                                    }
                                });

                                var tablehtml = '<table border="1">';
                                if (billingMode == "1") {
                                    tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>运费（RMB/KG）</th><th>挂号费RMB/件</th><th>操作</th></tr>";
                                } else {
                                    tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>进位制（KG）</th><th>首重（RMB）</th><th>续重（RMB）</th><th>操作</th></tr>";
                                }

                                $.each(freightQuotationDetailItem, function (id, DetailItem) {
                                    var detail = $.grep(freightQuotationDetail, function (item) {
                                        return item.ItemID === DetailItem.ItemID;
                                    });

                                    tablehtml += "<tr><td rowspan='" + detail.length + "'>" + DetailItem.Country + (DetailItem.Area.length > 0 ? "-" + DetailItem.Area : "") + "</td><td rowspan='" + detail.length + "'>" + DetailItem.ReferenceAging + "</td><td rowspan='" + detail.length + "'>" + DetailItem.MinWeight + "</td>";
                                    if (detail.length > 0) {
                                        tablehtml += "<td>" + detail[0].StartWeight + " < W <= " + detail[0].EndWeight + "</td>";
                                        if (billingMode == "1") {
                                            tablehtml += "<td>" + detail[0].Freight + "</td>";
                                            tablehtml += "<td>" + detail[0].RegistrationFee + "</td>";
                                        } else {
                                            tablehtml += "<td>" + detail[0].Scale + "</td>";
                                            tablehtml += "<td>" + detail[0].FirstWeight + "</td>";
                                            tablehtml += "<td>" + detail[0].ContinueWeight + "</td>";
                                        }

                                        tablehtml += "<td rowspan='" + detail.length + "'><a style='color:blue;cursor:pointer;' onclick=\"EditDetail('" + DetailItem.ItemID + "')\" >编辑</a >  <a style='color:blue;cursor:pointer;' onclick=\"DeleteDetail('" + DetailItem.ItemID + "')\" >删除</a ></td>";
                                    }
                                    tablehtml += '</tr>';

                                    for (var i = 1; i < detail.length; i++) {
                                        tablehtml += '<tr>';
                                        tablehtml += "<td>" + detail[i].StartWeight + " < W <= " + detail[i].EndWeight + "</td>";
                                        if (billingMode == "1") {
                                            tablehtml += "<td>" + detail[i].Freight + "</td>";
                                            tablehtml += "<td>" + detail[i].RegistrationFee + "</td>";
                                        } else {
                                            tablehtml += "<td>" + detail[i].Scale + "</td>";
                                            tablehtml += "<td>" + detail[i].FirstWeight + "</td>";
                                            tablehtml += "<td>" + detail[i].ContinueWeight + "</td>";
                                        }

                                        tablehtml += '</tr>';
                                    }

                                });

                                tablehtml += '</table>';
                                $("#detailtable").html(tablehtml);
                            }
                        });
                    }
                });
            });

            $("#RuleName").on("blur", function () {
                var value = $(this).val();
                isName = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/MeioErpFreightQuotation/CheckIsRuleName?name=" + value + "&id=" + keyValue).data;
                if (isName) {
                    Changjie.alert.error("规则名称已存在，请更换");
                }
            });

            $("#import").on('click', function () {

                //billingMode = $("input[name='BillingMode']:checked").val();
                billingMode = $("#BillingMode").mkselectGet();
                if (billingMode == "" || billingMode == null) {
                    Changjie.alert.error("请选择计费方式后，导入计费规则");
                    return false;
                }

                Changjie.layerForm({
                    id: 'formImport',
                    title: '导入计费规则',
                    url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/ImportForm?keyValue=' + keyValue + '&billingMode=' + billingMode,
                    width: 300,
                    height: 200,
                    callBack: function (id) {
                        //return top[id].acceptClick(refreshGirdData);
                        return top[id].acceptClick(function (freightQuotationDetailItemStr, freightQuotationDetailStr) {
                            freightQuotationDetailItem = $.parseJSON(freightQuotationDetailItemStr);
                            freightQuotationDetail = $.parseJSON(freightQuotationDetailStr);

                            var tablehtml = '<table border="1">';
                            if (billingMode == "1") {
                                tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>运费（RMB/KG）</th><th>挂号费RMB/件</th><th>操作</th></tr>";
                            } else {
                                tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>进位制（KG）</th><th>首重（RMB）</th><th>续重（RMB）</th><th>操作</th></tr>";
                            }

                            $.each(freightQuotationDetailItem, function (id, DetailItem) {
                                var detail = $.grep(freightQuotationDetail, function (item) {
                                    return item.ItemID === DetailItem.ItemID;
                                });

                                tablehtml += "<tr><td rowspan='" + detail.length + "'>" + DetailItem.Country + (DetailItem.Area.length > 0 ? "-" + DetailItem.Area : "") + "</td><td rowspan='" + detail.length + "'>" + DetailItem.ReferenceAging + "</td><td rowspan='" + detail.length + "'>" + DetailItem.MinWeight + "</td>";
                                if (detail.length > 0) {
                                    tablehtml += "<td>" + detail[0].StartWeight + " < W <= " + detail[0].EndWeight + "</td>";
                                    if (billingMode == "1") {
                                        tablehtml += "<td>" + (detail[0].Freight == null ? "0" : detail[0].Freight) + "</td>";
                                        tablehtml += "<td>" + (detail[0].RegistrationFee == null ? "0" : detail[0].RegistrationFee) + "</td>";
                                    } else {
                                        tablehtml += "<td>" + (detail[0].Scale == null ? "0" : detail[0].Scale) + "</td>";
                                        tablehtml += "<td>" + (detail[0].FirstWeight == null ? "0" : detail[0].FirstWeight) + "</td>";
                                        tablehtml += "<td>" + (detail[0].ContinueWeight == null ? "0" : detail[0].ContinueWeight) + "</td>";
                                    }

                                    tablehtml += "<td rowspan='" + detail.length + "'><a style='color:blue;cursor:pointer;' onclick=\"EditDetail('" + DetailItem.ItemID + "')\" >编辑</a >  <a style='color:blue;cursor:pointer;' onclick=\"DeleteDetail('" + DetailItem.ItemID + "')\" >删除</a ></td>";
                                }
                                tablehtml += '</tr>';

                                for (var i = 1; i < detail.length; i++) {
                                    tablehtml += '<tr>';
                                    tablehtml += "<td>" + detail[i].StartWeight + " < W <= " + detail[i].EndWeight + "</td>";
                                    if (billingMode == "1") {
                                        tablehtml += "<td>" + (detail[i].Freight == null ? "0" : detail[i].Freight) + "</td>";
                                        tablehtml += "<td>" + (detail[i].RegistrationFee == null ? "0" : detail[i].RegistrationFee) + "</td>";
                                    } else {
                                        tablehtml += "<td>" + (detail[i].Scale == null ? "0" : detail[i].Scale) + "</td>";
                                        tablehtml += "<td>" + (detail[i].FirstWeight == null ? "0" : detail[i].FirstWeight) + "</td>";
                                        tablehtml += "<td>" + (detail[i].ContinueWeight == null ? "0" : detail[i].ContinueWeight) + "</td>";
                                    }

                                    tablehtml += '</tr>';
                                }

                            });

                            tablehtml += '</table>';
                            $("#detailtable").html(tablehtml)
                        });
                    }
                });

            });
        },
        initData: function () {
            if (!!keyValue) {
                $("#GeneralCarrier").attr("readonly", "readonly");
                $("#LogisticsChannel").attr("readonly", "readonly");
                $("#BillingMode").attr("readonly", "readonly");

                $("#GeneralCarrier").attr('style', 'background-color: #f0f0f0;');
                $("#LogisticsChannel").attr('style', 'background-color: #f0f0f0;');
                $("#BillingMode").attr('style', 'background-color: #f0f0f0;');

                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                    if (iscopy == 1) {
                        $("#RuleName").val($("#RuleName").val() + "-复制");
                        $("#GeneralCarrier").removeAttr("readonly");
                        $("#LogisticsChannel").removeAttr("readonly");
                        $("#BillingMode").removeAttr("readonly");

                        $("#GeneralCarrier").removeAttr("style");
                        $("#LogisticsChannel").removeAttr("style");
                        $("#BillingMode").removeAttr("style");
                    }

                    //billingMode = $("input[name='BillingMode']:checked").val();
                    billingMode = $("#BillingMode").mkselectGet();

                    freightQuotationDetail = data["MeioErpFreightQuotationDetail"];
                    freightQuotationDetailItem = data["MeioErpFreightQuotationDetailItem"];

                    freightQuotationDetailItem.sort(function (a, b) {
                        return a.ItemID.localeCompare(b.ItemID);
                    });

                    freightQuotationDetail.sort(function (a, b) {
                        return a.ItemID.localeCompare(b.ItemID);
                    });

                    var tablehtml = '<table border="1">';
                    if (billingMode == "1") {
                        tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>运费（RMB/KG）</th><th>挂号费RMB/件</th><th>操作</th></tr>";
                    } else {
                        tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>进位制（KG）</th><th>首重（RMB）</th><th>续重（RMB）</th><th>操作</th></tr>";
                    }

                    $.each(freightQuotationDetailItem, function (id, DetailItem) {
                        var detail = $.grep(freightQuotationDetail, function (item) {
                            return item.ItemID === DetailItem.ItemID;
                        });

                        tablehtml += "<tr><td rowspan='" + detail.length + "'>" + DetailItem.Country + (DetailItem.Area.length > 0 ? "-" + DetailItem.Area : "") + "</td><td rowspan='" + detail.length + "'>" + DetailItem.ReferenceAging + "</td><td rowspan='" + detail.length + "'>" + DetailItem.MinWeight + "</td>";
                        if (detail.length > 0) {
                            tablehtml += "<td>" + detail[0].StartWeight + " < W <= " + detail[0].EndWeight + "</td>";
                            if (billingMode == "1") {
                                tablehtml += "<td>" + detail[0].Freight + "</td>";
                                tablehtml += "<td>" + detail[0].RegistrationFee + "</td>";
                            } else {
                                tablehtml += "<td>" + detail[0].Scale + "</td>";
                                tablehtml += "<td>" + detail[0].FirstWeight + "</td>";
                                tablehtml += "<td>" + detail[0].ContinueWeight + "</td>";
                            }

                            tablehtml += "<td rowspan='" + detail.length + "'><a style='color:blue;cursor:pointer;' onclick=\"EditDetail('" + DetailItem.ItemID + "')\" >编辑</a >  <a style='color:blue;cursor:pointer;' onclick=\"DeleteDetail('" + DetailItem.ItemID + "')\" >删除</a ></td>";
                        }
                        tablehtml += '</tr>';

                        for (var i = 1; i < detail.length; i++) {
                            tablehtml += '<tr>';
                            tablehtml += "<td>" + detail[i].StartWeight + " < W <= " + detail[i].EndWeight + "</td>";
                            if (billingMode == "1") {
                                tablehtml += "<td>" + detail[i].Freight + "</td>";
                                tablehtml += "<td>" + detail[i].RegistrationFee + "</td>";
                            } else {
                                tablehtml += "<td>" + detail[i].Scale + "</td>";
                                tablehtml += "<td>" + detail[i].FirstWeight + "</td>";
                                tablehtml += "<td>" + detail[i].ContinueWeight + "</td>";
                            }

                            tablehtml += '</tr>';
                        }

                    });

                    tablehtml += '</table>';
                    $("#detailtable").html(tablehtml);
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;

        if (isName) {
            Changjie.alert.error("规则名称已存在，请更换");
            return false;
        }

        var formdata = $('[data-table="MeioErpFreightQuotation"]').mkGetFormData();
        var dateStart = new Date(formdata.EffectiveStartTime.split(' ')[0]);
        var dateEnd = new Date(formdata.EffectiveEndTime.split(' ')[0]);

        if (dateStart >= dateEnd) {
            Changjie.alert.warning("有效结束时间必须大于有效开始时间");
            return false;
        }

        if (type == "add") {
            keyValue = top.Changjie.newGuid();
        }

        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    //var deleteList = [];
    //var isgridpass = true;
    //var errorInfos = [];
    //for (var item in subGrid) {
    //    deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
    //    var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
    //    if (!info.isPass) {
    //        isgridpass = false;
    //        errorInfos.push(info.errorCells);
    //    }
    //}
    //if (!isgridpass) {
    //    for (var i in errorInfos[0]) {
    //        top.Changjie.alert.error(errorInfos[0][i].Msg);
    //    }
    //    return false;
    //}
    //if (type == "add") {
    //    keyValue = mainId;
    //}
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="MeioErpFreightQuotation"]').mkGetFormData());
    postData.strmeioErpFreightQuotationDetailList = JSON.stringify(freightQuotationDetail);
    //postData.strmeioErpFreightQuotationDetailList = JSON.stringify($('#MeioErpFreightQuotationDetail').jfGridGet('rowdatas'));
    //postData.deleteList = JSON.stringify(deleteList);
    return postData;
}

var DeleteDetail = function (id) {
    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
        if (res) {
            billingMode = $("#BillingMode").mkselectGet();
            freightQuotationDetailItem = $.grep(freightQuotationDetailItem, function (item) {
                return item.ItemID !== id;
            });

            freightQuotationDetail = $.grep(freightQuotationDetail, function (item) {
                return item.ItemID !== id;
            });

            var tablehtml = '<table border="1">';
            if (billingMode == "1") {
                tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>运费（RMB/KG）</th><th>挂号费RMB/件</th><th>操作</th></tr>";
            } else {
                tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>进位制（KG）</th><th>首重（RMB）</th><th>续重（RMB）</th><th>操作</th></tr>";
            }

            $.each(freightQuotationDetailItem, function (id, DetailItem) {
                var detail = $.grep(freightQuotationDetail, function (item) {
                    return item.ItemID === DetailItem.ItemID;
                });

                tablehtml += "<tr><td rowspan='" + detail.length + "'>" + DetailItem.Country + (DetailItem.Area.length > 0 ? "-" + DetailItem.Area : "") + "</td><td rowspan='" + detail.length + "'>" + DetailItem.ReferenceAging + "</td><td rowspan='" + detail.length + "'>" + DetailItem.MinWeight + "</td>";
                if (detail.length > 0) {
                    tablehtml += "<td>" + detail[0].StartWeight + " < W <= " + detail[0].EndWeight + "</td>";
                    if (billingMode == "1") {
                        tablehtml += "<td>" + detail[0].Freight + "</td>";
                        tablehtml += "<td>" + detail[0].RegistrationFee + "</td>";
                    } else {
                        tablehtml += "<td>" + detail[0].Scale + "</td>";
                        tablehtml += "<td>" + detail[0].FirstWeight + "</td>";
                        tablehtml += "<td>" + detail[0].ContinueWeight + "</td>";
                    }

                    tablehtml += "<td rowspan='" + detail.length + "'><a style='color:blue;cursor:pointer;' onclick=\"EditDetail('" + DetailItem.ItemID + "')\" >编辑</a >  <a style='color:blue;cursor:pointer;' onclick=\"DeleteDetail('" + DetailItem.ItemID + "')\" >删除</a ></td>";
                }
                tablehtml += '</tr>';

                for (var i = 1; i < detail.length; i++) {
                    tablehtml += '<tr>';
                    tablehtml += "<td>" + detail[i].StartWeight + " < W <= " + detail[i].EndWeight + "</td>";
                    if (billingMode == "1") {
                        tablehtml += "<td>" + detail[i].Freight + "</td>";
                        tablehtml += "<td>" + detail[i].RegistrationFee + "</td>";
                    } else {
                        tablehtml += "<td>" + detail[i].Scale + "</td>";
                        tablehtml += "<td>" + detail[i].FirstWeight + "</td>";
                        tablehtml += "<td>" + detail[i].ContinueWeight + "</td>";
                    }

                    tablehtml += '</tr>';
                }

            });

            tablehtml += '</table>';
            $("#detailtable").html(tablehtml);
        }
    });
};

var EditDetail = function (itemid) {
    billingMode = $("#BillingMode").mkselectGet();

    var freightQuotationDetailData = $.grep(freightQuotationDetail, function (item) {
        return item.ItemID === itemid;
    });
   
    console.log("freightQuotationDetailData:" + JSON.stringify(freightQuotationDetailData));
    top.Changjie.layerForm({
        id: 'form11',
        title: '编辑计费规则',
        url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/DetailForm?keyValue=' + keyValue + '&billingMode=' + billingMode + '&freightQuotationDetailData=' + encodeURIComponent(JSON.stringify(freightQuotationDetailData)),
        width: 800,
        height: 600,
        callBack: function (id) {
            //return top[id].acceptClick(refreshGirdData);
            return top[id].acceptClick(function (data, datadetail) {
                if (!!data) {
                    freightQuotationDetailItem = $.grep(freightQuotationDetailItem, function (item) {
                        return item.ItemID !== itemid;
                    });

                    freightQuotationDetail = $.grep(freightQuotationDetail, function (item) {
                        return item.ItemID !== itemid;
                    });

                    console.log("form:" + JSON.stringify(datadetail))

                    var res = data.Country.split(",");

                    $.each(res, function (eid, Country) {
                        //var itemID = top.Changjie.newGuid();
                        var itemID = itemid;

                        var detailItem = {};
                        detailItem.Country = Country;
                        detailItem.Area = data.Area;
                        detailItem.ReferenceAging = data.ReferenceAging;
                        detailItem.MinWeight = data.MinWeight;
                        detailItem.ItemID = itemID;
                        freightQuotationDetailItem.push(detailItem);

                        for (var i = 0; i < datadetail.length; i++) {
                            var itemDetail = {};
                            itemDetail.FreightQuotationID = keyValue;
                            itemDetail.Country = Country;
                            itemDetail.Area = data.Area;
                            itemDetail.ReferenceAging = data.ReferenceAging;
                            itemDetail.MinWeight = data.MinWeight;
                            itemDetail.StartWeight = datadetail[i].StartWeight;
                            itemDetail.EndWeight = datadetail[i].EndWeight;
                            itemDetail.Freight = datadetail[i].Freight;
                            itemDetail.RegistrationFee = datadetail[i].RegistrationFee;
                            itemDetail.Scale = datadetail[i].Scale;
                            itemDetail.FirstWeight = datadetail[i].FirstWeight;
                            itemDetail.ContinueWeight = datadetail[i].ContinueWeight;
                            itemDetail.ItemID = itemID;
                            itemDetail.ID = datadetail[i].ID;
                            freightQuotationDetail.push(itemDetail);
                        }
                    });

                    freightQuotationDetailItem.sort(function (a, b) {
                        return a.ItemID.localeCompare(b.ItemID);
                    });

                    freightQuotationDetail.sort(function (a, b) {
                        return a.ItemID.localeCompare(b.ItemID);
                    });

                    var tablehtml = '<table border="1">';
                    if (billingMode == "1") {
                        tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>运费（RMB/KG）</th><th>挂号费RMB/件</th><th>操作</th></tr>";
                    } else {
                        tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>进位制（KG）</th><th>首重（RMB）</th><th>续重（RMB）</th><th>操作</th></tr>";
                    }

                    $.each(freightQuotationDetailItem, function (eid, DetailItem) {
                        var detail = $.grep(freightQuotationDetail, function (item) {
                            return item.ItemID === DetailItem.ItemID;
                        });

                        tablehtml += "<tr><td rowspan='" + detail.length + "'>" + DetailItem.Country + (DetailItem.Area.length > 0 ? "-" + DetailItem.Area : "") + "</td><td rowspan='" + detail.length + "'>" + DetailItem.ReferenceAging + "</td><td rowspan='" + detail.length + "'>" + DetailItem.MinWeight + "</td>";
                        if (detail.length > 0) {
                            tablehtml += "<td>" + detail[0].StartWeight + " < W <= " + detail[0].EndWeight + "</td>";
                            if (billingMode == "1") {
                                tablehtml += "<td>" + detail[0].Freight + "</td>";
                                tablehtml += "<td>" + detail[0].RegistrationFee + "</td>";
                            } else {
                                tablehtml += "<td>" + detail[0].Scale + "</td>";
                                tablehtml += "<td>" + detail[0].FirstWeight + "</td>";
                                tablehtml += "<td>" + detail[0].ContinueWeight + "</td>";
                            }

                            tablehtml += "<td rowspan='" + detail.length + "'><a style='color:blue;cursor:pointer;' onclick=\"EditDetail('" + DetailItem.ItemID + "')\" >编辑</a >  <a style='color:blue;cursor:pointer;' onclick=\"DeleteDetail('" + DetailItem.ItemID + "')\" >删除</a ></td>";
                        }
                        tablehtml += '</tr>';

                        for (var i = 1; i < detail.length; i++) {
                            tablehtml += '<tr>';
                            tablehtml += "<td>" + detail[i].StartWeight + " < W <= " + detail[i].EndWeight + "</td>";
                            if (billingMode == "1") {
                                tablehtml += "<td>" + detail[i].Freight + "</td>";
                                tablehtml += "<td>" + detail[i].RegistrationFee + "</td>";
                            } else {
                                tablehtml += "<td>" + detail[i].Scale + "</td>";
                                tablehtml += "<td>" + detail[i].FirstWeight + "</td>";
                                tablehtml += "<td>" + detail[i].ContinueWeight + "</td>";
                            }

                            tablehtml += '</tr>';
                        }

                    });

                    tablehtml += '</table>';
                    $("#detailtable").html(tablehtml)
                }
            });
        },
        end: function () {
        }

    });
};

var displayExcelData = function (excelData) {
    //billingMode = $("input[name='BillingMode']:checked").val();
    billingMode = $("#BillingMode").mkselectGet();
    var itemID = "";
    var country = "";
    var area = "";
    var minweight = "0";
    var referenceAging = "";
    $.each(excelData, function (id, dataItem) {
        if ((dataItem["国家/地区"] != null && dataItem["国家/地区"] != "") || (dataItem["分区"] != null && dataItem["分区"] != "")) {
            var detailItemQuery = [];
            if (dataItem["分区"] != null && dataItem["分区"] != "") {
                detailItemQuery = $.grep(freightQuotationDetailItem, function (item) {
                    return item.Country.replace(" ", "") + item.Area.replace(" ", "") == dataItem["国家/地区"].replace(" ", "") + dataItem["分区"].replace(" ", "");
                });
            } else {
                detailItemQuery = $.grep(freightQuotationDetailItem, function (item) {
                    return item.Country.replace(" ", "") == dataItem["国家/地区"].replace(" ", "");
                });
            }
            if (detailItemQuery.length == 0) {
                itemID = top.Changjie.newGuid();

                var detailItem = {};
                if (dataItem["国家/地区"] != null && dataItem["国家/地区"] != "") {
                    detailItem.Country = dataItem["国家/地区"];
                    country = dataItem["国家/地区"];
                } else {
                    detailItem.Country = country;
                }
                if (dataItem["分区"] != null && dataItem["分区"] != "") {
                    detailItem.Area = dataItem["分区"];
                    area = dataItem["分区"];
                } else {
                    detailItem.Area = area;
                }
                if (dataItem["最低计费重(KG)"] != null && dataItem["最低计费重(KG)"] != "") {
                    detailItem.MinWeight = dataItem["最低计费重(KG)"];
                    minweight = dataItem["最低计费重(KG)"];
                } else {
                    detailItem.MinWeight = minweight;
                }
                if (dataItem["参考时效"] != null && dataItem["参考时效"] != "") {
                    detailItem.ReferenceAging = dataItem["参考时效"];
                    referenceAging = dataItem["参考时效"];
                } else {
                    detailItem.ReferenceAging = referenceAging;
                }
                detailItem.ItemID = itemID;
                freightQuotationDetailItem.push(detailItem);
            }
        }

        var itemDetail = {};
        itemDetail.FreightQuotationID = top.Changjie.newGuid();
        itemDetail.Country = country;
        itemDetail.Area = area;
        itemDetail.MinWeight = minweight;
        if (dataItem["重量(KG)"] != null && dataItem["重量(KG)"] != "") {
            var weightstr = dataItem["重量(KG)"].replace("＜W≤", ",").split(",");

            itemDetail.StartWeight = weightstr[0];
            itemDetail.EndWeight = weightstr[1];
        }
        if (dataItem["运费(RMB/KG)"] != null && dataItem["运费(RMB/KG)"] != "") {
            itemDetail.Freight = dataItem["运费(RMB/KG)"];
        }
        if (dataItem["挂号费(RMB/票)"] != null && dataItem["挂号费(RMB/票)"] != "") {
            itemDetail.RegistrationFee = dataItem["挂号费(RMB/票)"];
        }
        if (dataItem["进位制(KG)"] != null && dataItem["进位制(KG)"] != "") {
            itemDetail.Scale = dataItem["进位制(KG)"];
        }
        if (dataItem["首重"] != null && dataItem["首重"] != "") {
            itemDetail.FirstWeight = dataItem["首重"];
        }
        if (dataItem["续重"] != null && dataItem["续重"] != "") {
            itemDetail.ContinueWeight = dataItem["续重"];
        }
        itemDetail.ItemID = itemID;
        itemDetail.ID = top.Changjie.newGuid();
        freightQuotationDetail.push(itemDetail);

    });


    var tablehtml = '<table border="1">';
    if (billingMode == "1") {
        tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>运费（RMB/KG）</th><th>挂号费RMB/件</th><th>操作</th></tr>";
    } else {
        tablehtml += "<tr><th>国家/地区</th><th>参考时效</th><th>最低起重（KG）</th><th>重量段（KG）</th><th>进位制（KG）</th><th>首重（RMB）</th><th>续重（RMB）</th><th>操作</th></tr>";
    }

    $.each(freightQuotationDetailItem, function (id, DetailItem) {
        var detail = $.grep(freightQuotationDetail, function (item) {
            return item.ItemID === DetailItem.ItemID;
        });

        tablehtml += "<tr><td rowspan='" + detail.length + "'>" + DetailItem.Country + (DetailItem.Area.length > 0 ? "-" + DetailItem.Area : "") + "</td><td rowspan='" + detail.length + "'>" + DetailItem.ReferenceAging + "</td><td rowspan='" + detail.length + "'>" + DetailItem.MinWeight + "</td>";
        if (detail.length > 0) {
            tablehtml += "<td>" + detail[0].StartWeight + " < W <= " + detail[0].EndWeight + "</td>";
            if (billingMode == "1") {
                tablehtml += "<td>" + detail[0].Freight + "</td>";
                tablehtml += "<td>" + detail[0].RegistrationFee + "</td>";
            } else {
                tablehtml += "<td>" + detail[0].Scale + "</td>";
                tablehtml += "<td>" + detail[0].FirstWeight + "</td>";
                tablehtml += "<td>" + detail[0].ContinueWeight + "</td>";
            }

            tablehtml += "<td rowspan='" + detail.length + "'><a style='color:blue;cursor:pointer;' onclick=\"EditDetail('" + DetailItem.ItemID + "')\" >编辑</a >  <a style='color:blue;cursor:pointer;' onclick=\"DeleteDetail('" + DetailItem.ItemID + "')\" >删除</a ></td>";
        }
        tablehtml += '</tr>';

        for (var i = 1; i < detail.length; i++) {
            tablehtml += '<tr>';
            tablehtml += "<td>" + detail[i].StartWeight + " < W <= " + detail[i].EndWeight + "</td>";
            if (billingMode == "1") {
                tablehtml += "<td>" + detail[i].Freight + "</td>";
                tablehtml += "<td>" + detail[i].RegistrationFee + "</td>";
            } else {
                tablehtml += "<td>" + detail[i].Scale + "</td>";
                tablehtml += "<td>" + detail[i].FirstWeight + "</td>";
                tablehtml += "<td>" + detail[i].ContinueWeight + "</td>";
            }

            tablehtml += '</tr>';
        }

    });

    tablehtml += '</table>';
    $("#detailtable").html(tablehtml)
};
