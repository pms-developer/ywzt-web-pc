﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2025-02-08 16:38
 * 描  述：运费报价
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var country;
    var enabled;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#Country').mkDataItemSelect({ code: 'Country' });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);

            $('#Enabled').mkselect({
                data: [{ value: 1, text: '启用' }, { value: 0, text: '禁用' }],
                value: 'value',
                text: 'text',
                title: 'text',
                select: function (item) {
                    if (item) {
                        enabled = item.value;
                    }
                }
            });
            // 查询
            $('#query').on('click', function () {
                //page.search({
                //    "Country": $("#Country").mkselectGet(),
                //    "RuleName": $("#RuleName").val(),
                //    "Enabled": $("#Enabled").mkselectGet()
                //});
                page.search();
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/Form',
                    width: 1200,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            $('#copy').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要复制的运费报价单");
                    return false;
                }
                if (selectedRow.length > 1) {
                    top.Changjie.alert.error("只能选择一个运费报价单进行复制");
                    return false;
                }
                var keyValue = selectedRow[0].ID;

                top.Changjie.layerForm({
                    id: 'form45',
                    title: '复制',
                    //isShowConfirmBtn: true,
                    url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/Form?keyValue=' + keyValue + '&iscopy=1',
                    width: 1200,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

            //  启用
            $('#btn_enable').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要启用的运费报价单");
                    return false;
                }
                var ids = "";
                selectedRow.forEach(function (row) {
                    ids += "," + row.ID;
                });

                top.Changjie.layerConfirm('是否确认启用选中运费报价单！', function (res) {
                    if (res) {
                        top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/SetIsEnable', { ids: ids.substring(1), isEnable: true }, function () {
                            refreshGirdData();
                        });
                    }
                });

            });
            //  禁用
            $('#btn_disable').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要禁用的运费报价单");
                    return false;
                }
                var ids = "";
                selectedRow.forEach(function (row) {
                    ids += "," + row.ID;
                });

                top.Changjie.layerConfirm('是否确认禁用选中运费报价单！', function (res) {
                    if (res) {
                        top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/SetIsEnable', { ids: ids.substring(1), isEnable: false }, function () {
                            refreshGirdData();
                        });
                    }
                });

            });


            $('#editEffectiveTime').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要修改起止日期的运费报价单");
                    return false;
                }
                var ids = "";
                selectedRow.forEach(function (row) {
                    ids += "," + row.ID;
                });
                Changjie.layerForm({
                    id: 'form233',
                    title: '修改起止日期',
                    url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/Dialog?keyValue=' + ids.substring(1),
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

            $('#export').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0 || selectedRow.length>1) {
                    top.Changjie.alert.error("请选择需要导出的报价单,并且只能选择一个报价单导出");
                    return false;
                }
                var id = "";
                var BillingMode = 1;
                selectedRow.forEach(function (row) {
                    id = row.ID;
                    BillingMode = row.BillingMode;
                    $("#excelExportName").val(row.RuleName);
                    console.log($("#excelExportName").val());
                });

                if (BillingMode == 1) {
                    $("#export1").click();
                } else {
                    $("#export2").click();
                }

            });

            $('#log').on('click', function () {
                Changjie.layerForm({
                    id: 'formLog',
                    title: '操作日志',
                    isShowConfirmBtn: false,
                    url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/LogForm',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            $('#Country').change(function () {
                 country = $('#Country').mkselectGet();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/GetPageList',
                headData: [
                    {
                        label: "规则名称", name: "RuleName", width: 200, align: "left",
                        formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"ShowDetail('" + row['ID'] + "')\" >" + row.RuleName + "</a > "
                        }
                    },
                    {
                        label: "启用状态", name: "Enabled", width: 100, align: "left", formatter: function (value, row) {
                            if (value)
                                return "启用";
                            else
                                return "禁用";
                        }
                    },
                    { label: "有效开始时间", name: "EffectiveStartTime", width: 100, align: "left" },
                    { label: "有效结束时间", name: "EffectiveEndTime", width: 100, align: "left" },
                    {
                        label: "承运商", name: "GeneralCarrier", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'GeneralCarrierList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    {
                        label: "物流渠道", name: "LogisticsChannel", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'LogisticsChannelList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    {
                        label: "计费方式", name: "BillingMode", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FreightQuotationBillingMode',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "国家", name: "Country", width: 200, align: "left" },
                    { label: "最后修改人", name: "ModificationName", width: 100, align: "left" },
                    { label: "最后修改时间", name: "ModificationDate", width: 150, align: "left" },
                ],
                mainId: 'ID',
                isPage: true,
                isMultiselect: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            param.FristSearchPage = 0;
            param.RuleName = $("#RuleName").val();
            param.Country = country;
            param.Enabled = enabled;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var selectedRow = $('#gridtable').jfGridGet('rowdata');
    if (selectedRow.length == 0) {
        top.Changjie.alert.error("请选择需要删除的运费报价单");
        return false;
    }
    var ids = "";
    selectedRow.forEach(function (row) {
        ids += "," + row.ID;
    });

    if (top.Changjie.checkrow(ids)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/DeleteForm', { keyValue: ids.substring(1) }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var selectedRow = $('#gridtable').jfGridGet('rowdata');
    if (selectedRow.length == 0) {
        top.Changjie.alert.error("请选择需要编辑的运费报价单");
        return false;
    }
    if (selectedRow.length > 1) {
        top.Changjie.alert.error("只能选择一个运费报价单进行编辑");
        return false;
    }
    var keyValue = selectedRow[0].ID;

    top.Changjie.layerForm({
        id: 'form',
        title: title,
        isShowConfirmBtn: isShowConfirmBtn,
        url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/Form?keyValue=' + keyValue + '&viewState=' + viewState,
        width: 1200,
        height: 800,
        callBack: function (id) {
            return top[id].acceptClick(refreshGirdData);
        }
    });

};

var ShowDetail = function (id) {
    top.Changjie.layerForm({
        id: 'form',
        title: "运费报价单明细",
        isShowConfirmBtn: true,
        url: top.$.rootUrl + '/BaseInfo/MeioErpFreightQuotation/Form?keyValue=' + id,
        width: 1200,
        height: 800,
        callBack: function (id) {
            return top[id].acceptClick(refreshGirdData);
        }
    });
}
