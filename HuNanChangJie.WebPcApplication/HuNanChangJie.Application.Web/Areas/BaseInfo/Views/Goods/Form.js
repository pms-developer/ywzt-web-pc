﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-06-27 19:22
 * 描  述：货品信息
 */
jQuery.extend({
    createUploadIframe: function (id, mytype) {
        var ajid = "jUploadFrame" + id;
        var c = '<iframe id="' + ajid + '" name="' + ajid + '" style="position:absolute; top:-9999px; left:-9999px"';
        if (window.ActiveXObject) {
            if (typeof mytype == "boolean") {
                c += ' src="javascript:false"';
            } else {
                if (typeof mytype == "string") {
                    c += ' src="' + mytype + '"';
                }
            }
        } c += " />";
        jQuery(c).appendTo(document.body);
        return jQuery("#" + ajid).get(0);
    },
    createUploadForm: function (gid, eid, inputs) {
        var ejformid = "jUploadForm" + gid;
        var cjfileid = "jUploadFile" + gid;
        var dpostElement = jQuery('<form  action="" method="POST" name="' + ejformid + '" id="' + ejformid + '" enctype="multipart/form-data"></form>');
        if (inputs) {
            for (var f in inputs) {
                jQuery('<input type="hidden" name="' + f + '" value="' + inputs[f].replace(/"([^"]*)"/g, "'$1'") + '" />').appendTo(dpostElement);
            }
        } var j = jQuery("#" + eid);
        var h = jQuery(j).clone();
        jQuery(j).attr("id", cjfileid);
        jQuery(j).before(h);
        jQuery(j).appendTo(dpostElement);
        jQuery(dpostElement).css("position", "absolute");
        jQuery(dpostElement).css("top", "-1200px");
        jQuery(dpostElement).css("left", "-1200px");
        jQuery(dpostElement).appendTo("body");
        return dpostElement;
    },
    ajaxFileUpload: function (i) {
        i = jQuery.extend({}, jQuery.ajaxSettings, i);
        var f = new Date().getTime();
        var b = jQuery.createUploadForm(f, i.fileElementId, (typeof (i.data) == "undefined" ? false : i.data));
        var g = jQuery.createUploadIframe(f, i.secureuri);
        var d = "jUploadFrame" + f;
        var c = "jUploadForm" + f;
        if (i.global && !jQuery.active++) {
            jQuery.event.trigger("ajaxStart");
        } var h = false;
        var k = {};
        if (i.global) {
            jQuery.event.trigger("ajaxSend", [k, i]);
        } var j = function (o) {
            var n = document.getElementById(d);
            try {
                if (n.contentWindow) {
                    k.responseText = n.contentWindow.document.body ? n.contentWindow.document.body.innerHTML : null;
                    k.responseXML = n.contentWindow.document.XMLDocument ? n.contentWindow.document.XMLDocument : n.contentWindow.document
                } else {
                    if (n.contentDocument) {
                        k.responseText = n.contentDocument.document.body ? n.contentDocument.document.body.innerHTML : null;
                        k.responseXML = n.contentDocument.document.XMLDocument ? n.contentDocument.document.XMLDocument : n.contentDocument.document
                    }
                }
            } catch (m) {
                jQuery.handleError(i, k, null, m);
            } if (k || o == "timeout") {
                h = true;
                var p;
                try {
                    p = o != "timeout" ? "success" : "error";
                    if (p != "error") {
                        var l = jQuery.uploadHttpData(k, i.dataType);
                        if (i.success) {
                            i.success(l, p);
                        } if (i.global) {
                            jQuery.event.trigger("ajaxSuccess", [k, i]);
                        }
                    } else {
                        jQuery.handleError(i, k, p);
                    }
                } catch (m) {
                    p = "error";
                    jQuery.handleError(i, k, p, m);
                } if (i.global) {
                    jQuery.event.trigger("ajaxComplete", [k, i]);
                } if (i.global && !--jQuery.active) {
                    jQuery.event.trigger("ajaxStop");
                } if (i.complete) {
                    i.complete(k, p);
                } jQuery(n).unbind();
                setTimeout(function () {
                    try {
                        jQuery(n).remove();
                        jQuery(b).remove();
                    } catch (q) {
                        jQuery.handleError(i, k, null, q);
                    }
                }, 100);
                k = null;
            }
        };
        if (i.timeout > 0) {
            setTimeout(function () {
                if (!h) {
                    j("timeout");
                }
            },
                i.timeout);
        } try {
            var b = jQuery("#" + c);
            jQuery(b).attr("action", i.url);
            jQuery(b).attr("method", "POST");
            jQuery(b).attr("target", d);
            if (b.encoding) {
                jQuery(b).attr("encoding", "multipart/form-data");
            } else {
                jQuery(b).attr("enctype", "multipart/form-data");
            }
            jQuery(b).submit();
        } catch (a) {
            jQuery.handleError(i, k, null, a);
        } jQuery("#" + d).load(j);
        return { abort: function () { } }
    }, uploadHttpData: function (r, type) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        if (type == "script") {
            jQuery.globalEval(data);
        } if (type == "json") {
            eval("data = " + data);
        } if (type == "html") {
            jQuery("<div>").html(data).evalScripts();
        }
        return data;
    }
});
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpGoods";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/Goods/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var isCode = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            function d(g, h) {
                var e = document.getElementById(g).files[0];
                var i = window.URL.createObjectURL(e);
                document.getElementById(h).src = i;
            }

            $("#uploadFile").on("change",
                function () {
                    d("uploadFile", "uploadPreview");
                });
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $('#OwnerID').mkDataSourceSelect({ code: 'OwnerList', value: 'id', text: 'title' });
            $('#Type').mkDataItemSelect({ code: 'GoodsType' });
            $('#SupplierID').mkDataSourceSelect({ code: 'Meio_Supplier', value: 'id', text: 'title' });
            $("#Code").on("blur", function () {
                var value = $(this).val();
                isCode = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Goods/CheckIsGoodsCode?code=" + value + "&id=" + keyValue).data;
                if (isCode) {
                    Changjie.alert.error("货品编码已存在，请更换");
                }
            });
            $("#Length").on("input", function () {
                CalculatedVolume();
            });
            $("#Width").on("input", function () {
                CalculatedVolume();
            });
            $("#Height").on("input", function () {
                CalculatedVolume();
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/Goods/GetformInfoList?keyValue=' + keyValue, function (data) {
                    $("#file").prepend('<img id="uploadPreview" style="width:100%;height:100%" src="' + data["MeioErpGoods"].ImageUrl + '">');
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                    if (data["MeioErpGoods"].IsNominatedSupplier) {
                        $('#IsNominatedSupplierNum').prop('checked', true);
                    }
                    else {
                        $('#IsNominatedSupplierNum').prop('checked', false);
                    }
                });
            } else {
                $("#file").prepend('<img id="uploadPreview" style="width:100%;height:100%" src=' + top.$.rootUrl +'"/Content/images/add.jpg">');
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;

        if (isCode) {
            Changjie.alert.error("货品编码已存在，请更换");
        }

        if ($('#IsNominatedSupplierNum').is(':checked')) {
            postData.IsNominatedSupplier = true;
        } else {
            postData.IsNominatedSupplier = false;
        }

        var e = document.getElementById("uploadFile").files[0];
        if (!!e) {
            //$.ajaxFileUpload({
            //    url: top.$.rootUrl + "/BaseInfo/Goods/UploadFile?code=qrcode",
            //    secureuri: false,
            //    fileElementId: "uploadFile",
            //    dataType: "json",
            //    success: function (f) {
            //        if (f.code == 200) {
            //            postData.ImageUrl = f.info;
            //            console.log(postData.ImageUrl);
            //            $.mkSaveForm(top.$.rootUrl + '/BaseInfo/Goods/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            //                // 保存成功后才回调
            //                if (!!callBack) {
            //                    callBack();
            //                }
            //            });
            //        }
            //    }
            //});

            const form = new FormData();
            form.append("file", e)
            Changjie.httpPostFile(top.$.rootUrl + '/BaseInfo/Goods/UpLoadFile?code=qrcode', form, function (data) {
                Changjie.loading(false);
                let obj = JSON.parse(data)
                if (obj.code == 200) {
                    postData.ImageUrl = obj.info;
                    $.mkSaveForm(top.$.rootUrl + '/BaseInfo/Goods/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                        // 保存成功后才回调
                        if (!!callBack) {
                            callBack();
                        }
                    });
                } else {
                    top.Changjie.alert.error(obj.info);
                }
            });
        } else {
            $.mkSaveForm(top.$.rootUrl + '/BaseInfo/Goods/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                // 保存成功后才回调
                if (!!callBack) {
                    callBack();
                }
            });
        }


    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}

var CalculatedVolume = function () {
    var length = Number($("#Length").val());
    var width = Number($("#Width").val());
    var height = Number($("#Height").val());
    var volume = length * width * height;
    $("#Volume").val(volume);
}
