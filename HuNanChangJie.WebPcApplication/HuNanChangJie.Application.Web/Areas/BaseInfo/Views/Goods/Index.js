﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-06-27 19:22
 * 描  述：货品信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#OwnerID').mkDataSourceSelect({ code: 'Meio_Customer', value: 'id', text: 'title' });
            $('#Enabled').mkselect({ data: [{ value:true , text: "启用" }, { value: false, text: "禁用" }], value: 'value', text: 'text', title: 'text' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "Code": $("#Code").val(),
                    "Name": $("#Name").val(),
                    "OwnerID": $("#OwnerID").mkselectGet(),
                    "Enabled": $("#Enabled").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/Goods/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            ////  导入
            //$('#btn_Import').on('click', function () {
            //});
            ////  导出
            //$('#btn_Export').on('click', function () {
            //});

            //  启用
            $('#btn_enable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认启用选中货品！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Goods/SetIsEnable', { ids: keyValue, isEnable: true }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            //  禁用
            $('#btn_disable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认禁用选中货品！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Goods/SetIsEnable', { ids: keyValue, isEnable: false }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/Goods/GetPageList',
                headData: [
                    {
                        label: "图片", name: "ImageUrl", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            if (value != "" && value != null)
                                callback('<img style="width:100%;height:100%" src="' + value + '">')
                            else
                                callback('')
                        }
                    },
                    { label: "货品编码", name: "Code", width: 200, align: "left" },
                    {
                        label: "货主", name: "OwnerID", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['title']);
                                }
                            });
                        }
                    },
                    { label: "名称", name: "Name", width: 300, align: "left" },
                    {
                        label: "货品分类", name: "Type", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'GoodsType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "启用状态", name: "Enabled", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (value) {
                                return "启用";
                            } else {
                                return "禁用";
                            }
                        }
                    },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "货品信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/Goods/Form?keyValue=' + row.ID,
                        width: 800,
                        height: 600
                    });
                },
                mainId: 'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Goods/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/BaseInfo/Goods/Form?keyValue=' + keyValue + '&viewState=' + viewState,
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
