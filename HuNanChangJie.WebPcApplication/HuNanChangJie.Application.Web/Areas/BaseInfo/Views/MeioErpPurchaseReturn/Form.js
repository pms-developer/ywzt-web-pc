﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-09-05 15:34
 * 描  述：采购退货
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="MeioErpPurchaseReturn";
var processCommitUrl=top.$.rootUrl + '/BaseInfo/MeioErpPurchaseReturn/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'MeioErpPurchaseReturnDetail',"gridId":'MeioErpPurchaseReturnDetail'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList',value: 'id',text: 'name' });
            $('#SupplierID').mkDataSourceSelect({ code: 'Meio_Supplier',value: 'id',text: 'title' });
            $('#MeioErpPurchaseReturnDetail').jfGrid({
                headData: [
                    {
                        label: '图片', name: 'GoodsImageUrl', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '货品编码', name: 'GoodsCode', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '货品名称', name: 'GoodsName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '采购数量', name: 'PurchaseQuantity', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:''                     },
                    {
                        label: '退货数量', name: 'ReturnQuantity', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'int', tabname:'' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                ],
                mainId:"ID",
                bindTable:"MeioErpPurchaseReturnDetail",
                isEdit: true,
                height: 300,
                rowdatas:[
                {
                     "SortCode": 1,
                     "rowState": 1,
                     "ID": Changjie.newGuid(),
                     "EditType": 1,
                }],
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                 }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpPurchaseReturn/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpPurchaseReturn/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="MeioErpPurchaseReturn"]').mkGetFormData());
        postData.strmeioErpPurchaseReturnDetailList = JSON.stringify($('#MeioErpPurchaseReturnDetail').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
