﻿var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            //$('#Purchase').mkDataSourceSelect({ code: 'AwaitDeliveryPurchaseList', value: 'id', text: 'code' });

            //Changjie.httpGet(top.$.rootUrl + '/BaseInfo/MeioErpPurchase/GetAwaitDeliveryPurchaseList', function (data) {

            //});
            $('#Purchase').mkselect({
                allowSearch: true,
                title: 'Code',
                text: 'Code',
                value: 'ID',
                url: top.$.rootUrl + '/BaseInfo/MeioErpPurchase/GetAwaitDeliveryPurchaseList',
                showtop: true
            });
        }
    };
    acceptClick = function (callBack) {
        if ($('#Purchase').mkselectGet() == "") {
            top.Changjie.alert.error("请选择采购单");
            return false;
        }
        Changjie.layerForm({
            id: 'form',
            title: '新增',
            url: top.$.rootUrl + '/BaseInfo/MeioErpDelivery/Form?purchaseId=' + $('#Purchase').mkselectGet(),
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(callBack, window.name);
            }
        });
        
    };
    page.init();
};