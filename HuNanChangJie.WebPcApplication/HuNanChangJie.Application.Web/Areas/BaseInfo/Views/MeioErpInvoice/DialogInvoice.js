﻿
var keyValue = request('keyValue');
var id=request('id');

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.bind();
            //page.initData();
        },
        bind: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpInvoice/GetPageList',
                headData: [
                    { label: "结算单号", name: "StatementNo", width: 150, align: "left"},
                    { label: "账单金额", name: "AccountAmount", width: 100, align: "left" },
                    { label: "付款类型", name: "PayType", width: 100, align: "left" },
                    { label: "付款币种", name: "PayCurrency", width: 100, align: "left" },
                    { label: "发票抬头", name: "InvoiceTitle", width: 100, align: "left" },
                    { label: "税务登记号", name: "TaxRegistrationNumber", width: 100, align: "left" },
                    { label: "开户银行", name: "BankName", width: 100, align: "left" },
                    { label: "银行账号", name: "BankAccount", width: 100, align: "left" },
                    { label: "申请开票类型", name: "ApplyInvoiceType", width: 100, align: "left" },
                    { label: "申请开票税率(%)", name: "ApplyTaxRate", width: 100, align: "left" },
                    { label: "申请开票税额", name: "ApplyTaxAmount", width: 100, align: "left" },
                    { label: "申请合计开票金额", name: "ApplyTotalAmount", width: 100, align: "left" },
                    { label: "申请发票备注", name: "ApplyInvoiceRemark", width: 100, align: "left" },
                    {
                        label: "货主", name: "OwnerID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['title']);
                                }
                            });
                        }
                    },
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left" },
                    {
                        label: "收款确认", name: "IsCollectionConfirm", width: 100, align: "left",
                        formatter: function (callvalue, row) {
                            if (callvalue)
                                return "已确认"
                            else
                                return "待确认"
                        }
                    },
                    { label: "收款账户", name: "CollectionAccount", width: 100, align: "left" },
                    { label: "查账编号", name: "AuditNumber", width: 100, align: "left" },
                    {
                        label: "电子发票", name: "IsUploadElectronicInvoice", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            if (cellvalue)
                                return "已上传"
                            else
                                return "未上传"
                        }
                    },
                    {
                        label: "发票", name: "InvoiceImageUrl", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            if (value != "" && value != null)
                            {
                                if (value.indexOf(".pdf") > -1) {
                                    callback('<a href="' + value + '">下载发票</a>')
                                } else {
                                    callback('<a href="' + value + '"><img style="width:100%;height:100%" src="' + value + '"></a>')
                                }
                            }
                            else
                                callback('')
                        }
                    },
                    { label: "上传人", name: "UploadPeople", width: 100, align: "left" },
                    { label: "上传时间", name: "UploadDate", width: 100, align: "left" },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "发票信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpInvoice/Form?keyValue=' + row.ID,
                        width: 800,
                        height: 600
                    });
                },
                mainId: 'ID',
                isPage: true,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            param.StatementNo=keyValue;
            param.NotID=id;

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    page.init();
}