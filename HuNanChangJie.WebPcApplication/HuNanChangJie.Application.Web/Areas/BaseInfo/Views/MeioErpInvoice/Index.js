﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-07-25 17:29
 * 描  述：发票信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#OwnerID').mkDataSourceSelect({ code: 'OwnerList', value: 'id', text: 'title' });
            //$('#IsUploadElectronicInvoice').mkDataItemSelect({ code: 'BESF' });
            $('#IsUploadElectronicInvoice').mkselect({ data: [{ value: true, text: "已上传" }, { value: false, text: "未上传" }], value: 'value', text: 'text', title: 'text' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "StatementNo": $("#StatementNo").val(),
                    "OwnerID": $("#OwnerID").mkselectGet(),
                    "IsUploadElectronicInvoice": $("#IsUploadElectronicInvoice").mkselectGet()
                });
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
            //  上传
            $('#btn_Upload').on('click', function () {
                //var statementNo = $('#gridtable').jfGridValue('StatementNo');
                //Changjie.httpGet(top.$.rootUrl + "/BaseInfo/MeioErpStatement/GetStatementByCode?code=" + statementNo, function (data) {
                //    if (!data.IsCollectionConfirm) {
                //        top.Changjie.alert.error("结算单未确认收款不能上传发票");
                //        return false;
                //    }
                //});
                var isCollectionConfirm = $('#gridtable').jfGridValue('IsCollectionConfirm');
                if (!isCollectionConfirm) {
                    top.Changjie.alert.error("结算单未确认收款不能上传发票");
                    return false;
                }

                var keyValue = $('#gridtable').jfGridValue('ID');
                if (keyValue == null || keyValue == "") {
                    top.Changjie.alert.error("请选择需要上传发票的结算单");
                    return false;
                }

                var code = $('#gridtable').jfGridValue('StatementNo');
                if (top.Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'dialog',
                        title: '上传发票',
                        url: top.$.rootUrl + '/BaseInfo/MeioErpInvoice/Dialog?keyValue=' + keyValue + "&code=" + code,
                        width: 500,
                        height: 300,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpInvoice/GetPageList',
                headData: [
                    { label: "结算单号", name: "StatementNo", width: 150, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"openDialogInvoice('"+v+"','" + row['ID'] + "')\" >" + v + "</a>"
                        }
                    },
                    { label: "账单金额", name: "AccountAmount", width: 100, align: "left" },
                    { label: "付款类型", name: "PayType", width: 100, align: "left" },
                    { label: "付款币种", name: "PayCurrency", width: 100, align: "left" },
                    { label: "发票抬头", name: "InvoiceTitle", width: 100, align: "left" },
                    { label: "税务登记号", name: "TaxRegistrationNumber", width: 100, align: "left" },
                    { label: "开户银行", name: "BankName", width: 100, align: "left" },
                    { label: "银行账号", name: "BankAccount", width: 100, align: "left" },
                    { label: "申请开票类型", name: "ApplyInvoiceType", width: 100, align: "left" },
                    { label: "申请开票税率(%)", name: "ApplyTaxRate", width: 100, align: "left" },
                    { label: "申请开票税额", name: "ApplyTaxAmount", width: 100, align: "left" },
                    { label: "申请合计开票金额", name: "ApplyTotalAmount", width: 100, align: "left" },
                    { label: "申请发票备注", name: "ApplyInvoiceRemark", width: 100, align: "left" },
                    {
                        label: "货主", name: "OwnerID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['title']);
                                }
                            });
                        }
                    },
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left" },
                    {
                        label: "收款确认", name: "IsCollectionConfirm", width: 100, align: "left",
                        formatter: function (callvalue, row) {
                            if (callvalue)
                                return "已确认"
                            else
                                return "待确认"
                        }
                    },
                    { label: "收款账户", name: "CollectionAccount", width: 100, align: "left" },
                    { label: "查账编号", name: "AuditNumber", width: 100, align: "left" },
                    {
                        label: "电子发票", name: "IsUploadElectronicInvoice", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            if (cellvalue)
                                return "已上传"
                            else
                                return "未上传"
                        }
                    },
                    {
                        label: "发票", name: "InvoiceImageUrl", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            if (value != "" && value != null) {
                                if (value.indexOf(".pdf") > -1) {
                                    callback('<a href="' + value + '">下载发票</a>')
                                } else {
                                    callback('<a href="' + value + '"><img style="width:100%;height:100%" src="' + value + '"></a>')
                                }
                            }
                            else
                                callback('')
                        }
                    },
                    { label: "上传人", name: "UploadPeople", width: 100, align: "left" },
                    { label: "上传时间", name: "UploadDate", width: 100, align: "left" },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "发票信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpInvoice/Form?keyValue=' + row.ID,
                        width: 800,
                        height: 600
                    });
                },
                mainId: 'ID',
                isPage: true,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpInvoice/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/BaseInfo/MeioErpInvoice/Form?keyValue=' + keyValue + '&viewState=' + viewState,
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};

function openDialogInvoice(keyValue,id)
{
    top.Changjie.layerForm({
                        id: 'form11',
                        title: "结算单："+keyValue+"  开票记录信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpInvoice/DialogInvoice?keyValue=' + keyValue+'&id='+id,
                        width: 800,
                        height: 600
                    });
}
