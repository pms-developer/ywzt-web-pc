﻿/* *  
 * 创建人：超级管理员
 * 日  期：2025-03-10 18:06
 * 描  述：电商平台
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="MeioErpWarehouseCommercePlatform";
var processCommitUrl=top.$.rootUrl + '/BaseInfo/MeioErpWarehouseCommercePlatform/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            if (type!="add") {
                $("#Code").css("pointer-events", "none");
            }
            // 点击自定义按钮时触发文件选择框
            $("#UploadButton").click(function () {
                $("#ImgUpload").click(); // 触发文件选择框
            });

            // 监听文件选择框的 change 事件
            $("#ImgUpload").change(function (event) {
                const file = event.target.files[0]; // 获取用户选择的文件
                if (file) {
                    const form = new FormData();
                    form.append("file", file);
                    Changjie.httpPostFile(top.$.rootUrl + '/BaseInfo/MeioErpWarehouseCommercePlatform/UpLoadFile', form, function (data) {
                        let obj = JSON.parse(data);
                        if (obj != null && obj.info != null && obj.info != "") {
                            if (obj.info == "不支持该格式") {
                                Changjie.alert.error("不支持该格式!!");
                                return;
                            }
                            const form1 = new FormData();
                            form1.append("path", obj.info);

                            const reader = new FileReader(); // 创建 FileReader 对象

                            // 文件读取完成后的回调
                            reader.onload = function (e) {
                                $("#ImgShow").attr("src", obj.info); // 设置图片的 src
                            };

                            $("#Img").val(obj.info);
                            reader.readAsDataURL(file); 
                        }
                    });
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpWarehouseCommercePlatform/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);
                            if (data[id].Img != null && data[id].Img.length>0) {
                                $("#ImgShow").attr("src", data[id].Img);
                                $("#Img").val( data[id].Img);
                            }
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpWarehouseCommercePlatform/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
        }
        if (type == "add") {
            keyValue = mainId;
        }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
     return postData;
}
