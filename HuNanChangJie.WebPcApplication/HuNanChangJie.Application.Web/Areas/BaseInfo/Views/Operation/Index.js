﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-05-09 19:00
 * 描  述：作业档案信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#IsValid').mkselect({ data: [{ value: true, text: "启用" }, { value: false, text: "禁用" }], value: 'value', text: 'text', title: 'text' });
            $('#ExpenseType').mkDataItemSelect({ code: 'ExpenseType' });
            $('#ChargePoint').mkDataItemSelect({ code: 'OperationPoint' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "ItemName": $("#ItemName").val(),
                    "IsValid": $("#IsValid").mkselectGet(),
                    "ExpenseType": $("#ExpenseType").mkselectGet(),
                    "ChargePoint": $("#ChargePoint").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/Operation/Form',
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

            $('#copy').on('click', function () {
                //var selectedRow = $('#gridtable').jfGridGet('rowdata');
                //if (selectedRow.length == 0) {
                //    top.Changjie.alert.error("请选择需要复制的计费项");
                //    return false;
                //}
                //if (selectedRow.length > 1) {
                //    top.Changjie.alert.error("只能选择一个计费项进行复制");
                //    return false;
                //}
                //var keyValue = selectedRow[0].ID;

                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'formcopy',
                        title: '复制',
                        //isShowConfirmBtn: true,
                        url: top.$.rootUrl + '/BaseInfo/Operation/Form?keyValue=' + keyValue + '&iscopy=1',
                        width: 1000,
                        height: 800,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });

            //  启用
            $('#btn_enable').on('click', function () {
                //var selectedRow = $('#gridtable').jfGridGet('rowdata');
                //if (selectedRow.length == 0) {
                //    top.Changjie.alert.error("请选择需要启用的计费项");
                //    return false;
                //}
                //var ids = "";
                //selectedRow.forEach(function (row) {
                //    ids += "," + row.ID;
                //});
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认启用选中计费项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Operation/SetIsEnable', { ids: keyValue, isEnable: true }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            //  禁用
            $('#btn_disable').on('click', function () {
                //var selectedRow = $('#gridtable').jfGridGet('rowdata');
                //if (selectedRow.length == 0) {
                //    top.Changjie.alert.error("请选择需要禁用的计费项");
                //    return false;
                //}
                //var ids = "";
                //selectedRow.forEach(function (row) {
                //    ids += "," + row.ID;
                //});
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认禁用选中计费项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Operation/SetIsEnable', { ids: keyValue, isEnable: false }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });

            $('#log').on('click', function () {
                Changjie.layerForm({
                    id: 'formLog',
                    title: '操作日志',
                    isShowConfirmBtn: false,
                    url: top.$.rootUrl + '/BaseInfo/Operation/LogForm',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/Operation/GetPageList',
                headData: [
                    { label: "计费项名称", name: "ItemName", width: 300, align: "left"},
                    {
                        label: "费用类型", name: "ExpenseType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ExpenseType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    {
                        label: "计费点", name: "ChargePoint", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'OperationPoint',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "条件项", name: "Conditions", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'Conditions',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "计费单位", name: "Unit", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ChargeUnit',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "启用状态", name: "IsValid", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (value)
                                return "启用";
                            else
                                return "禁用";
                        }
                    },
                    {
                        label: "自动审核", name: "IsAutomaticAudit", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'SZSF',
                                callback: function (_data) {
                                    callback(!value ? "否" : _data.text);
                                }
                            });
                        }
                    },
                    { label: "备注", name: "Remark", width: 100, align: "left" },
                    { label: "创建人", name: "CreatedBy", width: 100, align: "left" },
                    { label: "创建时间", name: "CreatedDate", width: 100, align: "left" },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "发货阶梯档案信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/Operation/Form?keyValue=' + row.ID,
                        width: 1000,
                        height: 800
                    });
                },
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    var isEdit = top.Changjie.httpGet(top.$.rootUrl + "/BaseInfo/PriceSheet/IsEditForm?itemId=" + keyValue).data;
                    if (!isEdit) {
                        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                            if (res) {
                                top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Operation/DeleteForm', { keyValue: keyValue }, function () {
                                    refreshGirdData();
                                });
                            }
                        });
                    } else {
                        top.Changjie.alert.warning("不能删除该档案信息，报价单已经使用该档案信息");
                    }
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    var isEdit = top.Changjie.httpGet(top.$.rootUrl + "/BaseInfo/PriceSheet/IsEditForm?itemId=" + keyValue).data;
                    if (!isEdit) {
                        top.Changjie.layerForm({
                            id: 'form',
                            title: title,
                            isShowConfirmBtn: isShowConfirmBtn,
                            url: top.$.rootUrl + '/BaseInfo/Operation/Form?keyValue=' + keyValue + '&viewState=' + viewState,
                            width: 1000,
                            height: 800,
                            callBack: function (id) {
                                return top[id].acceptClick(refreshGirdData);
                            }
                        });
                    } else {
                        top.Changjie.alert.warning("不能编辑该档案信息，报价单已经使用该档案信息");
                    }
                }
            };
