﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-09 19:00
 * 描  述：作业档案信息
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="MeioErpOperation";
var processCommitUrl=top.$.rootUrl + '/BaseInfo/Operation/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var isName = false;
var maxValue = 0;
var iscopy = request('iscopy');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue && (iscopy == null || iscopy == "")){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'MeioErpOperationDetail',"gridId":'MeioErpOperationDetail'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#Type').mkDataItemSelect({ code: 'BJDLX' });
            //$('#Unit').mkDataItemSelect({ code: 'DW' });
            $('#Unit').mkDataItemSelect({ code: 'ChargeUnit' });
            $('#ExpenseType').mkDataItemSelect({ code: 'ExpenseType' });
            $('#ExpenseItem').mkDataItemSelect({ code: 'ExpenseItem' });
            $('#Conditions').mkDataItemSelect({ code: 'Conditions' });
            $('#ChargePoint').mkDataItemSelect({ code: 'OperationPoint' });
            $('#WeightUnit').mkDataItemSelect({ code: 'WeightUnit' });
            $('#IsValid').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#IsAutomaticAudit').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#MeioErpOperationDetail').jfGrid({
                headData: [
                    {
                        label: '最小值', name: 'MinValue', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        //edit: {
                        //    type: 'input',
                        //    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        //    },
                        //    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        //    }
                        //}
                    },
                    {
                        label: '最大值', name: 'MaxValue', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                if (parseFloat(row.MaxValue) <= parseFloat(row.MinValue)) {
                                    $("#jfgrid_toolbar_MeioErpOperationDetail").hide();
                                } else {
                                    $("#jfgrid_toolbar_MeioErpOperationDetail").show();
                                    maxValue = row.MaxValue;
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {

                            }
                        }
                    },
                    {
                        label: '说明', name: 'ItemName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: "操作", name: "option", width: 50, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"DeleteDetail('" + row['ID'] + "')\" >移除</a > "
                        }
                    },
                ],
                mainId:"ID",
                bindTable:"MeioErpOperationDetail",
                isEdit: true,
                height: 500,
                showdelete: false,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //        "EditType": 1,
                //     "MinValue":1
                //}],
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;

                    row.MinValue = maxValue;

                    $("#jfgrid_toolbar_MeioErpOperationDetail").hide();
                 },
                onMinusRow: function(row,rows,headData){
                 }
            });

            $("#ItemName").on("blur", function () {
                var value = $(this).val();
                isName = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Operation/CheckIsItemName?name=" + value + "&id=" + keyValue).data;
                if (isName) {
                    Changjie.alert.error("计费项名称已存在，请更换名称");
                }
            });

            $("#addOperation").on("click", function () {
                var rows = $("#MeioErpOperationDetail").jfGridGet("rowdatas");
                var strMaxValue = $("#MaxValue").val();
                if (strMaxValue == "") {
                    top.Changjie.alert.warning("最大值不能为空");
                } else {
                    if (!isInteger(strMaxValue)) {
                        top.Changjie.alert.warning("最大值必须是整数");
                        $("#MaxValue").val("");
                        $("#MaxValue").focus();
                        return false;
                    }
                    if (parseInt(strMaxValue) <= 0 || parseInt(strMaxValue) > 2147483647) {
                        top.Changjie.alert.warning("最大值必须大于0，且小于2147483647");
                    } else {
                        var maxValue = 0;
                        rows.forEach(function (itemrow) {
                            if (parseInt(itemrow.MaxValue) > maxValue)
                                maxValue = parseInt(itemrow.MaxValue);
                        });
                        if (parseInt(strMaxValue) <= maxValue) {
                            top.Changjie.alert.warning("填写的最大值必须大于当前列表中的最大值");
                        } else {
                            var row = {};
                            row.ID = top.Changjie.newGuid();
                            row.MinValue = maxValue + 1;
                            row.MaxValue = strMaxValue;
                            row.ItemName = "";
                            rows.push(row);
                            $("#MeioErpOperationDetail").jfGridSet("refreshdata", rows);
                        }
                    }
                }
                $("#MaxValue").val("");
                $("#MaxValue").focus();
            });

            $("#deleteOperation").on("click", function () {
                var rows = $('#MeioErpOperationDetail').jfGridGet('rowdatas');
                if (rows.length > 0) {
                    $('#MeioErpOperationDetail').jfGridSet('removeRow', rows[rows.length - 1].ID);
                }
            });

            $("#ExpenseType").change(function () {
                var expenseType = $("#ExpenseType").mkselectGet();
                var expenseItem = $("#ExpenseItem").mkselectGet();
                SetSelect(expenseType, expenseItem);
            });
            $("#ExpenseItem").change(function () {
                var expenseType = $("#ExpenseType").mkselectGet();
                var expenseItem = $("#ExpenseItem").mkselectGet();
                SetSelect(expenseType, expenseItem);
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/Operation/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }

                    if (iscopy == 1) {
                        $("#ItemName").val($("#ItemName").val() + "-复制");
                    }

                    var detailRowdatas = $('#MeioErpOperationDetail').jfGridGet('rowdatas');
                    if (detailRowdatas.length > 0) {
                        maxValue = detailRowdatas[detailRowdatas.length - 1].MaxValue;
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;

        if (isName) {
            Changjie.alert.error("计费项名称已存在，请更换名称");
            return false;
        }

        var verify = true;
        var detailRowdatas = $('#MeioErpOperationDetail').jfGridGet('rowdatas');
        detailRowdatas.forEach(function (row) {
            if (parseFloat(row.MinValue) >= parseFloat(row.MaxValue)) {
                verify = false;
            }
        });
        if (!verify) {
            Changjie.alert.warning("最大值不能小于最小值");
            return false;
        }
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/Operation/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="MeioErpOperation"]').mkGetFormData());
        postData.strmeioErpOperationDetailList = JSON.stringify($('#MeioErpOperationDetail').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
function isInteger(str) {
    return /^\d+$/.test(str);
}

var DeleteDetail = function (id) {
    var data = $("#MeioErpOperationDetail").jfGridGet('rowdatas');
    newData = $.grep(data, function (item) {
        return item.ID !== id;
    });

    if (newData == null || newData.length == 0) {
        maxValue = 0;
        $("#jfgrid_toolbar_MeioErpOperationDetail").show();
    } else {
        maxValue = newData[newData.length - 1].MaxValue;
        if (newData[newData.length - 1].MaxValue > newData[newData.length - 1].MinValue)
            $("#jfgrid_toolbar_MeioErpOperationDetail").show();
    }

    var maxnum = 0;
    $.each(newData, function (id, item) {
        if (item.MinValue !== maxnum) {
            item.MinValue = maxnum;
        }

        if (newData.length > 1) {
            maxnum = item.MaxValue;
        } else {
            maxnum = 0;
        }
    });

    $("#MeioErpOperationDetail").jfGridSet("refreshdata", newData);
};

var SetSelect = function (expenseType, expenseItem) {

    $('#Unit').mkDataItemSelect({ code: 'ChargeUnit' });

    $("#ExpenseItem").removeAttr("readonly");
    $("#Conditions").removeAttr("readonly");
    $("#Unit").removeAttr("readonly");

    $("#ExpenseItem").removeAttr("style");
    $("#Conditions").removeAttr("style");
    $("#Unit").removeAttr("style");

    if (expenseType == "仓租费" && expenseItem == "仓租费") {
        $("#Conditions").mkselectSet("库龄");
        $("#Unit").mkselectSet("CBM/天");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "入库费" && expenseItem == "卸货费-整柜") {
        $("#Conditions").mkselectSet("货柜尺数");
        $("#Unit").mkselectSet("柜");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "入库费" && expenseItem == "卸货费-托盘") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("托");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "入库费" && expenseItem == "卸货费-散箱") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("箱");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "入库费" && expenseItem == "上架费") {
        $("#Conditions").mkselectSet("重量");
        $('#Unit').mkselectRefresh({ data: [{ id: "个", text: "个" }, { id: "箱", text: "箱" }] });

        $("#Conditions").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "出库费" && expenseItem == "订单处理费") {
        $("#Conditions").mkselectSet("重量");
        $('#Unit').mkselectRefresh({ data: [{ id: "件", text: "件" }, { id: "个", text: "个" }, { id: "单", text: "单" }, { id: "箱", text: "箱" },{ id: "票", text: "票" }] });
        $("#Conditions").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "出库费" && expenseItem == "中转费(开箱)") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("箱");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "出库费" && expenseItem == "中转费(不开箱)") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("箱");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "出库费" && expenseItem == "贴标费") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("张");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "出库费" && expenseItem == "托盘出库") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("托");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "自提费" && expenseItem == "自提费") {
        $("#Conditions").mkselectSet("重量");
        $('#Unit').mkselectRefresh({ data: [{ id: "件", text: "件" }, { id: "个", text: "个" }, { id: "单", text: "单" }, { id: "箱", text: "箱" }, { id: "票", text: "票" }] });
        $("#Conditions").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "自提费" && expenseItem == "自提贴标费") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("张");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "包材费" && expenseItem == "包材打包费") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "运费" && expenseItem == "") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }

    if (expenseType == "增值服务费" && expenseItem == "") {
        $("#Conditions").mkselectSet("");
        $("#Unit").mkselectSet("");

        $("#Conditions").attr("readonly", "readonly");
        $("#Unit").attr("readonly", "readonly");

        $("#Conditions").attr('style', 'background-color: #f0f0f0;');
        $("#Unit").attr('style', 'background-color: #f0f0f0;');
    }
}
