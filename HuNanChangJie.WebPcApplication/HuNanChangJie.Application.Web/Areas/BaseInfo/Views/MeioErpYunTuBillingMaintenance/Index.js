﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2025-02-18 11:47
 * 描  述：云途账单维护
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var syncState;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#SyncState').mkDataItemSelect({ code: 'SyncState' });

            $('#SyncState').change(function () {
                syncState = $('#SyncState').mkselectGet();
            });
            // 查询
            $('#query').on('click', function () {
                //page.search({
                //    "SyncState": $("#SyncState").mkselectGet(),
                //    "Code": $("#Code").val()
                //});
                page.search();
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/Form',
                    width: 600,
                    height: 450,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            $('#sync').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                Changjie.loading(true, '正在同步...');

                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/Sync', { keyValue: keyValue }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("同步成功");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });
            $('#compute').on('click', function () {
                
                var keyValue = $('#gridtable').jfGridValue('ID');
                Changjie.loading(true, '正在计算账单...');

                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/Compute', { keyValue: keyValue }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("计算账单成功，仅未审核的运单费用会进行重算");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/GetPageList',
                headData: [
                    { label: "账单号", name: "Code", width: 200, align: "left"},
                    { label: "账单类型", name: "Type", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'BillType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "账单开始日期", name: "BillingCycleStartTime", width: 100, align: "left"},
                    { label: "账单结束日期", name: "BillingCycleEndTime", width: 100, align: "left"},
                    { label: "创建者名称", name: "CreationName", width: 100, align: "left"},
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left"},
                    { label: "同步时间", name: "SyncTime", width: 100, align: "left"},
                    { label: "同步状态", name: "SyncState", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'SyncState',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    { label: "云途账单条数", name: "YunTuSyncBillCount", width: 100, align: "left" },
                    {
                        label: "计算成功条数", name: "CalculateSuccessCount", width: 200, align: "left",
                        formatter: function (v, row) {
                            if (row.YunTuSyncBillCount != row.CalculateSuccessCount)
                                return row.CalculateSuccessCount + "  <a style='color:blue' onclick=\"BillingMaintenanceError('" + row['ID'] + "')\" >错误信息</a > ";
                            else
                                return row.CalculateSuccessCount;
                        }
                    },
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            param.SyncState = syncState;
            param.Code = $("#Code").val();
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 600,
                        height: 450,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
};

var BillingMaintenanceError = function (BillingMaintenanceID) {
    top.Changjie.layerForm({
        id: 'form11',
        title: "错误信息",
        isShowConfirmBtn: false,
        url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/BillingMaintenanceErrorForm?BillingMaintenanceID=' + BillingMaintenanceID,
        width: 600,
        height: 500,
        callBack: function (id) {
            return top[id].acceptClick(refreshGirdData);
        }
    });
}
