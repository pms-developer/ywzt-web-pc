﻿/* *  
 * 创建人：超级管理员
 * 日  期：2025-02-18 11:47
 * 描  述：云途账单维护
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="MeioErpYunTuBillingMaintenance";
var processCommitUrl=top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');

var isCode = false;
var strCodes = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#Type').mkDataItemSelect({ code: 'BillType' });
            $('#SyncState').mkDataItemSelect({ code: 'SyncState' });

            $("#Code").on("blur", function () {
                var lines = $("#Code").val().split('\n');
                var value = "";
                lines.forEach(function (item) {
                    if (item != "") {
                        value += "," + item;
                    }
                });
                if (keyValue != "" && keyValue != null) {
                    isCode = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/MeioErpYunTuBillingMaintenance/CheckIsYunTuBillingMaintenanceCode?code=" + value.substring(1) + "&id=" + keyValue).info;
                    if (isCode) {
                        Changjie.alert.error("账单号已存在，请更换");
                    }
                } else {
                    strCodes = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/MeioErpYunTuBillingMaintenance/CheckIsYunTuBillingMaintenanceCode1?code=" + value.substring(1)).info;
                    if (strCodes.length > 0) {
                        Changjie.alert.error("账单号" + strCodes + "已存在，请更换");
                    }
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;

        var formdata = $('body').mkGetFormData();
        var dateStart = new Date(formdata.BillingCycleStartTime);
        var dateEnd = new Date(formdata.BillingCycleEndTime);

        if (dateStart >= dateEnd) {
            Changjie.alert.warning("账单结束日期必须大于账单开始日期");
            return false;
        }

        var lines = $("#Code").val().split('\n');
        var repeatItemvalue = "";
        for (let i = 0; i < lines.length; i++) {
            if (lines.indexOf(lines[i]) !== i) {
                repeatItemvalue += "," + lines[i]; // 找到重复项
            }
        }
        if (repeatItemvalue.length > 0) {
            Changjie.alert.error("账单号：" + repeatItemvalue.substring(1) + "重复录入");
            return false;
        }

        if (isCode) {
            Changjie.alert.error("账单号已存在，请更换");
            return false;
        }

        if (strCodes.length > 0) {
            Changjie.alert.error("账单号" + strCodes + "已存在，请更换");
            return false;
        }
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBillingMaintenance/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
     return postData;
}
