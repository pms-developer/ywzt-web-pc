﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-13 16:32
 * 描  述：合同管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpContract";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/Contract/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var isCode = false;
var isContractCode = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
                $("#ServiceStartDate").hide();
                $("#ServiceEndDate").hide();
                $("#txtServiceStartDate").show();
                $("#txtServiceEndDate").show();
                $("#ClientCode").attr("readonly", true);
            }
            else {
                mainId = top.Changjie.newGuid();
                //subGrid.push({ "tableName": 'MeioErpContractDetail', "gridId": 'MeioErpContractDetail' });
                //subGrid.push({ "tableName": 'MeioErpContractRecord', "gridId": 'MeioErpContractRecord' });
                type = "add";
                $("#ServiceStartDate").show();
                $("#ServiceEndDate").show();
                $("#txtServiceStartDate").hide();
                $("#txtServiceEndDate").hide();
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $('#IsWarehouseRent').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#PackagePrice').hide();
            $('#Discount').hide();
            $('#DiscountUnit').hide();
            $("#ContractingParty").mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/UserCenter/GetAllUser',
                value: "F_UserId",
                text: "F_RealName"
            });

            $('#BillingType').mkselect({ value: 'id', text: 'text', title: 'text' });
            //Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'ContractCode1' }, function (data) {
            //    if (!$('#Code').val()) {
            //        $('#Code').val(data);
            //    }
            //});
            $("#contract_form_tabs").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: false,
            });
            $('#contract_form_tabs').mkFormTab();
            $('#contract_form_tabs ul li').eq(0).trigger('click');

            $('#PriceSheetType').mkDataItemSelect({ code: 'ContractPriceSheetType' });
            $('#PriceSheetCode').mkDataSourceSelect({ code: 'PriceSheetList', value: 'id', text: 'code' });
            $('#PriceSheetCodeXiaoBao').mkDataSourceSelect({ code: 'PriceSheetListXiaoBao', value: 'id', text: 'code' });
            //$('#BillingType').mkDataItemSelect({ code: 'BillingType' });
            $('#LogisticsPackageAccountBillingType').mkDataItemSelect({ code:'ContractLogisticsPackageAccountBillingType'})
            $("#contract_form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: false,
                isShowWorkflow: false,
            });
            $('#contract_form_tabs_sub').mkFormTab();
            $('#contract_form_tabs_sub ul li').eq(0).trigger('click');

            $('#MeioErpContractDetailWarehouseRent').jfGrid({
                headData: [
                    {
                        label: '阶梯项', name: 'ItemName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 300, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '定价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '签约价/RMB', name: 'ContractPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var discount = (row.Price - row.ContractPrice) * 100 / row.Price;
                                discount = Changjie.format45(discount, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                                discount = discount > 0 ? discount : 0;
                                row.Discount = discount;
                                setCellValue("Discount", row, index, "MeioErpContractDetailWarehouseRent", discount);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '优惠幅度/%', name: 'Discount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                    },
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#MeioErpContractDetailOperation').jfGrid({
                headData: [
                    {
                        label: '日单量', name: 'ItemName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 300, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '定价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '签约价/RMB', name: 'ContractPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var discount = (row.Price - row.ContractPrice) * 100 / row.Price;
                                discount = Changjie.format45(discount, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                                discount = discount > 0 ? discount : 0;
                                row.Discount = discount;
                                setCellValue("Discount", row, index, "MeioErpContractDetailOperation", discount);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '优惠幅度/%', name: 'Discount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                    },
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#MeioErpContractDetailValueAddedServices').jfGrid({
                headData: [
                    {
                        label: '服务项', name: 'ItemName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: "作业点", name: "OperationPoint", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'OperationPoint',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: '定价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '签约价/RMB', name: 'ContractPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var discount = (row.Price - row.ContractPrice) * 100 / row.Price;
                                discount = Changjie.format45(discount, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                                discount = discount > 0 ? discount : 0;
                                row.Discount = discount;
                                setCellValue("Discount", row, index, "MeioErpContractDetailValueAddedServices", discount);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '优惠幅度/%', name: 'Discount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                    },
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#MeioErpContractDetailValueAddedServicesXiaoBao').jfGrid({
                headData: [
                    {
                        label: '服务项', name: 'ItemName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: "作业点", name: "OperationPoint", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'OperationPoint',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: '定价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '签约价/RMB', name: 'ContractPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var discount = (row.Price - row.ContractPrice) * 100 / row.Price;
                                discount = Changjie.format45(discount, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                                discount = discount > 0 ? discount : 0;
                                row.Discount = discount;
                                setCellValue("Discount", row, index, "MeioErpContractDetailValueAddedServicesXiaoBao", discount);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '优惠幅度/%', name: 'Discount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: ''
                    },
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });

            $('#MeioErpContractRecord').jfGrid({
                headData: [
                    { label: "服务起始日期", name: "ServiceStartDate", width: 100, align: "left" },
                    { label: "服务截止日期", name: "ServiceEndDate", width: 100, align: "left" },
                    {
                        label: "合同状态", name: "ContractStatus", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ContractStatus',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "创建用户", name: "CreatedBy", width: 100, align: "left" },
                    { label: "创建时间", name: "CreatedDate", width: 100, align: "left" },
                ],
                mainId: "ID",
                height: 300
            });

            $('#MeioErpContractDetailWarehouseRent1').jfGrid({
                headData: [
                    {
                        label: '阶梯项', name: 'ItemName', width: 200, align: "left"
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 300, align: "left"
                    },
                    {
                        label: '定价', name: 'Price', width: 100, align: "left"
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, align: "left"
                    }
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#MeioErpContractDetailOperation1').jfGrid({
                headData: [
                    {
                        label: '日单量', name: 'ItemName', width: 200, align: "left"
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 300, align: "left"
                    },
                    {
                        label: '定价', name: 'Price', width: 100, align: "left"
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, align: "left"
                    }
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#MeioErpContractDetailValueAddedServices1').jfGrid({
                headData: [
                    {
                        label: '服务项', name: 'ItemName', width: 200, align: "left"
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 300, align: "left"
                    },
                    {
                        label: "作业点", name: "OperationPoint", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'OperationPoint',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: '定价', name: 'Price', width: 100, align: "left"
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, align: "left"
                    }
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#MeioErpContractDetailValueAddedServicesXiaoBao1').jfGrid({
                headData: [
                    {
                        label: '服务项', name: 'ItemName', width: 200, align: "left"
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 300, align: "left"
                    },
                    {
                        label: "作业点", name: "OperationPoint", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'OperationPoint',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: '定价', name: 'Price', width: 100, align: "left"
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, align: "left"
                    }
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                onMinusRow: function (row, rows, headData) {
                }
            });

            $('#MeioErpContractDetailWarehouseRent2').jfGrid({
                headData: [
                    {
                        label: '阶梯项', name: 'ItemName', width: 200, align: "left"
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 300, align: "left"
                    },
                    {
                        label: '定价', name: 'Price', width: 100, align: "left"
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, align: "left"
                    },
                    {
                        label: '签约价/RMB', name: 'ContractPrice', width: 100, align: "left"
                    },
                    {
                        label: '优惠幅度/%', name: 'Discount', width: 100, align: "left"
                    },
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#MeioErpContractDetailOperation2').jfGrid({
                headData: [
                    {
                        label: '日单量', name: 'ItemName', width: 200, align: "left"
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 300, align: "left"
                    },
                    {
                        label: '定价', name: 'Price', width: 100, align: "left"
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, align: "left"
                    },
                    {
                        label: '签约价/RMB', name: 'ContractPrice', width: 100, align: "left"
                    },
                    {
                        label: '优惠幅度/%', name: 'Discount', width: 100, align: "left"
                    },
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#MeioErpContractDetailValueAddedServices2').jfGrid({
                headData: [
                    {
                        label: '服务项', name: 'ItemName', width: 200, align: "left"
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 200, align: "left"
                    },
                    {
                        label: "作业点", name: "OperationPoint", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'OperationPoint',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: '定价', name: 'Price', width: 100, align: "left"
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, align: "left"
                    },
                    {
                        label: '签约价/RMB', name: 'ContractPrice', width: 100, align: "left"
                    },
                    {
                        label: '优惠幅度/%', name: 'Discount', width: 100, align: "left"
                    },
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });

            $('#MeioErpContractDetailValueAddedServicesXiaoBao2').jfGrid({
                headData: [
                    {
                        label: '服务项', name: 'ItemName', width: 200, align: "left"
                    },
                    {
                        label: '说明', name: 'ItemDetailName', width: 200, align: "left"
                    },
                    {
                        label: "作业点", name: "OperationPoint", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'OperationPoint',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: '定价', name: 'Price', width: 100, align: "left"
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, align: "left"
                    },
                    {
                        label: '签约价/RMB', name: 'ContractPrice', width: 100, align: "left"
                    },
                    {
                        label: '优惠幅度/%', name: 'Discount', width: 100, align: "left"
                    },
                ],
                mainId: "ID",
                //bindTable:"MeioErpContractDetail",
                //isEdit: true,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                //onAddRow: function(row,rows){
                //     row["ID"]=Changjie.newGuid();
                //     row.rowState=1;
                //     row.SortCode=rows.length;
                //     row.EditType=1;
                // },
                onMinusRow: function (row, rows, headData) {
                }
            });

            $('#PriceSheetCode').change(function () {
                var priceSheetId = $('[data-table="MeioErpContract"]').mkGetFormData().PriceSheetCode;
                var priceSheetDetailData = Changjie.httpGet(top.$.rootUrl + '/BaseInfo/PriceSheet/GetPriceSheetDetail?priceSheetId=' + priceSheetId).data;
                $('#MeioErpContractDetailWarehouseRent').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailWarehouseRent, 'edit');
                //$('#MeioErpContractDetailOperation').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailOperation, 'edit');
                $('#MeioErpContractDetailValueAddedServices').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailValueAddedServices, 'edit');

                $('#MeioErpContractDetailWarehouseRent1').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailWarehouseRent);
                //$('#MeioErpContractDetailOperation1').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailOperation);
                $('#MeioErpContractDetailValueAddedServices1').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailValueAddedServices);

                $('#MeioErpContractDetailWarehouseRent2').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailWarehouseRent);
                //$('#MeioErpContractDetailOperation2').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailOperation);
                $('#MeioErpContractDetailValueAddedServices2').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailValueAddedServices);

            });

            $('#PriceSheetCodeXiaoBao').change(function () {

                var priceSheetId = $('[data-table="MeioErpContract"]').mkGetFormData().PriceSheetCodeXiaoBao;
                var priceSheetDetailData = Changjie.httpGet(top.$.rootUrl + '/BaseInfo/PriceSheet/GetPriceSheetDetail?priceSheetId=' + priceSheetId).data;
                //$('#MeioErpContractDetailWarehouseRent').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailWarehouseRent, 'edit');
                $('#MeioErpContractDetailOperation').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailOperation, 'edit');
                $('#MeioErpContractDetailValueAddedServicesXiaoBao').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailValueAddedServices, 'edit');

                //$('#MeioErpContractDetailWarehouseRent1').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailWarehouseRent);
                $('#MeioErpContractDetailOperation1').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailOperation);
                $('#MeioErpContractDetailValueAddedServicesXiaoBao1').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailValueAddedServices);

                //$('#MeioErpContractDetailWarehouseRent2').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailWarehouseRent);
                $('#MeioErpContractDetailOperation2').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailOperation);
                $('#MeioErpContractDetailValueAddedServicesXiaoBao2').jfGridSet('refreshdata', priceSheetDetailData.MeioErpPriceSheetDetailValueAddedServicess);
            });

            $('#PriceSheetType').change(function () {
                var priceSheetType = $('[data-table="MeioErpContract"]').mkGetFormData().PriceSheetType;
                if (priceSheetType == 1) {
                    $('#Discount').hide();
                    $('#DiscountUnit').hide();
                    $('#BillingType').mkselectRefresh({ data: [{ id: 1, text: "计件" }, { id: 2, text: "包干" }] });
                    //隐藏【签约价】【优惠幅度】字段
                    hideContractPrice();
                }
                else if (priceSheetType == 2) {
                    $('#Discount').show();
                    $('#DiscountUnit').show();
                    //$('#Discount').val("");
                    $('#BillingType').mkselectRefresh({ data: [{ id: 1, text: "计件" }] });

                    //显示【签约价】【优惠幅度】字段 不可编辑
                    showContractPrice();

                } else if (priceSheetType == 3) {
                    $('#Discount').hide();
                    $('#DiscountUnit').hide();
                    $('#BillingType').mkselectRefresh({ data: [{ id: 1, text: "计件" }] });
                    //编辑【签约价】字段，显示【优惠幅度】字段
                    editContractPrice();
                }
            });

            $('#MeioErpDocumentAuditRecord').jfGrid({
                headData: [
                    { label: "审批人", name: "Auditor", width: 100, align: "left" },
                    {
                        label: "审批结果", name: "AuditResult", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ContractAuditStatus',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "审批时间", name: "AuditDate", width: 150, align: "left" },
                    { label: "备注", name: "Remark", width: 300, align: "left" },
                ],
                mainId: "ID",
                height: 300
            });

            $('#BillingType').change(function () {
                var BillingType = $('[data-table="MeioErpContract"]').mkGetFormData().BillingType;
                var priceSheetType = $('[data-table="MeioErpContract"]').mkGetFormData().PriceSheetType;
                if (BillingType == 2) {
                    $('#PackagePrice').show();
                    if (priceSheetType == 1 || priceSheetType == 3) {
                        hideContractPrice();
                    }
                    //隐藏 仓库租金，常规作业，增值服务选项卡
                    //$('#tabWarehouseRent').attr("style", "display:none");
                    //$('#tabOperation').attr("style", "display:none");
                    //$('#tabValueAddServices').attr("style", "display:none");

                    //$("#tabContractRecord").click();

                    var warhouseRentData = $('#MeioErpContractDetailWarehouseRent').jfGridGet('rowdatas');
                    var operationData = $('#MeioErpContractDetailOperation').jfGridGet('rowdatas');
                    var valueAddedServicesData = $('#MeioErpContractDetailValueAddedServices').jfGridGet('rowdatas');
                    var valueAddedServicesXiaoBaoData = $('#MeioErpContractDetailValueAddedServicesXiaoBao').jfGridGet('rowdatas');

                    warhouseRentData.forEach(function (row) {
                        row.ContractPrice = 0;
                        row.Discount = 0;
                    });
                    $('#MeioErpContractDetailWarehouseRent').jfGridSet("refreshdata", warhouseRentData, 'edit');
                    $('#MeioErpContractDetailWarehouseRent2').jfGridSet("refreshdata", warhouseRentData);
                    operationData.forEach(function (row) {
                        row.ContractPrice = 0;
                        row.Discount = 0;
                    });
                    $('#MeioErpContractDetailOperation').jfGridSet("refreshdata", operationData, 'edit');
                    $('#MeioErpContractDetailOperation2').jfGridSet("refreshdata", operationData);
                    valueAddedServicesData.forEach(function (row) {
                        row.ContractPrice = 0;
                        row.Discount = 0;
                    });
                    $('#MeioErpContractDetailValueAddedServices').jfGridSet("refreshdata", valueAddedServicesData, 'edit');
                    $('#MeioErpContractDetailValueAddedServices2').jfGridSet("refreshdata", valueAddedServicesData);
                    valueAddedServicesXiaoBaoData.forEach(function (row) {
                        row.ContractPrice = 0;
                        row.Discount = 0;
                    });
                    $('#MeioErpContractDetailValueAddedServicesXiaoBao').jfGridSet("refreshdata", valueAddedServicesXiaoBaoData, 'edit');
                    $('#MeioErpContractDetailValueAddedServicesXiaoBao2').jfGridSet("refreshdata", valueAddedServicesXiaoBaoData);
                }
                else {
                    $('#PackagePrice').hide();
                    if (priceSheetType == 1) {
                        //隐藏【签约价】【优惠幅度】字段
                        hideContractPrice();
                    }
                    else if (priceSheetType == 2) {
                        //显示【签约价】【优惠幅度】字段 不可编辑
                        showContractPrice();
                    }
                    else if (priceSheetType == 3) {
                        //编辑【签约价】字段，显示【优惠幅度】字段
                        editContractPrice();
                    }

                    //$('#tabWarehouseRent').removeAttr("style");
                    //$('#tabOperation').removeAttr("style");
                    //$('#tabValueAddServices').removeAttr("style");
                }

            });

            $("#ClientCode").on("blur", function () {
                if (type == "add") {
                    var value = $(this).val();
                    isCode = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Contract/CheckIsClientContract?clientCode=" + value).data;
                    if (isCode) {
                        Changjie.alert.error("该客户编码存在服务期内的合同。解决方法：1，终止该客户编码相关的服务期内合同。2，修改客户编码。");
                    }
                }
            });

            $("#Code").on("blur", function () {
                if (type == "add") {
                    var value = $(this).val();
                    isContractCode = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Contract/CheckIsCode?code=" + value + "&id=" + keyValue).data;
                    if (isContractCode) {
                        Changjie.alert.error("合同编码已存在");
                    }
                }
            });

            $("#ServiceStartDate").change(function () {
                var startDate = new Date($('[data-table="MeioErpContract"]').mkGetFormData().ServiceStartDate);
                var year = startDate.getFullYear() + 1;
                var month = startDate.getMonth() + 1;
                var day = startDate.getDate();
                var dateString = year + "-" + (month < 10 ? '0' + month : month) + "-" + (day < 10 ? '0' + day : day);
                $("#ServiceEndDate").val(dateString);
            });

            $("#Discount").on("input", function () {
                var discount = $(this).val();
                if (discount > 100) {
                    Changjie.alert.error("最大折扣不要超过100");
                    return false;
                }
                //计算签约价=折扣*定价   优惠幅度=（定价-签约价）*100/定价
                var warhouseRentData = $('#MeioErpContractDetailWarehouseRent').jfGridGet('rowdatas');
                var operationData = $('#MeioErpContractDetailOperation').jfGridGet('rowdatas');
                var valueAddedServicesData = $('#MeioErpContractDetailValueAddedServices').jfGridGet('rowdatas');
                var valueAddedServicesXiaoBaoData = $('#MeioErpContractDetailValueAddedServicesXiaoBao').jfGridGet('rowdatas');

                warhouseRentData.forEach(function (row) {
                    row.ContractPrice = discount * row.Price / 100;
                    row.ContractPrice = Changjie.format45(row.ContractPrice, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                    if (row.ContractPrice > row.Price)
                        row.Discount = 0;
                    else {
                        row.Discount = (row.Price - row.ContractPrice) * 100 / row.Price;
                        row.Discount = Changjie.format45(row.Discount, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                    }
                });
                $('#MeioErpContractDetailWarehouseRent').jfGridSet("refreshdata", warhouseRentData);
                $('#MeioErpContractDetailWarehouseRent2').jfGridSet("refreshdata", warhouseRentData);
                operationData.forEach(function (row) {
                    row.ContractPrice = discount * row.Price / 100;
                    row.ContractPrice = Changjie.format45(row.ContractPrice, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                    if (row.ContractPrice > row.Price)
                        row.Discount = 0;
                    else {
                        row.Discount = (row.Price - row.ContractPrice) * 100 / row.Price;
                        row.Discount = Changjie.format45(row.Discount, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                    }
                });
                $('#MeioErpContractDetailOperation').jfGridSet("refreshdata", operationData);
                $('#MeioErpContractDetailOperation2').jfGridSet("refreshdata", operationData);
                valueAddedServicesData.forEach(function (row) {
                    row.ContractPrice = discount * row.Price / 100;
                    row.ContractPrice = Changjie.format45(row.ContractPrice, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                    if (row.ContractPrice > row.Price)
                        row.Discount = 0;
                    else {
                        row.Discount = (row.Price - row.ContractPrice) * 100 / row.Price;
                        row.Discount = Changjie.format45(row.Discount, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                    }
                });
                $('#MeioErpContractDetailValueAddedServices').jfGridSet("refreshdata", valueAddedServicesData);
                $('#MeioErpContractDetailValueAddedServices2').jfGridSet("refreshdata", valueAddedServicesData);
                valueAddedServicesXiaoBaoData.forEach(function (row) {
                    row.ContractPrice = discount * row.Price / 100;
                    row.ContractPrice = Changjie.format45(row.ContractPrice, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                    if (row.ContractPrice > row.Price)
                        row.Discount = 0;
                    else {
                        row.Discount = (row.Price - row.ContractPrice) * 100 / row.Price;
                        row.Discount = Changjie.format45(row.Discount, Changjie.sysGlobalSettings.pointGlobal2) || 0;
                    }
                });
                $('#MeioErpContractDetailValueAddedServicesXiaoBao').jfGridSet("refreshdata", valueAddedServicesXiaoBaoData);
                $('#MeioErpContractDetailValueAddedServicesXiaoBao2').jfGridSet("refreshdata", valueAddedServicesXiaoBaoData);
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/Contract/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }

                        var BillingType = $('[data-table="MeioErpContract"]').mkGetFormData().BillingType;
                        if (BillingType == 2) {
                            $('#PackagePrice').show();
                        } else {
                            $('#PackagePrice').hide();
                        }

                        $('#txtServiceStartDate').val($('[data-table="MeioErpContract"]').mkGetFormData().ServiceStartDate);
                        $('#txtServiceEndDate').val($('[data-table="MeioErpContract"]').mkGetFormData().ServiceEndDate);

                        $('#MeioErpContractDetailWarehouseRent1').jfGridSet('refreshdata', data.MeioErpContractDetailWarehouseRent);
                        $('#MeioErpContractDetailOperation1').jfGridSet('refreshdata', data.MeioErpContractDetailOperation);
                        $('#MeioErpContractDetailValueAddedServices1').jfGridSet('refreshdata', data.MeioErpContractDetailValueAddedServices);
                        $('#MeioErpContractDetailValueAddedServicesXiaoBao1').jfGridSet('refreshdata', data.MeioErpContractDetailValueAddedServicesXiaoBao);

                        $('#MeioErpContractDetailWarehouseRent2').jfGridSet('refreshdata', data.MeioErpContractDetailWarehouseRent);
                        $('#MeioErpContractDetailOperation2').jfGridSet('refreshdata', data.MeioErpContractDetailOperation);
                        $('#MeioErpContractDetailValueAddedServices2').jfGridSet('refreshdata', data.MeioErpContractDetailValueAddedServices);
                        $('#MeioErpContractDetailValueAddedServicesXiaoBao2').jfGridSet('refreshdata', data.MeioErpContractDetailValueAddedServicesXiaoBao);
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;

        if (postData.ServiceStartDate >= postData.ServiceEndDate) {
            Changjie.alert.warning("服务起始时间必须大于服务截止时间");
            return false;
        }

        if (postData.BillingType == 2 && postData.PackagePrice == "") {
            Changjie.alert.warning("【计费类型】为：【包干】时，包干单价不能为空");
            return false;
        }

        if (postData.PriceSheetType == 2 && postData.Discount == "") {
            Changjie.alert.warning("【报价类型】为：【折扣报价】时，折扣不能为空");
            return false;
        }
        if (isContractCode) {
            Changjie.alert.error("合同编码已存在");
            return false;
        }

        if (isCode) {
            Changjie.alert.error("该客户编码存在服务期内的合同。解决方法：1，终止该客户编码相关的服务期内合同。2，修改客户编码。");
            return false;
        }

        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/Contract/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var contractDetailData = [];
    var warhouseRentData = $('#MeioErpContractDetailWarehouseRent').jfGridGet('rowdatas');
    var operationData = $('#MeioErpContractDetailOperation').jfGridGet('rowdatas');
    var valueAddedServicesData = $('#MeioErpContractDetailValueAddedServices').jfGridGet('rowdatas');
    var valueAddedServicesXiaoBaoData = $('#MeioErpContractDetailValueAddedServicesXiaoBao').jfGridGet('rowdatas');

    if (warhouseRentData.length > 0) {
        warhouseRentData.forEach(function (item) {
            item.Type = 2;
            item.ItemType = 1;
        });
        contractDetailData = contractDetailData.concat(warhouseRentData);
    }
    if (operationData.length > 0) {
        operationData.forEach(function (item) {
            item.Type = 1;
            item.ItemType = 2;
        });
        contractDetailData = contractDetailData.concat(operationData);
    }
    if (valueAddedServicesData.length > 0) {
        valueAddedServicesData.forEach(function (item) {
            item.Type = 2;
            item.ItemType = 3;
        });
        contractDetailData = contractDetailData.concat(valueAddedServicesData);
    }
    if (valueAddedServicesXiaoBaoData.length > 0) {
        valueAddedServicesXiaoBaoData.forEach(function (item) {
            item.Type = 1;
            item.ItemType = 3;
        });
        contractDetailData = contractDetailData.concat(valueAddedServicesXiaoBaoData);
    }

    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="MeioErpContract"]').mkGetFormData());
    postData.strmeioErpContractDetailList = JSON.stringify(contractDetailData);
    //postData.strmeioErpContractRecordEntity = JSON.stringify($('[data-table="MeioErpContractRecord"]').mkGetFormData());
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}

var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;

    if (row.EditType == 0) {
        row.EditType = 2;
    }

    $cell.html(value);
    $edit.val(value);
};

var hideContractPrice = function () {
    //隐藏【签约价】【优惠幅度】字段
    $('#MeioErpContractDetailWarehouseRent').hide();
    $('#MeioErpContractDetailOperation').hide();
    $('#MeioErpContractDetailValueAddedServices').hide();
    $('#MeioErpContractDetailValueAddedServicesXiaoBao').hide();

    $('#MeioErpContractDetailWarehouseRent1').show();
    $('#MeioErpContractDetailOperation1').show();
    $('#MeioErpContractDetailValueAddedServices1').show();
    $('#MeioErpContractDetailValueAddedServicesXiaoBao1').show();

    $('#MeioErpContractDetailWarehouseRent2').hide();
    $('#MeioErpContractDetailOperation2').hide();
    $('#MeioErpContractDetailValueAddedServices2').hide();
    $('#MeioErpContractDetailValueAddedServicesXiaoBao2').hide();

    //if (type == "add") {

    var warhouseRentData = $('#MeioErpContractDetailWarehouseRent').jfGridGet('rowdatas');
    var operationData = $('#MeioErpContractDetailOperation').jfGridGet('rowdatas');
    var valueAddedServicesData = $('#MeioErpContractDetailValueAddedServices').jfGridGet('rowdatas');
    var valueAddedServicesXiaoBaoData = $('#MeioErpContractDetailValueAddedServicesXiaoBao').jfGridGet('rowdatas');

    warhouseRentData.forEach(function (row) {
        row.ContractPrice = row.Price;
        row.Discount = 0;
    });
    $('#MeioErpContractDetailWarehouseRent').jfGridSet("refreshdata", warhouseRentData, 'edit');
    $('#MeioErpContractDetailWarehouseRent2').jfGridSet("refreshdata", warhouseRentData);
    operationData.forEach(function (row) {
        row.ContractPrice = row.Price;
        row.Discount = 0;
    });
    $('#MeioErpContractDetailOperation').jfGridSet("refreshdata", operationData, 'edit');
    $('#MeioErpContractDetailOperation2').jfGridSet("refreshdata", operationData);
    valueAddedServicesData.forEach(function (row) {
        row.ContractPrice = row.Price;
        row.Discount = 0;
    });
    $('#MeioErpContractDetailValueAddedServices').jfGridSet("refreshdata", valueAddedServicesData, 'edit');
    $('#MeioErpContractDetailValueAddedServices2').jfGridSet("refreshdata", valueAddedServicesData);
    valueAddedServicesXiaoBaoData.forEach(function (row) {
        row.ContractPrice = row.Price;
        row.Discount = 0;
    });
    $('#MeioErpContractDetailValueAddedServicesXiaoBao').jfGridSet("refreshdata", valueAddedServicesXiaoBaoData, 'edit');
    $('#MeioErpContractDetailValueAddedServicesXiaoBao2').jfGridSet("refreshdata", valueAddedServicesXiaoBaoData);
    //}
}

var showContractPrice = function () {
    $('#MeioErpContractDetailWarehouseRent').hide();
    $('#MeioErpContractDetailOperation').hide();
    $('#MeioErpContractDetailValueAddedServices').hide();
    $('#MeioErpContractDetailValueAddedServicesXiaoBao').hide();

    $('#MeioErpContractDetailWarehouseRent1').hide();
    $('#MeioErpContractDetailOperation1').hide();
    $('#MeioErpContractDetailValueAddedServices1').hide();
    $('#MeioErpContractDetailValueAddedServicesXiaoBao1').hide();

    $('#MeioErpContractDetailWarehouseRent2').show();
    $('#MeioErpContractDetailOperation2').show();
    $('#MeioErpContractDetailValueAddedServices2').show();
    $('#MeioErpContractDetailValueAddedServicesXiaoBao2').show();
}

var editContractPrice = function () {
    $('#MeioErpContractDetailWarehouseRent').show();
    $('#MeioErpContractDetailOperation').show();
    $('#MeioErpContractDetailValueAddedServices').show();
    $('#MeioErpContractDetailValueAddedServicesXiaoBao').show();

    $('#MeioErpContractDetailWarehouseRent1').hide();
    $('#MeioErpContractDetailOperation1').hide();
    $('#MeioErpContractDetailValueAddedServices1').hide();
    $('#MeioErpContractDetailValueAddedServicesXiaoBao1').hide();

    $('#MeioErpContractDetailWarehouseRent2').hide();
    $('#MeioErpContractDetailOperation2').hide();
    $('#MeioErpContractDetailValueAddedServices2').hide();
    $('#MeioErpContractDetailValueAddedServicesXiaoBao2').hide();
}
