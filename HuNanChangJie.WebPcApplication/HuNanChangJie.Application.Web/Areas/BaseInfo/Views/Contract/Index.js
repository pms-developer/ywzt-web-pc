﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-05-13 16:32
 * 描  述：合同管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#Status').mkDataItemSelect({ code: 'ContractStatus' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "Code": $("#Code").val(),
                    "ClientName": $("#ClientName").val(),
                    "Status": $("#Status").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/Contract/Form1',
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            //合约处理
            $('#btn_ContractProcessing').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                var serviceStartDate = $('#gridtable').jfGridValue('ServiceStartDate');
                var serviceEndDate = $('#gridtable').jfGridValue('ServiceEndDate');
                if (top.Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'dialog',
                        title: '合约处理',
                        url: top.$.rootUrl + '/BaseInfo/Contract/Dialog?keyValue=' + keyValue + "&serviceStartDate=" + serviceStartDate + "&serviceEndDate=" + serviceEndDate,
                        width: 300,
                        height: 300,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            //审核
            $('#btn_Audit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                var auditStatus = $('#gridtable').jfGridValue('AuditStatus');
                if (auditStatus != 0) {
                    top.Changjie.alert.warning("审核失败，合同已审核");
                    return false;
                }
                if (top.Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'audit',
                        title: '审批',
                        url: top.$.rootUrl + '/BaseInfo/Contract/Audit?keyValue=' + keyValue,
                        width: 300,
                        height: 300,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            //反审核
            $("#btn_Unaudit").on("click", function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                var auditStatus = $('#gridtable').jfGridValue('AuditStatus');
                if (auditStatus == 0) {
                    top.Changjie.alert.warning("合同为待审核状态，无法反审核操作");
                    return false;
                }

                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认反审核该项！', function (res) {
                        var param = {};
                        param['__RequestVerificationToken'] = $.mkToken;
                        param['keyValue'] = keyValue;
                        top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/Contract/UnAuditContract',
                            param,
                            function (res) {
                                refreshGirdData();
                                });
                        
                    });
                    
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/Contract/GetPageList',
                headData: [
                    { label: "合同编号", name: "Code", width: 200, align: "left" },
                    { label: "客户名称", name: "ClientName", width: 200, align: "left" },
                    { label: "法人", name: "LegalPerson", width: 100, align: "left" },
                    { label: "客户负责人", name: "ClientPersonCharge", width: 100, align: "left" },
                    { label: "负责人电话", name: "ClientPersonChargePhone", width: 100, align: "left" },
                    { label: "服务起始日期", name: "ServiceStartDate", width: 100, align: "left" },
                    { label: "服务截止日期", name: "ServiceEndDate", width: 100, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ContractAuditStatus',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "合同状态", name: "Status", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ContractStatus',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "签约人", name: "ContractingPartyName", width: 100, align: "left" },
                    { label: "创建用户", name: "CreatedBy", width: 100, align: "left" },
                    { label: "创建时间", name: "CreatedDate", width: 100, align: "left" },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "合同信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/Contract/Form1?keyValue=' + row.ID,
                        width: 1000,
                        height: 800
                    });
                },
                mainId: 'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        console.log("delete");
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Contract/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/BaseInfo/Contract/Form1?keyValue=' + keyValue + '&viewState=' + viewState,
            width: 1000,
            height: 800,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
