﻿var acceptClick;
var keyValue = request('keyValue');
var strServiceStartDate = request('serviceStartDate');
var strServiceEndDate = request('serviceEndDate');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            //$('#Status').mkDataItemSelect({ code: 'ContractStatus' });
            $('#Status').mkselect({ data: [{ id: 2, text: "合同续约" }, { id: 3, text: "合同终止" }], value: 'id', text: 'text', title: 'text' });
            
            $('#Status').change(function () {
                var status = $('[data-table="MeioErpContractRecord"]').mkGetFormData().Status;
                $('#ServiceStartDate').val(strServiceStartDate.split(' ')[0]);
                //if (status == 2) {
                //    $('#ServiceStartDate').val(strServiceEndDate.split(' ')[0]);
                //} else if (status == 3) {
                //    var date = new Date();
                //    var year = date.getFullYear();
                //    var month = date.getMonth() + 1;
                //    var day = date.getDate();
                //    var dateString = year + "-" + (month < 10 ? '0' + month : month) + "-" + (day < 10 ? '0' + day : day);
                //    $('#ServiceStartDate').val(dateString);
                //}
            });
        }
    };
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }

        var formdata = $('[data-table="MeioErpContractRecord"]').mkGetFormData();

        var dateStart = new Date(formdata.ServiceStartDate.split(' ')[0]);

        var dateEnd = new Date(formdata.ServiceEndDate.split(' ')[0]);

        console.log("dateStart:" + dateStart);
        console.log("dateEnd:" + dateEnd);


        if (dateStart >= dateEnd) {
            Changjie.alert.warning("服务截止时间必须大于服务起始时间");
            return false;
        }
        
        var serviceEndDate = new Date(strServiceEndDate);
        var serviceStartDate = new Date(strServiceStartDate);

        console.log("dateStart1:" + serviceStartDate);
        console.log("dateEnd1:" + serviceEndDate);

        if (formdata.Status == 3) {
            if (dateEnd > serviceEndDate || dateEnd < serviceStartDate) {
                Changjie.alert.warning("服务截止时间必须在合同服务生效时间内");
                return false;
            }
        }

        formdata.keyValue = keyValue;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/Contract/ContractProcessing',
            formdata,
            function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
};
