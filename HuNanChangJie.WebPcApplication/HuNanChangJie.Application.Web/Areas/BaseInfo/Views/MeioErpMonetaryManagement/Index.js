﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2025-03-10 15:52
 * 描  述：货币管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                refreshGirdData();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/MeioErpMonetaryManagement/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpMonetaryManagement/GetPageList',
                headData: [
                    { label: "货币编码", name: "CurrencyCode", width: 100, align: "left"},
                    { label: "货币名称", name: "CurrencyName", width: 100, align: "left"},
                    { label: "货币英文名称", name: "CurrencyNameEN", width: 100, align: "left"},
                    { label: "左标识符", name: "LeftIdentifier", width: 100, align: "left" },
                    { label: "更新人", name: "ModificationName", width: 100, align: "left" },
                    { label: "更新时间", name: "ModificationDate", width: 200, align: "left" },
                    {
                        label: "操作", name: "CurrencyCode", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback("<a style='color:blue' onclick=\"openView('" + row['ID'] + "')\" >编辑</a>&nbsp;<a style='color:blue' onclick=\"toDelete('" + row['ID'] + "')\" >删除</a>")
                        }
                    },
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search({
            CurrencyCode: $("#CurrencyCode").val()
        });
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpMonetaryManagement/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpMonetaryManagement/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
};

var openView = function (v) {
    var keyValue = v;
    if (keyValue) {
        top.Changjie.layerForm({
            id: 'view',
            title: "编辑",
            isShowConfirmBtn: true,
            url: top.$.rootUrl + '/BaseInfo/MeioErpMonetaryManagement/Form?keyValue=' + keyValue,
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }

}

var toDelete = function (v) {
    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
        if (res) {
            top.Changjie.loading(true, '正在删除...');
            top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/MeioErpMonetaryManagement/DeleteForm', { keyValue: v }, function (data) {
                top.Changjie.loading(false);
                if (data.code == 200) {
                    refreshGirdData();
                    top.Changjie.alert.success(data.info);
                }
                else
                    top.Changjie.alert.error(data.info);
            })
        }
    })
}