﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-07-19 10:01
 * 描  述：客户服务信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#OrderType').mkDataItemSelect({ code: 'OrderType' });
            $('#State').mkDataItemSelect({ code: 'CustomerServiceState' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "OrderCode": $("#OrderCode").val(),
                    "OrderType": $("#OrderType").mkselectGet(),
                    "State": $("#State").mkselectGet()
                });
            });
            $('#push').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要发送的客户服务");
                    return false;
                }
                
                var serviceIDList = [];
                selectedRow.forEach(function (row) {
                    if (row.State == "0") {
                        serviceIDList.push(row.ID)
                    }
                });
                if (serviceIDList.length == 0) {
                    top.Changjie.alert.error("已发送的客户服务，不能再发送");
                    return false;
                }
                Changjie.loading(true, '正在发送...');
                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/MeioErpCustomerService/PushCustomerService', { keyValue: serviceIDList }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("发送成功");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpCustomerService/GetPageList',
                headData: [
                    {
                        label: "订单号", name: "OrderCode", width: 150, align: "left", formatter: function (v, row) {
                            return "<a style='color:blue' onclick=\"openCustomerServiceEdit('" + row['ID'] + "')\" >" + v + "</a>"
                        }
                    },
                    { label: "订单类型", name: "OrderType", width: 150, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'OrderType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "仓库", name: "WarehouseID", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'WarehouseList',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['name']);
                                 }
                             });
                        }},
                    { label: "货主", name: "OwnerID", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['title']);
                                 }
                             });
                        }},
                    { label: "状态", name: "State", width: 150, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'CustomerServiceState',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    { label: "创建时间", name: "CreationDate", width: 150, align: "left", }
                ],
                mainId:'ID',
                isPage: true,
                isMultiselect: true,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpCustomerService/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpCustomerService/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
};

var openCustomerServiceEdit = function (id) {
    top.Changjie.layerForm({
        id: 'form',
        title: "客户服务明细信息",
        isShowConfirmBtn: true,
        url: top.$.rootUrl + '/BaseInfo/MeioErpCustomerService/Form?keyValue=' + id,
        width: 1000,
        height: 600,
        callBack: function (id) {
            return top[id].acceptClick(refreshGirdData);
        }
    });
};
