﻿var acceptClick;
var archivesType = request('archivesType');
var supplierID = request('supplierID');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $("#btn_Search").on("click", function () {
                page.search();
            })
            $("#btn_Reset").on("click", function () {
                $("#Code").val("")
                $("#Name").val("")
                page.search();
            })

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/Goods/GetPageList',
                headData: [
                    {
                        label: "供应商", name: "SupplierID", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'Meio_Supplier',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['title']);
                                }
                            });
                        }
                    },
                    {
                        label: "图片", name: "ImageUrl", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            if (value != "" && value!=null)
                                callback('<img style="width:100%;height:100%" src="' + value + '">')
                            else
                                callback('')
                        }
                    },
                    { label: "货品编码", name: "Code", width: 200, align: "left" },
                    { label: "货品名称", name: "Name", width: 300, align: "left" },

                ],
                mainId: 'ID',
                isPage: true,
                height: 400,
                isMultiselect: true
            });
            page.search();

        },
        search: function (param) {
            param = param || {};
            param.Enabled = true;
            param.SupplierID = supplierID;
            param.IsPurchaseGoodsSearch = 1;

            if ($("#Code").val() != "") {
                param.Code = $("#Code").val()
            }
            if ($("#Name").val() != "") {
                param.Name = $("#Name").val()
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};