﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-07-03 10:46
 * 描  述：采购单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpPurchase";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/MeioErpPurchase/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'MeioErpPurchaseDetail', "gridId": 'MeioErpPurchaseDetail' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'PurchaseCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList', value: 'id', text: 'name' });
            $('#CompanyID').mkDataItemSelect({ code: 'PurchaseCompany' });
            $('#SupplierID').mkDataSourceSelect({ code: 'Meio_Supplier', value: 'id', text: 'title' });
            $('#PurchasePersonID').mkDataSourceSelect({ code: 'User', value: 'f_userid', text: 'f_realname' });
            $('#Type').mkDataItemSelect({ code: 'PurchaseContractType' });
            $('#MeioErpPurchaseDetail').jfGrid({
                headData: [
                    {
                        label: '计划单号', name: 'PurchasePlanCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '商品图片url', name: 'GoodsImageUrl', width: 100,
                        formatterAsync: function (callback, value, row, op, $cell) {
                            callback('<img style="width:100%;height:100%" src="' + value + '">')
                        }
                    },
                    {
                        label: '商品编码', name: 'GoodsCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '商品名称', name: 'GoodsName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '采购数量', name: 'PurchaseQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'int', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '含税单价CNY', name: 'UnitPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '已发货数量', name: 'DeliveryQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    },
                    {
                        label: '入库数量', name: 'InStorageQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: ''
                    }
                ],
                mainId: "ID",
                bindTable: "MeioErpPurchaseDetail",
                isEdit: false,
                height: 300,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //     "EditType": 1,
                //}],
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });

            $('#add').on('click', function () {
                var supplierId = $("#SupplierID").mkselectGet();
                if (supplierId == '') {
                    top.Changjie.alert.error("请选择供应商后，再添加货品");
                    return false;
                }

                Changjie.layerForm({
                    id: "selectGoods",
                    width: 800,
                    height: 450,
                    title: "选择货品信息",
                    url: top.$.rootUrl + "/BaseInfo/MeioErpPurchase/Dialog?supplierID=" + supplierId,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                var rows = $('#MeioErpPurchaseDetail').jfGridGet('rowdatas');
                                for (var i = 0; i < data.length; i++) {
                                    var isEx = false;
                                    rows.forEach(function (row) {
                                        if (row.GoodsCode == data[i].Code) {
                                            isEx = true;
                                        }
                                    });
                                    if (!isEx) {
                                        var row = {};
                                        row.GoodsImageUrl = data[i].ImageUrl;
                                        row.GoodsCode = data[i].Code;
                                        row.GoodsName = data[i].Name;
                                        rows.push(row);
                                    }
                                }
                                $("#MeioErpPurchaseDetail").jfGridSet("refreshdata", rows);

                            }
                        });
                    }
                });
            });
            $('#delete').on('click', function () {
                var selectRow = $('#MeioErpPurchaseDetail').jfGridGet('rowdatas');
                selectRow.forEach(function (row) {
                    $('#MeioErpPurchaseDetail').jfGridSet('removeRow', row.ID);
                });
            });

            $('#SupplierID').change(function () {
                $("#MeioErpPurchaseDetail").jfGridSet("refreshdata", []);
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/MeioErpPurchase/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpPurchase/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    if ($('#MeioErpPurchaseDetail').jfGridGet('rowdatas').length == 0) {
        top.Changjie.alert.error("采购商品明细不能为空");
        return false;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="MeioErpPurchase"]').mkGetFormData());
    postData.strmeioErpPurchaseDetailList = JSON.stringify($('#MeioErpPurchaseDetail').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
