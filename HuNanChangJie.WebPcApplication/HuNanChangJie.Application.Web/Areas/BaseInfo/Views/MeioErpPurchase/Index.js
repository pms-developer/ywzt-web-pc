﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-07-03 10:46
 * 描  述：采购单
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#Type').mkDataItemSelect({ code: 'PurchaseContractType' });
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList', value: 'id', text: 'name' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "Code": $("#Code").val(),
                    "WarehouseID": $("#WarehouseID").mkselectGet(),
                    "Type": $("#Type").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/MeioErpPurchase/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

            //审核
            $('#btn_Audit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                var auditStatus = $('#gridtable').jfGridValue('AuditStatus');
                if (auditStatus!=null && auditStatus != "0" && auditStatus != "") {
                    top.Changjie.alert.warning("审核失败，采购单已审核");
                    return false;
                } 
                if (top.Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'audit',
                        title: '审批',
                        url: top.$.rootUrl + '/BaseInfo/MeioErpPurchase/Audit?keyValue=' + keyValue,
                        width: 300,
                        height: 300,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            //反审核
            $("#btn_Unaudit").on("click", function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                var auditStatus = $('#gridtable').jfGridValue('AuditStatus');
                if (auditStatus==null || auditStatus == "0" || auditStatus=="") {
                    top.Changjie.alert.warning("采购单为待审核状态，无法反审核操作");
                    return false;
                }

                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认反审核该项！', function (res) {
                        var param = {};
                        param['__RequestVerificationToken'] = $.mkToken;
                        param['keyValue'] = keyValue;
                        top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/MeioErpPurchase/UnAuditPurchase',
                            param,
                            function (res) {
                                if (res.code != 200) {
                                    top.Changjie.alert.error(res.info);
                                }
                                refreshGirdData();
                            });

                    });

                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpPurchase/GetPageList',
                headData: [
                    { label: "采购单号", name: "Code", width: 150, align: "left" },
                    { label: "采购员", name: "PurchasePersonID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'User',
                                 key: value,
                                 keyId: 'f_userid',
                                 callback: function (_data) {
                                     callback(_data['f_realname']);
                                 }
                             });
                        }},
                    { label: "采购仓库", name: "WarehouseID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'WarehouseList',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['name']);
                                 }
                             });
                        }},
                    { label: "供应商", name: "SupplierID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'Meio_Supplier',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['title']);
                                 }
                             });
                        }
                    },
                    { label: "合计金额", name: "TotalAmount", width: 100, align: "left", },
                    {
                        label: "采购类型", name: "Type", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'PurchaseContractType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "单据状态", name: "State", width: 100, align: "left", formatter: function (value, row) {
                            if (value == 1)
                                return "已关闭"
                            else
                                return "未关闭"
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left", formatter: function (value, row) {
                            if (value == "1")
                                return "已通过"
                            else if (value=="2")
                                return "已拒绝"
                            else
                                return "未审核"
                        }
                    },
                    { label: "审核人", name: "AuditorName", width: 100, align: "left", },
                    { label: "审核时间", name: "AuditTime", width: 100, align: "left", },
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left" },
                    { label: "备注", name: "Remark", width: 100, align: "left" },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "采购单信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpPurchase/Form?keyValue=' + row.ID,
                        width: 800,
                        height: 600
                    });
                },
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpPurchase/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpPurchase/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
