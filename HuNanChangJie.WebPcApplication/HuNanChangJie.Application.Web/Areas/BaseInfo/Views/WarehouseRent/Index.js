﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-05-09 18:11
 * 描  述：仓库租金档案信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#IsValid').mkselect({ data: [{ value: true, text: "是" }, { value: false, text: "否" }], value: 'value', text: 'text', title: 'text' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "ItemName": $("#ItemName").val(),
                    "IsValid": $("#IsValid").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/WarehouseRent/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/WarehouseRent/GetPageList',
                headData: [
                    { label: "仓库租金档案名称", name: "ItemName", width: 300, align: "left"},
                    { label: "报价单类型", name: "Type", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'BJDLX',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "单位", name: "Unit", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'DW',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "是否有效", name: "IsValid", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'SZSF',
                                 callback: function (_data) {
                                     callback(!value ? "否" : _data.text);
                                 }
                             });
                        }},
                    { label: "备注", name: "Remark", width: 100, align: "left"},
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "仓库租金档案信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/WarehouseRent/Form?keyValue=' + row.ID ,
                        width: 800,
                        height: 600
                    });
                },
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    var isEdit = top.Changjie.httpGet(top.$.rootUrl + "/BaseInfo/PriceSheet/IsEditForm?itemId=" + keyValue).data;
                    if (!isEdit) {
                        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                            if (res) {
                                top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/WarehouseRent/DeleteForm', { keyValue: keyValue }, function () {
                                    refreshGirdData();
                                });
                            }
                        });
                    } else {
                        top.Changjie.alert.warning("不能删除该档案信息，报价单已经使用该档案信息");
                    }
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    var isEdit = top.Changjie.httpGet(top.$.rootUrl + "/BaseInfo/PriceSheet/IsEditForm?itemId=" + keyValue).data;
                    if (!isEdit) {
                        top.Changjie.layerForm({
                            id: 'form',
                            title: title,
                            isShowConfirmBtn: isShowConfirmBtn,
                            url: top.$.rootUrl + '/BaseInfo/WarehouseRent/Form?keyValue=' + keyValue + '&viewState=' + viewState,
                            width: 800,
                            height: 600,
                            callBack: function (id) {
                                return top[id].acceptClick(refreshGirdData);
                            }
                        });
                    } else {
                        top.Changjie.alert.warning("不能编辑该档案信息，报价单已经使用该档案信息");
                    }
                }
            };
