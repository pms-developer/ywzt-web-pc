﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-09 18:11
 * 描  述：仓库租金档案信息
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpWarehouseRent";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/WarehouseRent/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var isName = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'MeioErpWarehouseRentDetail', "gridId": 'MeioErpWarehouseRentDetail' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $('#Type').mkDataItemSelect({ code: 'BJDLX' });
            $('#Unit').mkDataItemSelect({ code: 'DW' });
            $('#IsValid').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#MeioErpWarehouseRentDetail').jfGrid({
                headData: [
                    {
                        label: '最小值', name: 'MinValue', width: 100
                    },
                    {
                        label: '最大值', name: 'MaxValue', width: 100
                    },
                    {
                        label: '说明', name: 'ItemName', width: 300, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: ''
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "MeioErpWarehouseRentDetail",
                isEdit: true,
                height: 300,
                showadd: false,
                //rowdatas:[
                //{
                //     "SortCode": 1,
                //     "rowState": 1,
                //     "ID": Changjie.newGuid(),
                //        "EditType": 1,
                //        "MinValue": 1
                //}],
                onAddRow: function (row, rows) {
                },
                onMinusRow: function (row, rows, headData) {
                }
            });

            $("#ItemName").on("blur", function () {
                var value = $(this).val();
                isName = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/WarehouseRent/CheckIsItemName?name=" + value + "&id=" + keyValue).data;
                if (isName) {
                    Changjie.alert.error("仓库租金档案名称已存在，请更换名称");
                }
            });
            $("#addWarehouseRent").on("click", function () {
                var rows = $("#MeioErpWarehouseRentDetail").jfGridGet("rowdatas");
                var strMaxValue = $("#MaxValue").val();
                if (strMaxValue == "") {
                    top.Changjie.alert.warning("最大值不能为空");
                } else {
                    if (!isInteger(strMaxValue)) {
                        top.Changjie.alert.warning("最大值必须是整数");
                        $("#MaxValue").val("");
                        $("#MaxValue").focus();
                        return false;
                    }
                    if (parseInt(strMaxValue) <= 0 || parseInt(strMaxValue) > 2147483647) {
                        top.Changjie.alert.warning("最大值必须大于0，且小于2147483647");
                    } else {
                        var maxValue = 0;
                        rows.forEach(function (itemrow) {
                            if (parseInt(itemrow.MaxValue) > maxValue)
                                maxValue = parseInt(itemrow.MaxValue);
                        });
                        if (parseInt(strMaxValue) <= maxValue) {
                            top.Changjie.alert.warning("填写的最大值必须大于当前列表中的最大值");
                        } else {
                            var row = {};
                            row.ID = top.Changjie.newGuid();
                            row.MinValue = maxValue + 1;
                            row.MaxValue = strMaxValue;
                            row.ItemName = "";
                            rows.push(row);
                            $("#MeioErpWarehouseRentDetail").jfGridSet("refreshdata", rows);
                        }
                    }
                }
                $("#MaxValue").val("");
                $("#MaxValue").focus();
            });

            $("#deleteWarehouseRent").on("click", function () {
                var rows = $('#MeioErpWarehouseRentDetail').jfGridGet('rowdatas');
                if (rows.length > 0) {
                    $('#MeioErpWarehouseRentDetail').jfGridSet('removeRow', rows[rows.length - 1].ID);
                }
               
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/WarehouseRent/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;

        if (isName) {
            Changjie.alert.error("仓库租金档案名称已存在，请更换名称");
            return false;
        }

        var verify = true;
        var detailRowdatas = $('#MeioErpWarehouseRentDetail').jfGridGet('rowdatas');
        detailRowdatas.forEach(function (row) {
            if (parseInt(row.MinValue) >= parseInt(row.MaxValue)) {
                verify = false;
            }
        });
        if (!verify) {
            Changjie.alert.warning("最大值不能小于最小值");
            return false;
        }
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/WarehouseRent/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="MeioErpWarehouseRent"]').mkGetFormData());
    postData.strmeioErpWarehouseRentDetailList = JSON.stringify($('#MeioErpWarehouseRentDetail').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}

function isInteger(str) {
    return /^\d+$/.test(str);
}
