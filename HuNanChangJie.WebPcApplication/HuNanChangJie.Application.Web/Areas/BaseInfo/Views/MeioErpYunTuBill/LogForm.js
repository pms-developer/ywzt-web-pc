﻿var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $("#btn_Search").on("click", function () {
                page.search();
            })
            $("#btn_Reset").on("click", function () {
                $("#WaybillNumber").val("")
                page.search();
            })

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/GetLog',
                headData: [
                    { label: "运单号", name: "keyValue", width: 150, align: "left" },
                    { label: "操作人", name: "operUser", width: 100, align: "left" },
                    { label: "操作时间", name: "operTime", width: 150, align: "left" },
                    { label: "操作内容", name: "operLog", width: 200, align: "left" },
                    { label: "操作类型", name: "operType", width: 100, align: "left" },
                ],
                mainId: 'ID',
                isPage: false,
                height: 500
            });
            page.search();

        },
        search: function (param) {
            param = param || {};
            param.Enabled = true;
            if ($("#WaybillNumber").val() != "") {
                param.WaybillNumber = $("#WaybillNumber").val()
            }

            $('#gridtable').jfGridSet('reload', { WaybillNumber: $("#WaybillNumber").val() });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};