﻿var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
          
        }
    };
    acceptClick = function (callBack) {
        var remark = $('#Remark').val();
        var formdata = {};
        formdata.keyValue = keyValue;
        formdata.remark = remark;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/BatchRemark',
            formdata,
            function (res) {
                // 保存成功后才回调
                if (!!callBack) {
                    callBack();
                }
            });
    };
    page.init();
};