﻿var acceptClick;
var keyValue = request('keyValue');
var remark = request('remark');
var amount = request('amount');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $('#Remark').val(remark);
        }
    };
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var adjustmentAmount = $('#AdjustmentAmount').val();
        //var isValid = /^[-\d]*$/.test(adjustmentAmount); // 检查是否只包含数字和负号（如果有）
        var isValid = /^(-?\d+)(\.\d+)?$/.test(adjustmentAmount);
        if (!isValid) {
            top.Changjie.alert.error("调整金额必须为数字");
            return false;
        } 

        var remark = $('#Remark').val();
        var formdata = {};
        formdata.keyValue = keyValue;
        formdata.amount = adjustmentAmount;
        formdata.remark = remark;
        formdata.oldAmount = amount;
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/AdjustmentAmount',
            formdata,
            function (res) {
                // 保存成功后才回调
                if (!!callBack) {
                    callBack();
                }
            });
    };
    page.init();
};