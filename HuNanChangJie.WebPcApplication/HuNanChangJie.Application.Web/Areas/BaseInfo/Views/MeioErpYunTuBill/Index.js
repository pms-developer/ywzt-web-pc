﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2025-02-19 11:00
 * 描  述：云途账单
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var startTime1;
    var endTime1;
    var waybillState;
    var warehouseID;
    var ownerCode;
    var waybillNumber;
    var auditStatus;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '0',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    //page.search();
                }
            });
            $('#datesearch1').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime1 = begin;
                    endTime1 = end;
                    //page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#WaybillState').mkDataItemSelect({ code: 'YunTuWaybillState1' });
            $('#WarehouseID').mkDataSourceSelect({ code: 'WarehouseList', value: 'id', text: 'name' });
            $('#OwnerCode').mkDataSourceSelect({ code: 'OwnerList', value: 'code', text: 'title' });
            $('#AuditStatus').mkDataItemSelect({ code: 'YunTuBillAuditStatus' });
            // 查询
            $('#query').on('click', function () {
                //page.search({
                //    "WaybillState": $("#WaybillState").mkselectGet(),
                //    "WarehouseID": $("#WarehouseID").mkselectGet(),
                //    "OwnerCode": $("#OwnerCode").mkselectGet(),
                //    "WaybillNumber": $("#WaybillNumber").val(),
                //    "AuditStatus": $("#AuditStatus").mkselectGet()
                //});
                page.search();
            });
            $('#WaybillState').change(function () {
                waybillState = $('#WaybillState').mkselectGet();
            });
            $('#WarehouseID').change(function () {
                warehouseID = $('#WarehouseID').mkselectGet();
            });
            $('#OwnerCode').change(function () {
                ownerCode = $('#OwnerCode').mkselectGet();
            });
            $('#AuditStatus').change(function () {
                auditStatus = $('#AuditStatus').mkselectGet();
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            //审核
            $('#audit1').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要审核的账单");
                    return false;
                }
                Changjie.loading(true, '正在审核...');
                var ids = "";
                selectedRow.forEach(function (row) {
                    ids += "," + row.ID;
                });

                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/Audit', { keyValue: ids.substring(1), auditStatus: 1 }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("审核成功");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });
            //反审核
            $('#unaudit1').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要反审核的账单");
                    return false;
                }
                Changjie.loading(true, '正在反审核...');
                var ids = "";
                selectedRow.forEach(function (row) {
                    ids += "," + row.ID;
                });

                top.Changjie.httpPost(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/Audit', { keyValue: ids.substring(1), auditStatus: 0 }, function (data) {
                    Changjie.loading(false);
                    if (data.code == 200) {
                        refreshGirdData();
                        top.Changjie.alert.success("反审核成功");
                    }
                    else
                        top.Changjie.alert.error(data.info);
                });
            });
            //批量备注
            $('#batchReamrk').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要备注的账单");
                    return false;
                }
                var ids = "";
                selectedRow.forEach(function (row) {
                    ids += "," + row.ID;
                });
                top.Changjie.layerForm({
                    id: 'form',
                    title: "备注",
                    isShowConfirmBtn: true,
                    url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/RemarkForm?keyValue=' + ids.substring(1),
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

            $('#export1').on('click', function () {
                var selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (selectedRow.length == 0) {
                    top.Changjie.alert.error("请选择需要导出的账单");
                    return false;
                }
                var ids = "";
                var iserror = false;
                selectedRow.forEach(function (row) {
                    if (row.AuditStatus == "1") {
                        ids += "," + row.ID;
                    } else {
                        iserror = true;
                    }
                });
                if (iserror) {
                    top.Changjie.alert.error("未审核的费用不能导出账单");
                    return false;
                }

                $('#export').click()
            });
            $('#log').on('click', function () {
                Changjie.layerForm({
                    id: 'formLog',
                    title: '操作日志',
                    isShowConfirmBtn: false,
                    url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/LogForm',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/GetPageList',
                headData: [
                    { label: "运单号", name: "WaybillNumber", width: 100, align: "left" },
                    { label: "运单状态", name: "WaybillState", width: 100, align: "left" },
                    { label: "订单号", name: "OrderCode", width: 100, align: "left" },
                    { label: "货主名称", name: "OwnerName", width: 100, align: "left" },
                    { label: "仓库名称", name: "WarehouseName", width: 100, align: "left" },
                    { label: "发货时间", name: "DeliveryTime", width: 100, align: "left" },
                    { label: "仓库称重", name: "NetWeight", width: 100, align: "left" },
                    { label: "计费重量", name: "ChargeableWeight", width: 100, align: "left" },
                    { label: "目的地", name: "DestinationCountry", width: 100, align: "left" },
                    { label: "品名", name: "ProductName", width: 100, align: "left" },
                    { label: "使用报价规则", name: "RuleName", width: 100, align: "left" },
                    { label: "运费", name: "Freight", width: 100, align: "left" },
                    { label: "挂号费", name: "RegistrationFee", width: 100, align: "left" },
                    { label: "手续费", name: "HandlingFee", width: 100, align: "left" },
                    { label: "代收税费", name: "TotalFee", width: 100, align: "left" },
                    {
                        label: "调整金额", name: "AdjustmentAmount", width: 100, align: "left",
                        formatter: function (v, row) {
                            if (row.AuditStatus == 0) {
                                return "<a style='color:blue' onclick=\"AdjustmentAmount('" + row['ID'] + "','" + (row.Remark == null ? "" : row.Remark) + "','" + row.AdjustmentAmount + "')\" >" + row.AdjustmentAmount + "</a > "
                            } else {
                                return row.AdjustmentAmount;
                            }
                        }
                    },
                    { label: "总金额", name: "TotalAmount", width: 100, align: "left" },
                    { label: "账单生成时间", name: "CreationDate", width: 100, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (value) {
                            if (value == "1")
                                return "已审核";
                            else
                                return "未审核";
                        }
                    },
                    { label: "审核人", name: "AuditorName", width: 100, align: "left" },
                    { label: "审核时间", name: "AuditTime", width: 100, align: "left" },
                    { label: "备注", name: "Remark", width: 100, align: "left" },
                ],
                mainId: 'ID',
                isPage: true,
                isMultiselect: true,
                rows: 500,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            param.FristSearchPage = 0;
            param.CreateStartTime = startTime1;
            param.CreateEndTime = endTime1;
            param.WaybillState = waybillState;
            param.WarehouseID = warehouseID;
            param.OwnerCode = ownerCode;
            param.WaybillNumber = $("#WaybillNumber").val();
            param.AuditStatus = auditStatus;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/Form?keyValue=' + keyValue + '&viewState=' + viewState,
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};

var AdjustmentAmount = function (id, remark, amount) {
    top.Changjie.layerForm({
        id: 'form23',
        title: "调整金额",
        isShowConfirmBtn: true,
        url: top.$.rootUrl + '/BaseInfo/MeioErpYunTuBill/AdjustmentAmountForm?keyValue=' + id + '&remark=' + remark + '&amount=' + amount,
        width: 400,
        height: 300,
        callBack: function (id) {
            return top[id].acceptClick(refreshGirdData);
        }
    });
}
