﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-05-24 17:47
 * 描  述：仓库管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#Enabled').mkselect({ data: [{ value: true, text: "启用" }, { value: false, text: "禁用" }], value: 'value', text: 'text', title: 'text' });
            $('#Type').mkDataItemSelect({ code: 'WarehouseType' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "Code": $("#Code").val(),
                    "Name": $("#Name").val(),
                    "Enabled": $("#Enabled").mkselectGet(),
                    "Type": $("#Type").mkselectGet()
                });
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/BaseInfo/Warehouse/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

            //  启用
            $('#btn_enable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认启用选中仓库！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Warehouse/SetIsValid', { ids: keyValue, isvalid: true }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            //  禁用
            $('#btn_disable').on('click', function () {
                //var selectRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    //var ids = "";
                    //selectRow.forEach(function (row) {
                    //    ids += "," + row.ID;
                    //});

                    top.Changjie.layerConfirm('是否确认禁用选中仓库！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Warehouse/SetIsValid', { ids: keyValue, isvalid: false }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/Warehouse/GetPageList',
                headData: [
                    { label: "仓库编码", name: "Code", width: 100, align: "left"},
                    { label: "仓库名称", name: "Name", width: 100, align: "left" },
                    {
                        label: "仓库类型", name: "Type", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'WarehouseType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "仓库属性", name: "Attribute", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'WarehouseAttribute',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "第三方系统", name: "ServiceProvider", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ServiceProvider',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "负责人", name: "PersonCharge", width: 100, align: "left" },
                    { label: "联系电话", name: "Telphone", width: 100, align: "left"},
                    { label: "仓库地址", name: "Addrss", width: 100, align: "left" },
                    {
                        label: "启用状态", name: "IsValid", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'SZSF',
                                callback: function (_data) {
                                    callback(_data.text ? '启用' : '禁用');
                                }
                            });
                        }
                    },
                    { label: "创建用户", name: "CreatedBy", width: 100, align: "left" },
                    { label: "创建时间", name: "CreatedDate", width: 100, align: "left" },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "仓库信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/Warehouse/Form?keyValue=' + row.ID,
                        width: 800,
                        height: 600
                    });
                },
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/Warehouse/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/Warehouse/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
