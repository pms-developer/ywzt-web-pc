﻿/* *  
 * 创建人：超级管理员
 * 日  期：2024-05-24 17:47
 * 描  述：仓库管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "MeioErpWarehouse";
var processCommitUrl = top.$.rootUrl + '/BaseInfo/Warehouse/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var isName = false;
var isCode = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $('#Type').mkDataItemSelect({ code: 'WarehouseType' });
            $('#Country').mkDataItemSelect({ code: 'Country' });
            $('#Attribute').mkDataItemSelect({ code: 'WarehouseAttribute' });
            $('#ServiceProvider').mkDataItemSelect({ code: 'ServiceProvider' });
            $("#Name").on("blur", function () {
                var value = $(this).val();
                isName = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Warehouse/CheckIsWarehouseName?name=" + value + "&id=" + keyValue).data;
                if (isName) {
                    Changjie.alert.error("仓库名称已存在，请更换名称");
                }
            });
            $("#Code").on("blur", function () {
                var value = $(this).val();
                isCode = Changjie.httpGet(top.$.rootUrl + "/BaseInfo/Warehouse/CheckIsCode?code=" + value + "&id=" + keyValue).data;
                if (isCode) {
                    Changjie.alert.error("仓库编码已存在");
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/BaseInfo/Warehouse/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;

        if (isName) {
            Changjie.alert.error("仓库名称已存在，请更换名称");
            return false;
        }

        if (isCode) {
            Changjie.alert.error("仓库编码已存在");
            return false;
        }
        $.mkSaveForm(top.$.rootUrl + '/BaseInfo/Warehouse/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
