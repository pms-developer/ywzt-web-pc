﻿jQuery.extend({
    createUploadIframe: function (id, mytype) {
        var ajid = "jUploadFrame" + id;
        var c = '<iframe id="' + ajid + '" name="' + ajid + '" style="position:absolute; top:-9999px; left:-9999px"';
        if (window.ActiveXObject) {
            if (typeof mytype == "boolean") {
                c += ' src="javascript:false"';
            } else {
                if (typeof mytype == "string") {
                    c += ' src="' + mytype + '"';
                }
            }
        } c += " />";
        jQuery(c).appendTo(document.body);
        return jQuery("#" + ajid).get(0);
    },
    createUploadForm: function (gid, eid, inputs) {
        var ejformid = "jUploadForm" + gid;
        var cjfileid = "jUploadFile" + gid;
        var dpostElement = jQuery('<form  action="" method="POST" name="' + ejformid + '" id="' + ejformid + '" enctype="multipart/form-data"></form>');
        if (inputs) {
            for (var f in inputs) {
                jQuery('<input type="hidden" name="' + f + '" value="' + inputs[f].replace(/"([^"]*)"/g, "'$1'") + '" />').appendTo(dpostElement);
            }
        } var j = jQuery("#" + eid);
        var h = jQuery(j).clone();
        jQuery(j).attr("id", cjfileid);
        jQuery(j).before(h);
        jQuery(j).appendTo(dpostElement);
        jQuery(dpostElement).css("position", "absolute");
        jQuery(dpostElement).css("top", "-1200px");
        jQuery(dpostElement).css("left", "-1200px");
        jQuery(dpostElement).appendTo("body");
        return dpostElement;
    },
    ajaxFileUpload: function (i) {
        i = jQuery.extend({}, jQuery.ajaxSettings, i);
        var f = new Date().getTime();
        var b = jQuery.createUploadForm(f, i.fileElementId, (typeof (i.data) == "undefined" ? false : i.data));
        var g = jQuery.createUploadIframe(f, i.secureuri);
        var d = "jUploadFrame" + f;
        var c = "jUploadForm" + f;
        if (i.global && !jQuery.active++) {
            jQuery.event.trigger("ajaxStart");
        } var h = false;
        var k = {};
        if (i.global) {
            jQuery.event.trigger("ajaxSend", [k, i]);
        } var j = function (o) {
            var n = document.getElementById(d);
            try {
                if (n.contentWindow) {
                    k.responseText = n.contentWindow.document.body ? n.contentWindow.document.body.innerHTML : null;
                    k.responseXML = n.contentWindow.document.XMLDocument ? n.contentWindow.document.XMLDocument : n.contentWindow.document
                } else {
                    if (n.contentDocument) {
                        k.responseText = n.contentDocument.document.body ? n.contentDocument.document.body.innerHTML : null;
                        k.responseXML = n.contentDocument.document.XMLDocument ? n.contentDocument.document.XMLDocument : n.contentDocument.document
                    }
                }
            } catch (m) {
                //jQuery.handleError(i, k, null, m);
            } if (k || o == "timeout") {
                h = true;
                var p;
                try {
                    p = o != "timeout" ? "success" : "error";
                    if (p != "error") {
                        var l = jQuery.uploadHttpData(k, i.dataType);
                        if (i.success) {
                            i.success(l, p);
                        } if (i.global) {
                            jQuery.event.trigger("ajaxSuccess", [k, i]);
                        }
                    } else {
                        //jQuery.handleError(i, k, p);
                    }
                } catch (m) {
                    p = "error";
                    //jQuery.handleError(i, k, p, m);
                } if (i.global) {
                    jQuery.event.trigger("ajaxComplete", [k, i]);
                } if (i.global && !--jQuery.active) {
                    jQuery.event.trigger("ajaxStop");
                } if (i.complete) {
                    i.complete(k, p);
                } jQuery(n).unbind();
                setTimeout(function () {
                    try {
                        jQuery(n).remove();
                        jQuery(b).remove();
                    } catch (q) {
                        //jQuery.handleError(i, k, null, q);
                    }
                }, 100);
                k = null;
            }
        };
        if (i.timeout > 0) {
            setTimeout(function () {
                if (!h) {
                    j("timeout");
                }
            },
                i.timeout);
        } try {
            var b = jQuery("#" + c);
            jQuery(b).attr("action", i.url);
            jQuery(b).attr("method", "POST");
            jQuery(b).attr("target", d);
            if (b.encoding) {
                jQuery(b).attr("encoding", "multipart/form-data");
            } else {
                jQuery(b).attr("enctype", "multipart/form-data");
            }
            jQuery(b).submit();
        } catch (a) {
            //jQuery.handleError(i, k, null, a);
        } jQuery("#" + d).load(j);
        return { abort: function () { } }
    }, uploadHttpData: function (r, type) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        if (type == "script") {
            jQuery.globalEval(data);
        } if (type == "json") {
            eval("data = " + data);
        } if (type == "html") {
            jQuery("<div>").html(data).evalScripts();
        }
        return data;
    }
});
var acceptClick;
var keyValue = request('keyValue');
var code = request('code');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $("#Code").val(code);
        }
    };
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }

        var formdata = $('[data-table="MeioErpStatement"]').mkGetFormData();

        formdata.keyValue = keyValue;

        var e = document.getElementById("uploadFile").files[0];
        if (!!e) {
            //$.ajaxFileUpload({
            //    url: top.$.rootUrl + "/BaseInfo/MeioErpStatement/UploadFile?code=qrcode",
            //    secureuri: false,
            //    fileElementId: "uploadFile",
            //    dataType: "json",
            //    success: function (f) {
            //        if (f.code == 200) {
            //            formdata.CollectionImageUrl = f.info;
            //            console.log(formdata.CollectionImageUrl);
            //            $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpStatement/ConfirmCollection',
            //                formdata,
            //                function (res) {
            //                    // 保存成功后才回调
            //                    if (!!callBack) {
            //                        callBack();
            //                    }
            //                });
            //        }
            //    }
            //});
            const form = new FormData();
            form.append("file", e)
            Changjie.httpPostFile(top.$.rootUrl + '/BaseInfo/MeioErpStatement/UpLoadFile?code=' + $('#Code').val(), form, function (data) {
                Changjie.loading(false);
                let obj = JSON.parse(data)
                if (obj.code == 200) {
                    formdata.CollectionImageUrl = obj.info;
                    console.log(formdata.CollectionImageUrl);
                    $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpStatement/ConfirmCollection',
                        formdata,
                        function (res) {
                            // 保存成功后才回调
                            if (!!callBack) {
                                callBack();
                            }
                        });
                } else {
                    top.Changjie.alert.error(obj.info);
                }
            });
        } else {
            $.mkSaveForm(top.$.rootUrl + '/BaseInfo/MeioErpStatement/ConfirmCollection',
                formdata,
                function (res) {
                    // 保存成功后才回调
                    if (!!callBack) {
                        callBack();
                    }
                });
        }
    };
    page.init();
};
