﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-07-23 10:57
 * 描  述：结算单信息
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#IsInvoice').mkDataItemSelect({ code: 'BESF' });
            $('#OwnerID').mkDataSourceSelect({ code: 'OwnerList',value: 'id',text: 'title' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查询
            $('#query').on('click', function () {
                page.search({
                    "Code": $("#Code").val(),
                    "IsInvoice": $("#IsInvoice").mkselectGet(),
                    "OwnerID": $("#OwnerID").mkselectGet()
                });
            });
            //收款确认
            $('#btn_CollectionConfirm').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (keyValue == null || keyValue == "") {
                    top.Changjie.alert.error("请选择需要收款确认的结算单");
                    return false;
                }

                var code = $('#gridtable').jfGridValue('Code');
                if (top.Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'dialog',
                        title: '收款确认',
                        url: top.$.rootUrl + '/BaseInfo/MeioErpStatement/Dialog?keyValue=' + keyValue + "&code=" + code,
                        width: 500,
                        height: 300,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/BaseInfo/MeioErpStatement/GetPageList',
                headData: [
                    { label: "结算单号", name: "Code", width: 120, align: "left" },
                    { label: "结算方式", name: "StatementMethod", width: 100, align: "left" },
                    { label: "支付方式", name: "PayMethod", width: 100, align: "left" },
                    { label: "结算金额", name: "StatementAmount", width: 100, align: "left"},
                    { label: "币种", name: "Currency", width: 100, align: "left" },
                    //{ label: "货主", name: "OwnerID", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('custmerData', {
                    //             url: '/SystemModule/DataSource/GetDataTable?code=' + 'OwnerList',
                    //             key: value,
                    //             keyId: 'id',
                    //             callback: function (_data) {
                    //                 callback(_data['title']);
                    //             }
                    //         });
                    //    }},
                    { label: "货主编码", name: "OwnerCode", width: 100, align: "left" },
                    { label: "货主名称", name: "OwnerName", width: 100, align: "left" },
                    { label: "收款账户", name: "CollectionAccount", width: 100, align: "left" },
                    { label: "创建日期", name: "CreationDate", width: 100, align: "left" },
                    { label: "是否开票", name: "IsInvoice", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (value)
                                return "是";
                            else
                                return "否";
                        }
                    },
                    { label: "查账编号", name: "AuditNumber", width: 100, align: "left" },
                    { label: "收款确认", name: "IsCollectionConfirm", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (value)
                                return "已确认";
                            else
                                return "待确认";
                        }},
                    {
                        label: "收款截图", name: "CollectionImageUrl", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            if (value != "" && value != null) {
                                if (value.indexOf(".pdf") > -1) {
                                    callback('<a href="' + value + '">下载</a>')
                                } else {
                                    callback('<a href="' + value + '"><img style="width:100%;height:100%" src="' + value + '"></a>')
                                }
                            }
                            else
                                callback('')
                        }
                    },
                    { label: "确认人", name: "CollectionConfirmPeople", width: 100, align: "left" },
                    { label: "确认时间", name: "CollectionConfirmDate", width: 150, align: "left" },
                ],
                dblclick: function (row) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "结算单明细信息",
                        isShowConfirmBtn: false,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpStatement/Form?keyValue=' + row.ID,
                        width: 800,
                        height: 600
                    });
                },
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.FristSearchPage = 0;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/BaseInfo/MeioErpStatement/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/BaseInfo/MeioErpStatement/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
