﻿var acceptClick;
var keyValue = request("keyValue");
var bootstrap = function (a, b) {
    var c = {
        init: function () {
            c.bind();
            c.initData()
        },
        bind: function () {
            a("#F_ParentId").mkselect({
                url: top.$.rootUrl + "/SystemModule/Module/GetExpendModuleTree",
                type: "tree",
                maxHeight: 180,
                allowSearch: true
            });
            a("#selectIcon").on("click",
                function () {
                    b.layerForm({
                        id: "iconForm",
                        title: "选择图标",
                        url: top.$.rootUrl + "/Utility/Icon",
                        height: 700,
                        width: 1000,
                        btn: null,
                        maxmin: true,
                        end: function () {
                            if (top._learunSelectIcon != "") {
                                a("#F_Icon").val(top._learunSelectIcon)
                            }
                        }
                    })
                });
            a("#F_KanBanId").mkselect({
                text: "F_KanBanName",
                value: "F_Id",
                url: top.$.rootUrl + "/DisplayBoard/KBKanBanInfo/GetList?queryJson=null",
                allowSearch: true
            });
            a("#lr_preview").on("click",
                function () {
                    var d = a("#F_KanBanId").mkselectGet();
                    if (!!d) {
                        b.layerForm({
                            id: "custmerForm_PreviewForm",
                            title: "预览当前模板",
                            url: top.$.rootUrl + "/DisplayBoard/KBKanBanInfo/PreviewForm?keyValue=" + d,
                            width: 1000,
                            height: 800,
                            maxmin: true,
                            btn: null
                        })
                    } else {
                        b.alert.warning("请选择看板！")
                    }
                })
        },
        initData: function () {
            if (!!keyValue) {
                a.mkSetForm(top.$.rootUrl + "/DisplayBoard/KBFeaManage/GetFormData?keyValue=" + keyValue,
                    function (d) {
                        a("#form").mkSetFormData(d)
                    })
            }
        }
    };
    acceptClick = function (d) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var e = a("#form").mkGetFormData();
        a.mkSaveForm(top.$.rootUrl + "/DisplayBoard/KBFeaManage/SaveForm?keyValue=" + keyValue, e,
            function (f) {
                if (!!d) {
                    d()
                }
            })
    };
    c.init()
};