﻿var selectedRow;
var refreshGirdData;
var bootstrap = function (a, b) {
    var c = {
        init: function () {
            c.initGird();
            c.bind()
        },
        bind: function () {
            a("#btn_Search").on("click",
                function () {
                    var d = a("#txt_Keyword").val();
                    c.search({
                        keyword: d
                    })
                });
            a("#lr_refresh").on("click",
                function () {
                    location.reload()
                });
            a("#lr_add").on("click",
                function () {
                    selectedRow = null;
                    b.layerForm({
                        id: "form",
                        title: "新增",
                        url: top.$.rootUrl + "/DisplayBoard/KBFeaManage/Form",
                        width: 700,
                        height: 500,
                        callBack: function (d) {
                            return top[d].acceptClick(refreshGirdData)
                        }
                    })
                });
            a("#lr_edit").on("click",
                function () {
                    var d = a("#gridtable").jfGridValue("F_Id");
                    selectedRow = a("#gridtable").jfGridGet("rowdata");
                    if (b.checkrow(d)) {
                        b.layerForm({
                            id: "form",
                            title: "编辑",
                            url: top.$.rootUrl + "/DisplayBoard/KBFeaManage/Form?keyValue=" + d,
                            width: 700,
                            height: 500,
                            callBack: function (e) {
                                return top[e].acceptClick(refreshGirdData)
                            }
                        })
                    }
                });
            a("#lr_delete").on("click",
                function () {
                    var d = a("#gridtable").jfGridValue("F_Id");
                    if (b.checkrow(d)) {
                        b.layerConfirm("是否确认删除该项！",
                            function (e) {
                                if (e) {
                                    b.deleteForm(top.$.rootUrl + "/DisplayBoard/KBFeaManage/DeleteForm", {
                                        keyValue: d
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                })
        },
        initGird: function () {
            a("#gridtable").mkAuthorizeJfGrid({
                url: top.$.rootUrl + "/DisplayBoard/KBFeaManage/GetPageList",
                headData: [{
                    label: "功能名称",
                    name: "F_FullName",
                    width: 200,
                    align: "left"
                },
                {
                    label: "上级功能",
                    name: "F_ParentId",
                    width: 200,
                    align: "left"
                },
                {
                    label: "看板选择",
                    name: "F_KanBanId",
                    width: 200,
                    align: "left"
                },
                {
                    label: "功能描述",
                    name: "F_Description",
                    width: 200,
                    align: "left"
                },
                ],
                mainId: "F_Id",
                isPage: true
            });
            c.search()
        },
        search: function (d) {
            d = d || {};
            a("#gridtable").jfGridSet("reload", {
                queryJson: JSON.stringify(d)
            })
        }
    };
    refreshGirdData = function () {
        c.search()
    };
    c.init()
};