﻿var acceptClick;
var type = request("type");
var sqlFieldMap = {};
var urlFieldMap = {};
var bootstrap = function (a, b) {
    var d = top.layer_form.selectedModel;
    var c = {
        init: function () {
            c.bind();
            c.initData()
        },
        bind: function () {
            a("#F_WidthValue").mkselect();
            a("#F_LeftValue").mkselect();
            a("#F_DataSourceId").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库",
                allowSearch: true,
                select: function () {
                    var f = a("#F_DataSourceId").mkselectGet();
                    var g = a("#F_Sql").val();
                    if (f && g) {
                        c.sqldata(f, g,
                            function (j) {
                                var h = a("#F_FullName").mkselectGet();
                                a("#F_FullName").mkselectRefresh({
                                    data: j
                                });
                                a("#F_FullName").mkselectSet(h);
                                if (type != "barChart") {
                                    var i = a("#F_Value").mkselectGet();
                                    a("#F_Value").mkselectRefresh({
                                        data: j
                                    });
                                    a("#F_Value").mkselectSet(i)
                                }
                            })
                    }
                }
            });
            a("#mk-info").hover(function () {
                a("#mk-message").show()
            },
                function () {
                    a("#mk-message").hide()
                });
            e();
            a("input[type = 'radio']").click(function () {
                e()
            });
            function e() {
                if (a("input[type = 'radio']:checked").val() == "sql") {
                    a("#db").show();
                    a("#sql").show();
                    a("#api").hide()
                } else {
                    a("#db").hide();
                    a("#sql").hide();
                    a("#api").show()
                }
            }
            a("#wizard").wizard().on("change",
                function (i, h) {
                    var f = a("#btn_finish");
                    var g = a("#btn_next");
                    if (h.direction == "next") {
                        if (!a("#step-1").mkValidform()) {
                            return false
                        }
                        g.attr("disabled", "disabled");
                        f.removeAttr("disabled")
                    } else {
                        if (h.direction == "previous") {
                            f.attr("disabled", "disabled");
                            g.removeAttr("disabled")
                        } else {
                            f.attr("disabled", "disabled");
                            g.removeAttr("disabled")
                        }
                    }
                });
            a("#F_FullName").mkselect({
                value: "F_FullName",
                text: "F_FullName",
                title: "F_FullName",
                allowSearch: true
            });
            if (type == "barChart") {
                var ap = a("#F_Value").parent();
                a("#F_Value").remove();
                $(ap).append("<input type='text' id='F_Value' class='form-control' />");
                $("#step-2").append("");
            }
            else {
                a("#F_Value").mkselect({
                    value: "F_FullName",
                    text: "F_FullName",
                    title: "F_FullName",
                    allowSearch: true
                });
            }
            a("#F_Sql").on("blur",
                function () {
                    var f = a("#F_DataSourceId").mkselectGet();
                    var g = a("#F_Sql").val();
                    if (f && g) {
                        c.sqldata(f, g,
                            function (j) {
                                var h = a("#F_FullName").mkselectGet();
                                a("#F_FullName").mkselectRefresh({
                                    data: j
                                });
                                a("#F_FullName").mkselectSet(h);
                                if (type != "barChart") {
                                    var i = a("#F_Value").mkselectGet();
                                    a("#F_Value").mkselectRefresh({
                                        data: j
                                    });
                                    a("#F_Value").mkselectSet(i)
                                }
                            })
                    } else {
                        b.alert.warning("请检查数据库或sql语句是否输入")
                    }
                });
            a("#F_Interface").on("blur",
                function () {
                    var f = a("#F_Interface").val();
                    if (!!f) {
                        c.interfaceData(f,
                            function (i) {
                                var g = a("#F_FullName").mkselectGet();
                                a("#F_FullName").mkselectRefresh({
                                    data: i
                                });
                                a("#F_FullName").mkselectSet(g);
                                if (type != "barChart") {
                                    var h = a("#F_Value").mkselectGet();
                                    a("#F_Value").mkselectRefresh({
                                        data: i
                                    });
                                    a("#F_Value").mkselectSet(h)
                                }
                            })
                    } else {
                        b.alert.warning("请检查填写接口地址")
                    }
                });
            a("#btn_finish").on("click",
                function () {
                    acceptClick()
                })
        },
        initData: function () {
            if (d) {
                a("#step-1").mkSetFormData(d);
                if (d._configuration) {
                    a("#step-2").mkSetFormData(d._configuration);
                    if (d._configuration.F_DataSourceId && d._configuration.F_Sql) {
                        c.sqldata(d._configuration.F_DataSourceId, d._configuration.F_Sql,
                            function (e) {
                                a("#F_FullName").mkselectRefresh({
                                    data: e
                                });
                                if (type != "barChart") {
                                    a("#F_Value").mkselectRefresh({
                                        data: e
                                    });
                                }
                                a("#F_FullName").mkselectSet(d._configuration.F_FullName);
                                if (type != "barChart") {
                                    a("#F_Value").mkselectSet(d._configuration.F_Value)
                                }
                            })
                    } else {
                        if (d._configuration.F_Interface) {
                            a("input[name=radio][value=api]").trigger("click");
                            c.interfaceData(d._configuration.F_Interface,
                                function (e) {
                                    a("#F_FullName").mkselectRefresh({
                                        data: e
                                    });
                                    if (type != "barChart") {
                                        a("#F_Value").mkselectRefresh({
                                            data: e
                                        });
                                    }
                                    a("#F_FullName").mkselectSet(d._configuration.F_FullName);
                                    if (type != "barChart") {
                                        a("#F_Value").mkselectSet(d._configuration.F_Value)
                                    }
                                })
                        }
                    }
                    a("#F_LinkInfo").val(d._configuration.F_LinkInfo)
                }
            }
        },
        sqldata: function (f, g, e) {
            if (sqlFieldMap[g]) {
                e(sqlFieldMap[g])
            } else {
                g = top.Changjie.filterSysParams(g);
                b.httpAsync("GET", top.$.rootUrl + "/SystemModule/DatabaseTable/GetSqlColName", {
                    databaseLinkId: f,
                    strSql: g
                },
                    function (h) {
                        if (h) {
                            sqlFieldMap[g] = [];
                            a.each(h,
                                function (i, j) {
                                    sqlFieldMap[g].push({
                                        F_FullName: j
                                    })
                                })
                        }
                        e(sqlFieldMap[g] || [])
                    })
            }
        },
        interfaceData: function (f, e) {
            if (urlFieldMap[f]) {
                e(urlFieldMap[f])
            } else {
                b.httpAsync("GET", top.$.rootUrl + "/DisplayBoard/KBConfigInfo/GetApiData", {
                    path: f
                },
                    function (g) {
                        if (g) {
                            urlFieldMap[f] = [];
                            a.each(g[0] || [],
                                function (h, i) {
                                    urlFieldMap[f].push({
                                        F_FullName: h
                                    })
                                })
                        }
                        e(urlFieldMap[f] || [])
                    })
            }
        }
    };
    acceptClick = function (f) {
        if (!a("#step-1").mkValidform()) {
            return false
        }
        var e = a("#step-1").mkGetFormData();
        var g = a("#step-2").mkGetFormData();
        
        if (a("input[type = 'radio']:checked").val() == "sql") {
            if (g.F_DataSourceId == "" || g.F_Sql == "") {
                b.alert.error("请检查数据库或sql语句是否输入");
                return false
            }
            g.F_Interface = ""
        } else {
            if (g.F_Interface == "") {
                b.alert.error("请检查填写接口地址");
                return false
            }
            g.F_DataSourceId = "";
            g.F_Sql = ""
        }
        var h = {
            F_ModeName: e.F_ModeName,
            F_TopValue: e.F_TopValue,
            F_LeftValue: e.F_LeftValue,
            F_WidthValue: e.F_WidthValue,
            F_HightValue: e.F_HightValue,
            F_RefreshTime: e.F_RefreshTime,
            F_Type: type,
            _configuration: {
                F_LinkInfo: e.F_LinkInfo,
                F_DataSourceId: g.F_DataSourceId,
                F_Sql: g.F_Sql,
                F_Interface: g.F_Interface,
                F_FullName: g.F_FullName,
                F_Value: g.F_Value
            }
        };
        if (type == "barChart") {
            if (g.sub_FullName) delete g.sub_FullName;
            if (g.sub_Value) delete g.sub_Value;
            var arr = [];
            $("div[name='subBars']").each(function () {
                var _key = $(this).find("input[name='sub_FullName']").first().val();
                var _val = $(this).find("input[name='sub_Value']").first().val();
                arr.push({ sub_FullName: _key, sub_Value: _val });
            });
            h._configuration.subBars = JSON.stringify(arr);
        }
        if (d) {
            d.F_ModeName = h.F_ModeName;
            d.F_TopValue = h.F_TopValue;
            d.F_LeftValue = h.F_LeftValue;
            d.F_WidthValue = h.F_WidthValue;
            d.F_HightValue = h.F_HightValue;
            d.F_RefreshTime = h.F_RefreshTime;
            d._configuration = h._configuration;
            top.layer_form.displayBoard.updateModel(d)
        } else {
            top.layer_form.displayBoard.addModel(h)
        }
        b.layerClose(window.name);
        return true
    };
    c.init()
};
function addSubBar() {
    $("#step-2").append('<div name="subBars"><div class="col-xs-4 mk-form-item"><div class="mk-form-item-title language">名称对应字段</div><input name="sub_FullName" type="text" class="form-control" /></div><div class="col-xs-4 mk-form-item"><div class="mk-form-item-title language">值对应字段</div><input name="sub_Value" type="text" class="form-control" /></div><div class="col-xs-4 mk-form-item"><a href="#" onclick="removeSubBar(this)">移除此条</a></div></div>');
}
function removeSubBar(self) {
    $(self).parent().parent().remove();
}