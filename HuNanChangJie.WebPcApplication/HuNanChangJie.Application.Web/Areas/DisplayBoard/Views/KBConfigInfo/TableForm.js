﻿var acceptClick;
var fields = [];
var sqlFieldMap = {};
var urlFieldMap = {};
var bootstrap = function (a, b) {
    var d = top.layer_form.selectedModel;
    var c = {
        init: function () {
            c.bind();
            c.initData()
        },
        bind: function () {
            a("#F_WidthValue").mkselect();
            a("#F_LeftValue").mkselect();
            a("#F_DataSourceId").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库",
                allowSearch: true
            });
            a("#lr-info").hover(function () {
                a("#lr-message").show()
            },
                function () {
                    a("#lr-message").hide()
                });
            e();
            a("input[type = 'radio']").click(function () {
                e()
            });
            function e() {
                if (a("input[type = 'radio']:checked").val() == "sql") {
                    a("#db").show();
                    a("#sql").show();
                    a("#api").hide()
                } else {
                    a("#db").hide();
                    a("#sql").hide();
                    a("#api").show()
                }
            }
            a("#wizard").wizard().on("change",
                function (i, h) {
                    var f = a("#btn_finish");
                    var g = a("#btn_next");
                    if (h.direction == "next") {
                        if (!a("#step-1").mkValidform()) {
                            return false
                        }
                        g.attr("disabled", "disabled");
                        f.removeAttr("disabled")
                    } else {
                        if (h.direction == "previous") {
                            f.attr("disabled", "disabled");
                            g.removeAttr("disabled")
                        } else {
                            f.attr("disabled", "disabled");
                            g.removeAttr("disabled")
                        }
                    }
                });
            a("#F_Sql").on("blur",
                function () {
                    var f = a("#F_DataSourceId").mkselectGet();
                    var g = a("#F_Sql").val();
                    if (f && g) {
                        c.sqldata(f, g,
                            function (h) {
                                fields = h
                            })
                    } else {
                        b.alert.warning("请检查数据库或sql语句是否输入")
                    }
                });
            a("#F_Interface").on("blur",
                function () {
                    var f = a("#F_Interface").val();
                    if (!!f) {
                        c.interfaceData(f,
                            function (g) {
                                fields = g
                            })
                    } else {
                        b.alert.warning("请检查填写接口地址")
                    }
                });
            a("#btns_girdtable").jfGrid({
                headData: [{
                    label: "",
                    name: "btn1",
                    width: 50,
                    align: "center",
                    formatter: function (i, h, g, f) {
                        f.on("click",
                            function () {
                                var k = parseInt(f.attr("rowindex"));
                                var j = a("#btns_girdtable").jfGridSet("moveUp", k);
                                return false
                            });
                        return '<span class="label label-info" style="cursor: pointer;">上移</span>'
                    }
                },
                {
                    label: "",
                    name: "btn2",
                    width: 50,
                    align: "center",
                    formatter: function (i, h, g, f) {
                        f.on("click",
                            function () {
                                var k = parseInt(f.attr("rowindex"));
                                var j = a("#btns_girdtable").jfGridSet("moveDown", k);
                                return false
                            });
                        return '<span class="label label-success" style="cursor: pointer;">下移</span>'
                    }
                },
                {
                    label: "字段项名称",
                    name: "F_FullName",
                    width: 150,
                    align: "left",
                    formatter: function (i, h, g, f) {
                        return h.F_FullName || ""
                    },
                    edit: {
                        type: "select",
                        init: function (g, f) {
                            f.mkselectRefresh({
                                data: fields
                            })
                        },
                        op: {
                            value: "F_FullName",
                            text: "F_FullName",
                            allowSearch: true
                        },
                        change: function (g, h, f) {
                            if (f != null) {
                                g.id = f.table + f.F_FullName
                            } else {
                                g.id = ""
                            }
                        }
                    }
                },
                {
                    label: "标题",
                    name: "F_HeadTitle",
                    width: 120,
                    align: "left",
                    edit: {
                        type: "input"
                    }
                },
                {
                    label: "宽度(百分比%)",
                    name: "F_Width",
                    width: 100,
                    align: "left",
                    edit: {
                        type: "input"
                    }
                },
                {
                    label: "对齐",
                    name: "F_Align",
                    width: 100,
                    align: "left",
                    edit: {
                        type: "select",
                        op: {
                            data: [{
                                id: "left",
                                text: "左对齐"
                            },
                            {
                                id: "center",
                                text: "居中"
                            },
                            {
                                id: "right",
                                text: "右对齐"
                            }]
                        }
                    }
                },
                ],
                mainId: "id",
                isEdit: true,
                isMultiselect: true
            });
            a("#btn_finish").on("click",
                function () {
                    acceptClick()
                })
        },
        initData: function () {
            if (d) {
                a("#step-1").mkSetFormData(d);
                if (d._configuration) {
                    var e = d._configuration;
                    a("#tabconfigration").mkSetFormData(e);
                    if (e.F_DataSourceId && e.F_Sql) {
                        c.sqldata(e.F_DataSourceId, e.F_Sql,
                            function (f) {
                                fields = f
                            })
                    } else {
                        if (e.F_Interface) {
                            a("input[name=radio][value=api]").trigger("click");
                            c.interfaceData(e.F_Interface,
                                function (f) {
                                    fields = f
                                })
                        }
                    }
                    a("#F_LinkInfo").val(e.F_LinkInfo);
                    a("#btns_girdtable").jfGridSet("refreshdata", e.grid)
                }
            }
        },
        sqldata: function (f, g, e) {
            if (sqlFieldMap[g]) {
                e(sqlFieldMap[g])
            } else {
                g = top.Changjie.filterSysParams(g);
                b.httpAsync("GET", top.$.rootUrl + "/SystemModule/DatabaseTable/GetSqlColName", {
                    databaseLinkId: f,
                    strSql: g
                },
                    function (h) {
                        if (h) {
                            sqlFieldMap[g] = [];
                            a.each(h,
                                function (i, j) {
                                    sqlFieldMap[g].push({
                                        F_FullName: j
                                    })
                                })
                        }
                        e(sqlFieldMap[g] || [])
                    })
            }
        },
        interfaceData: function (f, e) {
            if (urlFieldMap[f]) {
                e(urlFieldMap[f])
            } else {
                b.httpAsync("GET", top.$.rootUrl + "/DisplayBoard/KBConfigInfo/GetApiData", {
                    path: f
                },
                    function (g) {
                        if (g) {
                            urlFieldMap[f] = [];
                            a.each(g[0] || [],
                                function (h, i) {
                                    urlFieldMap[f].push({
                                        F_FullName: h
                                    })
                                })
                        }
                        e(urlFieldMap[f] || [])
                    })
            }
        }
    };
    acceptClick = function (f) {
        if (!a("#step-1").mkValidform()) {
            return false
        }
        var e = a("#step-1").mkGetFormData();
        var h = a("#tabconfigration").mkGetFormData();
        if (a("input[type = 'radio']:checked").val() == "sql") {
            if (h.F_DataSourceId == "" || h.F_Sql == "") {
                b.alert.error("请检查数据库或sql语句是否输入");
                return false
            }
            h.F_Interface = ""
        } else {
            if (h.F_Interface == "") {
                b.alert.error("请检查填写接口地址");
                return false
            }
            h.F_DataSourceId = "";
            h.F_Sql = ""
        }
        var i = a("#btns_girdtable").jfGridGet("rowdatas");
        if (i.length <= 0) {
            b.alert.error("请新增显示字段");
            return false
        }
        var g = {
            F_ModeName: e.F_ModeName,
            F_TopValue: e.F_TopValue,
            F_LeftValue: e.F_LeftValue,
            F_WidthValue: e.F_WidthValue,
            F_HightValue: e.F_HightValue,
            F_RefreshTime: e.F_RefreshTime,
            F_Type: "table",
            _configuration: {
                F_LinkInfo: e.F_LinkInfo,
                F_DataSourceId: h.F_DataSourceId,
                F_Sql: h.F_Sql,
                F_Interface: h.F_Interface,
                grid: i
            }
        };
        if (d) {
            d.F_ModeName = g.F_ModeName;
            d.F_TopValue = g.F_TopValue;
            d.F_LeftValue = g.F_LeftValue;
            d.F_WidthValue = g.F_WidthValue;
            d.F_HightValue = g.F_HightValue;
            d.F_RefreshTime = g.F_RefreshTime;
            d.F_Type = g.F_Type;
            d._configuration = g._configuration;
            top.layer_form.displayBoard.updateModel(d)
        } else {
            top.layer_form.displayBoard.addModel(g)
        }
        b.layerClose(window.name)
    };
    c.init()
};