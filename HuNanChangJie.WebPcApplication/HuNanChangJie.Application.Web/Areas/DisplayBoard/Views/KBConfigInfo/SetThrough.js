﻿var acceptClick;
var keyValue = request('keyValue');
var id = request('id');
var isItem = request('isItem');
var gridSetting;

var bootstrap = function ($, Changjie) {
    if (isItem.toString() == "true")
        $("#sub").show();
    //console.log(gridSetting);
    //初始化数据
    Changjie.httpGet(top.$.rootUrl + "/DisplayBoard/KBConfigInfo/GetThrough",
        {
            targetId: id,
            isItem: isItem
        },
        function (ret) {
            if (ret) {
                gridSetting = JSON.parse(ret.data);
            }
        })
    if (gridSetting) {
        if (isItem.toString() == "true") {
            var main = JSON.parse(gridSetting.main);
            var sub = JSON.parse(gridSetting.sub);
            if (main) {
                if (main.dynamicParam == "keyValue") {
                    $("input[name='jumpType'][value='keyValue']").attr("checked", "checked");
                    $("input[name='jumpType'][value='projectId']").removeAttr("checked");
                }
                else {
                    $("input[name='jumpType'][value='projectId']").attr("checked", "checked");
                    $("input[name='jumpType'][value='keyValue']").removeAttr("checked");
                }
                $("#jumpUrl").val(main.url);
                $("#targetTitle").val(main.title);
                $("#reportId").val(main.reportId);
                $("#queryJson").val(main.queryJson);
                $("#moduleId").val(main.moduleId);
                $("#formId").val(main.formId);
            }
            if (sub) {
                if (sub.dynamicParam == "keyValue") {
                    $("#s_keyValue").attr("checked", "checked");
                    $("#s_projectId").removeAttr("checked");
                }
                else {
                    $("#s_projectId").attr("checked", "checked");
                    $("#s_keyValue").removeAttr("checked");
                }
                $("#s_jumpUrl").val(sub.url);
                $("#s_targetTitle").val(sub.title);
                $("#s_reportId").val(sub.reportId);
                $("#s_queryJson").val(sub.queryJson);
                $("#s_moduleId").val(sub.moduleId);
                $("#s_formId").val(sub.formId);
            }
        }
        else {
            if (gridSetting.dynamicParam == "keyValue") {
                $("input[value='keyValue']").attr("checked", "checked");
                $("input[value='projectId']").removeAttr("checked");
            }
            else {
                $("input[value='projectId']").attr("checked", "checked");
                $("input[value='keyValue']").removeAttr("checked");
            }
            $("#jumpUrl").val(gridSetting.url);
            $("#targetTitle").val(gridSetting.title);
            $("#reportId").val(gridSetting.reportId);
            $("#queryJson").val(gridSetting.queryJson);
            $("#moduleId").val(gridSetting.moduleId);
            $("#formId").val(gridSetting.formId);
        }
    }
    // 保存数据
    acceptClick = function (callBack) {
        //alert();
        if (isItem.toString() == "true") {
            var main = {};
            main.dynamicParam = $("input[name='jumpType']:checked").val();
            main.url = $("#jumpUrl").val();
            main.title = $("#targetTitle").val();
            main.reportId = $("#reportId").val();
            main.queryJson = $("#queryJson").val();
            main.moduleId = $("#moduleId").val();
            main.formId = $("#formId").val();
            var sub = {};
            sub.dynamicParam = $("input[name='s_jumpType']:checked").val();
            sub.url = $("#s_jumpUrl").val();
            sub.title = $("#s_targetTitle").val();
            sub.reportId = $("#s_reportId").val();
            sub.queryJson = $("#s_queryJson").val();
            sub.moduleId = $("#s_moduleId").val();
            sub.formId = $("#s_formId").val();
            var retValue = {};
            retValue.main = JSON.stringify(main);
            retValue.sub = JSON.stringify(sub);
            callBack(JSON.stringify(retValue));
        }
        else {
            var retValue = {};
            retValue.dynamicParam = $("input[name='jumpType']:checked").val();
            retValue.url = $("#jumpUrl").val();
            retValue.title = $("#targetTitle").val();
            retValue.reportId = $("#reportId").val();
            retValue.queryJson = $("#queryJson").val();
            retValue.moduleId = $("#moduleId").val();
            retValue.formId = $("#formId").val();
            callBack(JSON.stringify(retValue));
        }
        return true;
    };
}