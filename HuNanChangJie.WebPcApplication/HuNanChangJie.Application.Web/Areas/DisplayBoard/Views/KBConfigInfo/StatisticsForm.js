﻿var acceptClick;
var bootstrap = function (a, b) {
    var d = top.layer_form.selectedModel;
    var c = {
        init: function () {
            c.bind();
            c.initData();
        },
        bind: function () {
            a("#F_WidthValue").mkselect();
            a("#F_LeftValue").mkselect();
        },
        initData: function () {
            if (d) {
                a("#form").mkSetFormData(d);
            }
        }
    };
    acceptClick = function (e) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var f = a("#form").mkGetFormData();
        f.F_Configuration = "";
        f.F_Type = "statistics";
        f.F_HightValue = "257";
        if (d) {
            d.F_ModeName = f.F_ModeName;
            d.F_TopValue = f.F_TopValue;
            d.F_LeftValue = f.F_LeftValue;
            d.F_HightValue = f.F_HightValue;
            d.F_WidthValue = f.F_WidthValue;
            d.F_RefreshTime = f.F_RefreshTime;
        }
        e(f);
        return true;
    };
    c.init();
};