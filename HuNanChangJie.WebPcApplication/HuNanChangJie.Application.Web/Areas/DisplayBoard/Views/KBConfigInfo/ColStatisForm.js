﻿var acceptClick;
var sqlFieldMap = {};
var urlFieldMap = {};
var bootstrap = function (a, b) {
    var d = top.layer_form.selectColData;
    var c = {
        init: function () {
            c.bind();
            c.initData()
        },
        bind: function () {
            e();
            a("input[type = 'radio']").click(function () {
                e()
            });
            function e() {
                if (a("input[type = 'radio'][name='radio']:checked").val() == "sql") {
                    a("#db").show();
                    a("#sql").show();
                    a("#api").hide()
                } else {
                    a("#db").hide();
                    a("#sql").hide();
                    a("#api").show()
                }
                if (a("input[type = 'radio'][name='radiot']:checked").val() == "sql") {
                    a("#dbt").show();
                    a("#sqlt").show();
                    a("#apit").hide()
                } else {
                    a("#dbt").hide();
                    a("#apit").hide();
                    a("#sqlt").show()
                }
            }
            a("#F_TBaseData").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库",
                allowSearch: true,
                select: function () {
                    var f = a("#F_TBaseData").mkselectGet();
                    var g = a("#F_TSQL").val();
                    if (f && g) {
                        c.sqldata(f, g,
                            function (i) {
                                var h = a("#F_TitleValue").mkselectGet();
                                a("#F_TitleValue").mkselectRefresh({
                                    data: i
                                });
                                a("#F_TitleValue").mkselectSet(h)
                            })
                    }
                }
            });
            a("#F_SBaseData").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库",
                allowSearch: true,
                select: function () {
                    var f = a("#F_SBaseData").mkselectGet();
                    var g = a("#F_SubSQL").val();
                    if (f && g) {
                        c.sqldata(f, g,
                            function (i) {
                                var h = a("#F_SubtitleValue").mkselectGet();
                                a("#F_SubtitleValue").mkselectRefresh({
                                    data: i
                                });
                                a("#F_SubtitleValue").mkselectSet(h)
                            })
                    }
                }
            });
            a("#F_TSQL").on("blur",
                function () {
                    var f = a("#F_TBaseData").mkselectGet();
                    var g = a("#F_TSQL").val();
                    if (f && g) {
                        var sql = c.filterSysParams(g);
                        g = sql || g;
                        c.sqldata(f, g,
                            function (i) {
                                var h = a("#F_TitleValue").mkselectGet();
                                a("#F_TitleValue").mkselectRefresh({
                                    data: i
                                });
                                a("#F_TitleValue").mkselectSet(h);
                            })
                    } else {
                        b.alert.warning("请检查数据库或sql语句是否输入");
                    }
                });
            a("#F_TInterface").on("blur",
                function () {
                    var f = a("#F_TInterface").val();
                    if (!!f) {
                        c.InterfaceData(f,
                            function (h) {
                                var g = a("#F_TitleValue").mkselectGet();
                                a("#F_TitleValue").mkselectRefresh({
                                    data: h
                                });
                                a("#F_TitleValue").mkselectSet(g)
                            })
                    } else {
                        b.alert.warning("请检查填写接口地址")
                    }
                });
            a("#F_SubSQL").on("blur",
                function () {
                    var f = a("#F_SBaseData").mkselectGet();
                    var g = a("#F_SubSQL").val();
                    if (f && g) {
                        var sql = c.filterSysParams(g);
                        g = sql || g;
                        c.sqldata(f, g,
                            function (i) {
                                var h = a("#F_SubtitleValue").mkselectGet();
                                a("#F_SubtitleValue").mkselectRefresh({
                                    data: i
                                });
                                a("#F_SubtitleValue").mkselectSet(h);
                            })
                    } else {
                        b.alert.warning("请检查数据库或sql语句是否输入");
                    }
                });
            a("#F_SInterface").on("blur",
                function () {
                    var f = a("#F_SInterface").val();
                    if (!!f) {
                        c.InterfaceData(f,
                            function (h) {
                                var g = a("#F_SubtitleValue").mkselectGet();
                                a("#F_SubtitleValue").mkselectRefresh({
                                    data: h
                                });
                                a("#F_SubtitleValue").mkselectSet(g)
                            })
                    } else {
                        b.alert.warning("请检查填写接口地址")
                    }
                });
            a("#F_TitleValue").mkselect({
                value: "F_FullName",
                text: "F_FullName",
                allowSearch: true
            });
            a("#F_SubtitleValue").mkselect({
                value: "F_FullName",
                text: "F_FullName",
                allowSearch: true
            })
        },
        initData: function () {
            if (d) {
                a("#form").mkSetFormData(d);
                if (d.F_TBaseData && d.F_TSQL) {
                    c.sqldata(d.F_TBaseData, c.filterSysParams(d.F_TSQL),
                        function (e) {
                            a("#F_TitleValue").mkselectRefresh({
                                data: e
                            });
                            a("#F_TitleValue").mkselectSet(d.F_TitleValue)
                        })
                }
                if (d.F_SBaseData && d.F_SubSQL) {
                    c.sqldata(d.F_SBaseData, c.filterSysParams(d.F_SubSQL),
                        function (e) {
                            a("#F_SubtitleValue").mkselectRefresh({
                                data: e
                            });
                            a("#F_SubtitleValue").mkselectSet(d.F_SubtitleValue)
                        })
                }
                if (d.F_TInterface) {
                    a("input[name=radio][value=api]").trigger("click");
                    c.InterfaceData(d.F_TInterface,
                        function (e) {
                            a("#F_TitleValue").mkselectRefresh({
                                data: e
                            });
                            a("#F_TitleValue").mkselectSet(d.F_TitleValue)
                        })
                }
                if (d.F_SInterface) {
                    a("input[name=radiot][value=api]").trigger("click");
                    c.InterfaceData(d.F_SInterface,
                        function (e) {
                            a("#F_SubtitleValue").mkselectRefresh({
                                data: e
                            });
                            a("#F_SubtitleValue").mkselectSet(d.F_SubtitleValue)
                        })
                }
            }
        },
        sqldata: function (f, g, e) {
            if (sqlFieldMap[g]) {
                e(sqlFieldMap[g])
            } else {
                g=c.filterSysParams(g)
                b.httpAsync("GET", top.$.rootUrl + "/SystemModule/DatabaseTable/GetSqlColName", {
                    databaseLinkId: f,
                    strSql: g
                },
                    function (h) {
                        if (h) {
                            sqlFieldMap[g] = [];
                            a.each(h,
                                function (i, j) {
                                    sqlFieldMap[g].push({
                                        F_FullName: j
                                    })
                                })
                        }
                        e(sqlFieldMap[g] || [])
                    })
            }
        },
        filterSysParams: function (g) {
            var value = "";
            if (g.indexOf("@projectId@") >= 0) {
                if (g.indexOf(" ") >= 0) {
                    var list = g.split(' ');
                    for (var i = 0; i < list.length; i++) {
                        var item = list[i];
                        if (item.indexOf("@projectId@") == -1) {
                            value += item + " ";
                        }
                        else {
                            value += " 1=1 ";
                        }
                    }
                }
            }
            else {
                return g;
            }
            return value;
        },
        rfaceData: function (f, e) {
            if (urlFieldMap[f]) {
                e(urlFieldMap[f])
            } else {
                b.httpAsync("GET", top.$.rootUrl + "/DisplayBoard/KBConfigInfo/GetApiData", {
                    path: f
                },
                    function (g) {
                        if (g) {
                            urlFieldMap[f] = [];
                            a.each(g,
                                function (h, i) {
                                    urlFieldMap[f].push({
                                        F_FullName: h
                                    })
                                })
                        }
                        e(urlFieldMap[f] || [])
                    })
            }
        }
    };
    acceptClick = function (e) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var f = a("#form").mkGetFormData();
        if (a("input[type = 'radio'][name='radio']:checked").val() == "sql") {
            if (f.F_TBaseData == "" || f.F_TSQL == "") {
                b.alert.error("请检查数据库或sql语句是否输入");
                return false
            }
            f.F_TInterface = ""
        } else {
            if (f.F_TInterface == "") {
                b.alert.error("请检查填写接口地址");
                return false
            }
            f.F_TBaseData = "";
            f.F_TSQL = ""
        }
        if (a("input[type = 'radio'][name='radiot']:checked").val() == "sql") {
            if (f.F_SBaseData == "" || f.F_SubSQL == "") {
                b.alert.error("请检查数据库或sql语句是否输入");
                return false
            }
            f.F_SInterface = ""
        } else {
            if (f.F_SInterface == "") {
                b.alert.error("请检查填写接口地址");
                return false
            }
            f.F_SBaseData = "";
            f.F_SubSQL = ""
        }
        if (d) {
            a.extend(d, f)
        }
        f.id = b.newGuid();
        e(f);
        return true
    };
    c.init()
};