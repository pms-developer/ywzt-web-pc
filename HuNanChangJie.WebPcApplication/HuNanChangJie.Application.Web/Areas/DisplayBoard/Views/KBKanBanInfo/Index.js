﻿///* 
// * Copyright (c) 2013-2019 
// * 创建人：超级管理员
// * 日  期：2019-01-17 13:19
// * 描  述：KBKanBanInfo
// */
//var refreshGirdData;
//var selectedRow;
//var bootstrap = function ($, Changjie) {
//    "use strict";
//    var page = {
//        init: function () {
//            page.initGird();
//            page.bind();
//        },
//        bind: function () {
//            // 刷新
//            $('#refresh').on('click', function () {
//                location.reload();
//            });
//            // 新增
//            $('#add').on('click', function () {
//                selectedRow = null;
//                Changjie.layerForm({
//                    id: 'form',
//                    title: '新增',
//                    url: top.$.rootUrl + '/DisplayBoard/KBKanBanInfo/Form', 
//                    width: 1180,
//                    height: 792,
//                    btn: null
                    
//                });
//            });
//            // 编辑
//            $('#edit').on('click', function () {
//                var keyValue = $('#gridtable').jfGridValue('F_Id');
//                if (Changjie.checkrow(keyValue)) {
//                    Changjie.layerForm({
//                        id: 'form',
//                        title: '编辑',
//                        url: top.$.rootUrl + '/DisplayBoard/KBKanBanInfo/Form?keyValue=' + keyValue,
//                        width: 1180,
//                        height: 800,
//                        btn: null
                         
//                    });
//                }
//            });
//            // 删除
//            $('#delete').on('click', function () {
//                var keyValue = $('#gridtable').jfGridValue('F_Id');
//                if (Changjie.checkrow(keyValue)) {
//                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
//                        if (res) {
//                            Changjie.deleteForm(top.$.rootUrl + '/DisplayBoard/KBKanBanInfo/DeleteForm', { keyValue: keyValue}, function () {
//                                refreshGirdData();
//                            });
//                        }
//                    });
//                }
//            });
//            // 打印
//            $('#print').on('click', function () {
//                $('#gridtable').jqprintTable();
//            });


//            $("#see").on("click",
//                function () {
//                    var keyvalue = $("#gridtable").jfGridValue("F_Id");
//                    if (!!keyvalue) {
//                        Changjie.layerForm({
//                            id: "custmerForm_PreviewForm",
//                            title: "预览当前看板",
//                            url: top.$.rootUrl + "/DisplayBoard/KBKanBanInfo/PreviewForm?keyValue=" + keyvalue,
//                            width: 1000,
//                            height: 800,
//                            maxmin: true,
//                            btn: null
//                        });
//                    } else {
//                        Changjie.alert.warning("请选择看板！");
//                    }
//                });
//        },
//        // 初始化列表
//        initGird: function () {
//            $('#gridtable').mkAuthorizeJfGrid({
//                url: top.$.rootUrl + '/DisplayBoard/KBKanBanInfo/GetPageList',
//                headData:  
//                [{
//                        label: "看板名称",
//                        name: "F_KanBanName",
//                        width: 200 
//                    },
//                    {
//                        label: "看板编号",
//                        name: "F_KanBanCode",
//                        width: 130 
//                    },
//                    {
//                        label: "刷新时间",
//                        name: "F_RefreshTime",
//                        width: 100 
//                    },
//                    {
//                        label: "创建日期",
//                        name: "CreationDate",
//                        width: 100 
                        
//                    },
//                    {
//                        label: "创建用户",
//                        name: "CreationName",
//                        width: 100
                         
//                    },
//                    {
//                        label: "看板说明",
//                        name: "F_Description",
//                        width: 300
                        
//                    }],
//                mainId:'F_Id',
//                isPage: true
//            });
//            page.search();
//        },
//        search: function (param) {
//            param = param || {};
//            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
//        }
//    };
//    refreshGirdData = function () {
//        page.search();
//    };
//    page.init();
//}
