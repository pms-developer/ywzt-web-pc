﻿using System.Web.Mvc;
namespace HuNanChangJie.Application.Web.Areas.DisplayBoard
{
    public class DisplayBoardAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DisplayBoard"; 
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DisplayBoard_default", 
                "DisplayBoard/{controller}/{action}/{id}", 
                             new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
