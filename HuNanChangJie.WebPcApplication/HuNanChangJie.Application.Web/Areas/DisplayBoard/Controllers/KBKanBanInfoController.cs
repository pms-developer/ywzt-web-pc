﻿using HuNanChangJie.Application.TwoDevelopment.DisplayBoard;
using HuNanChangJie.Util;
using HuNanChangJie.Util.Model;
using System.Web.Mvc;
using HuNanChangJie.Application.Report;

namespace HuNanChangJie.Application.Web.Areas.DisplayBoard.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:19
    /// 描 述：KBKanBanInfo
    /// </summary>
    public class KBKanBanInfoController : MvcControllerBase
    {
        private KBKanBanInfoIBLL KBKanBanInfoIBLL = new KBKanBanInfoBLL();
        private KBConfigInfoIBLL KBConfigInfoIBLL = new KBConfigInfoBLL();
        private ReportTempIBLL reportTempIBLL = new ReportTempBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }


        public ActionResult PreviewForm()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = KBKanBanInfoIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetKanBanInfoByName(string kbName)
        {
            var data = KBKanBanInfoIBLL.GetKanBanInfoByName(kbName);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GeTList(string queryJson)
        {
            XqPagination paginationobj = new XqPagination();
            paginationobj.page = 1;
            paginationobj.rows = 10000; 
            var data = KBKanBanInfoIBLL.GetPageList(paginationobj, queryJson);
            return Success(data);
        }


        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var KBKanBanInfoData = KBKanBanInfoIBLL.GetKBKanBanInfoEntity(keyValue);

            var configinfo = KBConfigInfoIBLL.GetList(keyValue);

            var jsonData = new
            {
                baseinfo = KBKanBanInfoData,
                configinfo = configinfo,

                KBKanBanInfo = KBKanBanInfoData,
            };
            return Success(jsonData);
        }

      

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            KBKanBanInfoIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string kanbaninfo, string kbconfigInfo
        )
        {
            KBKanBanInfoEntity kanbaninfoentity = kanbaninfo.ToObject<KBKanBanInfoEntity>();
            var kbconfigInfoentity = kbconfigInfo.ToList<KBConfigInfoEntity>();
            KBKanBanInfoIBLL.SaveEntity(keyValue, kanbaninfoentity);
            KBConfigInfoIBLL.DeleteEntityByKanBanID(keyValue);
            foreach (var lfie in kbconfigInfoentity)
            {
                lfie.F_KanBanId = kanbaninfoentity.F_Id;
                KBConfigInfoIBLL.SaveEntity("", lfie);
            }
            return Success("保存成功！");
        }
        #endregion

    }
}
