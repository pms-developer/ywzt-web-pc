﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.DisplayBoard;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.Web.Areas.DisplayBoard.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:17
    /// 描 述：看板管理
    /// </summary>
    public class KBFeaManageController : MvcControllerBase
    {
        private KBFeaManageIBLL KBFeaManageIBLL = new KBFeaManageBLL();

        private ModuleIBLL moduleIbll = new ModuleBLL();
        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = KBFeaManageIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var KBFeaManageData = KBFeaManageIBLL.GetKBFeaManageEntity( keyValue );
             
            return Success(KBFeaManageData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            KBFeaManageIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, KBFeaManageEntity entity)
        {

            ModuleEntity me = new ModuleEntity();
            me.F_UrlAddress = $"/DisplayBoard/KBKanBanInfo/PreviewForm?keyValue={entity.F_KanBanId}";
            me.F_ParentId = entity.F_ParentId;
            me.F_ModuleId = entity.F_ModuleId;
            me.F_AllowExpand = 1;
            me.F_EnabledMark = 1;
            me.F_FullName = entity.F_FullName;
            me.F_DeleteMark = 0;
            me.F_Target = "iframe";
            me.F_SortCode = entity.F_SortCode;
            me.F_Icon = entity.F_Icon;
            me.F_EnCode = entity.F_EnCode;
            me.F_IsMenu = 1;
            
            moduleIbll.SaveEntity(entity.F_ModuleId??"",me,null,null,null);
            entity.F_ModuleId = me.F_ModuleId;
            KBFeaManageIBLL.SaveEntity(keyValue,entity);
            return Success("保存成功！");
        }
        #endregion

    }
}
