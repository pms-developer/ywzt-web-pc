﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.DisplayBoard;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.Report;
using HuNanChangJie.Util.Model;

namespace HuNanChangJie.Application.Web.Areas.DisplayBoard.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:16
    /// 描 述：看板配置信息
    /// </summary>
    public class KBConfigInfoController : MvcControllerBase
    {
        private KBConfigInfoIBLL KBConfigInfoIBLL = new KBConfigInfoBLL();
        private ReportTempIBLL reportTempIBLL = new ReportTempBLL();
        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = KBConfigInfoIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var KBConfigInfoData = KBConfigInfoIBLL.GetKBConfigInfoEntity(keyValue);
            var jsonData = new
            {
                KBConfigInfo = KBConfigInfoData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            KBConfigInfoIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            KBConfigInfoEntity entity = strEntity.ToObject<KBConfigInfoEntity>();
            KBConfigInfoIBLL.SaveEntity(keyValue, entity);
            return Success("保存成功！");
        }

        /// <summary>
        /// 设置穿透配置
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult SetThrough(string targetId, string jsonStr, bool isItem)
        {
            KBConfigInfoIBLL.SetThrough(targetId, jsonStr, isItem);
            return Success("保存成功！");
        }

        /// <summary>
        /// 设置穿透配置
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetThrough(string targetId, bool isItem)
        {
            return Success("ok", KBConfigInfoIBLL.GetThrough(targetId, isItem));
        }
        #endregion


        #region 看板
        /// <summary>
        /// 根据sql获取值
        /// </summary>
        /// <param name="configInfoList"></param>
        /// <returns></returns>

        public ActionResult GetConfigData(string configInfoList)
        {


            var cilDatas = configInfoList.ToList<KBResSqlData>();
            foreach (var tdata in cilDatas)
            {
                tdata.data = reportTempIBLL.GetReportData(tdata.dbId, tdata.sql);

            }
            return Success(cilDatas);
        }

        public ActionResult StatisticsForm()
        {

            return View();
        }

        public ActionResult ColStatisForm()
        {

            return View();
        }

        public ActionResult ChartForm(string type)
        {
            return View();
        }


        public ActionResult TableForm()
        {
            return View();
        }
        public ActionResult ThroughConfig()
        {
            return View();
        }
        #endregion
    }
}
