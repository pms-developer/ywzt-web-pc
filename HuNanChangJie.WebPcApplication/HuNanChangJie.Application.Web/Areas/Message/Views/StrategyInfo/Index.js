﻿var selectedRow;
var refreshGirdData;
var dateBegin = "";
var dateEnd = "";
var bootstrap = function (a, b) {
    var c = {
        init: function () {
            c.initGird();
            c.bind()
        },
        bind: function () {
            a("#datesearch").mkdate({
                dfdata: [{
                    name: "今天",
                    begin: function () {
                        return b.getDate("yyyy-MM-dd 00:00:00")
                    },
                    end: function () {
                        return b.getDate("yyyy-MM-dd 23:59:59")
                    }
                },
                {
                    name: "近7天",
                    begin: function () {
                        return b.getDate("yyyy-MM-dd 00:00:00", "d", -6)
                    },
                    end: function () {
                        return b.getDate("yyyy-MM-dd 23:59:59")
                    }
                },
                {
                    name: "近1个月",
                    begin: function () {
                        return b.getDate("yyyy-MM-dd 00:00:00", "m", -1)
                    },
                    end: function () {
                        return b.getDate("yyyy-MM-dd 23:59:59")
                    }
                },
                {
                    name: "近3个月",
                    begin: function () {
                        return b.getDate("yyyy-MM-dd 00:00:00", "m", -3)
                    },
                    end: function () {
                        return b.getDate("yyyy-MM-dd 23:59:59")
                    }
                }],
                mShow: false,
                premShow: false,
                jShow: false,
                prejShow: false,
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                selectfn: function (d, e) {
                    dateBegin = d;
                    dateEnd = e;
                    c.search()
                }
            });
            a("#btn_Search").on("click",
                function () {
                    var d = a("#txt_Keyword").val();
                    c.search({
                        keyword: d
                    })
                });
            a("#refresh").on("click",
                function () {
                    location.reload()
                });
            a("#add").on("click",
                function () {
                    selectedRow = null;
                    b.layerForm({
                        id: "form",
                        title: "新增",
                        url: top.$.rootUrl + "/Message/StrategyInfo/Form",
                        width: 500,
                        height: 400,
                        callBack: function (d) {
                            return top[d].acceptClick(refreshGirdData)
                        }
                    })
                });
            a("#send").on("click",
                function () {
                    selectedRow = null;
                    var d = a("#gridtable").jfGridGet("rowdata").F_StrategyCode;
                    b.layerForm({
                        id: "sendform",
                        title: "发送消息",
                        url: top.$.rootUrl + "/Message/StrategyInfo/SendForm?keyValue=" + d,
                        width: 500,
                        height: 600,
                        callBack: function (e) {
                            return top[e].acceptClick(refreshGirdData);
                        }
                    })
                });
            a("#edit").on("click",
                function () {
                    var d = a("#gridtable").jfGridValue("F_Id");
                    selectedRow = a("#gridtable").jfGridGet("rowdata");
                    if (b.checkrow(d)) {
                        b.layerForm({
                            id: "form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Message/StrategyInfo/Form?keyValue=" + d,
                            width: 500,
                            height: 400,
                            callBack: function (e) {
                                return top[e].acceptClick(refreshGirdData)
                            }
                        })
                    }
                });
            a("#delete").on("click",
                function () {
                    var d = a("#gridtable").jfGridValue("F_Id");
                    if (b.checkrow(d)) {
                        b.layerConfirm("是否确认删除该项！",
                            function (e) {
                                if (e) {
                                    b.deleteForm(top.$.rootUrl + "/Message/StrategyInfo/DeleteForm", {
                                        keyValue: d
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                })
        },
        initGird: function () {
            a("#gridtable").mkAuthorizeJfGrid({
                url: top.$.rootUrl + "/Message/StrategyInfo/GetPageList",
                headData: [{
                    label: "策略名称",
                    name: "F_StrategyName",
                    width: 140,
                    align: "left"
                },
                {
                    label: "策略编码",
                    name: "F_StrategyCode",
                    width: 110,
                    align: "left"
                },
                {
                    label: "消息类型",
                    name: "F_MessageType",
                    width: 120,
                    align: "left",
                    formatter: function (d, f) {
                        d = (d || "") + "";
                        var g = d.split(",");
                        var e = [];
                        a.each(g,
                            function (h, i) {
                                switch (i) {
                                    case "1":
                                        e.push("邮件");
                                        break;
                                    case "2":
                                        e.push("微信");
                                        break;
                                    case "3":
                                        e.push("短信");
                                        break;
                                    case "4":
                                        e.push("系统IM");
                                        break;
                                }
                            });
                        return String(e);
                    }
                },
                {
                    label: "备注",
                    name: "F_Description",
                    width: 300,
                    align: "left"
                },
                ],
                mainId: "F_Id",
                sidx: "CreationDate",
                isPage: true
            });
            c.search()
        },
        search: function (d) {
            d = d || {};
            d.dateBegin = dateBegin;
            d.dateEnd = dateEnd;
            a("#gridtable").jfGridSet("reload", {
                queryJson: JSON.stringify(d)
            })
        }
    };
    refreshGirdData = function () {
        c.search()
    };
    c.init()
};