﻿var acceptClick;
var keyValue = request("keyValue");
var bootstrap = function (a, b) {
    var selectrow = b.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            a("#F_SendRole").mkselect({
                value: "F_RoleId",
                text: "F_FullName",
                type: "multiple",
                maxHeight: 200,
                allowSearch: true,
                url: top.$.rootUrl + "/OrganizationModule/Role/GetRoleList",
                param: {
                    parentId: ""
                },
            });
            a("#F_MessageType").mkselect({
                type: "multiple",
                value: "id",
                text: "text",
                data: [{
                        id: "1",
                        text: "邮件"
                    },
                    {
                        id: "2",
                        text: "微信"
                    },
                    {
                        id: "3",
                        text: "短信"
                    },
                    {
                        id: "4",
                        text: "系统IM"
                    }]
            });
            a("#F_StrategyCode").on("blur",
                function() {
                    a.mkExistField(keyValue,
                        "F_StrategyCode",
                        top.$.rootUrl + "/Message/StrategyInfo/ExistStrategyCode");
                });
        },
        initData: function () {
            if (!!selectrow) {
                a("#form").mkSetFormData(selectrow);
            }
        }
    };
    acceptClick = function (callback) {
        if (!a("#form").mkValidform()) {
            return false;
        }
        var formdata = a("#form").mkGetFormData();
        a.mkSaveForm(top.$.rootUrl + "/Message/StrategyInfo/SaveForm?keyValue=" + keyValue,
            formdata,
            function(g) {
                if (!!callback) {
                    callback();
                }
            });
    };
    page.init();
};