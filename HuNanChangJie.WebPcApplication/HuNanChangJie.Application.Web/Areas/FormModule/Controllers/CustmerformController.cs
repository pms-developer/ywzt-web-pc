﻿using HuNanChangeJie.Application.SystemForm.Bll;
using HuNanChangeJie.Application.SystemForm.Model;
using HuNanChangJie.Application.Form;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.FormModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.04.01
    /// 描 述：普通表单设计
    /// </summary>
    public class CustmerformController : MvcControllerBase
    {
        private FormSchemeIBLL formSchemeIBLL = new FormSchemeBLL();
        private ISystemFormBll sysformBll = new SystemFormBll();
        #region  视图功能
        /// <summary>
        /// 管理页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单设计页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        /// <summary>
        /// 表单预览
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PreviewForm()
        {
            return View();
        }
        /// <summary>
        /// 表单模板历史记录查询
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult HistoryForm()
        {
            return View();
        } 
        /// <summary>
        /// 数据库表增改
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DataTableForm()
        {
            return View();
        }



        /// <summary>
        /// 设置表格字段
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SetFieldIndex()
        {
            return View();
        }
        /// <summary>
        /// 设置表格字段
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SetFieldForm()
        {
            return View();
        }
        /// <summary>
        /// 设置表格字段
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SetSelectFieldForm()
        {
            return View();
        }


        /// <summary>
        /// 自定义表单弹层实例
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LayerInstanceForm()
        {
            return View();
        }

        /// <summary>
        /// 自定义表单窗口页实例
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TabInstanceForm()
        {
            return View();
        }

        /// <summary>
        /// 自定义表单用于工作流实例
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WorkflowInstanceForm()
        {
            return View();
        }


        #endregion

        #region  获取数据
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">关键字</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string keyword, string category)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = formSchemeIBLL.GetSchemeInfoPageList(paginationobj, keyword, category, 0);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="schemeInfoId"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSchemePageList(string pagination, string schemeInfoId)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = formSchemeIBLL.GetSchemePageList(paginationobj, schemeInfoId);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
        /// <summary>
        /// 获取设计表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormAllData(string keyValue, string formName)
        {
            var type = "customer";
            if (!string.IsNullOrWhiteSpace(formName))
            {
                if (formName.Contains("(系统)"))
                {
                    type = "system";
                }
            }
            switch (type)
            {
                case "customer":
                    FormSchemeInfoEntity schemeInfoEntity = formSchemeIBLL.GetSchemeInfoEntity(keyValue);
                    FormSchemeEntity schemeEntity = formSchemeIBLL.GetSchemeEntity(schemeInfoEntity.F_SchemeId);
                    var jsonData = new
                    {
                        schemeInfoEntity = schemeInfoEntity,
                        schemeEntity = schemeEntity,
                        formtype= type,
                    };
                    return JsonResult(jsonData);
                case "system":
                    var details = sysformBll.GetFormDetailList(keyValue);
                    var sysmFormData = new
                    {
                        details = details,
                        formtype = type,
                    };
                    return JsonResult(sysmFormData);
            }
            return null;
        }

        /// <summary>
        /// 获取设计表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormSettingData(string schemeId)
        {
           
            FormSchemeInfoEntity schemeInfoEntity = formSchemeIBLL.GetSchemeInfoEntity(schemeId);
            FormSchemeEntity schemeEntity = formSchemeIBLL.GetSchemeEntity(schemeInfoEntity.F_SchemeId);
            var jsonData = new
            {
                SettingData= schemeEntity.F_Scheme,
            };
            return JsonResult(jsonData);
        }

        /// <summary>
        /// 获取设计表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            FormSchemeInfoEntity schemeInfoEntity = formSchemeIBLL.GetSchemeInfoEntity(keyValue);
            FormSchemeEntity schemeEntity = formSchemeIBLL.GetSchemeEntity(schemeInfoEntity.F_SchemeId);
            var jsonData = new
            {
                schemeInfoEntity = schemeInfoEntity,
                schemeEntity = schemeEntity
            };
            return JsonResult(jsonData);
        }

        /// <summary>
        /// 获取自定义表单模板数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSchemeEntity(string keyValue)
        {
            var data = formSchemeIBLL.GetSchemeEntity(keyValue);
            return JsonResult(data);
        }
        /// <summary>
        /// 获取自定义表单列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSchemeInfoList()
        {
            var data = formSchemeIBLL.GetCustmerSchemeInfoList();
            return JsonResult(data);
        }

        /// <summary>
        /// 获取自定义表单及系统表单列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormList()
        {
            //var custList = formSchemeIBLL.GetCustmerSchemeInfoList();

            var sysList = sysformBll.GetFormList();

            //var combineList = new List<FormEntity>();
            //if (custList != null)
            //{
            //    foreach (var item in custList)
            //    {
            //        if (item.F_DeleteMark.HasValue && item.F_DeleteMark.Value == 1)
            //            continue;

            //        if (item.F_EnabledMark.HasValue && item.F_EnabledMark.Value == 0)
            //            continue;

            //        var info = new FormEntity();
            //        info.F_Id = item.F_Id;
            //        info.F_Name = item.F_Name+"(自定义)";
            //        info.Type = "自定义";
            //        info.Url = $"/FormModule/Custmerform/WorkflowInstanceForm";
            //        combineList.Add(info);
            //    }
            //}

            List<FormEntity> formList = new List<FormEntity>();
            if (sysList != null)
            {
                foreach (var item in sysList)
                {
                    var info = new FormEntity();
                    info.F_Id = item.ID;
                   
                    info.MainTable = item.MainTable;
                    info.SubTables = item.SubTables.ToObject<List<SubTable>>();
                    if (item.FormType == "Customer")
                    {
                        info.F_Name = item.FormName+ "(自定义)";
                        info.Url = $"/FormModule/Custmerform/WorkflowInstanceForm";
                        info.Type = "自定义";
                    }
                    else
                    {
                        info.F_Name = item.FormName;
                        info.Type = "系统";
                        info.Url = item.Url;
                    }
                    formList.Add(info);
                }
            }




            return JsonResult(formList);
        }
        /// <summary>
        /// 获取表单流程ID
        /// </summary>
        /// <param name="schemeCode">表单ID</param>
        /// <param name="formKeyValue">数据ID</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetProcessinfoBySchemeForm(string schemeCode, string formKeyValue)
        {
            if (string.IsNullOrEmpty(schemeCode) || string.IsNullOrEmpty(formKeyValue))
            { return JsonResult(new { res = false, msg = "参数错误[0001]" }); }

            FormSchemeInfoEntity formSchemeInfoEntity = formSchemeIBLL.GetSchemeInfoEntity(schemeCode);
            if (formSchemeInfoEntity == null || string.IsNullOrEmpty(formSchemeInfoEntity.F_SchemeId))
            { return JsonResult(new { res = false, msg = "参数错误[0002]" }); }

            FormSchemeEntity formEntity = formSchemeIBLL.GetSchemeEntity(formSchemeInfoEntity.F_SchemeId);
            if (formEntity == null || string.IsNullOrEmpty(formEntity.F_Scheme))
            { return JsonResult(new { res = false, msg = "参数错误[0003]" }); }

            FormSchemeModel formSchemeModel = formEntity.F_Scheme.ToObject<FormSchemeModel>();
            if (formSchemeModel == null || formSchemeModel.dbTable.Count == 0)
            {
                return JsonResult(new { res = false, msg = "参数错误[0004]" });
            }

            var tableQuery = from t in formSchemeModel.dbTable where t.isMain == "1" select t.name;
            string tableName = tableQuery.FirstOrDefault();
            if (string.IsNullOrEmpty(tableName))
            { return JsonResult(new { res = false, msg = "参数错误[0005]" }); }

            string processinfo = formSchemeIBLL.GetProcessinfoBySchemeForm(tableName, formKeyValue);
            if (string.IsNullOrEmpty(processinfo))
            { return JsonResult(new { res = false, msg = "参数错误[0006]" }); }
            return JsonResult(new { res = true, msg = "", data = processinfo });
        }
        /// <summary>
        /// 获取自定义表单数据
        /// </summary>
        /// <param name="schemeInfoId">表单模板主键</param>
        /// <param name="processIdName">流程关联字段名</param>
        /// <param name="keyValue">数据主键值</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetInstanceForm(string schemeInfoId,string processIdName, string keyValue)
        {
            if (string.IsNullOrEmpty(processIdName))
            {
                var data = formSchemeIBLL.GetInstanceForm(schemeInfoId, keyValue);
                return JsonResult(data);
            }
            else
            {
                var data = formSchemeIBLL.GetInstanceForm(schemeInfoId, processIdName, keyValue);
                return JsonResult(data);
            }
           
           
        }
        #endregion

        #region  提交数据
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="schemeInfo">表单设计模板信息</param>
        /// <param name="scheme">模板内容</param>
        /// <param name="type">类型1.正式2.草稿</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string schemeInfo, string scheme, int type)
        {
            FormSchemeInfoEntity schemeInfoEntity = schemeInfo.ToObject<FormSchemeInfoEntity>();
            FormSchemeEntity schemeEntity = new FormSchemeEntity();
            schemeEntity.F_Scheme = scheme;
            schemeEntity.F_Type = type;

            formSchemeIBLL.SaveEntity(keyValue, schemeInfoEntity, schemeEntity);
            return Success("保存成功！");
        }
        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            formSchemeIBLL.VirtualDelete(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 更新表单模板版本
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="state">状态1启用0禁用</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult UpdateScheme(string schemeInfoId, string schemeId)
        {
            formSchemeIBLL.UpdateScheme(schemeInfoId, schemeId);
            return Success("更新成功！");
        }
        /// <summary>
        /// 启用/停用表单
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="state">状态1启用0禁用</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult UpDateSate(string keyValue, int state)
        {
            formSchemeIBLL.UpdateState(keyValue, state);
            return Success((state == 1 ? "启用" : "禁用") + "成功！");
        }

        /// <summary>
        /// 保存自定义表单数据
        /// </summary>
        /// <param name="schemeInfoId">表单模板主键</param>
        /// <param name="processIdName">流程关联字段名</param>
        /// <param name="keyValue">数据主键值</param>
        /// <param name="formData">自定义表单数据</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [ValidateInput(false)]
        public ActionResult SaveInstanceForm(string schemeInfoId, string processIdName, string keyValue, string formData)
        {
            formSchemeIBLL.SaveInstanceForm(schemeInfoId, processIdName, keyValue, formData);
            return Success("保存成功！");
        }

        /// <summary>
        /// 保存自定义表单数据
        /// </summary>
        /// <param name="schemeInfoId">表单模板主键</param>
        /// <param name="processIdName">流程关联字段名</param>
        /// <param name="keyValue">数据主键值</param>
        /// <param name="formData">自定义表单数据</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [ValidateInput(false)]
        public ActionResult SaveFormInfo(string schemeInfoId, string processIdName, string keyValue, string formData, string type, string deleteList,string projectId)
        {
            //var schemeInfoId = form["schemeInfoId"].ToString();
            //var processIdName = form["processIdName"].ToString();
            //var keyValue = form["keyValue"].ToString();
            formSchemeIBLL.SaveFormInfo(schemeInfoId, processIdName, keyValue, formData, deleteList, type,projectId);
            return Success("保存成功！");
        }



        /// <summary>
        /// 删除自定义表单数据
        /// </summary>
        /// <param name="schemeInfoId">表单模板主键</param>
        /// <param name="keyValue">数据主键值</param>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteInstanceForm(string schemeInfoId, string keyValue)
        {
            formSchemeIBLL.DeleteInstanceForm(schemeInfoId, keyValue);
            return Success("删除成功！");
        }
        #endregion
    }
}