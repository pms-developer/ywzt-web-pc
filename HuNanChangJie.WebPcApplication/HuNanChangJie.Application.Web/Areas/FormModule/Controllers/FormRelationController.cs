﻿using HuNanChangeJie.Application.SystemForm.Bll;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Application.Form;
using HuNanChangJie.Util;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.FormModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.04.01
    /// 描 述：表单关联功能
    /// </summary>
    public class FormRelationController : MvcControllerBase
    {
        private FormRelationIBLL formRelationIBLL = new FormRelationBLL();
        private ModuleIBLL moduleIBLL = new ModuleBLL();
        private FormSchemeIBLL formSchemeIBLL = new FormSchemeBLL();
        private ISystemFormBll systemFormIBll = new SystemFormBll();


        #region  视图功能
        /// <summary>
        /// 主页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        /// <summary>
        /// 表单弹窗
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FormDialog()
        {
            return View();
        }
        /// <summary>
        /// 添加条件查询字段
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult QueryFieldForm()
        {
            return View();
        }
        /// <summary>
        /// 列表设置
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ColFieldForm()
        {
            return View();
        }

        /// <summary>
        /// 发布的功能页面（主页面）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PreviewIndex(string id)
        {
            string currentUrl = (string)WebHelper.GetHttpItems("currentUrl");
            if (!currentUrl.Contains("cjtype"))
            {
                currentUrl = currentUrl + "?id=" + id;
            }
            WebHelper.UpdateHttpItem("currentUrl", currentUrl);

            return View();
        }
        #endregion

        #region  获取数据
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">关键字</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string keyword)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = formRelationIBLL.GetPageList(paginationobj, keyword);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }

        public ActionResult GetFormList(string pagination, string keyword)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = formRelationIBLL.GetFormList(paginationobj, keyword);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }

        /// <summary>
        /// 获取关系数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var relation = formRelationIBLL.GetEntity(keyValue);
            var module = moduleIBLL.GetModuleEntity(relation.F_ModuleId);

            var jsonData = new
            {
                relation = relation,
                module = module
            };
            return JsonResult(jsonData);
        }

        #endregion

        #region  提交数据
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="relationJson"></param>
        /// <param name="moduleJson"></param>
        /// <param name="moduleColumnJson"></param>
        /// <param name="moduleFormJson"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string relationJson, string moduleJson, string moduleColumnJson, string moduleFormJson)
        {
            keyValue = keyValue.Replace("null", "");
            FormRelationEntity formRelationEntity = relationJson.ToObject<FormRelationEntity>();
            ModuleEntity moduleEntity = moduleJson.ToObject<ModuleEntity>();
            moduleEntity.F_IsMenu = 1;
            moduleEntity.F_EnabledMark = 1;

            List<ModuleButtonEntity> moduleButtonList = new List<ModuleButtonEntity>();

            if (string.IsNullOrEmpty(keyValue))// 新增
            {
                formRelationEntity.Create();
                moduleEntity.F_Target = "iframe";
                moduleEntity.F_UrlAddress = "/FormModule/FormRelation/PreviewIndex?id=" + formRelationEntity.F_Id + "&formId=" + formRelationEntity.F_FormId + "&formType=Customer&cjtype=1";

                #region 添加按钮

                ModuleButtonEntity addButtonEntity = new ModuleButtonEntity();
                addButtonEntity.Create();
                addButtonEntity.F_EnCode = "add";
                addButtonEntity.F_FullName = "新增";
                moduleButtonList.Add(addButtonEntity);
                ModuleButtonEntity editButtonEntity = new ModuleButtonEntity();
                editButtonEntity.Create();
                editButtonEntity.F_EnCode = "edit";
                editButtonEntity.F_FullName = "编辑";
                moduleButtonList.Add(editButtonEntity);
                ModuleButtonEntity deleteButtonEntity = new ModuleButtonEntity();
                deleteButtonEntity.Create();
                deleteButtonEntity.F_EnCode = "delete";
                deleteButtonEntity.F_FullName = "删除";
                moduleButtonList.Add(deleteButtonEntity);

                var audit = new ModuleButtonEntity();
                audit.Create();
                audit.F_EnCode = "audit";
                audit.F_FullName = "审核";
                moduleButtonList.Add(audit);

                var unaudit = new ModuleButtonEntity();
                unaudit.Create();
                unaudit.F_EnCode = "unaudit";
                unaudit.F_FullName = "反审核";
                moduleButtonList.Add(unaudit);

                var send = new ModuleButtonEntity();
                send.Create();
                send.F_EnCode = "send";
                send.F_FullName = "发送";
                moduleButtonList.Add(send);

                #endregion
            }
            else
            {
                moduleEntity.F_UrlAddress = "/FormModule/FormRelation/PreviewIndex?id=" + keyValue + "&formId=" + formRelationEntity.F_FormId + "&formType=Customer&cjtype=1";

                List<ModuleButtonEntity> moduleButtonListByModuleId = moduleIBLL.GetButtonList(formRelationEntity.F_ModuleId);

                foreach (var item in moduleButtonListByModuleId)
                {
                    ModuleButtonEntity addButtonEntity = new ModuleButtonEntity();
                    addButtonEntity.Create();
                    addButtonEntity.F_EnCode = item.F_EnCode;
                    addButtonEntity.F_FullName = item.F_FullName;
                    moduleButtonList.Add(addButtonEntity);
                }
            }

            #region 添加按钮

            //ModuleButtonEntity addButtonEntity = new ModuleButtonEntity();
            //addButtonEntity.Create();
            //addButtonEntity.F_EnCode = "add";
            //addButtonEntity.F_FullName = "新增";
            //moduleButtonList.Add(addButtonEntity);
            //ModuleButtonEntity editButtonEntity = new ModuleButtonEntity();
            //editButtonEntity.Create();
            //editButtonEntity.F_EnCode = "edit";
            //editButtonEntity.F_FullName = "编辑";
            //moduleButtonList.Add(editButtonEntity);
            //ModuleButtonEntity deleteButtonEntity = new ModuleButtonEntity();
            //deleteButtonEntity.Create();
            //deleteButtonEntity.F_EnCode = "delete";
            //deleteButtonEntity.F_FullName = "删除";
            //moduleButtonList.Add(deleteButtonEntity);

            //var audit = new ModuleButtonEntity();
            //audit.Create();
            //audit.F_EnCode = "audit";
            //audit.F_FullName = "审核";
            //moduleButtonList.Add(audit);

            //var unaudit = new ModuleButtonEntity();
            //unaudit.Create();
            //unaudit.F_EnCode = "unaudit";
            //unaudit.F_FullName = "反审核";
            //moduleButtonList.Add(unaudit);

            //var send = new ModuleButtonEntity();
            //send.Create();
            //send.F_EnCode = "send";
            //send.F_FullName = "发送";
            //moduleButtonList.Add(send);

            #endregion


            //var printButtonEntity = new ModuleButtonEntity();
            //printButtonEntity.Create();
            //printButtonEntity.F_EnCode = "print";
            //printButtonEntity.F_FullName = "打印";
            //moduleButtonList.Add(printButtonEntity);

            List<ModuleColumnEntity> moduleColumnList = moduleColumnJson.ToObject<List<ModuleColumnEntity>>();
            List<ModuleFormEntity> moduleFormEntitys = moduleFormJson.ToObject<List<ModuleFormEntity>>();

            moduleIBLL.SaveEntity(formRelationEntity.F_ModuleId, moduleEntity, moduleButtonList, moduleColumnList, moduleFormEntitys);



            formRelationEntity.F_ModuleId = moduleEntity.F_ModuleId;
            formRelationIBLL.SaveEntity(keyValue, formRelationEntity);

            var formUrl = new StringBuilder();
            formUrl.Append("/FormModule/Custmerform/LayerInstanceForm");
            formUrl.Append($"?id={formRelationEntity.F_FormId}");
            formUrl.Append($"&moduleId={moduleEntity.F_ModuleId}");
            systemFormIBll.UpdateInfoByFormId(formRelationEntity.F_FormId, moduleEntity.F_FullName, moduleEntity.F_ModuleId, formUrl.ToString());

            return Success("保存成功！");
        }
        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            var relation = formRelationIBLL.GetEntity(keyValue);
            formRelationIBLL.DeleteEntity(keyValue);
            moduleIBLL.Delete(relation.F_ModuleId);
            return Success("删除成功！");
        }
        #endregion

        #region  扩展方法
        /// <summary>
        /// 获取自定义表单设置内容和表单模板
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        public ActionResult GetCustmerFormData(string keyValue)
        {
            var relation = formRelationIBLL.GetEntity(keyValue);

            FormSchemeInfoEntity schemeInfoEntity = formSchemeIBLL.GetSchemeInfoEntity(relation.F_FormId);
            string F_SchemeId = "";
            if (schemeInfoEntity != null)
            {
                F_SchemeId = schemeInfoEntity.F_SchemeId;
            }
            FormSchemeEntity schemeEntity = formSchemeIBLL.GetSchemeEntity(F_SchemeId);

            var jsonData = new
            {
                relation = relation,
                schemeInfo = schemeInfoEntity,
                scheme = schemeEntity
            };
            return JsonResult(jsonData);
        }
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyValue">主键</param>
        /// <param name="queryJson">关键字</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPreviewPageList(string pagination, string keyValue, string queryJson)
        {
            var relation = formRelationIBLL.GetEntity(keyValue);
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = formSchemeIBLL.GetFormPageList(relation.F_FormId, paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">关键字</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPreviewList(string keyValue, string queryJson)
        {
            var relation = formRelationIBLL.GetEntity(keyValue);
            var data = formSchemeIBLL.GetFormList(relation.F_FormId, queryJson);
            return JsonResult(data);
        }

        [HttpPost, AjaxOnly]
        public ActionResult Audit(string keyValue, string formId)
        {
            formSchemeIBLL.Audit(keyValue, formId);
            return Success("操作成功");
        }

        [HttpPost, AjaxOnly]
        public ActionResult UnAudit(string keyValue, string formId)
        {
            formSchemeIBLL.UnAudit(keyValue, formId);
            return Success("操作成功");
        }
        #endregion
    }
}