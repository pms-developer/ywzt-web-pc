﻿/*
 * 日 期：2017.04.17
 * 描 述：自定义表单
 */
var id = request('id');
 
var keyValue = request('keyValue');
var projectId = request("projectId");
var moduleId = request("moduleId");
var moduleInfoId = "";
var moduleType = "";
var acceptClick;
var subGrid = [];
var mianId = "";
var type = "add";
var formId = request("formId");
var viewState = request("viewState");

// 保存数据
var save;
// 设置权限
var setAuthorize;
// 设置表单数据
var setFormData = function () { };
// 验证数据是否填写完整
var validForm;
// 获取表单数据
var getFormData;

var intputList = [];

var bootstrap = function ($, Changjie) {
    "use strict";
    var formModule;
    var girdCompontMap;
    var page = {
        init: function () {
            if (!!id) {
                $.mkSetForm(top.$.rootUrl + '/FormModule/Custmerform/GetFormData?keyValue=' + id, function (data) {//
                    formModule = JSON.parse(data.schemeEntity.F_Scheme);
                    if (!!keyValue) {
                        mianId = keyValue;
                        type = "edit";
                    }
                    else {
                        mianId = Changjie.newGuid();
                        keyValue = mianId;
                        type = "add";
                    }
                    if (!!projectId) {
                        moduleType = "Project"
                        moduleInfoId = projectId;
                    }
                    else {
                        moduleInfoId = mianId;
                    }
                    var fileop = {
                        moduleType: moduleType, 
                        moduleId: moduleId,
                        moduleInfoId: moduleInfoId, 
                        folderName:""  
                    };
                    girdCompontMap = $('body').mkCustmerFormRender(formModule.data, mianId, type, fileop);
                    if (viewState == "1") {
                        $("#btn_uploadfile").attr('disabled', true);
                        $("#btn_delfile").attr("disabled", true);
                    }
 
                });
            }
            page.initData();
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/FormModule/Custmerform/GetInstanceForm?schemeInfoId=' + id + '&keyValue=' + keyValue, function (data) {//
                    page.setFormData(data);
                });
            }
        },
        setFormData: function (data) {
           
            if (!!formModule) {
                $.each(data, function (tableName, item) {
                    if (!!girdCompontMap[tableName]) {
                        var fieldMap = {};
                        var cells = girdCompontMap[tableName].fieldsData || girdCompontMap[tableName] ;
                        $.each(cells, function (index, girdFiled) {
                           
                            if (!!girdFiled.field) {
                                fieldMap[girdFiled.field.toLowerCase()] = girdFiled.field;
                            }
                        });
                        
                        var rowDatas = [];
                        for (var i = 0, l = item.length; i < l; i++) {
                            var _point = {};
                            for (var _field in item[i]) {
                                _point[fieldMap[_field]] = item[i][_field];
                                //_point[_field] = item[i][_field];
                            }
                            rowDatas.push(_point);
                        }
                        var gridId = girdCompontMap[tableName].id || tableName;

                        subGrid.push({ "tableName": tableName, "gridId": tableName });
                        $('#' + gridId).jfGridSet('refreshdata', { rowdatas: rowDatas }, "edit");

                      
                    }
                    else {
                        $('body').mkSetCustmerformData(item[0], tableName);
                    }
                });
            }
            else {
                setTimeout(function () {
                    page.setFormData(data);
                }, 100);
            }
        }
    };
    page.init();
    
    // 保存调用函数
    acceptClick = function (callBack) {

        if (viewState == "1") {
            Changjie.alert.warning('当前记录正在审核或已审核完毕，无法编辑。');
            return;
        }
         
        if (!$.mkValidCustmerform()) {
            return false;
        }
        
        var formData = $('body').mkGetCustmerformData(keyValue);


        //if (!$.backstageVerify(formData, formId, projectId)) {
        //    return false;
        //}
        
        var deleteList = [];
        var isgridpass = true;
        var errorInfos = [];
        for (var item in subGrid) {
            deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
            var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
            if (!info.isPass) {
               
                isgridpass = false;
                errorInfos.push(info.errorCells);
            }
        }

        if (!isgridpass) {
            for (var i in errorInfos[0]) {
                
                Changjie.alert.error(errorInfos[0][i].Msg);
            }
            return false;
        }

      
        var postData =
        {
            "formData": JSON.stringify(formData),
            "deleteList": JSON.stringify(deleteList),
        };
        postData.projectId = projectId;
        $.mkSaveForm(top.$.rootUrl + '/FormModule/Custmerform/SaveFormInfo?keyValue=' + keyValue + "&schemeInfoId=" + id + "&type=" + type, postData, function (res) {

            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    }

    // 保存调用函数
    save = function (processId, callBack, i) {
        if (isUpdate) {
            keyValue = processId;
        }
        var formData = $('body').mkGetCustmerformData(keyValue);

        if (!!processIdName) {
            formData[processIdName] = processId;
        }
        var postData =
        {
            formData: JSON.stringify(formData)
        };
        $.mkSaveForm(top.$.rootUrl + '/FormModule/Custmerform/SaveInstanceForm?keyValue=' + keyValue + "&schemeInfoId=" + id + '&processIdName=' + processIdName, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack(res, formData, i);
            }
        });
    };
    // 设置权限
    setAuthorize = function (data) {
         if (!!data == false) return;
        $("#btn_uploadfile").attr("disabled", "disabled");
        $("#btn_delfile").attr("disabled", "disabled");
        $(".fa.fa-plus").css("display", "none");
        $(".fa.fa-minus").css("display", "none");
        $(".fa.fa-file-excel-o").css("display", "none");
        if (!!formModule) {
            var girdMap = {};
            var _flag = false;
            for (var i = 0, l = data.length; i < l; i++) {
                var field = data[i];
                var _ids = field.fieldId.split('|');
                if (_ids.length > 1) {
                    if (field.isLook != 1 || field.isEdit != 1) {
                        girdMap[_ids[0]] = girdMap[_ids[0]] || {};
                        girdMap[_ids[0]][_ids[1]] = field;
                        _flag = true;
                    }
                }
            }
            if (_flag) {
                $.each(formModule.data, function (_i, _item) {
                    $.each(_item.componts, function (_j, _jitem) {
                        if (_jitem.type == 'gridtable' && !!girdMap[_jitem.id]) {
                            var _gird = girdMap[_jitem.id];
                            var _fieldsData = [];
                            $.each(_jitem.fieldsData, function (_m, _mitem) {
                                if (!_gird[_mitem.id] || _gird[_mitem.id].isLook == 1) {
                                    _fieldsData.push(_mitem);
                                    if (!!_gird[_mitem.id] && _gird[_mitem.id].isEdit != 1) {
                                        _mitem._isEdit = 1;
                                    }
                                }
                            });
                            _jitem.fieldsData = _fieldsData;
                        }
                    });
                });
            }

            girdCompontMap = $('body').mkCustmerFormRender(formModule.data, infoId, "edit");
            for (var i = 0, l = data.length; i < l; i++) {
                var field = data[i];
                var _ids = field.fieldId.split('|');
                if (_ids.length == 1) {
                    if (field.isLook != 1) {// 如果没有查看权限就直接移除
                        $('#' + _ids[0]).parent().remove();
                    }
                    else {
                        if (field.isEdit != 1) {
                            $('#' + _ids[0]).attr('disabled', 'disabled');
                            if ($('#' + _ids[0]).hasClass('mkUploader-wrap')) {
                                $('#' + _ids[0]).css({ 'padding-right': '58px' });
                                $('#' + _ids[0]).find('.btn-success').remove();
                            }
                        }
                    }
                }
            }
        }
        else {
            setTimeout(function () {
                setAuthorize(data);
            }, 100);
        }
    };

    // 验证数据是否填写完整
    validForm = function () {
        if (!$.mkValidCustmerform()) {
            return false;
        }
        return true;
    };

    // 获取表单数据
    getFormData = function () {
        return $('body').mkGetCustmerformData(keyValue);
    }
}

