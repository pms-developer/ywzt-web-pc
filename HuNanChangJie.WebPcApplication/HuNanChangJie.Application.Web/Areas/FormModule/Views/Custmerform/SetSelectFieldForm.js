﻿/*
 * 日 期：2017.04.11
 * 描 述：表格选择项字段选择	
 */
var dbId = request('dbId');
var tableName = request('tableName');

var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var selectFieldData = top.layer_SetFieldForm.selectFieldData;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            // 绑定字段
            $('#value').mkselect({
                value: 'f_column',
                text: 'f_column',
                title: 'f_remark',
                allowSearch: true,
                maxHeight:160
            });
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldList', { databaseLinkId: dbId, tableName: tableName }, function (data) {
                $('#value').mkselectRefresh({
                    data: data
                });
            });
            // 对齐方式
            $('#align').mkselect({ placeholder: false }).mkselectSet('left');
            // 是否隐藏
            $('#hide').mkselect({ placeholder: false }).mkselectSet('0');
        },
        initData: function () {
            if (!!selectFieldData)
            {
                $('#form').mkSetFormData(selectFieldData);
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData();
        callBack(postData);

        return true;
    };
    page.init();
}