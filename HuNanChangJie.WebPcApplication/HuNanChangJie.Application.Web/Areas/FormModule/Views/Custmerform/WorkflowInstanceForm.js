﻿/*
 * 日 期：2017.04.17
 * 描 述：自定义表单-用于工作流
 */
var id = request('id');//表单id
var keyValue = request("keyValue"); // 信息ID
var infoId = request("infoId");     // 信息ID    
var formId = request("formId");//表单id
var processIdName = "";
var isUpdate = false;

// 保存数据
var save;
// 设置权限
var setAuthorize;
// 设置表单数据
var setFormData;
// 验证数据是否填写完整
var validForm;
// 获取表单数据
var getFormData;

var auditPassEvent;
var unauditPassEvent;


var bootstrap = function ($, Changjie) {
    "use strict";
    var formModule;
    var girdCompontMap;

    var page = {
        init: function () {
            if (!!id) {
                $.mkSetForm(top.$.rootUrl + '/FormModule/Custmerform/GetFormData?keyValue=' + id, function (data) {//
                    formModule = JSON.parse(data.schemeEntity.F_Scheme);
                });
            }
        },
        setFormData: function (data) {
            if (!!formModule && !!girdCompontMap) {
                $.each(data, function (id, item) {
                    if (!!girdCompontMap[id]) {
                        var fieldMap = {};
                        $.each(girdCompontMap[id], function (id, girdFiled) {
                            if (!!girdFiled.field) {
                               
                                fieldMap[girdFiled.field.toLowerCase()] = girdFiled.field;
                            }
                        });
                        var rowDatas = [];
                        for (var i = 0, l = item.length; i < l; i++) {
                            var _point = {};
                            for (var _field in item[i]) {
                                _point[fieldMap[_field]] = item[i][_field];
                            }
                            rowDatas.push(_point);
                        }
                        if (rowDatas.length > 0) {
                            isUpdate = true;
                        }
                        $('#' +id).jfGridSet('refreshdata', { rowdatas: rowDatas });
                    }
                    else {
                        if (!!item[0]) {
                            isUpdate = true;
                            $('body').mkSetCustmerformData(item[0], id);
                        }
                    }
                    
                });
                $("#btn_uploadfile").attr("disabled", "disabled");
                $("#btn_delfile").attr("disabled", "disabled");
                $(".fa.fa-plus").css("display", "none");
                $(".fa.fa-minus").css("display", "none");
                $(".fa.fa-file-excel-o").css("display", "none");
            }
            else {
                setTimeout(function () {
                    page.setFormData(data);
                }, 100);
            }
        }
    };
    page.init();

    // 保存调用函数
    save = function (processId, callBack, i) {
        keyValue = infoId;
        var formData = $('body').mkGetCustmerformData(keyValue);

        if (!!processIdName) {
            formData[processIdName] = processId;
        }
        var postData =
            {
                formData: JSON.stringify(formData)
            };
        $.mkSaveForm(top.$.rootUrl + '/FormModule/Custmerform/SaveInstanceForm?keyValue=' + keyValue + "&schemeInfoId=" + id + '&processIdName=' + processIdName, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack(res, formData, i);
            }
        });
    };
    // 设置权限
    setAuthorize = function (data) {
        if (!!data == false) return;
        $("#btn_uploadfile").attr("disabled", "disabled");
        $("#btn_delfile").attr("disabled", "disabled");
        $(".fa.fa-plus").css("display", "none");
        $(".fa.fa-minus").css("display", "none");
        $(".fa.fa-file-excel-o").css("display", "none");
        if (!!formModule) {
            girdCompontMap = $('body').mkCustmerFormRender(formModule.data, infoId, "edit");
            for (var i = 0, l = data.length; i < l; i++) {
                var info = data[i];
                var inputId = info.id;
                var $input = $("#" + inputId);

                if (info.isWrite == 1) {
                    $input.removeAttr("disabled");
                }
                else {
                    if (info.isRead != 1) {
                        $input.parent().remove();
                    }
                    else {
                        $input.attr("disabled", "disabled");
                    }
                }
            }
        }
        else {
            setTimeout(function () {
                setAuthorize(data);
            }, 100);
        }
    };

    // 设置表单数据
    setFormData = function (processId) {
        if (!!processId) {
            $.mkSetForm(top.$.rootUrl + '/FormModule/Custmerform/GetInstanceForm?schemeInfoId=' + id + '&keyValue=' + processId + '&processIdName=' + processIdName, function (data) {//
                page.setFormData(data);
            });
        }
    };

    // 验证数据是否填写完整
    validForm = function () {
        if (!$.mkValidCustmerform()) {
            return false;
        }
        return true;
    };

    // 获取表单数据
    getFormData = function () {
        return $('body').mkGetCustmerformData(keyValue);
    }

    auditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/FormModule/FormRelation/Audit', { keyValue: keyValue, formId: formId }, function (data) {
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/FormModule/FormRelation/UnAudit', { keyValue: keyValue, formId: formId }, function (data) {
        });
    };
}