﻿/*
 * 创建人：
 * 日 期：2017.04.05
 * 描 述：自定义表单设计	
 */
var keyValue = request('keyValue');
var categoryId = request('categoryId');
var dbTable = [];
var dbId = '';
var subSql = null;
var selectedRow = null;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        /*绑定事件和初始化控件*/
        bind: function () {
            // 加载导向
            $('#wizard').wizard().on('change', function (e, data) {
                var $finish = $("#btn_finish");
                var $next = $("#btn_next");
                if (data.direction == "next") {
                    if (data.step == 1) {
                        if (!$('#step-1').mkValidform()) {
                            return false;
                        }
                    } else if (data.step == 2) {
                        dbTable = $('#gridtable').jfGridGet('rowdatas');
                        if (dbId == '' || dbTable.length == 0) {
                            Changjie.alert.error('至少选择一张数据表');
                            return false;
                        }
                        var parameter = { dbId: dbId, dbTable: dbTable}
                        if (!keyValue) {
                            var dfop = $('#step-3').mkCustmerFormDesigner('loadDefaultFields', parameter);
                            $('#step-3').mkCustmerFormDesigner("set", dfop);
                        }
                        $('#step-3').mkCustmerFormDesigner('updatedb', parameter);
                    }
                    else if (data.step == 4)
                    {
                        var $tabs = $("#subTabs");
                        var $tabcontext = $("#sub_tab_content");
                        for (var i in dbTable) {
                            var row = dbTable[i];
                            if (row.isMain == "1") continue;
                            var isexist = false;
                            $tabs.find('li').each(function () {
                                var tabname = $(this).find("a").attr("data-value");
                                if (tabname == row.name) {
                                    isexist = true;
                                    return;
                                }
                            });
                            if (isexist == false) {
                                $tabs.append("<li><a data-value='" + row.name + "'>" + row.name + "(子表)</a></li>");
                                var $context = $('<div class="mk-form-wrap tab-pane" id="' + row.name + '"></div>');
                                $tabcontext.append($context);
                                var subid = "SubAuditSql_" + row.name;
                                var subUnid = "SubUnauditSql_" + row.name;
                                var html = '<div>' +
                                    '<div style="float: left;width: 150px;text-align: right;padding-right: 12px;">审核SQL</div>' +
                                    '<div style="float: left;width: 85%;">' +
                                    '<textarea id="' + subid + '" name="' + row.name+'" class="form-control" style="height: 150px;"></textarea>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div style="clear:both"></div>' +
                                    '<div style="margin-top:18px">' +
                                    '<div style="float: left;width: 150px;text-align: right;padding-right: 12px;">反审核SQL</div>' +
                                    '<div style="float: left;width: 85%;">' +
                                    '<textarea id="' + subUnid + '" name="' + row.name +'" class="form-control" style="height: 150px;"></textarea>' +
                                    '</div>' +
                                    '</div>';
                                $context.append(html);
                                for (var item in subSql) {
                                    var info = subSql[item];
                                    if (info.SubTable == row.name) {
                                        $("#" + subid).val(info.Sql);
                                        $("#" + subUnid).val(info.UnSql);
                                    }
                                }
                                $("#" + subid).on("input propertychange", function () {
                                    
                                    var $this = $(this);
                                    var name = $this.attr("name");
                                    for (var item in subSql) {
                                        var info = subSql[item];
                                        if (info.SubTable == name) {
                                            info.Sql = $this.val();
                                        }
                                    }
                                });

                                $("#" + subUnid).on("input propertychange", function () {
                                    var $this = $(this);
                                    var name = $this.attr("name");
                                    for (var item in subSql) {
                                        var info = subSql[item];
                                        if (info.SubTable == name) {
                                            info.UnSql = $this.val();
                                        }
                                    }
                                });
                                
                            }
                        }

                        
                        $('#form_tabs_sub').mkFormTab();
                        $('#form_tabs_sub ul li').eq(0).trigger('click');

                        $finish.removeAttr('disabled');
                        $next.attr('disabled', 'disabled');
                    }
                    else {
                        $finish.attr('disabled', 'disabled');
                    }
                }
                else {
                    $finish.attr('disabled', 'disabled');
                    $next.removeAttr('disabled');
                }
            });

            // 数据库表选择
            $('#F_DbId').mkselect({
                url: top.$.rootUrl + '/SystemModule/DatabaseLink/GetTreeList',
                type: 'tree',
                placeholder:'请选择数据库',
                allowSearch: true,
                select: function (item) {
                    if (item.hasChildren) {
                        dbId = '';
                        dbTable = [];
                        $('#gridtable').jfGridSet('refreshdata', []);
                    }
                    else if (dbId != item.id) {
                        dbId = item.id;
                        dbTable = [];
                        $('#gridtable').jfGridSet('refreshdata', []);
                    }
                }
            });
            // 数据库表选择
            $('#Audit_DbId').mkselect({
                url: top.$.rootUrl + '/SystemModule/DatabaseLink/GetTreeList',
                type: 'tree',
                placeholder: '请选择数据库',
                allowSearch: true,
                select: function (item) {
                    if (item.hasChildren) {
                        dbId = '';
                        dbTable = [];
                        $('#gridtable').jfGridSet('refreshdata', []);
                    }
                    else if (dbId != item.id) {
                        dbId = item.id;
                        dbTable = [];
                        $('#gridtable').jfGridSet('refreshdata', []);
                    }
                }
            });
            $('#F_Category').mkDataItemSelect({ code: 'FormSort' });
            $('#F_Category').mkselectSet(categoryId);
            // 新增
            $('#db_add').on('click', function () {
                dbTable = $('#gridtable').jfGridGet('rowdatas');
                selectedRow = null;
                Changjie.layerForm({
                    id: 'DataTableForm',
                    title: '添加数据表',
                    url: top.$.rootUrl + '/FormModule/Custmerform/DataTableForm?dbId=' + dbId,
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            var rows = $('#gridtable').jfGridGet("rowdatas");
                            var isSetMainTable = false;
                            for (var item in rows) {
                                if (rows[item].isMain == "1") {
                                    isSetMainTable = true;
                                    break;
                                }
                            }
                            if (!isSetMainTable) {
                                data.isMain = "1";
                            }
                            else {
                                data.isMain = "0";
                            }
                            $('#gridtable').jfGridSet('addRow', data);
                        });
                    }
                });
            });
            // 编辑
            $('#db_edit').on('click', function () {
                dbTable = $('#gridtable').jfGridGet('rowdatas');
                selectedRow = $('#gridtable').jfGridGet('rowdata');
                var keyValue = $('#gridtable').jfGridValue('name');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'DataTableForm',
                        title: '编辑数据表',
                        url: top.$.rootUrl + '/FormModule/Custmerform/DataTableForm?dbId=' + dbId,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                $.extend(selectedRow, data);
                                $('#gridtable').jfGridSet('updateRow');
                            });
                        }
                    });
                }
            });
            // 删除
            $('#db_delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('name');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res, index) {
                        if (res) {
                            $('#gridtable').jfGridSet('removeRow', keyValue);
                            top.layer.close(index); //再执行关闭  
                        }
                    });
                }
            });



            $('#gridtable').jfGrid({
                headData: [
                    { label: "数据表名", name: "name", width: 150, align: "left" },
                    { label: "数据表字段", name: "field", width: 150, align: "left" },
                    { label: "被关联表", name: "relationName", width: 150, align: "left" },
                    { label: "被关联表字段", name: "relationField", width: 150, align: "left" },
                    {
                        label: "是否主表", name: "isMain", width: 150, align: "left",
                        formatter: function (cellvalue) {
                            return cellvalue == 1 ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            change: function (rowdata,index,oldValue,colname,alldatarows ) {
                                if (rowdata.isMain == "1") {
                                    for (var item in alldatarows) {
                                        if (item != index) {
                                            alldatarows[item].isMain = '0';
                                        }
                                    }
                                }
                                else {
                                    var isSetMainTable = false;
                                    for (var item in alldatarows) {
                                        if (alldatarows[item].isMain == "1") {
                                            isSetMainTable = true;
                                            break;
                                        }
                                    }
                                    if (!isSetMainTable) {
                                        rowdata.isMain = "1";
                                        Changjie.alert.warning("值编辑失败,必须设置一张主表");
                                    }
                                }
                                $("#gridtable").jfGridSet("refreshdata");
                            },
                            data: [{ "id": '0', "text": "否" }, { "id": "1", "text": "是" }],
                            dfvalue: '0'

                        }
                    }
                ],
                mainId: 'name',
                reloadSelected: true
            });

 

            
            // 设计页面初始化
            $('#step-3').mkCustmerFormDesigner('init');
            // 保存数据按钮
            $("#btn_finish").on('click', page.save);
            $("#btn_draft").on('click', page.draftsave);
        },

        /*初始化数据*/
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/FormModule/Custmerform/GetFormData?keyValue=' + keyValue, function (data) {//
                    $('#step-1').mkSetFormData(data.schemeInfoEntity);
                    var scheme = JSON.parse(data.schemeEntity.F_Scheme);
                    $('#F_DbId').mkselectSet(scheme.dbId);
                    $('#gridtable').jfGridSet('refreshdata', scheme.dbTable);
                    $('#step-3').mkCustmerFormDesigner('set', scheme);
                    $("#AuditSql").val(scheme.AuditSql);
                    $("#UnauditSql").val(scheme.UnauditSql);
                    subSql = scheme.SubAuditSql;
                });
            }
        },
        /*保存数据*/
        save: function () {
            var schemeInfo = $('#step-1').mkGetFormData(keyValue);
            if (!$('#step-3').mkCustmerFormDesigner('valid')) {
                return false;
            }
            var scheme = $('#step-3').mkCustmerFormDesigner('get');
            scheme.AuditSql = $("#AuditSql").val();
            scheme.UnauditSql = $("#UnauditSql").val();

            scheme.SubAuditSql = [];
            for (var i in dbTable) {
                var row = dbTable[i];
                if (row.isMain == "1") continue;
                var item = {};
                item.SubTable = row.name;
                item.Sql = $("#SubAuditSql_" + row.name + "").val();
                item.UnSql = $("#SubUnauditSql_" + row.name + "").val();
                scheme.SubAuditSql.push(item);
            }

           
            schemeInfo.F_Type = 0;
            schemeInfo.F_EnabledMark = 1;
            var postData = {
                keyValue: keyValue,
                schemeInfo: JSON.stringify(schemeInfo),
                scheme: JSON.stringify(scheme),
                type: 1
            };
            $.mkSaveForm(top.$.rootUrl + '/FormModule/Custmerform/SaveForm', postData, function (res) {
                // 保存成功后才回调
                Changjie.frameTab.currentIframe().refreshGirdData();
            });
        },
        /*保存草稿数据*/
        draftsave: function () {
            var schemeInfo = $('#step-1').mkGetFormData(keyValue);
            var scheme = $('#step-3').mkCustmerFormDesigner('get');
            scheme.AuditSql = $("#AuditSql").val();
            scheme.UnauditSql = $("#UnauditSql").val();
            dbTable = $('#gridtable').jfGridGet('rowdatas');
            scheme.dbId = dbId;
            scheme.dbTable = dbTable;
            schemeInfo.F_EnabledMark = 0;
            schemeInfo.F_Type = 0;
            var postData = {
                keyValue: keyValue,
                schemeInfo: JSON.stringify(schemeInfo),
                scheme: JSON.stringify(scheme),
                type: 2
            };

            $.mkSaveForm(top.$.rootUrl + '/FormModule/Custmerform/SaveForm', postData, function (res) {
                // 保存成功后才回调
                Changjie.frameTab.currentIframe().refreshGirdData();
            });
        }
    };

    page.init();
}