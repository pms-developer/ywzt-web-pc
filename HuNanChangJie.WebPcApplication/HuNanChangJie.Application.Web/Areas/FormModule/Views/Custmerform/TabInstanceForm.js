﻿/*
 * 日 期：2017.04.17
 * 描 述：自定义表单
 */
var id = request('id');
var keyValue = request('keyValue');
var projectId = request("projectId");
var acceptClick;
var subGrid = [];
var mianId = "";
var type = "add";
var bootstrap = function ($, Changjie) {
    "use strict";
    var formModule;
    var girdCompontMap;

    var page = {
        init: function () {
            if (!!id) {
                $.mkSetForm(top.$.rootUrl + '/FormModule/Custmerform/GetFormData?keyValue=' + id, function (data) {//
                    formModule = JSON.parse(data.schemeEntity.F_Scheme);
                    if (!!keyValue) {
                        mianId = keyValue;
                        type = "edit";
                    }
                    else {
                        mianId = Changjie.newGuid();
                        type = "add";
                    }

                    girdCompontMap = $('body').mkCustmerFormRender(formModule.data, mianId, type);
                    page.bind();
                });
            }
            page.initData();

        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/FormModule/Custmerform/GetInstanceForm?schemeInfoId=' + id + '&keyValue=' + keyValue, function (data) {//
                    page.setFormData(data);
                });
            }
        },
        setFormData: function (data) {
            if (!!formModule) {
                $.each(data, function (tableName, item) {

                    if (!!girdCompontMap[tableName]) {
                        var fieldMap = {};
                        $.each(girdCompontMap[tableName], function (index, girdFiled) {
                            if (!!girdFiled.field) {
                                fieldMap[girdFiled.field.toLowerCase()] = girdFiled.field;
                            }
                        });
                        var rowDatas = [];
                        for (var i = 0, l = item.length; i < l; i++) {
                            var _point = {};
                            for (var _field in item[i]) {
                                _point[fieldMap[_field]] = item[i][_field];
                            }
                            rowDatas.push(_point);
                        }
                        var gridId = tableName;
                        subGrid.push({ "tableName": tableName, "gridId": tableName });
                        $('#' + gridId).jfGridSet('refreshdata', { rowdatas: rowDatas }, "edit");


                    }
                    else {
                        $('body').mkSetCustmerformData(item[0], tableName);
                    }
                });
            }
            else {
                setTimeout(function () {
                    page.setFormData(data);
                }, 100);
            }
        },
        bind: function () {
            // 保存数据
            $('#savaAndAdd').on('click', function () {
                acceptClick(0);
            });
            $('#save').on('click', function () {
                acceptClick(1);
            });
        }
    };
    page.init();

    // 保存数据
    acceptClick = function (type) {// 0保存并新增 1保存

        if (viewState == "1") {
            Changjie.alert.warning('当前记录正在审核或已审核完毕，无法编辑。');
            return;
        }

        if (!$.mkValidCustmerform()) {
            return false;
        }
        var formData = $('body').mkGetCustmerformData(keyValue);

        var deleteList = [];
        var isgridpass = true;
        var errorInfos = [];
        for (var item in subGrid) {
            deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
            var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
            if (!info.isPass) {

                isgridpass = false;
                errorInfos.push(info.errorCells);
            }

        }
        if (!isgridpass) {
            for (var i in errorInfos[0]) {

                Changjie.alert.error(errorInfos[0][i].Msg);
            }
            return false;
        }

        var postData =
        {
            formData: JSON.stringify(formData),
            deleteList: JSON.stringify(deleteList),
        };
        postData.projectId = projectId;
        $.mkSaveForm(top.$.rootUrl + '/FormModule/Custmerform/SaveFormInfo?keyValue=' + keyValue + "&schemeInfoId=" + id, postData, function (res) {
            if (res.code == 200) {
                Changjie.frameTab.parentIframe().refreshGirdData();
                if (type == 0) {
                    window.location.href = top.$.rootUrl + '/FormModule/Custmerform/TabInstanceForm?id=' + id;
                }
                else {
                    Changjie.frameTab.close(Changjie.frameTab.iframeId);
                }
            }
        });
    };
}