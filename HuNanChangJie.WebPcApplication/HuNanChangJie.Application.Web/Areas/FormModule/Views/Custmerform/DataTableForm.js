﻿/*
 * 创建人：
 * 日 期：2017.04.11
 * 描 述：表单设计数据表添加	
 */
var dbId = request('dbId');

var selectedRow = top.layer_Form.selectedRow;
var dbTable = top.layer_Form.dbTable;


var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#field').mkselect({
                value: 'f_column',
                text: 'f_column',
                title: 'f_remark',
                allowSearch: true
            });
            $('#relationField').mkselect({
                value: 'f_column',
                text: 'f_column',
                title: 'f_remark',
                allowSearch: true
            });
            $('#name').mkselect({
                url: top.$.rootUrl + '/SystemModule/DatabaseTable/GetList',
                param: { databaseLinkId: dbId },
                value: 'name',
                text: 'name',
                title: 'tdescription',
                allowSearch: true,
                select: function (item) {
                    if (!!item) {
                        $('#field').mkselectRefresh({
                            url: top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldList',
                            param: { databaseLinkId: dbId, tableName: item.name }
                        });
                    }
                }
            });
            $('#relationName').mkselect({
                data: dbTable,
                param: { databaseLinkId: dbId },
                value: 'name',
                text: 'name',
                maxHeight: 160,
                allowSearch: true,
                select: function (item) {
                    if (!!item) {
                        $('#relationField').mkselectRefresh({
                            url: top.$.rootUrl + '/SystemModule/DatabaseTable/GetFieldList',
                            param: { databaseLinkId: dbId, tableName: item.name }
                        });
                    }
                }
            });
        },
        initData: function () {
            if (!!selectedRow) {
                $('#form').mkSetFormData(selectedRow);
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }

        var data = $('#form').mkGetFormData();
        if (data.name == data.relationName)
        {
            Changjie.alert.error('关联表不能是自己本身！');
            return false;
        }
        if (!!callBack) {
            callBack(data);
        }
        return true;
    };
    page.init();
}