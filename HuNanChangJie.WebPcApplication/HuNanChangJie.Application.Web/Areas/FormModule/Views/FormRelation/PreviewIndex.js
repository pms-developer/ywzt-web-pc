﻿/*
 * 日 期：2017.04.17
 * 描 述：自定义表单发布功能	
 */
var id = request('id');
var formId = request("formId");
var refreshGirdData; // 更新数据

var formScheme;
var settingJson;
var relation;
var schemeInfo_F_Name;

var mainTablePk = "";
var mainTable = "";
var mainCompontId = "";
var projectId = request("projectId");
var moduleId = request("moduleId");
var auditPassEvent;
var unauditPassEvent;
var bootstrap = function ($, Changjie) {
    "use strict";

    var queryJson = {};

    var page = {
        init: function () {
            // 获取自定义表单设置内容
            Changjie.httpAsync('GET', top.$.rootUrl + '/FormModule/FormRelation/GetCustmerFormData', { keyValue: id }, function (data) {
                relation = data.relation;
                settingJson = JSON.parse(data.relation.F_SettingJson);
                formScheme = JSON.parse(data.scheme.F_Scheme);
                schemeInfo_F_Name = data.schemeInfo.F_Name;

                for (var i = 0, l = formScheme.dbTable.length; i < l; i++) {
                    var tabledata = formScheme.dbTable[i];
                    if (tabledata.relationName == "") {
                        mainTable = tabledata.name;
                        mainTablePk = tabledata.field;
                    }
                }

                // 条件项设置
                if (settingJson.query.isDate == '1' && !!settingJson.query.DateField) {// 时间搜索框
                    $('#datesearch').mkdate({
                        dfdata: [
                            { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                            { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                            { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                            { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                        ],
                        // 月
                        mShow: false,
                        premShow: false,
                        // 季度
                        jShow: false,
                        prejShow: false,
                        // 年
                        ysShow: false,
                        yxShow: false,
                        preyShow: false,
                        yShow: false,
                        // 默认
                        dfvalue: '2',
                        selectfn: function (begin, end) {
                            queryJson.mkbegin = begin;
                            queryJson.mkend = end;
                            queryJson.mkdateField = settingJson.query.DateField;

                            page.search();
                        }
                    });
                }

                // 复合条件查询
                if (!!settingJson.query.fields && settingJson.query.fields.length > 0) {
                    $('#multiple_condition_query_item').html('<div id="multiple_condition_query"><div class="mk-query-formcontent"></div></div>');
                    $('#multiple_condition_query').mkMultipleQuery(function (_queryJson) {
                        queryJson = _queryJson;
                        page.search();
                    }, 220);
                    var $content = $('#multiple_condition_query .mk-query-content');

                    var queryFieldMap = {};
                    $.each(settingJson.query.fields, function (id, item) {
                        queryFieldMap[item.fieldId] = item;
                    });
                    for (var i = 0, l = formScheme.data.length; i < l; i++) {
                        var componts = formScheme.data[i].componts;
                        for (var j = 0, jl = componts.length; j < jl; j++) {
                            var item = componts[j];
                            var queryItem = queryFieldMap[item.id];
                            if (!!queryItem) {
                                queryItem.compont = item;
                            }
                        }
                    }
                    $.each(queryFieldMap, function (id, item) {
                        if (!!item.compont) {
                            var $row = $('<div class="col-xs-' + (12 / parseInt(item.portion)) + ' mk-form-item" ></div>');
                            var $title = $(' <div class="mk-form-item-title language">' + item.fieldName + '</div>');
                            $row.append($title);
                            $content.append($row);
                            $.mkFormComponents[item.compont.type].renderQuery(item.compont, $row)[0].compont = item.compont;
                        }
                    });
                }
                // 列表设置
                page.initGrid();
                // 按钮绑定事件
                page.bind();
            });
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                var url = ""
                if (!!projectId) {
                    url = top.$.rootUrl + '/FormModule/Custmerform/LayerInstanceForm?id=' + relation.F_FormId + '&projectId=' + projectId + '&moduleId=' + moduleId + '&formId=' + formId;
                }
                else {
                    url = top.$.rootUrl + '/FormModule/Custmerform/LayerInstanceForm?id=' + relation.F_FormId + '&moduleId=' + moduleId + '&formId=' + formId;
                }
                if (settingJson.layer.opentype == '1') {// 窗口弹层页
                    var width = 800;
                    if (settingJson && settingJson.layer && settingJson.layer.width > 800) { width = settingJson.layer.width; }  
                    console.log(settingJson.layer)
                    Changjie.layerForm({
                        id: 'Form',
                        title: '新增',
                        url: url,
                        width: width,
                        height: settingJson.layer.height,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
                else {// 窗口页
                    var url = ""
                    if (!!projectId) {
                        url = '/FormModule/Custmerform/TabInstanceForm?id=' + relation.F_FormId + '&projectId=' + projectId + '&moduleId=' + moduleId;
                    }
                    else {
                        url = '/FormModule/Custmerform/TabInstanceForm?id=' + relation.F_FormId + '&moduleId=' + moduleId;
                    }
                    Changjie.frameTab.open({
                        F_ModuleId: id,
                        F_Icon: 'fa fa-pencil-square-o',
                        F_FullName: '新增',
                        F_UrlAddress: url
                    });
                }
            });
            // 编辑
        },

        initGrid: function () {
            var colFieldMap = {};
            for (var i = 0, l = settingJson.col.fields.length; i < l; i++) {
                colFieldMap[settingJson.col.fields[i].compontId] = settingJson.col.fields[i];
            }

            var headData = [];

            var tableIndex = 0;
            var tableMap = {};

            for (var i = 0, l = formScheme.data.length; i < l; i++) {
                var componts = formScheme.data[i].componts;

                for (var j = 0, jl = componts.length; j < jl; j++) {
                    var item = componts[j];
                    if (!!item.table && tableMap[item.table] == undefined) {
                        tableMap[item.table] = tableIndex;
                        tableIndex++;
                    }

                    if (mainTable == item.table && mainTablePk == item.field) {
                        mainCompontId = item.field + tableMap[item.table];
                    }

                    var colItem = colFieldMap[item.id];
                    if (!!colItem) {
                        colItem.compont = item;
                    }
                }
            }

            $.each(colFieldMap, function (id, item) {
                if (!!item.compont) {
                    if (item.compont.field == "HTzzrq" || item.compont.field == "HTsxrq") {
                        var point = {
                            label: item.fieldName, name: (item.compont.field + tableMap[item.compont.table]).toLowerCase(), width: parseInt(item.width), align: item.align,
                            formatter: function (cellvalue, row) {
                                var ct = "";
                                if (cellvalue) {
                                    var currentTime = new Date();
                                    var customTime = cellvalue;
                                    customTime = customTime.replace("-", "/");
                                    customTime = new Date(Date.parse(customTime));

                                    if (item.compont.field == "HTzzrq") {
                                        if (currentTime > customTime) {
                                            ct = "<span style='font-size:13px;font-weight:bold;color:red'>" + Changjie.formatDate(cellvalue, 'yyyy-MM-dd') + "</span>";
                                        }
                                        else {
                                            ct = Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                                        }
                                    } else {
                                        ct = Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                                    }
                                }
                                return ct;
                            }
                        };
                    } else {
                        var point = {
                            label: item.fieldName, name: (item.compont.field + tableMap[item.compont.table]).toLowerCase(), width: parseInt(item.width), align: item.align
                        }
                    }

                    //if (item.fieldName == "")
                    switch (item.compont.type) {

                        case 'radio':
                        case 'checkbox':
                        case 'select':
                            if (item.compont.dataSource == "0") {
                                point.formatterAsync = function (callback, value, row) {
                                    Changjie.clientdata.getAsync('dataItem', {
                                        key: value,
                                        code: item.compont.itemCode,
                                        callback: function (_data) {
                                            callback(_data.text);
                                        }
                                    });
                                }
                            }
                            else {
                                var vlist = item.compont.dataSourceId.split(',');
                                point.formatterAsync = function (callback, value, row) {
                                    Changjie.clientdata.getAsync('sourceData', {
                                        key: value,
                                        keyId: vlist[2],
                                        code: vlist[0],
                                        callback: function (_data) {
                                            callback(_data[vlist[1]]);
                                        }
                                    });
                                }
                            }
                            break;
                        case 'organize':
                        case 'currentInfo':
                            debugger
                            if (item.compont.dataType == 'user') {

                                point.formatterAsync = function (callback, value, row) {
                                    Changjie.clientdata.getAsync('user', {
                                        key: value,
                                        callback: function (item) {
                                            callback(item.name);
                                        }
                                    });
                                }
                            }
                            else if (item.compont.dataType == 'company') {
                                point.formatterAsync = function (callback, value, row) {
                                    Changjie.clientdata.getAsync('company', {
                                        key: value,
                                        callback: function (_data) {
                                            callback(_data.name);
                                        }
                                    });
                                }
                            }
                            else if (item.compont.dataType == 'department') {
                                point.formatterAsync = function (callback, value, row) {
                                    Changjie.clientdata.getAsync('department', {
                                        key: value,
                                        callback: function (item) {
                                            callback(item.name);
                                        }
                                    });
                                }
                            }
                            break;
                    }
                    headData.push(point);
                }
            });

            var girdurl = "";
            if (settingJson.col.isPage == "1") {
                girdurl = top.$.rootUrl + '/FormModule/FormRelation/GetPreviewPageList?keyValue=' + id;
            }
            else {
                girdurl = top.$.rootUrl + '/FormModule/FormRelation/GetPreviewList?keyValue=' + id;
            }

            $('#gridtable').mkAuthorizeJfGrid({
                url: girdurl,
                headData: headData,
                reloadSelected: true,
                isPage: (settingJson.col.isPage == "1" ? true : false),
                mainId: mainCompontId.toLowerCase(),
                bindTable: mainTable,
                enableWorkflow: true,
            });

            page.search();
        },
        search: function (param) {
            param = param || {};
            if (!!projectId) {
                queryJson.ProjectID = projectId;
            }
            param.queryJson = JSON.stringify(queryJson);

            $('#gridtable').jfGridSet('reload', param);
        }
    };

    // 保存数据后回调刷新
    refreshGirdData = function () {
        page.search();
    }

    auditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/FormModule/FormRelation/Audit', { keyValue: keyValue, formId: formId }, function (data) {
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/FormModule/FormRelation/UnAudit', { keyValue: keyValue, formId: formId }, function (data) {
        });
    };

    page.init();
};

var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue(mainCompontId.toLowerCase());
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/FormModule/Custmerform/DeleteInstanceForm', { keyValue: keyValue, schemeInfoId: relation.F_FormId }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};

var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue(mainCompontId.toLowerCase());
    if (top.Changjie.checkrow(keyValue)) {
        if (settingJson.layer.opentype == '1') {// 窗口弹层页
            var url = "";
            if (!!projectId) {
                url = top.$.rootUrl + '/FormModule/Custmerform/LayerInstanceForm?id=' + relation.F_FormId + "&keyValue=" + keyValue + "&viewState=" + viewState + '&projectId=' + projectId + '&moduleId=' + moduleId + "&formId=" + formId;
            }
            else {
                url = top.$.rootUrl + '/FormModule/Custmerform/LayerInstanceForm?id=' + relation.F_FormId + "&keyValue=" + keyValue + "&viewState=" + viewState + '&moduleId=' + moduleId + "&formId=" + formId;
            }
            top.Changjie.layerForm({
                id: 'Form',
                title: title,
                isShowConfirmBtn: isShowConfirmBtn,
                url: url,
                width: settingJson.layer.width,
                height: settingJson.layer.height,
                callBack: function (id) {
                    return top[id].acceptClick(refreshGirdData);
                }
            });
        }
        else {// 窗口页
            var url = "";
            if (!!projectId) {
                url = '/FormModule/Custmerform/TabInstanceForm?id=' + relation.F_FormId + "&keyValue=" + keyValue + "&viewState=" + viewState + '&projectId=' + projectId + '&moduleId=' + moduleId;
            }
            else {
                url = '/FormModule/Custmerform/TabInstanceForm?id=' + relation.F_FormId + "&keyValue=" + keyValue + "&viewState=" + viewState + '&moduleId=' + moduleId;
            }
            top.Changjie.frameTab.open({
                F_ModuleId: id,
                F_Icon: 'fa fa-pencil-square-o',
                F_FullName: title,
                F_UrlAddress: url
            });
        }
    }
};

