﻿var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/FormModule/FormRelation/GetFormList',
                headData: [
                    { label: "表单编码", name: "Code", width: 150, align: "left" },
                    { label: "表单名称", name: "Name", width: 180, align: "left" },
                ],
                mainId: 'ID',
                isPage: true,
                height: 450
            });
            page.search();

        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',param);//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.iniChangjie
};