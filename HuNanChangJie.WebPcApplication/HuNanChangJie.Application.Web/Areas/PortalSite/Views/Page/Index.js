﻿/*
 * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-01-29 15:23
 * 描  述：子页面管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.frameTab.open({
                    F_ModuleId: "PL_Page_List_add",
                    F_Icon: "fa fa-file-text-o",
                    F_FullName: "新增子页面",
                    F_UrlAddress: "/PortalSite/Page/Form"
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.frameTab.open({
                        F_ModuleId: "PL_Page_List_edit",
                        F_Icon: "fa fa-file-text-o",
                        F_FullName: "编辑子页面",
                        F_UrlAddress: "/PortalSite/Page/Form?keyValue=" + keyValue
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/PortalSite/Page/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/PortalSite/Page/GetPageList',
                headData: [
                    { headerName: "标题", field: "F_Title", width: 100, cellStyle: { 'text-align': 'left' } },
                    {
                        headerName: "图片", field: "F_Img", width: 100, cellStyle: { 'text-align': 'left' },
                        cellRenderer: function (params) {

                            //if (params.value == 1) {
                            //    return '<span class="label label-default">列表</span>';
                            //} else if (params.value == 2) {
                            //    return '<span class="label label-primary">图形列表</span>';
                            //} else if (params.value == 3) {
                            //    return '<span class="label label-success">详细信息</span>';
                            //}
                            

                            return '<img src="/PortalSite/Page/GetImg?keyValue=' + params.value+'"   />';;
                        }
                    },
                    {
                        headerName: "类型", field: "F_Type", width: 100, cellStyle: { 'text-align': 'left' },
                        cellRenderer: function (params) {

                            if (params.value == 1) {
                                return '<span class="label label-default">列表</span>';
                            } else if (params.value == 2) {
                                return '<span class="label label-primary">图形列表</span>';
                            } else if (params.value == 3) {
                                return '<span class="label label-success">详细信息</span>';
                            }

                            return params.value;
                        }
                    },
                    //{ headerName: "内容", field: "F_Scheme", width: 100, cellStyle: { 'text-align': 'left' } },
                ],
                mainId:'F_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
