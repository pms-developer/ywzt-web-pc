﻿var type = request("type");
var acceptClick;
var bootstrap = function (a, c) {
    var b = c.frameTab.currentIframe().categoryData;
    var d = {
        init: function () {
            a("#F_Category").mkDataItemSelect({
                code: "PortalSiteType"
            });
            if (type == "3") {
                a("#F_Article").mkselect({
                    text: "F_Title",
                    value: "F_Id",
                    allowSearch: true
                });
                a("#F_Category").on("change",
                    function () {
                        var e = a(this).mkselectGet();
                        if (e != "") {
                            a("#F_Article").mkselectRefresh({
                                data: [],
                                url: top.$.rootUrl + "/PortalSite/Article/GetList",
                                param: {
                                    queryJson: JSON.stringify({
                                        category: e
                                    })
                                }
                            })
                        } else {
                            a("#F_Article").mkselectRefresh({
                                data: [],
                                url: false
                            })
                        }
                    })
            } else {
                a("#F_Article").parent().remove()
            }
            d.initData()
        },
        initData: function () {
            if (b) {
                a("#F_Name").val(b.name);
                a("#F_Category").mkselectSet(b.category);
                if (type == "3") {
                    a("#F_Article").mkselectSet(b.articleId)
                }
            }
        }
    };
    acceptClick = function (e) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var f = a("#form").mkGetFormData();
        e(f);
        c.layerClose(window.name)
    };
    d.init()
};