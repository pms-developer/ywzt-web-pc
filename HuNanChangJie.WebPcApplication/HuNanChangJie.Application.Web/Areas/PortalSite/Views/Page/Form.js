﻿jQuery.extend({
    createUploadIframe: function (b, d) {
        var a = "jUploadFrame" + b;
        var c = '<iframe id="' + a + '" name="' + a + '" style="position:absolute; top:-9999px; left:-9999px"';
        if (window.ActiveXObject) {
            if (typeof d == "boolean") {
                c += ' src="javascript:false"'
            } else {
                if (typeof d == "string") {
                    c += ' src="' + d + '"'
                }
            }
        }
        c += " />";
        jQuery(c).appendTo(document.body);
        return jQuery("#" + a).get(0)
    },
    createUploadForm: function (g, b, a) {
        var e = "jUploadForm" + g;
        var c = "jUploadFile" + g;
        var d = jQuery('<form  action="" method="POST" name="' + e + '" id="' + e + '" enctype="multipart/form-data"></form>');
        if (a) {
            for (var f in a) {
                jQuery('<input type="hidden" name="' + f + '" value="' + a[f] + '" />').appendTo(d)
            }
        }
        var j = jQuery("#" + b);
        var h = jQuery(j).clone();
        jQuery(j).attr("id", c);
        jQuery(j).before(h);
        jQuery(j).appendTo(d);
        jQuery(d).css("position", "absolute");
        jQuery(d).css("top", "-1200px");
        jQuery(d).css("left", "-1200px");
        jQuery(d).appendTo("body");
        return d
    },
    ajaxFileUpload: function (i) {
        i = jQuery.extend({},
            jQuery.ajaxSettings, i);
        var f = new Date().getTime();
        var b = jQuery.createUploadForm(f, i.fileElementId, (typeof (i.data) == "undefined" ? false : i.data));
        var g = jQuery.createUploadIframe(f, i.secureuri);
        var d = "jUploadFrame" + f;
        var c = "jUploadForm" + f;
        if (i.global && !jQuery.active++) {
            jQuery.event.trigger("ajaxStart")
        }
        var h = false;
        var k = {};
        if (i.global) {
            jQuery.event.trigger("ajaxSend", [k, i])
        }
        var j = function (o) {
            var n = document.getElementById(d);
            try {
                if (n.contentWindow) {
                    k.responseText = n.contentWindow.document.body ? n.contentWindow.document.body.innerHTML : null;
                    k.responseXML = n.contentWindow.document.XMLDocument ? n.contentWindow.document.XMLDocument : n.contentWindow.document
                } else {
                    if (n.contentDocument) {
                        k.responseText = n.contentDocument.document.body ? n.contentDocument.document.body.innerHTML : null;
                        k.responseXML = n.contentDocument.document.XMLDocument ? n.contentDocument.document.XMLDocument : n.contentDocument.document
                    }
                }
            } catch (m) {
                jQuery.handleError(i, k, null, m)
            }
            if (k || o == "timeout") {
                h = true;
                var p;
                try {
                    p = o != "timeout" ? "success" : "error";
                    if (p != "error") {
                        var l = jQuery.uploadHttpData(k, i.dataType);
                        if (i.success) {
                            i.success(l, p)
                        }
                        if (i.global) {
                            jQuery.event.trigger("ajaxSuccess", [k, i])
                        }
                    } else {
                        jQuery.handleError(i, k, p)
                    }
                } catch (m) {
                    p = "error";
                    jQuery.handleError(i, k, p, m)
                }
                if (i.global) {
                    jQuery.event.trigger("ajaxComplete", [k, i])
                }
                if (i.global && !--jQuery.active) {
                    jQuery.event.trigger("ajaxStop")
                }
                if (i.complete) {
                    i.complete(k, p)
                }
                jQuery(n).unbind();
                setTimeout(function () {
                    try {
                        jQuery(n).remove();
                        jQuery(b).remove()
                    } catch (q) {
                        jQuery.handleError(i, k, null, q)
                    }
                },
                    100);
                k = null
            }
        };
        if (i.timeout > 0) {
            setTimeout(function () {
                if (!h) {
                    j("timeout")
                }
            },
                i.timeout)
        }
        try {
            var b = jQuery("#" + c);
            jQuery(b).attr("action", i.url);
            jQuery(b).attr("method", "POST");
            jQuery(b).attr("target", d);
            if (b.encoding) {
                jQuery(b).attr("encoding", "multipart/form-data")
            } else {
                jQuery(b).attr("enctype", "multipart/form-data")
            }
            jQuery(b).submit()
        } catch (a) {
            jQuery.handleError(i, k, null, a)
        }
        jQuery("#" + d).load(j);
        return {
            abort: function () { }
        }
    },
    uploadHttpData: function (r, type) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        if (type == "script") {
            jQuery.globalEval(data)
        }
        if (type == "json") {
            eval("data = " + data)
        }
        if (type == "html") {
            jQuery("<div>").html(data).evalScripts()
        }
        return data
    }
});
var acceptClick;
var keyValue = request("keyValue");
var type = request("type");
var titleName = "";
var categoryData = null;
var activeItem = null;
var bootstrap = function (a, b) {
    function f() {
        var g = document.getElementById("uploadFile").files[0];
        var h = window.URL.createObjectURL(g);
        document.getElementById("uploadPreview").src = h
    }
    function c(g) {
        var h = {
            rows: 10,
            page: g,
            sidx: "F_PushDate",
            sord: "DESC",
            records: 0,
            total: 0
        };
        var i = {
            pagination: JSON.stringify(h),
            queryJson: JSON.stringify({
                F_Category: activeItem.category
            })
        };
        b.httpAsync("GET", top.$.rootUrl + "/PortalSite/Article/GetPageList", i,
            function (k) {
                var j = a("#body_cotent");
                j.html("");
                if (type == "1") {
                    a.each(k.rows,
                        function (m, n) {
                            var l = a('<div class="mk-site-body-list-item">                                        <div class="text">' + n.F_Title + '</div>                                        <div class="date">' + b.formatDate(n.F_PushDate, "yyyy-MM-dd") + "</div></div>");
                            j.append(l)
                        })
                } else {
                    a.each(k.rows,
                        function (m, n) {
                            var l = a('<div class="col-md-4 col-sm-6 mk-site-img-item">                                    <div class="mk-site-img-content2">                                        <img class="img" src="' + top.$.rootUrl + "/PortalSite/Article/GetImg?keyValue=" + n.F_Id + '" />                                        <div class="text" title="' + n.F_Title + '" >' + n.F_Title + "</div>                                    </div></div>");
                            j.append(l)
                        })
                }
                e(k)
            })
    }
    function e(g) {
        laypage({
            cont: "page",
            pages: g.total,
            curr: g.page,
            groups: 5,
            skip: true,
            first: "首页",
            last: "尾页",
            jump: function(i, h) {
                if (!h) {
                    c(i.curr);
                }
            }
        });
    }
    var d = {
        init: function () {
            d.bind();
            d.initData();
        },
        bind: function () {
            a("#uploadFile").on("change", f);
            a(".file").prepend('<img id="uploadPreview"  src="' + top.$.rootUrl + "/PortalSite/Page/GetImg?keyValue=" + keyValue + '" >');
            a(".mk-site-body-left .mk-site-title").on("click",
                function () {
                    var g = a(this);
                    titleName = g.find("span").text();
                    b.layerForm({
                        id: "settingTitle",
                        title: "设置名称",
                        url: top.$.rootUrl + "/PortalSite/Page/SetNameForm",
                        width: 350,
                        height: 140,
                        callBack: function(h) {
                            return top[h].acceptClick(function(i) {
                                a(".left-title").find("span").text(i)
                            });
                        }
                    });
                });
            a("#add_item").on("click",
                function () {
                    categoryData = null;
                    b.layerForm({
                        id: "SetCategoryForm",
                        title: "添加分类项",
                        url: top.$.rootUrl + "/PortalSite/Page/SetCategoryForm?type=" + type,
                        width: 400,
                        height: 300,
                        callBack: function (g) {
                            return top[g].acceptClick(function (i) {
                                var h = a('<div class="mk-site-title-item value-item"><div class="btn-list" ><span>上移</span><span>下移</span><span>编辑</span><span>删除</span></div><span class="itemname" >' + i.F_Name + "</span></div>");
                                h[0].data = {
                                    name: i.F_Name,
                                    category: i.F_Category,
                                    article: i.F_Article
                                };
                                a("#add_item").before(h);
                                h.trigger("click")
                            })
                        }
                    })
                });
            a(".mk-site-body-left").on("click", ".btn-list>span",
                function () {
                    var h = a(this);
                    var g = h.parents(".mk-site-title-item");
                    var i = h.text();
                    switch (i) {
                        case "上移":
                            g.prev().before(g);
                            break;
                        case "下移":
                            if (!g.next().hasClass("active2")) {
                                g.next().after(g);
                            }
                            break;
                        case "编辑":
                            categoryData = g[0].data;
                            b.layerForm({
                                id: "SetCategoryForm",
                                title: "编辑分类项",
                                url: top.$.rootUrl + "/PortalSite/Page/SetCategoryForm?type=" + type,
                                width: 400,
                                height: 300,
                                callBack: function (j) {
                                    return top[j].acceptClick(function (k) {
                                        g.find(".itemname").text(k.F_Name);
                                        g[0].data.name = k.F_Name;
                                        g[0].data.category = k.F_Category;
                                        g[0].data.article = k.F_Article;
                                    });
                                    g.trigger("click");
                                }
                            });
                            break;
                        case "删除":
                            g.remove();
                            break;
                    }
                    return false;
                });;
            a(".mk-site-body-left").on("click", ".value-item",
                function () {
                    var g = a(this);
                    if (!g.hasClass("active")) {
                        g.parent().find(".active").removeClass("active");
                        g.addClass("active");
                        var h = g[0].data;
                        a(".right-title>span").text(h.name);
                        activeItem = h;
                        if (type == "3") {
                            b.httpAsync("GET",
                                top.$.rootUrl + "/PortalSite/Article/GetFormData",
                                {
                                    keyValue: activeItem.article
                                },
                                function(j) {
                                    var i = a("#body_cotent");
                                    i.html(j.F_Content);
                                });
                        } else {
                            c(1);
                        }
                    }
                });
            switch (type) {
                case "1":
                    a("#body_cotent").addClass("mk-site-body-list");
                    break;
                case "2":
                    a("#body_cotent").addClass("row");
                    break;
                case "3":
                    a("#body_cotent").addClass("mk-site-body-list");
                    break;
            }
            a("#save").on("click",
                function() {
                    if (!a(".container-top").mkValidform()) {
                        return false
                    }
                    var g = a(".container-top").mkGetFormData();
                    var i = {
                        title: a(".left-title>span").text(),
                        list: []
                    };
                    a(".mk-site-body-left .value-item").each(function() {
                        var j = a(this)[0].data;
                        i.list.push(j);
                    });
                    var h = {
                        F_Img: g.F_Img,
                        F_Title: g.F_Title,
                        F_Type: type,
                        F_Scheme: JSON.stringify(i).replace(/[<>&"]/g,
                            function(j) {
                                return {
                                    "<": "&lt;",
                                    ">": "&gt;",
                                    "&": "&amp;",
                                    '"': "&quot;"
                                }[j];
                            })
                    };
                    b.loading(true, "正在保存...");
                    a.ajaxFileUpload({
                        data: h,
                        url: top.$.rootUrl + "/PortalSite/Page/UploadFile?keyValue=" + keyValue,
                        secureuri: false,
                        fileElementId: "uploadFile",
                        dataType: "json",
                        success: function(j) {
                            b.loading(false);
                            if (j.code = "200") {
                                b.frameTab.parentIframe().refreshGirdData();
                                b.frameTab.close(b.frameTab.iframeId);
                            }
                        }
                    });
                });
        },
        initData: function () {
            if (keyValue) {
                a.mkSetForm(top.$.rootUrl + "/PortalSite/Page/GetFormData?keyValue=" + keyValue,
                    function(i) {
                        a(".container-top").mkSetFormData(i);
                        var h = JSON.parse(i.F_Scheme);
                        a(".left-title>span").text(h.title);
                        var g = a("#add_item");
                        a.each(h.list,
                            function(k, l) {
                                var j = a(
                                    '<div class="mk-site-title-item value-item"><div class="btn-list" ><span>上移</span><span>下移</span><span>编辑</span><span>删除</span></div><span class="itemname" >' +
                                    l.name +
                                    "</span></div>");
                                j[0].data = l;
                                g.before(j);
                                if (k == 0) {
                                    j.trigger("click");
                                }
                            });
                    });
            }
        }
    };
    d.init();
};