﻿var sort = request("sort");
var tabName = "";
var acceptClick;
var bootstrap = function (a, g) {
    var f = "";
    var b = "";
    var c = [];
    var d = {};
    var e = g.frameTab.currentIframe().currentModule;
    var h = {
        init: function () {
            h.bind();
            h.initData()
        },
        bind: function () {
            a("#prop").mkselect({
                data: [{
                    id: "0.5",
                    text: "1/2"
                },
                {
                    id: "0.333333",
                    text: "1/3"
                },
                {
                    id: "0.66666",
                    text: "2/3"
                },
                {
                    id: "1",
                    text: "1"
                }],
                placeholder: false
            }).mkselectSet("1");
            a("#F_Url").mkselect({
                text: "F_Title",
                value: "F_Id",
                url: top.$.rootUrl + "/PortalSite/Page/GetList",
                allowSearch: true
            });
            a("#F_Category").mkDataItemSelect({
                code: "PortalSiteType"
            });
            a("#F_Category").on("change",
                function () {
                    var i = a(this).mkselectGet();
                    b = i;
                    h.search()
                });
            a("#select_grid").jfGrid({
                url: top.$.rootUrl + "/PortalSite/Article/GetPageList",
                headData: [{
                    label: "标题",
                    name: "F_Title",
                    width: 330,
                    align: "left"
                },
                {
                    label: "分类",
                    name: "F_Category",
                    width: 100,
                    align: "center",
                    formatterAsync: function (i, k, j) {
                        g.clientdata.getAsync("dataItem", {
                            key: k,
                            code: "PortalSiteType",
                            callback: function (l) {
                                i(l.text)
                            }
                        })
                    }
                },
                {
                    label: "发布时间",
                    name: "F_PushDate",
                    width: 80,
                    align: "center",
                    formatter: function (i) {
                        return g.formatDate(i, "yyyy-MM-dd")
                    }
                }],
                mainId: "F_Id",
                isPage: true,
                isMultiselect: true,
                multiselectfield: "isCheck",
                sidx: "F_PushDate desc",
                onRenderBefore: function (i) {
                    a.each(i,
                        function (j, k) {
                            if (d[k.F_Id]) {
                                k.isCheck = 1
                            }
                        })
                },
                onSelectRow: function (k, j) {
                    if (j) {
                        var i = {
                            F_Id: k.F_Id,
                            F_Title: k.F_Title,
                            F_Category: k.F_Category,
                            F_PushDate: k.F_PushDate
                        };
                        a("#selected_grid").jfGridSet("addRow", i);
                        d[k.F_Id] = true
                    } else {
                        a("#selected_grid").jfGridSet("removeRow", k.F_Id);
                        d[k.F_Id] = false
                    }
                }
            });
            a("#selected_grid").jfGrid({
                headData: [{
                    label: "",
                    name: "btn",
                    width: 60,
                    align: "center",
                    formatter: function (l, k, j, i) {
                        i.on("click",
                            function () {
                                a("#selected_grid").jfGridSet("removeRow", k.F_Id);
                                a("#select_grid").jfGridSet("nocheck", k.F_Id);
                                d[k.F_Id] = false;
                                return false
                            });
                        return '<span class="label label-danger " style="cursor: pointer;">移除</span>'
                    }
                },
                {
                    label: "标题",
                    name: "F_Title",
                    width: 300,
                    align: "left"
                },
                {
                    label: "分类",
                    name: "F_Category",
                    width: 100,
                    align: "center",
                    formatterAsync: function (i, k, j) {
                        g.clientdata.getAsync("dataItem", {
                            key: k,
                            code: "PortalSiteType",
                            callback: function (l) {
                                i(l.text)
                            }
                        })
                    }
                },
                {
                    label: "发布时间",
                    name: "F_PushDate",
                    width: 80,
                    align: "center",
                    formatter: function (i) {
                        return g.formatDate(i, "yyyy-MM-dd")
                    }
                }],
                mainId: "F_Id"
            });
            a("#left_list").on("click", ".tab-item",
                function () {
                    var i = a(this);
                    if (!i.hasClass("active")) {
                        i.parent().find(".active").removeClass("active");
                        i.addClass("active");
                        a("#txt_Keyword").val("");
                        c = i[0].data.list;
                        d = {};
                        a.each(c,
                            function (j, k) {
                                d[k.F_Id] = true
                            });
                        a("#selected_grid").jfGridSet("refreshdata", c);
                        h.search()
                    }
                });
            a("#add_item").on("click",
                function () {
                    tabName = "";
                    g.layerForm({
                        id: "settingTitle",
                        title: "添加tab标签项",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/AddTabForm",
                        width: 350,
                        height: 140,
                        callBack: function (i) {
                            return top[i].acceptClick(function (k) {
                                var j = a('<div class="left-list-item  tab-item"><div class="btn-list" ><span>上移</span><span>下移</span><span>编辑</span><span>删除</span></div><span class="itemname" >' + k + "</span></div>");
                                j[0].data = {
                                    name: k,
                                    list: []
                                };
                                a("#add_item").before(j);
                                j.trigger("click")
                            })
                        }
                    })
                });
            a("#left_list").on("click", ".btn-list>span",
                function () {
                    var j = a(this);
                    var i = j.parents(".left-list-item");
                    var k = j.text();
                    switch (k) {
                        case "上移":
                            i.prev().before(i);
                            break;
                        case "下移":
                            if (!i.next().hasClass("active2")) {
                                i.next().after(i)
                            }
                            break;
                        case "编辑":
                            tabName = i.find(".itemname").text();
                            g.layerForm({
                                id: "settingTitle",
                                title: "编辑tab标签项",
                                url: top.$.rootUrl + "/PortalSite/HomeConfig/AddTabForm",
                                width: 350,
                                height: 140,
                                callBack: function (l) {
                                    return top[l].acceptClick(function (m) {
                                        i.find(".itemname").text(m)
                                    })
                                }
                            });
                            break;
                        case "删除":
                            if (i.prev().length > 0) {
                                i.prev().trigger("click")
                            } else {
                                i.next().trigger("click")
                            }
                            i.remove();
                            break
                    }
                    return false
                });
            a("#btn_Search").on("click",
                function () {
                    var j = {};
                    var i = a("#txt_Keyword").val();
                    j.F_Category = b;
                    j.F_Title = i;
                    a("#select_grid").jfGridSet("reload", {
                        queryJson: JSON.stringify(j)
                    })
                })
        },
        search: function (i) {
            i = i || {};
            i.F_Category = b;
            a("#select_grid").jfGridSet("reload", {
                queryJson: JSON.stringify(i)
            })
        },
        initData: function () {
            if (e) {
                var i = JSON.parse(e.F_Scheme);
                f = e.F_Id;
                e.prop = i.prop;
                a("#form1").mkSetFormData(e);
                a.each(i.list,
                    function (k, l) {
                        var j = a('<div class="left-list-item  tab-item"><div class="btn-list" ><span>上移</span><span>下移</span><span>编辑</span><span>删除</span></div><span class="itemname" >' + l.name + "</span></div>");
                        j[0].data = l;
                        a("#add_item").before(j)
                    })
            }
        }
    };
    acceptClick = function (i) {
        if (!a("#form1").mkValidform()) {
            return false
        }
        var j = a("#form1").mkGetFormData(f);
        var k = [];
        a(".tab-item").each(function () {
            var m = a(this)[0].data;
            k.push(m)
        });
        if (k.length == 0) {
            g.alert.warning("请设置tab标签项！");
            return false
        }
        var l = {
            F_Name: "模块3",
            F_Type: 9,
            F_Scheme: JSON.stringify({
                list: k,
                prop: j.prop,
                type: "3"
            }),
            F_Sort: sort
        };
        a.mkSaveForm(top.$.rootUrl + "/PortalSite/HomeConfig/SaveForm?keyValue=" + f, l,
            function (m) {
                l.F_Id = m.data;
                i && i(l)
            })
    };
    h.init()
};