﻿var sort = request("sort");
var acceptClick;
var bootstrap = function (a, d) {
    var c = "";
    var b = d.frameTab.currentIframe().currentModule;
    var e = {
        init: function () {
            a("#prop").mkselect({
                data: [{
                    id: "0.5",
                    text: "1/2"
                },
                {
                    id: "0.333333",
                    text: "1/3"
                },
                {
                    id: "0.66666",
                    text: "2/3"
                },
                {
                    id: "1",
                    text: "1"
                }],
                placeholder: false
            }).mkselectSet("1");
            a("#F_Category").mkDataItemSelect({
                code: "PortalSiteType"
            });
            a("#F_Article").mkselect({
                text: "F_Title",
                value: "F_Id",
                allowSearch: true
            });
            a("#F_Category").on("change",
                function () {
                    var f = a(this).mkselectGet();
                    if (f != "") {
                        a("#F_Article").mkselectRefresh({
                            data: [],
                            url: top.$.rootUrl + "/PortalSite/Article/GetList",
                            param: {
                                queryJson: JSON.stringify({
                                    category: f
                                })
                            }
                        })
                    } else {
                        a("#F_Article").mkselectRefresh({
                            data: [],
                            url: false
                        })
                    }
                });
            e.initData()
        },
        initData: function () {
            if (b) {
                var f = JSON.parse(b.F_Scheme);
                b.F_Category = f.category;
                b.F_Article = f.article;
                b.prop = f.prop;
                a("#form").mkSetFormData(b);
                c = b.F_Id
            }
        }
    };
    acceptClick = function (f) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var g = a("#form").mkGetFormData();
        var h = {
            F_Name: g.F_Name,
            F_Type: 9,
            F_Scheme: JSON.stringify({
                category: g.F_Category,
                article: g.F_Article,
                prop: g.prop,
                type: "5"
            }),
            F_Sort: sort
        };
        a.mkSaveForm(top.$.rootUrl + "/PortalSite/HomeConfig/SaveForm?keyValue=" + c, h,
            function (i) {
                h.F_Id = i.data;
                f && f(h)
            })
    };
    e.init()
};