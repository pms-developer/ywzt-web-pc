﻿var sort = request("sort");
var acceptClick;
var bootstrap = function (a, g) {
    var f = "";
    var b = "";
    var c = [];
    var d = {};
    var e = g.frameTab.currentIframe().currentModule;
    var h = {
        init: function () {
            h.bind();
            h.initData()
        },
        bind: function () {
            a("#prop").mkselect({
                data: [{
                    id: "0.5",
                    text: "1/2"
                },
                {
                    id: "0.333333",
                    text: "1/3"
                },
                {
                    id: "0.66666",
                    text: "2/3"
                },
                {
                    id: "1",
                    text: "1"
                }],
                placeholder: false
            }).mkselectSet("1");
            a("#F_Url").mkselect({
                text: "F_Title",
                value: "F_Id",
                url: top.$.rootUrl + "/PortalSite/Page/GetList",
                allowSearch: true
            });
            a("#F_Category").mkDataItemSelect({
                code: "PortalSiteType"
            });
            a("#F_Category").on("change",
                function () {
                    var i = a(this).mkselectGet();
                    b = i;
                    h.search()
                });
            a("#select_grid").jfGrid({
                url: top.$.rootUrl + "/PortalSite/Article/GetPageList",
                headData: [{
                    label: "标题",
                    name: "F_Title",
                    width: 330,
                    align: "left"
                },
                {
                    label: "分类",
                    name: "F_Category",
                    width: 100,
                    align: "center",
                    formatterAsync: function (i, k, j) {
                        g.clientdata.getAsync("dataItem", {
                            key: k,
                            code: "PortalSiteType",
                            callback: function (l) {
                                i(l.text)
                            }
                        })
                    }
                },
                {
                    label: "发布时间",
                    name: "F_PushDate",
                    width: 80,
                    align: "center",
                    formatter: function (i) {
                        return g.formatDate(i, "yyyy-MM-dd")
                    }
                }],
                mainId: "F_Id",
                isPage: true,
                isMultiselect: true,
                multiselectfield: "isCheck",
                sidx: "F_PushDate desc",
                onRenderBefore: function (i) {
                    a.each(i,
                        function (j, k) {
                            if (d[k.F_Id]) {
                                k.isCheck = 1
                            }
                        })
                },
                onSelectRow: function (k, j) {
                    if (j) {
                        var i = {
                            F_Id: k.F_Id,
                            F_Title: k.F_Title,
                            F_Category: k.F_Category,
                            F_PushDate: k.F_PushDate
                        };
                        a("#selected_grid").jfGridSet("addRow", i);
                        d[k.F_Id] = true
                    } else {
                        a("#selected_grid").jfGridSet("removeRow", k.F_Id);
                        d[k.F_Id] = false
                    }
                }
            });
            a("#selected_grid").jfGrid({
                headData: [{
                    label: "",
                    name: "btn",
                    width: 60,
                    align: "center",
                    formatter: function (l, k, j, i) {
                        i.on("click",
                            function () {
                                a("#selected_grid").jfGridSet("removeRow", k.F_Id);
                                a("#select_grid").jfGridSet("nocheck", k.F_Id);
                                d[k.F_Id] = false;
                                return false
                            });
                        return '<span class="label label-danger " style="cursor: pointer;">移除</span>'
                    }
                },
                {
                    label: "标题",
                    name: "F_Title",
                    width: 300,
                    align: "left"
                },
                {
                    label: "分类",
                    name: "F_Category",
                    width: 100,
                    align: "center",
                    formatterAsync: function (i, k, j) {
                        g.clientdata.getAsync("dataItem", {
                            key: k,
                            code: "PortalSiteType",
                            callback: function (l) {
                                i(l.text)
                            }
                        })
                    }
                },
                {
                    label: "发布时间",
                    name: "F_PushDate",
                    width: 80,
                    align: "center",
                    formatter: function (i) {
                        return g.formatDate(i, "yyyy-MM-dd")
                    }
                }],
                mainId: "F_Id"
            });
            a("#left_list").on("click", ".left-list-item",
                function () {
                    var i = a(this);
                    if (!i.hasClass("active")) {
                        i.parent().find(".active").removeClass("active");
                        i.addClass("active");
                        a("#txt_Keyword").val("");
                        c = i[0].data;
                        d = {};
                        a.each(c,
                            function (j, k) {
                                d[k.F_Id] = true
                            });
                        a("#selected_grid").jfGridSet("refreshdata", c);
                        h.search()
                    }
                });
            a("#btn_Search").on("click",
                function () {
                    var j = {};
                    var i = a("#txt_Keyword").val();
                    j.F_Category = b;
                    j.F_Title = i;
                    a("#select_grid").jfGridSet("reload", {
                        queryJson: JSON.stringify(j)
                    })
                })
        },
        search: function (i) {
            i = i || {};
            i.F_Category = b;
            a("#select_grid").jfGridSet("reload", {
                queryJson: JSON.stringify(i)
            })
        },
        initData: function () {
            if (e) {
                var i = JSON.parse(e.F_Scheme);
                f = e.F_Id;
                e.prop = i.prop;
                a("#form1").mkSetFormData(e);
                a("#list1")[0].data = i.list1;
                a("#list2")[0].data = i.list2;
                a("#list1").trigger("click")
            } else {
                a("#list1")[0].data = [];
                a("#list2")[0].data = [];
                a("#list1").trigger("click")
            }
        }
    };
    acceptClick = function (i) {
        if (!a("#form1").mkValidform()) {
            return false
        }
        var j = a("#form1").mkGetFormData(f);
        var k = a("#list1")[0].data;
        var l = a("#list2")[0].data;
        var m = {
            F_Name: j.F_Name,
            F_Type: 9,
            F_UrlType: 1,
            F_Url: j.F_Url,
            F_Scheme: JSON.stringify({
                list1: k,
                list2: l,
                prop: j.prop,
                type: "2"
            }),
            F_Sort: sort
        };
        a.mkSaveForm(top.$.rootUrl + "/PortalSite/HomeConfig/SaveForm?keyValue=" + f, m,
            function (n) {
                m.F_Id = n.data;
                i && i(m)
            })
    };
    h.init()
};