﻿var acceptClick;
var bootstrap = function (a, b) {
    var d = top.layer_ModuleForm3.tabName;
    var c = {
        init: function () {
            c.initData();
        },
        initData: function () {
            a("#text").val(d);
        }
    };
    acceptClick = function (e) {
        if (!a("#form").mkValidform()) {
            return false;
        }
        var f = a("#form").mkGetFormData();
        e && e(f.text);
        b.layerClose(window.name);
    };
    c.init();
};