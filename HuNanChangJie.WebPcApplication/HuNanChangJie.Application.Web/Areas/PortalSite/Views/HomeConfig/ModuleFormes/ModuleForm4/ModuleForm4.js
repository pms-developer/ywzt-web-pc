﻿var sort = request("sort");
var acceptClick;
var bootstrap = function (a, g) {
    var f = "";
    var b = "";
    var c = [];
    var d = {};
    var e = g.frameTab.currentIframe().currentModule;
    var h = {
        init: function () {
            h.bind();
            h.initData();
            h.search()
        },
        bind: function () {
            a("#F_Url").mkselect({
                text: "F_Title",
                value: "F_Id",
                url: top.$.rootUrl + "/PortalSite/Page/GetList",
                allowSearch: true
            });
            a("#F_Category").mkDataItemSelect({
                code: "PortalSiteType"
            });
            a("#F_Category").on("change",
                function () {
                    var i = a(this).mkselectGet();
                    b = i;
                    h.search()
                });
            a("#select_grid").jfGrid({
                url: top.$.rootUrl + "/PortalSite/Article/GetPageList",
                headData: [{
                    label: "标题",
                    name: "F_Title",
                    width: 330,
                    align: "left"
                },
                {
                    label: "分类",
                    name: "F_Category",
                    width: 150,
                    align: "center",
                    formatterAsync: function (i, k, j) {
                        g.clientdata.getAsync("dataItem", {
                            key: k,
                            code: "PortalSiteType",
                            callback: function (l) {
                                i(l.text)
                            }
                        })
                    }
                },
                {
                    label: "发布时间",
                    name: "F_PushDate",
                    width: 100,
                    align: "center",
                    formatter: function (i) {
                        return g.formatDate(i, "yyyy-MM-dd")
                    }
                }],
                mainId: "F_Id",
                isPage: true,
                isMultiselect: true,
                multiselectfield: "isCheck",
                sidx: "F_PushDate desc",
                onRenderBefore: function (i) {
                    a.each(i,
                        function (j, k) {
                            if (d[k.F_Id]) {
                                k.isCheck = 1
                            }
                        })
                },
                onSelectRow: function (k, j) {
                    if (j) {
                        var i = {
                            F_Id: k.F_Id,
                            F_Title: k.F_Title,
                            F_Category: k.F_Category,
                            F_PushDate: k.F_PushDate
                        };
                        a("#selected_grid").jfGridSet("addRow", i);
                        d[k.F_Id] = true
                    } else {
                        a("#selected_grid").jfGridSet("removeRow", k.F_Id);
                        d[k.F_Id] = false
                    }
                }
            });
            a("#selected_grid").jfGrid({
                headData: [{
                    label: "",
                    name: "btn",
                    width: 60,
                    align: "center",
                    formatter: function (l, k, j, i) {
                        i.on("click",
                            function () {
                                a("#selected_grid").jfGridSet("removeRow", k.F_Id);
                                a("#select_grid").jfGridSet("nocheck", k.F_Id);
                                d[k.F_Id] = false;
                                return false
                            });
                        return '<span class="label label-danger " style="cursor: pointer;">移除</span>'
                    }
                },
                {
                    label: "标题",
                    name: "F_Title",
                    width: 300,
                    align: "left"
                },
                {
                    label: "分类",
                    name: "F_Category",
                    width: 150,
                    align: "center",
                    formatterAsync: function (i, k, j) {
                        g.clientdata.getAsync("dataItem", {
                            key: k,
                            code: "PortalSiteType",
                            callback: function (l) {
                                i(l.text)
                            }
                        })
                    }
                },
                {
                    label: "发布时间",
                    name: "F_PushDate",
                    width: 100,
                    align: "center",
                    formatter: function (i) {
                        return g.formatDate(i, "yyyy-MM-dd")
                    }
                }],
                mainId: "F_Id"
            });
            a("#btn_Search").on("click",
                function () {
                    var j = {};
                    var i = a("#txt_Keyword").val();
                    j.F_Category = b;
                    j.F_Title = i;
                    a("#select_grid").jfGridSet("reload", {
                        queryJson: JSON.stringify(j)
                    })
                })
        },
        search: function (i) {
            i = i || {};
            i.F_Category = b;
            a("#select_grid").jfGridSet("reload", {
                queryJson: JSON.stringify(i)
            })
        },
        initData: function () {
            if (e) {
                a("#form1").mkSetFormData(e);
                c = JSON.parse(e.F_Scheme).list;
                a.each(c,
                    function (i, j) {
                        d[j.F_Id] = true
                    });
                a("#selected_grid").jfGridSet("refreshdata", c);
                f = e.F_Id
            } else {
                a("#selected_grid").jfGridSet("refreshdata", c)
            }
        }
    };
    acceptClick = function (i) {
        if (!a("#form1").mkValidform()) {
            return false
        }
        var j = a("#form1").mkGetFormData(f);
        if (c.length > 4) {
            g.alert.warning("最多选择4篇文章！");
            return false
        }
        var k = {
            F_Name: j.F_Name,
            F_Type: 9,
            F_UrlType: 1,
            F_Url: j.F_Url,
            F_Scheme: JSON.stringify({
                list: c || [],
                type: "4"
            }),
            F_Sort: sort
        };
        a.mkSaveForm(top.$.rootUrl + "/PortalSite/HomeConfig/SaveForm?keyValue=" + f, k,
            function (l) {
                k.F_Id = l.data;
                i && i(k)
            })
    };
    h.init()
};