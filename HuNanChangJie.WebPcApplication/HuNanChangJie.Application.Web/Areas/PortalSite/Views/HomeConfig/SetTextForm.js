﻿var type = request("type");
var acceptClick;
var bootstrap = function (a, b) {
    var titlename = b.frameTab.currentIframe().titleName;
    var page = {
        init: function () {
            page.initData();
        },
        initData: function () {
            a("#text").val(titlename);
        }
    };
    acceptClick = function (callback) {
        if (!a("#form").mkValidform()) {
            return false;
        }
        var formdata = a("#form").mkGetFormData();
        formdata.type = type;
        a.mkSaveForm(top.$.rootUrl + "/PortalSite/HomeConfig/SaveText",
            formdata,
            function(g) {
                callback && callback(formdata.text);
            });
    };
    page.init();
};