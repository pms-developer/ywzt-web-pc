﻿jQuery.extend({
    createUploadIframe: function (b, d) {
        var a = "jUploadFrame" + b;
        var c = '<iframe id="' + a + '" name="' + a + '" style="position:absolute; top:-9999px; left:-9999px"';
        if (window.ActiveXObject) {
            if (typeof d == "boolean") {
                c += ' src="javascript:false"';
            } else {
                if (typeof d == "string") {
                    c += ' src="' + d + '"';
                }
            }
        }
        c += " />";
        jQuery(c).appendTo(document.body);
        return jQuery("#" + a).get(0);
    },
    createUploadForm: function (g, b, a) {
        var e = "jUploadForm" + g;
        var c = "jUploadFile" + g;
        var d = jQuery('<form  action="" method="POST" name="' + e + '" id="' + e + '" enctype="multipart/form-data"></form>');
        if (a) {
            for (var f in a) {
                jQuery('<input type="hidden" name="' + f + '" value="' + a[f] + '" />').appendTo(d);
            }
        }
        var j = jQuery("#" + b);
        var h = jQuery(j).clone();
        jQuery(j).attr("id", c);
        jQuery(j).before(h);
        jQuery(j).appendTo(d);
        jQuery(d).css("position", "absolute");
        jQuery(d).css("top", "-1200px");
        jQuery(d).css("left", "-1200px");
        jQuery(d).appendTo("body");
        return d;
    },
    ajaxFileUpload: function (i) {
        i = jQuery.extend({},
            jQuery.ajaxSettings, i);
        var f = new Date().getTime();
        var b = jQuery.createUploadForm(f, i.fileElementId, (typeof (i.data) == "undefined" ? false : i.data));
        var g = jQuery.createUploadIframe(f, i.secureuri);
        var d = "jUploadFrame" + f;
        var c = "jUploadForm" + f;
        if (i.global && !jQuery.active++) {
            jQuery.event.trigger("ajaxStart");
        }
        var h = false;
        var k = {};
        if (i.global) {
            jQuery.event.trigger("ajaxSend", [k, i]);
        }
        var j = function (o) {
            var n = document.getElementById(d);
            try {
                if (n.contentWindow) {
                    k.responseText = n.contentWindow.document.body ? n.contentWindow.document.body.innerHTML : null;
                    k.responseXML = n.contentWindow.document.XMLDocument ? n.contentWindow.document.XMLDocument : n.contentWindow.document
                } else {
                    if (n.contentDocument) {
                        k.responseText = n.contentDocument.document.body ? n.contentDocument.document.body.innerHTML : null;
                        k.responseXML = n.contentDocument.document.XMLDocument ? n.contentDocument.document.XMLDocument : n.contentDocument.document
                    }
                }
            } catch (m) {
                jQuery.handleError(i, k, null, m);
            }
            if (k || o == "timeout") {
                h = true;
                var p;
                try {
                    p = o != "timeout" ? "success" : "error";
                    if (p != "error") {
                        var l = jQuery.uploadHttpData(k, i.dataType);
                        if (i.success) {
                            i.success(l, p);
                        }
                        if (i.global) {
                            jQuery.event.trigger("ajaxSuccess", [k, i]);
                        }
                    } else {;
                        jQuery.handleError(i, k, p);
                    }
                } catch (m) {
                    p = "error";
                    jQuery.handleError(i, k, p, m);
                }
                if (i.global) {
                    jQuery.event.trigger("ajaxComplete", [k, i]);
                }
                if (i.global && !--jQuery.active) {
                    jQuery.event.trigger("ajaxStop");
                }
                if (i.complete) {
                    i.complete(k, p);
                }
                jQuery(n).unbind();
                setTimeout(function () {
                    try {
                        jQuery(n).remove();
                        jQuery(b).remove();
                    } catch (q) {
                        jQuery.handleError(i, k, null, q);
                    }
                },
                    100);
                k = null;
            }
        };
        if (i.timeout > 0) {
            setTimeout(function() {
                    if (!h) {
                        j("timeout");
                    }
                },
                i.timeout);
        }
        try {
            var b = jQuery("#" + c);
            jQuery(b).attr("action", i.url);
            jQuery(b).attr("method", "POST");
            jQuery(b).attr("target", d);
            if (b.encoding) {
                jQuery(b).attr("encoding", "multipart/form-data");
            } else {
                jQuery(b).attr("enctype", "multipart/form-data");
            }
            jQuery(b).submit();
        } catch (a) {
            jQuery.handleError(i, k, null, a);
        }
        jQuery("#" + d).load(j);
        return {
            abort: function () { }
        }
    },
    uploadHttpData: function (r, type) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        if (type == "script") {
            jQuery.globalEval(data);
        }
        if (type == "json") {
            eval("data = " + data);
        }
        if (type == "html") {
            jQuery("<div>").html(data).evalScripts();
        }
        return data;
    }
});
var bootstrap = function (a, b) {
    function e(j) {
        var g = a(this);
        var i = g[0].files[0];
        var k = window.URL.createObjectURL(i);
        g.next().attr("src", k);
        var j = g.attr("id");
        var h = g.attr("data-index");
        b.loading(true, "正在保存...");
        a.ajaxFileUpload({
            url: top.$.rootUrl + "/PortalSite/HomeConfig/UploadFile2?keyValue=" + j + "&sort=" + h,
            secureuri: false,
            fileElementId: j,
            dataType: "json",
            success: function(f) {
                b.loading(false);
                d();
            }
        });
    }
    function d() {
        var f = [];
        a(".item").each(function () {
            var h = a(this).find("img").attr("src");
            if (h != top.$.rootUrl + "/PortalSite/HomeConfig/GetImg2") {
                var g = {
                    src: h
                };
                f.push(g);
            }
        });
        b.frameTab.currentIframe().renderPicture(f);
    }
    var c = {
        init: function () {
            c.bind();
            c.initData();
        },
        bind: function () {
            a(".body").mkscroll();
            a(".body").on("click", ".edit-btn>span",
                function () {
                    var k = a(this);
                    var g = k.parents(".item");
                    var h = null;
                    var r = a(this).attr("data-value");
                    var f = g.find("input");
                    var l = f.attr("id");
                    var m = f.attr("data-index");
                    switch (r) {
                        case "up":
                            h = g.prev();
                            if (h.length > 0) {
                                var j = h.find("input");
                                var p = j.attr("id");
                                var q = j.attr("data-index");
                                f.attr("data-index", q);
                                j.attr("data-index", m);
                                b.httpAsync("post", top.$.rootUrl + "/PortalSite/HomeConfig/UpdateForm", {
                                    keyValue1: l,
                                    keyValue2: n
                                },
                                    function (s) { });
                                h.before(g);
                                d();
                            }
                            break;
                        case "down":
                            h = g.next();
                            if (!h.hasClass("pictureBtn")) {
                                var i = h.find("input");
                                var n = i.attr("id");
                                var o = i.attr("data-index");
                                f.attr("data-index", o);
                                i.attr("data-index", m);
                                b.httpAsync("post", top.$.rootUrl + "/PortalSite/HomeConfig/UpdateForm", {
                                    keyValue1: l,
                                    keyValue2: n
                                },
                                    function (s) { });
                                h.after(g);
                                d();
                            }
                            break;
                        case "delete":
                            b.layerConfirm("是否确认删除该图片！",
                                function (s) {
                                    if (s) {
                                        b.deleteForm(top.$.rootUrl + "/PortalSite/HomeConfig/DeleteForm",
                                            {
                                                keyValue: l
                                            },
                                            function() {
                                                g.remove();
                                                d();
                                            });
                                    }
                                });
                            break;
                    }
                });
            a("#addBtn").on("click",
                function() {
                    var f = a(this);
                    var h = f.prev();
                    var j = 1;
                    if (h.length > 0) {
                        j = parseInt(h.find("input").attr("data-index")) + 1;
                    }
                    var i = b.newGuid();
                    var g = a(
                        '<div class="item">                                   <div class="file">                                       <input type="file"  name="' +
                        i +
                        '"  id="' +
                        i +
                        '" data-index="' +
                        j +
                        '"  >                                       <img src="' +
                        top.$.rootUrl +
                        '/PortalSite/HomeConfig/GetImg2">                                   </div>                                   <div class="edit-btn"><span data-value="up" >上移</span><span data-value="down"  >下移</span><span data-value="delete"  >删除</span></div>                               </div>');
                    g.find("input").on("change", e);
                    f.before(g);
                });
        },
        initData: function () {
            b.httpAsync("GET",
                top.$.rootUrl + "/PortalSite/HomeConfig/GetList?type=8",
                {},
                function(g) {
                    var f = a("#addBtn");
                    a.each(g || [],
                        function(i, j) {
                            var h = a(
                                '<div class="item">                                   <div class="file">                                       <input type="file"  name="' +
                                j.F_Id +
                                '"  id="' +
                                j.F_Id +
                                '" data-index="' +
                                j.F_Sort +
                                '"  >                                       <img src="' +
                                top.$.rootUrl +
                                "/PortalSite/HomeConfig/GetImg2?keyValue=" +
                                j.F_Id +
                                '">                                   </div>                                   <div class="edit-btn"><span data-value="up" >上移</span><span data-value="down"  >下移</span><span data-value="delete"  >删除</span></div>                               </div>');
                            h.find("input").on("change", e);
                            f.before(h);
                        });
                });
        }
    };
    c.init();
};