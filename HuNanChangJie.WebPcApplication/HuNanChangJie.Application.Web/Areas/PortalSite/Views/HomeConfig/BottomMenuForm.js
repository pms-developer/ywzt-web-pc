﻿var parentId = request("parentId");
var acceptClick;
var keyValue = "";
var bootstrap = function (a, c) {
    var e = top.layer_BottomMenuIndex.selectedRow;
    var b = "1";
    var d = {
        init: function () {
            d.bind();
            d.initData()
        },
        bind: function () {
            a("#F_Page").mkselect({
                text: "F_Title",
                value: "F_Id",
                url: top.$.rootUrl + "/PortalSite/Page/GetList",
                allowSearch: true
            });
            a('[name="F_UrlType"]').on("click",
                function () {
                    var f = a(this).val();
                    if (f == "1") {
                        a("#F_Url").parent().hide();
                        a("#F_Page").parent().show()
                    } else {
                        a("#F_Page").parent().hide();
                        a("#F_Url").parent().show()
                    }
                })
        },
        initData: function () {
            if (e) {
                keyValue = e.F_Id || "";
                if (e.F_UrlType == "1") {
                    e.F_Page = e.F_Url
                }
                a("#form").mkSetFormData(e)
            }
        }
    };
    acceptClick = function (f) {
        if (!a("#form").mkValidform()) {
            return false
        }
        var g = a("#form").mkGetFormData(keyValue);
        if (!keyValue && top.layer_BottomMenuIndex.alldata.length >= 7) {
            c.alert.warning("菜单最多维护7个！");
            return false
        }
        if (g.F_UrlType == "1") {
            g.F_Url = g.F_Page
        }
        g.F_Type = 7;
        a.mkSaveForm(top.$.rootUrl + "/PortalSite/HomeConfig/SaveForm?keyValue=" + keyValue, g,
            function (h) {
                f && f()
            })
    };
    d.init()
};