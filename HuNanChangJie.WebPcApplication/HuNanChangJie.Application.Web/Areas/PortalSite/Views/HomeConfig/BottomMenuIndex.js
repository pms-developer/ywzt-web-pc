﻿var refreshGirdData;
var selectedRow;
var alldata = [];
var bootstrap = function (a, b) {
    var c = {
        init: function () {
            c.initGrid();
            c.bind()
        },
        bind: function () {
            a("#btn_Search").on("click",
                function () {
                    var d = a("#txt_Keyword").val();
                    c.search({
                        keyword: d
                    })
                });
            a("#refresh").on("click",
                function () {
                    location.reload()
                });
            a("#add").on("click",
                function () {
                    selectedRow = null;
                    b.layerForm({
                        id: "BottomMenuForm",
                        title: "添加菜单",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/BottomMenuForm",
                        width: 500,
                        height: 400,
                        callBack: function (d) {
                            return top[d].acceptClick(refreshGirdData)
                        }
                    })
                });
            a("#edit").on("click",
                function () {
                    selectedRow = a("#gridtable").jfGridGet("rowdata");
                    var d = a("#gridtable").jfGridValue("F_Id");
                    if (b.checkrow(d)) {
                        b.layerForm({
                            id: "BottomMenuForm",
                            title: "编辑菜单",
                            url: top.$.rootUrl + "/PortalSite/HomeConfig/BottomMenuForm",
                            width: 500,
                            height: 400,
                            callBack: function (e) {
                                return top[e].acceptClick(refreshGirdData)
                            }
                        })
                    }
                });
            a("#delete").on("click",
                function () {
                    var d = a("#gridtable").jfGridValue("F_Id");
                    if (b.checkrow(d)) {
                        b.layerConfirm("是否确认删除该项！",
                            function (e) {
                                if (e) {
                                    b.deleteForm(top.$.rootUrl + "/PortalSite/HomeConfig/DeleteForm", {
                                        keyValue: d
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                })
        },
        initGrid: function () {
            a("#gridtable").jfGrid({
                url: top.$.rootUrl + "/PortalSite/HomeConfig/GetList?type=7",
                headData: [{
                    label: "名称",
                    name: "F_Name",
                    width: 200,
                    align: "left"
                },
                {
                    label: "排序码",
                    name: "F_Sort",
                    width: 50,
                    align: "center"
                },
                {
                    label: "链接类型",
                    name: "F_UrlType",
                    width: 80,
                    align: "center",
                    formatter: function (d, e) {
                        if (d == 1) {
                            return '<span class="label label-warning" style="cursor: pointer;">内部链接</span>'
                        } else {
                            return '<span class="label label-primary" style="cursor: pointer;">外部链接</span>'
                        }
                    }
                },
                {
                    label: "链接地址",
                    name: "F_Url",
                    width: 200,
                    align: "left",
                    formatterAsync: function (d, f, e) {
                        if (e.F_UrlType == 1) {
                            b.clientdata.getAsync("custmerData", {
                                url: "/PortalSite/Page/GetList",
                                key: f,
                                keyId: "F_Id",
                                callback: function (g) {
                                    d(g.F_Title)
                                }
                            })
                        } else {
                            return d(f)
                        }
                    }
                }],
                isTree: true,
                mainId: "F_Id",
                reloadSelected: true,
                onRenderComplete: function (d) {
                    alldata = d;
                    b.frameTab.currentIframe().renderBottomMenu(alldata)
                }
            });
            c.search()
        },
        search: function (d) {
            a("#gridtable").jfGridSet("reload", d)
        }
    };
    refreshGirdData = function () {
        a("#gridtable").jfGridSet("reload")
    };
    c.init()
};