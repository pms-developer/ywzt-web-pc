﻿jQuery.extend({
    createUploadIframe: function (b, d) {
        var a = "jUploadFrame" + b;
        var c = '<iframe id="' + a + '" name="' + a + '" style="position:absolute; top:-9999px; left:-9999px"';
        if (window.ActiveXObject) {
            if (typeof d == "boolean") {
                c += ' src="javascript:false"'
            } else {
                if (typeof d == "string") {
                    c += ' src="' + d + '"'
                }
            }
        }
        c += " />";
        jQuery(c).appendTo(document.body);
        return jQuery("#" + a).get(0)
    },
    createUploadForm: function (g, b, a) {
        var e = "jUploadForm" + g;
        var c = "jUploadFile" + g;
        var d = jQuery('<form  action="" method="POST" name="' + e + '" id="' + e + '" enctype="multipart/form-data"></form>');
        if (a) {
            for (var f in a) {
                jQuery('<input type="hidden" name="' + f + '" value="' + a[f] + '" />').appendTo(d)
            }
        }
        var j = jQuery("#" + b);
        var h = jQuery(j).clone();
        jQuery(j).attr("id", c);
        jQuery(j).before(h);
        jQuery(j).appendTo(d);
        jQuery(d).css("position", "absolute");
        jQuery(d).css("top", "-1200px");
        jQuery(d).css("left", "-1200px");
        jQuery(d).appendTo("body");
        return d
    },
    ajaxFileUpload: function (i) {
        i = jQuery.extend({},
            jQuery.ajaxSettings, i);
        var f = new Date().getTime();
        var b = jQuery.createUploadForm(f, i.fileElementId, (typeof (i.data) == "undefined" ? false : i.data));
        var g = jQuery.createUploadIframe(f, i.secureuri);
        var d = "jUploadFrame" + f;
        var c = "jUploadForm" + f;
        if (i.global && !jQuery.active++) {
            jQuery.event.trigger("ajaxStart")
        }
        var h = false;
        var k = {};
        if (i.global) {
            jQuery.event.trigger("ajaxSend", [k, i])
        }
        var j = function (o) {
            var n = document.getElementById(d);
            try {
                if (n.contentWindow) {
                    k.responseText = n.contentWindow.document.body ? n.contentWindow.document.body.innerHTML : null;
                    k.responseXML = n.contentWindow.document.XMLDocument ? n.contentWindow.document.XMLDocument : n.contentWindow.document
                } else {
                    if (n.contentDocument) {
                        k.responseText = n.contentDocument.document.body ? n.contentDocument.document.body.innerHTML : null;
                        k.responseXML = n.contentDocument.document.XMLDocument ? n.contentDocument.document.XMLDocument : n.contentDocument.document
                    }
                }
            } catch (m) {
                jQuery.handleError(i, k, null, m)
            }
            if (k || o == "timeout") {
                h = true;
                var p;
                try {
                    p = o != "timeout" ? "success" : "error";
                    if (p != "error") {
                        var l = jQuery.uploadHttpData(k, i.dataType);
                        if (i.success) {
                            i.success(l, p)
                        }
                        if (i.global) {
                            jQuery.event.trigger("ajaxSuccess", [k, i])
                        }
                    } else {
                        jQuery.handleError(i, k, p)
                    }
                } catch (m) {
                    p = "error";
                    jQuery.handleError(i, k, p, m)
                }
                if (i.global) {
                    jQuery.event.trigger("ajaxComplete", [k, i])
                }
                if (i.global && !--jQuery.active) {
                    jQuery.event.trigger("ajaxStop")
                }
                if (i.complete) {
                    i.complete(k, p)
                }
                jQuery(n).unbind();
                setTimeout(function () {
                    try {
                        jQuery(n).remove();
                        jQuery(b).remove()
                    } catch (q) {
                        jQuery.handleError(i, k, null, q)
                    }
                },
                    100);
                k = null
            }
        };
        if (i.timeout > 0) {
            setTimeout(function () {
                if (!h) {
                    j("timeout")
                }
            },
                i.timeout)
        }
        try {
            var b = jQuery("#" + c);
            jQuery(b).attr("action", i.url);
            jQuery(b).attr("method", "POST");
            jQuery(b).attr("target", d);
            if (b.encoding) {
                jQuery(b).attr("encoding", "multipart/form-data")
            } else {
                jQuery(b).attr("enctype", "multipart/form-data")
            }
            jQuery(b).submit()
        } catch (a) {
            jQuery.handleError(i, k, null, a)
        }
        jQuery("#" + d).load(j);
        return {
            abort: function () { }
        }
    },
    uploadHttpData: function (r, type) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        if (type == "script") {
            jQuery.globalEval(data)
        }
        if (type == "json") {
            eval("data = " + data)
        }
        if (type == "html") {
            jQuery("<div>").html(data).evalScripts()
        }
        return data
    }
});
var titleName = "";
var renderTopMenu;
var renderBottomMenu;
var renderPicture;
var renderModule;
var addModule;
var bannerSwiper;
var currentModule;
var bootstrap = function ($, Changjie) {
    function dlogoUpload() {
        var g = document.getElementById("uploadFile1").files[0];
        var h = window.URL.createObjectURL(g);
        document.getElementById("uploadPreview1").src = h;
        Changjie.loading(true, "正在保存...");
        $.ajaxFileUpload({
            url: top.$.rootUrl + "/PortalSite/HomeConfig/UploadFile?type=4",
            secureuri: false,
            fileElementId: "uploadFile1",
            dataType: "json",
            success: function (i) {
                Changjie.loading(false);
            }
        });
    }

    function ebottomlogoUpload() {
        var g = document.getElementById("uploadFile2").files[0];
        var h = window.URL.createObjectURL(g);
        document.getElementById("uploadPreview2").src = h;
        Changjie.loading(true, "正在保存...");
        $.ajaxFileUpload({
            url: top.$.rootUrl + "/PortalSite/HomeConfig/UploadFile?type=10",
            secureuri: false,
            fileElementId: "uploadFile2",
            dataType: "json",
            success: function (i) {
                Changjie.loading(false)
            }
        })
    }

    function fbottomlink() {
        var g = document.getElementById("uploadFile3").files[0];
        var h = window.URL.createObjectURL(g);
        document.getElementById("uploadPreview3").src = h;
        Changjie.loading(true, "正在保存...");
        $.ajaxFileUpload({
            url: top.$.rootUrl + "/PortalSite/HomeConfig/UploadFile?type=5",
            secureuri: false,
            fileElementId: "uploadFile3",
            dataType: "json",
            success: function(i) {
                Changjie.loading(false);
            }
        });
    }

    $.fn.SiteCarousel = function (j) {
        var g = {
            speed: 4000,
            rowHeight: 47
        };
        var k = $.extend({},
            g, j),
            h;
        function i(m, n, l) {
            m.find("ul").animate({
                    marginTop: "-=" + n
                },
                300,
                function() {
                    $(this).find("li").slice(0, 1).appendTo($(this));
                    $(this).css("margin-top", 0);
                    l();
                });
        }

        this.each(function(n) {
            var o = k.rowHeight,
                p = k.speed,
                m = $(this);
            var l = function(q) {
                if (q) {
                    clearInterval(h);
                    h = setTimeout(l, p);
                } else {
                    if (m.find("ul").height() > m.height()) {
                        i(m,
                            o,
                            function() {
                                clearInterval(h);
                                h = setTimeout(l, p);
                            });
                    }
                }
            };
            l(true);
            m.hover(function() {
                    if (h) {
                        clearInterval(h);
                    }
                },
                function() {
                    l(true);
                });
        });
    };
    var cpage = {
        init: function () {
            bannerSwiper = new Swiper(".mk-site-banner-swiper-container", {
                direction: "horizontal",
                autoplay: true,
                loop: true,
                speed: 600,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true
                },
            });
            cpage.bind();
            cpage.initData();
        },
        bind: function () {
            $("#site_top_text").on("click",
                function () {
                    var g = $(this);
                    titleName = g.find("span").text();
                    Changjie.layerForm({
                        id: "SetTextForm",
                        title: "设置",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/SetTextForm?type=1",
                        width: 700,
                        height: 140,
                        callBack: function(h) {
                            return top[h].acceptClick(function(i) {
                                g.find("span").text(i);
                            });
                        }
                    });
                });
            $("#site_footer_text").on("click",
                function () {
                    var g = $(this);
                    titleName = g.find("span").text();
                    Changjie.layerForm({
                        id: "SetTextForm",
                        title: "设置",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/SetTextForm?type=3",
                        width: 700,
                        height: 140,
                        callBack: function(h) {
                            return top[h].acceptClick(function(i) {
                                g.find("span").text(i);
                            });
                        }
                    });
                });
            $("#site_bottom_text").on("click",
                function () {
                    var g = $(this);
                    titleName = g.find("span").text();
                    Changjie.layerForm({
                        id: "SetTextForm",
                        title: "设置",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/SetTextForm?type=2",
                        width: 700,
                        height: 140,
                        callBack: function(h) {
                            return top[h].acceptClick(function(i) {
                                g.find("span").text(i);
                            });
                        }
                    });
                });
            $("#wechat_text").on("click",
                function () {
                    var g = $(this);
                    titleName = g.find("span").text();
                    Changjie.layerForm({
                        id: "SetTextForm",
                        title: "设置",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/SetTextForm?type=11",
                        width: 300,
                        height: 140,
                        callBack: function(h) {
                            return top[h].acceptClick(function(i) {
                                g.find("span").text(i);
                            });
                        }
                    });
                });
            $("#uploadFile1").on("change", dlogoUpload);
            $("#top_logo").prepend('<img id="uploadPreview1"  src="' + top.$.rootUrl + '/PortalSite/HomeConfig/GetImg?type=4" >');
            $("#uploadFile2").on("change", ebottomlogoUpload);
            $("#bottom_logo").prepend('<img id="uploadPreview2"  src="' + top.$.rootUrl + '/PortalSite/HomeConfig/GetImg?type=10" >');
            $("#uploadFile3").on("change", fbottomlink);
            $("#wechat_img").prepend('<img id="uploadPreview3"  src="' + top.$.rootUrl + '/PortalSite/HomeConfig/GetImg?type=5" >');
            $("#top_menu").on("click",
                function () {
                    Changjie.layerForm({
                        id: "TopMenuIndex",
                        title: "顶部菜单配置",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/TopMenuIndex",
                        width: 800,
                        height: 500,
                        maxmin: true,
                        btn: null
                    })
                });
            $("#bottom_menu").on("click",
                function () {
                    Changjie.layerForm({
                        id: "BottomMenuIndex",
                        title: "底部菜单配置",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/BottomMenuIndex",
                        width: 800,
                        height: 500,
                        maxmin: true,
                        btn: null
                    });
                });
            $("#banner_site").on("click",
                function () {
                    Changjie.layerForm({
                        id: "PictureForm",
                        title: "轮播图配置",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/PictureForm",
                        width: 400,
                        height: 500,
                        maxmin: true,
                        btn: null
                    });
                });
            $("#moduleAddBtn").on("click",
                function () {
                    currentModule = null;
                    Changjie.layerForm({
                        id: "SelectModuleForm",
                        title: "模块风格选择",
                        url: top.$.rootUrl + "/PortalSite/HomeConfig/SelectModuleForm",
                        width: 600,
                        height: 500,
                        maxmin: true,
                        btn: null
                    });
                });
            $(".mk-site-modules").on("click", ".edit-btn>span",
                function () {
                    var i = $(this);
                    var m = i.attr("data-value");
                    var g = i.parents(".mk-site-module");
                    var k = g[0].data;
                    var h;
                    var j;
                    switch (m) {
                        case "up":
                            h = g.prev();
                            if (h.length > 0) {
                                j = h[0].data.F_Sort;
                                h[0].data.F_Sort = g[0].data.F_Sort;
                                g[0].data.F_Sort = j;
                                Changjie.httpAsync("post", top.$.rootUrl + "/PortalSite/HomeConfig/UpdateForm", {
                                    keyValue1: g[0].data.F_Id,
                                    keyValue2: h[0].data.F_Id
                                },
                                    function (n) { });
                                h.before(g);
                            }
                            break;
                        case "down":
                            h = g.next();
                            if (!h.hasClass("mk-site-module-add")) {
                                j = h[0].data.F_Sort;
                                h[0].data.F_Sort = g[0].data.F_Sort;
                                g[0].data.F_Sort = j;
                                Changjie.httpAsync("post", top.$.rootUrl + "/PortalSite/HomeConfig/UpdateForm", {
                                    keyValue1: g[0].data.F_Id,
                                    keyValue2: h[0].data.F_Id
                                },
                                    function (n) { });
                                h.after(g);
                            }
                            break;
                        case "edit":
                            var l = JSON.parse(k.F_Scheme);
                            currentModule = k;
                            addModule(l.type, k.F_Sort);
                            break;
                        case "delete":
                            Changjie.layerConfirm("是否确认删除该模块?",
                                function (n) {
                                    if (n) {
                                        Changjie.deleteForm(top.$.rootUrl + "/PortalSite/HomeConfig/DeleteForm",
                                            {
                                                keyValue: k.F_Id
                                            },
                                            function() {
                                                g.remove();
                                            });
                                    }
                                });
                            break;
                    }
                    return false;
                });
            $(".mk-site-modules").on("click",
                ".mk-site-tab",
                function() {
                    var h = $(this);
                    if (!h.hasClass("active")) {
                        var g = h.parent();
                        g.find(".active").removeClass("active");
                        h.addClass("active");
                        var i = h.attr("data-value");
                        g.next().find(".active").removeClass("active");
                        g.next().find('[data-value="' + i + '"]').addClass("active");
                    }
                });
        },
        initData: function () {
            var i = {};
            var g = [];
            var h = [];
            Changjie.httpAsync("GET", top.$.rootUrl + "/PortalSite/HomeConfig/GetAllList", {},
                function (j) {
                    $.each(j,
                        function (k, l) {
                            switch (l.F_Type) {
                                case "1":
                                    $("#site_top_text span").text(l.F_Name);
                                    break;
                                case "2":
                                    $("#site_bottom_text span").text(l.F_Name);
                                    break;
                                case "3":
                                    $("#site_footer_text span").text(l.F_Name);
                                    break;
                                case "4":
                                    break;
                                case "5":
                                    break;
                                case "6":
                                    i[l.F_ParentId] = i[l.F_ParentId] || [];
                                    i[l.F_ParentId].push(l);
                                    break;
                                case "7":
                                    g.push(l);
                                    break;
                                case "8":
                                    h.push(l);
                                    break;
                                case "11":
                                    $("#wechat_text span").text(l.F_Name);
                                    break;
                            }
                        });
                    renderTopMenu(i);
                    renderBottomMenu(g);
                    renderPicture(h);
                });
            Changjie.httpAsync("GET",
                top.$.rootUrl + "/PortalSite/HomeConfig/GetList",
                {
                    type: 9
                },
                function(j) {
                    $.each(j,
                        function(k, l) {
                            renderModule(l);
                        });
                });
        }
    };
    renderTopMenu = function (h) {
        var gtopmenu = $("#top_menu ul");
        gtopmenu.find(".childPage").remove();
        $(".mk-site-sub-nav-ul").remove();
        $.each(h["0"] || [],
            function (l, m) {
                var ichildPage = $('<li class="mk-site-nav-li childPage">                               <a href="javascript:void(0);" class="mk-site-nav-item"><span class="text">' + m.F_Name + "</span></a>                           </li>");
                ichildPage[0].data = m;
                gtopmenu.append(ichildPage);
                if (h[m.F_Id]) {
                    var j = $('<div class="mk-site-sub-nav-ul" data-value="' + m.F_Id + '" ><div class="mk-site-content"><ul class="mk-site-sub-nav-menu"></ul></div></div>');
                    var k = j.find("ul");
                    $.each(h[m.F_Id],
                        function (p, q) {
                            var n = $('<li class="mk-site-sub-nav-li"><a href="javascript:void(0);" class="mk-site-sub-nav-item">' + q.F_Name + "</a></li>");
                            n[0].data = q;
                            if (h[q.F_Id]) {
                                var o = $('<ul class="mk-site-three-nav-menu"></ul>');
                                $.each(h[q.F_Id],
                                    function (s, t) {
                                        var r = $('<li class="mk-site-three-nav-li"><a href="javascript:void(0);" class="mk-site-three-nav-item">' + t.F_Name + "</a></li>");
                                        r[0].data = t;
                                        o.append(r);
                                    });
                                n.append(o);
                            }
                            k.append(n);
                        });
                    $("body").append(j);
                }
            });
        gtopmenu.find(".childPage").hover(function () {
            var j = $(this);
            var k = j[0].data;
            $(".mk-site-sub-nav-ul").hide();
            var i = $('.mk-site-sub-nav-ul[data-value="' + k.F_Id + '"]');
            if (i.length > 0) {
                i[0].isShow = false;
                i.show();
            }
        },
            function () {
                var i = $(this);
                var j = i[0].data;
                setTimeout(function() {
                        var k = $('.mk-site-sub-nav-ul[data-value="' + j.F_Id + '"]');
                        if (k.length > 0) {
                            if (!k[0].isShow) {
                                k.hide();
                            }
                        }
                    },
                    100);
            });
        $(".mk-site-sub-nav-ul").hover(function() {
                $(this)[0].isShow = true;
                $(this).show();
            },
            function() {
                $(this)[0].isShow = false;
                $(this).hide();
            });
    };
    renderBottomMenu = function (h) {
        var g = $("#bottom_menu ul");
        g.html("");
        $.each(h,
            function(j, k) {
                var i = $(
                    ' <li class="mk-site-footer-nav-li"><a href="javascript:void(0);" class="mk-site-footer-nav-item">' +
                    k.F_Name +
                    "</a></li>");
                g.append(i);
            });
    };
    renderPicture = function (h) {
        var g = $(".mk-site-banner-swiper-container .swiper-wrapper");
        g.html("");
        if (h.length > 0) {
            $(".mk-site-banner-swiper-container").show();
            $(".mk-site-banner-default").hide();
            $.each(h,
                function(j, k) {
                    var l = k.src || (top.$.rootUrl + "/PortalSite/HomeConfig/GetImg2?keyValue=" + k.F_Id);
                    var i = $('<div class="swiper-slide">                                   <img class="img"  src="' +
                        l +
                        '" />                               </div>');
                    g.append(i);
                });
        } else {
            $(".mk-site-banner-swiper-container").hide();
            $(".mk-site-banner-default").show();
        }
        bannerSwiper.update();
    };
    addModule = function (i, h) {
        if (!h) {
            var g = $("#moduleAddBtn").parent().prev();
            if (g.length > 0) {
                h = parseInt(g[0].data.F_Sort) + 1;
            } else {
                h = 1;
            }
        }
        switch (i) {
            case "1":
                Changjie.layerForm({
                    id:
                        "ModuleForm1",
                    title: "添加模块(风格一)",
                    url: top.$.rootUrl + "/PortalSite/HomeConfig/ModuleForm1?sort=" + h,
                    height: 700,
                    width: 800,
                    maxmin: true,
                    callBack: function (j) {
                        return top[j].acceptClick(renderModule);
                    }
                });
                break;
            case "2":
                Changjie.layerForm({
                    id:
                        "ModuleForm2",
                    title: "添加模块(风格二)",
                    url: top.$.rootUrl + "/PortalSite/HomeConfig/ModuleForm2?sort=" + h,
                    height: 700,
                    width: 800,
                    maxmin: true,
                    callBack: function (j) {
                        return top[j].acceptClick(renderModule);
                    }
                });
                break;
            case "3":
                Changjie.layerForm({
                    id:
                        "ModuleForm3",
                    title: "添加模块(风格三)",
                    url: top.$.rootUrl + "/PortalSite/HomeConfig/ModuleForm3?sort=" + h,
                    height: 700,
                    width: 800,
                    maxmin: true,
                    callBack: function (j) {
                        return top[j].acceptClick(renderModule);
                    }
                });
                break;
            case "4":
                Changjie.layerForm({
                    id:
                        "ModuleForm4",
                    title: "添加模块(风格四)",
                    url: top.$.rootUrl + "/PortalSite/HomeConfig/ModuleForm4?sort=" + h,
                    height: 700,
                    width: 800,
                    maxmin: true,
                    callBack: function (j) {
                        return top[j].acceptClick(renderModule);
                    }
                });
                break;
            case "5":
                Changjie.layerForm({
                    id:
                        "ModuleForm5",
                    title: "添加模块(风格五)",
                    url: top.$.rootUrl + "/PortalSite/HomeConfig/ModuleForm5?sort=" + h,
                    width: 400,
                    height: 300,
                    maxmin: true,
                    callBack: function (j) {
                        return top[j].acceptClick(renderModule);
                    }
                });
                break;
        }
    };
    renderModule = function (m) {
        var n = JSON.parse(m.F_Scheme);
        var h = $("#moduleAddBtn").parent();
        var i = $('.mk-site-module[data-value="' + m.F_Id + '"]');
        switch (n.type) {
            case "1":
                if (i.length == 0) {
                    i = $('<div class="mk-site-module module1" data-value="' + m.F_Id + '" >                               <div class="mk-site-box">                                   <div class="title">                                       <i class="fa fa-volume-down"></i>                                       <span class="name" >' + m.F_Name + '</span>                                       <span class="arrow"></span>                                   </div>                                   <div class="content">                                       <ul>                                       </ul>                                   </div>                                   <div class="mk-site-more">' + (m.F_Url ? "更多" : "") + '</div>                                   <div class="edit-btn"><span data-value="up" >上移</span><span data-value="down"  >下移</span><span data-value="edit"  >编辑</span><span data-value="delete"  >删除</span></div>                               </div>                           </div>');
                    i.find(".content").SiteCarousel();
                    h.before(i);
                } else {
                    i.find(".title .name").text(m.F_Name);
                    if (m.F_Url && m.F_Url != "&nbsp;") {
                        i.find(".mk-site-more").text("更多");
                    } else {
                        i.find(".mk-site-more").text("");
                    }
                }
                var l = i.find("ul");
                l.html("");
                $.each(n.list,
                    function (q, r) {
                        l.append('<li><a href="javascript:void(0);" class="mk-text-item">' + r.F_Title + "</a></li>");
                    });
                break;
            case "2":
                if (i.length == 0) {
                    i = $('<div class="mk-site-module module2" data-value="' + m.F_Id + '" >                               <div class="mk-site-box">                                   <div class="mk-site-title"><span class="name" >' + m.F_Name + '</span><div class="mk-site-more">' + (m.F_Url ? "更多" : "") + '</div></div>                                   <div class="mk-site-body">                                        <div class="mk-site-module-pic">                                            <div class="mk-site-module-swiper-container swiper-container">                                                <div class="swiper-wrapper">                                                </div>                                                <div class="swiper-pagination"></div>                                            </div>                                        </div>                                        <div class="mk-site-module-list">                                        </div>                                    </div>                                   <div class="edit-btn"><span data-value="up" >上移</span><span data-value="down"  >下移</span><span data-value="edit"  >编辑</span><span data-value="delete"  >删除</span></div>                               </div>                           </div>');
                    h.before(i);
                    i[0].swiper = new Swiper('[data-value="' + m.F_Id + '"] .mk-site-module-swiper-container', {
                        direction: "horizontal",
                        autoplay: true,
                        loop: true,
                        speed: 600,
                        pagination: {
                            el: ".swiper-pagination",
                            clickable: true
                        },
                    })
                } else {
                    i.find(".title .name").text(m.F_Name);
                    if (m.F_Url && m.F_Url != "&nbsp;") {
                        i.find(".mk-site-more").text("更多");
                    } else {
                        i.find(".mk-site-more").text("");
                    }
                }
                i.css({
                    width: parseFloat(n.prop) * 100 + "%"
                });
                if (n.list1.length == 0) {
                    i.addClass("noimg")
                } else {
                    i.removeClass("noimg");
                    var j = i.find(".swiper-wrapper");
                    j.html("");
                    $.each(n.list1,
                        function (r, s) {
                            var q = $('<div class="swiper-slide">                                   <img class="img"  src="' + top.$.rootUrl + "/PortalSite/Article/GetImg?keyValue=" + s.F_Id + '" />                               </div>');
                            j.append(q);
                        });
                    i[0].swiper.update();
                }
                var k = i.find(".mk-site-module-list");
                k.html("");
                $.each(n.list2,
                    function (r, s) {
                        var q = $('<div class="mk-site-module-item">                                    <div class="text">' + s.F_Title + '</div>                                    <div class="date">' + Changjie.formatDate(s.F_PushDate, "yyyy-MM-dd") + "</div>                                </div>");
                        k.append(q);
                    });
                break;
            case "3":
                if (i.length == 0) {
                    i = $('<div class="mk-site-module module3" data-value="' + m.F_Id + '" >                               <div class="mk-site-box">                                   <div class="mk-site-tabs"></div>                                   <div class="mk-site-body">                                    </div>                                   <div class="edit-btn"><span data-value="up" >上移</span><span data-value="down"  >下移</span><span data-value="edit"  >编辑</span><span data-value="delete"  >删除</span></div>                               </div>                           </div>');
                    h.before(i)
                }
                i.css({
                    width: parseFloat(n.prop) * 100 + "%"
                });
                var p = i.find(".mk-site-tabs");
                var o = i.find(".mk-site-body");
                p.html("");
                o.html("");
                $.each(n.list,
                    function (s, t) {
                        var q = $('<div class="mk-site-tab" data-value="' + s + '">' + t.name + "</div>");
                        p.append(q);
                        var r = $('<div class="mk-site-tab-content" data-value="' + s + '"></div>');
                        $.each(t.list,
                            function (u, v) {
                                r.append('<div class="mk-site-tab-content-item">' + v.F_Title + "</div>");
                            });
                        o.append(r);
                        if (s == 0) {
                            q.trigger("click");
                        }
                    });
                break;
            case "4":
                if (i.length == 0) {
                    i = $('<div class="mk-site-module module4" data-value="' + m.F_Id + '" >                               <div class="mk-site-box">                                   <div class="mk-site-title"><span class="name" >' + m.F_Name + '</span><div class="mk-site-more">' + (m.F_Url ? "更多" : "") + '</div></div>                                   <div class="mk-site-body">                                    </div>                                   <div class="edit-btn"><span data-value="up" >上移</span><span data-value="down"  >下移</span><span data-value="edit"  >编辑</span><span data-value="delete"  >删除</span></div>                               </div>                           </div>');
                    h.before(i)
                } else {
                    i.find(".title .name").text(m.F_Name);
                    if (m.F_Url && m.F_Url != "&nbsp;") {
                        i.find(".mk-site-more").text("更多");
                    } else {
                        i.find(".mk-site-more").text("");
                    }
                }
                var g = i.find(".mk-site-body");
                g.html("");
                $.each(n.list,
                    function (q, r) {
                        g.append(' <div class="mk-site-img-item">                                        <div class="mk-site-img-item-content">                                            <img class="img"  src="' + top.$.rootUrl + "/PortalSite/Article/GetImg?keyValue=" + r.F_Id + '" />                                            <div class="text">' + r.F_Title + "</div>                                        </div>                                    </div>")
                    });
                break;
            case "5":
                if (i.length == 0) {
                    i = $('<div class="mk-site-module module5" data-value="' + m.F_Id + '" >                               <div class="mk-site-box">                                   <div class="mk-site-title"><span class="name" >' + m.F_Name + '</span></div>                                   <div class="mk-site-body">                                        <div class="mk-site-article-pic">                                            <img class="img" src="' + top.$.rootUrl + "/PortalSite/Article/GetImg?keyValue=" + n.article + '" />                                        </div>                                        <div class="text-content">                                            <div class="text" data-text="' + n.article + '" ></div>                                        </div>                                    </div>                                   <div class="edit-btn"><span data-value="up" >上移</span><span data-value="down"  >下移</span><span data-value="edit"  >编辑</span><span data-value="delete"  >删除</span></div>                               </div>                           </div>');
                    h.before(i)
                } else {
                    i.find(".title .name").text(m.F_Name);
                    i.find(".img").attr("src", top.$.rootUrl + "/PortalSite/Article/GetImg?keyValue=" + n.article)
                }
                i.css({
                    width: parseFloat(n.prop) * 100 + "%"
                });
                Changjie.httpAsync("GET", top.$.rootUrl + "/PortalSite/Article/GetFormData", {
                    keyValue: n.article
                },
                    function (q) {
                        if (q) {
                            var r = $("<div></div>").html(q.F_Content).text().substring(0, 106) || "";
                            if (r.length == 106) {
                                r = r.substring(0, 105) + "...";
                            }
                            $('.module5 [data-text="' + q.F_Id + '"]').text(r);
                        }
                    });
                break;
        }
        i[0].data = m;
    };
    cpage.init();
};

