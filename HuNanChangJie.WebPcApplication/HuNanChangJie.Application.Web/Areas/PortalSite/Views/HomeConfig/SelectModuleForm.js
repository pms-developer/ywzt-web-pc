﻿var bootstrap = function (a, b) {
    var c = {
        init: function () {
            c.bind();
        },
        bind: function () {
            a(".box-content").on("click",
                function() {
                    var d = a(this).attr("data-value");
                    b.frameTab.currentIframe().addModule(d);
                    b.layerClose(window.name);
                });
        }
    };
    c.init();
};