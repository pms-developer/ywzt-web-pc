﻿var refreshGirdData;
var bootstrap = function(a, e) {
    var c = "";
    var d = "";
    var b = "";
    var f = {
        init: function() {
            f.initGird();
            f.bind()
        },
        bind: function() {
            a("#datesearch").mkdate({
                dfdata: [{
                    name: "今天",
                    begin: function() {
                        return e.getDate("yyyy-MM-dd 00:00:00")
                    },
                    end: function() {
                        return e.getDate("yyyy-MM-dd 23:59:59")
                    }
                },
                {
                    name: "近7天",
                    begin: function() {
                        return e.getDate("yyyy-MM-dd 00:00:00", "d", -6)
                    },
                    end: function() {
                        return e.getDate("yyyy-MM-dd 23:59:59")
                    }
                },
                {
                    name: "近1个月",
                    begin: function() {
                        return e.getDate("yyyy-MM-dd 00:00:00", "m", -1)
                    },
                    end: function() {
                        return e.getDate("yyyy-MM-dd 23:59:59")
                    }
                },
                {
                    name: "近3个月",
                    begin: function() {
                        return e.getDate("yyyy-MM-dd 00:00:00", "m", -3)
                    },
                    end: function() {
                        return e.getDate("yyyy-MM-dd 23:59:59")
                    }
                }],
                mShow: false,
                premShow: false,
                jShow: false,
                prejShow: false,
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                selectfn: function(g, h) {
                    c = g;
                    d = h;
                    f.search()
                }
            });
            a("#btn_Search").on("click",
            function() {
                var h = {};
                var g = a("#txt_Keyword").val();
                h.dateBegin = c;
                h.dateEnd = d;
                h.F_Category = b;
                h.F_Title = g;
                a("#gridtable").jfGridSet("reload", {
                    queryJson: JSON.stringify(h)
                })
            });
            a("#F_Category").mktree({
                url: top.$.rootUrl + "/SystemModule/DataItem/GetDetailTree",
                param: {
                    itemCode: "PortalSiteType"
                },
                nodeClick: f.treeNodeClick
            });
            a("#refresh").on("click",
            function() {
                location.reload()
            });
            a("#add").on("click",
            function() {
                e.frameTab.open({
                    F_ModuleId: "PL_ArticleInfo_List_add",
                    F_Icon: "fa fa-file-text-o",
                    F_FullName: "新增文章信息",
                    F_UrlAddress: "/PortalSite/Article/Form?categoryId=" + b
                });
            });
            a("#edit").on("click",
            function() {
                var g = a("#gridtable").jfGridValue("F_Id");
                if (e.checkrow(g)) {
                    if (g.split(",").length > 1) {
                        e.alert.warning("编辑只能选择一项")
                    } else {
                        e.frameTab.open({
                            F_ModuleId: "PL_ArticleInfo_List_add",
                            F_Icon: "fa fa-file-text-o",
                            F_FullName: "编辑文章信息",
                            F_UrlAddress: "/PortalSite/Article/Form?keyValue=" + g
                        })
                    }
                }
            });
            a("#delete").on("click",
            function() {
                var g = a("#gridtable").jfGridValue("F_Id");
                if (e.checkrow(g)) {
                    e.layerConfirm("是否确认删除所有选择项！",
                    function(h) {
                        if (h) {
                            e.deleteForm(top.$.rootUrl + "/PortalSite/Article/DeleteForm", {
                                keyValue: g
                            },
                            function() {
                                refreshGirdData()
                            })
                        }
                    })
                }
            });
            a("#category").on("click",
            function() {
                e.layerForm({
                    id: "ClassifyIndex",
                    title: "分类管理",
                    url: top.$.rootUrl + "/SystemModule/DataItem/DetailIndex?itemCode=PortalSiteType",
                    width: 800,
                    height: 500,
                    maxmin: true,
                    btn: null,
                    end: function() {
                        e.clientdata.update("dataItem");
                        location.reload()
                    }
                })
            })
        },
        treeNodeClick: function(g) {
            b = g.value;
            f.search()
        },
        initGird: function() {
            a("#gridtable").mkAuthorizeJfGrid({
                url: top.$.rootUrl + "/PortalSite/Article/GetPageList",
                headData: [{
                    label: "标题",
                    name: "F_Title",
                    width: 200,
                    align: "left"
                },
                {
                    label: "分类",
                    name: "F_Category",
                    width: 150,
                    align: "center",
                    formatterAsync: function(g, i, h) {
                        e.clientdata.getAsync("dataItem", {
                            key: i,
                            code: "PortalSiteType",
                            callback: function(j) {
                                g(j.text)
                            }
                        })
                    }
                },
                {
                    label: "发布时间",
                    name: "F_PushDate",
                    width: 100,
                    align: "center",
                    formatter: function(g) {
                        return e.formatDate(g, "yyyy-MM-dd")
                    }
                },
                {
                    label: "创建人",
                    name: "CreationName",
                    width: 100,
                    align: "center"
                },
                ],
                mainId: "F_Id",
                isPage: true,
                isMultiselect: true,
                sidx: "F_PushDate desc"
            });
            f.search()
        },
        search: function(g) {
            g = g || {};
            g.dateBegin = c;
            g.dateEnd = d;
            g.F_Category = b;
            a("#gridtable").jfGridSet("reload", {
                queryJson: JSON.stringify(g)
            })
        }
    };
    refreshGirdData = function() {
        f.search()
    };
    f.init()
};