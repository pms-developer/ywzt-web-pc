﻿jQuery.extend({
    createUploadIframe: function (id, mytype) {
        var ajid = "jUploadFrame" + id;
        var c = '<iframe id="' + ajid + '" name="' + ajid + '" style="position:absolute; top:-9999px; left:-9999px"';
        if (window.ActiveXObject) {
            if (typeof mytype == "boolean") {
                c += ' src="javascript:false"';
            } else {
                if (typeof mytype == "string") {
                    c += ' src="' + mytype + '"';
                }
            }
        } c += " />";
        jQuery(c).appendTo(document.body);
        return jQuery("#" + ajid).get(0);
    },
    createUploadForm: function (gid, eid, inputs) {
        var ejformid = "jUploadForm" + gid;
        var cjfileid = "jUploadFile" + gid;
        var dpostElement = jQuery('<form  action="" method="POST" name="' + ejformid + '" id="' + ejformid + '" enctype="multipart/form-data"></form>');
        if (inputs) {
            for (var f in inputs) {
                jQuery('<input type="hidden" name="' + f + '" value="' + inputs[f].replace(/"([^"]*)"/g, "'$1'") + '" />').appendTo(dpostElement);
            }
        } var j = jQuery("#" + eid);
        var h = jQuery(j).clone();
        jQuery(j).attr("id", cjfileid);
        jQuery(j).before(h);
        jQuery(j).appendTo(dpostElement);
        jQuery(dpostElement).css("position", "absolute");
        jQuery(dpostElement).css("top", "-1200px");
        jQuery(dpostElement).css("left", "-1200px");
        jQuery(dpostElement).appendTo("body");
        return dpostElement;
    },
    ajaxFileUpload: function (i) {
        i = jQuery.extend({}, jQuery.ajaxSettings, i);
        var f = new Date().getTime();
        var b = jQuery.createUploadForm(f, i.fileElementId, (typeof (i.data) == "undefined" ? false : i.data));
        var g = jQuery.createUploadIframe(f, i.secureuri);
        var d = "jUploadFrame" + f;
        var c = "jUploadForm" + f;
        if (i.global && !jQuery.active++) {
            jQuery.event.trigger("ajaxStart");
        } var h = false;
        var k = {};
        if (i.global) {
            jQuery.event.trigger("ajaxSend", [k, i]);
        } var j = function (o) {
            var n = document.getElementById(d);
            try {
                if (n.contentWindow) {
                    k.responseText = n.contentWindow.document.body ? n.contentWindow.document.body.innerHTML : null;
                    k.responseXML = n.contentWindow.document.XMLDocument ? n.contentWindow.document.XMLDocument : n.contentWindow.document
                } else {
                    if (n.contentDocument) {
                        k.responseText = n.contentDocument.document.body ? n.contentDocument.document.body.innerHTML : null;
                        k.responseXML = n.contentDocument.document.XMLDocument ? n.contentDocument.document.XMLDocument : n.contentDocument.document
                    }
                }
            } catch (m) {
                jQuery.handleError(i, k, null, m);
            } if (k || o == "timeout") {
                h = true;
                var p;
                try {
                    p = o != "timeout" ? "success" : "error";
                    if (p != "error") {
                        var l = jQuery.uploadHttpData(k, i.dataType);
                        if (i.success) {
                            i.success(l, p);
                        } if (i.global) {
                            jQuery.event.trigger("ajaxSuccess", [k, i]);
                        }
                    } else {
                        jQuery.handleError(i, k, p);
                    }
                } catch (m) {
                    p = "error";
                    jQuery.handleError(i, k, p, m);
                } if (i.global) {
                    jQuery.event.trigger("ajaxComplete", [k, i]);
                } if (i.global && !--jQuery.active) {
                    jQuery.event.trigger("ajaxStop");
                } if (i.complete) {
                    i.complete(k, p);
                } jQuery(n).unbind();
                setTimeout(function () {
                    try {
                        jQuery(n).remove();
                        jQuery(b).remove();
                    } catch (q) {
                        jQuery.handleError(i, k, null, q);
                    }
                }, 100);
                k = null;
            }
        };
        if (i.timeout > 0) {
            setTimeout(function() {
                    if (!h) {
                        j("timeout");
                    }
                },
                i.timeout);
        } try {
            var b = jQuery("#" + c);
            jQuery(b).attr("action", i.url);
            jQuery(b).attr("method", "POST");
            jQuery(b).attr("target", d);
            if (b.encoding) {
                jQuery(b).attr("encoding", "multipart/form-data");
            } else {
                jQuery(b).attr("enctype", "multipart/form-data");
            }
            jQuery(b).submit();
        } catch (a) {
            jQuery.handleError(i, k, null, a);
        } jQuery("#" + d).load(j);
        return { abort: function () { } }
    }, uploadHttpData: function (r, type) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        if (type == "script") {
            jQuery.globalEval(data);
        } if (type == "json") {
            eval("data = " + data);
        } if (type == "html") {
            jQuery("<div>").html(data).evalScripts();
        }
        return data;
    }
});
var acceptClick;
var keyValue = request("keyValue");
var categoryId = request("categoryId");
var bootstrap = function ($, cchangjie) {
    var eurl = ""; 
    "use strict";
    var fueEditor = UE.getEditor("F_Content", { initialFrameWidth: null, autoHeightEnabled: false });
    $("#F_Content")[0].ue = fueEditor;
    function g() {
        var hupfile = document.getElementById("uploadFile").files[0];
        eurl = window.URL.createObjectURL(hupfile);
        $("#F_ImgName1").html(hupfile.name);
    }

    var b = function (h) {
        if (!$("#form1").mkValidform()) {
            return false;
        } var i = $("#form1").mkGetFormData();
        var jpostdata = { F_ImgName: i.F_ImgName, F_Img: i.F_Img, F_Title: i.F_Title, F_Category: i.F_Category, F_PushDate: i.F_PushDate, F_Content: i.F_Content };
        cchangjie.loading(true, "正在保存...");
        
        $.ajaxFileUpload({
            data: jpostdata,
            url: top.$.rootUrl + "/PortalSite/Article/UploadFile?keyValue=" + keyValue,
            secureuri: false,
            fileElementId: "uploadFile",
            dataType: "json",
            success: function (data) {
                cchangjie.loading(false);
                if (data.code == "200") {
                    cchangjie.frameTab.parentIframe().refreshGirdData();
                    cchangjie.frameTab.close("PL_ArticleInfo_List_add");
                }
            }
        });
    };
    var page = {
        init: function () {
            page.bind();
            page.initData();
        }, bind: function () {
            $("#F_Category").mkDataItemSelect({ code: "PortalSiteType" });
            $("#F_Category").mkselectSet(categoryId);
            $("#savaAndAdd").on("click", function () {
                b();
            });
            $("#save").on("click", function () {
                cchangjie.frameTab.close("PL_ArticleInfo_List_add");
            });
            $("#uploadFile").on("change", g);
            $("#closebutton").on("click", function () {
                $("#showimg").html("");
                document.getElementById("showimg").style.display = "none";
                document.getElementById("closebutton").style.display = "none";
            });
            $("#F_ImgName1").on("click",
                function() {
                    if (eurl != "") {
                        var himg = "<img src=" + eurl + ">";
                        $("#showimg").html(himg);
                        document.getElementById("showimg").style.display = "block";
                        document.getElementById("closebutton").style.display = "block";
                    } else {
                        if (keyValue) {
                            if ($("#F_Img").val()) {
                                $("#showimg").html('<img src="' +
                                    top.$.rootUrl +
                                    "/PortalSite/Article/GetImg?keyValue=" +
                                    keyValue +
                                    '" >');
                                document.getElementById("showimg").style.display = "block";
                                document.getElementById("closebutton").style.display = "block";
                            }
                        }
                    }
                });
        }, initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + "/PortalSite/Article/GetFormData?keyValue=" + keyValue,
                    function (data) {
                        $("#form1").mkSetFormData(data);
                        $("#F_ImgName1").html(data.F_ImgName);
                    });
            }
        }
    };
    page.init();
};