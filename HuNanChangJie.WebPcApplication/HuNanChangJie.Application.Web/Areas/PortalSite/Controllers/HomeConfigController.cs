﻿using HuNanChangJie.Application.TwoDevelopment.PortalSite;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.PortalSite.Controllers
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-01-29 15:12
    /// 描 述：首页配置
    /// </summary>
    public class HomeConfigController : MvcControllerBase
    {
        private HomeConfigIBLL homeConfigIBLL = new HomeConfigBLL();


        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult TopMenuIndex()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TopMenuForm()
        {
            return View();
        }


        public ActionResult PictureForm()
        {
            return View();
        }


        [HttpGet]
        public ActionResult DeleteForm()
        {
            return View();
        }

        public ActionResult SelectModuleForm()
        {
            return View();
        }

        public ActionResult BottomMenuIndex()
        {
            return View();
        }
        public ActionResult BottomMenuForm()
        {
            return View();
        }


        public ActionResult ModuleForm1()
        {
            return View("ModuleFormes/ModuleForm1/ModuleForm1");
        }
        public ActionResult ModuleForm2()
        {
            return View("ModuleFormes/ModuleForm2/ModuleForm2");
        }

        public ActionResult ModuleForm3()
        {
            return View("ModuleFormes/ModuleForm3/ModuleForm3");
        }

        public ActionResult ModuleForm4()
        {
            return View("ModuleFormes/ModuleForm4/ModuleForm4");
        }

        public ActionResult ModuleForm5()
        {
            return View("ModuleFormes/ModuleForm5/ModuleForm5");
        }
        public ActionResult AddTabForm()
        {
            return View("ModuleFormes/AddTabForm");
        }

        public ActionResult PreviewForm()
        {
            return View("PreviewForm");
        }


        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = homeConfigIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var PS_HomeConfigData = homeConfigIBLL.GetPS_HomeConfigEntity(keyValue);

            return Success(PS_HomeConfigData);
        }


        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTree()
        {
            return Success(GetData());
        }

        public List<TreeModel> GetData()
        {
            List<TreeModel> result = homeConfigIBLL.GetAllList().Select(x => new TreeModel()
            {
                id = x.F_Id,
                parentId = x.F_ParentId,
                text = x.F_Name,
                value = x.F_Id
            }).ToList().ToTree("0");

            return result;
        }

        //递归获取子节点




        [HttpGet]

        public ActionResult GetImg(string type)
        {
            var queryAllList = homeConfigIBLL.GetAllList().Where(p => p.F_Type == type);
            PS_HomeConfigEntity homeConfigEntity = null;
            if (queryAllList.Any())
            {
                homeConfigEntity = queryAllList.First();
            }

            if (homeConfigEntity == null)
            {
                return null;
            }

            if (!string.IsNullOrEmpty(homeConfigEntity.F_Img))
            {
                string fileHomeImg = Config.GetValue("fileHomeImg");
                string fullFileName = string.Format("{0}/{1}/{2}", fileHomeImg, "Home", homeConfigEntity.F_Img + ".png");
                if (DirFileHelper.IsExistFile(fullFileName))
                {

                    FileDownHelper.DownLoadnew(fullFileName);
                    return null;
                }
            }
            return null;
        }


        public ActionResult UploadFile(string type)
        {

            HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;
            // return Success("成功");
            //没有文件上传，直接返回
            if (!(files[0].ContentLength == 0 || string.IsNullOrEmpty(files[0].FileName)))
            {
                var queryAllList = homeConfigIBLL.GetAllList().Where(p => p.F_Type == type);
                PS_HomeConfigEntity homeConfigEntity = null;
                if (queryAllList.Any())
                {
                    homeConfigEntity = queryAllList.First();
                }

                if (homeConfigEntity == null)
                {
                    homeConfigEntity = new PS_HomeConfigEntity();
                    homeConfigEntity.F_Img = Guid.NewGuid().ToString();
                }

                homeConfigEntity.F_Type = type;



                if (string.IsNullOrEmpty(homeConfigEntity.F_Img))
                {
                    homeConfigEntity.F_Img = Guid.NewGuid().ToString();
                }
                //说明有文件 TODO 
                string fileHomeImg = Config.GetValue("fileHomeImg");
                string fullFileName = string.Format("{0}/{1}/{2}", fileHomeImg, "Home", homeConfigEntity.F_Img + ".png");
                //创建文件夹，保存文件
                string path = Path.GetDirectoryName(fullFileName);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                files[0].SaveAs(fullFileName);
                homeConfigIBLL.SaveEntity(homeConfigEntity.F_Id ?? "", homeConfigEntity);
            }

            return Success("保存成功！");
        }




        public ActionResult GetImg2(string keyValue)
        {

            if (string.IsNullOrEmpty(keyValue))
            {
                string rootPath = Server.MapPath("~");
                FileDownHelper.DownLoadnew(Path.Combine(rootPath, "Content/images/plhome/addImg.png"));
            }
            else
            {
                var entity = homeConfigIBLL.GetPS_HomeConfigEntity(keyValue);
                string fileHomeImg = Config.GetValue("fileHomeImg");
                string fullFileName = string.Format("{0}/{1}/{2}", fileHomeImg, "Home", entity.F_Img + ".png");
                if (DirFileHelper.IsExistFile(fullFileName))
                {

                    FileDownHelper.DownLoadnew(fullFileName);
                    return null;
                }


            }
            return null;
        }


        public ActionResult UploadFile2(string keyValue, string sort)
        {

            HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;
            // return Success("成功");
            //没有文件上传，直接返回
            if (!(files[0].ContentLength == 0 || string.IsNullOrEmpty(files[0].FileName)))
            {
                PS_HomeConfigEntity psEntity = homeConfigIBLL.GetPS_HomeConfigEntity(keyValue);
                if (psEntity == null)
                {
                    psEntity = new PS_HomeConfigEntity();
                    psEntity.F_Type = "8";
                }
                psEntity.F_Sort = int.Parse(sort);


                if (string.IsNullOrEmpty(psEntity.F_Img))
                {
                    psEntity.F_Img = Guid.NewGuid().ToString();
                }
                //说明有文件 TODO 
                string fileHomeImg = Config.GetValue("fileHomeImg");
                string fullFileName = string.Format("{0}/{1}/{2}", fileHomeImg, "Home", psEntity.F_Img + ".png");
                //创建文件夹，保存文件
                string path = Path.GetDirectoryName(fullFileName);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                files[0].SaveAs(fullFileName);
                homeConfigIBLL.SaveEntity(psEntity.F_Id ?? "", psEntity);
            }

            return Success("保存成功！");
        }


        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList(int type)
        {
            List<PS_HomeConfigEntity> result = homeConfigIBLL.GetList(type);
            return Success(result);
        }

        public ActionResult GetAllList()
        {
            List<PS_HomeConfigEntity> result = homeConfigIBLL.GetAllList();
            return Success(result);
        }

        public ActionResult SetTextForm(string type)
        {
            return View();
        }

        public ActionResult UpdateForm(string keyValue1, string keyValue2)
        {

            var query1 = homeConfigIBLL.GetPS_HomeConfigEntity(keyValue1);
             
            var query2 = homeConfigIBLL.GetPS_HomeConfigEntity(keyValue2);

            int? tempsort = query1.F_Sort;

            query1.F_Sort = query2.F_Sort;
            query2.F_Sort = tempsort;

            homeConfigIBLL.SaveEntity(query2.F_Id, query2);
            homeConfigIBLL.SaveEntity(query1.F_Id,query1);






            return Success("更新成功");
        }

        public ActionResult SaveText(string type, string text)
        {
            var queryAllList = homeConfigIBLL.GetAllList().Where(p => p.F_Type == type);
            PS_HomeConfigEntity homeConfigEntity = null;
            if (queryAllList.Any())
            {
                homeConfigEntity = queryAllList.First();
            }

            if (homeConfigEntity == null)
            {
                homeConfigEntity = new PS_HomeConfigEntity();

            }

            homeConfigEntity.F_Type = type;
            homeConfigEntity.F_Name = text;

            homeConfigIBLL.SaveEntity(homeConfigEntity.F_Id ?? "", homeConfigEntity);

            return Success("保存成功");
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            homeConfigIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, PS_HomeConfigEntity psEntity)
        {

            homeConfigIBLL.SaveEntity(keyValue, psEntity);
            return Success("保存成功！");
        }
        #endregion




    }



}
