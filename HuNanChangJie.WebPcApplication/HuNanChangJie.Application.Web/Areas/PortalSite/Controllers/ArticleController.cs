﻿using System;
using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.PortalSite;
using System.Web.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Web;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.Web.Areas.PortalSite.Controllers
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-01-29 15:22
    /// 描 述：文章
    /// </summary>
    public class ArticleController : MvcControllerBase
    {
        private ArticleIBLL articleIBLL = new ArticleBLL();
        private AnnexesFileIBLL annexesFileIbll = new AnnexesFileBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = articleIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList(string queryJson)
        {
           
            List<PS_ArticleEntity> data = articleIBLL.GetList(queryJson);
             
            return Success(data);
        }



        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var PS_ArticleData = articleIBLL.GetPS_ArticleEntity( keyValue );
            //var jsonData = new {
            //    PS_Article = PS_ArticleData,
            //};
            return Success(PS_ArticleData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            articleIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            PS_ArticleEntity entity = strEntity.ToObject<PS_ArticleEntity>();
            articleIBLL.SaveEntity(keyValue,entity);
            return Success("保存成功！");
        }
        #endregion

        /// <summary>
        /// 获取图片地址
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>

        public ActionResult GetImg(string keyValue)
        {
            var PS_ArticleData = articleIBLL.GetPS_ArticleEntity(keyValue);
            if (!string.IsNullOrEmpty(PS_ArticleData.F_Img))
            {
                string fileHomeImg = Config.GetValue("fileHomeImg");
                string fullFileName = string.Format("{0}/{1}/{2}", fileHomeImg, "Home", PS_ArticleData.F_Img + ".png");
                if (DirFileHelper.IsExistFile(fullFileName))
                {

                    FileDownHelper.DownLoadnew(fullFileName);
                    return null;
                }
            }

            return null;
        }


        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
       
        public ActionResult UploadFile(string keyValue , PS_ArticleEntity bodyEntity)
        {
            HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;
           // return Success("成功");
            //没有文件上传，直接返回
            if (!(files[0].ContentLength == 0 || string.IsNullOrEmpty(files[0].FileName)))
            {
                if (string.IsNullOrEmpty(bodyEntity.F_Img))
                {
                    bodyEntity.F_Img = Guid.NewGuid().ToString();
                }
                //说明有文件 TODO 
                string fileHomeImg = Config.GetValue("fileHomeImg");
                string fullFileName = string.Format("{0}/{1}/{2}", fileHomeImg, "Home", bodyEntity.F_Img + ".png");
                //创建文件夹，保存文件
                string path = Path.GetDirectoryName(fullFileName);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                bodyEntity.F_ImgName = files[0].FileName;
                files[0].SaveAs(fullFileName);

            }
           
          

            try
            {
                
               
                    articleIBLL.SaveEntity(keyValue, bodyEntity);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return Fail(e.Message);
            }

            return Success("保存成功");
        }
    }
}
