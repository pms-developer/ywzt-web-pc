﻿using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.PortalSite
{
    public class PortalSiteAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PortalSite";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PortalSite_default",
                "PortalSite/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}