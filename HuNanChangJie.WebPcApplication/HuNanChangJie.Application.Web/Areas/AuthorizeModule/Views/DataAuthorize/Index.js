﻿/*
 * 日  期：2017-06-21 16:30
 * 描  述：数据权限
 */
var objectId = request("objectId");
var objectType = request("objectType");

var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";

    var interfaceId = '';

    var page = {
        init: function () {
            page.inittree();
            page.initGrid();
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            if (!!objectId) {
                // 新增
                $('#add').on('click', function () {
                    if (!interfaceId) {
                        Changjie.alert.warning('请选择左侧接口！');
                        return false;
                    }
                    Changjie.layerForm({
                        id: 'form',
                        title: '新增数据权限',
                        url: top.$.rootUrl + '/AuthorizeModule/DataAuthorize/Form?interfaceId=' + interfaceId + '&objectId=' + objectId + '&objectType=' + objectType,
                        width: 700,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                });
            }
            else {
                $('#add').hide();
            }
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑数据权限',
                        url: top.$.rootUrl + '/AuthorizeModule/DataAuthorize/Form?interfaceId=' + interfaceId + '&objectId=' + objectId + '&objectType=' + objectType + '&keyValue=' + keyValue,
                        width: 700,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/AuthorizeModule/DataAuthorize/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });

            /*接口管理*/
            $('#interface').on('click', function () {
                Changjie.layerForm({
                    id: 'InterfaceIndex',
                    title: '接口管理',
                    url: top.$.rootUrl + '/SystemModule/Interface/Index',
                    width: 800,
                    height: 500,
                    maxmin: true,
                    btn: null,
                    end: function () {
                        location.reload();
                    }
                });
            });
        },
        inittree: function () {
            $('#interface_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/Interface/GetTree',
                nodeClick: page.treeNodeClick
            });
        },
        treeNodeClick: function (item) {
            interfaceId = item.id;
            $('#titleinfo').text(item.text);
            page.search();
        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/AuthorizeModule/DataAuthorize/GetRelationPageList',
                headData: [
                    { label: "名称", name: "F_Name", width: 180 },
                    {
                        label: "用户/角色", name: "F_ObjectId", width: 180,
                        cellRenderer: function (param) {
                            return !!param.data.UserName ? param.data.UserName : param.data.RoleName;
                        }
                    },
                    { label: "公式", name: "F_Formula", width: 280 },
                    {
                        label: '创建人', name: 'CreationName', width: 100, align: 'left'
                    },
                    {
                        label: "创建时间", name: "CreationDate", width: 100,
                        cellRenderer: function (cellvalue) {
                            return Changjie.formatDate(cellvalue.value, 'yyyy-MM-dd');
                        }
                    }
                ],
                isPage: true,
                reloadSelected: true,
                mainId: 'F_Id'
            });
            page.search();
        },
        search: function (param) {
            param = param || { };
            param.interfaceId = interfaceId;
            param.objectId = objectId;
            $('#gridtable').AgGridSet('reload', param);
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
