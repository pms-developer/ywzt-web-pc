﻿/*
 * 日 期：2017.04.05
 * 描 述：功能模块	
 */
var objectId = request('objectId');
var objectType = request('objectType');
var g_currentstep = 1;
var bootstrap = function ($, Changjie) {
    "use strict";

    var selectData;

    var treeLoadDataFinish = [];
    var checkModuleIds = [];

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        loadTree: function (type) {
            Changjie.httpAsyncGet(top.$.rootUrl + '/SystemModule/Module/GetCheckTreeByType?type=' + type, function (res) {
                if (res.code == 200) {
                    $.each(res.data, function (ii, ee) {
                        ee.isexpand = false;
                        if (ee.ChildNodes) {
                            $.each(ee.ChildNodes, function (iii, eee) {
                                eee.isexpand = false;
                            })
                        }
                    })

                    $('#step-' + type).mktree({
                        data: res.data
                    });

                    if (!!selectData) {
                        if (type == 1) {
                            $('#step-1').mktreeSet('setCheck', selectData.modules);
                        }
                        else if (type == 2) {
                            $('#step-2').mktreeSet('setCheck', selectData.buttons);
                        }
                        else if (type == 3) {
                            $('#step-3').mktreeSet('setCheck', selectData.columns);
                        }
                        else if (type == 4) {
                            $('#step-4').mktreeSet('setCheck', selectData.forms);
                        }
                    }

                    treeLoadDataFinish[type - 1] = true;
                }
            })
        },
        /*绑定事件和初始化控件*/
        bind: function () {
            
            // 加载导向
            $('#wizard').wizard().on('change', function (e, data) {
                g_currentstep = data.currentStep;
                var $finish = $("#btn_finish");
                var $next = $("#btn_next");
                if (data.direction == "next") {
                    if (!treeLoadDataFinish[g_currentstep - 1]) {
                        page.loadTree(g_currentstep);
                    }

                    //console.log(data,"abcd")
                    if (data.step == 1) {
                        checkModuleIds = $('#step-1').mktreeSet('getCheckNodeIds');
                        $('#step-2 .mk-tree-root [id$="_changjie_moduleId"]').parent().hide();
                        $('#step-3 .mk-tree-root [id$="_changjie_moduleId"]').parent().hide();
                        $('#step-4 .mk-tree-root [id$="_changjie_moduleId"]').parent().hide();
                        $.each(checkModuleIds, function (id, item) {
                            $('#step-2_' + item.replace(/-/g, '_') + '_changjie_moduleId').parent().show();
                            $('#step-3_' + item.replace(/-/g, '_') + '_changjie_moduleId').parent().show();
                            $('#step-4_' + item.replace(/-/g, '_') + '_changjie_moduleId').parent().show();
                        });
                    } else if (data.step == 3) {
                       
                        $finish.removeAttr('disabled');
                        $next.attr('disabled', 'disabled');
                    } else {
                        $finish.attr('disabled', 'disabled');
                    }
                } else {
                    $finish.attr('disabled', 'disabled');
                    $next.removeAttr('disabled');
                }

                $("#checkall").unbind('click').removeProp("checked").on('click', function () {
                    page.bindallcheck(this)
                });
            });
            // 保存数据按钮
            $("#btn_finish").on('click', page.save);
            $("#checkall").on('click', function () {
                page.bindallcheck(this)
            });
        },
        /*初始化数据*/
        initData: function () {
            if (!!objectId) {
                $.mkSetForm(top.$.rootUrl + '/AuthorizeModule/Authorize/GetFormData?objectId=' + objectId, function (data) {//
                    selectData = data;
                    page.loadTree(1);

                    setTimeout(function () {
                        page.loadTree(2);
                    }, 50)

                    setTimeout(function () {
                        page.loadTree(3);
                    }, 100)

                    setTimeout(function () {
                        page.loadTree(4);
                    }, 150)
                });
            }
        },
        bindallcheck: function (ee) {
            if (ee.checked) {
                if (g_currentstep == 2) {
                    $('#step-2').mktreeSet('newAllCheck');
                }
                else if (g_currentstep == 3) {
                    $('#step-3').mktreeSet('newAllCheck');
                }
                else if (g_currentstep == 4) {
                    $('#step-4').mktreeSet('newAllCheck');
                }
                else {
                    $('#step-1').mktreeSet('newAllCheck');
                }
            }
            else {
                if (g_currentstep == 2) {
                    $('#step-2').mktreeSet('allNoCheck');
                }
                else if (g_currentstep == 3) {
                    $('#step-3').mktreeSet('allNoCheck');
                }
                else if (g_currentstep == 4) {
                    $('#step-4').mktreeSet('allNoCheck');
                }
                else
                    $('#step-1').mktreeSet('allNoCheck');
            }
        },
        /*保存数据*/
        save: function () {
            var buttonList = [], columnList = [],formList = [];
            var checkButtonIds = $('#step-2').mktreeSet('getCheckNodeIds');
            var checkColumnIds = $('#step-3').mktreeSet('getCheckNodeIds');
            var checkFormIds = $('#step-4').mktreeSet('getCheckNodeIds');
            
            
            $.each(checkButtonIds, function (id, item) {
                if (item.indexOf('_changjie_moduleId') == -1) {
                    buttonList.push(item);
                }
            });
            $.each(checkColumnIds, function (id, item) {
                if (item.indexOf('_changjie_moduleId') == -1) {
                    columnList.push(item);
                }
            });
            $.each(checkFormIds, function (id, item) {
                if (item.indexOf('_changjie_moduleId') == -1) {
                    formList.push(item);
                }
            });


            var postData = {
                objectId: objectId,
                objectType: objectType,
                strModuleId: String(checkModuleIds),
                strModuleButtonId: String(buttonList),
                strModuleColumnId: String(columnList),
                strModuleFormId: String(formList)
            };
            $.mkSaveForm(top.$.rootUrl + '/AuthorizeModule/Authorize/SaveForm', postData, function (res) {
                if (res.code == 200) {
                    
                }
            });
        }
    };

    page.init();
}