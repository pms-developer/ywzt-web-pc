﻿using HuNanChangjie.Workflow.WorkflowRecord;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using HuNanChangJie.Workflow.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.WorkFlowModule.Controllers
{
    public class WorkFlowController : MvcControllerBase
    {
        IWorkflowRecord _workflow = new WorkflowRecordBll();
        // GET: WorkFlowModule/WorkFlow

        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult StartForm()
        {
            return View();
        }

        [HttpGet]
        public ActionResult NodeHistory()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TaskIndex()
        {
            return View();
        }

        [HttpGet]
        public ActionResult DoneTaskIndex()
        {
            return View();
        }

        [HttpGet]
        public ActionResult NoticeIndex()
        {
            return View();
        }

        /// <summary>
        /// 流程进度
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WfProgress()
        {
            return View();
        }


        [HttpGet]
        public ActionResult AuditForm()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AuditFlowForm()
        {
            return View();
        }

        [HttpGet]
        public ActionResult WorkflowRecordIndex()
        {
            return View();
        }


        /// <summary>
        /// 审核流程
        /// </summary>
        /// <param name="taskId">任务id</param>
        /// <param name="description">描述</param>
        /// <param name="verifyType">审核类型 1：通过 4:加签同过   2：不通过  3：不通过并驳回某一节点 </param>
        /// <param name="nopassType">verifyType为2 即不通过时的处理方式 Back：驳回至上一节点；Toback：驳回至指定节点；End：结束此流程</param>
        /// <param name="toNode">驳回节点Id</param>
        /// <param name="addUserId">加签人id</param>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        public ActionResult Audit(string taskId, string description, string verifyType, string nopassType, string toNode, string addUserId)
        {
            var auditType = verifyType;
            if (nopassType == "Back" || nopassType == "Toback")
            {
                auditType = "3";
            }
            else if (nopassType == "End")
            {
                auditType = "2";
            }
            if (!string.IsNullOrEmpty(addUserId))
            {
                auditType = "4";
            }

            SysWfResult result = new SysWfResult();

            if (taskId.Contains(","))
            {
                string[] taskIdSplit = taskId.Split(',');

                for (int i = 0; i < taskIdSplit.Length; i++)
                {
                    result = _workflow.Audit(LoginUserInfo.UserInfo.userId, taskIdSplit[i], description, auditType, toNode, addUserId);
                }
            }
            else
            {
                result = _workflow.Audit(LoginUserInfo.UserInfo.userId, taskId, description, auditType, toNode, addUserId);
            }

            return JsonResult(result);
        }


        [HttpPost, AjaxOnly]
        public ActionResult Start(string projectId, string infoId, string schemeId, string formId, string workflowName, string level, string description, string workflowId, bool isNew)
        {

            UserInfo userInfo = LoginUserInfo.Get();
            SysWfResult res = _workflow.Create(workflowId, projectId, userInfo.userId, userInfo.realName, infoId, schemeId, formId, workflowName, level, description, isNew);
            return JsonResult(res);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetTaskInfo(string workflowId, string taskId, bool isLook)
        {
            var data = _workflow.GetTaskInfo(workflowId, taskId, isLook);
            return JsonResult(data);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetFinishedNodes(string taskId)
        {
            var data = _workflow.GetFinishedNodes(taskId);
            return JsonResult(data);
        }


        [HttpGet, AjaxOnly]
        public ActionResult GetTaskList(string pagination, string queryJson, string type)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var list = _workflow.GetTaskList(paginationobj, queryJson, type);
            var jsonData = new
            {
                rows = list,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetDoneTaskList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var list = _workflow.GetDoneTaskList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = list,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }

        [HttpPost, AjaxOnly]
        public ActionResult Revocation(string workflowId)
        {
            _workflow.Revocation(workflowId);
            return Success("撤销成功!");
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetNoticeList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var list = _workflow.GetNoticeList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = list,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }


        [HttpGet, AjaxOnly]
        public ActionResult GetWorkflowRecordList(string pagination, string queryJson, string auditStatus)
        {
            var status = (AuditStatus)auditStatus.ToInt();
            XqPagination paginationobj = pagination.ToObject<XqPagination>();


            if (paginationobj != null && string.IsNullOrEmpty(paginationobj.sidx))
            {
                paginationobj.sidx = "CreateTime";
                paginationobj.sord = "DESC";
            }

            var list = _workflow.GetWorkflowRecordList(paginationobj, queryJson, status);
            var jsonData = new
            {
                rows = list,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }

        [HttpPost, AjaxOnly]
        public ActionResult SetNoticeRead(string noticeId)
        {
            _workflow.SetNoticeRead(noticeId);
            return Success("操作成功!");
        }

        [HttpPost, AjaxOnly]
        public ActionResult SetWorkflowRecordRead(string workflowId)
        {
            _workflow.SetWorkflowRecordRead(workflowId);
            return Success("操作成功!");
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetWaitTaskCount()
        {
            var data = _workflow.GetWaitTaskCount();
            var jsonData = new
            {
                count = data
            };
            return JsonResult(jsonData);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetNoticeCount()
        {
            var data = _workflow.GetNoticeCount();
            var jsonData = new
            {
                count = data
            };
            return JsonResult(jsonData);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetWorkflowRecordCount(string auditStatus)
        {
            var status = (AuditStatus)auditStatus.ToInt();
            var data = _workflow.GetWorkflowRecordCount(status);
            var jsonData = new
            {
                count = data
            };
            return JsonResult(jsonData);
        }

        /// <summary>
        /// 获取待处理流程信息
        /// </summary>
        public ActionResult GetWaitProcessInfo()
        {
            var finishedCount = _workflow.GetWorkflowRecordCount(AuditStatus.Pass);
            var notifyCount = _workflow.GetNoticeCount();
            var waitCount = _workflow.GetWaitTaskCount();
            var backCount = _workflow.GetWorkflowRecordCount(AuditStatus.NoPass);
            var json = new
            {
                finishedCount,
                notifyCount,
                waitCount,
                backCount,
                allCount = finishedCount + notifyCount + waitCount + backCount
            };
            return JsonResult(json);
        }

        /// <summary>
        /// 获取历史记录
        /// </summary>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        [HttpGet, AjaxOnly]
        public ActionResult GetHistory(string workflowId)
        {
            var data = _workflow.GetWorkflowHistoryByWorkflowId(workflowId);
            return JsonResult(data);
        }

        /// <summary>
        /// 获取历史记录
        /// </summary>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        [HttpGet, AjaxOnly]
        public ActionResult GetNodeHistoryList(string nodeId)
        {
            var data = _workflow.GetNodeHistoryByNodeId(nodeId);
            return JsonResult(data);
        }
    }
}