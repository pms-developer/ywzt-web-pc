﻿using HuNanChangJie.Application.Base.AuthorizeModule;
using HuNanChangJie.Application.Organization;
using HuNanChangJie.Application.WorkFlow;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.WorkFlowModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2018.01.16
    /// 描 述：流程引擎（流程接口）
    /// </summary>
    public class WfEngineController : MvcControllerBase
    {
        private UserRelationIBLL userRelationIBLL = new UserRelationBLL();
        private UserIBLL userIBLL = new UserBLL();
        
        /// <summary>
        /// 工作流引擎
        /// </summary>
        private WfEngineIBLL wfEngineIBLL = new WfEngineBLL();
        private WfProcessInstanceIBLL wfProcessInstanceIBLL = new WfProcessInstanceBLL();
        private WfTaskIBLL wfTaskIBLL = new WfTaskBLL();
        private WfSchemeIBLL wfSchemeIBLL = new WfSchemeBLL();
        private WFNotifyIBLL wfNotifyIBLL = new WFNotifyBLL();

        #region  获取数据
        /// <summary>
        /// 初始化流程模板->获取开始节点数据
        /// </summary>
        /// <param name="isNew">是否是新发起的实例</param>
        /// <param name="processId">流程实例ID</param>
        /// <param name="wfschemeId">流程模板Id</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult Bootstraper(bool isNew, string processId, string wfschemeId)
        {
            WfParameter wfParameter = new WfParameter();
            UserInfo userInfo = LoginUserInfo.Get();

            wfParameter.companyId = userInfo.companyId;
            wfParameter.departmentId = userInfo.departmentId;
            wfParameter.userId = userInfo.userId;
            wfParameter.userName = userInfo.realName;
            wfParameter.isNew = isNew;
            wfParameter.processId = processId;
            wfParameter.wfschemeId = wfschemeId;

            WfResult<WfContent> res = wfEngineIBLL.Bootstraper(wfParameter);
            return JsonResult(res);
        }
        /// <summary>
        /// 流程任务信息
        /// </summary>
        /// <param name="processId">流程实例ID</param>
        /// <param name="taskId">流程模板编码</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult Taskinfo(string processId, string taskId)
        {
            WfParameter wfParameter = new WfParameter();
            UserInfo userInfo = LoginUserInfo.Get();

            wfParameter.companyId = userInfo.companyId;
            wfParameter.departmentId = userInfo.departmentId;
            wfParameter.userId = userInfo.userId;
            wfParameter.userName = userInfo.realName;
            wfParameter.processId = processId;
            wfParameter.taskId = taskId;

            WfResult<WfContent> res = wfEngineIBLL.GetTaskInfo(wfParameter);
            return JsonResult(res);
        }
        /// <summary>
        /// 获取流程实例信息
        /// </summary>
        /// <param name="processId">流程实例ID</param>
        /// <param name="taskId">流程模板编码</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult Processinfo(string processId, string taskId)
        {
            WfParameter wfParameter = new WfParameter();
            UserInfo userInfo = LoginUserInfo.Get();
            if (taskId == "null")
                taskId = "";
            wfParameter.companyId = userInfo.companyId;
            wfParameter.departmentId = userInfo.departmentId;
            wfParameter.userId = userInfo.userId;
            wfParameter.userName = userInfo.realName;
            wfParameter.processId = processId;
            wfParameter.taskId = taskId;

            WfResult<WfContent> res = wfEngineIBLL.GetProcessInfo(wfParameter);
            return JsonResult(res);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetWorkFlowId(string tableName, string keyValue)
        {
            var workFlowId = wfEngineIBLL.GetWorkFlowId(tableName,keyValue);
            return Success(workFlowId);
        }
        /// <summary>
        /// 获取流程实例信息(流程监控)
        /// </summary>
        /// <param name="processId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult ProcessinfoByMonitor(string processId, string taskId)
        {
            WfParameter wfParameter = new WfParameter();
            UserInfo userInfo = LoginUserInfo.Get();

            wfParameter.companyId = userInfo.companyId;
            wfParameter.departmentId = userInfo.departmentId;
            wfParameter.userId = userInfo.userId;
            wfParameter.userName = userInfo.realName;
            wfParameter.processId = processId;
            wfParameter.taskId = taskId;

            WfResult<WfContent> res = wfEngineIBLL.GetProcessInfoByMonitor(wfParameter);
            return JsonResult(res);
        }

        /// <summary>
        /// 获取已完成审核的节点集合
        /// </summary>
        /// <param name="processId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public ActionResult GetFinishedNodes(string processId, string taskId)
        {
            var nodes = wfEngineIBLL.GetFinishedNodes(processId, taskId);
            return JsonResult(nodes);
        }

        /// <summary>
        /// 获取下一个节点审核人员
        /// </summary>
        /// <param name="processId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult Auditer(bool isNew, string processId, string wfschemeId, string taskId,string formData)
        {
            WfParameter wfParameter = new WfParameter();
            UserInfo userInfo = LoginUserInfo.Get();

            wfParameter.companyId = userInfo.companyId;
            wfParameter.departmentId = userInfo.departmentId;
            wfParameter.userId = userInfo.userId;
            wfParameter.userName = userInfo.realName;

            wfParameter.isNew = isNew;
            wfParameter.processId = processId;
            wfParameter.wfschemeId = wfschemeId;
            wfParameter.taskId = taskId;
            wfParameter.formData = formData;

            WfResult<List<object>> res = wfEngineIBLL.GetAuditer(wfParameter);

            if (res.status == 1)
            {
                List<object> nodelist = new List<object>();
                var list = res.data;
                foreach (var item1 in list) {
                    var item = item1.ToJson().ToJObject();
                    if (item["auditors"].IsEmpty())
                    {
                        var point = new
                        {
                            all = true,
                            name = item["name"],
                            nodeId = item["nodeId"]
                        };
                        nodelist.Add(point);
                    }
                    else {
                        List<object> userlist = new List<object>();
                        foreach (var auditor in item["auditors"]) {
                            switch (auditor["type"].ToString()) {//获取人员信息1.岗位2.角色3.用户
                                case "1":
                                case "2":
                                    GetUserList(userlist, auditor);
                                    break;
                                case "3":
                                    userlist.Add(new { id = auditor["auditorId"], name = auditor["auditorName"] });
                                    break;
                            }

                        }
                        var point = new
                        {
                            name = item["name"],
                            nodeId = item["nodeId"],
                            list = userlist
                        };
                        nodelist.Add(point);
                    }
                }

                return JsonResult(nodelist);
            }
            else
            {
                return Fail("获取数据失败！");
            }
        }


        private void GetUserList(List<object> userlist, Newtonsoft.Json.Linq.JToken auditor)
        {
            var userRelationList = userRelationIBLL.GetUserIdList(auditor["auditorId"].ToString());
            string userIds = "";
            foreach (var userRelation in userRelationList)
            {
                if (userIds != "")
                {
                    userIds += ",";
                }
                userIds += userRelation.F_UserId;
            }
            var userList = userIBLL.GetListByUserIds(userIds);
            if (userList != null)
            {
                foreach (var user in userList)
                {
                    if (user != null)
                    {
                        userlist.Add(new { id = user.F_UserId, name = user.F_RealName });
                    }
                }
            }
        }
        #endregion

        #region  提交数据
        /// <summary>
        /// 创建流程实例
        /// </summary>
        /// <param name="isNew">是否是新发起的实例</param>
        /// <param name="processId">流程实例ID</param>
        /// <param name="wfschemeId">流程模板ID</param>
        /// <param name="processName">流程实例名称</param>
        /// <param name="processLevel">流程重要等级</param>
        /// <param name="description">备注说明</param>
        /// <param name="formData">表单数据</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult Create(bool isNew,string processId,string wfschemeId, string processName,int processLevel,string description,string auditers, string formData) {

            WfParameter wfParameter = new WfParameter();
            UserInfo userInfo = LoginUserInfo.Get();

            wfParameter.companyId = userInfo.companyId;
            wfParameter.departmentId = userInfo.departmentId;
            wfParameter.userId = userInfo.userId;
            wfParameter.userName = userInfo.realName;

            wfParameter.isNew = isNew;
            wfParameter.processId = processId;
            wfParameter.wfschemeId = wfschemeId;
            wfParameter.processName = processName;
            wfParameter.processLevel = processLevel;
            wfParameter.description = description;
            wfParameter.auditers = auditers;
            wfParameter.formData = formData;

            WfResult res = wfEngineIBLL.Create(wfParameter);
            return JsonResult(res);
        }

        /// <summary>
        /// 创建流程实例
        /// </summary>
        /// <param name="isNew">是否是新发起的实例</param>
        /// <param name="processId">流程实例ID</param>
        /// <param name="wfschemeId">流程模板Id</param>
        /// <param name="processName">流程实例名称</param>
        /// <param name="processLevel">流程重要等级</param>
        /// <param name="description">备注说明</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult Start(bool isNew, string processId, string wfschemeId, string processName, int processLevel, string description, string auditers,string formType,string formId,string infoId,string projectId)
        {
            var parma = new WfParameter();
            UserInfo userInfo = LoginUserInfo.Get();

            parma.companyId = userInfo.companyId;
            parma.departmentId = userInfo.departmentId;
            parma.userId = userInfo.userId;
            parma.userName = userInfo.realName;

            parma.isNew = isNew;
            parma.processId = processId;
            parma.wfschemeId = wfschemeId;
            parma.processName = processName;
            parma.processLevel = processLevel;
            parma.description = description;
            parma.auditers = auditers;
            parma.formId = formId;
            parma.formInfoId = infoId;
            parma.formType = formType;
            parma.projectId = projectId;

            var result = wfEngineIBLL.Start(parma);

            return JsonResult(result);
        }

        /// <summary>
        /// 创建流程实例
        /// </summary>
        /// <param name="taskId">流程实例ID</param>
        /// <param name="verifyType">流程模板编码</param>
        /// <param name="description">流程实例名称</param>
        /// <param name="auditorId">加签人员Id</param>
        /// <param name="auditorName">备注说明</param>
        /// <param name="formData">表单数据</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string taskId, string verifyType, string description, string auditorId, string auditorName,string auditers, string formData,string formType, string formId,string infoId, string nopassType,string toNode)
        {

            WfParameter wfParameter = new WfParameter();
            UserInfo userInfo = LoginUserInfo.Get();

            wfParameter.companyId = userInfo.companyId;
            wfParameter.departmentId = userInfo.departmentId;
            wfParameter.userId = userInfo.userId;
            wfParameter.userName = userInfo.realName;

            wfParameter.taskId = taskId;
            wfParameter.verifyType = verifyType;
            wfParameter.auditorId = auditorId;
            wfParameter.auditorName = auditorName;
            wfParameter.description = description;
            wfParameter.auditers = auditers;
            wfParameter.formData = formData;
            wfParameter.formType = formType;
            wfParameter.formId = formId;
            wfParameter.formInfoId = infoId;
            wfParameter.nopassType = nopassType;
            wfParameter.toNode = toNode;

            WfResult res = wfEngineIBLL.Audit(wfParameter);
            return JsonResult(res);
        }
        #endregion

        public ActionResult DirectlyAudit(string formId, string infoId, string formType, string auditType)
        {
            var type = (AuditType)Enum.Parse(typeof(AuditType), auditType);
            WfResult res = wfEngineIBLL.DirectlyAudit(formId,infoId,formType,type);
            return JsonResult(res);
        }

        /// <summary>
        /// 获取待处理流程信息
        /// </summary>
        public ActionResult GetWaitProcessInfo()
        {
            var userId = LoginUserInfo.UserInfo.userId;
            var finishedCount=wfProcessInstanceIBLL.GetFinishedProcessCount(userId);
            var notifyCount = wfNotifyIBLL.GetNotReadNofityCount(LoginUserInfo.UserInfo);
            var waitCount = wfTaskIBLL.GetActiveCount(LoginUserInfo.UserInfo);
            var backCount = wfTaskIBLL.GetBackCount(LoginUserInfo.UserInfo);
            var json = new {
                finishedCount,
                notifyCount,
                waitCount,
                backCount,
                allCount= finishedCount+ notifyCount+ waitCount+ backCount
            };
            return JsonResult(json);
        }
    }
}