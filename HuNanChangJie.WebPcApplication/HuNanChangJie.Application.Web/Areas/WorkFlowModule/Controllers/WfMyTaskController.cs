﻿using HuNanChangJie.Application.WorkFlow;
using HuNanChangJie.Util;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.WorkFlowModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.04.17
    /// 描 述：我的任务
    /// </summary>
    public class WfMyTaskController : MvcControllerBase
    {
        private WfProcessInstanceIBLL wfProcessInstanceIBLL = new WfProcessInstanceBLL();
        private WfTaskIBLL wfTaskIBLL = new WfTaskBLL();
        private WFNotifyIBLL wfNotifyIBLL = new WFNotifyBLL();
        #region  视图功能
        /// <summary>
        /// 主页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult WorkflowIndex()
        {
            return View();
        }

        /// <summary>
        /// 发起流程
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ReleaseForm()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ChooseWorkflow()
        {
            return View();
        }

        /// <summary>
        /// 流程处理页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CustmerWorkFlowForm()
        {
            return View();
        }
        /// <summary>
        /// 流程节点信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WfNodeForm()
        {
            return View();
        }

        /// <summary>
        /// 提交发起流程界面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ReleaseFlowForm()
        {
            return View();
        }


        /// <summary>
        /// 审核流程
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AuditFlowForm()
        {
            return View();
        }
        /// <summary>
        /// 流程加签
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SignFlowForm()
        {
            return View();
        }
        #endregion

        #region 知会已阅

        [HttpGet]
        [AjaxOnly]
        public ActionResult ReadNotify(string processId)
        {
            if (string.IsNullOrEmpty(processId))
            {
                return JsonResult(new { ret = false,msg ="参数错误"});
            }

            UserInfo userInfo = LoginUserInfo.Get();
            return JsonResult(new { ret = wfNotifyIBLL.ReadNotify(processId, userInfo.userId), msg = "" });
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult ChangeReadState(string notifyId)
        {
            var userId = LoginUserInfo.UserInfo.userId;
            var isRead = true;
            wfNotifyIBLL.ChangeReadState(userId, notifyId, isRead);
            return Success("成功！");
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult ChangeFinishedReadState(string processId)
        {
            var userId = LoginUserInfo.UserInfo.userId;
            var isFinishedRead = true;
            wfProcessInstanceIBLL.ChangeFinishedReadState(userId, processId, isFinishedRead);
            return Success("成功！");
        }

        #endregion


        #region  获取数据
        /// <summary>
        /// 获取我的流程信息列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询条件</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTaskList(string pagination, string queryJson, string categoryId,string state)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            IEnumerable<WfProcessInstanceEntity> list = new List<WfProcessInstanceEntity>();

            UserInfo userInfo = LoginUserInfo.Get();
            if (categoryId == "2")//待办流程
            {
                list = wfTaskIBLL.GetActiveList(userInfo, paginationobj, queryJson);
            }
            if (categoryId == "4")//知会流程
            {
                list = wfNotifyIBLL.GetNotifyList(userInfo, paginationobj, queryJson, 0);
            }
            if (categoryId == "3")//已办流程
            {
                list = wfTaskIBLL.GetHasList(userInfo.userId, paginationobj, queryJson);
            }
            if (categoryId == "1" && state == "1")//审批中的流程
            {
                list = wfProcessInstanceIBLL.GetAuditingList(userInfo.userId, paginationobj, queryJson);
            }
            if (categoryId == "1" && state == "2")//已审批的流程
            {
                list = wfTaskIBLL.GetFinishedList(userInfo, paginationobj, queryJson);
            }
            if (categoryId == "1" && state == "3")//被打回的流程
            {
                list = wfTaskIBLL.GetBackList(userInfo, paginationobj, queryJson);
            }

            //switch (categoryId)
            //{
            //    case "1":
            //        list = wfProcessInstanceIBLL.GetMyPageList(userInfo.userId, paginationobj, queryJson);
            //        break;
            //    case "2":
            //        list = wfTaskIBLL.GetActiveList(userInfo, paginationobj, queryJson); 
            //        break;
            //    case "3":
            //        list = wfTaskIBLL.GetHasList(userInfo.userId, paginationobj, queryJson);
            //        break;
            //    case "4":
            //        list = wfNotifyIBLL.GetNotifyList(userInfo, paginationobj, queryJson, 0);
            //        break;
            //    case "5":
            //        list = wfNotifyIBLL.GetNotifyList(userInfo, paginationobj, queryJson, 1);
            //        break;
            //}

            var jsonData = new
            {
                rows = list,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
        #endregion
    }
}