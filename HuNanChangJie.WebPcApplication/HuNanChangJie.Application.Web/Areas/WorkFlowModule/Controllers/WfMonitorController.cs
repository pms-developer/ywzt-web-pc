﻿using HuNanChangJie.Application.WorkFlow;
using HuNanChangJie.Util;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.WorkFlowModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.04.17
    /// 描 述：流程实例监控
    /// </summary>
    public class WfMonitorController : MvcControllerBase
    {
        private WfProcessInstanceIBLL wfProcessInstanceIBLL = new WfProcessInstanceBLL();

        #region  视图功能
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region  获取数据
        /// <summary>
        /// 获取我的流程信息列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询条件</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPorcessList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            IEnumerable<WfProcessInstanceEntity> list = list = wfProcessInstanceIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = list,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records,
            };
            return JsonResult(jsonData);
        }
        #endregion
    }
}