﻿/*
 * 日 期：2017.04.18
 * 描 述：角色添加	
 */
var flag = request('flag');

var acceptClick;
var auditorName = '';
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $('#projectRole').mkDataItemSelect({ code: 'ProjectRole' });
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var item = $('#projectRole').mkselectGetEx();
        var formData = {};
        formData.id = Changjie.newGuid();
        formData.approverType = "ProjectRole";
        formData.approverId = item.k;
        formData.approverName = item.text;
        callBack(formData);
        return true;
    };
    page.init();
}