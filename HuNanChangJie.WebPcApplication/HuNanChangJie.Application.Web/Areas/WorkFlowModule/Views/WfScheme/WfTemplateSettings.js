﻿/*
 * 日 期：2017.04.05
 * 描 述：工作流模板设计	
 */
var keyValue = request('keyValue');
var categoryId = request('categoryId');
var shcemeCode = request('shcemeCode');

var currentNode; // 当前设置节点
var currentLine; // 当前设置线条

var custmerformlist = null;
var listforms = [];
var mainfields;//主表字段
var roleData=[];
var postData=[];
var departmentData=[];
var projectRoleData=[];

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('#projectRole').mkDataItemSelect({ code: 'ProjectRole' });
            page.bind();
            page.initData();
            if (!!keyValue == false) {
                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0001' }, function (data) {
                    $('#F_Code').val(data);
                });
            }
            page.loadBaseData();
        },
        loadBaseData: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/OrganizationModule/Department/GetAllList', {}, function (data) {
                if (data) {
                    for (var _i in data) {
                        var row = data[_i];
                        departmentData.push({ id: row.F_DepartmentId, text: row.F_FullName });
                    }
                }
            });
            Changjie.httpAsync('GET', top.$.rootUrl + '/OrganizationModule/Post/GetAllList', {}, function (data) {
                if (data) {
                    for (var _i in data) {
                        var row = data[_i];
                        postData.push({ id: row.F_PostId, text: row.F_Name });
                    }
                }
            });
            Changjie.httpAsync('GET', top.$.rootUrl + '/OrganizationModule/Role/GetRoleList', {}, function (data) {
                if (data) {
                    for (var _i in data) {
                        var row = data[_i];
                        roleData.push({ id: row.F_RoleId, text: row.F_FullName });
                    }
                }
            });

            var data = $('#projectRole').mkselectGetAll();

            for (var _i in data) {
                var row = data[_i];
                projectRoleData.push({ id: row.k, text: row.text });
            }
                
            
        },
        /*绑定事件和初始化控件*/
        bind: function () {
            // 加载导向
            $('#wizard').wizard().on('change', function (e, data) {
                var $finish = $("#btn_finish");
                var $next = $("#btn_next");
                console.log(data.direction);
                console.log(data.step);
                if (data.direction == "next") {
                    if (data.step == 1) {
                        if (!$('#step-1').mkValidform()) {
                            return false;
                        }
                        $next.attr('disabled', 'disabled');
                        $finish.removeAttr('disabled');
                    }
                     
                } else {
                    $finish.attr('disabled', 'disabled');
                    $next.removeAttr('disabled');
                }
            });
            $('#F_Category').mkDataItemSelect({ code: 'FlowSort' });
            $('#F_Category').mkselectSet(categoryId);

            Changjie.httpAsync('GET', top.$.rootUrl + '/FormModule/Custmerform/GetFormList', {}, function (data) {
                custmerformlist = data;
                $('#F_Form').mkselect({
                    data: data,
                    value: "F_Id",
                    text: "F_Name", allowSearch: true
                }).on("change", function () {
                    var item = $(this).mkselectGetEx();
                    var param = {};
                    param.maintable = item.MainTable;
                    param.subs = item.SubTables;
                    param.type = item.Type;
                    param.schemeId = item.F_Id;
                    page.loadFormField(param);
                });
            });

            // 设计页面初始化
            $('#step-2').mkworkflow({
                openNode: function (node) {

                    currentNode = node;

                    if (node.type == 'stepnode') {
                        var currentnodeformid = "";
                        if (currentNode.wfForms && currentNode.wfForms.length > 0)
                            currentnodeformid = currentNode.wfForms[0].formId;

                        var selformid = $('#F_Form').mkselectGet();
                        if (custmerformlist != null && currentnodeformid != selformid) {
                            $.each(custmerformlist, function (seli, sele) {
                                debugger;
                                if (sele.F_Id == selformid) {
                                    listforms = [{
                                        id: Changjie.newGuid(), field: "", formId: selformid, name: sele.F_Name, type: 1, url: sele.Url + "?id=" + sele.F_Id
                                    }];
                                    currentNode.wfForms = listforms;
                                    console.log(listforms);
                                    return false;
                                }
                            })
                        }

                        Changjie.layerForm({
                            id: 'NodeForm',
                            title: '节点信息设置【' + node.name + '】',
                            url: top.$.rootUrl + '/WorkFlowModule/WfScheme/NodeFormSettings?layerId=layer_Form',
                            width: 800,
                            height: 500,
                            callBack: function (id) {
                                return top[id].acceptClick(function () {
                                    $('#step-2').mkworkflowSet('updateNodeName', { nodeId: currentNode.nodeId });
                                });
                            }
                        });
                    }
                },
                openLine: function (line) {
                    currentLine = line;
                    Changjie.layerForm({
                        id: 'LineForm',
                        title: '线条信息设置',
                        url: top.$.rootUrl + '/WorkFlowModule/WfScheme/LineFormSettings?layerId=layer_Form',
                        width: 850,
                        height: 500,
                        callBack: function (id) {
                            return top[id].acceptClick(function () {
                                $('#step-2').mkworkflowSet('updateLineName', { lineId: currentLine.lineId });
                            });
                        }
                    });
                }
            });
            // 保存草稿
            $("#btn_draft").on('click', page.draftsave);
            // 保存数据按钮
            $("#btn_finish").on('click', page.save);
        },
        /*初始化数据*/
        initData: function () {
            if (!!shcemeCode) {
                $.mkSetForm(top.$.rootUrl + '/WorkFlowModule/WfScheme/GetFormInfo?keyValue=' + keyValue, function (data) {//
                    $('#step-1').mkSetFormData(data.schemeInfoEntity);
                    var shceme = JSON.parse(data.schemeEntity.F_Scheme);
                    $('#step-2').mkworkflowSet('set', { data: shceme });

                });
            }
        },
        loadFormField: function (param) {
            debugger;
            if (param) {
                var customerFields = [];
                if (param.type == "自定义") {
                    var data = Changjie.httpGet(top.$.rootUrl + '/FormModule/Custmerform/GetFormSettingData', { schemeId: param.schemeId }).data;
                    customerFields = JSON.parse(data.SettingData).data[0];
                }
                if (param.maintable) {
                    Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetTableFieldList', { tableName: param.maintable }, function (data) {
                        if (data) {
                            if (param.type == "自定义") {
                                for (var _i in data) {
                                    var item = data[_i];
                                    for (var _j in customerFields.componts) {
                                        var info = customerFields.componts[_j];
                                        if (item.id.toLowerCase() != info.field.toLowerCase()) continue;
                                        item.id = info.id;
                                    }
                                }
                            }
                            mainfields = data
  
                            mainfields.push({ id: "SendDepartment", text: '发起人部门', type: "SendDepartment", f_datatype:"varchar" });
                            mainfields.push({ id: "SendRole", text: '发起人角色', type: "SendRole", f_datatype: "varchar" });
                            mainfields.push({ id: "SendProjectRole", text: '发起人岗位', type: "SendProjectRole", f_datatype: "varchar" });
                            mainfields.push({ id: "SendPost", text: '发起人项目角色', type: "SendPost", f_datatype: "varchar" });
                        }
                    });
                }
                //if (param.subs) {
                //    for (var _i in param.subs) {
                //        var item = param.subs[_i];
                //        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DatabaseTable/GetTableFieldList', { tableName: item.name }, function (data) {
                //            if (data) {
                //                console.log(data);
                //            }
                //        });
                //    }
                //}
            }
        },
        /*保存草稿*/
        draftsave: function () {
            var formdata = $('#step-1').mkGetFormData(keyValue);
            var shcemeData = $('#step-2').mkworkflowGet();

            var postData = {
                schemeInfo: JSON.stringify(formdata),
                scheme: JSON.stringify(shcemeData),
                type: 2
            };

            $.mkSaveForm(top.$.rootUrl + '/WorkFlowModule/WfScheme/SaveForm?keyValue=' + keyValue, postData, function (res) {
                // 保存成功后才回调
                Changjie.frameTab.currentIframe().refreshGirdData(formdata);
            });
        },
        /*保存数据*/
        save: function () {
            console.log(1);
            if (!$('#step-1').mkValidform()) {
                return false;
            }
            var formdata = $('#step-1').mkGetFormData(keyValue);
            var shcemeData = $('#step-2').mkworkflowGet();

            var postData = {
                schemeInfo: JSON.stringify(formdata),
                scheme: JSON.stringify(shcemeData),
                type: 1
            };

            $.mkSaveForm(top.$.rootUrl + '/WorkFlowModule/WfScheme/SaveFormData?keyValue=' + keyValue, postData, function (res) {
                // 保存成功后才回调
                Changjie.frameTab.currentIframe().refreshGirdData(formdata);
            });
        }
    };
    page.init();
}