﻿/*
 * 日 期：2017.04.18
 * 描 述：表单权限字段导入
 */
var id = request('id');
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
           

            $('#field').mkselect({
                placeholder: '请选择字段',
                maxHeight: 80,
                allowSearch: true
            });
            $('#formId').mkselect({
                placeholder: '请选择表单',
                text: 'F_Name',
                value: 'F_Id',
                url: top.$.rootUrl + '/FormModule/Custmerform/GetSchemeInfoList',
                maxHeight: 140,
                allowSearch: true,
                select: function (item) {
                    if (!!item) {
                        var formName = item.F_Name;
                        var url = '/FormModule/Custmerform/WorkflowInstanceForm?id=' + item.F_Id;
                        $('#name').val(formName);
                        $('#url').val(url);
                        $.mkSetForm(top.$.rootUrl + '/FormModule/Custmerform/GetFormData?keyValue=' + item.F_Id, function (data) {
                            var scheme = JSON.parse(data.schemeEntity.F_Scheme);
                            var fields = [];
                            for (var i = 0, l = scheme.data.length; i < l; i++) {
                                var componts = scheme.data[i].componts;
                                for (var j = 0, jl = componts.length; j < jl; j++) {
                                    var compont = componts[j];
                                    if (!!compont.title && !!compont.field) {
                                        var point = { text: compont.title, id: compont.id };
                                        fields.push(point);
                                    }
                                }
                            }
                            $('#field').mkselectRefresh({ data: fields });
                        });
                    }
                    else {
                        $('#field').mkselectRefresh({ data: [] });
                    }
                }
            });
            $('#preview').on('click', function () {
                var formId = $('#formId').mkselectGet();
                if (!!formId) {
                    Changjie.layerForm({
                        id: 'custmerForm_PreviewForm',
                        title: '预览当前表单',
                        url: top.$.rootUrl + '/FormModule/Custmerform/PreviewForm?schemeInfoId=' + formId,
                        width: 800,
                        height: 600,
                        maxmin: true,
                        btn: null
                    });
                }
                else {
                    Changjie.alert.warning('请选择表单！');
                }
            });


            $('#type').mkselect({
                data: [{ id: '1', text: '自定义表单' }, { id: '0', text: '系统表单' }],
                placeholder: false,
                maxHeight: 80,
                select: function (item) {
                    if (item.id != '1') {
                        $('.custmer-form').hide();
                    }
                    else {
                        $('.custmer-form').show();
                    }
                }
            }).mkselectSet('1');
        },
        initData: function () {
            if (!!id) {
                var workforms = top.layer_NodeForm.workforms;
                for (var i = 0, l = workforms.length; i < l; i++) {
                    if (workforms[i].id == id) {
                        $('#form').mkSetFormData(workforms[i]);
                        break;
                    }
                }
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var formdata = $('#form').mkGetFormData();
        formdata.id = id;
        if (formdata.type != '1') {
            formdata.formId = '';
        }
        callBack(formdata);
        return true;
    };
    page.init();
}