﻿/*
 * 日 期：2017.04.05
 * 描 述：节点设置	
 */
var layerId = request('layerId');
var isPreview = request('isPreview');

var acceptClick;

var auditors = [];
var authorize = [];
var workforms = [];
var notifys = [];

var bootstrap = function ($, Changjie) {
    "use strict";
    var currentNode = top[layerId].currentNode;
    var formcomponts = [];

    function isRepeat(id) {
        var res = false;
        for (var i = 0, l = auditors.length; i < l; i++) {
            if (auditors[i].approverId == id) {
                Changjie.alert.warning('重复添加审核人员信息');
                res = true;
                break;
            }
        }
        return res;
    }

    function isRepeatType(type) {
        var res = false;
        for (var i = 0, l = auditors.length; i < l; i++) {
            if (auditors[i].approverType == type) {
                Changjie.alert.warning('重复添加审核人员信息');
                res = true;
                break;
            }
        }
        return res;
    }

    function isRepeatNType(type) {
        var res = false;
        for (var i = 0, l = notifys.length; i < l; i++) {
            if (notifys[i].approverType == type) {
                Changjie.alert.warning('重复添加审核人员信息');
                res = true;
                break;
            }
        }
        return res;
    }
    
    function isRepeatN(id) {
        var res = false;
        for (var i = 0, l = notifys.length; i < l; i++) {
            if (notifys[i].approverId == id) {
                Changjie.alert.warning('重复添加知会人员信息');
                res = true;
                break;
            }
        }
        return res;
    }

    var page = {
        init: function () {

            if (!!currentNode.fromPermission) {
                formcomponts = currentNode.fromPermission;
            }
            else {
                for (var i in top[layerId].mainfields) {
                    var item = top[layerId].mainfields[i];
                    if (item.type) continue;
                    item.field = item.f_column;
                    item.fieldName = item.text;
                    item.isRead = 1;
                    item.isWrite = 0;
                    formcomponts.push(item);
                }
            }
            page.nodeInit();
            page.bind();
            page.initData();
            if (!!isPreview) {
                $('input,textarea').attr('readonly', 'readonly');
                $('.mk-form-jfgrid-btns').remove();
            }
        },
        nodeInit: function () {
 
        },
        /*绑定事件和初始化控件*/
        bind: function () {
            $('#form_tabs').mkFormTab();

            $('#isNotify').mkselect({
                placeholder: false,
                data: [
                    { id: '0', text: '否' }, 
                    { id: '1', text: '是' }
                ]
            }).mkselectSet('0');

            $('#isFreedom').mkselect({
                placeholder: false,
                data: [
                    { id: '0', text: '否' },
                    { id: '1', text: '是' }
                ]
            }).mkselectSet('0');

            $('#isCountersign').mkselect({
                placeholder: false,
                data: [
                    { id: '0', text: '否' },
                    { id: '1', text: '是' }
                ]
            }).mkselectSet('0');
             
            // 审核者
            $('#auditor_girdtable').jfGrid({
                headData: [
                    {
                        label: "审核者类型", name: "approverType", width: 260, align: "center",
                        formatter: function (cellvalue) {//审核者类型1.岗位2.角色3.用户
                            switch (cellvalue) {
                                case 'Post':
                                    return '岗位';
                                    break;
                                case 'Role':
                                    return '角色';
                                    break;
                                case 'User':
                                    return '用户';
                                    break;
                                case 'Department':
                                    return '部门';
                                    break;
                                case 'ProjectRole':
                                    return '项目角色';
                                    break;
                                case 'Sponsor':
                                    return '发起人';
                                    break;
                                case 'SponsorSuperior':
                                    return '发起人上级';
                                    break;
                                case 'AuditorSuperior':
                                    return '审批人上级';
                                    break;
                                case 'Company':
                                    return '公司';
                                    break;
                            }
                        }
                    },
                    { label: "名称", name: "approverName", width: 260, align: "left" },
                    {
                        label: "附加条件", name: "condition", width: 150, align: "left",
                        formatter: function (cellvalue) {// 1.同一个部门2.同一个公司
                            switch (cellvalue) {
                                case 'Department':
                                    return '同一个部门';
                                case 'Company':
                                    return '同一个公司';
                                default:
                                    return '无'
                            }
                        }
                    }
                ]
            });
            // 知会者
            $('#notify_girdtable').jfGrid({
                headData: [
                    {
                        label: "审核者类型", name: "approverType", width: 260, align: "center",
                        formatter: function (cellvalue) {//审核者类型1.岗位2.角色3.用户
                            switch (cellvalue) {
                                case 'Post':
                                    return '岗位';
                                    break;
                                case 'Role':
                                    return '角色';
                                    break;
                                case 'User':
                                    return '用户';
                                    break;
                                case 'Department':
                                    return '部门';
                                    break;
                                case 'ProjectRole':
                                    return '项目角色';
                                    break;
                                case 'Sponsor':
                                    return '发起人';
                                    break;
                                case 'SponsorSuperior':
                                    return '发起人上级';
                                    break;
                                case 'AuditorSuperior':
                                    return '审批人上级';
                                    break;
                                case 'Company':
                                    return '公司';
                                    break;
                            }
                        }
                    },
                    { label: "名称", name: "approverName", width: 260, align: "left" },
                    {
                        label: "附加条件", name: "condition", width: 150, align: "left",
                        formatter: function (cellvalue) {// 1.同一个部门2.同一个公司
                            switch (cellvalue) {
                                case 'Department':
                                    return '同一个部门';
                                case 'Company':
                                    return '同一个公司';
                                default:
                                    return '无'
                            }
                        }
                    }
                ]
            });
            // 岗位添加
            $('#post_auditor').on('click', function () {
                Changjie.layerForm({
                    id: 'AuditorPostForm',
                    title: '添加审核岗位',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/PostForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {

                            if (!isRepeat(data.auditorId)) {
                                data.id = Changjie.newGuid();
                                var item = {};
                                item.id = data.id;
                                item.approverType = "Post";
                                item.approverId = data.auditorId;
                                item.condition = data.condition;
                                item.approverName = data.auditorName;
                                auditors.push(item);
                                $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
                            }

                        });
                    }
                });
            });
            $("#sponsor_auditor").on("click", function () {
                if (isRepeatType("Sponsor")) return;
                var item = {};
                item.id = Changjie.newGuid();
                item.approverType = "Sponsor";
                item.approverName = "发起人";
                auditors.push(item);
                $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
            });

            $("#sponsorSuperior_auditor").on("click", function () {
                if (isRepeatType("SponsorSuperior")) return;
                var item = {};
                item.id = Changjie.newGuid();
                item.approverType = "SponsorSuperior";
                item.approverId = '';
                item.approverName = "发起人上级";
                item.condition = '';
                auditors.push(item);
                $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
            });

            $("#auditorSuperior_auditor").on("click", function () {
                if (isRepeatType("AuditorSuperior")) return;
                var item = {};
                item.id = Changjie.newGuid();
                item.approverType = "AuditorSuperior";
                item.approverId = '';
                item.approverName = "审批人上级";
                item.condition = '';
                auditors.push(item);
                $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
            });

            // 添加项目角色
            $('#project_role_auditor').on('click', function () {
                Changjie.layerForm({
                    id: 'ProjectRoleForm',
                    title: '添加项目角色',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/ProjectRoleForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeat(data.approverId)) {
                                auditors.push(data);
                                $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
                            }
                        });
                    }
                });
            });

            // 角色添加
            $('#role_auditor').on('click', function () {
                Changjie.layerForm({
                    id: 'AuditorRoleForm',
                    title: '添加审核角色',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/RoleForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeat(data.auditorId)) {
                                data.id = Changjie.newGuid();
                                var item = {};
                                item.id = data.id;
                                item.approverType = "Role";
                                item.approverId = data.auditorId;
                                item.condition = data.condition;
                                item.approverName = data.auditorName;
                                auditors.push(item);
                                $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
                            }
                        });
                    }
                });
            });

            // 添加项目角色
            $('#project_role_auditor').on('click', function () {
                Changjie.layerForm({
                    id: 'ProjectRoleForm',
                    title: '添加项目角色',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/ProjectRoleForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeat(data.approverId)) {
                                auditors.push(data);
                                $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
                            }
                        });
                    }
                });
            });

            // 人员添加
            $('#user_auditor').on('click', function () {
                Changjie.layerForm({
                    id: 'AuditorUserForm',
                    title: '添加审核人员',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/UserForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeat(data.auditorId)) {
                                data.id = Changjie.newGuid();
                                var item = {};
                                item.id = data.id;
                                item.approverType = "User";
                                item.approverId = data.auditorId;
                                item.approverName = data.auditorName;
                                item.condition = data.condition;
                                auditors.push(item);
                                $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
                            }
                        });
                    }
                });
            });
            // 审核人员移除
            $('#delete_auditor').on('click', function () {
                var _id = $('#auditor_girdtable').jfGridValue('id');
                if (Changjie.checkrow(_id)) {
                    Changjie.layerConfirm('是否确认删除该审核人员！', function (res, index) {
                        if (res) {
                            for (var i = 0, l = auditors.length; i < l; i++) {
                                if (auditors[i].id == _id) {
                                    auditors.splice(i, 1);
                                    $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
                                    break;
                                }
                            }
                            top.layer.close(index); //再执行关闭  
                        }
                    });
                }
            });

            $("#countersignRate").on('input propertychange', function () {
                var $this = $(this);
                var value = parseFloat($this.val()) || 0;
                if (value <= 0)
                    $this.val("1");
                if (value > 100)
                    $this.val("100");

            });
            // 知会人员--岗位添加
            $('#post_notify').on('click', function () {
                Changjie.layerForm({
                    id: 'AuditorPostForm',
                    title: '添加知会岗位',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/PostForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {

                            if (!isRepeatN(data.auditorId)) {
                                data.id = Changjie.newGuid();
                                var item = {};
                                item.id = data.id;
                                item.approverType = "Post";
                                item.approverId = data.auditorId;
                                item.condition = data.condition;
                                item.approverName = data.auditorName;
                                notifys.push(item);
                                $('#notify_girdtable').jfGridSet('refreshdata', notifys);
                            }

                        });
                    }
                });
            });
            // 知会人员--角色添加
            $('#role_notify').on('click', function () {
                Changjie.layerForm({
                    id: 'AuditorRoleForm',
                    title: '添加知会角色',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/RoleForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeatN(data.auditorId)) {
                                data.id = Changjie.newGuid();
                                var item = {};
                                item.id = data.id;
                                item.approverType = "Role";
                                item.approverId = data.auditorId;
                                item.condition = data.condition;
                                item.approverName = data.auditorName;
                                notifys.push(item);
                                $('#notify_girdtable').jfGridSet('refreshdata', notifys);
                            }
                        });
                    }
                });
            });
            // 知会人员--人员添加
            $('#user_notify').on('click', function () {
                Changjie.layerForm({
                    id: 'AuditorUserForm',
                    title: '添加知会人员',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/UserForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeatN(data.auditorId)) {
                                data.id = Changjie.newGuid();
                                var item = {};
                                item.id = data.id;
                                item.approverType = "User";
                                item.approverId = data.auditorId;
                                item.approverName = data.auditorName;
                                item.condition = data.condition;
                                notifys.push(item);
                                $('#notify_girdtable').jfGridSet('refreshdata', notifys);
                            }
                        });
                    }
                });
            });
            // 知会人员--审核人员移除
            $('#delete_notify').on('click', function () {
                var _id = $('#notify_girdtable').jfGridValue('id');
                if (Changjie.checkrow(_id)) {
                    Changjie.layerConfirm('是否确认删除该知会人员！', function (res, index) {
                        if (res) {
                            for (var i = 0, l = notifys.length; i < l; i++) {
                                if (notifys[i].id == _id) {
                                    notifys.splice(i, 1);
                                    $('#notify_girdtable').jfGridSet('refreshdata', notifys);
                                    break;
                                }
                            }
                            top.layer.close(index); //再执行关闭  
                        }
                    });
                }
            });

            $("#sponsor_notify").on("click", function () {
                if (isRepeatNType("Sponsor")) return;
                var item = {};
                item.id = Changjie.newGuid();
                item.approverType = "Sponsor";
                item.approverName = "发起人";
                notifys.push(item);
                $('#notify_girdtable').jfGridSet('refreshdata', notifys);
            });

            $("#sponsorSuperior_notify").on("click", function () {
                if (isRepeatNType("SponsorSuperior")) return;
                var item = {};
                item.id = Changjie.newGuid();
                item.approverType = "SponsorSuperior";
                item.approverId = '';
                item.approverName = "发起人上级";
                item.condition = '';
                notifys.push(item);
                $('#notify_girdtable').jfGridSet('refreshdata', notifys);
            });

            $("#auditorSuperior_notify").on("click", function () {
                if (isRepeatNType("AuditorSuperior")) return;
                var item = {};
                item.id = Changjie.newGuid();
                item.approverType = "AuditorSuperior";
                item.approverId = '';
                item.approverName = "审批人上级";
                item.condition = '';
                notifys.push(item);
                $('#notify_girdtable').jfGridSet('refreshdata', notifys);
            });

            // 添加项目角色
            $('#project_role_notify').on('click', function () {
                Changjie.layerForm({
                    id: 'ProjectRoleForm',
                    title: '添加项目角色',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/ProjectRoleForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeatN(data.approverId)) {
                                notifys.push(data);
                                $('#notify_girdtable').jfGridSet('refreshdata', notifys);
                            }
                        });
                    }
                });
            });

            // 表单权限
            $('#authorize_girdtable').jfGrid({
                headData: [
                    { label: "字段名称", name: "fieldName", width: 200, align: "left" },
                    { label: "字段ID", name: "field", width: 200, align: "left" },
                    {
                        label: "查看", name: "isRead", width: 200, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                if (row.isRead == 1) {
                                    row.isRead = 0;
                                    $(this).html('<span class=\"label label-default \" style=\"cursor: pointer;\">否</span>');
                                }
                                else {
                                    row.isRead = 1;
                                    $(this).html('<span class=\"label label-success \" style=\"cursor: pointer;\">是</span>');
                                }
                            });
                            if (cellvalue == 1) {
                                return '<span class=\"label label-success \" style=\"cursor: pointer;\">是</span>';
                            } else if (cellvalue == 0) {
                                return '<span class=\"label label-default \" style=\"cursor: pointer;\">否</span>';
                            }
                        }
                    },
                    {
                        label: "编辑", name: "isWrite", width: 70, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on('click', function () {
                                if (row.isWrite == 1) {
                                    row.isWrite = 0;
                                    $(this).html('<span class=\"label label-default \" style=\"cursor: pointer;\">否</span>');
                                }
                                else {
                                    row.isWrite = 1;
                                    $(this).html('<span class=\"label label-success \" style=\"cursor: pointer;\">是</span>');
                                }
                            });
                            if (cellvalue == 1) {
                                return '<span class=\"label label-success \" style=\"cursor: pointer;\">是</span>';
                            } else if (cellvalue == 0) {
                                return '<span class=\"label label-default \" style=\"cursor: pointer;\">否</span>';
                            }
                        }
                    }
                ]
            });


        },
        /*初始化数据*/
        initData: function () {
            $('#baseInfo').mkSetFormData(currentNode);

            if (!!currentNode.auditorList) {
                auditors = currentNode.auditorList;
            }
            if (!!currentNode.fromPermission) {
                $('#authorize_girdtable').jfGridSet('refreshdata', currentNode.fromPermission);
            }
            else {
                $('#authorize_girdtable').jfGridSet('refreshdata', formcomponts);
            }
            if (!!currentNode.notifyList) {
                notifys = currentNode.notifyList;
            }
           
            $('#auditor_girdtable').jfGridSet('refreshdata', auditors);
            $('#notify_girdtable').jfGridSet('refreshdata', notifys);
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#baseInfo').mkValidform()) {
            return false;
        }
        var baseInfoData = $('#baseInfo').mkGetFormData();

        currentNode.name = baseInfoData.name;

        currentNode.auditorList = auditors
        currentNode.notifyList = notifys;
        currentNode.fromPermission = $("#authorize_girdtable").jfGridGet("rowdatas");

        currentNode.isNotify = baseInfoData.isNotify;
        currentNode.isCountersign = baseInfoData.isCountersign;
        currentNode.countersignRate = baseInfoData.countersignRate;
        currentNode.isFreedom = baseInfoData.isFreedom;

        currentNode.timeoutAction = baseInfoData.timeoutAction;// 超时流转时间
        currentNode.timeoutNotice = baseInfoData.timeoutNotice;// 超时通知时间
        

        callBack();
        return true;
    };
    page.init();
}