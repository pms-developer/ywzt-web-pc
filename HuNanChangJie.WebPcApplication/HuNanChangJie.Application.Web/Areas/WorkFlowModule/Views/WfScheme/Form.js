﻿/*
 * 日 期：2017.04.05
 * 描 述：工作流模板设计	
 */
var keyValue = request('keyValue');
var categoryId = request('categoryId');
var shcemeCode = request('shcemeCode');

var currentNode; // 当前设置节点
var currentLine; // 当前设置线条
var schemeAuthorizes = []; // 模板权限人员
var authorizeType = 1;// 模板权限类型

var custmerformlist = null;
var listforms = [];

var bootstrap = function ($, Changjie) {
    "use strict";

    function isRepeat(id) {
        var res = false;
        for (var i = 0, l = schemeAuthorizes.length; i < l; i++) {
            if (schemeAuthorizes[i].F_ObjectId == id) {
                Changjie.alert.warning('重复添加信息');
                res = true;
                break;
            }
        }
        return res;
    }

    var page = {
        init: function () {
            page.bind();
            page.initData();
            if (!$('#F_Code').val()) {
                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0001' }, function (data) {
                    $('#F_Code').val(data);
                });
            }
        },
        /*绑定事件和初始化控件*/
        bind: function () {
            // 加载导向
            $('#wizard').wizard().on('change', function (e, data) {
                var $finish = $("#btn_finish");
                var $next = $("#btn_next");
                if (data.direction == "next") {
                    if (data.step == 1) {
                        if (!$('#step-1').mkValidform()) {
                            return false;
                        }
                    }
                    else if (data.step == 2)
                    {
                        if (authorizeType != 1) {
                            if (schemeAuthorizes.length == 0) {
                                Changjie.alert.error('请添加权限人员信息');
                                return false;
                            }
                        }
                        $finish.removeAttr('disabled');
                        $next.attr('disabled', 'disabled');
                    }
                    else {
                        
                        $finish.attr('disabled', 'disabled');
                    }
                } else {
                    $finish.attr('disabled', 'disabled');
                    $next.removeAttr('disabled');
                }
            });
            $('#F_Category').mkDataItemSelect({ code: 'FlowSort' });
            $('#F_Category').mkselectSet(categoryId);

            Changjie.httpAsync('GET', top.$.rootUrl + '/FormModule/Custmerform/GetFormList', {}, function (data) {
                custmerformlist = data;
                $('#F_Form').mkselect({
                    data: data,
                    value: "F_Id",
                    text: "F_Name", allowSearch: true
                })
            });

            // 权限设置
            $('[name="authorizeType"]').on('click', function () {
                var $this = $(this);
                var value = $this.val();
                authorizeType = value;
                if (value == '1') {
                    $('#shcemeAuthorizeBg').show();
                }
                else {
                    $('#shcemeAuthorizeBg').hide();
                }
            });
            $('#authorize_girdtable').jfGrid({
                headData: [
                    {
                        label: "类型", name: "F_ObjectType", width: 100, align: "center",
                        formatter: function (cellvalue) {//审核者类型1.岗位2.角色3.用户
                            switch (parseInt(cellvalue)) {
                                case 1:
                                    return '岗位';
                                    break;
                                case 2:
                                    return '角色';
                                    break;
                                case 3:
                                    return '用户';
                                    break;
                            }
                        }
                    },
                    { label: "名称", name: "F_ObjectName", width: 700, align: "left" }
                ]
            });
            // 岗位添加
            $('#post_authorize').on('click', function () {
                Changjie.layerForm({
                    id: 'AuthorizePostForm',
                    title: '添加岗位',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/PostForm?flag=1',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeat(data.auditorId)) {
                                var _data = {};
                                _data.F_Id = Changjie.newGuid();
                                _data.F_ObjectName = data.auditorName;
                                _data.F_ObjectId = data.auditorId;
                                _data.F_ObjectType = data.type;
                                schemeAuthorizes.push(_data);
                                $('#authorize_girdtable').jfGridSet('refreshdata', { rowdatas: schemeAuthorizes });
                            }
                        });
                    }
                });
            });
            // 角色添加
            $('#role_authorize').on('click', function () {
                Changjie.layerForm({
                    id: 'AuthorizeRoleForm',
                    title: '添加角色',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/RoleForm?flag=1',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeat(data.auditorId)) {
                                var _data = {};
                                _data.F_Id = Changjie.newGuid();
                                _data.F_ObjectName = data.auditorName;
                                _data.F_ObjectId = data.auditorId;
                                _data.F_ObjectType = data.type;
                                schemeAuthorizes.push(_data);
                                $('#authorize_girdtable').jfGridSet('refreshdata', { rowdatas: schemeAuthorizes });
                            }
                        });
                    }
                });
            });
            // 人员添加
            $('#user_authorize').on('click', function () {
                Changjie.layerForm({
                    id: 'AuthorizeUserForm',
                    title: '添加人员',
                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/UserForm',
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!isRepeat(data.auditorId)) {
                                var _data = {};
                                _data.F_Id = Changjie.newGuid();
                                _data.F_ObjectName = data.auditorName;
                                _data.F_ObjectId = data.auditorId;
                                _data.F_ObjectType = data.type;
                                schemeAuthorizes.push(_data);
                                $('#authorize_girdtable').jfGridSet('refreshdata', { rowdatas: schemeAuthorizes });
                            }
                        });
                    }
                });
            });
            // 人员移除
            $('#delete_authorize').on('click', function () {
                var _id = $('#authorize_girdtable').jfGridValue('F_Id');
                if (Changjie.checkrow(_id)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res, index) {
                        if (res) {
                            for (var i = 0, l = schemeAuthorizes.length; i < l; i++) {
                                if (schemeAuthorizes[i].F_Id == _id) {
                                    schemeAuthorizes.splice(i, 1);
                                    $('#authorize_girdtable').jfGridSet('refreshdata', { rowdatas: schemeAuthorizes });
                                    break;
                                }
                            }
                            top.layer.close(index); //再执行关闭  
                        }
                    });
                }
            });


            // 设计页面初始化
            $('#step-3').mkworkflow({
                openNode: function (node) {

                    currentNode = node;
                    
                    if (node.type != 'endround') {
                        var currentnodeformid = "";
                        if (currentNode.wfForms && currentNode.wfForms.length > 0)
                            currentnodeformid = currentNode.wfForms[0].formId;

                        var selformid = $('#F_Form').mkselectGet();
                        if (custmerformlist != null && currentnodeformid != selformid) {
                            $.each(custmerformlist, function (seli, sele) {
                                if (sele.F_Id == selformid) {
                                    listforms = [{
                                        id: Changjie.newGuid(), field: "", formId: selformid, name: sele.F_Name, type: 1, url: sele.Url + "?id=" + sele.F_Id
                                    }];
                                    currentNode.wfForms = listforms;
                                    return false;
                                }
                            })
                        }

                        Changjie.layerForm({
                            id: 'NodeForm',
                            title: '节点信息设置【' + node.name + '】',
                            url: top.$.rootUrl + '/WorkFlowModule/WfScheme/NodeForm?layerId=layer_Form',
                            width: 700,
                            height: 500,
                            callBack: function (id) {
                                return top[id].acceptClick(function () {
                                    $('#step-3').mkworkflowSet('updateNodeName', { nodeId: currentNode.id });
                                });
                            }
                        });
                    }
                },
                openLine: function (line) {
                    currentLine = line;
                    Changjie.layerForm({
                        id: 'LineForm',
                        title: '线条信息设置',
                        url: top.$.rootUrl + '/WorkFlowModule/WfScheme/LineForm?layerId=layer_Form',
                        width: 400,
                        height: 300,
                        callBack: function (id) {
                            return top[id].acceptClick(function () {
                                $('#step-3').mkworkflowSet('updateLineName', { lineId: currentLine.id });
                            });
                        }
                    });
                }
            });
            // 保存草稿
            $("#btn_draft").on('click', page.draftsave);
            // 保存数据按钮
            $("#btn_finish").on('click', page.save);
        },
        /*初始化数据*/
        initData: function () {
            if (!!shcemeCode) {
                $.mkSetForm(top.$.rootUrl + '/WorkFlowModule/WfScheme/GetFormInfo?keyValue=' + keyValue, function (data) {//
                    $('#step-1').mkSetFormData(data.schemeInfoEntity);
                    var shceme = JSON.parse(data.schemeEntity.F_Scheme);
                    $('#step-3').mkworkflowSet('set', { data: shceme });

                    if (data.wfSchemeAuthorizeList.length > 0 && data.wfSchemeAuthorizeList[0].F_ObjectType != 4) {
                        $('#authorizeType2').trigger('click');
                        schemeAuthorizes = data.wfSchemeAuthorizeList;
                        $('#authorize_girdtable').jfGridSet('refreshdata', { rowdatas: schemeAuthorizes });
                        authorizeType = 2;
                    }
                });
            }
        },
        /*保存草稿*/
        draftsave: function () {
            var formdata = $('#step-1').mkGetFormData(keyValue);
            var shcemeData = $('#step-3').mkworkflowGet();

            if (authorizeType == 1) {
                schemeAuthorizes = [];
            }

            var postData = {
                schemeInfo: JSON.stringify(formdata),
                scheme: JSON.stringify(shcemeData),
                shcemeAuthorize: JSON.stringify(schemeAuthorizes),
                type: 2
            };

            $.mkSaveForm(top.$.rootUrl + '/WorkFlowModule/WfScheme/SaveForm?keyValue=' + keyValue, postData, function (res) {
                // 保存成功后才回调
                Changjie.frameTab.currentIframe().refreshGirdData(formdata);
            });
        },
        /*保存数据*/
        save: function () {
            if (!$('#step-1').mkValidform()) {
                return false;
            }
            var formdata = $('#step-1').mkGetFormData(keyValue);
            var shcemeData = $('#step-3').mkworkflowGet();

            if (authorizeType == 1) {
                schemeAuthorizes = [];
                schemeAuthorizes.push({
                    F_Id: Changjie.newGuid(),
                    F_ObjectType: 4
                });
            }

            var startNode;
            for (var item in shcemeData.nodes) {
                var currtNode = shcemeData.nodes[item];
                if (currtNode.type != "startround") continue;
                startNode = currtNode;
                break;
            }

            if (startNode.wfForms.length == 0) {
                var selformid = $('#F_Form').mkselectGet();
                if (!!custmerformlist) {
                    $.each(custmerformlist, function (seli, sele) {
                        if (sele.F_Id == selformid) {
                            listforms = [{
                                id: Changjie.newGuid(), field: "", formId: selformid, name: sele.F_Name, type: 1, url: sele.Url + "?id=" + sele.F_Id
                            }];
                            startNode.wfForms = listforms;
                            return false;
                        }
                    })
                }
            }
            
            if (startNode.authorizeFields.length == 0) {
                var formId = startNode.wfForms[0].formId;
                var formName = startNode.wfForms[0].name
                loadStartNodeDefaultAuthority(formId, formName);
                startNode.authorizeFields = authorize;
            }
             
            var postData = {
                schemeInfo: JSON.stringify(formdata),
                scheme: JSON.stringify(shcemeData),
                shcemeAuthorize: JSON.stringify(schemeAuthorizes),
                type: 1
            };

            $.mkSaveForm(top.$.rootUrl + '/WorkFlowModule/WfScheme/SaveForm?keyValue=' + keyValue, postData, function (res) {
                // 保存成功后才回调
                Changjie.frameTab.currentIframe().refreshGirdData(formdata);
            });
        }
    };
    var authorize = [];
    var loadStartNodeDefaultAuthority = function (formId, formName) {
        var url = top.$.rootUrl + '/FormModule/Custmerform/GetFormAllData?keyValue=' + formId + "&formName=" + formName;
        var param = {
            keyValue: formId,
            formName: formName
        }
        var data = top.Changjie.httpGet(url, param);
        data = data.data;
        if (data.formtype == "customer") {
            var scheme = JSON.parse(data.schemeEntity.F_Scheme);
            for (var i = 0, l = scheme.data.length; i < l; i++) {
                var componts = scheme.data[i].componts;
                for (var j = 0, jl = componts.length; j < jl; j++) {
                    var compont = componts[j];
                    if (compont.type == 'gridtable') {
                        $.each(compont.fieldsData, function (_i, _item) {
                            if (_item.type != 'guid') {
                                var point = { id: Changjie.newGuid(), formId: formId, formName: formName, fieldName: compont.title + '-' + _item.name, fieldId: compont.id + '|' + _item.id, isLook: '1', isEdit: '1' };
                                authorize.push(point);
                            }

                        });
                    }
                    else {
                        var point = { id: Changjie.newGuid(), formId: formId, formName: formName, fieldName: compont.title, fieldId: compont.id, isLook: '1', isEdit: '1' };
                        authorize.push(point);
                    }
                }
            }

        }
        else {
            var details = data.details;
            for (var i in details) {
                var item = details[i];
                var point = { id: Changjie.newGuid(), formId: formId, formName: formName, fieldName: item.ShowName, fieldId: item.ControlName, isLook: '1', isEdit: '1' };
                authorize.push(point);
            }
        }
    }
    page.init();
}