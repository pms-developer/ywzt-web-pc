﻿/*
 * 日 期：2020.04.18
 * 描 述：流程线条设置	
 */
var layerId = request('layerId');
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var currentLine = top[layerId].currentLine;
    var mainfields = top[layerId].mainfields;

    var roleData = top[layerId].roleData;
    var postData = top[layerId].postData;
    var departmentData = top[layerId].departmentData;
    var projectRoleData = top[layerId].projectRoleData;

    var page = {
        init: function () {
            page.bind();
            page.initTable();
            page.initData();
        },
        initTable() {
            $("#gridtable").jfGrid({
                headData: [
                    {
                        label: '编号', name: 'code', width: 200, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '条件描述', name: 'conditionStr', width: 200, cellStyle: { 'text-align': 'left' }
                    },
                ],
                isEdit: true,
                height: 200,
            });
        },
        bind: function () {
            $('#wftype').mkselect({
                placeholder: false,
                data: [{ id: '1', text: '正常流转' }]
            }).mkselectSet('1');

            $("#leftObj").mkselect({
                placeholder: false,
                data: mainfields
            }).on("change", function () {
                var $this = $(this);
                var item = $this.mkselectGetEx();
                if (item) {
                    if (item.type) {
                        switch (item.type) {
                            case "SendDepartment":
                                $("#rightObj").mkselectRefresh({
                                    placeholder: false,
                                    data: departmentData
                                });
                                break;
                            case "SendRole":
                                $("#rightObj").mkselectRefresh({
                                    placeholder: false,
                                    data: roleData
                                });
                                break;
                            case "SendPost":
                                $("#rightObj").mkselectRefresh({
                                    placeholder: false,
                                    data: postData
                                });
                                break;
                            case "SendProjectRole":
                                $("#rightObj").mkselectRefresh({
                                    placeholder: false,
                                    data: projectRoleData
                                });
                                break;
                        }
                       
                    }
                    else {

                        $("#rightObj").mkselectRefresh({
                            placeholder: false,
                            data: mainfields
                        });
                    }
                }
            });
            $("#rightObj").mkselect();

            $("#operators").mkselect({
                placeholder: "运行符",
                data: [
                    { id: "==", text: "等于" },
                    { id: "!=", text: "不等于" },
                    { id: ">", text: "大于" },
                    { id: ">=", text: "大于等于" },
                    { id: "<", text: "小于" },
                    { id: "<=", text: "小于等于" },
                    { id: "contain", text: "包含" },
                    { id: "notContain", text: "不包含" }
                ]
            }).mkselectSet('==');

            $("#valueType").mkselect({
                placeholder: false,
                data: [
                    { id: "system", text: "系统属性" },
                    { id: "custom", text: "自定义" },
                ]
            }).mkselectSet('custom').on("change", function () {
                var $this = $(this);
                var value = $this.mkselectGet();
                if (value == "system") {
                    $("#rightObj").show();
                    $("#custom").hide();
                }
                else {
                    $("#rightObj").hide();
                    $("#custom").show();
                }
            });

            $("#add").on("click", function () {
                var data = $("#gridtable").jfGridGet("rowdatas");
                var code = 1;
                if (data) {
                    code = data.length+1;
                }
                var leftItem = $("#leftObj").mkselectGetEx();
                var rightItem = $("#rightObj").mkselectGetEx();

                var row = { code: code };
                row.leftObj = leftItem.f_column || leftItem.id;
                row.operators = $("#operators").mkselectGet();

                var isCustom = $("#valueType").mkselectGet() == "system" ? false : true;
                row.isCustom = isCustom;
                row.leftDataType = leftItem.f_datatype 
                row.isFormObj = leftItem.type == null ? true : false;
                if (isCustom == true) {
                    row.rightObj = $("#custom").val();
                    row.conditionStr = leftItem.text + "  " + row.operators + "  " + row.rightObj;
                }
                else {
                    row.rightObj = rightItem.id;
                    row.rightDataType = rightItem.f_datatype;
                    row.conditionStr = leftItem.text + "  " + row.operators + "  " + rightItem.text;
                }
                 
                
                $("#gridtable").jfGridSet("addRow", row);
            })
        },
        initData: function () {
            $('#form').mkSetFormData(currentLine);
            var lineContition = currentLine.lineCondition;
            if (lineContition) {
                $("#combination").val(lineContition.combination);
                if (lineContition.conditionItems) {
                    $('#gridtable').jfGridSet('refreshdata', lineContition.conditionItems);
                }
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var formData = $('#form').mkGetFormData();
        currentLine.lineName = formData.lineName;
        currentLine.wftype = formData.wftype;
        currentLine.lineCondition = { combination: formData.combination, conditionItems: $("#gridtable").jfGridGet("rowdatas") };
        callBack();
        return true;
    };
    page.init();
}