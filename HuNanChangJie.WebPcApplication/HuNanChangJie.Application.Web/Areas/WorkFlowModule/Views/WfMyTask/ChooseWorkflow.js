﻿/*
 * 日 期：2017.04.18
 * 描 述：提交发起流程	
 */
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var type = Changjie.frameTab.currentIframe().type;
    var shcemeCode = Changjie.frameTab.currentIframe().shcemeCode;
    var processId = Changjie.frameTab.currentIframe().processId;
    var taskId = Changjie.frameTab.currentIframe().taskId;
    var formData = Changjie.frameTab.currentIframe().allFormDatas;



    var page = {
        init: function () {
            if (type == 2) {
                $('#processName').parent().remove();
            }
            var infoId = request("infoId");
            var formId = request("formId");
            page.bind();
          
            if (!!formId) {
                var url = top.$.rootUrl + '/WorkFlowModule/WfScheme/GetSchemeInfoListNoDelete';
                var param = { formId: formId};
                var data = top.Changjie.httpGet(url, param);
                    
                    $("#workflowInfo").mkselect({
                        value: "F_Id",
                        text: "F_Name",
                        data: data.data,
                        select: function (item) {
                            var $form = $('#selectnode');
                            $form.html("")
                            if (!!item) {
                                /// 获取下一个节点的审核人信息数据
                                Changjie.workflowapi.auditer({
                                    isNew: (!!item.F_Id) ? true : false,
                                    wfschemeId: item.F_Id,
                                    processId: "",//processId,
                                    taskId: "",//taskId,
                                    formData: JSON.stringify(formData),
                                    callback: function (res) {
                                        $.each(res, function (_i, _item) {
                                            if (_item.all || _item.list.length == 0) {
                                                $form.append('<div class="col-xs-12 mk-form-item"><div class="mk-form-item-title language" >' + _item.name + '</div><div id="' + _item.nodeId + '" class="nodeId"></div></div >');
                                                $('#' + _item.nodeId).mkUserSelect(0);
                                            }
                                            else if (_item.list.length > 1) {
                                                $form.append('<div class="col-xs-12 mk-form-item"><div class="mk-form-item-title language" >' + _item.name + '</div><div id="' + _item.nodeId + '" class="nodeId" ></div></div >');
                                                $('#' + _item.nodeId).mkselect({
                                                    data: _item.list,
                                                    id: 'id',
                                                    text: 'name'
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
   
            }


        },
        bind: function () {
            var title = "";
            var projectName = "";
            var moduleName = "";
            var projectId = request("projectId");
            var moduleId = request("moduleId");
            if (projectId) {
                var info = Changjie.httpGet(top.$.rootUrl + '/ProjectModule/Project/GetFormData?keyValue=' + projectId).data;
                projectName = info.ProjectName;
            }
            if (moduleId) {
                var info = Changjie.httpGet(top.$.rootUrl + '/SystemModule/Module/GetModuleInfo?keyValue=' + moduleId).data;
                moduleName = info.F_FullName;
            }

            if (projectName) {
                $("#projectName").val(projectName);
                $("#projectInfo").show();
                title = "【" + projectName + "】项目"
            }
            else {
                $("#projectInfo").hide();
            }
            if (moduleName) {
                title += "【" + moduleName + "】单据审批"
            }
            $("#processName").val(title);
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var formData = $('#form').mkGetFormData();
        // 获取审核人员
        var auditers = {};
        $('#form').find('.nodeId').each(function () {
            var $this = $(this);
            var id = $this.attr('id');
            var type = $this.attr('type');
            if (!!formData[id]) {
                var point = {
                    userId: formData[id],
                };
                if (type == 'mkselect') {
                    point.userName = $this.find('.mk-select-placeholder').text();
                }
                else {
                    point.userName = $this.find('span').text();
                }
                auditers[id] = point;
            }
        });

        callBack(formData, auditers);
        return true;
    };
    page.init();
}