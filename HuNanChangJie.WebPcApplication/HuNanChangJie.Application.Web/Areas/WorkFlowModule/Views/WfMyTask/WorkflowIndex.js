﻿/*
 * 日 期：2017.08.04
 * 描 述：流程（我的任务）	
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var categoryId = request("categoryId");
    var state = request("state");
 
    var logbegin = '';
    var logend = '';

    refreshGirdData = function () {
        page.search();
    }

    var page = {
        init: function () {
            if (categoryId == "2") {
                $('#verify').show();
            } else {
                $('#verify').hide();
            }
            
            page.initGrid();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    logbegin = begin;
                    logend = end;

                    page.search();
                }
            });

            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查看流程进度
            $('#eye').on('click', function () {
                var processId = $('#gridtable').jfGridValue('F_Id');
                var taskId = $('#gridtable').jfGridValue('F_TaskId');
                var processName = $('#gridtable').jfGridValue('F_ProcessName');
                var taskName = $('#gridtable').jfGridValue('F_TaskName');
                var formType = $('#gridtable').jfGridValue('F_FormType');
                var formId = $('#gridtable').jfGridValue('FormId');
                var infoId = $('#gridtable').jfGridValue('InfoId');
                var projectId = $('#gridtable').jfGridValue("ProjectID");
                
                if (Changjie.checkrow(processId)) {
                    if (categoryId == "4") {
                        var isRead = $('#gridtable').jfGridValue('IsRead');
                        if (isRead == 0) {
                            Changjie.httpAsync('GET', top.$.rootUrl + '/WorkFlowModule/WfMyTask/ChangeReadState?notifyId=' + taskId + "&projectId=" + projectId, {}, function (data) {
                                top.badge.setNotiy();
                                refreshGirdData();
                            });
                        }
                    }
                    if (categoryId == "1" && state == "2") {
                        var finishedRead = $('#gridtable').jfGridValue('FinishedRead');
                        if (!finishedRead) {
                            Changjie.httpAsync('GET', top.$.rootUrl + '/WorkFlowModule/WfMyTask/ChangeFinishedReadState?processId=' + processId + "&projectId=" + projectId, {}, function (data) {
                                top.badge.setFinished();
                                refreshGirdData();
                            });
                        }
                    }

                    Changjie.frameTab.open({ F_ModuleId: processId + taskId, F_Icon: 'fa magic', F_FullName: '查看流程进度【' + processName + '】', F_UrlAddress: '/WorkFlowModule/WfMyTask/CustmerWorkFlowForm?tabIframeId=' + processId + taskId + '&type=100' + "&processId=" + processId + "&taskId=" + taskId + "&formType=" + formType + "&formId=" + formId + "&infoId=" + infoId + "&projectId=" + projectId });
                }
            });

            // 审核流程
            $('#verify').on('click', function () {
                var processId = $('#gridtable').jfGridValue('F_Id');
                var taskId = $('#gridtable').jfGridValue('F_TaskId');
                var processName = $('#gridtable').jfGridValue('F_ProcessName');
                var taskName = $('#gridtable').jfGridValue('F_TaskName');
                var taskType = $('#gridtable').jfGridValue('F_TaskType');
                var formType = $('#gridtable').jfGridValue('F_FormType');
                var formId = $('#gridtable').jfGridValue('FormId');
                var infoId = $('#gridtable').jfGridValue('InfoId');
                var projectId = $('#gridtable').jfGridValue("ProjectID");
                if (taskType == 4) {
                    if (Changjie.checkrow(taskId)) {
                        Changjie.frameTab.open(
                            {
                                F_ModuleId: taskId,
                                F_Icon: 'fa magic',
                                F_FullName: '审核流程【' + processName + '/' + taskName + '】',
                                F_UrlAddress: '/WorkFlowModule/WfMyTask/CustmerWorkFlowForm?tabIframeId=' + taskId + '&type=4' + "&processId=" + processId + "&taskId=" + taskId + "&formType=" + formType + "&formId=" + formId + "&infoId=" + infoId + "&projectId=" + projectId,
                            });
                    }
                }
                else if (taskType == 1) {
                    if (Changjie.checkrow(taskId)) {
                        Changjie.frameTab.open(
                            {
                                F_ModuleId: taskId,
                                F_Icon: 'fa magic',
                                F_FullName: '审核流程【' + processName + '/' + taskName + '】',
                                F_UrlAddress: '/WorkFlowModule/WfMyTask/CustmerWorkFlowForm?tabIframeId=' + taskId + '&type=1' + "&processId=" + processId + "&taskId=" + taskId + "&formType=" + formType + "&formId=" + formId + "&infoId=" + infoId + "&projectId=" + projectId,
                            });
                    }
                }
                else if (taskType == 2) {
                    Changjie.alert.warning('请点击重新发起');
                }
                else {
                    if (Changjie.checkrow(taskId)) {
                        Changjie.frameTab.open({ F_ModuleId: taskId, F_Icon: 'fa magic', F_FullName: '审核流程【' + processName + '/' + taskName + '】', F_UrlAddress: '/WorkFlowModule/WfMyTask/CustmerWorkFlowForm?tabIframeId=' + taskId + '&type=3' + "&processId=" + processId + "&taskId=" + taskId + "&formType=" + formType + "&formId=" + formId + "&infoId=" + infoId + "&projectId=" + projectId});
                    }
                }
            });
        },
        initGrid: function () {
            var headData = [];
             
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/WorkFlowModule/WfMyTask/GetTaskList',
                headData: [
                    {
                        label: "任务", name: "F_TaskName", width: 160, align: "left",
                        formatter: function (cellvalue, row, dfop, $cell) {
                            var isaAain = false;
                            if (categoryId == "1" && state == "3") {
                                $cell.on('click', function () {

                                    Changjie.frameTab.open({ F_ModuleId: row.F_Id, F_Icon: 'fa magic', F_FullName: '重新发起流程【' + row.F_ProcessName + '】', F_UrlAddress: '/WorkFlowModule/WfMyTask/CustmerWorkFlowForm?processId=' + row.F_Id + '&tabIframeId=' + row.F_Id + '&type=2&wfshcemeId=' + row.F_SchemeId + '&formId=' + row.FormId + '&infoId=' + row.InfoId + '&formType=' + row.F_FormType });
                                });
                                return "<span class=\"label label-danger\">重新发起</span>";
                            }
                            else {
                                if (categoryId == '1') {
                                    if (row.F_IsAgain == 1) {
                                        isaAain = true;
                                    }
                                    else {
                                        if (state == "2") {
                                            if (!row.FinishedRead) {
                                                return '<span style="color:red">【未读】</span>  本人发起';
                                            }
                                            else {
                                                return '<span style="color:green">【已读】</span>  本人发起';
                                            }
                                        }
                                        return '本人发起';
                                    }
                                }

                                if (row.F_TaskType == 4) {
                                    return "【加签】" + cellvalue;
                                }
                                else if (row.F_TaskType == 2) {
                                    isaAain = true;
                                }
                                if (isaAain) {
                                    $cell.on('click', function () {
                                        Changjie.frameTab.open({ F_ModuleId: row.F_Id, F_Icon: 'fa magic', F_FullName: '重新发起流程【' + row.F_ProcessName + '】', F_UrlAddress: '/WorkFlowModule/WfMyTask/CustmerWorkFlowForm?processId=' + row.F_Id + '&tabIframeId=' + row.F_Id + '&type=2&wfshcemeId=' + row.F_SchemeId + '&formId=' + row.FormId + '&infoId=' + row.InfoId + '&formType=' + row.F_FormType });
                                    });
                                    return "<span class=\"label label-danger\">重新发起</span>";
                                }
                                if (categoryId == "4") {
                                    
                                    if (row.IsRead == 0) {
                                        return '<span style="color:red">【未读】</span>  ' + cellvalue;
                                    }
                                    else {
                                        return '<span style="color:green">【已读】</span>  ' + cellvalue;
                                    }
                                }
                               
                                
                                return cellvalue;
                            }
                        }
                    },
                    { label: "标题", name: "F_ProcessName", width: 300, align: "left" },
                    { label: "模板名称", name: "F_SchemeName", width: 150, align: "left" },
                    {
                        label: "等级", name: "F_ProcessLevel", width: 80, align: "center",
                        formatter: function (cellvalue) {
                            switch (cellvalue) {
                                case 0:
                                    return '普通';
                                    break;
                                case 1:
                                    return '重要';
                                    break;
                                case 2:
                                    return '紧急';
                                    break;
                            }
                        }
                    },
                    {
                        label: "状态", name: "F_EnabledMark", width: 70, align: "center",
                        formatter: function (cellvalue, row) {
                            if (row.F_IsFinished == 0) {
                                if (cellvalue == 1) {
                                    return "<span class=\"label label-success\">运行中</span>";
                                } else {
                                    return "<span class=\"label label-danger\">暂停</span>";
                                }
                            }
                            else {
                                return "<span class=\"label label-warning\">结束</span>";
                            }

                        }
                    },
                    { label: "发起者", name: "CreationName", width: 70, align: "center" },
                    {
                        label: "发起时间", name: "CreationDate", width: 150, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd hh:mm:ss');
                        }
                    },
                    { label: "备注", name: "F_Description", width: 300, align: "left" }

                ],
                mainId: 'F_Id',
                isPage: true,
                sidx: 'CreationDate'
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = logbegin;
            param.EndTime = logend;

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param), categoryId: categoryId, state: state });
        }
    };

    page.init();
}


