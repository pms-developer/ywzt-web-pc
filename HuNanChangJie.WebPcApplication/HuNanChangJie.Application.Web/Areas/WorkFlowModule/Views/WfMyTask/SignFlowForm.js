﻿/*
 * 日 期：2017.04.18
 * 描 述：流程加签	
 */
var acceptClick;
var auditorName = '';
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('#auditorId').mkselect({
                value: 'F_UserId',
                text: 'F_RealName',
                title: 'F_RealName',
                // 展开最大高度
                maxHeight: 190,
                // 是否允许搜索
                allowSearch: true,
                select: function (item) {
                    if (item) {
                        auditorName = item.F_RealName;
                    }
                }
            });
            $('#department').mkDepartmentSelect({
                maxHeight: 220
            }).on('change', function () {
                var value = $(this).mkselectGet();
                $('#auditorId').mkselectRefresh({
                    url: top.$.rootUrl + '/OrganizationModule/User/GetList',
                    param: { departmentId: value }
                });
            });
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var formData = $('#form').mkGetFormData();
        callBack(formData);
        return true;
    };
    page.init();
}