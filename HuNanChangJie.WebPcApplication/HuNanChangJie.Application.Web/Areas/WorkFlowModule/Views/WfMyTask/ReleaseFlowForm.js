﻿/*
 * 日 期：2017.04.18
 * 描 述：提交发起流程	
 */
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var type = Changjie.frameTab.currentIframe().type;
    var shcemeCode = Changjie.frameTab.currentIframe().shcemeCode;
    var processId = Changjie.frameTab.currentIframe().processId;
    var taskId = Changjie.frameTab.currentIframe().taskId;
    var formData = Changjie.frameTab.currentIframe().allFormDatas;

    var page = {
        init: function () {
            if (type == 2) {
                $('#processName').parent().remove();
            }
            /// 获取下一个节点的审核人信息数据
            Changjie.workflowapi.auditer({
                isNew: (!!shcemeCode) ? true : false,
                schemeCode: shcemeCode,
                processId: processId,
                taskId: taskId,
                formData: JSON.stringify(formData),
                callback: function (res) {
  
                    var $form = $('#description').parent();
                    $.each(res, function (_i, _item) {
                        if (_item.all || _item.list.length == 0) {
                            $form.before('<div class="col-xs-12 mk-form-item"><div class="mk-form-item-title language" >' + _item.name + '</div><div id="' + _item.nodeId + '" class="nodeId"></div></div >');
                            $('#' + _item.nodeId).mkUserSelect(0);
                        }
                        else if (_item.list.length > 1) {
                            $form.before('<div class="col-xs-12 mk-form-item"><div class="mk-form-item-title language" >' + _item.name + '</div><div id="' + _item.nodeId + '" class="nodeId" ></div></div >');
                            $('#' + _item.nodeId).mkselect({
                                data: _item.list,
                                id: 'id',
                                text: 'name'
                            });
                        }
                    });
                }
            });

        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var formData = $('#form').mkGetFormData();
        // 获取审核人员
        var auditers = {};
        $('#form').find('.nodeId').each(function () {
            var $this = $(this);
            var id = $this.attr('id');
            var type = $this.attr('type');
            if (!!formData[id]) {
                var point = {
                    userId: formData[id],
                };
                if (type == 'mkselect') {
                    point.userName = $this.find('.mk-select-placeholder').text();
                }
                else {
                    point.userName = $this.find('span').text();
                }
                auditers[id] = point;
            }
        });

        callBack(formData, auditers);
        return true;
    };
    page.init();
}