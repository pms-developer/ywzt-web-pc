﻿/*
 * 日 期：2017.08.04
 * 描 述：流程（我的任务）	
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var auditStatus = request("auditStatus");

    var logbegin = '';
    var logend = '';

    refreshGirdData = function () {
        page.search();
    }

    var page = {
        init: function () {

            page.initGrid();
            page.bind();
        },
        bind: function () {
            $("#LevelType").mkselect({
                placeholder: '流程等级',
                text: 'name',
                value: 'id',
                data: [{ id: '', name: '全部' }, { id: '0', name: '普通' }, { id: '1', name: '重要' }, { id: '2', name: '紧急' }]
            })
            //$("#AuditStatus").mkselect({
            //    placeholder: '流程状态',
            //    text: 'name',
            //    value: 'id',
            //    data: [{ id: '', name: '全部' }, { id: '0', name: '审核中' }, { id: '1', name: '已通过' }, { id: '2', name: '未通过' }, { id: '3', name: '未审核' }]
            //})
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    logbegin = begin;
                    logend = end; 
                    page.search( );
                }
            });

            // 查询
            $('#btn_Search').on('click', function () { 
                page.search( );
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查看流程进度
            $('#eye').on('click', function () {
                var workflowId = $('#gridtable').jfGridValue('Workflow_Id');
                var taskId = $('#gridtable').jfGridValue('TaskId');
                var workflowName = $('#gridtable').jfGridValue('WorkflowName');
                var nodeName = $('#gridtable').jfGridValue('NodeName');
                var formId = $('#gridtable').jfGridValue('FormId');
                var infoId = $('#gridtable').jfGridValue('InfoId');
                var projectId = $('#gridtable').jfGridValue("ProjectID");

                if (Changjie.checkrow(workflowId)) {

                    var isRead = $('#gridtable').jfGridValue('IsRead');
                    if (!isRead) {
                        if (auditStatus == "2" || auditStatus == "3") {
                            Changjie.httpAsyncPost(top.$.rootUrl + '/WorkFlowModule/WorkFlow/SetWorkflowRecordRead', { workflowId: workflowId}, function (data) {
                                top.badge.setFinished();
                                refreshGirdData();
                            });
                        }

                    }

                    Changjie.frameTab.open({
                        F_ModuleId: workflowId + taskId,
                        F_Icon: 'fa magic',
                        F_FullName: '查看流程进度【' + workflowName + '】',
                        F_UrlAddress: '/WorkFlowModule/WorkFlow/WfProgress?tabIframeId=' + workflowId + taskId + '&type=100' + "&workflowId=" + workflowId + "&taskId=" + taskId + "&formId=" + formId + "&infoId=" + infoId + "&projectId=" + projectId
                    });
                }
            });

        },
        initGrid: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/WorkFlowModule/WorkFlow/GetWorkflowRecordList',
                headData: [
                    {
                        label: "标题", name: "WorkflowName", width: 300, align: "left",
                    },
                    {
                        label: "等级", name: "Level", width: 60, align: "center",
                        formatter: function (cellvalue) {
                            switch (cellvalue) {
                                case 0:
                                    return '普通';
                                    break;
                                case 1:
                                    return '<div style="background-color:orange;color:#fff;font-weight:bold;font-size:14px">重要</div>';
                                    break;
                                case 2:
                                    return '<div style="background-color:red;color:#fff;font-weight:bold;font-size:14px">紧急</div>';
                                    break;
                            }
                        }
                    },
                    {
                        label: "读取状态", name: "IsRead", width: 65, align: "center",
                        formatter: function (cellvalue, row) {
                            switch (row.IsRead) {
                                case false:
                                    return '<span class="label label-default" style="cursor: pointer;">未读</span>';
                                case true:
                                    return '<span class="label label-success" style="cursor: pointer;">已读</span>';
                                default:
                                    return '<span class="label label-default" style="cursor: pointer;">未读</span>';
                            }
                        }
                    },
                    {
                        label: "申请人", name: "CreateUserName", width: 80, align: "center",
                    },
                    {
                        label: "流程分类", name: "WfSchemeType", width: 70, align: "center",
                    },
                    {
                        label: "流程名称", name: "WfSchemeName", width: 160, align: "left",
                    },
                    {
                        label: "流程状态", name: "AuditStatus", width: 70, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatusUseWorkFlow(cellvalue);
                        }
                    },
                    {
                        label: "关联项目", name: "ProjectName", width: 100, align: "left",
                    },
                    {
                        label: "流程节点", name: "NodeName", width: 100, align: "left",
                    },
                    {
                        label: "审批人类型", name: "ApproverType", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case "User":
                                    return '人员';
                                case "Post":
                                    return '岗位';
                                case "Role":
                                    return '角色';
                                case "Company":
                                    return '公司';
                                case "Department":
                                    return '部门';
                                case "ProjectRole":
                                    return '项目角色';
                                case "Sponsor":
                                    return '发起人';
                                case "SponsorSuperior":
                                    return '发起人上级';
                                case "AuditorSuperior":
                                    return '审批人上级';
                                default:
                                    return cellvalue;
                            }
                        }
                    },
                    {
                        label: "当前审批人", name: "ApproverName", width: 70, align: "center",
                    },
                    {
                        label: "发起时间", name: "CreateTime", width: 130, align: "center",
                    },
                    {
                        label: "完成时间", name: "FinishTime", width: 130, align: "center",
                    },
                    {
                        label: "读取时间", name: "ReadTime", width: 130, align: "center",
                    },
                    {
                        label: "流程耗时", name: "ElapsedTimeDescription", width: 150, align: "center",
                    },
                    {
                        label: "备注", name: "Description", width: 160, align: "left",
                    }

                ],
                mainId: 'Id',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = logbegin;
            param.EndTime = logend;
            var keyword = $('#txt_Keyword').val();
            if (keyword)
                param.keyword = keyword;

            var workflowName = $('#WorkflowName').val();
            if (workflowName)
                param.WorkflowName = workflowName;

            var wfSchemeName = $('#WfSchemeName').val();
            if (wfSchemeName)
                param.WfSchemeName = wfSchemeName;

            var nodeName = $('#NodeName').val();
            if (nodeName)
                param.NodeName = nodeName;

            var projectName = $('#ProjectName').val();
            if (projectName)
                param.ProjectName = projectName;

            var wfSchemeType = $('#WfSchemeType').val();
            if (wfSchemeType)
                param.WfSchemeType = wfSchemeType;

            var levelType = $('#LevelType').mkselectGet();
            if (levelType)
                param.LevelType = levelType;

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param), auditStatus: auditStatus });
        }
    };

    page.init();
}


