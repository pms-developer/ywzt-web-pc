﻿/*
 * 日 期：2017.08.04
 * 描 述：流程（我的任务）	
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";

    var currentNodes = [];
    // 表单页面对象集合
    var formIframes = [];
    //var formIframesData = {};
    var formIframesHave = {};

    var type = request("type");
    var state = request("state");

    var logbegin = '';
    var logend = '';

    refreshGirdData = function () {
        page.search();
    }

    var page = {
        init: function () {
            if (type == "0") {
                $('#verify').show();
            } else {
                $('#verify').hide();
            }

            page.initGrid();
            page.bind();
        },
        bind: function () {
            $("#LevelType").mkselect({
                placeholder:'流程等级',
                text: 'name',
                value: 'id',
                data: [{ id: '', name: '全部' }, { id: '0', name: '普通' }, { id: '1', name: '重要' }, { id: '2', name: '紧急' }]
            })
            $("#AuditStatus").mkselect({
                placeholder: '流程状态',
                text: 'name',
                value: 'id',
                data: [{ id: '', name: '全部' }, { id: '0', name: '审核中' }, { id: '1', name: '已通过' }]
            })
            
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    logbegin = begin;
                    logend = end;
                     
                    page.search( );
                }
            });

            // 查询
            $('#btn_Search').on('click', function () {
             
                page.search( );
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 查看流程进度
            $('#eye').on('click', function () {
                var workflowId = $('#gridtable').jfGridValue('Workflow_Id');
                var taskId = $('#gridtable').jfGridValue('Id');
                var workflowName = $('#gridtable').jfGridValue('WorkflowName');
                var nodeName = $('#gridtable').jfGridValue('NodeName');
                var formId = $('#gridtable').jfGridValue('FormId');
                var infoId = $('#gridtable').jfGridValue('InfoId');
                var projectId = $('#gridtable').jfGridValue("ProjectID");

                if (Changjie.checkrow(workflowId)) {
                    Changjie.frameTab.open({
                        F_ModuleId: workflowId + taskId,
                        F_Icon: 'fa magic',
                        F_FullName: '查看流程进度【' + workflowName + '】',
                        F_UrlAddress: '/WorkFlowModule/WorkFlow/WfProgress?tabIframeId=' + workflowId + taskId + '&type=100' + "&workflowId=" + workflowId + "&taskId=" + taskId + "&formId=" + formId + "&infoId=" + infoId + "&projectId=" + projectId
                    });
                }
            });


            // 审核流程
            $('#verify').on('click', function () {
                var workflowId = $('#gridtable').jfGridValue('Workflow_Id');
                var sear = new RegExp(',');
                if (!sear.test(workflowId)) {
                    var taskId = $('#gridtable').jfGridValue('Id');
                    var workflowName = $('#gridtable').jfGridValue('WorkflowName');
                    var nodeName = $('#gridtable').jfGridValue('NodeName');
                    var formId = $('#gridtable').jfGridValue('FormId');
                    var infoId = $('#gridtable').jfGridValue('InfoId');
                    var projectId = $('#gridtable').jfGridValue("ProjectID");
                    var wfshcemeId = $('#gridtable').jfGridValue("F_SchemeId");

                    if (Changjie.checkrow(taskId)) {
                        Changjie.frameTab.open(
                            {
                                F_ModuleId: taskId,
                                F_Icon: 'fa magic',
                                F_FullName: '审核流程【' + workflowName + '/' + nodeName + '】',
                                F_UrlAddress: '/WorkFlowModule/WorkFlow/AuditForm?tabIframeId=' + taskId + "&workflowId=" + workflowId + "&taskId=" + taskId + "&formId=" + formId + "&infoId=" + infoId + "&projectId=" + projectId + "&wfshcemeId=" + wfshcemeId,
                            });
                    }
                } else {
                    Changjie.alert.warning("多条不能进行审核操作！");
                }

            });


            //// 批量审核流程
            $('#batch_verify').on('click', function () {
                var workflowId = '["' + $('#gridtable').jfGridValue('Workflow_Id') + '"]';
                var taskId = '["' + $('#gridtable').jfGridValue('Id') + '"]';
                var infoId = '["' + $('#gridtable').jfGridValue('InfoId') + '"]';
                var projectId = '["' + $('#gridtable').jfGridValue("ProjectID") + '"]';
                var formId = '["' + $('#gridtable').jfGridValue('FormId') + '"]';




                top.Changjie.layerConfirm('是否确认批量通过审批！', function (res) {

                    workflowId = workflowId.replace(",", '","');
                    taskId = taskId.replace(",", '","');
                    infoId = infoId.replace(",", '","');
                    projectId = projectId.replace(",", '","');
                    formId = formId.replace(",", '","');
                    var workflowIds = JSON.parse(workflowId);
                    var taskIds = JSON.parse(taskId);
                    var formIds = JSON.parse(formId);


                    for (var i = 0; i < workflowIds.length; i++) {
                        Changjie.httpGet(top.$.rootUrl + '/WorkFlowModule/WorkFlow/GetTaskInfo', { workflowId: workflowIds[i], taskId: taskIds[i], isLook: false }, function (res) {
                            currentNodes.push(res.data);
                        });
                    }
                    page.iframesa(workflowId, taskId, infoId, projectId, formId);

                    page.search();

                    currentNodes = [];
                });
            });
        },

        iframesa: function (workflowId, taskId, infoId, projectId, formId) {
            // var workflowIds = JSON.parse(workflowId);
            var taskIds = JSON.parse(taskId);
            var infoIds = JSON.parse(infoId);
            var projectIds = JSON.parse(projectId);
            var formIds = JSON.parse(formId);

            var $iframes = $('#form_list_iframes');
            for (var i = 0, l = currentNodes.length; i < l; i++) {
                var forminfo = currentNodes[i].data.currentNode.wfForms[0];

                $iframes.append('<iframe id="wfFormIframe' + i + '" class="form-list-iframe" style="display:none" data-value="' + i + '" frameborder="0" ></iframe>');
                var url = forminfo.url;
                if (url.indexOf("?") == -1) {
                    url += "?1=1";
                }
                if (url.indexOf("keyValue=") == -1) {
                    url += "&keyValue=" + infoIds[i];
                }

                url += "&infoId=" + infoIds[i] + "&projectId=" + projectIds[i] + "&formId=" + formIds[i];

                Changjie.httpGet(top.$.rootUrl + '/WorkFlowModule/WorkFlow/GetFinishedNodes', { taskId: taskIds[i] }, function (res, a) {

                    var flowScheme1 = currentNodes[i].data;
                    var flowScheme2 = JSON.parse(flowScheme1.scheme);
                    var nodes = flowScheme2.nodes.length - 2;//获取有几个审核节点
                    var finishedNodes = res.data.length;//获取已经审核过的节点数量

                    if (nodes == finishedNodes) {
                        page.iframeLoad("wfFormIframe" + i, url, function (iframeObj, _index) {
                            if (typeof iframeObj.auditPassEventBat !== "undefined") {
                                iframeObj.auditPassEventBat(infoIds[_index]);
                            }
                            if (typeof iframeObj.auditPassEvent !== "undefined") {
                                iframeObj.auditPassEvent(infoIds[_index]);
                            }
                        }, i);
                    }
                });
            }

            for (var j = 0; j < taskIds.length; j++) {
                Changjie.httpPost(top.$.rootUrl + '/WorkFlowModule/WorkFlow/Audit', { taskId: taskIds[j], verifyType: 1 }, function (res) {
                });
            }
        },

        // iframe 加载
        iframeLoad: function (iframeId, url, callback, i) {
            var _iframe = document.getElementById(iframeId);
            var _iframeLoaded = function () {
                var iframeObj = Changjie.iframe(iframeId, frames);
                formIframes.push(iframeObj);
                if (!!iframeObj.$) {
                    callback(iframeObj, i);
                }
            };

            if (_iframe.attachEvent) {
                _iframe.attachEvent("onload", _iframeLoaded);
            } else {
                _iframe.onload = _iframeLoaded;
            }

            setTimeout(function () {
                $('#' + iframeId).attr('src', top.$.rootUrl + url);
            });

        },

        initGrid: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/WorkFlowModule/WorkFlow/GetTaskList',
                headData: [
                    {
                        label: "标题", name: "WorkflowName", width: 300, align: "left",
                    },
                    {
                        label: "等级", name: "Level", width: 60, align: "center",
                        formatter: function (cellvalue) {
                            switch (cellvalue) {
                                case 0:
                                    return '普通';
                                    break;
                                case 1:
                                    return '<div style="background-color:orange;color:#fff;font-weight:bold;font-size:14px">重要</div>';
                                    break;
                                case 2:
                                    return '<div style="background-color:red;color:#fff;font-weight:bold;font-size:14px">紧急</div>';
                                    break;
                            }
                        }
                    },
                    {
                        label: "申请人", name: "CreateUserName", width: 60, align: "center",
                    },
                    {
                        label: "流程分类", name: "WfSchemeType", width: 70, align: "center",
                    },
                    {
                        label: "流程名称", name: "WfSchemeName", width: 120, align: "left",
                    },
                    {
                        label: "流程状态", name: "AuditStatus", width: 60, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatusUseWorkFlow(cellvalue);
                        }
                    },
                    {
                        label: "关联项目", name: "ProjectName", width: 100, align: "left",
                    },
                    {
                        label: "流程节点", name: "NodeName", width: 100, align: "left",
                    },
                    {
                        label: "审批人类型", name: "ApproverType", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case "User":
                                    return '人员';
                                case "Post":
                                    return '岗位';
                                case "Role":
                                    return '角色';
                                case "Company":
                                    return '公司';
                                case "Department":
                                    return '部门';
                                case "ProjectRole":
                                    return '项目角色';
                                case "Sponsor":
                                    return '发起人';
                                case "SponsorSuperior":
                                    return '发起人上级';
                                case "AuditorSuperior":
                                    return '审批人上级';
                                default:
                                    return cellvalue;
                            }
                        }
                    },
                    {
                        label: "当前审批人", name: "ApproverName", width: 80, align: "center",
                    },
                    {
                        label: "发起时间", name: "StartTime", width: 130, align: "center",
                    },
                    {
                        label: "完成时间", name: "FinishTime", width: 130, align: "center",
                    },
                    {
                        label: "流程耗时", name: "ElapsedTimeDescription", width: 150, align: "center",
                    },
                    {
                        label: "备注", name: "Des", width: 160, align: "left",
                    }

                ],
                mainId: 'Id',
                isPage: true,
                sidx: 'CreateTime',
                isMultiselect: true,
            });
        },

        search: function (param) {
            param = param || {};
            param.StartTime = logbegin;
            param.EndTime = logend;
            var keyword = $('#txt_Keyword').val();
            if (keyword)
                param.keyword = keyword;

            var workflowName = $('#WorkflowName').val();
            if (workflowName)
                param.WorkflowName = workflowName;

            var wfSchemeName = $('#WfSchemeName').val();
            if (wfSchemeName)
                param.WfSchemeName = wfSchemeName;

            var nodeName = $('#NodeName').val();
            if (nodeName)
                param.NodeName = nodeName;

            var projectName = $('#ProjectName').val();
            if (projectName)
                param.ProjectName = projectName;

            var wfSchemeType = $('#WfSchemeType').val();
            if (wfSchemeType)
                param.WfSchemeType = wfSchemeType;

            var auditStatus = $('#AuditStatus').mkselectGet();
            if (auditStatus)
                param.AuditStatus = auditStatus;

            var levelType = $('#LevelType').mkselectGet();
            if (levelType)
                param.LevelType = levelType;

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param), type: type });
        }
    };

    page.init();
}


