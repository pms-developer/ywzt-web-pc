﻿/*
 * 日 期：2017.04.18
 * 描 述：工作流操作界面
 */
var tabIframeId = request('tabIframeId');  // 当前窗口ID
var wfschemeId = request('wfshcemeId');    // 流程模板ID
var formType = request("formType");        // Customer自定义表单 System系统表单
var workflowId = request('workflowId');      // 流程实例主键
var taskId = request('taskId');            // 任务主键
var infoId = request("infoId");            // 信息ID
var formId = request("formId");            //表单ID
var projectId = request("projectId");
var currentNode;
var flowScheme;
var flowHistory = [];
var currentIds = [];
var allFormDatas = {};                     // 表单数据 

var bootstrap = function ($, Changjie) {
    "use strict";

    // 表单页面对象集合
    var formIframes = [];
    var formIframesData = {};

    var page = {
        init: function () {

            page.bind();
            page.initflow();
        },
        bind: function () {
            // 显示信息选项卡
            $('#tablist').mkFormTabEx(function (id) {
            });

            $('#flow').mkworkflow({
                isPreview: true,
                openNode: function (node) {
                    currentNode = node;
                    if (!!node.history) {
                        Changjie.layerForm({
                            id: 'WfNodeForm',
                            title: '审核记录查看【' + node.name + '】',
                            url: top.$.rootUrl + '/WorkFlowModule/WfMyTask/WfNodeForm',
                            width: 600,
                            height: 400,
                            btn: null
                        });
                    }
                }
            });
        },
        // 初始化流程信息
        initflow: function () {

            Changjie.workflowApi.taskinfo({
                workflowId: workflowId,
                taskId: taskId,
                isLook: true,
                callback: function (res, data) {
                    if (res) {
                        // 初始化表单信息
                        currentNode = data.currentNode;
                        var $iframes = $('#form_list_iframes');
                        for (var i = 0, l = currentNode.wfForms.length; i < l; i++) {
                            var forminfo = currentNode.wfForms[i];

                            $iframes.append('<iframe id="wfFormIframe' + i + '" class="form-list-iframe" style="display:block" data-value="' + i + '" frameborder="0" ></iframe>');
                            formIframesData[i] = forminfo;
                            var url = getFormUrl(forminfo.url);
                            page.iframeLoad("wfFormIframe" + i, url, function (iframeObj, _index) {
                                // 设置字段权限
                                iframeObj.setAuthorize(currentNode.fromPermission);
                                if (!!formIframesData[_index].field) {
                                    iframeObj.workflowIdName = formIframesData[_index].field;
                                }
                                iframeObj.setFormData(workflowId);
                            }, i);
                        }
                        // 初始化流程信息
                        flowScheme = JSON.parse(data.scheme);
                        flowHistory = data.history || [];
                        currentIds = data.currentNodeIds || [];
                        initScheme();
                        initTimeLine(flowHistory);
                    }
                    else {
                        Changjie.frameTab.close(tabIframeId);
                    }
                }
            });

        },
        // iframe 加载
        iframeLoad: function (iframeId, url, callback, i) {
            var _iframe = document.getElementById(iframeId);
            var _iframeLoaded = function () {
                var iframeObj = Changjie.iframe(iframeId, frames);
                formIframes.push(iframeObj);
                if (!!iframeObj.$) {
                    callback(iframeObj, i);
                }
            };

            if (_iframe.attachEvent) {
                _iframe.attachEvent("onload", _iframeLoaded);
            } else {
                _iframe.onload = _iframeLoaded;
            }
            setTimeout(function () {
                $('#' + iframeId).attr('src', top.$.rootUrl + url);
            }, i * 500);

        }
    };


    function initScheme() {
        // 初始化工作流节点历史处理信息
        var nodeInfoes = {};
        $.each(flowHistory, function (id, item) {
            nodeInfoes[item.NodeId] = nodeInfoes[item.NodeId] || [];
            nodeInfoes[item.NodeId].push(item);
        });
        var strcurrentIds = String(currentIds);
        // 初始化节点状态
        for (var i = 0, l = flowScheme.nodes.length; i < l; i++) {
            var node = flowScheme.nodes[i];
            node.state = '3';
            if (!!nodeInfoes[node.nodeId]) {
                node.history = nodeInfoes[node.nodeId];
                if (nodeInfoes[node.nodeId][0].Result == 1) {
                    node.state = '1';
                }
                else {
                    node.state = '2';
                }
            }
            if (strcurrentIds.indexOf(node.nodeId) > -1) {
                node.state = '0';
            }
            if (currentNode.id == node.nodeId) {
                node.state = '4';
            }
        }
        $('#flow').mkworkflowSet('set', { data: flowScheme });
    }

    function initTimeLine(flowHistory) {
        var nodelist = [];
        for (var i = 0, l = flowHistory.length; i < l; i++) {
            var item = flowHistory[i];
            var content = '【审核:<span style="color:Red">';
            if (item.Result == 1) {
                content += '通过';
            }
            else {
                content += '不通过';
            }
            content += '</span>】';
            if (item.Description) {
                content += '【意见:<span style="color:Red">' + item.Description + '</span>】';
            }
            content += ' 【耗时：' + item.ElapsedTimeDescription+'】';
            var point = {
                title: item.NodeName,
                people: item.AuditorUserName + ':',
                content: content,
                time: item.AuditTime
            };
            nodelist.push(point);
        }
        $('#auditinfo').mktimeline(nodelist);
        $('#auditinfo').mkscroll();
    }

    page.init();



}

var getFormUrl = function (formurl) {
    var url = formurl;
    if (url.indexOf("?") == -1) {
        url += "?1=1";
    }
    if (url.indexOf("keyValue=") == -1) {
        url += "&keyValue=" + infoId;
    }
    url += "&infoId=" + infoId + "&projectId=" + projectId + "&formId=" + formId;
    return url;
};