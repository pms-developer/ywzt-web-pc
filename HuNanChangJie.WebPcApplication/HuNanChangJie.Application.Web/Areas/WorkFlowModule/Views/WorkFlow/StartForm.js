﻿/*
 * 描 述：发起流程	
 */
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
 
    var page = {
        init: function () {

            var infoId = request("infoId");
            var formId = request("formId");
            page.bind();

            if (!!formId) {
                var url = top.$.rootUrl + '/WorkFlowModule/WfScheme/GetSchemeInfoListNoDelete';
                var param = { formId: formId };
                var data = top.Changjie.httpGet(url, param);

                $("#workflowInfo").mkselect({
                    value: "F_Id",
                    text: "F_Name",
                    data: data.data,
                    select: function (item) {
                        
                    }
                });

            }


        },
        bind: function () {

            var title = "";
            var projectName = "";
            var moduleName = "";

            $('#projectId').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
            
            var projectId = request("projectId");
            if (projectId) {
                $('#projectId').mkselectSet(projectId);
                $('#projectId').attr("readonly", "readonly");
                title = "【" + $('#projectId').mkselectGetText() + "】项目"
            }
            else {
                $('#projectId').removeAttr("readonly");
            }



            var moduleId = request("moduleId");
            if (moduleId) {
                var info = Changjie.httpGet(top.$.rootUrl + '/SystemModule/Module/GetModuleInfo?keyValue=' + moduleId).data;
                moduleName = info.F_FullName;
            }
            if (moduleName) {
                title += "【" + moduleName + "】单据审批"
            }

            $("#workflowName").val(title);

            $('#projectId').on("change", function () {

                title = "【" + $('#projectId').mkselectGetText() + "】项目"
                if (moduleName) {
                    title += "【" + moduleName + "】单据审批"
                }

                $("#workflowName").val(title);
            });
            
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        
        var formData = $('#form').mkGetFormData();
        callBack(formData);
        return true;
    };
    page.init();
}