﻿/*
 * 描 述：节点信息展示
 */
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";

    var currentNode = Changjie.frameTab.currentIframe().currentNode;


    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#gridtable').jfGrid({
                headData: [
                    { label: "执行人", name: "AuditorUserName", width: 100, align: "left", frozen: true },
                    {
                        label: "执行动作", name: "Result", width: 80, align: "center", frozen: true,
                        formatter: function (cellvalue, row) {
                            switch (cellvalue)//1：通过；2：未通过；3：驳回至某一节点；4、加签通过（自由选择人）
                            {
                                case 0:
                                    return '<span class=\"label label-success \" >发起</span>';
                                case 1:
                                    return '<span class=\"label label-primary \" >审批-同意</span>';
                                case 2:
                                    return '<span class=\"label label-info \" >未通过</span>';
                                case 3:
                                    return '<span class=\"label label-primary \" >审批-驳回</span>';
                                case 4:
                                    return '<span class=\"label label-warning \" >加签通过</span>';
                            }
                        }
                    },
                    {
                        label: "执行时间", name: "AuditTime", width: 140, align: "center", frozen: true,
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd hh:mm:ss');
                        }
                    },
                    { label: "内容", name: "Description", width: 400, align: "left" }
                ]
            });
        },
        initData: function () {
            if (!!currentNode) {
                $('#gridtable').jfGridSet('refreshdata', currentNode.history);
            }
        }
    };
    page.init();
}