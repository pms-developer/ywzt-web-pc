﻿/*
 * 日 期：2017.04.18
 * 描 述：审核流程	
 */
var acceptClick;

var workflowId = request('workflowId');      // 流程实例主键
var taskId = request('taskId');            // 任务主键

var bootstrap = function ($, Changjie) {
    "use strict";
    var workflowId = Changjie.frameTab.currentIframe().workflowId;
    var taskId = Changjie.frameTab.currentIframe().taskId;
    var formData = Changjie.frameTab.currentIframe().allFormDatas;
    var finishData = [];

    var page = {
        init: function () {
            page.initData();

            /// 获取下一个节点的审核人信息数据
            //Changjie.workflowApi.auditer({
            //    isNew: (!!shcemeCode) ? true : false,
            //    schemeCode: shcemeCode,
            //    processId: processId,
            //    taskId: taskId,
            //    formData: JSON.stringify(formData),
            //    callback: function (res) {
            //        var $form = $('#description').parent();
            //        $.each(res, function (_i, _item) {
            //            if (_item.all || _item.list.length == 0) {
            //                $form.before('<div class="col-xs-12 mk-form-item"><div class="mk-form-item-title language" >' + _item.name + '</div><div id="' + _item.nodeId + '" class="nodeId"></div></div >');
            //                $('#' + _item.nodeId).mkUserSelect(0);
            //            }
            //            else if (_item.list.length > 1) {
            //                $form.before('<div class="col-xs-12 mk-form-item"><div class="mk-form-item-title language" >' + _item.name + '</div><div id="' + _item.nodeId + '" class="nodeId" ></div></div >');
            //                $('#' + _item.nodeId).mkselect({
            //                    data: _item.list,
            //                    id: 'id',
            //                    text: 'name'
            //                });
            //            }
            //        });
            //    }
            //});

            // 如果驳回隐藏掉下一个节点审核人员
            // 权限设置
            $('[name="verifyType"]').on('click', function () {
                var $this = $(this);
                var value = $this.val();
                if (value == '1') {
                    $(".nopassType").remove();
                    $(".toNode").remove();
                }
                else {
                    var $html = $(".verifyType");
                    var div = '<div class="col-xs-12 mk-form-item nopassType">' +
                        '<div class="mk-form-item-title language" >处理方式<font face = "宋体">*</font></div>' +
                        '<div id="nopassType" isvalid="yes" checkexpession="NotNull"></div>' +
                        '</div>';
                    $html.after(div);

                    $("#nopassType").mkselect({
                        placeholder: "请选择",
                        value: "id",
                        text: "text",
                        title: "title",
                        data: [
                            { id: "Back", text: "驳回至上一节点" },
                            { id: "Toback", text: "驳回至指定节点" },
                            { id: "End", text: "结束此流程" },
                        ],
                        select: function (item) {

                            if (!!item) {
                                if (item.id == "Toback" ) {
                                    var $html = $(".nopassType");
                                    var div = '<div class="col-xs-12 mk-form-item toNode">' +
                                        '<div class="mk-form-item-title language">流程节点<font face="宋体" >*</font></div>' +
                                        '<div id="toNode" isvalid="yes" checkexpession="NotNull"></div>' +
                                        '</div>';
                                    $html.after(div);

                                    $("#toNode").mkselect({
                                        placeholder: false,
                                        value: "nodeId",
                                        text: "name",
                                        title: "name",
                                        data: finishData,
                                    });
                                }
                                else {
                                    $(".toNode").remove();

                                }
                            }
                        }
                    });
                }
            });
        },

        initData: function () {
            //$('#form').mkSetFormData(currentLine);

            var url = top.$.rootUrl + '/WorkFlowModule/WorkFlow/GetFinishedNodes'
            var param = { taskId: taskId };
            finishData = top.Changjie.httpGet(url, param).data;
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var formData = $('#form').mkGetFormData();

        if (formData.verifyType == undefined) {
            Changjie.alert.error('请选择审核结果！');
            return false;
        }
        // 获取审核人员
        var auditers = {};
        $('#form').find('.nodeId').each(function () {
            var $this = $(this);
            var id = $this.attr('id');
            var type = $this.attr('type');
            if (!!formData[id]) {
                var point = {
                    userId: formData[id],
                };
                if (type == 'mkselect') {
                    point.userName = $this.find('.mk-select-placeholder').text();
                }
                else {
                    point.userName = $this.find('span').text();
                }
                auditers[id] = point;
            }
        });
        if (formData.verifyType == "2") {
            if (formData.nopassType == "Back") {
                formData.toNode = finishData[0].nodeId;
            }
        }

        callBack(formData);
        return true;
    };
    page.init();
}