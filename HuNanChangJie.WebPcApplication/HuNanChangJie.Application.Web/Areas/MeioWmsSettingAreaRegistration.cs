﻿using System.Web.Mvc;
namespace HuNanChangJie.Application.Web.Areas.MeioWmsSetting
{
    public class MeioWmsSettingAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MeioWmsSetting"; 
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MeioWmsSetting_default", 
                "MeioWmsSetting/{controller}/{action}/{id}", 
                             new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
