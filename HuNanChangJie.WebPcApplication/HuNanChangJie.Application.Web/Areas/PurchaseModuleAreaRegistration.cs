﻿using System.Web.Mvc;
namespace HuNanChangJie.Application.Web.Areas.PurchaseModule
{
    public class PurchaseModuleAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PurchaseModule"; 
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PurchaseModule_default", 
                "PurchaseModule/{controller}/{action}/{id}", 
                             new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
