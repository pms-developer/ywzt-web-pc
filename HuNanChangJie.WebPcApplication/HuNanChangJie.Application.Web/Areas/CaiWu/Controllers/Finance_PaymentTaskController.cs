﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    public class Finance_PaymentTaskController : MvcControllerBase
    {

        private Finance_ExternalPaymentIBLL finance_ExternalPaymentIBLL = new Finance_ExternalPaymentBLL();
        private Finance_PaymentTaskIBLL finance_PaymentTaskIBLL = new Finance_PaymentTaskBLL();

        // GET: CaiWu/Finance_PaymentTask
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Dialog()
        {
            return View();
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetDetailsList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_ExternalPaymentIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_PaymentTaskIBLL.GetPageList(paginationobj, queryJson);
            //var jsonData = new { rows = data };
            //return Success(jsonData);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
    }
}