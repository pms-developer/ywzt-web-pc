﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-23 12:14
    /// 描 述：财务 分包收票
    /// </summary>
    public class Finance_SubcontractInvoiceController : MvcControllerBase
    {
        private Finance_SubcontractInvoiceIBLL finance_SubcontractInvoiceIBLL = new Finance_SubcontractInvoiceBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }

        public ActionResult Dialog()
        {
            return View();
        }


        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_SubcontractInvoiceIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Finance_SubcontractInvoiceData = finance_SubcontractInvoiceIBLL.GetFinance_SubcontractInvoiceEntity( keyValue );
            var Finance_SubcontractInvoice_CertificateData = finance_SubcontractInvoiceIBLL.GetFinance_SubcontractInvoice_CertificateList( Finance_SubcontractInvoiceData.ID );
            var Finance_SubcontractInvoice_InvoiceData = finance_SubcontractInvoiceIBLL.GetFinance_SubcontractInvoice_InvoiceList( Finance_SubcontractInvoiceData.ID );
            var jsonData = new {
                Finance_SubcontractInvoice = Finance_SubcontractInvoiceData,
                Finance_SubcontractInvoice_Certificate = Finance_SubcontractInvoice_CertificateData,
                Finance_SubcontractInvoice_Invoice = Finance_SubcontractInvoice_InvoiceData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = finance_SubcontractInvoiceIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = finance_SubcontractInvoiceIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            finance_SubcontractInvoiceIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strfinance_SubcontractInvoice_CertificateList, string strfinance_SubcontractInvoice_InvoiceList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Finance_SubcontractInvoiceEntity>();
            var finance_SubcontractInvoice_CertificateList = strfinance_SubcontractInvoice_CertificateList.ToObject<List<Finance_SubcontractInvoice_CertificateEntity>>();
            var finance_SubcontractInvoice_InvoiceList = strfinance_SubcontractInvoice_InvoiceList.ToObject<List<Finance_SubcontractInvoice_InvoiceEntity>>();
            finance_SubcontractInvoiceIBLL.SaveEntity(keyValue,mainInfo,finance_SubcontractInvoice_CertificateList,finance_SubcontractInvoice_InvoiceList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
