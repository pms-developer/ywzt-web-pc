﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-06-29 18:37
    /// 描 述：借款冲销
    /// </summary>
    public class Finance_LoanWriteOffController : MvcControllerBase
    {
        private Finance_LoanWriteOffIBLL finance_LoanWriteOffIBLL = new Finance_LoanWriteOffBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        #endregion

        #region  获取数据
        
        //[HttpGet]
        //[AjaxOnly]
        //public ActionResult GetDanJuList(string pagination, string queryJson)
        //{
        //    XqPagination paginationobj = pagination.ToObject<XqPagination>();
        //    var data = finance_LoanWriteOffIBLL.GetDanJuList(paginationobj, queryJson);
        //    var jsonData = new
        //    {
        //        rows = data,
        //        total = paginationobj.total,
        //        page = paginationobj.page,
        //        records = paginationobj.records
        //    };
        //    return Success(jsonData);
        //}
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_LoanWriteOffIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Finance_LoanWriteOffData = finance_LoanWriteOffIBLL.GetFinance_LoanWriteOffEntity( keyValue );
            var Finance_LoanWriteOffDetailsData = finance_LoanWriteOffIBLL.GetFinance_LoanWriteOffDetailsList( Finance_LoanWriteOffData.ID );
            var jsonData = new {
                Finance_LoanWriteOff = Finance_LoanWriteOffData,
                Finance_LoanWriteOffDetails = Finance_LoanWriteOffDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = finance_LoanWriteOffIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = finance_LoanWriteOffIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            finance_LoanWriteOffIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strfinance_LoanWriteOffDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Finance_LoanWriteOffEntity>();
            var finance_LoanWriteOffDetailsList = strfinance_LoanWriteOffDetailsList.ToObject<List<Finance_LoanWriteOffDetailsEntity>>();
            finance_LoanWriteOffIBLL.SaveEntity(keyValue,mainInfo,finance_LoanWriteOffDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
