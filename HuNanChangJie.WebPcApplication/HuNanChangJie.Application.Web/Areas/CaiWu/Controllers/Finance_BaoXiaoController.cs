﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-07 10:21
    /// 描 述：报销单
    /// </summary>
    public class Finance_BaoXiaoController : MvcControllerBase
    {
        private Finance_BaoXiaoIBLL finance_BaoXiaoIBLL = new Finance_BaoXiaoBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        [HttpGet]
        public ActionResult IndexCai()
        {
            return View();
        }
        
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_BaoXiaoIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Finance_BaoXiaoData = finance_BaoXiaoIBLL.GetFinance_BaoXiaoEntity( keyValue );
            var Finance_BaoXiaoDetailsData = finance_BaoXiaoIBLL.GetFinance_BaoXiaoDetailsList( Finance_BaoXiaoData.ID );
            var jsonData = new {
                Finance_BaoXiao = Finance_BaoXiaoData,
                Finance_BaoXiaoDetails = Finance_BaoXiaoDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            finance_BaoXiaoIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strfinance_BaoXiaoDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Finance_BaoXiaoEntity>();
            var finance_BaoXiaoDetailsList = strfinance_BaoXiaoDetailsList.ToObject<List<Finance_BaoXiaoDetailsEntity>>();
            finance_BaoXiaoIBLL.SaveEntity(keyValue,mainInfo,finance_BaoXiaoDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
