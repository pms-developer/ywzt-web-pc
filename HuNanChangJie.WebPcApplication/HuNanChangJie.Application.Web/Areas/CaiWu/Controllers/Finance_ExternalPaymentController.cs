﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-05 19:09
    /// 描 述：财务-对外付款单
    /// </summary>
    public class Finance_ExternalPaymentController : MvcControllerBase
    {
        private Finance_ExternalPaymentIBLL finance_ExternalPaymentIBLL = new Finance_ExternalPaymentBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_ExternalPaymentIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取页面显示列表数据 来自付款任务
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageListByTask(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_ExternalPaymentIBLL.GetPageListByTask(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetHeTongList(string pagination,string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_ExternalPaymentIBLL.GetHeTongList(paginationobj, queryJson);
            //var jsonData = new { rows = data };
            //return Success(jsonData);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetHeTongListByTask(string queryJson)
        {
            var data = finance_ExternalPaymentIBLL.GetHeTongListByTask(queryJson);
            return Success(data);
        }
        

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Finance_ExternalPaymentData = finance_ExternalPaymentIBLL.GetFinance_ExternalPaymentEntity( keyValue );
            var Finance_ExternalPaymentDetailsData = finance_ExternalPaymentIBLL.GetFinance_ExternalPaymentDetailsList( Finance_ExternalPaymentData.ID );
            var jsonData = new {
                Finance_ExternalPayment = Finance_ExternalPaymentData,
                Finance_ExternalPaymentDetails = Finance_ExternalPaymentDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = finance_ExternalPaymentIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }


        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = finance_ExternalPaymentIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult BeforeSendingCheckData(string keyValue)
        {
            var result = finance_ExternalPaymentIBLL.BeforeSendingCheckData(keyValue);

            return Success(result);
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            finance_ExternalPaymentIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strfinance_ExternalPaymentDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Finance_ExternalPaymentEntity>();
            var finance_ExternalPaymentDetailsList = strfinance_ExternalPaymentDetailsList.ToObject<List<Finance_ExternalPaymentDetailsEntity>>();
            finance_ExternalPaymentIBLL.SaveEntity(keyValue,mainInfo,finance_ExternalPaymentDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
