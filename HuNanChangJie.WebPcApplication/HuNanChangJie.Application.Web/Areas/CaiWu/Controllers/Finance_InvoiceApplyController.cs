﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-27 20:15
    /// 描 述：财务 开票申请
    /// </summary>
    public class Finance_InvoiceApplyController : MvcControllerBase
    {
        private Finance_InvoiceApplyIBLL finance_InvoiceApplyIBLL = new Finance_InvoiceApplyBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_InvoiceApplyIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Finance_InvoiceApplyData = finance_InvoiceApplyIBLL.GetFinance_InvoiceApplyEntity( keyValue );
            var Finance_InvoiceApplyInfoData = finance_InvoiceApplyIBLL.GetFinance_InvoiceApplyInfoList( Finance_InvoiceApplyData.ID );
            var Finance_InvoiceApplyDetailsData = finance_InvoiceApplyIBLL.GetFinance_InvoiceApplyDetailsList( Finance_InvoiceApplyData.ID );
            var jsonData = new {
                Finance_InvoiceApply = Finance_InvoiceApplyData,
                Finance_InvoiceApplyInfo = Finance_InvoiceApplyInfoData,
                Finance_InvoiceApplyDetails = Finance_InvoiceApplyDetailsData,
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFinanceInvoiceApplyInfoList(string projectid)
        {
          
            var Finance_InvoiceApplyInfoData = finance_InvoiceApplyIBLL.GetFinance_InvoiceApplyInfoListByProjectId(projectid);
          
            var jsonData = new
            {
                Finance_InvoiceApplyInfo = Finance_InvoiceApplyInfoData
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            finance_InvoiceApplyIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strfinance_InvoiceApplyInfoList, string strfinance_InvoiceApplyDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Finance_InvoiceApplyEntity>();
            var finance_InvoiceApplyInfoList = strfinance_InvoiceApplyInfoList.ToObject<List<Finance_InvoiceApplyInfoEntity>>();
            var finance_InvoiceApplyDetailsList = strfinance_InvoiceApplyDetailsList.ToObject<List<Finance_InvoiceApplyDetailsEntity>>();
            finance_InvoiceApplyIBLL.SaveEntity(keyValue,mainInfo,finance_InvoiceApplyInfoList,finance_InvoiceApplyDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
