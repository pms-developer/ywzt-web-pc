﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-16 17:48
    /// 描 述：其它收款
    /// </summary>
    public class Finance_OtherProceedsController : MvcControllerBase
    {
        private Finance_OtherProceedsIBLL finance_OtherProceedsIBLL = new Finance_OtherProceedsBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_OtherProceedsIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Finance_OtherProceedsData = finance_OtherProceedsIBLL.GetFinance_OtherProceedsEntity( keyValue );
            var jsonData = new {
                Finance_OtherProceeds = Finance_OtherProceedsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = finance_OtherProceedsIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = finance_OtherProceedsIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            finance_OtherProceedsIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Finance_OtherProceedsEntity>();
            finance_OtherProceedsIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
