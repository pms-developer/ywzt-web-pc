﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 16:04
    /// 描 述：对内付款单
    /// </summary>
    public class Finance_InternalPaymentController : MvcControllerBase
    {
        private Finance_InternalPaymentIBLL finance_InternalPaymentIBLL = new Finance_InternalPaymentBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }

        #endregion

        #region  获取数据

        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = finance_InternalPaymentIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = finance_InternalPaymentIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_InternalPaymentIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFuKuanList(string pagination,string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_InternalPaymentIBLL.GetFuKuanList(paginationobj,queryJson);
            //var jsonData = new { rows = data };
            //return Success(jsonData);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult LoadDanJuData(int source,string keyid)
        {
            var data = finance_InternalPaymentIBLL.LoadDanJuData(source, keyid);
            return Success(data);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Finance_InternalPaymentData = finance_InternalPaymentIBLL.GetFinance_InternalPaymentEntity( keyValue );
            var Finance_InternalPaymentDetailsData = finance_InternalPaymentIBLL.GetFinance_InternalPaymentDetailsList( Finance_InternalPaymentData.ID );
            var jsonData = new {
                Finance_InternalPayment = Finance_InternalPaymentData,
                Finance_InternalPaymentDetails = Finance_InternalPaymentDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        [HttpPost]
        [AjaxOnly]
        public ActionResult BeforeSendingCheckData(string keyValue)
        {
            var result = finance_InternalPaymentIBLL.BeforeSendingCheckData(keyValue);

            return Success(result);
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            finance_InternalPaymentIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strfinance_InternalPaymentDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Finance_InternalPaymentEntity>();
            var finance_InternalPaymentDetailsList = strfinance_InternalPaymentDetailsList.ToObject<List<Finance_InternalPaymentDetailsEntity>>();
            finance_InternalPaymentIBLL.SaveEntity(keyValue,mainInfo,finance_InternalPaymentDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
