﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 17:44
    /// 描 述：收款单管理
    /// </summary>
    public class Finance_ProceedsController : MvcControllerBase
    {
        private Finance_ProceedsIBLL finance_ProceedsIBLL = new Finance_ProceedsBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = finance_ProceedsIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Finance_ProceedsData = finance_ProceedsIBLL.GetFinance_ProceedsEntity( keyValue );
            var Finance_ProceedsAccountData = finance_ProceedsIBLL.GetFinance_ProceedsAccountList( Finance_ProceedsData.ID );
            var Finance_ProceedsAgreementData = finance_ProceedsIBLL.GetFinance_ProceedsAgreementList( Finance_ProceedsData.ID );
            var Finance_ProceedsVerificationData = finance_ProceedsIBLL.GetFinance_ProceedsVerificationList( Finance_ProceedsData.ID );
            var jsonData = new {
                Finance_Proceeds = Finance_ProceedsData,
                Finance_ProceedsAccount = Finance_ProceedsAccountData,
                Finance_ProceedsAgreement = Finance_ProceedsAgreementData,
                Finance_ProceedsVerification = Finance_ProceedsVerificationData,
            };
            return Success(jsonData);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult GetFinance_ProceedsAgreementListByContractid(string contractid)
        {
            var data = finance_ProceedsIBLL.GetFinance_ProceedsAgreementListByContractid(contractid);

            return Success(data);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult GetFinance_ProceedsVerificationListByContractid(string contractid)
        {

            var data = finance_ProceedsIBLL.GetFinance_ProceedsVerificationListByContractid(contractid);

            return Success(data);
        }
        #endregion

        #region  提交数据


        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = finance_ProceedsIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = finance_ProceedsIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            finance_ProceedsIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strfinance_ProceedsAccountList, string strfinance_ProceedsAgreementList, string strfinance_ProceedsVerificationList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Finance_ProceedsEntity>();
            var finance_ProceedsAccountList = strfinance_ProceedsAccountList.ToObject<List<Finance_ProceedsAccountEntity>>();
            var finance_ProceedsAgreementList = strfinance_ProceedsAgreementList.ToObject<List<Finance_ProceedsAgreementEntity>>();
            var finance_ProceedsVerificationList = strfinance_ProceedsVerificationList.ToObject<List<Finance_ProceedsVerificationEntity>>();
            finance_ProceedsIBLL.SaveEntity(keyValue,mainInfo,finance_ProceedsAccountList,finance_ProceedsAgreementList,finance_ProceedsVerificationList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
