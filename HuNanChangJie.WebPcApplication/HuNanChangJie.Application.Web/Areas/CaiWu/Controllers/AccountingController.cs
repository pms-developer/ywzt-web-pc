﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.CaiWu.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-12-14 11:43
    /// 描 述：税费核算
    /// </summary>
    public class AccountingController : MvcControllerBase
    {
        private AccountingIBLL accountingIBLL = new AccountingBLL();


        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        [HttpGet]
        public ActionResult FormPiao()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = accountingIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var AccountingData = accountingIBLL.GetAccountingEntity( keyValue );
            var AccountingTnvoiceDetailData = accountingIBLL.GetAccountingTnvoiceDetailList(AccountingData.ID);
            var AccountingTaxDetailData = accountingIBLL.GetAccountingTaxDetailList(AccountingData.ID);

            var jsonData = new {
                Accounting = AccountingData,
                AccountingTnvoiceDetail = AccountingTnvoiceDetailData,
                AccountingTaxDetail = AccountingTaxDetailData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据


        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
           
            var result = accountingIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = accountingIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            accountingIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strpurchase_AccountingTnvoiceDetailList,string strproject_AccountingTaxDetailList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<AccountingEntity>();
            var accountingTnvoiceDetailList = strpurchase_AccountingTnvoiceDetailList.ToObject<List<AccountingTnvoiceDetailEntity>>();
            var accountingTaxDetailList = strproject_AccountingTaxDetailList.ToObject<List<AccountingTaxDetailEntity>>();

            accountingIBLL.SaveEntity(keyValue, mainInfo, accountingTnvoiceDetailList, accountingTaxDetailList, deleteList, type);
            return Success("保存成功！");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveFormPiao(string keyValue, string type, string strEntity, string strpurchase_AccountingTnvoiceDetailList, string strproject_AccountingTaxDetailList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<AccountingEntity>();
            var accountingTnvoiceDetailList = strpurchase_AccountingTnvoiceDetailList.ToObject<List<AccountingTnvoiceDetailEntity>>();
            var accountingTaxDetailList = strproject_AccountingTaxDetailList.ToObject<List<AccountingTaxDetailEntity>>();

            var result = accountingIBLL.SaveEntityPiao(keyValue, mainInfo, accountingTnvoiceDetailList, accountingTaxDetailList, deleteList, type);

            if (result.Success)
                return Success("保存成功！");
            else
                return Fail("保存失败！" + result.Message); 
        }
        
        #endregion

    }
}
