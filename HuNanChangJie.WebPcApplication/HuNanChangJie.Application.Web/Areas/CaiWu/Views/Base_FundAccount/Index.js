﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-01-02 18:14
 * 描  述：资金帐户
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增资金帐户',
                    url: top.$.rootUrl + '/CaiWu/Base_FundAccount/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Base_FundAccount/GetPageList',
                headData: [
                    { label: "账户名称", name: "Name", width: 260, align: "left" },
                    { label: "开户银行", name: "Bank", width: 150, align: "left" },
                    { label: "账号", name: "BankCode", width: 150, align: "left" },
                    { label: "可用余额", name: "UsableBalance", width: 150, align: "left" },
                    { label: "余额", name: "Balance", width: 150, align: "left" },
                    { label: "冻结额", name: "Freezed", width: 150, align: "left" },
                    { label: "项目经理", name: "F_RealName", width: 150, align: "left" },
                    { label: "开户人", name: "Holder", width: 150, align: "left" },
                    {
                        label: "所属公司", name: "CompanyId", width: 150, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('company', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "绑定项目", name: "ProjectId", width: 300, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },
                    { label: "说明", name: "Remark", width: 100, align: "left" },
                    {
                        label: "操作", name: "More", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return "<a href='javascript:void(0)' onclick=\"getMore('" + row.ID + "','" + row.Name + "')\"><span style='color:blue'>查看详情</span></a>";
                        }
                    },
                ],
                dblclick: function (row) {
                    if (row && row.ID)
                        getMore(row.ID, row.Name)
                },
                mainId: 'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.ProjectId = projectId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Base_FundAccount/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/CaiWu/Base_FundAccount/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId,
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
var getMore = function (id, name) {
    top.Changjie.frameTab.open({ F_ModuleId: 'getMore' + id, F_Icon: 'fa fa-file-text-o', F_FullName: name + '资金详情', F_UrlAddress: '/CaiWu/Base_FundAccount/Details?keyValue=' + id });
};