﻿var refreshGirdData;
var formId = request("formId"); var checkJson = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '3',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 260, 400);
            $("#OccurrenceType").mkselect({
                text: 'name',
                value: 'id',
                data: [{ id: '', name: '全部' }, { id: 'Income', name: '收入' }, { id: 'Outlay', name: '支出' }]
            })
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/CaiWu/Base_FundAccount/GetDetails',
                headData: [
                    { label: "发生项目", name: "ProjectName", width: 200, align: "left" },
                    { label: "发生科目", name: "SubjectName", width: 150, align: "left" },
                    { label: "单据编号", name: "Code", width: 100, align: "left" },
                    { label: "项目经理", name: "F_Account", width: 70, align: "center" },
                    { label: "客户名称", name: "CustomerFullName", width: 220, align: "left" },

                    // 采购付款：供应商， 分包付款：分包单位。
                    { label: "对方单位", name: "FullName", width: 220, align: "left" },

                    { label: "期初金额", name: "PrevBalance", width: 100, align: "left" },
                    {
                        label: "发生额", name: "AmountOccurrence", width: 100, align: "left", statistics: true,formatter: function (cellvalue, row) {
                        if (row.OccurrenceTypeValue == '支出') {
                            return "-" + row.AmountOccurrence;
                        }
                        return row.AmountOccurrence;
                        } },
                    { label: "余额", name: "Balance", width: 100, align: "left" },
                    { label: "发生类型", name: "OccurrenceTypeValue", width: 100, align: "center" },
                    { label: "单据类型", name: "ReceiptTypeValue", width: 100, align: "center" },
                    { label: "发生时间", name: "OccurrenceDate", width: 150, align: "center" },
                    { label: "经办人", name: "F_RealName", width: 60, align: "center" },
                    { label: "摘要", name: "Abstract", width: 200, align: "left" },
                    {
                        label: "操作", name: "More", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            if (row.Url) {
                                return "<a href='javascript:void(0)' onclick=\"getMore('" + row.RelevanceId+"','" + row.Url + "','"+row.Code+"单据详情')\"><span style='color:blue'>查看详情</span></a>";
                            }
                            return "";
                        }
                    },
                ],
                mainId: 'ID',
                isPage: true,
                footerrow: true,
                isStatistics: true,
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            var keyValue = request("keyValue");
            if (keyValue) {
                param.FundAccountId = keyValue;
                $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
            }

        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};

var getMore = function (id,url,name) {
    top.Changjie.frameTab.open({ F_ModuleId: id, F_Icon: 'fa fa-file-text-o', F_FullName: name, F_UrlAddress: url });
};