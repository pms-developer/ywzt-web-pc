﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-16 18:32
 * 描  述：资金账户
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var type = request('type');
var companyId = request('companyId');

var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {

            $("#btn_Search").on("click", function () {
                page.search();
            })
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("")
                page.search();
            })

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/CaiWu/Base_FundAccount/GetPageList',
                headData: [
                    { label: "账户名称", name: "Name", width: 150, align: "left" },
                    { label: "开户银行", name: "Bank", width: 150, align: "left" },
                    { label: "账号", name: "BankCode", width: 150, align: "left" },
                    { label: "可用余额", name: "UsableBalance", width: 150, align: "left" },
                    { label: "余额", name: "Balance", width: 150, align: "left" },
                    { label: "冻结额", name: "Freezed", width: 150, align: "left" },
                    { label: "项目经理", name: "F_RealName", width: 150, align: "left" },
                    { label: "开户人", name: "Holder", width: 150, align: "left" },
                    {
                        label: "所属公司", name: "CompanyId", width: 150, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('company', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "绑定项目", name: "ProjectId", width: 300, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },
                    { label: "说明", name: "Remark", width: 100, align: "left" },
                ],
                mainId: 'ID',
                isPage: true,
                height: 294,

            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.is_verification = 1;
            param.type = type;
            param.companyId = companyId;

            if (projectId) {
                param.ProjectId = projectId;
            }
            if ($("#SearchValue").val() != "") {
                param.Bank = $("#SearchValue").val()
            }

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};