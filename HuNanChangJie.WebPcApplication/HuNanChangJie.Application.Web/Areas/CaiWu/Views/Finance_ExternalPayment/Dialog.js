﻿var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            $("#btn_Search").on("click", function () {
                page.search();
            })
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("")
                page.search();
            })
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/GetHeTongList',
                headData: [
                    { label: "收款单位", name: "danwei", width: 100, align: "left" },
                    { label: "申请单号", name: "code", width: 160, align: "left" },

                    { label: "申请时间", name: "ApplyDate", width: 160, align: "left" },

                    { label: "项目名称", name: "ProjectName", width: 100, align: "left" },
                    { label: "合同名称", name: "hetongname", width: 150, align: "left" },


                    { label: "项目经理", name: "F_RealName", width: 160, align: "left" },


                    { label: "合同金额", name: "TotalAmount", width: 100, align: "center" },
                    { label: "申请金额", name: "ApplyPaymentAmount", width: 100, align: "left", },
                    { label: "批准金额", name: "amount", width: 100, align: "left", },
                    { label: "待付金额", name: "waitpay", width: 100, align: "left" },
                    { label: "类型", name: "mode2", width: 100, align: "center" },
                    { label: "默认付款账号", name: "DefaultAccountName", width: 100, align: "center" },
                    { label: "所属公司", name: "AmountCompany", width: 100, align: "center" },
                    { label: "关联科目", name: "SubjectName", width: 100, align: "center" },
                    {
                        label: "申请日期", name: "sdate", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue != '0001-01-01 00:00:00')
                                return cellvalue;
                            return "";
                        }

                    },
                    { label: "备注", name: "Remark", width: 100, align: "center" }, 

                ],
                mainId: 'ID',
                isPage: true,
                height: 400,
                isMultiselect: true
            });
            page.search();

        },
        search: function (param) {
            param = param || {};
            if ($("#SearchValue").val() != "") {
                param.word = $("#SearchValue").val()
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};