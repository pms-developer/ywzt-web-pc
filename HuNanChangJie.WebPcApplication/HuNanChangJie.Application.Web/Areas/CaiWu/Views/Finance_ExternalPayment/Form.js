﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-05 19:09
 * 描  述：财务-对外付款单
 */
var acceptClick;
var keyValue = request('keyValue');

var applyid = request('applyid');
var cmode = request('cmode');

var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Finance_ExternalPayment";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/Audit', { keyValue: keyValue }, function (data) {
       
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/UnAudit', { keyValue: keyValue }, function (data) {
       
    });
};


var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Finance_ExternalPaymentDetails', "gridId": 'Finance_ExternalPaymentDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }

            if (applyid && cmode) {
                var param = {};
                param.id = applyid;
                var queryJson = JSON.stringify(param)

                Changjie.httpGet(top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/GetHeTongListByTask', { queryJson: queryJson }, function (res) {
                    if (res.code == 200 && res.data && res.data.length > 0) {
                        debugger;
                        var data = res.data[0], total = 0.0;
                        var rows = [];
                        var row = {};
                        row.PayeeName = data.realname;
                        row.Code = data.code;
                        row.BuMeng = data.deptid;
                        row.BaoXiaoId = data.ID;
                        row.BaoXiaoType = data.mode;
                        row.PayeeId = data.userid;
                        row.Amount = data.amount;
                        row.DefaultAccountName = data.DefaultAccountName;
                        row.BankId = data.DefaultAccountId;
                        row.ContractType = data.mode;
                        row.ContractType2 = data.mode2;
                        row.ContractName = data.hetongname;
                        row.Payee = data.Payee;
                        row.ProjectName = data.projectname || data.ProjectName;
                        row.ProjectId = data.ProjectId;
                        row.ApplyCode = data.code;
                        row.ApplyDate = data.sdate;
                        row.AccountName = data.AccountName;
                        row.BankName = data.BankName;
                        row.BankAccount = data.BankAccount;
                        row.ApplyPaymentAmount = data.ApplyPaymentAmount;
                        row.RatifyAmount = data.amount;
                        row.InvoiceAmount = data.InvioceAmount;
                        row.AccountPaidAmount = data.AccountPaidAmount;
                        if (data.ApplyPaymentAmount - data.AccountPaidAmount > 0)
                            row.PaymentAmount = row.InvoiceAmount;// data.ApplyPaymentAmount - data.AccountPaidAmount;
                        else
                            row.PaymentAmount = 0;
                        row.ContractId = data.ContractId;
                        row.PaymentApplyId = data.ApplyId;
                        var applyIds = row.PaymentApplyId+",";
                        row.ContractTypeId = data.ContractType;

                        row.AmountCompany = data.AmountCompany;
                        // row.Remark = data.Remark;

                        total += row.InvoiceAmount;
                        row.rowState = 1;
                        row.ID = Changjie.newGuid();
                        row.EditType = 1;
                        rows.push(row);
                        console.log(rows, total, "A")
                        total = Changjie.format45(total, Changjie.sysGlobalSettings.pointGlobal);

                        $("#Finance_ExternalPaymentDetails").jfGridSet("addRows", rows);
                        $("#PaymentAmount").val(total);

                        $("#autofiles").jfGridSet("clearallrow");
                        if (applyIds.length > 0) {
                            page.initAutoFileData(applyIds);
                        }
                    }
                });
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0014' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#PaymentType').mkDataItemSelect({ code: 'FKDLB' });

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            $('#PaymentCompanyId').mkDataItemSelect({ code: 'SPDW' });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#autofiles').jfGrid({
                headData: [
                    {
                        label: '创建者', name: 'CreationName', width: 100
                    },
                    {
                        label: '上传日期', name: 'CreationDate', width: 100
                    },
                    {
                        label: '文件大小', name: 'F_FileSize', width: 100
                    },
                    {
                        label: '文件类型', name: 'F_FileType', width: 100
                    },
                    {
                        label: '下载次数', name: 'F_DownloadCount', width: 100
                    },
                    {
                        label: '操作', name: 'operationTabs', width: 100,
                        formatter: function (value, coldata, rowData, rowItem, op, rowIndex, colname) {
                            if (!!coldata.F_Id) {
                                return "<a href='#' onclick=\"downloadFile2('" + coldata.F_Id + "')\"><span style='color:blue'>下载附件</span></a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"previewFile('" + coldata.F_Id + "')\"><span style='color:blue'>在线预览</span></a>";
                            }
                        }
                    }
                   
                ],
                bindTable: "Base_AnnexesFile"

            });
            $('#Finance_ExternalPaymentDetails').jfGrid({
                headData: [
                    {
                        label: '收款单位', name: 'Payee', width: 130
                    },
                    {
                        label: '合同名称', name: 'ContractName', width: 130
                    },
                    {
                        label: '合同类型', name: 'ContractType', width: 130
                    },
                    {
                        label: '项目名称', name: 'ProjectName', width: 130
                    },
                    {
                        label: '付款申请单号', name: 'ApplyCode', width: 130
                    },
                    {
                        label: '申请日期', name: 'ApplyDate', width: 130,
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: '开户名称', name: 'AccountName', width: 130, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, item, oldValue, colname, headData) {
                            }
                        }
                    },
                    {
                        label: '开户银行', name: 'BankName', width: 130, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, item, oldValue, colname, headData) {
                            }
                        }
                    },
                    {
                        label: '银行账号', name: 'BankAccount', width: 130, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, item, oldValue, colname, headData) {
                            }
                        }
                    },
                    //{
                    //    label: '申请金额', name: 'ApplyPaymentAmount', width: 130
                    //},
                    {
                        label: "申请金额", name: "ApplyPaymentAmount", width: 130, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购发票'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                page.checkApplyPaymentAmount();
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    }, {
                        label: '支付方式', name: 'PaymentType', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataItem',
                            code: 'ZFFS'
                        }
                    },
                    {
                        label: '账户名称', name: 'BankNames', width: 180, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'layer',
                            change: function (row, index, item, oldValue, colname, headData) {

                                //if (item.projectid) {
                                //    top.Changjie.alert.error("不能选择项目可用资金账户！");
                                //    return;
                                //}

                                row.BankId = item.id;
                                row.BankNames = item.name;
                                row.OpenBank = item.bank;
                                row.BankCode = item.bankcode;
                                row.Holder = item.holder;
                                row.AmountCompany = item.companyname;

                                $('#Finance_ExternalPaymentDetails').jfGridSet("updateRow", index)
                                $("#Finance_ExternalPaymentDetails").jfGridSetCellValue(index, "AmountCompany", row.AmountCompany)

                            },
                            op: {
                                width: 800,
                                height: 400,
                                listFieldInfo: [
                                    { label: "账户名称", name: "name", width: 150, align: "left" },
                                    { label: "开户银行", name: "bank", width: 150, align: "left" },
                                    { label: "账号", name: "bankcode", width: 150, align: "left" },
                                    { label: "可用余额", name: "usablebalance", width: 150, align: "left" },
                                    { label: "余额", name: "balance", width: 150, align: "left" },
                                    { label: "冻结额", name: "freezed", width: 150, align: "left" },
                                    { label: "项目经理", name: "f_realbame", width: 150, align: "left" },
                                    { label: "开户人", name: "holder", width: 150, align: "left" },
                                    {
                                        label: "所属公司", name: "companyid", width: 150, align: "left",
                                        formatterAsync: function (callback, value, row, op, $cell) {
                                            Changjie.clientdata.getAsync('company', {
                                                key: value,
                                                callback: function (_data) {
                                                    callback(_data.name);
                                                }
                                            });
                                        }
                                    },
                                    {
                                        label: "绑定项目", name: "projectid", width: 300, align: "left",
                                        formatterAsync: function (callback, value, row, op, $cell) {
                                            Changjie.clientdata.getAsync('sourceData', {
                                                code: 'BASE_XMLB',
                                                key: value,
                                                keyId: 'project_id',
                                                callback: function (_data) {
                                                    callback(_data['projectname']);
                                                }
                                            });
                                        }
                                    },
                                    { label: "说明", name: "remark", width: 100, align: "left" },
                                ],

                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: {
                                    code: 'ZJZHTK'
                                }
                            }
                        }
                    }, {
                        label: '本次付款', name: 'PaymentAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'PaymentAmount', required: '1',
                        datatype: 'float', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                page.checkPayAmount(row, index);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                page.checkPayAmount(row, rowindex);
                            }
                        }
                    },
                    {
                        label: '批准金额', name: 'RatifyAmount', width: 130
                    },
                    {
                        label: '已收票金额', name: 'InvoiceAmount', width: 130
                    },
                    {
                        label: '已付款金额', name: 'AccountPaidAmount', width: 130
                    },
                    {
                        label: '款项性质', name: 'PaymentApplyType', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataItem',
                            code: 'KXXZ'
                        }
                    },
                    
                    {
                        label: '单据默认付款账号', name: 'DefaultAccountName', width: 130
                    },


                    //{
                    //    label: '账户名称', name: 'BankId', width: 180, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    //    datatype: 'string', tabname: '付款明细'
                    //    , edit: {
                    //        type: 'select',
                    //        change: function (row, index, item, oldValue, colname, headData) {
                    //            row.OpenBank = item.bank;
                    //            row.BankCode = item.bankcode;
                    //            row.Holder = item.holder;
                    //            row.AmountCompany = item.companyname;
                    //            $('#Finance_ExternalPaymentDetails').jfGridSet("updateRow", index)
                    //            $("#Finance_ExternalPaymentDetails").jfGridSetCellValue(index, "AmountCompany", row.AmountCompany)
                    //        },
                    //        datatype: 'dataSource',
                    //        code: 'ZJZH',
                    //        op: {
                    //            value: 'id',
                    //            text: 'name',
                    //            title: 'name'
                    //        }
                    //    }
                    //},

                    {
                        label: '开户银行', name: 'OpenBank', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'

                    },
                    {
                        label: '账号名称', name: 'Holder', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'

                    },

                    {
                        label: '账号', name: 'BankCode', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'

                    },


                    {
                        label: '所属公司', name: 'AmountCompany', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                    },
                    {
                        label: '银行账号/票据号', name: 'AccountCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, item, oldValue, colname, headData) {
                            }
                        }
                    },
                  
                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_ExternalPaymentDetails",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true,
                onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectHeTong",
                        width: 1300,
                        height: 450,
                        title: "选择付款申请单",
                        url: top.$.rootUrl + "/CaiWu/Finance_ExternalPayment/Dialog",
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    var total = 0;
                                    var sum_ApplyPaymentAmount = 0;
                                    var applyIds = "";
                                    var cvv = $('#Finance_ExternalPaymentDetails').jfGridGet("rowdatas");

                                    for (var i = 0; i < data.length; i++) {
                                        var ishas = false;
                                        $.each(cvv, function (ii, ee) {
                                            //console.log(ee.PaymentApplyId)
                                            //console.log(data[i].ApplyId)

                                            if (ee.PaymentApplyId == data[i].ApplyId) {
                                                ishas = true;
                                                return false;
                                            }
                                        })

                                        if (ishas) {
                                            continue;
                                        }
                                        var row = {};
                                        row.PayeeName = data[i].realname;
                                        row.Code = data[i].code;
                                        row.BuMeng = data[i].deptid;
                                        row.BaoXiaoId = data[i].ID;
                                        row.BaoXiaoType = data[i].mode;
                                        row.PayeeId = data[i].userid;
                                        row.Amount = data[i].amount;
                                        row.DefaultAccountName = data[i].DefaultAccountName;
                                        row.BankId = data[i].DefaultAccountId;
                                        row.ContractType = data[i].mode;
                                        row.ContractType2 = data[i].mode2;
                                        row.ContractName = data[i].hetongname;
                                        row.Payee = data[i].Payee;
                                        row.ProjectName = data[i].projectname || data[i].ProjectName;
                                        row.ProjectId = data[i].ProjectId;
                                        row.ApplyCode = data[i].code;
                                        row.ApplyDate = data[i].sdate;
                                        row.AccountName = data[i].AccountName;
                                        row.BankName = data[i].BankName;
                                        row.BankAccount = data[i].BankAccount;
                                        row.ApplyPaymentAmount = data[i].ApplyPaymentAmount;
                                        row.RatifyAmount = data[i].amount;
                                        row.InvoiceAmount = data[i].InvioceAmount;
                                        row.AccountPaidAmount = data[i].AccountPaidAmount;
                                        if (data[i].ApplyPaymentAmount - data[i].AccountPaidAmount > 0)
                                            row.PaymentAmount = row.InvoiceAmount// data[i].ApplyPaymentAmount - data[i].AccountPaidAmount;
                                        else
                                            row.PaymentAmount = 0;
                                        row.ContractId = data[i].ContractId;
                                        row.PaymentApplyId = data[i].ApplyId;
                                        applyIds = applyIds + row.PaymentApplyId + ",";
                                        row.ContractTypeId = data[i].ContractType;

                                        row.AmountCompany = data[i].AmountCompany;

                                        row.Remark = data[i].Remark;
                                        sum_ApplyPaymentAmount += row.ApplyPaymentAmount;
                                        total += row.InvoiceAmount;
                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;

                                        rows.push(row);
                                    }

                                    total = Changjie.format45(total, Changjie.sysGlobalSettings.pointGlobal);

                                    $("#PaymentAmount").val(total);
                                    $("#ApplyPaymentAmount").val(parseFloat(sum_ApplyPaymentAmount).toFixed(top.Changjie.sysGlobalSettings.pointGlobal));

                                    $("#Finance_ExternalPaymentDetails").jfGridSet("addRows", rows);

                                    $("#autofiles").jfGridSet("clearallrow");
                                    if (applyIds.length > 0) {
                                        page.initAutoFileData(applyIds);
                                    }
                                }
                            });
                        }
                    });
                },
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        checkPayAmount: function (row, index) {
            var currentVale = Changjie.getFloatValue(row.PaymentAmount);
            var ratifyAmount = Changjie.getFloatValue(row.RatifyAmount);
            var isReset = false;
            if (currentVale > ratifyAmount) {
                isReset = true;
            }
            if (currentVale <= 0) {
                isReset = true;
            }
            if (isReset) {
                row.PaymentAmount = row.RatifyAmount;
                $("#Finance_ExternalPaymentDetails").jfGridSetCellValue(index, "PaymentAmount", row.RatifyAmount)
            }

        },
        checkApplyPaymentAmount: function () {

            var rowsQuantities = $('#Finance_ExternalPaymentDetails').jfGridGet('rowdatas');

            var sum_ApplyPaymentAmount = 0;

            for (var i = 0; i < rowsQuantities.length; i++) {
                if (!isNaN(rowsQuantities[i].ApplyPaymentAmount))
                    sum_ApplyPaymentAmount += Number(rowsQuantities[i].ApplyPaymentAmount);
            }

            $("#ApplyPaymentAmount").val(parseFloat(sum_ApplyPaymentAmount).toFixed(top.Changjie.sysGlobalSettings.pointGlobal));

        },
        initAutoFileData: function (autoFileInfoId) {
            autoFileInfoId = autoFileInfoId.substring(0, autoFileInfoId.length - 1);
            if (!!autoFileInfoId) {
                Changjie.httpAsyncGet(top.$.rootUrl + '/SystemModule/Annexes/GetAnnexesFilesList?infoIds=' + autoFileInfoId, function (res) {
                    if (res.code == Changjie.httpCode.success) {
                        for (var item in res.data) {
                            if (!res.data[item].F_DownloadCount) {
                                res.data[item].F_DownloadCount = 0;
                            }
                            var tokb = parseInt(res.data[item].F_FileSize) / 1024;
                            res.data[item].F_FileSize = (Math.round(tokb * 100) / 100) + "KB";
                            $('#autofiles').jfGridSet('refreshdata', res.data);
                        }
                    }
                });
            }
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                            var applyIds = "";
                            for (var i = 0; i < data[id].length; i++) {
                                debugger;
                                var item = data[id][i];
                                applyIds += item.PaymentApplyId + ",";
                            }
                           
                            if (applyIds.length > 0) {
                                page.initAutoFileData(applyIds);
                            }
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };

    //
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}

var downAutoFiles = function (fileId) {
        window.open(top.$.rootUrl + '/SystemModule/Annexes/DownAnnexesFile2?fileId=' + fileId);
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Finance_ExternalPayment"]').mkGetFormData());
    postData.strfinance_ExternalPaymentDetailsList = JSON.stringify($('#Finance_ExternalPaymentDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
