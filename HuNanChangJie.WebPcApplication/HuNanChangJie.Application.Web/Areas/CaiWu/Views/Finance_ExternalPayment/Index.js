﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-05 19:09
 * 描  述：财务-对外付款单
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');//"3bc6f353-2cbc-6bf5-a5da-34c0c20ab911";//
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var checkDataEvent = function (keyValue) {
    var checkflag = true;
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/BeforeSendingCheckData', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            var info = data.data;
            if (!!info.Success) {
                checkflag = true;
            }
            else {
                top.Changjie.alert.error(info.Message);
                checkflag = false;
            }
        }
    });
    return checkflag;
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 260, 600);

            $('#AuditStatus').mkselect({
                type: 'default',
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ]
            });

            $('#PaymentType').mkDataItemSelect({ code: 'FKDLB' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增对外付款单',
                    url: top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/GetPageList',
                headData: [
                    { label: "编号", name: "Code", width: 130, align: "center" },
                    {
                        label: "付款日期", name: "PaymentDate", width: 80, align: "center", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "付款类别", name: "PaymentType", width: 70, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FKDLB',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    }, {
                        label: "项目/合同信息", name: "TypeList", width: 350, align: "left"
                    },
                    { label: "项目经理", name: "ProjectManager", width: 60, align: "center" },
                    { label: "付款金额", name: "PaymentAmount", width: 100, align: "left" },
                    { label: "付款方式", name: "PaymentTypeList", width: 70, align: "center" },
                    { label: "收款单位", name: "PayeeList", width: 200, align: "left" },
                    { label: "付款账户", name: "PaymentZhanghuList", width: 300, align: "left" },
                    {
                        label: "经办人", name: "OperatorId", width: 60, align: "center",
                        //formatterAsync: function (callback, value, row, op, $cell) {
                        //    Changjie.clientdata.getAsync('user', {
                        //        key: value,
                        //        callback: function (_data) {
                        //            callback(_data.name);
                        //        }
                        //    });
                        //}
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 60, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }, { label: "说明", name: "Abstract", width: 200, align: "left" },
                    //{ label: "项目/合同信息", name: "TypeList", width: 300, align: "left" },
                    //{ label: "付款明细单据", name: "CodeList", width: 200, align: "left" },
                    //{
                    //    label: "付款单位", name: "PaymentCompanyId", width: 400, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('dataItem', {
                    //            key: value,
                    //            code: 'SPDW',
                    //            callback: function (_data) {
                    //                callback(_data.text);
                    //            }
                    //        });
                    //    }
                    //},
                ],
                mainId: 'ID',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
