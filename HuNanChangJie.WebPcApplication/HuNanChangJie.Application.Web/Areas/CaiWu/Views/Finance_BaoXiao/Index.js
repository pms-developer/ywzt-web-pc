﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-07 10:21
 * 描  述：报销单
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var checkJson = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 800);
            $('#AuditStatus').mkselect({
                type: 'default',
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ]
            });
            $('#BaoXiaoType').mkRadioCheckbox({
                type: 'radio',
                code: 'BXLX',
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增报销单',
                    url: top.$.rootUrl + '/CaiWu/Finance_BaoXiao/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_BaoXiao/GetPageList',
                headData: [
                    { label: "报销单号", name: "Code", width: 140, align: "left" },
                    { label: "报销金额", name: "BaoXiaoAmount", width: 100, align: "left" },
                    { label: "已付金额", name: "PayedAmount", width: 100, align: "left" },
                    {
                        label: "报销日期", name: "BaoXiaoDate", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    //{ label: "是否核销借款", name: "IsBorrow", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('dataItem', {
                    //             key: value,
                    //             code: 'SZSF',
                    //             callback: function (_data) {
                    //                 callback(_data.text);
                    //             }
                    //         });
                    //    }},
                    {
                        label: "报销类型", name: "BaoXiaoType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'BXLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    //{ label: "核销金额", name: "HeXiaoAmount", width: 100, align: "left"},
                    //{
                    //    label: "报销人", name: "BaoXiaoUserId", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('user', {
                    //            key: value,
                    //            callback: function (_data) {
                    //                callback(_data.name);
                    //            }
                    //        });
                    //    }
                    //},

                    //{ label: "报销人", name: "BaoXiaoUserId", width: 100, align: "left" },

                    {
                        label: "报销人", name: "BaoXiaoUserId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },


                    {
                        label: "部门", name: "DeparmentId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('department', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "是否有发票", name: "IsInvoice", width: 100, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'SZSF',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "项目名称", name: "ProjectID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },

                    {
                        label: "是否付款", name: "IsPay", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case true:
                                    return '<span class="label label-success" style="cursor: pointer;">已付款</span>';
                                default:
                                    return '<span class="label label-default" style="cursor: pointer;">未付款</span>';
                            }
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "摘要", name: "Abstract", width: 100, align: "left" },
                ],
                mainId: 'ID',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Finance_BaoXiao/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/CaiWu/Finance_BaoXiao/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
