﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-07 10:21
 * 描  述：报销单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid = [];
var click = "";
var tables=[];
var mainTable="Finance_BaoXiao";
var processCommitUrl=top.$.rootUrl + '/CaiWu/Finance_BaoXiao/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
                type = "edit";
                click = "1";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'Finance_BaoXiaoDetails',"gridId":'Finance_BaoXiaoDetails'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0015' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#IsBorrow').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#BaoXiaoType').mkRadioCheckbox({
                type: 'radio',
                code: 'BXLX',
            });

            $('input[name="BaoXiaoType"]').on("click",function(){
                if ($(this).val() == "公司费用") {
                    $("#ProjectIDDIV").hide()
                    $("#ToPaymentDIV").show()

                }
                else if ($(this).val() == "项目费用") {
                    $("#ProjectIDDIV").show()
                    $("#ToPaymentDIV").hide()
                }
            });

            var loginInfo = Changjie.clientdata.get(['userinfo']);

            $('#BaoXiaoUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);


            $('#DeparmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                showtop: true,

                //nodeClickComplete(dfop, $item, $option, $select, $inputText) {
                //    var tempcompany = dfop.currtentItem;
                //    while (tempcompany.parent) {
                //        tempcompany = tempcompany.parent
                //       console.log(tempcompany,119)
                
                //    }
                //    console.log(tempcompany, 111)
                //    $inputText.html($inputText.html() + "(" + tempcompany.text + ")")
                //},
            });
            $('#IsInvoice').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });

            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });


            $('#ProjectID').mkDataSourceSelect({
                code: 'BASE_XMLB',
                value: 'project_id',
                text: 'projectname',
                select: function (item) {
                    if (!!item) {
                        $("#ProjectCode").val(item.code);
                        $("#ProjectManager").mkformselectSet(item.marketingstaffid);
                    }
                }
            });

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);

                var pj = $("#ProjectID").mkselectGetEx();
                if (keyValue && click == "1") {
                    $("#ProjectManager").mkformselectSet();
                    click = "";
                } else {
                    $("#ProjectManager").mkformselectSet(pj.marketingstaffid);
                }
            }

            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"extend",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
          
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_BaoXiaoDetails').jfGrid({
                headData: [
                    {
                        label: '费用类型', name: 'SubjectId', width:200,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'string', tabname:'报销明细' 
                        , edit: {
                            init: function (a, b, c) {
                                if ($('input[name="BaoXiaoType"]:checked').val() == "公司费用") {
                                    this.code = "GSFYZCKM";
                                    b.mkDataSourceSelect({ code: "GSFYZCKM", value: 'id', text: 'fullname' });
                                }
                                else {
                                    this.code = "XMFYZCKM";
                                    b.mkDataSourceSelect({ code: "XMFYZCKM", value: 'id', text: 'fullname' });
                                }
                            },
                            type:'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                                //var gridName = "Finance_BaoXiaoDetails";
                                //row.SubjectName
                                row.SubjectName = item.name
                                row.SubjectId = item.id
                                //setCellValue("SubjectName", row, index, gridName, item.name);

                                //$('#Finance_BaoXiaoDetails').jfGridSet("updateRow",index)
                                //console.log(a)
                                //console.log(row)

                                //console.log(index)

                                //console.log(item)


                                //console.log(oldValue)

                                //console.log(colname)
                                //console.log(headData)
                                ////console.log(c)
                                //console.log(this)
                            },
                            datatype: 'dataSource',
                            code:'XMFYZCKM',
                            op:{
                                value: 'id',
                                text:'name',
                                title:'name'
                            }
                        }
                    },
                    {
                        label: '金额', name: 'Amount', width:100,cellStyle: { 'text-align': 'left' },aggtype:'sum',aggcolumn:'BaoXiaoAmount',required:'1',
                        datatype:'float', tabname:'报销明细' ,inlineOperation:{to:[{"t1":"Amount","t2":"InvoiceType","to":"TaxAmount","type":"mul","toTargets":null},{"t1":"Amount","t2":"TaxAmount","to":"TaxExclusive","type":"sub","toTargets":null}],isFirst:true}
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '发票类型', name: 'InvoiceType', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'报销明细' ,inlineOperation:{to:[{"t1":"Amount","t2":"InvoiceType","to":"TaxAmount","type":"mul","toTargets":null},{"t1":"Amount","t2":"TaxAmount","to":"TaxExclusive","type":"sub","toTargets":null}],isFirst:false}
                        ,edit:{
                            type:'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                                //row.TaxRate = item
                                //console.log(item)
                                //console.log(row.InvoiceType)
                                row.TaxRate = row.InvoiceType;
                                $('#Finance_BaoXiaoDetails').jfGridSet("updateRow", index)
                            },
                            datatype: 'dataItem',
                            code:'FPLX'
                        }
                    },
                    {
                        label: '税率', name: 'TaxRate', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'报销明细'                     },
                    {
                        label: '税额', name: 'TaxAmount', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'float', tabname:'报销明细' ,inlineOperation:{to:[{"t1":"Amount","t2":"TaxAmount","to":"TaxExclusive","type":"sub","toTargets":null}],isFirst:false}
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '不含税金额', name: 'TaxExclusive', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'float', tabname:'报销明细' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '特殊说明', name: 'Specical', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'报销明细' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '行备注', name: 'Remark', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'报销明细' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                ],
                mainId:"ID",
                bindTable:"Finance_BaoXiaoDetails",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid(); 
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_BaoXiao/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    var setCellValue = function (cellName, row, index, gridName, value) {
        var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
        var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
        row[cellName] = value;
        row.EditType = 2;
        $cell.html(value);
        $edit.val(value);
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();

        if (postData == false) return false;

        var tempprojectid = $("#ProjectID").mkselectGet();
        if ($('input[name="BaoXiaoType"]:checked').val() == "项目费用" && !tempprojectid) {
            top.Changjie.alert.error("报销类型为项目费用,请选择项目名称");
            return false;
        }

        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_BaoXiao/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="Finance_BaoXiao"]').mkGetFormData());
        postData.strfinance_BaoXiaoDetailsList = JSON.stringify($('#Finance_BaoXiaoDetails').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
