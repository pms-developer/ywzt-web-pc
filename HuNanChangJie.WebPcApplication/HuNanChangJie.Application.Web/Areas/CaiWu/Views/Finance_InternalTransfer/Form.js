﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-02 20:14
 * 描  述：内部转账
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Finance_InternalTransfer";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_InternalTransfer/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {

    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InternalTransfer/Audit', { keyValue: keyValue }, function (data) {
       
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InternalTransfer/UnAudit', { keyValue: keyValue }, function (data) {
       
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Finance_InternalTransferDetails', "gridId": 'Finance_InternalTransferDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0013' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#JingBanRen').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            if (projectId)
                $("#ProjectID").val(projectId);

            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_InternalTransferDetails').jfGrid({
                headData: [
                    //{
                    //    label: '转出账户', name: 'TransferredAccount', width: 250, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    //    datatype: 'string', tabname: '转账明细'
                    //    , edit: {
                    //        type: 'select',
                    //        change: function (row, index, item, oldValue, colname, headData) {
                    //            row.TransferredAccountProject = item.projectname;
                    //            setCellValue("TransferredAccountProject", row, 0, "Finance_InternalTransferDetails", row.TransferredAccountProject);
                    //        },
                    //        datatype: 'dataSource',
                    //        code: 'ZJZH',
                    //        op: {
                    //            value: 'id',
                    //            text: 'name',
                    //            title: 'name'
                    //        }
                    //    }
                    //},

                    {
                        label: '转出账户', name: 'TransferredAccountBankNames', width: 250, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '转账明细'
                        , edit: {
                            type: 'layer',
                            change: function (row, index, item, oldValue, colname, headData) {
                                row.TransferredAccount = item.id;
                                row.TransferredAccountBankNames = item.name;
                                row.TransferredAccountProject = item.projectname;
                                $('#Finance_InternalTransferDetails').jfGridSet("updateRow", index)
                            },
                            op: {
                                width: 900,
                                height: 400,
                                listFieldInfo: [
                                    { label: "账户名称", name: "name", width: 350, align: "left" },
                                    { label: "开户银行", name: "bank", width: 150, align: "left" },
                                    { label: "账号", name: "bankcode", width: 200, align: "left" },
                                    { label: "可用余额", name: "usablebalance", width: 150, align: "left" },
                                    { label: "余额", name: "balance", width: 150, align: "left" },
                                    { label: "冻结额", name: "freezed", width: 150, align: "left" },
                                    { label: "项目经理", name: "f_realname", width: 150, align: "left" },
                                    { label: "开户人", name: "holder", width: 150, align: "left" },
                                    {
                                        label: "所属公司", name: "companyid", width: 150, align: "left",
                                        formatterAsync: function (callback, value, row, op, $cell) {
                                            Changjie.clientdata.getAsync('company', {
                                                key: value,
                                                callback: function (_data) {
                                                    callback(_data.name);
                                                }
                                            });
                                        }
                                    },
                                ],
                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: {
                                    code: 'ZJZH'
                                }
                            }
                        }
                    },

                    {
                        label: '转出账户关联项目', name: 'TransferredAccountProject', width: 180, cellStyle: { 'text-align': 'left' }, aggtype: 'normal',
                        datatype: 'string', tabname: '转账明细'
                    },

                    {
                        label: '转入账户', name: 'BankNames', width: 250, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '转账明细'
                        , edit: {
                            type: 'layer',
                            change: function (row, index, item, oldValue, colname, headData) {
                                row.TransferAccount = item.id;
                                row.BankNames = item.name;
                                row.TransferAccountProject = item.projectname;
                                $('#Finance_InternalTransferDetails').jfGridSet("updateRow", index)
                            },
                            op: {
                                width: 900,
                                height: 400,
                                listFieldInfo: [
                                    { label: "账户名称", name: "name", width: 350, align: "left" },
                                    { label: "开户银行", name: "bank", width: 150, align: "left" },
                                    { label: "账号", name: "bankcode", width:200, align: "left" },
                                    { label: "可用余额", name: "usablebalance", width: 150, align: "left" },
                                    { label: "余额", name: "balance", width: 150, align: "left" },
                                    { label: "冻结额", name: "freezed", width: 150, align: "left" },
                                    { label: "项目经理", name: "f_realname", width: 150, align: "left" },
                                    { label: "开户人", name: "holder", width: 150, align: "left" },
                                    {
                                        label: "所属公司", name: "companyid", width: 150, align: "left",
                                        formatterAsync: function (callback, value, row, op, $cell) {
                                            Changjie.clientdata.getAsync('company', {
                                                key: value,
                                                callback: function (_data) {
                                                    callback(_data.name);
                                                }
                                            });
                                        }
                                    },
                                ],
                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: {
                                    code: 'ZJZH'
                                }
                            }
                        }
                    },



                    //{
                    //    label: '转入账户', name: 'TransferAccount', width: 250, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    //    datatype: 'string', tabname: '转账明细'
                    //    , edit: {
                    //        type: 'select',
                    //        change: function (row, index, item, oldValue, colname, headData) {
                    //            row.TransferAccountProject = item.projectname;

                    //            setCellValue("TransferAccountProject", row, 0, "Finance_InternalTransferDetails", row.TransferAccountProject);
                    //        },
                    //        datatype: 'dataSource',
                    //        code: 'ZJZH',
                    //        op: {
                    //            value: 'id',
                    //            text: 'name',
                    //            title: 'name'
                    //        }
                    //    }
                    //},
                    {
                        label: '转入账户关联项目', name: 'TransferAccountProject', width: 180, cellStyle: { 'text-align': 'left' }, aggtype: 'normal',
                        datatype: 'string', tabname: '转账明细'
                    },
                    {
                        label: '转账金额', name: 'Amount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'TransferAmount', required: '1',
                        datatype: 'float', tabname: '转账明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '转账明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_InternalTransferDetails",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_InternalTransfer/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_InternalTransfer/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}

var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;
    row.EditType = 2;
    $cell.html(value);
    $edit.val(value);
};
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Finance_InternalTransfer"]').mkGetFormData());
    postData.strfinance_InternalTransferDetailsList = JSON.stringify($('#Finance_InternalTransferDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
