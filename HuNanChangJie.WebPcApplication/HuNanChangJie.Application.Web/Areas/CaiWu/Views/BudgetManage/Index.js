﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2022-02-08 13:48
 * 描  述：预算管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#ProjectName').mkDataSourceSelect({ code: 'BASE_XMLB',value: 'f_companyid',text: 'f_fullname' });
            $('#Department_Id').mkDataSourceSelect({ code: 'dwbm',value: 'f_departmentid',text: 'f_fullname' });
            $('#CompanyName').mkDataSourceSelect({ code: 'gsmc',value: 'f_departmentid',text: 'f_fullname' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/CaiWu/BudgetManage/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/BudgetManage/GetPageList',
                headData: [
                    { label: "备注", name: "Remark", width: 100, align: "left"},
                    { label: "预算总金额", name: "TotalPrice", width: 100, align: "left"},
                    { label: "预算月份", name: "Month", width: 100, align: "left"},
                    { label: "店铺", name: "ProjectName", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'BASE_XMLB',
                                 key: value,
                                 keyId: 'f_companyid',
                                 callback: function (_data) {
                                     callback(_data['f_fullname']);
                                 }
                             });
                        }},
                    { label: "部门", name: "Department_Id", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'dwbm',
                                 key: value,
                                 keyId: 'f_departmentid',
                                 callback: function (_data) {
                                     callback(_data['f_fullname']);
                                 }
                             });
                        }},
                    { label: "公司", name: "CompanyName", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('custmerData', {
                                 url: '/SystemModule/DataSource/GetDataTable?code=' + 'gsmc',
                                 key: value,
                                 keyId: 'f_departmentid',
                                 callback: function (_data) {
                                     callback(_data['f_fullname']);
                                 }
                             });
                        }},
                    { label: "预算年份", name: "Year", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/BudgetManage/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/CaiWu/BudgetManage/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
