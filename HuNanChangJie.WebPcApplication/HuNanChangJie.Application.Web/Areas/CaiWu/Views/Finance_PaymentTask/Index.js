﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-05 19:09
 * 描  述：财务-对外付款单
 */
var refreshGirdData;
var formId = request("formId");
var checkJson = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 300, 400);
            //$('#PaymentType').mkDataItemSelect({ code: 'FKDLB' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            $('#PayCompanyId').mkCompanySelect({});
            $('#PayModel').mkDataItemSelect({ code: 'FKMS' });
            // 新增
            $('#details').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ContractId');
                var mode = $('#gridtable').jfGridValue('mode');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: '明细',
                        url: top.$.rootUrl + '/CaiWu/Finance_PaymentTask/Dialog?id=' + keyValue + "&mode=" + mode,
                        width: 800,
                        height: 600,
                        isShowConfirmBtn: false,
                    });
                }
            });
            $('#pay').on('click', function () {
                var applyId = $('#gridtable').jfGridValue('ApplyId');
                var mode = $('#gridtable').jfGridValue('mode');
                var waitpay = $('#gridtable').jfGridValue('waitpay');
                if (waitpay <= 0) {
                    top.Changjie.alert.warning("该付款单已结清！");
                    return;
                }
                if (top.Changjie.checkrow(applyId)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: '付款',
                        url: top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/Form?applyid=' + applyId + "&cmode=" + mode,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {


            //$('#Status').mkDataItemSelect({ code: 'ZFZT' });
           
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_PaymentTask/GetPageList',
                headData: [
                    { label: "收款单位", name: "danwei", width: 200, align: "left" },
                    { label: "申请单号", name: "code", width: 160, align: "left" },
                    { label: "项目名称", name: "ProjectName", width: 200, align: "left" },
                    { label: "合同名称", name: "hetongname", width: 200, align: "left" },
                    {
                        label: "付款模式", name: "PayModel", width: 100, align: "center",
                    },
                    {
                        label: "付款单位", name: "PayCompanyName", width: 100, align: "center",
                    },
                    //{ label: "合同金额", name: "TotalAmount", width: 100, align: "center" },
                    //{ label: "申请金额", name: "ApplyPaymentAmount", width: 100, align: "left", },
                    { label: "批准金额", name: "amount", width: 100, align: "left", },
                    { label: "已付金额", name: "payed", width: 100, align: "left", },
                    {
                        label: "待付金额", name: "waitpay", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            if (cellvalue > 0) {
                                return cellvalue;
                            }
                            else {
                                return '<span class="label label-success" style="cursor: pointer;">已结清</span>';
                            }
                        }
                    },
                    {
                        label: "是否打印", name: "printcount", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            if (cellvalue > 0) {
                                return '<span class="label label-success" style="cursor: pointer;">已打印</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">未打印</span>';
                            }
                        }
                    },
                    { label: "类型", name: "mode2", width: 80, align: "center" },
                    //{ label: "默认付款账号", name: "DefaultAccountName", width: 100, align: "center" },
                    { label: "关联科目", name: "SubjectName", width: 100, align: "center" },
                    {
                        label: "申请日期", name: "sdate", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue != '0001-01-01 00:00:00')
                                return cellvalue;
                            return "";
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true,
            });
            page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            if ($("#status1").get(0).checked) {
                param.paystatus = 0
            }
            else if ($("#status2").get(0).checked) {
                param.paystatus = 1
            }
            else if ($("#status3").get(0).checked) {
                param.paystatus = -1
            }
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
