﻿var acceptClick;

var id = request('id');
var mode = request('mode');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {

            var param = {};
            param.id = id;
            param.mode = mode == "分包合同" ? 0 : 1;
            var queryJson = JSON.stringify(param)
            //console.log(queryJson)
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_ExternalPayment/GetPageListByTask?queryJson=' + queryJson,
                headData: [
                    { label: "编号", name: "Code", width: 180, align: "center" },
                    {
                        label: "付款类别", name: "PaymentType", width: 100, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FKDLB',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "经办人", name: "OperatorId", width: 100, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "付款日期", name: "PaymentDate", width: 140, align: "center", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "付款金额", name: "PaymentAmount", width: 140, align: "center" },

                    {
                        label: "审核状态", name: "AuditStatus", width: 120, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true,
            });
            page.search();
            
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};