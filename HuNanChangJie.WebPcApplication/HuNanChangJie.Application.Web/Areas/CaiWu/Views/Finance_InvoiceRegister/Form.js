﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-29 09:58
 * 描  述：开票登记
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var click = "";
var mainTable = "Finance_InvoiceRegister";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_InvoiceRegister/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var formdata = null;
var isfirstload = true;
var ocrCallBack;
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InvoiceRegister/Audit', { keyValue: keyValue }, function (data) {
       
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InvoiceRegister/UnAudit', { keyValue: keyValue }, function (data) {
       
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
                click = "1";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Finance_InvoiceRegister_Proceeds', "gridId": 'Finance_InvoiceRegister_Proceeds' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }

            setTimeout(function () {
                isfirstload = false;
            }, 5000)
        },
        initrelationdata: function () {
            var selectData = $("#ProjectID").mkselectGetEx();

            if (selectData) {

                $("#a").val(selectData.code)
                $("#ProjectName").val(selectData.projectname)

                if (keyValue && click == "1") {
                    $("#ProjectManager").mkformselectSet();
                    click = "";
                } else {
                    $("#ProjectManager").mkformselectSet(selectData.marketingstaffid);
                }

            } else {
                $("#a").val("");
                $("#ProjectName").val("")
            }
        },
        calcrateamount: function () {

            //if ($("#InvoiceAmount").val() != "" && $("#TaxRate").val() != "") {

            //    $("#TaxAmount").val($("#InvoiceAmount").val().replace(/,/ig, "") * $("#TaxRate").val())
            //}

            var taxRate = $("#TaxRate").val();
            //金额
            var tempamount = $("#InvoiceAmount").val() || 0;

            if (taxRate && tempamount) {
                var rate = parseFloat(taxRate) / 100;

                //税额 （税额公式为: 金额 / (1 + 税率) * 税率）
                var taxAmount = (tempamount / (1 + rate) * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal2)

                $("#TaxAmount").val(taxAmount || 0);
            }
            else {
                $("#TaxAmount").val(0)
            }
        },
        initratedata: function () {
            var selectData = $("#InvoiceType").mkselectGetEx();
            //var selectData = $("#ProjectID").mkselectGetEx();

            if (selectData) {
                $("#TaxRate").val(parseFloat(selectData.id) * 100);
                //$("#TaxRage").val(selectData.id);
                page.calcrateamount();

            } else {
                $("#TaxRate").val(0)
                $("#TaxAmount").val(0)
            }
        },

        initcontractdata: function () {
            var selectData = $("#ProjectContractId").mkselectGetEx();
            //var selectData = $("#ProjectID").mkselectGetEx();
            if (selectData) {
                $("#b").val(selectData.Code)
                $("#c").val(selectData.TotalAmount)
                $("#InvoiceCompany").mkselectSet(selectData.Jia)

                if (isfirstload && type == "edit") {

                }
                else {
                    var sql = " ProjectContractId='" + selectData.ID + "' ";

                    Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'HKDLB', strWhere: sql }, function (res) {
                        //console.log(res)
                        var queryData = [];
                        if (res.code == 200) {
                            $.each(res.data, function (id, item) {
                                var temp = {};
                                temp.ID = Changjie.newGuid();
                                temp.SortCode = 1;
                                temp.rowState = 1;
                                temp.EditType = 1;
                                temp.Code = item.code;
                                temp.ReceivablesDate = item.receivablesdate;
                                temp.PreceedsAmount = item.preceedsamount;
                                temp.WrittenOff = item.writtenoff;
                                temp.UnWrittenOff = item.unwrittenoff;
                                temp.Fk_ProceedsId = item.id;

                                queryData.push(temp)
                            });
                        }

                        $('#Finance_InvoiceRegister_Proceeds').jfGridSet('refreshdata', queryData);

                    })
                }
            } else {
                $("#b").val("")
                $("#c").val("")
            }

        },
        bind: function () {
            $('#ApplyUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#ProjectMode').mkDataItemSelect({ code: 'ProjectMode' });
            $("#ProjectContractId").mkselect({});
            $("#invoiceocr").on("click", function () {
                Changjie.layerForm({
                    id: 'autoinvoiceocr',
                    title: '发票识别',
                    url: top.$.rootUrl + '/SystemModule/Ocr/InvoiceOcr',
                    width: 1000,
                    height: 700,
                    callBack: function (id) {
                        return top[id].acceptClick(ocrCallBack);
                    }
                });
            });
            var loginInfo = Changjie.clientdata.get(['userinfo']);

            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });


            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);
                page.initrelationdata();

                var projectEx = $("#ProjectID").mkselectGetEx();
                if (projectEx) {
                    $('#ProjectMode').mkselectSet(projectEx.mode);


                    Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectContract/GetPageList', { queryJson: JSON.stringify({ "ProjectId": projectEx.project_id, "AuditStatus": "2" }), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                        $("#ProjectContractId").mkselectRefresh({
                            value: "ID",
                            text: "Name",
                            title: "Code",
                            data: data.rows
                        })

                        if (formdata != null && type == "edit") {
                            if (formdata.Finance_InvoiceRegister && formdata.Finance_InvoiceRegister.ProjectContractId) {
                                $("#ProjectContractId").mkselectSet(formdata.Finance_InvoiceRegister.ProjectContractId);
                                formdata = null;
                            }
                        }
                    });
                }
            }

            $("#ProjectID").on('change', function () {
                page.initrelationdata();

                var projectEx = $("#ProjectID").mkselectGetEx();
                if (projectEx) {
                    $('#ProjectMode').mkselectSet(projectEx.mode)

                    Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectContract/GetPageList', { queryJson: JSON.stringify({ "ProjectId": projectEx.project_id, "AuditStatus": "2" }), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                        $("#ProjectContractId").mkselectRefresh({
                            value: "ID",
                            text: "Name",
                            title: "Code",
                            data: data.rows
                        })

                        if (formdata != null && type == "edit") {
                            if (formdata.Finance_InvoiceRegister && formdata.Finance_InvoiceRegister.ProjectContractId) {
                                //console.log(formdata.Finance_InvoiceRegister.ProjectContractId)
                                $("#ProjectContractId").mkselectSet(formdata.Finance_InvoiceRegister.ProjectContractId);
                                formdata = null;
                            }
                        }
                    });
                }
            })


            $("#ProjectContractId").on("change", function () {
                page.initcontractdata();
            })

            $('#InvoiceType').mkDataItemSelect({ code: 'FPLX' });

            $("#InvoiceType").on('change', function () {
                page.initratedata();
            })

            $("#InvoiceAmount").on("input propertychange", function (e) {
                page.initratedata();
            })

            //开票申请  金额 InvoiceAmount  单位 InvoiceCompany
            $('#FinanceInvoiceApplyId').mkDataSourceSelect({ code: 'KPSQDLB', value: 'id', text: 'title' });
            //项目名称：ProjectID 项目编号：a 项目经理：ProjectManager 工程合同：ProjectContractId 
            //开票单位：MakeInvoiceCompanyId 开票金额：InvoiceAmount 开票申请人：ApplyUserId

            $("#FinanceInvoiceApplyId").on("change", function () {
                var financeInvoiceApplyIdEx = $("#FinanceInvoiceApplyId").mkselectGetEx();
                if (financeInvoiceApplyIdEx) {
                    $('#ProjectID').mkselectSet(financeInvoiceApplyIdEx.projectid);
                    var projectEx = $("#ProjectID").mkselectGetEx();
                    page.initrelationdata();

                    $('#MakeInvoiceCompanyId').mkCompanySelect({}).mkselectSet(financeInvoiceApplyIdEx.invoicecompany);
                    $('#InvoiceAmount').val(financeInvoiceApplyIdEx.invoiceamount);

                    $('#ApplyUserId').mkformselect({
                        layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                        layerUrlW: 400,
                        layerUrlH: 300,
                        dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
                    }).mkformselectSet(financeInvoiceApplyIdEx.applyuserid);

                    if (projectEx) {
                        $('#ProjectMode').mkselectSet(projectEx.mode)
                        Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectContract/GetPageList', { queryJson: JSON.stringify({ "ProjectId": projectEx.project_id, "AuditStatus": "2" }), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                            $("#ProjectContractId").mkselectRefresh({
                                value: "ID",
                                text: "Name",
                                title: "Code",
                                data: data.rows
                            })

                            if (formdata != null && type == "edit") {
                                if (formdata.Finance_InvoiceRegister && formdata.Finance_InvoiceRegister.ProjectContractId) {
                                    $("#ProjectContractId").mkselectSet(formdata.Finance_InvoiceRegister.ProjectContractId);
                                    formdata = null;
                                }
                            }
                        });
                    }
                }

            })


            $('#MakeInvoiceCompanyId').mkCompanySelect({});

            //$('#ProjectContractId').mkDataSourceSelect({ code: 'CW-HTLB',value: 'id',text: 'name' });
           


            $('#MakeInvoiceUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            if (type == "add") {
                $('#MakeInvoiceUserId').mkformselectSet(loginInfo.userId)
            }

            $('#InvoiceCompany').mkDataSourceSelect({ code: 'KHLB', value: 'id', text: 'fullname' });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_InvoiceRegister_Proceeds').jfGrid({
                headData: [
                    {
                        label: '收款单号', name: 'Code', width: 150, cellStyle: { 'text-align': 'left' },
                        datatype: 'string', tabname: '收款信息'
                    },
                    {
                        label: '收款日期', name: 'ReceivablesDate', width: 90, cellStyle: { 'text-align': 'left' },
                        datatype: 'string', tabname: '收款信息', formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    {
                        label: '收款金额', name: 'PreceedsAmount', width: 100, cellStyle: { 'text-align': 'left' },
                        datatype: 'string', tabname: '收款信息'
                    },
                    {
                        label: '已核销金额', name: 'WrittenOff', width: 100, cellStyle: { 'text-align': 'left' },
                        datatype: 'string', tabname: '收款信息'
                    },
                    {
                        label: '本次核销金额', name: 'WriteOffAmount', width: 130, cellStyle: { 'text-align': 'left' }, required: '0',
                        datatype: 'float', tabname: '收款信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                //console.log(row)
                                //console.log(row.WriteOffAmount)
                                //console.log(row.UnWrittenOff)

                                if (row.UnWrittenOff == null)
                                    row.UnWrittenOff = 0;
                                if (row.WriteOffAmount > row.PreceedsAmount - row.WrittenOff) {
                                    top.Changjie.alert.error("本次核销金额不能大于可核销金额")
                                    return false;
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_InvoiceRegister_Proceeds",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false,
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_InvoiceRegister/GetformInfoList?keyValue=' + keyValue, function (data) {
                    formdata = data;
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }

                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_InvoiceRegister/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();

    ocrCallBack = function (data) {
        if (data) {
            var maindata = data.maindata;
            var subdata = data.subdata;
            var rate = 0;
            if (subdata) {
                rate = top.Changjie.extractTaxTateNum2(subdata[0].tax_rate);
                if (rate == 0) {
                    $("#TaxRate").val(rate);
                }
                else {
                    $("#TaxRate").val(eval(rate / 100));
                }
            }
            if (maindata) {

                $("#InvoiceCode").val(maindata.number);
                var orcinvoiceType = maindata.invoice_type;
                if (orcinvoiceType) {
                    var invoiceTypeDatas = $("#InvoiceType").mkselectGetAll();
                    var invoiceTypeId = "";
                    if (invoiceTypeDatas) {
                        for (var _i in invoiceTypeDatas) {
                            var item = invoiceTypeDatas[_i];
                            var text = item.text;
                            if (text.indexOf("电子") > -1) {//增值税电子普通发票
                                var itemRate = top.Changjie.extractTaxTateNum1(text);
                                if (itemRate == rate) {
                                    invoiceTypeId = item.id;
                                    break;
                                }
                            }
                            else if (text.indexOf("普通") > -1) {//增值税普通发票
                                var itemRate = top.Changjie.extractTaxTateNum1(text);
                                if (itemRate == rate) {
                                    invoiceTypeId = item.id;
                                    break;
                                }
                            }
                            else if (text.indexOf("专用") > -1) {//增值税专用发票
                                var itemRate = top.Changjie.extractTaxTateNum1(text);
                                if (itemRate == rate) {
                                    invoiceTypeId = item.id;
                                    break;
                                }
                            }
                        }
                    }
                    if (!!invoiceTypeId == false) {
                        Changjie.alert.error('发票类型及税率匹配失败，请人工选择');
                    }
                    else {
                        $("#InvoiceType").mkselectSet(invoiceTypeId);
                    }
                }
                function SetCompany(selectid, value, typename) {
                    if (value) {
                        var $this = $("#" + selectid);
                        var datas = $this.mkselectGetAll();
                        var ismapping = false;
                        if (datas) {
                            for (var _i in datas) {
                                var item = datas[_i];
                                if (item.text == value) {
                                    $this.mkselectSet(item.id);
                                    ismapping = true;
                                    break;
                                }
                            }
                        }
                        if (!ismapping) {
                            Changjie.alert.error(typename + '匹配失败，请人工选择');
                        }
                    }
                    else {
                        Changjie.alert.error(typename + '匹配失败，请人工选择');
                    }
                }

                $("#InvoiceAmount").val(maindata.total);
                SetCompany("MakeInvoiceCompanyId", maindata.seller_name, "开票单位");

                var subtotal_tax = top.Changjie.format45(maindata.subtotal_tax, Changjie.sysGlobalSettings.pointGlobal2);
                $("#TaxAmount").val(subtotal_tax);
                $("#MakeInvoiceDate").val(maindata.issue_date);
                SetCompany("InvoiceCompany", maindata.seller_name, "收票单位");
                $("#BankId").val(maindata.buyer_bank);
            }
        }
        return true;
    };
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Finance_InvoiceRegister"]').mkGetFormData());
    postData.strfinance_InvoiceRegister_ProceedsList = JSON.stringify($('#Finance_InvoiceRegister_Proceeds').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
