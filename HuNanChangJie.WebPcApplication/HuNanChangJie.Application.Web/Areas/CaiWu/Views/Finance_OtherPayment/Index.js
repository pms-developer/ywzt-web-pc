﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-16 18:32
 * 描  述：其它付款
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_OtherPayment/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData();
            //Changjie.clientdata.sourceData.load("DZDJ");
        }
        else
            top.Changjie.alert.error(data.info);
    });
};

var checkDataEvent = function (keyValue) {
    var checkflag = true;
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_OtherPayment/BeforeSendingCheckData', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            var info = data.data;
            if (!!info.Success) {
                checkflag = true;
            }
            else {
                top.Changjie.alert.error(info.Message);
                checkflag = false;
            }
        }
    });
    return checkflag;
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_OtherPayment/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData();
            //Changjie.clientdata.sourceData.load("DZDJ");
        }
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;

                page.search(queryJson);
            }, 220, 400);
            //$('#AuditStatus').mkRadioCheckbox({
            //    type: 'checkbox',
            //    code: 'AuditStatus',
            //});
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增其它付款',
                    url: top.$.rootUrl + '/CaiWu/Finance_OtherPayment/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_OtherPayment/GetPageList',
                headData: [
                    { label: "付款单号", name: "Code", width: 160, align: "left"},
                    {
                        label: "对方单位", name: "CustomerName", width: 200, align: "left"
                    },
                    {
                        label: "付款日期", name: "PaymentDate", width: 80, align: "center", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "款项类型", name: "FundType", width: 100, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FKKXLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    }, { label: "付款金额", name: "PaymentAmount", width: 100, align: "center" },
                    {
                        label: "资金账户", name: "BankId", width: 180, align: "left"},
                    {
                        label: "支出科目", name: "ProjectSubjectId", width: 200, align: "left",
                       },
                    //{ label: "汇票", name: "HuiPiaoId", width: 100, align: "left"},
                    //{ label: "本票", name: "BenPiaoId", width: 100, align: "left"},
                    //{ label: "支票", name: "ZhiPiaoId", width: 100, align: "left"},
                    
                    //{ label: "支付方式", name: "PaymentType", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('dataItem', {
                    //             key: value,
                    //             code: 'ZFFS',
                    //             callback: function (_data) {
                    //                 callback(_data.text);
                    //             }
                    //         });
                    //    }},
                    { label: "经办人", name: "OperatorId", width: 100, align: "left",
                       },
                    {
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",

                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }, { label: "摘要", name: "Remark", width: 100, align: "left" },
                ],
                mainId:'ID',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }

            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Finance_OtherPayment/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '其它付款',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/CaiWu/Finance_OtherPayment/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
