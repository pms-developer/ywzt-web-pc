﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-16 18:32
 * 描  述：其它付款
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {

            $("#btn_Search").on("click", function () {
                page.search();
            })
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("")
                page.search();
            })


            //// 刷新
            //$('#refresh').on('click', function () {
            //    location.reload();
            //});

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_OtherPayment/GetPageList',
                headData: [
                    { label: "付款单号", name: "Code", width: 160, align: "left" },
                    {
                        label: "对方单位", name: "CustomerId", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'FKDWLB',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    {
                        label: "付款日期", name: "PaymentDate", width: 80, align: "center", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "款项类型", name: "FundType", width: 100, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FKKXLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    }, { label: "付款金额", name: "PaymentAmount", width: 100, align: "center" },
                    {
                        label: "资金账户", name: "BankId", width: 180, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'ZJZH',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    {
                        label: "支出科目", name: "ProjectSubjectId", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'ZCKM',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['fullname']);
                                }
                            });
                        }
                    },

                    {
                        label: "经办人", name: "OperatorId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                ],
                mainId: 'ID',
                isPage: true,
                height: 362,
                isMultiselect: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.is_verification = 1;
            if (projectId) {
                param.ProjectID = projectId;
            }
            if ($("#SearchValue").val() != "") {
                param.Code = $("#SearchValue").val()
            }

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};