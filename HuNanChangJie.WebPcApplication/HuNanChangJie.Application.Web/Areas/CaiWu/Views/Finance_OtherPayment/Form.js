﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-16 18:32
 * 描  述：其它付款
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var click = "";
var tables = [];
var mainTable = "Finance_OtherPayment";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_OtherPayment/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_OtherPayment/Audit', { keyValue: keyValue }, function (data) {
      
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_OtherPayment/UnAudit', { keyValue: keyValue }, function (data) {
      
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
                click = "1";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0020' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#CustomerId').mkDataSourceSelect({ code: 'FKDWLB', value: 'id', text: 'name' });
            // $('#BankId').mkDataSourceSelect({ code: 'ZjzhNoProject', value: 'id', text: 'name' });



            $("#BankIdName").on("click", function () {
                top.Changjie.layerForm({
                    id: "selectContract",
                    title: "选择账户",
                    url: top.$.rootUrl + '/CaiWu/Base_FundAccount/Dialog?type=ZjzhNoProject',
                    width: 800,
                    hegiht: 294,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (data) {
                                $("#BankIdName").val(data.Bank);
                                $("#BankId").val(data.ID);
                            }
                        });
                    }
                });
            });


            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });


            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#is_verification').mkRadioCheckbox({
                type: 'radio',
                code: 'SZSF',
                default: '1'
            });


            $("#ProjectID").on("change", function () {
                var pj = $("#ProjectID").mkselectGetEx();
                if (keyValue && click == "1") {
                    $("#ProjectManager").mkformselectSet();
                    click = "";
                } else {
                    $("#ProjectManager").mkformselectSet(pj.marketingstaffid);
                }
            });

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);

                var pj = $("#ProjectID").mkselectGetEx();

                if (keyValue && click == "1") {
                    $("#ProjectManager").mkformselectSet();
                    click = "";
                } else {
                    $("#ProjectManager").mkformselectSet(pj.marketingstaffid);
                }
            }


            $('#FundType').mkDataItemSelect({ code: 'FKKXLX' });
            $('#ProjectSubjectId').mkDataSourceSelect({ code: 'ZCKM', value: 'id', text: 'fullname' });
            $('#PaymentType').mkRadioCheckbox({
                type: 'radio',
                code: 'ZFFS',
            });

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);




            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "newinstance",
                isShowAttachment: true,
                isShowWorkflow: true,
            });

            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#AuditStatus').mkRadioCheckbox({
                type: 'checkbox',
                code: 'AuditStatus',
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_OtherPayment/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_OtherPayment/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
