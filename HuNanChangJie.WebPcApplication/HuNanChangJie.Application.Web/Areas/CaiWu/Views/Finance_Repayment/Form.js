﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-21 15:38
 * 描  述：还款单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="Finance_Repayment";
var processCommitUrl=top.$.rootUrl + '/CaiWu/Finance_Repayment/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_Repayment/Audit', { keyValue: keyValue }, function (data) {
      
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_Repayment/UnAudit', { keyValue: keyValue }, function (data) {
       
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'Finance_RepaymentDetails',"gridId":'Finance_RepaymentDetails'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLBS', value: 'project_id', text: 'projectname' });
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0023' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $("#ProjectID").on("change", function () {
                var pj = $("#ProjectID").mkselectGetEx();
                if (pj) {
                    $("#ProjectName").val(pj.ProjectName);
                }
            });

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#JingBanRen').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);;
            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"extend",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_RepaymentDetails').jfGrid({
                headData: [
                    {
                        label: '借款人', name: 'BorrowerName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'string', tabname:'还款明细' 
                        ,edit:{
                            type:'layer',
                            change: function (data, rownum, selectData) {
                                data.BorrowerName = selectData.realname;
                                data.Borrower = selectData.userid;

                                data.Loan = selectData.loan;
                                data.GuaZhang = selectData.guazhang;
                                
                                //data.ModificationName = selectData.code;
                                //data.Modification_Id = selectData.fullname;  UserViewHasLoan
                                $('#Finance_RepaymentDetails').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 600,
                                height: 400,
                                listFieldInfo: [
                                    { label: "姓名", name: "realname", width: 100, align: "left" },
                                    { label: "未清借款", name: "loan", width: 100, align: "left" },
                                    { label: "未清挂账", name: "guazhang", width: 100, align: "left" },
                                    { label: "公司", name: "company", width: 100, align: "left" },
                                    { label: "部门", name: "deptname", width: 100, align: "left" }
                                ],
                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: {
                                    code: 'UserViewHasLoan'
}                             }
                        }
                    },
                    {
                        label: '未清借款', name: 'Loan', width: 100, cellStyle: { 'text-align': 'left' }, required: '0',
                        datatype: 'float', tabname: '还款明细'
                    },
                    {
                        label: '还款金额', name: 'Amount', width:100,cellStyle: { 'text-align': 'left' },aggtype:'sum',aggcolumn:'TotalAmount',required:'1',
                        datatype:'float', tabname:'还款明细' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    }, {
                        label: '未清挂账', name: 'GuaZhang', width: 100, cellStyle: { 'text-align': 'left' }, required: '0',
                        datatype: 'float', tabname: '还款明细'
                    },
                    {
                        label: '挂账还款金额', name: 'GuaAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'TotalGuaAmount', required: '0',
                        datatype: 'float', tabname: '还款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '支付方式', name: 'PaymentType', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'string', tabname:'还款明细' 
                        ,edit:{
                            type:'select',
                            change: function (row, index, item, oldValue, colname,headData) {
                            },
                            datatype: 'dataItem',
                            code:'ZFFS'
                        }
                    },
                    {
                        label: '支付账户', name: 'BankNames', width: 250, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '还款明细'
                        , edit: {
                            type: 'layer',
                            change: function (row, index, item, oldValue, colname, headData) {
                                row.Account = item.id;
                                row.BankNames = item.name;
                                $('#Finance_RepaymentDetails').jfGridSet("updateRow", index)
                            },
                            op: {
                                width: 600,
                                height: 400,
                                listFieldInfo: [
                                    { label: "账户名称", name: "name", width: 150, align: "left" },
                                    { label: "开户银行", name: "bank", width: 150, align: "left" },
                                    { label: "账号", name: "bankcode", width: 150, align: "left" },
                                    { label: "可用余额", name: "usablebalance", width: 150, align: "left" },
                                    { label: "余额", name: "balance", width: 150, align: "left" },
                                    { label: "冻结额", name: "freezed", width: 150, align: "left" },
                                    { label: "项目经理", name: "f_realname", width: 150, align: "left" },
                                    { label: "开户人", name: "holder", width: 150, align: "left" },
                                    {
                                        label: "所属公司", name: "companyid", width: 150, align: "left",
                                        formatterAsync: function (callback, value, row, op, $cell) {
                                            Changjie.clientdata.getAsync('company', {
                                                key: value,
                                                callback: function (_data) {
                                                    callback(_data.name);
                                                }
                                            });
                                        }
                                    },
                                ],
                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: {
                                    code: 'ZJZH'
                                }
                            }
                        }
                    },

                    //{
                    //    label: '支付账户', name: 'Account', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                    //    datatype:'string', tabname:'还款明细' 
                    //    ,edit:{
                    //        type:'select',
                    //        change: function (row, index, item, oldValue, colname,headData) {
                    //        },
                    //        datatype: 'dataSource',
                    //        code:'ZJZH',
                    //        op:{
                    //            value: 'id',
                    //            text:'name',
                    //            title:'name'
                    //        }
                    //    }
                    //},
                   
                    {
                        label: '摘要', name: 'Abstract', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'还款明细' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                ],
                mainId:"ID",
                bindTable:"Finance_RepaymentDetails",
                isEdit: true,
                height: 300,toolbarposition:"top",
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_Repayment/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_Repayment/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="Finance_Repayment"]').mkGetFormData());
        postData.strfinance_RepaymentDetailsList = JSON.stringify($('#Finance_RepaymentDetails').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
