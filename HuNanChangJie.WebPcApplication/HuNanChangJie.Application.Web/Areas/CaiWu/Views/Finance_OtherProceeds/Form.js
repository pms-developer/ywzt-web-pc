﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-16 17:48
 * 描  述：其它收款
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var click = "";
var tables = [];
var mainTable = "Finance_OtherProceeds";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_OtherProceeds/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_OtherProceeds/Audit', { keyValue: keyValue }, function (data) {
      
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_OtherProceeds/UnAudit', { keyValue: keyValue }, function (data) {
       
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
                click = "1";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0019' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });

            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });


            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            if (projectId) {

                $("#ProjectID").mkselectSet(projectId);

                var pj = $("#ProjectID").mkselectGetEx();
                if (pj) {

                    if (keyValue && click == "1") {
                        $("#ProjectManager").mkformselectSet();
                        click = "";
                    } else {
                        $("#ProjectManager").mkformselectSet(pj.marketingstaffid);
                    }

                    $("#ProjectName").val(pj.projectname);
                }
                else {
                    $("#ProjectName").val("");
                }
            }

            $('#PaymentCompany').mkDataSourceSelect({ code: 'KHLB', value: 'id', text: 'fullname' });


            $('#FundType').mkDataItemSelect({ code: 'SKKXLX' });
            $('#FundProperty').mkRadioCheckbox({
                type: 'radio',
                code: 'SKKXSX',
            });
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#PaymentType').mkRadioCheckbox({
                type: 'radio',
                code: 'ZFFS',
            });
         //   $('#BankAccount').mkDataSourceSelect({ code: 'ZjzhNoProject', value: 'id', text: 'name' });



            $("#BankAccountName").on("click", function () {
                top.Changjie.layerForm({
                    id: "selectContract",
                    title: "选择账户",
                    url: top.$.rootUrl + '/CaiWu/Base_FundAccount/Dialog?type=ZjzhNoProject',
                    width: 800,
                    hegiht: 294,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (data) {
                                $("#BankAccountName").val(data.Bank);
                                $("#BankAccount").val(data.ID);
                            }
                        });
                    }
                });
            });



            $('#ProjectSubjectId').mkDataSourceSelect({ code: 'SRKM', value: 'id', text: 'fullname' });
            $('#AuditStatus').mkRadioCheckbox({
                type: 'checkbox',
                code: 'AuditStatus',
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "newinstance",
                isShowAttachment: true,
                isShowWorkflow: true,
            });


            $("#ProjectID").on("change", function () {
                var pj = $("#ProjectID").mkselectGetEx();
            
                if (pj) {
                    $("#ProjectName").val(pj.projectname);

                    if (keyValue && click == "1") {
                        $("#ProjectManager").mkformselectSet();
                        click = "";
                    } else {
                        $("#ProjectManager").mkformselectSet(pj.marketingstaffid);
                    }
                }
                else {
                    $("#ProjectName").val("");
                }
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_OtherProceeds/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_OtherProceeds/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
