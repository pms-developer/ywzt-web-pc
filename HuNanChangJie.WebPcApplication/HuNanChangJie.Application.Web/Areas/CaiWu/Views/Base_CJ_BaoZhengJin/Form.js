﻿/* *  
 * 创建人：超级管理员
 * 日  期：2019-12-27 16:34
 * 描  述：保证金管理
 */
var acceptClick;
var keyValue = request('keyValue');
var projectId = request('projectId');
var mainId = "";
var type = "add";
var subGrid = [];
var click = "";
var tables = [];
var mainTable = "Base_CJ_BaoZhengJin";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Base_CJ_BaoZhengJin/SaveForm';

var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
                click = "1";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
            if (type == 'add') {
                page.loadProjectInfo();
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0005' }, function (data) {
                if (!$('#BianHao').val()) {
                    $('#BianHao').val(data);
                }
            });
            $('#Project_ID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });


            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            if (projectId) {
                $("#Project_ID").mkselectSet(projectId);
                var pj = $("#Project_ID").mkselectGetEx();
                if (pj) {
                    if (keyValue && click == "1") {
                        $("#ProjectManager").mkformselectSet();
                        click = "";
                    } else {
                        $("#ProjectManager").mkformselectSet(pj.marketingstaffid);
                    }
                }

            }

            $('#LeiXing').mkDataItemSelect({ code: 'BZJLX' });
            $('#JinBanBuMeng').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            });

            $('#JinBanRen').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
          

            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "newinstance",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            if (projectId) {
                new Promise(() => {
                    $('#Project_ID').mkselectSet(projectId);
                }).then(new Promise(() => {
                    page.initZbbh()
                }))
            }

            $("#Project_ID").on('change', function () {
                page.initZbbh();

            });

        },
        initZbbh: function () {
            //var pid = $("#Project_ID").mkselectGet();
            var pj = $("#Project_ID").mkselectGetEx();
          
            if (pj) {
                if (keyValue && click == "1") {
                    $("#ProjectManager").mkformselectSet();
                    click = "";
                } else {
                    $("#ProjectManager").mkformselectSet(pj.marketingstaffid);
                }

                Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectBid/GetFormInfo", { projectId: pj.project_id }, function (res) {
                    if (!!res.data) {
                        $("#ZhaoBiaoBianHao").val(res.data.Code)
                    }
                    else { $("#ZhaoBiaoBianHao").val("") }
                });

            } else { $("#ZhaoBiaoBianHao").val("") }
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Base_CJ_BaoZhengJin/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
        loadProjectInfo: function () {
            if (!!projectId) {
                top.Changjie.setProjectInfo('form_tabs');
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Base_CJ_BaoZhengJin/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    } if (type == "add") {
        keyValue = mainId;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
