﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-04-02 16:21
 * 描  述：分包付款申请
 */
var refreshGirdData;
var formId = request("formId");
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};
var projectId = request('projectId');
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 250, 800);
            $('#PayCompanyId').mkCompanySelect({});
            $('#PayModel').mkDataItemSelect({ code: 'FKMS' });

            $('#AuditStatus').mkselect({
                type: 'default',
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ]
            });

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            //if (projectId) {
            //    $('#CustomerId').mkselect({ url: '/ProjectModule/Base_SubcontractingUnit/GetPageListByProject?projectid=' + projectId, value: 'ID', text: 'Name' });
            //}
            //else {
            //    $('#CustomerId').mkDataSourceSelect({ code: 'FBDW', value: 'id', text: 'name' });
            //}

            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增分包付款申请',
                    url: top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/GetPageList',
                headData: [
                    { label: "编号", name: "Code", width: 140, align: "left" },
                    {
                        label: "分包单位", name: "CustomerId", width: 250, align: "left",
                    },

                    {
                        label: "申请日期", name: "ApplyDate", width: 75, align: "center", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    {
                        label: "计划付款日期", name: "PlanPaymentDate", width: 75, align: "center", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    {
                        label: "付款单位", name: "PayCompanyName", width: 130, align: "left"
                    },
                    {
                        label: "经办人", name: "OperatorId", width: 60, align: "center",
                    },
                    { label: "申请付款金额", name: "ApplyPaymentAmount", width: 90, align: "center" },
                    {
                        label: "是否打印", name: "printcount", width: 70, align: "center",
                        formatter: function (cellvalue, row) {
                            if (cellvalue > 0) {
                                return '<span class="label label-success" style="cursor: pointer;">已打印</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">未打印</span>';
                            }
                        }
                    },
                    {
                        label: "付款模式", name: "PayModel", width: 100, align: "center",
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 70, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }, { label: "项目/合同信息", name: "TypeList", width: 200, align: "left" }, { label: "摘要", name: "Abstract", width: 160, align: "left" }, { label: "开户名称", name: "AccountName", width: 200, align: "left" },
                    { label: "开户银行", name: "BankName", width: 200, align: "left" },
                    { label: "银行账号", name: "BankAccount", width: 200, align: "left" },
                    
                ],
                bindTable: "Project_SubcontractPaymentApply",
                mainId: 'ID',
                isPage: true
            });
            page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.Project_ID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '分包付款申请',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/Form?keyValue=' + keyValue + '&viewState=' + viewState + '&projectId=' + projectId + "&formId=" + formId,
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
