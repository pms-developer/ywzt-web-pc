﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-04-02 16:21
 * 描  述：分包付款申请
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_SubcontractPaymentApply";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var gridverifystate = true;

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/Audit', { keyValue: keyValue }, function (data) {
       
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/UnAudit', { keyValue: keyValue }, function (data) {
      
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_SubcontractPaymentApplyDetails', "gridId": 'Project_SubcontractPaymentApplyDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0026' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $("#ApplyPaymentAmount").on("input propertychange", function () {
                $("#RatifyAmount").val($(this).val());
                var e = jQuery.Event("propertychange");
                $("#RatifyAmount").trigger(e);
            });

            $('#PayCompanyId').mkCompanySelect({});

            if (projectId) {
                $('#CustomerId').mkselect({ url: '/ProjectModule/Base_SubcontractingUnit/GetPageListByProject?projectid=' + projectId, value: 'ID', text: 'Name' });
            }
            else {
                $('#CustomerId').mkDataSourceSelect({ code: 'FBDW', value: 'id', text: 'name' });
            }

            $('#PayModel').mkDataItemSelect({ code: 'FKMS' });

            $('#CustomerId').on("change", function () {
                var data = $(this).mkselectGetEx();
                if (data) {
                    if (!!$("#AccountName").val().length == 0) {
                        $("#AccountName").val(data.AccountName || data.accountname);
                    }
                    if ($("#BankName").val().length == 0) {
                        $("#BankName").val(data.BankName || data.bankname);
                    }
                    if ($("#BankAccount").val().length == 0) {
                        $("#BankAccount").val(data.BankAccount || data.bankaccount);
                    }
                }
                else {
                    $("#AccountName").val("");
                    $("#BankName").val("");
                    $("#BankAccount").val("");
                }
            });

            //$('#BankId').mkDataSourceSelect({ code: 'ZJZH',value: 'id',text: 'name' });

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            $('#AuditStatus').mkRadioCheckbox({
                type: 'checkbox',
                code: 'AuditStatus',
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_SubcontractPaymentApplyDetails').jfGrid({
                headData: [
                    {
                        label: '合同名称', name: 'Name', width: 220, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '分包付款申请明细'
                        , edit: {
                            type: 'layer',
                            init: function (a, b) {
                                var sql = "";
                                if (projectId) {
                                    sql += " ProjectID='" + projectId + "' ";
                                }
                                var tempcustomer = $('#CustomerId').mkselectGet();
                                if (tempcustomer) {
                                    if (sql != "")
                                        sql += " and "
                                    sql += " Yi='" + tempcustomer + "' ";
                                }

                                this.op.param.strWhere = sql;
                            },
                            change: function (data, rownum, selectData) {
                                $('#CustomerId').mkselectSet(selectData.yi);
                                data.ProjectSubcontractId = selectData.id;
                                data.Name = selectData.name;
                                data.TotalAmount = selectData.totalamount || "";
                                data.SettlementAmount = selectData.settlementamount || "";
                                data.InvioceAmount = selectData.invioceamount || "";
                                data.AccountPaidAmount = selectData.accountpaidamount || "";
                                data.YdyfKuan = selectData.ydyfkuan || 0;
                                data.YdfkBiLi = selectData.ydfkbili || "";
                                data.Sjfkbl = selectData.sjfkbl || "";
                                data.FineExpend = selectData.fineexpend || 0;
                                data.OtherExpend = selectData.otherexpend || 0;
                                data.OtherIncome = selectData.otherincome || 0;

                                data.YdyfKuan = data.YdyfKuan - data.FineExpend - data.OtherExpend + data.OtherIncome;

                                $('#Project_SubcontractPaymentApplyDetails').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 1000,
                                height: 400,
                                listFieldInfo: [
                                    { label: "编码", name: "code", width: 160, align: "left" },
                                    { label: "合同名称", name: "name", width: 200, align: "left" },
                                    { label: "分包类型", name: "subcontracttypedesc", width: 100, align: "left" },
                                    { label: "分包属性", name: "propertydesc", width: 100, align: "left" },
                                    { label: "乙方", name: "unitname", width: 100, align: "left" },
                                    { label: "签订金额", name: "totalamount", width: 100, align: "left" },
                                    {
                                        label: "签订时间", name: "signdate", width: 88, align: "center",
                                        formatter: function (cellvalue) {
                                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                                        }},
                                    { label: "累计罚款金额", name: "fineexpend", width: 130, align: "left" },
                                    { label: "累计支出金额", name: "otherexpend", width: 130, align: "left" },
                                    { label: "累计收入金额", name: "otherincome", width: 130, align: "left" },
                                ],
                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: {
                                    code: 'FBHTLB'
                                }
                            }
                        }
                    },
                    {
                        label: '当前合同金额', name: 'TotalAmount', width: 100
                    },
                    {
                        label: '已结算金额', name: 'SettlementAmount', width: 100
                    },
                    {
                        label: '已收票金额', name: 'InvioceAmount', width: 100
                    },
                    {
                        label: '已付款金额', name: 'AccountPaidAmount', width: 100
                    },
                    {
                        label: '累计罚款金额', name: 'FineExpend', width: 100
                    },
                    {
                        label: '累计支出金额', name: 'OtherExpend', width: 100
                    },
                    {
                        label: '累计收入金额', name: 'OtherIncome', width: 100
                    },
                    {
                        label: '实际付款比例(%)', name: 'Sjfkbl', width: 100
                    },
                    {
                        label: '约定付账比例(%)', name: 'YdfkBiLi', width: 100
                    },
                    {
                        label: '约定应付账款', name: 'YdyfKuan', width: 100
                    },
                    {
                        label: '本次申请', name: 'ApplyAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'ApplyPaymentAmount', required: '1',
                        datatype: 'float', tabname: '分包付款申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {

                                //2-23 时代消防系统要求去除验证
                                //var totalAmount = parseFloat(row["TotalAmount"] || 0);
                                //var accountPaidAmount = parseFloat(row["AccountPaidAmount"] || 0);

                                //if (totalAmount - accountPaidAmount < row.ApplyAmount) {
                                //    top.Changjie.alert.warning("本次付款金额不能大于剩余待付金额");
                                //    $(obj).css("color", "red");
                                //    $(obj).attr("color", "red");
                                //    gridverifystate = false;
                                //} else {
                                //    $(obj).css("color", "black");
                                //    $(obj).attr("color", "black");
                                //    gridverifystate = true;
                                //}
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    //{
                    //    label: '本次分包产值', name: 'Output', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                    //    datatype:'string', tabname:'分包付款申请明细' 
                    //    ,edit:{
                    //        type:'input',
                    //       change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //       },
                    //       blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //       }
                    //    }
                    //},
                    //{
                    //    label: '本次应付款', name: 'AccountPayable', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                    //    datatype:'float', tabname:'分包付款申请明细' 
                    //    ,edit:{
                    //        type:'input',
                    //       change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //       },
                    //       blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //       }
                    //    }
                    //},
                    //{
                    //    label: '实际应付款', name: 'ActualPayable', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                    //    datatype:'float', tabname:'分包付款申请明细' 
                    //    ,edit:{
                    //        type:'input',
                    //       change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //       },
                    //       blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //       }
                    //    }
                    //},
                    {
                        label: '备注', name: 'Remark', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '分包付款申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_SubcontractPaymentApplyDetails",
                isEdit: true,
                height: 220, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            debugger;
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                            var i11 = 11;
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (gridverifystate == false) {
            top.Changjie.alert.warning("数据验证失败。");
            return false;
        }
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Project_SubcontractPaymentApply/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();


}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var ratifyAmount = top.Changjie.clearNumSeparator($("#RatifyAmount").val(), "float");
    if (ratifyAmount <= 0) {
        top.Changjie.alert.warning("批准金额必须大于0");
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_SubcontractPaymentApply"]').mkGetFormData());
    postData.strproject_SubcontractPaymentApplyDetailsList = JSON.stringify($('#Project_SubcontractPaymentApplyDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
