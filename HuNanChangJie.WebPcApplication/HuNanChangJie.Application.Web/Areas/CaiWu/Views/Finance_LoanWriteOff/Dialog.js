﻿var acceptClick;

var loaner = request('loaner');

var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {

            $('#JingBanRen').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            if (loaner != "") {
                $('#JingBanRen').mkformselectSet(loaner)
            }

            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });

            $('#dosearch').on("click", function () {
               
                page.search();
            })
            var param = {};
            param.AuditStatus = 2;
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/ProjectModule/SporadicPurchase/GetPageList',
                headData: [
                    { label: "采购编码", name: "Code", width: 160, align: "left" },
                    {
                        label: "采购人", name: "PurchaseUserId", width: 80, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }},
                    {
                        label: "采购部门", name: "PurchaseDepartmentId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('department', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }},
                    { label: "采购金额", name: "Amount", width: 100, align: "left" },
                    { label: "项目名称", name: "ProjectName", width: 200, align: "left" }
                ],
                mainId: 'ID',
                isPage: true,
                height: 362,
                param: { queryJson: JSON.stringify(param), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) },
                isMultiselect:true
            });
            page.search();
            
        },
        search: function (param) {
            param = param || {};
            var param = {};
            param.AuditStatus = 2;

            var tempsword = $('#sword').val();
            if (tempsword) {
                param.sword = tempsword;
            }
            var tempproject = $('#ProjectID').mkselectGet();
            if (tempproject) {
                param.ProjectID = tempproject;
            }
            var tempuserid = $('#JingBanRen').mkformselectGet();
            if (tempuserid) {
                param.PurchaseUserId = tempuserid;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};