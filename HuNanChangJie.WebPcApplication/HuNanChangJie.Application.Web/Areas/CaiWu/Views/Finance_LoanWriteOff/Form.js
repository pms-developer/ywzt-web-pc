﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-06-29 18:37
 * 描  述：借款冲销
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Finance_LoanWriteOff";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_LoanWriteOff/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_LoanWriteOff/Audit', { keyValue: keyValue }, function (data) {
       
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_LoanWriteOff/UnAudit', { keyValue: keyValue }, function (data) {
       
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Finance_LoanWriteOffDetails', "gridId": 'Finance_LoanWriteOffDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0031' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#JingBanRen').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#Loaner').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#JingBanBuMeng').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });


            $("#LoanCode").on("click", function () {
                Changjie.layerForm({
                    id: "selectDanju1",
                    width: 900,
                    height: 480,
                    title: "选择借款单据",
                    url: top.$.rootUrl + "/CaiWu/Finance_Loan/Dialog?projectId=" + projectId,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                $('#Loaner').mkformselectSet(data.Loaner)
                                $("#LoanCode").val(data.Code)
                                $("#Fk_LoanId").val(data.ID)
                                $("#PayedAmount").val(data.PayedAmount)
                            }
                        });
                    }
                });
            })
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_LoanWriteOffDetails').jfGrid({
                headData: [
                    {
                        label: '单据类型', name: 'BillType', width: 80, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '冲销明细'
                    },

                    {
                        label: '单据编码', name: 'Code', width: 150, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '冲销明细'
                    },
                    {
                        label: '单据金额', name: 'Amount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '冲销明细'
                    },
                    {
                        label: '冲销金额', name: 'WriteOffAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'WriteOffAmount', required: '1',
                        datatype: 'float', tabname: '冲销明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '项目名称', name: 'ProjectName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '冲销明细'
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '冲销明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    }
                ],
                mainId: "ID",
                bindTable: "Finance_LoanWriteOffDetails",
                isEdit: true,
                height: 300, toolbarposition: "top", showchoose: true, showadd: false, onChooseEvent: function () {
                    if ($("#Fk_LoanId").val() == "") {
                        top.Changjie.alert.info("请先选择借款单号");
                        return;
                    }
                    var tempuserid = $('#Loaner').mkformselectGet();

                    Changjie.layerForm({
                        id: "selectDanju",
                        width: 900,
                        height: 480,
                        title: "选择冲销单据",
                        url: top.$.rootUrl + "/CaiWu/Finance_LoanWriteOff/Dialog?loaner=" + tempuserid,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    var total = 0;

                                    var cvv = $('#Finance_LoanWriteOffDetails').jfGridGet("rowdatas");

                                    for (var i = 0; i < data.length; i++) {
                                        var ishas = false;
                                        $.each(cvv, function (ii, ee) {
                                            //console.log(ee.PaymentApplyId)
                                            //console.log(data[i].ApplyId)

                                            if (ee.Fk_BillId == data[i].ID) {
                                                ishas = true;
                                                return false;
                                            }
                                        })

                                        if (ishas) {
                                            continue;
                                        }


                                        var row = {};
                                        row.BillType = '零星采购单'
                                        row.PurchaseUserId = data[i].PurchaseUserId;
                                        row.PurchaseDepartmentId = data[i].PurchaseDepartmentId;
                                        row.Code = data[i].Code;
                                        row.Amount = data[i].Amount;
                                        row.ProjectName = data[i].ProjectName;
                                        row.Fk_BillId = data[i].ID;
                                        total += data[i].Amount;
                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;

                                        rows.push(row);
                                    }

                                    $("#Finance_LoanWriteOffDetails").jfGridSet("addRows", rows);
                                }
                            });
                        }
                    });
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_LoanWriteOff/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_LoanWriteOff/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    if ($("#Fk_LoanId").val() == "") {
        top.Changjie.alert.info("请选择借款单号");
        return;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Finance_LoanWriteOff"]').mkGetFormData());
    postData.strfinance_LoanWriteOffDetailsList = JSON.stringify($('#Finance_LoanWriteOffDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
