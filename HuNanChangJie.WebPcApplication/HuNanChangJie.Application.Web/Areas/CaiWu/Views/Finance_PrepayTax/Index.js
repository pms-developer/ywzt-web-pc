﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-31 20:20
 * 描  述：开票预缴
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_PrepayTax/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_PrepayTax/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var bootstrap = function ($, Changjie) {
    "use strict";

    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 400);

           // $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB',value: 'project_id',text: 'projectname' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增开票预缴',
                    url: top.$.rootUrl + '/CaiWu/Finance_PrepayTax/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_PrepayTax/GetPageList',
                headData: [
                    { label: "编号", name: "Code", width: 200, align: "left" },
                    { label: "发票编号", name: "InvoiceCode", width: 140, align: "left" },
                    { label: "票号", name: "TicketNumber", width: 140, align: "left" },
                    { label: "项目名称", name: "ProjectName", width: 250, align: "left" },
                    { label: "项目经理", name: "F_RealName", width: 140, align: "left" },
                    { label: "开票金额", name: "InvoiceAmount", width: 140, align: "left" },
                    { label: "纳税人识别号", name: "TaxpayerNumber", width: 100, align: "left"},
                    { label: "纳税人名称", name: "TaxpayerName", width: 100, align: "left"},
                    {
                        label: "填发时间", name: "CertificateDate", width: 80, align: "left",
                        formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }},
                 
                    { label: "预缴类型", name: "PrepayType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'NSRXZ',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }}, 
                    { label: "预缴公司", name: "PrepayCompany", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('company', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "预缴日期", name: "PrepayDate", width: 80, align: "left"},
                    { label: "预缴总额", name: "PrepayAmount", width: 100, align: "left"},
                    
                    {
                        label: "审核状态", name: "AuditStatus", width: 70, align: "center", formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }, { label: "摘要", name: "Remark", width: 300, align: "left" },
                ],
                mainId:'ID',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }

            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Finance_PrepayTax/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title +'开票预缴',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/CaiWu/Finance_PrepayTax/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
