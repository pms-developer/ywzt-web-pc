﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-31 20:20
 * 描  述：开票预缴
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var click = "";
var mainTable = "Finance_PrepayTax";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_PrepayTax/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_PrepayTax/Audit', { keyValue: keyValue }, function (data) {
       
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_PrepayTax/UnAudit', { keyValue: keyValue }, function (data) {
      
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
                click = "1";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Finance_PrepayDetails', "gridId": 'Finance_PrepayDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0025' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });


            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#InvoiceCodeID').mkDataSourceSelect({ code: 'FIR', value: 'id', text: 'invoicecode' });

            $('#InvoiceCodeID').on("change", function () {
                var p = $('#InvoiceCodeID').mkselectGetEx();
                if (p) {
                    $("#InvoiceTime").val(p.makeinvoicedate);
                    $("#InvoiceAmount").val(p.invoiceamount)
                } else {
                    $("#InvoiceTime").val();
                    $("#InvoiceAmount").val();
                }
            })

            $("#Fk_ContractId").mkselect({});

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);
                var p = $('#ProjectID').mkselectGetEx();
                if (p) {
                    $("#c").val(p.code)

                    if (keyValue && click == "1") {
                        $("#ProjectManager").mkformselectSet();
                        click = "";
                    } else {
                        $("#ProjectManager").mkformselectSet(p.marketingstaffid);
                    }

                    var sql = " ProjectID='" + p.project_id + "' ";

                    Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'GCHTLB', strWhere: sql }, function (res) {
                        console.log(res)
                        if (res.code == 200) {
                            console.log(res.data)
                            $('#Fk_ContractId').mkselectRefresh({ data: res.data, value: 'id', text: 'name', title: 'name' });
                        }
                        else {
                            $('#Fk_ContractId').mkselectRefresh({ data: {} });
                            //$('#Fk_ContractId').mkselectSet("");
                        }
                    });

                    // $('#Fk_ContractId').mkselect({ url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', param: { code: 'GCHTLB', strWhere: sql }, value: 'id', text: 'name', title: 'name' });
                }

            }


            $('#ProjectID').on("change", function () {

                var p = $('#ProjectID').mkselectGetEx();
                if (p) {
                    $("#c").val(p.code)

                    if (keyValue && click == "1") {
                        $("#ProjectManager").mkformselectSet();
                        click = "";
                    } else {
                        $("#ProjectManager").mkformselectSet(p.marketingstaffid);
                    }

                    var sql = " ProjectID='" + p.project_id + "' ";

                    Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'GCHTLB', strWhere: sql }, function (res) {
                        console.log(res)
                        if (res.code == 200) {
                            console.log(res.data)
                            $('#Fk_ContractId').mkselectRefresh({ data: res.data, value: 'id', text: 'name', title: 'name' });
                        }
                        else {
                            $('#Fk_ContractId').mkselectRefresh({ data: {} });
                            //$('#Fk_ContractId').mkselectSet("");
                        }
                    });

                    //$('#Fk_ContractId').mkselect({ url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', param: { code: 'GCHTLB', strWhere: sql }, value: 'id', text: 'name', title: 'name' });
                }
            })

            $('#Fk_ContractId').on("change", function () {
                var c = $('#Fk_ContractId').mkselectGetEx();
                if (c) {
                    $("#b").val(c.totalamount)
                    $("#a").val(c.code)
                }
            })


            //$('#ContractType').mkDataItemSelect({ code: 'FKDLB' });
            $('#PrepayType').mkDataItemSelect({ code: 'NSRXZ' });
            //$('#Fk_ContractId').mkDataSourceSelect({ code: 'CW-HTLB',value: 'id',text: 'name' });
            //$('#Fk_InvoiceId').mkDataSourceSelect({ code: 'BASE_XMLB',value: 'project_id',text: 'projectname' });
            $('#PrepayCompany').mkCompanySelect({});
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_PrepayDetails').jfGrid({
                headData: [
                    {
                        label: '原凭证号', name: 'VoucherNumber', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '税种', name: 'TaxType', width: 160, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'ZCKM0502',
                            op: {
                                value: 'id',
                                text: 'fullname',
                                title: 'fullname'
                            }
                        }
                    },
                    {
                        label: '实缴(退)金额', name: 'PaidOrInAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'PrepayAmount', required: '1',
                        datatype: 'float', tabname: '完税证明'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '品目名称', name: 'ItemName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '税款所时期(开始)', name: 'StartDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '税款所时期(结束)', name: 'EndDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '入(退)库日期', name: 'OutOrInDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },

                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_PrepayDetails",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
            //$('#AuditStatus').mkRadioCheckbox({
            //    type: 'checkbox',
            //    code: 'AuditStatus',
            //});
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_PrepayTax/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_PrepayTax/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Finance_PrepayTax"]').mkGetFormData());
    postData.strfinance_PrepayDetailsList = JSON.stringify($('#Finance_PrepayDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
