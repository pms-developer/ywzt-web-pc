﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-27 20:15
 * 描  述：财务 开票申请
 */
var acceptClick;
var keyValue = request('keyValue');
var projectId = request('projectId');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var click = "";
var mainTable = "Finance_InvoiceApply";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_InvoiceApply/SaveForm';
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
                click = "1";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Finance_InvoiceApplyInfo', "gridId": 'Finance_InvoiceApplyInfo' });
                subGrid.push({ "tableName": 'Finance_InvoiceApplyDetails', "gridId": 'Finance_InvoiceApplyDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        initrelationdata: function () {
            var selectData = $("#ProjectID").mkselectGetEx();
            if (selectData) {
                $("#a").val(selectData.code)
                $("#ProjectName").val(selectData.projectname)
                $("#Address").val(selectData.address)

                if (keyValue && click == "1") {
                    $("#ProjectManager").mkformselectSet();
                    click = "";
                } else {
                    $("#ProjectManager").mkformselectSet(selectData.marketingstaffid);
                }

                //var param = {};
                //param.ProjectId = projectId;
                //Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectContract/GetPageList', { queryJson: JSON.stringify(param), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
                //}); 
            } else {
                $("#a").val("");
                $("#Address").val("")
                $("#ProjectName").val("")
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0010' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });

            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
          
            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);

               // $("#Address").val(selectData.address)

            }

            //console.log($('#ProjectID'))

            //if (projectId) {
            //    new Promise(() => {
            //        $('#ProjectID').mkselectSet(projectId);
            //    }).then(new Promise(() => {
            //        page.initrelationdata()
            //    }))
            //}

            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });


            $('#ApplyUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#ApplyDepartment').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            });
            $('#InvoiceCompany').mkCompanySelect({});
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });

            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_InvoiceApplyInfo').jfGrid({
                headData: [
                    {
                        label: '合同名称', name: 'Name', width: 160, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '',
                        datatype: 'string', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }, readonly: true
                        }
                    },
                    //{
                    //    label: '发票抬头', name: 'InvoiceTitle', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    //    datatype: 'string', tabname: '发票申请明细'
                    //    , edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},
                    {
                        label: '收票单位', name: 'InvoiceCompany', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '发票类型', name: 'InvoiceType', width: 140, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '发票申请明细'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                                var gridName = "Finance_InvoiceApplyInfo";
                                row.TaxRate = row.InvoiceType;
                                setCellValue("TaxRate", row, index, gridName, eval(row.InvoiceType * 100));

                                if (row.ApplyAmount > 0) {
                                    var rate = parseFloat(row.TaxRate / 100);

                                    var TaxRates = 1 + rate;

                                    row.AfterTaxAmount = row.ApplyAmount / TaxRates;
                                    setCellValue("AfterTaxAmount", row, index, gridName, Changjie.format45(row.AfterTaxAmount, Changjie.sysGlobalSettings.pointGlobal));

                                    row.TaxPrice = row.AfterTaxAmount * TaxRates;
                                    setCellValue("TaxPrice", row, index, gridName, Changjie.format45(row.TaxPrice, Changjie.sysGlobalSettings.pointGlobal));

                                    var taxAmount = (row.ApplyAmount / (1 + rate) * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal)
                                    row.Tax = taxAmount;
                                    setCellValue("Tax", row, index, gridName, Changjie.format45(row.Tax, Changjie.sysGlobalSettings.pointGlobal));

                                }

                            },
                            datatype: 'dataItem',
                            code: 'FPLX'
                        }
                    },

                    {
                        label: '税率', name: 'TaxRate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'int', tabname: '发票申请明细', inlineOperation: { isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }, readonly: true
                        }
                    },

                    {
                        label: '申请金额', name: 'ApplyAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                //var gridName = "Finance_InvoiceApplyInfo";

                                //if (row.TaxRate > 0) {
                                //    var TaxRates = 1 + Number(row.TaxRate);
                                //    row.AfterTaxAmount = row.ApplyAmount / TaxRates;
                                //    setCellValue("AfterTaxAmount", row, index, gridName, Changjie.format45(row.AfterTaxAmount, Changjie.sysGlobalSettings.pointGlobal));

                                //    row.TaxPrice = row.AfterTaxAmount * TaxRates;
                                //    setCellValue("TaxPrice", row, index, gridName, Changjie.format45(row.TaxPrice, Changjie.sysGlobalSettings.pointGlobal));

                                //    row.Tax = row.AfterTaxAmount * row.TaxRate;
                                //    setCellValue("Tax", row, index, gridName, Changjie.format45(row.Tax, Changjie.sysGlobalSettings.pointGlobal));
                                //}

                                var gridName = "Finance_InvoiceApplyInfo";

                                if (row.ApplyAmount > 0) {

                                    var rate = parseFloat(row.TaxRate / 100);
                                    var TaxRates = 1 + rate;

                                    row.AfterTaxAmount = row.ApplyAmount / TaxRates;
                                    setCellValue("AfterTaxAmount", row, index, gridName, Changjie.format45(row.AfterTaxAmount, Changjie.sysGlobalSettings.pointGlobal));

                                    row.TaxPrice = row.AfterTaxAmount * TaxRates;
                                    setCellValue("TaxPrice", row, index, gridName, Changjie.format45(row.TaxPrice, Changjie.sysGlobalSettings.pointGlobal));

                                    var taxAmount = (row.ApplyAmount / (1 + rate) * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal)
                                    row.Tax = taxAmount;
                                    setCellValue("Tax", row, index, gridName, Changjie.format45(row.Tax, Changjie.sysGlobalSettings.pointGlobal));

                                }




                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '税额', name: 'Tax', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: '', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '发票申请明细'
                    },
                    {
                        label: '税后金额', name: 'AfterTaxAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                //var gridName = "Finance_InvoiceApplyInfo";

                                //if (row.TaxRate > 0) {
                                //    row.TaxPrice = row.AfterTaxAmount * (1 + Number(row.TaxRate));
                                //    setCellValue("TaxPrice", row, index, gridName, row.TaxPrice);
                                //}
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }, readonly: true
                        }
                    },

                    {
                        label: '含税价', name: 'TaxPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }, readonly: true
                        }
                    },


                    {
                        label: '审计金额', name: 'AuditAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'InvoiceAmount', required: '1',
                        datatype: 'float', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '当前合同金额', name: 'TotalAmount', width: 160, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '',
                        datatype: 'string', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }, readonly: true
                        }
                    },
                    {
                        label: '未开票金额', name: 'NoInvioceAmount', width: 160, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '',
                        datatype: 'string', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }, readonly: true
                        }
                    },

                    {
                        label: '开户银行', name: 'Bank', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '联系电话', name: 'Phone', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '发票申请明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_InvoiceApplyInfo",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false,
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#Finance_InvoiceApplyDetails').jfGrid({
                headData: [
                    {
                        label: '发票类型', name: 'InvoiceType', width: 140, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '发票内容'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                                var gridName = "Finance_InvoiceApplyDetails";
                                row.TaxRate = row.InvoiceType;

                                setCellValue("TaxRate", row, index, gridName, row.InvoiceType);

                                if (row.TaxAmount > 0) {
                                    row.Tax = row.TaxAmount * row.TaxRate;
                                    setCellValue("Tax", row, index, gridName, row.Tax);
                                }
                            },
                            datatype: 'dataItem',
                            code: 'FPLX'
                        }
                    },
                    {
                        label: '开票内容', name: 'Content', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '发票内容'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '发票内容'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '发票内容'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'int', tabname: '发票内容', inlineOperation: { to: [{ "t1": "Quantity", "t2": "TaxPrice", "to": "TaxAmount", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "Finance_InvoiceApplyDetails";

                                if (row.TaxAmount > 0) {
                                    row.Tax = row.TaxAmount * row.TaxRate;
                                    setCellValue("Tax", row, index, gridName, row.Tax);
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '含税单价', name: 'TaxPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '发票内容', inlineOperation: { to: [{ "t1": "Quantity", "t2": "TaxPrice", "to": "TaxAmount", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "Finance_InvoiceApplyDetails";

                                if (row.TaxAmount > 0) {
                                    row.Tax = row.TaxAmount * row.TaxRate;
                                    setCellValue("Tax", row, index, gridName, row.Tax);
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '含税金额', name: 'TaxAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '发票内容', inlineOperation: { to: [{ "t1": "TaxAmount", "t2": "TaxRate", "to": "Tax", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },

                    {
                        label: '税率', name: 'TaxRate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '发票内容', inlineOperation: { to: [{ "t1": "TaxAmount", "t2": "TaxRate", "to": "Tax", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }, readonly: true
                        }
                    },
                    {
                        label: '税额', name: 'Tax', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '发票内容'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }, readonly: true
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_InvoiceApplyDetails",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_InvoiceApply/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                    page.initrelationdata();

                    $("#ProjectID").on('change', function () {
                        page.initrelationdata();

                        var pid = $("#ProjectID").mkselectGet()
                        if (pid) {
                            Changjie.httpAsync('GET', top.$.rootUrl + '/CaiWu/Finance_InvoiceApply/GetFinanceInvoiceApplyInfoList?projectid=' + pid, null, function (data) {
                                if (data && data.Finance_InvoiceApplyInfo && data.Finance_InvoiceApplyInfo.length > 0) {
                                    $("#Finance_InvoiceApplyInfo").jfGridSet('refreshdata', data.Finance_InvoiceApplyInfo)
                                }
                                else {
                                    $("#Finance_InvoiceApplyInfo").jfGridSet('clearallrow')
                                }
                            });
                        }
                        else {
                            $("#Finance_InvoiceApplyInfo").jfGridSet('clearallrow')
                        }
                    });
                });
            }
            else {
                $("#ProjectID").on('change', function () {
                    page.initrelationdata();

                    var pid = $("#ProjectID").mkselectGet()
                    if (pid) {
                        Changjie.httpAsync('GET', top.$.rootUrl + '/CaiWu/Finance_InvoiceApply/GetFinanceInvoiceApplyInfoList?projectid=' + pid, null, function (data) {
                            if (data && data.Finance_InvoiceApplyInfo && data.Finance_InvoiceApplyInfo.length > 0) {
                                $("#Finance_InvoiceApplyInfo").jfGridSet('refreshdata', data.Finance_InvoiceApplyInfo)
                            }
                            else {
                                $("#Finance_InvoiceApplyInfo").jfGridSet('clearallrow')
                            }
                        });
                    }
                    else {
                        $("#Finance_InvoiceApplyInfo").jfGridSet('clearallrow')
                    }
                });
                if (projectId) {
                    $("#ProjectID").mkselectSet(projectId);
                }
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_InvoiceApply/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var setCellValue = function (cellName, row, index, gridName, value) {

    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;
    row.EditType = 2;
    $cell.html(value);
    $edit.val(value);
};
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    var Finance_InvoiceApplyInfo = $('#Finance_InvoiceApplyInfo').jfGridGet('rowdatas')
    postData.strEntity = JSON.stringify($('[data-table="Finance_InvoiceApply"]').mkGetFormData());
    if (Finance_InvoiceApplyInfo != null && type != "add") {
        $.each(Finance_InvoiceApplyInfo, function (i, e) {
            e.EditType = 2;
        })
    }
    //console.log(Finance_InvoiceApplyInfo)

    postData.strfinance_InvoiceApplyInfoList = JSON.stringify(Finance_InvoiceApplyInfo);

    var Finance_InvoiceApplyDetails = $('#Finance_InvoiceApplyDetails').jfGridGet('rowdatas')
    if (Finance_InvoiceApplyDetails != null && type != "add") {
        $.each(Finance_InvoiceApplyDetails, function (i, e) {
            e.EditType = 2;
        })
    }
    //console.log(Finance_InvoiceApplyDetails)
    postData.strfinance_InvoiceApplyDetailsList = JSON.stringify(Finance_InvoiceApplyDetails);
   
    postData.deleteList = JSON.stringify(deleteList);

    return postData;
}
