﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-27 20:15
 * 描  述：财务 开票申请
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var checkJson = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 250, 400);

          //  $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });

            //$('#Enabled').mkRadioCheckbox({
            //    type: 'checkbox',
            //    code: 'SZSF',

            //});

            //$('#Enabled').find('input').eq(1).trigger('click');
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
          
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增开票申请',
                    url: top.$.rootUrl + '/CaiWu/Finance_InvoiceApply/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_InvoiceApply/GetPageList',
                headData: [
                    { label: "申请单号", name: "Code", width: 150, align: "left" },
                    {
                        label: "申请日期", name: "ApplyDate", width: 100, align: "left", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    //{
                    //    label: "项目名称", name: "ProjectID", width: 300, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('sourceData', {
                    //            code: 'BASE_XMLB',
                    //            key: value,
                    //            keyId: 'project_id',
                    //            callback: function (_data) {
                    //                callback(_data['projectname']);
                    //            }
                    //        });
                    //    }
                    //},
                    {
                        label: "项目名称", name: "ProjectName", width: 300, align: "left"
                    },
                    {
                        label: "收票单位", name: "ShouPiaoCompany", width: 250, align: "left"
                    },
                    { label: "开票金额", name: "InvoiceAmount", width: 100, align: "left" },
                    { label: "税率", name: "TaxRate", width: 70, align: "center" },
                    //{ label: "税额", name: "TaxAmount", width: 100, align: "left" },
                    { label: "项目经理", name: "ProjectManager", width: 70, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    {
                        label: "申请人", name: "ApplyUserId", width: 100, align: "left"
                        
                    },
                    //{
                    //    label: "申请部门", name: "ApplyDepartment", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('department', {
                    //            key: value,
                    //            callback: function (_data) {
                    //                callback(_data.name);
                    //            }
                    //        });
                    //    }
                    //}, 
                    { label: "项目地址", name: "Address", width: 200, align: "left" },

                    
                   
                   
                ],
                mainId: 'ID',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {

        page.search(checkJson);
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Finance_InvoiceApply/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '开票申请',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/CaiWu/Finance_InvoiceApply/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId,
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
