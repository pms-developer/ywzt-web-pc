﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-12-14 11:43
 * 描  述：税费核算
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Accounting";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Accounting/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var gridverifystate = true;
var makeinvoicecompanyid = "";
var checkprojectId = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();

            page.bind();
            page.initData();
            page.setTax();

            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLBS', value: 'project_id', text: 'projectname' });

            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#Finance_PrepayTaxID').mkDataSourceSelect({ code: 'KPYJ', value: 'id', text: 'code' });

            $('#Finance_InvoiceRegisterId').mkDataSourceSelect({ code: 'FIR', value: 'id', text: 'invoicecode' });

            //$('#CompanyAccount').mkDataSourceSelect({ code: 'ZJZH', value: 'id', text: 'name' });
            //$('#ProjectAccount').mkDataSourceSelect({ code: 'ZJZH', value: 'id', text: 'name' });

            if (type == "add") {
                bindEvent();
                if (projectId) {
                    $("#ProjectID").mkselectSet(projectId);
                }
            }

            $("#Finance_PrepayTaxID").on('change', function () {
                var selectData = $("#Finance_PrepayTaxID").mkselectGetEx();
                if (selectData) {

                    var sql = " FinancePrepayTaxId='" + selectData.id + "' ";

                    var gridName = "AccountingTaxDetail";

                    Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'KPYJXX', strWhere: sql }, function (res) {
                        if (res.code == 200) {
                            var row = $('#AccountingTaxDetail').jfGridGet('rowdatas');

                            $.each(res.data, function (ii, ee) {
                                for (var i = 0; i < row.length; i++) {
                                    if (row[i].SubjectName == ee.name) {
                                        setCellValue("WithholdingTax", row[i], i, gridName, ee.paidorinamount);//
                                    }
                                }
                            })
                            publicSetRate();
                        }
                    });
                }
            })


            //$("#Finance_InvoiceRegisterId").on('change', function () {

            //    var selectData = $("#Finance_InvoiceRegisterId").mkselectGetEx();
            //    if (selectData) {
            //        $("#MakeInvoiceCompanyId").val(selectData.f_fullname);//开票单位

            //        var invoiceamount = parseFloat(selectData.invoiceamount);
            //        $("#InvoiceAmount").val(invoiceamount);//开票金额

            //        var rate = parseFloat(selectData.invoicetype);//税率
            //        $("#Rate").val(rate * 100);//显示税率

            //        //开票金额 / (1 + 税率) * 税率
            //        var salesTax = invoiceamount / (1 + rate) * rate;

            //        salesTax = parseFloat(Changjie.format45(salesTax, Changjie.sysGlobalSettings.pointGlobal2));
            //        $("#SalesTax").val(salesTax);//销售税金

            //        //开票金额-销售税金
            //        var taxIncomeNotIncluded = invoiceamount - salesTax;
            //        taxIncomeNotIncluded = Changjie.format45(taxIncomeNotIncluded, Changjie.sysGlobalSettings.pointGlobal2);
            //        $("#TaxIncomeNotIncluded").val(taxIncomeNotIncluded);//不含税收入


            //        var selectData = $("#TaxRateExclusive").mkselectGetEx();
            //        if (selectData) {
            //            var tar = parseFloat(selectData.id)
            //            var invoiceAmount = parseFloat($("#InvoiceAmount").val() || 0);
            //            var prepay = invoiceAmount * tar;
            //            $("#Prepay").val(prepay);//应预缴
            //            $("#TaxRateExclusive").val(selectData.id);//税率
            //            $("#TaxRateExclusiveText").val(selectData.text);//税率文本

            //            //销售税金 - 应预缴
            //            var vatDeductible = prepay - invoiceAmount;
            //            vatDeductible = Changjie.format45(vatDeductible, Changjie.sysGlobalSettings.pointGlobal2);
            //            $("#VatDeductible").val(vatDeductible);//需抵扣增值税


            //        }

            //        var taxAmountSum = parseFloat($("#TaxAmountSum").val() || 0);//含税金额
            //        var taxAmount = invoiceamount - taxAmountSum;
            //        taxAmount = Changjie.format45(taxAmount, Changjie.sysGlobalSettings.pointGlobal2);

            //        var row = $('#AccountingTaxDetail').jfGridGet('rowdatas')[0];
            //        setCellValue("TaxAmount", row, 0, "AccountingTaxDetail", taxAmount);

            //        publicSetRate();

            //    }
            //});

            $('#TaxRateExclusive').mkDataItemSelect({ code: 'FPLX' });
            $("#TaxRateExclusive").on('change', function () {
                var selectData = $("#TaxRateExclusive").mkselectGetEx();

                if (selectData) {
                    var tar = parseFloat(selectData.id)
                    var invoiceAmount = parseFloat($("#TaxIncomeNotIncluded").val() || 0);
                    var prepay = Changjie.format45(invoiceAmount * tar, Changjie.sysGlobalSettings.pointGlobal2);
                    $("#Prepay").val(prepay);//应预缴
                    $("#TaxRateExclusive").val(selectData.id);//税率
                    $("#TaxRateExclusiveText").val(selectData.text);//税率文本

                    //销售税金 - 应预缴
                    var salesTax = parseFloat($("#SalesTax").val() || 0);

                    var vatDeductible = salesTax - prepay;
                    vatDeductible = Changjie.format45(vatDeductible, Changjie.sysGlobalSettings.pointGlobal2);
                    $("#VatDeductible").val(vatDeductible).trigger('change');//需抵扣增值税
                }
            })

            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#AccountingTnvoiceDetail').jfGrid({
                headData: [
                    {
                        label: '明细', name: 'InvoiceRateType', width: 180, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '采购发票'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                                var gridName = "AccountingTnvoiceDetail";

                                if (row.InvoiceRateType > 0 && row.TaxAmount > 0) {

                                    var taxAmount = parseFloat(row.TaxAmount || 0);//金额
                                    var invoiceRateType = parseFloat(row.InvoiceRateType || 0);//税率
                                    //含税金额 / (1 + 税率) * 税率
                                    var adjustableTax = taxAmount / (1 + invoiceRateType) * invoiceRateType;
                                    adjustableTax = Changjie.format45(adjustableTax, Changjie.sysGlobalSettings.pointGlobal2);

                                    row.AdjustableTax = adjustableTax;
                                    setCellValue("AdjustableTax", row, index, gridName, adjustableTax);

                                    publicSetRate();
                                }
                            },
                            datatype: 'dataItem',
                            code: 'FPLX'
                        }
                    },
                    {
                        label: "依据", name: "Basis", width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '采购发票'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    //{
                    //    label: "含税金额", name: "TaxAmount", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    //    datatype: 'float', tabname: '采购发票', //statistics: true, 
                    //    edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //            var gridName = "AccountingTnvoiceDetail";


                    //            if (row.InvoiceRateType > 0) {
                    //                var taxAmount = parseFloat(row.TaxAmount || 0);//金额
                    //                var invoiceRateType = parseFloat(row.InvoiceRateType || 0);//税率
                    //                //含税金额 / (1 + 税率) * 税率
                    //                var adjustableTax = taxAmount / (1 + invoiceRateType) * invoiceRateType;
                    //                adjustableTax = Changjie.format45(adjustableTax, Changjie.sysGlobalSettings.pointGlobal2);

                    //                row.AdjustableTax = adjustableTax;
                    //                setCellValue("AdjustableTax", row, index, gridName, adjustableTax);

                    //                publicSetRate();

                    //            }
                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},


                    {
                        label: "含税金额", name: "DisplayTaxAmount", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购发票',
                    },

                    {
                        label: "已抵金额", name: "AccountingInvoiceAmount", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购发票',
                    },

                    {
                        label: "需抵含税金额", name: "TaxAmount", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '采购发票', //statistics: true, 
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {
                                var gridName = "AccountingTnvoiceDetail";

                                var displayTaxAmount = parseFloat(row.DisplayTaxAmount || 0);//显示金额
                                var accountingInvoiceAmount = parseFloat(row.AccountingInvoiceAmount || 0);//已抵扣金额
                                var taxAmount = displayTaxAmount - accountingInvoiceAmount;
                                if (displayTaxAmount > 0) {
                                    if (row.TaxAmount > taxAmount) {
                                        Changjie.alert.warning("金额已超出限制！");
                                        $(obj).css("color", "red");
                                        $(obj).attr("color", "red");
                                        gridverifystate = false;
                                    }
                                    else {
                                        $(obj).css("color", "black");
                                        $(obj).attr("color", "black");
                                        gridverifystate = true;
                                    }
                                }

                                if (row.InvoiceRateType > 0) {

                                    var taxAmount = parseFloat(row.TaxAmount || 0);//金额
                                    var invoiceRateType = parseFloat(row.InvoiceRateType || 0);//税率
                                    //含税金额 / (1 + 税率) * 税率
                                    var adjustableTax = taxAmount / (1 + invoiceRateType) * invoiceRateType;
                                    adjustableTax = Changjie.format45(adjustableTax, Changjie.sysGlobalSettings.pointGlobal2);

                                    row.AdjustableTax = adjustableTax;
                                    setCellValue("AdjustableTax", row, index, gridName, adjustableTax);
                                    publicSetRate();
                                }
                                //else {
                                //    setCellValue("AdjustableTax", row, index, gridName, row.TaxAmount);
                                //}
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: "配票金额", name: "WithTicketAmount", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购发票'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                publicSetRate();
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: "可抵税金", name: "AdjustableTax", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购发票', //statistics: true
                    }
                ],
                mainId: "ID",
                bindTable: "AccountingTnvoiceDetail",
                isEdit: true,
                height: 400,
                toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.ProjectId = projectId;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },

                showchoose: true,
                onChooseEvent: function () {
                    var projectId = $('#ProjectID').mkselectGet();
                    if (!!projectId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "采购发票",
                            url: top.$.rootUrl + "/PurchaseModule/Purchase_Invoice/Dialog?projectId=" + projectId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.InvoiceRateType = data[i].InvoiceType;
                                            row.Basis = "";
                                            // row.TaxAmount = data[i].InvoiceAmount;

                                            row.AccountingInvoiceAmount = data[i].AccountingInvoiceAmount;
                                            row.DisplayTaxAmount = data[i].InvoiceAmount;

                                            var displayTaxAmount = parseFloat(row.DisplayTaxAmount || 0);//显示金额
                                            var AccountingInvoiceAmount = parseFloat(row.AccountingInvoiceAmount || 0);//已抵扣金额

                                            var taxAmount = displayTaxAmount - AccountingInvoiceAmount;
                                            row.TaxAmount = Changjie.format45(taxAmount, Changjie.sysGlobalSettings.pointGlobal2);//可抵金额


                                            row.WithTicketAmount = 0;
                                            // row.AdjustableTax = 0;
                                            row.Invoiceid = data[i].ID;
                                            row.InvoiceType = 1;
                                            var taxAmount = parseFloat(row.TaxAmount || 0);//金额
                                            var invoiceRateType = parseFloat(row.InvoiceRateType || 0);//税率
                                            //含税金额 / (1 + 税率) * 税率
                                            var adjustableTax = taxAmount / (1 + invoiceRateType) * invoiceRateType;
                                            adjustableTax = Changjie.format45(adjustableTax, Changjie.sysGlobalSettings.pointGlobal2);
                                            row.ProjectId = projectId;
                                            row.AdjustableTax = adjustableTax;
                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.MaterialsSource = 1;
                                            rows.push(row);
                                        }

                                        $("#AccountingTnvoiceDetail").jfGridSet("addRows", rows);

                                        publicSetRate();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择项目");
                    }
                },

                showchooses: true,
                onChooseEvents: function () {
                    var projectId = $('#ProjectID').mkselectGet();
                    if (!!projectId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "分包收票",
                            url: top.$.rootUrl + "/CaiWu/Finance_SubcontractInvoice/Dialog?projectId=" + projectId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        console.log(data);

                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};

                                            //row.InvoiceRateType = data[i].InvoiceType;
                                            //row.Basis = "";
                                            //row.TaxAmount = data[i].InvoiceAmount;
                                            //row.WithTicketAmount = 0;
                                            //// row.AdjustableTax = 0;
                                            //row.Invoiceid = data[i].ID;
                                            //row.InvoiceType = 2;
                                            //var taxAmount = parseFloat(row.TaxAmount || 0);//金额
                                            //var invoiceRateType = parseFloat(row.InvoiceRateType || 0);//税率

                                            row.InvoiceRateType = data[i].InvoiceType;
                                            row.Basis = "";
                                            row.WithTicketAmount = 0;
                                            row.Invoiceid = data[i].ID;
                                            row.InvoiceType = 2;
                                            var invoiceRateType = parseFloat(row.InvoiceRateType || 0);//税率

                                            row.AccountingInvoiceAmount = data[i].AccountingInvoiceAmount;
                                            row.DisplayTaxAmount = data[i].InvoiceAmount;

                                            var displayTaxAmount = parseFloat(row.DisplayTaxAmount || 0);//显示金额
                                            var AccountingInvoiceAmount = parseFloat(row.AccountingInvoiceAmount || 0);//已抵扣金额

                                            var taxAmount = displayTaxAmount - AccountingInvoiceAmount;
                                            row.TaxAmount = Changjie.format45(taxAmount, Changjie.sysGlobalSettings.pointGlobal2);//可抵金额


                                            //含税金额 / (1 + 税率) * 税率
                                            var adjustableTax = taxAmount / (1 + invoiceRateType) * invoiceRateType;
                                            adjustableTax = Changjie.format45(adjustableTax, Changjie.sysGlobalSettings.pointGlobal2);
                                            row.ProjectId = projectId;
                                            row.AdjustableTax = adjustableTax;
                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.MaterialsSource = 1;
                                            rows.push(row);
                                        }
                                        $("#AccountingTnvoiceDetail").jfGridSet("addRows", rows);

                                        publicSetRate();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择项目");
                    }
                },

            });

            $('#AccountingTaxDetail').jfGrid({
                headData: [
                    {
                        label: "明细", name: "SubjectName", width: 130, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '税金类型',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: "计税基础", name: "TaxFormula", width: 190, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '税金类型',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: "计税金额", name: "TaxAmount", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '税金类型',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },

                    {
                        label: "税率", name: "Tax", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '税金类型',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "AccountingTaxDetail";

                                if (row.SubjectName != "增值税") {
                                    if (row.TaxAmount > 0) {
                                        var taxAmount = parseFloat(row.TaxAmount || 0);//金额
                                        var tax = row.Tax / 100;
                                        tax = taxAmount * tax;
                                        tax = Changjie.format45(tax, Changjie.sysGlobalSettings.pointGlobal2);
                                        row.ShouldPayTax = tax;
                                        setCellValue("ShouldPayTax", row, index, gridName, row.ShouldPayTax);

                                        //应缴-预缴
                                        var actualTax = parseFloat(row.ShouldPayTax || 0) - parseFloat(row.WithholdingTax || 0);
                                        row.ActualTax = actualTax
                                        setCellValue("ActualTax", row, index, gridName, row.ActualTax);
                                    }

                                    publicSetRate();
                                }

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },

                    {
                        label: "应缴税", name: "ShouldPayTax", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '税金类型'
                    },

                    {
                        label: "预缴税", name: "WithholdingTax", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '税金类型',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                publicSetRate();
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },

                    {
                        label: "实际缴税", name: "ActualTax", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '税金类型',// statistics: true
                    }
                ],
                mainId: "ID",
                bindTable: "AccountingTaxDetail",
                isEdit: true,
                height: 400,
                toolbarposition: "top",

                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.ProjectId = projectId;
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                }
            });
        },

        setRate: function () {

            //var rowsQuantities = $('#AccountingTnvoiceDetail').jfGridGet('rowdatas');

            //var sum_TaxAmount = 0;
            //var sum_WithTicketAmount = 0;
            //var sum_AdjustableTax = 0;

            //for (var i = 0; i < rowsQuantities.length; i++) {
            //    if (!isNaN(rowsQuantities[i].TaxAmount))
            //        sum_TaxAmount += Number(rowsQuantities[i].TaxAmount);
            //    if (!isNaN(rowsQuantities[i].WithTicketAmount))
            //        sum_WithTicketAmount += Number(rowsQuantities[i].WithTicketAmount);
            //    if (!isNaN(rowsQuantities[i].AdjustableTax))
            //        sum_AdjustableTax += Number(rowsQuantities[i].AdjustableTax);
            //}

            //$("#TaxAmountSum").val(parseFloat(sum_TaxAmount).toFixed(Changjie.sysGlobalSettings.pointGlobal2));
            //$("#WithTicketAmountSum").val(parseFloat(sum_WithTicketAmount).toFixed(Changjie.sysGlobalSettings.pointGlobal2));
            //$("#AdjustableTaxSum").val(parseFloat(sum_AdjustableTax).toFixed(Changjie.sysGlobalSettings.pointGlobal2));

            //var invoiceAmount = parseFloat($("#InvoiceAmount").val() || 0);//开票金额
            //var taxAmountSum = parseFloat($("#TaxAmountSum").val() || 0);//含税金额
            //var taxIncomeNotIncluded = parseFloat($("#TaxIncomeNotIncluded").val() || 0);//不含税收入

            //var row = $('#AccountingTaxDetail').jfGridGet('rowdatas');

            //var gridName = "AccountingTaxDetail";

            //var salesTax = parseFloat($("#SalesTax").val() || 0);//销售税金
            //var adjustableTaxSum = parseFloat($("#AdjustableTaxSum").val() || 0);//可抵税金
            //var salesTaxAndadjustableTaxSum = salesTax - adjustableTaxSum;//销售税金-可抵税金
            //var a = 0;
            //var b = 0;
            //var withholdingTaxSum = 0;
            //var accountingTaxDetailSum = 0;

            //for (var i = 0; i < row.length; i++) {

            //    switch (row[i].SubjectName) {
            //        case "无成本扣票税":
            //            var taxAmount = invoiceAmount - taxAmountSum;
            //            taxAmount = Changjie.format45(taxAmount, Changjie.sysGlobalSettings.pointGlobal2);
            //            setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额
            //            //税率
            //            var tax = row[i].Tax / 100;

            //            //应缴税
            //            var taxAmount = parseFloat(taxAmount || 0);//金额
            //            tax = taxAmount * tax;
            //            tax = Changjie.format45(tax, Changjie.sysGlobalSettings.pointGlobal2);
            //            setCellValue("ShouldPayTax", row[i], i, gridName, tax);

            //            //应缴-预缴=实际缴费
            //            var withholdingTax = parseFloat(row[i].WithholdingTax || 0);
            //            withholdingTaxSum += withholdingTax;
            //            var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
            //            actualTax = Changjie.format45(actualTax, Changjie.sysGlobalSettings.pointGlobal2);
            //            accountingTaxDetailSum += actualTax;
            //            setCellValue("ActualTax", row[i], i, gridName, actualTax);

            //            break;
            //        case "增值税":
            //            //应缴增值税-可抵税金-预缴税金
            //            var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
            //            withholdingTaxSum += withholdingTax;

            //            a = withholdingTax;
            //            var taxAmount = Changjie.format45(salesTaxAndadjustableTaxSum - withholdingTax, Changjie.sysGlobalSettings.pointGlobal2);//
            //            setCellValue("TaxAmount", row[i], i, "AccountingTaxDetail", taxAmount);//计税金额
            //            setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", salesTax);//应缴税
            //            var actualTax = Changjie.format45(salesTaxAndadjustableTaxSum, Changjie.sysGlobalSettings.pointGlobal2);
            //            b = actualTax;
            //            accountingTaxDetailSum += actualTax;

            //            setCellValue("ActualTax", row[i], i, gridName, actualTax);//实缴税
            //            break;

            //        case "城建税":
            //            var taxAmount = a + b;
            //            setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额
            //            var tax = row[i].Tax / 100;
            //            var shouldPayTax = Changjie.format45(taxAmount * tax, Changjie.sysGlobalSettings.pointGlobal2);//应缴税
            //            setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

            //            var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
            //            withholdingTaxSum += withholdingTax;

            //            var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
            //            actualTax = Changjie.format45(actualTax, Changjie.sysGlobalSettings.pointGlobal2);
            //            accountingTaxDetailSum += actualTax;
            //            setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税
            //            break;
            //        case "教育费附加":
            //            var taxAmount = a + b;
            //            setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额
            //            var tax = row[i].Tax / 100;
            //            var shouldPayTax = Changjie.format45(taxAmount * tax, Changjie.sysGlobalSettings.pointGlobal2);//应缴税
            //            setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

            //            var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
            //            withholdingTaxSum += withholdingTax;

            //            var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
            //            actualTax = Changjie.format45(actualTax, Changjie.sysGlobalSettings.pointGlobal2);
            //            accountingTaxDetailSum += actualTax;
            //            setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税

            //            break;
            //        case "地方教育费附加": // 预缴税：WithholdingTax  应缴税：ShouldPayTax 税率：Tax  实缴税：ActualTax
            //            var taxAmount = a + b;
            //            setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额
            //            var tax = row[i].Tax / 100;
            //            var shouldPayTax = Changjie.format45(taxAmount * tax, Changjie.sysGlobalSettings.pointGlobal2);//应缴税
            //            setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

            //            var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
            //            withholdingTaxSum += withholdingTax;

            //            var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
            //            actualTax = Changjie.format45(actualTax, Changjie.sysGlobalSettings.pointGlobal2);
            //            accountingTaxDetailSum += actualTax;
            //            setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税

            //            break;

            //        case "个人所得税":
            //            setCellValue("TaxAmount", row[i], i, gridName, taxIncomeNotIncluded);//计税金额
            //            var tax = row[i].Tax / 100;
            //            var shouldPayTax = Changjie.format45(taxIncomeNotIncluded * tax, Changjie.sysGlobalSettings.pointGlobal2);//应缴税
            //            setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

            //            var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
            //            withholdingTaxSum += withholdingTax;

            //            var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
            //            actualTax = Changjie.format45(actualTax, Changjie.sysGlobalSettings.pointGlobal2);
            //            accountingTaxDetailSum += actualTax;

            //            setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税

            //            break;

            //        case "企业所得税":

            //            var taxAmount = Changjie.format45(invoiceAmount * 0.05, Changjie.sysGlobalSettings.pointGlobal2);
            //            setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额

            //            var tax = row[i].Tax / 100;
            //            var shouldPayTax = Changjie.format45(taxAmount * tax, Changjie.sysGlobalSettings.pointGlobal2);//应缴税
            //            setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

            //            var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
            //            withholdingTaxSum += withholdingTax;

            //            var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
            //            actualTax = Changjie.format45(actualTax, Changjie.sysGlobalSettings.pointGlobal2);
            //            accountingTaxDetailSum += actualTax;

            //            setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税


            //            break;
            //        default:
            //            setCellValue("TaxAmount", row[i], i, gridName, invoiceAmount);//计税金额
            //            var tax = row[i].Tax / 100;
            //            var shouldPayTax = Changjie.format45(invoiceAmount * tax, Changjie.sysGlobalSettings.pointGlobal2);//应缴税
            //            setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

            //            var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
            //            withholdingTaxSum += withholdingTax;

            //            var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
            //            actualTax = Changjie.format45(actualTax, Changjie.sysGlobalSettings.pointGlobal2);
            //            accountingTaxDetailSum += actualTax;

            //            setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税
            //    }
            //}
            //var accountingTaxDetailSum = parseFloat(accountingTaxDetailSum).toFixed(Changjie.sysGlobalSettings.pointGlobal2);
            //$("#AccountingTaxDetailSum").val(accountingTaxDetailSum);
            //$("#PrepayTaxSum").val(parseFloat(withholdingTaxSum).toFixed(Changjie.sysGlobalSettings.pointGlobal2));

            //var rateTota = invoiceAmount / accountingTaxDetailSum;
            //$("#RateTotal").val(parseFloat(rateTota).toFixed(Changjie.sysGlobalSettings.pointGlobal2));

        },

        setTax: function () {
            var queryData = [];
            //var temp = {};
            //temp.ID = Changjie.newGuid();
            //temp.SortCode = 1;
            //temp.rowState = 1;
            //temp.EditType = 1;
            //temp.SubjectId = "";
            //temp.SubjectName = "无成本扣票税";
            //temp.TaxFormula = "开票金额-含税金额";
            //var invoiceAmount = parseFloat($("#InvoiceAmount").val() || 0);//开票金额
            //var taxAmountSum = parseFloat($("#taxAmountSum").val() || 0);//含税金额
            //var taxAmount = invoiceAmount - taxAmountSum;
            //taxAmount = Changjie.format45(taxAmount, Changjie.sysGlobalSettings.pointGlobal2);
            //temp.TaxAmount = taxAmount;
            //queryData.push(temp);

            var arr = new Array();
            arr = ["无成本扣票税", "增值税", "城建税", "教育费附加", "地方教育费附加", "印花税（购销）", "工会经费", "水利建设基金", "个人所得税", "企业所得税", "财务费用", "管理费"];

            $.each(arr, function (index, value) {

                var temp = {};
                temp.ID = Changjie.newGuid();
                temp.SortCode = index;
                temp.rowState = 1;
                temp.EditType = 1;
                temp.SubjectId = "";
                temp.SubjectName = value;
                switch (value) {
                    case "无成本扣票税":
                        temp.TaxFormula = "开票金额-含税金额";
                        var invoiceAmount = parseFloat($("#InvoiceAmount").val() || 0);//开票金额
                        var taxAmountSum = parseFloat($("#taxAmountSum").val() || 0);//含税金额
                        var taxAmount = invoiceAmount - taxAmountSum;
                        taxAmount = Changjie.format45(taxAmount, Changjie.sysGlobalSettings.pointGlobal2);
                        temp.TaxAmount = taxAmount;//计税金额
                        break;
                    case "增值税":
                        temp.TaxFormula = "应缴增值税-可抵税金-预缴税金";
                        break;
                    case "城建税":
                        temp.TaxFormula = "（预缴+实缴增值税）*7%";
                        break;
                    case "教育费附加":
                        temp.TaxFormula = "（预缴+实缴增值税）*3%";
                        break;
                    case "地方教育费附加":
                        temp.TaxFormula = "（预缴+实缴增值税）*2%";
                        break;
                    case "印花税（购销）":
                        temp.TaxFormula = "开票金额*0.06%";
                        break;
                    case "工会经费":
                        temp.TaxFormula = "开票金额*0.12%";
                        break;
                    case "水利建设基金":
                        temp.TaxFormula = "开票金额*0.06%";
                        break;
                    case "个人所得税":
                        temp.TaxFormula = "不含税收入*1%";
                        break;
                    case "企业所得税":
                        temp.TaxFormula = "开票额*税率";
                        break;
                    case "财务费用":
                        temp.TaxFormula = "开票金额*0.1%";
                        break;
                    case "管理费":
                        temp.TaxFormula = "开票金额*1%";
                        break;
                    default:
                        temp.TaxFormula = "";
                }

                queryData.push(temp)

            });

            $('#AccountingTaxDetail').jfGridSet('refreshdata', queryData);


            Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'ZCKM0502' }, function (res) {

                //if (res.code == 200) {

                //    var code = 2;
                //    $.each(res.data, function (id, item) {
                //        var temp = {};
                //        temp.ID = Changjie.newGuid();
                //        temp.SortCode = 2;
                //        temp.rowState = 1;
                //        temp.EditType = 1;
                //        temp.SubjectId = item.id;
                //        temp.SubjectName = item.name;
                //        switch (item.name) {
                //            case "增值税":
                //                temp.TaxFormula = "应缴增值税-可抵税金-预缴税金";
                //                break;
                //            case "城建税":
                //                temp.TaxFormula = "（预缴+实缴增值税）*7%";
                //                break;
                //            case "教育费附加":
                //                temp.TaxFormula = "（预缴+实缴增值税）*3%";
                //                break;
                //            case "地方教育费附加":
                //                temp.TaxFormula = "（预缴+实缴增值税）*2%";
                //                break;
                //            case "印花税":
                //                temp.TaxFormula = "开票金额*0.06%";
                //                break;
                //            case "工会经费":
                //                temp.TaxFormula = "开票金额*0.12%";
                //                break;
                //            case "水利建设基金":
                //                temp.TaxFormula = "开票金额*0.06%";
                //                break;
                //            case "个人所得税":
                //                temp.TaxFormula = "不含税收入*1%";
                //                break;
                //            case "企业所得税":
                //                temp.TaxFormula = "开票额*5%*25%";
                //                break;
                //            case "财务费用":
                //                temp.TaxFormula = "开票金额*0.1%";
                //                break;
                //            case "管理费":
                //                temp.TaxFormula = "开票金额*1%";
                //                break;
                //            default:
                //                temp.TaxFormula = "";
                //        }

                //        queryData.push(temp)
                //    });
                //    }

                // $('#AccountingTaxDetail').jfGridSet('refreshdata', queryData);
            })
        },

        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Accounting/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (id == "Accounting") {
                            checkprojectId = data[id].ProjectID;
                            makeinvoicecompanyid = data[id].Company_ID;
                        }

                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);

                            bindEvent();
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (gridverifystate == false) {
            Changjie.alert.warning("数据验证失败。");
            return false;
        }

        $("#ProjectManagerName").val($("#ProjectManager").mkformselectGetText());

        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Accounting/SaveFormPiao?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
//var getFormData = function () {
//        if (!$('body').mkValidform()) {
//            return false;
//        }
//        var deleteList=[];
//        var isgridpass=true;
//        var errorInfos=[];
//        for(var item in subGrid){
//           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
//           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
//           if (!info.isPass) {
//              isgridpass = false;
//              errorInfos.push(info.errorCells);
//           }
//         }
//         if (!isgridpass) {
//             for (var i in errorInfos[0]) {
//                 top.Changjie.alert.error(errorInfos[0][i].Msg);
//             }
//             return false;
//         }
//        var postData = {
//            strEntity: JSON.stringify($('body').mkGetFormData()),
//            deleteList: JSON.stringify(deleteList),
//        };
//     return postData;
//}


var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;
    if (row.EditType == 0) {
        row.EditType = 2;
    }
    $cell.html(value);
    $edit.val(value);
};

var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }

    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Accounting"]').mkGetFormData());
    postData.strpurchase_AccountingTnvoiceDetailList = JSON.stringify($('#AccountingTnvoiceDetail').jfGridGet('rowdatas'));
    postData.strproject_AccountingTaxDetailList = JSON.stringify($('#AccountingTaxDetail').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
var bindEvent = function () {
    $("#ProjectID").on('change', function () {
        var pj = $("#ProjectID").mkselectGetEx();
        if (pj) {
            $("#ProjectName").val(pj.projectname);
            $("#ProjectManager").mkformselectSet(pj.marketingstaffid);
            $("#ProjectManagerName").val(pj.f_realname);
            $("#CustomerName").val(pj.fullname);

            var sqlWhere = " ProjectID='" + pj.project_id + "'";

            $('#Finance_InvoiceRegisterId').mkDataSourceSelect({ code: 'FIR', value: 'id', text: 'invoicecode', strWhere: sqlWhere });
            // $('#ProjectAccount').mkDataSourceSelect({ code: 'ZJZH', value: 'id', text: 'name', strWhere: sqlWhere });

            checkprojectId = pj.project_id;



        }
        else {
            $("#ProjectName").val("");
            $("#ProjectManager").mkformselectSet("");
            $("#ProjectManagerName").val("");
            $("#Finance_InvoiceRegisterId").mkselectSet("");

            $("#MakeInvoiceCompanyId").val("");
            $("#InvoiceAmount").val("");
            $("#TaxIncomeNotIncluded").val("");
            $("#Rate,#SalesTax,#TaxRateExclusiveText,#Rate,#SalesTax").val("");
            $("#TaxRateExclusive").mkselectSet("");
            $("#CompanyAccount").mkselectSet("");
            $("#ProjectAccount").mkselectSet("");

            $("#Prepay,#VatDeductible,#TaxAmountSum,#WithTicketAmountSum,#AdjustableTaxSum,#AccountingTaxDetailSum,#PrepayTaxSum,#RateTotal").val("");
            $("#NoCostTax").val("0");

        }

    });

    $("#ProjectAccountName").on("click", function () {
        top.Changjie.layerForm({
            id: "selectContract",
            title: "选择账户",
            url: top.$.rootUrl + '/CaiWu/Base_FundAccount/Dialog?projectId=' + checkprojectId,
            width: 800,
            hegiht: 294,
            callBack: function (id) {
                return top[id].acceptClick(function (data) {
                    if (data) {
                        $("#ProjectAccountName").val(data.Bank);
                        $("#ProjectAccount").val(data.ID);
                    }
                });
            }
        });
    });


    $("#Finance_InvoiceRegisterId").on('change', function () {

        var selectData = $("#Finance_InvoiceRegisterId").mkselectGetEx();
        if (selectData) {
            $("#MakeInvoiceCompanyId").val(selectData.f_fullname);//开票单位

            // var sqlWhereCompany = " CompanyId='" + selectData.makeinvoicecompanyid + "'";
            //  $('#CompanyAccount').mkDataSourceSelect({ code: 'ZJZH', value: 'id', text: 'name', strWhere: sqlWhereCompany });

            makeinvoicecompanyid = selectData.makeinvoicecompanyid;


            var invoiceamount = parseFloat(selectData.invoiceamount);
            $("#InvoiceAmount").val(invoiceamount);//开票金额

            var rate = parseFloat(selectData.invoicetype);//税率
            $("#Rate").val(rate * 100);//显示税率

            //开票金额 / (1 + 税率) * 税率
            var salesTax = invoiceamount / (1 + rate) * rate;

            salesTax = parseFloat(top.Changjie.format45(salesTax, top.Changjie.sysGlobalSettings.pointGlobal2));
            $("#SalesTax").val(salesTax);//销售税金

            //开票金额-销售税金
            var taxIncomeNotIncluded = invoiceamount - salesTax;
            taxIncomeNotIncluded = top.Changjie.format45(taxIncomeNotIncluded, top.Changjie.sysGlobalSettings.pointGlobal2);
            $("#TaxIncomeNotIncluded").val(taxIncomeNotIncluded);//不含税收入

            var selectData = $("#TaxRateExclusive").mkselectGetEx();
            if (selectData) {
                var tar = parseFloat(selectData.id)
                var invoiceAmount = parseFloat($("#TaxIncomeNotIncluded").val() || 0);
                var prepay = top.Changjie.format45(invoiceAmount * tar, top.Changjie.sysGlobalSettings.pointGlobal2);
                $("#Prepay").val(prepay);//应预缴
                $("#TaxRateExclusive").val(selectData.id);//税率
                $("#TaxRateExclusiveText").val(selectData.text);//税率文本

                //销售税金 - 应预缴
                var vatDeductible = salesTax - prepay;
                vatDeductible = top.Changjie.format45(vatDeductible, top.Changjie.sysGlobalSettings.pointGlobal2);
                $("#VatDeductible").val(vatDeductible).trigger('change');;//需抵扣增值税
            }
            else {

            }

            var taxAmountSum = parseFloat($("#TaxAmountSum").val() || 0);//含税金额
            var taxAmount = invoiceamount - taxAmountSum;
            taxAmount = top.Changjie.format45(taxAmount, top.Changjie.sysGlobalSettings.pointGlobal2);

            var row = $('#AccountingTaxDetail').jfGridGet('rowdatas')[0];
            setCellValue("TaxAmount", row, 0, "AccountingTaxDetail", taxAmount);

            publicSetRate();
        }
        else {

            $("#MakeInvoiceCompanyId").val("");
            $("#InvoiceAmount").val("");
            $("#TaxIncomeNotIncluded").val("");
            $("#Rate,#SalesTax,#TaxRateExclusiveText,#Rate,#SalesTax").val("");
            $("#TaxRateExclusive").mkselectSet("");
            $("#CompanyAccount").mkselectSet("");

            $("#Prepay,#VatDeductible,#TaxAmountSum,#WithTicketAmountSum,#AdjustableTaxSum,#AccountingTaxDetailSum,#PrepayTaxSum,#RateTotal").val("");
            $("#NoCostTax").val("0");

        }
    });

    $("#CompanyAccountName").on("click", function () {
        top.Changjie.layerForm({
            id: "selectContract",
            title: "选择账户",
            url: top.$.rootUrl + '/CaiWu/Base_FundAccount/Dialog?type=ZjzhNoProject&companyId=' + makeinvoicecompanyid,
            width: 800,
            hegiht: 294,
            callBack: function (id) {
                return top[id].acceptClick(function (data) {
                    if (data) {
                        $("#CompanyAccountName").val(data.Bank);
                        $("#CompanyAccount").val(data.ID);
                    }
                });
            }
        });
    });

}
var publicSetRate = function () {
    var rowsQuantities = $('#AccountingTnvoiceDetail').jfGridGet('rowdatas');

    var sum_TaxAmount = 0;
    var sum_WithTicketAmount = 0;
    var sum_AdjustableTax = 0;

    for (var i = 0; i < rowsQuantities.length; i++) {
        if (!isNaN(rowsQuantities[i].TaxAmount))
            sum_TaxAmount += Number(rowsQuantities[i].TaxAmount);
        if (!isNaN(rowsQuantities[i].WithTicketAmount))
            sum_WithTicketAmount += Number(rowsQuantities[i].WithTicketAmount);
        if (!isNaN(rowsQuantities[i].AdjustableTax))
            sum_AdjustableTax += Number(rowsQuantities[i].AdjustableTax);
    }

    $("#TaxAmountSum").val(parseFloat(sum_TaxAmount).toFixed(top.Changjie.sysGlobalSettings.pointGlobal2));
    $("#WithTicketAmountSum").val(parseFloat(sum_WithTicketAmount).toFixed(top.Changjie.sysGlobalSettings.pointGlobal2));
    $("#AdjustableTaxSum").val(parseFloat(sum_AdjustableTax).toFixed(top.Changjie.sysGlobalSettings.pointGlobal2));

    var invoiceAmount = parseFloat($("#InvoiceAmount").val() || 0);//开票金额
    var taxAmountSum = parseFloat($("#TaxAmountSum").val() || 0);//含税金额
    var taxIncomeNotIncluded = parseFloat($("#TaxIncomeNotIncluded").val() || 0);//不含税收入

    var row = $('#AccountingTaxDetail').jfGridGet('rowdatas');

    var gridName = "AccountingTaxDetail";

    var rate = parseFloat($("#Rate").val());//税率

    var salesTax = parseFloat($("#SalesTax").val() || 0);//销售税金
    var adjustableTaxSum = parseFloat($("#AdjustableTaxSum").val() || 0);//可抵税金
    var salesTaxAndadjustableTaxSum = salesTax - adjustableTaxSum;//销售税金-可抵税金
    var a = 0;
    var b = 0;
    var withholdingTaxSum = 0;
    var accountingTaxDetailSum = 0;

    for (var i = 0; i < row.length; i++) {

        switch (row[i].SubjectName) {
            case "无成本扣票税":
                var taxAmount = invoiceAmount - taxAmountSum;
                taxAmount = top.Changjie.format45(taxAmount, top.Changjie.sysGlobalSettings.pointGlobal2);
                setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额
                //税率
                var tax = row[i].Tax / 100;

                //应缴税
                var taxAmount = parseFloat(taxAmount || 0);//金额
                tax = taxAmount * tax;
                tax = top.Changjie.format45(tax, top.Changjie.sysGlobalSettings.pointGlobal2);
                setCellValue("ShouldPayTax", row[i], i, gridName, tax);

                //应缴-预缴=实际缴费
                var withholdingTax = parseFloat(row[i].WithholdingTax || 0);
                withholdingTaxSum += withholdingTax;
                var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
                actualTax = top.Changjie.format45(actualTax, top.Changjie.sysGlobalSettings.pointGlobal2);
                accountingTaxDetailSum += actualTax;
                setCellValue("ActualTax", row[i], i, gridName, actualTax);

                break;
            case "增值税":
                //应缴增值税-可抵税金-预缴税金
                var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
                withholdingTaxSum += withholdingTax;

                a = withholdingTax;
                var taxAmount = top.Changjie.format45(salesTaxAndadjustableTaxSum - withholdingTax, top.Changjie.sysGlobalSettings.pointGlobal2);//
                setCellValue("TaxAmount", row[i], i, "AccountingTaxDetail", taxIncomeNotIncluded);//计税金额
                //setCellValue("Tax", row[i], i, "AccountingTaxDetail", rate);//计税金额

                setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", salesTax);//应缴税
                var actualTax = top.Changjie.format45(salesTax - sum_AdjustableTax - withholdingTax, top.Changjie.sysGlobalSettings.pointGlobal2);
                b = actualTax;
                accountingTaxDetailSum += actualTax;

                setCellValue("ActualTax", row[i], i, gridName, actualTax);//实缴税
                break;

            case "城建税":
                var taxAmount = a + b;
                setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额
                var tax = row[i].Tax / 100;
                var shouldPayTax = top.Changjie.format45(taxAmount * tax, top.Changjie.sysGlobalSettings.pointGlobal2);//应缴税
                setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

                var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
                withholdingTaxSum += withholdingTax;

                var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
                actualTax = top.Changjie.format45(actualTax, top.Changjie.sysGlobalSettings.pointGlobal2);
                accountingTaxDetailSum += actualTax;
                setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税
                break;
            case "教育费附加":
                var taxAmount = a + b;
                setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额
                var tax = row[i].Tax / 100;
                var shouldPayTax = top.Changjie.format45(taxAmount * tax, top.Changjie.sysGlobalSettings.pointGlobal2);//应缴税
                setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

                var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
                withholdingTaxSum += withholdingTax;

                var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
                actualTax = top.Changjie.format45(actualTax, top.Changjie.sysGlobalSettings.pointGlobal2);
                accountingTaxDetailSum += actualTax;
                setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税

                break;
            case "地方教育费附加": // 预缴税：WithholdingTax  应缴税：ShouldPayTax 税率：Tax  实缴税：ActualTax
                var taxAmount = a + b;
                setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额
                var tax = row[i].Tax / 100;
                var shouldPayTax = top.Changjie.format45(taxAmount * tax, top.Changjie.sysGlobalSettings.pointGlobal2);//应缴税
                setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

                var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
                withholdingTaxSum += withholdingTax;

                var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
                actualTax = top.Changjie.format45(actualTax, top.Changjie.sysGlobalSettings.pointGlobal2);
                accountingTaxDetailSum += actualTax;
                setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税

                break;

            case "个人所得税":
                setCellValue("TaxAmount", row[i], i, gridName, taxIncomeNotIncluded);//计税金额
                var tax = row[i].Tax / 100;
                var shouldPayTax = top.Changjie.format45(taxIncomeNotIncluded * tax, top.Changjie.sysGlobalSettings.pointGlobal2);//应缴税
                setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

                var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
                withholdingTaxSum += withholdingTax;

                var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
                actualTax = top.Changjie.format45(actualTax, top.Changjie.sysGlobalSettings.pointGlobal2);
                accountingTaxDetailSum += actualTax;

                setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税

                break;

            case "企业所得税":

                var taxAmount = top.Changjie.format45(invoiceAmount, top.Changjie.sysGlobalSettings.pointGlobal2);
                setCellValue("TaxAmount", row[i], i, gridName, taxAmount);//计税金额

                var tax = row[i].Tax / 100;
                var shouldPayTax = top.Changjie.format45(taxAmount * tax, top.Changjie.sysGlobalSettings.pointGlobal2);//应缴税
                setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

                var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
                withholdingTaxSum += withholdingTax;

                var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
                actualTax = top.Changjie.format45(actualTax, top.Changjie.sysGlobalSettings.pointGlobal2);
                accountingTaxDetailSum += actualTax;

                setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税


                break;
            default:
                setCellValue("TaxAmount", row[i], i, gridName, invoiceAmount);//计税金额
                var tax = row[i].Tax / 100;
                var shouldPayTax = top.Changjie.format45(invoiceAmount * tax, top.Changjie.sysGlobalSettings.pointGlobal2);//应缴税
                setCellValue("ShouldPayTax", row[i], i, "AccountingTaxDetail", shouldPayTax);//应缴税

                var withholdingTax = parseFloat(row[i].WithholdingTax || 0);//预缴税
                withholdingTaxSum += withholdingTax;

                var actualTax = parseFloat(row[i].ShouldPayTax || 0) - withholdingTax;
                actualTax = top.Changjie.format45(actualTax, top.Changjie.sysGlobalSettings.pointGlobal2);
                accountingTaxDetailSum += actualTax;

                setCellValue("ActualTax", row[i], i, "AccountingTaxDetail", actualTax);//实缴税
        }
    }
    var accountingTaxDetailSum = parseFloat(accountingTaxDetailSum).toFixed(top.Changjie.sysGlobalSettings.pointGlobal2);
    $("#AccountingTaxDetailSum").val(accountingTaxDetailSum);
    $("#PrepayTaxSum").val(parseFloat(withholdingTaxSum).toFixed(top.Changjie.sysGlobalSettings.pointGlobal2));

    //var rateTota = accountingTaxDetailSum / invoiceAmount;
    //$("#RateTotal").val(parseFloat(rateTota).toFixed(top.Changjie.sysGlobalSettings.pointGlobal2));

    var rateTota = accountingTaxDetailSum / invoiceAmount;
    var parseFloatrateTota = parseFloat(rateTota).toFixed(top.Changjie.sysGlobalSettings.pointGlobal);

    $("#RateTotal").val(parseFloatrateTota);
    var rateTotalDisplay = parseFloat(parseFloatrateTota * 100).toFixed(top.Changjie.sysGlobalSettings.pointGlobal2);

    $("#RateTotalDisplay").val(rateTotalDisplay + "%");


    //销售税金 - 预缴税费合计
    var vatDeductible = salesTax - withholdingTaxSum;
    vatDeductible = top.Changjie.format45(vatDeductible, top.Changjie.sysGlobalSettings.pointGlobal2);
    $("#VatDeductible").val(vatDeductible);//需抵扣增值税


};

