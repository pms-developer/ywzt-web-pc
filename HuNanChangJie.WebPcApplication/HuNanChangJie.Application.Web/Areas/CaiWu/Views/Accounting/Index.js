﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-12-14 11:43
 * 描  述：税费核算
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var checkJson = "";

var auditPassEventBat = function (keyValue) {

    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Accounting/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Accounting/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 400);
            //$('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });


            // 刷新 
            $('#refresh').on('click', function () {
                location.reload();
            });

            // 新增  
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增税费核算',
                    url: top.$.rootUrl + '/CaiWu/Accounting/Form?projectId=' + projectId,
                    width: 900,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

            $("#piao").on("click", function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: "录入发票",
                        isShowConfirmBtn: true,
                        url: top.$.rootUrl + '/CaiWu/Accounting/FormPiao?keyValue=' + keyValue,
                        width: 900,
                        height: 800,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });

            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Accounting/GetPageList',
                headData: [
                    {
                        label: "项目", name: "ProjectID", width: 220, align: "left",
                    },
                    { label: "客户名称", name: "CustomerName", width: 220, align: "left" },
                    {
                        label: "开票时间", name: "MakeInvoiceDate", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        } },
                    {
                        label: "核算时间", name: "CreationDate", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }},
                    { label: "实缴税费合计", name: "AccountingTaxDetailSum", width: 80, align: "left" },
                    { label: "预缴税费合计", name: "PrepayTaxSum", width: 80, align: "left" },
                    { label: "制单人", name: "CreationName", width: 70, align: "center" },
                    { label: "综合税率", name: "RateTotalDisplay", width: 70, align: "center" },
                    { label: "项目经理", name: "ProjectManagerName", width: 70, align: "center" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 70, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },

                    //{ label: "开票登记", name: "Finance_InvoiceRegisterId", width: 150, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('custmerData', {
                    //             url: '/SystemModule/DataSource/GetDataTable?code=' + 'FIR',
                    //             key: value,
                    //             keyId: 'id',
                    //             callback: function (_data) {
                    //                 callback(_data['invoicecode']);
                    //             }
                    //         });
                    //    }},

                    { label: "开票登记", name: "InvoiceCode", width: 80, align: "left" },
                    { label: "开票单位", name: "MakeInvoiceCompanyId", width: 200, align: "left" },
                    { label: "开票金额", name: "InvoiceAmount", width: 100, align: "left" },

                    //{ label: "不含税收入", name: "TaxIncomeNotIncluded", width: 100, align: "left"},
                    { label: "税率", name: "Rate", width: 100, align: "left" },
                    //{ label: "销售税金", name: "SalesTax", width: 100, align: "left"},
                    { label: "应预缴", name: "Prepay", width: 100, align: "left" },
                    //{ label: "需抵扣增值税", name: "VatDeductible", width: 100, align: "left" },
                    
                ],
                mainId: 'ID',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson); 
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Accounting/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/CaiWu/Accounting/Form?keyValue=' + keyValue + '&viewState=' + viewState,
            width: 900,
            height: 800,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
