﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-11 16:04
 * 描  述：对内付款单
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Finance_InternalPayment";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_InternalPayment/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InternalPayment/Audit', { keyValue: keyValue }, function (data) {
       
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InternalPayment/UnAudit', { keyValue: keyValue }, function (data) {
       
    });
};

var source = request('source');
var keyid = request('keyid');

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Finance_InternalPaymentDetails', "gridId": 'Finance_InternalPaymentDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }

            if (source && keyid) {
                Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InternalPayment/LoadDanJuData', { source: source, keyid: keyid }, function (res) {
                    if (res.code == 200) {
                        var data = res.data;
                        var rows = [];
                        var row = {};
                        row.PayeeName = data.realname;
                        row.Code = data.code;
                        row.BuMeng = data.deptid;
                        row.BaoXiaoId = data.ID;
                        row.BaoXiaoType = data.mode;
                        row.PayeeId = data.userid;
                        row.Amount = data.amount;
                        row.rowState = 1;
                        row.ID = Changjie.newGuid();
                        row.EditType = 1;
                        rows.push(row);
                        $("#Finance_InternalPaymentDetails").jfGridSet("addRows", rows);
                    }
                });
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0016' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_InternalPaymentDetails').jfGrid({
                headData: [
                    {
                        label: '收款人', name: 'PayeeName', width: 80, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                    },

                    //{
                    //    label: '收款人', name: 'PayeeName', width: 80, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    //    datatype: 'string', tabname: '付款明细'
                    //    , edit: {
                    //        type: 'layer',
                    //        change: function (data, rownum, selectData) {
                    //            data.PayeeId = selectData.f_userid;
                    //            data.PayeeName = selectData.f_realname;
                    //            data.BuMeng = selectData.f_departmentid;
                    //            $('#Finance_InternalPaymentDetails').jfGridSet('updateRow', rownum);
                    //        },
                    //        op: {
                    //            width: 500,
                    //            height: 300,
                    //            listFieldInfo: [
                    //                { label: "所在部门", name: "f_fullname", width: 100, align: "left" },
                    //                { label: "人员姓名", name: "f_realname", width: 100, align: "left" },
                    //            ],
                    //            url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                    //            param: { code: 'User' }
                    //        }
                    //    }
                    //},

                    {
                        label: '单据编号', name: 'Code', width: 130, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                    },
                    {
                        label: "部门", name: "BuMeng", width: 120, cellStyle: { 'text-align': 'left' }, datatype: 'string', tabname: '付款明细',
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('department', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: '单据金额', name: 'Amount', width: 80, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                    },
                    //{
                    //    label: '待付金额', name: 'WaitPay', width: 80, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    //    datatype: 'string', tabname: '付款明细'
                    //},
                    {
                        label: '本次付款金额', name: 'PaymentAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'PaymentAmount', required: '1',
                        datatype: 'float', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '支付方式', name: 'PaymentType', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataItem',
                            code: 'ZFFS'
                        }
                    },
                    {
                        label: '单据默认付款账户', name: 'DefaultAccountName', width: 120
                    },
                    //{
                    //    label: '账户名称', name: 'BankId', width:120,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                    //    datatype:'string', tabname:'付款明细' 
                    //    ,edit:{
                    //        type: 'select',
                    //        change: function (row, index, item, oldValue, colname, headData) {
                    //            row.OpenBank = item.bank;
                    //            row.BankCode = item.bankcode;
                    //            row.Holder = item.holder;
                    //            $('#Finance_InternalPaymentDetails').jfGridSet("updateRow", index)
                    //        },
                    //        datatype: 'dataSource',
                    //        code: 'ZJZH',
                    //        op:{
                    //            value: 'id',
                    //            text:'name',
                    //            title:'name'
                    //        }
                    //    }
                    //},

                    {
                        label: '账户名称', name: 'BankNames', width: 180, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'layer',
                            change: function (row, index, item, oldValue, colname, headData) {
                                row.BankId = item.id;
                                row.BankNames = item.name;
                                row.OpenBank = item.bank;
                                row.BankCode = item.bankcode;
                                row.Holder = item.holder;
                                $('#Finance_InternalPaymentDetails').jfGridSet("updateRow", index)
                            },
                            op: {
                                width: 600,
                                height: 400,
                                listFieldInfo: [
                                    { label: "账户名称", name: "name", width: 150, align: "left" },
                                    { label: "开户银行", name: "bank", width: 150, align: "left" },
                                    { label: "账号", name: "bankcode", width: 150, align: "left" },
                                    { label: "可用余额", name: "usablebalance", width: 150, align: "left" },
                                    { label: "余额", name: "balance", width: 150, align: "left" },
                                    { label: "冻结额", name: "freezed", width: 150, align: "left" },
                                    { label: "项目经理", name: "f_realname", width: 150, align: "left" },
                                    { label: "开户人", name: "holder", width: 150, align: "left" },
                                    {
                                        label: "所属公司", name: "companyid", width: 150, align: "left",
                                        formatterAsync: function (callback, value, row, op, $cell) {
                                            Changjie.clientdata.getAsync('company', {
                                                key: value,
                                                callback: function (_data) {
                                                    callback(_data.name);
                                                }
                                            });
                                        }
                                    },
                                ],
                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: {
                                    code: 'ZJZH'
                                }
                            }
                        }
                    },

                    {
                        label: '开户银行', name: 'OpenBank', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'

                    },
                    {
                        label: '账号名称', name: 'Holder', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                    },

                    {
                        label: '账号', name: 'BankCode', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'

                    },

                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_InternalPaymentDetails",
                isEdit: true,
                height: 300, toolbarposition: "top",
                showadd: false, showchoose: true, onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectDanJu",
                        width: 800,
                        height: 400,
                        title: "选择付款明细",
                        url: top.$.rootUrl + "/CaiWu/Finance_InternalPayment/Dialog",
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.PayeeName = data[i].realname;
                                        row.Code = data[i].code;
                                        row.BuMeng = data[i].deptid;
                                        row.BaoXiaoId = data[i].ID;
                                        row.BaoXiaoType = data[i].mode;
                                        row.PayeeId = data[i].userid;
                                        row.Amount = data[i].amount;
                                        row.BankId = data[i].AccountId;
                                        row.DefaultAccountName = data[i].AccountName;
                                        row.DefaultAccountID = data[i].AccountId;
                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        rows.push(row);
                                    }

                                    $("#Finance_InternalPaymentDetails").jfGridSet("addRows", rows);
                                }
                            });
                        }
                    });
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_InternalPayment/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_InternalPayment/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Finance_InternalPayment"]').mkGetFormData());
    postData.strfinance_InternalPaymentDetailsList = JSON.stringify($('#Finance_InternalPaymentDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
