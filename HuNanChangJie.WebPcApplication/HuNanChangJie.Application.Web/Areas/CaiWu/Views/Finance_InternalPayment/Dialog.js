﻿var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {

            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_InternalPayment/GetFuKuanList',
                headData: [
                    { label: "单据编号", name: "code", width: 100, align: "left" },
                    { label: "收款名称", name: "shoukuanmingcheng", width: 100, align: "center" },
                    { label: "收款人", name: "realname", width: 100, align: "center" },
                    { label: "批准金额", name: "amount", width: 100, align: "left",},
                    { label: "待付金额", name: "waitpay", width: 100, align: "left"},
                    { label: "单据类型", name: "mode", width: 100, align: "center" },
                    { label:"付款账户",name:"AccountName",width:100,align:"center"},
                    {
                        label: "单据时间", name: "sdate", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true,
                height: 280,
                isMultiselect:true
            });
            page.search();
            
        },
        search: function (param) {
            param = param || {};
            //$('#gridtable').jfGridSet('reload');//{ queryJson: JSON.stringify(param) }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};