﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-11 16:04
 * 描  述：对内付款单
 */
var refreshGirdData;
var formId = request("formId");
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InternalPayment/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var checkDataEvent = function (keyValue) {
    var checkflag = true;
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InternalPayment/BeforeSendingCheckData', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            var info = data.data;
            if (!!info.Success) {
                checkflag = true;
            }
            else {
                top.Changjie.alert.error(info.Message);
                checkflag = false;
            }
        }
    });
    return checkflag;
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_InternalPayment/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();

            page.search();
        },
        bind: function () {
            // 时间搜索框
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 320, 600);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });


            $('#AuditStatus').mkselect({
                type: 'default',
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ]
            });

            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增对内付款单',
                    url: top.$.rootUrl + '/CaiWu/Finance_InternalPayment/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_InternalPayment/GetPageList',
                headData: [
                    { label: "付款单号", name: "Code", width: 140, align: "left"},
                    { label: "经办人", name: "OperatorId", width: 80, align: "center"},
                    {
                        label: "付款日期", name: "PaymentDate", width: 80, align: "center", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }},
                    { label: "付款金额", name: "PaymentAmount", width: 120, align: "center"},{
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "摘要", name: "Abstract", width: 400, align: "left"},
                    
                ],
                mainId:'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            //param.StartTime = startTime;
            //param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Finance_InternalPayment/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/CaiWu/Finance_InternalPayment/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
