﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-23 12:14
 * 描  述：财务 分包收票
 */
var refreshGirdData;
var formId = request("formId");
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData();
            //Changjie.clientdata.sourceData.load("DZDJ");
        }
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData();
            //Changjie.clientdata.sourceData.load("DZDJ");
        }
        else
            top.Changjie.alert.error(data.info);
    });
};
var projectId = request('projectId');

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 400);
            $('#CollectCompanyId').mkDataItemSelect({ code: 'SPDW' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增分包收票',
                    url: top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/Form?projectId=' + projectId,
                    width: 1000,
                    height: 750,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/GetPageList',
                headData: [
                    {
                        label: "分包合同", name: "ProjectSubcontractId", width: 300, align: "left"
                    },
                    { label: "发票金额", name: "InvoiceAmount", width: 100, align: "left" },
                    {
                        label: "发票类型", name: "InvoiceType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FPLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "税额", name: "TaxAmount", width: 100, align: "left" },
                    {
                        label: "登记时间", name: "RegisterDate", width: 80, align: "left", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    {
                        label: "开票时间", name: "MakeDate", width: 80, align: "left", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },

                    {
                        label: "经办人", name: "Creation_Id", width: 70, align: "left",
                    },
                    {
                        label: "收到发票日期", name: "ReceiveDate", width: 80, align: "left", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    { label: "发票价税合计", name: "TotalTaxAmount", width: 100, align: "left" },
                    { label: "摘要", name: "Abstract", width: 300, align: "left" },
                ],
                mainId: 'ID',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '分包收票',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/Form?keyValue=' + keyValue + '&viewState=' + viewState + '&projectId=' + projectId + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
