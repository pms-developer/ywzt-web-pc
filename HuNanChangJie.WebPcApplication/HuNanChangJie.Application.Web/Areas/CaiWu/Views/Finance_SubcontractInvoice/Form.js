﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-23 12:14
 * 描  述：财务 分包收票
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Finance_SubcontractInvoice";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var ocrCallBack;
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/Audit', { keyValue: keyValue }, function (data) {
      
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/UnAudit', { keyValue: keyValue }, function (data) {
       
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('#invoice_form_tabs').mkFormTab();
            $('#invoice_form_tabs ul li').eq(0).trigger('click');

            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Finance_SubcontractInvoice_Certificate', "gridId": 'Finance_SubcontractInvoice_Certificate' });
                subGrid.push({ "tableName": 'Finance_SubcontractInvoice_Invoice', "gridId": 'Finance_SubcontractInvoice_Invoice' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
            var ctltype11 = $("#TotalAmount").attr("type");;
            var ctltype12 = $("#TotalTax").attr("type");;
            if (ctltype11 == 'text' && ctltype12 == 'text') {
                $('#TotalAmount').on('input propertychange', function () {
                    var value1 = $(this).val();
                    var value2 = $('#TotalTax').val();
                    $('#TotalTaxAmount').val(top.Changjie.simpleMath(value1, value2, 'add', 2));
                });
                $('#TotalTax').on('input propertychange', function () {
                    var value1 = $('#TotalAmount').val();
                    var value2 = $(this).val();
                    $('#TotalTaxAmount').val(top.Changjie.simpleMath(value1, value2, 'add', 2));
                });
            }
            else if (ctltype11 == 'text' && ctltype12 == 'mkselect') {
                $('#TotalAmount').on('input propertychange', function () {
                    var value1 = $(this).val();
                    var value2 = $('#TotalTax').mkselectGetText();
                    $('#TotalTaxAmount').val(top.Changjie.simpleMath(value1, value2, 'add', 2));
                });
                $('#TotalTax').mkselect().on('change', function () {
                    var value1 = $('#TotalAmount').val();
                    var value2 = $(this).mkselectGetText();
                    $('#TotalTaxAmount').val(top.Changjie.simpleMath(value1, value2, 'add', 2));
                });
            }
            else if (ctltype11 == 'mkselect' && ctltype12 == 'text') {
                $('#TotalAmount').mkselect().on('change', function () {
                    var value1 = $(this).mkselectGetText();
                    var value2 = $('#TotalTax').val();
                    $('#TotalTaxAmount').val(top.Changjie.simpleMath(value1, value2, 'add', 2));
                });
                $('#TotalTax').on('input propertychange', function () {
                    var value1 = $('#TotalAmount').mkselectGetText();
                    var value2 = $(this).val();
                    $('#TotalTaxAmount').val(top.Changjie.simpleMath(value1, value2, 'add', 2));
                });
            }



        },
        calc: function () {

            if ($("#TaxRate").val() != "" && $("#InvoiceAmount").val() != "") {

                var TaxRate = parseFloat(top.Changjie.getFloatValue($("#TaxRate").val()));
                var InvoiceAmount = top.Changjie.getFloatValue($("#InvoiceAmount").val());
               
                //税额 （税额公式为: 金额 / (1 + 税率) * 税率）
                var taxAmount = (InvoiceAmount / (1 + TaxRate) * TaxRate).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                $("#TaxAmount").val(taxAmount || 0);
            }
            else {
                $("#TaxAmount").val("")
            }
        },
        bind: function () {
            $('#ProjectMode').mkDataItemSelect({ code: 'ProjectMode' });
            $("#invoiceocr").on("click", function () {
                Changjie.layerForm({
                    id: 'autoinvoiceocr',
                    title: '发票识别',
                    url: top.$.rootUrl + '/SystemModule/Ocr/InvoiceOcr',
                    width: 1000,
                    height: 700,
                    callBack: function (id) {
                        return top[id].acceptClick(ocrCallBack);
                    }
                });
            });

            var sql = "";
            if (projectId) {
                sql += " ProjectID='" + projectId + "' ";
            }

            $('#Base_SubcontractingUnit').mkselect({ value: 'yi', text: 'unitname' });
            Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'FBHTLB', strWhere: sql }, function (data) {

                var yiIds = [], dataArr = [];
                $.each(data.data, function (ii, ee) {
                    if (ee.yi && ee.unitname && yiIds.indexOf(ee.yi) == -1) {
                        dataArr.push(ee);
                    }
                    yiIds.push(ee.yi);
                })
                $('#Base_SubcontractingUnit').mkselectRefresh({ data: dataArr });
            });

            $('#ProjectSubcontractId').mkselect({ url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', param: { code: 'FBHTLB', strWhere: sql }, value: 'id', text: 'fulltitle', title: 'fulltitle' });
            //$('#Base_SubcontractingUnit').mkDataSourceSelect({ code: 'FBDW', value: 'id', text: 'name' });

            //$('#ProjectSubcontractId').mkselect({});


            $('#ProjectSubcontractId').on("change", function () {
                var subcontractEx = $('#ProjectSubcontractId').mkselectGetEx();

                console.log(subcontractEx, "abc")
                if (subcontractEx) {
                    $("#ProjectID").val(subcontractEx.projectid)
                    $("#ProjectName").val(subcontractEx.projectname)
                    $("#b").val(subcontractEx.totalamount)
                    $("#a").val(subcontractEx.code)
                    $('#ProjectMode').mkselectSet(subcontractEx.pmode)
                }
                else {
                    $("#ProjectID").val("")
                    $("#ProjectName").val("")
                    $("#b").val("")
                    $("#a").val("")
                }

                //var u = $('#Base_SubcontractingUnit').mkselectGetEx();
                //if (!u) {
                //    $("#Base_SubcontractingUnit").mkselectSet(c.yi)
                //}
            })
            //$('#ProjectSubcontractId').mkselect({ url: top.$.rootUrl + '/ProjectModule/ProjectContract/GetPageList', param: { queryJson: JSON.stringify(tparam), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, value: 'rows.ID', text: 'rows.Name', title: 'rows.Name' });

            //param.ProjectId = projectId;
            //Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectContract/GetPageList', { queryJson: JSON.stringify(param), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (data) {
            //});

            $('#InvoiceType').on("change", function () {
                var c = $('#InvoiceType').mkselectGet();
                $("#TaxRate").val(c);

                page.calc();
            })

            $("#InvoiceAmount").on('input propertychange', function () {
                page.calc();
            })


            $('#InvoiceType').mkDataItemSelect({ code: 'FPLX' });
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#JingBanRen').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);
            $('#CollectCompanyId').mkDataItemSelect({ code: 'SPDW' });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_SubcontractInvoice_Invoice').jfGrid({
                headData: [
                    {
                        label: '货物或应税劳务·服务名称', name: 'Name', width: 150, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '发票信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'Standards', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '发票信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单位', name: 'Unit', width: 80, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '发票信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单价', name: 'UnitPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '发票信息', inlineOperation: { to: [{ "t1": "UnitPrice", "t2": "Num", "to": "Amount", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '数量', name: 'Num', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '发票信息', inlineOperation: { to: [{ "t1": "UnitPrice", "t2": "Num", "to": "Amount", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '金额', name: 'Amount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'TotalAmount', required: '1',
                        datatype: 'float', tabname: '发票信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {

                        label: '税率', name: 'TaxRate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '发票信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '税额', name: 'TaxAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'TotalTax', required: '1',
                        datatype: 'float', tabname: '发票信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_SubcontractInvoice_Invoice",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#Finance_SubcontractInvoice_Certificate').jfGrid({
                headData: [
                    {
                        label: '原凭证号', name: 'OriginalCertificate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '税种', name: 'TaxCategory', width: 160, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'ZCKM0502',
                            op: {
                                value: 'id',
                                text: 'fullname',
                                title: 'fullname'
                            }
                        }
                    },
                    {
                        label: '品目名称', name: 'ItemName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '所属日期 开始', name: 'OwnershipStartDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '所属日期 结束', name: 'OwnershipEndDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '入(退)库时间', name: 'WarehousingTime', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '完税证明'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '实缴(退)金额', name: 'PaidAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'CertificateTotalAmount', required: '1',
                        datatype: 'float', tabname: '完税证明'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_SubcontractInvoice_Certificate",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                    var cc = $('#ProjectSubcontractId').mkselectGetEx();
                    if (cc && cc.yi) {
                        $('#Base_SubcontractingUnit').mkselectSet(cc.yi);
                    }

                    $('#Base_SubcontractingUnit').on("change", function () {
                        var unit = $('#Base_SubcontractingUnit').mkselectGetEx();

                        var sql = " 1 = 1 ";
                        if (projectId) {
                            sql += " and ProjectID='" + projectId + "' ";
                        }

                        if (unit && unit.yi) {
                            sql += " and yi='" + unit.yi + "' ";
                        }
                        $('#ProjectSubcontractId').mkselectRefresh({ url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', param: { code: 'FBHTLB', strWhere: sql }, value: 'id', text: 'name', title: 'name' });
                        $("#ProjectID").val("")
                        $("#ProjectName").val("")
                        $("#b").val("")
                        $("#a").val("")
                    })
                });
            }
            else {
                $('#Base_SubcontractingUnit').on("change", function () {
                    var unit = $('#Base_SubcontractingUnit').mkselectGetEx();

                    var sql = " 1 = 1 ";
                    if (projectId) {
                        sql += " and ProjectID='" + projectId + "' ";
                    }

                    if (unit && unit.yi) {
                        sql += " and yi='" + unit.yi + "' ";
                    }
                    $('#ProjectSubcontractId').mkselectRefresh({ url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', param: { code: 'FBHTLB', strWhere: sql }, value: 'id', text: 'name', title: 'name' });
                    $("#ProjectID").val("")
                    $("#ProjectName").val("")
                    $("#b").val("")
                    $("#a").val("")
                })
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_SubcontractInvoice/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();

    ocrCallBack = function (data) {
        if (data) {
            var maindata = data.maindata;
            var subdata = data.subdata;
            var rate = 0;
            if (subdata) {
                rate = top.Changjie.extractTaxTateNum2(subdata[0].tax_rate);
                if (rate == 0) {
                    $("#TaxRate").val(rate);//税率
                }
                else {
                    $("#TaxRate").val(eval(rate / 100));
                }

                for (var _i in subdata) {
                    var item = subdata[_i];
                    var row = {
                        ID: Changjie.newGuid(),
                        Name: item.name,//货物或应税劳务·服务名称
                        Standards: item.specification,//规格型号
                        Unit: item.unit,//单位
                        UnitPrice: item.unit_price,//单价
                        Num: item.quantity,//数量
                        Amount: item.amount,//金额
                        TaxRate: item.tax_rate,//税率
                        TaxAmount: item.tax//税额
                    };
                    $("#Finance_SubcontractInvoice_Invoice").jfGridSet("addRow", row);
                }
            }
            if (maindata) {
                $("#InvoiceNo").val(maindata.number);//发票号
                var orcinvoiceType = maindata.invoice_type;
                //发票类型
                if (orcinvoiceType) {
                    var invoiceTypeDatas = $("#InvoiceType").mkselectGetAll();
                    var invoiceTypeId = "";
                    if (invoiceTypeDatas) {
                        for (var _i in invoiceTypeDatas) {
                            var item = invoiceTypeDatas[_i];
                            var text = item.text;
                            if (text.indexOf("电子") > -1) {//增值税电子普通发票
                                var itemRate = top.Changjie.extractTaxTateNum1(text);
                                if (itemRate == rate) {
                                    invoiceTypeId = item.id;
                                    break;
                                }
                            }
                            else if (text.indexOf("普通") > -1) {//增值税普通发票
                                var itemRate = top.Changjie.extractTaxTateNum1(text);
                                if (itemRate == rate) {
                                    invoiceTypeId = item.id;
                                    break;
                                }
                            }
                            else if (text.indexOf("专用") > -1) {//增值税专用发票
                                var itemRate = top.Changjie.extractTaxTateNum1(text);
                                if (itemRate == rate) {
                                    invoiceTypeId = item.id;
                                    break;
                                }
                            }
                        }
                    }
                    if (!!invoiceTypeId == false) {
                        Changjie.alert.error('发票类型及税率匹配失败，请人工选择');
                    }
                    else {
                        $("#InvoiceType").mkselectSet(invoiceTypeId);
                    }
                }
                function SetCompany(selectid, value, typename) {
                    if (value) {
                        var $this = $("#" + selectid);
                        var datas = $this.mkselectGetAll();
                        var ismapping = false;
                        if (datas) {
                            for (var _i in datas) {
                                var item = datas[_i];
                                if (item.text == value) {
                                    $this.mkselectSet(item.id);
                                    ismapping = true;
                                    break;
                                }
                            }
                        }
                        if (!ismapping) {
                            Changjie.alert.error(typename + '匹配失败，请人工选择');
                        }
                    }
                    else {
                        Changjie.alert.error(typename + '匹配失败，请人工选择');
                    }
                }



                $("#InvoiceAmount").val(maindata.total);//发票总金额


                $("#MakeCompanyName").val(maindata.seller_name);//开票单位
                $("#TaxAmount").val(maindata.subtotal_tax);//总税额

                $("#MakeDate").val(maindata.issue_date);//开票日期
                SetCompany("CollectCompanyId", maindata.seller_name, "收票单位");//收票单位
                $("#SaleBank").val(maindata.buyer_bank);//销售方开户行账号

                $("#TaxpayerName").val(maindata.buyer_name);//购买方名称
                $("#TaxpayerNumber").val(maindata.buyer_id);//购买方纳税人识别号
                $("#InvoiceAddress").val(maindata.buyer_address);//购买方地址、电话
                $("#InvoiceBank").val(maindata.buyer_bank);//购买方开户行及账号

                $("#SaleName").val(maindata.seller_name);//销售方名称
                $("#SaleTaxpayer").val(maindata.seller_id);//销售方纳税人识别号
                $("#SaleAddress").val(maindata.seller_address);//销售方地址/电话
                $("#SaleBank").val(maindata.seller_bank);//销售方开户行账号

                $("#TotalAmount").val(maindata.subtotal_amount);//合计金额
                $("#TotalTax").val(maindata.subtotal_tax);//合计税额
                $("#TotalTaxAmount").val(maindata.total);//销售方地址/电话
            }
        }
        return true;
    };
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Finance_SubcontractInvoice"]').mkGetFormData());
    postData.strfinance_SubcontractInvoice_CertificateList = JSON.stringify($('#Finance_SubcontractInvoice_Certificate').jfGridGet('rowdatas'));
    postData.strfinance_SubcontractInvoice_InvoiceList = JSON.stringify($('#Finance_SubcontractInvoice_Invoice').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
