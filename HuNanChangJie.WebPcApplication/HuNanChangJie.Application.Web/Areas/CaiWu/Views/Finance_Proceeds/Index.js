﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-13 17:44
 * 描  述：收款单管理
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_Proceeds/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_Proceeds/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 260, 400);
            $('#ProceedsType').mkDataItemSelect({ code: 'FKDLB' });
            $('#AuditStatus').mkDataItemSelect({ code: 'AuditStatus' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增收款单',
                    url: top.$.rootUrl + '/CaiWu/Finance_Proceeds/Form?projectId=' + projectId,
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_Proceeds/GetPageList',
                headData: [
                    { label: "收款单号", name: "Code", width: 140, align: "left" },
                    //{
                    //    label: "收款类别", name: "ProceedsType", width: 80, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('dataItem', {
                    //            key: value,
                    //            code: 'SKDLB',
                    //            callback: function (_data) {
                    //                callback(_data.text);
                    //            }
                    //        });
                    //    }
                    //},
                    {
                        label: "收款时间", name: "ReceivablesDate", width: 80, align: "left", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    }, {
                        label: "项目名称", name: "ProjectName", width: 300, align: "left"
                    },
                    { label: "甲方", name: "Jia", width: 200, align: "left" },
                    { label: "项目经理", name: "ProjectManager", width: 60, align: "left" },
                    { label: "收款金额", name: "PreceedsAmount", width: 100, align: "left" },
                    { label: "收款方式", name: "PreceedsType", width: 70, align: "left" },
                    { label: "收款账户", name: "PreceedsZhanghu", width: 180, align: "left" }, {
                        label: "经办人", name: "OperatorId", width: 60, align: "left",
                        //formatterAsync: function (callback, value, row, op, $cell) {
                        //    Changjie.clientdata.getAsync('user', {
                        //        key: value,
                        //        callback: function (_data) {
                        //            callback(_data.name);
                        //        }
                        //    });
                        //}
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 60, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "工程合同", name: "ProjectContractName", width: 200, align: "left" },
                    //{
                    //    label: "工程合同", name: "ProjectContractId", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('sourceData', {
                    //            code: 'GCHTLB',
                    //            key: value,
                    //            keyId: 'id',
                    //            callback: function (_data) {
                    //                callback(_data['name']);
                    //            }
                    //        });
                    //    }
                    //},
                    //{
                    //    label: "借款单", name: "LoanBillId", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('sourceData', {
                    //            code: 'CW-HTLB',
                    //            key: value,
                    //            keyId: 'id',
                    //            callback: function (_data) {
                    //                callback(_data['name']);
                    //            }
                    //        });
                    //    }
                    //},
                    
                   
                    //{
                    //    label: "收款单位", name: "ProceedsCompany", width: 160, align: "left" 
                    //}, 
                    { label: "摘要", name: "Abstract", width: 300, align: "left" },

                ],
                mainId: 'ID',
                isPage: true
            });
            page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Finance_Proceeds/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '收款单',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/CaiWu/Finance_Proceeds/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
