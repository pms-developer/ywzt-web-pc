﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-13 17:44
 * 描  述：收款单管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var click = "";
var subGrid = [];
var tables = [];
var mainTable = "Finance_Proceeds";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Finance_Proceeds/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_Proceeds/Audit', { keyValue: keyValue }, function (data) {
      
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_Proceeds/UnAudit', { keyValue: keyValue }, function (data) {
      
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
                click = "1";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Finance_ProceedsAccount', "gridId": 'Finance_ProceedsAccount' });
                subGrid.push({ "tableName": 'Finance_ProceedsAgreement', "gridId": 'Finance_ProceedsAgreement' });
                subGrid.push({ "tableName": 'Finance_ProceedsVerification', "gridId": 'Finance_ProceedsVerification' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0018' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#ProceedsType').mkDataItemSelect({ code: 'SKDLB' });

            $('#ProceedsType').on("change", function () {
                var pt = $('#ProceedsType').mkselectGet();
                $("#ProjectContractIdDiv,#ProjectContractBDiv,#ProjectContractADiv,#ProjectContractCDiv,#ProjectContractDDiv,#ProjectContractEDiv,#LoanBillIdDiv,#ProjectContractFDiv,#ProjectContractGDiv").hide();
                if (pt == "合同收款") {
                    $("#ProjectContractIdDiv,#ProjectContractADiv,#ProjectContractBDiv,#ProjectContractCDiv,#ProjectContractDDiv,#ProjectContractEDiv,#ProjectContractFDiv,#ProjectContractGDiv").show();
                }
                else if (pt == "借款收入") {
                    $("#LoanBillIdDiv").show();
                }
                else if (pt == "还款收入") {
                    $("#LoanBillIdDiv").show();
                }
            })

            var sql = "";

            if (projectId) {
                sql = " ProjectID='" + projectId + "' ";
            }

            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#ProjectContractId').mkDataSourceSelect({ code: 'GCHTLB', value: 'id', text: 'name', strWhere: sql });

            $('#ProjectContractId').on("change", function () {
                var ct = $('#ProjectContractId').mkselectGetEx(); http://139.9.250.175/Login/Index


                if (ct) {
                    $("#a").val(ct.code)
                    var b = ct.visaamount + ct.totalamount;
                    $("#b").val(b.toLocaleString())
                    $("#c").val(ct.receivedamount.toLocaleString())
                    $("#d").val(ct.pcode)
                    $("#e").val(ct.pname)
                    $("#f").val(ct.bfunnname);

                    if (keyValue && click == "1") {
                        $("#ProjectManager").mkformselectSet();
                        click = "";
                    } else {
                        $("#ProjectManager").mkformselectSet(ct.marketingstaffid);
                    }



                    $("#ProjectID").val(ct.projectid)
                    $("#ProjectName").val(ct.projectname)

                    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_Proceeds/GetFinance_ProceedsAgreementListByContractid', { contractid: ct.id }, function (res) {
                        if (res.code == 200) {
                            console.log(res.data)
                            $.each(res.data, function (iii, eee) {
                                eee.ID = top.Changjie.newGuid();
                                eee.rowState = 1;
                                eee.SortCode = iii;
                                eee.EditType = 1;
                            })

                            $("#Finance_ProceedsAgreement").jfGridSet('refreshdata', res.data);
                        }
                    });

                    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Finance_Proceeds/GetFinance_ProceedsVerificationListByContractid', { contractid: ct.id }, function (res) {
                        if (res.code == 200) {
                            console.log(res.data)
                            $.each(res.data, function (iii, eee) {
                                eee.ID = top.Changjie.newGuid();
                                eee.rowState = 1;
                                eee.SortCode = iii;
                                eee.EditType = 1;
                            })

                            $("#Finance_ProceedsVerification").jfGridSet('refreshdata', res.data)

                        }
                    });
                }
                else {
                    $("#a").val("")
                    $("#b").val("")
                    $("#c").val("")
                    $("#d").val("")
                    $("#e").val("")
                    $("#ProjectID").val("")
                    $("#ProjectName").val("")

                    $("#Finance_ProceedsAgreement").jfGridSet("clearallrow");
                    $("#Finance_ProceedsVerification").jfGridSet("clearallrow");

                }
            })
            $("#LoanBillId").mkselect({});

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            //$('#ProceedsCompany').mkDataItemSelect({ code: 'SPDW' });

            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Finance_ProceedsAccount').jfGrid({
                headData: [
                    {
                        label: '支付方式', name: 'PaymentType', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '账户信息'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataItem',
                            code: 'ZFFS'
                        }
                    },
                    //{
                    //    label: '账户名称', name: 'Bank', width: 180, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                    //    datatype: 'string', tabname: '账户信息'
                    //    , edit: {
                    //        type: 'select',
                    //        change: function (row, index, item, oldValue, colname, headData) {
                    //            var gridName = "Finance_ProceedsAccount";
                    //            row.OpenBank = item.bank;
                    //            row.BankCode = item.bankcode;
                    //            row.Holder = item.holder;
                    //            $('#Finance_ProceedsAccount').jfGridSet("updateRow", index)

                    //        },
                    //        datatype: 'dataSource',
                    //        code: 'ZjzhNoProject',
                    //        op: {
                    //            value: 'id',
                    //            text: 'name',
                    //            title: 'name'
                    //        }
                    //    }
                    //},


                    {
                        label: '账户名称', name: 'BankNames', width: 180, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '账户信息'
                        , edit: {
                            type: 'layer',
                            change: function (data, rownum, selectData) {
                                data.Bank = selectData.id;
                                data.BankNames = selectData.name;
                                data.OpenBank = selectData.bank;
                                data.BankCode = selectData.bankcode;
                                data.Holder = selectData.holder;
                                $('#Finance_ProceedsAccount').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 600,
                                height: 400,
                                listFieldInfo: [
                                    { label: "账户名称", name: "name", width: 150, align: "left" },
                                    { label: "开户银行", name: "bank", width: 150, align: "left" },
                                    { label: "账号", name: "bankcode", width: 150, align: "left" },
                                    { label: "可用余额", name: "usablebalance", width: 150, align: "left" },
                                    { label: "余额", name: "balance", width: 150, align: "left" },
                                    { label: "冻结额", name: "freezed", width: 150, align: "left" },
                                    { label: "项目经理", name: "f_realname", width: 150, align: "left" },
                                    { label: "开户人", name: "holder", width: 150, align: "left" },
                                    {
                                        label: "所属公司", name: "companyid", width: 150, align: "left",
                                        formatterAsync: function (callback, value, row, op, $cell) {
                                            Changjie.clientdata.getAsync('company', {
                                                key: value,
                                                callback: function (_data) {
                                                    callback(_data.name);
                                                }
                                            });
                                        }
                                    },
                                ],
                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: {
                                    code: 'ZjzhNoProject'
                                }
                            }
                        }
                    },
                    {
                        label: '开户银行', name: 'OpenBank', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '账户信息'

                    },
                    {
                        label: '账号名称', name: 'Holder', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '账户信息'

                    },

                    {
                        label: '账号', name: 'BankCode', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '账户信息'

                    },
                    {
                        label: '账行账号/票据号', name: 'BankAccount', width: 120, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '账户信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '收款金额', name: 'Amount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'PreceedsAmount', required: '1',
                        datatype: 'float', tabname: '账户信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '账户信息'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_ProceedsAccount",
                isEdit: true,
                height: 370, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#Finance_ProceedsAgreement').jfGrid({
                headData: [
                    {
                        label: '款项性质', name: 'FundType', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                    },
                    {
                        label: '收款方式', name: 'PaymentType', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议', formatter: function (cellvalue, row) {
                            if (cellvalue == 'ContractRatio') {
                                return '按合同比例';
                            } else if (cellvalue == 'MonthOutputRatio') {
                                return '按月完成产值比例';
                            } else if (cellvalue == 'TotalFinishedRatio') {
                                return '按累计完成产值比例';
                            } else if (cellvalue == 'ProjectNode') {
                                return '按项目进度节点';
                            }
                        }
                    },
                    {
                        label: '支付条件', name: 'ProceedsCondition', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                    },
                    {
                        label: '协议支付比例', name: 'PaymentPercent', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                    },
                    {
                        label: '协议收款金额', name: 'SAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                    },
                    {
                        label: '实际收款', name: 'PracticalAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                    },
                    {
                        label: '本次收款金额', name: 'Amount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '收款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_ProceedsAgreement",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showdelete: false,
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#Finance_ProceedsVerification').jfGrid({
                headData: [
                    {
                        label: '发票编号', name: 'InvoiceCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '开票核销'
                    },
                    {
                        label: '开票日期', name: 'MakeInvoiceDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '开票核销', formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: '发票类型', name: 'InvoiceType', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '开票核销', formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FPLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: '开票金额', name: 'InvoiceAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '开票核销'
                    },

                    {
                        label: '已核销金额', name: 'WrittenOff', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        //datatype float
                        datatype: 'string', tabname: '开票核销'
                    },

                    {
                        label: '未核销', name: 'UnWrittenOff', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '开票核销'
                    },
                    {
                        label: '本次核销金额', name: 'Amount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '开票核销'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {

                                //Modified by Wangwei 09/24/2020 修改本次核销金额不能大于可核销金额
                                //row.Amount 本次核销金额
                                //row.UnWrittenOff 可核销金额  
                                if (row.UnWrittenOff == null)
                                    row.UnWrittenOff = 0;


                                if (row.Amount > row.UnWrittenOff) {
                                    top.Changjie.alert.error("本次核销金额不能大于可核销金额")
                                    hexiaoMoneyflag = false;
                                    return false;
                                }
                                else {
                                    hexiaoMoneyflag = true;
                                }
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                alert("blur");
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '开票核销'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Finance_ProceedsVerification",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showdelete: false,
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Finance_Proceeds/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }


                });
            }
        },
    };
    // 保存数据
    //保存按钮提交方法
    //Modified by wangwei 09/24/2020 修改本次核销金额必须小于未核销金额的问题
    var hexiaoMoneyflag = true;

    acceptClick = function (callBack) {
        //Modified by wangwei 09/24/2020 修改本次核销金额必须小于未核销金额的问题
        if (!hexiaoMoneyflag) {
            return false;
        }
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Finance_Proceeds/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}

var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;
    $cell.html(value);
    $edit.val(value);
};

var getFormData = function () {

    //验证本次核销金额小于开票金额
    //top.Changjie.alert.error("test error");

    //验证表单
    if (!$('body').mkValidform()) {
        return false;
    }

    var pt = $('#ProceedsType').mkselectGet();
    if (pt == "合同收款") {
        var ct = $('#ProjectContractId').mkselectGetEx();
        if (ct == null) {
            top.Changjie.alert.error("请选择工程合同");
            return false;
        }
    }


    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Finance_Proceeds"]').mkGetFormData());
    postData.strfinance_ProceedsAccountList = JSON.stringify($('#Finance_ProceedsAccount').jfGridGet('rowdatas'));
    postData.strfinance_ProceedsAgreementList = JSON.stringify($('#Finance_ProceedsAgreement').jfGridGet('rowdatas'));
    postData.strfinance_ProceedsVerificationList = JSON.stringify($('#Finance_ProceedsVerification').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
