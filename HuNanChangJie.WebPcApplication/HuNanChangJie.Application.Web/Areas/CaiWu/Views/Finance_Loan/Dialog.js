﻿var acceptClick;

var projectId = request('projectId');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {

            $('#Loaner').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });

            $('#dosearch').on("click", function () {
                var param = {};
                param.AuditStatus = 2;
                param.HasPay = 1;

                var tempsword = $('#sword').val();
                if (tempsword) {
                    param.Code = tempsword;
                }
                var tempproject = $('#ProjectID').mkselectGet();
                if (tempproject) {
                    param.ProjectID = tempproject;
                }
                var tempuserid = $('#Loaner').mkformselectGet();
                if (tempuserid) {
                    param.Loaner = tempuserid;
                }
                page.search(param);
            })
            var param = {};
            param.AuditStatus = 2;
            param.HasPay = 1;
            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_Loan/GetPageList',
                headData: [
                    { label: "借款单号", name: "Code", width: 150, align: "left" },
                    {
                        label: "申请日期", name: "ApplyDate", width: 100, align: "left", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "借款人", name: "Loaner", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "部门", name: "Dept", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('department', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "项目名称", name: "ProjectID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code:  'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },
                    { label: "申请金额", name: "Amount", width: 100, align: "left" },
                    { label: "已付金额", name: "PayedAmount", width: 100, align: "left" },
                    {
                        label: "预计还款日期", name: "RepaymentDate", width: 100, align: "left", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "借款用途", name: "Purpose", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'JKYT',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true,
                height: 362,
                param: { queryJson: JSON.stringify(param), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }
            });
            page.search();
            
        },
        search: function (param) {
            param = param || {};
            if (projectId) {
                param.ProjectID = projectId;
            }

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};