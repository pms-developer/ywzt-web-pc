﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-20 17:13
 * 描  述：借款申请
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#Loaner').mkUserSelect(0);
            $('#PayStatus').mkDataItemSelect({ code: 'ZFZT' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增借款申请',
                    url: top.$.rootUrl + '/CaiWu/Finance_Loan/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Finance_Loan/GetPageList',
                headData: [
                    { label: "借款单号", name: "Code", width: 150, align: "left"},
                    {
                        label: "申请日期", name: "ApplyDate", width: 100, align: "left", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }},
                    { label: "借款人", name: "Loaner", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "部门", name: "Dept", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('department', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "项目名称", name: "ProjectID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'BASE_XMLB',
                                 key: value,
                                 keyId: 'project_id',
                                 callback: function (_data) {
                                     callback(_data['projectname']);
                                 }
                             });
                        }},
                    { label: "申请金额", name: "Amount", width: 100, align: "left" },
                    { label: "已付金额", name: "PayedAmount", width: 100, align: "left" },
                    {
                        label: "预计还款日期", name: "RepaymentDate", width: 100, align: "left", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }},
                    { label: "借款用途", name: "Purpose", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'JKYT',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    {
                        label: "付款状态", name: "PayStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            switch (cellvalue) {
                                case '1':
                                    return '<span class="label label-success" style="cursor: pointer;">已付款</span>';
                                case '-1':
                                    return '<span class="label label-success" style="cursor: pointer;">已关闭</span>';
                                default:
                                    return '<span class="label label-default" style="cursor: pointer;">未付款</span>';
                            }
                        }
                    },
                    { label: "借款说明", name: "LoanExplain", width: 100, align: "left"},
                    { label: "特殊说明", name: "SpecialExplain", width: 100, align: "left"},
                    
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center", formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }, { label: "摘要", name: "Abstract", width: 100, align: "left" },
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};

            if (projectId) {
                param.ProjectID = projectId;
            }

            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Finance_Loan/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '借款申请',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/CaiWu/Finance_Loan/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
