﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-13 00:28
 * 描  述：垫资退款管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var click = "";
var tables = [];
var mainTable = "Base_CJ_DianZiHuan";
var processCommitUrl = top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/Audit', { keyValue: keyValue }, function (data) {
      
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/UnAudit', { keyValue: keyValue }, function (data) {
       
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
                click = "1";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        initSqd: function () {
            //localStorage.removeItem(name, JSON.stringify(data));

            var data = $('#DianZiDanHao').mkselectGetEx()
            console.log(data)
            if (data) {
                $("#a").val(data.bprojectname)
                $("#b").val(data.dianziren)
                $("#c").val(data.jine)
                $("#d").val(Changjie.formatDate(data.dianzishijian, 'yyyy-MM-dd'));
                $("#e").val(data.yituijine)
                //$("#e").val(data.danweimingchen)
                //$("#f").val(data.tuihuanshijian)
                if (keyValue && click == "1") {
                    $("#ProjectManager").mkformselectSet();
                    click = "";
                } else {
                    $("#ProjectManager").mkformselectSet(data.marketingstaffid);
                }

                $("#ProjectID").val(data.projectid)
                $("#ProjectName").val(data.bprojectname)
            }
            else {
                $("#a").val("")
                $("#b").val("")
                $("#c").val("")
                $("#d").val("")
                $("#e").val("")
                //$("#f").val("")
                $("#ProjectID").val("")
                $("#ProjectName").val("")
            }
        },
        bind: function () {
            top.Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0009' }, function (data) {
                if (!$('#BianHao').val()) {
                    $('#BianHao').val(data);
                }
            });
            var sqlWhere = "";
            if (projectId) {
                sqlWhere = " ProjectID='" + projectId + "' ";
            }

            $('#DianZiDanHao').mkDataSourceSelect({ code: 'DZDJ', value: 'id', text: 'title', strWhere: sqlWhere });

            //$('#TuiHuanZhangHao').mkDataSourceSelect({ code: 'ZjzhNoProject', value: 'id', text: 'name' });

            $("#TuiHuanZhangHaoName").on("click", function () {
                top.Changjie.layerForm({
                    id: "selectContract",
                    title: "选择账户",
                    url: top.$.rootUrl + '/CaiWu/Base_FundAccount/Dialog?type=ZjzhNoProject',
                    width: 800,
                    hegiht: 294,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (data) {
                                $("#TuiHuanZhangHaoName").val(data.Bank);
                                $("#TuiHuanZhangHao").val(data.ID);
                            }
                        });
                    }
                });
            });


            $('#DianZiDanHao').on('change', function () {
                page.initSqd();
            });

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#JinBanRen').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);


            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });


            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "newinstance",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }

                    page.initSqd();
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        $.mkSaveForm(top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var v1 = top.Changjie.format45($("#c").val(), top.Changjie.sysGlobalSettings.pointGlobal)

    var v2 = top.Changjie.format45($("#JinE").val(), top.Changjie.sysGlobalSettings.pointGlobal)

    var v3 = top.Changjie.format45($("#e").val(), top.Changjie.sysGlobalSettings.pointGlobal)
    if (!v3)
        v3 = 0;

    if (v2 > v1 - v3) {
        top.Changjie.alert.error("退还金额不能大于垫资金额");
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
