﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-13 00:28
 * 描  述：垫资退款管理
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData();
            //Changjie.clientdata.sourceData.load("DZDJ");
        }
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200) {
            refreshGirdData();
            //Changjie.clientdata.sourceData.load("DZDJ");
        }
        else
            top.Changjie.alert.error(data.info);
    });
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 260, 400);

            //$('#DianZiDanHao').mkDataSourceSelect({ code: 'DZDJ', value: 'bianhao', text: 'bianhao' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增垫资还款',
                    url: top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/GetPageList',
                headData: [
                    { label: "编号", name: "BianHao", width: 150, align: "left" },
                    {
                        label: "垫资时间", name: "DianZiShiJian", width: 100, align: "left", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    { label: "项目经理", name: "ProjectManager", width: 70, align: "left" },
                    { label: "垫资项目", name: "ProjectName", width: 300, align: "left" },
                    { label: "垫资人", name: "DianZiRen", width: 100, align: "left" },
                    { label: "垫资金额", name: "JinEDian", width: 100, align: "left" },
                    { label: "收款账户", name: "TuiHuanRen", width: 100, align: "left" },
                    
                    //{
                    //    label: "垫资单号", name: "DianZiDanHao", width: 500, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('sourceData', {
                    //            code: 'DZDJ',
                    //            key: value,
                    //            keyId: 'id',
                    //            callback: function (_data) {
                    //                callback(_data['title']);
                    //            }
                    //        });
                    //    }
                    //},
                    { label: "已退还金额", name: "YiTuiJinE", width: 120, align: "left" },
                    { label: "本次退还金额", name: "JinE", width: 150, align: "left" },
                    { label: "还款账户", name: "TuiHuanZhangHao", width: 200, align: "left" },

                   
                    { label: "经办人", name: "JinBanRen", width: 100, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 70, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    {
                        label: "退还时间", name: "TuiHuanShiJian", width: 100, align: "left", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    { label: "垫资单号", name: "DianZiDanHao", width: 100, align: "left" },
                    //{
                    //    label: "经办人", name: "JinBanRen", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('user', {
                    //            key: value,
                    //            callback: function (_data) {
                    //                callback(_data.name);
                    //            }
                    //        });
                    //    }
                    //},
                    //{ label: "垫资人收款账号", name: "TuiHuanRen", width: 100, align: "left" },
                    //{
                    //    label: "还款账户", name: "TuiHuanZhangHao", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('sourceData', {
                    //            code: 'ZJZH',
                    //            key: value,
                    //            keyId: 'id',
                    //            callback: function (_data) {
                    //                callback(_data['name']);
                    //            }
                    //        });
                    //    }
                    //},
                    
                ],
                mainId: 'ID',
                isPage: true
            });
            page.search(checkJson);
        },
        search: function (param) {
            param = param || {};

            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }

            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/CaiWu/Base_CJ_DianZiHuan/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
