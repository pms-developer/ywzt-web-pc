﻿/*  
 * 创建人：超级管理员 
 * 日  期：2019-01-23 09:06 
 * 描  述：1232131 
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#sign').mkDataItemSelect({ code: 'Task_Sign' });

            $('#sign').change(function(d) {
                if (d.currentTarget.textContent=="md5(token)") {
                    $("#signstring").val('{token:"sdfsdfsfsdf",sign:"encryptionParameters"}');
                } else if (d.currentTarget.textContent == "md5(渠道+token)") {
                    $("#signstring").val('{channel:"50",token:"sdfsdfsfsdf",sign:"encryptionParameters"}');
                } else if (d.currentTarget.textContent == "md5(时间戳+token)") {
                    $("#signstring").val('{time:"theCurrentTimestamp",token:"sdfsdfsfsdf",sign:"encryptionParameters"}');
                } else if (d.currentTarget.textContent == "md5(渠道+时间戳+token)") {
                    $("#signstring").val('{channel:"50",time:"theCurrentTimestamp",token:"sdfsdfsfsdf",sign:"encryptionParameters"}');
                }
            });



            $('#isrecemail').mkRadioCheckbox({
                type: 'radio',
                code: 'YesOrNo',
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/TaskScheduling/TaskProject/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).AgGridSet('refreshdata', data[id]);
                        }
                        else {
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        }
    };
    // 保存数据 
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData())
        };
        $.mkSaveForm(top.$.rootUrl + '/TaskScheduling/TaskProject/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调 
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
} 