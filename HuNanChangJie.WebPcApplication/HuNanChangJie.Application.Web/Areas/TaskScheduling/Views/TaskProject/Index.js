﻿/* 
 * Copyright (c) 2013-2019  
 * 创建人：超级管理员 
 * 日  期：2019-01-22 14:09 
 * 描  述：服务器列表 
 */
var refreshGirdData;
 
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新 
            $('#refresh').on('click', function () {
                location.reload();
            });
            //任务管理
            $("#taskmanage").on('click',
                function () {

                    var keyValue = $('#gridtable').AgGridValue('id');
                    if (!!keyValue) {
                      var rowd=  $('#gridtable').AgGridGet("rowdata");
                        Changjie.frameTab.open({ F_ModuleId: keyValue + "1", F_Icon: 'fa fa-desktop', F_FullName: rowd.name + '_任务管理', F_UrlAddress: '/TaskScheduling/TaskJob/Index?projectid=' + keyValue }, false);
                    } else {
                        Changjie.alert.warning("请选择一个项目");
                    }
                });
            // 新增 
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增项目',
                    url: top.$.rootUrl + '/TaskScheduling/TaskProject/Form',
                    width: 600,
                    height: 630,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑 
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑项目',
                        url: top.$.rootUrl + '/TaskScheduling/TaskProject/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 630,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除 
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/TaskScheduling/TaskProject/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            }); 


        },
        // 初始化列表 
        initGird: function () {
           
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/TaskScheduling/TaskProject/GetPageList',
                headData: [
                    { headerName: "编号", field: "id", width: 50, cellStyle: { 'text-align': 'left' } }, 
                    { headerName: "项目名称", field: "name", width: 120, cellStyle: { 'text-align': 'left' } },
                    {
                        headerName: "邮件通知", field: "isrecemail", width: 120, cellStyle: { 'text-align': 'left' },
                        cellRenderer: function (params) {

                            if (params.value == 0) {
                                return '<span class="label label-danger">否</span>';
                            } else if (params.value == 1) {
                                return '<span class="label label-primary">是</span>';
                            }


                            return params.value;
                        }
                    },
                    { headerName: "接收邮箱", field: "recemail", width: 120, cellStyle: { 'text-align': 'left' } }, 
                    {
                        headerName: "加密方式", field: "sign", width: 120, cellStyle: { 'text-align': 'left' },
                        cellRenderer: function (params) {

                            if (params.value == 0) {
                                return '<span class="label label-danger">不加密</span>';
                            } else if (params.value == 10) {
                                return '<span class="label label-default">md5(token)</span>';
                            }
                            else if (params.value == 20) {
                                return '<span class="label label-success">md5(渠道+token)</span>';
                            } else if (params.value == 30) {
                                return '<span class="label label-info">md5(时间戳+token)</span>';
                            } else if (params.value == 40) {
                                return '<span class="label label-warning">md5(渠道+时间戳+token)</span>';
                            }


                            return params.value;
                        }
                    }, 
                    { headerName: "签名内容", field: "signstring", width: 120, cellStyle: { 'text-align': 'left' } }, 
                  
                     
                ],
                mainId: 'id',
                
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            
           $('#gridtable').AgGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
} 