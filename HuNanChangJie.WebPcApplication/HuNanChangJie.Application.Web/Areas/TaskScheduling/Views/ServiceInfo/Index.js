﻿/*
 * Copyright (c) 2013-2019  
 * 创建人：超级管理员 
 * 日  期：2019-01-22 14:09 
 * 描  述：服务器列表 
 */
var refreshGirdData;

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新 
            $('#refresh').on('click', function () {
                location.reload();
            });


            $.LoadReport({
                url: top.$.rootUrl + "/TaskScheduling/ServiceInfo/GetServChartData",
                element: $("#report-pane")

            });
            $.getJSON(top.$.rootUrl + "/TaskScheduling/ServiceInfo/GetProjectChartData", function (result) {

                var info =
                {
                    projectsJson: result,
                    projectJob: function () {
                        var myChart = echarts.init(document.getElementById('echartProject'));
                        var _xAxisData = [];

                        var _seriesTotalData = [];
                        var _seriesNormalData = [];
                        var _seriesStopData = [];
                        //得到项目
                        var _projects = [];
                        $.each(info.projectsJson, function (i, obj) {
                            if (_projects.indexOf(obj.projectname) === -1) {
                                _projects.push(obj.projectname);
                                //设置坐标的名称
                                _xAxisData.push(obj.projectname);
                            }
                        });

                        $.each(_projects, function (i, projectname) {
                            var _totalNum = 0;
                            var _normalNum = 0;
                            var _stopNum = 0;
                            $.each(info.projectsJson, function (i, prj) {
                                if (projectname == prj.projectname) {
                                    _totalNum += prj.total;
                                    if (prj.status == "0") {//正常
                                        _normalNum = prj.total;
                                        //代表continue
                                        return true;
                                    } else if (prj.status == "50") {//停止
                                        _stopNum = prj.total;
                                        return true;
                                    }
                                }
                            });
                            //设置总任务数值
                            _seriesTotalData.push(_totalNum);
                            //设置正常值
                            _seriesNormalData.push(_normalNum);
                            //设置停止值
                            _seriesStopData.push(_stopNum);

                            // 指定图表的配置项和数据
                            var option = {

                                tooltip: {
                                    trigger: 'axis',
                                    textStyle: { fontSize: '30px' }
                                },
                                legend: {
                                    data: ['总任务数', '正常任务数', '停止任务数']
                                },
                                xAxis: {
                                    /*设置倾斜 axisLabel: {
                                        rotate: 60,
                                    }, */
                                    data: _xAxisData
                                    //data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
                                },
                                yAxis: {},
                                series: [
                                    {
                                        name: '总任务数',
                                        type: 'bar',
                                        //data: [5, 20, 36, 10, 10, 20],
                                        data: _seriesTotalData,
                                        itemStyle: {
                                            normal: {
                                                barBorderColor: '#adadad',
                                                color: '#adadad'
                                            },
                                            emphasis: {
                                                barBorderColor: '#cccccc',
                                                color: '#cccccc'
                                            }
                                        }
                                    }, {
                                        name: '正常任务数',
                                        type: 'bar',
                                        //data: [5, 20, 36, 10, 10, 20],
                                        data: _seriesNormalData,
                                        itemStyle: {
                                            normal: {
                                                barBorderColor: '#449d44',
                                                color: '#449d44'
                                            },
                                            emphasis: {
                                                barBorderColor: '#5cb85c',
                                                color: '#5cb85c'
                                            }
                                        }
                                    }, {
                                        name: '停止任务数',
                                        type: 'bar',
                                        //data: [5, 20, 36, 10, 10, 20],
                                        data: _seriesStopData,
                                        itemStyle: {
                                            normal: {
                                                barBorderColor: '#c9302c',
                                                color: '#c9302c'
                                            },
                                            emphasis: {
                                                barBorderColor: '#d9534f',
                                                color: '#d9534f'
                                            }
                                        }
                                    }
                                ]
                            };

                            // 使用刚指定的配置项和数据显示图表。
                            myChart.setOption(option);

                        });
                    }
                }

                info.projectJob();
            });

            $("#projectmanage").on('click',
                function () {



                    Changjie.frameTab.open({ F_ModuleId: 'TaskCluster', F_Icon: 'fa fa-desktop', F_FullName: '任务集群-项目管理', F_UrlAddress: '/TaskScheduling/TaskProject/Index' }, false);

                });

        },
        // 初始化列表 
        initGird: function () {

            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/TaskScheduling/ServiceInfo/GetPageList',
                headData: [
                    { headerName: "服务编号", field: "servid", width: 100, cellStyle: { 'text-align': 'left' } },
                    {
                        headerName: "IP地址", field: "ip", width: 120, cellStyle: { 'text-align': 'left' },
                        cellRenderer: function (params) {

                            if (params.data.isleader == 1) {
                                return params.value + '&nbsp;&nbsp;<span class="label label-success">主服务</span>';
                            }


                            return params.value;
                        }
                    },
                    {
                        headerName: "状态", field: "status", width: 60, cellStyle: { 'text-align': 'left' },
                        cellRenderer: function (params) {

                            if (params.value == 20) {
                                return '<span class="label label-danger">已销毁</span>';
                            } else if (params.value == 10) {
                                return '<span class="label label-primary">正常</span>';
                            }


                            return params.value;
                        }
                    },
                    {
                        headerName: "更新时间", field: "updatetime", width: 80, cellStyle: { 'text-align': 'left' }

                    },
                    { headerName: "添加时间", field: "addtime", width: 80, cellStyle: { 'text-align': 'left' } },
                    {
                        headerName: "运行时间", field: "runminute", width: 100, cellStyle: { 'text-align': 'left' },
                        cellRenderer: function (params) {
                            var _runtime = "";
                            
                            if (params.value != -1) {
                                var _daynum = parseInt(params.value / (60 * 24));
                                if (_daynum > 0) {
                                    _runtime = '已运行 ' + _daynum + ' 天';
                                } else {
                                    var _hournum = parseInt(params.value / 60);
                                    if (_hournum > 0) {
                                        _runtime = '已运行 ' + _hournum + ' 小时';
                                    } else {
                                        _runtime = '已运行 ' + params.value + ' 分钟';
                                    }
                                }
                                _runtime = '<span class="label label-success">' + _runtime + '</span>';
                            } else {
                                _runtime = params.data.statusname;
                            }
                            return _runtime;
                        }

                    },

                ],
                mainId: 'servid',

            });
            page.search();
        },
        search: function (param) {
            param = param || {};

            $('#gridtable').AgGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
} 