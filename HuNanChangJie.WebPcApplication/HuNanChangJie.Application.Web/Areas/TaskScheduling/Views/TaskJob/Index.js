﻿/*
 * Copyright (c) 2013-2019  
 * 创建人：超级管理员 
 * 日  期：2019-01-22 14:09 
 * 描  述：服务器列表 
 */
var refreshGirdData;
var projectid = request('projectid');
var projectid1 = request('keyValue');


var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新 
            $('#refresh').on('click', function () {
                location.reload();
            });
            //调度日志管理
            $("#log").on('click',
                function () {

                    //var keyValue = $('#gridtable').AgGridValue('id');
                    //if (!!keyValue) {
                    //    var rowd = $('#gridtable').AgGridGet("rowdata");
                    //    Changjie.frameTab.open({ F_ModuleId: keyValue, F_Icon: 'fa fa-desktop', F_FullName: rowd.name + '_任务管理', F_UrlAddress: '/TaskScheduling/TaskJob/Index?keyValue=' + keyValue + '&projectid=' + projectid }, false);
                    //} else {
                    //    Changjie.alert.warning("请选择一个项目");
                    //}
                });

            //暂停
            $("#disabled").on('click',
                function () {

                    var keyValue = $('#gridtable').AgGridValue('id');
                    if (Changjie.checkrow(keyValue)) {
                        Changjie.layerConfirm("确定要暂停吗？", function (ret, index) {
                            if (ret) {
                                $.getJSON(top.$.rootUrl + "/TaskScheduling/TaskJob/UpdateStatus?id=" + keyValue +'&status=50', function (result) {

                                    if (result.code==200) {
                                        Changjie.alert.success(result.info);
                                    } else {
                                        Changjie.alert.warning("暂停失败！");
                                    }
                                });
                                
                            }

                            Changjie.layerClose("",index);

                        });

                    }  
                 

                });
            //启动
            $("#enabled").on('click',
                function () {

                    var keyValue = $('#gridtable').AgGridValue('id');
                    if (Changjie.checkrow(keyValue)) {
                        Changjie.layerConfirm("确定要启动任务吗？", function (ret, index) {
                            if (ret) {
                                $.getJSON(top.$.rootUrl + "/TaskScheduling/TaskJob/UpdateStatus?id=" + keyValue + '&status=0', function (result) {

                                    if (result.code == 200) {
                                        Changjie.alert.success(result.info);
                                    } else {
                                        Changjie.alert.warning("启动失败！");
                                    }
                                });

                            }

                            Changjie.layerClose("", index);

                        });

                    }  
                });

            //立即执行
            $("#execjob").on('click',
                function () {

                    var keyValue = $('#gridtable').AgGridValue('id');
                    if (Changjie.checkrow(keyValue)) {
                        Changjie.layerConfirm("确定要立即执行任务吗？", function (ret, index) {
                            if (ret) {
                                $.getJSON(top.$.rootUrl + "/TaskScheduling/TaskJob/ExecJob?id=" + keyValue, function (result) {

                                    if (result.code == 200) {
                                        Changjie.alert.success(result.info);
                                    } else {
                                        Changjie.alert.warning("启动失败！");
                                    }
                                });

                            }

                            Changjie.layerClose("", index);

                        });

                    }  
                });

            // 新增 
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增项目',
                    url: top.$.rootUrl + '/TaskScheduling/TaskJob/Form?projectid=' + projectid,
                    width: 600,
                    height: 630,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑 
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑项目',
                        url: top.$.rootUrl + '/TaskScheduling/TaskJob/Form?keyValue=' + keyValue + '&projectid=' + projectid,
                        width: 600,
                        height: 630,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除 
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/TaskScheduling/TaskJob/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });


        },
        // 初始化列表 
        initGird: function () {

            $('#gridtable').AgGrid({
                url: top.$.rootUrl + '/TaskScheduling/TaskJob/GetPageList?keyValue=' + projectid,
                headData: [
                    { headerName: "编号", field: "id", width: 50, cellStyle: { 'text-align': 'left' } },
                    { headerName: "任外名称", field: "name", width: 180, cellStyle: { 'text-align': 'left' } },
                    { headerName: "链接", field: "link", width: 300, cellStyle: { 'text-align': 'left' } },
                    { headerName: "周期", field: "cron", width: 100, cellStyle: { 'text-align': 'left' } },
                    {
                        headerName: "邮件通知", field: "isfailmail", width: 120, cellStyle: { 'text-align': 'left' },
                        cellRenderer: function (params) {

                            if (params.value == 0) {
                                return '<span class="label label-danger">否</span>';
                            } else if (params.value == 1) {
                                return '<span class="label label-primary">是</span>';
                            }


                            return params.value;
                        }
                    },
                    {
                        headerName: "异常发邮件", field: "isfailmail", width: 120, cellStyle: { 'text-align': 'left' },
                        cellRenderer: function (params) {

                            if (params.value == "0") {
                                return '<span class="label label-danger">否</span>';
                            } else if (params.value == "1") {
                                return '<span class="label label-primary">是</span>';
                            }


                            return params.value;
                        }
                    },

                    { headerName: "状态", field: "statusname", width: 100, cellStyle: { 'text-align': 'left' } },
                    { headerName: "所在服务器", field: "servid", width: 140, cellStyle: { 'text-align': 'left' } },
                    { headerName: "描述", field: "name", width: 230, cellStyle: { 'text-align': 'left' } },



                ],
                mainId: 'id',

            });
            page.search();
        },
        search: function (param) {
            param = param || {};

            $('#gridtable').AgGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
} 