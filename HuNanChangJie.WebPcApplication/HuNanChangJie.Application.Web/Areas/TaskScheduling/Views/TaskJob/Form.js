﻿/* 
 * 创建人：超级管理员 
 * 日  期：2019-01-23 09:06 
 * 描  述：1232131 
 */
var acceptClick;
var keyValue = request('keyValue');
var projectid = request('projectid');
var setCorn;
var bootstrap = function ($, Changjie) {
    "use strict";
    setCorn = function (f) {
        $("#cron").val(f);
    };
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
        },
        bind: function () {
            

            $(".mk-form-item-text").on("click",
                function () {
                    Changjie.layerForm({
                        id: "SelectExpressForm",
                        title: "添加预置表达式",
                        url: top.$.rootUrl + "/TaskScheduling/TaskJob/SelectExpressForm",
                        width: 600,
                        height: 500,
                        btn: null
                    });
                });

            //任务运行状态
            $('#status').mkRadioCheckbox({
                type: 'radio',
                code: 'YesOrNo',
            });
            //失败邮件通知
            $('#isfailmail').mkRadioCheckbox({
                type: 'radio',
                code: 'YesOrNo',
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/TaskScheduling/TaskJob/GetFormData?keyValue=' + keyValue+ '&projectid=' + projectid, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).AgGridSet('refreshdata', data[id]);
                        }
                        else {
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        }
    };
    // 保存数据 
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData())
        };
        $.mkSaveForm(top.$.rootUrl + '/TaskScheduling/TaskJob/SaveForm?keyValue=' + keyValue+ '&projectid=' + projectid, postData, function (res) {
            // 保存成功后才回调 
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
} 