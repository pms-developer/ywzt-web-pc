﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuNanChangJie.Util;

namespace HuNanChangJie.Application.Web.Areas.TaskScheduling.Controllers
{
    public class TaskProjectController : MvcControllerBase
    {
        // GET: TaskScheduling/TaskProject
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Form()
        {
            return View();
        }
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            var Jo = strEntity.ToJObject();
            string url = $"{Config.GetValue("taskmicsApi")}taskProject/f_json/save?id={keyValue}&isrecemail={Jo["isrecemail"].ToString()}&name={Jo["name"].ToString()}&recemail={Jo["recemail"].ToString()}&remark={Jo["remark"].ToString()}&sign={Jo["sign"].ToString()}&signstring={Jo["signstring"].ToString()}";

            string queryResult = HttpMethods.Get(url);

            return Success("添加成功！");
        }
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
           

            string url = $"{Config.GetValue("taskmicsApi")}taskProject/f_json/delete?id={keyValue}"; 
            string queryResult = HttpMethods.Get(url);
            return Success("删除成功！");
        }


        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            string url = $"{Config.GetValue("taskmicsApi")}taskProject/f_view/get?id={keyValue}";

            string queryResult = HttpMethods.Get(url);
            var jO = queryResult.ToJObject();
            var jsonData = new
            {
                task_project = jO
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            string url = $"{Config.GetValue("taskmicsApi")}taskProject/f_json/pageQuery?page={paginationobj.page}&size={paginationobj.rows}&name=";

            string queryResult = HttpMethods.Get(url);

            var mydata = queryResult.ToJObject();
            //var data = serviceInfoIBLL.GetPageList(paginationobj, queryJson);
            paginationobj.records = Convert.ToInt32(mydata["total"]);
            var jsonData = new
            {
                rows = mydata["rows"],
                total = paginationobj.total,
                page = mydata["page"],
                records = mydata["total"]
            };
            return Success(jsonData);
        }
    }
}