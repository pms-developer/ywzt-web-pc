﻿using System;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.TaskScheduling;
using HuNanChangJie.Util;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace HuNanChangJie.Application.Web.Areas.TaskScheduling.Controllers
{
    /// <summary> 
        ///  
        ///   
        /// 创 建：超级管理员 
        /// 日 期：2019-01-22 14:09 
        /// 描 述：服务器列表 
        /// </summary> 
    public class ServiceInfoController : MvcControllerBase
    {
        private ServiceInfoIBLL serviceInfoIBLL = new ServiceInfoBLL();

        #region  视图功能 

        /// <summary> 
                /// 主页面 
                /// <summary> 
                /// <returns></returns> 
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary> 
                /// 表单页 
                /// <summary> 
                /// <returns></returns> 
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region  获取数据 

        /// <summary> 
                /// 获取页面显示列表数据 
                /// <summary> 
                /// <param name="queryJson">查询参数</param> 
                /// <returns></returns> 
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            string url = $"{Config.GetValue("taskmicsApi")}servInfo/f_json/pageQuery?page={paginationobj.page}&size={paginationobj.rows}";

            string queryResult=HttpMethods.Get(url);

            var mydata = queryResult.ToJObject();
            //var data = serviceInfoIBLL.GetPageList(paginationobj, queryJson);
            paginationobj.records = Convert.ToInt32(mydata["total"]) ;
            var jsonData = new
            {
                rows = mydata["rows"],
                total = paginationobj.total,
                page = mydata["page"],
                records = mydata["total"]
            };
            return Success(jsonData);
        }
        /// <summary> 
                /// 获取表单数据 
                /// <summary> 
                /// <returns></returns> 
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var serv_infoData = serviceInfoIBLL.Getserv_infoEntity(keyValue);
            var jsonData = new
            {
                serv_info = serv_infoData,
            };
            return Success(jsonData);
        }


        [HttpGet]
        [AjaxOnly]
        public ActionResult GetServChartData()
        {
            string url = $"{Config.GetValue("taskmicsApi")}servInfo/f_view/chartJson"; 
            string queryResult = HttpMethods.Get(url);
            var mydata = queryResult.ToObject<List<JObject>>();
            List<Object> dd = new   List<object>();
            foreach (var da in mydata)
            {
                dd.Add(new
                {
                    name= da["servid"],
                    value= da["total"]
                });
            }
            var data = new
            {
                tempStyle = "2",
                chartType = "pie",
                chartData = dd,
                listData = ""
            };
            return Content(data.ToJson());
             
        }


        [HttpGet]
        [AjaxOnly]
        public ActionResult GetProjectChartData()
        {
            string url = $"{Config.GetValue("taskmicsApi")}sysUser/f_view/mainJson";
            string queryResult = HttpMethods.Get(url);
             
            return Content(queryResult);

        }



        #endregion

        #region  提交数据 

        /// <summary> 
                /// 删除实体数据 
                /// <param name="keyValue">主键</param> 
                /// <summary> 
                /// <returns></returns> 
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            serviceInfoIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary> 
                /// 保存实体数据（新增、修改） 
                /// <param name="keyValue">主键</param> 
                /// <summary> 
                /// <returns></returns> 
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            serv_infoEntity entity = strEntity.ToObject<serv_infoEntity>();
            serviceInfoIBLL.SaveEntity(keyValue, entity);
            return Success("保存成功！");
        }
        #endregion

    }
}