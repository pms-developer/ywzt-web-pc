﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuNanChangJie.Util;

namespace HuNanChangJie.Application.Web.Areas.TaskScheduling.Controllers
{
    public class TaskJobController : MvcControllerBase
    {
        // GET: TaskScheduling/TaskJob
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson,string keyValue)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            string url = $"{Config.GetValue("taskmicsApi")}taskJob/f_json/pageQuery?page={paginationobj.page}&size={paginationobj.rows}&projectid={keyValue}";

            string queryResult = HttpMethods.Get(url);

            var mydata = queryResult.ToJObject();
            //var data = serviceInfoIBLL.GetPageList(paginationobj, queryJson);
            paginationobj.records = Convert.ToInt32(mydata["total"]);
            var jsonData = new
            {
                rows = mydata["rows"],
                total = paginationobj.total,
                page = mydata["page"],
                records = mydata["total"]
            };
            return Success(jsonData);
           
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue,string projectid)
        {
            string url = $"{Config.GetValue("taskmicsApi")}taskJob/f_view/editJson?id={keyValue}&projectid={projectid}";

            string queryResult = HttpMethods.Get(url);
            var jO = queryResult.ToJObject();
            var jsonData = new
            {
                taskjob_project = jO
            };
            return Success(jsonData);

            //taskJob/f_view/editJso
             
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            string url = $"{Config.GetValue("taskmicsApi")}taskJob/f_json/delete?id={keyValue}";
            string queryResult = HttpMethods.Get(url);
            return Success("删除成功！");
             
        }


        public ActionResult SaveForm(string keyValue,string projectid, string strEntity)
        {
            var Jo = strEntity.ToJObject();
            string args = $"id={keyValue}&projectid={projectid}&name={Jo["name"]}&remark={Jo["remark"]}&link={Jo["link"]}&cron={Jo["cron"]}&status={Jo["status"]}&isfailmail={Jo["isfailmail"]}";
            string url = $"{Config.GetValue("taskmicsApi")}taskJob/f_json/save?{args}"; 
            string queryResult = HttpMethods.Get(url); 
            return Success("添加成功！");

           
        }

        public ActionResult Form()
        {
            return View();
        }

        public ActionResult SelectExpressForm()
        {
            return View();
        }

        public ActionResult UpdateStatus(string id ,int status)
        {

            string url = $"{Config.GetValue("taskmicsApi")}taskJob/f_json/updateStatus?id={id}&status={status}";
            string queryResult = HttpMethods.Get(url);
            if (status==50)
            {
                return Success("任务已经暂停！");
            }
            else if (status == 0)
            {
                return Success("启动任务成功！");

            }

            return Success("成功！");
        }

        public ActionResult ExecJob(string id)
        {

            string url = $"{Config.GetValue("taskmicsApi")}taskJob/f_json/execJob?id={id}";
            string queryResult = HttpMethods.Get(url); 
            return Success("执行成功！");
        }
    }
}