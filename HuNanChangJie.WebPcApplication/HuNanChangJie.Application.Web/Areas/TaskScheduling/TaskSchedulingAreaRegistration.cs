﻿using System.Web.Mvc;
namespace HuNanChangJie.Application.Web.Areas.TaskScheduling
{
    public class TaskSchedulingAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TaskScheduling"; 
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "TaskScheduling_default", 
                "TaskScheduling/{controller}/{action}/{id}", 
                             new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
