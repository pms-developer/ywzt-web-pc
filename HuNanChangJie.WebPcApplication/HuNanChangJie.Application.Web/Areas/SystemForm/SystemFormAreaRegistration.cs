﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.SystemForm
{
    public class SystemFormAreaRegistration : AreaRegistration
    {
        public override string AreaName => "SystemForm";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SystemForm_default",
                "SystemForm/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }


}