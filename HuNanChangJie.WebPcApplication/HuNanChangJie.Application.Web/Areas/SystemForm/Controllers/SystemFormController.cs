﻿using HuNanChangeJie.Application.SystemForm.Bll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.SystemForm.Controllers
{
    public class SystemFormController : MvcControllerBase
    {
        private ISystemFormBll formBll = new SystemFormBll();

        public ActionResult GetFormWorkDetais(string formId)
        {
            var list=formBll.GetWorkflowList(formId);
            var json = new
            {
                WorkflowList = list,
            };
            return JsonResult(json);
        }

        
        /// <summary>
        /// 获取系统表单
        /// </summary>
        /// <returns></returns>
        [HttpGet,AjaxOnly]
        public ActionResult GetSystemForms()
        {
            var list = formBll.GetSystemForms();
            return JsonResult(list);
        }
    }
}