﻿using HuNanChangJie.Util;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace HuNanChangJie.Application.Web.Areas.CodeDemo.Controllers
{
    public class GanttDemoController : MvcControllerBase
    {
        // GET: CodeDemo/GanttDemo
        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult Index2()
        {
            return View();
        }
        public ActionResult Index3()
        {
            return View();
        }
        public ActionResult Index4()
        {
            return View();
        }

        public ActionResult GetTimeList(string parentId)
        {
            if (string.IsNullOrEmpty(parentId))
            {
                string demo =
               "[{\"id\":\"46ef7727-66e9-47fb-bf51-24f8c500233f\",\"text\":\"计划任务1\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"96e08f4b-b852-4852-8301-8e2a2d06d1ad\",\"text\":\"计划任务2\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"9922c894-e29b-45cf-8f02-f40135db0169\",\"text\":\"计划任务3\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"0fed4f06-1034-4089-bba7-2b89b2e2461f\",\"text\":\"计划任务4\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"df8b0156-8d78-4ec2-ae97-3239c067227e\",\"text\":\"计划任务5\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"4211ee8c-13ab-4a37-b0bc-9776a0cf0f7f\",\"text\":\"计划任务6\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"c86954a3-cebb-47a5-9c04-647b022cb354\",\"text\":\"计划任务7\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"0babadd1-d93a-4444-ad2c-d2a60c49ac0a\",\"text\":\"计划任务8\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"fb9e5a5c-7387-41bf-a9bf-d97bebef2213\",\"text\":\"计划任务9\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"a4682db6-6a5d-4052-a241-8a4df22d0ab4\",\"text\":\"计划任务10\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true}]";
                return Success(demo.ToList<JObject>());
            }
            else
            {
                string demo =
              "[{\"id\":\"46ef7727-66e9-47fb-bf51-24f8c500233f\",\"text\":\"计划任务1\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"96e08f4b-b852-4852-8301-8e2a2d06d1ad\",\"text\":\"计划任务2\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"9922c894-e29b-45cf-8f02-f40135db0169\",\"text\":\"计划任务3\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"0fed4f06-1034-4089-bba7-2b89b2e2461f\",\"text\":\"计划任务4\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"df8b0156-8d78-4ec2-ae97-3239c067227e\",\"text\":\"计划任务5\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"4211ee8c-13ab-4a37-b0bc-9776a0cf0f7f\",\"text\":\"计划任务6\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"c86954a3-cebb-47a5-9c04-647b022cb354\",\"text\":\"计划任务7\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"0babadd1-d93a-4444-ad2c-d2a60c49ac0a\",\"text\":\"计划任务8\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"fb9e5a5c-7387-41bf-a9bf-d97bebef2213\",\"text\":\"计划任务9\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true},{\"id\":\"a4682db6-6a5d-4052-a241-8a4df22d0ab4\",\"text\":\"计划任务10\",\"isexpand\":false,\"complete\":false,\"timeList\":[{\"beginTime\":\"2019-02-28\",\"endTime\":\"2019-03-08\",\"color\":\"#3286ed\",\"overtime\":false,\"text\":\"执行时间9天\"}],\"hasChildren\":true}]";
                return Success(demo.ToList<JObject>());
            }
           
        }

        public ActionResult GetPageList(string pagination)
        {
            return null;
        }




    }
}