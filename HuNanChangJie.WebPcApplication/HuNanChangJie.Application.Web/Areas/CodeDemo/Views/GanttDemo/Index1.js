﻿(function ($, Changjie) {
    Date.prototype.DateAdd = function (g, f) {
        var e = this;
        switch (g) {
            case "s":
                return new Date(Date.parse(e) + (1000 * f));
            case "n":
                return new Date(Date.parse(e) + (60000 * f));
            case "h":
                return new Date(Date.parse(e) + (3600000 * f));
            case "d":
                return new Date(Date.parse(e) + (86400000 * f));
            case "w":
                return new Date(Date.parse(e) + ((86400000 * 7) * f));
            case "q":
                return new Date(e.getFullYear(), (e.getMonth()) + f * 3, e.getDate(), e.getHours(), e.getMinutes(), e.getSeconds());
            case "m":
                return new Date(e.getFullYear(), (e.getMonth()) + f, e.getDate(), e.getHours(), e.getMinutes(), e.getSeconds());
            case "y":
                return new Date((e.getFullYear() + f), e.getMonth(), e.getDate(), e.getHours(), e.getMinutes(), e.getSeconds())
        }
    };
    Date.prototype.DateDiff = function (g, e) {
        var f = this;
        if (typeof e == "string") {
            e = Changjie.parseDate(e)
        }
        switch (g) {
            case "s":
                return parseInt((e - f) / 1000);
            case "n":
                return parseInt((e - f) / 60000);
            case "h":
                return parseInt((e - f) / 3600000);
            case "d":
                return parseInt((e - f) / 86400000);
            case "w":
                return parseInt((e - f) / (86400000 * 7));
            case "m":
                return (e.getMonth() + 1) + ((e.getFullYear() - f.getFullYear()) * 12) - (f.getMonth() + 1);
            case "y":
                return e.getFullYear() - f.getFullYear()
        }
    };
    Date.prototype.MaxDayOfDate = function () {
        var g = this;
        var e = Changjie.parseDate(Changjie.formatDate(g, "yyyy-MM-01 00:00:00"));
        var f = e.DateAdd("m", 1);
        var h = e.DateDiff("d", f);
        return h;
    };
    Date.prototype.isLeapYear = function () {
        return (0 == this.getYear() % 4 && ((this.getYear() % 100 != 0) || (this.getYear() % 400 == 0)))
    };
    var b = {
        init: function (i, k) {
            i.addClass("mk-gantt");
            var j = $('<div class="mk-gantt-title">                                <div class="mk-gantt-title-text" >时间维度:</div>                                <div class="btn-group btn-group-sm" >                                </div>                            </div>');
            $.each(k.timebtns,
                function (l, m) {
                    switch (m) {
                        case "month":
                            j.find(".btn-group-sm").append('<a class="btn btn-default month" data-value="month" >月</a>');
                            break;
                        case "week":
                            j.find(".btn-group-sm").append('<a class="btn btn-default week" data-value="week" >周</a>');
                            break;
                        case "day":
                            j.find(".btn-group-sm").append('<a class="btn btn-default day" data-value="day" >天</a>');
                            break;
                        case "hour":
                            j.find(".btn-group-sm").append('<a class="btn btn-default hour" data-value="hour" >时</a>');
                            break
                    }
                });
            var f = $('<div class="mk-gantt-footer" ></div>');
            var e = $('<div class="mk-gantt-body mk-gantt-body-pr" ></div>').css({
                "padding-left": k.leftWidh
            });
            var g = $('<div class="mk-gantt-left" ><div class="mk-gantt-left-content" ></div></div>').css({
                width: k.leftWidh
            });
            var h = $('<div class="mk-gantt-right" >                                <div class="mk-gantt-rightheader" >                                </div>                                <div class="mk-gantt-rightbody" ></div>                            </div>');
            g.append('<div class="mk-gantt-move" ></div>');
            h.append('<div class="mk-gantt-nodata-img"  ><img src="' + k.imgUrl + '"></div>');
            e.append('<div class="mk-gantt-showtext" >                            <div class="mk-gantt-showtext-title-remove"><i title="关闭" class="fa fa-close"></i></div>                            <div class="mk-gantt-showtext-content" ></div>                          </div>');
            k._row = 0;
            b.initTitle(j, k);
            b.initFooter(f, i, k);
            b.initLeft(g, i, k);
            b.initRight(h, i, k);
            b.initMove(i);
            b.initShow(e);
            e.append(g);
            e.append(h);
            i.append(j);
            i.append(e);
            i.append(f);
            b.renderData(i, k);
            j = null;
            e = null;
            f = null;
            g = null;
            h = null;
            i = null;
        },
        initShow: function (e) {
            e.find(".mk-gantt-showtext-content").mkscroll();
            e.find(".mk-gantt-showtext-title-remove").on("click",
                function () {
                    var f = $(this);
                    f.parent().removeClass("active");
                    f.parents(".mk-gantt").removeClass("mk-gantt-showtext-active");
                    f.parent().find(".mk-scroll-box").html("");
                    f = null
                })
        },
        initMove: function (e) {
            e.on("mousedown",
                function (i) {
                    i = i || window.event;
                    var j = i.target || i.srcElement;
                    var f = $(j);
                    var g = $(this);
                    var h = g[0].dfop;
                    if (f.hasClass("mk-gantt-move")) {
                        h._ismove = true;
                        h._pageX = i.pageX
                    }
                });
            e.mousemove(function (i) {
                var g = $(this);
                var h = g[0].dfop;
                if (h._ismove) {
                    var f = g.find(".mk-gantt-left");
                    f.css("width", h.leftWidh + (i.pageX - h._pageX));
                    g.find(".mk-gantt-body").css("padding-left", h.leftWidh + (i.pageX - h._pageX))
                }
            });
            e.on("click",
                function (h) {
                    var f = $(this);
                    var g = f[0].dfop;
                    if (g._ismove) {
                        g.leftWidh += (h.pageX - g._pageX);
                        g._ismove = false
                    }
                })
        },
        initTitle: function (e, f) {
            e.find("." + f.type).addClass("active");
            e.find("a").on("click",
                function () {
                    var h = $(this);
                    if (!h.hasClass("active")) {
                        h.parent().find(".active").removeClass("active");
                        var g = h.parents(".mk-gantt");
                        var i = g[0].dfop;
                        i.type = h.attr("data-value");
                        setTimeout(function () {
                            b.renderRightHeader(g.find(".mk-gantt-rightheader"), f);
                            b.renderRightData(g, f);
                            g = null
                        });
                        h.addClass("active")
                    }
                    h = null
                })
        },
        initFooter: function (e, g, h) {
            if (h.isPage) {
                var f = $('<div class="mk-gantt-page-bar" id="gantt_page_bar_' + h.id + '"><div class="mk-gantt-page-bar-info" >无显示数据</div>                <div class="paginations" id="gantt_page_bar_nums_' + h.id + '" style="display:none;" >                <ul class="pagination pagination-sm"><li><a href="javascript:void(0);" class="pagebtn">首页</a></li></ul>                <ul class="pagination pagination-sm"><li><a href="javascript:void(0);" class="pagebtn">上一页</a></li></ul>                <ul class="pagination pagination-sm" id="gantt_page_bar_num_' + h.id + '" ></ul>                <ul class="pagination pagination-sm"><li><a href="javascript:void(0);" class="pagebtn">下一页</a></li></ul>                <ul class="pagination pagination-sm"><li><a href="javascript:void(0);" class="pagebtn">尾页</a></li></ul>                <ul class="pagination"><li><span></span></li></ul>                <ul class="pagination"><li><input type="text" class="form-control"></li></ul>                <ul class="pagination pagination-sm"><li><a href="javascript:void(0);" class="pagebtn">跳转</a></li></ul>                </div></div>');
                e.append(f);
                f.find("#gantt_page_bar_num_" + h.id).on("click", b.turnPage);
                f.find("#gantt_page_bar_nums_" + h.id + " .pagebtn").on("click", {
                    op: h
                },
                    b.turnPage2);
                f = null
            } else {
                g.addClass("mk-gantt-nopage")
            }
            e = null
        },
        initLeft: function (e, f, g) {
            e.find(".mk-gantt-left-content").mkscroll(function (h, i) {
                if (!f.is(":hidden")) {
                    f.find(".mk-gantt-rightbody").mkscrollSet("moveY", i)
                }
            })
        },
        initRight: function (e, f, g) {
            e.find(".mk-gantt-rightbody").mkscroll(function (h, i) {
                if (!f.is(":hidden")) {
                    f.find(".mk-gantt-rightheader").css("left", -h);
                    f.find(".mk-gantt-left-content").mkscrollSet("moveY", i)
                }
            })
        },
        renderRightHeader: function (f, n) {
            f.hide();
            f.html("");
            n._time = c.getBoundaryDatesFromData(n.data, n.cellNum, n.type);
            var g = $('<div class="mk-gantt-rightheader-months" ></div>');
            var e = $('<div class="mk-gantt-rightheader-days" ></div>');
            var m = 0;
            var l = n._time.min;
            var q = "";
            var p = 0;
            var h = null;
            n._num = 0;
            switch (n.type) {
                case "month":
                    m = n._time.min.DateDiff("m", n._time.max) + 1;
                    p = 0;
                    for (var k = 0; k < m; k++) {
                        var j = l.getFullYear();
                        if (q != j) {
                            q = j;
                            if (h != null) {
                                h.css({
                                    width: p * 28
                                })
                            }
                            h = $('<div class="mk-gantt-rightheader-month" >' + q + "</div>");
                            g.append(h);
                            p = 0
                        }
                        e.append('<div class="mk-gantt-rightheader-day" >' + (l.getMonth() + 1) + "</div>");
                        l = l.DateAdd("m", 1);
                        p++;
                        n._num++
                    }
                    h.css({
                        width: p * 28
                    });
                    break;
                case "week":
                    m = n._time.min.DateDiff("w", n._time.max) + 1;
                    p = 0;
                    var o = null;
                    for (var k = 0; k < m; k++) {
                        var j = n.monthNames[l.getMonth()] + "/" + l.getFullYear();
                        if (q != j) {
                            q = j;
                            if (h != null) {
                                if (n._time.min.DateDiff("m", o) > 0) {
                                    h.css({
                                        width: o.MaxDayOfDate() * 4
                                    })
                                } else {
                                    h.css({
                                        width: (o.MaxDayOfDate() - o.getDate() + 1) * 4
                                    })
                                }
                            }
                            o = l;
                            h = $('<div class="mk-gantt-rightheader-month" >' + q + "</div>");
                            g.append(h);
                            p = 0
                        }
                        e.append('<div class="mk-gantt-rightheader-day" >' + c.getWeekNumber(l) + "</div>");
                        l = l.DateAdd("w", 1);
                        p++;
                        n._num++
                    }
                    h.css({
                        width: (l.DateAdd("w", -1).getDate() + 6) * 4
                    });
                    break;
                case "day":
                    m = n._time.min.DateDiff("d", n._time.max) + 1;
                    p = 0;
                    for (var k = 0; k < m; k++) {
                        var j = n.monthNames[l.getMonth()] + "/" + l.getFullYear();
                        if (q != j) {
                            q = j;
                            if (h != null) {
                                h.css({
                                    width: p * 28
                                })
                            }
                            h = $('<div class="mk-gantt-rightheader-month" >' + q + "</div>");
                            g.append(h);
                            p = 0
                        }
                        e.append('<div class="mk-gantt-rightheader-day ' + (c.isWeekend(l) ? "mk-gantt-weekend" : "") + ' " >' + l.getDate() + "</div>");
                        l = l.DateAdd("d", 1);
                        p++;
                        n._num++
                    }
                    h.css({
                        width: p * 28
                    });
                    break;
                case "hour":
                    m = n._time.min.DateDiff("h", n._time.max) + 1;
                    p = 0;
                    for (var k = 0; k < m; k++) {
                        var j = l.getDate() + "/" + n.monthNames[l.getMonth()] + "/" + l.getFullYear();
                        if (q != j) {
                            q = j;
                            if (h != null) {
                                h.css({
                                    width: p * 28
                                })
                            }
                            h = $('<div class="mk-gantt-rightheader-month" >' + q + "</div>");
                            g.append(h);
                            p = 0
                        }
                        e.append('<div class="mk-gantt-rightheader-day " >' + l.getHours() + "</div>");
                        l = l.DateAdd("h", 1);
                        p++;
                        n._num++
                    }
                    h.css({
                        width: p * 28
                    });
                    break
            }
            n._width = n._num * 28;
            f.css("width", n._width + "px");
            f.append(g);
            f.append(e);
            f.show();
            f = null;
            g = null;
            e = null
        },
        renderRightGird: function (f, i) {
            f.hide();
            f.css({
                width: i._width
            });
            f.html("");
            var g = $('<div class="mk-gantt-grid-row"></div>');
            for (var h = 0; h < i._num; h++) {
                var e = $('<div class="mk-gantt-grid-row-cell" ></div>', {
                    "class": "ganttview-grid-row-cell"
                });
                g.append(e);
                e = null
            }
            for (var h = 0; h < i._row; h++) {
                f.append(g.clone())
            }
            f.show();
            if (i._row == 0) {
                f.parents(".mk-gantt-right").find(".mk-gantt-nodata-img").show()
            } else {
                f.parents(".mk-gantt-right").find(".mk-gantt-nodata-img").hide()
            }
            g = null;
            f = null
        },
        loadData: function (e, h) {
            var f = h.param || {};
            if (h.isPage) {
                Changjie.loading(true, "正在获取数据");
                h.pageparam = h.pageparam || {
                    rows: h.rows,
                    page: 1,
                    sidx: "",
                    sord: "",
                    records: 0,
                    total: 0
                };
                h.pageparam.rows = h.rows;
                h.pageparam.sidx = h.sidx;
                h.pageparam.sord = h.sord;
                h.pageparam.page = h.pageparam.page || 1;
                h.pageparam.records = 0;
                h.pageparam.total = 0;
                h.param = h.param || {};
                delete h.param.pagination;
                var g = JSON.stringify(h.param);
                if (h.paramString != g) {
                    h.paramString = g;
                    h.pageparam.page = 1
                }
                h.param.pagination = JSON.stringify(h.pageparam);
                
                Changjie.httpAsync("GET", h.url, h.param,
                    function (p) {
                        Changjie.loading(false);
                        if (p) {
                            h.data = p.rows;
                            h.pageparam.page = p.page;
                            h.pageparam.records = p.records;
                            h.pageparam.total = p.total
                        } else {
                            h.data = [];
                            h.pageparam.page = 1;
                            h.pageparam.records = 0;
                            h.pageparam.total = 0
                        }
                        b.renderData(e, h);
                        var j = e.find("#gantt_page_bar_" + h.id);
                        var k = j.find("#gantt_page_bar_num_" + h.id);
                        var l = j.find("#gantt_page_bar_nums_" + h.id);
                        var t = "";
                        var o = "";
                        if (h.data.length == 0) {
                            t = "无显示数据"
                        } else {
                            var u = h.pageparam;
                            var m = (u.page - 1) * u.rows + 1;
                            var q = m + h.data.length - 1;
                            t = "显示第 " + m + " - " + q + " 条记录  <span>|</span> 检索到 " + u.records + " 条记录";
                            if (u.total > 1) {
                                var n = u.page - 6;
                                n = n < 0 ? 0 : n;
                                var r = n + 10;
                                if (r > u.total) {
                                    r = u.total
                                }
                                if ((r - n) < 10) {
                                    n = r - 10
                                }
                                n = n < 0 ? 0 : n;
                                for (var s = n; s < r; s++) {
                                    o += '<li><a href="javascript:void(0);" class=" pagebtn ' + ((s + 1) == u.page ? "active" : "") + '" >' + (s + 1) + "</a></li>"
                                }
                                l.find("span").text("共" + u.total + "页,到");
                                l.show()
                            } else {
                                l.hide()
                            }
                        }
                        k.html(o);
                        j.find(".mk-gantt-page-bar-info").html(t);
                        h.onRenderComplete && h.onRenderComplete(h.data)
                    })
            } else {
                if (h.url) {
                    Changjie.loading(true, "正在获取数据");
                    
                    Changjie.httpAsync("GET", h.url, f,
                        function (i) {
                            Changjie.loading(false);
                            h.data = i || [];
                            b.renderData(e, h);
                            h.onRenderComplete && h.onRenderComplete(h.data)
                        })
                }
            }
        },
        turnPage: function (i) {
            i = i || window.event;
            var h = $(this);
            var g = $("#" + h.attr("id").replace("gantt_page_bar_num_", ""));
            var k = g[0].dfop;
            var j = i.target || i.srcElement;
            var f = $(j);
            if (f.hasClass("pagebtn")) {
                var l = parseInt(f.text());
                if (l != k.pageparam.page) {
                    h.find(".active").removeClass("active");
                    f.addClass("active");
                    k.pageparam.page = l;
                    b.loadData(g, k)
                }
            }
        },
        turnPage2: function (h) {
            var g = $(this);
            var k = h.data.op;
            var j = g.text();
            var f = $("#gantt_page_bar_num_" + k.id);
            var m = parseInt(f.find(".active").text());
            var i = false;
            switch (j) {
                case "首页":
                    if (m != 1) {
                        k.pageparam.page = 1;
                        i = true
                    }
                    break;
                case "上一页":
                    if (m > 1) {
                        k.pageparam.page = m - 1;
                        i = true
                    }
                    break;
                case "下一页":
                    if (m < k.pageparam.total) {
                        k.pageparam.page = m + 1;
                        i = true
                    }
                    break;
                case "尾页":
                    if (m != k.pageparam.total) {
                        k.pageparam.page = k.pageparam.total;
                        i = true
                    }
                    break;
                case "跳转":
                    var n = g.parents("#gantt_page_bar_nums_" + k.id).find("input").val();
                    if (!!n) {
                        var l = parseInt(n);
                        if (String(l) != "NaN") {
                            if (l < 1) {
                                l = 1
                            }
                            if (l > k.pageparam.total) {
                                l = k.pageparam.total
                            }
                            k.pageparam.page = l;
                            i = true
                        }
                    }
                    break
            }
            if (i) {
                b.loadData($("#" + k.id), k)
            }
        },
        renderData: function (e, f) {
            b.hideinfo(e);
            b.renderRightHeader(e.find(".mk-gantt-rightheader"), f);
            b.renderLeftData(e, f);
            b.renderRightData(e, f)
        },
        renderLeftData: function (f, k) {
            var g = $('<ul class="mk-gantt-tree-root" ></ul>');
            var h = k.data.length;
            k._timeDatas = {};
            k._row = 0;
            for (var j = 0; j < h; j++) {
                var e = b.renderNode(k.data[j], 0, j, k, true);
                g.append(e)
            }
            f.find(".mk-gantt-left .mk-scroll-box").html(g)
        },
        renderNode: function (w, p, x, q, r) {
            if (r) {
                q._timeDatas[x + ""] = w;
                q._row++
            }
            w._deep = p;
            w._path = x;
            var v = w.id.replace(/[^\w]/gi, "_");
            var y = w.title || w.text;
            var g = $('<li class="mk-gantt-tree-node"></li>');
            var h = $('<div id="' + q.id + "_" + v + '" tpath="' + x + '" title="' + y + '"  dataId="' + q.id + '"  class="mk-gantt-tree-node-el" ></div>');
            if (w.hasChildren) {
                var o = (w.isexpand || q.isAllExpand) ? "mk-gantt-tree-node-expanded" : "mk-gantt-tree-node-collapsed";
                h.addClass(o)
            } else {
                h.addClass("mk-gantt-tree-node-leaf")
            }
            var i = $('<span class="mk-gantt-tree-node-indent"></span>');
            if (p == 1) {
                i.append('<img class="mk-gantt-tree-icon" src="' + q.cbiconpath + 's.gif"/>')
            } else {
                if (p > 1) {
                    i.append('<img class="mk-gantt-tree-icon" src="' + q.cbiconpath + 's.gif"/>');
                    for (var s = 1; s < p; s++) {
                        i.append('<img class="mk-gantt-tree-icon" src="' + q.cbiconpath + 's.gif"/>')
                    }
                }
            }
            h.append(i);
            var f = $('<img class="mk-gantt-tree-ec-icon" src="' + q.cbiconpath + 's.gif"/>');
            h.append(f);
            var n = '<a class="mk-gantt-tree-node-anchor" href="javascript:void(0);">';
            n += '<span data-value="' + w.id + '" class="mk-gantt-tree-node-text" >' + w.text + "</span>";
            n += "</a>";
            h.append(n);
            h.on("click", b.nodeClick);
            if (!w.complete) {
                h.append('<div class="mk-gantt-tree-loading"><img class="mk-gantt-tree-ec-icon" src="' + q.cbiconpath + 'loading.gif"/></div>')
            }
            g.append(h);
            if (w.hasChildren) {
                var m = $('<ul  class="mk-gantt-tree-node-ct" >');
                if (!w.isexpand && !q.isAllExpand) {
                    m.css("display", "none")
                }
                if (w.children) {
                    var u = w.children.length;
                    for (var t = 0; t < u; t++) {
                        w.children[t].parent = w;
                        var e = b.renderNode(w.children[t], p + 1, x + "." + t, q);
                        m.append(e)
                    }
                    g.append(m)
                }
            }
            w.render = true;
            return g
        },
        renderNodeAsync: function (f, m, h) {
            var g = $('<ul  class="mk-gantt-tree-node-ct" >');
            if (!m.isexpand && !h.isAllExpand) {
                g.css("display", "none")
            }
            if (m.children) {
                var j = m.children.length;
                for (var i = 0; i < j; i++) {
                    m.children[i].parent = m;
                    var e = b.renderNode(m.children[i], m._deep + 1, m._path + "." + i, h);
                    g.append(e)
                }
                f.parent().append(g)
            }
            return g
        },
        getItem: function (h, f) {
            var e = h.split(".");
            var j = f.data;
            for (var g = 0; g < e.length; g++) {
                if (g == 0) {
                    j = j[e[g]]
                } else {
                    j = j.children[e[g]]
                }
            }
            return j
        },
        nodeClick: function (k) {
            k = k || window.event;
            var l = k.target || k.srcElement;
            var h = $(this);
            var g = $("#" + h.attr("dataId"));
            var j = g[0].dfop;
            var o = h.attr("tpath");
            var m = b.getItem(o, j);
            if (l.tagName == "IMG") {
                var f = $(l);
                var i = h.next(".mk-gantt-tree-node-ct");
                if (f.hasClass("mk-gantt-tree-ec-icon")) {
                    if (h.hasClass("mk-gantt-tree-node-expanded")) {
                        i.slideUp(200,
                            function () {
                                h.removeClass("mk-gantt-tree-node-expanded");
                                h.addClass("mk-gantt-tree-node-collapsed");
                                b.removeTimeDatas(m.children, j);
                                b.renderRightData(g, j)
                            })
                    } else {
                        if (h.hasClass("mk-gantt-tree-node-collapsed")) {
                            if (!m.complete) {
                                if (!m._loading) {
                                    m._loading = true;
                                    h.find(".mk-gantt-tree-loading").show();
                                    var n = j.childParam || {};
                                    n.parentId = m.id;
                                    var p = j.childUrl || j.url;
                                    
                                    Changjie.httpAsync("GET", p, n,
                                        function (e) {
                                            if (e) {
                                                m.children = e;
                                                i = b.renderNodeAsync(h, m, j);
                                                i.slideDown(200,
                                                    function () {
                                                        h.removeClass("mk-gantt-tree-node-collapsed");
                                                        h.addClass("mk-gantt-tree-node-expanded");
                                                        b.addTimeDatas(m.children, j);
                                                        b.renderRightData(g, j)
                                                    });
                                                m.complete = true;
                                                h.find(".mk-gantt-tree-loading").hide()
                                            }
                                            m._loading = false
                                        })
                                }
                            } else {
                                i.slideDown(200,
                                    function () {
                                        h.removeClass("mk-gantt-tree-node-collapsed");
                                        h.addClass("mk-gantt-tree-node-expanded");
                                        b.addTimeDatas(m.children, j);
                                        b.renderRightData(g, j)
                                    })
                            }
                        }
                    }
                }
            } else {
                j.currentItem = m;
                g.find(".mk-gantt-tree-selected").removeClass("mk-gantt-tree-selected");
                h.addClass("mk-gantt-tree-selected");
                j.click && j.click(m, h)
            }
            return false
        },
        addTimeDatas: function (e, f) {
            $.each(e,
                function (h, i) {
                    var k = i.id.replace(/[^\w]/gi, "_");
                    var j = f.id + "_" + k;
                    var g = $("#" + j);
                    if (!g.is(":hidden")) {
                        var l = g.attr("tpath");
                        f._timeDatas[l] = i;
                        f._row++;
                        if (i.hasChildren && i.children && i.children.length) {
                            b.addTimeDatas(i.children, f)
                        }
                    }
                })
        },
        removeTimeDatas: function (e, f) {
            $.each(e,
                function (h, i) {
                    var k = i.id.replace(/[^\w]/gi, "_");
                    var j = f.id + "_" + k;
                    var g = $("#" + j);
                    if (g.is(":hidden")) {
                        var l = g.attr("tpath");
                        if (f._timeDatas) {
                            delete f._timeDatas[l];
                            f._row--
                        }
                        if (i.hasChildren && i.children && i.children.length) {
                            b.addTimeDatas(i.children, f)
                        }
                    }
                })
        },
        renderRightData: function (f, h) {
            b.renderRightGird(f.find(".mk-gantt-rightbody .mk-scroll-box"), h);
            var e = $('<div class="mk-gantt-blocks" ></div>');
            var g = [];
            $.each(h._timeDatas,
                function (i, j) {
                    if (i.indexOf(".") == -1) {
                        g.push(j);
                        b.addRightChildTimeDatas(h._timeDatas, g, i)
                    }
                });
            $.each(g,
                function (j, k) {
                    var i = $('<div class="mk-gantt-block-container" ></div>');
                    $.each(k.timeList || [],
                        function (m, n) {
                            var o = c.getDateBlock(n.beginTime, n.endTime, h);
                            if (o.width > 0) {
                                var l = $('<div class="mk-gantt-block" ><div class="mk-gantt-block-text" ></div></div>').css({
                                    width: o.width,
                                    left: o.left,
                                    "background-color": n.color || "#3286ed"
                                });
                                if (n.text) {
                                    l.find(".mk-gantt-block-text").text(n.text)
                                }
                                if (n.overtime) {
                                    l.append('<div class="mk-gantt-block-icon" title="超时" ><i class="fa fa-arrow-circle-down" ></i></div>')
                                }
                                l[0].ganttData = {
                                    item: k,
                                    mytime: n
                                };
                                l.on("click", {
                                    op: h
                                },
                                    function (q) {
                                        q = q || window.event;
                                        var p = q.data.op;
                                        var r = $(this)[0].ganttData;
                                        p.timeClick && p.timeClick(r, $("#" + p.id))
                                    });
                                l.on("dblclick", {
                                    op: h
                                },
                                    function (q) {
                                        q = q || window.event;
                                        var p = q.data.op;
                                        var r = $(this)[0].ganttData;
                                        p.timeDoubleClick && p.timeDoubleClick(r, $("#" + p.id))
                                    });
                                l.hover(function () {
                                    var p = $(this)[0].ganttData;
                                    h.timeHover && h.timeHover(p, true, $("#" + h.id))
                                },
                                    function () {
                                        var p = $(this)[0].ganttData;
                                        h.timeHover && h.timeHover(p, false, $("#" + h.id))
                                    });
                                i.append(l)
                            }
                        });
                    e.append(i)
                });
            f.find(".mk-gantt-rightbody .mk-scroll-box").append(e)
        },
        addRightChildTimeDatas: function (g, e, i) {
            var h = 0;
            while (true) {
                var f = i + "." + h;
                if (g[f]) {
                    e.push(g[f]);
                    b.addRightChildTimeDatas(g, e, f);
                    h++
                } else {
                    break
                }
            }
        },
        showInfo: function (f, h) {
            var e = f.find(".mk-gantt-showtext-content .mk-scroll-box");
            e.html(h);
            var g = f.find(".mk-gantt-showtext");
            if (!g.hasClass("active")) {
                g.addClass("active");
                f.addClass("mk-gantt-showtext-active")
            }
        },
        hideinfo: function (f) {
            var e = f.find(".mk-gantt-showtext-content .mk-scroll-box");
            var g = f.find(".mk-gantt-showtext");
            if (g.hasClass("active")) {
                g.removeClass("active");
                f.removeClass("mk-gantt-showtext-active");
                e.html("")
            }
        }
    };
    var c = {
        getDateBlock: function (l, h, j) {
            l = c.parseDate(l, "h");
            h = c.parseDate(h, "h");
            var m = 0;
            var e = 0;
            var k = {
                left: 0,
                width: 0
            };
            switch (j.type) {
                case "day":
                    m = l.DateDiff("d", h) + 1;
                    e = j._time.min.DateDiff("d", l);
                    break;
                case "week":
                    e = j._time.min.DateDiff("w", l);
                    var i = j._time.min.DateDiff("w", h);
                    m = i - e + 1;
                    break;
                case "month":
                    e = j._time.min.DateDiff("m", l);
                    var g = j._time.min.DateDiff("m", h);
                    m = g - e + 1;
                    break;
                case "hour":
                    e = j._time.min.DateDiff("h", l);
                    var f = j._time.min.DateDiff("h", h);
                    m = f - e + 1;
                    break
            }
            k.left = e * 28 + 2;
            k.width = m * 28 - 4;
            return k
        },
        isLeapYear: function (e) {
            return (e % 400 == 0) || (e % 4 == 0 && e % 100 != 0)
        },
        getMonthDays: function (f, e) {
            return [31, (c.isLeapYear(f) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][e]
        },
        getWeekNumber: function (j) {
            var l = j.getFullYear();
            var h = j.getMonth();
            var f = j.getDate();
            for (var g = 0; g < h; g++) {
                f += c.getMonthDays(l, g)
            }
            var m = (new Date(l, 0, 1)).getDay();
            f += m;
            var k = Math.ceil(f / 7);
            var e = 7 - j.getDay();
            if (j.DateAdd("d", e).getFullYear() != l) {
                return 1
            }
            return k
        },
        isWeekend: function (e) {
            return e.getDay() % 6 == 0
        },
        getBoundaryDatesFromData: function (f, j, l) {
            var k = {
                min: c.parseDate(new Date(), "h"),
                max: c.parseDate(new Date(), "h")
            };
            c.getMinMax(f, k, true);
            switch (l) {
                case "month":
                    k.min = c.parseDate(k.min, "m");
                    k.max = c.parseDate(k.max, "m").DateAdd("m", 1);
                    if (k.min.DateDiff("m", k.max) < j) {
                        k.max = k.min.DateAdd("m", j)
                    }
                    if (k.min.getMonth() == 11) {
                        k.min = k.min.DateAdd("m", -1)
                    }
                    if (k.max.getMonth() == 0) {
                        k.max = k.max.DateAdd("m", 1)
                    }
                    break;
                case "week":
                    k.min = c.parseDate(k.min, "w");
                    k.max = c.parseDate(k.max, "w").DateAdd("w", 1);
                    if (k.min.DateDiff("w", k.max) < j) {
                        k.max = k.min.DateAdd("w", j)
                    }
                    if (k.min.MaxDayOfDate() - k.min.getDate() + 1 < 21) {
                        var e = k.min.getDate() - k.min.getDate() % 7;
                        k.min = k.min.DateAdd("d", -e)
                    }
                    if (k.max.getDate() < 21) {
                        var e = parseInt((k.max.MaxDayOfDate() - k.max.getDate()) / 7);
                        k.max = k.max.DateAdd("w", e)
                    }
                    break;
                case "day":
                    if (k.min.DateDiff("d", k.max) < j) {
                        k.max = k.min.DateAdd("d", j)
                    }
                    var i = k.min.MaxDayOfDate();
                    var h = k.min.getDate();
                    var g = k.max.getDate();
                    if (i - h < 2) {
                        k.min = k.min.DateAdd("d", -(2 + h - i))
                    }
                    if (g < 3) {
                        k.max = k.max.DateAdd("d", (3 - g))
                    }
                    break;
                case "hour":
                    if (k.min.DateDiff("h", k.max) < j) {
                        k.max = k.min.DateAdd("h", j)
                    }
                    break
            }
            return k
        },
        getMinMax: function (e, g, f) {
            $.each(e || [],
                function (h, i) {
                    $.each(i.timeList,
                        function (j, k) {
                            var m = c.parseDate(k.beginTime, "h");
                            var l = c.parseDate(k.endTime, "h");
                            if (f) {
                                g.min = m;
                                g.max = l;
                                f = false
                            }
                            if (g.min.DateDiff("h", m) < 0) {
                                g.min = m
                            }
                            if (g.max.DateDiff("h", l) > 0) {
                                g.max = l
                            }
                        });
                    if (e.children && e.children.length > 0) {
                        c.getMinMax(e.children, g, false)
                    }
                })
        },
        parseDate: function (f, g) {
            switch (g) {
                case "d":
                    return Changjie.parseDate(Changjie.formatDate(f, "yyyy-MM-dd 00:00:00"));
                    break;
                case "w":
                    var e = Changjie.parseDate(Changjie.formatDate(f, "yyyy-MM-dd 00:00:00"));
                    var h = e.getDay();
                    return e.DateAdd("d", (1 - h));
                    break;
                case "m":
                    return Changjie.parseDate(Changjie.formatDate(f, "yyyy-MM-01 00:00:00"));
                    break;
                case "h":
                    return Changjie.parseDate(Changjie.formatDate(f, "yyyy-MM-dd hh:00:00"));
                    break;
                default:
                    return Changjie.parseDate(Changjie.formatDate(f, "yyyy-MM-dd 00:00:00"));
                    break
            }
        }
    };
    $.fn.mkGantt = function (g) {
        var f = {
            url: false,
            childUrl: false,
            data: [],
            param: false,
            childParam: false,
            leftWidh: 200,
            type: "day",
            timebtns: ["month", "week", "day", "hour"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一", "十二"],
            isAllExpand: false,
            isPage: false,
            rows: 50,
            imgUrl: top.$.rootUrl + "/Content/images/jfgrid/nodata.jpg",
            cbiconpath: top.$.rootUrl + "/Content/images/changjietree/",
            cellNum: 100,
            click: false,
            timeClick: false,
            timeDoubleClick: false,
            timeHover: false,
            onRenderComplete: false
        };
        $.extend(f, g || {});
        var e = $(this);
        e[0].dfop = f;
        f.id = e.attr("id");
        b.init(e, f);
        return e
    };
    $.fn.mkGanttSet = function (g, f) {
        var e = $(this);
        var h = e[0].dfop;
        switch (g) {
            case "showinfo":
                b.showInfo(e, f);
                break;
            case "hideinfo":
                b.hideinfo(e);
                break;
            case "refreshdata":
                h.data = f || [];
                b.renderData(e, h);
                break;
            case "reload":
                if (f) {
                    h.param = f
                }
                b.loadData(e, h);
                break
        }
    }
})(window.jQuery, top.Changjie);

var bootstrap = function (a, c) {
    var b = [];
    var d = {
        init: functionChangjie
            for (var f = 0; f < 10; f++) {
                var e = new Date();
                e = e.DateAdd("d", f * 2);
                var g = {
                    id: c.newGuid(),
                    text: "计划任务" + (f + 1),
                    isexpand: false,
                    complete: true,
                    timeList: [{
                        beginTime: c.formatDate(e, "yyyy-MM-dd"),
                        endTime: c.formatDate(e.DateAdd("d", 3), "yyyy-MM-dd"),
                        color: "#3286ed",
                        overtime: true,
                        text: "执行时间9天"
                    },
                    {
                        beginTime: c.formatDate(e.DateAdd("d", 4), "yyyy-MM-dd"),
                        endTime: c.formatDate(e.DateAdd("d", 7), "yyyy-MM-dd"),
                        color: "#1bb99a",
                        overtime: false,
                        text: "执行时间4天"
                    }]
                };
                b.push(g)
            }
            d.initGantt();
            d.bind()
        },
        bind: function () {
            a("#btn_Search").on("click",
                function () {
                    var f = a("#txt_Keyword").val();
                    if (f) {
                        var e = [];
                        a.each(b,
                            function (g, h) {
                                if (h.text.indexOf(f) != -1) {
                                    e.push(h)
                                }
                            });
                        a("#gridtable").mkGanttSet("refreshdata", e)
                    } else {
                        a("#gridtable").mkGanttSet("refreshdata", b)
                    }
                });
            a("#refresh").on("click",
                function () {
                    location.reload()
                })
        },
        initGantt: function () {
            a("#gridtable").mkGantt({
                data: b,
                timeHover: function (g, h, e) {
                    if (h) {
                        var f = '<div class="title" >任务名称</div><div><input  type="text" class="text" value="' + g.item.text + '" ></div>';
                        f += '<div class="title" >开始时间</div><div><input  type="text" class="text" value="' + c.formatDate(g.mytime.beginTime, "yyyy-MM-dd") + '" ></div>';
                        f += '<div class="title" >结束时间</div><div><input  type="text" class="text" value="' + c.formatDate(g.mytime.endTime, "yyyy-MM-dd") + '" ></div>';
                        e.mkGanttSet("showinfo", f)
                    } else { }
                },
                timebtns: ["month", "week", "day"],
                timeClick: function (f, e) {
                    console.log("单击", f)
                },
                timeDoubleClick: function (f, e) {
                    console.log("双击", f)
                },
                click: function (f, e) {
                    console.log("点击", f)
                }
            })
        },
        search: function (e) { }
    };
    d.init();
};