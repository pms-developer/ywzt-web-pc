﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuNanChangJie.Util;
using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json.Linq;

namespace HuNanChangJie.Application.Web.Areas.Backup
{
    /// <summary>
    /// 
    ///  
    /// 日 期：2021-01-25 11:35
    /// 描 述：服务器备份项目文件
    /// </summary>
    public class ServerBackupController : MvcControllerBase
    {
        [HttpPost]
        public ActionResult BackUpServerProject(string keyValue)
        {
            bool result = false;
            var mydata = keyValue.ToObject<JObject>();
            if (mydata != null)
            {
                string backDic = mydata["backupDic"].ToString();
                string projectPath = Server.MapPath("~/");
                if (!string.IsNullOrEmpty(backDic) && !string.IsNullOrEmpty(projectPath))
                {
                    if (System.IO.Directory.Exists(projectPath))
                        return Fail("备份失败");

                    ZipOutputStream zipStream = new ZipOutputStream(System.IO.File.Create(backDic));
                    zipStream.SetLevel(6);

                    result = ZipDirectory(projectPath, zipStream, "");

                    zipStream.Finish();
                    zipStream.Close();
                }
            }
            if (result)
                return Success("备份成功");
            else
                return Fail("备份失败");
        }



        private static bool ZipDirectory(string folderToZip, ZipOutputStream zipStream, string parentFolderName)
        {
            bool result = true;
            string[] folders, files;
            ZipEntry ent = null;
            FileStream fs = null;
            Crc32 crc = new Crc32();

            try
            {
                ent = new ZipEntry(Path.Combine(parentFolderName, Path.GetFileName(folderToZip) + "/"));
                zipStream.PutNextEntry(ent);
                zipStream.Flush();

                files = Directory.GetFiles(folderToZip);
                foreach (string file in files)
                {
                    fs = System.IO.File.OpenRead(file);

                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    ent = new ZipEntry(Path.Combine(parentFolderName, Path.GetFileName(folderToZip) + "/" + Path.GetFileName(file)));
                    ent.DateTime = DateTime.Now;
                    ent.Size = fs.Length;

                    fs.Close();

                    crc.Reset();
                    crc.Update(buffer);

                    ent.Crc = crc.Value;
                    zipStream.PutNextEntry(ent);
                    zipStream.Write(buffer, 0, buffer.Length);
                }

            }
            catch
            {
                result = false;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
                if (ent != null)
                {
                    ent = null;
                }
                GC.Collect();
                GC.Collect(1);
            }

            folders = Directory.GetDirectories(folderToZip);
            foreach (string folder in folders)
                if (!ZipDirectory(folder, zipStream, folderToZip))
                    return false;

            return result;
        }

    }
}