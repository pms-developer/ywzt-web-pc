﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;

namespace HuNanChangJie.Application.Web.Areas.MeioProducts.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-10 13:43
    /// 描 述：美鸥产品辅料
    /// </summary>
    public class MeioProductAccessoriesController : MvcControllerBase
    {
        private MeioProductAccessoriesIBLL meioProductAccessoriesIBLL = new MeioProductAccessoriesBLL();

        private AnnexesFileIBLL annexesFileIBLL = new AnnexesFileBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = meioProductAccessoriesIBLL.GetPageList(paginationobj, queryJson);
            foreach (var item in data)
            {
                var fileList = annexesFileIBLL.GetList(item.Imgs);
                if (fileList.Count() > 0)
                {
                    var imgList = fileList.ToList();
                    item.baseImgId = imgList[0].F_Id;
                    string htmlUrl = "";
                    PrevewFilePdf prevewFilePdf = new PrevewFilePdf();

                    var dataFile = annexesFileIBLL.GetEntity(item.baseImgId);
                    string filename = dataFile.F_FileName;//文件名称
                    string filepath = dataFile.F_FilePath;//文件路径
                    if (System.IO.File.Exists(filepath))
                    {
                        htmlUrl = prevewFilePdf.PreviewFile(filepath, filename);
                    }
                    int port = System.Web.HttpContext.Current.Request.Url.Port;
                    string text = System.Web.HttpContext.Current.Request.Url.Host;
                    if (port > 0)
                    {
                        text = text + ":" + port;
                    }
                    item.baseImg = htmlUrl.Replace(text, "");
                }

            }
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var meio_product_accessoriesData = meioProductAccessoriesIBLL.Getmeio_product_accessoriesEntity( keyValue );
            var jsonData = new {
                meio_product_accessories = meio_product_accessoriesData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            meioProductAccessoriesIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<meio_product_accessoriesEntity>();
            if (type == "add")
            {
                int num = meioProductAccessoriesIBLL.checkNoCount(mainInfo.SKU);
                if (num != 0)
                {
                    //CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    //mainInfo.Code = codeRuleIBLL.GetBillCode("CL_Code");
                    return Fail("SKU重复！");
                }
            }
            meioProductAccessoriesIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
