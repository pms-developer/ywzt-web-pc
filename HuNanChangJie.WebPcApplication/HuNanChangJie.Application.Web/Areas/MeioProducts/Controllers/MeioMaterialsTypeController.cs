﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.MeioProducts;

namespace HuNanChangJie.Application.Web.Areas.MeioProducts.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-18 14:43
    /// 描 述：材料分类
    /// </summary>
    public class MeioMaterialsTypeController : MvcControllerBase
    {
        private MeioMaterialsTypeIBLL baseMaterialsTypeIBLL = new MeioMaterialsTypeBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string queryJson)
        {
            var data = baseMaterialsTypeIBLL.GetPageList(queryJson);
            
            return JsonResult(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult CheckRename(string value, string type)
        {
            var data = baseMaterialsTypeIBLL.CheckRename(value,type);
            return Success(data);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetTreeList(string projectId,bool showProject=false)
        {
            //增加条件是否把项目也当作一级
            var data = baseMaterialsTypeIBLL.GetTreeList(projectId, showProject);
            return JsonResult(data);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Base_MaterialsTypeData = baseMaterialsTypeIBLL.GetBase_MaterialsTypeEntity( keyValue );
            var jsonData = new {
                MeioMaterialsType = Base_MaterialsTypeData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            baseMaterialsTypeIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<MeioMaterialsTypeEntity>();
            baseMaterialsTypeIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
