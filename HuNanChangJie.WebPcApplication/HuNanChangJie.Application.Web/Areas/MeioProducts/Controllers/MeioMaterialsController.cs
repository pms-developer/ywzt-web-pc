﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using HuNanChangJie.Application.Base.SystemModule;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment;
using Castle.DynamicProxy;
using Newtonsoft.Json;
using System;

namespace HuNanChangJie.Application.Web.Areas.MeioProducts.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-19 11:20
    /// 描 述：材料档案
    /// </summary>
    public class MeioMaterialsController : MvcControllerBase
    {
        private MeioMaterialsIBLL baseMaterialsIBLL = new MeioMaterialsBLL();
        private AnnexesFileIBLL annexesFileIBLL = new AnnexesFileBLL();
        public MeioMaterialsController()
        {
            baseMaterialsIBLL = MyInterceptor.InterCreateObj(baseMaterialsIBLL);
        }

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }

        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MaterialsDialog()
        {
            return View();
        }

        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseMaterialsIBLL.GetPageList(paginationobj, queryJson);
            foreach (var item in data)
            {
                var fileList = annexesFileIBLL.GetList(item.Imgs);
                if (fileList.Count() > 0)
                {
                    var imgList = fileList.ToList();
                    item.baseImgId = imgList[0].F_Id;
                    string htmlUrl = "";
                    PrevewFilePdf prevewFilePdf = new PrevewFilePdf();

                    var dataFile = annexesFileIBLL.GetEntity(item.baseImgId);
                    string filename = dataFile.F_FileName;//文件名称
                    string filepath = dataFile.F_FilePath;//文件路径
                    if (System.IO.File.Exists(filepath))
                    {
                        htmlUrl = prevewFilePdf.PreviewFile(filepath, filename);
                    }
                    int port = System.Web.HttpContext.Current.Request.Url.Port;
                    string text = System.Web.HttpContext.Current.Request.Url.Host;
                    if (port > 0)
                    {
                        text = text + ":" + port;
                    }
                    item.baseImg= htmlUrl.Replace(text, "");
                }
                
            }
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }


        /// <summary>
        /// 获取下拉列表数据
        /// <summary>
        /// <param name="projectId">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSelectList(string projectId)
        {
            var data = baseMaterialsIBLL.GetSelectList(projectId);
            return Success(data);
        }



        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsSameName(string name)
        {
            var data = baseMaterialsIBLL.CheckIsSameName(name);
            return Success(data);
        }


        /// <summary>
        /// 获取材料档案清单
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaterials(string pagination, string projectId,string queryJson )
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseMaterialsIBLL.GetMaterials(paginationobj, projectId, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }



        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaterialsList(string pagination, string typeId, string projectId, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseMaterialsIBLL.GetMaterialsList(paginationobj,typeId, projectId, queryJson);

            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };

            //return JsonResult(data);

            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Base_MaterialsData = baseMaterialsIBLL.GetBase_MaterialsEntity( keyValue );
            var meio_product_box_gaugeData = baseMaterialsIBLL.Getmeio_product_box_gaugeList(Base_MaterialsData.ID);
            var meio_product_pick_accessoriesData = baseMaterialsIBLL.Getmeio_product_pick_accessoriesList(Base_MaterialsData.ID);



            foreach (var item in meio_product_pick_accessoriesData)
            {
                var fileList = annexesFileIBLL.GetList(item.Imgs);
                if (fileList.Count() > 0)
                {
                    var imgList = fileList.ToList();
                    item.baseImgId = imgList[0].F_Id;
                    string htmlUrl = "";
                    PrevewFilePdf prevewFilePdf = new PrevewFilePdf();

                    var dataFile = annexesFileIBLL.GetEntity(item.baseImgId);
                    string filename = dataFile.F_FileName;//文件名称
                    string filepath = dataFile.F_FilePath;//文件路径
                    if (System.IO.File.Exists(filepath))
                    {
                        htmlUrl = prevewFilePdf.PreviewFile(filepath, filename);
                    }
                    int port = System.Web.HttpContext.Current.Request.Url.Port;
                    string text = System.Web.HttpContext.Current.Request.Url.Host;
                    if (port > 0)
                    {
                        text = text + ":" + port;
                    }
                    item.baseImg = htmlUrl.Replace(text, "");
                }
            }

            var meio_product_pick_comboData = baseMaterialsIBLL.Getmeio_product_pick_comboList(Base_MaterialsData.ID);

            foreach (var item in meio_product_pick_comboData)
            {
                var fileList = annexesFileIBLL.GetList(item.Imgs);
                if (fileList.Count() > 0)
                {
                    var imgList = fileList.ToList();
                    item.baseImgId = imgList[0].F_Id;
                    string htmlUrl = "";
                    PrevewFilePdf prevewFilePdf = new PrevewFilePdf();

                    var dataFile = annexesFileIBLL.GetEntity(item.baseImgId);
                    string filename = dataFile.F_FileName;//文件名称
                    string filepath = dataFile.F_FilePath;//文件路径
                    if (System.IO.File.Exists(filepath))
                    {
                        htmlUrl = prevewFilePdf.PreviewFile(filepath, filename);
                    }
                    int port = System.Web.HttpContext.Current.Request.Url.Port;
                    string text = System.Web.HttpContext.Current.Request.Url.Host;
                    if (port > 0)
                    {
                        text = text + ":" + port;
                    }
                    item.baseImg = htmlUrl.Replace(text, "");
                }
            }
           List<LogResult> logList =  MyInterceptor.getLogResult(DateTime.Now, "MeioMaterialsIBLL", Base_MaterialsData.ID);
            foreach (var item in logList)
            {
                if (item.detailList != null)
                {
                    item.detailList = item.detailList.Where(x =>(!string.IsNullOrEmpty(x.updateKey) && x.updateKey.IndexOf("Cell_Edit") < 0) ||x.operType!= "Update").ToList();
                }
            }
            var jsonData = new {
                Base_Materials = Base_MaterialsData,
                meio_product_box_gauge = meio_product_box_gaugeData,
                meio_product_pick_accessories = meio_product_pick_accessoriesData,
                meio_product_pick_combo = meio_product_pick_comboData,
                logList = logList,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue,string logList)
        {
            baseMaterialsIBLL.DeleteEntity(keyValue, logList);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity
            , string strmeio_product_box_gaugeList
            , string strmeio_product_pick_accessoriesList
            , string strmeio_product_pick_comboList
            , string deleteList
            , string updateList)
        {
            var mainInfo = strEntity.ToObject<MeioMaterialsEntity>();
            var meio_product_box_gaugeList = strmeio_product_box_gaugeList.ToObject<List<meio_product_box_gaugeEntity>>();
            var meio_product_pick_accessoriesList = strmeio_product_pick_accessoriesList.ToObject<List<meio_product_pick_accessoriesEntity>>();
            var meio_product_pick_comboList = strmeio_product_pick_comboList.ToObject<List<meio_product_pick_comboEntity>>();

            if (type == "add")
            {
                int num = baseMaterialsIBLL.checkNoCount(mainInfo.Code);
                if (num != 0)
                {
                    /*CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    mainInfo.Code = codeRuleIBLL.GetBillCode("CL_Code");*/
                    return Fail("SKU重复！");
                }
            }
            string logList = JsonConvert.SerializeObject(new
            {
                strEntity= strEntity,
                strmeio_product_box_gaugeList = strmeio_product_box_gaugeList,
                strmeio_product_pick_accessoriesList = strmeio_product_pick_accessoriesList,
                strmeio_product_pick_comboList = strmeio_product_pick_comboList,
                deleteList = deleteList,
                updateList = updateList
            });
            baseMaterialsIBLL.SaveEntity(keyValue, mainInfo
                , meio_product_box_gaugeList
                , meio_product_pick_accessoriesList
                , meio_product_pick_comboList
                , deleteList
                , logList
                , type);
            return Success("保存成功！");
        }
        #endregion

    }
}
