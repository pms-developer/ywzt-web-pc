﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-05-19 11:20
 * 描  述：材料档案
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");

var bootstrap = function ($, Changjie) {
    "use strict";
    var typeId = "";
    var typeName = "";

    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            var param = {};
            //$("#revocation").hide()

            //$('#dataTree').mktree({
            //    url: top.$.rootUrl + '/MeioProducts/BASE_CJ_ZhengZhao/GetTree?deep=0',
            //    nodeClick: function (item) {
            //        if (item._deep == 0)
            //            page.search();
            //        else
            //            page.search({ fk_zhengzhaomingcheng: item.value, deep: item._deep });
            //    }
            //});

            // 分类
            $('#dataTree').mktree({
                nodeClick: function (item) {
                    console.log(item);

                    typeId = item.id;
                    typeName = item.text;
                    param.TypeId = typeId;
                    param.TypeName = typeName;
                    page.search(param);
                }
            });

            //分类
            $('#dataTree').mktreeSet('refresh', {
                url: top.$.rootUrl + '/MeioProducts/MeioMaterialsType/GetTreeList?showProject=true&projectId=' + projectId,
            });

            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 100, 950);

            $('#ProjectID').mkDataSourceSelect({ code: 'Meio_Customer', value: 'id', text: 'title' });

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);
            }

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });;
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增产品',
                    url: top.$.rootUrl + '/MeioProducts/MeioMaterials/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/MeioProducts/MeioMaterials/GetPageList',
                headData: [
                    {
                        label: "图片", name: "Code", width: 140, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            //到附件夹中查询第一张图片
                            if (row['baseImg']) {
                                callback("<img src='" + row['baseImg'] + "' style='height:28px;width:28px;' />")
                            } else {
                                callback("无图片")
                            }
                        },
                        edit: {
                            type: 'layer',
                            click: function (row) {
                                if (row['baseImgId']) {
                                    Changjie.layerForm({
                                        id: 'yulanform',
                                        title: '预览',
                                        url: top.$.rootUrl + '/SystemModule/Annexes/PreviewFile?fileId=' + row['baseImgId'],
                                        width: 500,
                                        height: 570,
                                        callBack: function (id) {
                                        }
                                    });
                                }
                                
                            }
                        }
                    },
                    { label: "SKU", name: "Code", width: 140, align: "left"},
                    { label: "品名", name: "Name", width: 100, align: "left" },
                    { label: "型号", name: "Model", width: 100, align: "left" },
                    {
                        label: "单位", name: "UnitName", width: 100, align: "left",
                    },
                    { label: "类别", name: "TypeName", width: 100, align: "left" },
                    { label: "品牌", name: "BrandName", width: 100, align: "left" },
                    { label: "状态", name: "WarrantyType", width: 100, align: "left" },
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left" },
                    { label: "修改时间", name: "ModificationDate", width: 100, align: "left" },
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.TypeId = typeId;
            param.TypeName = typeName;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                var selectedRows = $('#gridtable').jfGridGet('rowdata');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/MeioProducts/MeioMaterials/DeleteForm',
                                { keyValue: keyValue, logList: JSON.stringify(selectedRows) },
                                function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '产品',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/MeioProducts/MeioMaterials/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
