﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-05-19 11:20
 * 描  述：材料档案
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Base_Materials";
var processCommitUrl = top.$.rootUrl + '/MeioProducts/MeioMaterials/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var oldObj = {};
var meio_product_box_gaugeList = [];
var meio_product_pick_accessoriesList = [];
var meio_product_pick_comboList = [];

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'meio_product_box_gauge', "gridId": 'meio_product_box_gauge' });
                type = "add";
                $(".auditLogBtn").hide();
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }


            var ctltype11 = $("#SingleSpecOne").attr("type");;
            var ctltype12 = $("#SingleSpecTwo").attr("type");;
            var ctltype13 = $("#SingleSpecThree").attr("type");;
            if (ctltype11 == 'text' && ctltype12 == 'text') {
                $('#SingleSpecOne').on('input propertychange', function () {
                    var value1 = $(this).val();
                    var value2 = $('#SingleSpecTwo').val();
                    var value3 = $('#SingleSpecThree').val();
                    var r = (value1 * value2 * value3).toFixed(2);
                    $('#Capacity').val(r);
                });
                $('#SingleSpecTwo').on('input propertychange', function () {
                    var value1 = $('#SingleSpecOne').val();
                    var value2 = $(this).val();
                    var value3 = $('#SingleSpecThree').val();
                    var r = (value1 * value2 * value3).toFixed(2);
                    $('#Capacity').val(r);
                });
                $('#SingleSpecThree').on('input propertychange', function () {
                    var value1 = $('#SingleSpecOne').val();
                    var value2 = $('#SingleSpecTwo').val();
                    var value3 = $(this).val();
                    var r = (value1 * value2 * value3).toFixed(2);
                    $('#Capacity').val(r);
                });
            }

            
        },
        bind: function () {
            $(".zuheBtn").hide();


            $('#ProjectID').mkDataSourceSelect({ code: 'Meio_Customer', value: 'id', text: 'title' });

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);
            }

            

            $('#Parameter2').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $("#Parameter3").mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectForm',
                layerUrlW: 800,
                layerUrlH: 520,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#Parameter5').mkUploader();

            $('#Imgs').mkUploader();

            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            /*Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CL_Code' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });*/
            //系统管理-数据数据字典配置
            $("#TypeId").mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/MeioProducts/MeioMaterialsType/GetTreeList?projectId=' + projectId,
            });
            //$('#TypeId').mkDataSourceSelect({ code: 'MaterialsType', value: 'id', text: 'title' });
            //$('#BrandId').mkDataSourceSelect({ code: 'MaterialsBrand', value: 'id', text: 'cnname' });
            $("#BrandId").mkselect({
                allowSearch: true,
                url: top.$.rootUrl + '/MeioProducts/MeioMaterialsBrand/GetSelectList?projectId=' + projectId,
                value: "ID",
                text:"CnName"
            });
            $('#Origin').mkDataItemSelect({ code: 'meio_product_type' });
            $('#UnitId').mkDataSourceSelect({ code: 'MaterialsUnit', value: 'id', text: 'name' });
            $('#WarrantyType').mkDataItemSelect({ code: 'meio_product_state' });
            $('#SingleNetWeightUnit').mkDataItemSelect({ code: 'meio_product_single_net_weight_unit' });
            $('#State').mkRadioCheckbox({
                type: 'radio',
                code: 'MaterialStatus',
            });
            $('#IsProject').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
                default: '1'
            });

            $('#Origin').on("change", function (e) {
                if ($(e.currentTarget).children('.mk-select-placeholder').text() == '组合产品') {
                    $(".zuheBtn").show();
                } else {
                    $(".zuheBtn").hide();
                }
            })
          
            $('#Buyer').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            $('#meio_product_box_gauge').jfGrid({
                headData: [
                    {
                        label: '箱规名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (row.IsDefault && row[colname] != "默认箱规") {
                                    row[colname] = "默认箱规";
                                    Changjie.alert.error('表单信息输入有误,</br>默认箱规名称不可修改');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '单箱数量', name: 'Num', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('.')>-1 || row[colname].indexOf('-')>-1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '外箱规格-长（cm）', name: 'OutBoxSpecOne', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '外箱规格-宽（cm）', name: 'OutBoxSpecTwo', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '外箱规格-高（cm）', name: 'OutBoxSpecThree', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '包装规格-长（cm）', name: 'PackSpecOne', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '包装规格-宽（cm）', name: 'PackSpecTwo', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '包装规格-高（cm）', name: 'PackSpecThree', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '单箱重量', name: 'SingleBoxWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '单箱重量单位', name: 'SingleBoxWeightUnit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'select',
                            init: function (row, $self) {
                                $self.mkDataItemSelect({ code: 'meio_product_single_net_weight_unit' });
                            },
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单箱毛重', name: 'SingleBoxGrossWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_box_gauge').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '单箱毛重单位', name: 'SingleBoxGrossWeightUnit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '箱规'
                        , edit: {
                            type: 'select',
                            init: function (row, $self) {
                                $self.mkDataItemSelect({ code: 'meio_product_single_net_weight_unit' });
                            },
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "meio_product_box_gauge",
                isEdit: true,
                height: 300,
                rowdatas: [
                    {
                        "Name":"默认箱规",
                        "SortCode": 1,
                        "rowState": 1,
                        "ID": Changjie.newGuid(),
                        "EditType": 1,
                        IsDefault:true
                    }],
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });

            $('#meio_product_pick_accessories').jfGrid({
                headData: [
                    {
                        label: "图片", name: "", width: 140, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            //到附件夹中查询第一张图片
                            if (row['baseImg']) {
                                callback("<img src='" + row['baseImg'] + "' style='height:28px;width:28px;' />")
                            } else {
                                callback("无图片")
                            }
                        },
                        edit: {
                            type: 'layer',
                            click: function (row) {
                                if (row['baseImgId']) {
                                    Changjie.layerForm({
                                        id: 'yulanform',
                                        title: '预览',
                                        url: top.$.rootUrl + '/SystemModule/Annexes/PreviewFile?fileId=' + row['baseImgId'],
                                        width: 500,
                                        height: 570,
                                        callBack: function (id) {
                                        }
                                    });
                                }

                            }
                        }
                    },
                    {
                        label: '辅料品名', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '产品辅料'
                        , edit: {
                            type: 'select',
                            init: function (row, $self) {
                                if (projectId) {
                                    $self.mkDataSourceSelect({ code: 'meio_product_accessories', value: 'id', text: 'title', strWhere: "projectid='" + projectId +"'" });
                                } else {
                                    $self.mkDataSourceSelect({ code: 'meio_product_accessories', value: 'id', text: 'title' });
                                }
                                //$self.mkDataSourceSelect({ code: 'meio_product_accessories', value: 'id', text: 'title' });
                            },
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                Changjie.clientdata.getAsync('sourceData', {
                                    code: 'meio_product_accessories',
                                    key: row['Name'],
                                    keyId: 'id',
                                    callback: function (_data) {
                                        row['AccessoriesId'] = _data['id'];
                                        row['Name'] = _data['name'];
                                        row['SKU'] = _data['sku'];
                                        row['UnitCost'] = _data['unitcost'];
                                        row['Text'] = _data['text'];
                                        row['Imgs'] = _data['imgs'];
                                        row['MainRatio'] = 1;
                                        row['AuxiliaryRatio'] = 1;
                                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/Annexes/GetAnnexesImgOne', { infoId: _data['imgs'] }, function (data) {
                                            row['baseImg'] = data['baseImg'];
                                            row['baseImgId'] = data['baseImgId'];
                                            $('#meio_product_pick_accessories').jfGridSet('refreshdata');
                                        });

                                    }
                                });
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '辅料SKU', name: 'SKU', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '产品辅料'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单位成本(￥)', name: 'UnitCost', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '产品辅料'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    }
                    ,
                    {
                        label: '主料比例', name: 'MainRatio', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '产品辅料'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_pick_accessories').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('.') > -1 || row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_pick_accessories').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '辅料比例', name: 'AuxiliaryRatio', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '产品辅料'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_pick_accessories').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('.') > -1 || row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_pick_accessories').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Text', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '产品辅料'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    }
                ],
                mainId: "ID",
                bindTable: "meio_product_pick_accessories",
                isEdit: true,
                height: 300,
                rowdatas: [
                    {
                        "SortCode": 1,
                        "rowState": 1,
                        "ID": Changjie.newGuid(),
                        "EditType": 1,
                    }],
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });

            $('#meio_product_pick_combo').jfGrid({
                headData: [
                    {
                        label: "图片", name: "", width: 140, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            //到附件夹中查询第一张图片
                            if (row['baseImg']) {
                                callback("<img src='" + row['baseImg'] + "' style='height:28px;width:28px;' />")
                            } else {
                                callback("无图片")
                            }
                        },
                        edit: {
                            type: 'layer',
                            click: function (row) {
                                if (row['baseImgId']) {
                                    Changjie.layerForm({
                                        id: 'yulanform',
                                        title: '预览',
                                        url: top.$.rootUrl + '/SystemModule/Annexes/PreviewFile?fileId=' + row['baseImgId'],
                                        width: 500,
                                        height: 570,
                                        callBack: function (id) {
                                        }
                                    });
                                }

                            }
                        }
                    },
                    {
                        label: '品名', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'select',
                            init: function (row, $self) {
                                /*$self.mkselect({
                                    allowSearch: true,
                                    url: top.$.rootUrl + '/MeioProducts/MeioMaterials/GetSelectList?projectId=' + projectId,
                                    value: "ID",
                                    text: "Name"
                                });*/
                                if (projectId) {
                                    $self.mkDataSourceSelect({ code: 'meio_product_list', value: 'id', text: 'title', strWhere: "projectid='" + projectId +"'" });
                                } else {
                                    $self.mkDataSourceSelect({ code: 'meio_product_list', value: 'id', text: 'title' });
                                }
                                
                            },
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                console.log(row['Name']);
                                Changjie.clientdata.getAsync('sourceData', {
                                    code: 'meio_product_list',
                                    key: row['Name'],
                                    keyId: 'id',
                                    callback: function (_data) {
                                        console.log(_data);
                                        row['ComboId'] = _data['id'];
                                        row['Name'] = _data['name'];
                                        row['Code'] = _data['code'];
                                        row['Model'] = _data['model'];
                                        row['Unit'] = _data['unitid'];
                                        row['LeadDay'] = _data['leadday'];
                                        row['PurchaseCost'] = _data['purchasecost'];
                                        row['SingleSpecOne'] = _data['singlespecone'];
                                        row['SingleSpecTwo'] = _data['singlespectwo'];
                                        row['SingleSpecThree'] = _data['singlespecthree'];
                                        row['SingleNetWeight'] = _data['singlenetweight'];
                                        row['Imgs'] = _data['imgs'];
                                        row['SingleBoxGrossWeight'] = 0;//后端sql取值
                                        row['Material'] = _data['Parameter1'];
                                        row['Num'] = 1;
                                        Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/Annexes/GetAnnexesImgOne', { infoId: _data['imgs'] }, function (data) {
                                            row['baseImg'] = data['baseImg'];
                                            row['baseImgId'] = data['baseImgId'];
                                            $('#meio_product_pick_combo').jfGridSet('refreshdata');
                                        });
                                    }
                                });
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: 'SKU', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '型号', name: 'Model', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    }
                    ,
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '采购交期', name: 'LeadDay', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '采购成本', name: 'PurchaseCost', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单品规格-长', name: 'SingleSpecOne', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单品规格-宽', name: 'SingleSpecTwo', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单品规格-高', name: 'SingleSpecThree', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单品净重', name: 'SingleNetWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单品毛重', name: 'SingleBoxGrossWeight', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '产品材质', name: 'Material', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'label',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '数量', name: 'Num', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '包含单品'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                if (isNaN(row[colname])) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_pick_combo').jfGridSet('refreshdata');
                                    return;
                                }
                                if (row[colname].indexOf('.') > -1 || row[colname].indexOf('-') > -1) {
                                    row[colname] = "";
                                    Changjie.alert.error('表单信息输入有误,</br>请输入正确的数字');
                                    $('#meio_product_pick_combo').jfGridSet('refreshdata');
                                    return;
                                }
                            }
                        }
                    }
                ],
                mainId: "ID",
                bindTable: "meio_product_pick_combo",
                isEdit: true,
                height: 300,
                rowdatas: [
                    {
                        "SortCode": 1,
                        "rowState": 1,
                        "ID": Changjie.newGuid(),
                        "EditType": 1,
                    }],
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });

        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/MeioProducts/MeioMaterials/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0 && id !='logList') {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                        oldObj = $('body').mkGetFormData();
                        $('#meio_product_box_gauge').jfGridGet('rowdatas').forEach(function (x) {
                            meio_product_box_gaugeList.push({ ...x });
                        });
                        $('#meio_product_pick_accessories').jfGridGet('rowdatas').forEach(function (x) {
                            meio_product_pick_accessoriesList.push({ ...x });
                        });
                        $('#meio_product_pick_combo').jfGridGet('rowdatas').forEach(function (x) {
                            meio_product_pick_comboList.push({ ...x });
                        });
                        page.initLog(data.logList);
                    }
                });
            }
        },
        initLog: function (data) {
            var typeStr = {
                "Update": "修改",
                "Add": "添加",
                "Delete":"删除"
            }
            var defaultKey = {
                "Model": "型号",
                "Parameter": "产品描述",
                "Parameter1": "材质",
                "UnitId": "单位",
                "Origin": "产品类型",
                "WarrantyType": "状态",
                "BrandId": "品牌",
                "TypeId": "分类",
                "Parameter2": "开发人",
                "Parameter3": "产品责任人",
                "Buyer": "采购员",
                "LeadDay": "采购交期",
                "PurchaseCost": "采购成本",
                "PurchaseText": "采购备注",
                "SingleSpecOne": "单品规格-长",
                "SingleSpecTwo": "单品规格-宽",
                "SingleSpecThree": "单品规格-高",
                "SingleNetWeight": "单品净重",
                "SingleNetWeightUnit": "净重单位",
            }
            var tables = {
                "meio_product_box_gauge": {
                    "name": "箱规",
                    "keys": {
                        "Num": "单箱数量",
                        "OutBoxSpecOne": "外箱规格-长",
                        "OutBoxSpecTwo": "外箱规格-宽",
                        "OutBoxSpecThree": "外箱规格-高",
                    },
                },
                "meio_product_pick_accessories": {
                    "name": "辅料",
                    "keys": {
                        "AccessoriesId": "值",
                        "Name": "名称",
                        "SKU": "SKU",
                        "MainRatio": "主料比例",
                        "AuxiliaryRatio": "辅料比例",
                        "Text": "备注",
                        "Imgs":"图片"
                    },
                },
                "meio_product_pick_combo": {
                    "name": "包含产品",
                    "keys": {}
                }

            }
            var nodelist = [];
            data.forEach(function (x) {
                var point = {
                    title: typeStr[x.operType],
                    people: x.operUser,
                    content: getContent(x),
                    time: x.operTime
                };
                nodelist.push(point);
            })
            $('#tab5').mktimeline(nodelist);
            $('#tab5').mkscroll();
            function getContent(x) {
                if (x.operType == "Update") {
                    var str = "";
                    x.detailList.forEach(y => {
                        if (y.operType != "Update") {
                            str += typeStr[y.operType] + "了[" + tables[y.model].name + "]的数据。<br />";
                        } else {
                            if (y.isDefault) {
                                if (defaultKey[y.updateKey]) {
                                    if (y.updateNewVal.indexOf('-') > -1 && (y.updateNewVal.length == 36 || y.updateNewVal.indexOf(',') > -1 )) {
                                        str += typeStr[y.operType] + "了产品的[" + defaultKey[y.updateKey] + "]。<br />";
                                    } else {
                                        str += typeStr[y.operType] + "了产品的[" + defaultKey[y.updateKey] + "]，从[" + strOrWu(y.updateOldVal) + "]改为[" + strOrWu(y.updateNewVal) + "]。<br />";
                                    }
                                    
                                }
                            } else {
                                if (tables[y.model] && tables[y.model].keys[y.updateKey]) {
                                    if (y.updateNewVal.indexOf('-') > -1 && (y.updateNewVal.length == 36 || y.updateNewVal.indexOf(',') > -1)) {
                                        str += typeStr[y.operType] + "了[" + tables[y.model].name + "]的[" + tables[y.model].keys[y.updateKey] + "]。<br />";
                                    } else {
                                        str += typeStr[y.operType] + "了[" + tables[y.model].name + "]的[" + tables[y.model].keys[y.updateKey] + "]，从[" + strOrWu(y.updateOldVal) + "]改为[" + strOrWu(y.updateNewVal) + "]。<br />";
                                    }
                                    
                                }
                                
                            }
                        }
                        
                    })
                    return str;
                }
                return "";
            }
        }

    };
    var strOrWu = function (str) {
        return str ? str : '空';
    }
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") keyValue = mainId;
        $.mkSaveForm(top.$.rootUrl + '/MeioProducts/MeioMaterials/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {

    if (!$('body').mkValidform()) {
        return false;
    }
    let updateList = [];
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }

    if (type == "edit") {
        deleteList.forEach(function (x) {
            var ids = x.idList.split(',');
            ids.forEach(function (y) {
                if (y) {
                    updateList.push({
                        isDefault: false,
                        model: x.TableName,
                        keyValue: y,
                        operType: "Delete"
                    })
                }
            })

        })
    }
    
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    postData.strmeio_product_box_gaugeList = JSON.stringify($('#meio_product_box_gauge').jfGridGet('rowdatas'));
    postData.strmeio_product_pick_accessoriesList = JSON.stringify($('#meio_product_pick_accessories').jfGridGet('rowdatas'));
    postData.strmeio_product_pick_comboList = JSON.stringify($('#meio_product_pick_combo').jfGridGet('rowdatas'))

    if (type == "edit") {
        var newObj = $('body').mkGetFormData();
        
        for (var item in newObj) {
            if (item.indexOf("jfgrid") < 0) {
                if (newObj[item] != oldObj[item]) {
                    updateList.push({
                        isDefault: true,
                        model: "",
                        keyValue: keyValue,
                        operType: "Update",
                        updateKey: item,
                        updateOldVal: oldObj[item],
                        updateNewVal: newObj[item],
                    })
                }
            }
        }
        var checkmeio_product_box_gauge = $('#meio_product_box_gauge').jfGridGet('rowdatas');
        checkmeio_product_box_gauge.forEach(function (x) {

            if (x.EditType == 1) {
                updateList.push({
                    isDefault: false,
                    model: "meio_product_box_gauge",
                    keyValue: x["ID"],
                    operType: "Add",
                })
            }
            if (x.EditType == 2) {
                var old = meio_product_box_gaugeList.find(j => j.ID == x["ID"]);
                for (var item in x) {
                    if (x[item] != old[item]) {
                        updateList.push({
                            isDefault: false,
                            model: "meio_product_box_gauge",
                            keyValue: x["ID"],
                            operType: "Update",
                            updateKey: item,
                            updateOldVal: old[item],
                            updateNewVal: x[item],
                        })
                    }
                }
            }
        })


        var checkmeio_product_pick_accessories = $('#meio_product_pick_accessories').jfGridGet('rowdatas');
        checkmeio_product_pick_accessories.forEach(function (x) {

            if (x.EditType == 1) {
                updateList.push({
                    isDefault: false,
                    model: "meio_product_pick_accessories",
                    keyValue: x["ID"],
                    operType: "Add",
                })
            }
            if (x.EditType == 2) {
                var old = meio_product_pick_accessoriesList.find(j => j.ID == x["ID"]);
                for (var item in x) {
                    if (x[item] != old[item]) {
                        updateList.push({
                            isDefault: false,
                            model: "meio_product_pick_accessories",
                            keyValue: x["ID"],
                            operType: "Update",
                            updateKey: item,
                            updateOldVal: old[item],
                            updateNewVal: x[item],
                        })
                    }
                }
            }
        })

        if (newObj["Origin"] == '组合产品') {
            var checkmeio_product_pick_combo = $('#meio_product_pick_combo').jfGridGet('rowdatas');
            checkmeio_product_pick_combo.forEach(function (x) {

                if (x.EditType == 1) {
                    updateList.push({
                        isDefault: false,
                        model: "meio_product_pick_combo",
                        keyValue: x["ID"],
                        operType: "Add",
                    })
                }
                if (x.EditType == 2) {
                    var old = meio_product_pick_comboList.find(j => j.ID == x["ID"]);
                    for (var item in x) {
                        if (x[item] != old[item]) {
                            updateList.push({
                                isDefault: false,
                                model: "meio_product_pick_combo",
                                keyValue: x["ID"],
                                operType: "Update",
                                updateKey: item,
                                updateOldVal: old[item],
                                updateNewVal: x[item],
                            })
                        }
                    }
                }
            })
        }
        
    }
    postData.updateList = JSON.stringify(updateList);
    return postData;
}
