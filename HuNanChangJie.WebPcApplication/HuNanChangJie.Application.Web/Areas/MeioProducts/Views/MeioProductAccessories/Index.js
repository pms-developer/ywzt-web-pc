﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2024-04-10 13:43
 * 描  述：美鸥产品辅料
 */
var refreshGirdData;
var projectId = request("projectId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);

            $('#ProjectID').mkDataSourceSelect({ code: 'Meio_Customer', value: 'id', text: 'title' });

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);
            }
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/MeioProducts/MeioProductAccessories/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/MeioProducts/MeioProductAccessories/GetPageList',
                headData: [
                    {
                        label: "图片", name: "Name", width: 140, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            //到附件夹中查询第一张图片
                            if (row['baseImg']) {
                                callback("<img src='" + row['baseImg'] + "' style='height:28px;width:28px;' />")
                            } else {
                                callback("无图片")
                            }
                        },
                        edit: {
                            type: 'layer',
                            click: function (row) {
                                if (row['baseImgId']) {
                                    Changjie.layerForm({
                                        id: 'yulanform',
                                        title: '预览',
                                        url: top.$.rootUrl + '/SystemModule/Annexes/PreviewFile?fileId=' + row['baseImgId'],
                                        width: 500,
                                        height: 570,
                                        callBack: function (id) {
                                        }
                                    });
                                }

                            }
                        }
                    },
                    { label: "辅料品名", name: "Name", width: 100, align: "left"},
                    { label: "辅料SKU", name: "SKU", width: 100, align: "left"},
                    { label: "单位成本(CNY)", name: "UnitCost", width: 100, align: "left"},
                    { label: "采购备注", name: "PurchaseText", width: 100, align: "left"},
                    { label: "重量(g)", name: "Weight", width: 100, align: "left"},
                    { label: "尺寸-长(cm)", name: "SizeOne", width: 100, align: "left"},
                    { label: "尺寸-宽(cm)", name: "SizeTwo", width: 100, align: "left"},
                    { label: "尺寸-高(cm)", name: "SizeThree", width: 100, align: "left"},
                    { label: "备注", name: "Text", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/MeioProducts/MeioProductAccessories/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/MeioProducts/MeioProductAccessories/Form?keyValue=' + keyValue+'&viewState='+viewState,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
