﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-05-18 14:43
 * 描  述：材料分类
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            


            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 300, 400);


            $('#ProjectID').mkDataSourceSelect({ code: 'Meio_Customer', value: 'id', text: 'title' });

            if (projectId) {
                $("#ProjectID").mkselectSet(projectId);
            }

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增分类',
                    url: top.$.rootUrl + '/MeioProducts/MeioMaterialsType/Form?projectId=' + projectId,
                    width: 800,
                    height: 260,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            /*$("#myPrint").on("click", function () {
                window.print();
            })*/
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/MeioProducts/MeioMaterialsType/GetPageList',
                headData: [
                    { label: "分类编码", name: "Code", width: 100, align: "left"},
                    { label: "分类名称", name: "Name", width: 100, align: "left"},
                    /*{ label: "采购员", name: "BuyerId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    {
                        label: "是否启用", name: "IsEnable", width: 100, align: "left",
                        formatter: function (value) {
                            if (value) {
                                return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">已禁用</span>';
                            }
                        }
                    },*/
                    { label: "排序号", name: "SortCode", width: 100, align: "left"},
                    { label: "分类全名", name: "FullName", width: 100, align: "left"},
                    /*{
                        label: "备注", name: "Remark", width: 100, align: "left",
                        formatter: function (value, row) {
                            if (row.IsSystem) {
                                return "系统科目,不可修改或删除";
                            }
                            return value;
                        }
                    },*/
                ],
                mainId: 'ID',
                isTree: true,
                parentId: "ParentId"
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    /*var isSystem = $('#gridtable').jfGridValue('IsSystem');
                    if (isSystem) {
                        top.Changjie.alert.warning("系统预制项不可删除 ");
                        return;
                    }*/
                    
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/MeioProducts/MeioMaterialsType/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    var isSystem = $('#gridtable').jfGridValue('IsSystem');
                    if (isSystem) {
                        top.Changjie.alert.warning("系统预制项不可编辑");
                        return;
                    }
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title +'产品分类',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/MeioProducts/MeioMaterialsType/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 260,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
