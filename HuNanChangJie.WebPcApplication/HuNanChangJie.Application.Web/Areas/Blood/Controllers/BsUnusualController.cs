﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.Blood;
using System.Web.Mvc;
using System.Collections.Generic;
using System;

namespace HuNanChangJie.Application.Web.Areas.Blood.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-10-17 15:53
    /// 描 述：特殊稀有血型献血者
    /// </summary>
    public class BsUnusualController : MvcControllerBase
    {
        private BsUnusualIBLL bsUnusualIBLL = new BsUnusualBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = bsUnusualIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var tBs_UnusualData = bsUnusualIBLL.GettBs_UnusualEntity(Guid.Parse(keyValue) );
            var jsonData = new {
                tBs_Unusual = tBs_UnusualData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            bsUnusualIBLL.DeleteEntity(Guid.Parse(keyValue));
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            if(keyValue == "")
            {
                keyValue = Guid.Empty.ToString();
            }
            tBs_UnusualEntity entity = strEntity.ToObject<tBs_UnusualEntity>();
            bsUnusualIBLL.SaveEntity(Guid.Parse(keyValue), entity);
            return Success("保存成功！");
        }
        #endregion

    }
}
