﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.Blood;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using NPOI.SS.UserModel;
using System;
using System.IO;
using System.Linq;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

namespace HuNanChangJie.Application.Web.Areas.Blood.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-15 10:06
    /// 描 述：献血表彰
    /// </summary>
    public class DedicationPraiseController : MvcControllerBase
    {
        private DedicationPraiseIBLL dedicationPraiseIBLL = new DedicationPraiseBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = dedicationPraiseIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var tBs_DedicationPraiseData = dedicationPraiseIBLL.GettBs_DedicationPraiseList(keyValue);
            var jsonData = new {
                tBs_DedicationPraise = tBs_DedicationPraiseData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            dedicationPraiseIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[AjaxOnly]
        //public ActionResult SaveForm(string keyValue, string baseEntity, string strEntity)
        //{


        //    var dbModel = strEntity.ToObject<List<tBs_DedicationPraiseEntity>>();
        //    var dtModel = baseEntity.ToObject<tBs_DedicationPraiseBaseEntity>();
        //    dedicationPraiseIBLL.SaveEntity(keyValue, dtModel, dbModel);
        //    return Success("保存成功！");
        //}
        #endregion

        #region 导入Excel数据
        /// <summary>
        /// 导入Excel数据
        /// </summary>
        /// <param name="fileName">文件路径</param>
        /// <param name="isFirstRowColumn">是否从第一行第一列开始(暂时不需要)</param>
        /// <param name="sheetName">sheet名称(暂时不需要)</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public string ImportBloodPraise(string fileName, bool isFirstRowColumn=true, string sheetName = "")
        {
            var data = ExcelHelper.ExcelImport(fileName);
            return data.ToJson();

            #region 另一个方法
            //if (string.IsNullOrEmpty(fileName))
            //{
            //    throw new ArgumentNullException(fileName);
            //}
            //var data = new DataTable();
            //IWorkbook workbook = null;
            //FileStream fs = null;
            //try
            //{
            //    fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            //    if (fileName.IndexOf(".xlsx", StringComparison.Ordinal) > 0)
            //    {
            //        workbook = new XSSFWorkbook(fs);
            //    }
            //    else if (fileName.IndexOf(".xls", StringComparison.Ordinal) > 0)
            //    {
            //        workbook = new HSSFWorkbook(fs);
            //    }

            //    ISheet sheet = null;
            //    if (workbook != null)
            //    {
            //        //如果没有找到指定的sheetName对应的sheet，则尝试获取第一个sheet
            //        if (sheetName == "")
            //        {
            //            sheet = workbook.GetSheetAt(0);
            //        }
            //        else
            //        {
            //            sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);
            //        }
            //    }
            //    if (sheet == null)
            //    {
            //        return data.ToJson();
            //    }
            //    var firstRow = sheet.GetRow(0);
            //    //一行最后一个cell的编号 即总的列数
            //    int cellCount = firstRow.LastCellNum;
            //    int startRow;
            //    if (isFirstRowColumn)
            //    {
            //        for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
            //        {
            //            var cell = firstRow.GetCell(i);
            //            var cellValue = cell.StringCellValue;
            //            if (cellValue == null) continue;
            //            var column = new DataColumn(cellValue);
            //            data.Columns.Add(column);
            //        }
            //        startRow = sheet.FirstRowNum + 1;
            //    }
            //    else
            //    {
            //        startRow = sheet.FirstRowNum;
            //    }
            //    //最后一列的标号
            //    var rowCount = sheet.LastRowNum;
            //    for (var i = startRow; i <= rowCount; ++i)
            //    {
            //        var row = sheet.GetRow(i);
            //        //没有数据的行默认是null
            //        if (row == null) continue;
            //        var dataRow = data.NewRow();
            //        for (int j = row.FirstCellNum; j < cellCount; ++j)
            //        {
            //            //同理，没有数据的单元格都默认是null
            //            if (row.GetCell(j) != null)
            //                dataRow[j] = row.GetCell(j).ToString();
            //        }
            //        data.Rows.Add(dataRow);
            //    }

            //    return data.ToJson();
            //}
            //catch (IOException ioex)
            //{
            //    throw new IOException(ioex.Message);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
            //finally
            //{
            //    if (fs != null)
            //    {
            //        fs.Close();
            //    }
            //}
            #endregion
        }
        #endregion


    }
}
