﻿using System;
using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.Blood;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.Organization;
using HuNanChangJie.Application.WorkFlow;

namespace HuNanChangJie.Application.Web.Areas.Blood.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-16 16:47
    /// 描 述：献血表彰主表
    /// </summary>
    public class DedicationPraiseBaseController : MvcControllerBase
    {
        private DedicationPraiseBaseIBLL dedicationPraiseBaseIBLL = new DedicationPraiseBaseBLL();
        private WfEngineIBLL wfEngineIBLL = new WfEngineBLL();
        private WfProcessInstanceIBLL wfProcessInstanceIBLL = new WfProcessInstanceBLL();
        private WfTaskIBLL wfTaskIBLL = new WfTaskBLL();
        private WfSchemeIBLL wfSchemeIBLL = new WfSchemeBLL();
        private CompanyIBLL companyIbll = new CompanyBLL();


        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = dedicationPraiseBaseIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取页面显示列表数据(执行存储过程)
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList_ByProc(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = dedicationPraiseBaseIBLL.GetPageList_ByProc(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var tBs_DedicationPraiseBaseData = dedicationPraiseBaseIBLL.GettBs_DedicationPraiseBaseEntity( keyValue );
            var jsonData = new {
                tBs_DedicationPraiseBase = tBs_DedicationPraiseBaseData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            dedicationPraiseBaseIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <param name="baseEntity">主表</param>
        /// <param name="strEntity">子表</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string baseEntity, string strEntity)
        {
            var dtModel = baseEntity.ToObject<tBs_DedicationPraiseBaseEntity>();
            var dbModel = strEntity.ToObject<List<tBs_DedicationPraiseEntity>>();
            dedicationPraiseBaseIBLL.SaveEntity(keyValue, dtModel, dbModel);
            return Success("保存成功！");
        }
        #endregion

        #region 导入Excel数据
        /// <summary>
        /// 导入Excel数据
        /// </summary>
        /// <param name="fileName">文件路径</param>
        /// <param name="isFirstRowColumn">是否从第一行第一列开始(暂时不需要)</param>
        /// <param name="sheetName">sheet名称(暂时不需要)</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult ImportBloodPraise(string fileName, bool isFirstRowColumn = true, string sheetName = "")
        {
            try
            {
                var data = ExcelHelper.ExcelImport(fileName);
                ExcelHelper.DeleteDir(fileName);
                return Success("导入数据成功", data.ToJson());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Fail("导入数据失败");
            }            

            #region 另一个方法
            //if (string.IsNullOrEmpty(fileName))
            //{
            //    throw new ArgumentNullException(fileName);
            //}
            //var data = new DataTable();
            //IWorkbook workbook = null;
            //FileStream fs = null;
            //try
            //{
            //    fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            //    if (fileName.IndexOf(".xlsx", StringComparison.Ordinal) > 0)
            //    {
            //        workbook = new XSSFWorkbook(fs);
            //    }
            //    else if (fileName.IndexOf(".xls", StringComparison.Ordinal) > 0)
            //    {
            //        workbook = new HSSFWorkbook(fs);
            //    }

            //    ISheet sheet = null;
            //    if (workbook != null)
            //    {
            //        //如果没有找到指定的sheetName对应的sheet，则尝试获取第一个sheet
            //        if (sheetName == "")
            //        {
            //            sheet = workbook.GetSheetAt(0);
            //        }
            //        else
            //        {
            //            sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);
            //        }
            //    }
            //    if (sheet == null)
            //    {
            //        return data.ToJson();
            //    }
            //    var firstRow = sheet.GetRow(0);
            //    //一行最后一个cell的编号 即总的列数
            //    int cellCount = firstRow.LastCellNum;
            //    int startRow;
            //    if (isFirstRowColumn)
            //    {
            //        for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
            //        {
            //            var cell = firstRow.GetCell(i);
            //            var cellValue = cell.StringCellValue;
            //            if (cellValue == null) continue;
            //            var column = new DataColumn(cellValue);
            //            data.Columns.Add(column);
            //        }
            //        startRow = sheet.FirstRowNum + 1;
            //    }
            //    else
            //    {
            //        startRow = sheet.FirstRowNum;
            //    }
            //    //最后一列的标号
            //    var rowCount = sheet.LastRowNum;
            //    for (var i = startRow; i <= rowCount; ++i)
            //    {
            //        var row = sheet.GetRow(i);
            //        //没有数据的行默认是null
            //        if (row == null) continue;
            //        var dataRow = data.NewRow();
            //        for (int j = row.FirstCellNum; j < cellCount; ++j)
            //        {
            //            //同理，没有数据的单元格都默认是null
            //            if (row.GetCell(j) != null)
            //                dataRow[j] = row.GetCell(j).ToString();
            //        }
            //        data.Rows.Add(dataRow);
            //    }

            //    return data.ToJson();
            //}
            //catch (IOException ioex)
            //{
            //    throw new IOException(ioex.Message);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
            //finally
            //{
            //    if (fs != null)
            //    {
            //        fs.Close();
            //    }
            //}
            #endregion
        }
        #endregion

        #region 无偿奉献奖申报--工作流
        /// <summary>
        /// 无偿奉献奖申报   工作流
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="baseEntity"></param>
        /// <param name="strEntity"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult CreateProcess(string keyValue, string baseEntity, string strEntity)
        {
            var dtModel = baseEntity.ToObject<tBs_DedicationPraiseBaseEntity>();
            var dbModel = strEntity.ToObject<List<tBs_DedicationPraiseEntity>>();

            UserInfo userInfo = LoginUserInfo.Get();
            dtModel.F_Encode = companyIbll.GetEntity(userInfo.companyId)?.F_EnCode;
            dedicationPraiseBaseIBLL.SaveEntity(keyValue, dtModel, dbModel);

            try
            {
                Dictionary<string, object> formdataDictionary = new Dictionary<string, object>();
                WfParameter wfParameter = null;

                wfParameter = new WfParameter
                {
                    companyId = userInfo.companyId,
                    departmentId = userInfo.departmentId,
                    userId = userInfo.userId,
                    userName = userInfo.realName,
                    isNew = true,
                    processId = dtModel.F_ID,
                    //schemeCode = "FXJ"
                };

                WfResult<WfContent> res = wfEngineIBLL.Bootstraper(wfParameter);
                foreach (var field in res.data.currentNode.authorizeFields)
                {
                    switch (field.fieldId)
                    {
                        case "41acce60-6615-fc9a-173a-89427b842854":
                            formdataDictionary.Add("41acce60-6615-fc9a-173a-89427b842854", dtModel.F_Company);
                            break;
                        case "488c6b13-bb9d-9ddf-16aa-2d210243f433":
                            formdataDictionary.Add("488c6b13-bb9d-9ddf-16aa-2d210243f433", dtModel.F_Name);
                            break;
                        case "cd95e454-a444-6fed-15d9-4801903ded21":
                            formdataDictionary.Add("cd95e454-a444-6fed-15d9-4801903ded21", dtModel.F_Date);
                            break;
                        case "d2c4b8fe-a849-7ff3-fba0-deed25ef076f":
                            formdataDictionary.Add("d2c4b8fe-a849-7ff3-fba0-deed25ef076f", dbModel);
                            break;
                        case "0de495ed-2a0f-00ca-988a-6a556370268f":
                            formdataDictionary.Add("0de495ed-2a0f-00ca-988a-6a556370268f", dtModel.F_ID);
                            break;
                        case "3259bd97-b6b0-cc24-4032-afb862b60294":
                            formdataDictionary.Add("3259bd97-b6b0-cc24-4032-afb862b60294", dtModel.F_Encode);
                            break;
                    }

                }

                string formJson = formdataDictionary.ToJson();
                wfParameter = new WfParameter();
                wfParameter.companyId = userInfo.companyId;
                wfParameter.departmentId = userInfo.departmentId;
                wfParameter.userId = userInfo.userId;
                wfParameter.userName = userInfo.realName;

                wfParameter.isNew = true;
                wfParameter.processId = dtModel.F_ID;
                //wfParameter.schemeCode = "FXJ";
                wfParameter.processName = dtModel.F_Company + "【无偿奉献奖申报】";
                wfParameter.processLevel = 0;
                wfParameter.description = "";
                wfParameter.auditers = "";
                wfParameter.formData = formJson;
                var resrResult = wfEngineIBLL.Create(wfParameter);
                if (resrResult.status == 1)
                {
                    return Success("无偿奉献奖申报申请成功！");
                }
                else
                {
                    return Fail("申请失败！");
                }


            }
            finally
            {

            }
        }
        #endregion

    }
}
