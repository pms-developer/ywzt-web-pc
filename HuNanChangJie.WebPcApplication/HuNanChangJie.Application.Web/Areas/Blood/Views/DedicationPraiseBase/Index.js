﻿/*
 * 创建人：超级管理员
 * 日  期：2018-11-16 16:47
 * 描  述：献血表彰主表
 */
var refreshGirdData;
var bootstrap = function ($, Minke) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Minke.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/Blood/DedicationPraiseBase/Form',
                    width: 800,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_ID');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/Blood/DedicationPraiseBase/Form?keyValue=' + keyValue,
                        width: 800,
                        height: 800,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_ID');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Minke.deleteForm(top.$.rootUrl + '/Blood/DedicationPraiseBase/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/Blood/DedicationPraiseBase/GetPageList',
                headData: [
                    { label: "申报单位", name: "F_Company", width: 100, align: "left" },
                    { label: "申报人", name: "F_Name", width: 100, align: "left" },
                    { label: "省份", name: "F_Province", width: 100, align: "left" },
                    { label: "市", name: "F_City", width: 100, align: "left" },
                    { label: "申报年度", name: "F_Date", width: 100, align: "left" },
                    { label: "表彰状态", name: "F_Status", width: 100, align: "left" },
                ],
                mainId:'F_ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
