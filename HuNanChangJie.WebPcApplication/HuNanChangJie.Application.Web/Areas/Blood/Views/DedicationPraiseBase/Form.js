﻿/* 
 * 创建人：超级管理员
 * 日  期：2018-11-16 16:47
 * 描  述：献血表彰主表
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Minke) {
    "use strict";
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            page.tableData();
        },
        clearFile: function () {
            var file = document.getElementById('fileup');
            file.value = ''; 
        },
        bind: function () {

            $("#F_AuthorStatus").mkselect({

                data: [{ value: 1, text: '审核通过' }, { value: 0, text: '审核不通过' }],
                value: 'value',
                text: 'text',
                title: 'text'
            });
            $('#F_Company')[0].mkvalue = Minke.clientdata.get(['userinfo']).companyId;
            Minke.clientdata.getAsync('company', {
                key: Minke.clientdata.get(['userinfo']).companyId,
                callback: function (_data) {
                    $('#F_Company').val(_data.name);
                }
            }); 

            $('#tBs_DedicationPraiseBase').jfGrid({
                headData: [
                    { label: "姓名", name: "F_Name", width: 100, align: "left" },
                    { label: "性别", name: "F_Sex", width: 100, align: "left" },
                    { label: "证件号", name: "F_IDCard", width: 100, align: "left" },
                    { label: "工作单位", name: "F_WorkCompany", width: 100, align: "left" },
                    { label: "申报奖项", name: "F_AppPraise", width: 100, align: "left" },
                    { label: "献血总量", name: "F_BloodVolume", width: 100, align: "left" },
                    { label: "献血总次数", name: "F_BloodCount", width: 100, align: "left" },
                    { label: "是否表彰", name: "F_AuthorStatus", width: 100, align: "left" },
                    { label: "描述", name: "F_Description", width: 100, align: "left" }
                ],
                mainId: 'F_AutoID',
                isEidt: true,
                footerrow: true,
                isStatistics: true,
                height: 500,
                isMultiselect: true
            });
            //选择文件并上传(Excel)
            $("#fileup").on('change',
                function () {
                    $("#add").addClass("disabled").find("i").removeClass("fa-file-excel-o").addClass("fa-spinner fa-spin");
                    $("#add").find("span").text("正在导入...");
                    var formData = new FormData();
                    formData.append("basefile", document.getElementById("fileup").files[0]);
                    $.ajax({
                        url: top.$.rootUrl + "/SystemModule/Annexes/UploadFileMvc",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        async: true,
                        cache: false,
                        success: function (data) {
                            console.log(JSON.parse(data).data);
                            $("#file_id").val(JSON.parse(data).data);

                            //上传完Excel之后，导入数据
                            $.ajax({
                                url: top.$.rootUrl + "/Blood/DedicationPraiseBase/ImportBloodPraise",
                                type: "GET",
                                data: { isFirstRowColumn: true, fileName: $("#file_id").val() },
                                dataType: "json",
                                async: true,
                                cache: false,
                                success: function (data) {
                                    var model = JSON.parse(data.data);
                                    for (var i = 0; i < model.length; i++) {
                                        var jsonData =
                                        {
                                            F_Name: model[i]["姓名"],
                                            F_Sex: model[i]["性别"],
                                            F_IDCard: model[i]["证件号"],
                                            F_WorkCompany: model[i]["工作单位"],
                                            F_AppPraise: model[i]["申报奖项"],
                                            F_BloodVolume: model[i]["献血总量"],
                                            F_BloodCount: model[i]["献血总次数"]
                                        };
                                        $('#tBs_DedicationPraiseBase').jfGridSet('addRow', jsonData);
                                    }
                                    page.clearFile();//清空选定文件
                                    $("#file_id").val("");
                                    $("#add").removeClass("disabled").find("i").removeClass("fa-spinner fa-spin").addClass("fa-file-excel-o");
                                    $("#add").find("span").text("导入名单");
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {

                                },
                                beforeSend: function () {
                                },
                                complete: function () {
                                    
                                }
                            });
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {

                        },
                        beforeSend: function () {
                        },
                        complete: function () {

                        }
                    });

                });
            //上传完Excel之后，导入数据
            //$("#import").on('click', function () {
            //    $.ajax({
            //        url: top.$.rootUrl + "/Blood/DedicationPraiseBase/ImportBloodPraise",
            //        type: "GET",
            //        data: { isFirstRowColumn: true, fileName: $("#file_id").val() },
            //        dataType: "json",
            //        async: true,
            //        cache: false,
            //        success: function (data) {
            //            var model = JSON.parse(data.data);
            //            for (var i = 0; i < 8; i++) {
            //                var jsonData =
            //                {
            //                    F_Name: model[i]["姓名"],
            //                    F_Sex: model[i]["性别"],
            //                    F_IDCard: model[i]["证件号"],
            //                    F_WorkCompany: model[i]["工作单位"],
            //                    F_AppPraise: model[i]["申报奖项"],
            //                    F_BloodVolume: model[i]["献血总量"],
            //                    F_BloodCount: model[i]["献血总次数"]
            //                };
            //                $('#tBs_DedicationPraiseBase').jfGridSet('addRow', jsonData);
            //            }
            //            page.clearFile();//清空选定文件
            //            $("#file_id").val("");
            //        },
            //        error: function (XMLHttpRequest, textStatus, errorThrown) {

            //        },
            //        beforeSend: function () {
            //        },
            //        complete: function () {
            //        }
            //    });

            //});

        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/Blood/DedicationPraiseBase/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id]);
                        }
                        else {
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
        tableData: function () {
            var param = { AreaName: '110000' };
            $.ajax({
                type: "Get",
                url: top.$.rootUrl + '/Blood/DedicationPraiseBase/GetPageList_ByProc',
                data: { queryJson: JSON.stringify(param)},
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    var model = JSON.parse(data.data);
                    for (var i = 0; i < model.length; i++) {
                        var jsonData =
                        {
                            F_Name: model[i]["姓名"],
                            F_Sex: model[i]["性别"],
                            F_IDCard: model[i]["证件号"],
                            F_WorkCompany: model[i]["工作单位"],
                            F_AppPraise: model[i]["申报奖项"],
                            F_BloodVolume: model[i]["献血总量"],
                            F_BloodCount: model[i]["献血总次数"]
                        };
                        $('#tBs_DedicationPraiseBase').jfGridSet('addRow', jsonData);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                },
                beforeSend: function () {
                },
                complete: function () {

                }
            });
        }
    };
    // 保存数据
    acceptClick = function (callBack) {

        if (!$('body').mkValidform()) {
            return false;
        }
        var strEntity = JSON.stringify($("#tBs_DedicationPraiseBase").jfGridGet('rowdatas'));
       
        if (strEntity == "[]") {
            top.Minke.alert.warning("请导入名单！");
            return false;
        }
        var postData = {
            baseEntity: JSON.stringify($(".mk-form-wrap").mkGetFormData(keyValue)),
            strEntity: JSON.stringify($("#tBs_DedicationPraiseBase").jfGridGet('rowdatas'))
        };
        $.mkSaveForm(top.$.rootUrl + '/Blood/DedicationPraiseBase/CreateProcess?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
