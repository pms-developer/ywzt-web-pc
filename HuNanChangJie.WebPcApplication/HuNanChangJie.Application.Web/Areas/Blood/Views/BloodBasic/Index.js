﻿/*
 * 创建人：超级管理员
 * 日  期：2018-10-17 14:27
 * 描  述：血站基本信息
 */
var refreshGirdData;
var bootstrap = function ($, Minke) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Minke.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/Blood/BloodBasic/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('Item_Id');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/Blood/BloodBasic/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('Item_Id');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Minke.deleteForm(top.$.rootUrl + '/Blood/BloodBasic/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/Blood/BloodBasic/GetPageList',
                headData: [
                    { label: "数据时间", name: "Da_Time", width: 100, align: "left"},
                    { label: "血站名称", name: "Mc", width: 100, align: "left"},
                    { label: "统一社会信用代码", name: "Shxydm", width: 100, align: "left"},
                    { label: "机构代码", name: "Jgdm", width: 100, align: "left"},
                    { label: "血站类型", name: "Xzlx", width: 100, align: "left"},
                    { label: "经费来源", name: "Jfly", width: 100, align: "left"},
                    { label: "主管部门", name: "Zgbm", width: 100, align: "left"},
                    { label: "法定代表人", name: "Fr", width: 100, align: "left"},
                    { label: "建立时间", name: "Jlsj", width: 100, align: "left"},
                    { label: "审批机关", name: "Spjg", width: 100, align: "left"},
                    { label: "审批文号", name: "Spwh", width: 100, align: "left"},
                    { label: "有效期限", name: "Yxqx", width: 100, align: "left"},
                    { label: "行政区划代码", name: "Xzqhdm", width: 100, align: "left"},
                    { label: "通讯地址", name: "Address", width: 100, align: "left"},
                    { label: "邮政编码", name: "Postcode", width: 100, align: "left"},
                    { label: "联系电话", name: "Phone", width: 100, align: "left"},
                ],
                mainId:'Item_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
