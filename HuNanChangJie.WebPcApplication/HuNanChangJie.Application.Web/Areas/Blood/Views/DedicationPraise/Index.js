﻿/* 
 * 创建人：超级管理员
 * 日  期：2018-11-15 10:06
 * 描  述：献血表彰
 */
var refreshGirdData;
var bootstrap = function ($, Minke) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Minke.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/Blood/DedicationPraise/Form',
                    width: 800,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_AutoID');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/Blood/DedicationPraise/Form?keyValue=' + keyValue,
                        width: 800,
                        height: 800,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_AutoID');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Minke.deleteForm(top.$.rootUrl + '/Blood/DedicationPraise/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/Blood/DedicationPraise/GetPageList',
                headData: [
                    { label: "姓名", name: "F_Name", width: 100, align: "left" },
                    { label: "性别", name: "F_Sex", width: 100, align: "left" },
                    { label: "证件号", name: "F_IDCard", width: 100, align: "left" },
                    { label: "工作单位", name: "F_WorkCompany", width: 100, align: "left" },
                    { label: "申报奖项", name: "F_AppPraise", width: 100, align: "left" },
                    { label: "献血总量", name: "F_BloodVolume", width: 100, align: "left" },
                    { label: "献血总次数", name: "F_BloodCount", width: 100, align: "left" },
                    { label: "是否表彰", name: "F_AuthorStatus", width: 100, align: "left" },
                    { label: "描述", name: "F_Description", width: 100, align: "left" }
                ],
                mainId:'F_AutoID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
