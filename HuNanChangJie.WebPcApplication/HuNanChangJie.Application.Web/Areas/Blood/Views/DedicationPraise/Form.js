﻿/*
 * 创建人：超级管理员
 * 日  期：2018-11-15 10:06
 * 描  述：献血表彰
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Minke) {
    "use strict";
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#tBs_DedicationPraise').jfGrid({
                headData: [
                    { label: "姓名", name: "F_Name", width: 100, align: "left" },
                    { label: "性别", name: "F_Sex", width: 100, align: "left" },
                    { label: "证件号", name: "F_IDCard", width: 100, align: "left" },
                    { label: "工作单位", name: "F_WorkCompany", width: 100, align: "left" },
                    { label: "申报奖项", name: "F_AppPraise", width: 100, align: "left" },
                    { label: "献血总量", name: "F_BloodVolume", width: 100, align: "left" },
                    { label: "献血总次数", name: "F_BloodCount", width: 100, align: "left" },
                    { label: "是否表彰", name: "F_AuthorStatus", width: 100, align: "left" },
                    { label: "描述", name: "F_Description", width: 100, align: "left" }

                ],
                mainId: 'F_AutoID',
                isEidt: true,
                footerrow: true,
                isStatistics: true,
                height: 500,
                isMultiselect: true,
            });
            //选择文件并上传(Excel)
            $("#fileup").on('change',
                function() {
                    var formData = new FormData();
                    formData.append("basefile", document.getElementById("fileup").files[0]);
                    $.ajax({
                        url: top.$.rootUrl + "/SystemModule/Annexes/UploadFileMvc",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        async: true,
                        cache: false,
                        success: function(data) {
                            console.log(JSON.parse(data).data);
                            $("#file_id").val(JSON.parse(data).data);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {

                        },
                        beforeSend: function() {
                        },
                        complete: function() {

                        }
                    });

                });
            //上传完Excel之后，导入数据
            $("#import").on('click', function () {
                $.ajax({
                    url: top.$.rootUrl + "/Blood/DedicationPraise/ImportBloodPraise",
                    type: "GET",
                    data: { isFirstRowColumn: true, fileName: $("#file_id").val() },
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data) {
                        for (var i = 0; i < 8; i++) {
                            var jsonData =
                            {
                                F_Name: data[i]["姓名"],
                                F_Sex: data[i]["性别"],
                                F_IDCard: data[i]["证件号"],
                                F_WorkCompany: data[i]["工作单位"],
                                F_AppPraise: data[i]["申报奖项"],
                                F_BloodVolume: data[i]["献血总量"],
                                F_BloodCount: data[i]["献血总次数"],
                                F_AuthorStatus: data[i]["是否表彰"],
                                F_Description: data[i]["描述"]
                            };
                            $('#tBs_DedicationPraise').jfGridSet('addRow', jsonData);
                        }

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                    },
                    beforeSend: function () {
                    },
                    complete: function () {
                    }
                });

            });
           
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/Blood/DedicationPraise/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id]);
                        }
                        else {
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var postData = {
            baseEntity: JSON.stringify($(".mk-form-wrap").mkGetFormData(keyValue)),
            strEntity: JSON.stringify($("#tBs_DedicationPraise").jfGridGet('rowdatas'))
        };
        $.mkSaveForm(top.$.rootUrl + '/Blood/DedicationPraise/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
