﻿/* 
 * 创建人：超级管理员
 * 日  期：2018-10-17 15:53
 * 描  述：特殊稀有血型献血者
 */
var refreshGirdData;
var bootstrap = function ($, Minke) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Minke.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/Blood/BsUnusual/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('KID');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/Blood/BsUnusual/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('KID');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Minke.deleteForm(top.$.rootUrl + '/Blood/BsUnusual/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/Blood/BsUnusual/GetPageList',
                headData: [
                    { label: "血站ID", name: "BS_Id", width: 100, align: "left"},
                    { label: "献血者识别码", name: "bm", width: 100, align: "left"},
                    { label: "献血者姓名", name: "xm", width: 100, align: "left"},
                    { label: "身份证件类别", name: "zjlx", width: 100, align: "left"},
                    { label: "献血者身份证件号码", name: "sfz", width: 100, align: "left"},
                    { label: "稀有血型系统", name: "xx_xt", width: 100, align: "left"},
                    { label: "稀有血型表型", name: "xx_bx", width: 100, align: "left" },
                    { label: "创建时间", name: "Create_Time", width: 100, align: "left" },
                ],
                mainId:'KID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
