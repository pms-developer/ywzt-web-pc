﻿/*
 * 创建人：超级管理员
 * 日  期：2018-11-20 16:37
 * 描  述：接口调用者
 */
var refreshGirdData;
var bootstrap = function ($, Minke) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Minke.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/Blood/Partner/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('Partner_Id');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/Blood/Partner/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('Partner_Id');
                if (Minke.checkrow(keyValue)) {
                    Minke.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Minke.deleteForm(top.$.rootUrl + '/Blood/Partner/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/Blood/Partner/GetPageList',
                headData: [
                    { label: "编码", name: "Partner_Id", width: 250, align: "left" },
                    { label: "接口调用者", name: "Partner_Name", width: 100, align: "left"},
                    { label: "接口调用key", name: "Partner_Private", width: 100, align: "left"},
                    {
                        label: "接口调用状态", name: "Status", width: 100, align: "left", sortable: false,
                        formatter: function (cellvalue) {
                            if (cellvalue == 0) {
                                return "未启用，不可调用接口";
                            } else if (cellvalue == 1) {
                                return "启用，可调用接口";
                            } 
                        }
                    },
                ],
                mainId:'Partner_Id',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
