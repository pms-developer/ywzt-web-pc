﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.Blood
{
    public class BloodAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Blood";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Blood_default",
                "Blood/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}