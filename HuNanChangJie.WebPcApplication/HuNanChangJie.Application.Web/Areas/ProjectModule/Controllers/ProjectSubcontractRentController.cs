﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    public class ProjectSubcontractRentController: MvcControllerBase
    {
        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public System.Web.Mvc.ActionResult RentalIndex()
        {
            return View();
        }

        [HttpGet]
        public System.Web.Mvc.ActionResult Form()
        {
            return View();
        }

        [HttpGet]
        public System.Web.Mvc.ActionResult Leasehold()
        {
            return View();
        }
    }
}