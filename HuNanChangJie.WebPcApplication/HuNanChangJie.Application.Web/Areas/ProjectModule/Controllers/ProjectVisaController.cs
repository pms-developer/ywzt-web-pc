﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 10:56
    /// 描 述：工程签证
    /// </summary>
    public class ProjectVisaController : MvcControllerBase
    {
        private ProjectVisaIBLL projectVisaIBLL = new ProjectVisaBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectVisaIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var contractBll = new ProjectContractBLL();
            var Project_VisaData = projectVisaIBLL.GetProject_VisaEntity(keyValue);
            var Project_VisaMaterialsData = projectVisaIBLL.GetProject_VisaMaterialsList(Project_VisaData.ID);
            //var materialsIdList = Project_VisaMaterialsData.Select(i => i.ProjectContractMaterialsId);
            //var materialsList = contractBll.GetProject_ContractMaterialsLists(materialsIdList);

            //var materials = from a in Project_VisaMaterialsData
            //                join b in materialsList on a.ProjectContractMaterialsId equals b.ID 
            //                where a.MaterialsType != true
            //                orderby a.SortCode ascending
            //                select new Project_VisaMaterialsEntity()
            //                {
            //                    ID = a.ID,
            //                    ProjectId = a.ProjectId,
            //                    ProjectContractMaterialsId = a.ProjectContractMaterialsId,
            //                    ProjectVisaId = a.ProjectVisaId,
            //                    ListCode = a.ListCode,
            //                    Code = a.Code,
            //                    Name = a.Name,
            //                    Brand = a.Brand,
            //                    ModelNumber = a.ModelNumber,
            //                    Unit = a.Unit,
            //                    Quantity = a.Quantity,
            //                    Price = a.Price,
            //                    TotalPrice = a.TotalPrice,
            //                    Remark = a.Remark,
            //                    CreationDate = a.CreationDate,
            //                    SortCode = a.SortCode,

            //                    InitPrice = b.Price,
            //                    InitQuantity = b.Quantity,
            //                    InitTotal = b.TotalPrice
            //                };

            var Project_VisaQuantitiesData = projectVisaIBLL.GetProject_VisaQuantitiesListsql(Project_VisaData.ID);



            var jsonData = new
            {
                Project_Visa = Project_VisaData,
                Project_VisaMaterials = Project_VisaMaterialsData,
                Project_VisaQuantities = Project_VisaQuantitiesData
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectVisaIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strproject_VisaMaterialsList, string strproject_VisaQuantitiesList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_VisaEntity>();
            var project_VisaMaterialsList = strproject_VisaMaterialsList.ToObject<List<Project_VisaMaterialsEntity>>();
            var project_VisaQuantitiesList = strproject_VisaQuantitiesList.ToObject<List<Project_VisaQuantitiesEntity>>();
            projectVisaIBLL.SaveEntity(keyValue, mainInfo, project_VisaMaterialsList, project_VisaQuantitiesList, deleteList, type);
            return Success("保存成功！");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            projectVisaIBLL.Audit(keyValue);
            return Success("操作成功");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            projectVisaIBLL.UnAudit(keyValue);
            return Success("操作成功");
        }
        #endregion

    }
}
