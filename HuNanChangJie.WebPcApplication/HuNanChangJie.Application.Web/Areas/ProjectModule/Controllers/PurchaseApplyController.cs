﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-10 15:13
    /// 描 述：采购申请
    /// </summary>
    public class PurchaseApplyController : MvcControllerBase
    {
        private PurchaseApplyIBLL purchaseApplyIBLL = new PurchaseApplyBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }


        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = purchaseApplyIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_PurchaseApplyData = purchaseApplyIBLL.GetProject_PurchaseApplyEntity(keyValue);
            var budgetBll = new ProjectConstructionBudgetBLL();
            var Project_PurchaseApplyDetailsData = purchaseApplyIBLL.GetProject_PurchaseApplyDetailsList(Project_PurchaseApplyData.ID);
            var budgetIds = Project_PurchaseApplyDetailsData.Select(i => i.ProjectConstructionBudgetMaterialsId);
            var materials = budgetBll.GetProject_ConstructionBudgetMaterialsLists(budgetIds);

            var materialsApply = from a in Project_PurchaseApplyDetailsData
                                 join b in materials
                                 on a.ProjectConstructionBudgetMaterialsId equals b.ID into a_b
                                 from b in a_b.DefaultIfEmpty()
                                 orderby a.SortCode ascending
                                 select new
                                 {
                                     ProjectId = a.ProjectId,
                                     ListCode = a.ListCode,
                                     Code = a.Code,
                                     Name = a.Name,
                                     Brand = a.Brand,
                                     ModelNumber = a.ModelNumber,
                                     Unit = a.Unit,
                                     //清单数量、价格
                                     Quantity = b != null ? (b.Quantity ?? 0) : 0,
                                     Price = b != null ? (b.BudgetPrice ?? 0) : 0,
                                     //本次申请数量
                                     ApplyQuantity = (a.ApplyQuantity ?? 0),
                                     ListPrice = (a.ListPrice ?? 0),
                                     //已申请数量
                                     AppliedQuantity = b != null ? b.ApplyQuantity : 0,
                                     //可申请数量
                                     AllowQuantity = b != null ? b.AllowQuantity : 0,
                                     ID = a.ID,
                                     ProjectPurchaseApplyId = a.ProjectPurchaseApplyId,
                                     ProjectConstructionBudgetMaterialsId = a.ProjectConstructionBudgetMaterialsId,
                                     ContractQuntity = a.ContractQuntity,
                                     NeedDate = a.NeedDate,
                                     SortCode = a.SortCode,
                                     CreationDate = a.CreationDate,
                                     ApplyTotal = a.ApplyTotal,
                                 };

            var jsonData = new
            {
                Project_PurchaseApply = Project_PurchaseApplyData,
                Project_PurchaseApplyDetails = materialsApply,
            };
            return Success(jsonData);
        }



        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetApplyDetailListByApplyId(string keyValue)
        {
            var Project_PurchaseApplyData = purchaseApplyIBLL.GetProject_PurchaseApplyEntity(keyValue);
            var budgetBll = new ProjectConstructionBudgetBLL();
            var Project_PurchaseApplyDetailsData = purchaseApplyIBLL.GetProject_PurchaseApplyDetailsList(Project_PurchaseApplyData.ID);
            var budgetIds = Project_PurchaseApplyDetailsData.Select(i => i.ProjectConstructionBudgetMaterialsId);
            var materials = budgetBll.GetProject_ConstructionBudgetMaterialsLists(budgetIds);

            var materialsApply = from a in Project_PurchaseApplyDetailsData
                                 join b in materials
                                 on a.ProjectConstructionBudgetMaterialsId equals b.ID into a_b
                                 from b in a_b.DefaultIfEmpty()
                                 orderby a.SortCode ascending
                                 select new
                                 {
                                     ProjectId = a.ProjectId,
                                     ListCode = a.ListCode,
                                     Code = a.Code,
                                     Name = a.Name,
                                     Brand = a.Brand,
                                     ModelNumber = a.ModelNumber,
                                     Unit = a.Unit,
                                  
                                     BudgetPrice = b != null ? (b.BudgetPrice ?? 0) : 0,//预算单价（含税）

                                     CurrentAllowQuantity = a != null ? a.ApplyQuantity : 0,//待采购数量

                                     ////清单数量、价格
                                     //Quantity = b != null ? (b.Quantity ?? 0) : 0,

                                     AfterTaxPrices = b != null ? (b.AfterTaxPrices ?? 0) : 0,//单价(优惠后)

                                     ////已申请数量
                                     //AppliedQuantity = b != null ? b.ApplyQuantity : 0,
                                     ////可申请数量
                                     //AllowQuantity = b != null ? b.AllowQuantity : 0,


                                     ID = a.ID,
                                     ProjectPurchaseApplyId = a.ProjectPurchaseApplyId,
                                     ProjectConstructionBudgetMaterialsId = a.ProjectConstructionBudgetMaterialsId,
                                     ContractQuntity = a.ContractQuntity,//合同数量
                                     NeedDate = a.NeedDate,
                                     SortCode = a.SortCode,
                                     CreationDate = a.CreationDate,
                                     Amount = a.ApplyTotal,
                                 };

            var jsonData = new
            {
                //Project_PurchaseApply = Project_PurchaseApplyData,
                Project_PurchaseApplyDetails = materialsApply,
            };

            return Success(materialsApply);
        }


        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            purchaseApplyIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strproject_PurchaseApplyDetailsList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_PurchaseApplyEntity>();
            var project_PurchaseApplyDetailsList = strproject_PurchaseApplyDetailsList.ToObject<List<Project_PurchaseApplyDetailsEntity>>();
            purchaseApplyIBLL.SaveEntity(keyValue, mainInfo, project_PurchaseApplyDetailsList, deleteList, type);
            return Success("保存成功！");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            purchaseApplyIBLL.Audit(keyValue);
            return Success("操作成功");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            purchaseApplyIBLL.UnAudit(keyValue);
            return Success("操作成功");
        }
        #endregion

    }
}
