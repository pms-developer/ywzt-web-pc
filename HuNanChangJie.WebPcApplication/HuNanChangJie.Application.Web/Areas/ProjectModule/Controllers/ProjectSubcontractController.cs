﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 12:02
    /// 描 述：分包合同
    /// </summary>
    public class ProjectSubcontractController : MvcControllerBase
    {
        private ProjectSubcontractIBLL projectSubcontractIBLL = new ProjectSubcontractBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }

        /// <summary>
        /// 材料清单对话框
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MaterialsDialog()
        {
            return View();
        }

        /// <summary>
        /// 工程量清单对话框
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult QuantitiesDialog()
        {
            return View();
        }

        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectSubcontractIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetMaterials(string pagination,string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectSubcontractIBLL.GetMaterials(paginationobj, queryJson);
            //var jsonData = new { rows = data };
            //return Success(jsonData);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetLeasehold(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectSubcontractIBLL.GetLeasehold(paginationobj, queryJson);
            //var jsonData = new { rows = data };
            //return Success(jsonData);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }


        [HttpGet, AjaxOnly]
        public ActionResult GetQuantities(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectSubcontractIBLL.GetQuantities(paginationobj, queryJson);
            //var jsonData = new { rows = data };
            //return Success(jsonData);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetList(string projectId)
        {
            var data = projectSubcontractIBLL.GetList(projectId);   
            return Success(data);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetSubcontractList(string projectId)
        {
            var data = projectSubcontractIBLL.GetSubcontractList(projectId);
            return Success(data);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_SubcontractData = projectSubcontractIBLL.GetProject_SubcontractEntity( keyValue );
            var Project_SubcontractMaterialsData = projectSubcontractIBLL.GetProject_SubcontractMaterialsList( Project_SubcontractData.ID );
            var Project_SubcontractQuantitiesData = projectSubcontractIBLL.GetProject_SubcontractQuantitiesList( Project_SubcontractData.ID );
            var Project_PaymentAgreementData = projectSubcontractIBLL.GetProject_PaymentAgreementList( Project_SubcontractData.ID );
            var jsonData = new {
                Project_Subcontract = Project_SubcontractData,
                Project_SubcontractMaterials = Project_SubcontractMaterialsData,
                Project_SubcontractQuantities = Project_SubcontractQuantitiesData,
                Project_PaymentAgreement = Project_PaymentAgreementData,
            };
            return Success(jsonData);
        }


        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoListse(string keyValue)
        {
            var Project_SubcontractData = projectSubcontractIBLL.GetProject_SubcontractEntity(keyValue);
            //var Project_SubcontractMaterialsData = projectSubcontractIBLL.GetProject_SubcontractMaterialsList(Project_SubcontractData.ID);
            var CB_LeaseholdData = projectSubcontractIBLL.GetCB_LeaseholdList(Project_SubcontractData.ID);
           // var Project_PaymentAgreementData = projectSubcontractIBLL.GetProject_PaymentAgreementList(Project_SubcontractData.ID);
            var jsonData = new
            {
                Project_Subcontract = Project_SubcontractData,
                //Project_SubcontractMaterials = Project_SubcontractMaterialsData,
                CB_Leasehold = CB_LeaseholdData,
               // Project_PaymentAgreement = Project_PaymentAgreementData,
            };
            return Success(jsonData);
        }

        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = projectSubcontractIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = projectSubcontractIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectSubcontractIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_SubcontractMaterialsList, string strproject_SubcontractQuantitiesList, string strproject_PaymentAgreementList,string deleteList)
        {
            strEntity = strEntity.Replace("\"IsBudget\":\"1\"", "\"IsBudget\":\"true\"");
            strEntity = strEntity.Replace("\"IsBudget\":\"0\"", "\"IsBudget\":\"false\"");
            var mainInfo = strEntity.ToObject<Project_SubcontractEntity>();
            var project_SubcontractMaterialsList = strproject_SubcontractMaterialsList.ToObject<List<Project_SubcontractMaterialsEntity>>();
            var project_SubcontractQuantitiesList = strproject_SubcontractQuantitiesList.ToObject<List<Project_SubcontractQuantitiesEntity>>();
            var project_PaymentAgreementList = strproject_PaymentAgreementList.ToObject<List<Project_PaymentAgreementEntity>>();
            projectSubcontractIBLL.SaveEntity(keyValue,mainInfo,project_SubcontractMaterialsList,project_SubcontractQuantitiesList,project_PaymentAgreementList,deleteList,type);
            return Success("保存成功！");
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveFormse(string keyValue, string type, string strEntity,string strCB_LeaseholdList, string deleteList)
        {
            strEntity = strEntity.Replace("\"IsBudget\":\"1\"", "\"IsBudget\":\"true\"");
            strEntity = strEntity.Replace("\"IsBudget\":\"0\"", "\"IsBudget\":\"false\"");
            var mainInfo = strEntity.ToObject<Project_SubcontractEntity>();
            //var project_SubcontractMaterialsList = strproject_SubcontractMaterialsList.ToObject<List<Project_SubcontractMaterialsEntity>>();
            var CB_LeaseholdList = strCB_LeaseholdList.ToObject<List<CB_Leasehold>>();
            //var project_PaymentAgreementList = strproject_PaymentAgreementList.ToObject<List<Project_PaymentAgreementEntity>>();
            projectSubcontractIBLL.SaveEntityse(keyValue, mainInfo, CB_LeaseholdList, deleteList, type);
            return Success("保存成功！");
        } 
        #endregion

    }
}
