﻿using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Util;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-18 12:19
    /// 描 述：开标登记
    /// </summary>
    public class BidOpeningController : MvcControllerBase
    {
        private BidOpeningIBLL bidOpeningIBLL = new BidOpeningBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = bidOpeningIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_BidOpeningData = bidOpeningIBLL.GetProject_BidOpeningEntity( keyValue );
            var Project_BidOpeningDetailsData = bidOpeningIBLL.GetProject_BidOpeningDetailsList( Project_BidOpeningData.ID );
            var jsonData = new {
                Project_BidOpening = Project_BidOpeningData,
                Project_BidOpeningDetails = Project_BidOpeningDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            bidOpeningIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_BidOpeningDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_BidOpeningEntity>();
            var project_BidOpeningDetailsList = strproject_BidOpeningDetailsList.ToObject<List<Project_BidOpeningDetailsEntity>>();
            bidOpeningIBLL.SaveEntity(keyValue,mainInfo,project_BidOpeningDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
