﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 15:57
    /// 描 述：证照社保
    /// </summary>
    public class ProjectLicenseSheBaoController : MvcControllerBase
    {
        private ProjectLicenseSheBaoIBLL projectLicenseSheBaoIBLL = new ProjectLicenseSheBaoBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectLicenseSheBaoIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetformInfo(string projectId)
        {
            var data = projectLicenseSheBaoIBLL.GetFormInfo(projectId);
            return Success(data);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_LicenseSheBaoData = projectLicenseSheBaoIBLL.GetProject_LicenseSheBaoEntity( keyValue );
            var Project_LicenseSheBaoDetailsData = projectLicenseSheBaoIBLL.GetProject_LicenseSheBaoDetailsList( Project_LicenseSheBaoData.ID );
            var jsonData = new {
                Project_LicenseSheBao = Project_LicenseSheBaoData,
                Project_LicenseSheBaoDetails = Project_LicenseSheBaoDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectLicenseSheBaoIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_LicenseSheBaoDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_LicenseSheBaoEntity>();
            var project_LicenseSheBaoDetailsList = strproject_LicenseSheBaoDetailsList.ToObject<List<Project_LicenseSheBaoDetailsEntity>>();
            projectLicenseSheBaoIBLL.SaveEntity(keyValue,mainInfo,project_LicenseSheBaoDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
