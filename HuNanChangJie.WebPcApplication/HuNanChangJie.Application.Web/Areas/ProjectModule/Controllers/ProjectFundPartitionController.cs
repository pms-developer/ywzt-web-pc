﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.Formula;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 10:17
    /// 描 述：成本分割设置
    /// </summary>
    public class ProjectFundPartitionController : MvcControllerBase
    {
        private ProjectFundPartitionIBLL projectFundPartitionIBLL = new ProjectFundPartitionBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }

        [HttpGet]
        public ActionResult FormulaSettings()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectFundPartitionIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult FormulaTest(string projectid)
        {
            var formula = new Formula();
            var data= formula.TestFormula(projectid);
            return Success(data);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_FundPartitionData = projectFundPartitionIBLL.GetProject_FundPartitionEntity( keyValue );
            var Project_FundPartitionDetailsData = projectFundPartitionIBLL.GetProject_FundPartitionDetailsList( Project_FundPartitionData.ID );
            var jsonData = new {
                Project_FundPartition = Project_FundPartitionData,
                Project_FundPartitionDetails = Project_FundPartitionDetailsData,
            };
            return Success(jsonData);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetformInfo(string projectId)
        {
            var data = projectFundPartitionIBLL.GetFormInfo(projectId);
            return Success(data);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectFundPartitionIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [ValidateInput(false)]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_FundPartitionDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_FundPartitionEntity>();
            var project_FundPartitionDetailsList = strproject_FundPartitionDetailsList.ToObject<List<Project_FundPartitionDetailsEntity>>();
            projectFundPartitionIBLL.SaveEntity(keyValue,mainInfo,project_FundPartitionDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
