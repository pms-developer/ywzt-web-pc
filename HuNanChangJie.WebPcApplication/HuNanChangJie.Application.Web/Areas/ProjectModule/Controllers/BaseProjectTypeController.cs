﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-05 15:08
    /// 描 述：项目类型
    /// </summary>
    public class BaseProjectTypeController : MvcControllerBase
    {
        private BaseProjectTypeIBLL baseProjectTypeIBLL = new BaseProjectTypeBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        [HttpGet, AjaxOnly]
        public ActionResult CheckIsSameName(string value)
        {
            var data = baseProjectTypeIBLL.CheckIsSameName(value);
            return Success(data);
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseProjectTypeIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Base_ProjectTypeData = baseProjectTypeIBLL.GetBase_ProjectTypeEntity( keyValue );
            var Base_ProjectTypeDetailsData = baseProjectTypeIBLL.GetBase_ProjectTypeDetailsList( Base_ProjectTypeData.ID );
            var jsonData = new {
                Base_ProjectType = Base_ProjectTypeData,
                Base_ProjectTypeDetails = Base_ProjectTypeDetailsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            baseProjectTypeIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strbase_ProjectTypeDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Base_ProjectTypeEntity>();
            var base_ProjectTypeDetailsList = strbase_ProjectTypeDetailsList.ToObject<List<Base_ProjectTypeDetailsEntity>>();
            baseProjectTypeIBLL.SaveEntity(keyValue,mainInfo,base_ProjectTypeDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
