﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2020-03-24 10:58 
    /// 描 述：完成决算 
    /// </summary> 
    public class ProjectFinalController : MvcControllerBase
    {
        private ProjectFinalIBLL projectFinalIBLL = new ProjectFinalBLL();

        #region  视图功能 

        /// <summary> 
        /// 主页面 
        /// <summary> 
        /// <returns></returns> 
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary> 
        /// 表单页 
        /// <summary> 
        /// <returns></returns> 
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region  获取数据 

        /// <summary> 
        /// 获取页面显示列表数据 
        /// <summary> 
        /// <param name="queryJson">查询参数</param> 
        /// <returns></returns> 
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectFinalIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary> 
        /// 获取表单数据 
        /// <summary> 
        /// <returns></returns> 
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_FinalData = projectFinalIBLL.GetProject_FinalEntity(keyValue);
            var Project_ContractQuantitiesData = projectFinalIBLL.GetProject_ContractQuantitiesList(Project_FinalData.ID);
            var Project_ContractMaterialsData = projectFinalIBLL.GetProject_ContractMaterialsList(Project_FinalData.ID);
            var jsonData = new
            {
                Project_Final = Project_FinalData,
                Project_ContractQuantities = Project_ContractQuantitiesData,
                Project_ContractMaterials = Project_ContractMaterialsData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectFinalIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string strproject_ContractQuantitiesList, string strproject_ContractMaterialsList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_FinalEntity>();
            var project_ContractQuantitiesList = strproject_ContractQuantitiesList.ToObject<List<Project_ContractQuantitiesEntity>>();
            var project_ContractMaterialsList = strproject_ContractMaterialsList.ToObject<List<Project_ContractMaterialsEntity>>();
            projectFinalIBLL.SaveEntity(keyValue, mainInfo, project_ContractQuantitiesList, project_ContractMaterialsList, deleteList, type);
            return Success("保存成功！");
        }

        [HttpPost,AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            projectFinalIBLL.Audit(keyValue);
            return Success("保存成功！");
        }

        [HttpPost, AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            projectFinalIBLL.UnAudit(keyValue);
            return Success("保存成功！");
        }
        #endregion

    }
}
