﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-12 18:00
    /// 描 述：供应商管理
    /// </summary>
    public class Base_SupplierController : MvcControllerBase
    {
        private Base_SupplierIBLL base_SupplierIBLL = new Base_SupplierBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        [HttpGet]
        public ActionResult Review()
        {
            return View();
        }
        [HttpGet]
        public ActionResult IndexReview()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = base_SupplierIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Base_SupplierData = base_SupplierIBLL.GetBase_SupplierEntity( keyValue );
            var Base_SupplierBankData = base_SupplierIBLL.GetBase_SupplierBankList( Base_SupplierData.ID );
            var Base_SupplierLinkmanData = base_SupplierIBLL.GetBase_SupplierLinkmanList( Base_SupplierData.ID );
            var jsonData = new {
                Base_Supplier = Base_SupplierData,
                Base_SupplierBank = Base_SupplierBankData,
                Base_SupplierLinkman = Base_SupplierLinkmanData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            base_SupplierIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strbase_SupplierBankList, string strbase_SupplierLinkmanList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Base_SupplierEntity>();
            var base_SupplierBankList = strbase_SupplierBankList.ToObject<List<Base_SupplierBankEntity>>();
            var base_SupplierLinkmanList = strbase_SupplierLinkmanList.ToObject<List<Base_SupplierLinkmanEntity>>();
            base_SupplierIBLL.SaveEntity(keyValue,mainInfo,base_SupplierBankList,base_SupplierLinkmanList,deleteList,type);
            return Success("保存成功！");
        }
        /// <summary>
        /// 评审
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="type"></param>
        /// <param name="strEntity"></param>
        /// <param name="strbase_SupplierBankList"></param>
        /// <param name="strbase_SupplierLinkmanList"></param>
        /// <param name="deleteList"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult DoReview(string keyValue, string strEntity)
        {
            var mainInfo = strEntity.ToObject<Base_SupplierEntity>();
            base_SupplierIBLL.Review(keyValue, mainInfo);
            return Success("保存成功！");
        }
        #endregion

    }
}
