﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-29 17:02
    /// 描 述：证照使用工资
    /// </summary>
    public class ProjectLicenseWageController : MvcControllerBase
    {
        private ProjectLicenseWageIBLL projectLicenseWageIBLL = new ProjectLicenseWageBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }

        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectLicenseWageIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取详情数据
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet,AjaxOnly]
        public ActionResult GetDetails(string projectId)
        {
            var data = projectLicenseWageIBLL.GetDetails(projectId);
            var jsonData = new { rows = data };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_LicenseWageData = projectLicenseWageIBLL.GetProject_LicenseWageEntity( keyValue );
            var Project_LicenseWageDetailsData = projectLicenseWageIBLL.GetProject_LicenseWageDetailsList( Project_LicenseWageData.ID );
            var jsonData = new {
                Project_LicenseWage = Project_LicenseWageData,
                Project_LicenseWageDetails = Project_LicenseWageDetailsData,
            };
            return Success(jsonData);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetformInfo(string projectId)
        {
            var data = projectLicenseWageIBLL.GetFormInfo(projectId);
            return Success(data);
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectLicenseWageIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_LicenseWageDetailsList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_LicenseWageEntity>();
            var project_LicenseWageDetailsList = strproject_LicenseWageDetailsList.ToObject<List<Project_LicenseWageDetailsEntity>>();
            projectLicenseWageIBLL.SaveEntity(keyValue,mainInfo,project_LicenseWageDetailsList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
