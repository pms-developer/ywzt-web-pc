﻿using System.Web.Mvc;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    public class DocumentController : MvcControllerBase
    {
        private Base_FileTypeIBLL fileTypeIBLL = new Base_FileTypeBLL();
        // GET: ProjectModule/Document
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #region 获取数据

        /// <summary>
        /// 获取实体
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetBaseFileRelationTypeEntity(string keyValue)
        {
            var mainInfo = fileTypeIBLL.GetBaseFileRelationTypeEntity(keyValue);
            var jsonData = new
            {
                DocumentType = mainInfo
            };
            return Success(jsonData);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string fileTypeID, string fileID)
        {
            fileTypeIBLL.SaveRelationEntity(fileTypeID, fileID);
            return Success("保存成功！");
        }

        #endregion
    }
}