﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-26 17:47
    /// 描 述：分包结算
    /// </summary>
    public class Project_SubcontractSettlementController : MvcControllerBase
    {
        private Project_SubcontractSettlementIBLL project_SubcontractSettlementIBLL = new Project_SubcontractSettlementBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = project_SubcontractSettlementIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_SubcontractSettlementData = project_SubcontractSettlementIBLL.GetProject_SubcontractSettlementEntity( keyValue );
            var Project_SubcontractSettlementMaterialsData = project_SubcontractSettlementIBLL.GetProject_SubcontractSettlementMaterialsList( Project_SubcontractSettlementData.ID );
            var Project_SubcontractSettlementQuantitiesData = project_SubcontractSettlementIBLL.GetProject_SubcontractSettlementQuantitiesList( Project_SubcontractSettlementData.ID );
            var CB_LeaseholdData = project_SubcontractSettlementIBLL.GetCB_LeaseholdList(Project_SubcontractSettlementData.ID);
            var jsonData = new {
                Project_SubcontractSettlement = Project_SubcontractSettlementData,
                Project_SubcontractSettlementMaterials = Project_SubcontractSettlementMaterialsData,
                Project_SubcontractSettlementQuantities = Project_SubcontractSettlementQuantitiesData,
                 CB_Leasehold = CB_LeaseholdData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = project_SubcontractSettlementIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = project_SubcontractSettlementIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            project_SubcontractSettlementIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_SubcontractSettlementMaterialsList, string strproject_SubcontractSettlementQuantitiesList,string strCB_LeaseholdList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_SubcontractSettlementEntity>();
            var project_SubcontractSettlementMaterialsList = strproject_SubcontractSettlementMaterialsList.ToObject<List<Project_SubcontractSettlementMaterialsEntity>>();
            var project_SubcontractSettlementQuantitiesList = strproject_SubcontractSettlementQuantitiesList.ToObject<List<Project_SubcontractSettlementQuantitiesEntity>>();
            var CB_LeaseholdList = strCB_LeaseholdList.ToObject<List<CB_Leasehold>>();
            project_SubcontractSettlementIBLL.SaveEntity(keyValue,mainInfo,project_SubcontractSettlementMaterialsList,project_SubcontractSettlementQuantitiesList, CB_LeaseholdList,deleteList, type);
            return Success("保存成功！");
        }
        #endregion

    }
}
