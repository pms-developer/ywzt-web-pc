﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-20 11:49
    /// 描 述：工程合同变更
    /// </summary>
    public class ProjectContractChangeController : MvcControllerBase
    {
        private ProjectContractChangeIBLL projectContractChangeIBLL = new ProjectContractChangeBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectContractChangeIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var contractBll = new ProjectContractBLL();
            var Project_ContractChangeData = projectContractChangeIBLL.GetProject_ContractChangeEntity( keyValue );
            var Project_ContractChangeMaterialsData = projectContractChangeIBLL.GetProject_ContractChangeMaterialsList( Project_ContractChangeData.ID );

            var Project_ContractChangeQuantitiesData = projectContractChangeIBLL.GetProject_ContractChangeQuantitiesList( Project_ContractChangeData.ID );
            var quantitiesIdList = Project_ContractChangeQuantitiesData.Select(i => i.ProjectContractQuantitiesId);
            var quantitiesList = contractBll.GetProject_ContractQuantitiesLists(quantitiesIdList);

            var quantities = from a in Project_ContractChangeQuantitiesData
                             join b in quantitiesList on a.ProjectContractQuantitiesId equals b.ID
                             orderby a.SortCode ascending
                             select new Project_ContractChangeQuantitiesEntity
                             {
                                 ID = a.ID,
                                 ProjectId = a.ProjectId,
                                 ProjectContractQuantitiesId = a.ProjectContractQuantitiesId,
                                 ProjectContractChangeId = a.ProjectContractChangeId,
                                 Code = a.Code,
                                 Name = a.Name,
                                 Feature = a.Feature,
                                 Unit = a.Unit,
                                 Quantities = a.Quantities,
                                 Price = a.Price,
                                 TotalPrice = a.TotalPrice,
                                 AfterQuantities = a.AfterQuantities,
                                 AfterPrice = a.AfterPrice,
                                 AfterTotalPrice = a.AfterTotalPrice,
                                 CreationDate = a.CreationDate,
                                 Remark = a.Remark,
                                 SortCode = a.SortCode,

                                 InitPrice = b.Price,
                                 InitQuantities = b.Quantities,
                                 InitTotal=b.TotalPrice,
                                 ChangePrice=b.ChangePrice,
                                 ChangeQuantities=b.ChangeQuantities,
                                 ChangeTotalPrice=b.ChangeTotalPrice
                             };

            var jsonData = new {
                Project_ContractChange = Project_ContractChangeData,
                Project_ContractChangeMaterials = Project_ContractChangeMaterialsData,
                Project_ContractChangeQuantities = quantities,
            };
            return Success(jsonData);
        }

      
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectContractChangeIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            projectContractChangeIBLL.Audit(keyValue);
            return Success("操作成功");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            projectContractChangeIBLL.UnAudit(keyValue);
            return Success("操作成功");
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_ContractChangeMaterialsList, string strproject_ContractChangeQuantitiesList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_ContractChangeEntity>();
            var project_ContractChangeMaterialsList = strproject_ContractChangeMaterialsList.ToObject<List<Project_ContractChangeMaterialsEntity>>();
            var project_ContractChangeQuantitiesList = strproject_ContractChangeQuantitiesList.ToObject<List<Project_ContractChangeQuantitiesEntity>>();
            projectContractChangeIBLL.SaveEntity(keyValue,mainInfo,project_ContractChangeMaterialsList,project_ContractChangeQuantitiesList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
