﻿using HuNanChangeJie.Application.Project.ProjectSubject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    public class ProjectSubjectController : MvcControllerBase
    {
        private IProjectSubject subjectBll = new ProjectSubjectBll();
        // GET: ProjectModule/ProjectSubject
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList(string projectId)
        {
            var data = subjectBll.GetList(projectId);
            return JsonResult(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSubjectList(string projectId)
        {
            var data = subjectBll.GetSubjectList(projectId);
            return Success(data);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult Insert(string projectId, string subjectIds)
        {
            subjectBll.Insert(projectId, subjectIds);
            return Success("保存成功");
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult Delete(string BaseSubjectId, string projectId)
        {
            var msg = subjectBll.Delete(BaseSubjectId, projectId);
            if (msg.Flag)
                return Success(msg.Info);
            else
                return Fail(msg.Info);
        }
    }
}