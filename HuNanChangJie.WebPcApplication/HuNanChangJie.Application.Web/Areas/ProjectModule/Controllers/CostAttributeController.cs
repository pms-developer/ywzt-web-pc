﻿using HuNanChangeJie.Application.Project.BaseInfo.CostAttrubite;
using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    public class CostAttributeController : MvcControllerBase
    {
        private ICostAttributeBLL costAttributeBll = new CostAttributeBLL();
        // GET: ProjectModule/CostAttribute
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Form()
        {
            return View();
        }
 
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList()
        {
            var data = costAttributeBll.GetList();
            return JsonResult(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var data = costAttributeBll.GetFormData(keyValue);
            return JsonResult(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity)
        {
            var info = strEntity.ToObject<CostAttributeEntity>();
            costAttributeBll.Save(keyValue, type, info);
            return Success("保存成功");
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult SetDeploy(string keyValue, string isDeploy)
        {
            var isdeploy = isDeploy.ToBool();
            costAttributeBll.SetDeploy(keyValue, isdeploy);
            return Success("部署成功");
        }


        
    }
}