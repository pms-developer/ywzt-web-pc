﻿using HuNanChangeJie.Application.Project.IBll;
using HuNanChangeJie.Application.Project.Model;
using HuNanChangeJie.Application.Project.ProjectBaseInfo;
using HuNanChangeJie.Application.Project.Team;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    public class ProjectController : MvcControllerBase
    {
        private ProjectIBLL projectIBLL = new ProjectBLL();

        #region  视图功能

        [HttpGet]
        public ActionResult Main()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Authorization()
        {
            return View();
        }

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        /// <summary>
        /// 我录入的项目
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MyInputProject()
        {
            return View();
        }
        #endregion

        #region  获取数据
        public ActionResult GetAuthorization(string projectId)
        {
            //var data 
            return Success(null);
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var userInfo = LoginUserInfo.UserInfo;
            var data = projectIBLL.GetPageList(paginationobj, queryJson, userInfo);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetProjectName()
        {

            var Project = projectIBLL.GetProjectName();
            var jsonData = new
            {
                ProjectName = Project
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var info = projectIBLL.GetProjectEntity(keyValue);

            return Success(info);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetCustomerInfo(string projectId)
        {
            var info = projectIBLL.GetCustomerInfo(projectId);
            return Success(info);
        }

        [HttpGet, AjaxOnly]
        public ActionResult ChangeConcern(string project_id, string cid)
        {
            var query = "";
            if (cid == "null")
            {
                query = projectIBLL.InsertConcernEntity(project_id);
            }
            else
            {
                query = projectIBLL.DeleteConcernEntity(cid);
            }
            return Success(query);
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }

        /// <summary>
        /// 审核实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult AuditForm(string keyValue)
        {
            string message = projectIBLL.AuditForm(keyValue);
            if (string.IsNullOrEmpty(message))
            {
                return Success("审核成功！");
            }
            else
            {
                return Fail(message);
            }
        }

        /// <summary>
        /// 反审核实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAuditForm(string keyValue)
        {
            string message = projectIBLL.UnAuditForm(keyValue);
            if (string.IsNullOrEmpty(message))
            {
                return Success("反审核成功！");
            }
            else
            {
                return Fail(message);
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity)
        {
            var entity = strEntity.ToObject<ProjectEntity>();
            projectIBLL.SaveEntity(keyValue, type, entity);
            return Success("保存成功！");
        }

        /// <summary>
        /// 项目基本信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult BaseInfo()
        {
            return View();
        }

        /// <summary>
        /// 项目组成员
        /// </summary>
        /// <returns></returns>
        public ActionResult Team()
        {
            return View();
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTeamList(string projectId)
        {
            ITeamBll teamBll = new TeamBll(projectId);
            var teamList = teamBll.GetTeamList();
            return JsonResult(teamList);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult SaveTeam(string projectId, string formData)
        {
            ITeamBll teamBll = new TeamBll(projectId);
            var teamList = formData.ToObject<List<TeamEntity>>();
            teamBll.SaveTeam(teamList);
            return Success("保存成功！");
        }

        #endregion
    }
}