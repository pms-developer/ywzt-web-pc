﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Util;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    public class DocumentTypeController : MvcControllerBase
    {
        private Base_FileTypeIBLL fileTypeIBLL = new Base_FileTypeBLL();

        #region  视图功能
        // GET: ProjectModule/DocumentType
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取树形
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFileTypeTree(string keyValue)
        {
            var data = fileTypeIBLL.GetFileTypeTree(keyValue);
            return this.JsonResult(data);
        }

        /// <summary>
        /// 获取数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFileTypeEntity(string keyValue)
        {
            var mainInfo = fileTypeIBLL.GetFileTypeEntity(keyValue);
            var jsonData = new
            {
                DocumentType = mainInfo
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取文件列表
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFileList(string queryJson)
        {
            var queryParam = queryJson.ToJObject();
            var keyValue = "";
            var projectId = "";
            if (!queryParam["keyValue"].IsEmpty())
                keyValue = queryParam["keyValue"].ToString();
            if (!queryParam["projectId"].IsEmpty())
                projectId= queryParam["projectId"].ToString();
            var mainInfo = fileTypeIBLL.GetFileList(keyValue, projectId);
            var jsonData = new
            {
                rows = mainInfo
            };
            return Success(jsonData);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity, string type)
        {
            var mainInfo = strEntity.ToObject<Base_FileTypeEntity>();
            fileTypeIBLL.SaveEntity(keyValue, mainInfo, type);
            return Success("保存成功！");
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            fileTypeIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        #endregion
    }
}