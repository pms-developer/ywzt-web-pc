﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-01 15:02
    /// 描 述：简历处理
    /// </summary>
    public class zhibinJianliController : MvcControllerBase
    {
        private zhibinJianliIBLL zhibinJianliIBLL = new zhibinJianliBLL();


        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = zhibinJianliIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var zhibin_jianliData = zhibinJianliIBLL.Getzhibin_jianliEntity( keyValue );
            var zhibin_j_descData = zhibinJianliIBLL.Getzhibin_j_descList( zhibin_jianliData.ID );
            var jsonData = new {
                zhibin_jianli = zhibin_jianliData,
                zhibin_j_desc = zhibin_j_descData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            zhibinJianliIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strzhibin_j_descList,string deleteList)
        {
            
            var mainInfo = strEntity.ToObject<zhibin_jianliEntity>();
            var zhibin_j_descList = strzhibin_j_descList.ToObject<List<zhibin_j_descEntity>>();

            if (type == "add")
            {
                int num = zhibinJianliIBLL.checkNoCount(mainInfo.jl_no);
                if (num != 0)
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    mainInfo.jl_no = codeRuleIBLL.GetBillCode("zhibinCode");
                    //return Fail("简历编号重复！");
                }
            }

            zhibinJianliIBLL.SaveEntity(keyValue,mainInfo,zhibin_j_descList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
