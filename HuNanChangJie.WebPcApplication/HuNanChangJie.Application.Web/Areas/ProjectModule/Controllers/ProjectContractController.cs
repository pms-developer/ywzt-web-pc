﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-19 14:21
    /// 描 述：工程合同
    /// </summary>
    public class ProjectContractController : MvcControllerBase
    {
        private ProjectContractIBLL projectContractIBLL = new ProjectContractBLL();
        private BaseMaterialsIBLL baseMaterialsIBLL = new BaseMaterialsBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }

        /// <summary>
        /// 合同工程量清单对话框
        /// </summary>
        /// <returns></returns>
        public ActionResult QuantitiesDialog()
        {
            return View();
        }

        /// <summary>
        /// 合同材料清单对话框
        /// </summary>
        /// <returns></returns>
        public ActionResult MaterialsDialog()
        {
            return View();
        }

        /// <summary>
        /// 合同选择对话框
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Dialog()
        {
            return View();
        }

        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectContractIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取工程合
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet,AjaxOnly]
        public ActionResult GetList(string projectId)
        {
            var data = projectContractIBLL.GetList(projectId);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetCustomerName(string projectId)
        {
            var data = projectContractIBLL.GetCustomerName(projectId);
            var jsonData = new
            {
                Name= data
            };
            return Success(jsonData);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetWaitFinalList(string projectId)
        {
            var data = projectContractIBLL.GetWaitFinalList(projectId);
            return Success(data);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_ContractData = projectContractIBLL.GetProject_ContractEntity( keyValue );
            var Project_ContractAgreementData = projectContractIBLL.GetProject_ContractAgreementList( Project_ContractData.ID );
            var Project_ContractMaterialsData = projectContractIBLL.GetProject_ContractMaterialsList( Project_ContractData.ID );
            var Project_ContractQuantitiesData = projectContractIBLL.GetProject_ContractQuantitiesList( Project_ContractData.ID );
            var jsonData = new {
                Project_Contract = Project_ContractData,
                Project_ContractAgreement = Project_ContractAgreementData,
                Project_ContractMaterials = Project_ContractMaterialsData,
                Project_ContractQuantities = Project_ContractQuantitiesData,
            };
            return Success(jsonData);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetContractInfo(string projectId)
        {
            var data = projectContractIBLL.GetContractInfo(projectId);
            return Success(data);
        }

        [HttpGet,AjaxOnly]
        public ActionResult GetDetails(string contractId)
        {
            var materials = projectContractIBLL.GetProject_ContractMaterialsList(contractId);
            var quantities = projectContractIBLL.GetProject_ContractQuantitiesList(contractId);
            var jsonData = new {
                materials,
                quantities
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取合同主表信息 
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var data = projectContractIBLL.GetFormData(keyValue);
            var json = new { data };
            return Success(json);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetQuantitiesList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectContractIBLL.GetQuantitiesList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaterialsList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectContractIBLL.GetMaterialsList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取指定项目工程合同（审核通过后）的初始签定金额
        /// </summary>
        /// <param name="projectId">项目名称</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetInitAmountTotals(string projectId)
        {
            var data = projectContractIBLL.GetInitAmountTotals(projectId);
            return Success(data);
        }

        /// <summary>
        /// 获取材料档案
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaterialsInfoList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseMaterialsIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        #endregion

        #region  提交数据
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            var result = projectContractIBLL.Audit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            var result = projectContractIBLL.UnAudit(keyValue);
            if (result.Success)
                return Success("审核事务处理成功！");
            else
                return Fail("审核事务处理失败！" + result.Message);
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectContractIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_ContractAgreementList, string strproject_ContractMaterialsList, string strproject_ContractQuantitiesList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_ContractEntity>();
            var project_ContractAgreementList = strproject_ContractAgreementList.ToObject<List<Project_ContractAgreementEntity>>();
            var project_ContractMaterialsList = strproject_ContractMaterialsList.ToObject<List<Project_ContractMaterialsEntity>>();
            var project_ContractQuantitiesList = strproject_ContractQuantitiesList.ToObject<List<Project_ContractQuantitiesEntity>>();
            projectContractIBLL.SaveEntity(keyValue,mainInfo,project_ContractAgreementList,project_ContractMaterialsList,project_ContractQuantitiesList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
