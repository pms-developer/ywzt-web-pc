﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.ProjectSubject; 

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-03 20:43
    /// 描 述：施工预算
    /// </summary>
    public class ProjectConstructionBudgetController : MvcControllerBase
    {
        private ProjectConstructionBudgetIBLL projectConstructionBudgetIBLL = new ProjectConstructionBudgetBLL();
        private IProjectSubject subjectBll = new ProjectSubjectBll();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        public ActionResult MaterialsDialog()
        {
            return View();
        }

        public ActionResult MaterialsListDialog()
        {
            return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectConstructionBudgetIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取施工预算材料清单
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaterials(string projectId)
        {
            var data = projectConstructionBudgetIBLL.GetMaterials(projectId);
            var jsonData = new { rows = data };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetBudgetList(BudgetQueryModel query)
        {
             var jsonData = new
            {
              BudugetList=  projectConstructionBudgetIBLL.GetBudgetList(query)
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetBudgetDetails(string budgetId)
        {
            var Project_ConstructionBudgetData = projectConstructionBudgetIBLL.GetProject_ConstructionBudgetEntity(budgetId);
            var Project_SubjectList = subjectBll.GetListByBudetId(budgetId);
            var project_SubjectFeeDetails = projectConstructionBudgetIBLL.Project_SubjectFeeDetailsEntityList(budgetId);
             var Project_ConstructionBudgetMaterialsData = projectConstructionBudgetIBLL.GetProject_ConstructionBudgetMaterialsList(budgetId);
            var jsonData = new
            {
                Project_ConstructionBudget = Project_ConstructionBudgetData,
                Project_Subjects = Project_SubjectList,
                Project_SubjectFeeDetailsEntities = project_SubjectFeeDetails,
                Project_ConstructionBudgetMaterials = Project_ConstructionBudgetMaterialsData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue) 
        {
            var Project_ConstructionBudgetData = projectConstructionBudgetIBLL.GetProject_ConstructionBudgetEntity(keyValue);
            var Project_ConstructionBudgetQuantitiesData = projectConstructionBudgetIBLL.GetQuantities(Project_ConstructionBudgetData.ID);
            var Project_ConstructionBudgetMaterialsData = projectConstructionBudgetIBLL.GetProject_ConstructionBudgetMaterialsList(Project_ConstructionBudgetData.ID);
            var jsonData = new
            {
                Project_ConstructionBudget = Project_ConstructionBudgetData,
                Project_ConstructionBudgetQuantities = Project_ConstructionBudgetQuantitiesData,
                Project_ConstructionBudgetMaterials = Project_ConstructionBudgetMaterialsData,
            };
            return Success(jsonData);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetFromInfo(string projectId)
        {
            var data = projectConstructionBudgetIBLL.GetFormInfo(projectId);
            return Success(data);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetFormInfoByCompanyId(string companyId)
        {
            var data = projectConstructionBudgetIBLL.GetFormInfoByCompanyId(companyId);
            return Success(data);
        }

        [HttpGet, AjaxOnly]
        public ActionResult GetFormInfoByDepartmentId(string departmentId)
        {
            var data = projectConstructionBudgetIBLL.GetFormInfoByDepartmentId(departmentId);
            return Success(data);
        }


        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsExceedBudget(string subjectId, string projectId, string amount)
        {
            var data = projectConstructionBudgetIBLL.CheckIsExceedBudget(subjectId, projectId, amount.ToDecimal());
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsExceedBudgetByBaseId(string subjectId, string projectId, string amount, string id)
        {
            var data = projectConstructionBudgetIBLL.CheckIsExceedBudgetByBaseId(subjectId, projectId, amount.ToDecimal(), id);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckIsExceedBudgetByProjectId(string projectId, string amount, string id, string typeid)
        {
            var data = projectConstructionBudgetIBLL.CheckIsExceedBudgetByProjectId(projectId, amount.ToDecimal(), id, typeid);
            return Success(data);
        }

        /// <summary>
        /// 获取材料档案
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaterialsInfoList(string pagination, string projectId,string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectConstructionBudgetIBLL.GetMaterialsInfoList(paginationobj, projectId, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectConstructionBudgetIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity, string projectSubjectList, string strproject_ConstructionBudgetQuantitiesList, string strproject_ConstructionBudgetMaterialsList, string deleteList, string costAttribute)
        {
            var mainInfo = strEntity.ToObject<Project_ConstructionBudgetEntity>();
            var subjectInfo = projectSubjectList.ToObject<List<ProjectSubjectEntity>>();
            var project_ConstructionBudgetQuantitiesList = strproject_ConstructionBudgetQuantitiesList.ToObject<List<Project_ConstructionBudgetQuantitiesEntity>>();
            var project_ConstructionBudgetMaterialsList = strproject_ConstructionBudgetMaterialsList.ToObject<List<Project_ConstructionBudgetMaterialsEntity>>();
            projectConstructionBudgetIBLL.SaveEntity(keyValue, mainInfo, subjectInfo, project_ConstructionBudgetQuantitiesList, project_ConstructionBudgetMaterialsList, deleteList, type);
            return Success("保存成功！");
        }
        

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="budgetId">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveBudget(string budgetId, string type, string strEntity, string projectSubjectList, string budgetFeeList, string budgetMaterialsList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_ConstructionBudgetEntity>();
            var subjectInfo = projectSubjectList.ToObject<List<ProjectSubjectEntity>>();
            var budgetFeeDetailsList = budgetFeeList.ToObject<List<Project_SubjectFeeDetailsEntity>>();
            var project_ConstructionBudgetMaterialsList = budgetMaterialsList.ToObject<List<Project_ConstructionBudgetMaterialsEntity>>();
            projectConstructionBudgetIBLL.SaveBudget(budgetId, mainInfo, subjectInfo, budgetFeeDetailsList, project_ConstructionBudgetMaterialsList, deleteList, type);
            return Success("保存成功！");
        }
        #endregion

    }
}
