﻿using HuNanChangeJie.Application.Project.BaseInfo.Model;
using HuNanChangeJie.Application.Project.BaseInfo.Subject;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    public class SubjectController : MvcControllerBase
    {
        private ISubjectBLL subjectBll = new SubjectBLL();
        // GET: ProjectModule/Subject
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Form()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string type, string strEntity)
        {
            strEntity = strEntity.Replace("\"IsProject\":\"1\"", "\"IsProject\":\"true\"");
            strEntity = strEntity.Replace("\"IsProject\":\"0\"", "\"IsProject\":\"false\"");
            strEntity = strEntity.Replace("\"IsManagement\":\"1\"", "\"IsManagement\":\"true\"");
            strEntity = strEntity.Replace("\"IsManagement\":\"0\"", "\"IsManagement\":\"false\"");
            strEntity = strEntity.Replace("\"IsEnable\":\"1\"", "\"IsEnable\":\"true\"");
            strEntity = strEntity.Replace("\"IsEnable\":\"0\"", "\"IsEnable\":\"false\"");
            var info = strEntity.ToObject<SubjectEntity>();
            subjectBll.Save(keyValue, type, info);
            return Success("保存成功");
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult SetEnable(string keyValue, string isEnable)
        {
            var enable = isEnable.ToBool();
            subjectBll.SetEnable(keyValue, enable);
            return Success("操作成功");
        }

        public ActionResult Delete(string keyValue)
        {
            subjectBll.Delete(keyValue);
            return Success("删除成功");
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList()
        {
            var data = subjectBll.GetList();
            return JsonResult(data);
        }

        /// <summary>
        /// 获取收支科目列表
        /// </summary>
        /// <param name="type">in:收入科目  out:支出科目</param>
        /// <returns></returns>
        public ActionResult GetSubjectList(string type)
        {
            var data = subjectBll.GetSubjectList(type);
            return JsonResult(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult CheckCodeIsExist(string code)
        {
            var data = subjectBll.CheckCodeIsExist(code);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTree()
        {
            var data = subjectBll.GetTree();
            return JsonResult(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var data = subjectBll.GetFormData(keyValue);
            return JsonResult(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetParentInfo(string keyValue)
        {
            var data = subjectBll.GetParentInfo(keyValue);
            return JsonResult(data);
        }

        public ActionResult SubjectDialog()
        {
            return View();
        }
    }
}