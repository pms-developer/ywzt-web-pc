﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-08 16:58
    /// 描 述：投标文档
    /// </summary>
    public class ProjectBidDocumentController : MvcControllerBase
    {
        private ProjectBidDocumentIBLL projectBidDocumentIBLL = new ProjectBidDocumentBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }

        [HttpGet]
        public ActionResult UploadDialog()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectBidDocumentIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 合并上传附件的分片数据
        /// </summary>
        /// <param name="projectId">项目名称</param>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="fileName">文件名</param>
        /// <param name="chunks">文件总分片数</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewMergeAnnexesFile(string projectId, string fileGuid, string fileName, int chunks)
        {
            UserInfo userInfo = LoginUserInfo.Get();
            bool res = projectBidDocumentIBLL.SaveAnnexes(projectId, fileGuid, fileName, chunks, userInfo);
            if (res)
            {
                return Success("保存文件成功");

            }
            else
            {
                return Fail("保存文件失败");
            }
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_BidDocumentData = projectBidDocumentIBLL.GetProject_BidDocumentEntity( keyValue );
            var jsonData = new {
                Project_BidDocument = Project_BidDocumentData,
            };
            return Success(jsonData);
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectBidDocumentIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_BidDocumentEntity>();
            projectBidDocumentIBLL.SaveEntity(keyValue,mainInfo,deleteList,type);
            return Success("保存成功！");
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="fileId">文件id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DownAnnexesFile(string fileId)
        {
            var data = projectBidDocumentIBLL.GetProject_BidDocumentEntity(fileId);
            string filename = Server.UrlDecode(data.FileName);//返回客户端文件名称
            string filepath = data.FilePath;
            if (FileDownHelper.FileExists(filepath))
            {
                FileDownHelper.DownLoadold(filepath, filename);
            }
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="fileId">文件主键</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAnnexesFile(string fileId)
        {
            var data = projectBidDocumentIBLL.GetProject_BidDocumentEntity(fileId);
            projectBidDocumentIBLL.DeleteEntity(fileId);
            //删除文件
            if (System.IO.File.Exists(data.FilePath))
            {
                System.IO.File.Delete(data.FilePath);
            }
            return Success("删除附件成功");
        }
        #endregion

    }
}
