﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-23 11:52
    /// 描 述：分包变更
    /// </summary>
    public class SubcontractChangeController : MvcControllerBase
    {
        private SubcontractChangeIBLL subcontractChangeIBLL = new SubcontractChangeBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = subcontractChangeIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_SubcontractChangeData = subcontractChangeIBLL.GetProject_SubcontractChangeEntity( keyValue );
            var Project_SubcontractChangeMaterialsData = subcontractChangeIBLL.GetProject_SubcontractChangeMaterialsList( Project_SubcontractChangeData.ID );
            var Project_SubcontractChangeQuantitiesData = subcontractChangeIBLL.GetProject_SubcontractChangeQuantitiesList( Project_SubcontractChangeData.ID );
            var CB_LeaseholdData = subcontractChangeIBLL.GetCB_LeaseholdList(Project_SubcontractChangeData.ID);
            var jsonData = new {
                Project_SubcontractChange = Project_SubcontractChangeData,
                Project_SubcontractChangeMaterials = Project_SubcontractChangeMaterialsData,
                Project_SubcontractChangeQuantities = Project_SubcontractChangeQuantitiesData,
                CB_Leasehold = CB_LeaseholdData,
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取工程合同总金额
        /// </summary>
        /// <param name="ProjectID"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetTotalAmount(string ProjectID) {
            var TotalAmount = subcontractChangeIBLL.GetTotalAmount(ProjectID);
            var jsonData = new
            {
               TotalAmount
            };
           return Success(jsonData);
        }



        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            subcontractChangeIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_SubcontractChangeMaterialsList, string strproject_SubcontractChangeQuantitiesList, string strCB_LeaseholdList, string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_SubcontractChangeEntity>();
            var project_SubcontractChangeMaterialsList = strproject_SubcontractChangeMaterialsList.ToObject<List<Project_SubcontractChangeMaterialsEntity>>();
            var project_SubcontractChangeQuantitiesList = strproject_SubcontractChangeQuantitiesList.ToObject<List<Project_SubcontractChangeQuantitiesEntity>>();
            var CB_LeaseholdList = strCB_LeaseholdList.ToObject<List<CB_Leasehold>>();
            subcontractChangeIBLL.SaveEntity(keyValue,mainInfo,project_SubcontractChangeMaterialsList,project_SubcontractChangeQuantitiesList, CB_LeaseholdList,deleteList, type);
            return Success("保存成功！");
        }

        [HttpPost,AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            subcontractChangeIBLL.Audit(keyValue);
            return Success("保存成功！");
        }

        [HttpPost, AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            subcontractChangeIBLL.UnAudit(keyValue);
            return Success("保存成功！");
        }
        #endregion

    }
}
