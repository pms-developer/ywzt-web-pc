﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 18:57
    /// 描 述：工程合同产值结算
    /// </summary>
    public class ProjectOutputSettlementController : MvcControllerBase
    {
        private ProjectOutputSettlementIBLL projectOutputSettlementIBLL = new ProjectOutputSettlementBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectOutputSettlementIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var contractBll = new ProjectContractBLL();
            var Project_OutputSettlementData = projectOutputSettlementIBLL.GetProject_OutputSettlementEntity( keyValue );
            var Project_OutputSettlementMaterialsData = projectOutputSettlementIBLL.GetProject_OutputSettlementMaterialsList( Project_OutputSettlementData.ID );
            var materialsIdList = Project_OutputSettlementMaterialsData.Select(i => i.ProjectContractMaterialsId);
            var materialsList = contractBll.GetProject_ContractMaterialsLists(materialsIdList);

            var materials = from a in Project_OutputSettlementMaterialsData
                            join b in materialsList on a.ProjectContractMaterialsId equals b.ID
                            orderby a.SortCode ascending
                            select new Project_OutputSettlementMaterialsEntity()
                            {
                                ID = a.ID,
                                ProjectId = a.ProjectId,
                                ProjectOutputSettlementId = a.ProjectOutputSettlementId,
                                ProjectContractMaterialsId = a.ProjectContractMaterialsId,
                                ListCode = a.ListCode,
                                Code = a.Code,
                                Name = a.Name,
                                Brand = a.Brand,
                                ModelNumber = a.ModelNumber,
                                Unit = a.Unit,
                                Quantities = a.Quantities,
                                Price = a.Price,
                                TotalPrice = a.TotalPrice,
                                CreationDate = a.CreationDate,
                                ApprovalQuantities = a.ApprovalQuantities,
                                ApprovalPrice = a.ApprovalPrice,
                                ApprovalTotal = a.ApprovalTotal,
                                SortCode = a.SortCode,
                                VisualProgress = a.VisualProgress,
                                Remark = a.Remark,

                                CurrentPrice = b.CurrentPrice,
                                CurrentQuantity = b.CurrentQuantity,
                                SettlementQuantities = b.SettlementQuantities,
                                SurplusAmount = (b.CurrentQuantity??0) -(b.SettlementQuantities ?? 0)
                            };

            var Project_OutputSettlementQuantitiesData = projectOutputSettlementIBLL.GetProject_OutputSettlementQuantitiesList( Project_OutputSettlementData.ID );
            var quantitiesIdList = Project_OutputSettlementQuantitiesData.Select(i => i.ProjectContractQuantitiesId);
            var quantitiesList = contractBll.GetProject_ContractQuantitiesLists(quantitiesIdList);

            var quantities = from a in Project_OutputSettlementQuantitiesData
                             join b in quantitiesList on a.ProjectContractQuantitiesId equals b.ID
                             orderby a.SortCode ascending
                             select new Project_OutputSettlementQuantitiesEntity
                             {
                                 ID = a.ID,
                                 ProjectId = a.ProjectId,
                                 ProjectOutputSettlementId = a.ProjectOutputSettlementId,
                                 ProjectContractQuantitiesId = a.ProjectContractQuantitiesId,
                                 Code = a.Code,
                                 Name = a.Name,
                                 Feature = a.Feature,
                                 Unit = a.Unit,
                                 Price = a.Price,
                                 Quantities = a.Quantities,
                                 TotalPrice = a.TotalPrice,
                                 VisualProgress = a.VisualProgress,
                                 ApprovalQuantities = a.ApprovalQuantities,
                                 ApprovalPrice = a.ApprovalPrice,
                                 ApprovalTotal = a.ApprovalTotal,
                                 SortCode = a.SortCode,
                                 CreationDate = a.CreationDate,
                                 Remark = a.Remark,

                                 CurrentQuantities = b.CurrentQuantities,
                                 CurrentPrice = b.CurrentPrice,
                                 SettlementQuantities = b.SettlementQuantities,
                                 SurplusAmount = (b.CurrentQuantities ?? 0) - (b.SettlementQuantities ?? 0)
        };


            var jsonData = new {
                Project_OutputSettlement = Project_OutputSettlementData,
                Project_OutputSettlementMaterials = materials,
                Project_OutputSettlementQuantities = quantities,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetYearData(string projectId,string ye) {
            ProjectOutputSettlementService pos = new ProjectOutputSettlementService();
            string str = pos.GetYearData(projectId, ye);
            return SuccessString(str);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectOutputSettlementIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_OutputSettlementMaterialsList, string strproject_OutputSettlementQuantitiesList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_OutputSettlementEntity>();
            var project_OutputSettlementMaterialsList = strproject_OutputSettlementMaterialsList.ToObject<List<Project_OutputSettlementMaterialsEntity>>();
            var project_OutputSettlementQuantitiesList = strproject_OutputSettlementQuantitiesList.ToObject<List<Project_OutputSettlementQuantitiesEntity>>();
            projectOutputSettlementIBLL.SaveEntity(keyValue,mainInfo,project_OutputSettlementMaterialsList,project_OutputSettlementQuantitiesList,deleteList,type);
            return Success("保存成功！");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string keyValue)
        {
            projectOutputSettlementIBLL.Audit(keyValue);
            return Success("操作成功");
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UnAudit(string keyValue)
        {
            projectOutputSettlementIBLL.UnAudit(keyValue);
            return Success("操作成功");
        }

        #endregion

    }
}
