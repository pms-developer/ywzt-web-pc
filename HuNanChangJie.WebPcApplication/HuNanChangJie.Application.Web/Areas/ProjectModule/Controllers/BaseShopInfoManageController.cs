﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-04-27 17:00
    /// 描 述：店铺基本信息管理
    /// </summary>
    public class BaseShopInfoManageController : MvcControllerBase
    {
        private BaseShopInfoManageIBLL baseShopInfoManageIBLL = new BaseShopInfoManageBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = baseShopInfoManageIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var base_shopinfoData = baseShopInfoManageIBLL.Getbase_shopinfoEntity( keyValue );
            var base_shopinfo_accountData = baseShopInfoManageIBLL.Getbase_shopinfo_accountList( base_shopinfoData.ID );
            var base_shopinfo_criticalpathData = baseShopInfoManageIBLL.Getbase_shopinfo_criticalpathList( base_shopinfoData.ID );
            var jsonData = new {
                base_shopinfo = base_shopinfoData,
                base_shopinfo_account = base_shopinfo_accountData,
                base_shopinfo_criticalpath = base_shopinfo_criticalpathData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            baseShopInfoManageIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strbase_shopinfo_accountList, string strbase_shopinfo_criticalpathList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<base_shopinfoEntity>();
            var base_shopinfo_accountList = strbase_shopinfo_accountList.ToObject<List<base_shopinfo_accountEntity>>();
            var base_shopinfo_criticalpathList = strbase_shopinfo_criticalpathList.ToObject<List<base_shopinfo_criticalpathEntity>>();
            baseShopInfoManageIBLL.SaveEntity(keyValue,mainInfo,base_shopinfo_accountList,base_shopinfo_criticalpathList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
