﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Web.Mvc;
using System.Collections.Generic;
using HuNanChangJie.SystemCommon;
using System.Linq;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-25 19:58
    /// 描 述：项目立项
    /// </summary>
    public class ProjectApprovalController : MvcControllerBase
    {
        private ProjectApprovalIBLL projectApprovalIBLL = new ProjectApprovalBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = projectApprovalIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var Project_ApprovalData = projectApprovalIBLL.GetProject_ApprovalEntity( keyValue );
            var Project_ApprovalMembersData = projectApprovalIBLL.GetProject_ApprovalMembersList( Project_ApprovalData.ID );
            var jsonData = new {
                Project_Approval = Project_ApprovalData,
                Project_ApprovalMembers = Project_ApprovalMembersData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取项目组成员类型
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetValueType()
        {
            var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "XMZCYQZFS");

            int type = 1;
            if (config!=null)
            {
                type = config.Value;
            }
            return Success(type);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetKeyValue(string projectId)
        {
            var data = projectApprovalIBLL.GetKeyValue(projectId);
             
            return Success(data);
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            projectApprovalIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strproject_ApprovalMembersList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<Project_ApprovalEntity>();
            var project_ApprovalMembersList = strproject_ApprovalMembersList.ToObject<List<Project_ApprovalMembersEntity>>();
            projectApprovalIBLL.SaveEntity(keyValue,mainInfo,project_ApprovalMembersList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
