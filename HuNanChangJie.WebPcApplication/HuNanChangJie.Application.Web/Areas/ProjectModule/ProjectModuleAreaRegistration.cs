﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.ProjectModule
{
    public class ProjectModuleAreaRegistration : AreaRegistration
    {
        public override string AreaName => "ProjectModule";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute("ProjectModule_default",
                "ProjectModule/{controller}/{action}/{id}",
                new { action="Index",id=UrlParameter.Optional}
                );
        }
    }
}