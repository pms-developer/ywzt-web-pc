﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-26 17:47
 * 描  述：分包结算
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_SubcontractSettlement";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/Project_SubcontractSettlement/SaveForm';
var projectId = request('projectId');

var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_SubcontractSettlementMaterials', "gridId": 'Project_SubcontractSettlementMaterials' });
                subGrid.push({ "tableName": 'Project_SubcontractSettlementQuantities', "gridId": 'Project_SubcontractSettlementQuantities' });
                subGrid.push({ "tableName": 'CB_Leasehold', "gridId": 'ID' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();

            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            page.bindEvent();
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0024' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            $("#ProjectSubcontractId").mkselect({
                url: top.$.rootUrl + "/ProjectModule/ProjectSubcontract/GetList?projectId=" + projectId,
                value: "ID",
                text: "FullName",
                title: "FullName"
            });

            $("#ProjectSubcontractId").on("change", function () {
                var data = $(this).mkselectGetEx();
                debugger;
                //console.log(data)
                //if (data != null) {
                $("#InitAmount01").val(data.TotalAmount);
                $("#InitAmount02").val(data.SettlementAmount);
                $("#ProjectID").val(data.ProjectID);
                $("#ProjectName").val(data.ProjectName);
                $("#Yi").val(data.Yi);
                $("#SubcontractTypeName").val(data.SubcontractTypeName);
                if (data.SubcontractTypeName == "机械租赁") {
                    $("#tabsub1s").hide();
                    $("#tabsub2s").hide();
                    $("#tabsub3s").show();
                }
                else {
                    $("#tabsub3s").hide();
                    $("#tabsub2s").show();
                    $("#tabsub1s").show();
                }
                //}
                //else {
                //    $("#InitAmount01").val("");
                //    $("#InitAmount02").val("");
                //    $("#ProjectID").val("");
                //    $("#ProjectName").val("");
                //}
            }
            );

            $('#OperatorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {},
            }).mkselectSet(loginInfo.departmentId);


            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            $("#testSelect").mkselect({
                //url: top.$.rootUrl + "/ProjectModule/ProjectSubcontract/GetList?projectId=" + projectId,
                //value: "租赁合同",
                //text: "租赁合同",
                //title: "租赁合同",
                //name:"租赁合同"

            });

            $('#CB_Leasehold').jfGrid({
                headData: [
                    //{
                    //    label: '编号', name: 'ID', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'string', tabname: '租赁清单'
                    //    , edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},
                    {
                        label: '租赁清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'Specification', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '租赁设备单价', name: 'RentalEquipment', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '设备单位', name: 'EquipmentUnit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '设备数量', name: 'Qty', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '设备租金', name: 'RentalFee', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'InitAmount', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '租期单位', name: 'RentalUnit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: ' 租期', name: 'Rental', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var SummationQty = parseFloat(row["RentalEquipment"]) * parseFloat(row["Qty"]) * parseFloat(row["Rental"]);
                                row["Summation"] = SummationQty;
                                $("div[colname='Summation'][rownum='CB_Leasehold_" + index + "']").html(SummationQty);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合计金额', name: 'Summation', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remarks', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],

                mainId: "ID", toolbarposition: "top", isEdit: true, showchoose: true, showadd: true,
                bindTable: "CB_Leasehold",
                height: 300, onChooseEvent: function () {
                    var contractId = $('#ProjectSubcontractId').mkselectGet();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择租赁合同清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectSubcontractRent/Leasehold?projectid=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            //row.ProjectId = projectId
                                            row.Name = data[i].Name
                                            row.Specification = data[i].Specification;
                                            row.RentalEquipment = data[i].RentalEquipment;
                                            row.EquipmentUnit = data[i].EquipmentUnit;
                                            row.Qty = data[i].Qty;
                                            row.RentalFee = data[i].RentalFee;
                                            row.RentalUnit = data[i].RentalUnit;
                                            row.Rental = data[i].Rental;
                                            row.Summation = data[i].Summation;
                                            row.Remarks = data[i].Remarks;
                                            rows.push(row);

                                        }
                                        $("#CB_Leasehold").jfGridSet("addRows", rows);

                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择租赁合同");
                    }
                }


                //mainId: "ID", toolbarposition: "top", isEdit: true, showchoose: false, showadd: true,
                //bindTable: "CB_Leasehold",
                //isEdit: true,
                //height: 300, toolbarposition: "top",
                //onAddRow: function (row, rows) {
                //    row["ID"] = Changjie.newGuid();
                //    row.rowState = 1;
                //    row.SortCode = rows.length;
                //    row.EditType = 1;
                //}

            });

            $('#Project_SubcontractSettlementQuantities').jfGrid({
                headData: [
                    {
                        label: '清单编号', name: 'Code', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '清单名称', name: 'Name', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '项目特征', name: 'Feature', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    //{
                    //    label: '计量单位', name: 'Unit', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'string', tabname: '工程量清单'
                    //},

                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    {
                        label: '综合单价', name: 'Price', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'decimal', tabname: '工程量清单'
                    },
                    {
                        label: '本次上报', name: 'l21ca945f90534774950d210182cac99f', width: 100, align: "center"
                        , children: [
                            {
                                label: '数量', name: 'Quantities', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单'
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractSettlementQuantities";

                                        row.TotalPrice = Changjie.getFloatValue(row.Quantities) * Changjie.getFloatValue(row.Price);
                                        setCellValue("TotalPrice", row, index, gridName, row.TotalPrice);

                                        page.setRate();



                                    }
                                }
                            },
                            {
                                label: '合价', name: 'TotalPrice', width: 100, required: '0',
                                datatype: 'float', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '本次审批', name: 'l21ca945f90534774950d210182cac99f', width: 100, align: "center"
                        , children: [
                            {
                                label: '数量', name: 'AuditQuantities', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单'
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractSettlementQuantities";

                                        row.AuditTotalPrice = Changjie.getFloatValue(row.AuditQuantities) * Changjie.getFloatValue(row.Price);
                                        setCellValue("AuditTotalPrice", row, index, gridName, row.AuditTotalPrice);

                                        page.setRate();
                                    }
                                }
                            },
                            {
                                label: '合价', name: 'AuditTotalPrice', width: 100, required: '0', inlineOperation: { to: [{ "t1": "Price", "t2": "AuditQuantities", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true },
                                datatype: 'float', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, aggtype: 'normal', headColor: "blue", aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID", toolbarposition: "top", isEdit: true, showchoose: true, showadd: false,
                bindTable: "Project_SubcontractSettlementQuantities",
                height: 300, onChooseEvent: function () {
                    var contractId = $('#ProjectSubcontractId').mkselectGet();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择分包合同工程量清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectSubcontract/QuantitiesDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectId = projectId
                                            row.ProjectSubcontractQuantitiesId = data[i].ID
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Feature = data[i].Feature;
                                            row.Unit = data[i].Unit;

                                            row.Price = data[i].Price;
                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }
                                        $("#Project_SubcontractSettlementQuantities").jfGridSet("addRows", rows);

                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择分包合同");
                    }
                }
            });
            $('#Project_SubcontractSettlementMaterials').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '材料编码', name: 'Code', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '名称', name: 'Name', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    //{
                    //    label: '计量单位', name: 'Unit', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'string', tabname: '材料清单'
                    //},

                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },

                    {
                        label: '税率', name: 'TaxRate', width: 140, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                                var gridName = "Project_SubcontractSettlementMaterials";
                                //row.TaxRate = row.InvoiceType;
                                //setCellValue("TaxRate", row, index, gridName, row.InvoiceType);

                                //console.log(row.SettlementPrice, Changjie.sysGlobalSettings.pointGlobal )

                                var TaxRates = 1 + Number(row.TaxRate);
                                row.PriceNoTax = (row.Price / TaxRates).toFixed(4);
                                if (!isNaN(row.PriceNoTax))
                                    setCellValue("PriceNoTax", row, index, gridName, row.PriceNoTax);

                                row.TotalPrice = (row.Price * row.Quantity).toFixed(4);
                                if (!isNaN(row.TotalPrice))
                                    setCellValue("TotalPrice", row, index, gridName, row.TotalPrice);

                                row.TotalPriceNoTax = (row.PriceNoTax * row.Quantity).toFixed(4);
                                if (!isNaN(row.TotalPriceNoTax))
                                    setCellValue("TotalPriceNoTax", row, index, gridName, row.TotalPriceNoTax);

                                row.AuditTotalPrice = (row.Price * row.AuditQuantity).toFixed(4);
                                if (!isNaN(row.AuditTotalPrice))
                                    setCellValue("AuditTotalPrice", row, index, gridName, row.AuditTotalPrice);

                                row.AuditTotalPriceNoTax = (row.PriceNoTax * row.AuditQuantity).toFixed(4);
                                if (!isNaN(row.AuditTotalPriceNoTax))
                                    setCellValue("AuditTotalPriceNoTax", row, index, gridName, row.AuditTotalPriceNoTax);

                            },
                            datatype: 'dataItem',
                            code: 'FPLX'
                        }
                    },
                    {
                        label: '单价(含税)', name: 'Price', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'decimal', tabname: '材料清单'
                    },
                    {
                        label: '单价(不含税)', name: 'PriceNoTax', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'decimal', tabname: '材料清单'
                    },
                    {
                        label: '本次上报', name: 'l21ca945f90534774950d210182cac99f', width: 100, align: "center"
                        , children: [
                            {
                                label: '数量', name: 'Quantity', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单'
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractSettlementMaterials";

                                        //row.TotalPrice = Changjie.getFloatValue(row.Quantity) * Changjie.getFloatValue(row.Price);
                                        //setCellValue("TotalPrice", row, index, gridName, row.TotalPrice);


                                        //var gridName = "Project_SubcontractSettlementMaterials";
                                        //row.TaxRate = row.InvoiceType;
                                        //setCellValue("TaxRate", row, index, gridName, row.InvoiceType);

                                        //console.log(row.SettlementPrice, Changjie.sysGlobalSettings.pointGlobal )

                                        //var TaxRates = 1 + Number(row.TaxRate);
                                        //row.PriceNoTax = (row.Price / TaxRates).toFixed(4);
                                        //if (!isNaN(row.PriceNoTax))
                                        //    setCellValue("PriceNoTax", row, index, gridName, row.PriceNoTax);

                                        row.TotalPrice = (row.Price * row.Quantity).toFixed(4);
                                        if (!isNaN(row.TotalPrice))
                                            setCellValue("TotalPrice", row, index, gridName, row.TotalPrice);

                                        row.TotalPriceNoTax = (row.PriceNoTax * row.Quantity).toFixed(4);
                                        if (!isNaN(row.TotalPriceNoTax))
                                            setCellValue("TotalPriceNoTax", row, index, gridName, row.TotalPriceNoTax);

                                        page.setRate();

                                        //row.AuditTotalPrice = (row.Price * row.AuditQuantity).toFixed(4);
                                        //if (!isNaN(row.AuditTotalPrice))
                                        //    setCellValue("AuditTotalPrice", row, index, gridName, row.AuditTotalPrice);

                                        //row.AuditTotalPriceNoTax = (row.PriceNoTax * row.AuditQuantity).toFixed(4);
                                        //if (!isNaN(row.AuditTotalPriceNoTax))
                                        //    setCellValue("AuditTotalPriceNoTax", row, index, gridName, row.AuditTotalPriceNoTax);
                                    }
                                }
                            },
                            {
                                label: '合价(含税)', name: 'TotalPrice', width: 100, required: '0', aggtype: 'sum', aggcolumn: 'TotalPrice',
                                datatype: 'float', tabname: '材料清单'
                            },

                            {
                                label: '合价(不含税)', name: 'TotalPriceNoTax', width: 100, required: '0',
                                datatype: 'float', tabname: '材料清单'
                            },
                        ]
                    },
                    {
                        label: '本次审批', name: 'l21ca945f90534774950d210182cac99f', width: 100, align: "center"
                        , children: [
                            {
                                label: '数量', name: 'AuditQuantity', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单'
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractSettlementMaterials";

                                        //row.AuditTotalPrice = Changjie.getFloatValue(row.AuditQuantity) * Changjie.getFloatValue(row.Price);
                                        //setCellValue("AuditTotalPrice", row, index, gridName, row.AuditTotalPrice);

                                        //var TaxRates = 1 + Number(row.TaxRate);
                                        //row.PriceNoTax = (row.Price / TaxRates).toFixed(4);
                                        //if (!isNaN(row.PriceNoTax))
                                        //    setCellValue("PriceNoTax", row, index, gridName, row.PriceNoTax);

                                        //row.TotalPrice = (row.Price * row.Quantity).toFixed(4);
                                        //if (!isNaN(row.TotalPrice))
                                        //    setCellValue("TotalPrice", row, index, gridName, row.TotalPrice);

                                        //row.TotalPriceNoTax = (row.PriceNoTax * row.Quantity).toFixed(4);
                                        //if (!isNaN(row.TotalPriceNoTax))
                                        //    setCellValue("TotalPriceNoTax", row, index, gridName, row.TotalPriceNoTax);

                                        row.AuditTotalPrice = (row.Price * row.AuditQuantity).toFixed(4);
                                        if (!isNaN(row.AuditTotalPrice))
                                            setCellValue("AuditTotalPrice", row, index, gridName, row.AuditTotalPrice);

                                        row.AuditTotalPriceNoTax = (row.PriceNoTax * row.AuditQuantity).toFixed(4);
                                        if (!isNaN(row.AuditTotalPriceNoTax))
                                            setCellValue("AuditTotalPriceNoTax", row, index, gridName, row.AuditTotalPriceNoTax);

                                        page.setRate();
                                    }
                                }
                            },
                            {
                                label: '合价(含税)', name: 'AuditTotalPrice', width: 100, required: '0', aggtype: 'sum', aggcolumn: 'AuditTotalPrice', inlineOperation: { to: [{ "t1": "Price", "t2": "AuditQuantities", "to": "AuditTotalPrice", "type": "mul", "toTargets": null }], isFirst: true },
                                datatype: 'float', tabname: '材料清单'
                            },

                            //{
                            //    label: '合价(含税)', name: 'AuditTotalPrice', width: 100, required: '0', aggtype: 'sum', aggcolumn: 'ReportedAmount',
                            //    datatype: 'float', tabname: '材料清单'
                            //},

                            {
                                label: '合价(不含税)', name: 'AuditTotalPriceNoTax', width: 100, required: '0', inlineOperation: { to: [{ "t1": "PriceNoTax", "t2": "AuditQuantities", "to": "AuditTotalPriceNoTax", "type": "mul", "toTargets": null }], isFirst: true },
                                datatype: 'float', tabname: '材料清单'
                            },
                        ]
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID", toolbarposition: "top", isEdit: true, showchoose: true, showadd: false,
                bindTable: "Project_SubcontractSettlementMaterials",
                height: 300, onChooseEvent: function () {
                    var contractId = $('#ProjectSubcontractId').mkselectGet();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择分包合同材料清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectSubcontract/MaterialsDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectId = projectId;
                                            row.ProjectSubcontractMaterialsId = data[i].ID;
                                            row.ListCode = data[i].ListCode;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].Brand;
                                            row.ModelNumber = data[i].ModelNumber;
                                            row.Unit = data[i].Unit;

                                            row.Price = data[i].Price;
                                            //row.InitQuantity = data[i].Quantity;
                                            //row.InitTotalPrice = data[i].TotalPrice;
                                            //row.ChangePrice = data[i].ChangePrice;
                                            //row.ChangeQuantity = data[i].ChangeQuantity;
                                            //row.ChangeTotalPrice = data[i].ChangeTotalPrice;

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }
                                        $("#Project_SubcontractSettlementMaterials").jfGridSet("addRows", rows);

                                        var freight = parseFloat($("#Freight").val() || 0);
                                        var signAmount = parseFloat($("#Project_SubcontractSettlementMaterials").jfGridGet("footerdata")["Amount"] || 0);
                                        $("#SignAmount").val(freight + signAmount);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择分包合同");
                    }
                }
            });

            //$('#Project_SubcontractSettlementMaterials').jfGrid({
            //    headData: [
            //        {
            //            label: '分包合同材料清单ID', name: 'ModelNumber', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
            //            datatype:'string', tabname:'材料清单'                     },
            //        {
            //            label: '数量', name: 'Quantity', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
            //            datatype:'int', tabname:'材料清单' 
            //            ,edit:{
            //                type:'input',
            //               change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
            //               },
            //               blur: function (row, rowindex, oldValue, colname, obj, headData) {
            //               }
            //            }
            //        },
            //        {
            //            label: '审批数据', name: 'AuditQuantity', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
            //            datatype:'int', tabname:'材料清单' 
            //            ,edit:{
            //                type:'input',
            //               change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
            //               },
            //               blur: function (row, rowindex, oldValue, colname, obj, headData) {
            //               }
            //            }
            //        },
            //    ],
            //    mainId:"ID",
            //    bindTable:"Project_SubcontractSettlementMaterials",
            //    isEdit: true,
            //    height: 300,
            //    rowdatas:[
            //    {
            //         "SortCode": 1,
            //         "rowState": 1,
            //         "ID": Changjie.newGuid(),
            //         "EditType": 1,
            //    }],
            //    onAddRow: function(row,rows){
            //         row["ID"]=Changjie.newGuid();
            //         row.rowState=1;
            //         row.SortCode=rows.length;
            //         row.EditType=1;
            //     },
            //    onMinusRow: function(row,rows,headData){
            //    }
            //});
            //$('#Project_SubcontractSettlementQuantities').jfGrid({
            //    headData: [
            //        {
            //            label: '分包合同工程量清单ID', name: 'ProjectSubcontractQuantitiesId', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
            //            datatype:'string', tabname:'工程量清单'                     },
            //        {
            //            label: '工程量', name: 'Quantities', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
            //            datatype:'int', tabname:'工程量清单' 
            //            ,edit:{
            //                type:'input',
            //               change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
            //               },
            //               blur: function (row, rowindex, oldValue, colname, obj, headData) {
            //               }
            //            }
            //        },
            //        {
            //            label: '审批数量', name: 'AuditQuantities', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
            //            datatype:'int', tabname:'工程量清单' 
            //            ,edit:{
            //                type:'input',
            //               change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
            //               },
            //               blur: function (row, rowindex, oldValue, colname, obj, headData) {
            //               }
            //            }
            //        },
            //    ],
            //    mainId:"ID",
            //    bindTable:"Project_SubcontractSettlementQuantities",
            //    isEdit: true,
            //    height: 300,
            //    rowdatas:[
            //    {
            //         "SortCode": 1,
            //         "rowState": 1,
            //         "ID": Changjie.newGuid(),
            //         "EditType": 1,
            //    }],
            //    onAddRow: function(row,rows){
            //         row["ID"]=Changjie.newGuid();
            //         row.rowState=1;
            //         row.SortCode=rows.length;
            //         row.EditType=1;
            //     },
            //    onMinusRow: function(row,rows,headData){
            //    }
            //});

        },

        setRate: function () {
            //var rowsMaterials = $('#Project_SubcontractSettlementMaterials').jfGridGet('rowdatas');
            var rowsQuantities = $('#Project_SubcontractSettlementQuantities').jfGridGet('rowdatas');

            var sum_totalprice = 0;
            var sum_auditTotalPrice = 0;

            //for (var i = 0; i < rowsMaterials.length; i++) {
            //    if (!isNaN(rowsMaterials[i].TotalPrice))
            //        sum_totalprice += Number(rowsMaterials[i].TotalPrice);
            //    if (!isNaN(rowsMaterials[i].AuditTotalPrice))
            //        sum_auditTotalPrice += Number(rowsMaterials[i].AuditTotalPrice);
            //}

            for (var i = 0; i < rowsQuantities.length; i++) {
                if (!isNaN(rowsQuantities[i].TotalPrice))
                    sum_totalprice += Number(rowsQuantities[i].TotalPrice);
                if (!isNaN(rowsQuantities[i].AuditTotalPrice))
                    sum_auditTotalPrice += Number(rowsQuantities[i].AuditTotalPrice);
            }

            $("#ReportedAmount").val(parseFloat(sum_totalprice).toFixed(Changjie.sysGlobalSettings.pointGlobal));

            $("#AuditAmount").val(parseFloat(sum_auditTotalPrice).toFixed(Changjie.sysGlobalSettings.pointGlobal));
        },

        bindEvent: function () {

            //$("#addQuantities").on("click", function () {

            //});
            //$("#deleteQuantities").on("click", function () {
            //    var rowIndex = $("#Project_SubcontractSettlementQuantities").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Project_SubcontractSettlementQuantities").jfGridSet("removeRow");
            //    }
            //});

            //$("#addMaterials").on("click", function () {

            //});
            //$("#deleteMaterials").on("click", function () {
            //    var rowIndex = $("#Project_SubcontractSettlementMaterials").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Project_SubcontractSettlementMaterials").jfGridSet("removeRow");
            //    }
            //});
        },
        initData: function () {
            if (!!keyValue) {

                $.mkSetForm(top.$.rootUrl + '/ProjectModule/Project_SubcontractSettlement/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        debugger;
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/Project_SubcontractSettlement/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}

var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;
    $cell.html(value);
    $edit.val(value);
};


var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    //for(var item in subGrid){
    //   deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
    //   var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
    //   if (!info.isPass) {
    //      isgridpass = false;
    //      errorInfos.push(info.errorCells);
    //   }
    // }
    // if (!isgridpass) {
    //     for (var i in errorInfos[0]) {
    //         top.Changjie.alert.error(errorInfos[0][i].Msg);
    //     }
    //     return false;
    // }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_SubcontractSettlement"]').mkGetFormData());
    postData.strproject_SubcontractSettlementMaterialsList = JSON.stringify($('#Project_SubcontractSettlementMaterials').jfGridGet('rowdatas'));
    postData.strproject_SubcontractSettlementQuantitiesList = JSON.stringify($('#Project_SubcontractSettlementQuantities').jfGridGet('rowdatas'));
    postData.strCB_LeaseholdList = JSON.stringify($('#CB_Leasehold').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
