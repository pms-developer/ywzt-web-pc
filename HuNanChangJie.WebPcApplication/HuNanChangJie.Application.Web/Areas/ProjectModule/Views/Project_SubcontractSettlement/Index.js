﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-26 17:47
 * 描  述：分包结算
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');

var checkJson = "";
var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/ProjectModule/Project_SubcontractSettlement/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/ProjectModule/Project_SubcontractSettlement/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};


var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {



            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });

            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 800);

            $('#AuditStatus').mkselect({
                type: 'default',
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ]
            });

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增分包结算',
                    url: top.$.rootUrl + '/ProjectModule/Project_SubcontractSettlement/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
          
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/Project_SubcontractSettlement/GetPageList',
                headData: [
                    { label: "编号", name: "Code", width: 140, align: "left"},
                    {
                        label: "结算日期", name: "SettlementDate", width: 80, align: "left", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }
                    },
                    {
                        label: "分包合同", name: "ProjectSubcontractName", width: 300, align: "left",
                        //formatterAsync: function (callback, value, row, op, $cell) {
                        //    Changjie.clientdata.getAsync('sourceData', {
                        //        code:  'FBHTLB',
                        //        key: value,
                        //        keyId: 'id',
                        //        callback: function (_data) {
                        //            callback(_data['title']);
                        //        }
                        //    });
                        //}
                    },
                    { label: "上报金额", name: "ReportedAmount", width: 100, align: "center"},
                    { label: "结算应付总额", name: "SettlementPaymentTotalAmount", width: 100, align: "center"},
                    { label: "审批金额", name: "AuditAmount", width: 100, align: "center"},
                    { label: "结算质保金额", name: "SettlementWarrantyAmount", width: 100, align: "center"},
                    {
                        label: "经办人", name: "OperatorName", width: 60, align: "center"},
                    {
                        label: "经办部门", name: "OperatorDepartmentName", width: 100, align: "center"
                    }, {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "摘要", name: "Abstract", width: 300, align: "left"},
                    
                ],
                mainId:'ID',
                isPage: true
            });
            //page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/Project_SubcontractSettlement/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '分包结算',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/Project_SubcontractSettlement/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&projectId=" + projectId + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
