﻿var contractId = request("contractId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            if (!!contractId) {
                page.bind();
            }
        },
       


        bind: function () {

            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectContract/GetQuantitiesList',
                headData: [
                    {
                        label: '清单编号', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '项目特征', name: 'Feature', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '初始合同', field: 'lffec359be274440f80c3a356d3d08b3f', align: "center", width: 100, cellStyle: { 'text-align': 'right' }
                        , children: [
                            {
                                label: '综合单价', name: 'Price', width: 100
                            },
                            {
                                label: '工程量', name: 'Quantities', width: 100
                            },
                            {
                                label: '合价', name: 'TotalPrice', width: 100
                            }
                        ]
                    },
                    {
                        label: '累计变更', field: 'l2ef66be4768e4a7fa91244e8d9c274e4', width: 100, align: "center", cellStyle: { 'text-align': 'center' },
                        children: [
                            {
                                label: '综合单价', name: 'ChangePrice', width: 100
                            },
                            {
                                label: '工程量', name: 'ChangeQuantities', width: 100
                            },
                            {
                                label: '合价', name: 'ChangeTotalPrice', width: 100
                            }
                        ]
                    },
                    {
                        label: '当前合同', field: 'l8de81b6cc98b4cf4acc7d35f9cf9ec92', align: "center", width: 100, cellStyle: { 'text-align': 'left' },
                        children: [
                            {
                                label: '综合单价', name: 'CurrentPrice', width: 100
                            },
                            {
                                label: '工程量', name: 'CurrentQuantities', width: 100
                            },
                            {
                                label: '合价', name: 'CurrentTotalPrice', width: 100
                            },
                        ]
                    },
                    {
                        label: '累计签证', field: 'l8de81b6cc98b4cf4acc7d35f9cf9ec99', align: "center", width: 100, cellStyle: { 'text-align': 'left' },
                        children: [
                            {
                                label: '综合单价', name: 'VisaPrice', width: 100
                            },
                            {
                                label: '工程量', name: 'VisaQuantities', width: 100
                            },
                            {
                                label: '合价', name: 'VisaTotalPrice', width: 100
                            },
                        ]
                    },

                ],
                mainId: "ID",
                bindTable: "Project_ContractChangeQuantities",
                height: 280,
                isPage: true,
                isMultiselect:true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.ContractId = contractId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    page.init();
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

};