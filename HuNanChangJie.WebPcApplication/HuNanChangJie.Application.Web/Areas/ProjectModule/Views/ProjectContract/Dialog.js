﻿var projectId = request("projectId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            if (!!projectId) {
                $('#gridtable').jfGrid({
                    url: top.$.rootUrl + '/ProjectModule/ProjectContract/GetPageList',
                    headData: [
                        { label: "合同编号", name: "Code", width: 100, align: "left" },
                        { label: "合同名称", name: "Name", width: 100, align: "left" },
                        {
                            label: "甲方", name: "Jia", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('sourceData', {
                                    code:  'KHLB',
                                    key: value,
                                    keyId: 'id',
                                    callback: function (_data) {
                                        callback(_data['fullname']);
                                    }
                                });
                            }
                        },
                        {
                            label: "乙方", name: "Yi", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('company', {
                                    key: value,
                                    callback: function (_data) {
                                        callback(_data.name);
                                    }
                                });
                            }
                        },
                        { label: "合同金额", name: "TotalAmount", width: 100, align: "left" },
                        { label: "签订日期", name: "SignDate", width: 100, align: "left" },
                        {
                            label: "合同类型", name: "ContractType", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('dataItem', {
                                    key: value,
                                    code: 'ContractType',
                                    callback: function (_data) {
                                        callback(_data.text);
                                    }
                                });
                            }
                        },
                        {
                            label: "项目经理", name: "ManagementId", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('user', {
                                    key: value,
                                    callback: function (_data) {
                                        callback(_data.name);
                                    }
                                });
                            }
                        },
                        { label: "外部合同编号", name: "ExternalCode", width: 100, align: "left" },
                        {
                            label: "发票类型", name: "InvoiceType", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('dataItem', {
                                    key: value,
                                    code: 'FPLX',
                                    callback: function (_data) {
                                        callback(_data.text);
                                    }
                                });
                            }
                        },
                        {
                            label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                            formatter: function (cellvalue, row) {
                                return top.Changjie.tranAuditStatus(cellvalue);
                            },
                        }
                    ],
                    mainId: 'ID',
                    isPage: true,
                    height: 362,
                });
                page.search();
            }
        },
        search: function (param) {
            param = param || {};
            param.ProjectId = projectId;
            param.AuditStatus = "2";
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何合同");
        }

    };
    page.init();
};