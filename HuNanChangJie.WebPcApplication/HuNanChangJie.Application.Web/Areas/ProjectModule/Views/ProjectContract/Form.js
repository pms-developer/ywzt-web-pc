﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-19 14:21
 * 描  述：工程合同
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_Contract";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectContract/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');

var auditPassEventBat = function (keyValue) {
    top.Changjie.clientdata.refresh("sourceData", { code: 'GCHTLB' })
    top.Changjie.httpPost(top.$.rootUrl + '/ProjectModule/ProjectContract/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.clientdata.refresh("sourceData", { code: 'GCHTLB' })
    top.Changjie.httpPost(top.$.rootUrl + '/ProjectModule/ProjectContract/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_ContractAgreement', "gridId": 'Project_ContractAgreement' });
                subGrid.push({ "tableName": 'Project_ContractMaterials', "gridId": 'Project_ContractMaterials' });
                subGrid.push({ "tableName": 'Project_ContractQuantities', "gridId": 'Project_ContractQuantities' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {

            $("#InitAmount").on("input propertychange", function (e) {
                var rate = parseFloat($("#InvoiceType").mkselectGet());
                var baseValue = $(this).val();

                //税额 （税额公式为: 金额 / (1 + 税率) * 税率）
                var result = (baseValue / (1 + rate) * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                $("#TaxAmount").val(result || "");
            });

            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'ContractCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#ProjectMode').mkDataItemSelect({ code: 'ProjectMode' });
            $('#ContractType').mkDataItemSelect({ code: 'ContractType' });
            $('#Jia').mkDataSourceSelect({ code: 'KHLB', value: 'id', text: 'fullname' });
            $('#Yi').mkCompanySelect({});
            $('#PriceType').mkRadioCheckbox({
                type: 'radio',
                code: 'ValuationType',
            });
            $('#InvoiceType').mkDataItemSelect({ code: 'FPLX' }).on("change", function () {
                var rate = parseFloat($(this).mkselectGet());
                var baseValue = $("#InitAmount").val();

                //税额 （税额公式为: 金额 / (1 + 税率) * 税率）
                var result = (baseValue / (1 + rate) * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal)
               
                $("#TaxAmount").val(result || "");
            });


            $('#IsSignAgreement').mkRadioCheckbox({
                type: 'radio',
                code: 'SZSF',
            });


            $('#SaleUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#ManagementId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_ContractAgreement').jfGrid({
                headData: [
                    {
                        label: '款项性质', name: 'FundType', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '支付方式', name: 'PaymentType', width: 150, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataItem',
                            code: 'CustomerPaymentType'
                        }
                    },
                    {
                        label: '支付比例', name: 'PaymentPercent', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '收款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var baseValue = $("#InitAmount").val();
                                var result = baseValue * row["PaymentPercent"] / 100;
                                row["Amount"] = result
                                $("div[colname='Amount'][rownum='Project_ContractAgreement_" + index + "']").html(result);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '协议收款金额', name: 'Amount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '收款协议'
                    },
                    {
                        label: '预计收款时间', name: 'PredictDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '排序号', name: 'SortCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'int', tabname: '收款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '说明', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '收款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    }
                ],
                mainId: "ID", toolbarposition: "top",
                bindTable: "Project_ContractAgreement",
                isEdit: true,
                height: 300, //showchoose: true,
                onChooseEvent: function () {
                    var contractId = $("#ContractId").val();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectContarctQuantities",
                            width: 800,
                            height: 400,
                            title: "选择合同工程量清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectContract/QuantitiesDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectContractQuantitiesId = data[i].ID;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Feature = data[i].Feature;
                                            row.Unit = data[i].Unit;
                                            row.SortCode = data[i].SortCode;

                                            row.InitPrice = data[i].Price;
                                            row.InitQuantities = data[i].Quantities;
                                            row.InitTotal = data[i].TotalPrice;

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.ProjectId = projectId;

                                            rows.push(row);
                                        }
                                        $("#Project_VisaQuantities").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择合同");
                    }
                },
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                    row.Amount = 0;
                    row.PaymentPercent = 0;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#Project_ContractQuantities').jfGrid({
                headData: [
                    {
                        label: '清单编号', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '项目特征', name: 'Feature', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            }
                        }
                    },

                    //{
                    //    label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'string', tabname: '工程量清单'
                    //    , edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},
                    {
                        label: '工程量', name: 'Quantities', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "Quantities", "t2": "Price", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '综合单价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "Quantities", "t2": "Price", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合价', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '工程量清单'
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    }
                ],
                mainId: "ID",
                bindTable: "Project_ContractQuantities",
                isEdit: true,
                height: 300, toolbarposition: "top",

                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                    row.ProjectId = projectId;
                    row.Quantities = 0;
                    row.Price = 0;
                    row.TotalPrice = 0;
                    row.SortCode = rows.length;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#Project_ContractMaterials').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '材料编码', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Quantity", "t2": "Price", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Quantity", "t2": "Price", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合计金额', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '材料清单'
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    }
                ],
                mainId: "ID", toolbarposition: "top",
                bindTable: "Project_ContractMaterials",
                isEdit: true,
                height: 300, showchoose: true, onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectQuantities",
                        width: 800,
                        height: 400,
                        title: "选施工预算材料清单",
                        url: top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/MaterialsDialog?projectId=" + projectId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.ProjectConstructionBudgetMaterialsId = data[i].ID;
                                        row.ProjectId = projectId;
                                        row.ListCode = data[i].ListCode;
                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Brand = data[i].Brand;
                                        row.ModelNumber = data[i].ModelNumber;
                                        row.Unit = data[i].Unit;

                                        //数量、价格、金额
                                        row.Quantity = data[i].Quantity || 0;
                                        row.Price = data[i].BudgetPrice || 0;
                                        row.TotalPrice = row.Quantity * row.Price;

                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        row.MaterialsSource = data[i].MaterialsSource == 1 ? 1 : 0;
                                        rows.push(row);
                                    }
                                    $("#Project_ContractMaterials").jfGridSet("addRows", rows);
                                }
                            });
                        }
                    });
                },
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                    row.ProjectId = projectId;
                    row.Quantity = 0;
                    row.Price = 0;
                    row.TotalPrice = 0;
                    row.SortCode = rows.length;
                    row.MaterialsSource = 0;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectContract/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectContract/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return fChangjie
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_Contract"]').mkGetFormData());

    postData.strproject_ContractAgreementList = JSON.stringify($('#Project_ContractAgreement').jfGridGet('rowdatas'));
    postData.strproject_ContractMaterialsList = JSON.stringify($('#Project_ContractMaterials').jfGridGet('rowdatas'));
    postData.strproject_ContractQuantitiesList = JSON.stringify($('#Project_ContractQuantities').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
