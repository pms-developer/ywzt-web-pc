﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-19 14:21
 * 描  述：工程合同
 */
var projectId = request("projectId");
var refreshGirdData;
var formId = request("formId");
var checkJson = "";

var auditPassEventBat = function (keyValue) {
    top.Changjie.clientdata.refresh("sourceData", { code: 'GCHTLB' })
    top.Changjie.httpPost(top.$.rootUrl + '/ProjectModule/ProjectContract/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.clientdata.refresh("sourceData", { code: 'GCHTLB' })
    top.Changjie.httpPost(top.$.rootUrl + '/ProjectModule/ProjectContract/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};


var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 800);

            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });

            $('#AuditStatus').mkselect({
                type: 'default',
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ]
            });

            $('#ManagementId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });

            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增工程合同',
                    url: top.$.rootUrl + '/ProjectModule/ProjectContract/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

        },
        // 初始化列表
        initGird: function () {//项目 项目经理  合同名称  甲方名称
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectContract/GetPageList',
                headData: [
                    { label: "合同编码", name: "ExternalCode", width: 140, align: "left" },
                    { label: "系统编码", name: "Code", width: 140, align: "left" },
                    { label: "合同名称", name: "Name", width: 250, align: "left" },
                    { label: "项目名称", name: "ProjectName", width: 140, align: "left" },
                    { label: "项目经理", name: "F_RealName", width: 140, align: "left" },
                    {
                        label: "甲方", name: "CustomerName", width: 240, align: "left"
                    },

                    {
                        label: "乙方", name: "YiName", width: 180, align: "left" 
                    },
                    {
                        label: "签订日期", name: "SignDate", width: 80, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "合同类型", name: "ContractTypeName", width: 100, align: "left" 
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 70, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        },
                    },
                    { label: "合同签订金额", name: "InitAmount", width: 100, align: "left" },
                    { label: "累计变更金额", name: "ChangeAmount", width: 100, align: "left" },
                    { label: "累计签证金额", name: "VisaAmount", width: 100, align: "left" },
                    { label: "合同总金额", name: "TotalAmount", width: 100, align: "left" },
                    { label: "累计结算金额", name: "SettlementAmount", width: 100, align: "left" },
                    { label: "累计收款金额", name: "ReceivedAmount", width: 100, align: "left" },
                    { label: "累计开票金额", name: "InvioceAmount", width: 100, align: "left" },
                    { label: "完工决算金额", name: "FinalAmount", width: 100, align: "left" },
                    { label: "约定付款比例(%)", name: "YdfkBiLi", width: 100, align: "left" },
                    { label: "约定应收账款", name: "YdysKuan", width: 100, align: "left" },
                    { label: "签约单位", name: "QyDanWei", width: 100, align: "left" },
                    {
                        label: "发票类型", name: "InvoiceTypeName", width: 100, align: "left" 
                    }
                ],
                mainId: 'ID',
                isPage: true
            });
            page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.ProjectId = projectId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectContract/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '工程合同',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/ProjectModule/ProjectContract/Form?keyValue=' + keyValue + '&viewState=' + viewState + '&projectId=' + projectId + "&formId=" + formId,
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
