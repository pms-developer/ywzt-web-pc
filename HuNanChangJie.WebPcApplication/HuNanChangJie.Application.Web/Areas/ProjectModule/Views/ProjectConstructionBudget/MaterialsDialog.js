﻿var projectId = request("projectId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
            
        },
        bind: function () {
           
            if (!!projectId) {
                $('#gridtable').jfGrid({
                    url: top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/GetMaterials?projectId=' + projectId,
                    headData: [
                        { label: "清单编号", name: "ListCode", width: 100, align: "left" },
                        { label: "材料编号", name: "Code", width: 100, align: "left" },
                        { label: "材料名称", name: "Name", width: 100, align: "left" },
                        { label: "品牌", name: "Brand", width: 100, align: "left" },
                        { label: "规格型号", name: "ModelNumber", width: 100, align: "left" },
                        { label: "单位", name: "Unit", width: 100, align: "left" },
                        { label: "预算数量", name: "Quantity", width: 100, align: "left" },
                        { label: "预算单价", name: "BudgetPrice", width: 100, align: "left" },
                        { label: "已申请数量", name: "ApplyQuantity", width: 100, align: "left" },
                        { label: "可申请数量", name: "AllowQuantity", width: 100, align: "left" },

                    ],
                    mainId: 'ID',
                    height: 362,
                    isPage:true,
                    isMultiselect:true
                });
                page.search();
            }
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何合同");
        }

    };
    page.init();
};