﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-03 20:43
 * 描  述：施工预算
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="Project_ConstructionBudget";
var processCommitUrl=top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var costAttriteColumnNames = [];
var costAttriteData;
var listToValueData = [];
var bootstrap = function ($, Minke) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Minke.newGuid();
                subGrid.push({ "tableName": 'Project_ConstructionBudgetDetails', "gridId": 'Project_ConstructionBudgetDetails' });
                subGrid.push({ "tableName": 'Project_ConstructionBudgetQuantities', "gridId": 'Project_ConstructionBudgetQuantities' });
                subGrid.push({ "tableName": 'Project_ConstructionBudgetMaterials', "gridId": 'Project_ConstructionBudgetMaterials' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            var contractData = Minke.httpGet(top.$.rootUrl + "/ProjectModule/ProjectContract/GetInitAmountTotals", { projectId: projectId });
            if (!$('#ContractPrice').val()) {
                $('#ContractPrice').val(contractData.data);
            }
            $("#calculate").on("click", function () {
                refreshGridData();
            });
            $('#BidUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"extend",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_ConstructionBudgetDetails').jfGrid({
                headData: [
                    { label: "科目编码", name: "Code", width: 100, align: "left" },
                    { label: "科目名称", name: "Name", width: 120, align: "left" },
                    { label: "成本属性", name: "CostAttributeName", width: 100, align: "left" },
                    {
                        label: '取值方式', name: 'ValueType', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'施工预算' 
                        ,edit:{
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                                var gridName = "Project_ConstructionBudgetDetails";
                                var rateCell;
                                var formulasCell;
                                var budgetAmountCell;
                                for (var _index in headData.allHeads) {
                                    var head = headData.allHeads[_index].data;
                                    if (head.name == "Rate") rateCell = head;
                                    if (head.name == "Formulas") formulasCell = head;
                                    if (head.name == "BudgetAmount") budgetAmountCell = head;
                                }
                                switch (row["ValueType"]) {
                                    case "xjhz":
                                        rateCell.edit.readonly=true;
                                        formulasCell.edit.readonly = true;
                                        budgetAmountCell.edit.readonly = true;
                                         
                                        setCellValue("Rate", row, index, gridName, "");
                                        setCellValue("Formulas", row, index, gridName, "");

                                        break;
                                    case "qdqs":
                                        rateCell.edit.readonly = true;
                                        formulasCell.edit.readonly = true;
                                        budgetAmountCell.edit.readonly = true;
                                        setCellValue("Rate", row, index, gridName, "");
                                        setCellValue("Formulas", row, index, gridName, "");

                                        getListValue(row, index, gridName);
                                        break;
                                    case "sdsr":
                                        rateCell.edit.readonly = true;
                                        formulasCell.edit.readonly = true;
                                        budgetAmountCell.edit.readonly = false;

                                        setCellValue("Rate", row, index, gridName, "");
                                        setCellValue("Formulas", row, index, gridName, "");
                                        setCellValue("BudgetAmount", row, index, gridName, "");
                                        break;
                                    case "gsqz":
                                        
                                        rateCell.edit.readonly = false;
                                        formulasCell.edit.readonly = false;
                                        budgetAmountCell.edit.readonly = true;;

                                        setCellValue("BudgetAmount", row, index, gridName, "");

                                        break;
                                }
                                lowerTotal(gridName);

                                if (row["BudgetAmount"].toString() == "") {
                                    $("[rownum='" + gridName + "_" + index + "'][colname='BudgetAmount']").html("0.00");
                                }
            
                            },
                            datatype: 'dataItem',
                            code:'BudgetValueType'
                        }
                    },
                    {
                        label: '费率%', name: 'Rate', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'施工预算' 
                        ,edit:{
                            type: 'input',
                            readonly:true,
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                
                                //var gridName = "Project_ConstructionBudgetDetails";
                                //lowerTotal(gridName);
                                mathExpress(row["Formulas"], "Project_ConstructionBudgetDetails", index, "BudgetAmount", row);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                mathExpress(row["Formulas"], "Project_ConstructionBudgetDetails", rowindex, "BudgetAmount", row);
                           }
                        }
                    },
                    {
                        label: '计算公式', name: 'Formulas', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'施工预算' 
                        ,edit:{
                            type: 'input',
                            readonly:true,
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                mathExpress(row["Formulas"], "Project_ConstructionBudgetDetails", rowindex, "BudgetAmount", row);
                                var gridName = "Project_ConstructionBudgetDetails";
                                lowerTotal(gridName);
                           }
                        }
                    },
                    {
                        label: '预算金额', name: 'BudgetAmount', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'float',isBlur:true, tabname:'施工预算' 
                        ,edit:{
                           type: 'input',
                           readonly:true,
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                               
                                var gridName = "Project_ConstructionBudgetDetails";
                                if (row["BudgetAmount"].toString() == "") {
                                    $("[rownum='" + gridName + "_" + index + "'][colname='BudgetAmount']").html("0.00");
                                }
                               lowerTotal(gridName);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {  
                           }
                        }
                    },
                    {
                        label: '占比%', name: 'Ratio', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'施工预算'                     },
                    {
                        label: '描述', name: 'Remark', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'施工预算' 
                        ,edit:{
                           type: 'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                ],
                bindTable:"Project_ConstructionBudgetDetails",
                isEdit: false,
                mainId: "BaseSubjectId",
                isTree: true,
                parentId: "ParentId",
                height: 300,
                onRowClick: function (rowindex, row, headData) {
                    var rateCell;
                    var formulasCell;
                    var budgetAmountCell;
                    for (var _index in headData) {
                        var head = headData[_index].data;
                        if (head.name == "Rate") rateCell = head;
                        if (head.name == "Formulas") formulasCell = head;
                        if (head.name == "BudgetAmount") budgetAmountCell = head;
                    }
                    switch (row["ValueType"]) {
                        case "xjhz":
                            rateCell.edit.readonly = true;
                            formulasCell.edit.readonly = true;
                            budgetAmountCell.edit.readonly = true;
                            break;
                        case "qdqs":
                            rateCell.edit.readonly = true;
                            formulasCell.edit.readonly = true;
                            budgetAmountCell.edit.readonly = true;
                            break;
                        case "sdsr":
                            rateCell.edit.readonly = true;
                            formulasCell.edit.readonly = true;
                            budgetAmountCell.edit.readonly = false;
                            break;
                        case "gsqz":
                            rateCell.edit.readonly = false;
                            formulasCell.edit.readonly = false;
                            budgetAmountCell.edit.readonly = true;
                            break;
                        default:
                            rateCell.edit.readonly = true;
                            formulasCell.edit.readonly = true;
                            budgetAmountCell.edit.readonly = true;
                            break;
                    }

                    $("div[colname='BudgetAmount']").unbind("DOMNodeInserted");
                    $("div[colname='BudgetAmount']").bind('DOMNodeInserted', function (e) {

                        setRatio("Project_ConstructionBudgetDetails");
                        var $this = $(this);
                        var value = parseInt($this.html() || 0);
                        if (value == 0) {
                            var rowindex = $(this).attr("rowindex");
                            var row = $("#Project_ConstructionBudgetDetails").jfGridGet("rowdatas")[rowindex];
                            setCellValue("Ratio", row, rowindex, "Project_ConstructionBudgetDetails", 0);
                        }
                    });
                },
                onAddRow: function(row,rows){
                     row["ID"]=Minke.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
           
            $('#Project_ConstructionBudgetQuantities').jfGrid({
                headData: getHead(),
                mainId:"ID",
                bindTable:"Project_ConstructionBudgetQuantities",
                isEdit: true,
                height: 300,
                onAddRow: function(row,rows){
                     row["ID"]=Minke.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                    row.EditType = 1;
                    ProjectId = projectId;
                 }
            });
           
            $('#Project_ConstructionBudgetMaterials').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'材料清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '材料编码', name: 'Code', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'材料清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '名称', name: 'Name', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'材料清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '品牌', name: 'Brand', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'材料清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'材料清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '计量单位', name: 'Unit', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'材料清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '数量', name: 'Quantity', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'float', tabname:'材料清单' ,inlineOperation:{to:[{"t1":"Quantity","t2":"BudgetPrice","to":"TotalPrice","type":"mul","toTargets":null}],isFirst:true}
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '投标预算单价', name: 'BudgetPrice', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'float', tabname:'材料清单' ,inlineOperation:{to:[{"t1":"Quantity","t2":"BudgetPrice","to":"TotalPrice","type":"mul","toTargets":null}],isFirst:false}
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '合计金额', name: 'TotalPrice', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'float', tabname:'材料清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'材料清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                ],
                mainId:"ID",
                bindTable:"Project_ConstructionBudgetMaterials",
                isEdit: true,
                height: 300,
                onAddRow: function(row,rows){
                     row["ID"]=Minke.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                    row.EditType = 1;
                    ProjectId = projectId;
                 }
            });
        },
        initData: function () {
            $("#Project_ConstructionBudgetDetails").jfGridSet('reload');
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
            else {
                Minke.httpAsyncGet(top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/GetBudgetInfo?projectId=' + projectId, function (data) {
                    $('#Project_ConstructionBudgetDetails').jfGridSet('refreshdata', data.data, 'edit');
                });
            }
            registDivChangeEvent();
        }
    };
    var getHead = function () {
        var costAttruibut = loadCostAttribute();
        if (costAttruibut == null) {
            var head = [
                {
                    label: '清单编号', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '项目特征', name: 'Feature', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '工程量', name: 'Quantities', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'float', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            var total = parseFloat(Minke.clearNumSeparator((row["Quantities"] || 0))) * parseFloat(Minke.clearNumSeparator((row["Price"] || 0)));
                            total = Minke.toDecimal(total);
                            setCellValue("TotalPrice", row, index, "Project_ConstructionBudgetQuantities", total);
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },

                {
                    label: '综合单价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'float', statistics: true, tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            var total = parseFloat(Minke.clearNumSeparator((row["Quantities"] || 0))) * parseFloat(Minke.clearNumSeparator((row["Price"] || 0)));
                            total = Minke.toDecimal(total);
                            setCellValue("TotalPrice", row, index, "Project_ConstructionBudgetQuantities", total);
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '合价', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'float', statistics: true, tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
            ];
            return head;
        }
        else {
            var head = [
                {
                    label: '清单编号', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '项目特征', name: 'Feature', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '工程量', name: 'Quantities', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'float', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            var total = parseFloat(Minke.clearNumSeparator((row["Quantities"] || 0))) * parseFloat(Minke.clearNumSeparator((row["Price"] || 0)));
                            total = Minke.toDecimal(total);
                            setCellValue("TotalPrice", row, index, "Project_ConstructionBudgetQuantities", total);
                            setFooterValue(rows, headData);
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                costAttruibut,
                {
                    label: '综合单价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'float', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            var total = parseFloat(Minke.clearNumSeparator((row["Quantities"] || 0))) * parseFloat(Minke.clearNumSeparator((row["Price"] || 0)));
                            total = Minke.toDecimal(total);
                            setCellValue("TotalPrice", row, index, "Project_ConstructionBudgetQuantities", total);
                            setFooterValue(rows, headData);
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '合价', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'float', statistics: true, tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
                {
                    label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '工程量清单'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
            ];
            return head;
        }
    };
    var loadCostAttribute = function () {
        var columns = Minke.httpGet(top.$.rootUrl + '/ProjectModule/CostAttribute/GetList').data;
        costAttriteData = columns;
        if (columns.length > 0) {
            var costAttribute = { label: "综合单价组成", field: "zhdjzc", align: "center", headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, children: [] };
            for (var _index in columns) {
                var item = columns[_index];
                if (item.IsEnable && item.IsDeploy) {
                    costAttriteColumnNames.push(item.ColumnName);
                    var row = {};
                    row.label = item.Name;
                    row.name = item.ColumnName;
                    row.headColor = "blue";
                    row.width = 100;
                    row.datatype = "float";
                    row.tabname = "材料清单";
                    row.statistics = true,
                        row.quantityCol = "Quantities",
                    row.edit = {
                        type: "input",
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            var price = 0;
                            for (var i = 0; i < costAttriteColumnNames.length; i++) {
                                var colName = costAttriteColumnNames[i];
                                price += parseFloat(Minke.clearNumSeparator((row[colName] || 0)));
                            }
                            var total = price * parseFloat(Minke.clearNumSeparator((row["Quantities"] || 0)));

                            price = Minke.toDecimal(price);
                            setCellValue("Price", row, index, "Project_ConstructionBudgetQuantities", price);
                            total = Minke.toDecimal(total);
                            setCellValue("TotalPrice", row, index, "Project_ConstructionBudgetQuantities", total);
                            setFooterValue(rows, headData);
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    };
                    costAttribute.children.push(row);
                }
            }
            if (costAttribute.children.length > 0)
                return costAttribute;
            else {
                
                return null;
            }

        }
         
    }
    var setFooterValue = function (rows,runparam) {
        var totalPrice = 0;
        for (var _index in rows) {
            var row = rows[_index];
            totalPrice += parseFloat(Minke.clearNumSeparator((row["TotalPrice"] || 0)));
        }
        runparam.op.running.statisticData["TotalPrice"] = totalPrice;
        $("div.jfGird-statistic-cell[name='TotalPrice']").html(Minke.toDecimal(totalPrice));
    };
    var setRatio = function (gridName) {
        var rows = $("#" + gridName).jfGridGet("rowdatas");
        var code ="02";
        var result = 0;
        for (var i in rows) {
            var row = rows[i];
            if (row.Code.substring(0, 2) == code) {
                if (row.ValueType != "xjhz") {
                    result +=eval(Minke.clearNumSeparator(row.BudgetAmount || 0));
                }
            }
        }
        if (result == 0) return;
        setMainPrice(result);

        for (var i = 0; i < rows.length;i++) {
            var row = rows[i];
            if (row.Code.substring(0, 2) == code) {
                if (row.ValueType != "xjhz") {
                    var value = parseFloat(Minke.clearNumSeparator(row.BudgetAmount || 0));
                    var rate = Minke.format45(eval(value / result * 100), 100);
                    setCellValue("Ratio", row, i, gridName, rate);

                }
            }
        }
    };
    var setMainPrice = function (total) {
        $("#TotalPrice").val(total);
        var contractPrice = parseFloat($("#ContractPrice").val() || 0);
        if (contractPrice > 0) {
            var sr = contractPrice - total;

            var ml = Minke.format45(eval(sr / contractPrice  * 100), 100);
            $("#GrossProfitRate").val(ml);
        }
    };

    //下级汇总
    var lowerTotal = function (gridName) {
        var rows = $("#" + gridName).jfGridGet("rowdatas");
        var needTotalList = [];
        var total = 0;
        for (var _index in rows) {
            var item = rows[_index];
            if (item.ValueType != "xjhz") {
                if (item.Code.substring(0, 2) == "02") {
                    total += Minke.clearNumSeparator(item.BudgetAmount || 0);
                }
                continue;
            }
            var param = {};
            param.Code = item.Code;
            param.Len = item.Code.length;
            needTotalList.push(param);
        }
        setMainPrice(total);

        if (needTotalList.length == 0) return;
        function sortLen(x, y) {
            return x.Len + y.Len;
        }
        
        needTotalList.sort(sortLen);
        
        for (var idx in needTotalList) {
            var currentCode = needTotalList[idx].Code;
            var codelength = currentCode.length;

            var result = 0;
            var index = 0;
            for (var _index=0; _index < rows.length; _index++) {
                var item = rows[_index];
                if (currentCode !== item.Code) {
                    if (currentCode == item.Code.substring(0, codelength)) {
                        if (item.ValueType != "xjhz") {
                            result += Minke.clearNumSeparator(item.BudgetAmount || 0);
                        }
                    }
                }
                else {
                    index = _index;
                }
            }
            result = Minke.toDecimal(result).numToString(Minke.sysGlobalSettings.pointGlobal);
            setCellValue("BudgetAmount", rows[index], index, gridName, result);
        }
        
    };
    //清单取数
    var getListValue = function (row,index,gridName) {
        if (costAttriteData) {
            var footerdata = $("#Project_ConstructionBudgetQuantities").jfGridGet("footerdata");
            if (footerdata) {
                var colName = "";
                for (var _index in costAttriteData) {
                    if (costAttriteData[_index].ID != row["CostAttributeId"]) continue;
                    colName = costAttriteData[_index].ColumnName;

                    var item = {};
                    item.row = row;
                    item.index = index;
                    item.costAttruibut = colName;
                    listToValueData.push(item);

                    break;
                }
                if (colName) {
                    var value = Minke.toDecimal((footerdata[colName] || 0));
                    setCellValue("BudgetAmount", row, index, gridName, value);
                }
                else {
                    setCellValue("BudgetAmount", row, index, gridName, "");
                }
            }
            else {
                setCellValue("BudgetAmount", row, index, gridName, "");
            }
        }
        else {
            setCellValue("BudgetAmount", row, index, gridName, "");
        }
    };
    //注册清单值改变事件
    var registDivChangeEvent = function () {
        
        for (var i = 0; i < costAttriteColumnNames.length; i++) {
            var colName = costAttriteColumnNames[i];
            $("div.jfGird-statistic-cell[name='" + colName + "']").bind("DOMNodeInserted", function () {
                for (var _index in listToValueData) {
                    var item = listToValueData[_index];
                    setCellValue("BudgetAmount", item.row, item.index, "Project_ConstructionBudgetDetails", $(this).html());
                }
            });
        }
    };
    //公式取值
    var mathExpress = function (expressStr, gridName, rowindex, toTarget, row) {
        if (!!!expressStr) return;
        var str = expressStr.replace(/\s+/g, "");
        if (!!str) {
            var rows = $("#" + gridName).jfGridGet("rowdatas");
            while (str.indexOf("[") > -1) {
                for (var _index in rows) {
                    var item = rows[_index];
                    var code = "[" + item.Code + "]";
                    if (str.indexOf(code) > -1) {
                        var value = Minke.clearNumSeparator(item.BudgetAmount || 0);
                        str = str.replace(code, value);
                    }
                }
            }
            if (str.indexOf("R") > -1) {
                var rate = row.Rate || 0;
                str = str.replace("R", rate/100);
            }
            if (str.indexOf("r") > -1) {
                var rate = row.Rate || 0;
                str = str.replace("r", rate/100);
            }
            var result = Minke.toDecimal(eval(str)).numToString(Minke.sysGlobalSettings.pointGlobal);
            setCellValue(toTarget, row, rowindex, gridName, result);
        }
    };
    var refreshGridData = function () {
        var gridName = 'Project_ConstructionBudgetDetails';
        var rows = $("#" + gridName).jfGridGet("rowdatas");
        var codes = ["01", "02"];
        for (var i = 0; i < codes.length; i++) {
            var total = 0;
            for (var j = 0; j < rows.length; j++) {
                var row = rows[j];
                if (row.ValueType == "gsqz") {
                    mathExpress(row.Formulas, gridName, j, "BudgetAmount", row);
                }
                if (row.ValueType == "qdqs") {
                    getListValue(row, j, gridName)
                }
            }
        }
        lowerTotal(gridName);
    }
    var setCellValue = function (cellName, row, index, gridName,value) {
        var $cell = $("[rownum='" + gridName+"_" + index + "'][colname='"+cellName+"']");
        var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
        row[cellName] = value;
        row.EditType = 2;
        $cell.html(value);
        $edit.val(value);
    };
    // 保存数据
    acceptClick = function (callBack) {
        refreshGridData();
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Minke.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_ConstructionBudget"]').mkGetFormData());

    postData.strproject_ConstructionBudgetDetailsList = JSON.stringify($('#Project_ConstructionBudgetDetails').jfGridGet('rowdatas'));
    var quantitiesData = $('#Project_ConstructionBudgetQuantities').jfGridGet('rowdatas');
    if (costAttriteColumnNames.length > 0) {
        for (var _index in quantitiesData) {
            var row = quantitiesData[_index];
            var costInfos = [];
            for (var i = 0; i < costAttriteColumnNames.length; i++) {
                var costItem = {};
                costItem.ColumnName = costAttriteColumnNames[i];
                costItem.Value = row[costItem.ColumnName];
                costInfos.push(costItem);
            }
            row.CostInfos = costInfos;
        }
    }
    postData.strproject_ConstructionBudgetQuantitiesList = JSON.stringify(quantitiesData);
    postData.strproject_ConstructionBudgetMaterialsList = JSON.stringify($('#Project_ConstructionBudgetMaterials').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
