﻿

var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");
var acceptClick;

var bootstrap = function ($, Changjie) {
    "use strict";
    var typeId = "";

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {

            var param = {};
            // 分类
            $('#type_tree').mktree({
                nodeClick: function (item) {
                    typeId = item.id;
                    param.ProjectId = projectId;
                    param.TypeId = typeId;
                    page.search(param);

                }
            });

            if (!!projectId) {
                $('#gridtable').jfGrid({
                    url: top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/GetMaterialsInfoList',
                    headData: [
                        { label: "材料编码", name: "Code", width: 100, align: "left" },
                        { label: "材料名称", name: "Name", width: 100, align: "left" },
                        { label: "规格型号", name: "Model", width: 100, align: "left" },
                        {
                            label: "品牌", name: "BrandId", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('dataItem', {
                                    key: value,
                                    code: 'Brand',
                                    callback: function (_data) {
                                        callback(_data.text);
                                    }
                                });
                            }
                        },
                        {
                            label: "产地", name: "Origin", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('dataItem', {
                                    key: value,
                                    code: 'PlaceOfOrigin',
                                    callback: function (_data) {
                                        callback(_data.text);
                                    }
                                });
                            }
                        },
                        {
                            label: "计量单位", name: "UnitId", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('dataItem', {
                                    key: value,
                                    code: 'MeasuringUnit',
                                    callback: function (_data) {
                                        callback(_data.text);
                                    }
                                });
                            }
                        },
                        {
                            label: "项目", name: "ProjectId", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('sourceData', {
                                    code: 'BASE_XMLB',
                                    key: value,
                                    keyId: 'project_id',
                                    callback: function (_data) {
                                        callback(_data['projectname']);
                                    }
                                });
                            }
                        },
                        { label: "采购价", name: "Price", width: 100, align: "left" },
                        { label: "预算成本价", name: "BugetCost", width: 100, align: "left" },
                        {
                            label: "成本科目", name: "SubjectCost", width: 100, align: "left",
                            formatterAsync: function (callback, value, row, op, $cell) {
                                Changjie.clientdata.getAsync('dataItem', {
                                    key: value,
                                    code: 'SubjectCost',
                                    callback: function (_data) {
                                        callback(_data.text);
                                    }
                                });
                            }
                        },

                    ],
                    //mainId: 'ID',
                    //height: 292,
                    //isPage:true,
                    //isMultiselect:true
                    isPage: true,
                    mainId: "ID",
                    height: 360,
                    height: 290,
                    isMultiselect: true,
                    param: { queryJson: JSON.stringify(param) }

                });
            }

            //分类
            $('#type_tree').mktreeSet('refresh', {
                url: top.$.rootUrl + '/SystemModule/BaseMaterialsType/GetTreeList',
            });

            // 滚动条
            $('#user_list_warp').mkscroll();

        },
        initData: function () {
            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                param.ProjectId = projectId;
                param.TypeId = typeId;

                page.search(param);
            });
        },

        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    page.init();
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();

};