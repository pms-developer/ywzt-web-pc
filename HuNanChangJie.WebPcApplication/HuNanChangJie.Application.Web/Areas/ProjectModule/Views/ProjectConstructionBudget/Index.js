﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-03 20:43
 * 描  述：施工预算
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增施工预算',
                    url: top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/Form?projectId=' + projectId,
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/GetPageList',
                headData: [
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left"},
                    { label: "项目编码", name: "CompanyCode", width: 100, align: "left"},
                    { label: "施工预算", name: "TotalPrice", width: 100, align: "left"},
                    { label: "毛利率", name: "GrossProfitRate", width: 100, align: "left"},
                    { label: "税金", name: "Taxes", width: 100, align: "left"},
                    { label: "投标负责人", name: "BidUserId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "工程合同金额", name: "SortCode", width: 100, align: "left"},
                    { label: "备注", name: "Remark", width: 100, align: "left"},
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                    formatter: function (cellvalue, row) {
                        return top.Changjie.tranAuditStatus(cellvalue);
                        }},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.ProjectID = projectId;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '施工预算',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/ProjectConstructionBudget/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&projectId=" + projectId + "&formId=" + formId, 
                        width: 1000,
                        height: 800,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
