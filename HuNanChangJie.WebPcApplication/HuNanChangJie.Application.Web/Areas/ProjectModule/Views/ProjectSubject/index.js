﻿var projectId = request("projectId");
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
            page.initGrid();
            page.search();
        },
        bind: function () {
            $("#refresh").on("click", function () {
                refreshGridData();
            });

            $("#add").on("click", function () {
                Changjie.layerForm({
                    id: "addProjectSubjectForm",
                    title: '新增成本科目',
                    url: top.$.rootUrl + '/ProjectModule/Subject/SubjectDialog',
                    width: 300,
                    height: 380,
                    callBack: function (id) {
                        return top[id].acceptClick(insertData);
                    }
                });
            });

            $("#delete").on("click", function () {
                var BaseSubjectId = $("#gridtable").jfGridValue("BaseSubjectId");
                if (!!BaseSubjectId) {
                    var name = $("#gridtable").jfGridValue("Name");
                    Changjie.layerConfirm("是否删除【" + name + "】科目?", function (res) {
                        if (res) {
                            var url = top.$.rootUrl + '/ProjectModule/ProjectSubject/Delete';
                            var param = {
                                BaseSubjectId: BaseSubjectId,
                                projectId: projectId
                            };
                            var data = top.Changjie.httpGet(url, param);
                            refreshGridData();
                        }
                    });
                }
            });
        },
        initGrid: function () {
            $("#gridtable").mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectSubject/GetList?projectId=' + projectId,
                headData: [
                    { label: "科目编码", name: "Code", width: 100, align: "left" },
                    { label: "科目名称", name: "Name", width: 200, align: "left" },
                    { label: "成本属性", name: "CostAttributeName", width: 200, align: "left" },
                    {
                        label: "启用状态", name: "IsEnable", width: 100, align: "left",
                        formatter: function (value) {
                            if (value) {
                                return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">已禁用</span>';
                            }
                        }
                    },
                    {
                        label: "说明", name: "Remark", width: 200, align: "left",
                        formatter: function (value, row) {
                            if (row.IsSystem) {
                                return "系统科目,不可修改或删除";
                            }
                            return value;
                        }
                    }
                ],
                mainId: "BaseSubjectId",
                isTree: true,
                parentId: "ParentId"
            });
        },
        search: function (param) {
            param = param || {};
            $("#gridtable").jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };

    var insertData = function (data) {
        var ids = "";
        for (var item in data) {
            ids += data[item].id + ",";
        }
        ids = ids.substring(0, ids.length - 1);
        var postData = {
            projectId: projectId,
            subjectIds: ids
        };
        var url = top.$.rootUrl + "/ProjectModule/ProjectSubject/Insert";
        $.mkSaveForm(url, postData, function (res) {
            refreshGridData();
        });
    }
    var refreshGridData = function () {
        page.search();
    };
    page.init();
};