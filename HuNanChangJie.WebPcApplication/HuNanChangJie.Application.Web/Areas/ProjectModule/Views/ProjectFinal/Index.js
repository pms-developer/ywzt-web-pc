﻿/* * Copyright (c) 2013-2019  
 * 创建人：超级管理员 
 * 日  期：2020-03-24 10:58 
 * 描  述：完成决算 
 */
var refreshGirdData;
var projectId = request('projectId');
var auditPassEvent;
var unauditPassEvent;
var formId = request("formId");
var queryJson = { "ProjectId": projectId};
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新 
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增 
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增完成决算',
                    url: top.$.rootUrl + '/ProjectModule/ProjectFinal/Form?projectId=' + projectId,
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表 
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectFinal/GetPageList?queryJson=' + JSON.stringify(queryJson),
                headData: [
                    { label: "结算单号", name: "Code", width: 100, align: "left" },
                    { label: "合同", name: "ContractFullName", width: 200, align: "left" },
                    { label: "初始合同金额", name: "InitAmount", width: 100, align: "left" },
                    { label: "当前合同金额", name: "TotalAmount", width: 100, align: "left" },
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left" },
                    { label: "产值结算金额", name: "SettlementAmount", width: 100, align: "left" },
                    {
                        label: "决算时间", name: "FinalDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "完工决算金额", name: "FinalAmount", width: 100, align: "left" },
                    {
                        label: "经办部门", name: "OperatorDepartmentId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('department', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "经办人", name: "OperatorId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };

    auditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectFinal/Audit', { keyValue: keyValue }, function (data) {
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectFinal/UnAudit', { keyValue: keyValue }, function (data) {
        });
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除 
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectFinal/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑 
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '完成决算',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/ProjectModule/ProjectFinal/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&projectId=" + projectId + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
}; 
