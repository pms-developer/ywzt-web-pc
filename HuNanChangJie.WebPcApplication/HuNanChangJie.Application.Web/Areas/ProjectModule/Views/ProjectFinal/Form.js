﻿/* *   
 * 创建人：超级管理员 
 * 日  期：2020-03-24 10:58 
 * 描  述：完成决算 
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_Final";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectFinal/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_ContractQuantities', "gridId": 'Project_ContractQuantities' });
                subGrid.push({ "tableName": 'Project_ContractMaterials', "gridId": 'Project_ContractMaterials' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $("#ProjectContractId").mkselect({
                url: top.$.rootUrl + "/ProjectModule/ProjectContract/GetWaitFinalList?projectId=" + projectId,
                value: "ID",
                text: "Name",
                title:"Name"
            });
            $("#ProjectContractId").on("change", function () {
                var data = $(this).mkselectGetEx();
                if (data) {
                    $("#InitAmount").val(data.InitAmount||0);
                    $("#TotalAmount").val(data.TotalAmount||0);
                    $("#SettlementAmount").val(data.SettlementAmount || 0);

                    var data = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectContract/GetDetails?contractId=" + data.ID).data;
                    var quantities = data.quantities;
                    if (quantities) {
                        var total = 0;
                        for (var i = 0; i < quantities.length; i++) {
                            var row = quantities[i];
                            row.EditType = 2;
                            row.FinalTotalPrice = Changjie.getFloatValue(row.TotalPrice) + Changjie.getFloatValue(row.ChangeTotalPrice) + Changjie.getFloatValue(row.VisaTotalPrice)
                            row.FinalQuantities = Changjie.getFloatValue(row.Quantities) + Changjie.getFloatValue(row.ChangeQuantities) + Changjie.getFloatValue(row.VisaQuantities)
                            total += row.FinalTotalPrice;
                            row.FinalPrice = eval(row.FinalTotalPrice / row.FinalQuantities);
                        }
                        $("#FinalAmount").val(total);
                    }
                    $("#Project_ContractQuantities").jfGridSet('refreshdata', quantities);
                    var materials = data.materials;
                    if (materials) {
                        for (var i = 0; i < materials.length; i++) {
                            var row = materials[i];
                            row.EditType = 2;
                            row.FinalQuantity = Changjie.getFloatValue(row.Quantity) + Changjie.getFloatValue(row.ChangeQuantity) + Changjie.getFloatValue(row.VisaQuantity);
                            row.FinalTotalPrice = Changjie.getFloatValue(row.TotalPrice) + Changjie.getFloatValue(row.ChangeTotalPrice) + Changjie.getFloatValue(row.VisaTotalPrice);

                            row.FinalPrice = eval(row.FinalTotalPrice / row.FinalQuantity);
                        }
                    }
                    $("#Project_ContractMaterials").jfGridSet('refreshdata', materials);
                    
                }
                else {
                    $("#InitAmount").val("");
                    $("#TotalAmount").val("");
                    $("#SettlementAmount").val("");
                }
            });
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'ProjectFinalCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            }).mkselectSet(loginInfo.departmentId);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_ContractQuantities').jfGrid({
                headData: [
                    {
                        label: '清单编号', name: 'Code', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '清单名称', name: 'Name', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '项目特征', name: 'Feature', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '初始合同', name: 'ld796405a68de45129b4a85b85e08fa82',align:"center", width: 100
                        , children: [
                            {
                                label: '单价', name: 'Price', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '工程量', name: 'Quantities', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '合价', name: 'TotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '合同变更', name: 'lfce5d84da0df49dbb2579d8a72aed462', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'ChangePrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '工程量', name: 'ChangeQuantities', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '合价', name: 'ChangeTotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '合同签证', name: 'lcbd0180814df4b5bb0a08d08875dfedd', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'VisaPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '工程量', name: 'VisaQuantities', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '合价', name: 'VisaTotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '产值结算', name: 'l3cf4e300c33742b3b7c09ccb478af2f4', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'SettlementPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '工程量', name: 'SettlementQuantities', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '合价', name: 'SettlementTotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '完工决算', name: 'l56957dfa43dc474fb19e8b2819ca5848', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'FinalPrice', headColor: "blue", width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "FinalPrice", "t2": "FinalQuantities", "to": "FinalTotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '工程量', name: 'FinalQuantities', headColor: "blue", width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "FinalPrice", "t2": "FinalQuantities", "to": "FinalTotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '合价', name: 'FinalTotalPrice', width: 100, aggtype: 'sum', aggcolumn: 'FinalAmount', required: '0',
                                datatype: 'float', tabname: '工程量清单'
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                        ]
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                ],
                mainId: "ID",
                bindTable: "Project_ContractQuantities",
                isEdit: false,
                height: 300
            });
            $('#Project_ContractMaterials').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '材料编码', name: 'Code', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '名称', name: 'Name', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '初始合同', name: 'lfa314530d12d4024923e9379a927e683', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'Price', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '数量', name: 'Quantity', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '合计金额', name: 'TotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                        ]
                    }, 
                    {
                        label: '合同变更', name: 'la87a64c4fd8541d5aa28bbd59f205f34', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'ChangePrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '数量', name: 'ChangeQuantity', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '合计金额', name: 'ChangeTotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                        ]
                    },
                    {
                        label: '合同签证', name: 'ld6da9e29ac464cdd802493d0b983b7bc', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'VisaPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '数量', name: 'VisaQuantity', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '合计金额', name: 'VisaTotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                        ]
                    },
                    {
                        label: '产值结算', name: 'l63c26f2603264ff38dd862c2cb88fe05', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'SettlementPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '数量', name: 'SettlementQuantities', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '合计金额', name: 'SettlementTotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                        ]
                    },
                    {
                        label: '完工决算', name: 'l4c74e7c47d4f48cfaa1772c9907aebdb', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'FinalPrice', width: 100,headColor:"blue", aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "FinalPrice", "t2": "FinalQuantity", "to": "FinalTotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '数量', name: 'FinalQuantity', headColor: "blue", width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "FinalPrice", "t2": "FinalQuantity", "to": "FinalTotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '合计金额', name: 'FinalTotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单'
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                        ]
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                ],
                mainId: "ID",
                bindTable: "Project_ContractMaterials",
                height: 300
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectFinal/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据 
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectFinal/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调 
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_Final"]').mkGetFormData());
    postData.strproject_ContractQuantitiesList = JSON.stringify($('#Project_ContractQuantities').jfGridGet('rowdatas'));
    postData.strproject_ContractMaterialsList = JSON.stringify($('#Project_ContractMaterials').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
} 
