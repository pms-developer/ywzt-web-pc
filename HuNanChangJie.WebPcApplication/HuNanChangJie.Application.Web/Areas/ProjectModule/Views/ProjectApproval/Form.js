﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-25 19:58
 * 描  述：项目立项
 */
var acceptClick;
var keyValue = request('keyValue');
var formId = request("formId");
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_Approval";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectApproval/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var pageType = "form";
var auditStatus = "";
var auditPassEvent;
var unauditPassEvent;
var refreshGirdData;
var workflowId = "";
var valuetype = 1;
var typeList = [];


var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            valuetype = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectApproval/GetValueType").data;
            switch (valuetype) {
                case 1:
                    typeList.disType = "layer";
                    typeList.listFieldInfo = [
                        { label: "所在部门", name: "f_fullname", width: 100, align: "left" },
                        { label: "人员姓名", name: "f_realname", width: 100, align: "left" },
                    ]
                    typeList.dataSourceName = "User";
                    break;
                case 2:
                    typeList.disType = "layer";
                    typeList.listFieldInfo = [
                        { label: "证照名称", name: "zzmc_name", width: 100, align: "left" },
                        { label: "人员姓名", name: "zz_xinming", width: 100, align: "left" },
                        { label: "证照编号", name: "zz_bianhao", width: 150, align: "left" },
                    ]
                    typeList.dataSourceName = "ZhengZhao";
                    break;
                case 3:
                    typeList.disType = "input";
                    break;
                default:
            }

            Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectApproval/GetKeyValue", { projectId: projectId }, function (res) {
                if (!!res.data) {
                    auditStatus = res.data.AuditStatus;
                    keyValue = res.data.ID;
                    workflowId = res.data.Workflow_ID;
                    type = "edit";
                    $("#edit").show();
                    $("#edit").unbind("click");
                    $("#edit").on("click", function () {
                        page.setViewState(false);
                        $("#audit").hide();
                        $("#unaudit").hide();
                        $("#send").hide();
                        $(this).hide();
                        $("#save").show();
                    });
                    page.setViewState(true);
                    $("#save").hide();

                    if (auditStatus == "2") {
                        $("#audit").hide();
                        $("#unaudit").show();
                        $("#send").hide();
                        $("#edit").hide();
                        $("#save").hide();
                    }
                }
                else {
                    keyValue = "";
                    type = "add";
                    $("#edit").hide();
                    $("#audit").hide();
                    $("#unaudit").hide();
                    $("#send").hide();
                    $("#save").show();
                    page.setViewState(false);

                }
            });
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_ApprovalMembers', "gridId": 'Project_ApprovalMembers' });
                type = "add";
            }
            $("#save").on("click", function () {
                var postData = getFormData();
                if (postData == false) return false;
                if (type == "add") {
                    keyValue = mainId;
                }
                $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectApproval/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                    // 保存成功后才回调
                    if (res.code == Changjie.httpCode.success) {
                        type = "edit";
                        $("#edit").show();
                        $("#audit").show();
                        $("#unaudit").show();
                        $("#send").show();
                        $("#save").hide();
                        page.setViewState(true);
                    }
                });
            });
            $('.mk-layout-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        setViewState: function (flag) {
            if (flag) {
                $("[viewState='readOnly']").attr("readonly", "readonly");
                $("[viewState='readOnly']").addClass("readonly");
                $("#btn_uploadfile").attr("disabled", "disabled");
                $("#btn_delfile").attr("disabled", "disabled");

            }
            else {
                $("[viewState='readOnly']").removeAttr("readonly");
                $("[viewState='readOnly']").removeClass("readonly");
                $("#btn_uploadfile").removeAttr("disabled");
                $("#btn_delfile").removeAttr("disabled");
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectContract/GetInitAmountTotals', { projectId: projectId }, function (data) {
                if (!$('#ContractAmount').val()) {
                    $('#ContractAmount').val(data);
                }
            });
            $("#PreStartDate").on("input change", function () {
                var result = page.getDays($("#PreStartDate").val(), $("#PreEndDate").val());
                result = result || 0;
                $("#PreTotalPeriod").val(result);
            });
            $("#PreEndDate").on("input change", function () {
                var result = page.getDays($("#PreStartDate").val(), $("#PreEndDate").val());
                result = result || 0;
                $("#PreTotalPeriod").val(result);
            });
            $('#Company_ID').mkCompanySelect({}).mkselectSet($("#companyid").val());
            $('#ProjectManagerId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet($("#xmjl").val());

            $('#BusinessId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet($("#yewury").val());
            
            $('#ConstructionDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            });
            $('#QualityRequirements').mkDataItemSelect({ code: 'zlmb' });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            $('#Project_ApprovalMembers').jfGrid({
                headData: page.setGridHeadDate(),
                mainId: "ID",
                bindTable: "Project_ApprovalMembers",
                isEdit: true,
                height: 290,
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                    row.ProjectId = projectId;
                    row.ValueType = valuetype;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        setGridHeadDate: function () {
            var head = [
                {
                    label: '项目角色', name: 'ProjectRole', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '项目组成员'
                    , edit: {
                        type: 'select',
                        change: function (row, index, item, oldValue, colname, headData) {
                        },
                        datatype: 'dataItem',
                        code: 'ProjectRole'
                    }
                },
                {
                    label: '项目组成员', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '项目组成员'
                    , edit: {
                        type: typeList.disType,
                        change: function (data, rownum, selectData) {
                            if (valuetype == 1) {
                                data.NameId = selectData.f_userid;
                                data.Name = selectData.f_realname;
                            }
                            else if (valuetype == 2) {
                                data.NameId = selectData.zhengzhaoid;
                                data.Name = selectData.zz_xinming;
                                data.zz_bianhao = selectData.zz_bianhao;
                                data.zzmc_name = selectData.zzmc_name;
                            }
                            $('#Project_ApprovalMembers').jfGridSet('updateRow', rownum);
                        },
                        op: {
                            width: 500,
                            height: 300,
                            listFieldInfo: typeList.listFieldInfo,
                            url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                            param: { code: typeList.dataSourceName }
                        }
                    }
                },
                {
                    label: '开始任职时间', name: 'StartDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '项目组成员'
                    , edit: {
                        type: 'datatime',
                        change: function (row, index, oldValue, colname, headData) {
                        },
                        dateformat: '0'
                    }
                },
                {
                    label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    datatype: 'string', tabname: '项目组成员'
                    , edit: {
                        type: 'input',
                        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                        },
                        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                        }
                    }
                },
            ];
            if (valuetype == 2) {
                var index = 2;
                for (var item in typeList.listFieldInfo) {
                    var info = typeList.listFieldInfo[item];
                    if (info.name == "zz_xinming") continue;
                    head.splice(index, 0, typeList.listFieldInfo[item]);
                    index++;
                }
                
            }
            return head;
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectApproval/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                    page.loadAsyncData();
                });
            }
        },
        loadAsyncData: function () {
            if (valuetype == 2) {
                var rows = $('#Project_ApprovalMembers').jfGridGet("rowdatas");
                var data = Changjie.httpGet(top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: typeList.dataSourceName });
                for (var i in rows) {
                    var row = rows[i];
                    var id = row.NameId;
                    for (var a in data.data) {
                        var drow = data.data[a];
                        if (drow.zhengzhaoid == id) {
                            row.zzmc_name = drow.zzmc_name;
                            setCellValue("zzmc_name", i, "Project_ApprovalMembers", drow.zzmc_name);

                            row.zz_bianhao = drow.zz_bianhao;
                            setCellValue("zz_bianhao", i, "Project_ApprovalMembers", drow.zz_bianhao);
                        }
                    }
                }
            }
        },
        getDays: function (s1, s2) {
            var d1 = new Date(s2.replace(/-/g, '/'));
            var d2 = new Date(s1.replace(/-/g, '/'));
            var ms = Math.abs(d2.getTime() - d1.getTime());//毫秒
            var d = ms / 1000 / 60 / 60 / 24;//转为天
            return d;
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectApproval/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();

    auditPassEvent = function () {
        $("#edit").hide();
        $("#audit").hide();
        $("#unaudit").show();
        $("#send").hide();
        $("#save").hide();
    };
    unauditPassEvent = function () {
        $("#edit").show();
        $("#audit").show();
        $("#unaudit").hide();
        $("#send").show();
        $("#save").hide();
    };
    refreshGirdData = function () {
        page.init();
    };
}

var setCellValue = function (cellName, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    $cell.html(value);
    $edit.val(value);
};

var getFormData = function () {

    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_Approval"]').mkGetFormData());
    postData.strproject_ApprovalMembersList = JSON.stringify($('#Project_ApprovalMembers').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
