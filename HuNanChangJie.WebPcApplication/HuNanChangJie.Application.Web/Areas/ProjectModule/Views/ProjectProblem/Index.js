﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-28 19:56
 * 描  述：问题反馈
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");
var moduleId = request("moduleId");
var keyValue;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });

            $("#close").on("click", function () {
                if (!!keyValue) {
                    Changjie.httpPost(top.$.rootUrl + "/ProjectModule/ProjectProblem/Close", { keyValue: keyValue }, function (data) {
                        if (data.code == Changjie.httpCode.success) {
                            refreshGirdData();
                        }
                    });
                }
                else {
                    Changjie.alert.warning("请选择需要操作的行!");
                }
            });

            $("#reply").on("click", function () {
                if (!!keyValue) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '回复问题反馈',
                        url:top.$.rootUrl + '/ProjectModule/ProjectProblem/Form?keyValue=' + keyValue + '&viewState=1&projectId=' + projectId + '&moduleId=' + moduleId+"&isReply=1",
                        width: 850,
                        height: 700,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
                else {
                    Changjie.alert.warning("请选择需要操作的行!");
                }
            });
            $("#view").on("click", function () {
                if (!!keyValue) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '查看问题反馈',
                        isShowConfirmBtn:false,
                        url: top.$.rootUrl + '/ProjectModule/ProjectProblem/Form?keyValue=' + keyValue + '&viewState=1&projectId=' + projectId + '&moduleId=' + moduleId + "&isReply=1",
                        width: 850,
                        height: 700,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
                else {
                    Changjie.alert.warning("请选择需要操作的行!");
                }
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增问题反馈',
                    url: top.$.rootUrl + '/ProjectModule/ProjectProblem/Form?projectId=' + projectId + '&moduleId=' + moduleId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectProblem/GetPageList',
                headData: [
                    { label: "问题编号", name: "Code", width: 100, align: "left" },
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left" },
                    { label: "标题", name: "Title", width: 100, align: "left" },
                    { label: "创建者", name: "CreationName", width: 100, align: "left" },
                    { label: "填报时间", name: "CreationDate", width: 100, align: "left" },
                    { label: "问题类型", name: "Type", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ProblemType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "问题等级", name: "Level", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ProblemLevel',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    {
                        label: "主送人", name: "SendTo", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "抄送人", name: "CopytTos", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            if (!!value) { 
                                if (value.indexOf(",")>-1) {
                                    var userList = value.split(",");
                                    var names = "";
                                    for (var item in userList) {
                                        Changjie.clientdata.getAsync('user', {
                                            key: userList[item],
                                            callback: function (_data) {
                                                names += _data.name+",";
                                                
                                            }
                                        });
                                    }
                                   
                                    if (names.substring(names.length - 1, names.length) == ",") {
                                        names = names.substring(0, names.length - 1);
                                    }
                                    callback(names);
                                }
                                else {
                                    Changjie.clientdata.getAsync('user', {
                                        key: value,
                                        callback: function (_data) {
                                            callback(_data.name);
                                        }
                                    });
                                }
                            }
                        }
                    },
                    {
                        label: "回复状态", name: "IsReply", width: 100, align: "left",
                        formatter: function (cellValue) {
                            var value = cellValue || "";
                            if (value.toString() == "true") {
                                return '<span class="label label-success" style="cursor: pointer;">已回复</span>';
                            }
                            return '<span class="label label-default" style="cursor: pointer;">未回复</span>';
                        }
                    },
                    {
                        label: "回复者", name: "ReplyMan", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    { label: "回复时间", name: "ReplyDate", width: 100, align: "left" },
                    {
                        label: "关闭状态", name: "IsClose", width: 100, align: "left",
                        formatter: function (cellValue) {
                            var value = cellValue || "";
                            if (value.toString() == "true"){
                                return '<span class="label label-success" style="cursor: pointer;">已关闭</span>';
                            }
                            return '<span class="label label-default" style="cursor: pointer;">未关闭</span>';
                        }
                    }
                    
                ],
                mainId:'ID',
                isPage: true,
                onSelectRow: function (row) {
                    keyValue = row["ID"];
                    var isClose = row["IsClose"] || "";
                    if (!!row["IsReply"]) {
                        $("#edit").hide();
                    }
                    else {
                        $("#edit").show();
                    }
                    if (isClose.toString() == "true") {
                        $("#close").hide();
                        $("#reply").hide();
                        $("#edit").hide();
                        $("#delete").hide();
                    }
                    else {
                        $("#close").show();
                        $("#reply").show();
                    }
                }
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            param.ProjectID = projectId;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectProblem/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title +'问题反馈',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/ProjectProblem/Form?keyValue=' + keyValue + '&viewState=' + viewState + '&projectId=' + projectId + '&moduleId=' + moduleId + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
