﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-28 19:56
 * 描  述：问题反馈
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="Project_Problem";
var processCommitUrl=top.$.rootUrl + '/ProjectModule/ProjectProblem/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var moduleId = request("moduleId");
var isReply = request("isReply");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if(!!keyValue){
                mainId=keyValue;
                type = "edit";
                $("[reply]").each(function () {
                    $(this).hide();
                });
            }
            else{
               mainId=top.Changjie.newGuid();
                type = "add";
                $("[reply]").each(function () {
                    $(this).hide();
                });
            }
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
                $(".btn-success").each(function () {
                    $(this).hide();
                });
            }
            if (isReply == "1") {
                $("[reply]").each(function () {
                    $(this).show();
                });
                $("[problem]").each(function () {
                    $(this).show();
                });
                $(".btn-success").each(function () {
                    $(this).show();
                });
            }

            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();

        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'ProjectProblemCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#Type').mkDataItemSelect({ code: 'ProblemType' });
            $('#Level').mkDataItemSelect({ code: 'ProblemLevel' });
            $('#SendTo').mkUserSelect(0);
            $('#CopytTos').mkUserSelect(1);
            $('#ProblemFiles').mkUploader({
                moduleType: "Project",
                moduleId: moduleId,
                moduleInfoId: keyValue
            });
            $('#ReplyFiles').mkUploader({
                moduleType: "Project",
                moduleId: moduleId,
                moduleInfoId: keyValue
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectProblem/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;
        if (isReply == "1") {
            type="reply"
        }
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectProblem/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
