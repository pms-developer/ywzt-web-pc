﻿var acceptClick;
var projectId = request("projectId");
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            if (projectId) {
                page.bind();
            }
        },
        bind: function () {

            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectDispatchedWage/GetDetails',
                headData: [
                    { label: "姓名", name: "UserName", width: 100, align: "left" },
                    {
                        label: "开始时间", name: "StratDate", width: 100, align: "left", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "结束时间", name: "EndDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true,
                height: 280,
                isMultiselect: true
            });
            page.search();

        },
        search: function (param) {
            param = param || {};
            if (projectId) {
                param.projectId = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};