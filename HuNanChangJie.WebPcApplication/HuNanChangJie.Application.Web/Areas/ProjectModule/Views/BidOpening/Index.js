﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-18 12:19
 * 描  述：开标登记
 */
var projectId = request("projectId");
var formId = request("formId");
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增开标登记',
                    url: top.$.rootUrl + '/ProjectModule/BidOpening/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
          
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/BidOpening/GetPageList',
                headData: [
                    { label: "项目名称", name: "ProjectName", width: 150, align: "left" },
                    { label: "投标日期", name: "BidDate", width: 100, align: "left"},
                    { label: "投标地点", name: "Address", width: 100, align: "left"},
                    { label: "开标日期", name: "OpenDate", width: 100, align: "left"},
                    { label: "中标日期", name: "WinBidDate", width: 70, align: "left"},
                    { label: "是否中标", name: "IsWinBid", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            var itemKey = value == true ? "1" : "0";
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: itemKey,
                                 code: 'SZSF',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "中标价格", name: "WinBidPrice", width: 90, align: "left"},
                    { label: "预计成本", name: "PredictCost", width: 100, align: "left"},
                    { label: "毛利率", name: "GrossProfitRate", width: 90, align: "left"},
                    { label: "经办人", name: "OperationId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "经办部门", name: "OperationDeparmentId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('department', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "备注", name: "Remark", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.projectId = projectId;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/BidOpening/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '开标登记',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/BidOpening/Form?keyValue=' + keyValue + '&viewState=' + viewState + '&projectId=' + projectId + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
