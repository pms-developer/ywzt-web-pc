﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-18 12:19
 * 描  述：开标登记
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="Project_BidOpening";
var processCommitUrl=top.$.rootUrl + '/ProjectModule/BidOpening/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'Project_BidOpeningDetails',"gridId":'Project_BidOpeningDetails'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#IsWinBid').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });

            var loginInfo = Changjie.clientdata.get(['userinfo']);

            $('#OperationId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            $('#OperationDeparmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {} 
            }).mkselectSet(loginInfo.departmentId);

            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"extend",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_BidOpeningDetails').jfGrid({
                headData: [
                    {
                        label: '参与公司', name: 'CompanyName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'string', tabname:'竞争对手' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '报价', name: 'Offer', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'float', tabname:'竞争对手' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '商务得分', name: 'Business', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype: 'float', tabname: '竞争对手', multiColumnSum: { info: [{ "columns": [{ "column": "Business" }, { "column": "QingBiao" }, { "column": "Technology" }], "type": "add", "to": "Synthesize" }] } 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '技术得分', name: 'Technology', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype: 'float', tabname: '竞争对手', multiColumnSum: { info: [{ "columns": [{ "column": "Business" }, { "column": "QingBiao" }, { "column": "Technology" }], "type": "add", "to": "Synthesize" }] }
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '清标得分', name: 'QingBiao', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype: 'float', tabname: '竞争对手',
                        multiColumnSum: { info: [{ "columns": [{ "column": "Business" }, { "column": "QingBiao" }, { "column": "Technology" }], "type": "add", "to": "Synthesize" }] }
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '综合得分', name: 'Synthesize', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype: 'string', tabname: '竞争对手'
                    },
                    {
                        label: '排名', name: 'Ranking', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'int', tabname:'竞争对手' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '投标负责人', name: 'Leader', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'string', tabname:'竞争对手' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '是否挂靠', name: 'IsGuaKao', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype: 'string', tabname: '竞争对手',
                        formatter: function (cellvalue) {
                            return cellvalue == true ? "是" : "否";
                        }
                        ,edit:{
                            type:'radio',
                            change: function (row, index, oldValue, colname, rows,headData) {
                            },
                            datatype: 'dataItem',
                            code: 'BESF',
                            dfvalue:'false'
                        }
                    },
                    {
                        label: '是否中标', name: 'IsBid', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype: 'string', tabname: '竞争对手',
                        formatter: function (cellvalue) {
                            return cellvalue == true ? "是" : "否";
                        }
                        ,edit:{
                            type:'radio',
                            change: function (row, index, oldValue, colname, rows,headData) {
                            },
                            datatype: 'dataItem',
                            code:'BESF',
                            dfvalue:'false'
                        }
                    },
                    {
                        label: '工期(天)', name: 'Period', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'int', tabname:'竞争对手' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '质保期(天)', name: 'Warranty', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'int', tabname:'竞争对手' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '行备注', name: 'Remark', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'竞争对手' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    }
                ],
                mainId:"ID",
                bindTable:"Project_BidOpeningDetails",
                isEdit: true,
                height: 300,
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                    row.EditType = 1;
                    row.IsGuaKao = false;
                    row.IsBid = false;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/BidOpening/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/BidOpening/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
    var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="Project_BidOpening"]').mkGetFormData());
        postData.strproject_BidOpeningDetailsList = JSON.stringify($('#Project_BidOpeningDetails').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
