﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-20 11:49
 * 描  述：工程合同变更
 */
var projectId = request("projectId");
var refreshGirdData;
var formId = request("formId");
var auditPassEvent;
var unauditPassEvent;
var checkJson = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增工程合同变更',
                    url: top.$.rootUrl + '/ProjectModule/ProjectContractChange/Form?projectId='+projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectContractChange/GetPageList',
                headData: [
                    { label: "变更编号", name: "Code", width: 150, align: "left"},
                    {
                        label: "变更日期", name: "ChangeDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "合同名称", name: "ContractName", width: 120, align: "left"
                    },
                    { label: "合同编码", name: "ContractCode", width: 150, align: "left"},
                    { label: "合同签定金额", name: "InitAmount", width: 100, align: "left"},
                    { label: "变更前合同金额", name: "CurrentAmount", width: 100, align: "left"},
                    { label: "变更类型", name: "ChangeType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ContractChangeType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left"},
                    { label: "本次变更金额", name: "CurrentChangeAmount", width: 100, align: "left"},
                    { label: "变更后合同金额", name: "AfterAmount", width: 100, align: "left"},
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "摘要", name: "Abstract", width: 150, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.ProjectID = projectId;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };

    auditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectContractChange/Audit', { keyValue: keyValue }, function (data) {
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectContractChange/UnAudit', { keyValue: keyValue }, function (data) {
        });
    };

    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectContractChange/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '工程合同变更',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/ProjectContractChange/Form?keyValue=' + keyValue + '&viewState=' + viewState + '&projectId=' + projectId + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
