﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-20 11:49
 * 描  述：工程合同变更
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_ContractChange";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectContractChange/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var auditPassEvent;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_ContractChangeMaterials', "gridId": 'Project_ContractChangeMaterials' });
                subGrid.push({ "tableName": 'Project_ContractChangeQuantities', "gridId": 'Project_ContractChangeQuantities' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: '10004' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            //$("#addQuantity").on("click", function () {
            //    var contractId = $("#ProjectContractId").val();
            //    if (!!contractId) {
            //        Changjie.layerForm({
            //            id: "selectContarctQuantities",
            //            width: 800,
            //            height: 400,
            //            title: "选择合同工程量清单",
            //            url: top.$.rootUrl + "/ProjectModule/ProjectContract/QuantitiesDialog?contractId=" + contractId,
            //            callBack: function (id) {
            //                return top[id].acceptClick(function (data) {
            //                    if (!!data) {
            //                        var rows = [];
            //                        for (var i = 0; i < data.length; i++) {
            //                            var row = {};
            //                            row.ProjectContractQuantitiesId = data[i].ID;
            //                            row.Code = data[i].Code;
            //                            row.Name = data[i].Name;
            //                            row.Feature = data[i].Feature;
            //                            row.Unit = data[i].Unit;
            //                            row.SortCode = data[i].SortCode;

            //                            row.InitPrice = data[i].Price;
            //                            row.InitQuantities = data[i].Quantities;
            //                            row.InitTotal = data[i].TotalPrice;

            //                            row.ChangePrice = data[i].ChangePrice;
            //                            row.ChangeQuantities = data[i].ChangeQuantities;
            //                            row.ChangeTotalPrice = data[i].ChangeTotalPrice;

            //                            row.AfterQuantities = top.Changjie.simpleMath(row.InitQuantities, row.ChangeQuantities, "add");
            //                            row.AfterTotalPrice = top.Changjie.simpleMath(row.ChangeTotalPrice, row.InitTotal, "add");
            //                            row.AfterPrice = top.Changjie.simpleMath(row.AfterTotalPrice, row.AfterQuantities, "div");

            //                            row.rowState = 1;
            //                            row.ID = Changjie.newGuid();
            //                            row.EditType = 1;
            //                            rows.push(row);
            //                        }
            //                        $("#Project_ContractChangeQuantities").jfGridSet("addRows", rows);
            //                    }
            //                });
            //            }
            //        });
            //    }
            //    else {
            //        Changjie.alert.warning("请您先选择合同");
            //    }
            //});
            //$("#deleteQuantity").on("click", function () {
            //    var rowIndex = $("#Project_ContractChangeQuantities").jfGridGet("rowIndex");
            //    if (rowIndex!=-1) {
            //        $("#Project_ContractChangeQuantities").jfGridSet("removeRow");
            //    }
            //});
            $("#CurrentChangeAmount").on("input propertychange", function () {

                $("#AfterAmount").val(top.Changjie.simpleMath($("#CurrentAmount").val(), $(this).val(), "add"));
            });
            //$("#addMaterials").on("click", function () {

            //});
            //$("#deleteMaterials").on("click", function () {
            //    var rowIndex = $("#Project_ContractChangeMaterials").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Project_ContractChangeMaterials").jfGridSet("removeRow");
            //    }
            //});
            $('#ContractName').on("click", function () {
                Changjie.layerForm({
                    id: "selectContract",
                    title: "选择合同",
                    url: top.$.rootUrl + '/ProjectModule/ProjectContract/Dialog?projectId=' + projectId,
                    width: 800,
                    hegiht: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                page.bindContractInfo(data);
                            }
                        });
                    }
                });
            });
            $('#ChangeType').mkDataItemSelect({ code: 'ContractChangeType' });

            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);

            $('#OperatorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            }).mkselectSet(loginInfo.departmentId);

            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_ContractChangeQuantities').jfGrid({
                headData: [
                    {
                        label: '清单编号', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '项目特征', name: 'Feature', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '初始合同', field: 'lffec359be274440f80c3a356d3d08b3f', align: "center", width: 100, cellStyle: { 'text-align': 'right' }
                        , children: [
                            {
                                label: '综合单价', name: 'InitPrice', width: 100, cellStyle: { 'text-align': 'center' },
                            },
                            {
                                label: '工程量', name: 'InitQuantities', width: 100, cellStyle: { 'text-align': 'center' }
                            },
                            {
                                label: '合价', name: 'InitTotal', width: 100, cellStyle: { 'text-align': 'center' }
                            }
                        ]
                    },
                    {
                        label: '累计变更', field: 'l2ef66be4768e4a7fa91244e8d9c274e4', width: 100, align: "center", cellStyle: { 'text-align': 'center' },
                        children: [
                            {
                                label: '综合单价', name: 'ChangePrice', width: 100, cellStyle: { 'text-align': 'left' }
                            },
                            {
                                label: '工程量', name: 'ChangeQuantities', width: 100, cellStyle: { 'text-align': 'left' }
                            },
                            {
                                label: '合价', name: 'ChangeTotalPrice', width: 100, cellStyle: { 'text-align': 'left' }
                            }
                        ]
                    },
                    {
                        label: '本次变更', field: 'l1ec90c83030f4cf69f212e971602d422', align: "center", headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '综合单价', name: 'Price', width: 100, headColor: "blue", cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单', inlineOperation: {
                                    to: [
                                        { "t1": "Price", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }
                                    ], isFirst: true
                                },
                                edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {

                                        if (!!row["TotalPrice"]) {
                                            var afterTotalPrice = parseFloat(row["InitTotal"]) + parseFloat(row["ChangeTotalPrice"]) + parseFloat(row["TotalPrice"]);
                                            row["AfterTotalPrice"] = afterTotalPrice;
                                            $("div[colname='AfterTotalPrice'][rownum='Project_ContractChangeQuantities_" + index + "']").html(afterTotalPrice);

                                            if (!!row["AfterTotalPrice"]) {
                                                var afterPrice = top.Changjie.simpleMath(row["AfterTotalPrice"], row["AfterQuantities"], "div");
                                                row["AfterPrice"] = afterPrice;
                                                $("div[colname='AfterPrice'][rownum='Project_ContractChangeQuantities_" + index + "']").html(afterPrice);
                                            }
                                        }


                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '工程量', name: 'Quantities', width: 100, headColor: "blue", cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单', inlineOperation: {
                                    to: [
                                        { "t1": "Price", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }
                                    ], isFirst: true
                                },
                                edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var afterQuantities = parseFloat(row["InitQuantities"]) + parseFloat(row["ChangeQuantities"]) + parseFloat(row["Quantities"]);
                                        row["AfterQuantities"] = afterQuantities;
                                        $("div[colname='AfterQuantities'][rownum='Project_ContractChangeQuantities_" + index + "']").html(afterQuantities);

                                        var afterTotalPrice = parseFloat(row["InitTotal"]) + parseFloat(row["ChangeTotalPrice"]) + parseFloat(row["TotalPrice"]);
                                        row["AfterTotalPrice"] = afterTotalPrice;
                                        $("div[colname='AfterTotalPrice'][rownum='Project_ContractChangeQuantities_" + index + "']").html(afterTotalPrice);


                                        var afterPrice = top.Changjie.simpleMath(afterTotalPrice, afterQuantities, "div");
                                        row["AfterPrice"] = afterPrice;
                                        $("div[colname='AfterPrice'][rownum='Project_ContractChangeQuantities_" + index + "']").html(afterPrice);
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '合价', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'CurrentChangeAmount', required: '0',
                                datatype: 'float', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '本次变更后', field: 'l8de81b6cc98b4cf4acc7d35f9cf9ec92', align: "center", width: 100, cellStyle: { 'text-align': 'left' },
                        children: [
                            {
                                label: '综合单价', name: 'AfterPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0', datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '工程量', name: 'AfterQuantities', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                            {
                                label: '合价', name: 'AfterTotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, required: '0',
                                datatype: 'string', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '备注', name: 'Remark', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_ContractChangeQuantities",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    var contractId = $("#ProjectContractId").val();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectContarctQuantities",
                            width: 800,
                            height: 400,
                            title: "选择合同工程量清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectContract/QuantitiesDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectContractQuantitiesId = data[i].ID;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Feature = data[i].Feature;
                                            row.Unit = data[i].Unit;
                                            row.SortCode = data[i].SortCode;

                                            row.InitPrice = data[i].Price;
                                            row.InitQuantities = data[i].Quantities;
                                            row.InitTotal = data[i].TotalPrice;

                                            row.ChangePrice = data[i].ChangePrice;
                                            row.ChangeQuantities = data[i].ChangeQuantities;
                                            row.ChangeTotalPrice = data[i].ChangeTotalPrice;

                                            row.AfterQuantities = top.Changjie.simpleMath(row.InitQuantities, row.ChangeQuantities, "add");
                                            row.AfterTotalPrice = top.Changjie.simpleMath(row.ChangeTotalPrice, row.InitTotal, "add");
                                            row.AfterPrice = top.Changjie.simpleMath(row.AfterTotalPrice, row.AfterQuantities, "div");

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }
                                        $("#Project_ContractChangeQuantities").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择合同");
                    }
                }
            });
            $('#Project_ContractChangeMaterials').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '材料编码', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '初始合同', field: 'le7c814c577a14d20a65fe977cee0d934', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'InitPrice', width: 100, cellStyle: { 'text-align': 'left' }
                            },
                            {
                                label: '数量', name: 'InitQuantity', width: 100, cellStyle: { 'text-align': 'left' }
                            },
                            {
                                label: '金额', name: 'InitTotal', width: 100, cellStyle: { 'text-align': 'left' }
                            },
                        ]
                    },
                    {
                        label: '累计变更', field: 'ldacaeb14e5a841ce8b7ce290f0d63454', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'ChangePrice', width: 100, cellStyle: { 'text-align': 'left' }
                            },
                            {
                                label: '数量', name: 'ChangeQuantity', width: 100, cellStyle: { 'text-align': 'left' }
                            },
                            {
                                label: '金额', name: 'ChangeTotalPrice', width: 100, cellStyle: { 'text-align': 'left' }
                            },
                        ]
                    },
                    {
                        label: '本次变更', field: 'lfd226d672cc94472a54441fb5090c6c5', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'Price', width: 100, headColor: "blue", cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        if (row["changeType"] == 0) {
                                            var afterPrice = parseFloat(row["Price"]);
                                            row["AfterPrice"] = afterPrice;
                                            $("div[colname='AfterPrice'][rownum='Project_ContractChangeMaterials_" + index + "']").html(afterPrice);

                                            var afterTotalPrice = parseFloat(row["TotalPrice"]);
                                            row["AfterTotalPrice"] = afterTotalPrice;
                                            $("div[colname='AfterTotalPrice'][rownum='Project_ContractChangeMaterials_" + index + "']").html(afterTotalPrice);
                                        }
                                        else {
                                            if (!!row["TotalPrice"]) {
                                                var afterTotalPrice = parseFloat(row["InitTotal"]) + parseFloat(row["ChangeTotalPrice"]) + parseFloat(row["TotalPrice"]);
                                                row["AfterTotalPrice"] = afterTotalPrice;
                                                $("div[colname='AfterTotalPrice'][rownum='Project_ContractChangeMaterials_" + index + "']").html(afterTotalPrice);

                                                if (!!row["AfterTotalPrice"]) {
                                                    var afterPrice = top.Changjie.simpleMath(row["AfterTotalPrice"], row["AfterQuantity"], "div");
                                                    row["AfterPrice"] = afterPrice;
                                                    $("div[colname='AfterPrice'][rownum='Project_ContractChangeMaterials_" + index + "']").html(afterPrice);
                                                }
                                            }
                                        }
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '数量', name: 'Quantity', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        if (row["changeType"] == 0) {
                                            var afterQuantity = parseFloat(row["Quantity"]);
                                            row["AfterQuantity"] = afterQuantity;
                                            $("div[colname='AfterQuantity'][rownum='Project_ContractChangeMaterials_" + index + "']").html(afterQuantity);

                                            var afterTotalPrice = parseFloat(row["TotalPrice"]);
                                            row["AfterTotalPrice"] = afterTotalPrice;
                                            $("div[colname='AfterTotalPrice'][rownum='Project_ContractChangeMaterials_" + index + "']").html(afterTotalPrice);
                                        }
                                        else {
                                            var afterQuantity = parseFloat(row["InitQuantity"]) + parseFloat(row["ChangeQuantity"]) + parseFloat(row["Quantity"]);
                                            row["AfterQuantity"] = afterQuantity;
                                            $("div[colname='AfterQuantity'][rownum='Project_ContractChangeMaterials_" + index + "']").html(afterQuantity);

                                            var afterTotalPrice = parseFloat(row["InitTotal"]) + parseFloat(row["ChangeTotalPrice"]) + parseFloat(row["TotalPrice"]);
                                            row["AfterTotalPrice"] = afterTotalPrice;
                                            $("div[colname='AfterTotalPrice'][rownum='Project_ContractChangeMaterials_" + index + "']").html(afterTotalPrice);


                                            var afterPrice = top.Changjie.simpleMath(afterTotalPrice, afterQuantity, "div");
                                            row["AfterPrice"] = afterPrice;
                                            $("div[colname='AfterPrice'][rownum='Project_ContractChangeMaterials_" + index + "']").html(afterPrice);

                                        }
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '金额', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单'
                            },
                        ]
                    },
                    {
                        label: '本次变更后', field: 'ld03827d3105348b3a596a0193301e4f6', align: "center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '单价', name: 'AfterPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '数量', name: 'AfterQuantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '金额', name: 'AfterTotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                        ]
                    },
                    {
                        label: '备注', name: 'Remark', headColor: "blue", width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_ContractChangeMaterials",
                isEdit: true,
                height: 300,
                toolbarposition: "top", showchoose: true, onChooseEvent: function () {
                    var contractId = $("#ProjectContractId").val();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectContractMaterials",
                            width: 800,
                            height: 400,
                            title: "选择合同材料清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectContract/MaterialsDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectContractMaterialsId = data[i].ID;
                                            row.ListCode = data[i].ListCode;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].Brand;
                                            row.ModelNumber = data[i].ModelNumber;
                                            row.Unit = data[i].Unit;
                                            row.SortCode = data[i].SortCode;

                                            row.InitPrice = data[i].Price;
                                            row.InitQuantity = data[i].Quantity;
                                            row.InitTotal = data[i].TotalPrice;

                                            row.ChangePrice = data[i].ChangePrice;
                                            row.ChangeQuantity = data[i].ChangeQuantity;
                                            row.ChangeTotalPrice = data[i].ChangeTotalPrice;

                                            row.AfterQuantity = top.Changjie.simpleMath(row.InitQuantity, row.ChangeQuantity, "add");
                                            row.AfterTotalPrice = top.Changjie.simpleMath(row.ChangeTotalPrice, row.InitTotal, "add");
                                            row.AfterPrice = top.Changjie.simpleMath(row.AfterTotalPrice, row.AfterQuantity, "div");

                                            row.ProjectId = projectId;
                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.MaterialsSource = data[i].MaterialsSource == 1 ? 1 : 0;
                                            row.changeType = 1;  //材料变更类型 0：手动添加材料  1：初始合同所选材料
                                            rows.push(row);
                                        }
                                        $("#Project_ContractChangeMaterials").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择合同");
                    }
                },
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                    row.ProjectId = projectId;
                    row.Quantity = 0;
                    row.Price = 0;
                    row.TotalPrice = 0;
                    row.SortCode = rows.length;
                    row.MaterialsSource = 0;
                    row.changeType = 0;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectContractChange/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                    page.loadContractInfo($("#ProjectContractId").val());
                });
            }
        },
        loadContractInfo: function (conractId) {
            var url = top.$.rootUrl + '/ProjectModule/ProjectContract/GetFormData?keyValue=' + conractId;
            top.Changjie.httpAsyncGet(url, function (data) {
                if (data.code == 200) {
                    page.bindContractInfo(data.data.data);
                }
            });
        },
        bindContractInfo: function (data) {
            $("#ContractName").val(data.Name);
            $("#ContractCode").val(data.Code);
            $("#ContractPrice").val(data.InitAmount || 0);
            $("#CurrentAmount").val(data.TotalAmount || 0);
            $("#ProjectContractId").val(data.ID);
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectContractChange/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };

    auditPassEvent = function () {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectContractChange/Audit', { keyValue: keyValue }, function (data) {
        });
    };

    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_ContractChange"]').mkGetFormData());
    postData.strproject_ContractChangeMaterialsList = JSON.stringify($('#Project_ContractChangeMaterials').jfGridGet('rowdatas'));
    postData.strproject_ContractChangeQuantitiesList = JSON.stringify($('#Project_ContractChangeQuantities').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
