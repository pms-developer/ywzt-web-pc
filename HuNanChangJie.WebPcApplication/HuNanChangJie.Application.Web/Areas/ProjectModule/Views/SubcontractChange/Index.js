﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-23 11:52
 * 描  述：分包变更
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var auditPassEvent;
var unauditPassEvent;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增分包变更',
                    url: top.$.rootUrl + '/ProjectModule/SubcontractChange/Form?projectId=' + projectId,
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/SubcontractChange/GetPageList',
                headData: [
                    { label: "变更单号", name: "Code", width: 100, align: "left"},
                    { label: "分包合同", name: "SubcontractName", width: 100, align: "left"},
                    { label: "创建者", name: "Creation_Id", width: 100, align: "left"},
                    { label: "变更类型", name: "ChangeType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ContractChangeType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "变更金额", name: "ChangeAmount", width: 100, align: "left"},
                    { label: "变更日期", name: "ChangeDate", width: 100, align: "left"},
                    { label: "变更之后金额", name: "AfterAmount", width: 100, align: "left"},
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left"},
                    { label: "经办人", name: "OparetorId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "经办人部门", name: "OparetorDepartmentId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                     
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };

    auditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/subcontractChange/Audit', { keyValue: keyValue }, function (data) {
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/subcontractChange/UnAudit', { keyValue: keyValue }, function (data) {
        });
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/SubcontractChange/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '分包变更',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/SubcontractChange/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&projectId=" + projectId + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
