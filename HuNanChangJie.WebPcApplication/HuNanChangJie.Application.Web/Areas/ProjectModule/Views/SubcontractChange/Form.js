﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-23 11:52
 * 描  述：分包变更
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_SubcontractChange";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/SubcontractChange/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_SubcontractChangeMaterials', "gridId": 'Project_SubcontractChangeMaterials' });
                subGrid.push({ "tableName": 'Project_SubcontractChangeQuantities', "gridId": 'Project_SubcontractChangeQuantities' });
                subGrid.push({ "tableName": 'CB_Leasehold', "gridId": 'ID' });
                type = "add";
            }



            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            page.bindEvent();
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'ProjectSubcontractChangeCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $("#ProjectSubcontractId").mkselect({
                url: top.$.rootUrl + "/ProjectModule/ProjectSubcontract/GetList?projectId=" + projectId,
                value: "ID",
                text: "FullName",
                title: "FullName"
            });
            $("#ProjectSubcontractId").on("change", function () {
                var data = $(this).mkselectGetEx();
                $("#InitAmount").val(data.InitAmount);
                $("#TotalAmount").val(data.TotalAmount);
                $("#SubcontractTypeName").val(data.SubcontractTypeName);
                if (data.SubcontractTypeName == "机械租赁") {
                    $("#tabsub1s").hide();
                    $("#tabsub2s").hide();
                    $("#tabsub3s").show();
                }
                else {
                    $("#tabsub3s").hide();
                    $("#tabsub2s").show();
                    $("#tabsub1s").show();
                }
            });


            $("#ChangeAmount").on("input propertychange", function () {
                var AfterAmount = parseFloat(Changjie.clearNumSeparator($(this).val() || 0)) + parseFloat(Changjie.clearNumSeparator($("#TotalAmount").val() || 0));
                $("#AfterAmount").val(AfterAmount);
            });
            $('#ChangeType').mkRadioCheckbox({
                type: 'radio',
                code: 'ContractChangeType',
            });
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OparetorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds',
                select: function (item) {
                    //console.log(item.department);

                    $("#OparetorDepartmentId").children(":first").text(item.department);
                    $("#OparetorDepartmentId").mkselectSet(item.departmentid);
                    var e = jQuery.Event("change");
                    $("#OparetorDepartmentId").trigger(e);
                }
            }).mkformselectSet(loginInfo.userId);
            $("#Abstract").on("input properchange", function () {

            });
            $('#OparetorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            }).mkselectSet(loginInfo.departmentId);
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');



            //if (("#SubcontractTypeName").val().indexOf("租赁") > -1) {
            //    debugger;
            //    $("#tabsub1s").hide();
            //    $("#tabsub2s").hide();
            //    $("#tabsub3s").show();
            //}
            //else {
            //    $("#tabsub3s").hide();
            //    $("#tabsub2s").show();
            //    $("#tabsub1s").show();
            //}

            $('#CB_Leasehold').jfGrid({
                headData: [
                    //{
                    //    label: '编号', name: 'ID', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'string', tabname: '租赁清单'
                    //    , edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},
                    {
                        label: '租赁清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'Specification', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '租赁设备单价', name: 'RentalEquipment', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '设备单位', name: 'EquipmentUnit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '设备数量', name: 'Qty', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '设备租金', name: 'RentalFee', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'InitAmount', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '租期单位', name: 'RentalUnit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: ' 租期', name: 'Rental', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var SummationQty = parseFloat(row["RentalEquipment"]) * parseFloat(row["Qty"]) * parseFloat(row["Rental"]);
                                row["Summation"] = SummationQty;
                                $("div[colname='Summation'][rownum='CB_Leasehold_" + index + "']").html(SummationQty);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合计金额', name: 'Summation', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remarks', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],

                mainId: "ID", toolbarposition: "top", isEdit: true, showchoose: true, showadd: true,
                bindTable: "CB_Leasehold",
                height: 300, onChooseEvent: function () {
                    var contractId = $('#ProjectSubcontractId').mkselectGet();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择租赁合同清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectSubcontractRent/Leasehold?projectid=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    //alert(JSON.stringify(id))
                                    //debugger;
                                    if (!!data) {
                                        /* var rows = [];
                                         for (var i = 0; i < data.length; i++) {
                                             var row = {};
                                             //row.ProjectId = projectId
                                             row.Name = data[i].Name
                                             row.Specification = data[i].Specification;
                                             row.RentalEquipment = data[i].RentalEquipment;
                                             row.EquipmentUnit = data[i].EquipmentUnit;
                                             row.Qty = data[i].Qty;
                                             row.RentalFee = data[i].RentalFee;
                                             row.RentalUnit = data[i].RentalUnit;
                                             row.Rental = data[i].Rental;
                                             row.Summation = data[i].Summation;
                                             row.Remarks = data[i].Remarks;
                                             rows.push(row);
 
                                   
                                         }*/
                                        $("#CB_Leasehold").jfGridSet("addRows", data);

                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择租赁合同");
                    }
                }





                //mainId: "ID",
                //bindTable: "CB_Leasehold",
                //isEdit: true,
                //height: 300, toolbarposition: "top",
                //onAddRow: function (row, rows) {
                //    row["ID"] = Changjie.newGuid();
                //    row.rowState = 1;
                //    row.SortCode = rows.length;
                //    row.EditType = 1;
                //}
            });

            $('#Project_SubcontractChangeQuantities').jfGrid({
                headData: [
                    {
                        label: '清单编号', name: 'Code', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '项目特征', name: 'Name', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '项目特征', name: 'Feature', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '初始合同', name: 'l47d163976e9e46ef9808a070d805af83', width: 100, align: "center"
                        , children: [
                            {
                                label: '综合单价', name: 'InitPrice', width: 100, datatype: 'float',
                                edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {

                                        var gridName = "Project_SubcontractChangeQuantities";

                                        if (row.InitPrice > 0 && row.InitQuantities) {
                                            //var price = eval(total / row.AfterQuantities);
                                            row.InitTotalPrice = parseFloat(Changjie.getFloatValue(row.InitPrice) * Changjie.getFloatValue(row.InitQuantities)).toFixed(4);
                                            setCellValue("InitTotalPrice", row, index, gridName, row.InitTotalPrice);
                                        }
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '工程量', name: 'InitQuantities', width: 100, datatype: 'float',
                                edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractChangeQuantities";

                                        if (row.InitPrice > 0 && row.InitQuantities) {
                                            //var price = eval(total / row.AfterQuantities);
                                            // row.InitTotalPrice = Changjie.getFloatValue(row.InitPrice) * Changjie.getFloatValue(row.InitQuantities);

                                            row.InitTotalPrice = parseFloat(Changjie.getFloatValue(row.InitPrice) * Changjie.getFloatValue(row.InitQuantities)).toFixed(4);

                                            setCellValue("InitTotalPrice", row, index, gridName, row.InitTotalPrice);
                                        }
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '合价', name: 'InitTotalPrice', width: 100, datatype: 'float'
                            },
                        ]
                    },
                    {
                        label: '累计变更', name: 'l397f1c0c36ba46e0890d7e3818c508e4', width: 100, align: "center"
                        , children: [
                            {
                                label: '综合单价', name: 'ChangePrice', width: 100, datatype: 'float',
                                edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractChangeQuantities";

                                        if (row.ChangePrice > 0 && row.ChangeQuantities) {
                                            //var price = eval(total / row.AfterQuantities);
                                            row.ChangeTotalPrice = parseFloat(Changjie.getFloatValue(row.ChangePrice) * Changjie.getFloatValue(row.ChangeQuantities)).toFixed(4);

                                            setCellValue("ChangeTotalPrice", row, index, gridName, row.ChangeTotalPrice);
                                        }

                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '工程量', name: 'ChangeQuantities', width: 100, datatype: 'float',
                                edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractChangeQuantities";

                                        if (row.ChangePrice > 0 && row.ChangeQuantities) {
                                            //var price = eval(total / row.AfterQuantities);
                                            row.ChangeTotalPrice = parseFloat(Changjie.getFloatValue(row.ChangePrice) * Changjie.getFloatValue(row.ChangeQuantities)).toFixed(4);

                                            setCellValue("ChangeTotalPrice", row, index, gridName, row.ChangeTotalPrice);
                                        }
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '合价', name: 'ChangeTotalPrice', width: 100, datatype: 'float'
                            },
                        ]
                    },
                    {
                        label: '本次变更', name: 'l21ca945f90534774950d210182cac99f', width: 100, align: "center"
                        , children: [
                            {
                                label: '综合单价', name: 'Price', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractChangeQuantities";

                                        row.AfterQuantities = Changjie.getFloatValue(row.InitQuantities) + Changjie.getFloatValue(row.ChangeQuantities) + Changjie.getFloatValue(row.Quantities);
                                        setCellValue("AfterQuantities", row, index, gridName, row.AfterQuantities);
                                        var total = Changjie.getFloatValue(row.InitTotalPrice) + Changjie.getFloatValue(row.ChangeTotalPrice) + Changjie.getFloatValue(row.TotalPrice);
                                        if (row.AfterQuantities > 0) {
                                            var price = eval(total / row.AfterQuantities);
                                            row.AfterPrice = price;
                                            row.AfterTotalPrice = eval(row.AfterQuantities * row.AfterPrice);
                                            setCellValue("AfterPrice", row, index, gridName, price);
                                            setCellValue("AfterTotalPrice", row, index, gridName, row.AfterTotalPrice);
                                        }
                                        else {
                                            setCellValue("AfterPrice", row, index, gridName, "");
                                            setCellValue("AfterTotalPrice", row, index, gridName, "");
                                        }
                                    }
                                }
                            },
                            {
                                label: '工程量', name: 'Quantities', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractChangeQuantities";

                                        row.AfterQuantities = Changjie.getFloatValue(row.InitQuantities) + Changjie.getFloatValue(row.ChangeQuantities) + Changjie.getFloatValue(row.Quantities);
                                        setCellValue("AfterQuantities", row, index, gridName, row.AfterQuantities);
                                        var total = Changjie.getFloatValue(row.InitTotalPrice) + Changjie.getFloatValue(row.ChangeTotalPrice) + Changjie.getFloatValue(row.TotalPrice);
                                        if (row.AfterQuantities > 0) {
                                            var price = eval(total / row.AfterQuantities);
                                            row.AfterPrice = price;
                                            row.AfterTotalPrice = eval(row.AfterQuantities * row.AfterPrice);
                                            setCellValue("AfterPrice", row, index, gridName, price);
                                            setCellValue("AfterTotalPrice", row, index, gridName, row.AfterTotalPrice);
                                        }
                                        else {
                                            setCellValue("AfterPrice", row, index, gridName, "");
                                            setCellValue("AfterTotalPrice", row, index, gridName, "");
                                        }
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '合价', name: 'TotalPrice', width: 100, aggtype: 'sum', aggcolumn: 'ChangeAmount', required: '0',
                                datatype: 'float', tabname: '工程量清单'
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractChangeQuantities";

                                        row.AfterQuantities = Changjie.getFloatValue(row.InitQuantities) + Changjie.getFloatValue(row.ChangeQuantities) + Changjie.getFloatValue(row.Quantities);
                                        setCellValue("AfterQuantities", row, index, gridName, row.AfterQuantities);
                                        var total = Changjie.getFloatValue(row.InitTotalPrice) + Changjie.getFloatValue(row.ChangeTotalPrice) + Changjie.getFloatValue(row.TotalPrice);
                                        if (row.AfterQuantities > 0) {
                                            var price = eval(total / row.AfterQuantities);
                                            row.AfterPrice = price;
                                            row.AfterTotalPrice = eval(row.AfterQuantities * row.AfterPrice);
                                            setCellValue("AfterPrice", row, index, gridName, price);
                                            setCellValue("AfterTotalPrice", row, index, gridName, row.AfterTotalPrice);
                                        }
                                        else {
                                            setCellValue("AfterPrice", row, index, gridName, "");
                                            setCellValue("AfterTotalPrice", row, index, gridName, "");
                                        }
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                        ]
                    },
                    {
                        label: '本次变更后', name: 'l4e99188cad0e472badce642131699d12', width: 100, align: "center"
                        , children: [
                            {
                                label: '综合单价', name: 'AfterPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单',
                                edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '工程量', name: 'AfterQuantities', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单',
                                edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '本次变更合价', name: 'AfterTotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '工程量清单',
                                edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                        ]
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, aggtype: 'normal', headColor: "blue", aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_SubcontractChangeQuantities",
                isEdit: true,
                height: 300,
                toolbarposition: "top",
                // showadd: false,
                showchoose: true,
                onChooseEvent: function () {
                    var contractId = $('#ProjectSubcontractId').mkselectGet();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择分包合同工程量清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectSubcontract/QuantitiesDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectId = projectId
                                            row.ProjectSubcontractQuantitiesId = data[i].ID
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Feature = data[i].Feature;
                                            row.Unit = data[i].Unit;

                                            row.InitPrice = data[i].Price;
                                            row.InitQuantities = data[i].Quantities;
                                            row.InitTotalPrice = data[i].TotalPrice;
                                            row.ChangePrice = data[i].ChangePrice;
                                            row.ChangeQuantities = data[i].ChangeQuantities;
                                            row.ChangeTotalPrice = data[i].ChangeTotalPrice;

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            rows.push(row);
                                        }
                                        $("#Project_SubcontractChangeQuantities").jfGridSet("addRows", rows);

                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择分包合同");
                    }
                },
                onAddRowEvent: function () {
                    var rows = [];
                    var row = {};
                    row.ProjectId = projectId;
                    row.rowState = 1;
                    row.ID = Changjie.newGuid();
                    row.EditType = 1;
                    row.MaterialsSource = 0;
                    row.changeType = 0;
                    rows.push(row);
                    $("#Project_SubcontractChangeQuantities").jfGridSet("addRows", rows);
                }
            });

            $('#Project_SubcontractChangeMaterials').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '材料编码', name: 'Code', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '名称', name: 'Name', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },

                    {
                        label: '计量单位', name: 'Unit', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },

                    //{
                    //    label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                    //    edit: {
                    //        type: 'select',
                    //        init: function (row, $self) {// 选中单元格后执行
                    //        },
                    //        change: function (row, index, item, oldValue, colname, headData) {
                    //        },
                    //        datatype: 'dataSource',
                    //        code: 'MaterialsUnit',
                    //        op: {
                    //            value: 'id',
                    //            text: 'name',
                    //            title: 'name'
                    //        }
                    //    }
                    //},

                    {
                        label: '初始合同', name: 'lead6ca2fc3594269b074a3f3f79e4428', width: 100, align: "center"
                        , children: [
                            {
                                label: '单价', name: 'InitPrice', width: 100
                            },
                            {
                                label: '数量', name: 'InitQuantity', width: 100
                            },
                            {
                                label: '金额', name: 'InitTotalPrice', width: 100
                            },
                        ]
                    },
                    {
                        label: '累计变更', name: 'lce36bffcbf83494eb539b1e6b903d4ed', width: 100, align: "center"
                        , children: [
                            {
                                label: '单价', name: 'ChangePrice', width: 100
                            },
                            {
                                label: '数量', name: 'ChangeQuantity', width: 100
                            },
                            {
                                label: '金额', name: 'ChangeTotalPrice', width: 100
                            },
                        ]
                    },
                    {
                        label: '本次变更', name: 'l4ee57de3dc6a4c5ca7d29c6c168d5c71', width: 100, align: "center"
                        , children: [
                            {
                                label: '单价', name: 'Price', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractChangeMaterials";
                                        if (row["changeType"] == 0) {
                                            var afterPrice = parseFloat(row["Price"]);
                                            setCellValue("AfterPrice", row, index, gridName, afterPrice);

                                            var afterTotalPrice = parseFloat(row["TotalPrice"]);
                                            setCellValue("AfterTotalPrice", row, index, gridName, afterTotalPrice);
                                        }
                                        else {
                                            row.AfterQuantity = Changjie.getFloatValue(row.InitQuantity) + Changjie.getFloatValue(row.ChangeQuantity) + Changjie.getFloatValue(row.Quantity);
                                            setCellValue("AfterQuantity", row, index, gridName, row.AfterQuantity);
                                            var total = Changjie.getFloatValue(row.InitTotalPrice) + Changjie.getFloatValue(row.ChangeTotalPrice) + Changjie.getFloatValue(row.TotalPrice);
                                            if (row.AfterQuantity > 0) {
                                                var price = Changjie.format45(eval(total / row.AfterQuantity), Changjie.sysGlobalSettings.pointGlobal);
                                                row.AfterPrice = price;
                                                row.AfterTotalPrice = Changjie.format45(eval(row.AfterQuantity * row.AfterPrice), Changjie.sysGlobalSettings.pointGlobal);
                                                setCellValue("AfterPrice", row, index, gridName, price);
                                                setCellValue("AfterTotalPrice", row, index, gridName, row.AfterTotalPrice);
                                            }
                                            else {
                                                setCellValue("AfterPrice", row, index, gridName, "");
                                                setCellValue("AfterTotalPrice", row, index, gridName, "");
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                label: '数量', name: 'Quantity', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractChangeMaterials";
                                        if (row["changeType"] == 0) {
                                            var afterQuantity = Changjie.format45(row["Quantity"], Changjie.sysGlobalSettings.pointGlobal);
                                            setCellValue("AfterQuantity", row, index, gridName, afterQuantity);

                                            var afterTotalPrice = Changjie.format45(row["TotalPrice"], Changjie.sysGlobalSettings.pointGlobal);
                                            setCellValue("AfterTotalPrice", row, index, gridName, afterTotalPrice);
                                        }
                                        else {
                                            row.AfterQuantity = Changjie.getFloatValue(row.InitQuantity) + Changjie.getFloatValue(row.ChangeQuantity) + Changjie.getFloatValue(row.Quantity);
                                            setCellValue("AfterQuantity", row, index, gridName, row.AfterQuantity);
                                            var total = Changjie.getFloatValue(row.InitTotalPrice) + Changjie.getFloatValue(row.ChangeTotalPrice) + Changjie.getFloatValue(row.TotalPrice);
                                            if (row.AfterQuantity > 0) {
                                                var price = Changjie.format45(eval(total / row.AfterQuantity), Changjie.sysGlobalSettings.pointGlobal);
                                                row.AfterPrice = price;
                                                row.AfterTotalPrice = Changjie.format45(eval(row.AfterQuantity * row.AfterPrice), Changjie.sysGlobalSettings.pointGlobal);
                                                setCellValue("AfterPrice", row, index, gridName, price);
                                                setCellValue("AfterTotalPrice", row, index, gridName, row.AfterTotalPrice);
                                            }
                                            else {
                                                setCellValue("AfterPrice", row, index, gridName, "");
                                                setCellValue("AfterTotalPrice", row, index, gridName, "");
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                label: '金额', name: 'TotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '材料清单'
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        var gridName = "Project_SubcontractChangeMaterials";

                                        row.AfterQuantity = Changjie.getFloatValue(row.InitQuantity) + Changjie.getFloatValue(row.ChangeQuantity) + Changjie.getFloatValue(row.Quantity);
                                        setCellValue("AfterQuantity", row, index, gridName, row.AfterQuantity);
                                        var total = Changjie.getFloatValue(row.InitTotalPrice) + Changjie.getFloatValue(row.ChangeTotalPrice) + Changjie.getFloatValue(row.TotalPrice);
                                        if (row.AfterQuantity > 0) {
                                            var price = parseFloat((eval(total / row.AfterQuantity)));
                                            row.AfterPrice = price;
                                            row.AfterTotalPrice = eval(row.AfterQuantity * row.AfterPrice);
                                            setCellValue("AfterPrice", row, index, gridName, price);
                                            setCellValue("AfterTotalPrice", row, index, gridName, row.AfterTotalPrice);
                                        }
                                        else {
                                            setCellValue("AfterPrice", row, index, gridName, "");
                                            setCellValue("AfterTotalPrice", row, index, gridName, "");
                                        }
                                    }
                                }
                            },
                        ]
                    },
                    {
                        label: '本次变更后', name: 'l5c9f5bb3c46c489a93d9c63ad4cf965f', width: 100, align: "center"
                        , children: [
                            {
                                label: '单价', name: 'AfterPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '数量', name: 'AfterQuantity', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                            {
                                label: '金额', name: 'AfterTotalPrice', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'string', tabname: '材料清单'
                            },
                        ]
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, headColor: "blue", aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_SubcontractChangeMaterials",
                isEdit: true,
                height: 300,
                toolbarposition: "top",
                showchoose: true,
                onChooseEvent: function () {
                    var contractId = $('#ProjectSubcontractId').mkselectGet();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectQuantities",
                            width: 800,
                            height: 400,
                            title: "选择分包合同材料清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectSubcontract/MaterialsDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectId = projectId;
                                            row.ProjectSubcontactMaterialsId = data[i].ID;
                                            row.ListCode = data[i].ListCode;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].Brand;
                                            row.ModelNumber = data[i].ModelNumber;
                                            row.Unit = data[i].Unit;

                                            row.InitPrice = data[i].Price;
                                            row.InitQuantity = data[i].Quantity;
                                            row.InitTotalPrice = data[i].TotalPrice;
                                            row.ChangePrice = data[i].ChangePrice;
                                            row.ChangeQuantity = data[i].ChangeQuantity;
                                            row.ChangeTotalPrice = data[i].ChangeTotalPrice;
                                            row.MaterialsSource = data[i].MaterialsSource == 1 ? 1 : 0;

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.changeType = 1;  //材料变更类型 0：手动添加材料  1：初始合同所选材料
                                            rows.push(row);
                                        }
                                        $("#Project_SubcontractChangeMaterials").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择分包合同");
                    }
                },
                onAddRowEvent: function () {
                    var rows = [];
                    var row = {};
                    row.ProjectId = projectId;
                    row.rowState = 1;
                    row.ID = Changjie.newGuid();
                    row.EditType = 1;
                    row.MaterialsSource = 0;
                    row.changeType = 0;
                    rows.push(row);
                    $("#Project_SubcontractChangeMaterials").jfGridSet("addRows", rows);
                }
            });

        },
        bindEvent: function () {
            $("#addQuantities").on("click", function () {

            });
            $("#deleteQuantities").on("click", function () {
                var rowIndex = $("#Project_SubcontractChangeQuantities").jfGridGet("rowIndex");
                if (rowIndex != -1) {
                    $("#Project_SubcontractChangeQuantities").jfGridSet("removeRow");
                }
            });

            $("#addMaterials").on("click", function () {

            });
            $("#deleteMaterials").on("click", function () {
                var rowIndex = $("#Project_SubcontractChangeMaterials").jfGridGet("rowIndex");
                if (rowIndex != -1) {
                    $("#Project_SubcontractChangeMaterials").jfGridSet("removeRow");
                }
            });


        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/SubcontractChange/GetformInfoList?keyValue=' + keyValue, function (data) {
                    debugger;
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    var mathCalculate = function (row, index, target1, target2, to, gridName) {
        var result = 0;
        result = parseFloat(Changjie.clearNumSeparator(row[target1] || 0)) * parseFloat(Changjie.clearNumSeparator(row[target2] || 0));
        setCellValue(to, row, index, gridName, result);
    };
    var setAfterInfo = function (row, index, afterPrice, afterQuantity, afterTotal, gridName) { };
    var setCellValue = function (cellName, row, index, gridName, value) {
        var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
        var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
        row[cellName] = value;
        $cell.html(value);
        $edit.val(value);
    };

    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/SubcontractChange/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    /*for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }*/
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_SubcontractChange"]').mkGetFormData());
    postData.strproject_SubcontractChangeMaterialsList = JSON.stringify($('#Project_SubcontractChangeMaterials').jfGridGet('rowdatas'));
    postData.strproject_SubcontractChangeQuantitiesList = JSON.stringify($('#Project_SubcontractChangeQuantities').jfGridGet('rowdatas'));
    postData.strCB_LeaseholdList = JSON.stringify($('#CB_Leasehold').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;



}
