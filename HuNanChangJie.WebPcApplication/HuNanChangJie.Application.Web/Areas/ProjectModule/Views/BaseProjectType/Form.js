﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-04-05 15:08
 * 描  述：项目类型
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Base_ProjectType";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/BaseProjectType/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var isSameName = false;
var phaseDate;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Base_ProjectTypeDetails', "gridId": 'Base_ProjectTypeDetails' });
                type = "add";
                phaseDate = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/BaseProjectPhase/GetList").data;
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $("#Name").on("input propertychange", function () {
                var value = $(this).val();
                isSameName = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/BaseProjectType/CheckIsSameName?value=" + value).data;
                if (isSameName) {
                    Changjie.alert.error("类型名称已存在，请更换名称");
                }
            });
            $('#IsEnable').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Base_ProjectTypeDetails').jfGrid({
                headData: [
                    {
                        label: '项目阶段', name: 'Name', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '项目阶段'
                    },
                    {
                        label: '后续阶段', name: 'NextPhaseName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '项目阶段'
                        , edit: {
                            type: 'layer',
                            init: function (row) {
                                return "?currentId=" + row.PhaseId;
                            },
                            change: function (data, rownum, selectData) {
                                data.NextPhaseId = selectData.ID;
                                data.NextPhaseName = selectData.Name;
                                $('#Base_ProjectTypeDetails').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 600,
                                height: 400,
                                url: top.$.rootUrl + '/ProjectModule/BaseProjectPhase/Dialog',
                            }
                        }
                    },
                    {
                        label: '关联业务单据', name: 'FormName', width: 200, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '项目阶段'
                        , edit: {
                            type: 'layer',
                            change: function (data, rownum, selectData) {
                                data.FormName = selectData.Name;
                                data.FormId = selectData.FormId;
                                $('#Base_ProjectTypeDetails').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 410,
                                height: 600,
                                url: top.$.rootUrl + '/FormModule/FormRelation/FormDialog',
                            }
                        }
                    },
                    {
                        label: '是否启用', name: 'IsEnable', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '项目阶段',
                        formatter: function (cellvalue, row) {
                            if (cellvalue) {
                                return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                            }
                            return '<span class="label label-default" style="cursor: pointer;">未启用</span>';
                        }
                        , edit: {
                            type: 'radio',
                            change: function (row, index, oldValue, colname, rows, headData) {
                            },
                            datatype: 'dataItem',
                            code: 'BESF',
                            dfvalue: 'true'
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Base_ProjectTypeDetails",
                isEdit: false,
                height: 300
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/BaseProjectType/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
            else {
                if (phaseDate) {
                    for (var i = 0; i < phaseDate.length; i++)
                    {
                        var row = phaseDate[i];
                        row.ID = Changjie.newGuid();
                        row.rowState = 1;
                        row.SortCode = i;
                        row.EditType = 1;
                        row.IsEnable = "true";
                    }
                    $('#Base_ProjectTypeDetails').jfGridSet("refreshdata", phaseDate);
                }
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (isSameName) {
            Changjie.alert.error("类型名称已存在，请更换名称");
            return false;
        }
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/BaseProjectType/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Base_ProjectType"]').mkGetFormData());
    postData.strbase_ProjectTypeDetailsList = JSON.stringify($('#Base_ProjectTypeDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
