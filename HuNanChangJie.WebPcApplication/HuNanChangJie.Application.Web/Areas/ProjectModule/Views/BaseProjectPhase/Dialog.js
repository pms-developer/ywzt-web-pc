﻿var acceptClick;
var currentId = request("currentId");
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/ProjectModule/BaseProjectPhase/GetListByCurrentId?currentId=' + currentId,
                headData: [
                    { label: "项目阶段名称", name: "Name", width: 100, align: "left" },
                    { label: "排序号", name: "SortCode", width: 100, align: "left" },
                    {
                        label: "是否启用", name: "IsEnable", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            if (cellvalue) {
                                return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                            }
                            return '<span class="label label-default" style="cursor: pointer;">未启用</span>';
                        }
                    }

                ],
                mainId: 'ID',
                isPage: true,
                height: 362
            });
            page.search();

        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload');//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};