﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-04-05 14:45
 * 描  述：项目阶段
 */
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增项目阶段',
                    url: top.$.rootUrl + '/ProjectModule/BaseProjectPhase/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            //  启用
            $('#enable').on('click', function () {
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/BaseProjectPhase/GetPageList',
                headData: [
                    { label: "项目阶段名称", name: "Name", width: 100, align: "left" },
                    { label: "阶段完成占比(%)", name: "Rate", width: 100, align: "left" },
                    { label: "排序号", name: "SortCode", width: 100, align: "left" },
                    {
                        label: "是否启用", name: "IsEnable", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            if (cellvalue) {
                                return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                            }
                            return   '<span class="label label-default" style="cursor: pointer;">未启用</span>';
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/BaseProjectPhase/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '项目阶段',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/ProjectModule/BaseProjectPhase/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
