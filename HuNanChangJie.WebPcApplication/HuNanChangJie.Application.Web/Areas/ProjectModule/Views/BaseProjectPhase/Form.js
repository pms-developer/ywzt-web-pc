﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-04-05 14:45
 * 描  述：项目阶段
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Base_ProjectPhase";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/BaseProjectPhase/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var isSameName = false;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $("#Name").on("input propertychange", function () {
                var value = $(this).val();
                isSameName = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/BaseProjectPhase/CheckIsSameName?value=" + value).data;
                if (isSameName) {
                    Changjie.alert.error("阶段名称已存在，请更换名称");
                }
            });
            $('#IsEnable').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/BaseProjectPhase/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (isSameName) {
            Changjie.alert.error("阶段名称已存在，请更换名称");
            return false;
        }
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/BaseProjectPhase/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
