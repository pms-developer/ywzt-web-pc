﻿var refreshGirdData; // 更新数据
var projectId = request("projectId");
var formId = request("formId");
var filetypeid = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGrid();
            page.bind();
        },
        bind: function () {
            debugger;
            $('#module_tree').mktree({
                url: top.$.rootUrl + '/ProjectModule/DocumentType/GetFileTypeTree?keyValue=' + projectId,
                nodeClick: function (item) {
                    debugger
                    filetypeid = item.id;
                    page.search();
                    $('#titleinfo').text(item.text);
                }
            });

            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/ProjectModule/DocumentType/Form?projectId=' + projectId + '&filetypeid=' + filetypeid + '&type=' + "add",
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '编辑',
                    url: top.$.rootUrl + '/ProjectModule/DocumentType/Form?projectId=' + projectId + '&filetypeid=' + filetypeid + '&type=' + "edit",
                    width: 400,
                    height: 300,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = filetypeid;
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！删除后文档的分类数据也将删除！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/DocumentType/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });

        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/DocumentType/GetFileList',
                headData: [
                    { label: "分类名称", name: "FileTypeName", width: 160, align: "left" },
                    { label: "文件", name: "F_FileName", width: 160, align: "left" },
                    { label: "项目", name: "ProjectName", width: 160, align: "left" }
                ],
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.keyValue = filetypeid;
            param.projectId = projectId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };

    // 保存数据后回调刷新
    refreshGirdData = function () {
        location.reload();
        page.search();
    }

    page.init();
}