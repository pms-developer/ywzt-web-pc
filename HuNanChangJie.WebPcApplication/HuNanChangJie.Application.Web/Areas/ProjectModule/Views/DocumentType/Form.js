﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-22 12:02
 * 描  述：文档分类
 */
var acceptClick;
var filetypeid = request('filetypeid');
var projectId = request('projectId');
var type = request('type');
var parentid = "";
var mindid = "";
var tables = [];
var mainTable = "DocumentType";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/DocumentType/SaveForm';
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
            if (filetypeid != "" && type == "add") {
                //选择了类型点击新增，父类型为选择的类型
                $('#ID').val(top.Changjie.newGuid());
                $('#ParentID').mkselectSet(filetypeid);
            }
            else if (filetypeid != "" && type == "edit") {
                $('#ID').val(filetypeid);
                page.initData();
            }
            else if (filetypeid == "") {
                //没有选择类型点击新增，父类型为空
                $('#ID').val(top.Changjie.newGuid());
            }
            $('#ProjectID').val(projectId);
        },
        bind: function () {
            var sql = " projectid='" + projectId + "' ";
            $('#ParentID').mkDataSourceSelect({ code: 'FileType', value: 'id', text: 'name', strWhere: sql });
        },
        initData: function () {
            if (filetypeid != "") {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/DocumentType/GetFileTypeEntity?keyValue=' + filetypeid, function (data) {
                    debugger
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        var keyValue = $('#ID').val();
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/DocumentType/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var isgridpass = true;
    var errorInfos = [];

    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="DocumentType"]').mkGetFormData());
    return postData;
}