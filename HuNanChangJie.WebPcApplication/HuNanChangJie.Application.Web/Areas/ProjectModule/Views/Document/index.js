﻿var refreshGirdData; // 更新数据
var projectId = request("projectId");
var formId = request("formId");
var fileId = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var moduleId = "";
    var page = {
        init: function () {
            page.initGrid();
            page.bind();
        },
        bind: function () {
            $('#module_tree').mktree({
                url: top.$.rootUrl + '/SystemModule/Module/GetProjectModuleTree',
                nodeClick: function (item) {
                    moduleId = item.id;
                    page.search();
                    $('#titleinfo').text(item.text);
                }
            });

            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });

            // 归档
            $('#file').on('click', function () {
                fileId = $('#gridtable').jfGridValue('F_Id');
                Changjie.layerForm({
                    id: 'form',
                    title: '归档',
                    url: top.$.rootUrl + '/ProjectModule/Document/Form?fileID=' + fileId + '&projectId=' + projectId ,
                    width: 400,
                    height: 260,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });

        },
        initGrid: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/SystemModule/Annexes/GetProjectFileListNew',
                headData: [
                    { label: "文件名称", name: "F_FileName", width: 160, align: "left" },
                    { label: "文件大小", name: "F_FileSize", width: 160, align: "left" },
                    { label: "文件后缀", name: "F_FileExtensions", width: 160, align: "left" },
                    { label: "上传人", name: "CreationName", width: 160, align: "left" },
                    {
                        label: '上传日期', name: 'CreationDate', width: 140, align: 'left',
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "操作", name: "DownFile", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return "<a href='javascript:void(0)' onclick=\"downloadFile2('" + row.F_Id + "')\"><span style='color:blue'>下载附件</span></a>";
                        }
                    }
                ],
                mainId: 'F_ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.moduleId = moduleId;
            param.moduleInfoId = projectId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };

    // 保存数据后回调刷新
    refreshGirdData = function () {
        page.search();
    }

    page.init();
}
function downloadFile2(fileId) {
    window.open(top.$.rootUrl + '/SystemModule/Annexes/DownAnnexesFile2?fileId=' + fileId);
}
function downloadFile(fileId) {
     
    var data = {
        url: top.$.rootUrl + '/SystemModule/Annexes/DownAnnexesFile',
        param:
        {
            fileId: fileId,
            __RequestVerificationToken: $.mkToken
        },
        method: 'POST'
    };
    top.Changjie.download(data);
}
