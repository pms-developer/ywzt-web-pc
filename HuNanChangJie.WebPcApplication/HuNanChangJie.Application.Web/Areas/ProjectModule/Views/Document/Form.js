﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-22 12:02
 * 描  述：文件归档
 */
var acceptClick;
var tables = [];
var fileID = request('fileID');
var projectId = request('projectId');
var processCommitUrl = top.$.rootUrl + '/ProjectModule/Document/SaveForm';
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            var sql = " projectid='" + projectId + "' ";
            $('#FileTypeID').mkDataSourceSelect({ code: 'FileType', value: 'id', text: 'name', strWhere: sql });
        },
        initData: function () {
            if (fileID != "") {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/Document/GetBaseFileRelationTypeEntity?keyValue=' + fileID + '&projectId=' + projectId, function (data) {
                    debugger
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var fileTypeID = $('#FileTypeID').mkselectGet();
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/Document/SaveForm?fileTypeID=' + fileTypeID + '&fileID=' + fileID, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}