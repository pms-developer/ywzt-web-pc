﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-04-14 15:29
 * 描  述：外出经营管理许可证
 */
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            $('#ProjectID').mkDataSourceSelect({ code: 'BASE_XMLB',value: 'project_id',text: 'projectname' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/ProjectModule/Project_ExternalManagementCertificate/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/Project_ExternalManagementCertificate/GetPageList',
                headData: [
                    { label: "项目名称", name: "ProjectID", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'BASE_XMLB',
                                 key: value,
                                 keyId: 'project_id',
                                 callback: function (_data) {
                                     callback(_data['projectname']);
                                 }
                             });
                        }},
                    { label: "项目类型", name: "ProjectType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'ProjectType',
                                 key: value,
                                 keyId: 'id',
                                 callback: function (_data) {
                                     callback(_data['name']);
                                 }
                             });
                        }},
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left"},
                    { label: "项目地址", name: "Address", width: 100, align: "left"},
                    { label: "项目负责人", name: "Lead", width: 100, align: "left"},
                    { label: "联系电话", name: "Phone", width: 100, align: "left"},
                    { label: "建设方名称", name: "Builder", width: 100, align: "left"},
                    { label: "合同金额", name: "ContractAmount", width: 100, align: "left"},
                    { label: "开票税率", name: "Rate", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'FPLX',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "工期", name: "TimeLimit", width: 100, align: "left"},
                    { label: "预缴税率", name: "AdvancePaymentRate", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'CW0101',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "创建者名称", name: "CreationName", width: 100, align: "left"},
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "编号", name: "Code", width: 100, align: "left"},
                    { label: "开具日期", name: "OpeningDate", width: 100, align: "left"},
                    { label: "到期日期", name: "ExpireDate", width: 100, align: "left"},
                    { label: "延长日期开始", name: "ExtendStartDate", width: 100, align: "left"},
                    { label: "延长日期结束", name: "ExtendEndDate", width: 100, align: "left"},
                    { label: "注销日期", name: "CancellationDate", width: 100, align: "left"},
                    { label: "是否注销", name: "IsCancellation", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'BESF',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "已预缴税款金额", name: "AdvancePaymentAmount", width: 100, align: "left"},
                    { label: "已开具发票金额", name: "InvoiceAmount", width: 100, align: "left"},
                    { label: "已抵减分包金额", name: "SubcontractAmount", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/Project_ExternalManagementCertificate/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title,
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/Project_ExternalManagementCertificate/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
