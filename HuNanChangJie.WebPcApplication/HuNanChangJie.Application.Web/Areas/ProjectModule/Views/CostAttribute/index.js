﻿ 
var bootstrap = function ($, Changjie) {
    "use strict"
    var page = {
        init: function () {
            page.bind();
            page.initGrid();
            page.search();
        },
        bind: function () {
            $("#add").on("click", function () {
                Changjie.layerForm({
                    id: 'addCostAttributeForm',
                    title: "添加项目属性",
                    url: top.$.rootUrl + '/ProjectModule/CostAttribute/Form',
                    width: 500,
                    height: 380,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGridData);
                    }
                });
            });

            $("#refresh").on("click", function () {
                refreshGridData();
            });

            $("#edit").on("click", function () {
                var keyValue = $("#gridtable").jfGridValue("ID");
                if (!!keyValue) {
                    var url = top.$.rootUrl + '/ProjectModule/CostAttribute/Form?keyValue=' + keyValue;
                    Changjie.layerForm({
                        id: "editCostAttributeForm",
                        title: '编辑项目属性',
                        url: url,
                        width: 500,
                        height: 380,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGridData);
                        }
                    });
                }
            });

            $("#deploy").on("click", function () {
                var keyValue = $("#gridtable").jfGridValue("ID");
                if (!!keyValue) {
                    var costname = $("#gridtable").jfGridValue("Name");
                    Changjie.layerConfirm("是否部署【" + costname + "】?", function (res) {
                        if (res) {
                            var url = top.$.rootUrl + '/ProjectModule/CostAttribute/SetDeploy';
                            var param = {
                                keyValue: keyValue,
                                isDeploy:"true"
                            };
                            var data = top.Changjie.httpGet(url, param);
                            refreshGridData();
                        }
                    });
                }
            });
        },
        initGrid: function () {
            $("#gridtable").mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/CostAttribute/GetList',
                headData: [
                    { label: "属性名称", name: "Name", width: 200, align: "left" },
                    { label: "排序", name: "SortCode", width: 200, align: "left" },
                    {
                        label: "部署状态", name: "IsDeploy", width: 200, align: "left",
                        formatter: function (value) {
                            if (value) {
                                return '<span class="label label-success" style="cursor: pointer;">已部署</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">未部署</span>';
                            }
                        }
                    },
                    {
                        label: "启用状态", name: "IsEnable", width: 200, align: "left",
                        formatter: function (value) {
                            if (value) {
                                return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">已禁用</span>';
                            }
                        }
                    },
                    { label: "备注", name: "Remark", width: 200, align: "left" }
                ],
                mainId: "ID"
            });
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    var refreshGridData = function () {
        page.search();
    };
    page.init();
};