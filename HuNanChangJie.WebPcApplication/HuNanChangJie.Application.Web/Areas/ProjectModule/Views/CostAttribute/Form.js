﻿var keyValue = request("keyValue");
var type = "add";
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict"
    var page = {
        init: function () {
            if (!!keyValue) {
                type = "edit";
            }
            else {
                keyValue = top.Changjie.newGuid();
                type = "add";
            }
            page.bind();
            page.initData();
        },
        bind: function () {
            $("#IsEnable").mkselect({
                placeholder:false,
                data: [
                    { value: "false", text: "否" },
                    { value: "true", text: "是" }
                ],
                value: "value",
                text:"text"
            }).mkselectSet("true");
        },
        initData: function () {
            if (type == "edit") {
                var url = top.$.rootUrl + '/ProjectModule/CostAttribute/GetFormData?keyValue=' + keyValue;
                $.mkSetForm(url, function (data) {
                    $("[data-table='Base_CostAttribute']").mkSetFormData(data);
                });
            }
        }
    };
    acceptClick = function (callBack) {
        if (!$("body").mkValidform()) {
            return false;
        }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData())
        };
        if (type == "add") {
            keyValue = mainId;
        }
        var url = top.$.rootUrl + '/ProjectModule/CostAttribute/SaveForm?keyValue=' + keyValue + '&type=' + type;
        $.mkSaveForm(url, postData, function (res) {
        });
        if (!!callBack) {
            callBack();
        }
    };
    page.init();
};