﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-24 18:57
 * 描  述：工程合同产值结算
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_OutputSettlement";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var gridverifystate = true;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_OutputSettlementMaterials', "gridId": 'Project_OutputSettlementMaterials' });
                subGrid.push({ "tableName": 'Project_OutputSettlementQuantities', "gridId": 'Project_OutputSettlementQuantities' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'OutputSettlement' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });

            Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/GetYearData', { projectId: projectId,ye:1 }, function (data) {
                if (!$('#yearCurrentProduction').val()) {
                    $('#yearCurrentProduction').val(data);
                }
            });
            Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/GetYearData', { projectId: projectId}, function (data) {
                if (!$('#zCurrentProduction').val()) {
                    $('#zCurrentProduction').val(data);
                }
            });
            $("#addQuantity").on("click", function () {
               
            });
            $("#deleteQuantity").on("click", function () {
                var rowIndex = $("#Project_OutputSettlementQuantities").jfGridGet("rowIndex");
                if (rowIndex != -1) {
                    $("#Project_OutputSettlementQuantities").jfGridSet("removeRow");
                }
            });

            $("#addMaterials").on("click", function () {
               
            });
            $("#deleteMaterials").on("click", function () {
                var rowIndex = $("#Project_OutputSettlementMaterials").jfGridGet("rowIndex");
                if (rowIndex != -1) {
                    $("#Project_OutputSettlementMaterials").jfGridSet("removeRow");
                }
            });

            $('#ContractName').on("click", function () {
                Changjie.layerForm({
                    id: "selectContract",
                    title: "选择合同",
                    url: top.$.rootUrl + '/ProjectModule/ProjectContract/Dialog?projectId=' + projectId,
                    width: 800,
                    hegiht: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                $("#ContractName").mkRemoveValidMessage()
                                page.bindContractInfo(data);
                            }
                        });
                    }
                });
            });

            Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectContract/GetCustomerName', { projectId: projectId }, function (data) {
                if (!$('#CustomerName').val()) {
                    $('#CustomerName').val(data.Name);
                }
            });
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);
            $('#OperatorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {} 
            }).mkselectSet(loginInfo.departmentId);
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_OutputSettlementQuantities').jfGrid({
                headData: [
                    {
                        label: '清单编号', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '项目特征', name: 'Feature', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "Unit", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }, { "t1": "Unit", "t2": "ApprovalQuantities", "to": "ApprovalTotal", "type": "mul", "toTargets": null }, { "t1": "Unit", "t2": "ApprovalQuantities", "to": "VisualProgress", "type": "div", "toTargets": null }], isFirst: true }
                    },
                    {
                        label: '综合单价', name: 'CurrentPrice', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '当前合同数量', name: 'CurrentQuantities', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '累计审批数量', name: 'SettlementQuantities', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '剩余数量', name: 'SurplusAmount', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '本次上报', name: 'l24ed1829e5ab450387cd1a1693d42757', align:"center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [
                            {
                                label: '数量', name: 'Quantities', width: 100,headColor:"blue", cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "CurrentPrice", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false },
                                validity: { "columnA": "Quantities", "columnB": "SurplusAmount","operators":">"}
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData,obj) {
                                        var currentAmount = parseFloat(row["Quantities"]);
                                        var surplusAmount = parseFloat(row["SurplusAmount"]);
                                        if (currentAmount > surplusAmount) {

                                            Changjie.alert.warning("上报数量不能超过剩余数量");
                                            $(obj).css("color", "red");
                                            $(obj).attr("color", "red");
                                            gridverifystate = false;
                                        }
                                        else {
                                            $(obj).css("color", "black");
                                            $(obj).attr("color", "black");
                                            gridverifystate = true;
                                        }
                                    }
                                }
                            },
                            {
                                label: '合价', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'FinishedAmount', required: '0',
                                datatype: 'float', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '本次审批', name: 'l8919b9dcba114568844ca877efd42860',align:'center', width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [{
                            label: '数量', name: 'ApprovalQuantities', width: 100, headColor: "blue", cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '工程量清单', 
                            multiColumnSum: {
                                info: [
                                    { "columns": [{ "column": "CurrentPrice" }, { "column": "ApprovalQuantities" }], "type": "mul", "to": "ApprovalTotal" },
                                    {
                                        "columns":
                                            [
                                            { "column": "ApprovalQuantities" }, { "column": "CurrentQuantities" }
                                        ]
                                        , "type": "div", "to": "VisualProgress","point":"4", "again": { "type": "mul", "value": "100", "valueType":"number" },
                                    }
                                ]
                            }
                            , edit: {
                                type: 'input',
                                change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData,obj) {
                                    var currentAmount = parseFloat(row["ApprovalQuantities"]);
                                    var quantities = parseFloat(row["Quantities"]);
                                    if (currentAmount > quantities) {

                                        Changjie.alert.warning("审批数量不能超过上报数量");
                                        $(obj).css("color", "red");
                                        $(obj).attr("color", "red");
                                        gridverifystate = false;
                                    }
                                    else {
                                        $(obj).css("color", "black");
                                        $(obj).attr("color", "black");
                                        gridverifystate = true;
                                    }
                                }
                            }
                        },
                        {
                            label: '合价', name: 'ApprovalTotal', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'ApplyAmount', required: '0',
                            datatype: 'float', tabname: '工程量清单'
                        },
                        ]
                    },
                    {
                        label: '形象进度(%)', name: 'VisualProgress', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '工程量清单'
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_OutputSettlementQuantities",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    var contractId = $("#ProjectContractId").val();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectContarctQuantities",
                            width: 800,
                            height: 400,
                            title: "选择合同工程量清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectContract/QuantitiesDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectContractQuantitiesId = data[i].ID;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Feature = data[i].Feature;
                                            row.Unit = data[i].Unit;
                                            row.SortCode = data[i].SortCode;

                                            row.CurrentQuantities = data[i].CurrentQuantities;
                                            row.CurrentPrice = data[i].CurrentPrice;

                                            row.SettlementQuantities = data[i].SettlementQuantities;
                                            row.SurplusAmount = parseFloat((row.CurrentQuantities || 0)) - parseFloat((row.SettlementQuantities || 0));

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.ProjectId = projectId;

                                            rows.push(row);
                                        }
                                        $("#Project_OutputSettlementQuantities").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择合同");
                    }
                }
            });
            $('#Project_OutputSettlementMaterials').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '材料编码', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Unit", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }, { "t1": "Unit", "t2": "ApprovalQuantities", "to": "VisualProgress", "type": "div", "toTargets": null }], isFirst: true }
                    },
                    {
                        label: '单价', name: 'CurrentPrice', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '当前合同数量', name: 'CurrentQuantity', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '累计审批量', name: 'SettlementQuantities', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '剩余数量', name: 'SurplusAmount', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '本次上报', name: 'l2d5ea163963f479d9734780f2a8eb916',align:"center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [{
                            label: '数量', name: 'Quantities', width: 100,headColor:"blue",cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "CurrentPrice", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                            , edit: {
                                type: 'input',
                                change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData,obj) {
                                    var currentAmount = parseFloat(row["Quantities"]);
                                    var surplusAmount = parseFloat(row["SurplusAmount"]);
                                    if (currentAmount > surplusAmount) {

                                        Changjie.alert.warning("上报数量不能超过剩余数量");
                                        $(obj).css("color", "red");
                                        $(obj).attr("color", "red");
                                        gridverifystate = false;
                                    }
                                    else {
                                        $(obj).css("color", "black");
                                        $(obj).attr("color", "black");
                                        gridverifystate = true;
                                    }
                                }
                            }
                        },
                        {
                            label: '合价', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '材料清单'
                        },
                        ]
                    },
                    {
                        label: '本次审批', name: 'l603c1a7c164c4eb1afb7dc65a4ff59d6', align:"center", width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [{
                            label: '数量', name: 'ApprovalQuantities', width: 100, headColor: "blue", cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            multiColumnSum: {
                                info: [
                                    { "columns": [{ "column": "CurrentPrice" }, { "column": "ApprovalQuantities" }], "type": "mul", "to": "ApprovalTotal" },
                                    {
                                        "columns": [{ "column": "ApprovalQuantities" }, { "column": "CurrentQuantity" }],
                                        "type": "div", "to": "VisualProgress", "point": "4", "again": { "type": "mul", "value": "100", "valueType": "number" }
                                    }
                                ]
                            },
                            datatype: 'float', tabname: '材料清单'
                            , edit: {
                                type: 'input',
                                change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData,obj) {
                                    var currentAmount = parseFloat(row["ApprovalQuantities"]);
                                    var quantities = parseFloat(row["Quantities"]);
                                    if (currentAmount > quantities) {

                                        Changjie.alert.warning("审批数量不能超过上报数量");
                                        $(obj).css("color", "red");
                                        $(obj).attr("color", "red");
                                        gridverifystate = false;
                                    }
                                    else {
                                        $(obj).css("color", "black");
                                        $(obj).attr("color", "black");
                                        gridverifystate = true;
                                    }
                                } 
                            }
                        },
                        {
                            label: '合价', name: 'ApprovalTotal', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'string', tabname: '材料清单'
                        },
                        ]
                    },
                    {
                        label: '形象进度', name: 'VisualProgress', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '材料清单'
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_OutputSettlementMaterials",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    var contractId = $("#ProjectContractId").val();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectContractMaterials",
                            width: 800,
                            height: 400,
                            title: "选择合同材料清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectContract/MaterialsDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectContractMaterialsId = data[i].ID;
                                            row.ListCode = data[i].ListCode;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].Brand;
                                            row.ModelNumber = data[i].ModelNumber;
                                            row.Unit = data[i].Unit;
                                            row.SortCode = data[i].SortCode;

                                            row.CurrentPrice = data[i].CurrentPrice;
                                            row.CurrentQuantity = data[i].CurrentQuantity;
                                            row.SettlementQuantities = data[i].SettlementQuantities;
                                            row.SurplusAmount = parseFloat((row.CurrentQuantity || 0)) - parseFloat((row.SettlementQuantities || 0));

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.ProjectId = projectId;
                                            rows.push(row);
                                        }
                                        $("#Project_OutputSettlementMaterials").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择合同");
                    }
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                    page.loadContractInfo($("#ProjectContractId").val());
                });
            }
        },
        loadContractInfo: function (contractId) {
            var url = top.$.rootUrl + '/ProjectModule/ProjectContract/GetFormData?keyValue=' + contractId;
            top.Changjie.httpAsyncGet(url, function (data) {
                if (data.code == 200) {
                    page.bindContractInfo(data.data.data);
                }
            });
        },
        bindContractInfo: function (data) {
            $("#ContractName").val(data.Name);
            $("#ProjectContractId").val(data.ID);

        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (gridverifystate == false) {
            Changjie.alert.warning("数据验证失败。");
            return false;
        }
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_OutputSettlement"]').mkGetFormData());
    postData.strproject_OutputSettlementMaterialsList = JSON.stringify($('#Project_OutputSettlementMaterials').jfGridGet('rowdatas'));
    postData.strproject_OutputSettlementQuantitiesList = JSON.stringify($('#Project_OutputSettlementQuantities').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
