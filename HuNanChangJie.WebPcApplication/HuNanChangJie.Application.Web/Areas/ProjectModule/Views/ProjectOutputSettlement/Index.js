﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-24 18:57
 * 描  述：工程合同产值结算
 */
var refreshGirdData;
var projectId = request("projectId");
var formId = request("formId");
var auditPassEvent;
var unauditPassEvent;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增工程合同产值结算',
                    url: top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/GetPageList',
                headData: [
                    { label: "结算单号", name: "Code", width: 150, align: "left"},
                    {
                        label: "结算日期", name: "SettlementDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, "yyyy-MM-dd");
                        }
                    },
                    { label: "计量期数", name: "Econometrics", width: 80, align: "left" },
                    { label: "上报计量产值", name: "CurrentProduction", width: 80, align: "left" },
                    {
                        label: "上报计量产值日期", name: "CurrentProductionDate", width: 120, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, "yyyy-MM-dd");
                        }
                    },
                    {
                        label: "合同名称", name: "ContractName", width: 100, align: "left"
                    },
                    { label: "本次完成产值", name: "FinishedAmount", width: 100, align: "left"},
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left"},
                    { label: "本次审批款", name: "ApplyAmount", width: 100, align: "left"},
                    { label: "客户名称", name: "CustomerName", width: 100, align: "left"},
                    { label: "本次应收款", name: "ReceivableAmount", width: 100, align: "left"},
                    { label: "经办人", name: "OperatorId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "经办人部门", name: "OperatorDepartmentId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                            Changjie.clientdata.getAsync('department', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "摘要", name: "Abstract", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.ProjectID = projectId;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    auditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/Audit', { keyValue: keyValue }, function (data) {
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/UnAudit', { keyValue: keyValue }, function (data) {
        });
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '工程合同产值结算',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/ProjectOutputSettlement/Form?keyValue=' + keyValue + '&viewState=' + viewState + '&projectId=' + projectId + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
