﻿var acceptClick;
var projectId = request("projectId");
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            if (projectId) {
                page.bind();
            }
        },
        bind: function () {

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectLicenseWage/GetDetails?projectId=' + projectId,
                headData: [
                    { label: "证书名称", name: "zzmc_name", width: 100, align: "left" },
                    { label: "证书编号", name: "zz_bianhao", width: 100, align: "left" },
                    { label: "姓名", name: "zz_xinming", width: 100, align: "left" },
                    { label: "身份证号码", name: "zz_shenfengzheng", width: 100, align: "left" },
                    {
                        label: "开始时间", name: "StratDate", width: 100, align: "left", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "结束时间", name: "EndDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    }
                ],
                mainId: 'ID',
                isPage: true,
                height: 362,
                isMultiselect: true
            });
            page.search();

        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload');//
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};