﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-29 17:02
 * 描  述：证照使用工资
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_LicenseWage";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectLicenseWage/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var pageType = "form";
var auditStatus = "";
var auditPassEvent;
var unauditPassEvent;
var refreshGirdData;
var formId = request("formId");
var workflowId = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $("#ProjectID").val(projectId);
            Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectLicenseWage/GetformInfo", { projectId: projectId }, function (res) {

                if (!!res.data) {
                    auditStatus = res.data.AuditStatus;
                    keyValue = res.data.ID;
                    workflowId = res.data.Workflow_ID;
                    type = "edit";
                    $("#edit").show();
                    $("#edit").unbind("click");
                    $("#edit").on("click", function () {
                        page.setViewState(false);
                        $("#audit").hide();
                        $("#unaudit").hide();
                        $("#send").hide();
                        $(this).hide();
                        $("#save").show();
                    });
                    page.setViewState(true);
                    $("#save").hide();

                    if (auditStatus == "2") {
                        $("#audit").hide();
                        $("#unaudit").show();
                        $("#send").hide();
                        $("#edit").hide();
                        $("#save").hide();
                    }
                }
                else {
                    $("#AccrueState").val("未完成计提");
                    keyValue = "";
                    type = "add";
                    $("#edit").hide();
                    $("#audit").hide();
                    $("#unaudit").hide();
                    $("#send").hide();
                    $("#save").show();
                    page.setViewState(false);
                }
            });
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_LicenseWageDetails', "gridId": 'Project_LicenseWageDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $("#AccrueDate").mkselect({
                value: "value",
                text: "text",
                data: function () {
                    var data = [];
                    for (var i = 1; i <= 31; i++) {
                        data.push({value:i,text:"每月"+i+"号,进行成本计提"});
                    }
                    return data;
                }(),
                dfvalue:1
            });
            $("#ProjectSubjectId").mkselect({
                url: top.$.rootUrl + "/ProjectModule/ProjectSubject/GetSubjectList?projectId=" + projectId,
                value: "ID",
                text: "FullName",
                title: "FullName"
            });
            $("#addZhengZhao").on("click", function () {
          
            });
            $("#deleteZhengZhao").on("click", function () {
                var rowIndex = $("#Project_LicenseWageDetails").jfGridGet("rowIndex");
                if (rowIndex != -1) {
                    $("#Project_LicenseWageDetails").jfGridSet("removeRow");
                }
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_LicenseWageDetails').jfGrid({
                headData: [
                    { label: '持证人', name: 'zz_xinming', width: 100 },
                    { label: '证件号', name: 'zz_bianhao', width: 100 },
                    { label: '证件名称', name: 'zzmc_name', width: 100 },
                    { label: '身份证号', name: 'zz_shenfengzheng', width: 100 },
                    {
                        label: '开始时间', name: 'StratDate', width: 100, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '工资详情'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                                page.updateDateRow(row, index);
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '结束时间', name: 'EndDate', width: 100, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '工资详情'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                                page.updateDateRow(row, index);
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '每月工资', name: 'Wage', width: 100, aggtype: 'sum', aggcolumn: 'MonthTotalAmount', required: '1',
                        datatype: 'float', tabname: '工资详情'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                page.updateDateRow(row, index);
                                var total = 0;
                                for (var i = 0; i < rows.length; i++) {
                                    var row = rows[i];
                                    total += Changjie.getFloatValue(row.TotalAmount);
                                }
                                $("#TotalAmount").val(total);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '未满月部分工资', name: 'ComplementWage', width: 100, aggtype: 'sum', aggcolumn: 'LastTotalAmount', required: '0',
                        datatype: 'float', tabname: '工资详情'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                page.updateDateRow(row, index);
                                var total = 0;
                                for (var i = 0; i < rows.length; i++) {
                                    var row = rows[i];
                                    total += Changjie.getFloatValue(row.TotalAmount);
                                }
                                $("#TotalAmount").val(total);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '时间小计', name: 'TimeCount', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工资详情'
                    },
                    {
                        label: '工资小计', name: 'TotalAmount', width: 100, aggtype: 'sum', aggcolumn: 'TotalAmount', required: '0',
                        datatype: 'string', tabname: '工资详情'
                    },
                    {
                        label: '行备注', name: 'Remark', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工资详情'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_LicenseWageDetails",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectContractMaterials",
                        width: 800,
                        height: 400,
                        title: "选择证照",
                        url: top.$.rootUrl + "/SystemModule/BASE_CJ_ZhengZhao/Dialog",
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.ZhengZhaoId = data[i].ID;
                                        row.ProjectId = projectId;

                                        row.zzmc_name = data[i].zzmc_name;
                                        row.zz_bianhao = data[i].zz_bianhao;
                                        row.zz_xinming = data[i].zz_xinming;
                                        row.zz_shenfengzheng = data[i].zz_shenfengzheng;

                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        rows.push(row);
                                    }
                                    $("#Project_LicenseWageDetails").jfGridSet("addRows", rows);
                                }
                            });
                        }
                    });
                }
            });

            $("#save").on("click", function () {
                var postData = getFormData();
                if (postData == false) return false;
                if (type == "add") {
                    keyValue = mainId;
                }
                $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectLicenseWage/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                    // 保存成功后才回调
                    if (res.code == Changjie.httpCode.success) {
                        type = "edit";
                        $("#edit").show();
                        $("#audit").show();
                        $("#unaudit").show();
                        $("#send").show();
                        $("#save").hide();
                        page.setViewState(true);
                    }
                });
            });
            $("#batchStartDate").on("blur", function () {
                var start = $(this).val();
                var end = $("batchStartDate").val();
                var duration = null;
                var months = null;
                if (start && end) {
                    var time = GetDateTimeDiff(start, end);
                    duration = time.Months;
                    months = time.Months;
                    var days = time.Days;
                    duration = "共" + months + "月,零(" + (days - months * 30) + "天)";
                }
                page.batchSetValue(start, "StratDate", duration, months);
                var count = months + (days > 0 ? 1 : 0);
                $("#TotalCount").val(count);
            });
            $("#batchEndDate").on("blur", function () {
                var start = $("#batchStartDate").val();
                var end = $(this).val();
                var duration = null;
                var months = null;
                if (start && end) {
                    var time = GetDateTimeDiff(start, end);
                    duration = time.Months;
                    months = time.Months;
                    var days = time.Days;
                    duration = "共" + months + "月,零(" + (days - months * 30) + "天)";
                    var count = months + (days > 0 ? 1 : 0);
                    $("#TotalCount").val(count);
                }
                page.batchSetValue($(this).val(), "EndDate", duration, months);
            });
            $("#batchWage").on("input propertychange", function () {
                var start = $("#batchStartDate").val();
                var end = $("#batchEndDate").val();
                var duration = null;
                var months = null;
                if (start && end) {
                    var time = GetDateTimeDiff(start, end);
                    duration = time.Months;
                    months = time.Months;
                    var days = time.Days;
                    duration = "共" + months + "月,零(" + (days - months * 30) + "天)";
                    var count = months + (days > 0 ? 1 : 0);
                    $("#TotalCount").val(count);
                }
                page.batchSetValue($(this).val(), "Wage", duration, months);
            });
            $("#batchComplement").on("input propertychange", function () {
                var start = $("#batchStartDate").val();
                var end = $("#batchEndDate").val();
                var duration = null;
                var months = null;
                if (start && end) {
                    var time = GetDateTimeDiff(start, end);
                    duration = time.Months;
                    months = time.Months;
                    var days = time.Days;
                    duration = "共" + months + "月,零(" + (days - months * 30) + "天)";
                    var count = months + (days > 0 ? 1 : 0);
                    $("#TotalCount").val(count);
                }
                page.batchSetValue($(this).val(), "ComplementWage", duration, months);
            });
        },
        updateDateRow: function (row, index) {
            var start = row.StratDate;
            var end = row.EndDate;
            var duration = null;
            var months = null;
            if (start && end) {
                var time = GetDateTimeDiff(start, end);
                duration = time.Months;
                months = time.Months;
                var days = time.Days;
                duration = "共" + months + "月,零(" + (days - months * 30) + "天)";
                row.TimeCount = duration;
                row.Months = Changjie.getFloatValue(months);
                var wage = Changjie.getFloatValue(row.Wage);
                row.TotalAmount = months * wage + Changjie.getFloatValue(row.ComplementWage);
                $("#Project_LicenseWageDetails").jfGridSet("updateRow", index);
            }
        },
        batchSetValue: function (value, columnName, duration, months) {
            var rows = $("#Project_LicenseWageDetails").jfGridGet("rowdatas");
            var mouthTotal = 0;
            var listTotal = 0;
            var total = 0;
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                row[columnName] = value;
                if (duration) {
                    row.TimeCount = duration;
                }
                row.Months = Changjie.getFloatValue(months);
                var wage = Changjie.getFloatValue(row.Wage);
                var complementWage = Changjie.getFloatValue(row.ComplementWage)
                row.TotalAmount = months * wage + complementWage;
                mouthTotal += wage;
                listTotal += complementWage;
               
                total += row.TotalAmount;
            }
   
            $("#MonthTotalAmount").val(mouthTotal);
            $("#LastTotalAmount").val(listTotal);
            $("#TotalAmount").val(total);

            $("#Project_LicenseWageDetails").jfGridSet("refreshdata");
        },
        setViewState: function (flag) {
            if (flag) {
                $("[viewState='readOnly']").attr("readonly", "readonly");
                $("[viewState='readOnly']").addClass("readonly");
                $("#btn_uploadfile").attr("disabled", "disabled");
                $("#btn_delfile").attr("disabled", "disabled");

            }
            else {
                $("[viewState='readOnly']").removeAttr("readonly");
                $("[viewState='readOnly']").removeClass("readonly");
                $("#btn_uploadfile").removeAttr("disabled");
                $("#btn_delfile").removeAttr("disabled");
            }
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectLicenseWage/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                            $("#AccrueState").val(data[id].AccrueState == true ? "计提完成" : "未完成计提");
                        }
                    }
                });
            }
        },
    };

    function GetDateTimeDiff(startTime, endTime) {
        startTime = new Date(startTime.replace(/-/g, "/"));
        endTime = new Date(endTime.replace(/-/g, "/"));
        var retValue = {};

        var date3 = endTime.getTime() - startTime.getTime();  //时间差的毫秒数

        //计算出相差天数
        var days = Math.floor(date3 / (24 * 3600 * 1000));
        retValue.Days = days;

        var years = Math.floor(days / 365);
        retValue.Years = years;

        var months = Math.floor(days / 30);
        retValue.Months = months;

        //计算出小时数
        var leave1 = date3 % (24 * 3600 * 1000);    //计算天数后剩余的毫秒数
        var hours = Math.floor(leave1 / (3600 * 1000));
        retValue.Hours = hours;

        //计算相差分钟数
        var leave2 = leave1 % (3600 * 1000);        //计算小时数后剩余的毫秒数
        var minutes = Math.floor(leave2 / (60 * 1000));
        retValue.Minutes = minutes;

        //计算相差秒数
        var leave3 = leave2 % (60 * 1000);      //计算分钟数后剩余的毫秒数
        var seconds = Math.round(leave3 / 1000);
        retValue.Seconds = seconds;

        var strTime = "";
        if (years >= 1) {
            strTime = years + "年前";
        } else if (months >= 1) {
            strTime = months + "个月前";
        } else if (days >= 1) {
            strTime = days + "天前";
        } else if (hours >= 1) {
            strTime = hours + "小时前";
        } else {
            strTime = minutes + "分钟前";
        }
        retValue.PubTime = strTime;
        return retValue;
    };

    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectLicenseWage/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
    refreshGirdData = function () {
        page.init();
    };
    auditPassEvent = function () {
        $("#edit").hide();
        $("#audit").hide();
        $("#unaudit").show();
        $("#send").hide();
        $("#save").hide();
    };
    unauditPassEvent = function () {
        $("#edit").show();
        $("#audit").show();
        $("#unaudit").hide();
        $("#send").show();
        $("#save").hide();
    };
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    var data = $('[data-table="Project_LicenseWage"]').mkGetFormData();
    if (data.AccrueState == "计提完成") {
        data.AccrueState = true;
    }
    else {
        data.AccrueState = false;
    }
    postData.strEntity = JSON.stringify(data);
    postData.strproject_LicenseWageDetailsList = JSON.stringify($('#Project_LicenseWageDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
