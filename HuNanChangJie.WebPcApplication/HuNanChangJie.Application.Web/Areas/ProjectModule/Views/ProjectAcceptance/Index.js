﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-24 09:48
 * 描  述：项目验收
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request("projectId");
var auditPassEvent;
var unauditPassEvent;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {

                if ($('#gridtable').jfGridGet("rowdatas").length > 0) {
                    Changjie.alert.warning("同一项目只能做一次验收！");
                }
                else {
                    Changjie.layerForm({
                        id: 'form',
                        title: '新增项目验收',
                        url: top.$.rootUrl + '/ProjectModule/ProjectAcceptance/Form?projectId=' + projectId,
                        width: 1000,
                        height: 800,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectAcceptance/GetPageList',
                headData: [
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left" },
                    { label: "客户名称", name: "CustomerName", width: 100, align: "left" },
                    { label: "合同签订金额", name: "InitAmount", width: 100, align: "left" },
                    {
                        label: "验收日期", name: "AcceptanceDate", width: 100, align: "left", formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "合同结算金额", name: "SettlementAmount", width: 100, align: "left" },
                    {
                        label: "实际开始时间", name: "StartDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: "实际完工时间", name: "EndDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "累计开票", name: "InvioceAmount", width: 100, align: "left" },
                    { label: "累计收款", name: "ReceivedAmount", width: 100, align: "left" },
                    {
                        label: "质保起始日期", name: "QualityStartDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "实际工期", name: "PracticalDuration", width: 100, align: "left" },
                    {
                        label: "验收类型", name: "AcceptanceType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'AcceptanceType',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                ],
                mainId: 'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.ProjectId = projectId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };

    auditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectAcceptance/Audit', { keyValue: keyValue }, function (data) {
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectAcceptance/UnAudit', { keyValue: keyValue }, function (data) {
        });
    };

    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectAcceptance/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id:'form',
            title: title + '项目验收',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/ProjectModule/ProjectAcceptance/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&projectId=" + projectId + "&formId=" + formId, 
            width: 1000,
            height: 800,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
