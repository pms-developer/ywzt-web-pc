﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-24 09:48
 * 描  述：项目验收
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_Acceptance";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectAcceptance/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "newinstance",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $("#StartDate").on("input change", function () {
                page.setDuration();
            });
            $("#EndDate").on("input change", function () {
                page.setDuration();
            });
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);
            $('#OperatorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            }).mkselectSet(loginInfo.departmentId);
            $('#AcceptanceType').mkDataItemSelect({ code: 'AcceptanceType' });
        },
        setDuration: function () {
            var result = page.getDays($("#StartDate").val(), $("#EndDate").val());
            result = result || 0;
            $("#PracticalDuration").val(result);
        },
        initData: function () {
            page.loadContractInfo();
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectAcceptance/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
        getDays: function (s1, s2) {
            var d1 = new Date(s2.replace(/-/g, '/'));
            var d2 = new Date(s1.replace(/-/g, '/'));
            var ms = Math.abs(d2.getTime() - d1.getTime());//毫秒
            var d = ms / 1000 / 60 / 60 / 24;//转为天
            return d;
        },
        loadContractInfo: function () {
            Changjie.httpAsyncGet(top.$.rootUrl + "/ProjectModule/ProjectContract/GetContractInfo?projectId=" + projectId, function (res) {
                if (res.code == Changjie.httpCode.success) {
                    var data = res.data;
                    $("#CustomerName").val(data.CustomerName);
                    $("#InitAmount").val(data.InitAmount);
                    $("#SettlementAmount").val(data.SettlementAmount);
                    $("#InvioceAmount").val(data.InvioceAmount);
                    $("#ReceivedAmount").val(data.ReceivedAmount);
                    $("#TotalDuration").val(data.TotalDuration);
                }
            });
        },
    };

    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectAcceptance/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData()),
        deleteList: JSON.stringify(deleteList),
    };
    return postData;
}
