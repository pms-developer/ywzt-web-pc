﻿var subcontractId = request("subcontractId");
var warehouseId = request("warehouseId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();

        },
        bind: function () {

            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.MaterialsName = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });

            $('#gridtable').jfGrid({
                url: top.$.rootUrl + '/ProjectModule/Warehouse/GetMaterialsInfoList',
                headData: [
                    { label: "材料编码", name: "Code", width: 130, align: "left" },
                    { label: "材料名称", name: "Name", width: 150, align: "left" },
                    { label: "规格型号", name: "Model", width: 100, align: "center" },
                    {
                        label: "品牌", name: "BrandId", width: 100, align: "center",

                    },
                    {
                        label: "产地", name: "Origin", width: 70, align: "center",

                    },
                    {
                        label: "计量单位", name: "UnitId", width: 80, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code: 'MaterialsUnit',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }

                    },
                    { label: "单价", name: "Price", width: 100, align: "center" },
                    { label: "库存数量", name: "Inventory", width: 120, align: "center" },
                    { label: "总价", name: "SalePrice", width: 120, align: "center" },
                    //{ label: "预算成本价", name: "BugetCost", width: 100, align: "left" },
                    //{
                    //    label: "成本科目", name: "SubjectCost", width: 100, align: "left",
                    //    formatterAsync: function (callback, value, row, op, $cell) {
                    //        Changjie.clientdata.getAsync('dataItem', {
                    //            key: value,
                    //            code: 'SubjectCost',
                    //            callback: function (_data) {
                    //                callback(_data.text);
                    //            }
                    //        });
                    //    }
                    //},

                ],
                mainId: 'ID',
                height: 500,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            //if (subcontractId)
            //    param.subcontractId = subcontractId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param), warehouseId: warehouseId });
        }
    };
    page.init();
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };
};