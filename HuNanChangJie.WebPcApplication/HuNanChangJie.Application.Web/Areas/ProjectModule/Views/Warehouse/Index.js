﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-10-19 11:32
 * 描  述：仓库管理
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $("#revocation").hide()
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 100, 950);
            $('#ProjectId').mkDataSourceSelect({ code: 'BASE_XMLB', value: 'project_id', text: 'projectname' });
            $('#ManagerId').mkUserSelect(0);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增仓库',
                    url: top.$.rootUrl + '/ProjectModule/Warehouse/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            $('#cang').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '库存明细',
                        url: top.$.rootUrl + '/ProjectModule/Warehouse/MaterialsListDialog?warehouseId=' + keyValue,
                        width: 1000,
                        height: 630, isShowConfirmBtn: false,
                        callBack: function (id) {
                            //return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/Warehouse/GetPageList',
                headData: [
                    { label: "仓库编码", name: "Code", width: 100, align: "left" },
                    { label: "仓库名称", name: "Name", width: 200, align: "left" },
                    {
                        label: "关联项目", name: "ProjectId", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('custmerData', {
                                url: '/SystemModule/DataSource/GetDataTable?code=' + 'BASE_XMLB',
                                key: value,
                                keyId: 'project_id',
                                callback: function (_data) {
                                    callback(_data['projectname']);
                                }
                            });
                        }
                    },
                    {
                        label: "管理员", name: "ManagerId", width: 80, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    { label: "库存总价", name: "OccupyMoney", width: 100, align: "left" },

                    { label: "管理员电话", name: "Telphone", width: 100, align: "left" },
                    { label: "仓库地址", name: "Address", width: 280, align: "left" },
                    { label: "备注", name: "Remark", width: 100, align: "left" },
                ],
                dblclick(row) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '库存明细',
                        url: top.$.rootUrl + '/ProjectModule/Warehouse/MaterialsListDialog?warehouseId=' + row.ID,
                        width: 1000,
                        height: 630, isShowConfirmBtn: false,
                        callBack: function (id) {
                            //return top[id].acceptClick(refreshGirdData);
                        }
                    });
                },
                mainId: 'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            if (projectId) {
                param.ProjectId = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.httpGet(top.$.rootUrl + '/ProjectModule/Warehouse/IsCanDelete', { keyValue: keyValue }, function (data) {
            if (data.code == 200) {
                if (data.data.iscan) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/Warehouse/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
                else {
                    top.Changjie.alert.error("当前仓库库存不为0,不能删除");
                }
            }
            else {
                top.Changjie.alert.error(data.info);
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '仓库',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/ProjectModule/Warehouse/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
