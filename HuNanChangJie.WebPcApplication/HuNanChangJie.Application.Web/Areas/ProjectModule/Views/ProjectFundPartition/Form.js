﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-31 10:17
 * 描  述：成本设置
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_FundPartition";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectFundPartition/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var pageType = "form";
var auditStatus = "";
var auditPassEvent;
var unauditPassEvent;
var formId = request("formId");
var refreshGirdData;
var workflowId = "";

var bootstrap = function ($, Changjie) {
   
    "use strict";
    var page = {
        init: function () {
            $("#ProjectID").val(projectId);
            Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectFundPartition/GetformInfo", { projectId: projectId }, function (res) {
                if (!!res.data) {
                    auditStatus = res.data.AuditStatus;
                    keyValue = res.data.ID;
                    workflowId = res.data.Workflow_ID;
                    type = "edit";
                    $("#edit").show();
                    $("#edit").unbind("click");
                    $("#edit").on("click", function () {
                        page.setViewState(false);
                        $("#audit").hide();
                        $("#unaudit").hide();
                        $("#send").hide();
                        $(this).hide();
                        $("#save").show();
                    });
                    page.setViewState(true);
                    $("#save").hide();

                    if (auditStatus == "2") {
                        $("#audit").hide();
                        $("#unaudit").show();
                        $("#send").hide();
                        $("#edit").hide();
                        $("#save").hide();
                    }
                }
                else {
                    keyValue = "";
                    type = "add";
                    $("#edit").hide();
                    $("#audit").hide();
                    $("#unaudit").hide();
                    $("#send").hide();
                    $("#save").show();
                    page.setViewState(false);
                }
            });


            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_FundPartitionDetails', "gridId": 'Project_FundPartitionDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },


        //Page绑定方法，如绑定单选框
        bind: function () {
            $('#IsNative').mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#TaxType').mkRadioCheckbox({
                type: 'radio',
                code: 'TaxType',
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_FundPartitionDetails').jfGrid({
                headData: [
                    {
                        label: '成本科目', name: 'ProjectSubjectId', width: 300, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'string', tabname: '详情'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            op: {
                                data: function () {
                                    var data = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectSubject/GetSubjectList?projectId=" + projectId);
                                    return data.data;
                                }(),
                                value: "ID",
                                text: "FullName",
                                title: "FullName"
                            }
                        }
                    },
                    {
                        label: '是否自动计算', name: 'IsAuto', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '详情',
                        formatter: function (cellvalue) {
                            var val = JSON.parse(cellvalue || "false");
                            return val == true ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            change: function (row, rowIndex, oldValue, colname, rows, para) {
                                for (var i = 0; i < rows.length; i++) {
                                    var info = rows[i];
                                    info.IsAuto = row.IsAuto;
                                }
                                $("#Project_FundPartitionDetails").jfGridSet("refreshdata", rows);
                            },
                            init: function (data, $edit) {
                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: true// 默认选中项
                        }
                    },
                    {
                        label: '控件点', name: 'ControlPoint', width: 150, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '详情'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData, rows) {
                                for (var i = 0; i < rows.length; i++) {
                                    var info = rows[i];
                                    info.ControlPoint = row.ControlPoint;
                                }
                                $("#Project_FundPartitionDetails").jfGridSet("refreshdata", rows);
                            },
                            op: {
                                data: [
                                    { Type: "Invoice", Value: "开票登记" },
                                    { Type: "Receipt", Value: "工程合同收款单" },
                                    { Type: "PayTaxes", Value: "完税凭证" }
                                ],
                                value: "Type",
                                text: "Value",
                                title: "Value"
                            }
                        }
                    },
                    {
                        label: '公式', name: 'Formula', width: 200, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '详情'
                        , edit: {
                            type: "layer",
                            init: function (row, $obj, rownum) {
                                top.FormulaSettings = JSON.parse((row.FormulaSettings || "{}"));
                                return "?keyValue=FormulaSettings";
                            },
                            change: function (data, rownum, selectData) {
                                data.Formula = selectData.text;
                                data.FormulaSettings = JSON.stringify(selectData);
                                if (data.EditType != 1) {
                                    data.EditType = 2;
                                }
                                $('#Project_FundPartitionDetails').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 800,
                                height: 600,
                                title: '公式设计器',
                                customurl: top.$.rootUrl + '/ProjectModule/ProjectFundPartition/FormulaSettings'
                            }
                        }
                    },
                    {
                        label: '说明', name: 'Remark', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '详情'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '测试结果', name: 'TestResult', width: 300, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '详情'
                    },
                ],
                mainId: "ID",
                bindTable: "Project_FundPartitionDetails",
                isEdit: true,
                height: 300,
                rowdatas: [
                    {
                        "SortCode": 1,
                        "rowState": 1,
                        "IsAuto": true,
                        "ID": Changjie.newGuid(),
                        "EditType": 1,
                    }],
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.ProjectId = projectId;
                    row.SortCode = rows.length;
                    row.IsAuto = true;
                    row.EditType = 1;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });

           
            //保存按钮事件
            $("#save").on("click", function () {
              
                
                var postData = getFormData();
                if (postData == false) return false;
                if (type == "add") {
                    keyValue = mainId;
                }
                $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectFundPartition/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                    // 保存成功后才回调
                    if (res.code == Changjie.httpCode.success) {
                       

                        type = "edit";
                        $("#edit").show();
                        $("#audit").show();
                        $("#unaudit").show();
                        $("#send").show();
                        $("#save").hide();
                        page.setViewState(true);
                    }
                });
            });
            $("#formulaTest").on("click", function () {
                var rows = $("#Project_FundPartitionDetails").jfGridGet("rowdatas");
                if (rows.length > 0) {
                    Changjie.httpAsyncGet(top.$.rootUrl + '/ProjectModule/ProjectFundPartition/FormulaTest?projectid=' + projectId, function (res) {
                        var data = res.data;
                        if (data) {
                            for (var item in data) {
                                var info = data[item];
                                for (var i = 0; i < rows.length; i++) {
                                    var row = rows[i];
                                    if (info.ID != row.ID) continue;
                                    var testInfo = "计算结果:" + info.Result;
                                    testInfo += "   运行公式:" + info.Runnable;
                                    testInfo += "   解析后公式:" + info.Analysis;
                                    row.TestResult = testInfo;
                                }
                            }
                            $("#Project_FundPartitionDetails").jfGridSet("refreshdata");
                        }
                    });
                }
            });
        },
        setViewState: function (flag) {
            if (flag) {
                $("[viewState='readOnly']").attr("readonly", "readonly");
                $("[viewState='readOnly']").addClass("readonly");
                $("#btn_uploadfile").attr("disabled", "disabled");
                $("#btn_delfile").attr("disabled", "disabled");

            }
            else {
                $("[viewState='readOnly']").removeAttr("readonly");
                $("[viewState='readOnly']").removeClass("readonly");
                $("#btn_uploadfile").removeAttr("disabled");
                $("#btn_delfile").removeAttr("disabled");
            }
        },
        initData: function () {

            //tables = [];

            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectFundPartition/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        //alert(id);

                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                            auditStatus = data[id].AuditStatus;
                            workflowId = data[id].Workflow_ID;

                            if (auditStatus == "2") {
                                page.setViewState(true);
                                $("#audit").hide();
                                $("#unaudit").show();
                                $("#send").hide();
                                $("#editInfo").hide();
                                $("#save").hide();
                            } else {
                                page.setViewState(false);
                                $("#editInfo").show();
                                $("#audit").show();
                                $("#unaudit").hide();
                                $("#send").show();
                                $("#save").hide();
                            }
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectFundPartition/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
    refreshGirdData = function () {

        //Modified by Wangwei 09/22/2020
        $('#IsNative').empty();
        $('#TaxType').empty();

        page.init();
    };
    auditPassEvent = function () {
        $("#edit").hide();
        $("#audit").hide();
        $("#unaudit").show();
        $("#send").hide();
        $("#save").hide();
    };
    unauditPassEvent = function () {
        $("#edit").show();
        $("#audit").show();
        $("#unaudit").hide();
        $("#send").show();
        $("#save").hide();
    };
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    var data = $('#Project_FundPartitionDetails').jfGridGet('rowdatas');
 
    postData.strEntity = JSON.stringify($('[data-table="Project_FundPartition"]').mkGetFormData());
    postData.strproject_FundPartitionDetailsList = JSON.stringify($('#Project_FundPartitionDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
