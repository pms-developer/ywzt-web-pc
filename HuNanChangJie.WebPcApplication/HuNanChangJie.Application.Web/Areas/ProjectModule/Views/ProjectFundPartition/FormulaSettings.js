﻿
var acceptClick;
var keyValue = request('keyValue');
var gridSetting = top[keyValue];
var bootstrap = function ($, Changjie) {
    "use strict";
    var row = gridSetting;
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            var formulas = Changjie.httpGet(top.$.rootUrl + "/SystemModule/BaseFormulaSettings/GetList").data;
            $("#formulaList").formulaDesigner({ formulas: formulas,value:row.html});
        }
    };

    page.init();

    acceptClick = function (callBack) {
        var editorid = "custmerform_designer_layout_area_formulaList";
        var ue = UE.getEditor(editorid);
        var $html;
        var html = "";
        ue.ready(function () {
            html = ue.getContent()
            $html = $(html);
        });
        var formulas = {};
        formulas.infoList = [];
        formulas.html = html;
        $html.find("a").each(function () {
            var id = $(this).attr("title");
            formulas.infoList.push({ id: id , name: $(this).html() });
            $(this).after(id);
            $(this).remove();
        });
        formulas.value = $html.html();
        formulas.text = $html.html();
        for (var i = 0; i < formulas.infoList.length; i++) {
            var info = formulas.infoList[i];
            formulas.text = formulas.text.replace(info.id, info.name);
        }
        callBack(formulas);
        return true;
    };
}