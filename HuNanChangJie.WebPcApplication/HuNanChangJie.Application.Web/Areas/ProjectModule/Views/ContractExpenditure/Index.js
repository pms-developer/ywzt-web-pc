﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-10-30 14:35
 * 描  述：合同支出
 */
var refreshGirdData;
var projectId = request("projectId");
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增合同支出',
                    url: top.$.rootUrl + '/ProjectModule/ContractExpenditure/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ContractExpenditure/GetPageList',
                headData: [
                    { label: "关联项目", name: "ProjectName", width: 100, align: "left"},
                    { label: "合同类型1:工程合同，2：分包合同，3：采购合同", name: "ContactType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: '',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "合同ID", name: "ContractId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: '',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "支出类型1:罚款，2：其他支出", name: "ExpenditureType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: '',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "金额", name: "Amount", width: 100, align: "left"},
                    { label: "支出时间", name: "ExpenditureTime", width: 100, align: "left"},
                    { label: "经办人", name: "OperatorId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "说明", name: "Remark", width: 100, align: "left"},
                    { label: "备注", name: "Description", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ContractExpenditure/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '合同支出',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/ContractExpenditure/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
