﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-10-30 14:35
 * 描  述：合同支出
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="Contract_Expenditure";
var processCommitUrl=top.$.rootUrl + '/ProjectModule/ContractExpenditure/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var contractData = null;//合同数据
var contarctType = 0;//合同类型
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#ContactType').mkselect({
                text: 'text',
                value: 'id',
                data: [
                    { id: 1, text:"工程合同"},
                    { id: 2, text: "分包合同" },
                    { id: 3, text: "采购合同" },
                ]
            });
            $('#ContactType').on("change", function () {
                var type = $(this).mkselectGet();
                contractData = [];
                switch (type) {
                    case 1:
                            var data = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectContract/GetList", { projectId: projectId }).data;
                            for (var _i in data) {
                                var item = data[_i];
                                var info = {id:item.ID,name:item.Name};
                                contractData.push(info);
                            }
                        break;
                    case 2: 
                        var data = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectSubcontract/GetSubcontractList", { projectId: projectId }).data;
                        for (var _i in data) {
                            var item = data[_i];
                            var info = { id: item.ID, name: item.Name };
                            contractData.push(info);
                        }
                        break;
                    case 3:
                        var data = Changjie.httpGet(top.$.rootUrl + "/PurchaseModule/PurchaseContract/GetList", { projectId: projectId }).data;
                        for (var _i in data) {
                            var item = data[_i];
                            var info = { id: item.ID, name: item.Name };
                            contractData.push(info);
                        }
                        break;
                }
                contarctType = type;
                $('#ContractId').mkselectRefresh({ data: contractData });
            });
            $('#ContractId').mkselect({
                text: 'name',
                value: 'id',
                data: contractData||[]
            });
            $('#ExpenditureType').mkselect({
                text: 'text',
                value: 'id',
                data: [
                    { id: 1, text: "罚款" },
                    { id: 2, text: "其他支出" }
                ]
            });;
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"newinstance",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ContractExpenditure/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ContractExpenditure/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
     return postData;
}
