﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-04-07 15:30
 * 描  述：投标立项
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables = [];
var formId = request("formId");
var mainTable="Project_Bid";
var processCommitUrl=top.$.rootUrl + '/ProjectModule/ProjectBid/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var pageType = "form";
var auditStatus = "";
var auditPassEvent;
var unauditPassEvent;
var refreshGirdData;
var workflowId = "";
 auditPassEvent = function (infoid) {
    window.location.reload();
}
 unauditPassEvent = function (infoid) {
    window.location.reload();
}
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            var projectData = Changjie.httpGet(top.$.rootUrl + '/ProjectModule/Project/GetCustomerInfo?projectId=' + projectId).data;
            $("#ProjectID").val(projectData.ID);
            $("#ProjectName").val(projectData.ProjectName);
            $("#CustomerName").val(projectData.CustomerName);
            $("#Linkman").val(projectData.CustomerContacts);
            $("#Telphone").val(projectData.CustomerPhone);
            $("#Phone").val(projectData.CustomerPhone);
            $("#BidAddress").val(projectData.CustomerAddress);
            $("#PostCode").val(projectData.CustomerPostCode);

            Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectBid/GetFormInfo", { projectId: projectId }, function (res) {
                if (!!res.data) {
                    auditStatus = res.data.AuditStatus;
                    keyValue = res.data.ID;
                    workflowId = res.data.Workflow_ID;
                    type = "edit";
                    $("#edit").show();
                    $("#edit").unbind("click");
                    $("#edit").on("click", function () {
                        page.setViewState(false);
                        $("#audit").hide();
                        $("#unaudit").hide();
                        $("#send").hide();
                        $(this).hide();
                        $("#save").show();
                    });
                    page.setViewState(true);
                    $("#save").hide();

                    if (auditStatus == "2") {
                        $("#audit").hide();
                        $("#unaudit").show();
                        $("#send").hide();
                        $("#edit").hide();
                        $("#save").hide();
                    }
                }
                else {
                    keyValue = "";
                    type = "add";
                    $("#edit").hide();
                    $("#audit").hide();
                    $("#unaudit").hide();
                    $("#send").hide();
                    $("#save").show();
                    page.setViewState(false);

                }
            });
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
                mainId = top.Changjie.newGuid();
                type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#OperatorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {} 
            });
            $('#BiddingType').mkDataItemSelect({ code: 'BiddingType' });
            $('#EvaluationType').mkDataItemSelect({ code: 'EvaluationType' });
            $('#IsAffect').html("").mkRadioCheckbox({
                type: 'radio',
                code: 'BESF',
            });
            $('#DepositType').mkDataItemSelect({ code: 'DepositType' });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "newinstance",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $("#save").on("click", function () {
                var postData = getFormData();
                if (postData == false) return false;
                if (type == "add") {
                    keyValue = mainId;
                }
                $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectBid/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                    // 保存成功后才回调
                    if (res.code == Changjie.httpCode.success) {
                        type = "edit";
                        $("#edit").show();
                        $("#audit").show();
                        $("#unaudit").show();
                        $("#send").show();
                        $("#save").hide();
                        page.setViewState(true);
                    }
                });
            });
        },
        setViewState: function (flag) {
            if (flag) {
                $("[viewState='readOnly']").attr("readonly", "readonly");
                $("[viewState='readOnly']").addClass("readonly");
                $("#btn_uploadfile").attr("disabled", "disabled");
                $("#btn_delfile").attr("disabled", "disabled");

            }
            else {
                $("[viewState='readOnly']").removeAttr("readonly");
                $("[viewState='readOnly']").removeClass("readonly");
                $("#btn_uploadfile").removeAttr("disabled");
                $("#btn_delfile").removeAttr("disabled");
            }
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectBid/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                            auditStatus = data[id].AuditStatus;
                            workflowId = data[id].Workflow_ID;

                            if (auditStatus == "2") {
                                page.setViewState(true);
                                $("#audit").hide();
                                $("#unaudit").show();
                                $("#send").hide();
                                $("#editInfo").hide();
                                $("#save").hide();
                            } else {
                                page.setViewState(false);
                                $("#editInfo").show();
                                $("#audit").show();
                                $("#unaudit").hide();
                                $("#send").show();
                                $("#save").hide();
                            }
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectBid/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
    refreshGirdData = function () {
        page.init();
    };
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
     return postData;
}
