﻿/* *  
 * 创建人：超级管理员
 * 日  期：2022-04-27 17:00
 * 描  述：店铺基本信息管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="base_shopinfo";
var processCommitUrl=top.$.rootUrl + '/ProjectModule/BaseShopInfoManage/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'base_shopinfo_account',"gridId":'base_shopinfo_account'});
               subGrid.push({"tableName":'base_shopinfo_criticalpath',"gridId":'base_shopinfo_criticalpath'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            $('#Company_ID').mkCompanySelect({});
            $('#DepartmentId').mkDepartmentSelect();
            $('#DepartmentId').mkselect({
                type: 'tree',
                allowSearch: true
            });
            //$('#Company_ID').on('change', function () {
            //    var value = $(this).mkselectGet();
            //    //$('#DepartmentId').mkselectRefresh({
            //    //    url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
            //    //    param: { companyId: value }
            //    //});
            //});
            $('#ManagerId').mkselect({
                value: 'F_UserId',
                text: 'F_RealName',
                title: 'F_RealName',
                allowSearch: true
            });
            $('#DepartmentId').on('change', function () {
                var value = $(this).mkselectGet();
                if (value == '')
                {
                    $('#ManagerId').mkselectRefresh({
                        url: '',
                        data: []
                    });
                }
                else
                {
                    $('#ManagerId').mkselectRefresh({
                        url: top.$.rootUrl + '/OrganizationModule/User/GetList',
                        param: { departmentId: value }
                    });
                }
            })
            $('#ShopManagerId').mkselect({
                value: 'F_UserId',
                text: 'F_RealName',
                title: 'F_RealName',
                allowSearch: true
            });
            $('#DepartmentId').on('change', function () {
                var value = $(this).mkselectGet();
                if (value == '')
                {
                    $('#ShopManagerId').mkselectRefresh({
                        url: '',
                        data: []
                    });
                }
                else
                {
                    $('#ShopManagerId').mkselectRefresh({
                        url: top.$.rootUrl + '/OrganizationModule/User/GetList',
                        param: { departmentId: value }
                    });
                }
            })
            $('#PlatformName').mkDataItemSelect({ code: 'PlatformType' });
            $('#OperationState').mkDataItemSelect({ code: 'ShopStatus' });
            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"extend",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#base_shopinfo_account').jfGrid({
                headData: [
                    {
                        label: '用户名', name: 'UserName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'账号管理' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '密码', name: 'Password', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'账号管理' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '是否主账号', name: 'IsMain', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'账号管理' 
                        ,edit:{
                            type:'radio',
                            change: function (row, index, oldValue, colname, rows,headData) {
                            },
                            datatype: 'dataItem',
                            code:'BESF',
                        }
                    },
                    {
                        label: '是否信息部专用账号', name: 'IsItExclusive', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'账号管理' 
                        ,edit:{
                            type:'radio',
                            change: function (row, index, oldValue, colname, rows,headData) {
                            },
                            datatype: 'dataItem',
                            code:'BESF',
                        }
                    },
                    {
                        label: '帐号是否验证', name: 'IsVerified', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '账号管理'
                        , edit: {
                            type: 'radio',
                            change: function (row, index, oldValue, colname, rows, headData) {
                            },
                            datatype: 'dataItem',
                            code: 'BESF',
                        }
                    },
                    {
                        label: '账户名称', name: 'AccountName', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '账号管理'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                toolbarposition: "top",
                bindTable:"base_shopinfo_account",
                isEdit: true,
                height: 300,
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
            $('#base_shopinfo_criticalpath').jfGrid({
                headData: [
                    {
                        label: '路径名称', name: 'PathName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'RPA工作路径' 
                        ,edit:{
                            type:'select',
                            change: function (row, index, item, oldValue, colname,headData) {
                            },
                            datatype: 'dataItem',
                            code:'PathType'
                        }
                    },
                    {
                        label: '路径地址', name: 'PathUrl', width:300,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'RPA工作路径' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '备注', name: 'Remrak', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'RPA工作路径' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                ],
                mainId: "ID",
                toolbarposition: "top",
                bindTable:"base_shopinfo_criticalpath",
                isEdit: true,
                height: 300,
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
            //页面首次加载隐藏运营时间
            $('.cOperationDate').hide();
            $('#OperationDate').removeAttr('isvalid checkexpession');
            // 运营状态改变
            $('#OperationState').on('change', function () {
                var value = $(this).mkselectGet();
                if (value == "Running") {
                    //运营中
                    $('.cOperationDate').show();
                    $('#OperationDate').attr({ 'isvalid': 'yes', "checkexpession": "NotNull" });

                } else {
                    $('.cOperationDate').hide();
                    $('#OperationDate').removeAttr('isvalid checkexpession');
                    $('#OperationDate').val("");
                }
            });

        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/BaseShopInfoManage/GetformInfoList?keyValue=' + keyValue, function (data) {
                    debugger
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            if (id == 'base_shopinfo_account') {
                                for (var i = 0; i <  data[id].length; i++) {
                                    var item = data[id][i];
                                    var MainIsNull = (!item.IsMain && typeof (item.IsMain) != "undefined" && item.IsMain != 0)
                                    if (!MainIsNull) {
                                        item.IsMain = item.IsMain.toString();
                                    }
                                    var ItExclusiveIsNull = (!item.IsItExclusive && typeof (item.IsItExclusive) != "undefined" && item.IsItExclusive != 0)
                                    if (!ItExclusiveIsNull) {
                                        item.IsItExclusive = item.IsItExclusive.toString();
                                    }
                                    var IsVerified = (!item.IsVerified && typeof (item.IsVerified) != "undefined" && item.IsVerified != 0)
                                    if (!IsVerified) {
                                        item.IsVerified = item.IsVerified.toString();
                                    }
                                }
                            }
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
    if (postData == false)return false;
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/BaseShopInfoManage/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="base_shopinfo"]').mkGetFormData());
        postData.strbase_shopinfo_accountList = JSON.stringify($('#base_shopinfo_account').jfGridGet('rowdatas'));
        postData.strbase_shopinfo_criticalpathList = JSON.stringify($('#base_shopinfo_criticalpath').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
