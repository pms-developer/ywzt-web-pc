﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-30 15:57
 * 描  述：证照社保
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_DispatchedSheBao";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectDispatchedSheBao/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var pageType = "form";
var auditStatus = "";
var auditPassEvent;
var unauditPassEvent;
var formId = request("formId");
var refreshGirdData;
var workflowId = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $("#ProjectID").val(projectId);
            Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectDispatchedSheBao/GetformInfo", { projectId: projectId }, function (res) {

                if (!!res.data) {
                    auditStatus = res.data.AuditStatus;
                    keyValue = res.data.ID;
                    workflowId = res.data.Workflow_ID;
                    type = "edit";
                    $("#edit").show();
                    $("#edit").unbind("click");
                    $("#edit").on("click", function () {
                        page.setViewState(false);
                        $("#audit").hide();
                        $("#unaudit").hide();
                        $("#send").hide();
                        $(this).hide();
                        $("#save").show();
                    });
                    page.setViewState(true);
                    $("#save").hide();

                    if (auditStatus == "2") {
                        $("#audit").hide();
                        $("#unaudit").show();
                        $("#send").hide();
                        $("#edit").hide();
                        $("#save").hide();
                    }
                }
                else {
                    $("#AccrueState").val("未完成计提");
                    keyValue = "";
                    type = "add";
                    $("#edit").hide();
                    $("#audit").hide();
                    $("#unaudit").hide();
                    $("#send").hide();
                    $("#save").show();
                    page.setViewState(false);
                }
            });
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_DispatchedSheBaoDetails', "gridId": 'Project_DispatchedSheBaoDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            $("#AccrueDate").mkselect({
                value: "value",
                text: "text",
                data: function () {
                    var data = [];
                    for (var i = 1; i <= 31; i++) {
                        data.push({ value: i, text: "每月" + i + "号,进行成本计提" });
                    }
                    return data;
                }(),
                dfvalue: 1
            });
            $("#ProjectSubjectId").mkselect({
                url: top.$.rootUrl + "/ProjectModule/ProjectSubject/GetSubjectList?projectId=" + projectId,
                value: "ID",
                text: "FullName",
                title: "FullName"
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $("#addSheBao").on("click", function () {
               
            });
            $("#deleteSheBao").on("click", function () {
                var rowIndex = $("#Project_DispatchedSheBaoDetails").jfGridGet("rowIndex");
                if (rowIndex != -1) {
                    $("#Project_DispatchedSheBaoDetails").jfGridSet("removeRow");
                }
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_DispatchedSheBaoDetails').jfGrid({
                headData: [
                    { label: '姓名', name: 'UserName', width: 100 },
                    {
                        label: '开始时间', name: 'StratDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '社保明细',
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: '结束时间', name: 'EndDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '社保明细',
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    {
                        label: '每月社保', name: 'SocialSecurity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'MonthTotalAmount', required: '0',
                        datatype: 'float', tabname: '社保明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                page.updateDateRow(row, index);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '时间小计', name: 'TimeCount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '社保明细'
                    },
                    {
                        label: '社保小计', name: 'TotalAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '社保明细'
                    },
                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '社保明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_DispatchedSheBaoDetails",
                isEdit: true,
                height: 300, toolbarposition: "top", showadd: false, showchoose: true, onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectContractMaterials",
                        width: 800,
                        height: 400,
                        title: "选择人员",
                        url: top.$.rootUrl + "/ProjectModule/ProjectDispatchedWage/Dialog?projectId=" + projectId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.UserId = data[i].UserId;
                                        row.ProjectId = projectId;

                                        row.UserName = data[i].UserName;

                                        row.StratDate = data[i].StratDate;
                                        row.EndDate = data[i].EndDate;
                                        var time = GetDateTimeDiff(row.StratDate, row.EndDate);
                                        var count = time.Months + (time.Days > 0 ? 1 : 0);
                                        row.Months = count;

                                        var count = time.Months + (time.Days > 0 ? 1 : 0);
                                        var tiemCount = "共" + count + "月";
                                        row.TimeCount = tiemCount;

                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        row.SortCode = i;
                                        rows.push(row);
                                    }
                                    $("#Project_DispatchedSheBaoDetails").jfGridSet("addRows", rows);
                                }
                            });
                        }
                    });
                }
            });

            $("#save").on("click", function () {
                var postData = getFormData();
                if (postData == false) return false;
                if (type == "add") {
                    keyValue = mainId;
                }
                $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectDispatchedSheBao/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
                    // 保存成功后才回调
                    if (res.code == Changjie.httpCode.success) {
                        type = "edit";
                        $("#edit").show();
                        $("#audit").show();
                        $("#unaudit").show();
                        $("#send").show();
                        $("#save").hide();
                        page.setViewState(true);
                    }
                });
            });
            $("#batchWage").on("input propertychange", function () {

                page.batchSetValue($(this).val(), "SocialSecurity");
            });

        },
        updateDateRow: function (row, index) {
            row.TotalAmount = Changjie.getFloatValue(row.Months) * Changjie.getFloatValue(row.SocialSecurity);
            $("#Project_DispatchedSheBaoDetails").jfGridSet("updateRow", index);
            var rows = $("#Project_DispatchedSheBaoDetails").jfGridGet("rowdatas");

            var total = 0;
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                total += Changjie.getFloatValue(row.TotalAmount);
            }
            $("#TotalAmount").val(total);

        },
        batchSetValue: function (value, columnName) {
            var rows = $("#Project_DispatchedSheBaoDetails").jfGridGet("rowdatas");
            var mouthTotal = 0;
            var total = 0;

            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                row[columnName] = value;

                var start = row.StratDate;
                var end = row.EndDate;
                if (start && end) {
                    var time = GetDateTimeDiff(start, end);
                    var count = time.Months + (time.Days > 0 ? 1 : 0);
                    var tiemCount = "共" + count + "月";
                    $("#TotalCount").val(count);
                    row.TimeCount = tiemCount;
                    row.Months = Changjie.getFloatValue(count);
                    var wage = Changjie.getFloatValue(row.SocialSecurity);
                    row.TotalAmount = count * wage;

                    mouthTotal += wage;
                    total += row.TotalAmount;
                }
            }

            $("#MonthTotalAmount").val(mouthTotal);
            $("#TotalAmount").val(total);

            $("#Project_DispatchedSheBaoDetails").jfGridSet("refreshdata");
        },
        setViewState: function (flag) {
            if (flag) {
                $("[viewState='readOnly']").attr("readonly", "readonly");
                $("[viewState='readOnly']").addClass("readonly");
                $("#btn_uploadfile").attr("disabled", "disabled");
                $("#btn_delfile").attr("disabled", "disabled");

            }
            else {
                $("[viewState='readOnly']").removeAttr("readonly");
                $("[viewState='readOnly']").removeClass("readonly");
                $("#btn_uploadfile").removeAttr("disabled");
                $("#btn_delfile").removeAttr("disabled");
            }
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectDispatchedSheBao/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                            $("#AccrueState").val(data[id].AccrueState == true ? "计提完成" : "未完成计提");
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectDispatchedSheBao/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    function GetDateTimeDiff(startTime, endTime) {
        startTime = new Date(startTime.replace(/-/g, "/"));
        endTime = new Date(endTime.replace(/-/g, "/"));
        var retValue = {};

        var date3 = endTime.getTime() - startTime.getTime();  //时间差的毫秒数

        //计算出相差天数
        var days = Math.floor(date3 / (24 * 3600 * 1000));
        retValue.Days = days;

        var years = Math.floor(days / 365);
        retValue.Years = years;

        var months = Math.floor(days / 30);
        retValue.Months = months;

        //计算出小时数
        var leave1 = date3 % (24 * 3600 * 1000);    //计算天数后剩余的毫秒数
        var hours = Math.floor(leave1 / (3600 * 1000));
        retValue.Hours = hours;

        //计算相差分钟数
        var leave2 = leave1 % (3600 * 1000);        //计算小时数后剩余的毫秒数
        var minutes = Math.floor(leave2 / (60 * 1000));
        retValue.Minutes = minutes;

        //计算相差秒数
        var leave3 = leave2 % (60 * 1000);      //计算分钟数后剩余的毫秒数
        var seconds = Math.round(leave3 / 1000);
        retValue.Seconds = seconds;

        var strTime = "";
        if (years >= 1) {
            strTime = years + "年前";
        } else if (months >= 1) {
            strTime = months + "个月前";
        } else if (days >= 1) {
            strTime = days + "天前";
        } else if (hours >= 1) {
            strTime = hours + "小时前";
        } else {
            strTime = minutes + "分钟前";
        }
        retValue.PubTime = strTime;
        return retValue;
    };

    page.init();
    refreshGirdData = function () {
        page.init();
    };
    auditPassEvent = function () {
        $("#edit").hide();
        $("#audit").hide();
        $("#unaudit").show();
        $("#send").hide();
        $("#save").hide();
    };
    unauditPassEvent = function () {
        $("#edit").show();
        $("#audit").show();
        $("#unaudit").hide();
        $("#send").show();
        $("#save").hide();
    };
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    var data = $('[data-table="Project_DispatchedSheBao"]').mkGetFormData();
    if (data.AccrueState == "计提完成") {
        data.AccrueState = true;
    }
    else {
        data.AccrueState = false;
    }
    postData.strEntity = JSON.stringify(data);
    postData.strProject_DispatchedSheBaoDetailsList = JSON.stringify($('#Project_DispatchedSheBaoDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
