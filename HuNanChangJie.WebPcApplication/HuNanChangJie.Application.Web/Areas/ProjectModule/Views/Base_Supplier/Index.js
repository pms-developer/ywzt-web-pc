﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-12 18:00
 * 描  述：供应商管理
 */
var refreshGirdData;
var formId = request("formId");
var checkJson = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
            //page.search();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '一年内', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'y', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '2',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search(checkJson);
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 400);
            $('#SupplierType').mkDataItemSelect({ code: 'GYSLB' });
            $('#TaxpayerNature').mkDataItemSelect({ code: 'NSRXZ' });
            //$('#Status').mkRadioCheckbox({
            //    type: 'radio',
            //    code: 'HZZT',
            //});
            $('#AuditStatus').mkselect({
                type: 'default', width:"250px",
                allowSearch: true,
                data: [
                    { id: "0", text: "未审核" },
                    { id: "1", text: "审核中" },
                    { id: "2", text: "已通过" },
                    { id: "3,4,5", text: "未通过" },
                ], dfvalue:"0"
            });
           
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增供应商',
                    url: top.$.rootUrl + '/ProjectModule/Base_Supplier/Form',
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
           
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/Base_Supplier/GetPageList',
                headData: [
                    { label: "编码", name: "Code", width: 150, align: "left"},
                    { label: "供应商名称", name: "Name", width: 250, align: "left" },
                    { label: "证件号码", name: "Flag", width: 150, align: "left" },
                    { label: "公司名称", name: "Company_ID", width: 160, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    {
                        label: "供应商类别", name: "SupplierType", width: 80, align: "center",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'GYSLB',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    //{
                    //    label: "纳税人性质", name: "TaxpayerNature", width: 80, align: "center",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('dataItem', {
                    //             key: value,
                    //             code: 'NSRXZ',
                    //             callback: function (_data) {
                    //                 callback(_data.text);
                    //             }
                    //         });
                    //    }},
                    //{
                    //    label: "开票税率", name: "TaxRage", width: 80, align: "center",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('dataItem', {
                    //             key: value,
                    //             code: 'CW0101',
                    //             callback: function (_data) {
                    //                 callback(_data.text);
                    //             }
                    //         });
                    //    }},
                    //{ label: "省", name: "ProvinceId", width: 60, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('sourceData', {
                    //             code:  'Province',
                    //             key: value,
                    //             keyId: 'f_areaid',
                    //             callback: function (_data) {
                    //                 callback(_data['f_areaname']);
                    //             }
                    //         });
                    //    }},
                    //{
                    //    label: "市", name: "CityId", width: 60, align: "left",
                    //    formatterAsync: function (callback, value, row, op,$cell) {
                    //         Changjie.clientdata.getAsync('sourceData', {
                    //             code:  'CityOrDistrict',
                    //             key: value,
                    //             keyId: 'f_areaid',
                    //             callback: function (_data) {
                    //                 callback(_data['f_areaname']);
                    //             }
                    //         });
                    //    }
                    //},
                    {
                        label: "审核状态", name: "AuditStatus", width: 70, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    //{ label: "注册资金", name: "RegisterFund", width: 100, align: "left"},
                    //{ label: "供货周期", name: "DeliveryPeriod", width: 100, align: "left"},
                    //{ label: "账期", name: "PaymentDates", width: 100, align: "left"},
                    
                    
                    
                    { label: "等级", name: "Grade", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'PGXJ',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "合作状态", name: "Status", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'HZZT',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    }, { label: "备注", name: "Remark", width: 300, align: "left" },
                ],
                mainId:'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (typeof param.AuditStatus == "undefined") {
                param.AuditStatus = "0";
            }
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/Base_Supplier/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '供应商',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/Base_Supplier/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
