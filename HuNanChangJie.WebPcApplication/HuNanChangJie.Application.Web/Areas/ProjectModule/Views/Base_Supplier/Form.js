﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-12 18:00
 * 描  述：供应商管理
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="Base_Supplier";
var processCommitUrl=top.$.rootUrl + '/ProjectModule/Base_Supplier/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               subGrid.push({"tableName":'Base_SupplierBank',"gridId":'Base_SupplierBank'});
               subGrid.push({"tableName":'Base_SupplierLinkman',"gridId":'Base_SupplierLinkman'});
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0017' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#SubmitDepartment').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            });

            $('#SupplierType').mkDataItemSelect({ code: 'GYSLB' });
            $('#TaxpayerNature').mkDataItemSelect({ code: 'NSRXZ' });
            $('#TaxRage').mkDataItemSelect({ code: 'CW0101' });

            $('#ProvinceId').mkProvinceSelect({
                code: 'Province',
                value: 'F_AreaId',
                text: 'F_AreaName',
            });

            $("#ProvinceId").on('change', function () {
                var parentId = $(this).mkselectGet();
                if (!!parentId) {
                    $('#CityId').mkSubAreaSelect({
                        parentId: parentId,
                        value: 'F_AreaId',
                        text: 'F_AreaName'
                    });
                }
            });
            $('#Grade').mkDataItemSelect({ code: 'PGXJ' });
            $('#Status').mkRadioCheckbox({
                type: 'radio',
                code: 'HZZT',
            });
            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"extend",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Base_SupplierLinkman').jfGrid({
                headData: [
                    {
                        label: '姓名', name: 'Name', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'string', tabname:'联系人信息' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '职务', name: 'Position', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'联系人信息' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '电话', name: 'Phone', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'string', tabname:'联系人信息' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '邮箱', name: 'Email', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'联系人信息' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                ],
                mainId:"ID",
                bindTable:"Base_SupplierLinkman",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
            $('#Base_SupplierBank').jfGrid({
                headData: [
                    {
                        label: '账户名称', name: 'AccountName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'string', tabname:'银行账户信息' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '开户银行', name: 'BankName', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'string', tabname:'银行账户信息' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '银行账号', name: 'BankNumber', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'银行账户信息' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                ],
                mainId:"ID",
                bindTable:"Base_SupplierBank",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                     row.SortCode=rows.length;
                     row.EditType=1;
                 },
                onMinusRow: function(row,rows,headData){
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/Base_Supplier/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/Base_Supplier/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="Base_Supplier"]').mkGetFormData());
        postData.strbase_SupplierBankList = JSON.stringify($('#Base_SupplierBank').jfGridGet('rowdatas'));
        postData.strbase_SupplierLinkmanList = JSON.stringify($('#Base_SupplierLinkman').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
     return postData;
}
