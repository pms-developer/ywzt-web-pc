﻿var projectId = request("projectId");
var moduleId = request("moduleId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.loadkb();

            //page.loadMenu();
           // page.changeTabName();
        },
        loadMenu: function () {
            parent.$(".mk-second-menu-list").hide();
            var $menu = parent.$("#projectmenu");
            $menu.find(".mk-menu-item").each(function (index, element) {
                var $this = $(element);
                $this.attr("projectId", projectId);
            });
            $menu.show();
        },
        changeTabName: function () {
            var $span = parent.$("span[tabid=" + moduleId + "]");
            $span.html(decodeURI(request("name")));
        },
        loadkb: function () {
            if (projectId) {
                var data = Changjie.httpGet(top.$.rootUrl + '/DisplayBoard/KBKanBanInfo/GetKanBanInfoByName', { kbName: "项目主页" }).data;
                if (data) {
                    var url = "/DisplayBoard/KBKanBanInfo/PreviewForm?keyValue=" + data.F_Id + "&projectId=" + projectId;
                    $("#project_kb").attr("src", url);
                }
            }
        }

    };

    page.init();
}