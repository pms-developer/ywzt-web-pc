﻿var projectId = request("projectId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bindEvent();
            page.initGrid();
            page.initData();
        },
        initGrid: function () {
            $("#gridtable").jfGrid({
                headData: [
                    {
                        label: "项目角色", name: "RoleName", width: 200, align: "left",
                        edit: {
                            type: "input",
                            change: function (row) { },
                        },
                    },
                    {
                        label: "项目组成员", name: "UserList", width: 200, align: "left",
                        edit: {
                            type: 'layer',
                            click: function (row) {
                                Changjie.layerForm({
                                    id: 'ProjectTeams',
                                    title: '添加项目组人员',
                                    url: top.$.rootUrl + '/WorkFlowModule/WfScheme/UserForm',
                                    width: 400,
                                    height: 300,
                                    callBack: function (id) {
                                        return top[id].acceptClick(function (data) {
                                            if (!!data.auditorId) {
                                                if (!row.UserList) {
                                                    row.UserList = [];
                                                }
                                                var flag = false;
                                                for (var item in row.UserList) {
                                                    var userinfo = row.UserList[item];
                                                    if (userinfo.userId != data.auditorId) continue;
                                                    flag = true;
                                                    break;
                                                }
                                                if (!flag) {
                                                    row.UserList.push({ UserId: data.auditorId, UserName: data.auditorName });
                                                    $('#gridtable').jfGridSet('refreshdata');
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        },
                        formatter: function (value, row, a, b, c, index) {
                           
                            if (!!row.UserList) {
                                
                                var html = "";
                                for (var item in row.UserList) {
                                    var userInfo = row.UserList[item];
                                    var liststr = JSON.stringify(row.UserList);
                                    liststr = liststr.replace(/\"/g, "#");
                                    html += '<span>' + userInfo.UserName + '<button type="button" class="close" id=\'' + userInfo.UserId + '\'" onclick="deleuser(\'' + liststr + '\',this,\'' + index+'\')" aria-hidden="true">&times;</button></span>  ';
                                }
                                
                                return html;
                            }
                        },
                    }
                ],
                onAddRow: function (row, rows) {
                },
                isEdit: true,
                //isMultiselect: true,
            });
        },
        initData: function () {
            var url = top.$.rootUrl + '/ProjectModule/Project/GetTeamList?projectId=' + projectId;
            $.mkSetForm(url, function (data) {
                $("#gridtable").jfGridSet("refreshdata",data);
            });
        },
        bindEvent: function () {
            $("#save").on("click", page.save);
        },
        save: function () {
            if (!!projectId) {
                var formData = $('#gridtable').jfGridGet("rowdatas");
                formData = JSON.stringify(formData);
                var postData = {
                    formData: formData
                };
                $.mkSaveForm(top.$.rootUrl + '/ProjectModule/Project/SaveTeam?projectId=' + projectId, postData, function (res) {
                });
            }
        }
    };
    page.init();
};

var deleuser = function (jsonstr, obj,index) {
    jsonstr = jsonstr.replace(/#/g, '"');
    var json = jQuery.parseJSON(jsonstr);
    event.stopPropagation(); 
    var $this = $(obj);
    var currentrowdata = $('#gridtable').jfGridGet("rowdatas")[index];
    for (var item in json) {
        var userinfo = json[item];
        if (userinfo.UserId != $this.attr("id")) continue;
        json.splice(item, 1);
    }
    currentrowdata.UserList = json;
    $('#gridtable').jfGridSet('refreshdata');
     
}