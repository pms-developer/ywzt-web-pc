﻿var bootstrap = function ($, Changjie) {
    load = {
        init: function () {
            load.initMenu();
        },
        initMenu: function () {
            $("#loadmenu").on("click", function () {
                parent.$(".mk-second-menu-list").hide();
                parent.$("#projectmenu").show();

                //var li = "<li class='mk-meun-had' title='sdfas'>12313</li>"
                //ul.append(li);

                //$pare.append(ul);



                //var data = Changjie.myProjectData;

                //for (var i in data) {
                //    var item = data[i];
                //    var ul = $("<ul class='mk-second-menu-list' name='projectmenu'></ul>");
                //    for (var node in item.submenu) {
                //        var sub = item.submenu[node];
                //        var li=
                //    }
                //}
            });
        },
    };
    load.init();
}

/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2019-11-04 15:18
 * 描  述：项目基本信息
 */
var moduleId = request("moduleId");
var urltype = request("urltype");
var checkJson = "";

var auditPassEvent = function (infoid) {
    top.Changjie.clientdata.refresh("sourceData", { code: 'BASE_XMLB' })
}
var refreshGirdData;
var formId = request("formId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 1000);
            $('#Type').mkDataItemSelect({ code: 'ProjectType' });
            $('#MarketingDepartmentID').mkDepartmentSelect();
            $('#Company_ID').mkCompanySelect();
            $('#Mode').mkDataItemSelect({ code: 'ProjectMode' });
            $('#Grade').mkDataItemSelect({ code: 'ProjectGrade' });
            $('#Type').mkDataItemSelect({ code: 'ProjectType' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                //Changjie.frameTab.open({
                //    F_ModuleId: 'projectAddForm',
                //    F_Icon: 'fa fa-pencil-square-o',
                //    F_FullName: '新增项目',
                //    F_UrlAddress: '/ProjectModule/Project/Form?moduleId=' + moduleId
                //});
                Changjie.layerForm({
                    id: 'form',
                    title: '新增项目',
                    url: top.$.rootUrl + '/ProjectModule/Project/Form?moduleId=' + moduleId + '&urltype=' + urltype,
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '项目信息编辑',
                        url: top.$.rootUrl + '/ProjectModule/Project/Form?keyValue=' + keyValue + '&moduleId=' + moduleId,
                        width: 1000,
                        height: 800,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/Project/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });

            // 审核
            $('#audit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否审核该记录！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/Project/AuditForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });


            // 反审核
            $('#unaudit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否反审核该记录！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/Project/UnAuditForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });

        },
        // 初始化列表  
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/Project/GetPageList',
                headData: [
                    {
                        label: "关注", name: "unConcern", width: 60, align: "center",
                        formatter: function (value, row) {
                            if (value == '1') {
                                return "<a onclick=\"changeconcern('" + row.ID + "','" + row.cID + "')\" ><img src='../../Content/images/logo/concern.png' width='20' height='20' /></a>";
                            }
                            else {
                                return "<a onclick=\"changeconcern('" + row.ID + "','" + row.cID + "')\" ><img src='../../Content/images/logo/unconcern.png' width='20' height='20'></a>";
                            }
                        }
                    },
                    {
                        label: "店铺名称", name: "ProjectName", width: 350, align: "left",
                        formatter: function (value, row) {
                            return "<a style='color:blue' onclick=\"openWin('" + row.ID + "','" + row.ProjectName + "')\" >" + value + "</a>";
                        }
                    },
                    {
                        label: "店铺类型", name: "ProjectType", width: 200, align: "left"
                    },   
                    {
                        label: "电商平台", name: "PlatformName", width: 200, align: "left"
                    },  
                    {
                        label: "店铺所在地", name: "Address", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            row.Address = (row.ProvinceID ? row.ProvinceID : "") + (row.CityID ? row.CityID : "") + (row.District ? row.District : "") + (value ? value : "");
                            callback(row.Address);
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 80, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    }, {
                        label: "店铺等级", name: "Grade", width: 70, align: "center",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'ProjectGrade',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    {
                        label: "所属公司", name: "Company_ID", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('company', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                ],
                mainId: 'ID',
                isPage: true
            });
            page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            param.urltype = urltype;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }

    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();


}

function changeconcern(id, cid) {
    url = top.$.rootUrl + "/ProjectModule/Project/ChangeConcern";
    var data = top.Changjie.httpGet(url, { 'project_id': id, 'cid': cid });
    if (data.code == top.Changjie.httpCode.success) {
        refreshGirdData();
    }
}

var openWin = function (id, name) {
    parent.$(".mk-second-menu-list").hide();
    var $menu = parent.$("#projectmenu");
    $menu.find(".mk-menu-item").each(function (index, element) {
        var $this = $(element);
        $this.attr("projectId", id);
    });
    $menu.show();
    var url = "/ProjectModule/Project/Main?projectId=" + id + "&name=" + name;
    top.Changjie.frameTab.closeallprojecttab();
    top.Changjie.frameTab.open({
        F_ModuleId: id,
        F_Icon: 'fa fa-file-text-o',
        F_FullName: name,
        F_UrlAddress: url,
        IsProject: true
    });
}
