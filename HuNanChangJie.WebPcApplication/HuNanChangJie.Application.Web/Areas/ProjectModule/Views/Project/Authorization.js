﻿var projectId = request("projectId");
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
            page.initGrid();
            page.initData();
        },
        bind: function () {
            $("#add").on("click", function () {
                Changjie.layerForm({
                    id: 'setAuthorization',
                    title: '项目授权',
                    url: top.$.rootUrl + '/OrganizationModule/Company/ChooseOrganization?projectId=' + projectId,
                    width: 600,
                    height: 500,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            page.setGridData(data);
                        });
                
                    }
                });
            });
            $("#save").on("click", function () {
                page.save();
            });
            $("#delete").on("click", function () {
                $("#gridtable").jfGridSet("removeRow");
            })
            
        },
        initGrid: function () {
            $("#gridtable").jfGrid({
                headData: [
                    { label: "授权对象", name: 'Name', width: 200, align: 'left' },
                    {
                        label: "授权类型", name: 'Type', width: 200, align: 'left',
                        formatter: function (value) {
                            switch (value) {
                                case "Role":
                                    return "角色";
                                case "User":
                                    return "用户";
                                case "Department":
                                    return "部门";
                                case "Company":
                                    return "公司";
                            }
                        },
                    }
                ],
            });
        },
        initData: function () {
            var url = top.$.rootUrl + '/SystemModule/ModuleInfoAuthorization/GetList?moduleInfoId=' + projectId +'&moduleType=Project';
            $.mkSetForm(url, function (data) {
                $("#gridtable").jfGridSet("refreshdata", data);
            });
        },
        setGridData: function (data) {
            var rowsdata = $("#gridtable").jfGridGet("rowdatas");
          
            if (rowsdata.length > 0) {
               
                for (var i in rowsdata) {
                    var row = rowsdata[i];
                    for (var n in data) {
                        var info = data[n];
                        if (row.ObjectId != info.ObjectId) continue;
                        data.splice(n, 1);
                    }
                }
                for (var i in data) {
                    rowsdata.push(data[i]);
                }
                
                $("#gridtable").jfGridSet("refreshdata", rowsdata);
            }
            else {
                $("#gridtable").jfGridSet("refreshdata", data);
            }
        },
        save: function ()
        {
            var url = top.$.rootUrl + '/SystemModule/ModuleInfoAuthorization/Save';
            var postData = {
                moduleInfoId: projectId,
                moduleType: "Project",
                formData: JSON.stringify($("#gridtable").jfGridGet("rowdatas"))
            };
            $.mkSaveForm(url, postData, function (res) {
            });
        }
    };
    page.init();
};