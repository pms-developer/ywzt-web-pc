﻿/* *  
 * 创建人：超级管理员
 * 日  期：2019-11-04 15:18
 * 描  述：项目基本信息
 */
var acceptClick;
var formId = request("formId");
var projectId = request("projectId") || request("keyValue");
var moduleId = request("moduleId");
var keyValue = projectId;
var subGrid = [];
var tables = [];
var mainTable="Base_CJ_Project"
var processCommitUrl = top.$.rootUrl + '/ProjectModule/Project/SaveForm';
var pageType = "form";
var auditStatus = "";
var auditPassEvent;
var unauditPassEvent;
var refreshGirdData;
var workflowId = "";
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $("#cancel").hide();
            $("#save").hide();
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            
        },
        setViewState: function (flag) {
            if (flag) {
                $("[viewState='readOnly']").attr("readonly", "readonly");
                $("[viewState='readOnly']").addClass("readonly");
                $("#btn_uploadfile").attr("disabled", "disabled");
                $("#btn_delfile").attr("disabled", "disabled");

            }
            else {
                $("[viewState='readOnly']").removeAttr("readonly");
                $("[viewState='readOnly']").removeClass("readonly");
                $("#btn_uploadfile").removeAttr("disabled");
                $("#btn_delfile").removeAttr("disabled");
            }
        },
        bind: function () {
            $("#editInfo").on("click", function () {
                $("#save").show();
                $(this).hide();
                $("#audit").hide();
                $("#unaudit").hide();
                $("#send").hide();
                page.setViewState(false);
            });
            $("#cancel").on("click", function () {
                page.setViewState(true);
            });
            $("#save").on("click", function () {
                $("#audit").show();
                $("#unaudit").hide();
                $("#send").show();
                $("#editInfo").show();
                $("#save").hide();
                page.save();
            });
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $("#form_tabs_sub").systemtables({
                type: "edit",
                keyvalue: projectId,
                state: "1",
                moduleType: "Project",
                moduleId: moduleId,
                moduleInfoId: projectId,
                folderName: "项目登记",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: '10000' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#Type').mkDataSourceSelect({ code: 'ProjectType', value: 'id', text: 'name' });
            $('#Section').mkDataItemSelect({ code: 'ProjectSection' });
            $('#MarketingStaffID').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#MarketingDepartmentID').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            });
            $('#Company_ID').mkCompanySelect({});
            $('#Source').mkDataItemSelect({ code: 'ProjectSource' });
            $('#Grade').mkDataItemSelect({ code: 'ProjectGrade' });
            $('#Mode').mkDataItemSelect({ code: 'ProjectMode' });
            $('#ProvinceID').mkProvinceSelect({
                code: 'Province',
                value: 'F_AreaId',
                text: 'F_AreaName',
            });

            $("#ProvinceID").on('change', function () {
                var parentId = $(this).mkselectGet();
                $('#CityID').mkSubAreaSelect({
                    parentId: parentId,
                    value: 'F_AreaId',
                    text: 'F_AreaName'
                });
            });

            $("#CityID").mkselect().on("change", function () {
                var parentId = $(this).mkselectGet();
                $('#District').mkSubAreaSelect({
                    parentId: parentId,
                    value: 'F_AreaId',
                    text: 'F_AreaName'
                });

            });

            $("#District").mkselect();


            $('#CustomerID').mkDataSourceSelect({ code: 'KHLB', value: 'id', text: 'fullname' });
            $('#FinalUserID').mkDataSourceSelect({ code: 'KHLB', value: 'id', text: 'fullname' });
            $('#AccountManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#BigAccountManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#Designer').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#Didder').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
        },
        initData: function () {
            if (!!projectId) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/Project/GetFormData?keyValue=' + projectId, function (data) {
                    $('[data-table="Base_CJ_Project"]').mkSetFormData(data);
                    auditStatus = data.AuditStatus;
                    workflowId = data.Workflow_ID;

                    if (auditStatus == "2") {
                        page.setViewState(true);
                        $("#audit").hide();
                        $("#unaudit").show();
                        $("#send").hide();
                        $("#editInfo").hide();
                        $("#save").hide();
                    } else {
                        page.setViewState(false);
                        $("#editInfo").show();
                        $("#audit").show();
                        $("#unaudit").hide();
                        $("#send").show();
                        $("#save").hide();
                    }
                    
                });
            }
        },
        save: function () {
            var postData = getFormData();
            $.mkSaveForm(top.$.rootUrl + '/ProjectModule/Project/SaveForm?keyValue=' + projectId+"&type=edit", postData,
                function (res) {
                }
            );
        }
    };
    page.init();

    auditPassEvent = function () {
        $("#editInfo").hide();
        $("#audit").hide();
        $("#unaudit").show();
        $("#send").hide();
        $("#save").hide();
    };
    unauditPassEvent = function () {
        $("#editInfo").show();
        $("#audit").show();
        $("#unaudit").hide();
        $("#send").show();
        $("#save").hide();
    };
    refreshGirdData = function () {
        page.init();
    };
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var postData = {
        strEntity: JSON.stringify($('body').mkGetFormData())
    };
    return postData;
}
