﻿/* *  
 * 创建人：超级管理员
 * 日  期：2019-11-04 15:18
 * 描  述：项目基本信息
 */
var acceptClick;
var keyValue = request('keyValue');
var projectId = request("projectId");
var moduleId = request("moduleId");
var isConcern = request("isConcern");
var type = "add";
var mainId = "";
if (!!projectId) {
    if (!keyValue) {
        keyValue = projectId;
    }
}
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!keyValue) {
                mainId = keyValue;
                projectId = mainId;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                projectId = mainId;
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
        },
        bind: function () {
            $("#TestList").mkselect({
                type: 'gird',
                value: "id",
                text:'text',
                data: [{ id: '1', text: "选项1" }, { id: '2', text: "选项2" }]
            });
            $('#form_tabs').mkFormTab();
            $('#form_tabs ul li').eq(0).trigger('click');
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "1",
                moduleType: "Project",
                moduleId: moduleId,
                moduleInfoId: projectId,
                folderName:"项目登记",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'ProjectCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#Type').mkDataSourceSelect({ code: 'ProjectType', value: 'id', text: 'name' });
            $("#Xmcbfs").mkDataItemSelect({ code: 'xmcbfs' });
            $('#Section').mkDataItemSelect({ code: 'ProjectSection' });
            $('#MarketingStaffID').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds',
                select: function (item, id, obj) {
                    if (item != null && item.useritem != null && item.useritem.F_Mobile) {
                        $("#Phone").val(item.useritem.F_Mobile)
                    }
                }
            });
            $('#MarketingDepartmentID').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {} 
            });
            $('#Company_ID').mkCompanySelect({});
            $('#Source').mkDataItemSelect({ code: 'ProjectSource' });
            $('#Grade').mkDataItemSelect({ code: 'ProjectGrade' });
            $('#Mode').mkDataItemSelect({ code: 'ProjectMode' });
            $('#ProvinceID').mkProvinceSelect({
                code: 'Province',
                value: 'F_AreaId',
                text: 'F_AreaName',
            });

            $("#ProvinceID").on('change', function () {
                var parentId = $(this).mkselectGet();
                $('#CityID').mkSubAreaSelect({
                    parentId: parentId,
                    value: 'F_AreaId',
                    text: 'F_AreaName' 
                });
            });

            $("#CityID").mkselect().on("change", function () {
                var parentId = $(this).mkselectGet();
                $('#District').mkSubAreaSelect({
                    parentId: parentId,
                    value: 'F_AreaId',
                    text: 'F_AreaName'
                });

            });

            $("#District").mkselect();
 
            $('#CustomerID').mkDataSourceSelect({ code: 'KHLB', value: 'id', text: 'fullname' });
            $('#CustomerID').mkDataSourceSelect({ code: 'KHLB', value: 'id', text: 'fullname' });
            $('#AccountManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#BigAccountManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#Designer').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#Didder').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#ProjectManager').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/Project/GetFormData?keyValue=' + keyValue, function (data) {
                    $('[data-table="Base_CJ_Project"]').mkSetFormData(data);
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var strEntity = $('body').mkGetFormData();
        strEntity.isConcern = isConcern;
        var postData = {
            strEntity: JSON.stringify(strEntity)
        };
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/Project/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
