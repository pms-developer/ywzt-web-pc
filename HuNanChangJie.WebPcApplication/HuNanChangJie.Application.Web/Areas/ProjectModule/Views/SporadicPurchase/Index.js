﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-11 09:22
 * 描  述：零星采购
 */
var refreshGirdData;
var formId = request("formId");
var auditPassEventBat= function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/AuditSporadicPurchase', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/PurchaseModule/PurchaseInStorage/UnAuditPassSporadicPurchase', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var projectId = request('projectId');
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '3',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增零星采购',
                    url: top.$.rootUrl + '/ProjectModule/SporadicPurchase/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/SporadicPurchase/GetPageList',
                headData: [
                    { label: "零星采购编码", name: "Code", width: 100, align: "left" },
                    {
                        label: "零星采购人", name: "PurchaseUserId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('user', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "采购部门", name: "PurchaseDepartmentId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('department', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    { label: "零星采购金额", name: "Amount", width: 100, align: "left" },
                    { label: "项目名称", name: "ProjectName", width: 100, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },

                    { label: "摘要", name: "Abstract", width: 100, align: "left" },
                ],
                mainId: 'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            if (projectId) {
                param.ProjectID = projectId;
            }
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/SporadicPurchase/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '零星采购',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/ProjectModule/SporadicPurchase/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&projectId=" + projectId + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
