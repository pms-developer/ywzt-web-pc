﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-11 09:22
 * 描  述：零星采购
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_SporadicPurchase";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/SporadicPurchase/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_SporadicPurchaseDetails', "gridId": 'Project_SporadicPurchaseDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'SporadicPurchaseCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#PurchaseUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            });
            $('#PurchaseDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_SporadicPurchaseDetails').jfGrid({
                headData: [
                    {
                        label: '材料编码', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '零星采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '材料名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '零星采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '零星采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '零星采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '零星采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    //修改
                    {
                        label: '自动入库', name: 'IsPutInStorage', width: 100, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '零星采购明细',
                        formatter: function (cellvalue) {
                            var val = JSON.parse(cellvalue || "false");
                            //alert(val);
                            //if (val == true) {
                            //    alert("true");
                            //    $('#Base_WarehouseId').validatebox({ required: false });
                            //} else {
                            //    alert("false");
                            //    $('#Base_WarehouseId').validatebox({ required: true });
                            //}
                            return val == true ? "是" : "否";
                        },
                        edit: {
                            type: 'radio',
                            init: function (data, $edit) {

                            },
                            data: [
                                { 'id': true, 'text': '是' },
                                { 'id': false, 'text': '否' }
                            ],
                            dfvalue: false// 默认选中项
                        }
                    },

                    {
                        label: '入库仓库', name: 'Base_WarehouseId', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '零星采购明细'
                        , edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'Warehouse',
                            op: {
                                value: 'id',
                                text: 'title',
                                title: 'name'
                            }
                        }
                    },
                    {
                        label: '税率', name: 'TaxRate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '零星采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "Project_SporadicPurchaseDetails";

                                var TaxRates = 1 + Number(row.TaxRate) / 100;
                                row.ContainTaxPrice = (row.Price / TaxRates).toFixed(4);

                                if (!isNaN(row.ContainTaxPrice))
                                    setCellValue("ContainTaxPrice", row, index, gridName, row.ContainTaxPrice);

                                row.NoContainTaxPrice = (row.ContainTaxPrice * row.Quantity).toFixed(4);
                                if (!isNaN(row.NoContainTaxPrice))
                                    setCellValue("NoContainTaxPrice", row, index, gridName, row.NoContainTaxPrice);

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '单价(含税)', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '零星采购明细', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "Amount", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "Project_SporadicPurchaseDetails";

                                var TaxRates = 1 + Number(row.TaxRate) / 100;
                                row.ContainTaxPrice = (row.Price / TaxRates).toFixed(4);

                                if (!isNaN(row.ContainTaxPrice))
                                    setCellValue("ContainTaxPrice", row, index, gridName, row.ContainTaxPrice);

                                row.NoContainTaxPrice = (row.ContainTaxPrice * row.Quantity).toFixed(4);
                                if (!isNaN(row.NoContainTaxPrice))
                                    setCellValue("NoContainTaxPrice", row, index, gridName, row.NoContainTaxPrice);

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '零星采购明细', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "Amount", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var gridName = "Project_SporadicPurchaseDetails";

                                row.NoContainTaxPrice = (row.ContainTaxPrice * row.Quantity).toFixed(4);
                                if (!isNaN(row.NoContainTaxPrice))
                                    setCellValue("NoContainTaxPrice", row, index, gridName, row.NoContainTaxPrice)

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '总金额(含税)', name: 'Amount', width: 100, cellStyle: { 'text-align': 'left' }, statistics: true, aggtype: 'sum', aggcolumn: 'Amount', required: '0',
                        datatype: 'float', tabname: '零星采购明细'
                    },

                    {
                        label: '单价(不含税)', name: 'ContainTaxPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '零星采购明细'
                    },
                    {
                        label: '总金额(不含税)', name: 'NoContainTaxPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '1',
                        datatype: 'float', tabname: '零星采购明细'
                    },
                    {
                        label: '采购供应商', name: 'Supplier', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '零星采购明细'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_SporadicPurchaseDetails",
                isEdit: true,
                height: 300,
                toolbarposition: "top",
                rowdatas: [],
                showchoose: true,
                onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectQuantities",
                        width: 800,
                        height: 400,
                        title: "选择材料清单",
                        url: top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/MaterialsDialog?projectId=" + projectId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        //row.ProjectSporadicPurchaseId = Changjie.newGuid();
                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Brand = data[i].Brand;
                                        row.ModelNumber = data[i].ModelNumber;
                                        row.Unit = data[i].Unit;

                                        //数量、价格、金额
                                        row.Quantity = data[i].AllowQuantity || 0;
                                        row.Price = data[i].BudgetPrice || 0;
                                        row.Amount = row.Quantity * row.Price;

                                        var TaxRates = 1 + Number(0) / 100;

                                        row.ContainTaxPrice = (row.Price / TaxRates);
                                        row.NoContainTaxPrice = (row.ContainTaxPrice * row.Quantity);

                                        row.rowState = 1;
                                        row.IsPutInStorage = 0;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        row.MaterialsSource = data[i].MaterialsSource == 1 ? 1 : 0;
                                        rows.push(row);
                                    }
                                    $("#Project_SporadicPurchaseDetails").jfGridSet("addRows", rows);

                                    var signAmount = parseFloat($("#Project_SporadicPurchaseDetails").jfGridGet("footerdata")["Amount"] || 0);
                                    $("#Amount").val(signAmount);
                                }
                            });
                        }
                    });
                },

                showchooses: true,
                onChooseEvents: function () {
                    Changjie.layerForm({
                        id: "selectQuantities",
                        width: 1100,
                        height: 1000,
                        title: "选择材料档案清单",
                        url: top.$.rootUrl + "/SystemModule/BaseMaterials/MaterialsDialog?projectId=" + projectId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.Base_MaterialsId = data[i].ID;
                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Brand = data[i].BrandName;
                                        row.ModelNumber = data[i].Model;
                                        row.Unit = data[i].UnitName;

                                        //数量、价格、金额
                                        row.Quantity = data[i].SafeStock || 0;
                                        row.Price = data[i].Price || 0;
                                        row.Amount = row.Quantity * row.Price;

                                        var TaxRates = 1 + Number(0) / 100;

                                        row.ContainTaxPrice = (row.Price / TaxRates);
                                        row.NoContainTaxPrice = (row.ContainTaxPrice * row.Quantity);

                                        row.rowState = 1;
                                        row.IsPutInStorage = 0;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        row.MaterialsSource = data[i].MaterialsSource == 1 ? 1 : 0;
                                        rows.push(row);
                                    }
                                    $("#Project_SporadicPurchaseDetails").jfGridSet("addRows", rows);

                                    var signAmount = parseFloat($("#Project_SporadicPurchaseDetails").jfGridGet("footerdata")["Amount"] || 0);
                                    $("#Amount").val(signAmount);
                                }
                            });
                        }
                    });
                },

                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                    row.MaterialsSource = 0;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/SporadicPurchase/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/SporadicPurchase/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var setCellValue = function (cellName, row, index, gridName, value) {
    var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
    var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
    row[cellName] = value;
    $cell.html(value);
    $edit.val(value);
};

var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_SporadicPurchase"]').mkGetFormData());
    postData.strproject_SporadicPurchaseDetailsList = JSON.stringify($('#Project_SporadicPurchaseDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
