﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-04-08 16:58
 * 描  述：投标文档
 */
var projectId = request("projectId");
var refreshGirdData;
var formId = request("formId");
var moduleId = request("moduleId");
var checkJson = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });

            $("#uploadFile").on("click", function () {
                $("#uploadFile").mkUploader({
                    isShowDefaultButton: false,
                    isCustom: true,
                    parentId: projectId,
                    id: "gridtable"
                });

            });

            $("#deleteFile").on("click", function () {
                var id = $("#gridtable").jfGridValue("ID");
                if (!!id) {
                    Changjie.layerConfirm('是否删除该文件？', function (res) {
                        if (res) {

                            var param = {};
                            param['__RequestVerificationToken'] = $.mkToken;
                            param['fileId'] = id;
                            Changjie.httpAsyncPost(top.$.rootUrl + "/ProjectModule/ProjectBidDocument/DeleteAnnexesFile", param, function (res) {
                                if (res.code == Changjie.httpCode.success) {
                                    $("#gridtable").jfGridSet("removeRow", id);
                                }
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectBidDocument/GetPageList',
                headData: [
                    { label: "文件名称", name: "FileName", width: 200, align: "left" },
                    { label: "文件大小", name: "FileSizeToMB", width: 200, align: "left" },
                    { label: "上传者", name: "CreationName", width: 200, align: "left" },
                    { label: "上传日期", name: "CreationDate", width: 200, align: "left" },
                    {
                        label: "操作", name: "DownFile", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return "<a href='javascript:void(0)' onclick=\"downloadFile2('" + row.ID + "')\"><span style='color:blue'>下载附件</span></a>";
                        }
                    },
                ],
                mainId: 'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            param.projectId = projectId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();


};
function downloadFile2(fileId) {
    window.open(top.$.rootUrl + '/SystemModule/Annexes/DownAnnexesFile2?fileId=' + fileId);
}
function downloadFile(fileId) {
    var data = {
        url: top.$.rootUrl + '/ProjectModule/ProjectBidDocument/DownAnnexesFile',
        param:
        {
            fileId: fileId,
            __RequestVerificationToken: $.mkToken
        },
        method: 'POST'
    };
    top.Changjie.download(data);
}

// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectBidDocument/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title,
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/ProjectModule/ProjectBidDocument/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
