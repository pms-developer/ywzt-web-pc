﻿
var bootstrap = function ($, Changjie) {    
    var page = {
        init: function () {
            page.bind();
            page.initGrid();
            page.search();
        },
        bind: function () {
            $("#refresh").on("click", function () {
                refreshGridData();
            });

            $("#add").on("click", function () {
                var keyValue = $("#gridtable").jfGridValue("ID");
                if (!!keyValue) {
                    Changjie.layerForm({
                        id: "addSubjectForm",
                        title: '新增收支科目',
                        url: top.$.rootUrl + '/ProjectModule/Subject/Form?keyValue=' + keyValue+'&type=add',
                        width: 500,
                        height: 380,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGridData);
                        }
                    });
                }
            });

            $("#delete").on("click", function () {
                var keyValue = $("#gridtable").jfGridValue("ID");
                if (!!keyValue) {
                    var name = $("#gridtable").jfGridValue("Name");
                    Changjie.layerConfirm("是否删除【" + name + "】科目?", function (res) {
                        if (res) {
                            var url = top.$.rootUrl + '/ProjectModule/Subject/Delete';
                            var param = {
                                keyValue:keyValue
                            };
                            var data = top.Changjie.httpGet(url, param);
                            refreshGridData();
                            
                        }
                    });
                }
            });

            $("#edit").on("click", function () {
                var keyValue = $("#gridtable").jfGridValue("ID");
                if (keyValue) {
                    Changjie.layerForm({
                        id: "editSubjectForm",
                        title: "编辑收支科目",
                        url: top.$.rootUrl + '/ProjectModule/Subject/Form?keyValue=' + keyValue+'&type=edit',
                        width: 500,
                        height: 380,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGridData);
                        }
                    });
                }
            });

            $("#view").on("click", function () {
                var keyValue = $("#gridtable").jfGridValue("ID");
                if (keyValue) {
                    Changjie.layerFrom({
                        id: "viewSubjectForm",
                        title: "查看收支科目详情",
                        url: top.$.rootUrl + '/ProjectModule/Subject/Form?keyValue=' + keyValue + '&type=browse',
                        width: 500,
                        height: 380,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGridData);
                        }
                    });
                }
            });
        },
        initGrid: function () {
            $("#gridtable").mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/Subject/GetList',
                headData: [
                    { label: "科目编码", name: "Code", width: 100, align: "left" },
                    { label: "科目名称", name: "Name", width: 200, align: "left" },
                    { label: "成本属性", name: "CostAttributeName", width: 200, align: "left" },
                    {
                        label: "启用状态", name: "IsEnable", width: 100, align: "left",
                        formatter: function (value) {
                            if (value) {
                                return '<span class="label label-success" style="cursor: pointer;">已启用</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">已禁用</span>';
                            }
                        }
                    },
                    {
                        label: "是否项目科目", name: "IsProject", width: 100, align: "left",
                        formatter: function (value) {
                            if (value) {
                                return '<span class="label label-success" style="cursor: pointer;">是</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">否</span>';
                            }
                        }
                    },
                    {
                        label: "是否管理科目", name: "IsManagement", width: 100, align: "left",
                        formatter: function (value) {
                            if (value) {
                                return '<span class="label label-success" style="cursor: pointer;">是</span>';
                            }
                            else {
                                return '<span class="label label-default" style="cursor: pointer;">否</span>';
                            }
                        }
                    },
                    {
                        label: "说明", name: "Remark", width: 200, align: "left",
                        formatter: function (value, row) {
                            if (row.IsSystem) {
                                return "系统科目,不可修改或删除";
                            }
                            return value;
                        }
                    }
                ],
                mainId: "ID",
                isTree: true,
                parentId:"ParentId"
            });
        },
        search: function (param) {
            param = param || {};
            $("#gridtable").jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    var refreshGridData = function () {
        page.search();
    };
    page.init();
};