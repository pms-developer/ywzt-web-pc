﻿var keyValue = request("keyValue");
var type = request("type");
var acceptClick;
var self = "";
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            var url = top.$.rootUrl + '/ProjectModule/CostAttribute/GetList';
            var selectdata = Changjie.httpGet(url);
            $("#CostAttributeId").mkselect({
                data: selectdata.data,
                value: "ID",
                text:"Name"
            });
            $("#Name").on("input propertychange", function () {
                
                    var fullName = $("#ParentName").val() + "/" + $(this).val();
                    $("#FullName").val(fullName);
                 
 
            });
            $("#PostfixCode").on("input propertychange", function () {
                var value = $(this).val();
                if (value.length != 2) return;
                var code = $("#ParentCode").val() + value;
                if (type == "edit") {
                    if (self != value) {
                        page.checkCode(code);
                    }
                }
                else {
                    page.checkCode(code);
                }
            })
        },
        checkCode: function (code) {
            var data = Changjie.httpGet(top.$.rootUrl + '/ProjectModule/Subject/CheckCodeIsExist?code=' + code).data;
            if (data) {
                $("#PostfixCode").val("");
                Changjie.alert.warning("当前编码存在，请重新输入！");
            }
        },
        initData: function () {
            if (type == "edit" || type == "browse") {
                var url = top.$.rootUrl + "/ProjectModule/Subject/GetFormData?keyValue=" + keyValue;
                $.mkSetForm(url, function (data) {
                    $("[data-table='Base_Subject']").mkSetFormData(data);
                    page.setViewState(true);
                    
                    if (data.IsEnable) {
                        $("#IsEnable").attr("checked", true);
                    }
                    else {
                        $("#IsEnable").attr("checked", false);
                    }
                    if (data.IsProject) {
                        $("#IsProject").attr("checked", true);
                    }
                    else {
                        $("#IsProject").attr("checked", false);
                    }
                    if (data.IsManagement) {
                        $("#IsManagement").attr("checked", true);
                    }
                    else {
                        $("#IsManagement").attr("checked", false);
                    }
                    self = data.PostfixCode;
                });
            }
            else {
                if (!!keyValue) {
                    var url = top.$.rootUrl + "/ProjectModule/Subject/GetParentInfo?keyValue=" + keyValue;
                    $.mkSetForm(url, function (data) {
                        $("[data-table='Base_Subject']").mkSetFormData(data);
                        page.setViewState(true);
                        page.clearMessage();
                    });
                }
            }
        },
        clearMessage: function () {
            $("#Name").val("");
            $("#SortCode").val("");
            $("#Remark").val("");
        },
        setViewState: function (state) {
            if (state) {
                $("[viewState='readOnly']").attr("readonly", "readonly");
            }
        }
    };
    page.init();
    acceptClick = function (callBack) {
        if (type == "browse") return;
        var fullName = $("#ParentName").val() + "/" + $("#Name").val();
        $("#FullName").val(fullName);
        if (!$("body").mkValidform()) {
            return false;
        }
        var postData = {
            keyValue: keyValue,
            type:type,
            strEntity: JSON.stringify($("body").mkGetFormData())
        };
        var url = top.$.rootUrl + "/ProjectModule/Subject/SaveForm";
        $.mkSaveForm(url, postData, function (res) { });
        if (!!callBack) {
            callBack();
        }
    }
};