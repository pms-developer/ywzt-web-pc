﻿var acceptClick;
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
            page.initData();
            page.initTree();
        },
        bind: function () {
            
        },
        initData: function () { },
        initTree: function () {
            $("#subjectTree").mkscroll();
            $("#subjectTree").mktree({
                url: top.$.rootUrl + '/ProjectModule/Subject/GetTree',
                param: { parentId: '0' },
                nodeClick: page.treeNodeClick,
                getItem:page.getItem,
            });
        },
        treeNodeClick: function (item) {
            
        },
        getItem: function (item) {
            
        },
    };
    acceptClick = function (callBack) {
        var formdata = $("#subjectTree").mktreeSet("getCheckNodes");
        if (formdata.length > 0) {
            callBack(formdata);
           return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何科目。");
        }

    };
    page.init();
};