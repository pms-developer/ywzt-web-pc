﻿var refreshGirdData;
var contractId = request("projectid");
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            if (!!contractId) {
                page.bind();
            }
        },
        bind: function () {
            $("#refresh").on("click", function () {
                refreshGirdData();
            });
            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                //?contractId=' + contractId
                url: top.$.rootUrl + '/ProjectModule/ProjectSubContract/GetLeasehold',
                headData: [
                    { label: "租赁清单名称", name: "Name", width: 150, align: "left" },
                    { label: "规格型号", name: "Specification", width: 150, align: "left" },
                    { label: "租赁设备单价", name: "RentalEquipment", width: 100, align: "left" },
                    { label: "设备单位", name: "EquipmentUnit", width: 100, align: "left" },
                    { label: "设备数量", name: "Qty", width: 100, align: "left" },
                    { label: "设备租金", name: "RentalFee", width: 100, align: "left" },
                    { label: "租期单位", name: "RentalUnit", width: 100, align: "left" },
                    { label: "租期", name: "Rental", width: 100, align: "left" },
                    { label: "合计金额", name: "Summation", width: 100, align: "left" },
                   
                    { label: "备注", name: "Remarks", width: 100, align: "left" }
                ],
                mainId: 'id',
                isMultiselect: true,
                isPage: true,
                height: 280,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.projectid = contractId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        debugger;
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
