﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-07-10
 * 描  述：租赁合同
 */
var refreshGirdData;
var formId = request("formId");
var projectId = request('projectId');
var auditPassEvent;
var unauditPassEvent;
auditPassEvent = function (keyValue) {
    //alert(keyValue);
   /* Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectSubcontract/Audit', { keyValue: keyValue }, function (data) {
        alert(JSON.stringify(data));
    });*/


};

unauditPassEvent = function (keyValue) {
    //alert(keyValue);
    /*Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectSubcontract/UnAudit', { keyValue: keyValue }, function (data) {
        alert(JSON.stringify(data));
    });*/
};
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增租赁合同',
                    url: top.$.rootUrl + '/ProjectModule/ProjectSubcontractRent/Form?projectId=' + projectId,
                    width: 1000,
                    height: 800,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectSubcontract/GetPageList',
                headData: [
                    { label: "合同编码", name: "Code", width: 120, align: "left" },
                    { label: "合同名称", name: "Name", width: 120, align: "left" },
                    {
                        label: "分包类型", name: "SubcontractTypeName", width: 100, align: "left"
                    },
                    {
                        label: "甲方", name: "Jia", width: 200, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('company', {
                                key: value,
                                callback: function (_data) {
                                    callback(_data.name);
                                }
                            });
                        }
                    },
                    {
                        label: "乙方", name: "Yi", width: 120, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('sourceData', {
                                code:  'FBDW',
                                key: value,
                                keyId: 'id',
                                callback: function (_data) {
                                    callback(_data['name']);
                                }
                            });
                        }
                    },
                    { label: "合同签订金额/合同暂估金额", name: "InitAmount", width: 100, align: "left" },
                    {
                        label: "发票类型", name: "InvoiceType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op, $cell) {
                            Changjie.clientdata.getAsync('dataItem', {
                                key: value,
                                code: 'FPLX',
                                callback: function (_data) {
                                    callback(_data.text);
                                }
                            });
                        }
                    },
                    { label: "收票金额", name: "InvioceAmount", width: 100, align: "left" },
                    { label: "结算金额", name: "SettlementAmount", width: 100, align: "left" },
                    { label: "已付款金额", name: "AccountPaidAmount", width: 100, align: "left" },
                    { label: "应付款", name: "AccountPayable", width: 100, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "合同总金额", name: "TotalAmount", width: 100, align: "left" },
                    { label: "累计变更金额", name: "ChangeAmount", width: 100, align: "left" },
                ],
                mainId: 'ID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            param.ProjectId = projectId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };

    page.init();
};
// 删除
var deleteMsg = function () {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
            if (res) {
                top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectSubcontract/DeleteForm', { keyValue: keyValue }, function () {
                    refreshGirdData();
                });
            }
        });
    }
};
// 编辑
var editMsg = function (isShowConfirmBtn, title, viewState) {
    var keyValue = $('#gridtable').jfGridValue('ID');
    if (top.Changjie.checkrow(keyValue)) {
        top.Changjie.layerForm({
            id: 'form',
            title: title + '租赁合同',
            isShowConfirmBtn: isShowConfirmBtn,
            url: top.$.rootUrl + '/ProjectModule/ProjectSubcontractRent/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&projectId=" + projectId + "&formId=" + formId, 
            width: 800,
            height: 600,
            callBack: function (id) {
                return top[id].acceptClick(refreshGirdData);
            }
        });
    }
};
