﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-07-10
 * 描  述：租赁合同
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_Subcontract";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectSubcontract/SaveFormse';
var projectId = request('projectId');
var viewState = request('viewState');
var subjectId = null;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            debugger
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'CB_Leasehold', "gridId": 'ID' });

                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        calc: function () {
            //var sum = 0;
            //if ($("#InitAmount").val() != "" && $("#EngineeringAmount").val() != "") {

            //    sum = Number($("#InitAmount").val()) / Number($('#EngineeringAmount').val());
            //    sum = Number(sum * 100).toFixed(1);
            //    if (sum.indexOf("%") > -1)
            //        sum += "%";
            //    $('#subpackageEngineeringProportion').val(sum);

            //}
            //else {
            //    $('#subpackageEngineeringProportion').val("")
            //}

            var sum = 0;
            if ($("#InitAmount").val() != "" && $("#EngineeringAmount").val() != "") {
                if ($('#EngineeringAmount').val() == 0)
                    $('#subpackageEngineeringProportion').val("0.0%");
                else {
                    sum = Number(($("#InitAmount").val()).replace(/,/g, "")) / Number($('#EngineeringAmount').val());
                    sum = Number(sum * 100).toFixed(1);
                    if (sum.indexOf("%") > -1)
                        sum += "%";
                    $('#subpackageEngineeringProportion').val(sum);
                }
            }
            else {
                $('#subpackageEngineeringProportion').val("")
            }


        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'ProjectSubcontractCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/SubcontractChange/GetTotalAmount', { ProjectID: projectId }, function (data) {
                if (!$('#EngineeringAmount').val()) {
                    $('#EngineeringAmount').val(Changjie.format45(data.TotalAmount, Changjie.sysGlobalSettings.pointGlobal));
                }
            });
            $('#SubcontractType').mkselect({
                url: top.$.rootUrl + "/SystemModule/BaseContractType/GetListByProperty?property=SubContract",
                value: 'ID',
                text: "Name",
                title: "Name"

            });


            $('#Property').mkDataItemSelect({ code: 'ProjectSubcontractProperty' });
            $('#Jia').mkCompanySelect({});
            $('#Yi').mkDataSourceSelect({ code: 'FBDW', value: 'id', text: 'name' });


            $("#InitAmount").on("input propertychange", function () {
                page.setRate();
                page.calc();
            });
            $('#PriceType').mkRadioCheckbox({
                type: 'radio',
                code: 'SubcontractPriceType',
            });

            $('#InvoiceType').mkDataItemSelect({ code: 'FPLX' });
            $("#InvoiceType").on("change", function () {

                page.setRate();
            });
            $("#PriceType").on("change", function () {

                var type = $("input[name='PriceType']:checked").val();
                var $title = $("#InitAmountTitle");
                if (type == "zjht") {
                    $title.html("合同签订金额");
                }
                else {
                    $title.html("合同暂估金额");
                }
            });
            $("#SubcontractType").on("change", function () {
                var data = $(this).mkselectGetEx();
                if (data) {
                    subjectId = data.BaseSubjectId;
                }
                else {
                    subjectId = "";
                }
            });
            $('#PurchaseType').mkDataItemSelect({ code: 'PurchaseType' });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#CB_Leasehold').jfGrid({
                headData: [
                    //{
                    //    label: '编号', name: 'ID', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'string', tabname: '租赁清单'
                    //    , edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},
                    {
                        label: '租赁清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'Specification', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单',
                        edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '租赁设备单价', name: 'RentalEquipment', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '设备单位', name: 'EquipmentUnit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '设备数量', name: 'Qty', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '设备租金', name: 'RentalFee', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'InitAmount', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '租期单位', name: 'RentalUnit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: ' 租期', name: 'Rental', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var SummationQty = parseFloat(row["RentalEquipment"]) * parseFloat(row["Qty"]) * parseFloat(row["Rental"]);
                                row["Summation"] = SummationQty;
                                $("div[colname='Summation'][rownum='CB_Leasehold_" + index + "']").html(SummationQty);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合计金额', name: 'Summation', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remarks', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '租赁清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "CB_Leasehold",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                }
            });
        },
        setRate: function () {
            var temprate = $("#InvoiceType").mkselectGet();
            var tempamount = $("#InitAmount").val();
            if (temprate && tempamount) {
                var rate = parseFloat($("#InvoiceType").mkselectGet());

                $("#TaxRate").val(rate * 100);
                var price = parseFloat(Changjie.clearNumSeparator($("#InitAmount").val() || 0));
                var result = (price / (1 + rate) * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal)

                $("#TaxAmount").val(result);
            }
            else {
                $("#TaxRate,#TaxAmount").val("")
            }
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectSubcontract/GetformInfoListse?keyValue=' + keyValue, function (data) {
                    var isIsBudget = false;
                    debugger
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);

                            if (!isIsBudget) {
                                if (data[id].IsBudget) {
                                    $("#IsBudget").attr("checked", true);
                                    isIsBudget = true;
                                }
                                else {
                                    $("#IsBudget").attr("checked", false);
                                }
                            }
                        }
                    }

                    var sum = 0;
                    if ($("#InitAmount").val() != "" && $("#EngineeringAmount").val() != "") {
                        if ($('#EngineeringAmount').val() == 0)
                            $('#subpackageEngineeringProportion').val("0.0%");
                        else {
                            sum = Number(($("#InitAmount").val()).replace(/,/g, "")) / Number($('#EngineeringAmount').val());
                            sum = Number(sum * 100).toFixed(1);
                            if (sum.indexOf("%") > -1)
                                sum += "%";
                            $('#subpackageEngineeringProportion').val(sum);
                        }
                    }
                    else {
                        $('#subpackageEngineeringProportion').val("")
                    }



                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();

        if (checkBudget() == false) {
            Changjie.alert.warning("签定金额不能超过预算！");
            return false;
        }
        if ($("#IsBudget").prop("checked")) {
            if (!!subjectId) {
                var costAmount = $("#TargetCost").val();

            }
        }
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }


        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectSubcontract/SaveFormse?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    var setCellValue = function (cellName, row, index, gridName, value) {
        var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
        var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
        row[cellName] = value;
        $cell.html(value);
        $edit.val(value);
    };
    var checkBudget = function () {
        if ($("#IsBudget").prop("checked")) {
            var amount = $("#InitAmount").val();
            var param = { subjectId: subjectId, projectId: projectId, amount: amount, id: mainId };
            var data = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/CheckIsExceedBudgetByBaseId", param).data;
            if (data) { return false; }
            else { return true; }
        }
        else {
            return true;
        }

    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    //for (var item in subGrid) {
    //      deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
    //      var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
    //      if (!info.isPass) {
    //          isgridpass = false;
    //          errorInfos.push(info.errorCells);
    //      }
    //  }

    //if (!isgridpass) {
    //    for (var i in errorInfos[0]) {
    //        top.Changjie.alert.error(errorInfos[0][i].Msg);
    //    }
    //    return false;
    //}
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_Subcontract"]').mkGetFormData());
    //postData.strproject_SubcontractMaterialsList = JSON.stringify($('#Project_SubcontractMaterials').jfGridGet('rowdatas'));
    //alert(JSON.stringify($('#CB_Leasehold').jfGridGet('rowdatas')));
    postData.strCB_LeaseholdList = JSON.stringify($('#CB_Leasehold').jfGridGet('rowdatas'));
    //postData.strproject_PaymentAgreementList = JSON.stringify($('#Project_PaymentAgreement').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}