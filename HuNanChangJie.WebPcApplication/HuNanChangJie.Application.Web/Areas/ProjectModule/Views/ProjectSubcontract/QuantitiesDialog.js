﻿var refreshGirdData;
var formId = request("formId");
var contractId = request("contractId");
var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            if (!!contractId) {
                page.bind();
            }
        },
        bind: function () {
            //$("#refresh").on("click", function () {
            //    refreshGirdData();
            //});
            $("#btn_Search").on("click", function () {
                var param = {};
                var findName = $("#SearchValue").val();
                if (!!findName) {
                    param.Name = findName;
                }
                page.search(param);
            });
            $("#btn_Reset").on("click", function () {
                $("#SearchValue").val("");
                page.search();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').jfGrid({
                //?contractId=' + contractId
                url: top.$.rootUrl + '/ProjectModule/ProjectSubContract/GetQuantities',
                headData: [
                    { label: "清单编号", name: "Code", width: 150, align: "left" },
                    { label: "清单名称", name: "Name", width: 150, align: "left" },
                    { label: "项目特征", name: "Feature", width: 100, align: "left" },
                    //{ label: "计量单位", name: "Unit", width: 100, align: "left" },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },


                    {
                        label: "初始合同", name: "112222", width: 100, align: "left", align: "center", children: [
                            { label: "单价", name: "Price", width: 100, align: "left" },
                            { label: "数量", name: "Quantities", width: 100, align: "left" },
                            { label: "金额", name: "TotalPrice", width: 100, align: "left" }
                        ]
                    },
                    {
                        label: "累计变更", name: "2123332", width: 100, align: "left", align: "center", children: [
                            { label: "单价", name: "ChangePrice", width: 100, align: "left" },
                            { label: "数量", name: "ChangeQuantities", width: 100, align: "left" },
                            { label: "金额", name: "ChangeTotalPrice", width: 100, align: "left" }
                        ]
                    },
                    { label: "备注", name: "Remark", width: 100, align: "left" }
                ],
                mainId: 'id',
                isMultiselect: true,
                isPage: true,
                height: 280,
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.contractId = contractId;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }
    };

    refreshGirdData = function () {
        page.search();
    };
    page.init();
};
