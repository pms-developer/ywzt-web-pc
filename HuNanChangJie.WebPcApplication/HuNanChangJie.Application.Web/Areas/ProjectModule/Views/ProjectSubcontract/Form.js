﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-22 12:02
 * 描  述：分包合同
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_Subcontract";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectSubcontract/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var subjectId = null;

var auditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/ProjectModule/ProjectSubContract/Audit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var unauditPassEventBat = function (keyValue) {
    top.Changjie.httpPost(top.$.rootUrl + '/ProjectModule/ProjectSubContract/UnAudit', { keyValue: keyValue }, function (data) {
        if (data.code == 200)
            refreshGirdData();
        else
            top.Changjie.alert.error(data.info);
    });
};

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_SubcontractMaterials', "gridId": 'Project_SubcontractMaterials' });
                subGrid.push({ "tableName": 'Project_SubcontractQuantities', "gridId": 'Project_SubcontractQuantities' });
                subGrid.push({ "tableName": 'Project_PaymentAgreement', "gridId": 'Project_PaymentAgreement' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        calc: function () {
            var sum = 0;
            if ($("#InitAmount").val() != "" && $("#EngineeringAmount").val() != "") {
                if ($('#EngineeringAmount').val() == 0)
                    $('#subpackageEngineeringProportion').val("0.0%");
                else {
                    sum = Number(($("#InitAmount").val()).replace(/,/g, "")) / Number($('#EngineeringAmount').val());
                    sum = Number(sum * 100).toFixed(1);
                    if (sum.indexOf("%") == -1)
                        sum += "%";
                    $('#subpackageEngineeringProportion').val(sum);
                }

            }
            else {
                $('#subpackageEngineeringProportion').val("")
            }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'ProjectSubcontractCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/SubcontractChange/GetTotalAmount', { ProjectID: projectId }, function (data) {
                if (!$('#EngineeringAmount').val()) {
                    $('#EngineeringAmount').val(Changjie.format45(data.TotalAmount, Changjie.sysGlobalSettings.pointGlobal));
                }
            });

            $('#SubcontractType').mkselect({
                url: top.$.rootUrl + "/SystemModule/BaseContractType/GetListByProperty?property=SubContract",
                value: 'ID',
                text: "Name",
                title: "Name"
            });

            if (!keyValue) {
                var sqlWhere = "  project_id ='" + projectId + "' ";

                Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/DataSource/GetDataTable', { code: 'BASE_XMLB', strWhere: sqlWhere }, function (data) {
                    var marketingstaffid = "";
                    if (data) {
                        marketingstaffid = data[0].marketingstaffid;
                    }
                    $('#ProjectManager').mkformselect({
                        layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                        layerUrlW: 400,
                        layerUrlH: 300,
                        dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
                    }).mkformselectSet(marketingstaffid);
                });

            } else {
                $('#ProjectManager').mkformselect({
                    layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                    layerUrlW: 400,
                    layerUrlH: 300,
                    dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
                })
            }

            $('#ProjectMode').mkDataItemSelect({ code: 'ProjectMode' });
            $('#Property').mkDataItemSelect({ code: 'ProjectSubcontractProperty' });
            $('#Jia').mkCompanySelect({});
            $('#Yi').mkDataSourceSelect({ code: 'FBDW', value: 'id', text: 'name' });


            $("#InitAmount").on("input propertychange", function () {
                page.setRate();
                page.calc();
            });
            $('#PriceType').mkRadioCheckbox({
                type: 'radio',
                code: 'SubcontractPriceType',
            });

            $('#InvoiceType').mkDataItemSelect({ code: 'FPLX' });
            $("#InvoiceType").on("change", function () {

                page.setRate();
            });
            $("#PriceType").on("change", function () {

                var type = $("input[name='PriceType']:checked").val();
                var $title = $("#InitAmountTitle");
                if (type == "zjht") {
                    $title.html("合同签订金额");
                }
                else {
                    $title.html("合同暂估金额");
                }
            });
            $("#SubcontractType").on("change", function () {
                var data = $(this).mkselectGetEx();
                if (data) {
                    subjectId = data.BaseSubjectId;
                }
                else {
                    subjectId = "";
                }
            });
            $('#PurchaseType').mkDataItemSelect({ code: 'PurchaseType' });
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_SubcontractQuantities').jfGrid({
                headData: [
                    {
                        label: '清单编号', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '项目特征', name: 'Feature', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    //{
                    //    label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                    //    datatype: 'string', tabname: '工程量清单'
                    //    , edit: {
                    //        type: 'input',
                    //        change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                    //        },
                    //        blur: function (row, rowindex, oldValue, colname, obj, headData) {
                    //        }
                    //    }
                    //},

                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        ,
                        edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            }
                        }
                    },

                    {
                        label: '工程量', name: 'Quantities', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "Quantities", "t2": "Price", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '综合单价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "Quantities", "t2": "Price", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合价', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'InitAmount', required: '0',
                        datatype: 'float', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_SubcontractQuantities",
                isEdit: true,
                height: 300, toolbarposition: "top",
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                }
            });
            $('#Project_SubcontractMaterials').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '材料编码', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        ,
                        edit: {
                            type: 'select',
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            }
                        }
                    },
                    {
                        label: '数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Quantity", "t2": "Price", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            }
                        }
                    },
                    {
                        label: '单价', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Quantity", "t2": "Price", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '合计金额', name: 'TotalPrice', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_SubcontractMaterials",
                isEdit: true, toolbarposition: "top",
                height: 300,
                showchoose: true,
                onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectQuantities",
                        width: 800,
                        height: 400,
                        title: "选施工预算材料清单",
                        url: top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/MaterialsDialog?projectId=" + projectId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.ProjectConstructionBudgetMaterialsId = data[i].ID;
                                        row.ListCode = data[i].ListCode;
                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Brand = data[i].Brand;
                                        row.ModelNumber = data[i].ModelNumber;
                                        row.Unit = data[i].Unit;

                                        //数量、价格、金额
                                        row.Quantity = data[i].Quantity || 0;
                                        row.Price = data[i].BudgetPrice || 0;
                                        row.TotalPrice = row.Quantity * row.Price;

                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        row.MaterialsSource = data[i].MaterialsSource == 1 ? 1 : 0;
                                        row.Fk_BaseMaterialsId = data[i].Fk_BaseMaterialsId;
                                        rows.push(row);
                                    }
                                    $("#Project_SubcontractMaterials").jfGridSet("addRows", rows);
                                }
                            });
                        }
                    });
                },
                showchooses: true,
                onChooseEvents: function () {
                    Changjie.layerForm({
                        id: "selectQuantities",
                        width: 1100,
                        height: 1000,
                        title: "选择材料档案清单",
                        url: top.$.rootUrl + "/SystemModule/BaseMaterials/MaterialsDialog?projectId=" + projectId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Brand = data[i].BrandName;
                                        row.ModelNumber = data[i].Model;
                                        row.Unit = data[i].UnitId;

                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        row.MaterialsSource = 1;
                                        row.Fk_BaseMaterialsId = data[i].ID;
                                        rows.push(row);
                                    }
                                    $("#Project_SubcontractMaterials").jfGridSet("addRows", rows);

                                    var signAmount = parseFloat($("#Project_SubcontractMaterials").jfGridGet("footerdata")["Amount"] || 0);
                                    $("#Amount").val(signAmount);
                                }
                            });
                        }
                    });
                },
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                    row.MaterialsSource = 0;
                },
                onMinusRow: function (row, rows, headData) {
                }
            });
            $('#Project_PaymentAgreement').jfGrid({
                headData: [
                    {
                        label: '款项性质', name: 'FundType', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '付款比率', name: 'PaymentRate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '付款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                var price = parseFloat(Changjie.clearNumSeparator($("#InitAmount").val() || 0));
                                var rate = parseFloat(Changjie.clearNumSeparator(row["PaymentRate"] || 0));
                                var result = price * rate / 100;
                                if (!!result == false) {
                                    result = "";
                                }
                                setCellValue("PaymentAmount", row, index, "Project_PaymentAgreement", result);
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '付款金额', name: 'PaymentAmount', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '付款协议'
                    },
                    {
                        label: '预计付款日期', name: 'PrepayPaymentDate', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款协议'
                        , edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '付款责任人', name: 'OperatorId', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款协议'
                        , edit: {
                            type: 'layer',
                            change: function (data, rownum, selectData) {
                                data.OperatorId = selectData.f_realname;
                                //data.Name = selectData.f_realname;
                                $('#Project_PaymentAgreement').jfGridSet('updateRow', rownum);
                            },
                            op: {
                                width: 500,
                                height: 300,
                                listFieldInfo: [
                                    { label: "所在部门", name: "f_fullname", width: 100, align: "left" },
                                    { label: "人员姓名", name: "f_realname", width: 100, align: "left" },
                                ],
                                url: top.$.rootUrl + '/SystemModule/DataSource/GetDataTable',
                                param: { code: 'User' }
                            }
                        }
                    },
                    {
                        label: '排序', name: 'SortCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'int', tabname: '付款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '付款协议'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_PaymentAgreement",
                isEdit: true,
                height: 300,
                onAddRow: function (row, rows) {
                    row["ID"] = Changjie.newGuid();
                    row.rowState = 1;
                    row.SortCode = rows.length;
                    row.EditType = 1;
                }
            });
        },
        setRate: function () {
            var temprate = $("#InvoiceType").mkselectGet();

            //金额
            var tempamount = $("#InitAmount").val() || 0;

            //alert(tempamount);

            if (temprate && tempamount) {

                var rate = parseFloat($("#InvoiceType").mkselectGet());

                //税率
                $("#TaxRate").val(eval(rate * 100));
                // var price = parseFloat(Changjie.clearNumSeparator($("#InitAmount").val() || 0));

                // var taxAmount = (price * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal);

                //税额 （税额公式为: 金额 / (1 + 税率) * 税率）

                tempamount = parseFloat(tempamount.replace(/,/g, "")).toFixed(4);

                var taxAmount = (tempamount / (1 + rate) * rate).toFixed(Changjie.sysGlobalSettings.pointGlobal)
                $("#TaxAmount").val(taxAmount);
            }
            else {
                $("#TaxRate,#TaxAmount").val("")
            }
            page.calc();
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectSubcontract/GetformInfoList?keyValue=' + keyValue, function (data) {
                    var isIsBudget = false;
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);

                            if (!isIsBudget) {
                                if (data[id].IsBudget) {
                                    $("#IsBudget").attr("checked", true);
                                    isIsBudget = true;
                                }
                                else {
                                    $("#IsBudget").attr("checked", false);
                                }
                            }
                        }
                    }

                    if ($("#YiDesc").val() != "") {
                        $("#addsupper").trigger("click")
                    }

                    var sum = 0;
                    if ($("#InitAmount").val() != "" && $("#EngineeringAmount").val() != "") {
                        if ($('#EngineeringAmount').val() == 0)
                            $('#subpackageEngineeringProportion').val("0.0%");
                        else {
                            sum = Number(($("#InitAmount").val()).replace(/,/g, "")) / Number($('#EngineeringAmount').val());
                            sum = Number(sum * 100).toFixed(1);
                            if (sum.indexOf("%") == -1)
                                sum += "%";
                            $('#subpackageEngineeringProportion').val(sum);
                        }

                    }
                    else {
                        $('#subpackageEngineeringProportion').val("")
                    }


                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (checkBudget() == false) {
            Changjie.alert.warning("签定金额不能超过预算！");
            return false;
        }
        if ($("#IsBudget").prop("checked")) {
            if (!!subjectId) {
                var costAmount = $("#TargetCost").val();

            }
        }
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectSubcontract/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    var setCellValue = function (cellName, row, index, gridName, value) {
        var $cell = $("[rownum='" + gridName + "_" + index + "'][colname='" + cellName + "']");
        var $edit = $("#jfgrid_edit_" + gridName + "_" + cellName + "[rowindex='" + index + "']");
        row[cellName] = value;
        $cell.html(value);
        $edit.val(value);
    };
    var checkBudget = function () {
        if ($("#IsBudget").prop("checked")) {
            var amount = $("#InitAmount").val();
            var param = { subjectId: subjectId, projectId: projectId, amount: amount, id: mainId };
            var data = Changjie.httpGet(top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/CheckIsExceedBudgetByBaseId", param).data;
            if (data) { return false; }
            else { return true; }
        }
        else {
            return true;
        }

    };
    page.init();
}
var displaysupperadd = function (ee) {
    if ($(ee).hasClass("fa-plus")) {
        $("#YiDesc,#YiCode").show()
        $(ee).removeClass("fa-plus").addClass("fa-minus")
    }
    else {
        $("#YiDesc,#YiCode").hide()
        $(ee).removeClass("fa-minus").addClass("fa-plus")
    }
}

var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }

    if (($('#Yi').mkselectGet() == null || $('#Yi').mkselectGet() == "") && ($('#YiDesc').val() == "" || $('#YiCode').val() == "")) {
        top.Changjie.alert.error("请选择分包单位或者输入分包单位名称和证件号码");
        return false;
    }

    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_Subcontract"]').mkGetFormData());
    postData.strproject_SubcontractMaterialsList = JSON.stringify($('#Project_SubcontractMaterials').jfGridGet('rowdatas'));
    postData.strproject_SubcontractQuantitiesList = JSON.stringify($('#Project_SubcontractQuantities').jfGridGet('rowdatas'));
    postData.strproject_PaymentAgreementList = JSON.stringify($('#Project_PaymentAgreement').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}