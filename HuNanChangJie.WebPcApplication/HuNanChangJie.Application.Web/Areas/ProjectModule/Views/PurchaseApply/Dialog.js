﻿

var acceptClick;
var projectId = request('projectId');
var bootstrap = function ($, Changjie) {
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
            Changjie.httpGet(top.$.rootUrl + '/ProjectModule/PurchaseApply/GetPageList', { queryJson: JSON.stringify({ ProjectID: projectId }), pagination: JSON.stringify({ "rows": 100, "page": 1, "sidx": "", "sord": "ASC", "records": 0, "total": 0 }) }, function (res) {
                if (res.code == 200) {
                    $('#gridtable').jfGrid({
                        rowdatas: res.data.rows,
                        headData: [
                            { label: "采购申请单号", name: "Code", width: 100, align: "left" },
                            { label: "采购申请单名称", name: "Name", width: 100, align: "left" },
                            {
                                label: "申请人", name: "ApplyUserId", width: 100, align: "left",
                                formatterAsync: function (callback, value, row, op, $cell) {
                                    Changjie.clientdata.getAsync('user', {
                                        key: value,
                                        callback: function (_data) {
                                            callback(_data.name);
                                        }
                                    });
                                }
                            },
                            { label: "项目名称", name: "ProjectName", width: 100, align: "left" },
                            {
                                label: "申请日期", name: "ApplyDate", width: 100, align: "left",
                                formatter: function (cellvalue) {
                                    return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                                }
                            },
                            { label: "摘要", name: "Abstract", width: 100, align: "left" },
                        ],
                        mainId: 'ID',
                        isPage: true,
                        height: 362,
                    });
                }
            });
        }
    };
    acceptClick = function (callBack) {
        var formdata = $("#gridtable").jfGridGet("rowdata");
        if (!!formdata) {
            callBack(formdata);
            return true;
        }
        else {
            Changjie.alert.warning("您还没有选择任何数据");
        }

    };
    page.init();
};