﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-10 15:13
 * 描  述：采购申请
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_PurchaseApply";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/PurchaseApply/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var gridverifystate = true;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_PurchaseApplyDetails', "gridId": 'Project_PurchaseApplyDetails' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {

            var loginInfo = Changjie.clientdata.get(["userinfo"]);

            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'PurchaseApplyCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });

            $("#addMaterials").on("click", function () {

            });

            $("#deleteMaterials").on("click", function () {
                var rowIndex = $("#Project_PurchaseApplyDetails").jfGridGet("rowIndex");
                if (rowIndex != -1) {
                    $("#Project_PurchaseApplyDetails").jfGridSet("removeRow");
                }
            });


            $('#ApplyDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
            }).mkselectSet(loginInfo.departmentId);


            $('#ApplyUserId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);


            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_PurchaseApplyDetails').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'input'
                        }
                    },
                    {
                        label: '材料编码', name: 'Code', headColor: 'blue', width: 100, cellStyle: { 'text-align': 'left' }, datatype: "string",
                        edit: {
                            type: 'input'
                        }
                    },
                    {
                        label: '名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'input'
                        }
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'input'
                        }
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'input'
                        }
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' },
                        edit: {
                            type: 'select',
                            init: function (row, $self) {// 选中单元格后执行
                            },
                            change: function (row, index, item, oldValue, colname, headData) {
                            },
                            datatype: 'dataSource',
                            code: 'MaterialsUnit',
                            op: {
                                value: 'id',
                                text: 'name',
                                title: 'name'
                            },
                            readonly: true
                        }
                    },
                    {
                        label: '申请数量', name: 'ApplyQuantity', headColor: 'blue', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购申请明细', inlineOperation: { to: [{ "t1": "ApplyQuantity", "t2": "ListPrice", "to": "ApplyTotal", "type": "mul", "toTargets": null }], isFirst: true }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData, obj) {
                                if (row.DataState != 1) {
                                    var applyQuantity = parseFloat(row["ApplyQuantity"] || 0);
                                    var allowQuantity = parseFloat(row["AllowQuantity"] || 0);
                                    var materialsSource = parseFloat(row["MaterialsSource"] || 0);
                                    //  alert(materialsSource);


                                    //  if (row["MaterialsSource"] == 1 && applyQuantity > allowQuantity)

                                    if (applyQuantity > allowQuantity) {
                                        Changjie.alert.warning("【申请数量】不能超过【可申请数数量】");
                                        $(obj).css("color", "red");
                                        $(obj).attr("color", "red");
                                        gridverifystate = false;
                                    }
                                    else {
                                        $(obj).css("color", "black");
                                        $(obj).attr("color", "black");
                                        gridverifystate = true;
                                    }
                                }

                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '清单单价', name: 'ListPrice', headColor: 'blue', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'float', tabname: '采购申请明细', inlineOperation: { to: [{ "t1": "ApplyQuantity", "t2": "ListPrice", "to": "ApplyTotal", "type": "mul", "toTargets": null }], isFirst: false }
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '申请总价', name: 'ApplyTotal', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '需求日期', name: 'NeedDate', headColor: 'blue', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '采购申请明细',
                        edit: {
                            type: 'datatime',
                            change: function (row, index, oldValue, colname, headData) {
                            },
                            dateformat: '0'
                        }
                    },
                    {
                        label: '清单数量', name: 'Quantity', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '清单价格', name: 'Price', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '已申请数量', name: 'AppliedQuantity', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                    {
                        label: '可申请数量', name: 'AllowQuantity', width: 100, cellStyle: { 'text-align': 'left' }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_PurchaseApplyDetails",
                isEdit: true,
                height: 300, toolbarposition: "top", showchoose: true, onChooseEvent: function () {
                    Changjie.layerForm({
                        id: "selectQuantities",
                        width: 800,
                        height: 400,
                        title: "选施工预算材料清单",
                        url: top.$.rootUrl + "/ProjectModule/ProjectConstructionBudget/MaterialsDialog?projectId=" + projectId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};
                                        row.ProjectConstructionBudgetMaterialsId = data[i].ID;
                                        row.ProjectId = projectId;
                                        row.ListCode = data[i].ListCode;
                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Brand = data[i].Brand;
                                        row.ModelNumber = data[i].ModelNumber;
                                        row.Unit = data[i].Unit;

                                        //清单数量、价格
                                        row.Quantity = data[i].Quantity || 0;
                                        row.Price = data[i].BudgetPrice || 0;

                                        //本次申请数量
                                        row.ApplyQuantity = data[i].AllowQuantity;
                                        row.ListPrice = data[i].BudgetPrice;

                                        row.ApplyTotal = row.ApplyQuantity * row.ListPrice;

                                        //已申请数量
                                        row.AppliedQuantity = data[i].ApplyQuantity;
                                        //可申请数量
                                        row.AllowQuantity = data[i].AllowQuantity;
                                        row.Fk_BaseMaterialsId = data[i].Fk_BaseMaterialsId;
                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;


                                        row.MaterialsSource = data[i].MaterialsSource == 1 ? 1 : 0;

                                        rows.push(row);
                                    }
                                    $("#Project_PurchaseApplyDetails").jfGridSet("addRows", rows);
                                }
                            });
                        }
                    });
                },
                showchooses: true,
                onChooseEvents: function () {
                    Changjie.layerForm({
                        id: "selectQuantities",
                        width: 1100,
                        height: 1000,
                        title: "选择材料档案清单",
                        url: top.$.rootUrl + "/SystemModule/BaseMaterials/MaterialsDialog?projectId=" + projectId,
                        callBack: function (id) {
                            return top[id].acceptClick(function (data) {
                                if (!!data) {
                                    console.log(data, 9999)
                                    var rows = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var row = {};

                                        row.ProjectId = projectId;
                                        row.Code = data[i].Code;
                                        row.Name = data[i].Name;
                                        row.Brand = data[i].BrandName;
                                        row.ModelNumber = data[i].Model;
                                        row.Unit = data[i].UnitId;
                                        row.Fk_BaseMaterialsId = data[i].ID;
                                        row.rowState = 1;
                                        row.ID = Changjie.newGuid();
                                        row.EditType = 1;
                                        row.DataState = 1;
                                        row.MaterialsSource = 1;

                                        rows.push(row);
                                    }
                                    $("#Project_PurchaseApplyDetails").jfGridSet("addRows", rows);

                                    var signAmount = parseFloat($("#Project_PurchaseApplyDetails").jfGridGet("footerdata")["Amount"] || 0);
                                    $("#Amount").val(signAmount);
                                }
                            });
                        }
                    });
                },
                onAddRowEvent: function () {
                    var rows = [];
                    var row = {};
                    row.ProjectId = projectId;
                    row.rowState = 1;
                    row.ID = Changjie.newGuid();
                    row.EditType = 1;
                    row.MaterialsSource = 0;
                    rows.push(row);
                    $("#Project_PurchaseApplyDetails").jfGridSet("addRows", rows);
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/PurchaseApply/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id); $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (gridverifystate == false) {
            Changjie.alert.warning("数据验证失败。");
            return false;
        }
        var postData = getFormData();
        console.log(postData);

        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/PurchaseApply/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_PurchaseApply"]').mkGetFormData());
    postData.strproject_PurchaseApplyDetailsList = JSON.stringify($('#Project_PurchaseApplyDetails').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
