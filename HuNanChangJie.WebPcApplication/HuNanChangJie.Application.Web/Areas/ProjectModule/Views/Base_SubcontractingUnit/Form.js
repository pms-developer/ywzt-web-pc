﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-03-20 11:46
 * 描  述：分包单位
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var tables=[];
var mainTable="Base_SubcontractingUnit";
var processCommitUrl=top.$.rootUrl + '/ProjectModule/Base_SubcontractingUnit/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                 $('#btn_uploadfile').attr('disabled', true);
                 $('#btn_delfile').attr('disabled', true);
           }
        },
        bind: function () {
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'CJ_BASE_0022' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#Source').mkDataItemSelect({ code: 'KHLY' });
            $('#UnitType').mkDataItemSelect({ code: 'FBDWLX' });
            $('#Nature').mkDataItemSelect({ code: 'FBDWHZXZ' });
            $('#CooperationStatus').mkRadioCheckbox({
                type: 'radio',
                code: 'HZZT',
            });
            $('#Grade').mkRadioCheckbox({
                type: 'radio',
                code: 'PGXJ',
            });
            $('#ProvinceId').mkProvinceSelect({
                code: 'Province',
                value: 'F_AreaId',
                text: 'F_AreaName',
            });

            $("#ProvinceId").on('change', function () {
                var parentId = $(this).mkselectGet();
                if (!!parentId) {
                    $('#CityId').mkSubAreaSelect({
                        parentId: parentId,
                        value: 'F_AreaId',
                        text: 'F_AreaName'
                    });
                }
            });
            $("#form_tabs_sub").systemtables({
               type:type,
               keyvalue:mainId,
               state:"newinstance",
               isShowAttachment:true,
               isShowWorkflow:true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/Base_SubcontractingUnit/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            tables.push(id);                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
    var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/Base_SubcontractingUnit/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 top.Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
    } if (type == "add") {
        keyValue = mainId;
    }
        var postData = {
            strEntity: JSON.stringify($('body').mkGetFormData()),
            deleteList: JSON.stringify(deleteList),
        };
     return postData;
}
