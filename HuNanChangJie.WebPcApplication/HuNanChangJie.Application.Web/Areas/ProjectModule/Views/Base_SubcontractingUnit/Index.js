﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-03-20 11:46
 * 描  述：分包单位
 */
var refreshGirdData;
var formId = request("formId");
var checkJson = "";

var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                checkJson = queryJson;
                page.search(queryJson);
            }, 220, 400);
            $('#Grade').mkRadioCheckbox({
                type: 'radio',
                code: 'PGXJ',
            });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增分包单位',
                    url: top.$.rootUrl + '/ProjectModule/Base_SubcontractingUnit/Form',
                    width: 800,
                    height: 650,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
          
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/Base_SubcontractingUnit/GetPageList',
                headData: [
                    { label: "编号", name: "Code", width: 150, align: "left"},
                    { label: "单位名称", name: "Name", width: 200, align: "left" },
                    { label: "证件号码", name: "IdNumber", width: 100, align: "left" },
                    { label: "单位来源", name: "Source", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'KHLY',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "单位类型", name: "UnitType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'FBDWLX',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "单位性质", name: "Nature", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'FBDWHZXZ',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "合作状态", name: "CooperationStatus", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'HZZT',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    { label: "等级", name: "Grade", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'PGXJ',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }
                    },
                    { label: "服务项目", name: "ServiceProject", width: 100, align: "left" },

                    { label: "分数", name: "Fraction", width: 100, align: "left" },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }},
                    {
                        label: "注册日期", name: "RegisterDate", width: 100, align: "left", formatter: function (cellvalue, row) {
                            if (cellvalue.length > 10)
                                return cellvalue.substring(0, 10);
                            return cellvalue;
                        }},
                    { label: "注册资金", name: "RegisterFund", width: 100, align: "left"},
                    
                    { label: "省", name: "ProvinceId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'Province',
                                 key: value,
                                 keyId: 'f_areaid',
                                 callback: function (_data) {
                                     callback(_data['f_areaname']);
                                 }
                             });
                        }},
                    { label: "市", name: "CityId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('sourceData', {
                                 code:  'CityOrDistrict',
                                 key: value,
                                 keyId: 'f_areaid',
                                 callback: function (_data) {
                                     callback(_data['f_areaname']);
                                 }
                             });
                        }},
                    { label: "电话", name: "Tel", width: 100, align: "left"}, 
                ],
                mainId:'ID',
                isPage: true
            });
            page.search(checkJson);
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search(checkJson);
    };
    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/Base_SubcontractingUnit/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '分包单位',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/Base_SubcontractingUnit/Form?keyValue=' + keyValue + '&viewState=' + viewState + "&formId=" + formId, 
                        width: 800,
                        height: 650,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
