﻿/* *  
 * 创建人：超级管理员
 * 日  期：2020-02-24 10:56
 * 描  述：工程签证
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId = "";
var type = "add";
var subGrid = [];
var tables = [];
var mainTable = "Project_Visa";
var processCommitUrl = top.$.rootUrl + '/ProjectModule/ProjectVisa/SaveForm';
var projectId = request('projectId');
var viewState = request('viewState');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if (!!projectId) {
                $("#ProjectID").val(projectId);
            }
            if (!!keyValue) {
                mainId = keyValue;
                type = "edit";
            }
            else {
                mainId = top.Changjie.newGuid();
                subGrid.push({ "tableName": 'Project_VisaMaterials', "gridId": 'Project_VisaMaterials' });
                subGrid.push({ "tableName": 'Project_VisaQuantities', "gridId": 'Project_VisaQuantities' });
                type = "add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (viewState == '1') {
                $('#btn_uploadfile').attr('disabled', true);
                $('#btn_delfile').attr('disabled', true);
            }
        },
        bind: function () {
            //$("#addQuantity").on("click", function () {

            //});
            //$("#deleteQuantity").on("click", function () {
            //    var rowIndex = $("#Project_VisaQuantities").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Project_VisaQuantities").jfGridSet("removeRow");
            //    }
            //});

            //$("#addMaterials").on("click", function () {

            //});
            //$("#deleteMaterials").on("click", function () {
            //    var rowIndex = $("#Project_VisaMaterials").jfGridGet("rowIndex");
            //    if (rowIndex != -1) {
            //        $("#Project_VisaMaterials").jfGridSet("removeRow");
            //    }
            //});
            var loginInfo = Changjie.clientdata.get(['userinfo']);
            $('#ContractName').on("click", function () {
                Changjie.layerForm({
                    id: "selectContract",
                    title: "选择合同",
                    url: top.$.rootUrl + '/ProjectModule/ProjectContract/Dialog?projectId=' + projectId,
                    width: 800,
                    hegiht: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(function (data) {
                            if (!!data) {
                                page.bindContractInfo(data);
                            }
                        });
                    }
                });
            });
            Changjie.httpAsync('GET', top.$.rootUrl + '/SystemModule/CodeRule/GetEnCode', { code: 'ProjectVisaCode' }, function (data) {
                if (!$('#Code').val()) {
                    $('#Code').val(data);
                }
            });
            $('#VisaType').mkDataItemSelect({ code: 'ProjectVisaType' });
            $('#OperatorId').mkformselect({
                layerUrl: top.$.rootUrl + '/OrganizationModule/User/SelectOnlyForm',
                layerUrlW: 400,
                layerUrlH: 300,
                dataUrl: top.$.rootUrl + '/OrganizationModule/User/GetListByUserIds'
            }).mkformselectSet(loginInfo.userId);
            $('#OperatorDepartmentId').mkselect({
                type: 'tree',
                allowSearch: true,
                url: top.$.rootUrl + '/OrganizationModule/Department/GetTree',
                param: {}
            }).mkselectSet(loginInfo.departmentId);
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');
            $('#Project_VisaQuantities').jfGrid({
                headData: [
                    {
                        label: '清单编号', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '清单名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '项目特征', name: 'Feature', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '初始合同', name: 'l7c1c7ef0341e436895750653476e4bbc', align: 'center', width: 100, cellStyle: { 'text-align': 'left' },
                        children:
                            [
                                { label: '综合单价', name: 'InitPrice', width: 100, align: 'center', cellStyle: { 'text-align': 'left' } },
                                { label: '工程量', name: 'InitQuantities', width: 100, align: 'center', cellStyle: { 'text-align': 'left' } },
                                { label: '合价', name: 'InitTotal', width: 100, align: 'center', cellStyle: { 'text-align': 'left' } }
                            ]
                    },
                    {
                        label: '本次签证', name: 'le3aae640327847749ef787b1857d64b9', align: 'center', width: 100, cellStyle: { 'text-align': 'left' },
                        children: [
                            {
                                label: '综合单价', name: 'Price', width: 100, headColor: "blue", align: 'center', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        page.calc()
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '工程量', name: 'Quantities', width: 100, headColor: "blue", align: 'center', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                                datatype: 'float', tabname: '工程量清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantities", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                                , edit: {
                                    type: 'input',
                                    change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                        page.calc()
                                    },
                                    blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                    }
                                }
                            },
                            {
                                label: '合价', name: 'TotalPrice', width: 100, align: 'center', cellStyle: { 'text-align': 'left' }, aggtype: 'sum', aggcolumn: 'VisaPrice', required: '0',
                                datatype: 'float', tabname: '工程量清单'
                            },
                        ]
                    },
                    {
                        label: '行备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '工程量清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_VisaQuantities",
                isEdit: true,
                height: 300, toolbarposition: "top", showchoose: true,
                onChooseEvent: function () {
                    var contractId = $("#ContractId").val();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectContarctQuantities",
                            width: 800,
                            height: 400,
                            title: "选择合同工程量清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectContract/QuantitiesDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectContractQuantitiesId = data[i].ID;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Feature = data[i].Feature;
                                            row.Unit = data[i].Unit;
                                            row.SortCode = data[i].SortCode;

                                            row.InitPrice = data[i].Price;
                                            row.InitQuantities = data[i].Quantities;
                                            row.InitTotal = data[i].TotalPrice;

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.ProjectId = projectId;

                                            rows.push(row);
                                        }
                                        $("#Project_VisaQuantities").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择合同");
                    }
                },
                onAddRowEvent: function () {
                    var contractId = $("#ContractId").val();
                    if (!!contractId) {
                        var rows = [];
                        var row = {};
                        row.ProjectContractQuantitiesId = Changjie.newGuid();
                        row.rowState = 1;
                        row.ID = Changjie.newGuid();
                        row.EditType = 1;
                        row.ProjectId = projectId;
                        rows.push(row);
                        $("#Project_VisaQuantities").jfGridSet("addRows", rows);
                    }
                    else {
                        Changjie.alert.warning("请您先选择合同");
                    }
                }
            });
            $('#add').on('click', function () {
                var row1 = {};
                row1.ID = Changjie.newGuid();
                row1.ProjectId = projectId;
                row1.rowState = 1;
                row1.EditType = 1;
                $("#Project_VisaQuantities").jfGridSet("addRow", row1);
            });



            $('#Project_VisaMaterials').jfGrid({
                headData: [
                    {
                        label: '清单编码', name: 'ListCode', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '材料编码', name: 'Code', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '名称', name: 'Name', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '品牌', name: 'Brand', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '规格型号', name: 'ModelNumber', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '计量单位', name: 'Unit', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单', edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                    {
                        label: '初始合同', name: 'l7a889b9f473f43849eac3c141d19bf8f', align: 'center', width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [{
                            label: '单价', name: 'InitPrice', align: 'center', width: 100, cellStyle: { 'text-align': 'left' }
                        },
                        {
                            label: '数量', name: 'InitQuantity', align: 'center', width: 100, cellStyle: { 'text-align': 'left' }
                        },
                        {
                            label: '金额', name: 'InitTotal', align: 'center', width: 100, cellStyle: { 'text-align': 'left' }
                        },
                        ]
                    },
                    {
                        label: '本次签证', name: 'lbc0575666b1d44719b3c2e4c6d2a5c68', align: 'center', width: 100, cellStyle: { 'text-align': 'left' }
                        , children: [{
                            label: '单价', name: 'Price', width: 100, align: 'center', headColor: "blue", cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: true }
                            , edit: {
                                type: 'input',
                                change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    page.calc()
                                },
                                blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                }
                            }
                        },
                        {
                            label: '数量', name: 'Quantity', width: 100, headColor: "blue", align: 'center', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '材料清单', inlineOperation: { to: [{ "t1": "Price", "t2": "Quantity", "to": "TotalPrice", "type": "mul", "toTargets": null }], isFirst: false }
                            , edit: {
                                type: 'input',
                                change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                                    page.calc()
                                },
                                blur: function (row, rowindex, oldValue, colname, obj, headData) {
                                }
                            }
                        },
                        {
                            label: '金额', name: 'TotalPrice', width: 100, align: 'center', cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                            datatype: 'float', tabname: '材料清单'
                        },
                        ]
                    },
                    {
                        label: '备注', name: 'Remark', width: 100, cellStyle: { 'text-align': 'left' }, aggtype: 'normal', aggcolumn: '', required: '0',
                        datatype: 'string', tabname: '材料清单'
                        , edit: {
                            type: 'input',
                            change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                            },
                            blur: function (row, rowindex, oldValue, colname, obj, headData) {
                            }
                        }
                    },
                ],
                mainId: "ID",
                bindTable: "Project_VisaMaterials",
                isEdit: true,
                height: 300, toolbarposition: "top", showchoose: true,
                onChooseEvent: function () {
                    var contractId = $("#ContractId").val();
                    if (!!contractId) {
                        Changjie.layerForm({
                            id: "selectContractMaterials",
                            width: 800,
                            height: 400,
                            title: "选择合同材料清单",
                            url: top.$.rootUrl + "/ProjectModule/ProjectContract/MaterialsDialog?contractId=" + contractId,
                            callBack: function (id) {
                                return top[id].acceptClick(function (data) {
                                    if (!!data) {
                                        var rows = [];
                                        for (var i = 0; i < data.length; i++) {
                                            var row = {};
                                            row.ProjectContractMaterialsId = data[i].ID;
                                            row.ListCode = data[i].ListCode;
                                            row.Code = data[i].Code;
                                            row.Name = data[i].Name;
                                            row.Brand = data[i].Brand;
                                            row.ModelNumber = data[i].ModelNumber;
                                            row.Unit = data[i].Unit;
                                            row.SortCode = data[i].SortCode;

                                            row.InitPrice = data[i].Price;
                                            row.InitQuantity = data[i].Quantity;
                                            row.InitTotal = data[i].TotalPrice;

                                            row.rowState = 1;
                                            row.ID = Changjie.newGuid();
                                            row.EditType = 1;
                                            row.ProjectId = projectId;
                                            row.MaterialsType = 1;
                                            rows.push(row);
                                        }
                                        $("#Project_VisaMaterials").jfGridSet("addRows", rows);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Changjie.alert.warning("请您先选择合同");
                    }
                },
                onAddRowEvent: function () {
                    var contractId = $("#ContractId").val();
                    if (!!contractId) {
                        var rows = [];
                        var row = {};
                        row.ProjectContractMaterialsId = Changjie.newGuid();
                        row.rowState = 1;
                        row.ID = Changjie.newGuid();
                        row.EditType = 1;
                        row.ProjectId = projectId;
                        row.MaterialsType = 0;
                        rows.push(row);
                        $("#Project_VisaMaterials").jfGridSet("addRows", rows);
                    }
                    else {
                        Changjie.alert.warning("请您先选择合同");
                    }
                }
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/ProjectModule/ProjectVisa/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id).jfGridSet('refreshdata', data[id], 'edit');
                            subGrid.push({ "tableName": id, "gridId": id });
                        }
                        else {
                            tables.push(id);
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                    page.loadContractInfo($("#ContractId").val());
                });
            }
        },
        loadContractInfo: function (contractId) {
            var url = top.$.rootUrl + '/ProjectModule/ProjectContract/GetFormData?keyValue=' + contractId;
            top.Changjie.httpAsyncGet(url, function (data) {
                if (data.code == 200) {
                    page.bindContractInfo(data.data.data);
                }
            });
        },
        calc() {
            var total = 0;
            var cvv = $('#Project_VisaMaterials').jfGridGet("rowdatas");

            for (var i = 0; i < cvv.length; i++) {
                if (cvv[i].TotalPrice && !isNaN(cvv[i].TotalPrice)) {
                    total += cvv[i].TotalPrice
                }
            }

            var cvvv = $('#Project_VisaQuantities').jfGridGet("rowdatas");

            for (var i = 0; i < cvvv.length; i++) {
                if (cvvv[i].TotalPrice && !isNaN(cvvv[i].TotalPrice)) {
                    total += cvvv[i].TotalPrice
                }
            }

            $("#VisaPrice").val(total)
        },
        bindContractInfo: function (data) {
            $("#ContractName").val(data.Name);
            $("#ContractPrice").val(data.InitAmount);
            $("#VisaAmount").val(data.VisaAmount || 0);
            $("#ContractId").val(data.ID);
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        var postData = getFormData();
        if (postData == false) return false;
        if (type == "add") {
            keyValue = mainId;
        }
        $.mkSaveForm(top.$.rootUrl + '/ProjectModule/ProjectVisa/SaveForm?keyValue=' + keyValue + '&type=' + type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
var getFormData = function () {
    if (!$('body').mkValidform()) {
        return false;
    }
    var deleteList = [];
    var isgridpass = true;
    var errorInfos = [];
    for (var item in subGrid) {
        deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
        var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
        if (!info.isPass) {
            isgridpass = false;
            errorInfos.push(info.errorCells);
        }
    }
    if (!isgridpass) {
        for (var i in errorInfos[0]) {
            top.Changjie.alert.error(errorInfos[0][i].Msg);
        }
        return false;
    }
    if (type == "add") {
        keyValue = mainId;
    }
    var postData = {};
    postData.strEntity = JSON.stringify($('[data-table="Project_Visa"]').mkGetFormData());
    postData.strproject_VisaMaterialsList = JSON.stringify($('#Project_VisaMaterials').jfGridGet('rowdatas'));
    postData.strproject_VisaQuantitiesList = JSON.stringify($('#Project_VisaQuantities').jfGridGet('rowdatas'));
    postData.deleteList = JSON.stringify(deleteList);
    return postData;
}
