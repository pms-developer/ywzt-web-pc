﻿/* * Copyright (c) 2013-2019 
 * 创建人：超级管理员
 * 日  期：2020-02-24 10:56
 * 描  述：工程签证
 */
var projectId = request("projectId");
var formId = request("formId");
var refreshGirdData;
var auditPassEvent;
var unauditPassEvent;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增工程签证',
                    url: top.$.rootUrl + '/ProjectModule/ProjectVisa/Form?projectId=' + projectId,
                    width: 800,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
         
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/ProjectModule/ProjectVisa/GetPageList',
                headData: [
                    { label: "签证编码", name: "Code", width: 150, align: "left"},
                    { label: "合同签订金额", name: "InitAmount", width: 100, align: "left"},
                    {
                        label: "合同名称", name: "ContractName", width: 150, align: "left",
                        formatter: function (cellvalue, row, item, value, op, index) {
                            Changjie.httpAsync('GET', top.$.rootUrl + '/ProjectModule/ProjectContract/GetFormData', { keyValue: row["ContractId"] }, function (data) {
                                row["ContractName"] = data.data.Name;
                                row["InitAmount"] = data.data.InitAmount;
                                $("div[colname='ContractName'][rownum='gridtable_" + index + "']").html(data.data.Name);
                                $("div[colname='ContractName'][rownum='gridtable_" + index + "']").attr('title', data.data.Name);

                                $("div[colname='VisaAmount'][rownum='gridtable_" + index + "']").html(data.data.VisaAmount||0);
                                $("div[colname='VisaAmount'][rownum='gridtable_" + index + "']").attr('title', data.data.VisaAmount||0);

                                $("div[colname='InitAmount'][rownum='gridtable_" + index + "']").html(data.data.InitAmount);
                                $("div[colname='InitAmount'][rownum='gridtable_" + index + "']").attr('title', data.data.InitAmount);
                            });
                        }
                    },
                    { label: "累计签定金额", name: "VisaPrice", width: 100, align: "left"},
                    { label: "签证类型", name: "VisaType", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('dataItem', {
                                 key: value,
                                 code: 'ProjectVisaType',
                                 callback: function (_data) {
                                     callback(_data.text);
                                 }
                             });
                        }},
                    //{
                    //    label: "本次签证金额", name: "VisaAmount", width: 100, align: "left"
                    //},
                    {
                        label: "签证日期", name: "VisaDate", width: 100, align: "left",
                        formatter: function (cellvalue) {
                            return Changjie.formatDate(cellvalue, 'yyyy-MM-dd');
                        }
                    },
                    { label: "经办人", name: "OperatorId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('user', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }},
                    { label: "项目名称", name: "ProjectName", width: 150, align: "left"},
                    { label: "经办部门", name: "OperatorDepartmentId", width: 100, align: "left",
                        formatterAsync: function (callback, value, row, op,$cell) {
                             Changjie.clientdata.getAsync('department', {
                                 key: value,
                                 callback: function (_data) {
                                     callback(_data.name);
                                 }
                             });
                        }
                    },
                    {
                        label: "审核状态", name: "AuditStatus", width: 100, align: "left",
                        formatter: function (cellvalue, row) {
                            return top.Changjie.tranAuditStatus(cellvalue);
                        }
                    },
                    { label: "摘要", name: "Abstract", width: 100, align: "left"},
                    { label: "签证原因", name: "Remark", width: 100, align: "left"},
                ],
                mainId:'ID',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            param.ProjectID = projectId;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };

    auditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectVisa/Audit', { keyValue: keyValue }, function (data) {
        });
    };

    unauditPassEvent = function (keyValue) {
        Changjie.httpAsync('POST', top.$.rootUrl + '/ProjectModule/ProjectVisa/UnAudit', { keyValue: keyValue }, function (data) {
        });
    };

    page.init();
};
            // 删除
            var deleteMsg = function () {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            top.Changjie.deleteForm(top.$.rootUrl + '/ProjectModule/ProjectVisa/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            };
            // 编辑
            var editMsg = function (isShowConfirmBtn, title, viewState) {
                var keyValue = $('#gridtable').jfGridValue('ID');
                if (top.Changjie.checkrow(keyValue)) {
                    top.Changjie.layerForm({
                        id: 'form',
                        title: title + '工程签证',
                        isShowConfirmBtn: isShowConfirmBtn,
                        url: top.$.rootUrl + '/ProjectModule/ProjectVisa/Form?keyValue=' + keyValue + '&viewState=' + viewState + '&projectId=' + projectId + "&formId=" + formId, 
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            };
