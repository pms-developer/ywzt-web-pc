﻿using HuNanChangJie.Util;
using System.Data;
using HuNanChangJie.Application.TwoDevelopment.Desktop;
using System.Web.Mvc;
using System.Collections.Generic;

namespace HuNanChangJie.Application.Web.Areas.Desktop.Controllers
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-06 15:24
    /// 描 述：WorkflowTest
    /// </summary>
    public class WorkflowTestController : MvcControllerBase
    {
        private WorkflowTestIBLL workflowTestIBLL = new WorkflowTestBLL();

        #region  视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            XqPagination paginationobj = pagination.ToObject<XqPagination>();
            var data = workflowTestIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetformInfoList(string keyValue)
        {
            var WorkflowTestMainData = workflowTestIBLL.GetWorkflowTestMainEntity( keyValue );
            var WorkflowTestSubData = workflowTestIBLL.GetWorkflowTestSubList( WorkflowTestMainData.ID );
            var jsonData = new {
                WorkflowTestMain = WorkflowTestMainData,
                WorkflowTestSub = WorkflowTestSubData,
            };
            return Success(jsonData);
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            workflowTestIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue,string type, string strEntity, string strworkflowTestSubList,string deleteList)
        {
            var mainInfo = strEntity.ToObject<WorkflowTestMainEntity>();
            var workflowTestSubList = strworkflowTestSubList.ToObject<List<WorkflowTestSubEntity>>();
            workflowTestIBLL.SaveEntity(keyValue,mainInfo,workflowTestSubList,deleteList,type);
            return Success("保存成功！");
        }
        #endregion

    }
}
