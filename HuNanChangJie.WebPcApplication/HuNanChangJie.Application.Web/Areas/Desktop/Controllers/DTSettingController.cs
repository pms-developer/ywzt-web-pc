﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuNanChangJie.Util;

namespace HuNanChangJie.Application.Web.Areas.Desktop.Controllers
{
    public class DTSettingController : MvcControllerBase
    {
        // GET: LR_Desktop/DTSetting
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult PcIndex()
        {
            string learn_UItheme = WebHelper.GetCookie("hncjpms_UItheme");
            switch (learn_UItheme)
            {
                case "1":
                    return View("PC/AdminDefault/AdminDefault");     // 经典版本桌面配置
                case "2":
                    return View("PC/AdminAccordion/AdminAccordion");    // 风尚版桌面配置
                case "3":
                    return View("PC/AdminWindos/AdminWindos");       // 炫动版桌面配置
                case "4":
                    return View("PC/AdminTop/AdminTop");          // 飞扬版桌面配置
                default:
                    return View("PC/AdminDefault/AdminDefault");      // 经典版本桌面配置
            }
            return View();
        }

        public ActionResult AppIndex()
        {
            return View();
        }
    }
}