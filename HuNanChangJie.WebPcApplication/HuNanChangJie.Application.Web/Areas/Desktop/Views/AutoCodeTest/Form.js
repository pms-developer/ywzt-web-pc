﻿/* *  
 * 创建人：超级管理员
 * 日  期：2019-11-19 17:27
 * 描  述：AutoCodeTest
 */
var acceptClick;
var keyValue = request('keyValue');
var mainId="";
var type="add";
var subGrid=[];
var projectId = request('projectId');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            if(!!keyValue){
               mainId=keyValue;
               type="edit";
            }
            else{
               mainId=top.Changjie.newGuid();
               type="add";
            }
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
            if (type == 'add') {
                page.loadProjectInfo();
            }
            var ctltype11=$("#Name").attr("type");;
            var ctltype12=$("#Remark").attr("type");;
            if (ctltype11 == 'text' && ctltype12 == 'text') {
                 $('#Name').on('input propertychange', function () {
                     var value1 = $(this).val();
                     var value2 = $('#Remark').val();
                     $('#Code').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
                 $('#Remark').on('input propertychange', function () {
                     var value1 = $('#Name').val();
                     var value2 = $(this).val();
                     $('#Code').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
            }
            else if (ctltype11 == 'text' && ctltype12 == 'mkselect') {
                 $('#Name').on('input propertychange', function () {
                     var value1 = $(this).val();
                     var value2 = $('#Remark').mkselectGetText();
                     $('#Code').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
                 $('#Remark').mkselect().on('change', function () {
                     var value1 = $('#Name').val();
                     var value2 = $(this).mkselectGetText();
                     $('#Code').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
            }
            else if (ctltype11 == 'mkselect' && ctltype12 == 'text') {
                 $('#Name').mkselect().on('change', function () {
                     var value1 = $(this).mkselectGetText();
                     var value2 = $('#Remark').val();
                     $('#Code').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
                 $('#Remark').on('input propertychange', function () {
                     var value1 = $('#Name').mkselectGetText();
                     var value2 = $(this).val();
                     $('#Code').val(top.Changjie.simpleMath(value1, value2, 'mul', 2));
                 });
            }
        },
        bind: function () {
            $("#form_tabs_sub").systemtables({
                type: type,
                keyvalue: mainId,
                state: "extend",
                isShowAttachment: true,
                isShowWorkflow: true,
            });
            $('#form_tabs_sub').mkFormTab();
            $('#form_tabs_sub ul li').eq(0).trigger('click');

            
            $('#MainGridSub').jfGrid({
                headData: [
                    {
                        label: '价格', name: 'Price', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'1',
                        datatype:'float', tabname:'清单' ,inlineOperation:{to:[{"t1":"Price","t2":"Unit","to":"Description","type":"mul","toTargets":null},{"t1":"Price","t2":"Description","to":"pinyinShort","type":"sub","toTargets":null}],isFirst:true}
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '税率', name: 'Unit', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'string', tabname:'清单' ,inlineOperation:{to:[{"t1":"Price","t2":"Unit","to":"Description","type":"mul","toTargets":null},{"t1":"Price","t2":"Description","to":"pinyinShort","type":"sub","toTargets":null}],isFirst:false}
                        ,edit:{
                            type:'select',
                            change: function (row, index, item, oldValue, colname,headData) {
                            },
                            datatype: 'dataItem',
                            code:'CW0101'
                        }
                    },
                    {
                        label: '不含税金额', name: 'Description', width:100,cellStyle: { 'text-align': 'left' },aggtype:'normal',aggcolumn:'',required:'0',
                        datatype:'float', tabname:'清单' ,inlineOperation:{to:[{"t1":"Price","t2":"Description","to":"pinyinShort","type":"sub","toTargets":null}],isFirst:false}
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                    {
                        label: '含税金额', name: 'pinyinShort', width:100,cellStyle: { 'text-align': 'left' },aggtype:'sum',aggcolumn:'pinyin',required:'0',
                        datatype:'float', tabname:'清单' 
                        ,edit:{
                            type:'input',
                           change: function (row, index, oldValue, currentColname, rows, aggtype, aggcolumn, headData) {
                           },
                           blur: function (row, rowindex, oldValue, colname, obj, headData) {
                           }
                        }
                    },
                ],
                mainId:"ID",
                bindTable:"MainGridSub",
                isEdit: true,
                height: 300,
                rowdatas:[
                {
                     "SortCode": 1,
                     "rowState": 1,
                     "ID": Changjie.newGuid(),
                     "EditType": 1,
                     "Project_ID": projectId,
                     "ProjectName": $("#ProjectName").val(),
                }],
                onAddRow: function(row,rows){
                     row["ID"]=Changjie.newGuid();
                     row.rowState=1;
                    row.SortCode=rows.length;
                     row.EditType=1;
                     row.Project_ID=projectId;
                     row.ProjectName=$("#ProjectName").val();
                     for (var item in rows) {
                     	if (!!row.ProjectName) {
                     		rows[item].ProjectName = row.ProjectName;
                     	}
                     }
                 },
                onMinusRow: function(row,rows,headData){
                }
            });

        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/Desktop/AutoCodeTest/GetformInfoList?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id],'edit');
                            subGrid.push({"tableName":id,"gridId":id});                        }
                        else {
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        },
        loadProjectInfo: function () {
             if (!!projectId) {
                 top.Changjie.setProjectInfo('form_tabs');
             }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var deleteList=[];
        var isgridpass=true;
        var errorInfos=[];
        for(var item in subGrid){
           deleteList.push({ "TableName": subGrid[item].tableName, "idList": $('#' + subGrid[item].gridId).jfGridDelKeys().toString() })
           var info = $('#' + subGrid[item].gridId).jfGridDataVerify();
           if (!info.isPass) {
              isgridpass = false;
              errorInfos.push(info.errorCells);
           }
         }
         if (!isgridpass) {
             for (var i in errorInfos[0]) {
                 Changjie.alert.error(errorInfos[0][i].Msg);
             }
             return false;
         }
        if(type=="add"){
           keyValue=mainId;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="MainA"]').mkGetFormData());
        postData.strmainGridSubList = JSON.stringify($('#MainGridSub').jfGridGet('rowdatas'));
        postData.deleteList=JSON.stringify(deleteList);
        $.mkSaveForm(top.$.rootUrl + '/Desktop/AutoCodeTest/SaveForm?keyValue=' + keyValue+'&type='+type, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
