﻿/*
 * 创建人：超级管理员
 * 日  期：2019-06-28 16:40
 * 描  述：报表测试
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var map = {};
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
            // 导出
            $('#outport').on('click', function () {
                Changjie.download({
                    method: 'POST',
                    url: '/Utility/ExportExcel',
                    param: {
                        fileName: '导出数据列表',
                        columnJson: JSON.stringify($('#gridtable').jfGridGet('settingInfo').headData),
                        dataJson: JSON.stringify($('#gridtable').jfGridGet('settingInfo').rowdatas)
                    }
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/Desktop/测试报表/GetList',
                headData: [
                    { label: "f_userid", name: "f_userid", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_encode", name: "f_encode", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_account", name: "f_account", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_password", name: "f_password", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_secretkey", name: "f_secretkey", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_realname", name: "f_realname", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_nickname", name: "f_nickname", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_headicon", name: "f_headicon", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_quickquery", name: "f_quickquery", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_simplespelling", name: "f_simplespelling", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_gender", name: "f_gender", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_birthday", name: "f_birthday", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_mobile", name: "f_mobile", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_telephone", name: "f_telephone", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_email", name: "f_email", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_oicq", name: "f_oicq", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_wechat", name: "f_wechat", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_msn", name: "f_msn", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_companyid", name: "f_companyid", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_departmentid", name: "f_departmentid", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_securitylevel", name: "f_securitylevel", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_openid", name: "f_openid", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_question", name: "f_question", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_answerquestion", name: "f_answerquestion", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_checkonline", name: "f_checkonline", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_allowstarttime", name: "f_allowstarttime", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_allowendtime", name: "f_allowendtime", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "f_lockstartdate", name: "f_lockstartdate", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_lockenddate", name: "f_lockenddate", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_sortcode", name: "f_sortcode", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_deletemark", name: "f_deletemark", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_enabledmark", name: "f_enabledmark", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "f_description", name: "f_description", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "CreationDate", name: "CreationDate", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "Creation_Id", name: "Creation_Id", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "CreationName", name: "CreationName", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "ModificationDate", name: "ModificationDate", width: 100, cellStyle: { 'text-align': 'left' } } ,
                    { label: "Modification_Id", name: "Modification_Id", width: 100, cellStyle: { 'text-align': 'left' } },
                    { label: "ModificationName", name: "ModificationName", width: 100, cellStyle: { 'text-align': 'left' } } ,
                ],
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
