﻿/* 
 * 创建人：超级管理员
 * 日  期：2018-09-25 10:57
 * 描  述：消息配置
 */
var refreshGirdData;
var selectedRow;
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                selectedRow = null;
                Changjie.layerForm({
                    id: "Form",
                    title: "添加",
                    url: top.$.rootUrl + "/Desktop/DTList/Form",
                    width: 600,
                    height: 500,
                    btn: null
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                selectedRow = $("#gridtable").AgGridGet("rowdata");
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: "Form",
                        title: "编辑",
                        url: top.$.rootUrl + "/Desktop/DTList/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').AgGridValue('F_Id');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/Desktop/DTList/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeAgGrid({
                url: top.$.rootUrl + '/Desktop/DTList/GetList',
                headData: [{
                        label: "名称",
                        name: "F_Name",
                        width: 150,
                     
                    },
                    {
                        label: "图标",
                        name: "F_Icon",
                        width: 60,
                        
                        cellRenderer: function (d) {
                            return '<i class="' + d.value + '" ></i>';
                        }
                    },
                    {
                        label: "功能地址",
                        name: "F_Url",
                        width: 200,
                     
                    },
                    {
                        label: "查看地址",
                        name: "F_ItemUrl",
                        width: 200,
                     
                    },
                    {
                        label: "创建用户",
                        name: "CreationName",
                        width: 100,
                       
                    },
                    {
                        label: "创建时间",
                        name: "CreationDate",
                        width: 130,
                       
                        formatter: function (d) {
                            return Changjie.formatDate(d, "yyyy-MM-dd hh:mm");
                        }
                    },
                    {
                        label: "说明",
                        name: "F_Description",
                        width: 200,
                        align: "left"
                    }],
                mainId: 'F_Id',
                isPage: false
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').AgGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
