﻿var refreshGirdData;
var selectedRow = null;
var switchbtn = 1;
var bootstrap = function (a, d) {
    var g = {};
    var e = {};
    var b = {};
    var c = {};
    var f = {
        init: function () {
            a("#target_content").mkscroll();
            a("#desktop_list").mkscroll();
            f.bind();
            d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                function (h) {
                    f.target(h || [])
                });
            d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                function (h) {
                    f.list(h || [])
                });
            d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetPageList", {},
                function (h) {
                    f.chart(h || [])
                });
            return
        },
        target: function (j) {
            g = {};
            var k = 250;
            var i = 0;
            var l = a("#target_content").width();
            var k = l / j.length;
            if (k < 250) {
                k = 250
            }
            i = k * j.length;
            var h = a("#target_content .mk-scroll-box");
            h.css("width", i);
            a.each(j,
                function (n, o) {
                    g[o.F_Id] = o;
                    var m = '                    <div class="mk-item-20 targetItem" data-Id="' + o.F_Id + '">                        <div class="task-stat">                            <div class="visual">                                <i class="' + o.F_Icon + '"></i>                            </div>                            <div class="number" data-value="' + o.F_Id + '"></div>                            <div class="desc">' + o.F_Name + '</div>                            <div class="tool" data-Id="' + o.F_Id + '">                                <div>编辑</div>                                <div>删除</div>                            </div>                        </div>                    </div>';
                    h.append(m);
                    top.Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetSqlData", {
                        Id: o.F_Id
                    },
                        function (p) {
                            if (p) {
                                h.find('[data-value="' + p.Id + '"]').text(p.value)
                            }
                        })
                });
            h.find(".mk-item-20").css("width", k);
            a("#target_content").resize(function () {
                var n = a("#target_content").width();
                var m = n / j.length;
                if (m < 250) {
                    m = 250
                }
                i = m * j.length;
                h.css("width", i);
                h.find(".mk-item-20").css("width", m)
            })
        },
        list: function (i) {
            var h = a("#mk-add-list");
            e = {};
            a.each(i,
                function (k, l) {
                    e[l.F_Id] = l;
                    var j = '                <div class="mk-desktop-list listItem"  data-Id="' + l.F_Id + '">                    <div class="title">                        ' + l.F_Name + '<div class="tool"><div>编辑</div><div>删除</div></div>                    </div>                    <div class="content" data-value="' + l.F_Id + '">                    </div>                </div>';
                    h.before(j);
                    top.Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetSqlData", {
                        Id: l.F_Id
                    },
                        function (n) {
                            if (n) {
                                var m = a('[data-value="' + n.Id + '"]');
                                a.each(n.value,
                                    function (q, r) {
                                        var p = '                            <div class="mk-list-line">                                <div class="point"></div>                                <div class="text">' + r.f_title + '</div>                                <div class="date">' + r.f_time + "</div>                            </div>";
                                        var o = a(p);
                                        o[0].item = r;
                                        m.append(o)
                                    })
                            }
                        })
                })
        },
        chart: function (j) {
            b = {};
            c = {};
            var i = a(".mk-desktop-chat-panel-title");
            var h = a(".mk-desktop-chat-panel-content");
            a.each(j,
                function (l, m) {
                    c[m.F_Id] = m;
                    var n = '<div class="title-item chartItem' + (l == 0 ? " active" : "") + '" data-value="' + m.F_Id + '_Changjie_" >' + m.F_Name + "</div>";
                    i.append(n);
                    var k = '<div class="content-item chartItem' + (l == 0 ? " active" : "") + '" id="' + m.F_Id + '_Changjie_"  data-type="' + m.F_Type + '"><div  id="' + m.F_Id + '" class="chartc" ></div><div class="tool" data-Id="' + m.F_Id + '">                                <div>编辑</div>                                <div>删除</div>                            </div></div >';
                    h.append(k);
                    b[m.F_Id] = echarts.init(document.getElementById(m.F_Id));
                    top.Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetSqlData", {
                        Id: m.F_Id
                    },
                        function (o) {
                            if (o) {
                                var r = a("#" + o.Id + "_Changjie_").attr("data-type");
                                var p = [];
                                var s = [];
                                a.each(o.value,
                                    function (t, u) {
                                        p.push(u.name);
                                        s.push(u.value)
                                    });
                                var q = {};
                                switch (r) {
                                    case "0":
                                        q.tooltip = {
                                            trigger: "item",
                                            formatter: "{a} <br/>{b} : {c} ({d}%)"
                                        };
                                        q.legend = {
                                            bottom: "bottom",
                                            data: p
                                        };
                                        q.series = [{
                                            name: "占比",
                                            type: "pie",
                                            radius: "75%",
                                            center: ["50%", "50%"],
                                            label: {
                                                normal: {
                                                    formatter: "{b}:{c}: ({d}%)",
                                                    textStyle: {
                                                        fontWeight: "normal",
                                                        fontSize: 12,
                                                        color: "#000"
                                                    }
                                                }
                                            },
                                            data: o.value,
                                            itemStyle: {
                                                emphasis: {
                                                    shadowBlur: 10,
                                                    shadowOffsetX: 0,
                                                    shadowColor: "rgba(0, 0, 0, 0.5)"
                                                }
                                            }
                                        }];
                                        q.color = ["#df4d4b", "#304552", "#52bbc8", "rgb(224,134,105)", "#8dd5b4", "#5eb57d", "#d78d2f"];
                                        break;
                                    case "1":
                                        q.tooltip = {
                                            trigger: "axis"
                                        };
                                        q.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        q.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: p
                                        };
                                        q.yAxis = {
                                            type: "value"
                                        };
                                        q.series = [{
                                            type: "line",
                                            data: s
                                        }];
                                        break;
                                    case "2":
                                        q.tooltip = {
                                            trigger: "axis"
                                        };
                                        q.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        q.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: p
                                        };
                                        q.yAxis = {
                                            type: "value"
                                        };
                                        q.series = [{
                                            type: "bar",
                                            data: s
                                        }];
                                        break
                                }
                                b[o.Id].setOption(q)
                            }
                        })
                });
            window.onresize = function (k) {
                a.each(b,
                    function (l, m) {
                        m.resize(k)
                    })
            }
        },
        bind: function () {
            a("#target_content").on("click", ".tool>div",
                function () {
                    var h = a(this);
                    var k = h.text();
                    var i = h.parent().attr("data-Id");
                    var j = g[i];
                    selectedRow = j;
                    switchbtn = 1;
                    if (k === "编辑") {
                        d.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        d.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    d.deleteForm(top.$.rootUrl + "/Desktop/DTTarget/DeleteForm", {
                                        keyValue: j.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                    return false
                });
            a("#mk-add-target").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 1;
                    d.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            a("#desktop_list").on("click", ".listItem .tool>div",
                function () {
                    var h = a(this);
                    var k = h.text();
                    var i = h.parents(".listItem").attr("data-Id");
                    var j = e[i];
                    selectedRow = j;
                    switchbtn = 2;
                    if (k === "编辑") {
                        d.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTList/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        d.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    d.deleteForm(top.$.rootUrl + "/Desktop/DTList/DeleteForm", {
                                        keyValue: j.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            a("#mk-add-list").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 2;
                    d.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTList/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            a(".mk-desktop-chat-panel-title").on("click", ".title-item",
                function (j) {
                    var i = a(this);
                    if (!i.hasClass("active")) {
                        var h = i.parent();
                        h.find(".active").removeClass("active");
                        i.addClass("active");
                        var k = i.attr("data-value");
                        a(".mk-desktop-chat-panel-content .active").removeClass("active");
                        a("#" + k).addClass("active");
                        b[k.replace("_Changjie_", "")].resize(j)
                    }
                });
            a(".mk-desktop-chat").on("click", ".chartItem .tool>div",
                function () {
                    var h = a(this);
                    var k = h.text();
                    var i = h.parent().attr("data-Id");
                    var j = c[i];
                    selectedRow = j;
                    switchbtn = 3;
                    if (k === "编辑") {
                        d.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTChart/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        d.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    d.deleteForm(top.$.rootUrl + "/Desktop/DTChart/DeleteForm", {
                                        keyValue: j.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            a("#mk-add-chart").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 3;
                    d.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTChart/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                })
        }
    };
    refreshGirdData = function () {
        switch (switchbtn) {
            case 1:
                d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                    function (h) {
                        a(".targetItem").remove();
                        f.target(h || [])
                    });
                break;
            case 2:
                d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                    function (h) {
                        a(".listItem").remove();
                        f.list(h || [])
                    });
                break;
            case 3:
                d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetPageList", {},
                    function (h) {
                        a(".chartItem").remove();
                        f.chart(h || [])
                    });
                break
        }
    };
    f.init()
};