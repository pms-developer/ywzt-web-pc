﻿var refreshGirdData;
var selectedRow = null;
var switchbtn = 1;
var bootstrap = function ($, d) {
    var g = {};
    var e = {};
    var b = {};
    var c = {};
    var page = {
        init: function () {
            $(".mk-desktop-panel").mkscroll();
            $("#target").mkscroll();
            page.bind();
            d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                function (data) {
                    page.target(data || []);
                });
            d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                function (data) {
                    page.list(data || []);
                });
            d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetPageList", {},
                function (data) {
                    page.chart(data || []);
                })
        },
        target: function (j) {
            var k = 210;
            var i = 0;
            var l = $("#target").width() - 10;
            var k = l / j.length;
            if (k < 210) {
                k = 210;
            }
            i = k * j.length;
            var h = $("#target .mk-scroll-box");
            h.css("width", i);
            g = {};
            $.each(j,
                function (n, o) {
                    g[o.F_Id] = o;
                    var m = '<div class="mk-item-20 targetItem">' +
                        '<div class="task-stat" >' +
                        '<div class="visual">' +
                        '<i class="' + o.F_Icon + '"></i>' +
                        '</div>' +
                        '<div class="details">' +
                        '<div class="number" data-value="' + o.F_Id + '"></div>' +
                        '<div class="desc">' + o.F_Name + "</div>" +
                        "</div>";
                    if (o.F_Url) {
                        m += '<a class="more" data-Id="' + o.F_Id + '" >查看更多 <i class="fa fa-arrow-circle-right"></i></a>';
                    }
                    m += '<div class="tool" data-Id="' + o.F_Id + '"><div>编辑</div><div>删除</div>                    </div>';
                    m += "</div></div>";
                    h.append(m);
                    top.Changjie.httpAsync("GET",
                        top.$.rootUrl + "/Desktop/DTTarget/GetSqlData",
                        {
                            Id: o.F_Id
                        },
                        function (p) {
                            if (p) {
                                h.find('[data-value="' + p.Id + '"]').text(p.value);
                            }
                        });
                });
            h.find(".mk-item-20 .more").on("click",
                function () {
                    var m = $(this).attr("data-Id");
                    top.Changjie.frameTab.open({
                        F_ModuleId: m,
                        F_FullName: g[m].F_Name,
                        F_UrlAddress: g[m].F_Url
                    });
                    return false;
                });
            h.find(".mk-item-20").css("width", k);
            $("#target").resize(function () {
                var n = $("#target").width() - 10;
                var twidth = n / j.length;
                if (twidth < 210) {
                    twidth = 210;
                }
                i = twidth * j.length;
                h.css("width", i);
                h.find(".mk-item-20").css("width", twidth);
            });
        },
        list: function (i) {
            var hmkaddlist = $("#mk-add-list");
            e = {};
            $.each(i,
                function (k, l) {
                    e[l.F_Id] = l;
                    var mklistitem = '<div class="col-xs-6 listItem" data-Id="' + l.F_Id + '">' +
                        '<div class="portal-panel-title"><i class="' + l.F_Icon + '"></i>&nbsp;&nbsp;' + l.F_Name +
                        '<div class="tool"><div>编辑</div><div>删除</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="portal-panel-content" style="overflow: hidden; padding-top: 20px; padding-left: 30px; padding-right: 50px;height:225px;" data-value="' + l.F_Id + '" >' +
                        '</div>' +
                        '</div>';
                    hmkaddlist.before(mklistitem);
                    top.Changjie.httpAsync("GET",
                        top.$.rootUrl + "/Desktop/DTList/GetSqlData",
                        {
                            Id: l.F_Id
                        },
                        function (n) {
                            if (n) {
                                var m = $('[data-value="' + n.Id + '"]');
                                $.each(n.value,
                                    function (q, r) {
                                        var p =
                                            '<div class="mk-msg-line">' +
                                            '<a href="#" style="text-decoration: none;" >' + r.f_title + "</a>" +
                                            "<label>" + r.f_time + "</label>" +
                                            "</div>";
                                        var o = $(p);
                                        o.find("a")[0].item = r;
                                        m.append(o);
                                    });
                            }
                        });
                });
        },
        chart: function (i) {
            var h = $("#mk-add-chart");
            b = {};
            c = {};
            $.each(i,
                function (k, l) {
                    c[l.F_Id] = l;
                    var j = '                <div class="col-xs-' + (12 / parseInt(l.F_Proportion1)) + '  chartItem" data-Id="' + l.F_Id + '">                    <div class="portal-panel-title">                        <i class="' + l.F_Icon + '"></i>&nbsp;&nbsp;' + l.F_Name + '<div class="tool"><div>编辑</div><div>删除</div></div>                    </div>                    <div class="portal-panel-content">                        <div id="' + l.F_Id + '" class="mk-chart-container" data-type="' + l.F_Type + '"   ></div>                    </div>                </div>';
                    h.before(j);
                    b[l.F_Id] = echarts.init(document.getElementById(l.F_Id));
                    top.Changjie.httpAsync("GET",
                        top.$.rootUrl + "/Desktop/DTChart/GetSqlData",
                        {
                            Id: l.F_Id
                        },
                        function (m) {
                            if (m) {
                                var p = $("#" + m.Id).attr("data-type");
                                var n = [];
                                var q = [];
                                $.each(m.value,
                                    function (r, s) {
                                        n.push(s.name);
                                        q.push(s.value)
                                    });
                                var o = {};
                                switch (p) {
                                    case "0":
                                        o.legend = {
                                            bottom: "bottom",
                                            data: n
                                        };
                                        o.series = [
                                            {
                                                name: "占比",
                                                type: "pie",
                                                radius: "75%",
                                                center: ["50%", "50%"],
                                                label: {
                                                    normal: {
                                                        formatter: "{b}:{c}: ({d}%)",
                                                        textStyle: {
                                                            fontWeight: "normal",
                                                            fontSize: 12,
                                                            color: "#000"
                                                        }
                                                    }
                                                },
                                                data: m.value,
                                                itemStyle: {
                                                    emphasis: {
                                                        shadowBlur: 10,
                                                        shadowOffsetX: 0,
                                                        shadowColor: "rgba(0, 0, 0, 0.5)"
                                                    }
                                                }
                                            }
                                        ];
                                        o.color = [
                                            "#df4d4b", "#304552", "#52bbc8", "rgb(224,134,105)", "#8dd5b4", "#5eb57d",
                                            "#d78d2f"
                                        ];
                                        break;
                                    case "1":
                                        o.tooltip = {
                                            trigger: "axis"
                                        };
                                        o.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        o.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: n
                                        };
                                        o.yAxis = {
                                            type: "value"
                                        };
                                        o.series = [
                                            {
                                                type: "line",
                                                data: q
                                            }
                                        ];
                                        break;
                                    case "2":
                                        o.tooltip = {
                                            trigger: "axis"
                                        };
                                        o.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        o.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: n
                                        };
                                        o.yAxis = {
                                            type: "value"
                                        };
                                        o.series = [
                                            {
                                                type: "bar",
                                                data: q
                                            }
                                        ];
                                        break;
                                    case "3":

                                        o.title = {
                                            text: '',
                                            subtext: '',
                                            link: '',
                                            left: 'center',
                                            textStyle: {
                                                color: '#fff',
                                                fontSize: 16,
                                                fontWeight: 'normal',
                                                fontFamily: "Microsoft YaHei"
                                            },
                                            subtextStyle: {
                                                color: '#ccc',
                                                fontSize: 13,
                                                fontWeight: 'normal',
                                                fontFamily: "Microsoft YaHei"
                                            }
                                        };
                                        o.tooltip = {
                                            trigger: 'item',
                                            formatter: '{b}'
                                        };
                                        o.toolbox = {
                                            show: true,
                                            orient: 'vertical',
                                            left: 'right',
                                            top: 'center',
                                            feature: {
                                                dataView: { readOnly: false },
                                                restore: {},
                                                saveAsImage: {}
                                            },
                                            iconStyle: {
                                                normal: {
                                                    color: '#fff'
                                                }
                                            }
                                        };
                                        o.animationDuration = 1000;
                                        o.animationEasing = 'cubicOut';
                                        o.animationDurationUpdate = 1000;
                                        o.bmap = {
                                            center: [104.114129, 37.550339],
                                            zoom: 5,
                                            roam: true,
                                            mapStyle: {
                                                styleJson: [
                                                    {
                                                        "featureType": "water",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "color": "#044161"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "land",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "color": "#004981"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "boundary",
                                                        "elementType": "geometry",
                                                        "stylers": {
                                                            "color": "#064f85"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "railway",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "highway",
                                                        "elementType": "geometry",
                                                        "stylers": {
                                                            "color": "#004981"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "highway",
                                                        "elementType": "geometry.fill",
                                                        "stylers": {
                                                            "color": "#005b96",
                                                            "lightness": 1
                                                        }
                                                    },
                                                    {
                                                        "featureType": "highway",
                                                        "elementType": "labels",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "arterial",
                                                        "elementType": "geometry",
                                                        "stylers": {
                                                            "color": "#004981"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "arterial",
                                                        "elementType": "geometry.fill",
                                                        "stylers": {
                                                            "color": "#00508b"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "poi",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "green",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "color": "#056197",
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "subway",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "manmade",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "local",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "arterial",
                                                        "elementType": "labels",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "boundary",
                                                        "elementType": "geometry.fill",
                                                        "stylers": {
                                                            "color": "#029fd4"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "building",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "color": "#1a5787"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "label",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                        o.series = {};
                                        break;


                                }


                                b[m.Id].setOption(o);
                            }
                        });
                });
            window.onresize = function (j) {
                $.each(b,
                    function (k, l) {
                        l.resize(j);
                    });
            }
        },
        bind: function () {
            $("#target").on("click", ".tool>div",
                function () {
                    var h = $(this);
                    var k = h.text();
                    var i = h.parent().attr("data-Id");
                    var j = g[i];
                    selectedRow = j;
                    switchbtn = 1;
                    if (k === "编辑") {
                        d.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        d.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    d.deleteForm(top.$.rootUrl + "/Desktop/DTTarget/DeleteForm", {
                                        keyValue: j.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            $("#mk-add-target").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 1;
                    d.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            $(".mk-desktop-panel").on("click", ".listItem .tool>div",
                function () {
                    var h = $(this);
                    var k = h.text();
                    var i = h.parents(".listItem").attr("data-Id");
                    var j = e[i];
                    selectedRow = j;
                    switchbtn = 2;
                    if (k === "编辑") {
                        d.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTList/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        d.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    d.deleteForm(top.$.rootUrl + "/Desktop/DTList/DeleteForm", {
                                        keyValue: j.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            $("#mk-add-list").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 2;
                    d.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTList/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            $(".mk-desktop-panel").on("click", ".chartItem .tool>div",
                function () {
                    var h = $(this);
                    var k = h.text();
                    var i = h.parents(".chartItem").attr("data-Id");
                    var j = c[i];
                    selectedRow = j;
                    switchbtn = 3;
                    if (k === "编辑") {
                        d.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTChart/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        d.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    d.deleteForm(top.$.rootUrl + "/Desktop/DTChart/DeleteForm", {
                                        keyValue: j.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            $("#mk-add-chart").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 3;
                    d.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTChart/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                })
        }
    };
    refreshGirdData = function () {
        switch (switchbtn) {
            case 1:
                d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                    function (h) {
                        $(".targetItem").remove();
                        page.target(h || []);
                    });
                break;
            case 2:
                d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                    function (h) {
                        $(".listItem").remove();
                        page.list(h || []);
                    });
                break;
            case 3:
                d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetPageList", {},
                    function (h) {
                        $(".chartItem").remove();
                        page.chart(h || []);
                    });
                break;
        }
    };
    page.init();
};