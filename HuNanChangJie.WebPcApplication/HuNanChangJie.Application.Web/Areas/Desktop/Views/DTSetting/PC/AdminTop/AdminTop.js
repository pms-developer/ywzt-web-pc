﻿var refreshGirdData;
var selectedRow = null;
var switchbtn = 1;
var bootstrap = function (a, d) {
    var g = {};
    var e = {};
    var b = {};
    var c = {};
    var f = {
        init: function () {
            a("#target_content").mkscroll();
            a("#desktop_list").mkscroll();
            a("#desktop_chart").mkscroll();
            f.bind();
            d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                function (h) {
                    f.target(h || [])
                });
            d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                function (h) {
                    f.list(h || [])
                });
            d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetPageList", {},
                function (h) {
                    f.chart(h || [])
                })
        },
        target: function (i) {
            var h = a("#target_content .mk-scroll-box");
            a.each(i,
                function (k, l) {
                    g[l.F_Id] = l;
                    var j = '                    <div class="target-item targetItem" data-Id="' + l.F_Id + '">                        <div class="count"><span data-value="' + l.F_Id + '"></span></div>                        <div class="point"><span></span></div>                        <div class="content">                            <i class="' + l.F_Icon + '"></i>                            <div class="text">' + l.F_Name + '</div>                        </div>                        <div class="targetItemtool" ><div class="tool" data-Id="' + l.F_Id + '">                            <div>编辑</div>                            <div>删除</div>                        </div></div>                    </div>';
                    h.append(j);
                    top.Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetSqlData", {
                        Id: l.F_Id
                    },
                        function (m) {
                            if (m) {
                                h.find('[data-value="' + m.Id + '"]').text(m.value)
                            }
                        })
                })
        },
        list: function (i) {
            var h = a("#mk-add-list");
            e = {};
            a.each(i,
                function (k, l) {
                    e[l.F_Id] = l;
                    var j = '                <div class="mk-desktop-list listItem"  data-Id="' + l.F_Id + '">                    <div class="title">                        ' + l.F_Name + '<div class="tool"><div>编辑</div><div>删除</div></div>                    </div>                    <div class="content" data-value="' + l.F_Id + '">                    </div>                </div>';
                    h.before(j);
                    top.Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetSqlData", {
                        Id: l.F_Id
                    },
                        function (n) {
                            if (n) {
                                var m = a('[data-value="' + n.Id + '"]');
                                a.each(n.value,
                                    function (q, r) {
                                        var p = '                            <div class="mk-list-line">                                <div class="point"></div>                                <div class="text">' + r.f_title + '</div>                                <div class="date">' + r.f_time + "</div>                            </div>";
                                        var o = a(p);
                                        o[0].item = r;
                                        m.append(o)
                                    })
                            }
                        })
                })
        },
        chart: function (i) {
            var h = a("#mk-add-chart");
            b = {};
            c = {};
            a.each(i,
                function (k, l) {
                    c[l.F_Id] = l;
                    var j = '                <div class="col-xs-' + (12 / parseInt(l.F_Proportion1)) + ' chartItem"  data-Id="' + l.F_Id + '" >                    <div class="mk-desktop-chart">                        <div class="title">' + l.F_Name + '<div class="tool"><div>编辑</div><div>删除</div></div></div>                        <div class="content"  id="' + l.F_Id + '"  data-type="' + l.F_Type + '"></div>                    </div>                </div>';
                    h.before(j);
                    b[l.F_Id] = echarts.init(document.getElementById(l.F_Id));
                    top.Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetSqlData", {
                        Id: l.F_Id
                    },
                        function (m) {
                            if (m) {
                                var p = a("#" + m.Id).attr("data-type");
                                var n = [];
                                var q = [];
                                a.each(m.value,
                                    function (r, s) {
                                        n.push(s.name);
                                        q.push(s.value)
                                    });
                                var o = {};
                                switch (p) {
                                    case "0":
                                        o.tooltip = {
                                            trigger: "item",
                                            formatter: "{a} <br/>{b} : {c} ({d}%)"
                                        };
                                        o.legend = {
                                            bottom: "bottom",
                                            data: n
                                        };
                                        o.series = [{
                                            name: "占比",
                                            type: "pie",
                                            radius: "75%",
                                            center: ["50%", "50%"],
                                            label: {
                                                normal: {
                                                    formatter: "{b}:{c}: ({d}%)",
                                                    textStyle: {
                                                        fontWeight: "normal",
                                                        fontSize: 12,
                                                        color: "#000"
                                                    }
                                                }
                                            },
                                            data: m.value,
                                            itemStyle: {
                                                emphasis: {
                                                    shadowBlur: 10,
                                                    shadowOffsetX: 0,
                                                    shadowColor: "rgba(0, 0, 0, 0.5)"
                                                }
                                            }
                                        }];
                                        o.color = ["#df4d4b", "#304552", "#52bbc8", "rgb(224,134,105)", "#8dd5b4", "#5eb57d", "#d78d2f"];
                                        break;
                                    case "1":
                                        o.tooltip = {
                                            trigger: "axis"
                                        };
                                        o.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        o.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: n
                                        };
                                        o.yAxis = {
                                            type: "value"
                                        };
                                        o.series = [{
                                            type: "line",
                                            data: q
                                        }];
                                        break;
                                    case "2":
                                        o.tooltip = {
                                            trigger: "axis"
                                        };
                                        o.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        o.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: n
                                        };
                                        o.yAxis = {
                                            type: "value"
                                        };
                                        o.series = [{
                                            type: "bar",
                                            data: q
                                        }];
                                        break
                                }
                                b[m.Id].setOption(o)
                            }
                        })
                });
            window.onresize = function (j) {
                a.each(b,
                    function (k, l) {
                        l.resize(j)
                    })
            }
        },
        bind: function () {
            a("#target_content").on("click", ".tool>div",
                function () {
                    var h = a(this);
                    var k = h.text();
                    var i = h.parent().attr("data-Id");
                    var j = g[i];
                    selectedRow = j;
                    switchbtn = 1;
                    if (k === "编辑") {
                        d.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        d.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    d.deleteForm(top.$.rootUrl + "/Desktop/DTTarget/DeleteForm", {
                                        keyValue: j.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            a("#mk-add-target").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 1;
                    d.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            a(".mk-desktop-panel").on("click", ".listItem .tool>div",
                function () {
                    var h = a(this);
                    var k = h.text();
                    var i = h.parents(".listItem").attr("data-Id");
                    var j = e[i];
                    selectedRow = j;
                    switchbtn = 2;
                    if (k === "编辑") {
                        d.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTList/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        d.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    d.deleteForm(top.$.rootUrl + "/Desktop/DTList/DeleteForm", {
                                        keyValue: j.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            a("#mk-add-list").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 2;
                    d.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTList/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            a(".mk-desktop-panel").on("click", ".chartItem .tool>div",
                function () {
                    var h = a(this);
                    var k = h.text();
                    var i = h.parents(".chartItem").attr("data-Id");
                    var j = c[i];
                    selectedRow = j;
                    switchbtn = 3;
                    if (k === "编辑") {
                        d.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTChart/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        d.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    d.deleteForm(top.$.rootUrl + "/Desktop/DTChart/DeleteForm", {
                                        keyValue: j.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            a("#mk-add-chart").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 3;
                    d.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTChart/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                })
        }
    };
    refreshGirdData = function () {
        switch (switchbtn) {
            case 1:
                d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                    function (h) {
                        a(".targetItem").remove();
                        f.target(h || [])
                    });
                break;
            case 2:
                d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                    function (h) {
                        a(".listItem").remove();
                        f.list(h || [])
                    });
                break;
            case 3:
                d.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetPageList", {},
                    function (h) {
                        a(".chartItem").remove();
                        f.chart(h || [])
                    });
                break
        }
    };
    f.init()
};