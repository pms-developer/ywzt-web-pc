﻿var refreshGirdData;
var selectedRow = null;
var switchbtn = 1;
var bootstrap = function ($, Changjie) {
    var g = {};
    var mkListHtml = {};
    var b = {};
    var c = {};
    var page = {
        init: function () {
            $("#target_content").mkscroll();
            $("#desktop_list").mkscroll();
            $("#desktop_chart").mkscroll();
            page.bind();
            Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                function (data) {
                    page.target(data || []);
                });
            Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                function (data) {
                    page.list(data || []);
                });
            Changjie.httpAsync("GET",
                top.$.rootUrl + "/Desktop/DTChart/GetPageList",
                {},
                function (data) {
                    page.chart(data || []);
                });
        },
        target: function (i) {
            var mkaddtarget = $("#mk-add-target");
            $.each(i,
                function (k, targetItem) {
                    g[targetItem.F_Id] = targetItem;
                    var targetiHtml = '<div class="task-stat targetItem" data-Id="' +  targetItem.F_Id + '">' +
                        '                   <div class="visual">' +
                        '                       <i class="' +targetItem.F_Icon + '"></i>' +
                        '                   </div>' +
                        '                   <div class="iconbg"><i class="' +  targetItem.F_Icon + '"></i>' +
                        '                   </div>' +
                        '                   <div class="number" data-value="' +  targetItem.F_Id + '"></div>' +
                        '                   <div class="desc">' +  targetItem.F_Name + '</div>' +
                        '                   <div class="tool" data-Id="' + targetItem.F_Id + '">' +
                        '                       <div>编辑</div>' +
                        '                       <div>删除</div>' +
                        '                   </div>' +
                        '               </div>';
                    mkaddtarget.before(targetiHtml);
                    top.Changjie.httpAsync("GET",
                        top.$.rootUrl + "/Desktop/DTTarget/GetSqlData",
                        {
                            Id: targetItem.F_Id
                        },
                        function (data) {
                            if (data) {
                                $('[data-value="' + data.Id + '"]').text(data.value);
                            }
                        });
                });
        },
        list: function (datalist) {
            var mkaddlist = $("#mk-add-list");
            mkListHtml = {};
            $.each(datalist,
                function (index, item) {
                    mkListHtml[item.F_Id] = item;
                    //添加模板
                    var mkdesktoplist = '<div class="mk-desktop-list listItem"  data-Id="' +
                        item.F_Id +
                        '">' +
                        '                <div class="title">' +
                        item.F_Name +
                        '' +
                        '                   <div class="tool">' +
                        '                       <div>编辑</div>' +
                        '                       <div>删除</div>' +
                        '                   </div>' +
                        '               </div>' +
                        '               <div class="content" data-value="' +
                        item.F_Id +
                        '"></div>' +
                        '           </div>';
                    mkaddlist.before(mkdesktoplist);

                    //获取数据
                    top.Changjie.httpAsync("GET",
                        top.$.rootUrl + "/Desktop/DTList/GetSqlData",
                        {
                            Id: item.F_Id
                        },
                        //callbak 函数
                        function (data) {
                            if (data) {
                                var dv = $('[data-value="' + data.Id + '"]');
                                $.each(data.value,
                                    function (i, mitem) {
                                        var plistline =
                                            '<div class="mk-list-line">' +
                                            '   <div class="point"></div>' +
                                            '       <div class="text">' + mitem.f_title + '</div>' +
                                            '       <div class="date">' + mitem.f_time + "</div>" +
                                            "</div>";
                                        var listline = $(plistline);
                                        listline[0].item = mitem;
                                        dv.append(listline);
                                    });
                            }
                        });
                });
        },
        chart: function (chartlist) {
            var mkaddchart = $("#mk-add-chart");
            b = {};
            c = {};
            $.each(chartlist,
                function (k, l) {
                    c[l.F_Id] = l;
                    var j = '<div class="mk-desktop-chart chartItem" data-Id="' + l.F_Id + '" >' +
                        '       <div class="title">' + l.F_Name + '' +
                        '           <div class="tool">' +
                        '               <div>编辑</div>' +
                        '               <div>删除</div>' +
                        '           </div>' +
                        '       </div>' +
                        '       <div class="content" id="' + l.F_Id + '"  data-type="' + l.F_Type + '"></div>' +
                        '   </div>';
                    mkaddchart.before(j);
                    b[l.F_Id] = echarts.init(document.getElementById(l.F_Id));
                    top.Changjie.httpAsync("GET",
                        top.$.rootUrl + "/Desktop/DTChart/GetSqlData",
                        {
                            Id: l.F_Id
                        },
                        function (m) {
                            if (m) {
                                var p = $("#" + m.Id).attr("data-type");
                                var n = [];
                                var q = [];
                                $.each(m.value,
                                    function (r, s) {
                                        n.push(s.name);
                                        q.push(s.value);
                                    });
                                var o = {};
                                switch (p) {
                                    case "0":
                                        o.tooltip = {
                                            trigger: "item",
                                            formatter: "{a} <br/>{b} : {c} ({d}%)"
                                        };
                                        o.legend = {
                                            bottom: "bottom",
                                            data: n
                                        };
                                        o.series = [
                                            {
                                                name: "占比",
                                                type: "pie",
                                                radius: "75%",
                                                center: ["50%", "50%"],
                                                label: {
                                                    normal: {
                                                        formatter: "{b}:{c}: ({d}%)",
                                                        textStyle: {
                                                            fontWeight: "normal",
                                                            fontSize: 12,
                                                            color: "#000"
                                                        }
                                                    }
                                                },
                                                data: m.value,
                                                itemStyle: {
                                                    emphasis: {
                                                        shadowBlur: 10,
                                                        shadowOffsetX: 0,
                                                        shadowColor: "rgba(0, 0, 0, 0.5)"
                                                    }
                                                }
                                            }
                                        ];
                                        o.color = [
                                            "#df4d4b", "#304552", "#52bbc8", "rgb(224,134,105)", "#8dd5b4", "#5eb57d",
                                            "#d78d2f"
                                        ];
                                        break;
                                    case "1":
                                        o.tooltip = {
                                            trigger: "axis"
                                        };
                                        o.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        o.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: n
                                        };
                                        o.yAxis = {
                                            type: "value"
                                        };
                                        o.series = [
                                            {
                                                type: "line",
                                                data: q
                                            }
                                        ];
                                        break;
                                    case "2":
                                        o.tooltip = {
                                            trigger: "axis"
                                        };
                                        o.grid = {
                                            bottom: "8%",
                                            containLabel: true
                                        };
                                        o.xAxis = {
                                            type: "category",
                                            boundaryGap: false,
                                            data: n
                                        };
                                        o.yAxis = {
                                            type: "value"
                                        };
                                        o.series = [
                                            {
                                                type: "bar",
                                                data: q
                                            }
                                        ];
                                        break;
                                }
                                b[m.Id].setOption(o);
                            }
                        });
                });
            window.onresize = function (j) {
                $.each(b,
                    function (k, l) {
                        l.resize(j);
                    });
            };
        },
        bind: function () {
            $("#target_content").on("click", ".tool>div",
                function () {
                    var h = $(this);
                    var k = h.text();
                    var i = h.parent().attr("data-Id");
                    var j = g[i];
                    selectedRow = j;
                    switchbtn = 1;
                    if (k === "编辑") {
                        Changjie.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        Changjie.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    Changjie.deleteForm(top.$.rootUrl + "/Desktop/DTTarget/DeleteForm",
                                        {
                                            keyValue: j.F_Id
                                        },
                                        function () {
                                            refreshGirdData();
                                        });
                                }
                            });
                    }
                });
            $("#mk-add-target").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 1;
                    Changjie.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            $(".mk-desktop-panel").on("click", ".listItem .tool>div",
                function () {
                    var h = $(this);
                    var k = h.text();
                    var i = h.parents(".listItem").attr("data-Id");
                    var j = mkListHtml[i];
                    selectedRow = j;
                    switchbtn = 2;
                    if (k === "编辑") {
                        Changjie.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTList/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        Changjie.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    Changjie.deleteForm(top.$.rootUrl + "/Desktop/DTList/DeleteForm",
                                        {
                                            keyValue: j.F_Id
                                        },
                                        function () {
                                            refreshGirdData();
                                        });
                                }
                            });
                    }
                });
            $("#mk-add-list").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 2;
                    Changjie.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTList/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            $(".mk-desktop-panel").on("click", ".chartItem .tool>div",
                function () {
                    var h = $(this);
                    var k = h.text();
                    var i = h.parents(".chartItem").attr("data-Id");
                    var j = c[i];
                    selectedRow = j;
                    switchbtn = 3;
                    if (k === "编辑") {
                        Changjie.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTChart/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        Changjie.layerConfirm("是否确认删除该项！",
                            function (l) {
                                if (l) {
                                    Changjie.deleteForm(top.$.rootUrl + "/Desktop/DTChart/DeleteForm",
                                        {
                                            keyValue: j.F_Id
                                        },
                                        function () {
                                            refreshGirdData();
                                        });
                                }
                            });
                    }
                });
            $("#mk-add-chart").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 3;
                    Changjie.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTChart/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    });
                });
        }
    };
    refreshGirdData = function () {
        switch (switchbtn) {
            case 1:
                Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                    function (h) {
                        $(".targetItem").remove();
                        page.target(h || []);
                    });
                break;
            case 2:
                Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                    function (h) {
                        $(".listItem").remove();
                        page.list(h || []);
                    });
                break;
            case 3:
                Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetPageList", {},
                    function (h) {
                        $(".chartItem").remove();
                        page.chart(h || []);
                    });
                break;
        };
    };
    page.init();
};