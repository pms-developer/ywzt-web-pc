﻿var refreshGirdData;
var selectedRow = null;
var switchbtn = 1;
var bootstrap = function (a, b) {

    var special = ["北京", "天津", "上海", "重庆", "香港", "澳门"];
    var defaults = {
        geoCoordMap: {
            '上海': [121.4648, 31.2891],
            '东莞': [113.8953, 22.901],
            '东营': [118.7073, 37.5513],
            '中山': [113.4229, 22.478],
            '临汾': [111.4783, 36.1615],
            '临沂': [118.3118, 35.2936],
            '丹东': [124.541, 40.4242],
            '丽水': [119.5642, 28.1854],
            '乌鲁木齐': [87.9236, 43.5883],
            '佛山': [112.8955, 23.1097],
            '保定': [115.0488, 39.0948],
            '兰州': [103.5901, 36.3043],
            '甘肃': [103.5901, 36.3043],

            '包头': [110.3467, 41.4899],
            '北京': [116.4551, 40.2539],
            '北海': [109.314, 21.6211],
            '南京': [118.8062, 31.9208],
            '江苏': [118.8062, 31.9208],
            '南宁': [108.479, 23.1152],
            '广西': [108.479, 23.1152],

            '南昌': [116.0046, 28.6633],
            '江西': [116.0046, 28.6633],

            '南通': [121.1023, 32.1625],
            '厦门': [118.1689, 24.6478],
            '台州': [121.1353, 28.6688],

            '合肥': [117.29, 32.0581],
            '安徽': [117.29, 32.0581],

            '呼和浩特': [111.4124, 40.4901],
            '咸阳': [108.4131, 34.8706],
            '哈尔滨': [127.9688, 45.368],
            '黑龙江': [127.9688, 45.368],
            '唐山': [118.4766, 39.6826],
            '嘉兴': [120.9155, 30.6354],
            '大同': [113.7854, 39.8035],
            '大连': [122.2229, 39.4409],
            '天津': [117.4219, 39.4189],
            '太原': [112.3352, 37.9413],
            '山西': [112.3352, 37.9413],
            '威海': [121.9482, 37.1393],
            '宁波': [121.5967, 29.6466],
            '宝鸡': [107.1826, 34.3433],
            '宿迁': [118.5535, 33.7775],
            '常州': [119.4543, 31.5582],
            '广州': [113.5107, 23.2196],
            '广东': [113.5107, 23.2196],

            '廊坊': [116.521, 39.0509],
            '延安': [109.1052, 36.4252],
            '张家口': [115.1477, 40.8527],
            '徐州': [117.5208, 34.3268],
            '德州': [116.6858, 37.2107],
            '惠州': [114.6204, 23.1647],
            '成都': [103.9526, 30.7617],
            '四川': [103.9526, 30.7617],

            '扬州': [119.4653, 32.8162],
            '承德': [117.5757, 41.4075],
            '拉萨': [91.1865, 30.1465],
            '无锡': [120.3442, 31.5527],
            '日照': [119.2786, 35.5023],
            '昆明': [102.9199, 25.4663],
            '云南': [102.9199, 25.4663],

            '杭州': [119.5313, 29.8773],
            '浙江': [119.5313, 29.8773],

            '枣庄': [117.323, 34.8926],
            '柳州': [109.3799, 24.9774],
            '株洲': [113.5327, 27.0319],
            '武汉': [114.3896, 30.6628],
            '湖北': [114.3896, 30.6628],

            '汕头': [117.1692, 23.3405],
            '江门': [112.6318, 22.1484],
            '沈阳': [123.1238, 42.1216],
            '辽宁': [123.1238, 42.1216],
            '沧州': [116.8286, 38.2104],
            '河源': [114.917, 23.9722],
            '泉州': [118.3228, 25.1147],
            '泰安': [117.0264, 36.0516],
            '泰州': [120.0586, 32.5525],
            '济南': [117.1582, 36.8701],
            '山东': [117.1582, 36.8701],

            '济宁': [116.8286, 35.3375],
            '海口': [110.3893, 19.8516],
            '海南': [110.3893, 19.8516],

            '淄博': [118.0371, 36.6064],
            '淮安': [118.927, 33.4039],
            '深圳': [114.5435, 22.5439],
            '清远': [112.9175, 24.3292],
            '温州': [120.498, 27.8119],
            '渭南': [109.7864, 35.0299],
            '湖州': [119.8608, 30.7782],
            '湘潭': [112.5439, 27.7075],
            '滨州': [117.8174, 37.4963],
            '潍坊': [119.0918, 36.524],
            '烟台': [120.7397, 37.5128],
            '玉溪': [101.9312, 23.8898],
            '珠海': [113.7305, 22.1155],
            '盐城': [120.2234, 33.5577],
            '盘锦': [121.9482, 41.0449],
            '石家庄': [114.4995, 38.1006],
            '河北': [114.4995, 38.1006],
            '福州': [119.4543, 25.9222],
            '福建': [119.4543, 25.9222],

            '秦皇岛': [119.2126, 40.0232],
            '绍兴': [120.564, 29.7565],
            '聊城': [115.9167, 36.4032],
            '肇庆': [112.1265, 23.5822],
            '舟山': [122.2559, 30.2234],
            '苏州': [120.6519, 31.3989],
            '莱芜': [117.6526, 36.2714],
            '菏泽': [115.6201, 35.2057],
            '营口': [122.4316, 40.4297],
            '葫芦岛': [120.1575, 40.578],
            '衡水': [115.8838, 37.7161],
            '衢州': [118.6853, 28.8666],
            '西宁': [101.4038, 36.8207],
            '青海': [101.4038, 36.8207],

            '西安': [109.1162, 34.2004],
            '陕西': [109.1162, 34.2004],

            '贵阳': [106.6992, 26.7682],
            '贵州': [106.6992, 26.7682],

            '连云港': [119.1248, 34.552],
            '邢台': [114.8071, 37.2821],
            '邯郸': [114.4775, 36.535],
            '郑州': [113.4668, 34.6234],
            '河南': [113.4668, 34.6234],

            '鄂尔多斯': [108.9734, 39.2487],
            '重庆': [107.7539, 30.1904],
            '金华': [120.0037, 29.1028],
            '铜川': [109.0393, 35.1947],
            '银川': [106.3586, 38.1775],
            '宁夏': [106.3586, 38.1775],

            '镇江': [119.4763, 31.9702],
            '长春': [125.8154, 44.2584],
            '吉林': [125.8154, 44.2584],
            '长沙': [113.0823, 28.2568],
            '湖南': [113.0823, 28.2568],

            '长治': [112.8625, 36.4746],
            '阳泉': [113.4778, 38.0951],
            '青岛': [120.4651, 36.3373],
            '韶关': [113.7964, 24.7028]
        },

        provinces: {
            //23个省
            "台湾": "taiwan",
            "河北": "hebei",
            "山西": "shanxi",
            "辽宁": "liaoning",
            "吉林": "jilin",
            "黑龙江": "heilongjiang",
            "江苏": "jiangsu",
            "浙江": "zhejiang",
            "安徽": "anhui",
            "福建": "fujian",
            "江西": "jiangxi",
            "山东": "shandong",
            "河南": "henan",
            "湖北": "hubei",
            "湖南": "hunan",
            "广东": "guangdong",
            "海南": "hainan",
            "四川": "sichuan",
            "贵州": "guizhou",
            "云南": "yunnan",
            "陕西": "shanxi1",
            "甘肃": "gansu",
            "青海": "qinghai",
            //5个自治区
            "新疆": "xinjiang",
            "广西": "guangxi",
            "内蒙古": "neimenggu",
            "宁夏": "ningxia",
            "西藏": "xizang",
            //4个直辖市
            "北京": "beijing",
            "天津": "tianjin",
            "上海": "shanghai",
            "重庆": "chongqing",
            //2个特别行政区
            "香港": "xianggang",
            "澳门": "aomen"
        },
        convertData: function (data) {
            var res = [];
            for (var i = 0; i < data.length; i++) {
                var geoCoord = $.geoCoordMap[data[i].name];
                if (geoCoord) {
                    res.push({
                        name: data[i].name,
                        value: geoCoord.concat(data[i].value)
                    });
                }
            }



            return res;
        },
        convertDataQX: function (data) {
            //var res = [];
            //for (var i = 0; i < data.length; i++) {
            //    var geoCoord = $.geoCoordMap[data[i].name];
            //    if (geoCoord) {
            //        res.push({
            //            name: data[i].name,
            //            value: geoCoord.concat(data[i].value)
            //        });
            //    }
            //}


            var res = [];
            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                var fromCoord = $.geoCoordMap[dataItem[0].name];
                var toCoord = $.geoCoordMap[dataItem[1].name];
                if (fromCoord && toCoord) {
                    res.push({
                        fromName: dataItem[0].name,
                        toName: dataItem[1].name,
                        coords: [fromCoord, toCoord]
                    });
                }
            }
            return res;
        },
        option: {}
    };

    $.extend(defaults);
    function FlowData(dataT) {

        var biCategory = MKFormateGroupData(dataT, "map").category;
        var biSeries = MKFormateGroupData(dataT, "map").series;
        var TopData = [];
        for (var topi in biSeries) {
            var myobject = [];
            myobject[0] = biSeries[topi].name //最外层
            var BJData = [];
            for (var si in biSeries[topi].value) {
                var myboject2 = [];//内层数组
                myboject2[0] = { name: myobject[0] };
                myboject2[1] = { name: biSeries[topi].value[si].name, value: biSeries[topi].value[si].value };
                BJData.push(myboject2);
            }
            myobject[1] = BJData;
            TopData.push(myobject);
        }

        return TopData;
    }

    function MKFormateGroupData(data, type) {

        /*
         *data的格式如上的Result2，type为要渲染的图表类型：可以为line，bar，is_stack表示为是否是堆积图，
         *这种格式的数据多用于展示多条折线图、分组的柱图
         *
         *
         *
         */

        var chart_type = 'line';
        if (type)
            chart_type = type || 'line';
        var xAxis = [];
        var group = [];
        var series = [];
        for (var i = 0; i < data.length; i++) {
            for (var j = 0; j < xAxis.length && xAxis[j] != data[i].name; j++);
            if (j == xAxis.length)
                xAxis.push(data[i].name);
            for (var k = 0; k < group.length && group[k] != data[i].group; k++);
            if (k == group.length)
                group.push(data[i].group);
        }
        for (var i = 0; i < group.length; i++) {
            var temp = [];
            if (type != "warn") {
                for (var j = 0; j < data.length; j++) {
                    if (group[i] == data[j].group) {
                        if (type == "map") {
                            temp.push({ name: data[j].name, value: data[i].value });
                        } else {
                            temp.push(data[j].value);
                        }
                    }
                }
            } else {
                for (var m = 0; m < data.length; m++) {
                    if (group[i] == data[m].group) {
                        if (type == "warn") {
                            temp.push({ name: data[m].name, value: data[i].group });
                        } else {
                            temp.push(data[m].value);
                        }
                    }
                }
            }

            switch (type) {
                case 'bar':
                    var series_temp = { name: group[i], value: temp, type: chart_type };

                    break;
                case 'map':
                    var series_temp = {
                        name: group[i], value: temp, type: chart_type
                    };
                    break;
                case 'line':
                    var series_temp = { name: group[i], value: temp, type: chart_type };

                    break;
                case 'flow':
                    var series_temp = { name: group[i], value: temp, type: chart_type };

                    break;
                case 'warn':
                    var series_temp = { name: group[i], value: temp, type: chart_type };

                    break;
                default:
                    var series_temp = { name: group[i], value: temp, type: chart_type };
            }
            series.push(series_temp);
        }
        return { category: group, xAxis: xAxis, series: series };
    }


    var c = {
        init: function () {
            a(".content").mkscroll();
            b.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                function (d) {
                    c.target(d || [])
                });
            b.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                function (d) {
                    c.list(d || [])
                });
            b.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetPageList", {},
                function (d) {
                    c.chart(d || [])
                });
            c.bind()
        },
        target: function (e) {
            var d = a("#mk-add-target");
            a.each(e,
                function (g, h) {
                    var f = a('                    <div class="targetItem targetItem2">                        <div class="name">' + h.F_Name + '</div>                        <div class="number" data-number="' + h.F_Id + '"></div>                        <div class="tool">                            <div>编辑</div>                            <div>删除</div>                        </div>                    </div>');
                    f[0].item = h;
                    d.before(f);
                    top.Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetSqlData", {
                        Id: h.F_Id
                    },
                        function (i) {
                            if (i) {
                                a('[data-number="' + i.Id + '"]').text(i.value)
                            }
                        })
                })
        },
        list: function (e) {
            var d = a("#mk-add-list");
            a.each(e,
                function (g, h) {
                    var f = a('                <div class="mk-black-panel mk-black-panel-list">                    <div class="mk-title">' + h.F_Name + ' <div class="tool"><div>编辑</div><div>删除</div></div></div>                    <div class="mk-content" data-desktop="' + h.F_Id + '" ></div>                </div>');
                    f[0].item = h;
                    d.before(f);
                    top.Changjie.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetSqlData", {
                        Id: h.F_Id
                    },
                        function (j) {
                            if (j) {
                                var i = a('[data-desktop="' + j.Id + '"]');
                                a.each(j.value,
                                    function (l, m) {
                                        var k = '                                <div class="mk-list-item mk-dtlist-item">                                    <div class="mk-ellipsis">' + m.f_title + '</div>                                    <div class="date">' + b.formatDate(m.f_time, "yyyy-MM-dd") + "</div>                                </div>";
                                        i.append(k)
                                    });
                                i = null
                            }
                        })
                })
        },
        chart: function (f) {
            var d = a("#mk-add-chart");
            var chart = {};
            a.each(f,
                function (h, i) {
                    var g = a('                <div class="mk-black-panel mk-black-panel-chart">                    <div class="mk-title">' + i.F_Name + '<div class="tool"><div>编辑</div><div>删除</div></div></div>                    <div class="mk-content mk-chart-content">                        <div class="mk-chart-container" id="' + i.F_Id + '"  data-desktop="' + i.F_Type + '" ></div>                    </div>                </div>');
                    g[0].item = i;
                    d.before(g);
                    chart[i.F_Id] = echarts.init(document.getElementById(i.F_Id));
                    b.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetSqlData", {
                        Id: i.F_Id
                    },
                        function (data) {
                            if (data) {
                                var m = a("#" + data.Id).attr("data-desktop");
                                var k = [];
                                var n = [];
                                a.each(data.value,
                                    function (o, p) {
                                        k.push(p.name);
                                        n.push(p.value)
                                    });
                                var option = {};
                                switch (m) {
                                    case "0":
                                        option.tooltip = {
                                            trigger: "item",
                                            formatter: "{a} <br/>{b}: {c} ({d}%)"
                                        };
                                        option.legend = {
                                            orient: "vertical",
                                            left: "left",
                                            data: k
                                        };
                                        option.series = [{
                                            name: "占比",
                                            type: "pie",
                                            radius: ["50%", "70%"],
                                            avoidLabelOverlap: false,
                                            label: {
                                                normal: {
                                                    show: false,
                                                    position: "center"
                                                },
                                                emphasis: {
                                                    show: true,
                                                    textStyle: {
                                                        fontSize: "30",
                                                        fontWeight: "bold"
                                                    }
                                                }
                                            },
                                            labelLine: {
                                                normal: {
                                                    show: false
                                                }
                                            },
                                            data: data.value
                                        }];
                                        option.color = ["#df4d4b", "#304552", "#52bbc8", "rgb(224,134,105)", "#8dd5b4", "#5eb57d", "#d78d2f"];
                                        chart[data.Id].setOption(option);
                                        break;
                                    case "1":
                                    case "2":
                                        option = {
                                            grid: {
                                                top: "20px",
                                                bottom: "10px",
                                                left: "15px",
                                                right: "15px",
                                                containLabel: true
                                            },
                                            xAxis: {
                                                type: "category",
                                                data: k
                                            },
                                            yAxis: {
                                                type: "value"
                                            },
                                            series: [{
                                                data: n,
                                                type: m === "1" ? "line" : "bar"
                                            }]
                                        };
                                        chart[data.Id].setOption(option);
                                        break;
                                    case "3":
                                        option.title = {
                                            text: '',
                                            subtext: '',
                                            link: '',
                                            left: 'center',
                                            textStyle: {
                                                color: '#fff',
                                                fontSize: 16,
                                                fontWeight: 'normal',
                                                fontFamily: "Microsoft YaHei"
                                            },
                                            subtextStyle: {
                                                color: '#ccc',
                                                fontSize: 13,
                                                fontWeight: 'normal',
                                                fontFamily: "Microsoft YaHei"
                                            }
                                        };
                                        option.tooltip = {
                                            trigger: 'item',
                                            formatter: '{b}'
                                        };
                                        option.toolbox = {
                                            show: true,
                                            orient: 'vertical',
                                            left: 'right',
                                            top: 'center',
                                            feature: {
                                                dataView: { readOnly: false },
                                                restore: {},
                                                saveAsImage: {}
                                            },
                                            iconStyle: {
                                                normal: {
                                                    color: '#fff'
                                                }
                                            }
                                        };
                                        option.animationDuration = 1000;
                                        option.animationEasing = 'cubicOut';
                                        option.animationDurationUpdate = 1000;
                                        option.bmap = {
                                            center: [104.114129, 37.550339],
                                            zoom: 5,
                                            roam: true,
                                            mapStyle: {
                                                styleJson: [
                                                    {
                                                        "featureType": "water",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "color": "#044161"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "land",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "color": "#004981"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "boundary",
                                                        "elementType": "geometry",
                                                        "stylers": {
                                                            "color": "#064f85"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "railway",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "highway",
                                                        "elementType": "geometry",
                                                        "stylers": {
                                                            "color": "#004981"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "highway",
                                                        "elementType": "geometry.fill",
                                                        "stylers": {
                                                            "color": "#005b96",
                                                            "lightness": 1
                                                        }
                                                    },
                                                    {
                                                        "featureType": "highway",
                                                        "elementType": "labels",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "arterial",
                                                        "elementType": "geometry",
                                                        "stylers": {
                                                            "color": "#004981"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "arterial",
                                                        "elementType": "geometry.fill",
                                                        "stylers": {
                                                            "color": "#00508b"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "poi",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "green",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "color": "#056197",
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "subway",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "manmade",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "local",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "arterial",
                                                        "elementType": "labels",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "boundary",
                                                        "elementType": "geometry.fill",
                                                        "stylers": {
                                                            "color": "#029fd4"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "building",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "color": "#1a5787"
                                                        }
                                                    },
                                                    {
                                                        "featureType": "label",
                                                        "elementType": "all",
                                                        "stylers": {
                                                            "visibility": "off"
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                        chart[data.Id].setOption(option);
                                        break;
                                    case "4":
                                        var planePath = 'path:// M878.229514 645.809634c-40.072094-279.906746-298.66716-645.809634-375.45455-645.809634-46.917869 0-327.58588 367.1461-375.462032 645.809634-46.003852 267.897962 168.10054 378.190366 375.462032 378.190366S917.497324 920.091997 878.229514 645.809634zM683.499037 658.740541 572.745883 658.740541l0 110.774975-139.920641 0L432.825243 658.740541 322.072089 658.740541 322.072089 518.841722l110.753154 0L432.825243 408.074228l139.920641 0 0 110.767494L683.499037 518.841722 683.499037 658.740541z';
                                        var color = ['#a6c84c', '#ffa022', '#46bee9'];
                                        var series = [];
                                        FlowData(data.value).forEach(function (item, i) {
                                            series.push({
                                                name: item[0],
                                                type: 'lines',
                                                zlevel: 1,
                                                effect: {
                                                    show: true,
                                                    period: 6,
                                                    trailLength: 0.7,
                                                    color: '#fff',
                                                    symbolSize: 3
                                                },
                                                lineStyle: {
                                                    normal: {
                                                        color: color[i],
                                                        width: 0,
                                                        curveness: 0.2
                                                    }
                                                },
                                                data: $.convertDataQX(item[1])
                                            },
                                                {
                                                    name: item[0],
                                                    type: 'lines',
                                                    zlevel: 2,
                                                    effect: {
                                                        show: true,
                                                        period: 6,
                                                        trailLength: 0,
                                                        symbol: planePath,
                                                        symbolSize: 10
                                                    },
                                                    lineStyle: {
                                                        normal: {
                                                            color: color[i],
                                                            width: 1,
                                                            opacity: 0.4,
                                                            curveness: 0.2
                                                        }
                                                    },
                                                    data: $.convertDataQX(item[1])
                                                },
                                                {
                                                    name: item[0],
                                                    type: 'effectScatter',
                                                    coordinateSystem: 'geo',
                                                    zlevel: 2,
                                                    rippleEffect: {
                                                        brushType: 'stroke'
                                                    },
                                                    label: {
                                                        normal: {
                                                            show: true,
                                                            position: 'right',
                                                            formatter: '{b}'
                                                        }
                                                    },
                                                    symbolSize: function (val) {
                                                        return val[2] / 2000;
                                                    },
                                                    itemStyle: {
                                                        normal: {
                                                            color: color[i]
                                                        }
                                                    },
                                                    data: item[1].map(function (dataItem) {
                                                        var myname = dataItem[1].name;
                                                        var myvalue = dataItem[1].value;

                                                        return {
                                                            name: myname,
                                                            value: $.geoCoordMap[myname].concat([dataItem[1].value])
                                                        };
                                                    })
                                                });
                                        });
                                        option = {
                                            //        backgroundColor: '#404a59',
                                            title: {

                                                left: 'center',
                                                textStyle: {
                                                    color: '#fff'
                                                }
                                            },
                                            tooltip: {
                                                trigger: 'item'
                                            },

                                            geo: {
                                                map: 'china',
                                                label: {
                                                    emphasis: {
                                                        show: false
                                                    }
                                                },
                                                roam: true,
                                                itemStyle: {
                                                    normal: {
                                                        areaColor: '#353761',
                                                        borderColor: '#a38a8a'
                                                    },
                                                    emphasis: {
                                                        areaColor: '#2a333d'
                                                    }
                                                }
                                            },
                                            series: series
                                        };
                                        chart[data.Id].setOption(option);
                                        break;
                                       
                                }
                               
                            }
                        })
                })
        },
        bind: function () {
            a(".mk-flex-content").on("click", ".tool>div",
                function () {
                    var d = a(this);
                    var f = d.text();
                    var e = d.parents(".targetItem2")[0].item;
                    selectedRow = e;
                    switchbtn = 1;
                    if (f === "编辑") {
                        b.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        b.layerConfirm("是否确认删除该项！",
                            function (g) {
                                if (g) {
                                    b.deleteForm(top.$.rootUrl + "/Desktop/DTTarget/DeleteForm", {
                                        keyValue: e.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            a("#mk-add-target").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 1;
                    b.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTTarget/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            a(".content").on("click", ".mk-black-panel-list .tool>div",
                function () {
                    var d = a(this);
                    var f = d.text();
                    var e = d.parents(".mk-black-panel-list")[0].item;
                    selectedRow = e;
                    switchbtn = 2;
                    if (f === "编辑") {
                        b.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTList/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        b.layerConfirm("是否确认删除该项！",
                            function (g) {
                                if (g) {
                                    b.deleteForm(top.$.rootUrl + "/Desktop/DTList/DeleteForm", {
                                        keyValue: e.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            a("#mk-add-list").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 2;
                    b.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTList/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                });
            a(".content").on("click", ".mk-black-panel-chart .tool>div",
                function () {
                    var d = a(this);
                    var f = d.text();
                    var e = d.parents(".mk-black-panel-chart")[0].item;
                    selectedRow = e;
                    switchbtn = 3;
                    if (f === "编辑") {
                        b.layerForm({
                            id: "Form",
                            title: "编辑",
                            url: top.$.rootUrl + "/Desktop/DTChart/Form",
                            width: 600,
                            height: 500,
                            btn: null
                        })
                    } else {
                        b.layerConfirm("是否确认删除该项！",
                            function (g) {
                                if (g) {
                                    b.deleteForm(top.$.rootUrl + "/Desktop/DTChart/DeleteForm", {
                                        keyValue: e.F_Id
                                    },
                                        function () {
                                            refreshGirdData()
                                        })
                                }
                            })
                    }
                });
            a("#mk-add-chart").on("click",
                function () {
                    selectedRow = null;
                    switchbtn = 3;
                    b.layerForm({
                        id: "Form",
                        title: "添加",
                        url: top.$.rootUrl + "/Desktop/DTChart/Form",
                        width: 600,
                        height: 500,
                        btn: null
                    })
                })
        }
    };
    refreshGirdData = function () {
        switch (switchbtn) {
            case 1:
                b.httpAsync("GET", top.$.rootUrl + "/Desktop/DTTarget/GetPageList", {},
                    function (d) {
                        a(".mk-flex-content").find(".targetItem2").remove();
                        c.target(d || [])
                    });
                break;
            case 2:
                b.httpAsync("GET", top.$.rootUrl + "/Desktop/DTList/GetPageList", {},
                    function (d) {
                        a(".mk-black-panel-list").remove();
                        c.list(d || [])
                    });
                break;
            case 3:
                b.httpAsync("GET", top.$.rootUrl + "/Desktop/DTChart/GetPageList", {},
                    function (d) {
                        a(".mk-black-panel-chart").remove();
                        c.chart(d || [])
                    });
                break
        }
    };
    c.init()
};