﻿var keyValue = "";
var bootstrap = function ($, Changjie) {
    var d = Changjie.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $("#wizard").wizard().on("change",
                function (i, h) {
                    var btnFinish = $("#btn_finish");
                    var btnNext = $("#btn_next");
                    if (h.direction == "next") {
                        if (h.step == 1) {
                            if (!$("#step-1").mkValidform()) {
                                return false;
                            }
                            btnFinish.removeAttr("disabled");
                            btnNext.attr("disabled", "disabled");
                        }
                    } else {
                        btnFinish.attr("disabled", "disabled");
                        btnNext.removeAttr("disabled");
                    }
                });
            $("#selectIcon").on("click",
                function () {
                    Changjie.layerForm({
                        id: "iconForm",
                        title: "选择图标",
                        url: top.$.rootUrl + "/Utility/Icon",
                        height: 700,
                        width: 1000,
                        btn: null,
                        maxmin: true,
                        end: function() {
                            if (top._changjieSelectIcon != "") {
                                $("#F_Icon").val(top._changjieSelectIcon);
                            }
                        }
                    });
                });
            $("#F_DataSourceId").mkselect({
                url: top.$.rootUrl + "/SystemModule/DatabaseLink/GetTreeList",
                type: "tree",
                placeholder: "请选择数据库"
            });
            $("#F_Proportion1").mkselect({
                placeholder: false,
                data: [{
                    id: "1",
                    text: "1/1"
                },
                {
                    id: "2",
                    text: "1/2"
                }]
            }).mkselectSet("1");

            var dfop22 = {
                type: 'tree',
                // 展开最大高度
                maxHeight: 200,
                // 是否允许搜索
                allowSearch: true,
                // 访问数据接口地址
                url: top.$.rootUrl + '/Desktop/DTChart/GetTree',
                // 访问数据接口参数
                param: { parentId: '0' },
            }
            $('#F_ParentId').mkselect(dfop22);


            $("#F_Proportion2").mkselect({
                placeholder: false,
                data: [{
                    id: "1",
                    text: "1/1"
                },
                {
                    id: "2",
                    text: "1/2"
                }]
            }).mkselectSet("1");
            $("#F_Proportion3").mkselect({
                placeholder: false,
                data: [{
                    id: "1",
                    text: "1/1"
                },
                {
                    id: "2",
                    text: "1/2"
                }]
            }).mkselectSet("1");
            $("#F_Proportion4").mkselect({
                placeholder: false,
                data: [{
                    id: "1",
                    text: "1/1"
                },
                {
                    id: "2",
                    text: "1/2"
                }]
            }).mkselectSet("1");
            //图表类型0饼图1折线图2柱状图
            $("#F_Type").mkselect({
                placeholder: false,
                data: [
                    {
                        id: "0",
                        text: "饼图"
                    },
                    {
                        id: "1",
                        text: "折线图"
                    }, {
                        id: "2",
                        text: "柱状图"
                    }, {
                        id: "3",
                        text: "地图"
                    }, {
                        id: "4",
                        text: "地图-迁徙"
                    }
                ]
            }).mkselectSet("1");
            $("#btn_finish").click(function() {
                if (!$("#wizard-steps").mkValidform()) {
                    return false;
                }
                var e = $("#wizard-steps").mkGetFormData(keyValue);
                $.mkSaveForm(top.$.rootUrl + "/Desktop/DTChart/SaveForm?keyValue=" + keyValue,
                    e,
                    function(f) {
                        Changjie.frameTab.currentIframe().refreshGirdData();
                    });
            });
        },
        initData: function () {
            if (!!d) {
                keyValue = d.F_Id;
                $("#wizard-steps").mkSetFormData(d);
            }
        }
    };
    page.init();
};