﻿using System.Web.Mvc;
namespace HuNanChangJie.Application.Web.Areas.MeioProducts
{
    public class MeioProductsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MeioProducts"; 
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MeioProducts_default", 
                "MeioProducts/{controller}/{action}/{id}", 
                             new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
