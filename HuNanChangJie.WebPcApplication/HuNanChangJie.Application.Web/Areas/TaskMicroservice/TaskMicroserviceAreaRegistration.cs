﻿using System.Web.Mvc;
namespace HuNanChangJie.Application.Web.Areas.Microservice
{
    public class MicroserviceAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TaskMicroservice"; 
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "TaskMicroservice_default",
                "TaskMicroservice/{controller}/{action}/{id}", 
                             new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
