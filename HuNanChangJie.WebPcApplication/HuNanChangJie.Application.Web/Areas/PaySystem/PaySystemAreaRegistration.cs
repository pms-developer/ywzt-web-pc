﻿using System.Web.Mvc;

namespace HuNanChangJie.Application.Web.Areas.PaySystem
{
    public class PaySystemAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PaySystem";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PaySystem_default",
                "PaySystem/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}