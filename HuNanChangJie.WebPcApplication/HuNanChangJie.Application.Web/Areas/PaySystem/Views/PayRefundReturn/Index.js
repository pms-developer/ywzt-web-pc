﻿/*
 * 创建人：超级管理员
 * 日  期：2019-04-17 11:25
 * 描  述：退款管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/PaySystem/PayRefundReturn/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_RefundId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/PaySystem/PayRefundReturn/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_RefundId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/PaySystem/PayRefundReturn/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PaySystem/PayRefundReturn/GetPageList',
                headData: [

                    { label: "订单编号", name: "F_OrderSN", width: 180, align: "left" },
                    { label: "退款编号", name: "F_RefundSN", width: 120, align: "left" },

                    {
                        label: "状态", name: "F_RefundState", width: 100, align: "center",
                        formatter: function (cellvalue, row) {
                            var elab = "";
                             
                            if (row.F_AuditorStatus == 0) {
                                elab =
                                    '<span class="tag warn"><span class="minus" ></span ><span class="rectangle">待审</span><span class="plus"></span></span >';
                            } else {
                                if (row.F_AuditorStatus == 1) {
                                    elab =
                                        '<span class="tag success"><span class="minus" ></span ><span class="rectangle">驳回</span><span class="plus"></span></span >';
                                }
                                if (row.F_AuditorStatus == 2) {
                                    elab =
                                        '<span class="tag success"><span class="minus" ></span ><span class="rectangle">同意</span><span class="plus"></span></span >';
                                }
                            }
                            if (cellvalue == 1) {
                                elab +=
                                    '<span class="tag minus"><span class="minus" ></span ><span class="rectangle">正常</span><span class="plus"></span></span >';
                            } else {
                                if (cellvalue == 2) {
                                    elab +=
                                        '<span class="tag error"><span class="minus" ></span ><span class="rectangle">作废</span><span class="plus"></span></span >';
                                }

                                if (cellvalue == 3) {
                                    elab +=
                                        '<span class="tag error"><span class="minus" ></span ><span class="rectangle">已退</span><span class="plus"></span></span >';
                                }

                            }

                            return elab;

                        }
                    },
                    { label: "商品名称", name: "F_GoodsName", width: 200, align: "left" },
                    {
                        label: "退款金额", name: "F_RefundAmount", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            return Changjie.numberFormat(cellvalue, Changjie.sysGlobalSettings.pointGlobal, ".", ",") + " 元";
                        }
                    },

                    { label: "管理员操作日期", name: "F_AdminTime", width: 120, align: "left" },
                    { label: "申请原因", name: "F_BuyerMessage", width: 200, align: "left" },
                    { label: "管理员备注", name: "F_AdminMessage", width: 300, align: "left" },

                ],
                mainId: 'F_RefundId',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
