﻿/*
 * 创建人：超级管理员
 * 日  期：2019-04-11 10:07
 * 描  述：发票
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, Changjie) {
    "use strict";
    var page = {
        init: function () {
            $('.mk-form-wrap').mkscroll();
            page.bind();
            page.initData();
        },
        bind: function () {
        },
        initData: function () {
            if (!!keyValue) {
                $.mkSetForm(top.$.rootUrl + '/PaySystem/OrderInvoice/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                            $('#' + id ).jfGridSet('refreshdata', data[id]);
                        }
                        else {
                            $('[data-table="' + id + '"]').mkSetFormData(data[id]);
                        }
                    }
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').mkValidform()) {
            return false;
        }
        var postData = {};
        postData.strtPay_Order_InvoiceEntity = JSON.stringify($('[data-table="tPay_Order_Invoice"]').mkGetFormData());
        postData.strEntity = JSON.stringify($('[data-table="tPay_Order"]').mkGetFormData());
        $.mkSaveForm(top.$.rootUrl + '/PaySystem/OrderInvoice/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
