﻿/*
 * 创建人：超级管理员
 * 日  期：2019-04-11 10:07
 * 描  述：发票
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/PaySystem/OrderInvoice/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_InvId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/PaySystem/OrderInvoice/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_InvId');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/PaySystem/OrderInvoice/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });


            // 导出
            $('#outport').on('click', function () {
                Changjie.layerForm({
                    id: "ExcelExportForm",
                    title: '导出Excel数据',
                    url: top.$.rootUrl + '/Utility/ExcelExportForm?gridId=gridtable&filename=invoice',
                    width: 500,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick();
                    },
                    btn: ['导出Excel', '关闭']
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PaySystem/OrderInvoice/GetPageList',
                headData: [
                    { label: "订单编号", name: "F_OrderSN", width: 160, align: "left" },
                    {
                        label: "是否开票", name: "F_IsOutInvoice", width: 80, align: "center",
                        formatter: function (cellvalue, row, dfop, $dcell) {
                            $dcell.on("click",
                                function () {
                                    if (row.F_InvId == null) {
                                        return;
                                    } 
                                    if (row.F_IsOutInvoice == 1) {
                                        Changjie.layerConfirm('已经开票，是否重置到【未开票】？', function (res,index) {
                                            if (res) {
                                                row.F_IsOutInvoice = 0;
                                               
                                                Changjie.httpPost(top.$.rootUrl +
                                                    '/PaySystem/OrderInvoice/UpdateInvoiceStatus?keyValue=' +
                                                    row.F_InvId +
                                                    "&IsOutInvoice=" +
                                                    row.F_IsOutInvoice, null,
                                                    function (data) {
                                                        if (data.code == 200) {
                                                            Changjie.alert.success(data.info);
                                                            refreshGirdData();
                                                        } else {
                                                            Changjie.alert.warning(data.info);
                                                        }

                                                    });

                                               
                                            }
                                            top.layer.close(index);
                                           
                                        });


                                    } else {

                                        row.F_IsOutInvoice = 1;
                                        $(this).html(
                                            '<span class="label label-success " style="cursor: pointer;">是</span>');
                                        Changjie.httpPost(top.$.rootUrl +
                                            '/PaySystem/OrderInvoice/UpdateInvoiceStatus?keyValue=' +
                                            row.F_InvId +
                                            "&IsOutInvoice=" +
                                            row.F_IsOutInvoice, null,
                                            function (data) {
                                                if (data.code == 200) {
                                                    Changjie.alert.success(data.info);
                                                } else {
                                                    Changjie.alert.warning(data.info);
                                                }

                                            });
                                    }

                                    if (row.F_IsOutInvoice == 1) {
                                        $(this).html(
                                            '<span class="label label-success " style="cursor: pointer;">是</span>');
                                    } else {
                                        $(this).html(
                                            '<span class="label label-info " style="cursor: pointer;">否</span>');
                                    }
                       

                                });


                            if (!!row.F_InvId) {

                                if (row.F_IsOutInvoice == "0") {
                                    return "<span class='label label-default'>否</span>";
                                } else if (row.F_IsOutInvoice == "1") {
                                    return "<span class='label label-success'>是</span>";
                                } else {
                                    return "<span class='label label-default'>否</span>";

                                }

                            }
                            return "<span class='label label-info'>无发票</span>";


                        }
                    },
                    { label: "买家姓名", name: "F_BuyerName", width: 80, align: "center" },
                    {
                        label: "订单应付金额", name: "F_OrderAmount", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            return Changjie.numberFormat(cellvalue, Changjie.sysGlobalSettings.pointGlobal, ".", ",") + " 元";
                        }
                    },
                    { label: "发票抬头", name: "F_Title", width: 200, align: "center" },
                    {
                        label: "发票内容", name: "F_InvContent", width: 150, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue == "1") {
                                return "会务费";
                            } else if (cellvalue == "2") {
                                return "培训费";
                            } else  {
                                return "其它";

                            }
                        }
                    },
                    { label: "单位名称", name: "F_Company", width: 200, align: "center" },
                    { label: "纳税人识别号", name: "F_Code", width: 200, align: "left" },
                    { label: "注册地址", name: "F_RegAddr", width: 200, align: "left" },
                    { label: "注册电话", name: "F_RegPhone", width: 100, align: "left" },
                    { label: "开户银行", name: "F_RegBname", width: 200, align: "left" },
                    { label: "银行帐户", name: "F_RegBaccount", width: 200, align: "left" },
                    { label: "收票人姓名", name: "F_RecName", width: 80, align: "left" },
                    { label: "收票人手机号", name: "F_RecMobphone", width: 100, align: "left" },
                    { label: "收票人省份", name: "F_RecProvince", width: 100, align: "left" },
                    { label: "送票地址", name: "F_GotoAddr", width: 200, align: "left" },
                    { label: "创建时间", name: "CreationDate", width: 100, align: "left" },
                ],
                mainId: 'F_OrderID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
