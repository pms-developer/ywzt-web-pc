﻿/*
 * 创建人：超级管理员
 * 日  期：2019-04-17 16:30
 * 描  述：通知类
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/PaySystem/PayCILinkOrderNotity/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_ONID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/PaySystem/PayCILinkOrderNotity/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_ONID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/PaySystem/PayCILinkOrderNotity/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PaySystem/PayCILinkOrderNotity/GetPageList',
                headData: [
                    { label: "交易类型", name: "F_Busicd", width: 100, align: "left"},
                    { label: "交易方向", name: "F_Txndir", width: 100, align: "left"},
                    { label: "版本号", name: "F_Version", width: 100, align: "left"},
                    { label: "签名方式", name: "F_SignType", width: 100, align: "left"},
                    { label: "编码格式", name: "F_Charset", width: 100, align: "left"},
                    { label: "订单号", name: "F_OrderNum", width: 100, align: "left"},
                    { label: "交易结果", name: "F_Respcd", width: 100, align: "left"},
                    { label: "机构号", name: "F_Inscd", width: 100, align: "left"},
                    { label: "渠道", name: "F_Chcd", width: 100, align: "left"},
                    { label: "附加信息", name: "F_Attach", width: 100, align: "left"},
                    { label: "支付状态", name: "F_State", width: 100, align: "left"},
                    { label: "原订单号", name: "F_OrigOrderNum", width: 100, align: "left"},
                    { label: "app支付调起参数", name: "F_tn", width: 100, align: "left"},
                    { label: "商户号", name: "F_Mchntid", width: 100, align: "left"},
                    { label: "终端号", name: "F_Terminalid", width: 100, align: "left"},
                    { label: "订单金额", name: "F_Txamt", width: 100, align: "left"},
                    { label: "交易币种", name: "F_Currency", width: 100, align: "left"},
                    { label: "商品列表", name: "F_GoodsList", width: 100, align: "left"},
                    { label: "渠道交易号", name: "F_ChannelOrderNum", width: 100, align: "left"},
                    { label: "用户账号", name: "F_ConsumerAccount", width: 100, align: "left"},
                    { label: "渠道号", name: "F_ConsumerId", width: 100, align: "left"},
                    { label: "响应信息", name: "F_ErrorDetail", width: 100, align: "left"},
                    { label: "签名", name: "F_Sign", width: 100, align: "left"},
                    { label: "渠道优惠", name: "F_ChcdDiscount", width: 100, align: "left"},
                    { label: "商户优惠", name: "F_MerDiscount", width: 100, align: "left"},
                    { label: "外部订单号", name: "F_OutOrderNum", width: 100, align: "left"},
                    { label: "交易时间", name: "F_TransTime", width: 100, align: "left"},
                    { label: "银行标识", name: "F_BankType", width: 100, align: "left"},
                    { label: "订单标题", name: "F_Subject", width: 100, align: "left"},
                    { label: "二维码信息", name: "F_QRcode", width: 100, align: "left"},
                    { label: "异步通知地址", name: "F_BackUrl", width: 100, align: "left"},
                    { label: "前台地址", name: "F_FrontUrl", width: 100, align: "left"},
                    { label: "支付时间", name: "F_PayTime", width: 100, align: "left"},
                    { label: "交易起始时间", name: "F_TimeStart", width: 100, align: "left"},
                    { label: "交易结束时间", name: "F_TimeExpire", width: 100, align: "left"},
                    { label: "操作员号", name: "F_Operatorid", width: 100, align: "left"},
                ],
                mainId:'F_ONID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload',{ queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
