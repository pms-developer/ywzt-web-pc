﻿/*
 * 创建人：超级管理员
 * 日  期：2019-04-10 11:47
 * 描  述：订单管理
 */
var refreshGirdData;
var bootstrap = function ($, Changjie) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').mkdate({
                dfdata: [
                    { name: '今天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return Changjie.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return Changjie.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').mkMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 400, 600);
            $('#F_OrderPlatform').mkDataItemSelect({ code: 'OrderPlatform' });
            $('#F_PaymentState').mkDataItemSelect({ code: 'PaymentState' });
            $('#F_OrderState').mkDataItemSelect({ code: 'OrderState' });
            $('#F_RefundState').mkDataItemSelect({ code: 'RefundState' });
            // 刷新
            $('#refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#add').on('click', function () {
                Changjie.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/PaySystem/PayOrder/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_OrderID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/PaySystem/PayOrder/Form?keyValue=' + keyValue,
                        width: 800,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_OrderID');
                if (Changjie.checkrow(keyValue)) {
                    Changjie.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            Changjie.deleteForm(top.$.rootUrl + '/PaySystem/PayOrder/DeleteForm', { keyValue: keyValue }, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
            //退款
            $('#refund').on('click', function () {
               

                var keyValue = $('#gridtable').jfGridValue('F_OrderID');
                if (Changjie.checkrow(keyValue)) {
                    var rowdata = $('#gridtable').jfGridGet("rowdata");

                    if (rowdata.F_PaymentState==0) {
                        Changjie.alert.warning("订单未付款！");
                        return;
                    }
                    if (rowdata.F_RefundState == 2) {
                        Changjie.alert.warning("订单已经全部退款！");
                        return;
                    }

                    
                    if (rowdata.F_PaymentState == 0) {
                        Changjie.alert.warning("未付款的订单，无法进行退款！");
                        return;
                    }

                  
                    

                    Changjie.layerForm({
                        id: 'form',
                        title: '申请退款',
                        url: (top.$.rootUrl + '/PaymentModule/PayOrder/Refund?keyValue=' + keyValue),
                        width: 720,
                        height: 800,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }

               
            });
            // 打印
            $('#print').on('click', function () {
                $('#gridtable').jqprintTable();
            });
            // 导出
            $('#outport').on('click', function () {
                Changjie.layerForm({
                    id: "ExcelExportForm",
                    title: '导出Excel数据',
                    url: top.$.rootUrl + '/Utility/ExcelExportForm?gridId=gridtable&filename=ordersinfo',
                    width: 500,
                    height: 380,
                    callBack: function (id) {
                        return top[id].acceptClick();
                    },
                    btn: ['导出Excel', '关闭']
                });
            });
        },
        // 初始化列表
        initGird: function () {
            $('#gridtable').mkAuthorizeJfGrid({
                url: top.$.rootUrl + '/PaySystem/PayOrder/GetPageList',
                headData: [
                    { label: "订单编号", name: "F_OrderSN", width: 140, align: "left" },
                    { label: "商品名称", name: "F_GoodsName", width: 180, align: "left" },
                    {
                        label: "订单应付金额", name: "F_OrderAmount", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            return Changjie.numberFormat(cellvalue, Changjie.sysGlobalSettings.pointGlobal, ".", ",") + " 元";
                        }
                    },
                    {
                        label: "来自平台", name: "F_OrderPlatform", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue == "0") {
                                return "<span class='label label-pc'>浏览器</span>";
                            } else if (cellvalue == "1") {
                                return "<span class='label label-az'>安卓</span>";
                            } else if (cellvalue == "2") {
                                return "<span class='label label-ios'>苹果</span>";
                                 
                            }
                        }
                    },
                    {
                        label: "支付方式", name: "F_PaymentId", width: 60, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue == "ALP") {
                                return "<span class='label label-zhifubao'>支付宝</span>";
                            } else if (cellvalue == "WXP") {
                                return "<span class='label label-weixin'>微信</span>";
                            } else if (cellvalue == "BP") {
                                return "<span class='label label-info'>银行卡</span>";
                            } else {
                                return "<span class='label label-default'>未选择</span>";
                            }
                        }
                    },
                    {
                        label: "付款状态", name: "F_PaymentState", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue == "0") {
                                return "<span class='label label-info'>未付款</span>";
                            } else if (cellvalue == "1") {
                                return "<span class='label label-primary'>已付款</span>";
                            }
                        }
                    },
                    {
                        label: "订单状态", name: "F_OrderState", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue == "0") {
                                return "<span class='label label-default'>已取消</span>";
                            } else if (cellvalue == "10") {
                                return "<span class='label label-default'>待付款</span>";
                            } else if (cellvalue == "40") {
                                return "<span class='label label-success'>交易完成</span>";
                            } else if (cellvalue == "50") {
                                return "<span class='label label-default'>已提交</span>";
                            } else if (cellvalue == "60") {
                                return "<span class='label label-default'>已确认</span>";
                            }
                        }
                    },
                    {
                        label: "退款状态", name: "F_RefundState", width: 100, align: "center",
                        formatter: function (cellvalue) {
                            if (cellvalue == 1) {
                                return "<span class='label label-info'>无</span>";
                            } else if (cellvalue == 2) {
                                return "<span class='label label-primary'>部分退款</span>";
                            } else if (cellvalue == 3) {
                                return "<span class='label label-default'>全部退款</span>";
                            } else  {
                                return "<span class='label label-info'>无</span>";
                            }
                        }
                    },
                    { label: "退款金额", name: "F_RefundAmount", width: 100, align: "center" },
                    { label: "买家编号", name: "F_BuyerId", width: 100, align: "center" },
                    { label: "买家姓名", name: "F_BuyerName", width: 100, align: "center" },
                    { label: "买家邮箱", name: "F_BuyerEmail", width: 180, align: "left" },

                    { label: "手机号", name: "F_BuyerMobile", width: 120, align: "left" },
                    { label: "订单生成时间", name: "F_CreateTime", width: 130, align: "center" },
                    { label: "支付时间", name: "F_PaymentTime", width: 130, align: "left" },
                    { label: "外部订单号", name: "F_OutSN", width: 130, align: "left" },
                    { label: "交易流水号", name: "F_TradeSN", width: 210, align: "left" },
                ],
                mainId: 'F_OrderID',
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
