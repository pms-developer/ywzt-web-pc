﻿/*
 * 日 期：2017.04.11
 * 描 述：即时通讯用户管理
 */
var keyValue = request('keyValue');

var acceptClick;
var bootstrap = function ($, Changjie) {
    "use strict";

    var page = {
        init: function () {
            // 验证编码是否重复
            $('#F_Code').on('blur', function () {
                $.mkExistField(keyValue, 'F_Code', top.$.rootUrl + '/IM/SysUser/ExistCode');
            });
            // 选择图标
            $('#selectIcon').on('click', function () {
                Changjie.layerForm({
                    id: 'iconForm',
                    title: '选择图标',
                    url: top.$.rootUrl + '/Utility/Icon',
                    height: 700,
                    width: 1000,
                    btn: null,
                    maxmin: true,
                    end: function () {
                        if (top._changjieSelectIcon != '') {
                            $('#F_Icon').val(top._changjieSelectIcon);
                        }
                    }
                });
            });

            page.initData();
        },
        initData: function () {
            if (!!keyValue) {
                Changjie.httpAsync('GET', top.$.rootUrl + '/IM/SysUser/GetFormData', { keyValue: keyValue }, function (data) {
                    $('#form').mkSetFormData(data);
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').mkValidform()) {
            return false;
        }
        var postData = $('#form').mkGetFormData(keyValue);
        $.mkSaveForm(top.$.rootUrl + '/IM/SysUser/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}