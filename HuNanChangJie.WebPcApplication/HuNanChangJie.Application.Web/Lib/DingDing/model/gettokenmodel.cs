﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuNanChangJie.Application.Web.Lib.DingDing.model
{
    public class gettokenmodel:basemodel
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
    }
}