﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuNanChangJie.Application.Web.Lib.DingDing.model
{
    public class departmentlistitemmodel
    {
        public bool auto_add_user { get; set; }
        public bool create_dept_group { get; set; }
        public long dept_id { get; set; }
        public string name { get; set; }
        public int parent_id { get; set; }
    }
}