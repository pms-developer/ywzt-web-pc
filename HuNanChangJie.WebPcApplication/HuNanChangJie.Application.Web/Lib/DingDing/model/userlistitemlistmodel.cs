﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuNanChangJie.Application.Web.Lib.DingDing.model
{
    public class userlistitemlistmodel
    {
        public bool active { get; set; }
        public bool admin { get; set; }
        public string avatar { get; set; }
        public bool boss { get; set; }
        public List<int> dept_id_list { get; set; }
        public string mobile { get; set; }
        public bool exclusive_account { get; set; }
        public bool hide_mobile { get; set; }
        public bool leader { get; set; }
        public string unionid { get; set; }
        public string userid { get; set; }
        public string name { get; set; }
        public string job_number { get; set; }
        public string email { get; set; }
        public string remark { get; set; }
        public string title { get; set; }
        public string extension { get; set; }

    }
}