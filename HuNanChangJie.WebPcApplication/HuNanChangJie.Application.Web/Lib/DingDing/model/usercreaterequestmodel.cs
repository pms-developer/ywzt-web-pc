﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuNanChangJie.Application.Web.Lib.DingDing.model
{
    public class usercreaterequestmodel
    {
        public string job_number { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string userid { get; set; }
        
        public string dept_id_list { get; set; }
        public string email { get; set; }
        public string remark { get; set; }
        
    }
}