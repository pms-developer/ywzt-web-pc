﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuNanChangJie.Application.Web.Lib.DingDing.model
{
    public class userlistitemmodel
    {
        public bool has_more { get; set; }
        public List<userlistitemlistmodel> list { get; set; }
        public int next_cursor { get; set; }
    }
}