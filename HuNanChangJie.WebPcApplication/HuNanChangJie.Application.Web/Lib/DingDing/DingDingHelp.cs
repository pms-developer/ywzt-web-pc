﻿using HuNanChangJie.Application.Organization;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Application.Web.Lib.DingDing.model;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuNanChangJie.Application.Web.Lib.DingDing
{
    public class DingDingHelp : I3rdHelp
    {
        public string appKey { set; get; }
        public string appSecret { set; get; }
        public string companyId { set; get; }

        public DingDingHelp(string companyId)
        {
            CompanyEntity model = new CompanyBLL().GetEntity(companyId);
            if (model == null)
                throw new Exception("公司不存在");

            if (string.IsNullOrEmpty(model.DingAppkey) || string.IsNullOrEmpty(model.DingAppsecret))
                throw new Exception("公司钉钉参数设置错误");

            this.appKey = model.DingAppkey;
            this.appSecret = model.DingAppsecret;
            this.companyId = model.F_CompanyId;
        }

        public OperateResultEntity GetToken()
        {
            Base_AccessTokenBLL base_AccessTokenBLL = new Base_AccessTokenBLL();
            Base_AccessTokenEntity base_AccessTokenEntity = base_AccessTokenBLL.GetEntityByAppKey(this.appKey, false);
            if (base_AccessTokenEntity != null && base_AccessTokenEntity.ExpiresIn > DateTime.Now)
            {
                return new OperateResultEntity() { Success = true, Message = base_AccessTokenEntity.AccessToken };
            }

            HttpHelper http = new HttpHelper();
            HttpItem httpItem = new HttpItem()
            {
                URL = $"https://oapi.dingtalk.com/gettoken?appkey={this.appKey}&appsecret={this.appSecret}"
            };
            HttpResult result = http.GetHtml(httpItem);
            //Console.WriteLine("CoinSyncHandle" + item["syncsymbol"].ToString() + result.StatusCode);
            string html = result.Html;
            gettokenmodel model = JsonHelper.DeserializeJsonToObject<gettokenmodel>(html);
            if (model != null)
            {
                if (model.errcode == 0)
                {
                    if (base_AccessTokenEntity != null)
                    {
                        base_AccessTokenEntity.AccessToken = model.access_token;
                        base_AccessTokenEntity.ExpiresIn = DateTime.Now.AddSeconds(7100);
                        base_AccessTokenBLL.SaveEntity(base_AccessTokenEntity.id.Value.ToString(), base_AccessTokenEntity);
                    }
                    else
                    {
                        base_AccessTokenEntity = new Base_AccessTokenEntity()
                        {
                            AppKey = this.appKey,
                            AppType = "DingDing",
                            AccessToken = model.access_token,
                            ExpiresIn = DateTime.Now.AddSeconds(7100)
                        };
                        base_AccessTokenBLL.SaveEntity("", base_AccessTokenEntity);
                    }
                    return new OperateResultEntity() { Success = true, Message = model.access_token };
                }
                else
                    return new OperateResultEntity() { Success = false, Message = model.errmsg };
            }
            return new OperateResultEntity() { Success = true, Message = model.access_token };
        }

        public OperateResultEntity ImportDept()
        {
            OperateResultEntity getToken = GetToken();
            if (getToken.Success)
            {
                string accessToken = getToken.Message;
                HttpHelper http = new HttpHelper();
                HttpItem httpItem = new HttpItem()
                {
                    URL = $"https://oapi.dingtalk.com/topapi/v2/department/listsub?access_token={accessToken}"
                };
                HttpResult result = http.GetHtml(httpItem);
                string html = result.Html;
                departmentlistmodel model = JsonHelper.DeserializeJsonToObject<departmentlistmodel>(html);
                if (model != null)
                {
                    if (model.errcode == 0)
                    {
                        DepartmentBLL departmentBLL = new DepartmentBLL();
                        UserInfo userInfo = LoginUserInfo.Get();
                        List<DepartmentEntity> listDept = departmentBLL.GetList(this.companyId);
                        if (listDept == null)
                            listDept = new List<DepartmentEntity>();
                        foreach (var item in model.result)
                        {
                            DepartmentEntity departmentEntity = listDept.Find(m => m.F_FullName == item.name || m.DingDeptId == item.dept_id.ToString());

                            ///这里可能会产生数据问题 后期需要视情况优化一下
                            string realParentId = "0";
                            DepartmentEntity departmentParentEntity = listDept.Find(m => m.DingDeptId == item.parent_id.ToString());
                            if (departmentParentEntity != null)
                                realParentId = departmentParentEntity.F_DepartmentId;

                            if (departmentEntity != null)
                            {
                                departmentEntity.DingDeptId = item.dept_id.ToString();
                                departmentEntity.F_FullName = item.name;
                                departmentBLL.SaveEntity(departmentEntity.F_DepartmentId, departmentEntity);
                            }
                            else
                            {
                                departmentEntity = new DepartmentEntity()
                                {
                                    F_DepartmentId = Guid.NewGuid().ToString().ToLower(),
                                    DingDeptId = item.dept_id.ToString(),
                                    F_FullName = item.name,
                                    F_CompanyId = this.companyId,
                                    Creation_Id = userInfo.userId,
                                    CreationName = userInfo.realName,
                                    F_EnCode = item.dept_id.ToString(),
                                    F_ParentId = realParentId
                                };
                                departmentBLL.SaveEntity("", departmentEntity);
                            }
                        }
                        return new OperateResultEntity() { Success = true };
                    }
                    else
                    {
                        return new OperateResultEntity() { Success = false, Message = model.errmsg };
                    }
                }
                else
                {
                    return new OperateResultEntity() { Success = false, Message = "接口数据异常" };
                }
            }
            else
                return getToken;
        }

        public OperateResultEntity ExportDept()
        {
            OperateResultEntity getToken = GetToken();
            if (getToken.Success)
            {
                try
                {
                    string accessToken = getToken.Message;
                    DepartmentBLL departmentBLL = new DepartmentBLL();
                    UserInfo userInfo = LoginUserInfo.Get();
                    List<DepartmentEntity> listDept = departmentBLL.GetList(this.companyId);
                    if (listDept == null)
                        listDept = new List<DepartmentEntity>();
                    HttpHelper http = new HttpHelper();

                    List<DepartmentEntity> listRootDept = listDept.Where(m => m.F_ParentId == "0").ToList();
                    while (listRootDept.Count() > 0)
                    {
                        foreach (DepartmentEntity item in listRootDept)
                        {
                            if (!string.IsNullOrEmpty(item.DingDeptId))
                                continue;

                            long parentId = 1;
                            if (!string.IsNullOrEmpty(item.F_ParentId) && item.F_ParentId != "0")
                            {
                                DepartmentEntity parentDepartmentEntity = listDept.Where(m => m.F_DepartmentId == item.F_ParentId).FirstOrDefault();
                                if (parentDepartmentEntity != null)
                                {
                                    parentId = long.Parse(parentDepartmentEntity.DingDeptId);
                                }
                            }

                            departmentcreaterequestmodel json = new departmentcreaterequestmodel()
                            {
                                name = item.F_FullName,
                                parent_id = parentId
                            };

                            string response = JsonHelper.SerializeObject(json);

                            HttpItem httpItem = new HttpItem()
                            {
                                Method = "POST",
                                Postdata = response,
                                URL = $"https://oapi.dingtalk.com/topapi/v2/department/create?access_token={accessToken}"
                            };
                            HttpResult result = http.GetHtml(httpItem);
                            string html = result.Html;
                            departmentcreatemodel model = JsonHelper.DeserializeJsonToObject<departmentcreatemodel>(html);
                            if (model != null)
                            {
                                if (model.errcode == 0)
                                {
                                    item.DingDeptId = model.result.dept_id.ToString();

                                    departmentBLL.SaveEntity(item.F_DepartmentId, item);
                                }
                            }
                        }

                        List<string> parentIdList = listRootDept.Select(m => m.F_DepartmentId).ToList();
                        listRootDept = listDept.Where(m => parentIdList.Contains(m.F_ParentId)).ToList();
                    }

                    return new OperateResultEntity() { Success = true };

                }
                catch (Exception ex)
                {
                    return new OperateResultEntity() { Success = false, Message = ex.Message };
                }
            }
            else
                return getToken;
        }

        public OperateResultEntity ImportUser()
        {
            OperateResultEntity getToken = GetToken();
            if (getToken.Success)
            {
                string accessToken = getToken.Message;
                HttpHelper http = new HttpHelper();
                DepartmentBLL departmentBLL = new DepartmentBLL();

                UserBLL userBLL = new UserBLL();

                UserInfo userInfo = LoginUserInfo.Get();
                List<DepartmentEntity> listDept = departmentBLL.GetList(this.companyId);

                List<DepartmentEntity> listDingDept = listDept.FindAll(m => !string.IsNullOrEmpty(m.DingDeptId));

                listDingDept.Add(new DepartmentEntity() { F_DepartmentId = "0", DingDeptId = "1" });
                foreach (DepartmentEntity item in listDingDept)
                {
                    bool hasMore = true;
                    int nextCursor = 0;
                    while (hasMore)
                    {
                        hasMore = false;
                        HttpItem httpItem = new HttpItem()
                        {
                            URL = $"https://oapi.dingtalk.com/topapi/v2/user/list?access_token={accessToken}&dept_id={item.DingDeptId}&cursor={nextCursor}&size=100"
                        };
                        HttpResult result = http.GetHtml(httpItem);
                        string html = result.Html;
                        userlistmodel model = JsonHelper.DeserializeJsonToObject<userlistmodel>(html);
                        if (model != null)
                        {
                            if (model.errcode == 0)
                            {
                                hasMore = model.result.has_more;
                                nextCursor = model.result.next_cursor;
                                if (model.result != null && model.result.list != null && model.result.list.Count() > 0)
                                {
                                    foreach (userlistitemlistmodel userlistitemmodel in model.result.list)
                                    {
                                        UserEntity userEntity = userBLL.GetList(this.companyId).Find(m => m.DingUserId == userlistitemmodel.userid ||
                                        m.DingUnionId == userlistitemmodel.unionid ||
                                          m.F_Mobile == userlistitemmodel.mobile
                                            );

                                        string departmentId = "";
                                        if (userlistitemmodel.dept_id_list != null && userlistitemmodel.dept_id_list.Count() > 0)
                                        {
                                            foreach (int dingDeptItem in userlistitemmodel.dept_id_list)
                                            {
                                                DepartmentEntity departmentEntity = listDept.Find(m => m.DingDeptId == dingDeptItem.ToString());
                                                if (departmentEntity != null)
                                                {
                                                    departmentId = departmentEntity.F_DepartmentId;
                                                    break;
                                                }
                                            }
                                        }

                                        if (userEntity != null)
                                        {
                                            if (!string.IsNullOrEmpty(userlistitemmodel.job_number))
                                                userEntity.F_Account = userlistitemmodel.job_number;
                                            else if (!string.IsNullOrEmpty(userlistitemmodel.mobile))
                                                userEntity.F_Account = userlistitemmodel.mobile;
                                            else
                                                userEntity.F_Account = userlistitemmodel.name;
                                            userEntity.F_Gender = 1;

                                            userEntity.DingUnionId = userlistitemmodel.unionid;
                                            userEntity.F_Mobile = userlistitemmodel.mobile;
                                            userEntity.DingUserId = userlistitemmodel.userid;
                                            userEntity.DingUnionId = userlistitemmodel.unionid;
                                            userEntity.F_DepartmentId = departmentId;
                                            userEntity.F_RealName = userlistitemmodel.name;
                                            userEntity.F_EnCode = userlistitemmodel.job_number;
                                            userEntity.F_Email = userlistitemmodel.email;
                                            userEntity.F_Description = $"{userlistitemmodel.title};{userlistitemmodel.remark};{userlistitemmodel.extension}";
                                            userBLL.SaveEntity(userEntity.F_UserId, userEntity);
                                        }
                                        else
                                        {
                                            userEntity = new UserEntity()
                                            {
                                                F_Gender = 1,
                                                F_UserId = Guid.NewGuid().ToString().ToLower(),
                                                F_CompanyId = this.companyId,
                                                Creation_Id = userInfo.userId,
                                                CreationName = userInfo.realName,
                                                DingUnionId = userlistitemmodel.unionid,
                                                F_Mobile = userlistitemmodel.mobile,
                                                DingUserId = userlistitemmodel.userid,
                                                F_DepartmentId = departmentId,
                                                F_RealName = userlistitemmodel.name,
                                                F_Password = "123456",
                                                F_EnCode = userlistitemmodel.job_number,
                                                F_Email = userlistitemmodel.email,
                                                F_Description = $"{userlistitemmodel.title};{userlistitemmodel.remark};{userlistitemmodel.extension}"
                                            };

                                            if (!string.IsNullOrEmpty(userlistitemmodel.job_number))
                                                userEntity.F_Account = userlistitemmodel.job_number;
                                            else if (!string.IsNullOrEmpty(userlistitemmodel.mobile))
                                                userEntity.F_Account = userlistitemmodel.mobile;
                                            else
                                                userEntity.F_Account = userlistitemmodel.name;

                                            userBLL.SaveEntity("", userEntity);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return new OperateResultEntity() { Success = false, Message = "接口数据异常" };
            }
            else
                return getToken;
        }

        public OperateResultEntity ExportUser()
        {
            OperateResultEntity getToken = GetToken();
            if (getToken.Success)
            {
                try
                {
                    string accessToken = getToken.Message;

                    UserBLL userBLL = new UserBLL();
                    UserInfo userInfo = LoginUserInfo.Get();
                    List<UserEntity> listUser = userBLL.GetList(this.companyId);

                    DepartmentBLL departmentBLL = new DepartmentBLL();
                    List<DepartmentEntity> listDept = departmentBLL.GetList(this.companyId);

                    if (listUser == null)
                        listUser = new List<UserEntity>();
                    HttpHelper http = new HttpHelper();

                    foreach (UserEntity item in listUser)
                    {
                        if (!string.IsNullOrEmpty(item.DingUserId))
                            continue;
                        if (!string.IsNullOrEmpty(item.F_Mobile))
                            continue;
                        long deptId = 1;
                        DepartmentEntity departmentEntity = listDept.Where(m => m.F_DepartmentId == item.F_DepartmentId).FirstOrDefault();
                        if (departmentEntity != null && !string.IsNullOrEmpty(departmentEntity.DingDeptId))
                        {
                            deptId = long.Parse(departmentEntity.DingDeptId);
                        }
                        usercreaterequestmodel json = new usercreaterequestmodel()
                        {
                            name = item.F_RealName,
                            dept_id_list = deptId.ToString(),
                            email = item.F_Email,
                            job_number = item.F_EnCode,
                            mobile = item.F_Mobile,
                            remark = item.F_Description
                        };

                        string response = JsonHelper.SerializeObject(json);

                        HttpItem httpItem = new HttpItem()
                        {
                            Method = "POST",
                            Postdata = response,
                            URL = $"https://oapi.dingtalk.com/topapi/v2/user/create?access_token={accessToken}"
                        };
                        HttpResult result = http.GetHtml(httpItem);
                        string html = result.Html;
                        usercreatemodel model = JsonHelper.DeserializeJsonToObject<usercreatemodel>(html);
                        if (model != null)
                        {
                            if (model.errcode == 0)
                            {
                                item.DingUserId = model.result.userid.ToString();

                                userBLL.SaveEntity(item.F_UserId, item);
                            }
                        }
                    }

                    return new OperateResultEntity() { Success = true };

                }
                catch (Exception ex)
                {
                    return new OperateResultEntity() { Success = false, Message = ex.Message };
                }
            }
            else
                return getToken;
        }
    }
}