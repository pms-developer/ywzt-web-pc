﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Web.Lib
{
    interface I3rdHelp
    {
        OperateResultEntity GetToken();
        OperateResultEntity ImportDept();
        OperateResultEntity ExportDept();
        OperateResultEntity ImportUser();
        OperateResultEntity ExportUser();
    }
}
