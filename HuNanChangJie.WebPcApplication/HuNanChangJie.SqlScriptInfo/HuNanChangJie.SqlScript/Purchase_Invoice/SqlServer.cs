﻿using HuNanChangJie.DataBase.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuNanChangJie.SqlScript.Purchase_Invoice
{
    internal class SqlServer : IPurchaseInvoiceSql
    {
        private SqlServer()
        {
            
        }
        public static SqlServer GetInstance => new SqlServer();

        public string GetDetailsSql 
        {
            get 
            {
                return @"select * from (select 
                        a.*,
                        c.Code as OrderCode,
                        c.TaxRage as TaxRate,
                        e.Name as ContractName,
                        d.Code as MaterialsCode,
                        d.Name as MaterialsName,
                        d.Brand,
                        d.ModelNumber,
                        d.Unit,
                        d.BudgetPrice,
                        b.Quantity as OrderQuantity,
                        b.ReceivedInvoiceQuantity
                         from Purchase_InvoiceDetails as a left join
                        Purchase_OrderDetails as b on a.PurchaseOrderDetailsId = b.ID left join
                        Purchase_Order as c on a.PurchaseOrderId = c.ID left join
                        Purchase_ContractDetails as d on a.PurchaseContractDetailsId = d.ID left join
                        Purchase_Contract as e on a.PurchaseContractId = e.ID ) as t where 1=1 ";
            }
        }
    }
}
