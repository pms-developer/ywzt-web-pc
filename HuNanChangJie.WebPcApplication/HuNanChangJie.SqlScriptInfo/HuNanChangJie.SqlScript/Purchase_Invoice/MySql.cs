﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuNanChangJie.SqlScript.Purchase_Invoice
{
    internal class MySql : IPurchaseInvoiceSql
    {
        private MySql()
        { 
        }

        public static MySql GetInstance => new MySql();

        public string GetDetailsSql => SqlServer.GetInstance.GetDetailsSql;
    }
}
