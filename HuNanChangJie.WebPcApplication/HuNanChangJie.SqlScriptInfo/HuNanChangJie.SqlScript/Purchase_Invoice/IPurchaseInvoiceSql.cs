﻿using HuNanChangJie.DataBase.Repository;

namespace HuNanChangJie.SqlScript.Purchase_Invoice
{
    public  interface IPurchaseInvoiceSql
    {
        /// <summary>
        /// 获取采购发票详情
        /// </summary>
        string GetDetailsSql { get; }
    }
}