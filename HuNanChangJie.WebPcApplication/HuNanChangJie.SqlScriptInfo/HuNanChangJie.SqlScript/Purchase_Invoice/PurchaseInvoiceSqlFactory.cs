﻿using HuNanChangJie.DataBase.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using HuNanChangJie.SystemCommon;
namespace HuNanChangJie.SqlScript.Purchase_Invoice
{
    public class PurchaseInvoiceSqlFactory
    {
        IRepository _repository;
        public PurchaseInvoiceSqlFactory(IRepository repository)
        {
            _repository = repository;
        }
        public IPurchaseInvoiceSql GetPurchaseInvoiceSql
        {
            get
            {
                switch (_repository.DataBaseType)
                {
                    case DataBaseType.MySql:
                        return  MySql.GetInstance;
                    case DataBaseType.Oracle:
                        return  Oracle.GetInstance;
                    case DataBaseType.SqlServer:
                        return SqlServer.GetInstance;
                    default:
                        return SqlServer.GetInstance;
                }
            }
        }
    }
}
