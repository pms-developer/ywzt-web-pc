﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuNanChangJie.SqlScript.Purchase_Invoice
{
    internal class Oracle : IPurchaseInvoiceSql
    {
        private Oracle()
        { 
        }
        public static Oracle GetInstance => new Oracle();
        public string GetDetailsSql => SqlServer.GetInstance.GetDetailsSql;

    }
}
