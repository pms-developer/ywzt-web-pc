﻿using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuNanChangJie.SqlScript.Baes_Module
{
    public class BaesModuleSqlFactory
    {
        IRepository _repository;
        public BaesModuleSqlFactory(IRepository repository)
        {
            _repository = repository;
        }

        public IBaesModule GetBaesModule()
        {
            switch (_repository.DataBaseType)
            {
                case DataBaseType.MySql:
                    return new MySql();
                case DataBaseType.Oracle:
                    return new Oracle();
                case DataBaseType.SqlServer:
                    return new SqlServer();
                default:
                    return new SqlServer();
            }
        }
    }
}
