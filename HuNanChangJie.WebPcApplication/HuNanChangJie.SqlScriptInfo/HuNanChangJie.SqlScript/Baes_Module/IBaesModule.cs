﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuNanChangJie.SqlScript.Baes_Module
{
    public interface IBaesModule
    {
        /// <summary>
        /// 获取项目模块树形结构数
        /// </summary>
        string GetProjectModuleTreeSql { get; }
    }
}
