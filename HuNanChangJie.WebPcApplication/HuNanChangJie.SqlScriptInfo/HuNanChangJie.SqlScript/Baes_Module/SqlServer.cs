﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HuNanChangJie.SqlScript.Baes_Module
{
    internal class SqlServer : IBaesModule
    {
        public string GetProjectModuleTreeSql => @"WITH ProjectModuleTree AS
                                                (
                                                 SELECT fun.* FROM Base_Module fun WHERE fun.F_FullName='我的项目'  
                                                    UNION ALL
                                                 SELECT fun.* FROM ProjectModuleTree,Base_Module fun WHERE fun.F_ParentId=ProjectModuleTree.F_ModuleId
                                                )
                                                SELECT * FROM ProjectModuleTree";
    }
}
