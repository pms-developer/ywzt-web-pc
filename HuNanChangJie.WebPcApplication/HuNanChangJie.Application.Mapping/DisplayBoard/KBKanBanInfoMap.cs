﻿using HuNanChangJie.Application.TwoDevelopment.DisplayBoard;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:19
    /// 描 述：KBKanBanInfo
    /// </summary>
    public class KBKanBanInfoMap : EntityTypeConfiguration<KBKanBanInfoEntity>
    {
        public KBKanBanInfoMap()
        {
            #region  表、主键
            //表
            this.ToTable("KBKANBANINFO");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

