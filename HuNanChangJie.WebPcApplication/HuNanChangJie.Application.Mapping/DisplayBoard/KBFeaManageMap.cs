﻿using HuNanChangJie.Application.TwoDevelopment.DisplayBoard;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:17
    /// 描 述：看板管理
    /// </summary>
    public class KBFeaManageMap : EntityTypeConfiguration<KBFeaManageEntity>
    {
        public KBFeaManageMap()
        {
            #region  表、主键
            //表
            this.ToTable("KBFEAMANAGE");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

