﻿using HuNanChangJie.Application.TwoDevelopment.Desktop;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-09-25 11:32
    /// 描 述：图标配置
    /// </summary>
    public class DTChartMap : EntityTypeConfiguration<DTChartEntity>
    {
        public DTChartMap()
        {
            #region  表、主键
            //表
            this.ToTable("DT_CHART");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

