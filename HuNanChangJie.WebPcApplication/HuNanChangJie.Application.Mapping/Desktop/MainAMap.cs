﻿using HuNanChangJie.Application.TwoDevelopment.Desktop;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-19 17:27
    /// 描 述：AutoCodeTest
    /// </summary>
    public class MainAMap : EntityTypeConfiguration<MainAEntity>
    {
        public MainAMap()
        {
            #region  表、主键
            //表
            this.ToTable("MAINA");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

