﻿using HuNanChangJie.Application.TwoDevelopment.Desktop;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-06 15:24
    /// 描 述：WorkflowTest
    /// </summary>
    public class WorkflowTestSubMap : EntityTypeConfiguration<WorkflowTestSubEntity>
    {
        public WorkflowTestSubMap()
        {
            #region  表、主键
            //表
            this.ToTable("WORKFLOWTESTSUB");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

