﻿using HuNanChangJie.Application.TwoDevelopment.Desktop;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-07-07 10:49
    /// 描 述：App开发测试
    /// </summary>
    public class MobileTestMap : EntityTypeConfiguration<MobileTestEntity>
    {
        public MobileTestMap()
        {
            #region  表、主键
            //表
            this.ToTable("MOBILETEST");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

