﻿using HuNanChangJie.Application.Excel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.Excel
{
    public class ExcelImportTableSetMap : EntityTypeConfiguration<ExcelImportTableSetEntity>
    {
        public ExcelImportTableSetMap()
        {
            #region  表、主键
            //表
            this.ToTable("EXCEL_IMPORTTABLESET");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
