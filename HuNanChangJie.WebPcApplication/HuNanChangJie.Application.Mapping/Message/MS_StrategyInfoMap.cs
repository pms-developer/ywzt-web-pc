﻿using HuNanChangJie.Application.TwoDevelopment.Message;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-02-27 16:27
    /// 描 述：消息策略
    /// </summary>
    public class MS_StrategyInfoMap : EntityTypeConfiguration<MS_StrategyInfoEntity>
    {
        public MS_StrategyInfoMap()
        {
            #region  表、主键
            //表
            this.ToTable("MS_STRATEGYINFO");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

