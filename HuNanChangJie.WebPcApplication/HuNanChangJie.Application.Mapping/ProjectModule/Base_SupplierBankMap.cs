﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-12 18:00
    /// 描 述：供应商管理
    /// </summary>
    public class Base_SupplierBankMap : EntityTypeConfiguration<Base_SupplierBankEntity>
    {
        public Base_SupplierBankMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_SUPPLIERBANK");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

