﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-14 15:29
    /// 描 述：外出经营管理许可证
    /// </summary>
    public class Project_ExternalManagementCertificateMap : EntityTypeConfiguration<Project_ExternalManagementCertificateEntity>
    {
        public Project_ExternalManagementCertificateMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_EXTERNALMANAGEMENTCERTIFICATE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

