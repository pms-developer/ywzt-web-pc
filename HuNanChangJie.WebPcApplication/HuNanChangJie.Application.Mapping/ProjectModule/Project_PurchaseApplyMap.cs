﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-10 15:13
    /// 描 述：采购申请
    /// </summary>
    public class Project_PurchaseApplyMap : EntityTypeConfiguration<Project_PurchaseApplyEntity>
    {
        public Project_PurchaseApplyMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_PURCHASEAPPLY");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

