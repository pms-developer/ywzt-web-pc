﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-30 14:35
    /// 描 述：合同支出
    /// </summary>
    public class Contract_ExpenditureMap : EntityTypeConfiguration<Contract_ExpenditureEntity>
    {
        public Contract_ExpenditureMap()
        {
            #region  表、主键
            //表
            this.ToTable("CONTRACT_EXPENDITURE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

