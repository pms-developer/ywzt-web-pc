﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 18:57
    /// 描 述：工程合同产值结算
    /// </summary>
    public class Project_OutputSettlementMap : EntityTypeConfiguration<Project_OutputSettlementEntity>
    {
        public Project_OutputSettlementMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_OUTPUTSETTLEMENT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

