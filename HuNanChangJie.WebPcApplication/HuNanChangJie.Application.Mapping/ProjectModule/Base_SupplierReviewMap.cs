﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 10:52
    /// 描 述：供应商评审
    /// </summary>
    public class Base_SupplierReviewMap : EntityTypeConfiguration<Base_SupplierReviewEntity>
    {
        public Base_SupplierReviewMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_SUPPLIERREVIEW");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

