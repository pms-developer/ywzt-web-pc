﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 19:09
    /// 描 述：外派人员工资
    /// </summary>
    public class Project_DispatchedWageMap : EntityTypeConfiguration<Project_DispatchedWageEntity>
    {
        public Project_DispatchedWageMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_DISPATCHEDWAGE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

