﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-05 14:45
    /// 描 述：项目阶段
    /// </summary>
    public class Base_ProjectPhaseMap : EntityTypeConfiguration<Base_ProjectPhaseEntity>
    {
        public Base_ProjectPhaseMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_PROJECTPHASE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

