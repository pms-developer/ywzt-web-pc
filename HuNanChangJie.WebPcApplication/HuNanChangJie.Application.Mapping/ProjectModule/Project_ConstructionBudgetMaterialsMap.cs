﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-03 20:43
    /// 描 述：施工预算
    /// </summary>
    public class Project_ConstructionBudgetMaterialsMap : EntityTypeConfiguration<Project_ConstructionBudgetMaterialsEntity>
    {
        public Project_ConstructionBudgetMaterialsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_CONSTRUCTIONBUDGETMATERIALS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

