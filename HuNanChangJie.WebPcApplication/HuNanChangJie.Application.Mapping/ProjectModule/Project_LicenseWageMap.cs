﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-29 17:02
    /// 描 述：证照使用工资
    /// </summary>
    public class Project_LicenseWageMap : EntityTypeConfiguration<Project_LicenseWageEntity>
    {
        public Project_LicenseWageMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_LICENSEWAGE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

