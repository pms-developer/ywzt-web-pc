﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectModule
{
    /// <summary> 
    /// 创 建：超级管理员 
    /// 日 期：2020-08-18 11:32 
    /// 描 述：人员及证照扣款记录 
    /// </summary> 
    public class Project_StaffRecordMap : EntityTypeConfiguration<Project_StaffRecordEntity>
    {
        public Project_StaffRecordMap()
        {
            #region  表、主键 
            //表 
            this.ToTable("PROJECT_STAFFRECORD");
            //主键 
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系 
            #endregion
        }
    }
}
