﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-23 11:52
    /// 描 述：分包变更
    /// </summary>
    public class Project_SubcontractChangeMaterialsMap : EntityTypeConfiguration<Project_SubcontractChangeMaterialsEntity>
    {
        public Project_SubcontractChangeMaterialsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_SUBCONTRACTCHANGEMATERIALS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

