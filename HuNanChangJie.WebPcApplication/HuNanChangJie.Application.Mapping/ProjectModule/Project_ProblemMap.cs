﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-28 19:56
    /// 描 述：问题反馈
    /// </summary>
    public class Project_ProblemMap : EntityTypeConfiguration<Project_ProblemEntity>
    {
        public Project_ProblemMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_PROBLEM");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

