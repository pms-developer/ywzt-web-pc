﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-21 15:29
    /// 描 述：仓库材料明细
    /// </summary>
    public class Base_WarehouseDetailsMap : EntityTypeConfiguration<Base_WarehouseDetailsEntity>
    {
        public Base_WarehouseDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_WAREHOUSEDETAILS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

