﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-26 17:47
    /// 描 述：分包结算
    /// </summary>
    public class Project_SubcontractSettlementQuantitiesMap : EntityTypeConfiguration<Project_SubcontractSettlementQuantitiesEntity>
    {
        public Project_SubcontractSettlementQuantitiesMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_SUBCONTRACTSETTLEMENTQUANTITIES");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

