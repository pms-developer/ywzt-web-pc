﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-17 11:15
    /// 描 述：预算科目费用清单
    /// </summary>
    public class Project_SubjectFeeDetailsMap : EntityTypeConfiguration<Project_SubjectFeeDetailsEntity>
    {
        public Project_SubjectFeeDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_CONSTRUCTIONBUDGESUBJECTFEEDETAILS");
            //主键
            this.HasKey(t => t.SubjectFeeDetails_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

