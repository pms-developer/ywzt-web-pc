﻿using HuNanChangeJie.Application.Project.ProjectSubject;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectModule
{
    public class Tbbj_SubjectMap : EntityTypeConfiguration<Tbbj_SubjectEntity>
    {
        public Tbbj_SubjectMap()
        {
            #region  表、主键
            //表
            this.ToTable("Tbbj_Subject");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
