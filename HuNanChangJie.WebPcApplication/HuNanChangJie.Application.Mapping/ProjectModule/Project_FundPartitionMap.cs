﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 10:17
    /// 描 述：成本分割设置
    /// </summary>
    public class Project_FundPartitionMap : EntityTypeConfiguration<Project_FundPartitionEntity>
    {
        public Project_FundPartitionMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_FUNDPARTITION");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

