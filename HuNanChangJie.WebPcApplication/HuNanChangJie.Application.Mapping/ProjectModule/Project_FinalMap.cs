﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2020-03-24 10:58 
    /// 描 述：完成决算 
    /// </summary> 
    public class Project_FinalMap : EntityTypeConfiguration<Project_FinalEntity>
    {
        public Project_FinalMap()
        {
            #region  表、主键 
            //表 
            this.ToTable("PROJECT_FINAL");
            //主键 
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系 
            #endregion
        }
    }
}