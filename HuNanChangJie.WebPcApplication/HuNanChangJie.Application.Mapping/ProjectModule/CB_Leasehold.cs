﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectModule
{
  public class CB_LeaseholdMap : EntityTypeConfiguration<CB_Leasehold>
    {
        public CB_LeaseholdMap()
        {
            #region  表、主键
            //表
            this.ToTable("CB_LEASEHOLD");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
