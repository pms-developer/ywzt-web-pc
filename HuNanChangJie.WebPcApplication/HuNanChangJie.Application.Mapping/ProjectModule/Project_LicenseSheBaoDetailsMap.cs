﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 15:57
    /// 描 述：证照社保
    /// </summary>
    public class Project_LicenseSheBaoDetailsMap : EntityTypeConfiguration<Project_LicenseSheBaoDetailsEntity>
    {
        public Project_LicenseSheBaoDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_LICENSESHEBAODETAILS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

