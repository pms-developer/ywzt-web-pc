﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-20 11:46
    /// 描 述：分包单位
    /// </summary>
    public class Base_SubcontractingUnitMap : EntityTypeConfiguration<Base_SubcontractingUnitEntity>
    {
        public Base_SubcontractingUnitMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_SUBCONTRACTINGUNIT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

