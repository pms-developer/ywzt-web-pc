﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-18 12:19
    /// 描 述：开标登记
    /// </summary>
    public class Project_BidOpeningDetailsMap : EntityTypeConfiguration<Project_BidOpeningDetailsEntity>
    {
        public Project_BidOpeningDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_BIDOPENINGDETAILS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

