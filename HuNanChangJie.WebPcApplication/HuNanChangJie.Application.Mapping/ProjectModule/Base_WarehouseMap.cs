﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 11:32
    /// 描 述：仓库管理
    /// </summary>
    public class Base_WarehouseMap : EntityTypeConfiguration<Base_WarehouseEntity>
    {
        public Base_WarehouseMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_WAREHOUSE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

