﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-04-27 17:00
    /// 描 述：店铺基本信息管理
    /// </summary>
    public class base_shopinfoMap : EntityTypeConfiguration<base_shopinfoEntity>
    {
        public base_shopinfoMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_SHOPINFO");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

