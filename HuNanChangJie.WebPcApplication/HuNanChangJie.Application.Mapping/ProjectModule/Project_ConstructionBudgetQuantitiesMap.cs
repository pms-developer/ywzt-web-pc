﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-03 20:43
    /// 描 述：施工预算
    /// </summary>
    public class Project_ConstructionBudgetQuantitiesMap : EntityTypeConfiguration<Project_ConstructionBudgetQuantitiesEntity>
    {
        public Project_ConstructionBudgetQuantitiesMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_CONSTRUCTIONBUDGETQUANTITIES");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

