﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 09:22
    /// 描 述：零星采购
    /// </summary>
    public class Project_SporadicPurchaseDetailsMap : EntityTypeConfiguration<Project_SporadicPurchaseDetailsEntity>
    {
        public Project_SporadicPurchaseDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_SPORADICPURCHASEDETAILS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

