﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 12:02
    /// 描 述：分包合同
    /// </summary>
    public class Project_SubcontractMap : EntityTypeConfiguration<Project_SubcontractEntity>
    {
        public Project_SubcontractMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_SUBCONTRACT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

