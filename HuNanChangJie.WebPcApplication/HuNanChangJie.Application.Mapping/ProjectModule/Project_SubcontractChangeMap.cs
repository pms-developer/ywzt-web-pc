﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-23 11:52
    /// 描 述：分包变更
    /// </summary>
    public class Project_SubcontractChangeMap : EntityTypeConfiguration<Project_SubcontractChangeEntity>
    {
        public Project_SubcontractChangeMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_SUBCONTRACTCHANGE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

