﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-30 15:02
    /// 描 述：工程部位
    /// </summary>
    public class Engineering_PartsMap : EntityTypeConfiguration<Engineering_PartsEntity>
    {
        public Engineering_PartsMap()
        {
            #region  表、主键
            //表
            this.ToTable("ENGINEERING_PARTS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

