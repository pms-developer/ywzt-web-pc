﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-05 15:08
    /// 描 述：项目类型
    /// </summary>
    public class Base_ProjectTypeMap : EntityTypeConfiguration<Base_ProjectTypeEntity>
    {
        public Base_ProjectTypeMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_PROJECTTYPE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

