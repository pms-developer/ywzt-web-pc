﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-01 15:02
    /// 描 述：简历处理
    /// </summary>
    public class zhibin_jianliMap : EntityTypeConfiguration<zhibin_jianliEntity>
    {
        public zhibin_jianliMap()
        {
            #region  表、主键
            //表
            this.ToTable("ZHIBIN_JIANLI");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

