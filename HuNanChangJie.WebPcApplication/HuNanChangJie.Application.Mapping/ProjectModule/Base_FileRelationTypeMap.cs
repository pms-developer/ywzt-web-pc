﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping
{
    public class Base_FileRelationTypeMap : EntityTypeConfiguration<Base_FileRelationTypeEntity>
    {
        public Base_FileRelationTypeMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_FILERELATIONTYPE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
