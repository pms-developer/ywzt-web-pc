﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-20 11:49
    /// 描 述：工程合同变更
    /// </summary>
    public class Project_ContractChangeMaterialsMap : EntityTypeConfiguration<Project_ContractChangeMaterialsEntity>
    {
        public Project_ContractChangeMaterialsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_CONTRACTCHANGEMATERIALS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

