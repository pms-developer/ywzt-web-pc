﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-27 16:01
    /// 描 述：开工报告
    /// </summary>
    public class Project_StartWorkMap : EntityTypeConfiguration<Project_StartWorkEntity>
    {
        public Project_StartWorkMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_STARTWORK");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

