﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-07 15:30
    /// 描 述：投标立项
    /// </summary>
    public class Project_BidMap : EntityTypeConfiguration<Project_BidEntity>
    {
        public Project_BidMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_BID");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

