﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-19 14:21
    /// 描 述：工程合同
    /// </summary>
    public class Project_ContractMaterialsMap : EntityTypeConfiguration<Project_ContractMaterialsEntity>
    {
        public Project_ContractMaterialsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_CONTRACTMATERIALS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

