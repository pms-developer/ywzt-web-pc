﻿using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 10:56
    /// 描 述：工程签证
    /// </summary>
    public class Project_VisaQuantitiesMap : EntityTypeConfiguration<Project_VisaQuantitiesEntity>
    {
        public Project_VisaQuantitiesMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_VISAQUANTITIES");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

