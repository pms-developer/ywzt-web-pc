﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-25 15:59
    /// 描 述：客户管理
    /// </summary>
    public class Base_CustomerMap : EntityTypeConfiguration<Base_CustomerEntity>
    {
        public Base_CustomerMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CUSTOMER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

