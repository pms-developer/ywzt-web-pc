﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-02 21:00
    /// 描 述：用章申请
    /// </summary>
    public class Base_CJ_YinZhangYongZhangMap : EntityTypeConfiguration<Base_CJ_YinZhangYongZhangEntity>
    {
        public Base_CJ_YinZhangYongZhangMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_YINZHANGYONGZHANG");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

