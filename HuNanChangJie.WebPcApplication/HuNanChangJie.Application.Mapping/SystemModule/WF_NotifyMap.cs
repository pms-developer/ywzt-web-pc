﻿using HuNanChangJie.Application.WorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-10-20 11:00
    /// 描 述：知会者
    /// </summary>
    public class WF_NotifyMap : EntityTypeConfiguration<WFNotifyEntity>
    {
        public WF_NotifyMap()
        {
            #region  表、主键
            //表
            this.ToTable("WF_NOTIFY");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

