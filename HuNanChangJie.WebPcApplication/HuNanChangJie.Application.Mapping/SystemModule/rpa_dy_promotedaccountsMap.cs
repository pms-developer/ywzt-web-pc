﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAJob;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 14:27
    /// 描 述：RPA作业
    /// </summary>
    public class rpa_dy_promotedaccountsMap : EntityTypeConfiguration<rpa_dy_promotedaccountsEntity>
    {
        public rpa_dy_promotedaccountsMap()
        {
            #region  表、主键
            //表
            this.ToTable("RPA_DY_PROMOTEDACCOUNTS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

