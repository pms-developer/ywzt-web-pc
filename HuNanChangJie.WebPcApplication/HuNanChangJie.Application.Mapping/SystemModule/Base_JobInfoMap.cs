﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.ScheduleJobModel;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-07 10:01
    /// 描 述：计划任务中心
    /// </summary>
    public class Base_JobInfoMap : EntityTypeConfiguration<JobInfo>
    {
        public Base_JobInfoMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_JOBINFO");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

