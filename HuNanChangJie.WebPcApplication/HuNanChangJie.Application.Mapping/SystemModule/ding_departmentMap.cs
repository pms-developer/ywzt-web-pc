﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-05 14:16
    /// 描 述：钉钉部门源数据
    /// </summary>
    public class ding_departmentMap : EntityTypeConfiguration<Ding_departmentEntity>
    {
        public ding_departmentMap()
        {
            #region  表、主键
            //表
            this.ToTable("DING_DEPARTMENT");
            //主键
            this.HasKey(t => t.Dept_id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

