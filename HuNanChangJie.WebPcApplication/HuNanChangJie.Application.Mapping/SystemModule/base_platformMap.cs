﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-13 13:55
    /// 描 述：电商平台
    /// </summary>
    public class base_platformMap : EntityTypeConfiguration<base_platformEntity>
    {
        public base_platformMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_PLATFORM");
            //主键
            this.HasKey(t => t.F_PlatformId);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

