﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-19 09:31
    /// 描 述：材料品牌
    /// </summary>
    public class Base_MaterialsBrandMap : EntityTypeConfiguration<Base_MaterialsBrandEntity>
    {
        public Base_MaterialsBrandMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_MATERIALSBRAND");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

