﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 21:04
    /// 描 述：RPAJobLog
    /// </summary>
    public class rpa_joblogMap : EntityTypeConfiguration<rpa_joblogEntity>
    {
        public rpa_joblogMap()
        {
            #region  表、主键
            //表
            this.ToTable("RPA_JOBLOG");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

