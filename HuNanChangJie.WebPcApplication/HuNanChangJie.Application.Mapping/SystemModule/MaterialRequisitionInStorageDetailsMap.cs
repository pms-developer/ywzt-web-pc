﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-22 15:43
    /// 描 述：MaterialRequisitionInStorageDetails
    /// </summary>
    public class MaterialRequisitionInStorageDetailsMap : EntityTypeConfiguration<MaterialRequisitionInStorageDetailsEntity>
    {
        public MaterialRequisitionInStorageDetailsMap() 
        {
            #region  表、主键
            //表
            this.ToTable("MATERIALREQUISITIONINSTORAGEDETAILS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

