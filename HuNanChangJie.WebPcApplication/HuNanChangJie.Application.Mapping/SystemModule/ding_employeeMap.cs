﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-06 18:49
    /// 描 述：钉钉员工信息
    /// </summary>
    public class ding_employeeMap : EntityTypeConfiguration<ding_employeeEntity>
    {
        public ding_employeeMap()
        {
            #region  表、主键
            //表
            this.ToTable("DING_EMPLOYEE");
            //主键
            this.HasKey(t => t.userid);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

