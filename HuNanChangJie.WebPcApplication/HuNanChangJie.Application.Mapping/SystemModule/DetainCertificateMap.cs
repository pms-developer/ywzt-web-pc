﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 09:28
    /// 描 述：一证多押
    /// </summary>
    public class DetainCertificateMap : EntityTypeConfiguration<DetainCertificateEntity>
    {
        public DetainCertificateMap()
        {
            #region  表、主键
            //表
            this.ToTable("DETAINCERTIFICATE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

