﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-02 20:10
    /// 描 述：借章管理
    /// </summary>
    public class Base_CJ_YinZhangJieYongMap : EntityTypeConfiguration<Base_CJ_YinZhangJieYongEntity>
    {
        public Base_CJ_YinZhangJieYongMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_YINZHANGJIEYONG");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

