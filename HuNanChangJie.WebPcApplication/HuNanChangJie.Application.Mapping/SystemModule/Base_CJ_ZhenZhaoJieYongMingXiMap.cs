﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-19 16:03
    /// 描 述：证书借用
    /// </summary>
    public class Base_CJ_ZhenZhaoJieYongMingXiMap : EntityTypeConfiguration<Base_CJ_ZhenZhaoJieYongMingXiEntity>
    {
        public Base_CJ_ZhenZhaoJieYongMingXiMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_ZHENZHAOJIEYONGMINGXI");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

