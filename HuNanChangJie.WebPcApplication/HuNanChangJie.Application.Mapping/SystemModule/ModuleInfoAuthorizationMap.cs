﻿using HuNanChangJie.Application.Base.ModuleInfoAuthorization;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.SystemModule
{
    public class ModuleInfoAuthorizationMap:EntityTypeConfiguration<ModuleInfoAuthorizationEntity>
    {
        public ModuleInfoAuthorizationMap()
        {
            base.ToTable("Base_ModuletInfoAuthorization");
            base.HasKey(i =>i.ID);
        }
    }
}
