﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-10-28 17:03
    /// 描 述：证照名称管理
    /// </summary>
    public class Base_CJ_ZhenZhaoMingChengMap : EntityTypeConfiguration<Base_CJ_ZhenZhaoMingChengEntity>
    {
        public Base_CJ_ZhenZhaoMingChengMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_ZHENZHAOMINGCHENG");
            //主键
            this.HasKey(t => t.zzmc_id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

