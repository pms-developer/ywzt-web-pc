﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-02 14:53
    /// 描 述：系统消息
    /// </summary>
    public class Base_MessageMap : EntityTypeConfiguration<Base_MessageEntity>
    {
        public Base_MessageMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_MESSAGE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

