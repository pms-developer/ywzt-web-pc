﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-13 20:34
    /// 描 述：证照管理
    /// </summary>
    public class Base_CJ_ZhengZhaoMap : EntityTypeConfiguration<Base_CJ_ZhengZhaoEntity>
    {
        public Base_CJ_ZhengZhaoMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_ZHENGZHAO");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

