﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule.Base_FormTabs;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.SystemModule
{
    public class Base_FormTabsMap : EntityTypeConfiguration<Base_FormTabsEntity>
    {
        public Base_FormTabsMap()
        {
            #region  表、主键
            //表
            this.ToTable("Base_FormTabs");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
