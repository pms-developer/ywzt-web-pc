﻿using HuNanChangJie.Application.Base.SystemModule.Folder;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.SystemModule
{
    public class FolderMap:EntityTypeConfiguration<FolderEntity>
    {
        public FolderMap()
        {
            ToTable("Base_Folder");
            HasKey(i => i.ModuleId);
        }
    }
}
