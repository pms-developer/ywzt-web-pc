﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-08-03 17:49
    /// 描 述：PrintTestSystemForm
    /// </summary>
    public class PrintTestSubAMap : EntityTypeConfiguration<PrintTestSubAEntity>
    {
        public PrintTestSubAMap()
        {
            #region  表、主键
            //表
            this.ToTable("PRINTTESTSUBA");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

