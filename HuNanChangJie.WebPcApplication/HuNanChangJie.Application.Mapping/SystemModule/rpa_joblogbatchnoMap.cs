﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2022-09-12 10:15 
    /// 描 述：RPAJobLogBatchNo 
    /// </summary> 
    public class rpa_joblogbatchnoMap : EntityTypeConfiguration<rpa_joblogbatchnoEntity>
    {
        public rpa_joblogbatchnoMap()
        {
            #region  表、主键 
            //表 
            this.ToTable("RPA_JOBLOGBATCHNO");
            //主键 
            this.HasKey(t => t.Id);
            #endregion

            #region  配置关系 
            #endregion
        }
    }
}