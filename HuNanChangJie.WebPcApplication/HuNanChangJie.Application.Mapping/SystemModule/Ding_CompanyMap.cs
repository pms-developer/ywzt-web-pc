﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-05 10:23
    /// 描 述：钉钉上子公司源数据
    /// </summary>
    public class Ding_CompanyMap : EntityTypeConfiguration<Ding_CompanyEntity>
    {
        public Ding_CompanyMap()
        {
            #region  表、主键
            //表
            this.ToTable("DING_COMPANY");
            //主键
            this.HasKey(t => t.Dept_id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

