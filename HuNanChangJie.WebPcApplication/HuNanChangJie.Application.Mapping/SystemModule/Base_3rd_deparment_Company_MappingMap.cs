﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-05 16:41
    /// 描 述：钉钉部门与公司的mapping关系
    /// </summary>
    public class Base_3rd_deparment_Company_MappingMap : EntityTypeConfiguration<base_3rd_deparment_company_mappingEntity>
    {
        public Base_3rd_deparment_Company_MappingMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_3RD_DEPARMENT_COMPANY_MAPPING");
            //主键
            this.HasKey(t => t.F_Deparment_Company_MappingId);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

