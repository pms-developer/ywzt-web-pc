﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-29 14:14
    /// 描 述：印章管理
    /// </summary>
    public class Base_CJ_YinZhangMap : EntityTypeConfiguration<Base_CJ_YinZhangEntity>
    {
        public Base_CJ_YinZhangMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_YINZHANG");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

