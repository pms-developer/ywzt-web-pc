﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-18 17:18
    /// 描 述：计量单位
    /// </summary>
    public class Base_MaterialsUnitMap : EntityTypeConfiguration<Base_MaterialsUnitEntity>
    {
        public Base_MaterialsUnitMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_MATERIALSUNIT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

