﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 15:19
    /// 描 述：取数公式设置
    /// </summary>
    public class Base_FormulaSettingsMap : EntityTypeConfiguration<Base_FormulaSettingsEntity>
    {
        public Base_FormulaSettingsMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_FORMULASETTINGS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

