﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-18 14:42
    /// 描 述：材料分类
    /// </summary>
    public class Base_MaterialsTypeMap : EntityTypeConfiguration<Base_MaterialsTypeEntity>
    {
        public Base_MaterialsTypeMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_MATERIALSTYPE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

