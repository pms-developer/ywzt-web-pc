﻿using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 15:55
    /// 描 述：合同类型
    /// </summary>
    public class Base_ContractTypeMap : EntityTypeConfiguration<Base_ContractTypeEntity>
    {
        public Base_ContractTypeMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CONTRACTTYPE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

