﻿using HuNanChangJie.Application.Organization._3Rd;
using System.Data.Entity.ModelConfiguration;


namespace HuNanChangJie.Application.Mapping.Organization
{
    class BaseAccessTokenMap : EntityTypeConfiguration<Base_AccessTokenEntity>
    {
        public BaseAccessTokenMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_ACCESSTOKEN");
            //主键
            this.HasKey(t => t.id);
            #endregion

            #region  配置关系
            #endregion
        }

    }
}