﻿using HuNanChangJie.Application.TwoDevelopment.PortalSite;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-29 15:23
    /// 描 述：子页面管理
    /// </summary>
    public class PS_PageMap : EntityTypeConfiguration<PS_PageEntity>
    {
        public PS_PageMap()
        {
            #region  表、主键
            //表
            this.ToTable("PS_PAGE");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

