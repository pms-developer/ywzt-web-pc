﻿using HuNanChangJie.Application.TwoDevelopment.PortalSite;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-29 15:12
    /// 描 述：首页配置
    /// </summary>
    public class PS_HomeConfigMap : EntityTypeConfiguration<PS_HomeConfigEntity>
    {
        public PS_HomeConfigMap()
        {
            #region  表、主键
            //表
            this.ToTable("PS_HOMECONFIG");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

