﻿using HuNanChangJie.Application.AppMagager;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.App
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2018-06-26 10:32 
    /// 描 述：测试 
    /// </summary> 
    public class MyFunctionMap : EntityTypeConfiguration<MyFunctionEntity>
    {
        public MyFunctionMap()
        {
            #region  表、主键 
            //表 
            this.ToTable("APP_MYFUNCTION");
            //主键 
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系 
            #endregion
        }
    }
}
