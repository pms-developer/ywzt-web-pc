﻿using HuNanChangJie.Workflow.Entity.TableMapping;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.WorkFlow
{
    public class SysWfNoticeMap:EntityTypeConfiguration<SysWfNotice>
    {
        public SysWfNoticeMap()
        {
            #region  表、主键
            //表
            this.ToTable("Sys_WfNotice");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
