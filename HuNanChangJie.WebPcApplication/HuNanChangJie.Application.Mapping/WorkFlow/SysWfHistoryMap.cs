﻿using HuNanChangJie.Workflow.Entity.TableMapping;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.WorkFlow
{
    public class SysWfHistoryMap:EntityTypeConfiguration<SysWfHistory>
    {
        public SysWfHistoryMap()
        {
            #region  表、主键
            //表
            this.ToTable("Sys_WfHistory");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
