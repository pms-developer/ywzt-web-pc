﻿using HuNanChangJie.Workflow.Entity.TableMapping;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.WorkFlow
{
    public class SysWFTaskMap : EntityTypeConfiguration<SysWFTask>
    {
        public SysWFTaskMap()
        {
            #region  表、主键
            //表
            this.ToTable("Sys_WFTask");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
