﻿using HuNanChangJie.Workflow.Entity.TableMapping;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.WorkFlow
{
    public class SysWorkflowRecordMap : EntityTypeConfiguration<SysWorkflowRecord>
    {
        public SysWorkflowRecordMap()
        {
            #region  表、主键
            //表
            this.ToTable("Sys_WorkflowRecord");
            //主键
            this.HasKey(t => t.Workflow_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
