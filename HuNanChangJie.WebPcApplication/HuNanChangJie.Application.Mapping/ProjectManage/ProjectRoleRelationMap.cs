﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage
{
    /// <summary>
    /// 项目、项目角色、用户表映射
    /// </summary>
    public class ProjectRoleRelationMap:EntityTypeConfiguration<ProjectRoleRelationEntity>
    {
        public ProjectRoleRelationMap()
        {
            ToTable("Base_CJ_ProjectRoleRelation");
            this.HasKey(i => i.ID);
        }
    }
}
