﻿using HuNanChangeJie.Application.Project.BaseInfo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage
{
    public class SubjectMap:EntityTypeConfiguration<SubjectEntity>
    {
        public SubjectMap()
        {
            ToTable("Base_Subject");
            HasKey(i => i.ID);
        }
    }
}
