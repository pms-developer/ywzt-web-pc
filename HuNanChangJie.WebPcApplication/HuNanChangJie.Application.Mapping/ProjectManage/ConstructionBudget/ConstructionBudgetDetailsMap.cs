﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage.ConstructionBudget
{
    public class ConstructionBudgetDetailsMap:EntityTypeConfiguration<ConstructionBudgetDetailsEntity>
    {
        public ConstructionBudgetDetailsMap()
        {
            ToTable("Project_ConstructionBudgetDetails");
            HasKey(i => i.ID);
        }
    }
}
