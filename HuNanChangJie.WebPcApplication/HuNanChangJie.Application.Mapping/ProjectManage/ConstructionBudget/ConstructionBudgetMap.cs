﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage.ConstructionBudget
{
    public class ConstructionBudgetMap:EntityTypeConfiguration<ConstructionBudgetEntity>
    {
        public ConstructionBudgetMap()
        {
            ToTable("Project_ConstructionBudget");
            HasKey(i => i.ID);
        }
    }
}
