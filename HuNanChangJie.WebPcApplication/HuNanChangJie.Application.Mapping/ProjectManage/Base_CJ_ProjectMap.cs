﻿using HuNanChangeJie.Application.Project.Model;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping.ProjectManage
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-04 15:18
    /// 描 述：项目基本信息
    /// </summary>
    public class ProjectMap : EntityTypeConfiguration<ProjectEntity>
    {
        public ProjectMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_PROJECT");
            //主键
            this.HasKey(t => t.ID);
            #endregion


            #region  配置关系
            #endregion
        }
    }
}

