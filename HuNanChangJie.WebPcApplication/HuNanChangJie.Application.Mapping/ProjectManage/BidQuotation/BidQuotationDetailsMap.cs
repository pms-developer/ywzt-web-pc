﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage.BidQuotation
{
    public class BidQuotationDetailsMap:EntityTypeConfiguration<BidQuotationDetailsEntity>
    {
        public BidQuotationDetailsMap()
        {
            ToTable("Project_BidQuotationDetails");
            HasKey(i => i.ID);
        }
    }
}
