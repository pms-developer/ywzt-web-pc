﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage.BidQuotation
{
    public class BidQuotationMap:EntityTypeConfiguration<BidQuotationEntity>
    {
        public BidQuotationMap()
        {
            ToTable("Project_BidQuotation");
            HasKey(i => i.ID);
        }
    }
}
