﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage.BidQuotation
{
    public class BidQuotationMaterialsMap:EntityTypeConfiguration<BidQuotationMaterialsEntity>
    {
        public BidQuotationMaterialsMap()
        {
            ToTable("Project_BidQuotationMaterials");
            HasKey(i => i.ID);
        }
    }
}
