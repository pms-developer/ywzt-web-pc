﻿using HuNanChangeJie.Application.Project.BaseInfo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage
{
    public class CostAttributeMap:EntityTypeConfiguration<CostAttributeEntity>
    {
        public CostAttributeMap()
        {
            ToTable("Base_CostAttribute");
            HasKey(i => i.ID);
        }
    }
}
