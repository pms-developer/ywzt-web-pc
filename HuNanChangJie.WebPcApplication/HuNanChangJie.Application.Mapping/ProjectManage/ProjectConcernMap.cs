﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage
{
    public class ProjectConcernMap:EntityTypeConfiguration<ProjectConcernEntity>
    {
        public ProjectConcernMap()
        {
            ToTable("Base_CJ_ProjectConcern");
            HasKey(i => i.ID);
        }
    }
}
