﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage.BidBudget
{
    public class BidBudgetMaterialsMap:EntityTypeConfiguration<BidBudgetMaterialsEntity>
    {
        public BidBudgetMaterialsMap()
        {
            ToTable("Project_BidBudgetMaterials");
            HasKey(i => i.ID);
        }
    }
}
