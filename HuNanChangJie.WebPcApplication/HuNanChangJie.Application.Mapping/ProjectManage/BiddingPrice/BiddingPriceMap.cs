﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage.BiddingPrice
{
    public class BiddingPriceMap:EntityTypeConfiguration<BiddingPriceEntity>
    {
        public BiddingPriceMap()
        {
            ToTable("Project_BiddingPrice");
            HasKey(i => i.ID);
        }
    }
}
