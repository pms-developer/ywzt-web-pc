﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage.BiddingPrice
{
    public class BiddingPriceMaterialsMap:EntityTypeConfiguration<BiddingPriceMaterialsEntity>
    {
        public BiddingPriceMaterialsMap()
        {
            ToTable("Project_BiddingPriceMaterials");
            HasKey(i => i.ID);
        }
    }
}
