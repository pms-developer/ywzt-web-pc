﻿using HuNanChangeJie.Application.Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.ProjectManage.BiddingPrice
{
    public class BiddingPriceDetailsMap:EntityTypeConfiguration<BiddingPriceDetailsEntity>
    {
        public BiddingPriceDetailsMap()
        {
            ToTable("Project_BiddingPriceDetails");
            HasKey(i=>i.ID);
        }
    }
}
