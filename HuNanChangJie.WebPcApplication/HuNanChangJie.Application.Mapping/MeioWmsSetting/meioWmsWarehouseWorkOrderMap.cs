﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-30 16:19
    /// 描 述：工作单
    /// </summary>
    public class meioWmsWarehouseWorkOrderMap : EntityTypeConfiguration<meioWmsWarehouseWorkOrderEntity>
    {
        public meioWmsWarehouseWorkOrderMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSEWORKORDER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

