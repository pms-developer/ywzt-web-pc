﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 10:41
    /// 描 述：库存
    /// </summary>
    public class meioWmsWarehouseInventoryReceiptMap : EntityTypeConfiguration<meioWmsWarehouseInventoryReceiptEntity>
    {
        public meioWmsWarehouseInventoryReceiptMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSEINVENTORYRECEIPT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

