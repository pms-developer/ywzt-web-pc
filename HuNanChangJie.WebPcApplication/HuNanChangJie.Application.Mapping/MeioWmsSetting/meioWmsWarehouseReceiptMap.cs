﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-26 14:52
    /// 描 述：入库单
    /// </summary>
    public class meioWmsWarehouseReceiptMap : EntityTypeConfiguration<meioWmsWarehouseReceiptEntity>
    {
        public meioWmsWarehouseReceiptMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSERECEIPT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

