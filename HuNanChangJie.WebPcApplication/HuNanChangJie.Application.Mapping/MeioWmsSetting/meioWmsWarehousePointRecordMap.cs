﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 11:50
    /// 描 述：上架定位
    /// </summary>
    public class meioWmsWarehousePointRecordMap : EntityTypeConfiguration<meioWmsWarehousePointRecordEntity>
    {
        public meioWmsWarehousePointRecordMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSEPOINTRECORD");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

