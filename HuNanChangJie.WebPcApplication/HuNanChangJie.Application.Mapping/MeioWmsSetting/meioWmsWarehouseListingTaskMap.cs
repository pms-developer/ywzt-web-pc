﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 10:15
    /// 描 述：上架任务
    /// </summary>
    public class meioWmsWarehouseListingTaskMap : EntityTypeConfiguration<meioWmsWarehouseListingTaskEntity>
    {
        public meioWmsWarehouseListingTaskMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSELISTINGTASK");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

