﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-18 09:14
    /// 描 述：仓库配置-货架
    /// </summary>
    public class meioWmsShelvesMap : EntityTypeConfiguration<meioWmsShelvesEntity>
    {
        public meioWmsShelvesMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSSHELVES");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

