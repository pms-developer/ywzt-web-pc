﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-19 15:27
    /// 描 述：用户货主关系
    /// </summary>
    public class meioWmsUserShipperMap : EntityTypeConfiguration<meioWmsUserShipperEntity>
    {
        public meioWmsUserShipperMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSUSERSHIPPER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

