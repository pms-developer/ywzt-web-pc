﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 09:47
    /// 描 述：定位货位范围
    /// </summary>
    public class meioWmsWarehousePointStorageLocationRangeItemMap : EntityTypeConfiguration<meioWmsWarehousePointStorageLocationRangeItemEntity>
    {
        public meioWmsWarehousePointStorageLocationRangeItemMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSEPOINTSTORAGELOCATIONRANGEITEM");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

