﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 09:58
    /// 描 述：上架定位规则
    /// </summary>
    public class meioWmsWarehousePointListingRuleMap : EntityTypeConfiguration<meioWmsWarehousePointListingRuleEntity>
    {
        public meioWmsWarehousePointListingRuleMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSEPOINTLISTINGRULE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

