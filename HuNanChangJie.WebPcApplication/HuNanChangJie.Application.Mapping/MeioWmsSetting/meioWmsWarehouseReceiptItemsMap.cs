﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-26 17:22
    /// 描 述：入库单服务项目
    /// </summary>
    public class meioWmsWarehouseReceiptItemsMap : EntityTypeConfiguration<meioWmsWarehouseReceiptItemsEntity>
    {
        public meioWmsWarehouseReceiptItemsMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSERECEIPTITEMS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

