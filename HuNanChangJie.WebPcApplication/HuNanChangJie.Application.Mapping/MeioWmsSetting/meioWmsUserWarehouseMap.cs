﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-19 15:26
    /// 描 述：用户仓库关系
    /// </summary>
    public class meioWmsUserWarehouseMap : EntityTypeConfiguration<meioWmsUserWarehouseEntity>
    {
        public meioWmsUserWarehouseMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSUSERWAREHOUSE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

