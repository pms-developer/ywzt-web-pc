﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-18 09:54
    /// 描 述：仓库配置-货位
    /// </summary>
    public class meioWmsStorageLocationMap : EntityTypeConfiguration<meioWmsStorageLocationEntity>
    {
        public meioWmsStorageLocationMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSSTORAGELOCATION");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

