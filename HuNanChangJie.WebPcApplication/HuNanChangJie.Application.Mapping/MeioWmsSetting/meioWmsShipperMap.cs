﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-18 10:46
    /// 描 述：货主信息
    /// </summary>
    public class meioWmsShipperMap : EntityTypeConfiguration<meioWmsShipperEntity>
    {
        public meioWmsShipperMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSSHIPPER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

