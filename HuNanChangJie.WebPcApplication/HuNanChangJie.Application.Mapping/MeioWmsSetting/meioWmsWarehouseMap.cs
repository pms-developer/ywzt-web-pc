﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-19 10:50
    /// 描 述：仓库
    /// </summary>
    public class meioWmsWarehouseMap : EntityTypeConfiguration<meioWmsWarehouseEntity>
    {
        public meioWmsWarehouseMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

