﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-22 17:49
    /// 描 述：货品列表
    /// </summary>
    public class meioWmsGoodsMap : EntityTypeConfiguration<meioWmsGoodsEntity>
    {
        public meioWmsGoodsMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSGOODS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

