﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-13 00:28
    /// 描 述：垫资退款管理
    /// </summary>
    public class Base_CJ_DianZiHuanMap : EntityTypeConfiguration<Base_CJ_DianZiHuanEntity>
    {
        public Base_CJ_DianZiHuanMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_DIANZIHUAN");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

