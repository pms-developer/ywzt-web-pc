﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-05 14:36
    /// 描 述：资金帐户明细
    /// </summary>
    public class Base_FundAccoutDetailsMap : EntityTypeConfiguration<Base_FundAccoutDetailsEntity>
    {
        public Base_FundAccoutDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_FUNDACCOUTDETAILS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

