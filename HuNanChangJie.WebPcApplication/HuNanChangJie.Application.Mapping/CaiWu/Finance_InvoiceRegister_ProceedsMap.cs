﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-29 09:58
    /// 描 述：开票登记
    /// </summary>
    public class Finance_InvoiceRegister_ProceedsMap : EntityTypeConfiguration<Finance_InvoiceRegister_ProceedsEntity>
    {
        public Finance_InvoiceRegister_ProceedsMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_INVOICEREGISTER_PROCEEDS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

