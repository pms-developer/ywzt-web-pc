﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-21 15:38
    /// 描 述：还款单
    /// </summary>
    public class Finance_RepaymentMap : EntityTypeConfiguration<Finance_RepaymentEntity>
    {
        public Finance_RepaymentMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_REPAYMENT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

