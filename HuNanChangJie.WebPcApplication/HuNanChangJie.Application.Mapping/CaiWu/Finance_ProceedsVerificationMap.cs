﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 17:44
    /// 描 述：收款单管理
    /// </summary>
    public class Finance_ProceedsVerificationMap : EntityTypeConfiguration<Finance_ProceedsVerificationEntity>
    {
        public Finance_ProceedsVerificationMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_PROCEEDSVERIFICATION");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

