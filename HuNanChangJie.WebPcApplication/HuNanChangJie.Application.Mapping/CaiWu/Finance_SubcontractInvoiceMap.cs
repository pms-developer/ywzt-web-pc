﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-23 12:14
    /// 描 述：财务 分包收票
    /// </summary>
    public class Finance_SubcontractInvoiceMap : EntityTypeConfiguration<Finance_SubcontractInvoiceEntity>
    {
        public Finance_SubcontractInvoiceMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_SUBCONTRACTINVOICE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

