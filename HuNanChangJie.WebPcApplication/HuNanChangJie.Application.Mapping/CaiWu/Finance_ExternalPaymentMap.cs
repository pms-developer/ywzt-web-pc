﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-05 19:09
    /// 描 述：财务-对外付款单
    /// </summary>
    public class Finance_ExternalPaymentMap : EntityTypeConfiguration<Finance_ExternalPaymentEntity>
    {
        public Finance_ExternalPaymentMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_EXTERNALPAYMENT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

