﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-05 14:19
    /// 描 述：保证金支付管理
    /// </summary>
    public class Base_CJ_BaoZhengJinZhiFuMap : EntityTypeConfiguration<Base_CJ_BaoZhengJinZhiFuEntity>
    {
        public Base_CJ_BaoZhengJinZhiFuMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_BAOZHENGJINZHIFU");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

