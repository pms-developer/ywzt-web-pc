﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-02 18:14
    /// 描 述：资金帐户
    /// </summary>
    public class Base_FundAccountMap : EntityTypeConfiguration<Base_FundAccountEntity>
    {
        public Base_FundAccountMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_FUNDACCOUNT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

