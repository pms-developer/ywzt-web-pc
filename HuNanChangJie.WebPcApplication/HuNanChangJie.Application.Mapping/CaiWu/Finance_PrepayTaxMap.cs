﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 20:20
    /// 描 述：开票预缴
    /// </summary>
    public class Finance_PrepayTaxMap : EntityTypeConfiguration<Finance_PrepayTaxEntity>
    {
        public Finance_PrepayTaxMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_PREPAYTAX");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

