﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-12 23:52
    /// 描 述：垫资管理
    /// </summary>
    public class Base_CJ_DianZiMap : EntityTypeConfiguration<Base_CJ_DianZiEntity>
    {
        public Base_CJ_DianZiMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_DIANZI");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

