﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-02 20:14
    /// 描 述：内部转账
    /// </summary>
    public class Finance_InternalTransferMap : EntityTypeConfiguration<Finance_InternalTransferEntity>
    {
        public Finance_InternalTransferMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_INTERNALTRANSFER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

