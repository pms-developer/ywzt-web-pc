﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-12-14 11:43
    /// 描 述：税费核算
    /// </summary>
    public class AccountingTnvoiceDetailMap : EntityTypeConfiguration<AccountingTnvoiceDetailEntity>
    {
        public AccountingTnvoiceDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("ACCOUNTINGTNVOICEDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

