﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-20 17:13
    /// 描 述：借款申请
    /// </summary>
    public class Finance_LoanMap : EntityTypeConfiguration<Finance_LoanEntity>
    {
        public Finance_LoanMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_LOAN");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

