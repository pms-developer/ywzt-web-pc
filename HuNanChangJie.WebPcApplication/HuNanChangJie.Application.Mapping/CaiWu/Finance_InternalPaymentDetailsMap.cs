﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 16:04
    /// 描 述：对内付款单
    /// </summary>
    public class Finance_InternalPaymentDetailsMap : EntityTypeConfiguration<Finance_InternalPaymentDetailsEntity>
    {
        public Finance_InternalPaymentDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_INTERNALPAYMENTDETAILS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

