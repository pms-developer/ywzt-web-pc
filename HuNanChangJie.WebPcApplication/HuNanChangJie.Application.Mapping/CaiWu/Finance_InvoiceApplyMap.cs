﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-27 20:15
    /// 描 述：财务 开票申请
    /// </summary>
    public class Finance_InvoiceApplyMap : EntityTypeConfiguration<Finance_InvoiceApplyEntity>
    {
        public Finance_InvoiceApplyMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_INVOICEAPPLY");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

