﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-27 16:34
    /// 描 述：保证金管理
    /// </summary>
    public class Base_CJ_BaoZhengJinMap : EntityTypeConfiguration<Base_CJ_BaoZhengJinEntity>
    {
        public Base_CJ_BaoZhengJinMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_BAOZHENGJIN");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

