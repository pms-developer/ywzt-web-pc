﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-16 18:32
    /// 描 述：其它付款
    /// </summary>
    public class Finance_OtherPaymentMap : EntityTypeConfiguration<Finance_OtherPaymentEntity>
    {
        public Finance_OtherPaymentMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_OTHERPAYMENT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

