﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-23 12:14
    /// 描 述：财务 分包收票
    /// </summary>
    public class Finance_SubcontractInvoice_CertificateMap : EntityTypeConfiguration<Finance_SubcontractInvoice_CertificateEntity>
    {
        public Finance_SubcontractInvoice_CertificateMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_SUBCONTRACTINVOICE_CERTIFICATE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

