﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-19 18:38
    /// 描 述：系统开账
    /// </summary>
    public class Base_CJ_KaiZhangMap : EntityTypeConfiguration<Base_CJ_KaiZhangEntity>
    {
        public Base_CJ_KaiZhangMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CJ_KAIZHANG");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

