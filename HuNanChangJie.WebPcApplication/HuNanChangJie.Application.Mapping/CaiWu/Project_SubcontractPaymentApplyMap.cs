﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 16:21
    /// 描 述：分包付款申请
    /// </summary>
    public class Project_SubcontractPaymentApplyMap : EntityTypeConfiguration<Project_SubcontractPaymentApplyEntity>
    {
        public Project_SubcontractPaymentApplyMap()
        {
            #region  表、主键
            //表
            this.ToTable("PROJECT_SUBCONTRACTPAYMENTAPPLY");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

