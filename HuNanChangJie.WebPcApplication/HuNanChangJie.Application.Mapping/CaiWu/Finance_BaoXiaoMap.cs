﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-07 10:21
    /// 描 述：报销单
    /// </summary>
    public class Finance_BaoXiaoMap : EntityTypeConfiguration<Finance_BaoXiaoEntity>
    {
        public Finance_BaoXiaoMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_BAOXIAO");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

