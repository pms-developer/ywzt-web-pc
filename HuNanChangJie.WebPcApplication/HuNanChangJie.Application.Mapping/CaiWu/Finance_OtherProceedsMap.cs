﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-16 17:48
    /// 描 述：其它收款
    /// </summary>
    public class Finance_OtherProceedsMap : EntityTypeConfiguration<Finance_OtherProceedsEntity>
    {
        public Finance_OtherProceedsMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_OTHERPROCEEDS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

