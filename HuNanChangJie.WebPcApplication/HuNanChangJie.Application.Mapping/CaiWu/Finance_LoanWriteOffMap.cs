﻿using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-06-29 18:37
    /// 描 述：借款冲销
    /// </summary>
    public class Finance_LoanWriteOffMap : EntityTypeConfiguration<Finance_LoanWriteOffEntity>
    {
        public Finance_LoanWriteOffMap()
        {
            #region  表、主键
            //表
            this.ToTable("FINANCE_LOANWRITEOFF");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

