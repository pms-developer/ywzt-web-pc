﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 14:19
    /// 描 述：采购入库
    /// </summary>
    public class Purchase_InStorageMap : EntityTypeConfiguration<Purchase_InStorageEntity>
    {
        public Purchase_InStorageMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_INSTORAGE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

