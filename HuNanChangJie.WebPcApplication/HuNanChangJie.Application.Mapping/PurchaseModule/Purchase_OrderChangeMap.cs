﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-15 17:08
    /// 描 述：采购订单变更
    /// </summary>
    public class Purchase_OrderChangeMap : EntityTypeConfiguration<Purchase_OrderChangeEntity>
    {
        public Purchase_OrderChangeMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_ORDERCHANGE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

