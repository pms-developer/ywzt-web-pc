﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-17 12:04
    /// 描 述：采购合同增补
    /// </summary>
    public class Purchase_ContractSupplementMap : EntityTypeConfiguration<Purchase_ContractSupplementEntity>
    {
        public Purchase_ContractSupplementMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_CONTRACTSUPPLEMENT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

