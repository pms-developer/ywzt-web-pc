﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 19:12
    /// 描 述：采购合同
    /// </summary>
    public class Purchase_ContractMap : EntityTypeConfiguration<Purchase_ContractEntity>
    {
        public Purchase_ContractMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_CONTRACT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

