﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 20:25
    /// 描 述：采购合同结算
    /// </summary>
    public class Purchase_ContractSettlementDetailsMap : EntityTypeConfiguration<Purchase_ContractSettlementDetailsEntity>
    {
        public Purchase_ContractSettlementDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_CONTRACTSETTLEMENTDETAILS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

