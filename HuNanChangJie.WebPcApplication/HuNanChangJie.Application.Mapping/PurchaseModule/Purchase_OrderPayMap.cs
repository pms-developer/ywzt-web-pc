﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-06 10:09
    /// 描 述：采购订单付款申请
    /// </summary>
    public class Purchase_OrderPayMap : EntityTypeConfiguration<Purchase_OrderPayEntity>
    {
        public Purchase_OrderPayMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_ORDERPAY");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

