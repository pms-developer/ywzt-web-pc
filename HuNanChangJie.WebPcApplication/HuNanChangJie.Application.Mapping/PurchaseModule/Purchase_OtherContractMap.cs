﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-16 15:02
    /// 描 述：其他采购合同
    /// </summary>
    public class Purchase_OtherContractMap : EntityTypeConfiguration<Purchase_OtherContractEntity>
    {
        public Purchase_OtherContractMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_OTHERCONTRACT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

