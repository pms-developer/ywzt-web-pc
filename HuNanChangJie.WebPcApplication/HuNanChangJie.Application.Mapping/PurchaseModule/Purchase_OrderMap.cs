﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 11:09
    /// 描 述：采购订单
    /// </summary>
    public class Purchase_OrderMap : EntityTypeConfiguration<Purchase_OrderEntity>
    {
        public Purchase_OrderMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_ORDER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

