﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-19 09:55
    /// 描 述：现场收货单
    /// </summary>
    public class Storage_LocaleMatesReceiptMap : EntityTypeConfiguration<Storage_LocaleMatesReceiptEntity>
    {
        public Storage_LocaleMatesReceiptMap()
        {
            #region  表、主键
            //表
            this.ToTable("STORAGE_LOCALEMATESRECEIPT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

