﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 11:09
    /// 描 述：采购订单
    /// </summary>
    public class Purchase_OrderDetailsMap : EntityTypeConfiguration<Purchase_OrderDetailsEntity>
    {
        public Purchase_OrderDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_ORDERDETAILS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

