﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-03 15:51
    /// 描 述：采购发票
    /// </summary>
    public class Purchase_InvoiceMap : EntityTypeConfiguration<Purchase_InvoiceEntity>
    {
        public Purchase_InvoiceMap()
        {
            #region  表、主键
            //表
            this.ToTable("PURCHASE_INVOICE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

