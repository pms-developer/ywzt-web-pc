﻿using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.PurchaseModule
{
    public class Base_MaterialsBuyRecordMap : EntityTypeConfiguration<Base_MaterialsBuyRecordEntity>
    {
        public Base_MaterialsBuyRecordMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_MATERIALSBUYRECORD");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
