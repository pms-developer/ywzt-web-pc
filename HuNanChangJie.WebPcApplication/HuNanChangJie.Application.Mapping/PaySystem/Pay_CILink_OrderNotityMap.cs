﻿using HuNanChangJie.Application.TwoDevelopment.PaySystem;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-17 16:30
    /// 描 述：通知类
    /// </summary>
    public class Pay_CILink_OrderNotityMap : EntityTypeConfiguration<Pay_CILink_OrderNotityEntity>
    {
        public Pay_CILink_OrderNotityMap()
        {
            #region  表、主键
            //表
            this.ToTable("PAY_CILINK_ORDERNOTITY");
            //主键
            this.HasKey(t => t.F_ONID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

