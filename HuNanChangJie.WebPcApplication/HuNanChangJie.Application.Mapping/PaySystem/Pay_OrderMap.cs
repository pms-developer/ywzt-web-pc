﻿using HuNanChangJie.Application.TwoDevelopment.PaySystem;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-11 10:07
    /// 描 述：发票
    /// </summary>
    public class tPay_OrderMap : EntityTypeConfiguration<Pay_OrderEntity>
    {
        public tPay_OrderMap()
        {
            #region  表、主键
            //表
            this.ToTable("TPAY_ORDER");
            //主键
            this.HasKey(t => t.F_OrderID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

