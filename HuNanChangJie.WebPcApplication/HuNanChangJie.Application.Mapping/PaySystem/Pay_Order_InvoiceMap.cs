﻿using HuNanChangJie.Application.TwoDevelopment.PaySystem;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-11 10:07
    /// 描 述：发票
    /// </summary>
    public class tPay_Order_InvoiceMap : EntityTypeConfiguration<Pay_Order_InvoiceEntity>
    {
        public tPay_Order_InvoiceMap()
        {
            #region  表、主键
            //表
            this.ToTable("TPAY_ORDER_INVOICE");
            //主键
            this.HasKey(t => t.F_InvId);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

