﻿using HuNanChangJie.Application.TwoDevelopment.PaySystem;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-17 11:25
    /// 描 述：退款管理
    /// </summary>
    public class tPay_Refund_ReturnMap : EntityTypeConfiguration<Pay_Refund_ReturnEntity>
    {
        public tPay_Refund_ReturnMap()
        {
            #region  表、主键
            //表
            this.ToTable("TPAY_REFUND_RETURN");
            //主键
            this.HasKey(t => t.F_RefundId);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

