﻿using HuNanChangeJie.Application.SystemForm.Model;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.SystemForm
{
    public class SysFormWFDetaisMap:EntityTypeConfiguration<SysFormWFDetaisEntity>
    {
        public SysFormWFDetaisMap()
        {
            this.ToTable("Base_SysFormWFDetais");
            this.HasKey(t=>t.ID);
        }
    }
}
