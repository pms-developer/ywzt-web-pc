﻿using HuNanChangeJie.Application.SystemForm.Model;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.SystemForm
{
    public class SystemFormMap :EntityTypeConfiguration<SystemFormEntity>
    {
        public SystemFormMap()
        {
            this.ToTable("Base_SystemForm");
            this.HasKey(t => t.ID);
        }
    }
}
