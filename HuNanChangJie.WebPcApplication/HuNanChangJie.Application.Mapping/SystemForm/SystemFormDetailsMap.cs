﻿using HuNanChangeJie.Application.SystemForm.Model;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.SystemForm
{
    public class SystemFormDetailsMap:EntityTypeConfiguration<SystemFormDetailsEntity>
    {
        public SystemFormDetailsMap()
        {
            this.ToTable("Base_SystemFormDetails");
            this.HasKey(t => t.ID);
        }
    }
}
