﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-06 18:52
    /// 描 述：WMS退货单信息
    /// </summary>
    public class MeioErpWmsReturnOrderMap : EntityTypeConfiguration<MeioErpWmsReturnOrderEntity>
    {
        public MeioErpWmsReturnOrderMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWMSRETURNORDER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

