﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-07 14:39
    /// 描 述：账单附加费用明细
    /// </summary>
    public class MeioErpAccountAdditionalCostMap : EntityTypeConfiguration<MeioErpAccountAdditionalCostEntity>
    {
        public MeioErpAccountAdditionalCostMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPACCOUNTADDITIONALCOST");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

