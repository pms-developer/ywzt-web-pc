﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo.Owner;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.BaseInfo
{
    /// <summary>
    /// 货主仓库关系
    /// </summary>
    public class MeioErpOwnerWarehouseRelationMap : EntityTypeConfiguration<MeioErpOwnerWarehouseRelationEntity>
    {
        public MeioErpOwnerWarehouseRelationMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPOWNERWAREHOUSERELATION");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
