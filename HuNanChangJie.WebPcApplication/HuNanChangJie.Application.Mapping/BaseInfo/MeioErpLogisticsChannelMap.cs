﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-08 14:02
    /// 描 述：物流渠道维护
    /// </summary>
    public class MeioErpLogisticsChannelMap : EntityTypeConfiguration<MeioErpLogisticsChannelEntity>
    {
        public MeioErpLogisticsChannelMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPLOGISTICSCHANNEL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

