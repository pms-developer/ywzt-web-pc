﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 21:04
    /// 描 述：仓储费账单明细
    /// </summary>
    public class MeioErpWarehouseRentFeeAccountDetailMap : EntityTypeConfiguration<MeioErpWarehouseRentFeeAccountDetailEntity>
    {
        public MeioErpWarehouseRentFeeAccountDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWAREHOUSERENTFEEACCOUNTDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

