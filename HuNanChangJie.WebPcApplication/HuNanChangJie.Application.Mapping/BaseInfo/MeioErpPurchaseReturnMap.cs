﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-05 15:34
    /// 描 述：采购退货
    /// </summary>
    public class MeioErpPurchaseReturnMap : EntityTypeConfiguration<MeioErpPurchaseReturnEntity>
    {
        public MeioErpPurchaseReturnMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPPURCHASERETURN");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

