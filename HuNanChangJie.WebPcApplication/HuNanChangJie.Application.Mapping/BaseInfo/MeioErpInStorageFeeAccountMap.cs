﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-12 13:41
    /// 描 述：入库费账单
    /// </summary>
    public class MeioErpInStorageFeeAccountMap : EntityTypeConfiguration<MeioErpInStorageFeeAccountEntity>
    {
        public MeioErpInStorageFeeAccountMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPINSTORAGEFEEACCOUNT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

