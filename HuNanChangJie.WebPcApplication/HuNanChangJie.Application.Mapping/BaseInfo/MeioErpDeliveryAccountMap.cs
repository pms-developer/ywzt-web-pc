﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:16
    /// 描 述：出库账单信息
    /// </summary>
    public class MeioErpDeliveryAccountMap : EntityTypeConfiguration<MeioErpDeliveryAccountEntity>
    {
        public MeioErpDeliveryAccountMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPDELIVERYACCOUNT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

