﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-12-20 14:01
    /// 描 述：货主销售平台信息
    /// </summary>
    public class MeioErpOwnerSalesPlatformMap : EntityTypeConfiguration<MeioErpOwnerSalesPlatformEntity>
    {
        public MeioErpOwnerSalesPlatformMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPOWNERSALESPLATFORM");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

