﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-10 16:06
    /// 描 述：报价单管理
    /// </summary>
    public class MeioErpPriceSheetDetailMap : EntityTypeConfiguration<MeioErpPriceSheetDetailEntity>
    {
        public MeioErpPriceSheetDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPPRICESHEETDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

