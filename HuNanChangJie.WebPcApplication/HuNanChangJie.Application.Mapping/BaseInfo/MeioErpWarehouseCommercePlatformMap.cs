﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-10 18:06
    /// 描 述：电商平台
    /// </summary>
    public class MeioErpWarehouseCommercePlatformMap : EntityTypeConfiguration<MeioErpWarehouseCommercePlatformEntity>
    {
        public MeioErpWarehouseCommercePlatformMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWAREHOUSECOMMERCEPLATFORM");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

