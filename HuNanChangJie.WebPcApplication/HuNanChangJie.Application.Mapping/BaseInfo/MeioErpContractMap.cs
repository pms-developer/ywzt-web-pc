﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-15 15:22
    /// 描 述：货主管理
    /// </summary>
    public class MeioErpContractMap : EntityTypeConfiguration<MeioErpContractEntity>
    {
        public MeioErpContractMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPCONTRACT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

