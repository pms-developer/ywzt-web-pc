﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 14:47
    /// 描 述：领星发货单计费项明细
    /// </summary>
    public class meioWMSWarehouseDeliveryCostItemDetailMap : EntityTypeConfiguration<meioWMSWarehouseDeliveryCostItemDetailEntity>
    {
        public meioWMSWarehouseDeliveryCostItemDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSEDELIVERYCOSTITEMDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

