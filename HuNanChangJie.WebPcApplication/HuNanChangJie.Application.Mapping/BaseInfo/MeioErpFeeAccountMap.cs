﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-14 18:07
    /// 描 述：账单信息
    /// </summary>
    public class MeioErpFeeAccountMap : EntityTypeConfiguration<MeioErpFeeAccountEntity>
    {
        public MeioErpFeeAccountMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPFEEACCOUNT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

