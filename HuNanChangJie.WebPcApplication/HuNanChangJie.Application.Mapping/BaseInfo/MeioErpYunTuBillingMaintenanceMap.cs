﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-18 11:47
    /// 描 述：云途账单维护
    /// </summary>
    public class MeioErpYunTuBillingMaintenanceMap : EntityTypeConfiguration<MeioErpYunTuBillingMaintenanceEntity>
    {
        public MeioErpYunTuBillingMaintenanceMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPYUNTUBILLINGMAINTENANCE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

