﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-26 13:59
    /// 描 述：操作日志
    /// </summary>
    public class MeioErpOperationLogMap : EntityTypeConfiguration<MeioErpOperationLogEntity>
    {
        public MeioErpOperationLogMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPOPERATIONLOG");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

