﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 16:48
    /// 描 述：包材账单信息
    /// </summary>
    public class MeioErpPackingMaterialsFeeAccountMap : EntityTypeConfiguration<MeioErpPackingMaterialsFeeAccountEntity>
    {
        public MeioErpPackingMaterialsFeeAccountMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPPACKINGMATERIALSFEEACCOUNT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

