﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-12 19:23
    /// 描 述：出库费账单
    /// </summary>
    public class MeioErpDeliveryFeeAccountMap : EntityTypeConfiguration<MeioErpDeliveryFeeAccountEntity>
    {
        public MeioErpDeliveryFeeAccountMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPDELIVERYFEEACCOUNT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

