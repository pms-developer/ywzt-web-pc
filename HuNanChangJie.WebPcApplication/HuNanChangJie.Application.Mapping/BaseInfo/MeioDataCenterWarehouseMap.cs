﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-05 18:27
    /// 描 述：数据中台同步仓库信息
    /// </summary>
    public class MeioDataCenterWarehouseMap : EntityTypeConfiguration<MeioDataCenterWarehouseEntity>
    {
        public MeioDataCenterWarehouseMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIODATACENTERWAREHOUSE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

