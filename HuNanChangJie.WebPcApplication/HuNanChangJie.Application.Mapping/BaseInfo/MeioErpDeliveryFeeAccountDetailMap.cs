﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-12 19:25
    /// 描 述：出库账单明细信息
    /// </summary>
    public class MeioErpDeliveryFeeAccountDetailMap : EntityTypeConfiguration<MeioErpDeliveryFeeAccountDetailEntity>
    {
        public MeioErpDeliveryFeeAccountDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPDELIVERYFEEACCOUNTDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

