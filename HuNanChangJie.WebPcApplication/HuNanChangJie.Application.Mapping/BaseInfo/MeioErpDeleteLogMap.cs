﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-27 20:56
    /// 描 述：删除表记录
    /// </summary>
    public class MeioErpDeleteLogMap : EntityTypeConfiguration<MeioErpDeleteLogEntity>
    {
        public MeioErpDeleteLogMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPDELETELOG");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

