﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-06-28 16:38
    /// 描 述：供应商信息
    /// </summary>
    public class MeioErpSupplierMap : EntityTypeConfiguration<MeioErpSupplierEntity>
    {
        public MeioErpSupplierMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPSUPPLIER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

