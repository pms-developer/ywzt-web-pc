﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 19:59
    /// 描 述：领星库龄信息
    /// </summary>
    public class MeioDataCenterStockAgeMap : EntityTypeConfiguration<MeioDataCenterStockAgeEntity>
    {
        public MeioDataCenterStockAgeMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIODATACENTERSTOCKAGE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

