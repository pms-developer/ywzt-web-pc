﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-18 17:06
    /// 描 述：云途账单明细VAT记录信息
    /// </summary>
    public class MeioErpYunTuBillDetailVatRecordsMap : EntityTypeConfiguration<MeioErpYunTuBillDetailVatRecordsEntity>
    {
        public MeioErpYunTuBillDetailVatRecordsMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPYUNTUBILLDETAILVATRECORDS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

