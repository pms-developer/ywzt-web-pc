﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 17:11
    /// 描 述：库存管理
    /// </summary>
    public class MeioErpInventoryMap : EntityTypeConfiguration<MeioErpInventoryEntity>
    {
        public MeioErpInventoryMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPINVENTORY");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

