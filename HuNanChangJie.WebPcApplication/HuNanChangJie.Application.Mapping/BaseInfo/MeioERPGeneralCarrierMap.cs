﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-02 16:39
    /// 描 述：承运商信息
    /// </summary>
    public class MeioERPGeneralCarrierMap : EntityTypeConfiguration<MeioERPGeneralCarrierEntity>
    {
        public MeioERPGeneralCarrierMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPGENERALCARRIER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

