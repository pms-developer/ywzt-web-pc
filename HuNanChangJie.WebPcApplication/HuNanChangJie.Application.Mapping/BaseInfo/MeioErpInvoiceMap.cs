﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-25 17:29
    /// 描 述：发票信息
    /// </summary>
    public class MeioErpInvoiceMap : EntityTypeConfiguration<MeioErpInvoiceEntity>
    {
        public MeioErpInvoiceMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPINVOICE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

