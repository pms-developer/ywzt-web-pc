﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-12 14:39
    /// 描 述：WMS回传中台包材采购入库单
    /// </summary>
    public class MeioErpPurchaseWmsInStorageMap : EntityTypeConfiguration<MeioErpPurchaseWmsInStorageEntity>
    {
        public MeioErpPurchaseWmsInStorageMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPPURCHASEWMSINSTORAGE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

