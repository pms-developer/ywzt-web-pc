﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 09:58
    /// 描 述：账单金额调整
    /// </summary>
    public class MeioErpBillPriceAdjustmentMap : EntityTypeConfiguration<MeioErpBillPriceAdjustmentEntity>
    {
        public MeioErpBillPriceAdjustmentMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPBILLPRICEADJUSTMENT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

