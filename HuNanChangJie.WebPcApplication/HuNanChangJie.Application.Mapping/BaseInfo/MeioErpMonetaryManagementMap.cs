﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-10 15:52
    /// 描 述：货币管理
    /// </summary>
    public class MeioErpMonetaryManagementMap : EntityTypeConfiguration<MeioErpMonetaryManagementEntity>
    {
        public MeioErpMonetaryManagementMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPMONETARYMANAGEMENT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

