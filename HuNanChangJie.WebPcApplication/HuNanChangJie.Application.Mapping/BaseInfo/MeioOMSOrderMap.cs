﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-08 10:06
    /// 描 述：OMS发货单
    /// </summary>
    public class MeioOMSOrderMap : EntityTypeConfiguration<MeioOMSOrderEntity>
    {
        public MeioOMSOrderMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOOMSORDER");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

