﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-02 11:16
    /// 描 述：WMS入库单
    /// </summary>
    public class MeioErpWmsInStorageMap : EntityTypeConfiguration<MeioErpWmsInStorageEntity>
    {
        public MeioErpWmsInStorageMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWMSINSTORAGE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

