﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 16:37
    /// 描 述：增值服务档案信息维护
    /// </summary>
    public class MeioErpValueAddedServicesMap : EntityTypeConfiguration<MeioErpValueAddedServicesEntity>
    {
        public MeioErpValueAddedServicesMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPVALUEADDEDSERVICES");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

