﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-03 10:46
    /// 描 述：采购单
    /// </summary>
    public class MeioErpPurchaseMap : EntityTypeConfiguration<MeioErpPurchaseEntity>
    {
        public MeioErpPurchaseMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPPURCHASE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

