﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-15 11:01
    /// 描 述：WMS回传中台FBA发货单
    /// </summary>
    public class MeioErpWmsDeliveryDetailMap : EntityTypeConfiguration<MeioErpWmsDeliveryDetailEntity>
    {
        public MeioErpWmsDeliveryDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWMSDELIVERYDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

