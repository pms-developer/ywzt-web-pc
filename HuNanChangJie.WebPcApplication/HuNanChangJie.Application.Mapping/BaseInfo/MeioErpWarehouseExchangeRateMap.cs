﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-10 17:11
    /// 描 述：汇率
    /// </summary>
    public class MeioErpWarehouseExchangeRateMap : EntityTypeConfiguration<MeioErpWarehouseExchangeRateEntity>
    {
        public MeioErpWarehouseExchangeRateMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWAREHOUSEEXCHANGERATE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

