﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2024-07-31 19:17 
    /// 描 述：入库单包材明细 
    /// </summary> 
    public class MeioErpWmsInStorageAuxiliaryMaterialDetailMap : EntityTypeConfiguration<MeioErpWmsInStorageAuxiliaryMaterialDetailEntity>
    {
        public MeioErpWmsInStorageAuxiliaryMaterialDetailMap()
        {
            #region  表、主键 
            //表 
            this.ToTable("MEIOERPWMSINSTORAGEAUXILIARYMATERIALDETAIL");
            //主键 
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系 
            #endregion
        }
    }
}