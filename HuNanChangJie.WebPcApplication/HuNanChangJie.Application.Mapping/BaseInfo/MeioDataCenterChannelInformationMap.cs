﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-05 18:25
    /// 描 述：数据中台同步渠道信息
    /// </summary>
    public class MeioDataCenterChannelInformationMap : EntityTypeConfiguration<MeioDataCenterChannelInformationEntity>
    {
        public MeioDataCenterChannelInformationMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIODATACENTERCHANNELINFORMATION");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

