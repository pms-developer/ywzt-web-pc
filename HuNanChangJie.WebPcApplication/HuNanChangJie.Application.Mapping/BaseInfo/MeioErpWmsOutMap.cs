﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-18 14:32
    /// 描 述：WMS出库单信息
    /// </summary>
    public class MeioErpWmsOutMap : EntityTypeConfiguration<MeioErpWmsOutEntity>
    {
        public MeioErpWmsOutMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWMSOUT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

