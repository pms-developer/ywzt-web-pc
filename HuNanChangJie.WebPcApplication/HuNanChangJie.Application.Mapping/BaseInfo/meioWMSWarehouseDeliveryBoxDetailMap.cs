﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 13:48
    /// 描 述：领星海外仓发货单装箱明细
    /// </summary>
    public class meioWMSWarehouseDeliveryBoxDetailMap : EntityTypeConfiguration<meioWMSWarehouseDeliveryBoxDetailEntity>
    {
        public meioWMSWarehouseDeliveryBoxDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSEDELIVERYBOXDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

