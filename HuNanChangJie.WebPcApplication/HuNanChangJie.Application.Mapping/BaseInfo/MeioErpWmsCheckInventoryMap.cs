﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-08-14 15:17
    /// 描 述：WMS盘点信息
    /// </summary>
    public class MeioErpWmsCheckInventoryMap : EntityTypeConfiguration<MeioErpWmsCheckInventoryEntity>
    {
        public MeioErpWmsCheckInventoryMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWMSCHECKINVENTORY");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

