﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-08-29 20:38
    /// 描 述：包材，耗材批次库存
    /// </summary>
    public class MeioErpBatchInventoryMap : EntityTypeConfiguration<MeioErpBatchInventoryEntity>
    {
        public MeioErpBatchInventoryMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPBATCHINVENTORY");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

