﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-06-27 19:22
    /// 描 述：货品信息
    /// </summary>
    public class MeioErpGoodsMap : EntityTypeConfiguration<MeioErpGoodsEntity>
    {
        public MeioErpGoodsMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPGOODS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

