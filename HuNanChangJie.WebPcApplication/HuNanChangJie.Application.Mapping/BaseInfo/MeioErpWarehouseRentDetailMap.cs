﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 18:11
    /// 描 述：仓库租金档案信息
    /// </summary>
    public class MeioErpWarehouseRentDetailMap : EntityTypeConfiguration<MeioErpWarehouseRentDetailEntity>
    {
        public MeioErpWarehouseRentDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWAREHOUSERENTDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

