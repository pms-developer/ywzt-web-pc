﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-05 17:02
    /// 描 述：WMS小包发货信息
    /// </summary>
    public class MeioErpWmsXiaoBaoDeliveryMap : EntityTypeConfiguration<MeioErpWmsXiaoBaoDeliveryEntity>
    {
        public MeioErpWmsXiaoBaoDeliveryMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWMSXIAOBAODELIVERY");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

