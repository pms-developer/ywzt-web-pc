﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-10-09 16:54
    /// 描 述：货主账户余额变更记录表
    /// </summary>
    public class MeioErpOwnerAccountBalanceChangeRecordMap : EntityTypeConfiguration<MeioErpOwnerAccountBalanceChangeRecordEntity>
    {
        public MeioErpOwnerAccountBalanceChangeRecordMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPOWNERACCOUNTBALANCECHANGERECORD");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

