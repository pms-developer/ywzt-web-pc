﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-11-06 09:38
    /// 描 述：货主档案第三方系统信息
    /// </summary>
    public class MeioErpOwnerThirdpartySystemMap : EntityTypeConfiguration<MeioErpOwnerThirdpartySystemEntity>
    {
        public MeioErpOwnerThirdpartySystemMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPOWNERTHIRDPARTYSYSTEM");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

