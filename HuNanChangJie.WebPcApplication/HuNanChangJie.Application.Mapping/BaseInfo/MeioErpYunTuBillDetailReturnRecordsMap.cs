﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-20 14:54
    /// 描 述：云途退件支出记录
    /// </summary>
    public class MeioErpYunTuBillDetailReturnRecordsMap : EntityTypeConfiguration<MeioErpYunTuBillDetailReturnRecordsEntity>
    {
        public MeioErpYunTuBillDetailReturnRecordsMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPYUNTUBILLDETAILRETURNRECORDS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

