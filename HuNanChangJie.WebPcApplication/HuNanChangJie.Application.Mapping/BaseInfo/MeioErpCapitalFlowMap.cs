﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-12 19:45
    /// 描 述：资金流水
    /// </summary>
    public class MeioErpCapitalFlowMap : EntityTypeConfiguration<MeioErpCapitalFlowEntity>
    {
        public MeioErpCapitalFlowMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPCAPITALFLOW");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

