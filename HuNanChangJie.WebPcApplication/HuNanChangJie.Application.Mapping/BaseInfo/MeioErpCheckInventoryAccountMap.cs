﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-08-14 16:27
    /// 描 述：盘点账单
    /// </summary>
    public class MeioErpCheckInventoryAccountMap : EntityTypeConfiguration<MeioErpCheckInventoryAccountEntity>
    {
        public MeioErpCheckInventoryAccountMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPCHECKINVENTORYACCOUNT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

