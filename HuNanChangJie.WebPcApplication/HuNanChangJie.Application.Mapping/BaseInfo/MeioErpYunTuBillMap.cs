﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-19 11:00
    /// 描 述：云途账单
    /// </summary>
    public class MeioErpYunTuBillMap : EntityTypeConfiguration<MeioErpYunTuBillEntity>
    {
        public MeioErpYunTuBillMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPYUNTUBILL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

