﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-20 13:53
    /// 描 述：海外仓收货单物流状态
    /// </summary>
    public class meioWmsWarehouseOverseasReceiptLogisticsMap : EntityTypeConfiguration<meioWmsWarehouseOverseasReceiptLogisticsEntity>
    {
        public meioWmsWarehouseOverseasReceiptLogisticsMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSEOVERSEASRECEIPTLOGISTICS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

