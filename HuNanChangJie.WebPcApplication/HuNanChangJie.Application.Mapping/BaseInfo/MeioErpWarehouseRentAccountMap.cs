﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 19:54
    /// 描 述：仓租账单信息
    /// </summary>
    public class MeioErpWarehouseRentAccountMap : EntityTypeConfiguration<MeioErpWarehouseRentAccountEntity>
    {
        public MeioErpWarehouseRentAccountMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWAREHOUSERENTACCOUNT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

