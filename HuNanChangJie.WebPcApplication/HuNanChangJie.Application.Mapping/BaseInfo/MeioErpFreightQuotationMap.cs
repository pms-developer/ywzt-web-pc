﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-08 16:38
    /// 描 述：运费报价
    /// </summary>
    public class MeioErpFreightQuotationMap : EntityTypeConfiguration<MeioErpFreightQuotationEntity>
    {
        public MeioErpFreightQuotationMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPFREIGHTQUOTATION");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

