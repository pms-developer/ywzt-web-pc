﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-24 17:47
    /// 描 述：仓库管理
    /// </summary>
    public class MeioErpWarehouseMap : EntityTypeConfiguration<MeioErpWarehouseEntity>
    {
        public MeioErpWarehouseMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPWAREHOUSE");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

