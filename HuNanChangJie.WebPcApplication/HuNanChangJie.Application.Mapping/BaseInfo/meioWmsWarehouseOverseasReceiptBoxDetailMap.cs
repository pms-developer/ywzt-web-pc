﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-20 11:46
    /// 描 述：海外仓收货单
    /// </summary>
    public class meioWmsWarehouseOverseasReceiptBoxDetailMap : EntityTypeConfiguration<meioWmsWarehouseOverseasReceiptBoxDetailEntity>
    {
        public meioWmsWarehouseOverseasReceiptBoxDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOWMSWAREHOUSEOVERSEASRECEIPTBOXDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

