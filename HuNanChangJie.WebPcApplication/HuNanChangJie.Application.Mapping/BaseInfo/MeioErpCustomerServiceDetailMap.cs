﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-19 10:01
    /// 描 述：客户服务信息
    /// </summary>
    public class MeioErpCustomerServiceDetailMap : EntityTypeConfiguration<MeioErpCustomerServiceDetailEntity>
    {
        public MeioErpCustomerServiceDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPCUSTOMERSERVICEDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

