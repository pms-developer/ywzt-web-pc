﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-23 10:57
    /// 描 述：结算单信息
    /// </summary>
    public class MeioErpStatementMap : EntityTypeConfiguration<MeioErpStatementEntity>
    {
        public MeioErpStatementMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPSTATEMENT");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

