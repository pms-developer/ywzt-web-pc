﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-21 18:22
    /// 描 述：账单计算错误信息
    /// </summary>
    public class MeioErpYunTuBillingMaintenanceErrorMap : EntityTypeConfiguration<MeioErpYunTuBillingMaintenanceErrorEntity>
    {
        public MeioErpYunTuBillingMaintenanceErrorMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPYUNTUBILLINGMAINTENANCEERROR");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

