﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.BaseInfo
{
    public class MeioErpDocumentAuditRecordMap : EntityTypeConfiguration<MeioErpDocumentAuditRecordEntity>
    {
        public MeioErpDocumentAuditRecordMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPDOCUMENTAUDITRECORD");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
