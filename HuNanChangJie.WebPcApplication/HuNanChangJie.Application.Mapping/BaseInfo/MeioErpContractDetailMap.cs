﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-13 16:32
    /// 描 述：合同管理
    /// </summary>
    public class MeioErpContractDetailMap : EntityTypeConfiguration<MeioErpContractDetailEntity>
    {
        public MeioErpContractDetailMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPCONTRACTDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

