﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-08-28 12:09
    /// 描 述：合同变更记录信息
    /// </summary>
    public class MeioErpContractChangeLogMap : EntityTypeConfiguration<MeioErpContractChangeLogEntity>
    {
        public MeioErpContractChangeLogMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOERPCONTRACTCHANGELOG");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

