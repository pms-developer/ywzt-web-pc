﻿using HuNanChangeJie.Application.Project.ProjectSubject;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.Project
{
    public class ProjectSubjectMap:EntityTypeConfiguration<ProjectSubjectEntity>
    {
        public ProjectSubjectMap()
        {
            HasKey(i => i.ID);
            ToTable("Project_Subject");
        }
    }
}
