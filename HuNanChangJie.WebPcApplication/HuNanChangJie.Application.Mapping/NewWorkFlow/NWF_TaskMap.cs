﻿using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:59
    /// 描 述：NWF_Task
    /// </summary>
    public class NWF_TaskMap : EntityTypeConfiguration<NWF_TaskEntity>
    {
        public NWF_TaskMap()
        {
            #region  表、主键
            //表
            this.ToTable("NWF_TASK");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

