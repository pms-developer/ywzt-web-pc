﻿using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 14:42
    /// 描 述：1
    /// </summary>
    public class NWF_DelegateRelationMap : EntityTypeConfiguration<NWF_DelegateRelationEntity>
    {
        public NWF_DelegateRelationMap()
        {
            #region  表、主键
            //表
            this.ToTable("NWF_DELEGATERELATION");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

