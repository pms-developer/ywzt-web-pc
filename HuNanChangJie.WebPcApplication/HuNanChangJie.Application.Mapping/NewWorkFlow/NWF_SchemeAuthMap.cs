﻿using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:55
    /// 描 述：NWF_SchemeAuth
    /// </summary>
    public class NWF_SchemeAuthMap : EntityTypeConfiguration<NWF_SchemeAuthEntity>
    {
        public NWF_SchemeAuthMap()
        {
            #region  表、主键
            //表
            this.ToTable("NWF_SCHEMEAUTH");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

