﻿using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 14:41
    /// 描 述：流程会签统计
    /// </summary>
    public class NWF_ConfluenceMap : EntityTypeConfiguration<NWF_ConfluenceEntity>
    {
        public NWF_ConfluenceMap()
        {
            #region  表、主键
            //表
            this.ToTable("NWF_CONFLUENCE");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

