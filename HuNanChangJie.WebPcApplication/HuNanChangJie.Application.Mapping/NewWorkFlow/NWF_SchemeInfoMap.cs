﻿using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:57
    /// 描 述：NWFSchemeInfo
    /// </summary>
    public class NWF_SchemeInfoMap : EntityTypeConfiguration<NWF_SchemeInfoEntity>
    {
        public NWF_SchemeInfoMap()
        {
            #region  表、主键
            //表
            this.ToTable("NWF_SCHEMEINFO");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

