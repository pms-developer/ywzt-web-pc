﻿using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 14:46
    /// 描 述：222
    /// </summary>
    public class NWF_ProcessMap : EntityTypeConfiguration<NWF_ProcessEntity>
    {
        public NWF_ProcessMap()
        {
            #region  表、主键
            //表
            this.ToTable("NWF_PROCESS");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

