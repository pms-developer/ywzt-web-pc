﻿using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 16:04
    /// 描 述：NWFTaskMsg
    /// </summary>
    public class NWF_TaskMsgMap : EntityTypeConfiguration<NWF_TaskMsgEntity>
    {
        public NWF_TaskMsgMap()
        {
            #region  表、主键
            //表
            this.ToTable("NWF_TASKMSG");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

