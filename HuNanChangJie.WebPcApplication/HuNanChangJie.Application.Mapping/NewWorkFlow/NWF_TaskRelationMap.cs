﻿using HuNanChangJie.Application.TwoDevelopment.NewWorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 16:05
    /// 描 述：NWFTaskRelation
    /// </summary>
    public class NWF_TaskRelationMap : EntityTypeConfiguration<NWF_TaskRelationEntity>
    {
        public NWF_TaskRelationMap()
        {
            #region  表、主键
            //表
            this.ToTable("NWF_TASKRELATION");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

