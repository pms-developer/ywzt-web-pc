﻿using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-10 14:23
    /// 描 述：美鸥产品选择辅料
    /// </summary>
    public class meio_product_pick_accessoriesMap : EntityTypeConfiguration<meio_product_pick_accessoriesEntity>
    {
        public meio_product_pick_accessoriesMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIO_PRODUCT_PICK_ACCESSORIES");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

