﻿using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-10 19:15
    /// 描 述：组合产品包含关系
    /// </summary>
    public class meio_product_pick_comboMap : EntityTypeConfiguration<meio_product_pick_comboEntity>
    {
        public meio_product_pick_comboMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIO_PRODUCT_PICK_COMBO");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

