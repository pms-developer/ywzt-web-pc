﻿using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-11 17:34
    /// 描 述：审计日志
    /// </summary>
    public class meioAuditLogMap : EntityTypeConfiguration<meioAuditLogEntity>
    {
        public meioAuditLogMap()
        {
            #region  表、主键
            //表
            this.ToTable("MEIOAUDITLOG");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

