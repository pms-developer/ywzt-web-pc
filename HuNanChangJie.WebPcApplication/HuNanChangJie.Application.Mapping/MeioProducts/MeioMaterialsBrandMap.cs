﻿using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping
{
    public class MeioMaterialsBrandMap: EntityTypeConfiguration<MeioMaterialsBrandEntity>
    {
        public MeioMaterialsBrandMap()
        {
            #region  表、主键
            //表
            this.ToTable("MeioMaterialsBrand");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
