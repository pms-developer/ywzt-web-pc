﻿using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2024-04-10 08:47 
    /// 描 述：美鸥产品采购箱规 
    /// </summary> 
    public class meio_product_box_gaugeMap : EntityTypeConfiguration<meio_product_box_gaugeEntity>
    {
        public meio_product_box_gaugeMap()
        {
            #region  表、主键 
            //表 
            this.ToTable("MEIO_PRODUCT_BOX_GAUGE");
            //主键 
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系 
            #endregion
        }
    }
}