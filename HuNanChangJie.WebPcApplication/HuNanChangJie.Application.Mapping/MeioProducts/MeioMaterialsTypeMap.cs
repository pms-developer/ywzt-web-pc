﻿using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.MeioProducts
{
    public class MeioMaterialsTypeMap : EntityTypeConfiguration<MeioMaterialsTypeEntity>
    {
        public MeioMaterialsTypeMap()
        {
            #region  表、主键
            //表
            this.ToTable("MeioMaterialsType");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
