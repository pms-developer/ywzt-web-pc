﻿using HuNanChangJie.Application.TwoDevelopment.Site_Order;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-28 11:11
    /// 描 述：老订单库
    /// </summary>
    public class tOrderMap : EntityTypeConfiguration<tOrderEntity>
    {
        public tOrderMap()
        {
            #region  表、主键
            //表
            this.ToTable("TORDER");
            //主键
            this.HasKey(t => t.Order_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

