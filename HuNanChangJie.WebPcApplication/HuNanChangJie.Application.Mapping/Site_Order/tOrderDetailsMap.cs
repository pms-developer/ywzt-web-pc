﻿using HuNanChangJie.Application.TwoDevelopment.Site_Order;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-28 11:12
    /// 描 述：订单详情库
    /// </summary>
    public class tOrderDetailsMap : EntityTypeConfiguration<tOrderDetailsEntity>
    {
        public tOrderDetailsMap()
        {
            #region  表、主键
            //表
            this.ToTable("TORDERDETAILS");
            //主键
            this.HasKey(t => t.OrderDetail_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

