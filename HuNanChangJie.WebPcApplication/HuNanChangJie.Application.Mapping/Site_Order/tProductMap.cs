﻿using HuNanChangJie.Application.TwoDevelopment.Site_Order;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-28 11:12
    /// 描 述：产品库
    /// </summary>
    public class tProductMap : EntityTypeConfiguration<tProductEntity>
    {
        public tProductMap()
        {
            #region  表、主键
            //表
            this.ToTable("TPRODUCT");
            //主键
            this.HasKey(t => t.Product_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

