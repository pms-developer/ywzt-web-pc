﻿using HuNanChangJie.Application.Base.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.03.04
    /// 描 述：系统功能模块
    /// </summary>
    public class ModuleMap : EntityTypeConfiguration<ModuleEntity>
    {
        public ModuleMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_MODULE");
            //主键
            this.HasKey(t => t.F_ModuleId);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
