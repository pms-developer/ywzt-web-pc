﻿using HuNanChangJie.Application.Base.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.System.Module
{
    public class ModuleFormMap : EntityTypeConfiguration<ModuleFormEntity>
    {
        public ModuleFormMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_MODULEFORM");
            //主键
            this.HasKey(t => t.F_ModuleFormId);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
