﻿using HuNanChangJie.Application.Base.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创建人：
    /// 日 期：2017.03.04
    /// 描 述：编号规则
    /// </summary>
    public class CodeRuleMap : EntityTypeConfiguration<CodeRuleEntity>
    {
        public CodeRuleMap()
        {
            #region  表、主键
            //表
            this.ToTable("BASE_CODERULE");//Base_CodeRule
            //主键
            this.HasKey(t => t.F_RuleId);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
