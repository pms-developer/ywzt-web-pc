﻿using HuNanChangJie.Application.OA.Email.EmailSend;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.OA
{
    /// <summary>
    /// Copyright (c) 2013-2017 
    /// 创建人：
    /// 日 期：2017.04.17
    /// 描 述：邮件发送
    /// </summary>
    public class EmailSendMap : EntityTypeConfiguration<EmailSendEntity>
    {
        public EmailSendMap()
        {
            #region  表、主键
            //表
            this.ToTable("EMAILSEND");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}