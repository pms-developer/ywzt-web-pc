﻿using HuNanChangJie.Application.OA.Email.EmailConfig;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping.OA
{
    /// <summary>
    /// Copyright (c) 2013-2017 
    /// 创建人：
    /// 日 期：2017.04.17
    /// 描 述：邮件配置
    /// </summary>
    public class EmailConfigMap : EntityTypeConfiguration<EmailConfigEntity>
    {
        public EmailConfigMap()
        {
            #region  表、主键
            //表
            this.ToTable("EMAILCONFIG");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}