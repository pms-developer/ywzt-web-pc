﻿using HuNanChangJie.Application.TwoDevelopment.Blood;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-15 10:06
    /// 描 述：献血表彰
    /// </summary>
    public class tBs_DedicationPraiseMap : EntityTypeConfiguration<tBs_DedicationPraiseEntity>
    {
        public tBs_DedicationPraiseMap()
        {
            #region  表、主键
            //表
            this.ToTable("TBS_DEDICATIONPRAISE");
            //主键
            this.HasKey(t => t.F_AutoID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

