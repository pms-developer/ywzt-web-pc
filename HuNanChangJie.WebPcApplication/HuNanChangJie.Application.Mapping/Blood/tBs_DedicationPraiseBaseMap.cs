﻿using HuNanChangJie.Application.TwoDevelopment.Blood;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-16 16:47
    /// 描 述：献血表彰主表
    /// </summary>
    public class tBs_DedicationPraiseBaseMap : EntityTypeConfiguration<tBs_DedicationPraiseBaseEntity>
    {
        public tBs_DedicationPraiseBaseMap()
        {
            #region  表、主键
            //表
            this.ToTable("TBS_DEDICATIONPRAISEBASE");
            //主键
            this.HasKey(t => t.F_ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

