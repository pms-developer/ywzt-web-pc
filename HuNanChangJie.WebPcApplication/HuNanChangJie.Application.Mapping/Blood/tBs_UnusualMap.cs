﻿using HuNanChangJie.Application.TwoDevelopment.Blood;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-10-17 15:53
    /// 描 述：特殊稀有血型献血者
    /// </summary>
    public class tBs_UnusualMap : EntityTypeConfiguration<tBs_UnusualEntity>
    {
        public tBs_UnusualMap()
        {
            #region  表、主键
            //表
            this.ToTable("TBS_UNUSUAL");
            //主键
            this.HasKey(t => t.KID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

