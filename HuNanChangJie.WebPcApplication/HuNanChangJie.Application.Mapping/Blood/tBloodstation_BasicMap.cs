﻿using HuNanChangJie.Application.TwoDevelopment.Blood;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-10-17 14:27
    /// 描 述：血站基本信息
    /// </summary>
    public class tBloodstation_BasicMap : EntityTypeConfiguration<tBloodstation_BasicEntity>
    {
        public tBloodstation_BasicMap()
        {
            #region  表、主键
            //表
            this.ToTable("TBLOODSTATION_BASIC");
            //主键
            this.HasKey(t => t.Item_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

