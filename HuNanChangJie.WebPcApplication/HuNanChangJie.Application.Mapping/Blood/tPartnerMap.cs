﻿using HuNanChangJie.Application.TwoDevelopment.Blood;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-20 16:37
    /// 描 述：接口调用者
    /// </summary>
    public class tPartnerMap : EntityTypeConfiguration<tPartnerEntity>
    {
        public tPartnerMap()
        {
            #region  表、主键
            //表
            this.ToTable("TPARTNER");
            //主键
            this.HasKey(t => t.Partner_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

