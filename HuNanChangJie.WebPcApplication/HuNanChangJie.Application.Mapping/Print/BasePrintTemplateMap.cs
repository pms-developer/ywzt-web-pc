﻿using HuNanChangJie.Application.Print;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.Mapping.Print
{
    public class BasePrintTemplateMap : EntityTypeConfiguration<PrintTemplateEntity>
    {
        public BasePrintTemplateMap()
        {
            HasKey(i => i.ID);
            ToTable("Base_PrintTemplate");
        }
    }
}
