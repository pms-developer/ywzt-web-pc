﻿using HuNanChangJie.Application.TwoDevelopment.TaskScheduling;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-08 17:59
    /// 描 述：222
    /// </summary>
    public class XQ_HSInfoMap : EntityTypeConfiguration<XQ_HSInfoEntity>
    {
        public XQ_HSInfoMap()
        {
            #region  表、主键
            //表
            this.ToTable("XQ_HSINFO");
            //主键
            this.HasKey(t => t.F_ID);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

