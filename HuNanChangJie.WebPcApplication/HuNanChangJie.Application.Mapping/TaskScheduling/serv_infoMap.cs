﻿using HuNanChangJie.Application.TwoDevelopment.TaskScheduling;
using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.Application.Mapping
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2019-01-22 14:09 
    /// 描 述：服务器列表 
    /// </summary> 
    public class serv_infoMap : EntityTypeConfiguration<serv_infoEntity>
    {
        public serv_infoMap()
        {
            #region  表、主键 
            //表 
            this.ToTable("SERV_INFO");
            //主键 
            this.HasKey(t => t.servid);
            #endregion

            #region  配置关系 
            #endregion
        }
    }
}