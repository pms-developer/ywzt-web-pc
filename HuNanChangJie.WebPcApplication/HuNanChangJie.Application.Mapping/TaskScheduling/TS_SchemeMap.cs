﻿using HuNanChangJie.Application.TwoDevelopment.TaskScheduling;
using System.Data.Entity.ModelConfiguration;

namespace  HuNanChangJie.Application.Mapping
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-18 12:52
    /// 描 述：TSScheme
    /// </summary>
    public class TS_SchemeMap : EntityTypeConfiguration<TS_SchemeEntity>
    {
        public TS_SchemeMap()
        {
            #region  表、主键
            //表
            this.ToTable("TS_SCHEME");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}

