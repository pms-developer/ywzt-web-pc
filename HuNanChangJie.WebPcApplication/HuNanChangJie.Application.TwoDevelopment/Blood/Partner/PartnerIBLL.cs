﻿using System;
using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-20 16:37
    /// 描 述：接口调用者
    /// </summary>
    public interface PartnerIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<tPartnerEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取tPartner表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        tPartnerEntity GettPartnerEntity(Guid keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, tPartnerEntity entity);
        #endregion

    }
}
