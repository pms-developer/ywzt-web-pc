﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-20 16:37
    /// 描 述：接口调用者
    /// </summary>
    public class tPartnerEntity 
    {
        #region  实体成员
        /// <summary>
        /// Partner_Id
        /// </summary>
        [Column("PARTNER_ID")]
        public Guid Partner_Id { get; set; }
        /// <summary>
        /// Partner_Name
        /// </summary>
        [Column("PARTNER_NAME")]
        public string Partner_Name { get; set; }
        /// <summary>
        /// Partner_Private
        /// </summary>
        [Column("PARTNER_PRIVATE")]
        public string Partner_Private { get; set; }
        /// <summary>
        /// Create_Time
        /// </summary>
        [Column("CREATE_TIME")]
        public DateTime? Create_Time { get; set; }
        /// <summary>
        /// 0 未启用，不可调用接口
        /// 1 启用，可调用接口
        /// </summary>
        [Column("STATUS")]
        public int? Status { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Partner_Id = Guid.NewGuid();
            this.Create_Time=DateTime.Now;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Partner_Id = Guid.Parse(keyValue);
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

