﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-15 10:06
    /// 描 述：献血表彰
    /// </summary>
    public interface DedicationPraiseIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<tBs_DedicationPraiseEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取tBs_DedicationPraise表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<tBs_DedicationPraiseEntity> GettBs_DedicationPraiseList(string keyValue);
        /// <summary>
        /// 获取tBs_DedicationPraise表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        tBs_DedicationPraiseEntity GettBs_DedicationPraiseEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, tBs_DedicationPraiseEntity entity);
        #endregion

    }
}
