﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-15 10:06
    /// 描 述：献血表彰
    /// </summary>
    public class tBs_DedicationPraiseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_ID { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        [Column("F_IDCARD")]
        public string F_IDCard { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("F_STATUS")]
        public string F_Status { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        [Column("F_NAME")]
        public string F_Name { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        [Column("F_SEX")]
        public string F_Sex { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        [Column("F_BIRTHDAY")]
        public DateTime? F_Birthday { get; set; }
        /// <summary>
        /// 申报单位
        /// </summary>
        [Column("F_APPCOMPANY")]
        public string F_AppCompany { get; set; }
        /// <summary>
        /// 申报年度
        /// </summary>
        [Column("F_APPDATE")]
        public string F_AppDate { get; set; }
        /// <summary>
        /// 申报奖项
        /// </summary>
        [Column("F_APPPRAISE")]
        public string F_AppPraise { get; set; }
        /// <summary>
        /// 献血量
        /// </summary>
        [Column("F_BLOODVOLUME")]
        public decimal? F_BloodVolume { get; set; } = 0;
        /// <summary>
        /// 献血次数
        /// </summary>
        [Column("F_BLOODCOUNT")]
        public int? F_BloodCount { get; set; }
        /// <summary>
        /// CreationDate
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// Creation_Id
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// CreationName
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 工作单位
        /// </summary>
        [Column("F_WORKCOMPANY")]
        public string F_WorkCompany { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        [Column("F_PROVINCE")]
        public string F_Province { get; set; }
        /// <summary>
        /// 市
        /// </summary>
        [Column("F_CITY")]
        public string F_City { get; set; }
        /// <summary>
        /// F_AutoID
        /// </summary>
        [Column("F_AUTOID")]
        public string F_AutoID { get; set; }
        /// <summary>
        /// 是否表彰
        /// </summary>
        [Column("F_AUTHORSTATUS")]
        public string F_AuthorStatus { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_AutoID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_AutoID = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

