﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-10-17 15:53
    /// 描 述：特殊稀有血型献血者
    /// </summary>
    public class BsUnusualService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<tBs_UnusualEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.KID,
                t.BS_Id,
                t.bm,
                t.xm,
                t.zjlx,
                t.sfz,
                t.xx_xt,
                t.xx_bx,
                t.Partner_Id,
                t.Create_Time
                ");
                strSql.Append("  FROM tBs_Unusual t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["xm"].IsEmpty())
                {
                    dp.Add("xm", queryParam["xm"].ToString(), dbType: DbType.String);
                    strSql.Append(" AND t.xm= @xm ");
                }
                if (!queryParam["sfz"].IsEmpty())
                {
                    dp.Add("sfz", queryParam["sfz"].ToString(), dbType: DbType.String);
                    strSql.Append(" AND t.sfz= @sfz ");
                }
                if(pagination != null)
                {
                    return this.BaseRepository("Blood").FindList<tBs_UnusualEntity>(strSql.ToString(), dp, pagination);
                }
                else
                {
                    return this.BaseRepository("Blood").FindList<tBs_UnusualEntity>(strSql.ToString(), dp );
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取tBs_Unusual表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public tBs_UnusualEntity GettBs_UnusualEntity(Guid keyValue)
        {
            try
            {
                return this.BaseRepository("Blood").FindEntity<tBs_UnusualEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(Guid keyValue)
        {
            try
            {
                this.BaseRepository("Blood").Delete<tBs_UnusualEntity>(t=>t.KID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(Guid keyValue, tBs_UnusualEntity entity)
        {
            try
            {
                if (keyValue != Guid.Empty)
                {
                    entity.Modify(keyValue);
                    this.BaseRepository("Blood").Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository("Blood").Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
