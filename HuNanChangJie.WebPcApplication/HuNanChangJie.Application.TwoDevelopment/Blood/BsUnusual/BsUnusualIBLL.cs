﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using System;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-10-17 15:53
    /// 描 述：特殊稀有血型献血者
    /// </summary>
    public interface BsUnusualIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<tBs_UnusualEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取tBs_Unusual表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        tBs_UnusualEntity GettBs_UnusualEntity(Guid keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(Guid keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(Guid keyValue, tBs_UnusualEntity entity);
        #endregion

    }
}
