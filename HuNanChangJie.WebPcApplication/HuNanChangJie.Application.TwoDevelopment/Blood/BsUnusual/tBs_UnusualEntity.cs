﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-10-17 15:53
    /// 描 述：特殊稀有血型献血者
    /// </summary>
    public class tBs_UnusualEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("KID")]
        public Guid KID { get; set; }
        /// <summary>
        /// 外键 血站ID
        /// </summary>
        [Column("BS_ID")]
        public Guid BS_Id { get; set; }
        /// <summary>
        /// 献血者识别码
        /// </summary>
        [Column("BM")]
        public string bm { get; set; }
        /// <summary>
        /// 献血者姓名
        /// </summary>
        [Column("XM")]
        public string xm { get; set; }
        /// <summary>
        /// 身份证件类别
        /// </summary>
        [Column("ZJLX")]
        public int? zjlx { get; set; }
        /// <summary>
        /// 献血者身份证件号码
        /// </summary>
        [Column("SFZ")]
        public string sfz { get; set; }
        /// <summary>
        /// 稀有血型系统
        /// </summary>
        [Column("XX_XT")]
        public string xx_xt { get; set; }
        /// <summary>
        /// 稀有血型表型
        /// </summary>
        [Column("XX_BX")]
        public string xx_bx { get; set; }
        /// <summary>
        /// Partner_Id
        /// </summary>
        [Column("PARTNER_ID")]
        public Guid Partner_Id { get; set; }
        /// <summary>
        /// Create_Time
        /// </summary>
        [Column("CREATE_TIME")]
        public DateTime? Create_Time { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.KID = Guid.NewGuid();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(Guid keyValue)
        {
            this.KID = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

