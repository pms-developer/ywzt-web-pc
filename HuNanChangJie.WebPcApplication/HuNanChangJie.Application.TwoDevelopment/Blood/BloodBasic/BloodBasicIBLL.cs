﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using System;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-10-17 14:27
    /// 描 述：血站基本信息
    /// </summary>
    public interface BloodBasicIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<tBloodstation_BasicEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取tBloodstation_Basic表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        tBloodstation_BasicEntity GettBloodstation_BasicEntity(Guid keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(Guid keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(Guid keyValue, tBloodstation_BasicEntity entity);
        #endregion

    }
}
