﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-10-17 14:27
    /// 描 述：血站基本信息
    /// </summary>
    public class BloodBasicService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<tBloodstation_BasicEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Item_Id,
                t.Da_Time,
                t.Mc,
                t.Shxydm,
                t.Jgdm,
                t.Xzlx,
                t.Jfly,
                t.Zgbm,
                t.Fr,
                t.Jlsj,
                t.Spjg,
                t.Spwh,
                t.Yxqx,
                t.Xzqhdm,
                t.Address,
                t.Postcode,
                t.Phone
                ");
                strSql.Append("  FROM tBloodstation_Basic t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                return this.BaseRepository("Blood").FindList<tBloodstation_BasicEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取tBloodstation_Basic表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public tBloodstation_BasicEntity GettBloodstation_BasicEntity(Guid keyValue)
        {
            try
            {
                return this.BaseRepository("Blood").FindEntity<tBloodstation_BasicEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(Guid keyValue)
        {
            try
            {
                this.BaseRepository("Blood").Delete<tBloodstation_BasicEntity>(t=>t.Item_Id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(Guid keyValue, tBloodstation_BasicEntity entity)
        {
            try
            {
                if (keyValue != Guid.Empty)
                {
                    entity.Modify(keyValue);
                    this.BaseRepository("Blood").Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository("Blood").Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
