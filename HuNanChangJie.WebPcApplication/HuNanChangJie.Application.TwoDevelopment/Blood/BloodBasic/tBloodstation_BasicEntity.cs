﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-10-17 14:27
    /// 描 述：血站基本信息
    /// </summary>
    public class tBloodstation_BasicEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ITEM_ID")]
        public Guid Item_Id { get; set; }
        /// <summary>
        /// 外键 血站ID
        /// </summary>
        [Column("BS_ID")]
        public string BS_Id { get; set; }
        /// <summary>
        /// 数据时间
        /// </summary>
        [Column("DA_TIME")]
        public string Da_Time { get; set; }
        /// <summary>
        /// 血站名称
        /// </summary>
        [Column("MC")]
        public string Mc { get; set; }
        /// <summary>
        /// 统一社会信用代码
        /// </summary>
        [Column("SHXYDM")]
        public string Shxydm { get; set; }
        /// <summary>
        /// 机构代码
        /// </summary>
        [Column("JGDM")]
        public string Jgdm { get; set; }
        /// <summary>
        /// 血站类型
        /// </summary>
        [Column("XZLX")]
        public int? Xzlx { get; set; }
        /// <summary>
        /// 经费来源
        /// </summary>
        [Column("JFLY")]
        public int? Jfly { get; set; }
        /// <summary>
        /// 主管部门
        /// </summary>
        [Column("ZGBM")]
        public string Zgbm { get; set; }
        /// <summary>
        /// 法定代表人
        /// </summary>
        [Column("FR")]
        public string Fr { get; set; }
        /// <summary>
        /// 建立时间
        /// </summary>
        [Column("JLSJ")]
        public string Jlsj { get; set; }
        /// <summary>
        /// 审批机关
        /// </summary>
        [Column("SPJG")]
        public string Spjg { get; set; }
        /// <summary>
        /// 审批文号
        /// </summary>
        [Column("SPWH")]
        public string Spwh { get; set; }
        /// <summary>
        /// 有效期限
        /// </summary>
        [Column("YXQX")]
        public string Yxqx { get; set; }
        /// <summary>
        /// 行政区划代码
        /// </summary>
        [Column("XZQHDM")]
        public string Xzqhdm { get; set; }
        /// <summary>
        /// 通讯地址
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 邮政编码
        /// </summary>
        [Column("POSTCODE")]
        public string Postcode { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("PHONE")]
        public string Phone { get; set; }
        /// <summary>
        /// Partner_Id
        /// </summary>
        [Column("PARTNER_ID")]
        public string Partner_Id { get; set; }
        /// <summary>
        /// Create_Time
        /// </summary>
        [Column("CREATE_TIME")]
        public DateTime? Create_Time { get; set; }
        /// <summary>
        /// Update_Time
        /// </summary>
        [Column("UPDATE_TIME")]
        public DateTime? Update_Time { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Item_Id = Guid.NewGuid();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(Guid keyValue)
        {
            this.Item_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

