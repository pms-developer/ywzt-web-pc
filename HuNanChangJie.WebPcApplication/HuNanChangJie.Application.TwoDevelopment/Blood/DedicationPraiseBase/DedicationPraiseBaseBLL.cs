﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-16 16:47
    /// 描 述：献血表彰主表
    /// </summary>
    public class DedicationPraiseBaseBLL : DedicationPraiseBaseIBLL
    {
        private DedicationPraiseBaseService dedicationPraiseBaseService = new DedicationPraiseBaseService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<tBs_DedicationPraiseBaseEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return dedicationPraiseBaseService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取页面显示列表数据(执行存储过程)
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public DataTable GetPageList_ByProc(XqPagination pagination, string queryJson)
        {
            try
            {
                return dedicationPraiseBaseService.GetPageList_ByProc(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取tBs_DedicationPraiseBase表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public tBs_DedicationPraiseBaseEntity GettBs_DedicationPraiseBaseEntity(string keyValue)
        {
            try
            {
                return dedicationPraiseBaseService.GettBs_DedicationPraiseBaseEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                dedicationPraiseBaseService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <param name="entity">主表</param>
        /// <param name="dbModel">字表</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, tBs_DedicationPraiseBaseEntity entity, List<tBs_DedicationPraiseEntity> dbModel)
        {
            try
            {
                dedicationPraiseBaseService.SaveEntity(keyValue, entity, dbModel);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
