﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-16 16:47
    /// 描 述：献血表彰主表
    /// </summary>
    public class tBs_DedicationPraiseBaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_ID { get; set; }
        /// <summary>
        /// 申报单位
        /// </summary>
        [Column("F_COMPANY")]
        public string F_Company { get; set; }
        /// <summary>
        /// 申报人
        /// </summary>
        [Column("F_NAME")]
        public string F_Name { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        [Column("F_PROVINCE")]
        public string F_Province { get; set; }
        /// <summary>
        /// 市
        /// </summary>
        [Column("F_CITY")]
        public string F_City { get; set; }
        /// <summary>
        /// 申报年度
        /// </summary>
        [Column("F_DATE")]
        public string F_Date { get; set; }
        /// <summary>
        /// 表彰状态
        /// </summary>
        [Column("F_STATUS")]
        public string F_Status { get; set; }
        /// <summary>
        /// 企业编码
        /// </summary>
        [Column("F_ENCODE")]
        public string F_Encode { get; set; }
        /// <summary>
        /// 是否表彰
        /// </summary>
        [Column("F_AUTHORSTATUS")]
        public string F_AuthorStatus { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_ID = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_ID = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

