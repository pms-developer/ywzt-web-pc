﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-16 16:47
    /// 描 述：献血表彰主表
    /// </summary>
    public class DedicationPraiseBaseService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<tBs_DedicationPraiseBaseEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_ID,
                t.F_Company,
                F_Name,
                F_Province,
                F_City,
                F_Date,
                F_Status
                ");
                strSql.Append("  FROM tBs_DedicationPraiseBase t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                return this.BaseRepository("Blood").FindList<tBs_DedicationPraiseBaseEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取页面显示列表数据(执行存储过程)
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public DataTable GetPageList_ByProc(XqPagination pagination, string queryJson)
        {
            try
            {
                var procName = "p_GetWuChangFengXianJiang";
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { AreaName = queryParam["AreaName"] });
                return BaseRepository("Blood").ExecuteByProc<DataTable>(procName, dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取tBs_DedicationPraiseBase表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public tBs_DedicationPraiseBaseEntity GettBs_DedicationPraiseBaseEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository("Blood").FindEntity<tBs_DedicationPraiseBaseEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository("Blood").Delete<tBs_DedicationPraiseBaseEntity>(t=>t.F_ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, tBs_DedicationPraiseBaseEntity entity, List<tBs_DedicationPraiseEntity> dbModel)
        {
            var db = this.BaseRepository("Blood");
            db.BeginTrans();
            try
            {
                entity.Create();
                db.Insert<tBs_DedicationPraiseBaseEntity>(entity);

                foreach (var item in dbModel)
                {
                    item.Create();
                    item.F_ID = entity.F_ID;
                    db.Insert<tBs_DedicationPraiseEntity>(item);
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
