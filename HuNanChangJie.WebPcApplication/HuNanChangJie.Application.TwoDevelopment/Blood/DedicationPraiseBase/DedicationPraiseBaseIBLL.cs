﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.Blood
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-11-16 16:47
    /// 描 述：献血表彰主表
    /// </summary>
    public interface DedicationPraiseBaseIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<tBs_DedicationPraiseBaseEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取tBs_DedicationPraiseBase表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        tBs_DedicationPraiseBaseEntity GettBs_DedicationPraiseBaseEntity(string keyValue);

        /// <summary>
        /// 获取页面显示列表数据(执行存储过程)
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="queryJson"></param>
        /// <returns></returns>
        DataTable GetPageList_ByProc(XqPagination pagination, string queryJson);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        //void SaveEntity(string keyValue, tBs_DedicationPraiseBaseEntity entity);
        #endregion

        void SaveEntity(string keyValue, tBs_DedicationPraiseBaseEntity entity,
            List<tBs_DedicationPraiseEntity> dbModel);

    }
}
