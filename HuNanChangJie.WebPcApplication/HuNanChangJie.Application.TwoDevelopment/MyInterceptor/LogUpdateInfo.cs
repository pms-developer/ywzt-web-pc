﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment
{
    /// <summary>
    /// 日志更新信息
    /// </summary>
    public class LogUpdateInfo
    {
        /// <summary>
        /// 是否主表
        /// </summary>
        public bool isDefault { get; set; }

        /// <summary>
        /// 表模型
        /// </summary>
        public string model { get; set; }

        /// <summary>
        /// 明细操作类型
        /// </summary>
        public string operType { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public string keyValue { get; set; }

        /// <summary>
        /// 更新的键
        /// </summary>
        public string updateKey {  get; set; }
    
        /// <summary>
        /// 更新前值
        /// </summary>
        public string updateOldVal { get; set; }

        /// <summary>
        /// 更新后值
        /// </summary>
        public string updateNewVal { get; set; }


    }
}
