﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment
{
    /// <summary>
    /// 日志结果
    /// </summary>
    public class LogResult
    {
        /// <summary>
        /// 操作类型
        /// </summary>
        public string operType { get;set; }

        /// <summary>
        /// 模型
        /// </summary>
        public string model { get; set; }

        /// <summary>
        /// 操作主键
        /// </summary>
        public string keyValue { get; set; }

        /// <summary>
        /// 操作用户
        /// </summary>
        public string operUser { get;set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime? operTime { get; set; }

        /// <summary>
        /// 更新明细
        /// </summary>
        public List<LogUpdateInfo> detailList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        public void DefaultDetail(string json)
        {
            dynamic all = JsonConvert.DeserializeObject<dynamic>(json);
            string one = all.updateList;
            this.detailList = new List<LogUpdateInfo>();
            if (!string.IsNullOrEmpty(one))
            {
                this.detailList = JsonConvert.DeserializeObject<List<LogUpdateInfo>>(one);
            }
        }
        /// <summary>
        /// 操作日志
        /// </summary>
        public string operLog { get; set; }
    }
}
