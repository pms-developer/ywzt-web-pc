﻿using Castle.Core.Interceptor;
using Castle.DynamicProxy;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.DataBase;
using System;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using Newtonsoft.Json;
using HuNanChangJie.Util;
using System.Linq;
using System.Configuration;
using System.Data;
using HuNanChangJie.Application.Organization;
using HuNanChangJie.SystemCommon;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text.RegularExpressions;
using System.Reflection;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using System.Text;
using System.Threading;


namespace HuNanChangJie.Application.TwoDevelopment
{
    /// <summary>
    /// 日志拦截
    /// </summary>
    public class MyInterceptor : IInterceptor
    {
        static MyInterceptor myInterceptor = new MyInterceptor();
        static ProxyGenerator proxyGenerator = new ProxyGenerator();


        /// <summary>
        /// 创建代理对象
        /// </summary>
        /// <typeparam name="ST"></typeparam>
        /// <param name="BLL"></param>
        /// <returns></returns>
        public static ST InterCreateObj<ST>(ST BLL) where ST : class
        {
            return proxyGenerator.CreateInterfaceProxyWithTarget(BLL, myInterceptor);
        }

        /// <summary>
        /// 
        /// </summary>
        public static List<LogResult> getLogResult(string model, string keyValue)
        {
            string dtGetSql = string.Format("select * from meioAuditLogControl where ID='{0}'", keyValue);
            DataTable table = DbBase2().FindTable(dtGetSql);

            List<LogResult> rs = new List<LogResult>();
            foreach (DataRow item in table.Rows)
            {
                string tableName = item[1].ToString();
                if (!string.IsNullOrEmpty(tableName))
                {
                    var arr = getLogRows(item[1].ToString(), model, keyValue);
                    rs.AddRange(arr);
                }
            }
            return rs;
        }

        public static List<LogResult> getLogRows(string fullName, string model, string keyValue)
        {
            string sql = string.Format("select * from dbo.[{0}] where operType like '%{1}%' and keyValue='{2}' order by CreationDate desc"
            , fullName
                , model
                , keyValue);
            DataTable dataTable = DbBase2().FindTable(sql);
            List<LogResult> rs = new List<LogResult>();
            foreach (DataRow item in dataTable.Rows)
            {
                List<string> opArr = Regex.Split(item["operType"].ToString(), Regex.Escape("->"), RegexOptions.IgnoreCase).ToList();
                if (opArr[0] == "Update")
                {
                    LogResult lr = new LogResult();
                    lr.operType = "修改";//opArr[0];
                    lr.model = opArr[1];
                    lr.keyValue = item["keyValue"].ToString();
                    lr.operUser = item["CreationName"].ToString();
                    lr.operTime = DateTime.Parse(item["CreationDate"].ToString());
                    lr.DefaultDetail(item["sourceMainInfo"].ToString());
                    lr.operLog = item["error"].ToString();
                    rs.Add(lr);
                }
                if (opArr[0] == "Add")
                {
                    LogResult lr = new LogResult();
                    lr.operType = "新增";//opArr[0];
                    lr.model = opArr[1];
                    lr.keyValue = item["keyValue"].ToString();
                    lr.operUser = item["CreationName"].ToString();
                    lr.operTime = DateTime.Parse(item["CreationDate"].ToString());
                    lr.operLog = item["error"].ToString();
                    rs.Add(lr);
                }
                if (opArr.Count > 1)
                {
                    if (opArr[1] == "DeleteEntity")
                    {
                        LogResult lr = new LogResult();
                        lr.operType = "删除";//opArr[1];
                        lr.model = opArr[0];
                        lr.keyValue = item["keyValue"].ToString();
                        lr.operUser = item["CreationName"].ToString();
                        lr.operTime = DateTime.Parse(item["CreationDate"].ToString());
                        lr.operLog = item["error"].ToString();
                        rs.Add(lr);
                    }
                }
                if (opArr[0].Contains("审核")
                    || opArr[0].Contains("反审核")
                    || opArr[0].Contains("批量备注")
                    || opArr[0].Contains("调整金额")
                    || opArr[0].Contains("启用/禁用")
                    || opArr[0].Contains("修改有效期"))
                {
                    LogResult lr = new LogResult();
                    if (opArr[0].Contains("审核"))
                    {
                        lr.operType = "审核";
                        lr.model = opArr[0].Replace("审核", "");
                    }
                    if (opArr[0].Contains("反审核"))
                    {
                        lr.operType = "反审核";
                        lr.model = opArr[0].Replace("反审核", "");
                    }
                    if (opArr[0].Contains("批量备注"))
                    {
                        lr.operType = "批量备注";
                        lr.model = opArr[0].Replace("批量备注", "");
                    }
                    if (opArr[0].Contains("调整金额"))
                    {
                        lr.operType = "调整金额";
                        lr.model = opArr[0].Replace("调整金额", "");
                    }
                    if (opArr[0].Contains("启用/禁用"))
                    {
                        lr.operType = "启用/禁用";
                        lr.model = opArr[0].Replace("启用/禁用", "");
                    }
                    if (opArr[0].Contains("修改有效期"))
                    {
                        lr.operType = "修改有效期";
                        lr.model = opArr[0].Replace("修改有效期", "");
                    }
                    lr.keyValue = item["keyValue"].ToString();
                    lr.operUser = item["CreationName"].ToString();
                    lr.operTime = DateTime.Parse(item["CreationDate"].ToString());
                    lr.operLog = item["error"].ToString();
                    rs.Add(lr);
                }
            }

            return rs;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="model"></param>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public static List<LogResult> getLogResult(DateTime dt, string model, string keyValue)
        {
            string baseName = "meioAuditLog-";
            string date = dt.ToString("yyyyMM");
            string fullName = baseName + date;
            string sql = string.Format("select * from dbo.[{0}] where operType like '%{1}%' and keyValue='{2}' order by CreationDate desc"
                , fullName
                , model
                , keyValue);
            DataTable dataTable = DbBase2().FindTable(sql);
            List<LogResult> rs = new List<LogResult>();
            foreach (DataRow item in dataTable.Rows)
            {
                List<string> opArr = Regex.Split(item["operType"].ToString(), Regex.Escape("->"), RegexOptions.IgnoreCase).ToList();
                if (opArr[0] == "Update")
                {
                    LogResult lr = new LogResult();
                    lr.operType = opArr[0];
                    lr.model = opArr[1];
                    lr.keyValue = item["keyValue"].ToString();
                    lr.operUser = item["CreationName"].ToString();
                    lr.operTime = DateTime.Parse(item["CreationDate"].ToString());
                    lr.DefaultDetail(item["sourceMainInfo"].ToString());
                    rs.Add(lr);
                }
                if (opArr[0] == "Add")
                {
                    LogResult lr = new LogResult();
                    lr.operType = opArr[0];
                    lr.model = opArr[1];
                    lr.keyValue = item["keyValue"].ToString();
                    lr.operUser = item["CreationName"].ToString();
                    lr.operTime = DateTime.Parse(item["CreationDate"].ToString());
                    rs.Add(lr);
                }
                if (opArr[1] == "DeleteEntity")
                {
                    LogResult lr = new LogResult();
                    lr.operType = opArr[1];
                    lr.model = opArr[0];
                    lr.keyValue = item["keyValue"].ToString();
                    lr.operUser = item["CreationName"].ToString();
                    lr.operTime = DateTime.Parse(item["CreationDate"].ToString());
                    rs.Add(lr);
                }

            }

            return rs;
        }


        public void InsertLog(string keyValue, object oldobj, object newobj)
        {
            string operType = "";
            string log = "";
            if (oldobj != null && newobj == null)
            {
                operType = "DeleteEntity";
                log = JsonConvert.SerializeObject(oldobj);
            }

            if (oldobj == null && newobj != null)
            {
                operType = "Add";
                log = JsonConvert.SerializeObject(newobj);
            }

            if (oldobj != null && newobj != null)
            {
                operType = "Update";

                var list = ArePropertiesDifferent(oldobj, newobj);

                log = JsonConvert.SerializeObject(new
                {
                    updateList = JsonConvert.SerializeObject(list)
                });
            }

            meioAuditLogEntity entity = new meioAuditLogEntity();
            entity.Create();
            entity.keyValue = keyValue;
            entity.sourceMainInfo = log;
            entity.operType = operType;
            InserForDate(entity);
        }

        /// <summary>
        /// 比较对象值差异
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        public static List<LogUpdateInfo> ArePropertiesDifferent(object obj1, object obj2)
        {
            List<LogUpdateInfo> logList = new List<LogUpdateInfo>();


            Type type = obj1.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                // 只比较具有读访问器的属性
                if (property.CanRead)
                {
                    object value1 = property.GetValue(obj1, null);
                    object value2 = property.GetValue(obj2, null);

                    // 比较值是否相等，忽略大小写或类型转换等（根据需要调整）
                    if (!Equals(value1, value2))
                    {
                        string[] propertyNames = { "ID","ItemID","CreationDate", "Creation_Id", "CreationName", "ModificationDate", "Modification_Id", "ModificationName",
                            "Company_ID", "CompanyCode","CompanyFullName","CompanyName","CreatedBy","CreatedDate","ModifyBy","ModifyDate" };

                        if (!propertyNames.Contains(property.Name) && !property.Name.Contains("ID") && !property.Name.Contains("Id"))
                        {
                            var l = new LogUpdateInfo();
                            l.isDefault = true;
                            l.model = "";
                            l.operType = "Update";
                            l.updateKey = property.Name;
                            l.updateOldVal = value1!=null?value1.ToString():"";
                            l.updateNewVal = value2!=null?value2.ToString():"";

                            logList.Add(l);
                        }
                    }
                }
            }
            return logList;
        }

        private static IRepository DbBase2()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["logSql"].ConnectionString;
            IDatabase iDatabase = DbFactory.GetIDatabase(connectionString, DatabaseType.SqlServer);
            IRepository db = new Repository(iDatabase);
            return db;
        }

        private IRepository DbBase()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["logSql"].ConnectionString;
            IDatabase iDatabase = DbFactory.GetIDatabase(connectionString, DatabaseType.SqlServer);
            IRepository db = new Repository(iDatabase);
            return db;
        }

        private void InserForDate(meioAuditLogEntity model)
        {
            string baseName = "meioAuditLog-";
            string date = DateTime.Now.ToString("yyyyMM");
            string fullName = baseName + date;
            string checkSql = string.Format(@"
                IF EXISTS(SELECT * FROM sys.tables WHERE name = '{0}')
                    SELECT 1 b
                ELSE
                    SELECT 0 b
            ", fullName);

            object dt = DbBase().ExecuteScalar(checkSql);
            int b = int.Parse(dt.ToString());
            if (b == 0)
            {
                string sql = CreateTableSql(fullName);
                DbBase().ExecuteBySql(sql);
            }
            //记录数据所在的分表
            string checkRowsSql = string.Format(@"select count(1) from meioAuditLogControl where ID='{0}' and tableName='{1}'",
                model.keyValue, fullName);
            object dt2 = DbBase().ExecuteScalar(checkRowsSql);
            int b2 = int.Parse(dt2.ToString());
            if (b2 == 0)
            {
                string rowSql = string.Format(@"INSERT INTO meioAuditLogControl  VALUES('{0}', '{1}')", model.keyValue, fullName);
                DbBase().ExecuteBySql(rowSql);
            }

            string insert = InsertSql(fullName, model);
            DbBase().ExecuteBySql(insert);
        }

        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            try
            {
                // 在调用方法之前可以添加自己的逻辑
               

                if (invocation.Method.Name == AuditLogType.DeleteEntity.ToString())
                {
                    meioAuditLogEntity entity = new meioAuditLogEntity();
                    entity.Create();
                    entity.keyValue = invocation.Arguments[0].ToString();
                    entity.operType = invocation.Method.DeclaringType.Name + "->" + invocation.Method.Name;
                    entity.sourceMainInfo = invocation.Arguments[invocation.Arguments.Length - 1].ToString();
                    if (invocation.Method.DeclaringType.Name == "MeioErpFreightQuotationIBLL")
                    {
                        var oldData = entity.sourceMainInfo.ToObject<MeioErpFreightQuotationData>();
                        if (oldData != null)
                        {
                            entity.error = oldData.MeioErpFreightQuotation.RuleName;
                        }
                    }
                    InserForDate(entity);
                }
                if (invocation.Method.Name == AuditLogType.SaveEntity.ToString())
                {
                    string logs = invocation.Arguments[invocation.Arguments.Length - 2].ToString();
                    meioAuditLogEntity entity = new meioAuditLogEntity();
                    entity.Create();
                    entity.keyValue = invocation.Arguments[0].ToString();
                    entity.sourceMainInfo = logs;
                    if (invocation.Arguments[invocation.Arguments.Length - 1].ToString() == "edit")
                    {
                        entity.operType = AuditLogType.Update.ToString()
                            + "->"
                            + invocation.Method.DeclaringType.Name + "->" + invocation.Method.Name;

                        if (invocation.Method.DeclaringType.Name == "MeioErpFreightQuotationIBLL")
                        {
                            var oldData = logs.ToObject<MeioErpFreightQuotationData>();
                            if (oldData != null)
                            {
                                Dictionary<string, string> columnNameDict = new Dictionary<string, string>();
                                columnNameDict.Add("RuleName", "规则名称");
                                columnNameDict.Add("EffectiveStartTime", "有效开始时间");
                                columnNameDict.Add("EffectiveEndTime", "有效结束时间");
                                columnNameDict.Add("GeneralCarrier", "承运商");
                                columnNameDict.Add("LogisticsChannel", "物流渠道");
                                columnNameDict.Add("BillingMode", "计费方式");
                                columnNameDict.Add("RuleDescription", "规则说明");
                                columnNameDict.Add("Country", "国家");
                                columnNameDict.Add("MinWeight", "最低起重");
                                columnNameDict.Add("Freight", "运费");
                                columnNameDict.Add("RegistrationFee", "挂号费");
                                columnNameDict.Add("Scale", "进位制");
                                columnNameDict.Add("FirstWeight", "首重");
                                columnNameDict.Add("ContinueWeight", "续重");
                                columnNameDict.Add("StartWeight", "起始重量");
                                columnNameDict.Add("EndWeight", "结束重量");
                                columnNameDict.Add("Enabled", "启用/禁用");
                                columnNameDict.Add("ReferenceAging", "参考时效");


                                var newFreightQuotation = invocation.Arguments[1] as MeioErpFreightQuotationEntity;
                                var newFreightQuotationDetail = invocation.Arguments[2] as List<MeioErpFreightQuotationDetailEntity>;

                                StringBuilder strlog = new StringBuilder();
                                List<LogUpdateInfo> updateLog = ArePropertiesDifferent(oldData.MeioErpFreightQuotation, newFreightQuotation);
                                if (updateLog.Count > 0)
                                {
                                    foreach (var itemlog in updateLog)
                                    {
                                        if (itemlog.updateKey != "Country")
                                        {
                                            var columnName = columnNameDict.First(p => p.Key == itemlog.updateKey).Value;
                                            strlog.Append("；【" + (columnName == null ? itemlog.updateKey : columnName) + "】由" + itemlog.updateOldVal + "改为:" + itemlog.updateNewVal);
                                        }
                                    }

                                }

                                foreach (var itemolddetail in oldData.MeioErpFreightQuotationDetail)
                                {
                                    var itemnewdetail = newFreightQuotationDetail.Find(p => p.Country == itemolddetail.Country && p.Area == itemolddetail.Area && p.StartWeight == itemolddetail.StartWeight && p.EndWeight == itemolddetail.EndWeight);
                                    if (itemnewdetail != null)
                                    {
                                        List<LogUpdateInfo> updateLog1 = ArePropertiesDifferent(itemolddetail, itemnewdetail);
                                        if (updateLog1.Count > 0)
                                        {
                                            strlog.Append("；【国家】/【地区】" + itemolddetail.Country + (string.IsNullOrEmpty(itemolddetail.Area) ? "" : ("-" + itemolddetail.Area)) + " 【重量段】" + (itemolddetail.StartWeight + "<W<=" + itemolddetail.EndWeight));
                                            foreach (var itemlog in updateLog1)
                                            {
                                                var columnName = columnNameDict.First(p => p.Key == itemlog.updateKey).Value;
                                                strlog.Append("【" + (columnName == null ? itemlog.updateKey : columnName) + "】由" + itemlog.updateOldVal + "改为:" + itemlog.updateNewVal);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        strlog.Append("；【国家】/【地区】" + itemolddetail.Country + (string.IsNullOrEmpty(itemolddetail.Area) ? "" : ("-" + itemolddetail.Area)) + " 【重量段】" + (itemolddetail.StartWeight + "<W<=" + itemolddetail.EndWeight) + "已删除或更改");
                                    }
                                }
                                foreach (var itemnewdetail in newFreightQuotationDetail)
                                {
                                    var itemolddetail = oldData.MeioErpFreightQuotationDetail.Find(p => p.Country == itemnewdetail.Country && p.Area == itemnewdetail.Area && p.StartWeight == itemnewdetail.StartWeight && p.EndWeight == itemnewdetail.EndWeight);
                                    if (itemolddetail == null)
                                    {
                                        strlog.Append("；新增【国家】/【地区】" + itemnewdetail.Country + (string.IsNullOrEmpty(itemnewdetail.Area) ? "" : ("-" + itemnewdetail.Area)) + " 【重量段】" + (itemnewdetail.StartWeight + "<W<=" + itemnewdetail.EndWeight));

                                    }
                                }
                                
                                if (strlog.Length > 0)
                                {
                                    entity.error = strlog.ToString().Substring(1);
                                }
                            }
                        }

                        if (invocation.Method.DeclaringType.Name == "OperationIBLL")
                        {
                            var oldData = logs.ToObject<MeioErpOperationData>();
                            if (oldData != null)
                            {
                                Dictionary<string, string> columnNameDict = new Dictionary<string, string>();
                                columnNameDict.Add("ItemName", "计费项名称");
                                columnNameDict.Add("Unit", "计费单位");
                                columnNameDict.Add("Remark", "备注");
                                columnNameDict.Add("ExpenseType", "费用类型");
                                columnNameDict.Add("ExpenseItem", "费用项目");
                                columnNameDict.Add("Conditions", "条件项");
                                columnNameDict.Add("ChargePoint", "计费点");
                                columnNameDict.Add("WeightUnit", "重量单位");
                                columnNameDict.Add("IsAutomaticAudit", "自动审核");
                                columnNameDict.Add("MinValue", "最小值");
                                columnNameDict.Add("MaxValue", "最大值");

                                var newOperation = invocation.Arguments[1] as MeioErpOperationEntity;
                                var newOperationDetail = invocation.Arguments[2] as List<MeioErpOperationDetailEntity>;

                                StringBuilder strlog = new StringBuilder();
                                List<LogUpdateInfo> updateLog = ArePropertiesDifferent(oldData.MeioErpOperation, newOperation);
                                if (updateLog.Count > 0)
                                {
                                    foreach (var itemlog in updateLog)
                                    {
                                        if (columnNameDict.Any(p => p.Key == itemlog.updateKey))
                                        {
                                            var columnName = columnNameDict.First(p => p.Key == itemlog.updateKey).Value;
                                            strlog.Append("；【" + (columnName == null ? itemlog.updateKey : columnName) + "】由" + itemlog.updateOldVal + "改为:" + itemlog.updateNewVal);
                                        }
                                    }

                                }

                                foreach (var itemolddetail in oldData.MeioErpOperationDetail)
                                {
                                    var itemnewdetail = newOperationDetail.Find(p => p.MinValue == itemolddetail.MinValue && p.MaxValue == itemolddetail.MaxValue );
                                    if (itemnewdetail != null)
                                    {
                                        List<LogUpdateInfo> updateLog1 = ArePropertiesDifferent(itemolddetail, itemnewdetail);
                                        if (updateLog1.Count > 0)
                                        {
                                            strlog.Append("【规则明细】" + (itemolddetail.MinValue + "<W<=" + itemolddetail.MaxValue));
                                            foreach (var itemlog in updateLog1)
                                            {
                                                if (columnNameDict.Any(p => p.Key == itemlog.updateKey))
                                                {
                                                    var columnName = columnNameDict.First(p => p.Key == itemlog.updateKey).Value;
                                                    strlog.Append("【" + (columnName == null ? itemlog.updateKey : columnName) + "】由" + itemlog.updateOldVal + "改为:" + itemlog.updateNewVal);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        strlog.Append("【规则明细】" + (itemolddetail.MinValue + "<W<=" + itemolddetail.MaxValue) + "已删除或更改");
                                    }
                                }
                                foreach (var itemnewdetail in newOperationDetail)
                                {
                                    var itemolddetail = oldData.MeioErpOperationDetail.Find(p => p.MinValue == itemnewdetail.MinValue && p.MaxValue == itemnewdetail.MaxValue);
                                    if (itemolddetail == null)
                                    {
                                        strlog.Append("；新增【规则明细】" + (itemnewdetail.MinValue + "<W<=" + itemnewdetail.MaxValue));

                                    }
                                }

                                if (strlog.Length > 0)
                                {
                                    entity.error = strlog.ToString().Substring(1);
                                }
                            }
                        }

                        if (invocation.Method.DeclaringType.Name == "PriceSheetIBLL")
                        {
                            var oldData = logs.ToObject<MeioErpPriceSheetData>();
                            if (oldData != null)
                            {
                                Dictionary<string, string> columnNameDict = new Dictionary<string, string>();
                                columnNameDict.Add("Code", "报价单号");
                                columnNameDict.Add("Name", "报价单名称");
                                columnNameDict.Add("Type", "报价单类型");
                                columnNameDict.Add("EffectiveTime", "生效时间");
                                columnNameDict.Add("ExpirationTime", "失效时间");
                                columnNameDict.Add("Remark", "备注");
                                columnNameDict.Add("Currency", "币种");
                                columnNameDict.Add("WarehouseID", "仓库");
                                columnNameDict.Add("ItemType", "报价单项类型");
                                columnNameDict.Add("ItemName", "报价单项说明");
                                columnNameDict.Add("ItemDetailName", "说明");
                                columnNameDict.Add("ItemDetailMinValue", "最小值");
                                columnNameDict.Add("ItemDetailMaxValue", "最大值");
                                columnNameDict.Add("Unit", "单位");
                                columnNameDict.Add("Price", "单价");
                                columnNameDict.Add("ExpenseType", "费用类型");
                                columnNameDict.Add("ExpenseItem", "费用项目");
                                columnNameDict.Add("Conditions", "条件项");
                                columnNameDict.Add("ChargePoint", "计费点");

                                var newPriceSheet = invocation.Arguments[1] as MeioErpPriceSheetEntity;
                                var newPriceSheetDetail = invocation.Arguments[2] as List<MeioErpPriceSheetDetailEntity>;

                                StringBuilder strlog = new StringBuilder();
                                List<LogUpdateInfo> updateLog = ArePropertiesDifferent(oldData.MeioErpPriceSheet, newPriceSheet);
                                if (updateLog.Count > 0)
                                {
                                    foreach (var itemlog in updateLog)
                                    {
                                        if (columnNameDict.Any(p => p.Key == itemlog.updateKey))
                                        {
                                            var columnName = columnNameDict.First(p => p.Key == itemlog.updateKey).Value;
                                            strlog.Append("；【" + (columnName == null ? itemlog.updateKey : columnName) + "】由" + itemlog.updateOldVal + "改为:" + itemlog.updateNewVal);
                                        }
                                    }

                                }

                                foreach (var itemolddetail in oldData.MeioErpPriceSheetDetail)
                                {
                                    var itemnewdetail = newPriceSheetDetail.Find(p =>p.ExpenseType==itemolddetail.ExpenseType && p.ExpenseItem==itemolddetail.ExpenseItem && p.Conditions==itemolddetail.Conditions && p.ItemDetailMinValue == itemolddetail.ItemDetailMinValue && p.ItemDetailMaxValue == itemolddetail.ItemDetailMaxValue);
                                    if (itemnewdetail != null)
                                    {
                                        List<LogUpdateInfo> updateLog1 = ArePropertiesDifferent(itemolddetail, itemnewdetail);
                                        if (updateLog1.Count > 0)
                                        {
                                            strlog.Append("；【报价单明细】【计费项名称】"+ itemolddetail.ExpenseType+" 【费用项目】"+itemolddetail.ExpenseItem+"【条件项】"+itemolddetail.Conditions+"【阶梯项】" + (itemolddetail.ItemDetailMinValue + "~" + itemolddetail.ItemDetailMaxValue));
                                            foreach (var itemlog in updateLog1)
                                            {
                                                if (columnNameDict.Any(p => p.Key == itemlog.updateKey))
                                                {
                                                    var columnName = columnNameDict.First(p => p.Key == itemlog.updateKey).Value;
                                                    strlog.Append("【" + (columnName == null ? itemlog.updateKey : columnName) + "】由" + itemlog.updateOldVal + "改为:" + itemlog.updateNewVal);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        strlog.Append("；【报价单明细】【计费项名称】" + itemolddetail.ExpenseType + " 【费用项目】" + itemolddetail.ExpenseItem + "【条件项】" + itemolddetail.Conditions + "【阶梯项】" + (itemolddetail.ItemDetailMinValue + "~" + itemolddetail.ItemDetailMaxValue) + "已删除或更改");
                                    }
                                }
                                foreach (var itemnewdetail in newPriceSheetDetail)
                                {
                                    var itemolddetail = oldData.MeioErpPriceSheetDetail.Find(p =>p.ExpenseType== itemnewdetail.ExpenseType && p.ExpenseItem==itemnewdetail.ExpenseItem && p.Conditions==itemnewdetail.Conditions && p.ItemDetailMinValue == itemnewdetail.ItemDetailMinValue && p.ItemDetailMaxValue == itemnewdetail.ItemDetailMaxValue);
                                    if (itemolddetail == null)
                                    {
                                        strlog.Append("；新增【报价单明细】【计费项名称】" + itemnewdetail.ExpenseType + " 【费用项目】" + itemnewdetail.ExpenseItem + "【条件项】" + itemnewdetail.Conditions + "【阶梯项】" + (itemnewdetail.ItemDetailMinValue + "~" + itemnewdetail.ItemDetailMaxValue));

                                    }
                                }

                                if (strlog.Length > 0)
                                {
                                    entity.error = strlog.ToString().Substring(1);
                                }
                            }
                        }
                    }
                    else
                    {
                        entity.operType = AuditLogType.Add.ToString()
                            + "->"
                            + invocation.Method.DeclaringType.Name + "->" + invocation.Method.Name; ;
                    }
                    InserForDate(entity);
                }
                if (invocation.Method.Name == AuditLogType.Audit.ToString())
                {
                    var ids = invocation.Arguments[0].ToString().Split(',');
                    foreach (var id in ids)
                    {
                        meioAuditLogEntity entity = new meioAuditLogEntity();
                        entity.Create();
                        entity.keyValue = id;
                        if (invocation.Arguments[1].ToString() == "1")
                        {
                            entity.error = "已审核，审核人："+ invocation.Arguments[2].ToString();
                            entity.operType = "审核" + invocation.Method.DeclaringType.Name;
                        }
                        else
                        {
                            entity.error = "反审核，审核人：" + invocation.Arguments[2].ToString();
                            entity.operType = "反审核" + invocation.Method.DeclaringType.Name;
                        }
                        InserForDate(entity);
                    }
                }
                if (invocation.Method.Name == AuditLogType.BatchRemark.ToString())
                {

                    var ids = invocation.Arguments[0].ToString().Split(',');
                    foreach (var id in ids)
                    {
                        meioAuditLogEntity entity = new meioAuditLogEntity();
                        entity.Create();
                        entity.keyValue = id;
                        entity.error = invocation.Arguments[1].ToString(); ;
                        entity.operType = "批量备注" + invocation.Method.DeclaringType.Name;

                        InserForDate(entity);
                    }
                }

                if (invocation.Method.Name == AuditLogType.AdjustmentAmount.ToString())
                {
                    meioAuditLogEntity entity = new meioAuditLogEntity();
                    entity.Create();
                    entity.keyValue = invocation.Arguments[0].ToString();
                    entity.error = "调整金额：" + invocation.Arguments[1].ToString() + "  原调整金额："+ invocation.Arguments[3].ToString() + "  备注：" + invocation.Arguments[2].ToString();
                    entity.operType = "调整金额" + invocation.Method.DeclaringType.Name;

                    InserForDate(entity);
                }

                if (invocation.Method.Name == AuditLogType.SetIsEnable.ToString())
                {
                    var ids = invocation.Arguments[0].ToString().Split(',');
                    foreach (var id in ids)
                    {
                        meioAuditLogEntity entity = new meioAuditLogEntity();
                        entity.Create();
                        entity.keyValue = id;
                        entity.error = invocation.Arguments[1].ToString().ToLower() == "true" ? "启用" : "禁用";
                        entity.operType = "启用/禁用" + invocation.Method.DeclaringType.Name;
                        InserForDate(entity);
                    }
                }
                if (invocation.Method.Name == AuditLogType.UpdateEffectiveTime.ToString())
                {
                    //var ids = invocation.Arguments[0].ToString().Split(',');
                    //foreach (var id in ids)
                    //{
                        var list= invocation.Arguments[3] as List<MeioErpFreightQuotationEntity>;
                        if (list != null)
                        {
                            foreach (var item in list)
                            {
                                meioAuditLogEntity entity = new meioAuditLogEntity();
                                entity.Create();
                                entity.keyValue = item.ID;
                                entity.error = "开始日期由："+item.EffectiveStartTime+"改为：" + invocation.Arguments[1].ToString() + "  结束日期由："+item.EffectiveEndTime+"改为：" + invocation.Arguments[2].ToString();
                                entity.operType = "修改有效期" + invocation.Method.DeclaringType.Name;
                                InserForDate(entity);
                            }
                        }
                    //}
                }

                invocation.Proceed(); // 这里调用原始方法
            }
            catch (Exception ex)
            {
                meioAuditLogEntity entity = new meioAuditLogEntity();
                entity.Create();
                entity.keyValue = invocation.Arguments[0].ToString();
                if (invocation.Method.Name == AuditLogType.DeleteEntity.ToString())
                {
                    entity.sourceMainInfo = invocation.Arguments[invocation.Arguments.Length - 1].ToString();
                }
                if (invocation.Method.Name == AuditLogType.SaveEntity.ToString())
                {
                    entity.sourceMainInfo = invocation.Arguments[invocation.Arguments.Length - 2].ToString();
                }
                entity.operType = AuditLogType.Error.ToString()
                            + "->"
                            + invocation.Method.DeclaringType.Name + "->" + invocation.Method.Name; ;
                entity.error = ex.Message;
                InserForDate(entity);
                Console.WriteLine(ex.ToString());
            }
            finally
            {
            }

        }

        private string CreateTableSql(string name)
        {
            string sql = @"
                    CREATE TABLE [dbo].[{0}](
	                    [keyValue] [varchar](50) NULL,
	                    [operType] [varchar](50) NULL,
	                    [sourceMainInfo] [text] NULL,
	                    [error] [text] NULL,
	                    [ID] [varchar](50) NOT NULL,
	                    [Enabled] [bit] NULL,
	                    [CreationDate] [datetime] NULL,
	                    [Creation_Id] [varchar](50) NULL,
	                    [CreationName] [varchar](50) NULL,
	                    [ModificationDate] [datetime] NULL,
	                    [Modification_Id] [varchar](50) NULL,
	                    [ModificationName] [varchar](50) NULL,
	                    [SortCode] [int] NULL,
	                    [CompanyCode] [varchar](50) NULL,
	                    [Company_ID] [varchar](50) NULL,
	                    [CompanyFullName] [varchar](100) NULL,
	                    [CompanyName] [varchar](50) NULL,
	                    [PinYin] [varchar](100) NULL,
	                    [PinYinShort] [varchar](50) NULL,
	                    [Auditor_ID] [varchar](50) NULL,
	                    [AuditorName] [varchar](50) NULL,
	                    [AuditStatus] [varchar](1) NULL,
	                    [Base_ShopInfoID] [varchar](50) NULL,
	                    [ShopName] [varchar](50) NULL,
	                    [Workflow_ID] [varchar](50) NULL,
                    PRIMARY KEY CLUSTERED 
                    (
	                    [ID] ASC
                    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
                    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                ";

            return string.Format(sql, name);
        }

        private string InsertSql(string name, meioAuditLogEntity model)
        {
            string sql = @"
                INSERT INTO [dbo].[{0}]
                           ([keyValue]
                           ,[operType]
                           ,[sourceMainInfo]
                           ,[error]
                           ,[ID]
                           ,[Enabled]
                           ,[CreationDate]
                           ,[Creation_Id]
                           ,[CreationName]
                           ,[ModificationDate]
                           ,[Modification_Id]
                           ,[ModificationName]
                           ,[SortCode]
                           ,[CompanyCode]
                           ,[Company_ID]
                           ,[CompanyFullName]
                           ,[CompanyName]
                           ,[PinYin]
                           ,[PinYinShort]
                           ,[Auditor_ID]
                           ,[AuditorName]
                           ,[AuditStatus]
                           ,[Base_ShopInfoID]
                           ,[ShopName]
                           ,[Workflow_ID])
                     VALUES
                           ('{1}'
                           ,'{2}'
                           ,'{3}'
                           ,'{4}'
                           ,'{5}'
                           ,{6}
                           ,'{7}'
                           ,'{8}'
                           ,'{9}'
                           ,'{10}'
                           ,'{11}'
                           ,'{12}'
                           ,{13}
                           ,'{14}'
                           ,'{15}'
                           ,'{16}'
                           ,'{17}'
                           ,'{18}'
                           ,'{19}'
                           ,'{20}'
                           ,'{21}'
                           ,'{22}'
                           ,'{23}'
                           ,'{24}'
                           ,'{25}')";
            return string.Format(sql
                , name
                , model.keyValue
                , model.operType
                , model.sourceMainInfo
                , model.error
                , model.ID
                , model.Enabled.GetValueOrDefault() ? 1 : 0
                , model.CreationDate
                , model.Creation_Id
                , model.CreationName
                , model.ModificationDate
                , model.Modification_Id
                , model.ModificationName
                , model.SortCode.GetValueOrDefault()
                , model.CompanyCode
                , model.Company_ID
                , model.CompanyFullName
                , model.CompanyName
                , model.PinYin
                , model.PinYinShort
                , model.Auditor_ID
                , model.AuditorName
                , model.AuditStatus
                , model.Base_ShopInfoID
                , model.ShopName
                , model.Workflow_ID);

        }

    }
    public class MeioErpFreightQuotationData
    {
        public MeioErpFreightQuotationEntity MeioErpFreightQuotation { get; set; }
        public List<MeioErpFreightQuotationDetailEntity> MeioErpFreightQuotationDetail { get; set; }
    }

    public class MeioErpOperationData
    {
        public MeioErpOperationEntity MeioErpOperation { get; set; }
        public List<MeioErpOperationDetailEntity> MeioErpOperationDetail { get; set; }
    }

    public class MeioErpPriceSheetData
    {
        public MeioErpPriceSheetEntity MeioErpPriceSheet { get; set; }
        public List<MeioErpPriceSheetDetailEntity> MeioErpPriceSheetDetail { get; set; }
    }
}


