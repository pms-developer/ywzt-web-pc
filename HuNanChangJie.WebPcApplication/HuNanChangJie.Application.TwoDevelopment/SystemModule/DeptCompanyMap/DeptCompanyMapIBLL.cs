﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-05 16:41
    /// 描 述：钉钉部门与公司的mapping关系
    /// </summary>
    public interface DeptCompanyMapIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<base_3rd_deparment_company_mappingEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取base_3rd_deparment_company_mapping表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        base_3rd_deparment_company_mappingEntity Getbase_3rd_deparment_company_mappingEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, base_3rd_deparment_company_mappingEntity entity,string deleteList,string type);
        #endregion

    }
}
