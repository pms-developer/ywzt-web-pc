﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-21 18:46
    /// 描 述：领料单
    /// </summary>
    public class MaterialRequisitionBLL : MaterialRequisitionIBLL
    {
        private MaterialRequisitionService materialRequisitionService = new MaterialRequisitionService();

        #region  获取数据
        public IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination,string queryJson, string subcontractId, string warehouseId)
        {
            try
            {
                return materialRequisitionService.GetMaterialsInfoList(pagination, queryJson, subcontractId, warehouseId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MaterialRequisitionEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return materialRequisitionService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MaterialRequisitionDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MaterialRequisitionDetailsEntity> GetMaterialRequisitionDetailsList(string keyValue)
        {
            try
            {
                return materialRequisitionService.GetMaterialRequisitionDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MaterialRequisitionDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MaterialRequisitionDetailsEntity> GetMaterialRequisitionDetailsListConnectionMaterials(string keyValue)
        {
            try
            {
                return materialRequisitionService.GetMaterialRequisitionDetailsListConnectionMaterials(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取MaterialRequisition表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MaterialRequisitionEntity GetMaterialRequisitionEntity(string keyValue)
        {
            try
            {
                return materialRequisitionService.GetMaterialRequisitionEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MaterialRequisitionDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MaterialRequisitionDetailsEntity GetMaterialRequisitionDetailsEntity(string keyValue)
        {
            try
            {
                return materialRequisitionService.GetMaterialRequisitionDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return materialRequisitionService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return materialRequisitionService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                materialRequisitionService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MaterialRequisitionEntity entity,List<MaterialRequisitionDetailsEntity> materialRequisitionDetailsList,string deleteList,string type)
        {
            try
            {
                materialRequisitionService.SaveEntity(keyValue, entity,materialRequisitionDetailsList,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("MaterialRequisitionCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取项目领料单
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<MaterialRequisitionEntity> GetProjectMaterialRequisitions(string projectId)
        {
            try
            {
                return materialRequisitionService.GetProjectMaterialRequisitions(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
