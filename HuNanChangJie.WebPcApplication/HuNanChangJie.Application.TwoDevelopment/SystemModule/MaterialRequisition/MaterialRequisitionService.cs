﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-21 18:46
    /// 描 述：领料单
    /// </summary>
    public class MaterialRequisitionService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据
        public IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination, string queryJson, string subcontractId, string warehouseId)
        {
            try
            {
                var queryParam = queryJson.ToJObject();

                string wsql = "";
                if (string.IsNullOrEmpty(warehouseId))
                {
                    wsql = "Base_WarehouseDetails";
                }
                else
                {
                    wsql = $"(select * from Base_WarehouseDetails where fk_warehouseid='{warehouseId}')";
                }
                string sql = "";
                if (string.IsNullOrEmpty(subcontractId))
                {
                    sql = $"select b.ID,b.Code,b.Name,b.Model,b.Origin,b.BrandId,b.UnitId, c.Price1 Price,c.Quantity,c.Quantity MaxQuantity ,'库存数量限制' LimitDesc,bw.ID as Base_Warehouse_ID,bw.Name as Base_Warehouse_Name from  Base_Materials b left join  {wsql} c on b.id = c.fk_materialid join Base_Warehouse as bw on c.Fk_WarehouseId =bw.ID where isnull(c.Quantity,0)>0";// where c.Quantity>0
                }
                else
                {
                    sql = $"select  b.ID,b.Code,b.Name,b.Model,b.Origin,b.BrandId,b.UnitId, c.Price1 Price,c.Quantity,case when isnull(a.Quantity,0) > isnull(c.Quantity,0) then isnull(c.Quantity,0) else isnull(a.Quantity,0) end MaxQuantity,case when a.Quantity > c.Quantity then '库存数量限制' else '材料清单限制' end LimitDesc from Project_SubcontractMaterials a left join Base_Materials b on a.Code = b.Code left join  {wsql} c on b.id = c.fk_materialid where a.ProjectSubcontractId = '{subcontractId}' and isnull(c.Quantity,0)>0";// and c.Quantity>0
                }

                if (!queryParam["name"].IsEmpty())
                {
                    sql += " and b.Name like '%" + queryParam["name"].ToString() + "%'";
                }

                var list = this.BaseRepository().FindList<Base_MaterialsEntity>(sql, pagination);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MaterialRequisitionEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Project_SubcontractId,
                t.OutTIme,
                t.OutMoney,
                t.Period,
                t.Llr,
                t.Gcbw,
                t.Llyt,
                t.OperatorId,
                t.ProjectID,
                t.Workflow_ID,
                t.Remark,t.AuditStatus,t.CreationDate 
                ");
                strSql.Append("  FROM MaterialRequisition t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.OutTIme >= @startTime AND t.OutTIme <= @endTime ) ");
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }

                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Project_SubcontractId"].IsEmpty())
                {
                    dp.Add("Project_SubcontractId", queryParam["Project_SubcontractId"].ToString(), DbType.String);
                    strSql.Append(" AND t.Project_SubcontractId = @Project_SubcontractId ");
                }
                if (!queryParam["Llr"].IsEmpty())
                {
                    dp.Add("Llr", "%" + queryParam["Llr"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Llr Like @Llr ");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                if (!queryParam["Gcbw"].IsEmpty())
                {
                    dp.Add("Gcbw", "%" + queryParam["Gcbw"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Gcbw Like @Gcbw ");
                }
                if (!queryParam["Llyt"].IsEmpty())
                {
                    dp.Add("Llyt", "%" + queryParam["Llyt"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Llyt Like @Llyt ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<MaterialRequisitionEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MaterialRequisitionDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MaterialRequisitionDetailsEntity> GetMaterialRequisitionDetailsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MaterialRequisitionDetailsEntity>(t => t.MaterialRequisitionID == keyValue);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MaterialRequisitionEntity> GetProjectMaterialRequisitions(string projectId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(projectId))
                {
                    return this.BaseRepository().FindList<MaterialRequisitionEntity>(i => i.AuditStatus=="2");
                }
                else
                {
                    return this.BaseRepository().FindList<MaterialRequisitionEntity>(i=>i.ProjectID==projectId);
                }
                
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取MaterialRequisitionDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MaterialRequisitionDetailsEntity> GetMaterialRequisitionDetailsListConnectionMaterials(string keyValue)
        {
            try
            {
                var strSql = new StringBuilder();

                strSql.Append(" select mrd.*,");
                strSql.Append(" (select bmu.ID from Base_Materials as bm join Base_MaterialsUnit as bmu on bm.UnitId = bmu.ID where bm.ID = mrd.Base_MaterialsId) as Unit");
                strSql.Append(" from MaterialRequisitionDetails as mrd");
                strSql.Append(" where mrd.MaterialRequisitionID = '" + keyValue + "'");

                var list = this.BaseRepository().FindList<MaterialRequisitionDetailsEntity>(strSql.ToString());

                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取MaterialRequisition表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MaterialRequisitionEntity GetMaterialRequisitionEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MaterialRequisitionEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MaterialRequisitionDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MaterialRequisitionDetailsEntity GetMaterialRequisitionDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MaterialRequisitionDetailsEntity>(t => t.MaterialRequisitionID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            List<string> messageList = new List<string>();
            try
            {
                MaterialRequisitionEntity materialRequisitionEntity = db.FindEntity<MaterialRequisitionEntity>(keyValue);
                if (materialRequisitionEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                materialRequisitionEntity.AuditStatus = "4";
                db.Update<MaterialRequisitionEntity>(materialRequisitionEntity);

                IEnumerable<MaterialRequisitionDetailsEntity> materialRequisitionDetailsEntityList = db.FindList<MaterialRequisitionDetailsEntity>(m => m.MaterialRequisitionID == keyValue);
                foreach (var item in materialRequisitionDetailsEntityList)
                {
                    List<string> itemMessageList = new List<string>();
                    Base_WarehouseEntity base_WarehouseEntity = db.FindEntity<Base_WarehouseEntity>(item.Base_WarehouseId);
                    if (base_WarehouseEntity == null)
                        messageList.Add($"仓库不存在");

                    if (itemMessageList.Count() > 0)
                        continue;

                    Base_WarehouseDetailsEntity base_WarehouseDetailsEntity = db.FindEntity<Base_WarehouseDetailsEntity>(m => m.Fk_WarehouseId == item.Base_WarehouseId && m.Fk_MaterialId == item.Base_MaterialsId);

                    base_WarehouseDetailsEntity.Quantity += item.Quantity;
                    base_WarehouseDetailsEntity.TotalMoney += Math.Round(item.Price.Value * item.Quantity.Value, 4);
                    if (base_WarehouseDetailsEntity.Quantity > 0)
                        base_WarehouseDetailsEntity.Price1 = Math.Round(base_WarehouseDetailsEntity.TotalMoney.Value / base_WarehouseDetailsEntity.Quantity.Value, 4);
                    else
                        base_WarehouseDetailsEntity.Price1 = 0;
                    db.Update<Base_WarehouseDetailsEntity>(base_WarehouseDetailsEntity);

                    if (!base_WarehouseEntity.OccupyMoney.HasValue)
                        base_WarehouseEntity.OccupyMoney = 0;
                    base_WarehouseEntity.OccupyMoney += Math.Round(item.Price.Value * item.Quantity.Value, 4);
                    db.Update<Base_WarehouseEntity>(base_WarehouseEntity);


                }

                IEnumerable<MaterialRequisitionInStorageDetailsEntity> materialRequisitionInStorageDetailsList = db.FindList<MaterialRequisitionInStorageDetailsEntity>(m => m.Fk_MaterialRequisitionId == materialRequisitionEntity.ID);
                foreach (var item in materialRequisitionInStorageDetailsList)
                {
                    Purchase_InStorageDetailsEntity purchase_InStorageDetails = db.FindEntity<Purchase_InStorageDetailsEntity>(m => m.ID == item.Fk_InStorageDetailsId);
                    if (purchase_InStorageDetails != null)
                    {
                        purchase_InStorageDetails.IsAllOut = false;
                        purchase_InStorageDetails.OutQuantity -= item.Quantity;
                        db.Update<Purchase_InStorageDetailsEntity>(purchase_InStorageDetails);
                    }
                }
                db.Delete<MaterialRequisitionInStorageDetailsEntity>(materialRequisitionInStorageDetailsList.ToList());
                string tempMessage = "";
                if (messageList.Count() > 0)
                {
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }
                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            List<string> messageList = new List<string>();
            try
            {
                MaterialRequisitionEntity materialRequisitionEntity = db.FindEntity<MaterialRequisitionEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (materialRequisitionEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                materialRequisitionEntity.AuditStatus = "2";
                db.Update<MaterialRequisitionEntity>(materialRequisitionEntity);

                IEnumerable<MaterialRequisitionDetailsEntity> materialRequisitionDetailsEntityList = db.FindList<MaterialRequisitionDetailsEntity>(m => m.MaterialRequisitionID == keyValue);
                foreach (var item in materialRequisitionDetailsEntityList)
                {
                    List<string> itemMessageList = new List<string>();
                    Base_WarehouseEntity base_WarehouseEntity = db.FindEntity<Base_WarehouseEntity>(item.Base_WarehouseId);
                    if (base_WarehouseEntity == null)
                        itemMessageList.Add($"仓库不存在");

                    if (!string.IsNullOrEmpty(materialRequisitionEntity.Project_SubcontractId))
                    {
                        Project_SubcontractMaterialsEntity project_SubcontractMaterialsEntity = db.FindEntity<Project_SubcontractMaterialsEntity>(m => m.ProjectSubcontractId == materialRequisitionEntity.Project_SubcontractId && m.Code == item.Code);
                        if (project_SubcontractMaterialsEntity == null)
                        {
                            itemMessageList.Add($"材料【{item.Name}】不在分包合同材料内");
                        }
                        else
                        {
                            if (project_SubcontractMaterialsEntity.Quantity < item.Quantity)
                            {
                                itemMessageList.Add($"材料【{item.Name}】领料数量超出分包合同材料数量");
                            }
                        }

                        decimal auditStatusQuantity = 0;
                        object o = db.FindObject($"select sum(a.Quantity) from MaterialRequisitionDetails a left join MaterialRequisition b on a.MaterialRequisitionID = b.id where b.Project_SubcontractId = '{materialRequisitionEntity.Project_SubcontractId}' and a.Base_MaterialsId = '{item.Base_MaterialsId}' and b.AuditStatus = '2'");
                        if (o != null)
                        {
                            decimal.TryParse(o.ToString(), out auditStatusQuantity);
                            if (auditStatusQuantity > 0 && project_SubcontractMaterialsEntity.Quantity < auditStatusQuantity + item.Quantity)
                                itemMessageList.Add($"材料【{item.Name}】已领取【{auditStatusQuantity}】,领料数量超出分包合同材料数量");
                        }
                    }

                    Base_WarehouseDetailsEntity base_WarehouseDetailsEntity = db.FindEntity<Base_WarehouseDetailsEntity>(m => m.Fk_WarehouseId == item.Base_WarehouseId && m.Fk_MaterialId == item.Base_MaterialsId);

                    if (base_WarehouseDetailsEntity == null || base_WarehouseDetailsEntity.Quantity < item.Quantity)
                    {
                        itemMessageList.Add($"材料【{item.Name}】库存不足");
                    }

                    if (itemMessageList.Count() > 0)
                    {
                        messageList.AddRange(itemMessageList);
                        continue;
                    }

                    base_WarehouseDetailsEntity.Quantity -= item.Quantity;
                    base_WarehouseDetailsEntity.TotalMoney -= Math.Round(item.Price.Value * item.Quantity.Value, 4);
                    if (base_WarehouseDetailsEntity.Quantity > 0)
                        base_WarehouseDetailsEntity.Price1 = Math.Round(base_WarehouseDetailsEntity.TotalMoney.Value / base_WarehouseDetailsEntity.Quantity.Value, 4);
                    else
                        base_WarehouseDetailsEntity.Price1 = 0;
                    db.Update<Base_WarehouseDetailsEntity>(base_WarehouseDetailsEntity);

                    if (!base_WarehouseEntity.OccupyMoney.HasValue)
                        base_WarehouseEntity.OccupyMoney = 0;
                    base_WarehouseEntity.OccupyMoney -= Math.Round(item.Price.Value * item.Quantity.Value, 4);
                    db.Update<Base_WarehouseEntity>(base_WarehouseEntity);


                    IEnumerable<Purchase_InStorageDetailsEntity> purchase_InStorageDetailsList = db.FindList<Purchase_InStorageDetailsEntity>(m => m.Base_MaterialsId == item.Base_MaterialsId && (!m.IsAllOut.HasValue || !m.IsAllOut.Value)).OrderBy(m => m.InTime);
                    decimal quantity = item.Quantity.Value;

                    foreach (var purchase_InStorageDetails in purchase_InStorageDetailsList)
                    {
                        Purchase_InStorageEntity purchase_InStorageEntity = db.FindEntity<Purchase_InStorageEntity>(m => m.ID == purchase_InStorageDetails.Purchase_InStorageId && m.AuditStatus == "2");
                        if (purchase_InStorageEntity == null)
                            continue;
                        if (purchase_InStorageEntity.Base_WarehouseId != item.Base_WarehouseId)
                            continue;

                        if (!purchase_InStorageDetails.OutQuantity.HasValue)
                            purchase_InStorageDetails.OutQuantity = 0;

                        decimal canOutQuantity = purchase_InStorageDetails.Quantity.Value - purchase_InStorageDetails.OutQuantity.Value;

                        decimal outQuantity = 0;
                        if (quantity >= canOutQuantity)
                        {
                            outQuantity = canOutQuantity;
                            purchase_InStorageDetails.IsAllOut = true;
                            purchase_InStorageDetails.OutQuantity = canOutQuantity;
                            db.Update<Purchase_InStorageDetailsEntity>(purchase_InStorageDetails);
                        }
                        else
                        {
                            outQuantity = quantity;
                            purchase_InStorageDetails.OutQuantity = quantity;
                            db.Update<Purchase_InStorageDetailsEntity>(purchase_InStorageDetails);
                        }

                        db.Insert<MaterialRequisitionInStorageDetailsEntity>(new MaterialRequisitionInStorageDetailsEntity()
                        {
                            Fk_InStorageId = purchase_InStorageDetails.Purchase_InStorageId,
                            Fk_InStorageDetailsId = purchase_InStorageDetails.ID,
                            Fk_MaterialRequisitionId = item.MaterialRequisitionID,
                            Fk_MaterialRequisitionDetailsId = item.ID,
                            ID = Guid.NewGuid().ToString(),
                            Quantity = outQuantity,
                            InStorageInTime = purchase_InStorageDetails.InTime
                        });

                        quantity -= canOutQuantity;

                        if (quantity <= 0)
                            break;
                    }
                }

                string tempMessage = "";

                if (messageList.Count() > 0)
                {
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };

            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var materialRequisitionEntity = GetMaterialRequisitionEntity(keyValue);
                db.Delete<MaterialRequisitionEntity>(t => t.ID == keyValue);
                db.Delete<MaterialRequisitionDetailsEntity>(t => t.MaterialRequisitionID == materialRequisitionEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MaterialRequisitionEntity entity, List<MaterialRequisitionDetailsEntity> materialRequisitionDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var materialRequisitionEntityTmp = GetMaterialRequisitionEntity(keyValue);
                    entity.Modify(keyValue);
                    if (entity.OutTIme.HasValue)
                        entity.Period = Convert.ToInt32(entity.OutTIme.Value.ToString("yyyyMM"));
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var MaterialRequisitionDetailsUpdateList = materialRequisitionDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in MaterialRequisitionDetailsUpdateList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.UnitName = base_MaterialsUnitEntity.Name;
                        }

                        db.Update(item);
                    }
                    var MaterialRequisitionDetailsInserList = materialRequisitionDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in MaterialRequisitionDetailsInserList)
                    {
                        item.Create(item.ID);

                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.UnitName = base_MaterialsUnitEntity.Name;
                        }

                        item.MaterialRequisitionID = materialRequisitionEntityTmp.ID;
                        item.Project_SubcontractId = materialRequisitionEntityTmp.Project_SubcontractId;

                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    if (entity.OutTIme.HasValue)
                        entity.Period = Convert.ToInt32(entity.OutTIme.Value.ToString("yyyyMM"));
                    UserInfo userInfo = LoginUserInfo.Get();

                    db.Insert(entity);
                    foreach (MaterialRequisitionDetailsEntity item in materialRequisitionDetailsList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.UnitName = base_MaterialsUnitEntity.Name;
                        }

                        item.MaterialRequisitionID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
