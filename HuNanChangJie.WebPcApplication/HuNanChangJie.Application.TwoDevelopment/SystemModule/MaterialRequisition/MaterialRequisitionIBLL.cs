﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-21 18:46
    /// 描 述：领料单
    /// </summary>
    public interface MaterialRequisitionIBLL
    {
        #region  获取数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 获取材料档案
        /// <summary>
        /// <param name="subcontractId">分包合同</param>
        /// <returns></returns>
        IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination,string queryJson, string subcontractId, string warehouseId);
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MaterialRequisitionEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取项目领料单
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        IEnumerable<MaterialRequisitionEntity> GetProjectMaterialRequisitions(string projectId);

        /// <summary>
        /// 获取MaterialRequisitionDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MaterialRequisitionDetailsEntity> GetMaterialRequisitionDetailsList(string keyValue);

        IEnumerable<MaterialRequisitionDetailsEntity> GetMaterialRequisitionDetailsListConnectionMaterials(string keyValue);

        /// <summary>
        /// 获取MaterialRequisition表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MaterialRequisitionEntity GetMaterialRequisitionEntity(string keyValue);
        /// <summary>
        /// 获取MaterialRequisitionDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MaterialRequisitionDetailsEntity GetMaterialRequisitionDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MaterialRequisitionEntity entity,List<MaterialRequisitionDetailsEntity> materialRequisitionDetailsList,string deleteList,string type);
        #endregion

    }
}
