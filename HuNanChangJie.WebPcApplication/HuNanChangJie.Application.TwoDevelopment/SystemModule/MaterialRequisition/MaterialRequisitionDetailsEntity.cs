﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-21 18:46
    /// 描 述：领料单
    /// </summary>
    public class MaterialRequisitionDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 分包合同Id
        /// </summary>
        [Column("PROJECT_SUBCONTRACTID")]
        public string Project_SubcontractId { get; set; }
        /// <summary>
        /// 领料单Id
        /// </summary>
        [Column("MATERIALREQUISITIONID")]
        public string MaterialRequisitionID { get; set; }
        /// <summary>
        /// 材料ID
        /// </summary>
        [Column("BASE_MATERIALSID")]
        public string Base_MaterialsId { get; set; }
        /// <summary>
        /// 材料编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODEL")]
        public string Model { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary>
        /// 领料期间
        /// </summary>
        [Column("PERIOD")]
        public int? Period { get; set; }
        /// <summary>
        /// 材料名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 发出仓库
        /// </summary>
        [Column("BASE_WAREHOUSEID")]
        public string Base_WarehouseId { get; set; }
        /// <summary>
        /// 出库单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
        /// <summary>
        /// 出库总价
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// 出库数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 实时库存
        /// </summary>
        [Column("INVENTORY")]
        [DecimalPrecision(18, 4)]
        public decimal? Inventory { get; set; }

        /// <summary>
        /// 领料上限
        /// </summary>
        [Column("MAXQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? MaxQuantity { get; set; }

        /// <summary>
        /// 出库金额
        /// </summary>
        [Column("OUTMONEY")]
        [DecimalPrecision(18, 4)]
        public decimal? OutMoney { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        /// <summary>
        ///计量单位
        /// </summary>
        [Column("UNITNAME")]
        public string UnitName
        {
            get; set;
        }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }

        /// <summary>
        ///
        /// </summary>
        [NotMapped]
        public string Unit
        {
            get; set;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

