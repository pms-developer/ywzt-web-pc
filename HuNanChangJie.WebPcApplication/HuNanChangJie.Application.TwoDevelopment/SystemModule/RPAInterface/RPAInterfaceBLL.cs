﻿using System;
using System.Data;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAInterface
{
    /// <summary>
    /// 根据平台名返回各平台的帐单数据,
    /// </summary>
    /// <param name="platFormName"></param>
    /// <param name="beingDate"></param>
    /// <param name="endDate"></param>
    /// <returns></returns>
    public class RPAInterfaceBLL : RPAInterfaceIBLL
    {
        public RPAInterfaceServices _RPAInterfaceServices = new RPAInterfaceServices();

        public DataTable GetData(string platFormName, DateTime beingDate, DateTime endDate)
        {
           var queryConfig= InitQueryParmByPlatForm(platFormName, beingDate,endDate);

            string sql = $"select * from {queryConfig.TableName} where 1=1 and {queryConfig.ColumnName}>={queryConfig.BegingDate} and {queryConfig.ColumnName}<{queryConfig.EndDate}";

            return _RPAInterfaceServices.GetDate(sql);
        }

        private QueryConfig InitQueryParmByPlatForm(string platFormName, DateTime beingDate, DateTime endDate)
        {
            var config= new QueryConfig();
            switch (platFormName)
            {
                case "XZ_JD":
                    config.TableName = "jdbilling";
                    config.ColumnName = "BillingDate";
                    config.BegingDate = beingDate.ToString("yyyyMMdd");
                    config.EndDate = endDate.ToString("yyyyMMdd");
                    break;
                default:                    
                    break;
            }

            return config;
        }
    }

    public class QueryConfig
    {
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string BegingDate {get; set; }
        public string EndDate { get; set; }
    }
}
