﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAJob;
using System;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 14:27
    /// 描 述：RPA对外接口
    /// </summary>
    public interface RPAInterfaceIBLL
    {
        #region  获取数据

        /// <summary>
        /// 根据平台名返回各平台的帐单数据,
        /// </summary>
        /// <param name="platFormName">平台名</param>
        /// <param name="beingDate">帐单开始日期</param>
        /// <param name="endDate">帐单结束日期</param>
        /// <returns></returns>
        DataTable GetData(string platFormName, DateTime beingDate, DateTime endDate);
            
        #endregion

    }
}
