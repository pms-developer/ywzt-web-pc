﻿using HuNanChangJie.DataBase.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAInterface
{
    public class RPAInterfaceServices: RepositoryFactory
    {
        /// <summary>
        /// 返回DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable GetDate(string sql)
        {
            var result = this.BaseRepository().FindTable(sql);
            return result;
        }


    }
}
