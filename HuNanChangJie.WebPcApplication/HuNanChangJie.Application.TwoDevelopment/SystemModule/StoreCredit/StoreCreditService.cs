﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-22 19:48
    /// 描 述：退料单
    /// </summary>
    public class StoreCreditService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据
        public IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination, string materialRequisitionId)
        {
            try
            {
                string sql  = $"select  a.id Warranty,a.Base_WarehouseId CostType,b.ID,b.Code,b.Name,b.Model,b.Origin,b.BrandId,b.UnitId, a.Price,a.Quantity MaxQuantity ,'领料单数量限制' LimitDesc  from MaterialRequisitionDetails a left join Base_Materials b on a.Base_MaterialsId = b.id where a.materialrequisitionid='{materialRequisitionId}'";// where c.Quantity>0
               

                var list = this.BaseRepository().FindList<Base_MaterialsEntity>(sql, pagination);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<StoreCreditEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.MaterialRequisitionId,
                t.BackTime,
                t.BackMoney,
                t.Remark,
                t.Workflow_ID,
                t.AuditStatus,t.CreationDate
                ");
                strSql.Append("  FROM StoreCredit t join MaterialRequisition as mr on t.MaterialRequisitionId=mr.id ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.BackTime >= @startTime AND t.BackTime <= @endTime ) ");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }

                if (!queryParam["MaterialRequisitionId"].IsEmpty())
                {
                    dp.Add("MaterialRequisitionId", queryParam["MaterialRequisitionId"].ToString(), DbType.String);
                    strSql.Append(" AND t.MaterialRequisitionId = @MaterialRequisitionId ");
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND mr.ProjectID = @ProjectID ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate"; 
                    pagination.sord = "DESC";
                }
                var list =this.BaseRepository().FindList<StoreCreditEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取StoreCreditDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<StoreCreditDetailsEntity> GetStoreCreditDetailsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<StoreCreditDetailsEntity>(t=>t.StoreCreditId == keyValue );
                var query = from item in list  select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取StoreCreditDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<StoreCreditDetailsEntity> GetStoreCreditDetailsListConnMaterials(string keyValue)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(" select scd.*,");
                strSql.Append(" (select bmu.ID from Base_Materials as bm join Base_MaterialsUnit as bmu on bm.UnitId = bmu.ID where bm.ID = scd.Base_MaterialsId) as Unit");
                strSql.Append(" from StoreCreditDetails as scd");
                strSql.Append(" where scd.StoreCreditId = '" + keyValue + "'");

                var list = this.BaseRepository().FindList<StoreCreditDetailsEntity>(strSql.ToString());

                return list;

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        /// <summary>
        /// 获取StoreCredit表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public StoreCreditEntity GetStoreCreditEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<StoreCreditEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取StoreCreditDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public StoreCreditDetailsEntity GetStoreCreditDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<StoreCreditDetailsEntity>(t=>t.StoreCreditId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            List<string> messageList = new List<string>();
            try
            {
                StoreCreditEntity storeCreditEntity = db.FindEntity<StoreCreditEntity>(keyValue);
                if (storeCreditEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                storeCreditEntity.AuditStatus = "4";
                db.Update<StoreCreditEntity>(storeCreditEntity);

                IEnumerable<StoreCreditDetailsEntity> storeCreditDetailsEntityList = db.FindList<StoreCreditDetailsEntity>(m => m.StoreCreditId == keyValue);
                foreach (var item in storeCreditDetailsEntityList)
                {
                    List<string> itemMessageList = new List<string>();
                    Base_WarehouseEntity base_WarehouseEntity = db.FindEntity<Base_WarehouseEntity>(item.Base_WarehouseId);
                    if (base_WarehouseEntity == null)
                        itemMessageList.Add($"仓库不存在");

                    Base_WarehouseDetailsEntity base_WarehouseDetailsEntity = db.FindEntity<Base_WarehouseDetailsEntity>(m => m.Fk_WarehouseId == item.Base_WarehouseId && m.Fk_MaterialId == item.Base_MaterialsId);

                    if (base_WarehouseDetailsEntity == null || base_WarehouseDetailsEntity.Quantity < item.Quantity)
                    {
                        itemMessageList.Add($"材料【{item.Name}】库存不足");
                    }

                    if (itemMessageList.Count() > 0)
                    {
                        messageList.AddRange(itemMessageList);
                        continue;
                    }

                    base_WarehouseDetailsEntity.Quantity -= item.Quantity;
                    base_WarehouseDetailsEntity.TotalMoney -= item.TotalPrice;
                    if (base_WarehouseDetailsEntity.Quantity > 0)
                        base_WarehouseDetailsEntity.Price1 = Math.Round(base_WarehouseDetailsEntity.TotalMoney.Value / base_WarehouseDetailsEntity.Quantity.Value, 4);
                    else
                        base_WarehouseDetailsEntity.Price1 = 0;
                    db.Update<Base_WarehouseDetailsEntity>(base_WarehouseDetailsEntity);

                    if (!base_WarehouseEntity.OccupyMoney.HasValue)
                        base_WarehouseEntity.OccupyMoney = 0;
                    base_WarehouseEntity.OccupyMoney -= item.TotalPrice;
                    db.Update<Base_WarehouseEntity>(base_WarehouseEntity);

                    IEnumerable<Purchase_InStorageDetailsEntity> purchase_InStorageDetailsList = db.FindList<Purchase_InStorageDetailsEntity>(m => m.Base_MaterialsId == item.Base_MaterialsId && (!m.IsAllOut.HasValue || !m.IsAllOut.Value)).OrderBy(m => m.InTime);
                    decimal quantity = item.Quantity.Value;

                    foreach (var purchase_InStorageDetails in purchase_InStorageDetailsList)
                    {
                        Purchase_InStorageEntity purchase_InStorageEntity = db.FindEntity<Purchase_InStorageEntity>(m => m.ID == purchase_InStorageDetails.Purchase_InStorageId && m.AuditStatus == "2");
                        if (purchase_InStorageEntity == null)
                            continue;

                        if (purchase_InStorageEntity.Base_WarehouseId != item.Base_WarehouseId)
                            continue;

                        if (!purchase_InStorageDetails.OutQuantity.HasValue)
                            purchase_InStorageDetails.OutQuantity = 0;

                        decimal canOutQuantity = purchase_InStorageDetails.Quantity.Value - purchase_InStorageDetails.OutQuantity.Value;

                        decimal outQuantity = 0;
                        if (quantity >= canOutQuantity)
                        {
                            outQuantity = canOutQuantity;
                            purchase_InStorageDetails.IsAllOut = true;
                            purchase_InStorageDetails.OutQuantity = canOutQuantity;
                            db.Update<Purchase_InStorageDetailsEntity>(purchase_InStorageDetails);
                        }
                        else
                        {
                            outQuantity = quantity;
                            purchase_InStorageDetails.OutQuantity = quantity;
                            db.Update<Purchase_InStorageDetailsEntity>(purchase_InStorageDetails);
                        }

                        MaterialRequisitionDetailsEntity materialRequisitionDetailsEntity = db.FindEntity<MaterialRequisitionDetailsEntity>(m => m.ID == item.MaterialRequisitionDetailsId);
                        
                        db.Insert<MaterialRequisitionInStorageDetailsEntity>(new MaterialRequisitionInStorageDetailsEntity()
                        {
                            Fk_InStorageId = purchase_InStorageDetails.Purchase_InStorageId,
                            Fk_InStorageDetailsId = purchase_InStorageDetails.ID,
                            Fk_MaterialRequisitionId = materialRequisitionDetailsEntity.MaterialRequisitionID,
                            Fk_MaterialRequisitionDetailsId = item.MaterialRequisitionDetailsId,
                            ID = Guid.NewGuid().ToString(),
                            Quantity = outQuantity,
                            InStorageInTime = purchase_InStorageDetails.InTime
                        });

                        quantity -= canOutQuantity;

                        if (quantity <= 0)
                            break;
                    }
                }
                string tempMessage = "";
                if (messageList.Count() > 0)
                {
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }
                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message =ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            List<string> messageList = new List<string>();
            try
            {
                StoreCreditEntity storeCreditEntity = db.FindEntity<StoreCreditEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (storeCreditEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                storeCreditEntity.AuditStatus = "2";
                db.Update<StoreCreditEntity>(storeCreditEntity);

                IEnumerable<StoreCreditDetailsEntity> storeCreditDetailsEntityList = db.FindList<StoreCreditDetailsEntity>(m => m.StoreCreditId == keyValue);
                foreach (var item in storeCreditDetailsEntityList)
                {
                    List<string> itemMessageList = new List<string>();
                    Base_WarehouseEntity base_WarehouseEntity = db.FindEntity<Base_WarehouseEntity>(item.Base_WarehouseId);
                    if (base_WarehouseEntity == null)
                        itemMessageList.Add($"仓库不存在");


                    MaterialRequisitionDetailsEntity materialRequisitionDetailsEntity = db.FindEntity<MaterialRequisitionDetailsEntity>(m => m.ID == item.MaterialRequisitionDetailsId);
                    if (materialRequisitionDetailsEntity == null || materialRequisitionDetailsEntity.Quantity < item.Quantity)
                        itemMessageList.Add($"退料数量不能大于领料数量");

                    Base_WarehouseDetailsEntity base_WarehouseDetailsEntity = db.FindEntity<Base_WarehouseDetailsEntity>(m => m.Fk_WarehouseId == item.Base_WarehouseId && m.Fk_MaterialId == item.Base_MaterialsId);

                    if (base_WarehouseDetailsEntity == null)
                    {
                        itemMessageList.Add($"材料【{item.Name}】记录异常");
                    }
                    if (itemMessageList.Count() > 0)
                    {
                        messageList.AddRange(itemMessageList);
                        continue;
                    }
                    base_WarehouseDetailsEntity.Quantity += item.Quantity;
                    base_WarehouseDetailsEntity.TotalMoney += item.TotalPrice;
                    if (base_WarehouseDetailsEntity.Quantity > 0)
                        base_WarehouseDetailsEntity.Price1 = Math.Round(base_WarehouseDetailsEntity.TotalMoney.Value / base_WarehouseDetailsEntity.Quantity.Value, 4);
                    else
                        base_WarehouseDetailsEntity.Price1 = 0;
                    db.Update<Base_WarehouseDetailsEntity>(base_WarehouseDetailsEntity);

                    if (!base_WarehouseEntity.OccupyMoney.HasValue)
                        base_WarehouseEntity.OccupyMoney = 0;
                    base_WarehouseEntity.OccupyMoney += item.TotalPrice;
                    db.Update<Base_WarehouseEntity>(base_WarehouseEntity);

                    decimal tuiQuantity = item.Quantity.Value;

                    IEnumerable<MaterialRequisitionInStorageDetailsEntity> materialRequisitionInStorageDetailsList = db.FindList<MaterialRequisitionInStorageDetailsEntity>(m => m.Fk_MaterialRequisitionDetailsId == item.MaterialRequisitionDetailsId && m.Fk_MaterialRequisitionId == storeCreditEntity.MaterialRequisitionId).OrderByDescending(m=>m.InStorageInTime);

                    List<MaterialRequisitionInStorageDetailsEntity> delete = new List<MaterialRequisitionInStorageDetailsEntity>();
                    List<MaterialRequisitionInStorageDetailsEntity> update = new List<MaterialRequisitionInStorageDetailsEntity>();

                    foreach (var materialRequisitionInStorageDetails in materialRequisitionInStorageDetailsList)
                    {
                        Purchase_InStorageDetailsEntity purchase_InStorageDetails = db.FindEntity<Purchase_InStorageDetailsEntity>(m => m.ID == materialRequisitionInStorageDetails.Fk_InStorageDetailsId);

                        if (tuiQuantity > materialRequisitionInStorageDetails.Quantity)
                        {
                            if (purchase_InStorageDetails != null)
                            {
                                purchase_InStorageDetails.IsAllOut = false;
                                purchase_InStorageDetails.OutQuantity -= materialRequisitionInStorageDetails.Quantity;
                                db.Update<Purchase_InStorageDetailsEntity>(purchase_InStorageDetails);
                            }

                            delete.Add(materialRequisitionInStorageDetails);
                        }
                        else
                        {
                            if (purchase_InStorageDetails != null)
                            {
                                purchase_InStorageDetails.IsAllOut = false;
                                purchase_InStorageDetails.OutQuantity -= tuiQuantity;
                                db.Update<Purchase_InStorageDetailsEntity>(purchase_InStorageDetails);
                            }
                            materialRequisitionInStorageDetails.Quantity -= tuiQuantity;
                            update.Add(materialRequisitionInStorageDetails);
                        }

                        tuiQuantity -= materialRequisitionInStorageDetails.Quantity.Value;
                        if (tuiQuantity <= 0)
                            break;
                    }

                    if (update.Count() > 0)
                        db.Update<MaterialRequisitionInStorageDetailsEntity>(update);


                    if (delete.Count()>0)
                        db.Delete<MaterialRequisitionInStorageDetailsEntity>(delete);
                }

                string tempMessage = "";
                if (messageList.Count() > 0)
                {
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }
                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }


        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var storeCreditEntity = GetStoreCreditEntity(keyValue); 
                db.Delete<StoreCreditEntity>(t=>t.ID == keyValue);
                db.Delete<StoreCreditDetailsEntity>(t=>t.StoreCreditId == storeCreditEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, StoreCreditEntity entity,List<StoreCreditDetailsEntity> storeCreditDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var storeCreditEntityTmp = GetStoreCreditEntity(keyValue); 
                    entity.Modify(keyValue);
                    if (entity.BackTime.HasValue)
                        entity.Period = Convert.ToInt32(entity.BackTime.Value.ToString("yyyyMM"));
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var StoreCreditDetailsUpdateList= storeCreditDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in StoreCreditDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var StoreCreditDetailsInserList= storeCreditDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in StoreCreditDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.StoreCreditId = storeCreditEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    if (entity.BackTime.HasValue)
                        entity.Period = Convert.ToInt32(entity.BackTime.Value.ToString("yyyyMM"));
                    db.Insert(entity);
                    foreach (StoreCreditDetailsEntity item in storeCreditDetailsList)
                    {
                        item.StoreCreditId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
