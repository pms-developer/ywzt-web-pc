﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-22 19:48
    /// 描 述：退料单
    /// </summary>
    public interface StoreCreditIBLL
    {
        #region  获取数据
        IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination, string materialRequisitionId);
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<StoreCreditEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取StoreCreditDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<StoreCreditDetailsEntity> GetStoreCreditDetailsList(string keyValue);

        IEnumerable<StoreCreditDetailsEntity> GetStoreCreditDetailsListConnMaterials(string keyValue);


        /// <summary>
        /// 获取StoreCredit表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        StoreCreditEntity GetStoreCreditEntity(string keyValue);
        /// <summary>
        /// 获取StoreCreditDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        StoreCreditDetailsEntity GetStoreCreditDetailsEntity(string keyValue);
        #endregion

        #region  提交数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, StoreCreditEntity entity,List<StoreCreditDetailsEntity> storeCreditDetailsList,string deleteList,string type);
        #endregion

    }
}
