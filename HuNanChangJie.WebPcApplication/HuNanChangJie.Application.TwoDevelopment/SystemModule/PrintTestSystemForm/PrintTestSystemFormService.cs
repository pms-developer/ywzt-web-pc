﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-08-03 17:49
    /// 描 述：PrintTestSystemForm
    /// </summary>
    public class PrintTestSystemFormService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<PrintTestMianEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.UserName,
                t.Sex,
                t.Age,
                t.Phone,
                t.Address,
                t.Workflow_ID,
                t.AuditStatus
                ");
                strSql.Append("  FROM PrintTestMian t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                var list=this.BaseRepository().FindList<PrintTestMianEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestSubA表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<PrintTestSubAEntity> GetPrintTestSubAList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<PrintTestSubAEntity>(t=>t.PrintTestMianId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestSubB表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<PrintTestSubBEntity> GetPrintTestSubBList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<PrintTestSubBEntity>(t=>t.PrintTestMianId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestMian表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PrintTestMianEntity GetPrintTestMianEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<PrintTestMianEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestSubA表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PrintTestSubAEntity GetPrintTestSubAEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<PrintTestSubAEntity>(t=>t.PrintTestMianId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestSubB表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PrintTestSubBEntity GetPrintTestSubBEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<PrintTestSubBEntity>(t=>t.PrintTestMianId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var printTestMianEntity = GetPrintTestMianEntity(keyValue); 
                db.Delete<PrintTestMianEntity>(t=>t.ID == keyValue);
                db.Delete<PrintTestSubAEntity>(t=>t.PrintTestMianId == printTestMianEntity.ID);
                db.Delete<PrintTestSubBEntity>(t=>t.PrintTestMianId == printTestMianEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, PrintTestMianEntity entity,List<PrintTestSubAEntity> printTestSubAList,List<PrintTestSubBEntity> printTestSubBList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var printTestMianEntityTmp = GetPrintTestMianEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var PrintTestSubAUpdateList= printTestSubAList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in PrintTestSubAUpdateList)
                    {
                        db.Update(item);
                    }
                    var PrintTestSubAInserList= printTestSubAList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in PrintTestSubAInserList)
                    {
                        item.Create(item.ID);
                        item.PrintTestMianId = printTestMianEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                    
                    //没有生成代码 
                    var PrintTestSubBUpdateList= printTestSubBList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in PrintTestSubBUpdateList)
                    {
                        db.Update(item);
                    }
                    var PrintTestSubBInserList= printTestSubBList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in PrintTestSubBInserList)
                    {
                        item.Create(item.ID);
                        item.PrintTestMianId = printTestMianEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (PrintTestSubAEntity item in printTestSubAList)
                    {
                        item.PrintTestMianId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (PrintTestSubBEntity item in printTestSubBList)
                    {
                        item.PrintTestMianId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
