﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-08-03 17:49
    /// 描 述：PrintTestSystemForm
    /// </summary>
    public interface PrintTestSystemFormIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<PrintTestMianEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取PrintTestSubA表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<PrintTestSubAEntity> GetPrintTestSubAList(string keyValue);
        /// <summary>
        /// 获取PrintTestSubB表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<PrintTestSubBEntity> GetPrintTestSubBList(string keyValue);
        /// <summary>
        /// 获取PrintTestMian表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        PrintTestMianEntity GetPrintTestMianEntity(string keyValue);
        /// <summary>
        /// 获取PrintTestSubA表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        PrintTestSubAEntity GetPrintTestSubAEntity(string keyValue);
        /// <summary>
        /// 获取PrintTestSubB表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        PrintTestSubBEntity GetPrintTestSubBEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, PrintTestMianEntity entity,List<PrintTestSubAEntity> printTestSubAList,List<PrintTestSubBEntity> printTestSubBList,string deleteList,string type);
        #endregion

    }
}
