﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-08-03 17:49
    /// 描 述：PrintTestSystemForm
    /// </summary>
    public class PrintTestSystemFormBLL : PrintTestSystemFormIBLL
    {
        private PrintTestSystemFormService printTestSystemFormService = new PrintTestSystemFormService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<PrintTestMianEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return printTestSystemFormService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestSubA表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<PrintTestSubAEntity> GetPrintTestSubAList(string keyValue)
        {
            try
            {
                return printTestSystemFormService.GetPrintTestSubAList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestSubB表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<PrintTestSubBEntity> GetPrintTestSubBList(string keyValue)
        {
            try
            {
                return printTestSystemFormService.GetPrintTestSubBList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestMian表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PrintTestMianEntity GetPrintTestMianEntity(string keyValue)
        {
            try
            {
                return printTestSystemFormService.GetPrintTestMianEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestSubA表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PrintTestSubAEntity GetPrintTestSubAEntity(string keyValue)
        {
            try
            {
                return printTestSystemFormService.GetPrintTestSubAEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PrintTestSubB表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PrintTestSubBEntity GetPrintTestSubBEntity(string keyValue)
        {
            try
            {
                return printTestSystemFormService.GetPrintTestSubBEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                printTestSystemFormService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, PrintTestMianEntity entity,List<PrintTestSubAEntity> printTestSubAList,List<PrintTestSubBEntity> printTestSubBList,string deleteList,string type)
        {
            try
            {
                printTestSystemFormService.SaveEntity(keyValue, entity,printTestSubAList,printTestSubBList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
