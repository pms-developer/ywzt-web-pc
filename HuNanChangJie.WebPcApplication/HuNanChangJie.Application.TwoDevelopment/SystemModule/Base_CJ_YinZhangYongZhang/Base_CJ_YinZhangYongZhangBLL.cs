﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-02 21:00
    /// 描 述：用章申请
    /// </summary>
    public class Base_CJ_YinZhangYongZhangBLL : Base_CJ_YinZhangYongZhangIBLL
    {
        private Base_CJ_YinZhangYongZhangService base_CJ_YinZhangYongZhangService = new Base_CJ_YinZhangYongZhangService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_YinZhangYongZhangEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return base_CJ_YinZhangYongZhangService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_YinZhangYongZhang表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_YinZhangYongZhangEntity GetBase_CJ_YinZhangYongZhangEntity(string keyValue)
        {
            try
            {
                return base_CJ_YinZhangYongZhangService.GetBase_CJ_YinZhangYongZhangEntity(keyValue);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                base_CJ_YinZhangYongZhangService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_YinZhangYongZhangEntity entity,string deleteList,string type)
        {
            try
            {
                base_CJ_YinZhangYongZhangService.SaveEntity(keyValue, entity,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0029");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
