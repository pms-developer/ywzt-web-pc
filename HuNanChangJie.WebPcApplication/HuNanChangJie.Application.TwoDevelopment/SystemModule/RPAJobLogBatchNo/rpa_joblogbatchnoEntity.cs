﻿
using HuNanChangJie.Util; 
using System; 
using System.ComponentModel.DataAnnotations.Schema; 
namespace HuNanChangJie.Application.TwoDevelopment.SystemModule

{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2022-09-12 10:15 
    /// 描 述：RPAJobLogBatchNo 
    /// </summary> 
    public class rpa_joblogbatchnoEntity
    {
        #region  实体成员 
        /// <summary> 
        /// Id 
        /// </summary> 
        /// <returns></returns> 
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ID")]
        public int? Id { get; set; }
        /// <summary> 
        /// Rpa_jobLogId 
        /// </summary> 
        /// <returns></returns> 
        [Column("RPA_JOBLOGID")]
        public int? Rpa_jobLogId { get; set; }
        /// <summary> 
        /// BatchNo 
        /// </summary> 
        /// <returns></returns> 
        [Column("BATCHNO")]
        public string BatchNo { get; set; }
        /// <summary> 
        /// CreateTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATETIME")]
        public DateTime? CreateTime { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(int? keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
    }
}
