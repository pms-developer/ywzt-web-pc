﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2022-09-12 10:15 
    /// 描 述：RPAJobLogBatchNo 
    /// </summary> 
    public interface RPAJobLogBatchNoIBLL
    {
        #region  获取数据 

        /// <summary> 
        /// 获取列表数据 
        /// <summary> 
        /// <returns></returns> 
        IEnumerable<rpa_joblogbatchnoEntity> GetList(string queryJson);
        /// <summary> 
        /// 获取列表分页数据 
        /// <param name="pagination">分页参数</param> 
        /// <summary> 
        /// <returns></returns> 
        IEnumerable<rpa_joblogbatchnoEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary> 
        /// 获取实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        rpa_joblogbatchnoEntity GetEntity(string keyValue);
        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        void DeleteEntity(int keyValue);
        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        void SaveEntity(int keyValue, rpa_joblogbatchnoEntity entity);
        #endregion

    }
}
