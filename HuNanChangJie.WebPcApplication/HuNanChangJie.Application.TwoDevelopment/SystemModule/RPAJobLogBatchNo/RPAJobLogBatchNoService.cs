﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2022-09-12 10:15 
    /// 描 述：RPAJobLogBatchNo 
    /// </summary> 
    public class RPAJobLogBatchNoService : RepositoryFactory
    {
        #region  构造函数和属性 

        private string fieldSql;
        public RPAJobLogBatchNoService()
        {
            fieldSql = @" 
                t.Id, 
                t.Rpa_jobLogId, 
                t.BatchNo, 
                t.CreateTime 
            ";
        }
        #endregion

        #region  获取数据 

        /// <summary> 
        /// 获取列表数据 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<rpa_joblogbatchnoEntity> GetList(string queryJson)
        {
            try
            {
                //参考写法 
                //var queryParam = queryJson.ToJObject(); 
                // 虚拟参数 
                //var dp = new DynamicParameters(new { }); 
                //dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime); 
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM rpa_joblogbatchno t ");
                return this.BaseRepository("sql").FindList<rpa_joblogbatchnoEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取列表分页数据 
        /// <param name="pagination">分页参数</param> 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<rpa_joblogbatchnoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM rpa_joblogbatchno t ");
                return this.BaseRepository("sql").FindList<rpa_joblogbatchnoEntity>(strSql.ToString(), pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public rpa_joblogbatchnoEntity GetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository("sql").FindEntity<rpa_joblogbatchnoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void DeleteEntity(int keyValue)
        {
            try
            {
                this.BaseRepository("sql").Delete<rpa_joblogbatchnoEntity>(t => t.Id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void SaveEntity(int keyValue, rpa_joblogbatchnoEntity entity)
        {
            try
            {
                if (keyValue!=0)
                {
                    entity.Modify(keyValue);
                    this.BaseRepository("sql").Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository("sql").Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}