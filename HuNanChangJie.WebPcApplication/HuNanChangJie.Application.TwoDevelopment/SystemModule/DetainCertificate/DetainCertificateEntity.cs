﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.SystemModule

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 09:28
    /// 描 述：一证多押
    /// </summary>
    public class DetainCertificateEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 证照ID
        /// </summary>
        /// <returns></returns>
        [Column("ZHENGZHAOID")]
        public string ZhengZhaoId { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        /// <returns></returns>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        /// <returns></returns>
        [Column("STARTDATE")]
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        /// <returns></returns>
        [Column("ENDDATE")]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 是否归还
        /// </summary>
        /// <returns></returns>
        [Column("ISBACK")]
        public bool? IsBack { get; set; }
        /// <summary>
        /// 归还时间
        /// </summary>
        /// <returns></returns>
        [Column("BACKDATE")]
        public DateTime? BackDate { get; set; }
        /// <summary>
        /// 押证时长
        /// </summary>
        /// <returns></returns>
        [Column("DURATION")]
        public string Duration { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

