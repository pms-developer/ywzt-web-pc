﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 15:19
    /// 描 述：取数公式设置
    /// </summary>
    public interface BaseFormulaSettingsIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_FormulaSettingsEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_FormulaSettings表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_FormulaSettingsEntity GetBase_FormulaSettingsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_FormulaSettingsEntity entity,string deleteList,string type);
        IEnumerable<Base_FormulaSettingsEntity> GetList();
        #endregion

    }
}
