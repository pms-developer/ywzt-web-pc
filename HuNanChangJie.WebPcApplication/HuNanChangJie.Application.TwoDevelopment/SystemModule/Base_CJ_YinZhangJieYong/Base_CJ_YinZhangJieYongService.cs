﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-02 20:10
    /// 描 述：借章管理
    /// </summary>
    public class Base_CJ_YinZhangJieYongService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_YinZhangJieYongEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.BianHao,
                t.ID,
                t.CreationName,
                t.ShenQingRen,
                t.ShenQingBuMeng,
                t.Project_ID,
                t.JieYongZhang,
                t.KaiShiShiJian,
                t.GuiHuanShiJian,
                t.BeiZhu,
                t.CreationDate,
                t.AuditStatus,t.Workflow_ID
                ");
                strSql.Append("  FROM Base_CJ_YinZhangJieYong t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["CreationName"].IsEmpty())
                {
                    dp.Add("CreationName", "%" + queryParam["CreationName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.CreationName Like @CreationName ");
                }
                if (!queryParam["ShenQingRen"].IsEmpty())
                {
                    dp.Add("ShenQingRen",queryParam["ShenQingRen"].ToString(), DbType.String);
                    strSql.Append(" AND t.ShenQingRen = @ShenQingRen ");
                }
                var list=this.BaseRepository().FindList<Base_CJ_YinZhangJieYongEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_YinZhangJieYong表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_YinZhangJieYongEntity GetBase_CJ_YinZhangJieYongEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_CJ_YinZhangJieYongEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_CJ_YinZhangJieYongEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_YinZhangJieYongEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
