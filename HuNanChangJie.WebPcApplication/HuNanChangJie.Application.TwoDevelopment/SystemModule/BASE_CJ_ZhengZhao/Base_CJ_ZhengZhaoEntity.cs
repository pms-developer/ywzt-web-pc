﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-13 20:34
    /// 描 述：证照管理
    /// </summary>
    public class Base_CJ_ZhengZhaoEntity 
    {
        #region  实体成员

        [NotMapped]
        public EditType EditType{ get; set; }

        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 证照属性
        /// </summary>
        [Column("FK_ZHENGZHAOMINGCHENG")]
        public string fk_zhengzhaomingcheng { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        [Column("ZZ_BIANHAO")]
        public string zz_bianhao { get; set; }
        /// <summary>
        /// zz_leixing
        /// </summary>
        [Column("ZZ_LEIXING")]
        public string zz_leixing { get; set; }
        /// <summary>
        /// zz_guanlianjigou
        /// </summary>
        [Column("ZZ_GUANLIANJIGOU")]
        public string zz_guanlianjigou { get; set; }
        /// <summary>
        /// zz_guanlianxiangmu
        /// </summary>
        [Column("ZZ_GUANLIANXIANGMU")]
        public string zz_guanlianxiangmu { get; set; }
        /// <summary>
        /// 专业
        /// </summary>
        [Column("ZZ_ZHUANYE")]
        public string zz_zhuanye { get; set; }
        /// <summary>
        /// 等级/工种
        /// </summary>
        [Column("ZZ_DENGJIGONGZHONG")]
        public string zz_dengjigongzhong { get; set; }
        /// <summary>
        /// 是否公司员工
        /// </summary>
        [Column("ZZ_SHIFOUGONGSIYUANGONG")]
        public int? zz_shifougongsiyuangong { get; set; }
        /// <summary>
        /// 公司员工
        /// </summary>
        [Column("ZZ_GONGSIYUANGONG")]
        public string zz_gongsiyuangong { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        [Column("ZZ_XINMING")]
        public string zz_xinming { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        [Column("ZZ_XINGBIE")]
        public string zz_xingbie { get; set; }
        /// <summary>
        /// 身份证号码
        /// </summary>
        [Column("ZZ_SHENFENGZHENG")]
        public string zz_shenfengzheng { get; set; }
        /// <summary>
        /// 发证机构
        /// </summary>
        [Column("ZZ_FAZHENGJIGOU")]
        public string zz_fazhengjigou { get; set; }
        /// <summary>
        /// 发证日期
        /// </summary>
        [Column("ZZ_FAZHENGRIQI")]
        public DateTime? zz_fazhengriqi { get; set; }

        /// <summary>
        /// 社保开始日期
        /// </summary>
        [Column("ZZ_SOCIALSECURITYSTART")]
        public DateTime? zz_socialsecuritystart { get; set; }

        /// <summary>
        /// 社保结束日期
        /// </summary>
        [Column("ZZ_SOCIALSECURITYEND")]
        public DateTime? zz_socialsecurityend { get; set; }

        /// <summary>
        /// 是否有原件
        /// </summary>
        [Column("IS_MASTER")]
        public int? is_master { get; set; }

        /// <summary>
        /// 注册状态
        /// </summary>
        [Column("ZZ_ZHUCEZHUANTAI")]
        public string zz_zhucezhuantai { get; set; }
        /// <summary>
        /// 有效期 起
        /// </summary>
        [Column("ZZ_YOUXIAOQI1")]
        public DateTime? zz_youxiaoqi1 { get; set; }
        /// <summary>
        /// 有效期  止
        /// </summary>
        [Column("ZZ_YOUXIAOQI2")]
        public DateTime? zz_youxiaoqi2 { get; set; }
        /// <summary>
        /// 下次年检日期
        /// </summary>
        [Column("ZZ_NIANJIANRIQI")]
        public DateTime? zz_nianjianriqi { get; set; }
        [Column("ZZ_SHENFENGZHENGGUOQIRIQI")]
        public DateTime? zz_shenfengzhengguoqiriqi { get; set; }

        [Column("ZZ_SHENFENGZHENGGUOQIRIQI1")]
        public DateTime? zz_shenfengzhengguoqiriqi1 { get; set; }
        /// <summary>
        /// 存放位置
        /// </summary>
        [Column("ZZ_CUNFANGWEIZHI")]
        public string zz_cunfangweizhi { get; set; }
        /// <summary>
        /// 所属机构
        /// </summary>
        [Column("ZZ_SUOSHUJIGOU")]
        public string zz_suoshujigou { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        [Column("ZZ_GUANJIANZI")]
        public string zz_guanjianzi { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("ZZ_BEIZHU")]
        public string zz_beizhu { get; set; }
        /// <summary>
        /// 是否支持一证多押
        /// </summary>
        [Column("ZZ_YIZHENGDUOYA")]
        public int? zz_yizhengduoya { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        [Column("ZZ_LIANXIFANGSHI")]
        public string zz_lianxifangshi { get; set; }
        /// <summary>
        /// 引进日期
        /// </summary>
        [Column("ZZ_YINJINRIQI")]
        public DateTime? zz_yinjinriqi { get; set; }
        /// <summary>
        /// 转出日期
        /// </summary>
        [Column("ZZ_ZHUANCHURIQI")]
        public DateTime? zz_zhuanchuriqi { get; set; }
        /// <summary>
        /// 引进渠道
        /// </summary>
        [Column("ZZ_YINJINQUDAO")]
        public string zz_yinjinqudao { get; set; }
        /// <summary>
        /// 社保情况
        /// </summary>
        [Column("ZZ_SHEBAOQINGKUAN")]
        public int? zz_shebaoqingkuan { get; set; }
        /// <summary>
        /// 社保金额
        /// </summary>
        [Column("ZZ_SHEBAOJINE")]
        [DecimalPrecision(18, 4)]
        public decimal? zz_shebaojine { get; set; }
        /// <summary>
        /// 存档编号
        /// </summary>
        [Column("ZZ_CUNDANGBIANHAO")]
        public string zz_cundangbianhao { get; set; }
        /// <summary>
        /// zz_zhuangtai
        /// </summary>
        [Column("ZZ_ZHUANGTAI")]
        public string zz_zhuangtai { get; set; }
        /// <summary>
        /// zz_jiechuzhuangtai
        /// </summary>
        [Column("ZZ_JIECHUZHUANGTAI")]
        public string zz_jiechuzhuangtai { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SortCode")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECT_ID")]
        public string Project_ID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        [Column("ZZ_DENGJI")]
        /// <summary>
        /// 证书等级
        /// </summary>
        public string zz_dengji { get; set; }
        /// <summary>
        /// 起止日期
        /// </summary>
        [Column("StarStopTime")]
        public DateTime? StarStopTime { get; set; }
        /// <summary>
        /// 是否可以上岗
        /// </summary>
        [Column("WhetherToWorkOrNot")]
        public string WhetherToWorkOrNot { get; set; }
        /// <summary>
        /// 证件去向
        /// </summary>
        [Column("CredentialsWhereabouts")]
        public string CredentialsWhereabouts { get; set; }
        

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.zz_zhuangtai = "正常";
            this.zz_jiechuzhuangtai = "正常";
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.zz_zhuangtai = "正常";
            this.zz_jiechuzhuangtai = "正常";
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        /// <summary>
        /// 证书名称
        /// </summary>
        [NotMapped]
        public string zzmc_name { get; set; }

        [NotMapped]
        public string shiyongxiangmu { get; set; }

        [NotMapped]
        public string jiechuxiangmu { get; set; }


        [NotMapped]
        public int? zz { get; set; }
        [NotMapped]
        public int? nj { get; set; }
        [NotMapped]
        public int? sfz { get; set; }
        #endregion
    }
}

