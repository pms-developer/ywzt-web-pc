﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;
using System.Linq;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-13 20:34
    /// 描 述：证照管理
    /// </summary>
    public class BASE_CJ_ZhengZhaoService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
t.is_master,
zz_socialsecuritystart,
zz_socialsecurityend,
                t.fk_zhengzhaomingcheng,
                t.zz_bianhao,
                t.zz_yizhengduoya,
                t.zz_leixing,
                t.zz_guanlianjigou,
                t.zz_zhuanye,
                t.zz_dengjigongzhong,
                t.zz_gongsiyuangong,
                t.zz_xinming,
                t.zz_xingbie,
                t.zz_shenfengzheng,
                t.ModificationDate,
                t.zz_fazhengjigou,
                t.zz_fazhengriqi,
                t.zz_zhucezhuantai,
                t.zz_youxiaoqi1,
                t.zz_youxiaoqi2,
                t.zz_nianjianriqi,
                t.zz_cunfangweizhi,
                t.zz_guanjianzi,
                t.zz_beizhu,
                t.zz_yinjinqudao,
                t.zz_yinjinriqi,
                t.zz_lianxifangshi,
                t.zz_zhuanchuriqi,
                t.zz_shebaoqingkuan,
                t.zz_cundangbianhao,
                t.zz_zhuangtai,
                t.CreationDate,
                t.AuditStatus,
                t.Workflow_ID,
                t.WhetherToWorkOrNot,
                t.CredentialsWhereabouts,
                t.zz_jiechuzhuangtai,t1.fk_dataitemdetail,t.zz_shenfengzhengguoqiriqi,t.zz_shenfengzhengguoqiriqi1,'' as shiyongxiangmu,'' as jiechuxiangmu
                ");
                strSql.Append("  FROM Base_CJ_ZhengZhao t  left join Base_CJ_ZhenZhaoMingCheng t1 on t.fk_zhengzhaomingcheng = t1.zzmc_id ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.zz_youxiaoqi2 >= @startTime AND t.zz_youxiaoqi2 <= @endTime ) ");
                }
                int deep = 2;
                if (!queryParam["deep"].IsEmpty())
                {
                    int.TryParse(queryParam["deep"].ToString(), out deep);
                }

                if (!queryParam["fk_zhengzhaomingcheng"].IsEmpty())
                {
                    if (deep == 1)
                    {
                        dp.Add("fk_dataitemdetail", queryParam["fk_zhengzhaomingcheng"].ToString(), DbType.String);
                        strSql.Append(" AND t1.fk_dataitemdetail = @fk_dataitemdetail ");
                    }
                    else
                    {
                        dp.Add("fk_zhengzhaomingcheng", queryParam["fk_zhengzhaomingcheng"].ToString(), DbType.String);
                        strSql.Append(" AND t.fk_zhengzhaomingcheng = @fk_zhengzhaomingcheng ");
                    }
                }
                if (!queryParam["zz_xinming"].IsEmpty())
                {
                    dp.Add("zz_xinming", "%" + queryParam["zz_xinming"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.zz_xinming Like @zz_xinming ");
                }
                if (!queryParam["zz_bianhao"].IsEmpty())
                {
                    dp.Add("zz_bianhao", "%" + queryParam["zz_bianhao"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.zz_bianhao Like @zz_bianhao ");
                }
                if (!queryParam["zz_shenfengzheng"].IsEmpty())
                {
                    dp.Add("zz_shenfengzheng", "%" + queryParam["zz_shenfengzheng"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.zz_shenfengzheng Like @zz_shenfengzheng ");
                }
                if (!queryParam["zz_lianxifangshi"].IsEmpty())
                {
                    dp.Add("zz_lianxifangshi", "%" + queryParam["zz_lianxifangshi"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.zz_lianxifangshi Like @zz_lianxifangshi ");
                }
                if (!queryParam["zz_yinjinqudao"].IsEmpty())
                {
                    dp.Add("zz_yinjinqudao", "%" + queryParam["zz_yinjinqudao"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.zz_yinjinqudao Like @zz_yinjinqudao ");
                }
                if (!queryParam["zz_cunfangweizhi"].IsEmpty())
                {
                    dp.Add("zz_cunfangweizhi", "%" + queryParam["zz_cunfangweizhi"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.zz_cunfangweizhi Like @zz_cunfangweizhi ");
                }

                var list = this.BaseRepository().FindList<Base_CJ_ZhengZhaoEntity>(strSql.ToString(), dp, pagination);
                foreach (var item in list)
                {
                    var gcmcList = this.BaseRepository().FindList<string>($"select b.gcmc from ZJ_sgxkrmmx a left join ZJ_sgxkrm b on a.sgxkID = b.id  left join ZJ_gcys c on b.id = c.gcmc where b.auditstatus = '2' and isnull(c.auditstatus,'0')!='2' and zhengzhaoid = '{item.ID}' order by b.creationdate desc");
                    foreach (var itemGcmc in gcmcList)
                    {
                        if (!string.IsNullOrEmpty(item.shiyongxiangmu))
                            item.shiyongxiangmu += ",";

                        item.shiyongxiangmu += itemGcmc;
                    }
                }

                //需要修改
                foreach (var item in list)
                {
                    string sql = "select b.xmmc from ZJ_toubiaoryuanmx a";
                    sql += " left join ZJ_toubiaoryuan b on a.tbid = b.ID  where b.auditstatus = '2'";
                    sql += " and zhengzhaoid = '" + item.ID + "' and b.xmmc not in(";
                    sql += " select top 1 b.xmmc from toubiaoryguihuanmx a";
                    sql += " left join toubiaoryguihuan b on a.tbmxid = b.ID  where b.auditstatus = '2'";
                    sql += " and zhengzhaoid = '" + item.ID + "' order by b.creationdate desc";
                    sql += " ) order by b.creationdate desc";

                    var gcmcList = this.BaseRepository().FindList<string>(sql);
                    foreach (var itemGcmc in gcmcList)
                    {
                        if (!string.IsNullOrEmpty(item.jiechuxiangmu))
                            item.jiechuxiangmu += ",";

                        item.jiechuxiangmu += itemGcmc;
                    }
                }

                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetZhengZhaoListByJieYongId(string id)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select c.*,d.zzmc_name from Base_CJ_ZhenZhaoJieYong a left join Base_CJ_ZhenZhaoJieYongMingXi b on a.id = b.fk_JieYongId left join Base_CJ_ZhengZhao c on b.fk_ZhenZhaoId = c.id   left join Base_CJ_ZhenZhaoMingCheng d on c.fk_zhengzhaomingcheng = d.zzmc_id where isnull(b.isguihuan,0) = 0 and a.id = '" + id + "' and b.id is not null");
                return BaseRepository().FindList<Base_CJ_ZhengZhaoEntity>(sb.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetExpireZhengZhaoList(string sql)
        {
            try
            {
                return BaseRepository().FindList<Base_CJ_ZhengZhaoEntity>(sql);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetZhengZhaoList()
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.zzmc_name
                            from BASE_CJ_ZhengZhao as a left join Base_CJ_ZhenZhaoMingCheng as b
                            on a.fk_zhengzhaomingcheng = b.zzmc_id
                            where a.zz_yizhengduoya = 1 ");
                return BaseRepository().FindList<Base_CJ_ZhengZhaoEntity>(sb.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetZhengZhaoList2(string queryJson)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.zzmc_name
                            from BASE_CJ_ZhengZhao as a left join Base_CJ_ZhenZhaoMingCheng as b
                            on a.fk_zhengzhaomingcheng = b.zzmc_id
                            where 1 = 1 and isnull(zz_zhuangtai,'正常')='正常' and ((isnull(zz_jiechuzhuangtai,'正常')!='借出') or (isnull(zz_jiechuzhuangtai,'正常')='借出' and zz_yizhengduoya = 1))");

                if (!string.IsNullOrEmpty(queryJson))
                {
                    var queryParam = queryJson.ToJObject();

                    if (!queryParam["projectid"].IsEmpty())
                    {
                        object isSameProvince = BaseRepository().FindObject(" select 1 from base_cj_project a left join base_company b on a.company_id = b.F_CompanyId where a.ProvinceID = isnull(b.F_ProvinceId, '430000') and a.id = '" + queryParam["projectid"].ToString() + "'");
                        if (isSameProvince != null)
                        {
                            sb.Append(" and isnull(zz_jiechuzhuangtai,'正常')!='借出'");
                        }
                    }
                    if (!queryParam["sword"].IsEmpty())
                    {
                        string sword = queryParam["sword"].ToString().Trim().Replace("，", ",");
                        string[] swords = sword.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        if (swords.Length > 0)
                        {
                            string temp = "";
                            temp += " and ( ";
                            for (int i = 0; i < swords.Length; i++)
                            {
                                temp += $"  ( b.zzmc_name like '%{swords[i]}%' or a.zz_bianhao like '%{swords[i]}%'  or a.zz_zhuanye like '%{swords[i]}%'  or a.zz_shenfengzheng like '%{swords[i]}%'  or a.zz_xinming like '%{swords[i]}%' ) ";

                                if (!string.IsNullOrEmpty(temp) && i != swords.Length - 1)
                                    temp += "  or ";
                            }
                            temp += " ) ";

                            sb.Append(temp);
                        }
                    }
                }
                return BaseRepository().FindList<Base_CJ_ZhengZhaoEntity>(sb.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取Base_CJ_ZhengZhao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_ZhengZhaoEntity GetBase_CJ_ZhengZhaoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_CJ_ZhengZhaoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取树形数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetSqlTree()
        {
            try
            {
                return this.BaseRepository().FindTable("select * from (select F_ItemId, F_ItemDetailId,F_ItemName,F_SortCode,1 deep from Base_DataItemDetail where F_itemId = '84deece0-d60a-402c-bcd9-3bbe2ea30bda' AND F_DeleteMark=0 union select '','84deece0-d60a-402c-bcd9-3bbe2ea30bda','所有',0,0 union select fk_dataitemdetail,zzmc_id,zzmc_name,zzmc_sort,2 from Base_CJ_ZhenZhaoMingCheng ) t order by F_SortCode");
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        #endregion

        #region  提交数据

        public void UpdateStatus(string id, string status)
        {
            try
            {
                Base_CJ_ZhengZhaoEntity model = this.BaseRepository().FindEntity<Base_CJ_ZhengZhaoEntity>(t => t.ID == id);

                model.zz_zhuangtai = status;

                this.BaseRepository().Update(model);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_CJ_ZhengZhaoEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_ZhengZhaoEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
