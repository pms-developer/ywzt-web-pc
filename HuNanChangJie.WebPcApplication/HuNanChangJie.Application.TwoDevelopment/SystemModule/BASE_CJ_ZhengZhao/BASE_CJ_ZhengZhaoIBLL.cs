﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-13 20:34
    /// 描 述：证照管理
    /// </summary>
    public interface BASE_CJ_ZhengZhaoIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_CJ_ZhengZhaoEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_CJ_ZhengZhao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_CJ_ZhengZhaoEntity GetBase_CJ_ZhengZhaoEntity(string keyValue);

        /// <summary>
        /// 获取满足过期提醒要求的证照列表
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <returns></returns>
        IEnumerable<Base_CJ_ZhengZhaoEntity> GetExpireZhengZhaoList(int remindday);

        List<TreeModel> GetTree(int deep);

        #endregion

        #region  提交数据

        void UpdateStatus(string id, string status);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_CJ_ZhengZhaoEntity entity,string deleteList,string type);
        IEnumerable<Base_CJ_ZhengZhaoEntity> GetZhengZhaoList();
        IEnumerable<Base_CJ_ZhengZhaoEntity> GetZhengZhaoList2(string queryJson);
        IEnumerable<Base_CJ_ZhengZhaoEntity> GetZhengZhaoListByJieYongId(string id);
        
        #endregion

    }
}
