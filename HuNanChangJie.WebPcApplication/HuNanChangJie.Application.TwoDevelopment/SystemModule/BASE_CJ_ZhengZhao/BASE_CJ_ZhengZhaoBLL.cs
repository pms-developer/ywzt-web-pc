﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-13 20:34
    /// 描 述：证照管理
    /// </summary>
    public class BASE_CJ_ZhengZhaoBLL : BASE_CJ_ZhengZhaoIBLL
    {
        private BASE_CJ_ZhengZhaoService bASE_CJ_ZhengZhaoService = new BASE_CJ_ZhengZhaoService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return bASE_CJ_ZhengZhaoService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_ZhengZhao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_ZhengZhaoEntity GetBase_CJ_ZhengZhaoEntity(string keyValue)
        {
            try
            {
                return bASE_CJ_ZhengZhaoService.GetBase_CJ_ZhengZhaoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取左侧树形数据
        /// <summary>
        /// <returns></returns>
        public List<TreeModel> GetTree(int deep=0)
        {
            try
            {
                DataTable list = bASE_CJ_ZhengZhaoService.GetSqlTree();
                List<TreeModel> treeList = new List<TreeModel>();
                foreach (DataRow item in list.Rows)
                {
                    int tempDeep = 0;
                    int.TryParse(item["deep"].ToString(),out tempDeep);
                    TreeModel node = new TreeModel
                    {
                        id = item["f_itemdetailid"].ToString(),
                        text = item["f_itemname"].ToString(),
                        value = item["f_itemdetailid"].ToString(),
                        showcheck = false,
                        checkstate = 0,
                        isexpand = deep  >= tempDeep,
                        parentId = item["f_itemid"].ToString()
                    };
                    treeList.Add(node);
                }
                return treeList.ToTree();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetZhengZhaoListByJieYongId(string id)
        {
            try
            {
               return bASE_CJ_ZhengZhaoService.GetZhengZhaoListByJieYongId(id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetExpireZhengZhaoList(int remindday)
        {
            try
            {
                string sql= string.Format(@"select a.ID,zz_youxiaoqi2,zz_nianjianriqi,zz_shenfengzhengguoqiriqi,b.zzmc_name,zz_xinming,
                                                                                            DATEDIFF(day,GETDATE(),zz_youxiaoqi2) as 'zz',
                                                                                            DATEDIFF(day,GETDATE(),zz_nianjianriqi) as 'nj',
                                                                                            DATEDIFF(day,GETDATE(),zz_shenfengzhengguoqiriqi) as 'sfz' 
                                                                                            from Base_CJ_ZhengZhao a left join Base_CJ_ZhenZhaoMingCheng b on a.fk_zhengzhaomingcheng=b.zzmc_id 
                                                                                            where DATEDIFF(day,GETDATE(),zz_youxiaoqi2)<={0} 
                                                                                            or DATEDIFF(day,GETDATE(),zz_nianjianriqi)<={0} 
                                                                                            or DATEDIFF(day,GETDATE(),zz_shenfengzhengguoqiriqi)<={0}", remindday);
                return bASE_CJ_ZhengZhaoService.GetExpireZhengZhaoList(sql);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public void UpdateStatus(string id, string status)
        {
            try
            {
                bASE_CJ_ZhengZhaoService.UpdateStatus(id, status);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
         
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                bASE_CJ_ZhengZhaoService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_ZhengZhaoEntity entity,string deleteList,string type)
        {
            try
            {
                bASE_CJ_ZhengZhaoService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetZhengZhaoList2(string queryJson)
        {
            try
            {
                return bASE_CJ_ZhengZhaoService.GetZhengZhaoList2(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
       
        public IEnumerable<Base_CJ_ZhengZhaoEntity> GetZhengZhaoList()
        {
            try
            {
                return bASE_CJ_ZhengZhaoService.GetZhengZhaoList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
