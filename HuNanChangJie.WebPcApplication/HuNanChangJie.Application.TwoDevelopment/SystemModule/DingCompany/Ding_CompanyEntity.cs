﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-05 10:23
    /// 描 述：钉钉上子公司源数据
    /// </summary>
    public class Ding_CompanyEntity : BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 部门ID
        /// </summary>
        [Column("DEPT_ID")]
        public string Dept_id { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 父部门ID
        /// </summary>
        [Column("PARENT_ID")]
        public string Parent_id { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// CreationDate
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// Creation_Id
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Dept_id = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.Dept_id = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Dept_id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

