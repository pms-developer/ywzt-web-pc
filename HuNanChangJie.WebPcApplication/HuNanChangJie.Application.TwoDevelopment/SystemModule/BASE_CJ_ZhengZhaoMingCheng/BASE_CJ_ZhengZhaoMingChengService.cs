﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-10-28 17:03
    /// 描 述：证照名称管理
    /// </summary>
    public class BASE_CJ_ZhengZhaoMingChengService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_ZhenZhaoMingChengEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.zzmc_id,
                t.zzmc_name,
                t.zzmc_isjieyong,
                t.zzmc_baoguanren,
                t.zzmc_sort
                ");
                strSql.Append("  FROM Base_CJ_ZhenZhaoMingCheng t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["zzmc_name"].IsEmpty())
                {
                    dp.Add("zzmc_name", "%" + queryParam["zzmc_name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.zzmc_name Like @zzmc_name ");
                }
                if (!queryParam["fk_dataitemdetail"].IsEmpty() && queryParam["fk_dataitemdetail"].ToString() != "84deece0-d60a-402c-bcd9-3bbe2ea30bda")
                {
                    dp.Add("fk_dataitemdetail", queryParam["fk_dataitemdetail"].ToString(), DbType.String);
                    strSql.Append(" AND t.fk_dataitemdetail = @fk_dataitemdetail ");
                }
                
                return this.BaseRepository().FindList<Base_CJ_ZhenZhaoMingChengEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_ZhenZhaoMingCheng表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_ZhenZhaoMingChengEntity GetBase_CJ_ZhenZhaoMingChengEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_CJ_ZhenZhaoMingChengEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取树形数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetSqlTree()
        {
            try
            {
                return this.BaseRepository().FindTable("select * from (select F_ItemId, F_ItemDetailId,F_ItemName,F_SortCode from Base_DataItemDetail where  F_DeleteMark = 0 and F_itemId = '84deece0-d60a-402c-bcd9-3bbe2ea30bda' union select '','84deece0-d60a-402c-bcd9-3bbe2ea30bda','所有',0 ) t order by F_SortCode ");
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_CJ_ZhenZhaoMingChengEntity>(t=>t.zzmc_id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_ZhenZhaoMingChengEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
