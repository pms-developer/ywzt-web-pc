﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-10-28 17:03
    /// 描 述：证照名称管理
    /// </summary>
    public class Base_CJ_ZhenZhaoMingChengEntity 
    {
        #region  实体成员
        /// <summary>
        /// zzmc_id
        /// </summary>
        [Column("ZZMC_ID")]
        public string zzmc_id { get; set; }
        /// <summary>
        /// 证照类型
        /// </summary>
        [Column("FK_DATAITEMDETAIL")]
        public string fk_dataitemdetail { get; set; }
        /// <summary>
        /// 证照名称
        /// </summary>
        [Column("ZZMC_NAME")]
        public string zzmc_name { get; set; }
        /// <summary>
        /// 保管人
        /// </summary>
        [Column("ZZMC_BAOGUANREN")]
        public string zzmc_baoguanren { get; set; }
        /// <summary>
        /// 是否允许借用
        /// </summary>
        [Column("ZZMC_ISJIEYONG")]
        public int? zzmc_isjieyong { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        [Column("ZZMC_SORT")]
        public int? zzmc_sort { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        [Column("ZZMC_OPERTIME")]
        public DateTime? zzmc_opertime { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        [Column("ZZMC_OPER")]
        public string zzmc_oper { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyvalue)
        {
            this.zzmc_id = keyvalue;

            if(!this.zzmc_sort.HasValue)
            this.zzmc_sort = 0;

            if (!this.zzmc_opertime.HasValue)
                this.zzmc_opertime = DateTime.Now;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.zzmc_id = keyValue;
            if (!this.zzmc_opertime.HasValue)
                this.zzmc_opertime = DateTime.Now;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

