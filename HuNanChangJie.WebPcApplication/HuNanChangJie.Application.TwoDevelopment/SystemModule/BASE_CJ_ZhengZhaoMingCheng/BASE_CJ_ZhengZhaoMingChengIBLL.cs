﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-10-28 17:03
    /// 描 述：证照名称管理
    /// </summary>
    public interface BASE_CJ_ZhengZhaoMingChengIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_CJ_ZhenZhaoMingChengEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_CJ_ZhenZhaoMingCheng表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_CJ_ZhenZhaoMingChengEntity GetBase_CJ_ZhenZhaoMingChengEntity(string keyValue);
        /// <summary>
        /// 获取左侧树形数据
        /// <summary>
        /// <returns></returns>
        List<TreeModel> GetTree();
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_CJ_ZhenZhaoMingChengEntity entity, string deleteList, string type);
        #endregion

    }
}
