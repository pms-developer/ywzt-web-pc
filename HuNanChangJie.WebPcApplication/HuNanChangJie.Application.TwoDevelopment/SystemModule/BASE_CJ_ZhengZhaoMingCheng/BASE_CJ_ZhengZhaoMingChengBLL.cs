﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-10-28 17:03
    /// 描 述：证照名称管理
    /// </summary>
    public class BASE_CJ_ZhengZhaoMingChengBLL : BASE_CJ_ZhengZhaoMingChengIBLL
    {
        private BASE_CJ_ZhengZhaoMingChengService bASE_CJ_ZhengZhaoMingChengService = new BASE_CJ_ZhengZhaoMingChengService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_ZhenZhaoMingChengEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return bASE_CJ_ZhengZhaoMingChengService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_ZhenZhaoMingCheng表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_ZhenZhaoMingChengEntity GetBase_CJ_ZhenZhaoMingChengEntity(string keyValue)
        {
            try
            {
                return bASE_CJ_ZhengZhaoMingChengService.GetBase_CJ_ZhenZhaoMingChengEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取左侧树形数据
        /// <summary>
        /// <returns></returns>
         public List<TreeModel> GetTree()
        {
            try
            {
                DataTable list = bASE_CJ_ZhengZhaoMingChengService.GetSqlTree();
                List<TreeModel> treeList = new List<TreeModel>();
                foreach (DataRow item in list.Rows)
                {
                    TreeModel node = new TreeModel
                    {
                        id = item["f_itemdetailid"].ToString(),
                        text = item["f_itemname"].ToString(),
                        value = item["f_itemdetailid"].ToString(),
                        showcheck = false,
                        checkstate = 0,
                        isexpand = true,
                        parentId = item["f_itemid"].ToString()
                    };
                    treeList.Add(node);                }
                return treeList.ToTree();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                bASE_CJ_ZhengZhaoMingChengService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_ZhenZhaoMingChengEntity entity, string deleteList, string type)
        {
            try
            {
                bASE_CJ_ZhengZhaoMingChengService.SaveEntity(keyValue, entity, deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
