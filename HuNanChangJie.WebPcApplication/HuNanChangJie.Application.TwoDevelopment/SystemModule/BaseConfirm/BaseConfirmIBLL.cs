﻿using HuNanChangJie.Util;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.BaseConfirm
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2021-01-28 16:43 
    /// 描 述：顶替 
    /// </summary> 
    public interface BaseConfirmIBLL
    {
        #region  获取数据 

        /// <summary> 
        /// 获取列表数据 
        /// <summary> 
        /// <returns></returns> 
        IEnumerable<Base_ConfirmEntity> GetList(string queryJson);
        /// <summary> 
        /// 获取列表分页数据 
        /// <param name="pagination">分页参数</param> 
        /// <summary> 
        /// <returns></returns> 
        IEnumerable<Base_ConfirmEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary> 
        /// 获取实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        Base_ConfirmEntity GetEntity(string keyValue);
        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        void DeleteEntity(string keyValue);
        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        void SaveEntity(string keyValue,string type, Base_ConfirmEntity entity);
        #endregion

    }
}