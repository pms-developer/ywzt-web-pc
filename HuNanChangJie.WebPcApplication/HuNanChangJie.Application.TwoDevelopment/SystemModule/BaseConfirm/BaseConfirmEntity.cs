﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.BaseConfirm
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2021-01-28 16:43 
    /// 描 述：顶替 
    /// </summary> 
    public class Base_ConfirmEntity
    {
        #region  实体成员 
        /// <summary> 
        /// ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("ID")]
        public string ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreationDate { get; set; }

        /// <summary> 
        /// 表单ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("BASESYSTEMFORMID")]
        public string BaseSystemFormId { get; set; }

        /// <summary>
        /// 数据库连接ID
        /// </summary>
        public string F_DataSourceId { get; set; }
        /// <summary> 
        /// 提示内容 
        /// </summary> 
        /// <returns></returns> 
        [Column("INFOMSG")]
        public string InfoMsg { get; set; }
        /// <summary> 
        /// 是否强制（true:强制提醒，即不允许保存，false:仅提醒，可以继续保存 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISFORCE")]
        public bool? IsForce { get; set; }
        /// <summary> 
        /// 取数公式表达式 
        /// </summary> 
        /// <returns></returns> 
        [Column("EXPRESSION")]
        public string Expression { get; set; }
        /// <summary> 
        /// 是否启用 
        /// </summary> 
        /// <returns></returns> 
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary> 
        /// 排序号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            Enabled = true;
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            Enabled = true;
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region 扩展字段
        /// <summary>
        /// 表单名称
        /// </summary>
        [NotMapped]
        public string BaseSystemFormName { get; set; }
        #endregion
    }
}
