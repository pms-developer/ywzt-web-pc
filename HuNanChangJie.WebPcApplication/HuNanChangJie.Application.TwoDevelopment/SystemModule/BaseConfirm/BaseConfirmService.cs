﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.BaseConfirm
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2021-01-28 16:43 
    /// 描 述：顶替 
    /// </summary> 
    public class BaseConfirmService : RepositoryFactory
    {
        #region  构造函数和属性 

        private string fieldSql;
        public BaseConfirmService()
        {
            fieldSql = @" 
                t.ID, 
                t.Name,
                t.BaseSystemFormId, 
                t.InfoMsg, 
                t.IsForce, 
                t.CreationDate,
                t.Expression, 
                t.Enabled, 
                t.F_DataSourceId,
                t.SortCode 
            ";
        }
        #endregion

        #region  获取数据 

        /// <summary> 
        /// 获取列表数据 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<Base_ConfirmEntity> GetList(string queryJson)
        {
            try
            {
                //参考写法 
                //var queryParam = queryJson.ToJObject(); 
                // 虚拟参数 
                //var dp = new DynamicParameters(new { }); 
                //dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime); 
                var strSql = new StringBuilder();
                strSql.Append("SELECT * from ( ");
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(",f.FormName as BaseSystemFormName ");
                strSql.Append(" FROM Base_Confirm t left join Base_SystemForm as f on t.BaseSystemFormId=f.Id  ");
                strSql.Append(" ) as t where 1=1 ");
                var dp = new DynamicParameters(new { });
                var queryParam = queryJson.ToJObject();
                if (!queryParam["formId"].IsEmpty())
                {
                    dp.Add("BaseSystemFormId", "%" + queryParam["formId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.BaseSystemFormId Like @BaseSystemFormId ");
                }
                var list= this.BaseRepository().FindList<Base_ConfirmEntity>(strSql.ToString(), dp);
                var quyer = from item in list orderby item.SortCode ascending select item;
                return quyer;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取列表分页数据 
        /// <param name="pagination">分页参数</param> 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<Base_ConfirmEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT * from ( ");
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(",f.FormName as BaseSystemFormName ");
                strSql.Append(" FROM Base_Confirm t left join Base_SystemForm as f on t.BaseSystemFormId=f.Id  ");
                strSql.Append(" ) as t where 1=1 ");
                var dp = new DynamicParameters(new { });
                var queryParam = queryJson.ToJObject();
                if (!queryParam["formId"].IsEmpty())
                {
                    dp.Add("BaseSystemFormId", "%" + queryParam["formId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.BaseSystemFormId Like @BaseSystemFormId ");
                }
                var list= this.BaseRepository().FindList<Base_ConfirmEntity>(strSql.ToString(), dp, pagination);
                var quyer = from item in list orderby item.SortCode ascending select item;
                return quyer; 
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public Base_ConfirmEntity GetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_ConfirmEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_ConfirmEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void SaveEntity(string keyValue,string type, Base_ConfirmEntity entity)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
