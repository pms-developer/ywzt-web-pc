﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-19 11:20
    /// 描 述：材料档案
    /// </summary>
    public class Base_MaterialsEntity : BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNITID")]
        public string UnitId { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRANDID")]
        public string BrandId { get; set; }
        /// <summary>
        /// 分类
        /// </summary>
        [Column("TYPEID")]
        public string TypeId { get; set; }
        /// <summary>
        /// 材料编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODEL")]
        public string Model { get; set; }
        /// <summary>
        /// 是否组合产品
        /// </summary>
        [Column("ORIGIN")]
        public string Origin { get; set; }
        /// <summary>
        /// 成本类别
        /// </summary>
        [Column("COSTTYPE")]
        public string CostType { get; set; }
        /// <summary>
        /// 保质期
        /// </summary>
        [Column("WARRANTY")]
        public string Warranty { get; set; }
        /// <summary>
        /// 保质期类型
        /// </summary>
        [Column("WARRANTYTYPE")]
        public string WarrantyType { get; set; }
        /// <summary>
        /// 参数说明
        /// </summary>
        [Column("PARAMETER")]
        public string Parameter { get; set; }
        /// <summary>
        /// 参数说明1
        /// </summary>
        [Column("PARAMETER1")]
        public string Parameter1 { get; set; }
        /// <summary>
        /// 参数说明2
        /// </summary>
        [Column("PARAMETER2")]
        public string Parameter2 { get; set; }
        /// <summary>
        /// 参数说明3
        /// </summary>
        [Column("PARAMETER3")]
        public string Parameter3 { get; set; }
        /// <summary>
        /// 参数说明4
        /// </summary>
        [Column("PARAMETER4")]
        public string Parameter4 { get; set; }
        /// <summary>
        /// 参数说明5
        /// </summary>
        [Column("PARAMETER5")]
        public string Parameter5 { get; set; }
        /// <summary>
        /// 材料名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column("STATE")]
        public int? State { get; set; }
        /// <summary>
        /// 权限组织
        /// </summary>
        [Column("AUTHORITYORGANIZATION")]
        public string AuthorityOrganization { get; set; }
        /// <summary>
        /// 是否项目专用
        /// </summary>
        [Column("ISPROJECT")]
        public bool? IsProject { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 采购员
        /// </summary>
        [Column("BUYER")]
        public string Buyer { get; set; }

        /// <summary>
        /// 采购交期
        /// </summary>
        [Column("LeadDay")]
        public int? LeadDay { get; set; }

        /// <summary>
        /// 采购成本
        /// </summary>
        [Column("PurchaseCost")]
        [DecimalPrecision(18, 4)]
        public decimal? PurchaseCost { get; set; }
        /// <summary>
        /// 采购备注
        /// </summary>
        [Column("PurchaseText")]
        public string PurchaseText { get; set; }
        /// <summary>
        /// 单品规格1
        /// </summary>
        [Column("SingleSpecOne")]
        [DecimalPrecision(18, 2)]
        public decimal? SingleSpecOne { get; set; }
        /// <summary>
        /// 单品规格2
        /// </summary>
        [Column("SingleSpecTwo")]
        [DecimalPrecision(18, 2)]
        public decimal? SingleSpecTwo { get; set; }
        /// <summary>
        /// 单品规格3
        /// </summary>
        [Column("SingleSpecThree")]
        [DecimalPrecision(18, 2)]
        public decimal? SingleSpecThree { get; set; }

        /// <summary>
        /// 单品净重
        /// </summary>
        [Column("SingleNetWeight")]
        [DecimalPrecision(18, 4)]
        public decimal? SingleNetWeight { get; set; }

        /// <summary>
        /// 净重单位
        /// </summary>
        [Column("SingleNetWeightUnit")]
        public string SingleNetWeightUnit { get; set; }

        /// <summary>
        /// 图片信息
        /// </summary>
        [Column("Imgs")]
        public string Imgs { get; set; }

        /// <summary>
        /// 单位加工费
        /// </summary>
        [Column("UnitProcessingFee")]
        public decimal? UnitProcessingFee { get; set; }

        /// <summary>
        /// 加工备注
        /// </summary>
        [Column("ProcessingText")]
        public string ProcessingText { get; set; }





        /// <summary>
        /// 价格系数
        /// </summary>
        [Column("PRICEFACTOR")]
        public string PriceFactor { get; set; }
        /// <summary>
        /// 预算成本价
        /// </summary>
        [Column("BUGETCOST")]
        [DecimalPrecision(18, 4)]
        public decimal? BugetCost { get; set; }
        /// <summary>
        /// 销售价
        /// </summary>
        [Column("SALEPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SalePrice { get; set; }
        /// <summary>
        /// 列表价
        /// </summary>
        [Column("LISTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ListPrice { get; set; }
        /// <summary>
        /// 人工费
        /// </summary>
        [Column("DIRECTLABOR")]
        [DecimalPrecision(18, 4)]
        public decimal? DirectLabor { get; set; }
        /// <summary>
        /// 机械费
        /// </summary>
        [Column("MACHINERYPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? MachineryPrice { get; set; }
        /// <summary>
        /// 成本科目
        /// </summary>
        [Column("SUBJECTCOST")]
        public string SubjectCost { get; set; }
        /// <summary>
        /// 是否直进直出属性
        /// </summary>
        [Column("ISOUTTHEFRONT")]
        public bool? IsOutTheFront { get; set; }
        /// <summary>
        /// 工作压力
        /// </summary>
        [Column("WORKSTRESS")]
        public string WorkStress { get; set; }
        /// <summary>
        /// 产品材质
        /// </summary>
        [Column("PRODUCTMATERIAL")]
        public string ProductMaterial { get; set; }

        /// <summary>
        /// 成本核算方式 1：移动加权平均法  2：加权平均法
        /// </summary>
        public int? CostAccountingType { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        [Column("Inventory")]
        [DecimalPrecision(18, 4)]
        public decimal? Inventory { get; set; }


        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段

        /// <summary>
        /// 产品图片
        /// </summary>
        [NotMapped]
        public string baseImg { get; set; }

        /// <summary>
        /// 产品图片id
        /// </summary>
        [NotMapped]
        public string baseImgId { get; set; }

        [NotMapped]
        public decimal Quantity { get; set; }
        [NotMapped]
        public decimal MaxQuantity { get; set; }

        [NotMapped]
        public string Base_Warehouse_Name { get; set; }


        [NotMapped]
        public string Base_Warehouse_ID { get; set; }


        [NotMapped]
        public string LimitDesc { get; set; }

        /// <summary>
        /// 单位名
        /// </summary>
        [NotMapped]
        public string UnitName { get; set; }

        /// <summary>
        /// 品牌名
        /// </summary>
        [NotMapped]
        public string BrandName { get; set; }

        /// <summary>
        /// 父id
        /// </summary>
        [NotMapped]
        public string ParentId { get; set; }


        /// <summary>
        /// 材料类别名
        /// </summary>
        [NotMapped]
        public string TypeName { get; set; }
        #endregion
    }
}

