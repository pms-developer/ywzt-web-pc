﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.SystemCommon;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
using HuNanChangJie.Application.Base.SystemModule;
using System.Web.Mvc;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-19 11:20
    /// 描 述：材料档案
    /// </summary>
    public class BaseMaterialsBLL : BaseMaterialsIBLL
    {
        private BaseMaterialsService baseMaterialsService = new BaseMaterialsService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_MaterialsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return baseMaterialsService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        public int checkNoCount(string kyeValue)
        {
            return baseMaterialsService.checkNoCount(kyeValue);
        }

        public IEnumerable<Base_MaterialsEntity> GetMaterials(XqPagination pagination,string projectId,string queryJson)
        {
            try
            {
                return baseMaterialsService.GetMaterials(pagination, projectId, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        public IEnumerable<Base_MaterialsEntity> GetMaterialsList(XqPagination pagination, string typeId,string projectId, string queryJson)
        {
            try
            {
                return baseMaterialsService.GetMaterialsList(pagination,typeId, projectId, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Base_Materials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_MaterialsEntity GetBase_MaterialsEntity(string keyValue)
        {
            try
            {
                return baseMaterialsService.GetBase_MaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取箱规表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meio_product_box_gaugeEntity> Getmeio_product_box_gaugeList(string keyValue)
        {
            try
            {
                return baseMaterialsService.Getmeio_product_box_gaugeList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取产品辅料数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meio_product_pick_accessoriesEntity> Getmeio_product_pick_accessoriesList(string keyValue)
        {
            try
            {
                return baseMaterialsService.Getmeio_product_pick_accessoriesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取组合产品包含单品数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meio_product_pick_comboEntity> Getmeio_product_pick_comboList(string keyValue)
        {
            try
            {
                return baseMaterialsService.Getmeio_product_pick_comboList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue, string logList)
        {
            try
            {
                baseMaterialsService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_MaterialsEntity entity
            , List<meio_product_box_gaugeEntity> meio_product_box_gaugeList
            , List<meio_product_pick_accessoriesEntity> meio_product_pick_accessoriesList
            , List<meio_product_pick_comboEntity> meio_product_pick_comboList
            , string deleteList
            , string logList
            , string type)
        {
            try
            {
                    baseMaterialsService.SaveEntity(keyValue, entity
                        , meio_product_box_gaugeList
                        , meio_product_pick_accessoriesList
                        , meio_product_pick_comboList
                        , deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CL_Code");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsSameName(string name)
        {
            var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "MaterialsNameIsDuplicateName");
            if (configInfo == null) return false;
            var flag = configInfo.Value;
            if (flag)
            {
                try
                {
                    return baseMaterialsService.CheckIsSameName(name);
                }
                catch (Exception ex)
                {
                    if (ex is ExceptionEx)
                    {
                        throw;
                    }
                    else
                    {
                        throw ExceptionEx.ThrowBusinessException(ex);
                    }
                }
            }
            else
                return false;
        }

        #endregion

    }
}
