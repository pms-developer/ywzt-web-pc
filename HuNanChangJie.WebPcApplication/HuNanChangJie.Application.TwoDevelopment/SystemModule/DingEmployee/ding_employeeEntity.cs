﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-06 18:49
    /// 描 述：钉钉员工信息
    /// </summary>
    public class ding_employeeEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 钉钉Userid
        /// </summary>
        [Column("USERID")]
        public string userid { get; set; }
        /// <summary>
        /// 钉钉Name
        /// </summary>
        [Column("NAME")]
        public string name { get; set; }
        /// <summary>
        /// 用户在当前开发者企业账号范围内的唯一标识
        /// </summary>
        [Column("UNIONID")]
        public string unionid { get; set; }
        /// <summary>
        /// 是否为企业的管理员：
        /// </summary>
        [Column("ADMIN")]
        public string admin { get; set; }
        /// <summary>
        /// 头像地址
        /// </summary>
        [Column("AVATAR")]
        public string avatar { get; set; }
        /// <summary>
        /// 手机号码。
        /// </summary>
        [Column("MOBILE")]
        public string mobile { get; set; }
        /// <summary>
        /// 分机号
        /// </summary>
        [Column("TELEPHONE")]
        public string telephone { get; set; }
        /// <summary>
        /// 职位
        /// </summary>
        [Column("TITLE")]
        public string title { get; set; }
        /// <summary>
        /// 员工邮箱
        /// </summary>
        [Column("EMAIL")]
        public string email { get; set; }
        /// <summary>
        /// 员工的企业邮箱
        /// </summary>
        [Column("ORG_EMAIL")]
        public string org_email { get; set; }
        /// <summary>
        /// 办公地点
        /// </summary>
        [Column("WORK_PLACE")]
        public string work_place { get; set; }
        /// <summary>
        /// 员工工号
        /// </summary>
        [Column("JOB_NUMBER")]
        public string job_number { get; set; }
        /// <summary>
        /// 入职时间，Unix时间戳，单位毫秒。
        /// </summary>
        [Column("HIRED_DATE")]
        public string hired_date { get; set; }
        /// <summary>
        /// 是否激活了钉钉
        /// </summary>
        [Column("ACTIVE")]
        public string active { get; set; }
        /// <summary>
        /// 查询本组织专属帐号时可获得昵称
        /// </summary>
        [Column("NICKNAME")]
        public string nickname { get; set; }
        /// <summary>
        /// 专属帐号登录名
        /// </summary>
        [Column("LOGIN_ID")]
        public string login_id { get; set; }
        /// <summary>
        /// 是否是部门的主管
        /// </summary>
        [Column("LEADER")]
        public string leader { get; set; }
        /// <summary>
        /// 是否为企业的老板
        /// </summary>
        [Column("BOSS")]
        public string boss { get; set; }
        /// <summary>
        /// 所属部门ID列表
        /// </summary>
        [Column("DEPT_ID_LIST")]
        public string dept_id_list { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        [Column("extension")]
        public string extension { get; set; }

        
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.userid = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.userid = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.userid = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

