﻿using Dapper;
using HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAJob;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 21:04
    /// 描 述：RPAJobLog
    /// </summary>
    public class RpaDyPromotedaccountsService : RepositoryFactory
    {
        #region  构造函数和属性

        private string fieldSql;
        public RpaDyPromotedaccountsService()
        {
            fieldSql= @"
                t.base_shopname ,
                t.base_shopid ,
                t.PromotedAccountsID,
                t.PromotedAccountsName,
                t.CreateTime 
            ";
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取列表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<rpa_dy_promotedaccountsEntity> GetList(string queryJson)
        {
            try
            {
                
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM rpa_dy_promotedaccounts t where 1=1");

                var queryParam = queryJson.ToJObject();

                // 虚拟参数
                var dp = new DynamicParameters(new { });              
                if (!queryParam["base_shopid"].IsEmpty())
                {
                    dp.Add("base_shopid", queryParam["base_shopid"].ToString(), DbType.String);
                    strSql.Append(" AND t.base_shopid = @base_shopid");
                } 

                return this.BaseRepository().FindList<rpa_dy_promotedaccountsEntity>(strSql.ToString(), dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        } 

        /// <summary>
        /// 获取实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public rpa_dy_promotedaccountsEntity GetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<rpa_dy_promotedaccountsEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(int keyValue)
        {
            try
            {
                this.BaseRepository().Delete<rpa_dy_promotedaccountsEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(int keyValue, rpa_dy_promotedaccountsEntity entity)
        {
            try
            {
                if (keyValue!=0)
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
