﻿using HuNanChangJie.SystemCommon.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.Base_FormTabs
{
    public class FormTabInfo
    {
        /// <summary>
        /// 页签名
        /// </summary>
        public string tabName { get; set; }

        /// <summary>
        /// 取值sql
        /// </summary>
        public string sql { get; set; }

        /// <summary>
        /// 数据源Id
        /// </summary>
        public string dbId { get; set; }

        /// <summary>
        /// 表头信息集合
        /// </summary>
        public List<HeadInfo> heads { get; set; }
    }

    /// <summary>
    /// 表头信息
    /// </summary>
    public class HeadInfo: ColunmEntity
    {
        /// <summary>
        /// 是否穿透
        /// </summary>
        public bool isPenetrate { get; set; }

        /// <summary>
        /// 穿透Url
        /// </summary>
        public string penetrateUrl { get; set; }


    }
}
