﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.Base_FormTabs
{
    public class Base_FormTabsService : RepositoryFactory
    {
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_FormTabsEntity>(i=>i.ID==keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Base_FormTabsEntity GetBase_FormTabsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_FormTabsEntity>(i => i.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public List<FormTabInfo> GetFormTabInfo(string formId)
        {
            var list=new List<FormTabInfo>();
            try
            {
                var taps= this.BaseRepository().FindList<Base_FormTabsEntity>(i => i.BaseSystemFormID==formId && i.Enabled==true);
                if (taps.Any())
                {
                    taps = from item in taps orderby item.SortCode ascending select item;
                    foreach (var item in taps)
                    {
                        var info = new FormTabInfo();
                        info.dbId = item.F_DataSourceId;
                        info.sql = item.SqlInfo;
                        info.tabName = item.TabName;
                        info.heads = item.HeadInfo.ToObject<List<HeadInfo>>();
                        list.Add(info);
                    }
                    return list;
                }
                else
                {
                    return list;
                }
                
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Base_FormTabsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT *  FROM Base_FormTabs t  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["formId"].IsEmpty())
                {
                    dp.Add("formId", queryParam["formId"].ToString(), DbType.String);
                    strSql.Append(" AND t.BaseSystemFormID = @formId ");
                }


                var list = this.BaseRepository().FindList<Base_FormTabsEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void SaveEntity(string keyValue, Base_FormTabsEntity entity, string type)
        {
            try
            {
                entity.ID = keyValue;
                if (type == "edit")
                {
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
