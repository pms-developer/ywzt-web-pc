﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.Base_FormTabs
{
    /// <summary>
    /// 表单页签接口
    /// </summary>
    public interface Base_FormTabsIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_FormTabsEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取实体数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        Base_FormTabsEntity GetBase_FormTabsEntity(string keyValue);

        /// <summary>
        /// 获取表单tab信息
        /// </summary>
        /// <param name="formId"></param>
        /// <returns></returns>
        List<FormTabInfo> GetFormTabInfo(string formId);

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_FormTabsEntity entity, string type);

        #endregion
    }
}
