﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.Base_FormTabs
{
    public class Base_FormTabsBLL : Base_FormTabsIBLL
    {
        private Base_FormTabsService service = new Base_FormTabsService();
        public void DeleteEntity(string keyValue)
        {
            try
            {
                service.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Base_FormTabsEntity GetBase_FormTabsEntity(string keyValue)
        {
            try
            {
                return service.GetBase_FormTabsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<FormTabInfo> GetFormTabInfo(string formId)
        {
            try
            {
                return service.GetFormTabInfo(formId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Base_FormTabsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return service.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void SaveEntity(string keyValue, Base_FormTabsEntity entity, string type)
        {
            try
            {
                service.SaveEntity(keyValue, entity,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

    }
}
