﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.Base_FormTabs
{
    /// <summary>
    /// 表单页签实体对象
    /// </summary>
    public class Base_FormTabsEntity
    {

        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public string BaseSystemFormID { get; set; }

        /// <summary>
        /// 数据源ID
        /// </summary>
        public string F_DataSourceId { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// 页签名称
        /// </summary>
        public string TabName { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortCode { get; set; }

        /// <summary>
        /// 取值Sql
        /// </summary>
        public string SqlInfo { get; set; }

        /// <summary>
        /// 表头信息
        /// </summary>
        public string HeadInfo { get; set; }
    }
}
