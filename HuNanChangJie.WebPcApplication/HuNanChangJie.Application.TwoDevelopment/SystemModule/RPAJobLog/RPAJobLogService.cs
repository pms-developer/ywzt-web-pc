﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 21:04
    /// 描 述：RPAJobLog
    /// </summary>
    public class RPAJobLogService : RepositoryFactory
    {
        #region  构造函数和属性

        private string fieldSql;
        public RPAJobLogService()
        {
            fieldSql=@"
                t.Id,
                t.RPA_JobId,
                t.CurrentPageNo,
                t.StartTime,
                t.EndTime,
                t.CreatTime
            ";
        }
        #endregion

        #region  获取数据

        /// <summary>
        /// 获取列表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<rpa_joblogEntity> GetList(string queryJson)
        {
            try
            {
                //参考写法
                //var queryParam = queryJson.ToJObject();
                // 虚拟参数
                //var dp = new DynamicParameters(new { });
                //dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM rpa_joblog t where 1=1");

                var queryParam = queryJson.ToJObject();

                // 虚拟参数
                var dp = new DynamicParameters(new { });

              
                if (!queryParam["JobId"].IsEmpty())
                {
                    dp.Add("jobId", queryParam["JobId"].ToString(), DbType.String);
                    strSql.Append(" AND t.RPA_JobId = @jobId");
                }

                if (!queryParam["LogDate"].IsEmpty())
                {
                    dp.Add("logDateBeginTime", queryParam["LogDate"].ToString(), DbType.DateTime);
                    dp.Add("logDateEndTime", DateTime.Parse(queryParam["LogDate"].ToString()).AddDays(1), DbType.DateTime);
                    strSql.Append(" AND t.CreatTime >= @logDateBeginTime AND t.CreatTime<@logDateEndTime");
                }

                return this.BaseRepository().FindList<rpa_joblogEntity>(strSql.ToString(), dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取列表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<rpa_joblogEntity> GetRPAInfo(string queryJson)
        {
            try
            {
                //参考写法
                //var queryParam = queryJson.ToJObject();
                // 虚拟参数
                //var dp = new DynamicParameters(new { });
                //dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                var strSql = new StringBuilder();
                strSql.Append(@"SELECT
    joblog.*,
    job.Base_ShopInfoID	
FROM
	rpa_joblog joblog
	JOIN rpa_jobs job ON joblog.RPA_JobId = job.Id
	JOIN base_shopinfo shop ON job.base_shopinfoId = shop.id
	JOIN base_shopplatform shopplatform ON shop.PlatformName = shopplatform.platformcode 
WHERE
	shop.ShopCode = @ShopNo 
	AND shopplatform.WDTPlatformCode = @PlatformID 
	AND ((
			joblog.startTime <= @BillDateTimeStart AND joblog.EndTime > @BillDateTimeStart 
			) 
		OR ( joblog.startTime < @BillDateTimeEnd AND joblog.EndTime > @BillDateTimeEnd ) 
 or (joblog.StartTime>=@BillDateTimeStart and joblog.EndTime<=@BillDateTimeEnd)
	)
	
 ");                

                var queryParam = queryJson.ToJObject();

                // 虚拟参数
                var dp = new DynamicParameters(new { });


                if (!queryParam["ShopNo"].IsEmpty())
                {
                    dp.Add("ShopNo", queryParam["ShopNo"].ToString(), DbType.String);
                   
                }
                if (!queryParam["PlatformID"].IsEmpty())
                {
                    dp.Add("PlatformID", queryParam["PlatformID"].ToString(), DbType.String);

                }
                if (!queryParam["BillDateTimeStart"].IsEmpty())
                {
                    dp.Add("BillDateTimeStart", queryParam["BillDateTimeStart"].ToString(), DbType.String);

                }
                if (!queryParam["BillDateTimeEnd"].IsEmpty())
                {
                    dp.Add("BillDateTimeEnd", queryParam["BillDateTimeEnd"].ToString(), DbType.String);

                }

                return this.BaseRepository().FindList<rpa_joblogEntity>(strSql.ToString(), dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取列表分页数据
        /// <param name="pagination">分页参数</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<rpa_joblogEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM rpa_joblog t ");
                return this.BaseRepository().FindList<rpa_joblogEntity>(strSql.ToString(), pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public rpa_joblogEntity GetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<rpa_joblogEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(int keyValue)
        {
            try
            {
                this.BaseRepository().Delete<rpa_joblogEntity>(t=>t.Id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(int keyValue, rpa_joblogEntity entity)
        {
            try
            {
                if (keyValue!=0)
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
