﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAJob;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 14:27
    /// 描 述：RPA作业
    /// </summary>
    public class RPAJobService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

       

        /// <summary>
        /// 根据模块名称返回Job执行情况 
        /// <param name="moudleName">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<RPAJobDto> GetPRAJobInfos(string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();

                strSql.Append(@"select distinct job.ID,job.RPA_Moudle,job.Base_ShopInfoID ShopId, job.ShopName,shop.PlatformName, job.StartTime,job.EndTime ,joblog.CurrentPageNo, joblog.StartTime as JobLogLastStartTime , joblog.EndTime as JobLogLastEndTime  from rpa_jobs job
left join base_shopinfo shop on job.Base_ShopInfoID=shop.ID
left join rpa_joblog joblog
on job.ID=joblog.RPA_JobId and joblog.Id in (
select id from rpa_joblog where RPA_JobId =job.ID and StartTime=(
select Max(StartTime) as maxStartTime from rpa_joblog where RPA_JobId =job.ID
)) where job.status=0 and job.RPA_Moudle=@moudleName And job.Enabled=1
                ");

                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                dp.Add("moudleName", queryParam["MoudleName"].ToString(), DbType.String);

                if (!queryParam["PlatformName"].IsEmpty())
                {
                    dp.Add("platformName", queryParam["PlatformName"].ToString(), DbType.String);
                    strSql.Append(" AND shop.PlatformName = @platformName");
                }
                if (!queryParam["ShopId"].IsEmpty())
                {
                    dp.Add("shopId", queryParam["ShopId"].ToString(), DbType.String);
                    strSql.Append(" AND job.Base_ShopInfoID = @shopId");
                }

                var result = this.BaseRepository().FindList<RPAJobDto>(strSql.ToString(), dp);

                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<rpa_jobsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Base_ShopInfoID,
                t.ShopName,
                t.ID,
                t.RPA_Moudle,
                t.StartTime,
                t.EndTime
                ");
                strSql.Append("  FROM rpa_jobs t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["RPA_Moudle"].IsEmpty())
                {
                    dp.Add("RPA_Moudle",queryParam["RPA_Moudle"].ToString(), DbType.String);
                    strSql.Append(" AND t.RPA_Moudle = @RPA_Moudle ");
                }
                var list=this.BaseRepository().FindList<rpa_jobsEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取rpa_joblog表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<rpa_joblogEntity> Getrpa_joblogList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<rpa_joblogEntity>(t=>t.RPA_JobId == keyValue );
                var query = from item in list orderby item.CreatTime ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取rpa_jobs表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public rpa_jobsEntity Getrpa_jobsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<rpa_jobsEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取rpa_joblog表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public rpa_joblogEntity Getrpa_joblogEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<rpa_joblogEntity>(t=>t.RPA_JobId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var rpa_jobsEntity = Getrpa_jobsEntity(keyValue); 
                db.Delete<rpa_jobsEntity>(t=>t.ID == keyValue);
                db.Delete<rpa_joblogEntity>(t=>t.RPA_JobId == rpa_jobsEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, rpa_jobsEntity entity,List<rpa_joblogEntity> rpa_joblogList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var rpa_jobsEntityTmp = Getrpa_jobsEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var rpa_joblogUpdateList= rpa_joblogList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in rpa_joblogUpdateList)
                    {
                        db.Update(item);
                    }
                    var rpa_joblogInserList= rpa_joblogList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in rpa_joblogInserList)
                    {
                        item.Create();
                        item.RPA_JobId = rpa_jobsEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (rpa_joblogEntity item in rpa_joblogList)
                    {
                        item.RPA_JobId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 更新Job status状态为已执行，并添加joblog
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="status"></param>
        public void UpdateJobStatus(string jobId,byte status)
        {

            try
            {
                var entity = Getrpa_jobsEntity(jobId);
                entity.Status = status;
                entity.Modify(jobId);
                this.BaseRepository().Update(entity);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 通过模块名和平台名更新Job status状态
        /// </summary>,string moudleName, string paltformName, byte status
        /// <param name="jobId"></param>
        /// <param name="status"></param>
        public void UpdateJobStautsByMoudle(string queryJson)
        {

            try
            {
                var strSql = new StringBuilder();

                strSql.Append(@"UPDATE rpa_jobs set `status`=@status where RPA_Moudle=@moudleName and Enabled=1");

                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                dp.Add("moudleName", queryParam["moudleName"].ToString(), DbType.String);
                dp.Add("status", queryParam["status"].ToString(), DbType.String);

                if (!queryParam["PlatformName"].IsEmpty())
                {
                    dp.Add("platformName", queryParam["PlatformName"].ToString(), DbType.String);
                    strSql.Append(" and Base_shopInfoId in (select id from base_shopinfo where platformName=@platformName)");
                }
                

                this.BaseRepository().ExecuteBySql(strSql.ToString(),dp);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
