﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 14:27
    /// 描 述：RPA作业
    /// </summary>
    public class rpa_joblogEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        [Column("ID")]
        public int? Id { get; set; }
        /// <summary>
        /// RPA_JobId
        /// </summary>
        [Column("RPA_JOBID")]
        public string RPA_JobId { get; set; }
        /// <summary>
        /// 当前PageNo
        /// </summary>
        [Column("CURRENTPAGENO")]
        public int? CurrentPageNo { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        [Column("STARTTIME")]
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        [Column("ENDTIME")]
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATTIME")]
        public DateTime? CreatTime { get; set; }

        /// <summary>
        /// 是否有数据
        /// </summary>
        [Column("HasData")]
        public bool? HasData { get; set; }

        /// <summary>
        /// 批次号
        /// </summary>
        [Column("BatchNo")]
        public string BatchNo { get; set; }
        #endregion


        /// <summary>
        /// 商铺ID
        /// </summary>
        [NotMapped]
        public string Base_ShopInfoID { get; set; }

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(int? keyValue)
        {
            this.Id = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(int? keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
    }
}

