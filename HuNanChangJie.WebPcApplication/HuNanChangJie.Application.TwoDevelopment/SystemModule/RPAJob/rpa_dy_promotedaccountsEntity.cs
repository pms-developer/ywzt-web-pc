﻿using HuNanChangJie.SystemCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAJob
{
    /// <summary>
    /// RPA作业：关联抖音账户
    /// </summary>
    public class rpa_dy_promotedaccountsEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ID")]
        public int? ID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("base_shopname")]
        public string base_shopname { get; set; }

        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("base_shopid")]
        public string base_shopid { get; set; }

        /// <summary>
        /// 账户ID
        /// </summary>
        [Column("PromotedAccountsID")]
        public string PromotedAccountsID { get; set; }

        /// <summary>
        /// 账户名称
        /// </summary>
        [Column("PromotedAccountsName")]
        public string PromotedAccountsName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CreateTime")]
        public DateTime? CreateTime { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(int? keyValue)
        {
            this.ID = keyValue;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(int? keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}
