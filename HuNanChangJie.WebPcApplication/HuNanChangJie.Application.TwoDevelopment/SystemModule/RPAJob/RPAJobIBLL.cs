﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAJob;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 14:27
    /// 描 述：RPA作业
    /// </summary>
    public interface RPAJobIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<rpa_jobsEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取rpa_joblog表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<rpa_joblogEntity> Getrpa_joblogList(string keyValue);
        /// <summary>
        /// 获取rpa_jobs表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        rpa_jobsEntity Getrpa_jobsEntity(string keyValue);
        /// <summary>
        /// 获取rpa_joblog表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        rpa_joblogEntity Getrpa_joblogEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, rpa_jobsEntity entity,List<rpa_joblogEntity> rpa_joblogList,string deleteList,string type);

        /// <summary>
        /// 根据模块名称返回Job执行情况 
        /// <param name="moudleName">主键</param>
        /// <summary>
        /// <returns></returns>
        RPAJobDto GetPRAJobInfo(string queryJson);

        /// <summary>
        /// 根据模块名称返回所有店铺的Job执行情况 
        /// <param name="moudleName">主键</param>
        /// <summary>
        /// <returns></returns>
        IEnumerable<RPAJobDto> GetPRAJobInfos(string queryJson);

        /// <summary>
        /// 更新Job status状态为已执行，并添加joblog
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="status"></param>
        void UpdateJobStauts(string jobId,byte status);
        /// <summary>
        /// 通过模块名和平台名更新Job status状态
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="status"></param>
        void UpdateJobStautsByMoudle(string queryJson);
        #endregion

    }
}
