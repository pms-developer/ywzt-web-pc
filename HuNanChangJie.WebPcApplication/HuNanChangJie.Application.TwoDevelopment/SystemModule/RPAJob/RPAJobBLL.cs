﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAJob;
using System.Linq;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-05-12 14:27
    /// 描 述：RPA作业
    /// </summary>
    public class RPAJobBLL : RPAJobIBLL
    {
        private RPAJobService rPAJobService = new RPAJobService();

     

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<rpa_jobsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return rPAJobService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取rpa_joblog表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<rpa_joblogEntity> Getrpa_joblogList(string keyValue)
        {
            try
            {
                return rPAJobService.Getrpa_joblogList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取rpa_jobs表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public rpa_jobsEntity Getrpa_jobsEntity(string keyValue)
        {
            try
            {
                return rPAJobService.Getrpa_jobsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取rpa_joblog表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public rpa_joblogEntity Getrpa_joblogEntity(string keyValue)
        {
            try
            {
                return rPAJobService.Getrpa_joblogEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                rPAJobService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, rpa_jobsEntity entity,List<rpa_joblogEntity> rpa_joblogList,string deleteList,string type)
        {
            try
            {
                rPAJobService.SaveEntity(keyValue, entity,rpa_joblogList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 根据模块名称返回Job执行情况 
        /// <param name="moudleName">主键</param>
        /// <summary>
        /// <returns></returns>
        public RPAJobDto GetPRAJobInfo(string queryJson)
        {
            try
            {
              return rPAJobService.GetPRAJobInfos(queryJson).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }            
        }

        /// <summary>
        /// 根据模块名称返回所有店铺的Job执行情况 
        /// <param name="moudleName">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<RPAJobDto> GetPRAJobInfos(string queryJson)
        {
            try
            {
                return rPAJobService.GetPRAJobInfos(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 更新Job status状态为已执行，并添加joblog
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="status"></param>
        public void UpdateJobStauts(string jobId, byte status)
        {
            try
            {
                 rPAJobService.UpdateJobStatus(jobId, status);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 通过模块名和平台名更新Job status状态
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="status"></param>
        public void UpdateJobStautsByMoudle(string queryJson)
        {
            try
            {
                rPAJobService.UpdateJobStautsByMoudle(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        #endregion

    }
}
