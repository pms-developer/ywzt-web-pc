﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule.RPAJob
{
    public class RPAJobDto
    {
        /// <summary>
        /// RPAJOB Id
        /// </summary>
        public string ID { get; set; }
        
        /// <summary>
        /// 店铺Id
        /// </summary>
        public string ShopId { get; set; }
        /// <summary>
        /// 店铺名字       
        /// </summary>
        public string ShopName { get; set; }
        
       // public string RPA_MoudleName { get; set; }
        /// <summary>
        /// 初始开始时间     
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 初始结束时间    
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 最后搜索开始时间
        /// </summary>
        public DateTime? JobLogLastStartTime { get; set; }
        /// <summary>
        /// 最后搜索结束时间
        /// </summary>
        public DateTime? JobLogLastEndTime { get; set; }

        /// <summary>
        /// 最后搜索当前页码
        /// </summary>
        public int CurrentPageNo { get; set; }

        /// <summary>
        /// 日志创建时间
        /// </summary>
        public DateTime? LogCreateTime { get; set; }
    }
}
