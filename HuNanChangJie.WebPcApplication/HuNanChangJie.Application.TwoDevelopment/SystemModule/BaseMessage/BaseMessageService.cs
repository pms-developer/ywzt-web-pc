﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-02 14:53
    /// 描 述：系统消息
    /// </summary>
    public class BaseMessageService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_MessageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT *");
                strSql.Append("  FROM Base_Message t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });

                if (!LoginUserInfo.UserInfo.isSystem)
                {
                    dp.Add("UserId", LoginUserInfo.UserInfo.userId, DbType.String);
                    strSql.Append(" And UserID=@UserId ");
                }

                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.MessageDate >= @startTime AND t.MessageDate <= @endTime ) ");
                }
                if (!queryParam["Title"].IsEmpty())
                {
                    dp.Add("Title", "%" + queryParam["Title"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Title Like @Title ");
                }
                if (!queryParam["IsRead"].IsEmpty())
                {
                    dp.Add("IsRead", "%" + queryParam["IsRead"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.IsRead Like @IsRead ");
                }
                var list=this.BaseRepository().FindList<Base_MessageEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.MessageDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public int GetUnreadInfo()
        {
            try
            {
                return BaseRepository().FindList<Base_MessageEntity>(i => i.UserId == LoginUserInfo.UserInfo.userId && i.IsRead != true).Count();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_Message表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_MessageEntity GetBase_MessageEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_MessageEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根据信息ID获取消息列表
        /// </summary>
        /// <param name="infoId"></param>
        /// <returns></returns>
        public IEnumerable<Base_MessageEntity> GetMessageList(string infoId)
        {
            try
            {
                return this.BaseRepository().FindList<Base_MessageEntity>(i=>i.InfoId==infoId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void SaveMessage(List<Base_MessageEntity> insertList)
        {
            try
            {
                 BaseRepository().Insert(insertList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 已读
        /// </summary>
        /// <param name="keyValue"></param>
        public void Readed(string keyValue)
        {
            try
            {
                var info = new Base_MessageEntity();
                info.Modify(keyValue);
                info.IsRead = true;
                info.ReadDate = DateTime.Now;
                BaseRepository().Update(info);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_MessageEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_MessageEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    keyValue = Guid.NewGuid().ToString();
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
