﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.SystemCommon;
using System.Linq;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-02 14:53
    /// 描 述：系统消息
    /// </summary>
    public class BaseMessageBLL : BaseMessageIBLL
    {
        private BaseMessageService baseMessageService = new BaseMessageService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_MessageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return baseMessageService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_Message表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_MessageEntity GetBase_MessageEntity(string keyValue)
        {
            try
            {
                return baseMessageService.GetBase_MessageEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public int GetUnreadInfo()
        {
            try
            {
                return baseMessageService.GetUnreadInfo();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                baseMessageService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_MessageEntity entity,string deleteList,string type)
        {
            try
            {
                baseMessageService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存消息
        /// </summary>
        /// <param name="useridList">用户集合</param>
        /// <param name="infoId">表单信息ID</param>
        /// <param name="msgType">消息类型</param>
        /// <param name="title">标题</param>
        /// <param name="url">表单地址</param>
        public void SaveMessage(List<string> userIdList, string infoId, SystemMessageType msgType, string title,string url="")
        {
            try
            {
                var infoList = baseMessageService.GetMessageList(infoId);
                var insertList = new List<Base_MessageEntity>();
                foreach (var userId in userIdList)
                {
                    var item = infoList.FirstOrDefault(i => i.UserId == userId);
                    if (item != null) continue;
                    var info = new Base_MessageEntity();
                    info.Create();
                    info.Title = title;
                    info.UserId = userId;
                    info.InfoId = infoId;
                    info.Url = url;
                    info.MessageType = msgType.ToString();
                    info.IsRead = false;
                    insertList.Add(info);
                }
                baseMessageService.SaveMessage(insertList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 已读
        /// </summary>
        /// <param name="keyValue"></param>
        public void Readed(string keyValue)
        {
            try
            {
                baseMessageService.Readed(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
