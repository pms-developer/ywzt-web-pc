﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.SystemCommon;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-02 14:53
    /// 描 述：系统消息
    /// </summary>
    public interface BaseMessageIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_MessageEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_Message表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_MessageEntity GetBase_MessageEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_MessageEntity entity,string deleteList,string type);

        /// <summary>
        /// 已读
        /// </summary>
        /// <param name="keyValue"></param>
        void Readed(string keyValue);
        int GetUnreadInfo();

        /// <summary>
        /// 保存消息
        /// </summary>
        /// <param name="useridList">用户集合</param>
        /// <param name="infoId">表单信息ID</param>
        /// <param name="msgType">消息类型</param>
        /// <param name="title">标题</param>
        /// <param name="url">表单地址</param>
        void SaveMessage(List<string> useridList, string infoId, SystemMessageType msgType, string title, string url = "");
        #endregion

    }
}
