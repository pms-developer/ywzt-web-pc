﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-02 14:53
    /// 描 述：系统消息
    /// </summary>
    public class Base_MessageEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 消息主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [Column("TITLE")]
        public string Title { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        [Column("MESSAGETYPE")]
        public string MessageType { get; set; }
        /// <summary>
        /// 发布时间
        /// </summary>
        [Column("MESSAGEDATE")]
        public DateTime? MessageDate { get; set; }
        /// <summary>
        /// 表单地址
        /// </summary>
        [Column("URL")]
        public string Url { get; set; }
        /// <summary>
        /// 阅读时间
        /// </summary>
        [Column("READDATE")]
        public DateTime? ReadDate { get; set; }
        /// <summary>
        /// 是否已阅读
        /// </summary>
        [Column("ISREAD")]
        public bool? IsRead { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        [Column("USERID")]
        public string UserId { get; set; }
        /// <summary>
        /// 表单信息ID
        /// </summary>
        [Column("INFOID")]
        public string InfoId { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            MessageDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

