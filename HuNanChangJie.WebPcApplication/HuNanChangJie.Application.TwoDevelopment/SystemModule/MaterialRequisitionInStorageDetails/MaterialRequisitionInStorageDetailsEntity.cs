﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.SystemModule

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-22 15:43
    /// 描 述：MaterialRequisitionInStorageDetails
    /// </summary>
    public class MaterialRequisitionInStorageDetailsEntity 
    {
        #region  实体成员
        [Column("INSTORAGEINTIME")]
        public DateTime? InStorageInTime { get; set; }
        /// <summary>
        /// 领料ID
        /// </summary>
        /// <returns></returns>
        [Column("FK_MATERIALREQUISITIONID")]
        public string Fk_MaterialRequisitionId { get; set; }
        /// <summary>
        /// 领料详情ID
        /// </summary>
        /// <returns></returns>
        [Column("FK_MATERIALREQUISITIONDETAILSID")]
        public string Fk_MaterialRequisitionDetailsId { get; set; }
        /// <summary>
        /// 入库单详情ID
        /// </summary>
        /// <returns></returns>
        [Column("FK_INSTORAGEDETAILSID")]
        public string Fk_InStorageDetailsId { get; set; }
        /// <summary>
        /// 入库ID
        /// </summary>
        /// <returns></returns>
        [Column("FK_INSTORAGEID")]
        public string Fk_InStorageId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        /// <returns></returns>
        [Column("QUANTITY")]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

