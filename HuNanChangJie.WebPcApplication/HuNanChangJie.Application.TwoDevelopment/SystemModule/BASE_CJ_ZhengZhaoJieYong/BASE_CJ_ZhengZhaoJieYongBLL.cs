﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-19 16:03
    /// 描 述：证书借用
    /// </summary>
    public class BASE_CJ_ZhengZhaoJieYongBLL : BASE_CJ_ZhengZhaoJieYongIBLL
    {
        private BASE_CJ_ZhengZhaoJieYongService bASE_CJ_ZhengZhaoJieYongService = new BASE_CJ_ZhengZhaoJieYongService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_ZhenZhaoJieYongEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return bASE_CJ_ZhengZhaoJieYongService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_ZhenZhaoJieYongMingXi表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Base_CJ_ZhenZhaoJieYongMingXiEntity> GetBase_CJ_ZhenZhaoJieYongMingXiList(string keyValue)
        {
            try
            {
                return bASE_CJ_ZhengZhaoJieYongService.GetBase_CJ_ZhenZhaoJieYongMingXiList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_ZhenZhaoJieYong表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_ZhenZhaoJieYongEntity GetBase_CJ_ZhenZhaoJieYongEntity(string keyValue)
        {
            try
            {
                return bASE_CJ_ZhengZhaoJieYongService.GetBase_CJ_ZhenZhaoJieYongEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_ZhenZhaoJieYongMingXi表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_ZhenZhaoJieYongMingXiEntity GetBase_CJ_ZhenZhaoJieYongMingXiEntity(string keyValue)
        {
            try
            {
                return bASE_CJ_ZhengZhaoJieYongService.GetBase_CJ_ZhenZhaoJieYongMingXiEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return bASE_CJ_ZhengZhaoJieYongService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return bASE_CJ_ZhengZhaoJieYongService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity GuiHuan(string keyValue,List<string> idsList) {
            try
            {
               return bASE_CJ_ZhengZhaoJieYongService.GuiHuan(keyValue, idsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                bASE_CJ_ZhengZhaoJieYongService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_ZhenZhaoJieYongEntity entity,List<Base_CJ_ZhenZhaoJieYongMingXiEntity> base_CJ_ZhenZhaoJieYongMingXiList,string deleteList,string type)
        {
            try
            {
                bASE_CJ_ZhengZhaoJieYongService.SaveEntity(keyValue, entity,base_CJ_ZhenZhaoJieYongMingXiList,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0002");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
