﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-19 16:03
    /// 描 述：证书借用
    /// </summary>
    public class Base_CJ_ZhenZhaoJieYongMingXiEntity 
    {
        #region  实体成员
        /// <summary>
        /// 说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        [NotMapped]
        public EditType EditType{ get; set; }

        /// <summary>
        /// fk_JieYongId
        /// </summary>
        [Column("FK_JIEYONGID")]
        public string fk_JieYongId { get; set; }
        /// <summary>
        /// fk_ZhenZhaoId
        /// </summary>
        [Column("FK_ZHENZHAOID")]
        public string fk_ZhenZhaoId { get; set; }

        [Column("ID")]
        public string ID { get; set; }

        [Column("ISGUIHUAN")]
        public bool? IsGuiHuan { get; set; }

        [Column("GUIHUANDATE")]
        public DateTime? GuiHuanDate { get; set; }

        [NotMapped]
        public string zzmc_name { get; set; }
        [NotMapped]
        public string zz_bianhao { get; set; }
        [NotMapped]
        public string zz_xinming { get; set; }
        [NotMapped]
        public string zz_shenfengzheng { get; set; }
        
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

