﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-19 16:03
    /// 描 述：证书借用
    /// </summary>
    public interface BASE_CJ_ZhengZhaoJieYongIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_CJ_ZhenZhaoJieYongEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_CJ_ZhenZhaoJieYongMingXi表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Base_CJ_ZhenZhaoJieYongMingXiEntity> GetBase_CJ_ZhenZhaoJieYongMingXiList(string keyValue);
        /// <summary>
        /// 获取Base_CJ_ZhenZhaoJieYong表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_CJ_ZhenZhaoJieYongEntity GetBase_CJ_ZhenZhaoJieYongEntity(string keyValue);
        /// <summary>
        /// 获取Base_CJ_ZhenZhaoJieYongMingXi表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_CJ_ZhenZhaoJieYongMingXiEntity GetBase_CJ_ZhenZhaoJieYongMingXiEntity(string keyValue);
        #endregion

        #region  提交数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);

        OperateResultEntity GuiHuan(string keyValue, List<string> idsList);
       
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_CJ_ZhenZhaoJieYongEntity entity,List<Base_CJ_ZhenZhaoJieYongMingXiEntity> base_CJ_ZhenZhaoJieYongMingXiList,string deleteList,string type);
        #endregion

    }
}
