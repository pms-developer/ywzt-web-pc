﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-19 16:03
    /// 描 述：证书借用
    /// </summary>
    public class BASE_CJ_ZhengZhaoJieYongService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_ZhenZhaoJieYongEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT *  FROM Base_CJ_ZhenZhaoJieYong t  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ShenQinRen"].IsEmpty())
                {
                    dp.Add("ShenQinRen", "%" + queryParam["ShenQinRen"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ShenQinRen Like @ShenQinRen ");
                }
                if (!queryParam["Project_ID"].IsEmpty())
                {
                    dp.Add("Project_ID",queryParam["Project_ID"].ToString(), DbType.String);
                    strSql.Append(" AND t.Project_ID = @Project_ID ");
                }
                if (!queryParam["ShenQinBuMeng"].IsEmpty())
                {
                    dp.Add("ShenQinBuMeng",queryParam["ShenQinBuMeng"].ToString(), DbType.String);
                    strSql.Append(" AND t.ShenQinBuMeng = @ShenQinBuMeng ");
                }

                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate"; 
                    pagination.sord = "DESC";
                }
                var list=this.BaseRepository().FindList<Base_CJ_ZhenZhaoJieYongEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_ZhenZhaoJieYongMingXi表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Base_CJ_ZhenZhaoJieYongMingXiEntity> GetBase_CJ_ZhenZhaoJieYongMingXiList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Base_CJ_ZhenZhaoJieYongMingXiEntity>("select a.*,b.zz_bianhao,b.zz_xinming,b.zz_shenfengzheng,c.zzmc_name from Base_CJ_ZhenZhaoJieYongMingXi a left join Base_CJ_ZhengZhao b on a.fk_ZhenZhaoId = b.id left join Base_CJ_ZhenZhaoMingCheng c on b.fk_zhengzhaomingcheng=c.zzmc_id  where fk_jieyongId ='" + keyValue +"'");
                var query = from item in list select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_ZhenZhaoJieYong表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_ZhenZhaoJieYongEntity GetBase_CJ_ZhenZhaoJieYongEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_CJ_ZhenZhaoJieYongEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_ZhenZhaoJieYongMingXi表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_ZhenZhaoJieYongMingXiEntity GetBase_CJ_ZhenZhaoJieYongMingXiEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_CJ_ZhenZhaoJieYongMingXiEntity>(t=>t.fk_ZhenZhaoId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据 

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Base_CJ_ZhenZhaoJieYongEntity base_CJ_ZhenZhaoJieYongEntity = db.FindEntity<Base_CJ_ZhenZhaoJieYongEntity>(keyValue);
                List<string> messageList = new List<string>();
                if (base_CJ_ZhenZhaoJieYongEntity.ZhengZhaoBackCount > 0)
                {
                    throw new Exception("当前申请单有证照归还记录,不能取消审核");
                }

                if (base_CJ_ZhenZhaoJieYongEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                base_CJ_ZhenZhaoJieYongEntity.AuditStatus = "4";
               
                var base_CJ_ZhenZhaoJieYongMingXiEntityList = db.FindList<Base_CJ_ZhenZhaoJieYongMingXiEntity>(m => m.fk_JieYongId == keyValue);
                foreach (Base_CJ_ZhenZhaoJieYongMingXiEntity item in base_CJ_ZhenZhaoJieYongMingXiEntityList)
                {
                    Base_CJ_ZhengZhaoEntity base_CJ_ZhengZhaoEntity = db.FindEntity<Base_CJ_ZhengZhaoEntity>(item.fk_ZhenZhaoId);

                    //if (finance_ProceedsEntity.WrittenOff > finance_ProceedsEntity.PreceedsAmount)
                    //{
                    //    messageList.Add(item.Code + ":总核销金额大于还款单金额");
                    //    continue;
                    //}

                    base_CJ_ZhengZhaoEntity.zz_jiechuzhuangtai = "正常";
                    db.Update<Base_CJ_ZhengZhaoEntity>(base_CJ_ZhengZhaoEntity);
                }

                db.Update<Base_CJ_ZhenZhaoJieYongEntity>(base_CJ_ZhenZhaoJieYongEntity);

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Base_CJ_ZhenZhaoJieYongEntity base_CJ_ZhenZhaoJieYongEntity = db.FindEntity<Base_CJ_ZhenZhaoJieYongEntity>(keyValue);
                List<string> messageList = new List<string>();

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (base_CJ_ZhenZhaoJieYongEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                base_CJ_ZhenZhaoJieYongEntity.AuditStatus = "2";

                var base_CJ_ZhenZhaoJieYongMingXiEntityList = db.FindList<Base_CJ_ZhenZhaoJieYongMingXiEntity>(m => m.fk_JieYongId == keyValue);
                foreach (Base_CJ_ZhenZhaoJieYongMingXiEntity item in base_CJ_ZhenZhaoJieYongMingXiEntityList)
                {
                    Base_CJ_ZhengZhaoEntity base_CJ_ZhengZhaoEntity = db.FindEntity<Base_CJ_ZhengZhaoEntity>(item.fk_ZhenZhaoId);
                    ///后期需要做些更严格的判断
                    //if (finance_ProceedsEntity.WrittenOff > finance_ProceedsEntity.PreceedsAmount)
                    //{
                    //    messageList.Add(item.Code + ":总核销金额大于还款单金额");
                    //    continue;
                    //}

                    base_CJ_ZhengZhaoEntity.zz_jiechuzhuangtai = "借出";
                    db.Update<Base_CJ_ZhengZhaoEntity>(base_CJ_ZhengZhaoEntity);
                }

                db.Update<Base_CJ_ZhenZhaoJieYongEntity>(base_CJ_ZhenZhaoJieYongEntity);

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity GuiHuan(string keyValue, List<string> idsList)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {

                Base_CJ_ZhenZhaoJieYongEntity base_CJ_ZhenZhaoJieYongEntity = db.FindEntity<Base_CJ_ZhenZhaoJieYongEntity>(m => m.ID == keyValue);
                if (base_CJ_ZhenZhaoJieYongEntity.AuditStatus != "2")
                {
                    throw new Exception("申请单状态异常");
                }

                List<Base_CJ_ZhengZhaoEntity> list = db.FindList<Base_CJ_ZhengZhaoEntity>(m=> idsList.Contains(m.ID)).ToList();

                foreach (Base_CJ_ZhengZhaoEntity item in list)
                {
                    item.zz_jiechuzhuangtai = "正常";
                }

                db.Update<Base_CJ_ZhengZhaoEntity>(list);


                base_CJ_ZhenZhaoJieYongEntity.ZhengZhaoBackCount += idsList.Count();

                db.Update<Base_CJ_ZhenZhaoJieYongEntity>(base_CJ_ZhenZhaoJieYongEntity);

                List<Base_CJ_ZhenZhaoJieYongMingXiEntity> listMingxi = db.FindList<Base_CJ_ZhenZhaoJieYongMingXiEntity>(m => idsList.Contains(m.fk_ZhenZhaoId) && m.fk_JieYongId== keyValue).ToList();
                foreach (Base_CJ_ZhenZhaoJieYongMingXiEntity item in listMingxi)
                {
                    item.IsGuiHuan = true;
                    item.GuiHuanDate = DateTime.Now;
                }

                db.Update<Base_CJ_ZhenZhaoJieYongMingXiEntity>(listMingxi);
                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
            /// <summary>
            /// 删除实体数据
            /// <param name="keyValue">主键</param>
            /// <summary>
            /// <returns></returns>
            public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var base_CJ_ZhenZhaoJieYongEntity = GetBase_CJ_ZhenZhaoJieYongEntity(keyValue); 
                db.Delete<Base_CJ_ZhenZhaoJieYongEntity>(t=>t.ID == keyValue);
                db.Delete<Base_CJ_ZhenZhaoJieYongMingXiEntity>(t=>t.fk_ZhenZhaoId == base_CJ_ZhenZhaoJieYongEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_ZhenZhaoJieYongEntity entity,List<Base_CJ_ZhenZhaoJieYongMingXiEntity> base_CJ_ZhenZhaoJieYongMingXiList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var base_CJ_ZhenZhaoJieYongEntityTmp = GetBase_CJ_ZhenZhaoJieYongEntity(keyValue); 
                    entity.Modify(keyValue);

                    if (base_CJ_ZhenZhaoJieYongMingXiList != null)
                        entity.ZhengZhaoCount = base_CJ_ZhenZhaoJieYongMingXiList.Count();

                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Base_CJ_ZhenZhaoJieYongMingXiUpdateList= base_CJ_ZhenZhaoJieYongMingXiList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Base_CJ_ZhenZhaoJieYongMingXiUpdateList)
                    {
                        db.Update(item);
                    }
                    var Base_CJ_ZhenZhaoJieYongMingXiInserList= base_CJ_ZhenZhaoJieYongMingXiList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Base_CJ_ZhenZhaoJieYongMingXiInserList)
                    {
                        item.fk_JieYongId = base_CJ_ZhenZhaoJieYongEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    if (base_CJ_ZhenZhaoJieYongMingXiList != null)
                        entity.ZhengZhaoCount = base_CJ_ZhenZhaoJieYongMingXiList.Count();

                    db.Insert(entity);
                    foreach (Base_CJ_ZhenZhaoJieYongMingXiEntity item in base_CJ_ZhenZhaoJieYongMingXiList)
                    {
                        item.fk_JieYongId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
