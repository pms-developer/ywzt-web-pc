﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-19 16:03
    /// 描 述：证书借用
    /// </summary>
    public class Base_CJ_ZhenZhaoJieYongEntity 
    {
        #region  实体成员
        /// <summary>
        /// 说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        [NotMapped]
        public EditType EditType{ get; set; }

        /// <summary>
        /// 借用编号
        /// </summary>
        [Column("BIANHAO")]
        public string BianHao { get; set; }
        /// <summary>
        /// 申请人
        /// </summary>
        [Column("SHENQINREN")]
        public string ShenQinRen { get; set; }
        /// <summary>
        /// 借用日期
        /// </summary>
        [Column("JIEYONGRIQI")]
        public DateTime? JieYongRiQi { get; set; }
        /// <summary>
        /// 归还日期
        /// </summary>
        [Column("GUIHUANRIQI")]
        public DateTime? GuiHuanRiQi { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("BEIZHU")]
        public string BeiZhu { get; set; }
        /// <summary>
        /// 申请部门
        /// </summary>
        [Column("SHENQINBUMENG")]
        public string ShenQinBuMeng { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SortCode")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECT_ID")]
        public string Project_ID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }

        [Column("ZHENGZHAOCOUNT")]
        public int? ZhengZhaoCount { get; set; }
        [Column("ZHENGZHAOBACKCOUNT")]
        public int? ZhengZhaoBackCount { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ZhengZhaoBackCount = 0;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.ZhengZhaoBackCount = 0;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            if(!this.ZhengZhaoBackCount.HasValue)
            this.ZhengZhaoBackCount = 0;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

