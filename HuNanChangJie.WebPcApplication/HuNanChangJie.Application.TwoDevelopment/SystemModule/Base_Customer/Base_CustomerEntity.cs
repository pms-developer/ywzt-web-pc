﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-25 15:59
    /// 描 述：客户管理
    /// </summary>
    public class Base_CustomerEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 客户编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 客户名称
        /// </summary>
        [Column("FULLNAME")]
        public string FullName { get; set; }
        /// <summary>
        /// 客户简称
        /// </summary>
        [Column("SHORTNAME")]
        public string ShortName { get; set; }
        /// <summary>
        /// 客户性质
        /// </summary>
        [Column("PROPERTY")]
        public string Property { get; set; }
        /// <summary>
        /// 客户类型
        /// </summary>
        [Column("CUSTOMERTYPE")]
        public string CustomerType { get; set; }
        /// <summary>
        /// 所在地省
        /// </summary>
        [Column("PROVINCEID")]
        public string ProvinceId { get; set; }
        /// <summary>
        /// 所在地市
        /// </summary>
        [Column("CITYID")]
        public string CityId { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("PHONE")]
        public string Phone { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        [Column("POSTCODE")]
        public string Postcode { get; set; }
        /// <summary>
        /// 网址
        /// </summary>
        [Column("URL")]
        public string URL { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        [Column("FAX")]
        public string Fax { get; set; }
        /// <summary>
        /// 发票抬头
        /// </summary>
        [Column("INVOICETITLE")]
        public string InvoiceTitle { get; set; }
        /// <summary>
        /// 开户银行
        /// </summary>
        [Column("BANKNAME")]
        public string BankName { get; set; }
        /// <summary>
        /// 银行账号
        /// </summary>
        [Column("BANKACCOUNT")]
        public string BankAccount { get; set; }
        /// <summary>
        /// 税务登记号
        /// </summary>
        [Column("TAXID")]
        public string TaxID { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [Column("INVOICEADDRESS")]
        public string InvoiceAddress { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 业务负责人
        /// </summary>
        [Column("CHARGEUSERID")]
        public string ChargeUserId { get; set; }
        /// <summary>
        /// 负责部门
        /// </summary>
        [Column("CHARGEDEPARTMENTID")]
        public string ChargeDepartmentId { get; set; }
        /// <summary>
        /// 客户等级
        /// </summary>
        [Column("GRADE")]
        public string Grade { get; set; }
        /// <summary>
        /// 客户状态
        /// </summary>
        [Column("CSTATUS")]
        public string CStatus { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        [Column("INVOICEPHONE")]
        public string InvoicePhone { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 客户来源
        /// </summary>
        [Column("SOURCE")]
        public string Source { get; set; }


        /// <summary>
        /// 联系人
        /// </summary>
        [Column("Contacts")]
        public string Contacts { get; set; }

        /// <summary>
        /// 签约金额
        /// </summary>
        [Column("ContractedAmount")]
        [DecimalPrecision(18, 4)]
        public decimal? ContractedAmount { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

