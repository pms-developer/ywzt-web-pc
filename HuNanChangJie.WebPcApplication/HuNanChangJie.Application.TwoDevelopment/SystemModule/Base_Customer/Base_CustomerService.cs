﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-25 15:59
    /// 描 述：客户管理
    /// </summary>
    public class Base_CustomerService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CustomerEntity> GetPageList(XqPagination pagination, string queryJson) 
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT  *  FROM Base_Customer t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["CStatus"].IsEmpty())
                {
                    dp.Add("CStatus",queryParam["CStatus"].ToString(), DbType.String);
                    strSql.Append(" AND t.CStatus = @CStatus ");
                }
                if (!queryParam["Grade"].IsEmpty())
                {
                    dp.Add("Grade",queryParam["Grade"].ToString(), DbType.String);
                    strSql.Append(" AND t.Grade = @Grade ");
                }
                if (!queryParam["CustomerType"].IsEmpty())
                {
                    dp.Add("CustomerType",queryParam["CustomerType"].ToString(), DbType.String);
                    strSql.Append(" AND t.CustomerType = @CustomerType ");
                }
                if (!queryParam["ChargeUserId"].IsEmpty())
                {
                    dp.Add("ChargeUserId",queryParam["ChargeUserId"].ToString(), DbType.String);
                    strSql.Append(" AND t.ChargeUserId = @ChargeUserId ");
                }
                if (!queryParam["FullName"].IsEmpty())
                {
                    dp.Add("FullName", "%" + queryParam["FullName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.FullName like @FullName ");
                }

                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list=this.BaseRepository().FindList<Base_CustomerEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_Customer表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CustomerEntity GetBase_CustomerEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_CustomerEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_CustomerEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CustomerEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
