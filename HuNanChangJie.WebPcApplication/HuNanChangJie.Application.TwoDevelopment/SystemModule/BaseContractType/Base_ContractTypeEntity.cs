﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 15:55
    /// 描 述：合同类型
    /// </summary>
    public class Base_ContractTypeEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 合同类型名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 合同类型属性
        /// </summary>
        [Column("PROPERTY")]
        [Obsolete]
        public string Property { get; set; }

        /// <summary>
        /// 合同类型属性
        /// </summary>
        [NotMapped]
        public ContractProperty PropertyEnum
        {
            get
            {
                return (ContractProperty)Enum.Parse(typeof(ContractProperty), Property);
            }
            set
            {
                Property = value.ToString();
            }
        }
        /// <summary>
        /// 排序
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 收支科目ID
        /// </summary>
        [Column("BASESUBJECTID")]
        public string BaseSubjectId { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        [NotMapped]
        public string FullName { get; set; }

        [NotMapped]
        public string PropertyName
        {
            get {
                switch (PropertyEnum)
                {
                    case ContractProperty.Project:
                        return "工程";
                    case ContractProperty.SubContract:
                        return "分包";
                    case ContractProperty.PurchaseContract:
                        return "采购";
                    case ContractProperty.OtherPurchaseContract:
                        return "其他采购";
                    default:
                        return Property;
                }
            }
        }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

