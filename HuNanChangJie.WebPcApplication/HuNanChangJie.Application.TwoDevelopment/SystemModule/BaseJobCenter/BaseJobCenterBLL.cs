﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.ScheduleJobModel;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.SystemModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-07 10:01
    /// 描 述：计划任务中心
    /// </summary>
    public class BaseJobCenterBLL : BaseJobCenterIBLL
    {
        private BaseJobCenterService baseJobCenterService = new BaseJobCenterService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<JobInfo> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return baseJobCenterService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_JobInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public JobInfo GetJobInfo(string keyValue)
        {
            try
            {
                return baseJobCenterService.GetJobInfo(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                baseJobCenterService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, JobInfo entity,string deleteList,string type)
        {
            try
            {
                baseJobCenterService.SaveEntity(keyValue, entity,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("JobCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
