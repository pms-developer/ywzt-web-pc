﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.DisplayBoard
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:19
    /// 描 述：KBKanBanInfo
    /// </summary>
    public class KBKanBanInfoEntity 
    {
        #region  实体成员
        /// <summary>
        /// 看板主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 看板名称
        /// </summary>
        [Column("F_KANBANNAME")]
        public string F_KanBanName { get; set; }
        /// <summary>
        /// 看板编号
        /// </summary>
        [Column("F_KANBANCODE")]
        public string F_KanBanCode { get; set; }
        /// <summary>
        /// 刷新时间（分钟）
        /// </summary>
        [Column("F_REFRESHTIME")]
        public int? F_RefreshTime { get; set; }
        /// <summary>
        /// 模板id
        /// </summary>
        [Column("F_TEMPLATEID")]
        public string F_TemplateId { get; set; }
        /// <summary>
        /// 看板说明
        /// </summary>
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建用户主键
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        [Column("ModificationDate")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改用户主键
        /// </summary>
        [Column("Modification_Id")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改用户
        /// </summary>
        [Column("ModificationName")]
        public string ModificationName { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

