﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.DisplayBoard
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:19
    /// 描 述：KBKanBanInfo
    /// </summary>
    public interface KBKanBanInfoIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<KBKanBanInfoEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取KBKanBanInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        KBKanBanInfoEntity GetKBKanBanInfoEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, KBKanBanInfoEntity entity);

        /// <summary>
        /// 根据看板名称获取看板信息
        /// </summary>
        /// <param name="kbName"></param>
        /// <returns></returns>
        KBKanBanInfoEntity GetKanBanInfoByName(string kbName);
        #endregion

    }
}
