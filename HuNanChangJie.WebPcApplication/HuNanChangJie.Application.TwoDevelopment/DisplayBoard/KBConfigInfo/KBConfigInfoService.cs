﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.DisplayBoard
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:16
    /// 描 述：看板配置信息
    /// </summary>
    public class KBConfigInfoService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<KBConfigInfoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_Id,
                t.F_KanBanId,
                t.F_ModeName,
                t.F_Type,
                t.F_TopValue,
                t.F_LeftValue,
                t.F_WidthValue,
                t.F_HightValue,
                t.F_SortCode,
                t.F_RefreshTime,
                t.F_Configuration
                ");
                strSql.Append("  FROM KBConfigInfo t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                return this.BaseRepository().FindList<KBConfigInfoEntity>(strSql.ToString(), dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取KBConfigInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public KBConfigInfoEntity GetKBConfigInfoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<KBConfigInfoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<KBConfigInfoEntity>(t => t.F_Id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, KBConfigInfoEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal int DeleteEntityByKanBanID(string kanbanid)
        {
            try
            {
                return this.BaseRepository().Delete<KBConfigInfoEntity>(p => p.F_KanBanId == kanbanid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void SetThrough(string targetId, string jsonStr, bool isItem)
        {
            try
            {
                if (!string.IsNullOrEmpty(targetId))
                {
                    if (isItem)
                    {
                        string sql = "select * from KBConfigInfo where F_Type='statistics'";
                        List<KBConfigInfoEntity> list = this.BaseRepository().FindList<KBConfigInfoEntity>(sql).ToList();
                        if (list != null && list.Count > 0)
                        {
                            KBConfigInfoEntity target = list.FirstOrDefault(a => a.F_Configuration.IndexOf(targetId) > -1);
                            if (target != null && !string.IsNullOrEmpty(target.F_Id))
                            {
                                KBConfigInfoEntityJson model = JsonConvert.DeserializeObject<KBConfigInfoEntityJson>(target.F_Configuration);
                                model.colData.FirstOrDefault(a => a.Key == targetId).Value.ThroughConfig = JsonConvert.DeserializeObject<ThroughJsonModal>(jsonStr).main;
                                model.colData.FirstOrDefault(a => a.Key == targetId).Value.SubThroughConfig = JsonConvert.DeserializeObject<ThroughJsonModal>(jsonStr).sub;
                                target.F_Configuration = JsonConvert.SerializeObject(model);
                                this.BaseRepository().Update<KBConfigInfoEntity>(target);
                            }
                        }
                    }
                    else
                    {
                        KBConfigInfoEntity modal = this.BaseRepository().FindEntity<KBConfigInfoEntity>(targetId);
                        Dictionary<string, string> items = JsonConvert.DeserializeObject<Dictionary<string, string>>(modal.F_Configuration);
                        if (items != null && items.Count > 0)
                        {
                            var tc = items.FirstOrDefault(a => a.Key == "ThroughConfig");
                            if (!string.IsNullOrEmpty(tc.Value))
                            {
                                items.Remove("ThroughConfig");
                            }
                            items.Add("ThroughConfig", jsonStr);
                            modal.F_Configuration = JsonConvert.SerializeObject(items);
                            this.BaseRepository().Update(modal);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public string GetThrough(string targetId, bool isItem)
        {
            try
            {
                if (isItem)
                {
                    string sql = "select * from KBConfigInfo where F_Type='statistics'";
                    List<KBConfigInfoEntity> list = this.BaseRepository().FindList<KBConfigInfoEntity>(sql).ToList();
                    if (list != null && list.Count > 0)
                    {
                        KBConfigInfoEntity target = list.FirstOrDefault(a => a.F_Configuration.IndexOf(targetId) > -1);
                        KBConfigInfoEntityJson model = JsonConvert.DeserializeObject<KBConfigInfoEntityJson>(target.F_Configuration);
                        ThroughJsonModal ret = new ThroughJsonModal() { main = model.colData.FirstOrDefault(a => a.Key == targetId).Value.ThroughConfig, sub = model.colData.FirstOrDefault(a => a.Key == targetId).Value.SubThroughConfig };
                        return JsonConvert.SerializeObject(ret);
                    }
                }
                else
                {
                    KBConfigInfoEntity modal = this.BaseRepository().FindEntity<KBConfigInfoEntity>(targetId);
                    Dictionary<string, string> items = JsonConvert.DeserializeObject<Dictionary<string, string>>(modal.F_Configuration);
                    if (items != null && items.Count > 0)
                        return items.FirstOrDefault(a => a.Key == "ThroughConfig").Value;
                }

                return "";
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        class ThroughJsonModal
        {
            public string main { get; set; }
            public string sub { get; set; }
        }
        class KBConfigInfoEntityJson
        {
            public int num { get; set; }
            public Dictionary<string, KBConfigInfoEntityJsonItem> colData { get; set; }
        }
        class KBConfigInfoEntityJsonItem
        {
            public string F_Title { get; set; }
            public string F_TBaseData { get; set; }
            public string F_TSQL { get; set; }
            public string F_TInterface { get; set; }
            public string F_TitleValue { get; set; }
            public string ThroughConfig { get; set; }
            public string SubThroughConfig { get; set; }
            public string F_Subtitle { get; set; }
            public string F_SBaseData { get; set; }
            public string F_SubSQL { get; set; }
            public string F_SInterface { get; set; }
            public string F_SubtitleValue { get; set; }
            public string F_Link { get; set; }
            public string id { get; set; }
        }


        #endregion

        public IEnumerable<KBConfigInfoEntity> GetList(string kanbanId)
        {
            try
            {
                return this.BaseRepository().FindList<KBConfigInfoEntity>(p => p.F_KanBanId == kanbanId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
