﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.DisplayBoard
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:16
    /// 描 述：看板配置信息
    /// </summary>
    public class KBConfigInfoBLL : KBConfigInfoIBLL
    {
        private KBConfigInfoService KBConfigInfoService = new KBConfigInfoService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<KBConfigInfoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return KBConfigInfoService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<KBConfigInfoEntity> GetList(string kanbanID)
        {
            try
            {
                return KBConfigInfoService.GetList(kanbanID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取KBConfigInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public KBConfigInfoEntity GetKBConfigInfoEntity(string keyValue)
        {
            try
            {
                return KBConfigInfoService.GetKBConfigInfoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                KBConfigInfoService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, KBConfigInfoEntity entity)
        {
            try
            {
                KBConfigInfoService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public int DeleteEntityByKanBanID(string kanbanid)
        {
            try
            {
                return KBConfigInfoService.DeleteEntityByKanBanID(kanbanid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void SetThrough(string targetId, string jsonStr, bool isItem)
        {
            try
            {
                KBConfigInfoService.SetThrough(targetId, jsonStr, isItem);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public string GetThrough(string targetId, bool isItem)
        {
            try
            {
                return KBConfigInfoService.GetThrough(targetId, isItem);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
