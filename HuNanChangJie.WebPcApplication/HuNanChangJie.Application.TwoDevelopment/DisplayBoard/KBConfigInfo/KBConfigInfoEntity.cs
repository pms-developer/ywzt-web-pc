﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.DisplayBoard
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:16
    /// 描 述：看板配置信息
    /// </summary>
    public class KBConfigInfoEntity 
    {
        #region  实体成员
        /// <summary>
        /// 模块主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 看板id
        /// </summary>
        [Column("F_KANBANID")]
        public string F_KanBanId { get; set; }
        /// <summary>
        /// 模块名称
        /// </summary>
        [Column("F_MODENAME")]
        public string F_ModeName { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        [Column("F_TYPE")]
        public string F_Type { get; set; }
        /// <summary>
        /// 上边距
        /// </summary>
        [Column("F_TOPVALUE")]
        public string F_TopValue { get; set; }
        /// <summary>
        /// 左边距
        /// </summary>
        [Column("F_LEFTVALUE")]
        public string F_LeftValue { get; set; }
        /// <summary>
        /// 宽度
        /// </summary>
        [Column("F_WIDTHVALUE")]
        public string F_WidthValue { get; set; }
        /// <summary>
        /// 高度
        /// </summary>
        [Column("F_HIGHTVALUE")]
        public string F_HightValue { get; set; }
        /// <summary>
        /// 排序码
        /// </summary>
        [Column("F_SORTCODE")]
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 刷新时间（分钟）
        /// </summary>
        [Column("F_REFRESHTIME")]
        public int? F_RefreshTime { get; set; }
        /// <summary>
        /// 配置信息
        /// </summary>
        [Column("F_CONFIGURATION")]
        public string F_Configuration { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

