﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.DisplayBoard
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-17 13:16
    /// 描 述：看板配置信息
    /// </summary>
    public interface KBConfigInfoIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<KBConfigInfoEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 根据看板iD获取所有配置详细
        /// </summary>
        /// <param name="kanbanID"></param>
        /// <returns></returns>
        IEnumerable<KBConfigInfoEntity> GetList(string kanbanID);
        /// <summary>
        /// 获取KBConfigInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        KBConfigInfoEntity GetKBConfigInfoEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);

        /// <summary>
        /// 根据看板ID删除实体数据
        /// </summary>
        /// <param name="kanbanid"></param>

        int DeleteEntityByKanBanID(string kanbanid);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, KBConfigInfoEntity entity);

        /// <summary>
        /// 设置穿透参数
        /// </summary>
        void SetThrough(string targetId, string jsonStr, bool isItem);
        /// <summary>
        /// 获取穿透参数
        /// </summary>
        /// <param name="targetId"></param>
        /// <param name="isItem"></param>
        /// <returns></returns>
        string GetThrough(string targetId, bool isItem);
        #endregion

    }
}
