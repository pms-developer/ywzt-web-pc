﻿using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.DataBase;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangJie.Application.Organization;

namespace HuNanChangJie.Application.TwoDevelopment
{

    /// <summary>
    /// 仓储公共处理
    /// </summary>
    public class WarehouseUtil
    {
        private static ICache cache = CacheFactory.CaChe();

        /// <summary>
        /// 用户当前仓库Key
        /// </summary>
        public static string userWarehouseKey
        {
            get
            {
                UserInfo userInfo = LoginUserInfo.Get();
                return "userWarehouse-" + userInfo.userId;
            }
        }

        /// <summary>
        /// 用户当前仓库
        /// </summary>
        public static string UserWarehouse { get
            {
                return cache.Read<string>(userWarehouseKey);
            } }


        /// <summary>
        /// 用户仓库切换
        /// </summary>
        public static void userWarehouseSet(string Warehouse)
        {
            cache.Write<string>(userWarehouseKey, Warehouse);
        }

        /// <summary>
        /// 判断Code是否唯一
        /// </summary>
        /// <param name="tb"></param>
        /// <param name="no"></param>
        /// <param name="noKey"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool ExistCode(string tb,string no, string key = "",string noKey="Code")
        {
            IDatabase iDatabase = DbFactory.GetIDatabase();
            var db = new Repository(iDatabase);
            try
            {
                string sql = "select ID from [dbo].["+ tb + "] where "+ noKey + "='" + no + "'";
                DataTable dt = db.FindTable(sql);
                if(dt.Rows.Count < 1) 
                {
                    return true;
                }
                List<string> ids = new List<string>(); 
                foreach (DataRow item in dt.Rows)
                {
                    ids.Add(item["ID"].ToString());
                }
                return !string.IsNullOrEmpty(key) && ids.Where(x=>x==key).Count()>0;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



    }
}
