﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 09:47
    /// 描 述：定位货位范围
    /// </summary>
    public interface MeioWmsWarehousePointStorageLocationRangeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<meioWmsWarehousePointStorageLocationRangeEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取meioWmsWarehousePointStorageLocationRangeItem表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehousePointStorageLocationRangeItemEntity> GetmeioWmsWarehousePointStorageLocationRangeItemList(string keyValue);
        /// <summary>
        /// 获取meioWmsWarehousePointStorageLocationRange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehousePointStorageLocationRangeEntity GetmeioWmsWarehousePointStorageLocationRangeEntity(string keyValue);
        /// <summary>
        /// 获取meioWmsWarehousePointStorageLocationRangeItem表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehousePointStorageLocationRangeItemEntity GetmeioWmsWarehousePointStorageLocationRangeItemEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, meioWmsWarehousePointStorageLocationRangeEntity entity,List<meioWmsWarehousePointStorageLocationRangeItemEntity> meioWmsWarehousePointStorageLocationRangeItemList,string deleteList,string type);
        #endregion

    }
}
