﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 09:47
    /// 描 述：定位货位范围
    /// </summary>
    public class MeioWmsWarehousePointStorageLocationRangeService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehousePointStorageLocationRangeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name
                ");
                strSql.Append("  FROM meioWmsWarehousePointStorageLocationRange t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                var list=this.BaseRepository().FindList<meioWmsWarehousePointStorageLocationRangeEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehousePointStorageLocationRangeItem表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehousePointStorageLocationRangeItemEntity> GetmeioWmsWarehousePointStorageLocationRangeItemList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<meioWmsWarehousePointStorageLocationRangeItemEntity>(t=>t.PointStorageLocationRuleId == keyValue ).OrderBy(x=>x.SortCode);
                var query = from item in list select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehousePointStorageLocationRange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehousePointStorageLocationRangeEntity GetmeioWmsWarehousePointStorageLocationRangeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehousePointStorageLocationRangeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehousePointStorageLocationRangeItem表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehousePointStorageLocationRangeItemEntity GetmeioWmsWarehousePointStorageLocationRangeItemEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehousePointStorageLocationRangeItemEntity>(t=>t.PointStorageLocationRuleId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioWmsWarehousePointStorageLocationRangeEntity = GetmeioWmsWarehousePointStorageLocationRangeEntity(keyValue); 
                db.Delete<meioWmsWarehousePointStorageLocationRangeEntity>(t=>t.ID == keyValue);
                db.Delete<meioWmsWarehousePointStorageLocationRangeItemEntity>(t=>t.PointStorageLocationRuleId == meioWmsWarehousePointStorageLocationRangeEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehousePointStorageLocationRangeEntity entity,List<meioWmsWarehousePointStorageLocationRangeItemEntity> meioWmsWarehousePointStorageLocationRangeItemList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var meioWmsWarehousePointStorageLocationRangeEntityTmp = GetmeioWmsWarehousePointStorageLocationRangeEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var meioWmsWarehousePointStorageLocationRangeItemUpdateList= meioWmsWarehousePointStorageLocationRangeItemList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in meioWmsWarehousePointStorageLocationRangeItemUpdateList)
                    {
                        db.Update(item);
                    }
                    var meioWmsWarehousePointStorageLocationRangeItemInserList= meioWmsWarehousePointStorageLocationRangeItemList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in meioWmsWarehousePointStorageLocationRangeItemInserList)
                    {
                        item.Create(item.ID);
                        item.PointStorageLocationRuleId = meioWmsWarehousePointStorageLocationRangeEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (meioWmsWarehousePointStorageLocationRangeItemEntity item in meioWmsWarehousePointStorageLocationRangeItemList)
                    {
                        item.PointStorageLocationRuleId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
