﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 09:47
    /// 描 述：定位货位范围
    /// </summary>
    public class MeioWmsWarehousePointStorageLocationRangeBLL : MeioWmsWarehousePointStorageLocationRangeIBLL
    {
        private MeioWmsWarehousePointStorageLocationRangeService meioWmsWarehousePointStorageLocationRangeService = new MeioWmsWarehousePointStorageLocationRangeService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehousePointStorageLocationRangeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioWmsWarehousePointStorageLocationRangeService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehousePointStorageLocationRangeItem表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehousePointStorageLocationRangeItemEntity> GetmeioWmsWarehousePointStorageLocationRangeItemList(string keyValue)
        {
            try
            {
                return meioWmsWarehousePointStorageLocationRangeService.GetmeioWmsWarehousePointStorageLocationRangeItemList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehousePointStorageLocationRange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehousePointStorageLocationRangeEntity GetmeioWmsWarehousePointStorageLocationRangeEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehousePointStorageLocationRangeService.GetmeioWmsWarehousePointStorageLocationRangeEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehousePointStorageLocationRangeItem表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehousePointStorageLocationRangeItemEntity GetmeioWmsWarehousePointStorageLocationRangeItemEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehousePointStorageLocationRangeService.GetmeioWmsWarehousePointStorageLocationRangeItemEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsWarehousePointStorageLocationRangeService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehousePointStorageLocationRangeEntity entity,List<meioWmsWarehousePointStorageLocationRangeItemEntity> meioWmsWarehousePointStorageLocationRangeItemList,string deleteList,string type)
        {
            try
            {
                meioWmsWarehousePointStorageLocationRangeService.SaveEntity(keyValue, entity,meioWmsWarehousePointStorageLocationRangeItemList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
