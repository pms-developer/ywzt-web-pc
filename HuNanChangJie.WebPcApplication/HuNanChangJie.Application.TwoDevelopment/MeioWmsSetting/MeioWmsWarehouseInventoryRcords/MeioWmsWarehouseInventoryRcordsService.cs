﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 11:14
    /// 描 述：库存交易记录
    /// </summary>
    public class MeioWmsWarehouseInventoryRcordsService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseInventoryRcordsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Type,
                t.No,
                t.ShipperID,
                t.GoodCode,
                t.GoodName,
                t.QualityState,
                t.Size,
                t.Color,
                t.Attr1,
                t.OperUser,
                t.StorageLocation,
                t.InOut,
                t.Num,
                t.OldNum,
                t.NewNum,
                t.OperDate
                ");
                strSql.Append("  FROM meioWmsWarehouseInventoryRcords t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", "%" + queryParam["Type"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Type Like @Type ");
                }
                if (!queryParam["No"].IsEmpty())
                {
                    dp.Add("No", "%" + queryParam["No"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.No Like @No ");
                }
                if (!queryParam["ShipperID"].IsEmpty())
                {
                    dp.Add("ShipperID", "%" + queryParam["ShipperID"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ShipperID Like @ShipperID ");
                }
                if (!queryParam["GoodCode"].IsEmpty())
                {
                    dp.Add("GoodCode", "%" + queryParam["GoodCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.GoodCode Like @GoodCode ");
                }
                if (!queryParam["StorageLocation"].IsEmpty())
                {
                    dp.Add("StorageLocation", "%" + queryParam["StorageLocation"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.StorageLocation Like @StorageLocation ");
                }
                if (!queryParam["OperUser"].IsEmpty())
                {
                    dp.Add("OperUser", "%" + queryParam["OperUser"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OperUser Like @OperUser ");
                }
                if (!queryParam["OperDate"].IsEmpty())
                {
                    dp.Add("OperDate", "%" + queryParam["OperDate"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OperDate Like @OperDate ");
                }
                if (!queryParam["OperDate"].IsEmpty())
                {
                    dp.Add("OperDate", "%" + queryParam["OperDate"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OperDate Like @OperDate ");
                }
                var list=this.BaseRepository().FindList<meioWmsWarehouseInventoryRcordsEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseInventoryRcords表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseInventoryRcordsEntity GetmeioWmsWarehouseInventoryRcordsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseInventoryRcordsEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<meioWmsWarehouseInventoryRcordsEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseInventoryRcordsEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
