﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 11:14
    /// 描 述：库存交易记录
    /// </summary>
    public interface MeioWmsWarehouseInventoryRcordsIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseInventoryRcordsEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取meioWmsWarehouseInventoryRcords表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehouseInventoryRcordsEntity GetmeioWmsWarehouseInventoryRcordsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, meioWmsWarehouseInventoryRcordsEntity entity,string deleteList,string type);
        #endregion

    }
}
