﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 11:14
    /// 描 述：库存交易记录
    /// </summary>
    public class meioWmsWarehouseInventoryRcordsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 库存
        /// </summary>
        [Column("INVENTORYID")]
        public string InventoryID { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        [Column("SHIPPERID")]
        public string ShipperID { get; set; }
        /// <summary>
        /// 变动类型
        /// </summary>
        [Column("TYPE")]
        public string Type { get; set; }
        /// <summary>
        /// 单号
        /// </summary>
        [Column("NO")]
        public string No { get; set; }
        /// <summary>
        /// 货品编码
        /// </summary>
        [Column("GOODCODE")]
        public string GoodCode { get; set; }
        /// <summary>
        /// 货品名称
        /// </summary>
        [Column("GOODNAME")]
        public string GoodName { get; set; }
        /// <summary>
        /// 质量状态
        /// </summary>
        [Column("QUALITYSTATE")]
        public string QualityState { get; set; }
        /// <summary>
        /// 尺码
        /// </summary>
        [Column("SIZE")]
        public string Size { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>
        [Column("COLOR")]
        public string Color { get; set; }
        /// <summary>
        /// 属性1
        /// </summary>
        [Column("ATTR1")]
        public string Attr1 { get; set; }
        /// <summary>
        /// 属性2
        /// </summary>
        [Column("ATTR2")]
        public string Attr2 { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        [Column("OPERUSER")]
        public string OperUser { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        [Column("OPERDATE")]
        public DateTime? OperDate { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        [Column("OPERID")]
        public string OperID { get; set; }
        /// <summary>
        /// 货位
        /// </summary>
        [Column("STORAGELOCATION")]
        public string StorageLocation { get; set; }
        /// <summary>
        /// 进/出
        /// </summary>
        [Column("INOUT")]
        public string InOut { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Column("NUM")]
        public int? Num { get; set; }
        /// <summary>
        /// 之前数量
        /// </summary>
        [Column("OLDNUM")]
        public int? OldNum { get; set; }
        /// <summary>
        /// 之后数量
        /// </summary>
        [Column("NEWNUM")]
        public int? NewNum { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

