﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 11:14
    /// 描 述：库存交易记录
    /// </summary>
    public class MeioWmsWarehouseInventoryRcordsBLL : MeioWmsWarehouseInventoryRcordsIBLL
    {
        private MeioWmsWarehouseInventoryRcordsService meioWmsWarehouseInventoryRcordsService = new MeioWmsWarehouseInventoryRcordsService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseInventoryRcordsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioWmsWarehouseInventoryRcordsService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseInventoryRcords表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseInventoryRcordsEntity GetmeioWmsWarehouseInventoryRcordsEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseInventoryRcordsService.GetmeioWmsWarehouseInventoryRcordsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsWarehouseInventoryRcordsService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseInventoryRcordsEntity entity,string deleteList,string type)
        {
            try
            {
                meioWmsWarehouseInventoryRcordsService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
