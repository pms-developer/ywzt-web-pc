﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 10:41
    /// 描 述：库存
    /// </summary>
    public class MeioWmsWarehouseInventoryBLL : MeioWmsWarehouseInventoryIBLL
    {
        private MeioWmsWarehouseInventoryService meioWmsWarehouseInventoryService = new MeioWmsWarehouseInventoryService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseInventoryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioWmsWarehouseInventoryService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseInventory表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseInventoryEntity GetmeioWmsWarehouseInventoryEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseInventoryService.GetmeioWmsWarehouseInventoryEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseInventoryReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseInventoryReceiptEntity GetmeioWmsWarehouseInventoryReceiptEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseInventoryService.GetmeioWmsWarehouseInventoryReceiptEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsWarehouseInventoryService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseInventoryEntity entity,meioWmsWarehouseInventoryReceiptEntity meioWmsWarehouseInventoryReceiptEntity,string deleteList,string type)
        {
            try
            {
                meioWmsWarehouseInventoryService.SaveEntity(keyValue, entity,meioWmsWarehouseInventoryReceiptEntity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
