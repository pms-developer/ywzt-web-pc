﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 10:41
    /// 描 述：库存
    /// </summary>
    public class MeioWmsWarehouseInventoryService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseInventoryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.ShipperID,
                t.GoodCode,
                t.GoodName,
                t.StorageLocation,
                t.InNum,
                t.MoveInNum,
                t.AllocatedNum,
                t.FreezeNum,
                t.InventoryChangeDate,
                t1.ReceiptID,
                t1.ProductionDate,
                t1.ExpirationDate,
                t1.QualityState
                ");
                strSql.Append("  FROM meioWmsWarehouseInventory t ");
                strSql.Append("  LEFT JOIN meioWmsWarehouseInventoryReceipt t1 ON t1.InventoryID = t.ID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ShipperID"].IsEmpty())
                {
                    dp.Add("ShipperID", "%" + queryParam["ShipperID"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ShipperID Like @ShipperID ");
                }
                if (!queryParam["GoodCode"].IsEmpty())
                {
                    dp.Add("GoodCode", "%" + queryParam["GoodCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.GoodCode Like @GoodCode ");
                }
                if (!queryParam["GoodName"].IsEmpty())
                {
                    dp.Add("GoodName", "%" + queryParam["GoodName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.GoodName Like @GoodName ");
                }
                if (!queryParam["StorageLocation"].IsEmpty())
                {
                    dp.Add("StorageLocation", "%" + queryParam["StorageLocation"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.StorageLocation Like @StorageLocation ");
                }
                if (!queryParam["QualityState"].IsEmpty())
                {
                    dp.Add("QualityState", "%" + queryParam["QualityState"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t1.QualityState Like @QualityState ");
                }
                if (!queryParam["ReceiptID"].IsEmpty())
                {
                    dp.Add("ReceiptID", "%" + queryParam["ReceiptID"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t1.ReceiptID Like @ReceiptID ");
                }
                var list=this.BaseRepository().FindList<meioWmsWarehouseInventoryEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseInventory表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseInventoryEntity GetmeioWmsWarehouseInventoryEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseInventoryEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseInventoryReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseInventoryReceiptEntity GetmeioWmsWarehouseInventoryReceiptEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseInventoryReceiptEntity>(t=>t.InventoryID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioWmsWarehouseInventoryEntity = GetmeioWmsWarehouseInventoryEntity(keyValue); 
                db.Delete<meioWmsWarehouseInventoryEntity>(t=>t.ID == keyValue);
                db.Delete<meioWmsWarehouseInventoryReceiptEntity>(t=>t.InventoryID == meioWmsWarehouseInventoryEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseInventoryEntity entity,meioWmsWarehouseInventoryReceiptEntity meioWmsWarehouseInventoryReceiptEntity,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var meioWmsWarehouseInventoryEntityTmp = GetmeioWmsWarehouseInventoryEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    //(tb.name != mainTable)// 不是
                  /*  var updateList= meioWmsWarehouseInventoryReceiptList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in updateList)
                    {
                        db.Update(item);
                    }
                    var addList= meioWmsWarehouseInventoryReceiptList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in  addList)
                    {
                        item.Create(item.ID);
                        item.InventoryID = meioWmsWarehouseInventoryEntityTmp.ID;
                        db.Insert(item);
                    }*/
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    meioWmsWarehouseInventoryReceiptEntity.Create();
                    meioWmsWarehouseInventoryReceiptEntity.InventoryID = entity.ID;
                    db.Insert(meioWmsWarehouseInventoryReceiptEntity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
