﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-26 14:52
    /// 描 述：入库单
    /// </summary>
    public class MeioWmsWarehouseReceiptBLL : MeioWmsWarehouseReceiptIBLL
    {
        private MeioWmsWarehouseReceiptService meioWmsWarehouseReceiptService = new MeioWmsWarehouseReceiptService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseReceiptEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioWmsWarehouseReceiptService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        public int checkNoCount(string kyeValue)
        {
            try
            {
                return meioWmsWarehouseReceiptService.checkNoCount(kyeValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取meioWmsWarehouseReceiptDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseReceiptDetailEntity> GetmeioWmsWarehouseReceiptDetailList(string keyValue)
        {
            try
            {
                return meioWmsWarehouseReceiptService.GetmeioWmsWarehouseReceiptDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseReceiptReceivingDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseReceiptReceivingDetailEntity> GetmeioWmsWarehouseReceiptReceivingDetailList(string keyValue)
        {
            try
            {
                return meioWmsWarehouseReceiptService.GetmeioWmsWarehouseReceiptReceivingDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取meioWmsWarehouseReceiptItems表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseReceiptItemsEntity> GetmeioWmsWarehouseReceiptItemsList(string keyValue)
        {
            try
            {
                return meioWmsWarehouseReceiptService.GetmeioWmsWarehouseReceiptItemsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取meioWmsWarehouseReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseReceiptEntity GetmeioWmsWarehouseReceiptEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseReceiptService.GetmeioWmsWarehouseReceiptEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseReceiptDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseReceiptDetailEntity GetmeioWmsWarehouseReceiptDetailEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseReceiptService.GetmeioWmsWarehouseReceiptDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseReceiptReceivingDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseReceiptReceivingDetailEntity GetmeioWmsWarehouseReceiptReceivingDetailEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseReceiptService.GetmeioWmsWarehouseReceiptReceivingDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsWarehouseReceiptService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseReceiptEntity entity,List<meioWmsWarehouseReceiptDetailEntity> meioWmsWarehouseReceiptDetailList,List<meioWmsWarehouseReceiptReceivingDetailEntity> meioWmsWarehouseReceiptReceivingDetailList,string deleteList,string type)
        {
            try
            {
                meioWmsWarehouseReceiptService.SaveEntity(keyValue, entity,meioWmsWarehouseReceiptDetailList,meioWmsWarehouseReceiptReceivingDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
