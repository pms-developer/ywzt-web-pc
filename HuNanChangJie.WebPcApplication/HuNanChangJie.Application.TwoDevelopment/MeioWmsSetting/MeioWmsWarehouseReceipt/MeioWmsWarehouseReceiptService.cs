﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-26 14:52
    /// 描 述：入库单
    /// </summary>
    public class MeioWmsWarehouseReceiptService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private MeioWmsWarehouseService meioWmsWarehouseService = new MeioWmsWarehouseService();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseReceiptEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.No,
                t.Type,
                t.ShipperID,
                s.Name ShipperName,
                t.Total,
                t.RowsTotal,
                t.OrderNo,
                t.StartState,
                t.EndState,
                t.CreationDate
                ");
                strSql.Append("  FROM meioWmsWarehouseReceipt t left join meioWmsShipper s on t.ShipperID=s.ID left join meioWmsWarehouseReceiptDetail d on t.ID=d.ReceiptID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["No"].IsEmpty())
                {
                    dp.Add("No", "%" + queryParam["No"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.No Like @No ");
                }
                if (!queryParam["GoodsCode"].IsEmpty())
                {
                    dp.Add("GoodsCode", "%" + queryParam["GoodsCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND d.Code Like @GoodsCode ");
                }
                if (!queryParam["ShipperID"].IsEmpty())
                {
                    dp.Add("ShipperID", "%" + queryParam["ShipperID"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ShipperID Like @ShipperID ");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", "%" + queryParam["Type"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Type Like @Type ");
                }
                if (!queryParam["CreationStatrDate"].IsEmpty())
                {
                    dp.Add("CreationStatrDate", queryParam["CreationStatrDate"].ToString(), DbType.String);
                    strSql.Append(" AND t.CreationDate > @CreationStatrDate ");
                }
                if (!queryParam["CreationEndDate"].IsEmpty())
                {
                    dp.Add("CreationEndDate", queryParam["CreationEndDate"].ToString(), DbType.String);
                    strSql.Append(" AND t.CreationDate < @CreationEndDate ");
                }
                if (!queryParam["StartState"].IsEmpty())
                {
                    dp.Add("StartState", "%" + queryParam["StartState"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.StartState Like @StartState ");
                }
                if (!queryParam["EndState"].IsEmpty())
                {
                    dp.Add("EndState", "%" + queryParam["EndState"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.EndState Like @EndState ");
                }
                if (!queryParam["ShipperID"].IsEmpty())
                {
                    dp.Add("ShipperID", queryParam["ShipperID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ShipperID = @ShipperID ");
                }
                else
                {
                    UserInfo userInfo = LoginUserInfo.Get();
                    var sList = meioWmsWarehouseService.getUserShipper(userInfo.userId);
                    StringBuilder sb = new StringBuilder();

                    sb.Append(" and t.ShipperID in (");

                    foreach (var item in sList)
                    {
                        sb.Append("'" + item.ShipperID + "',");
                    }
                    string filter = sb.ToString().TrimEnd(',') + ") ";
                    strSql.Append(filter);
                }
                dp.Add("WarehouseID", WarehouseUtil.UserWarehouse, DbType.String);
                strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                var list=this.BaseRepository().FindList<meioWmsWarehouseReceiptEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        public int checkNoCount(string kyeValue)
        {
            string sql = "select count(1) as c from [dbo].[meioWmsWarehouseReceipt] where No='" + kyeValue + "'";
            DataTable dt = this.BaseRepository().FindTable(sql);
            return int.Parse(dt.Rows[0]["c"].ToString());
        }


        /// <summary>
        /// 获取meioWmsWarehouseReceiptDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseReceiptDetailEntity> GetmeioWmsWarehouseReceiptDetailList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<meioWmsWarehouseReceiptDetailEntity>(t=>t.ReceiptID == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseReceiptReceivingDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseReceiptReceivingDetailEntity> GetmeioWmsWarehouseReceiptReceivingDetailList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<meioWmsWarehouseReceiptReceivingDetailEntity>(t=>t.ReceiptID == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取meioWmsWarehouseReceiptItems表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseReceiptItemsEntity> GetmeioWmsWarehouseReceiptItemsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<meioWmsWarehouseReceiptItemsEntity>(t => t.ReceiptID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取meioWmsWarehouseReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseReceiptEntity GetmeioWmsWarehouseReceiptEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseReceiptEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseReceiptDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseReceiptDetailEntity GetmeioWmsWarehouseReceiptDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseReceiptDetailEntity>(t=>t.ReceiptID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseReceiptReceivingDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseReceiptReceivingDetailEntity GetmeioWmsWarehouseReceiptReceivingDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseReceiptReceivingDetailEntity>(t=>t.ReceiptID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioWmsWarehouseReceiptEntity = GetmeioWmsWarehouseReceiptEntity(keyValue); 
                db.Delete<meioWmsWarehouseReceiptEntity>(t=>t.ID == keyValue);
                db.Delete<meioWmsWarehouseReceiptDetailEntity>(t=>t.ReceiptID == meioWmsWarehouseReceiptEntity.ID);
                db.Delete<meioWmsWarehouseReceiptReceivingDetailEntity>(t=>t.ReceiptID == meioWmsWarehouseReceiptEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseReceiptEntity entity,List<meioWmsWarehouseReceiptDetailEntity> meioWmsWarehouseReceiptDetailList,List<meioWmsWarehouseReceiptReceivingDetailEntity> meioWmsWarehouseReceiptReceivingDetailList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var meioWmsWarehouseReceiptEntityTmp = GetmeioWmsWarehouseReceiptEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var meioWmsWarehouseReceiptDetailUpdateList= meioWmsWarehouseReceiptDetailList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in meioWmsWarehouseReceiptDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var meioWmsWarehouseReceiptDetailInserList= meioWmsWarehouseReceiptDetailList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in meioWmsWarehouseReceiptDetailInserList)
                    {
                        item.Create(item.ID);
                        item.ReceiptID = meioWmsWarehouseReceiptEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                    
                    //没有生成代码 
                    var meioWmsWarehouseReceiptReceivingDetailUpdateList= meioWmsWarehouseReceiptReceivingDetailList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in meioWmsWarehouseReceiptReceivingDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var meioWmsWarehouseReceiptReceivingDetailInserList= meioWmsWarehouseReceiptReceivingDetailList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in meioWmsWarehouseReceiptReceivingDetailInserList)
                    {
                        item.Create(item.ID);
                        item.ReceiptID = meioWmsWarehouseReceiptEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (meioWmsWarehouseReceiptDetailEntity item in meioWmsWarehouseReceiptDetailList)
                    {
                        item.ReceiptID = entity.ID;
                        db.Insert(item);
                    }
                    foreach (meioWmsWarehouseReceiptReceivingDetailEntity item in meioWmsWarehouseReceiptReceivingDetailList)
                    {
                        item.ReceiptID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
