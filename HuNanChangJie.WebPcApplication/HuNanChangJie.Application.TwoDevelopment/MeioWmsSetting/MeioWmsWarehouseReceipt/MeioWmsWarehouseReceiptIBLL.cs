﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-26 14:52
    /// 描 述：入库单
    /// </summary>
    public interface MeioWmsWarehouseReceiptIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseReceiptEntity> GetPageList(XqPagination pagination, string queryJson);


        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        int checkNoCount(string kyeValue);


        /// <summary>
        /// 获取meioWmsWarehouseReceiptDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseReceiptDetailEntity> GetmeioWmsWarehouseReceiptDetailList(string keyValue);
        /// <summary>
        /// 获取meioWmsWarehouseReceiptReceivingDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseReceiptReceivingDetailEntity> GetmeioWmsWarehouseReceiptReceivingDetailList(string keyValue);

        /// <summary>
        /// 获取meioWmsWarehouseReceiptItems表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseReceiptItemsEntity> GetmeioWmsWarehouseReceiptItemsList(string keyValue);

        /// <summary>
        /// 获取meioWmsWarehouseReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehouseReceiptEntity GetmeioWmsWarehouseReceiptEntity(string keyValue);
        /// <summary>
        /// 获取meioWmsWarehouseReceiptDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehouseReceiptDetailEntity GetmeioWmsWarehouseReceiptDetailEntity(string keyValue);
        /// <summary>
        /// 获取meioWmsWarehouseReceiptReceivingDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehouseReceiptReceivingDetailEntity GetmeioWmsWarehouseReceiptReceivingDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, meioWmsWarehouseReceiptEntity entity,List<meioWmsWarehouseReceiptDetailEntity> meioWmsWarehouseReceiptDetailList,List<meioWmsWarehouseReceiptReceivingDetailEntity> meioWmsWarehouseReceiptReceivingDetailList,string deleteList,string type);
        #endregion

    }
}
