﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 10:15
    /// 描 述：上架任务
    /// </summary>
    public class MeioWmsWarehouseListingTaskBLL : MeioWmsWarehouseListingTaskIBLL
    {
        private MeioWmsWarehouseListingTaskService meioWmsWarehouseListingTaskService = new MeioWmsWarehouseListingTaskService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseListingTaskEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioWmsWarehouseListingTaskService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseListingTaskItem表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseListingTaskItemEntity> GetmeioWmsWarehouseListingTaskItemList(string keyValue)
        {
            try
            {
                return meioWmsWarehouseListingTaskService.GetmeioWmsWarehouseListingTaskItemList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseListingTask表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseListingTaskEntity GetmeioWmsWarehouseListingTaskEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseListingTaskService.GetmeioWmsWarehouseListingTaskEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseListingTaskItem表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseListingTaskItemEntity GetmeioWmsWarehouseListingTaskItemEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseListingTaskService.GetmeioWmsWarehouseListingTaskItemEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsWarehouseListingTaskService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseListingTaskEntity entity,List<meioWmsWarehouseListingTaskItemEntity> meioWmsWarehouseListingTaskItemList,string deleteList,string type)
        {
            try
            {
                meioWmsWarehouseListingTaskService.SaveEntity(keyValue, entity,meioWmsWarehouseListingTaskItemList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
