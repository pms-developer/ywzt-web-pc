﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using System.Data.SqlClient;
namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-18 08:55
    /// 描 述：仓库配置-区域
    /// </summary>
    public class MeioWmsAreaService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();


        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="pagination">查询参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsAreaEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.code,
                t.name,
                t.cate
                ");
                strSql.Append("  FROM meioWmsArea t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["code"].IsEmpty())
                {
                    dp.Add("code", "%" + queryParam["code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.code Like @code ");
                }
                if (!queryParam["name"].IsEmpty())
                {
                    dp.Add("name", "%" + queryParam["name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.name Like @name ");
                }
                dp.Add("WarehouseID", WarehouseUtil.UserWarehouse, DbType.String);
                strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                var list=this.BaseRepository().FindList<meioWmsAreaEntity>(strSql.ToString(),dp, pagination);
                
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取下拉列表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsAreaEntity> GetSelectList()
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.code,
                t.name,
                t.cate
                ");
                strSql.Append("  FROM meioWmsArea t ");
                strSql.Append("  WHERE 1=1 ");
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                dp.Add("WarehouseID", WarehouseUtil.UserWarehouse, DbType.String);
                strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                var list = this.BaseRepository().FindList<meioWmsAreaEntity>(strSql.ToString(), dp);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        public int checkNoCount(string kyeValue)
        {
            string sql = "select count(1) as c from [dbo].[meioWmsArea] where code='" + kyeValue + "'";
            DataTable dt = this.BaseRepository().FindTable(sql);
            return int.Parse(dt.Rows[0]["c"].ToString());
        }

        /// <summary>
        /// 获取meioWmsArea表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsAreaEntity GetmeioWmsAreaEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsAreaEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<meioWmsAreaEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsAreaEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
