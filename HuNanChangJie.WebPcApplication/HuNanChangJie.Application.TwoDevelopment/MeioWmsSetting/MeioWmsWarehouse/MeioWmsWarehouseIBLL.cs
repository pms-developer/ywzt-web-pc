﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-19 10:50
    /// 描 述：仓库
    /// </summary>
    public interface MeioWmsWarehouseIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseEntity> GetPageList(XqPagination pagination, string queryJson);


        /// <summary>
        /// 查询用户授权仓库
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<meioWmsUserWarehouseEntity> getUserWarehouse(string userId);


        /// <summary>
        /// 查询用户授权货主
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<meioWmsUserShipperEntity> getUserShipper(string userId);


        /// <summary>
        /// 获取下拉数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseEntity> GetSelectList(string filter = "");


        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        int checkNoCount(string kyeValue);

        /// <summary>
        /// 获取meioWmsWarehouse表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehouseEntity GetmeioWmsWarehouseEntity(string keyValue);
        #endregion

        #region  提交数据



        /// <summary>
        /// 用户仓库授权
        /// </summary>
        /// <returns></returns>
        void setUserWarehouse(string userId, List<string> WarehouseIds);

        /// <summary>
        /// 用户货主授权
        /// </summary>
        /// <returns></returns>
        void setUserShipper(string userId, List<string> ShipperIds);

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, meioWmsWarehouseEntity entity,string deleteList,string type);

        /// <summary>
        /// 用户切换仓库
        /// </summary>
        /// <param name="id"></param>
        void userWarehouseChange(string id);
        #endregion



        /// <summary>
        /// 用户仓库初始化
        /// </summary>
        void userWarehouseInit();

    }
}
