﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Cache.Factory;
using HuNanChangJie.Cache.Base;
namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{

    

    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-19 10:50
    /// 描 述：仓库
    /// </summary>
    public class MeioWmsWarehouseService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();

        private ICache cache = CacheFactory.CaChe();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Company_ID,
                t.Addr,
                t.LinkMan,
                t.Tel,
                t.Phone
                ");
                strSql.Append("  FROM meioWmsWarehouse t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                var list=this.BaseRepository().FindList<meioWmsWarehouseEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 查询用户授权仓库
        /// </summary>
        /// <returns></returns>
        public IEnumerable<meioWmsUserWarehouseEntity> getUserWarehouse(string userId)
        {
            try
            {
                return this.BaseRepository().FindList<meioWmsUserWarehouseEntity>(x => x.UserID == userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 查询用户授权货主
        /// </summary>
        /// <returns></returns>
        public IEnumerable<meioWmsUserShipperEntity> getUserShipper(string userId)
        {
            try
            {
                return this.BaseRepository().FindList<meioWmsUserShipperEntity>(x => x.UserID == userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取下拉数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseEntity> GetSelectList(string filter="")
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Company_ID,
                t.Addr,
                t.LinkMan,
                t.Tel,
                t.Phone
                ");
                strSql.Append("  FROM meioWmsWarehouse t ");
                strSql.Append("  WHERE 1=1 ");
                if (!filter.IsEmpty())
                {
                    strSql.Append(filter);
                }
                var list = this.BaseRepository().FindList<meioWmsWarehouseEntity>(strSql.ToString());
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }




        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        public int checkNoCount(string kyeValue)
        {
            string sql = "select count(1) as c from [dbo].[meioWmsWarehouse] where code='" + kyeValue + "'";
            DataTable dt = this.BaseRepository().FindTable(sql);
            return int.Parse(dt.Rows[0]["c"].ToString());
        }

        /// <summary>
        /// 获取meioWmsWarehouse表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseEntity GetmeioWmsWarehouseEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据


        /// <summary>
        /// 用户仓库授权
        /// </summary>
        /// <returns></returns>
        public void setUserWarehouse(string userId,List<string> WarehouseIds)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                db.Delete<meioWmsUserWarehouseEntity>(x=>x.UserID==userId);
                foreach (var item in WarehouseIds)
                {
                    meioWmsUserWarehouseEntity m = new meioWmsUserWarehouseEntity();
                    m.Create();
                    m.UserID=userId;
                    m.WarehouseID = item;
                    db.Insert<meioWmsUserWarehouseEntity>(m);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 用户货主授权
        /// </summary>
        /// <returns></returns>
        public void setUserShipper(string userId, List<string> ShipperIds)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                db.Delete<meioWmsUserShipperEntity>(x => x.UserID == userId);
                foreach (var item in ShipperIds)
                {
                    meioWmsUserShipperEntity m = new meioWmsUserShipperEntity();
                    m.Create();
                    m.UserID = userId;
                    m.ShipperID = item;
                    db.Insert<meioWmsUserShipperEntity>(m);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<meioWmsWarehouseEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion


        /// <summary>
        /// 用户仓库初始化
        /// </summary>
        public void userWarehouseInit()
        {
            try
            {
                if (string.IsNullOrEmpty(WarehouseUtil.UserWarehouse))
                {
                    UserInfo userInfo = LoginUserInfo.Get();
                    List<string> wIds = this.BaseRepository().FindList<meioWmsUserWarehouseEntity>().Where(x => x.UserID == userInfo.userId).Select(x => x.WarehouseID).ToList();
                    var wList = this.BaseRepository().FindList<meioWmsWarehouseEntity>().Where(x => wIds.Contains(x.ID)).ToList();
                    if (wList.Count > 0)
                    {
                        WarehouseUtil.userWarehouseSet(wList[0].ID);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            
        }



    }
}
