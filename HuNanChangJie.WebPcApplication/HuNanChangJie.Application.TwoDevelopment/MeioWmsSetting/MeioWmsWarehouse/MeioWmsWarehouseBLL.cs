﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-19 10:50
    /// 描 述：仓库
    /// </summary>
    public class MeioWmsWarehouseBLL : MeioWmsWarehouseIBLL
    {
        private MeioWmsWarehouseService meioWmsWarehouseService = new MeioWmsWarehouseService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioWmsWarehouseService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 查询用户授权仓库
        /// </summary>
        /// <returns></returns>
        public IEnumerable<meioWmsUserWarehouseEntity> getUserWarehouse(string userId)
        {
            try
            {
                return meioWmsWarehouseService.getUserWarehouse(userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 查询用户授权货主
        /// </summary>
        /// <returns></returns>
        public IEnumerable<meioWmsUserShipperEntity> getUserShipper(string userId)
        {
            try
            {
                return meioWmsWarehouseService.getUserShipper(userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        /// <summary>
        /// 获取下拉数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseEntity> GetSelectList(string filter="")
        {
            try
            {
                return meioWmsWarehouseService.GetSelectList(filter);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        public int checkNoCount(string kyeValue)
        {
            try
            {
                return meioWmsWarehouseService.checkNoCount(kyeValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }




        /// <summary>
        /// 获取meioWmsWarehouse表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseEntity GetmeioWmsWarehouseEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseService.GetmeioWmsWarehouseEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据


        /// <summary>
        /// 用户仓库授权
        /// </summary>
        /// <returns></returns>
       public void setUserWarehouse(string userId, List<string> WarehouseIds)
        {
            try
            {
                meioWmsWarehouseService.setUserWarehouse(userId, WarehouseIds);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 用户货主授权
        /// </summary>
        /// <returns></returns>
        public void setUserShipper(string userId, List<string> ShipperIds)
        {
            try
            {
                meioWmsWarehouseService.setUserShipper(userId, ShipperIds);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsWarehouseService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseEntity entity,string deleteList,string type)
        {
            try
            {
                meioWmsWarehouseService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 用户仓库初始化
        /// </summary>
        public void userWarehouseChange(string id)
        {
            try
            {
                WarehouseUtil.userWarehouseSet(id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion



        /// <summary>
        /// 用户仓库初始化
        /// </summary>
        public void userWarehouseInit()
        {
            try
            {
                meioWmsWarehouseService.userWarehouseInit();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

    }
}
