﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-18 09:14
    /// 描 述：仓库配置-货架
    /// </summary>
    public interface MeioWmsShelvesIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<meioWmsShelvesEntity> GetPageList(XqPagination pagination, string queryJson);


        /// <summary>
        /// 获取下拉列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<meioWmsShelvesEntity> GetSelectList();


        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        int checkNoCount(string kyeValue);


        /// <summary>
        /// 获取meioWmsShelves表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsShelvesEntity GetmeioWmsShelvesEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, meioWmsShelvesEntity entity,string deleteList,string type);
        #endregion

    }
}
