﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-18 10:46
    /// 描 述：货主信息
    /// </summary>
    public class MeioWmsShipperBLL : MeioWmsShipperIBLL
    {
        private MeioWmsShipperService meioWmsShipperService = new MeioWmsShipperService();

        private MeioWmsWarehouseService meioWmsWarehouseService =new MeioWmsWarehouseService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsShipperEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioWmsShipperService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取下拉数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsShipperEntity> GetSelectList(string WarehouseID)
        {
            try
            {
                return meioWmsShipperService.GetSelectList(WarehouseID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 查询当前用户在当前仓库可查看的货主
        /// </summary>
        /// <returns></returns>
        public IEnumerable<meioWmsShipperEntity> GetUserShipperList()
        {
            try
            {
                UserInfo userInfo = LoginUserInfo.Get();
                var sList = meioWmsWarehouseService.getUserShipper(userInfo.userId);
                StringBuilder sb =new StringBuilder();

                sb.Append(" and t.id in (");

                foreach (var item in sList)
                {
                    sb.Append("'" + item.ShipperID + "',");
                }
                string filter=sb.ToString().TrimEnd(',')+ ") ";
                return meioWmsShipperService.GetUserShipperList(filter);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        public int checkNoCount(string kyeValue)
        {
            try
            {
                return meioWmsShipperService.checkNoCount(kyeValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsShipper表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsShipperEntity GetmeioWmsShipperEntity(string keyValue)
        {
            try
            {
                return meioWmsShipperService.GetmeioWmsShipperEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsShipperService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsShipperEntity entity,string deleteList,string type)
        {
            try
            {
                meioWmsShipperService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
