﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-18 10:46
    /// 描 述：货主信息
    /// </summary>
    public class MeioWmsShipperService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsShipperEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Enabled,
                t.WarehouseID,
                t.CreationDate,
                t.Creation_Id,
                t.CreationName,
                t.ModificationDate,
                t.Modification_Id,
                t.ModificationName
                ");
                strSql.Append("  FROM meioWmsShipper t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["Enabled"].IsEmpty())
                {
                    dp.Add("Enabled",queryParam["Enabled"].ToString(), DbType.String);
                    strSql.Append(" AND t.Enabled = @Enabled ");
                }
                dp.Add("WarehouseID", WarehouseUtil.UserWarehouse, DbType.String);
                strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                var list=this.BaseRepository().FindList<meioWmsShipperEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 查询当前用户在当前仓库可查看的货主
        /// </summary>
        /// <returns></returns>
        public IEnumerable<meioWmsShipperEntity> GetUserShipperList(string filter)
        {
            //查询用户货主授权
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Enabled,
                t.WarehouseID,
                t.CreationDate,
                t.Creation_Id,
                t.CreationName,
                t.ModificationDate,
                t.Modification_Id,
                t.ModificationName
                ");
                strSql.Append("  FROM meioWmsShipper t ");
                strSql.Append("  WHERE 1=1 ");
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                dp.Add("WarehouseID", WarehouseUtil.UserWarehouse, DbType.String);
                strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                if (!filter.IsEmpty())
                {
                    strSql.Append(filter);
                }
                var list = this.BaseRepository().FindList<meioWmsShipperEntity>(strSql.ToString(), dp);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取下拉数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsShipperEntity> GetSelectList(string WarehouseIds)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Enabled,
                t.WarehouseID,
                t.CreationDate,
                t.Creation_Id,
                t.CreationName,
                t.ModificationDate,
                t.Modification_Id,
                t.ModificationName,
                m.Name WarehouseName,
                m.Code WarehouseCode
                ");
                strSql.Append("  FROM meioWmsShipper t left join meioWmsWarehouse m on t.WarehouseID=m.ID ");
                strSql.Append("  WHERE 1=1 ");
                var dp = new DynamicParameters(new { });
                if (!WarehouseIds.IsEmpty())
                {
                    if (WarehouseIds.IndexOf(',') > -1)
                    {
                        dp.Add("WarehouseID", WarehouseIds, DbType.String);

                        string[] ids = WarehouseIds.Split(',');
                        string sqlIds= "AND t.WarehouseID in (";
                        foreach (var item in ids)
                        {
                            sqlIds+= "'"+item+"',";
                        }
                        sqlIds = sqlIds.TrimEnd(',');
                        sqlIds += ") ";
                        strSql.Append(sqlIds);
                    }
                    else
                    {
                        dp.Add("WarehouseID", WarehouseIds, DbType.String);
                        strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                    }
                }
                else
                {
                    return new List<meioWmsShipperEntity>();
                }
                var list = this.BaseRepository().FindList<meioWmsShipperEntity>(strSql.ToString(), dp);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        public int checkNoCount(string kyeValue)
        {
            string sql = "select count(1) as c from [dbo].[meioWmsShipper] where code='" + kyeValue + "'";
            DataTable dt = this.BaseRepository().FindTable(sql);
            return int.Parse(dt.Rows[0]["c"].ToString());
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsShipperEntity> GetAllList()
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT  *  FROM meioWmsShipper t ");
                strSql.Append("  WHERE 1=1 ");
                var list = this.BaseRepository().FindList<meioWmsShipperEntity>(strSql.ToString());
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsShipper表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsShipperEntity GetmeioWmsShipperEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsShipperEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<meioWmsShipperEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsShipperEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
