﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 09:58
    /// 描 述：上架定位规则
    /// </summary>
    public interface MeioWmsWarehousePointListingRuleIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<meioWmsWarehousePointListingRuleEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取meioWmsWarehousePointListingRuleItem表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehousePointListingRuleItemEntity> GetmeioWmsWarehousePointListingRuleItemList(string keyValue);
        /// <summary>
        /// 获取meioWmsWarehousePointListingRule表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehousePointListingRuleEntity GetmeioWmsWarehousePointListingRuleEntity(string keyValue);
        /// <summary>
        /// 获取meioWmsWarehousePointListingRuleItem表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehousePointListingRuleItemEntity GetmeioWmsWarehousePointListingRuleItemEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, meioWmsWarehousePointListingRuleEntity entity,List<meioWmsWarehousePointListingRuleItemEntity> meioWmsWarehousePointListingRuleItemList,string deleteList,string type);
        #endregion

    }
}
