﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-06 09:58
    /// 描 述：上架定位规则
    /// </summary>
    public class MeioWmsWarehousePointListingRuleBLL : MeioWmsWarehousePointListingRuleIBLL
    {
        private MeioWmsWarehousePointListingRuleService meioWmsWarehousePointListingRuleService = new MeioWmsWarehousePointListingRuleService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehousePointListingRuleEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioWmsWarehousePointListingRuleService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehousePointListingRuleItem表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehousePointListingRuleItemEntity> GetmeioWmsWarehousePointListingRuleItemList(string keyValue)
        {
            try
            {
                return meioWmsWarehousePointListingRuleService.GetmeioWmsWarehousePointListingRuleItemList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehousePointListingRule表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehousePointListingRuleEntity GetmeioWmsWarehousePointListingRuleEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehousePointListingRuleService.GetmeioWmsWarehousePointListingRuleEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehousePointListingRuleItem表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehousePointListingRuleItemEntity GetmeioWmsWarehousePointListingRuleItemEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehousePointListingRuleService.GetmeioWmsWarehousePointListingRuleItemEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsWarehousePointListingRuleService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehousePointListingRuleEntity entity,List<meioWmsWarehousePointListingRuleItemEntity> meioWmsWarehousePointListingRuleItemList,string deleteList,string type)
        {
            try
            {
                meioWmsWarehousePointListingRuleService.SaveEntity(keyValue, entity,meioWmsWarehousePointListingRuleItemList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
