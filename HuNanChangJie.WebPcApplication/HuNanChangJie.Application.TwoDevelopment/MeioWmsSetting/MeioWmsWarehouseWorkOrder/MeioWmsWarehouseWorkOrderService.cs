﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-30 16:19
    /// 描 述：工作单
    /// </summary>
    public class MeioWmsWarehouseWorkOrderService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseWorkOrderEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.No,
                t.ShipperID,
                t.Code,
                t.Name,
                t.Type,
                t.StorageLocation,
                t.PlanNum,
                t.CanNum,
                t.EndNum,
                t.StartState,
                t.EndState
                ");
                strSql.Append("  FROM meioWmsWarehouseWorkOrder t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["No"].IsEmpty())
                {
                    dp.Add("No", "%" + queryParam["No"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.No Like @No ");
                }
                if (!queryParam["ShipperID"].IsEmpty())
                {
                    dp.Add("ShipperID", "%" + queryParam["ShipperID"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ShipperID Like @ShipperID ");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", "%" + queryParam["Type"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Type Like @Type ");
                }
                if (!queryParam["StartState"].IsEmpty())
                {
                    dp.Add("StartState", "%" + queryParam["StartState"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.StartState Like @StartState ");
                }
                if (!queryParam["EndState"].IsEmpty())
                {
                    dp.Add("EndState", "%" + queryParam["EndState"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.EndState Like @EndState ");
                }
                var list=this.BaseRepository().FindList<meioWmsWarehouseWorkOrderEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseWorkOrderDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseWorkOrderDetailEntity> GetmeioWmsWarehouseWorkOrderDetailList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<meioWmsWarehouseWorkOrderDetailEntity>(t=>t.WorkOrderID == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseWorkOrder表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseWorkOrderEntity GetmeioWmsWarehouseWorkOrderEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseWorkOrderEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseWorkOrderDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseWorkOrderDetailEntity GetmeioWmsWarehouseWorkOrderDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseWorkOrderDetailEntity>(t=>t.WorkOrderID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioWmsWarehouseWorkOrderEntity = GetmeioWmsWarehouseWorkOrderEntity(keyValue); 
                db.Delete<meioWmsWarehouseWorkOrderEntity>(t=>t.ID == keyValue);
                db.Delete<meioWmsWarehouseWorkOrderDetailEntity>(t=>t.WorkOrderID == meioWmsWarehouseWorkOrderEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseWorkOrderEntity entity,List<meioWmsWarehouseWorkOrderDetailEntity> meioWmsWarehouseWorkOrderDetailList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var meioWmsWarehouseWorkOrderEntityTmp = GetmeioWmsWarehouseWorkOrderEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var meioWmsWarehouseWorkOrderDetailUpdateList= meioWmsWarehouseWorkOrderDetailList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in meioWmsWarehouseWorkOrderDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var meioWmsWarehouseWorkOrderDetailInserList= meioWmsWarehouseWorkOrderDetailList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in meioWmsWarehouseWorkOrderDetailInserList)
                    {
                        item.Create(item.ID);
                        item.WorkOrderID = meioWmsWarehouseWorkOrderEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (meioWmsWarehouseWorkOrderDetailEntity item in meioWmsWarehouseWorkOrderDetailList)
                    {
                        item.WorkOrderID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
