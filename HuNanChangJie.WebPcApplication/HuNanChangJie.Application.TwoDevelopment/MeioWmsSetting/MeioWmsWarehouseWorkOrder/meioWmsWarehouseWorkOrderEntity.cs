﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-30 16:19
    /// 描 述：工作单
    /// </summary>
    public class meioWmsWarehouseWorkOrderEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 仓库
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        [Column("SHIPPERID")]
        public string ShipperID { get; set; }
        /// <summary>
        /// 入库单
        /// </summary>
        [Column("RECEIPTID")]
        public string ReceiptID { get; set; }
        /// <summary>
        /// 入库单
        /// </summary>
        [Column("RECEIPTNO")]
        public string ReceiptNo { get; set; }
        /// <summary>
        /// 单号
        /// </summary>
        [Column("NO")]
        public string No { get; set; }
        /// <summary>
        /// 加工货位
        /// </summary>
        [Column("STORAGELOCATION")]
        public string StorageLocation { get; set; }
        /// <summary>
        /// 加工类型
        /// </summary>
        [Column("TYPE")]
        public string Type { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 单位规格
        /// </summary>
        [Column("UNITSPEC")]
        public string UnitSpec { get; set; }
        /// <summary>
        /// 单位数量
        /// </summary>
        [Column("UNITNUM")]
        public int? UnitNum { get; set; }
        /// <summary>
        /// 成品编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 成品名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 计划加工数量
        /// </summary>
        [Column("PLANNUM")]
        public int? PlanNum { get; set; }
        /// <summary>
        /// 可加工数量
        /// </summary>
        [Column("CANNUM")]
        public int? CanNum { get; set; }
        /// <summary>
        /// 已加工数量
        /// </summary>
        [Column("ENDNUM")]
        public int? EndNum { get; set; }
        /// <summary>
        /// 计划完工日期
        /// </summary>
        [Column("PLANENDDATE")]
        public DateTime? PlanEndDate { get; set; }
        /// <summary>
        /// 完成用户
        /// </summary>
        [Column("ENDUSER")]
        public string EndUser { get; set; }
        /// <summary>
        /// 完成时间
        /// </summary>
        [Column("ENDDATE")]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 首状态
        /// </summary>
        [Column("STARTSTATE")]
        public string StartState { get; set; }
        /// <summary>
        /// 尾状态
        /// </summary>
        [Column("ENDSTATE")]
        public string EndState { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

