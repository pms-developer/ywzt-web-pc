﻿using HuNanChangJie.Util;
using HuNanChangJie.Util.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-10 11:47
    /// 描 述：订单管理
    /// </summary>
    public class Pay_OrderEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ORDERID")]
        public string F_OrderID { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        [Column("F_ORDERSN")]
        public string F_OrderSN { get; set; }
        /// <summary>
        /// 买家ID
        /// </summary>
        [Column("F_BUYERID")]
        public string F_BuyerId { get; set; }
        /// <summary>
        /// 买家姓名
        /// </summary>
        [Column("F_BUYERNAME")]
        public string F_BuyerName { get; set; }
        /// <summary>
        /// 买家邮箱
        /// </summary>
        [Column("F_BUYEREMAIL")]
        public string F_BuyerEmail { get; set; }
        /// <summary>
        /// 订单生成时间
        /// </summary>
        [Column("F_CREATETIME")]
        public DateTime? F_CreateTime { get; set; }
        /// <summary>
        /// 来自什么平台的订单 默认 0 pc，1 安卓，2 ios
        /// </summary>
        [Column("F_ORDERPLATFORM")]
        public string F_OrderPlatform { get; set; }
        /// <summary>
        /// 支付方式ID
        /// </summary>
        [Column("F_PAYMENTID")]
        public string F_PaymentId { get; set; }
        /// <summary>
        /// 付款状态:0:未付款;1:已付款
        /// </summary>
        [Column("F_PAYMENTSTATE")]
        public int? F_PaymentState { get; set; }
        /// <summary>
        /// 外部订单号
        /// </summary>
        [Column("F_OUTSN")]
        public string F_OutSN { get; set; }
        /// <summary>
        /// 交易流水号
        /// </summary>
        [Column("F_TRADESN")]
        public string F_TradeSN { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        [Column("F_PAYMENTTIME")]
        public DateTime? F_PaymentTime { get; set; }
        /// <summary>
        /// 支付留言
        /// </summary>
        [Column("F_PAYMESSAGE")]
        public string F_PayMessage { get; set; }
        /// <summary>
        /// 订单完成时间
        /// </summary>
        [Column("F_FINNSHEDTIME")]
        public DateTime? F_FinnshedTime { get; set; }
        /// <summary>
        /// 发票信息
        /// </summary>
        [Column("F_INVOICE")]
        public string F_Invoice { get; set; }
        /// <summary>
        /// 商品总价格
        /// </summary>
        [Column("F_GOODSAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? F_GoodsAmount { get; set; }
        /// <summary>
        /// 优惠总金额
        /// </summary>
        [Column("F_DISCOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? F_Discount { get; set; } = 0;
        /// <summary>
        /// 订单应付金额
        /// </summary>
        [Column("F_ORDERAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? F_OrderAmount { get; set; } = 0;
        /// <summary>
        /// 订单总价格
        /// </summary>
        [Column("F_ORDERTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? F_OrderTotalPrice { get; set; } = 0;
        /// <summary>
        /// 订单状态：0:已取消;10:待付款;40:交易完成;50:已提交;60:已确认;
        /// </summary>
        [Column("F_ORDERSTATE")]
        public int? F_OrderState { get; set; } = 0;
        /// <summary>
        /// 代金券ID
        /// </summary>
        [Column("F_VOUCHERID")]
        public string F_VoucherId { get; set; }
        /// <summary>
        /// F_VoucherPrice
        /// </summary>
        [Column("F_VOUCHERPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? F_VoucherPrice { get; set; } = 0;
        /// <summary>
        /// 代金券代码
        /// </summary>
        [Column("F_VOUCHERCODE")]
        public string F_VoucherCode { get; set; }
        /// <summary>
        /// 退款状态:0是无退款,1是部分退款,2是全部退款
        /// </summary>
        [Column("F_REFUNDSTATE")]
        public int? F_RefundState { get; set; }
        /// <summary>
        /// 退款金额
        /// </summary>
        [Column("F_REFUNDAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? F_RefundAmount { get; set; } = 0;
        /// <summary>
        /// 退款说明
        /// </summary>
        [Column("F_REFUNDCAUSE")]
        public string F_RefundCause { get; set; }
        /// <summary>
        /// F_BuyerMobile
        /// </summary>
        [Column("F_BUYERMOBILE")]
        public string F_BuyerMobile { get; set; }
        /// <summary>
        /// 前台通知url
        /// </summary>
        [Column("F_FRONTURL")]
        public string F_FrontUrl { get; set; }
        /// <summary>
        /// F_BackUrl
        /// </summary>
        [Column("F_BACKURL")]
        public string F_BackUrl { get; set; }
        /// <summary>
        /// 银行简称
        /// </summary>
        [Column("F_BANKSHORT")]
        public string F_BankShort { get; set; }
        /// <summary>
        /// 银行名称
        /// </summary>
        [Column("F_BANKNAME")]
        public string F_BankName { get; set; }

        [Column("AddressJson")]
        public string AddressJson { get; set; }

        
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(UserInfo userInfo)
      {
            this.F_OrderID = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue, UserInfo userInfo)
        {
            this.F_OrderID = keyValue;
        }
        #endregion
        #region  扩展字段

        #region  实体成员
        /// <summary>
        /// F_InvId
        /// </summary>
        [NotMapped]//[NotMapped]//[Column("F_INVID")]
        
        public string F_InvId { get; set; }
        /// <summary>
        /// 会员ID
        /// </summary>
        [NotMapped]//[Column("F_MEMBERID")]
        public string F_MemberId { get; set; }
        /// <summary>
        /// 订单ID
        /// </summary>
        [NotMapped]//[Column("F_ORDERID")]
        public string F_OrderId { get; set; }
        /// <summary>
        /// 1普通发票2增值税发票
        /// </summary>
        [NotMapped]//[Column("F_STATE")]
        public string F_State { get; set; }
        /// <summary>
        /// 发票抬头[普通发票]
        /// </summary>
        [NotMapped]//[Column("F_TITLE")]
        public string F_Title { get; set; }
        /// <summary>
        /// 发票内容[普通发票]
        /// </summary>
        [NotMapped]//[Column("F_INVCONTENT")]
        public string F_InvContent { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        [NotMapped]//[Column("F_COMPANY")]
        public string F_Company { get; set; }
        /// <summary>
        /// 纳税人识别号
        /// </summary>
        [NotMapped]//[Column("F_CODE")]
        public string F_Code { get; set; }
        /// <summary>
        /// 注册地址
        /// </summary>
        [NotMapped]//[Column("F_REGADDR")]
        public string F_RegAddr { get; set; }
        /// <summary>
        /// 注册电话
        /// </summary>
        [NotMapped]//[Column("F_REGPHONE")]
        public string F_RegPhone { get; set; }
        /// <summary>
        /// 开户银行
        /// </summary>
        [NotMapped]//[Column("F_REGBNAME")]
        public string F_RegBname { get; set; }
        /// <summary>
        /// 银行帐户
        /// </summary>
        [NotMapped]//[Column("F_REGBACCOUNT")]
        public string F_RegBaccount { get; set; }
        /// <summary>
        /// 收票人姓名
        /// </summary>
        [NotMapped]//[Column("F_RECNAME")]
        public string F_RecName { get; set; }
        /// <summary>
        /// 收票人手机号
        /// </summary>
        [NotMapped]//[Column("F_RECMOBPHONE")]
        public string F_RecMobphone { get; set; }
        /// <summary>
        /// 收票人省份
        /// </summary>
        [NotMapped]//[Column("F_RECPROVINCE")]
        public string F_RecProvince { get; set; }
        /// <summary>
        /// 送票地址
        /// </summary>
        [NotMapped]//[Column("F_GOTOADDR")]
        public string F_GotoAddr { get; set; }
        /// <summary>
        /// 是否默认得
        /// </summary>
        [NotMapped]//[Column("F_ISDEFAULT")]
        public int? F_IsDefault { get; set; }
        /// <summary>
        /// 发票类型：电子发票 纸质发票
        /// </summary>
        [NotMapped]//[Column("F_INVTYPE")]
        public string F_InvType { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [NotMapped]//[Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        [NotMapped]
        public string F_IsOutInvoice { get; set; }



        [NotMapped]
        public string F_GoodsName { get; set; }

        #endregion
        #endregion
    }
}

