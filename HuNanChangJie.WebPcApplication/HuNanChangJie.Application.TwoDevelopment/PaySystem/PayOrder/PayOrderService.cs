﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-10 11:47
    /// 描 述：订单管理
    /// </summary>
    public class PayOrderService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表分页数据
        /// <summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_OrderEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_OrderID,
                t.F_OrderSN,
                t.F_BuyerMobile,
                t.F_BuyerId,
                t.F_BuyerName,
                t.F_BuyerEmail,
                t.F_CreateTime,
                t.F_OutSN,
                t.F_TradeSN,
                t.F_PaymentTime,
                t.F_OrderAmount,
                t.F_OrderPlatform,
                t.F_PaymentState,
                t.F_OrderState,
                t.F_PaymentId,
                t.F_RefundState,
                t.F_RefundAmount,
                t.F_RefundCause,
                t1.F_GoodsName

                ");
                strSql.Append("  FROM tPay_Order t  LEFT JOIN tPay_Order_Goods t1 on t1.F_OrderID=t.F_OrderID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.F_CreateTime >= @startTime AND t.F_CreateTime <= @endTime ) ");
                }
                if (!queryParam["F_OrderSN"].IsEmpty())
                {
                    dp.Add("F_OrderSN", "%" + queryParam["F_OrderSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderSN Like @F_OrderSN ");
                }
                if (!queryParam["F_BuyerId"].IsEmpty())
                {
                    dp.Add("F_BuyerId", "%" + queryParam["F_BuyerId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_BuyerId Like @F_BuyerId ");
                }
                if (!queryParam["F_BuyerName"].IsEmpty())
                {
                    dp.Add("F_BuyerName", "%" + queryParam["F_BuyerName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_BuyerName Like @F_BuyerName ");
                }
                if (!queryParam["F_BuyerEmail"].IsEmpty())
                {
                    dp.Add("F_BuyerEmail", "%" + queryParam["F_BuyerEmail"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_BuyerEmail Like @F_BuyerEmail ");
                }
                if (!queryParam["F_CreateTime"].IsEmpty())
                {
                    dp.Add("F_CreateTime",queryParam["F_CreateTime"].ToString(), DbType.String);
                    strSql.Append(" AND t.F_CreateTime = @F_CreateTime ");
                }
                if (!queryParam["F_OutSN"].IsEmpty())
                {
                    dp.Add("F_OutSN", "%" + queryParam["F_OutSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OutSN Like @F_OutSN ");
                }
                if (!queryParam["F_TradeSN"].IsEmpty())
                {
                    dp.Add("F_TradeSN", "%" + queryParam["F_TradeSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_TradeSN Like @F_TradeSN ");
                }
                if (!queryParam["F_PaymentTime"].IsEmpty())
                {
                    dp.Add("F_PaymentTime", "%" + queryParam["F_PaymentTime"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_PaymentTime Like @F_PaymentTime ");
                }
                if (!queryParam["F_OrderAmount"].IsEmpty())
                {
                    dp.Add("F_OrderAmount", "%" + queryParam["F_OrderAmount"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderAmount Like @F_OrderAmount ");
                }
                if (!queryParam["F_OrderPlatform"].IsEmpty())
                {
                    dp.Add("F_OrderPlatform",queryParam["F_OrderPlatform"].ToString(), DbType.String);
                    strSql.Append(" AND t.F_OrderPlatform = @F_OrderPlatform ");
                }
                if (!queryParam["F_PaymentState"].IsEmpty())
                {
                    dp.Add("F_PaymentState",queryParam["F_PaymentState"].ToString(), DbType.String);
                    strSql.Append(" AND t.F_PaymentState = @F_PaymentState ");
                }
                if (!queryParam["F_RefundState"].IsEmpty())
                {
                    dp.Add("F_RefundState", queryParam["F_RefundState"].ToString(), DbType.Int32);
                    strSql.Append(" AND t.F_RefundState = @F_RefundState ");
                }
                if (!queryParam["F_OrderState"].IsEmpty())
                {
                    dp.Add("F_OrderState",queryParam["F_OrderState"].ToString(), DbType.String);
                    strSql.Append(" AND t.F_OrderState = @F_OrderState ");
                }
                if (!queryParam["F_GoodsName"].IsEmpty())
                {
                    dp.Add("F_GoodsName", "%" + queryParam["F_GoodsName"].ToString() + "%", DbType.String);
                   
                    strSql.Append(" AND t1.F_GoodsName Like @F_GoodsName ");
                }
                return this.BaseRepository("PaySystem").FindList<Pay_OrderEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_OrderEntity> GetList(string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_OrderID,
                t.F_OrderSN,
                t.F_BuyerId,
                t.F_BuyerName,
                t.F_BuyerEmail,
                t.F_CreateTime,
                t.F_OutSN,
                t.F_TradeSN,
                t.F_PaymentTime,
                t.F_OrderAmount,
                t.F_OrderPlatform,
                t.F_PaymentState,
                t.F_OrderState,
                t.AddressJson
                ");
                strSql.Append("  FROM tPay_Order t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.F_CreateTime >= @startTime AND t.F_CreateTime <= @endTime ) ");
                }
                if (!queryParam["F_OrderSN"].IsEmpty())
                {
                    dp.Add("F_OrderSN", "%" + queryParam["F_OrderSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderSN Like @F_OrderSN ");
                }
                if (!queryParam["F_BuyerId"].IsEmpty())
                {
                    dp.Add("F_BuyerId", "%" + queryParam["F_BuyerId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_BuyerId Like @F_BuyerId ");
                }
                if (!queryParam["F_BuyerName"].IsEmpty())
                {
                    dp.Add("F_BuyerName", "%" + queryParam["F_BuyerName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_BuyerName Like @F_BuyerName ");
                }
                if (!queryParam["F_BuyerEmail"].IsEmpty())
                {
                    dp.Add("F_BuyerEmail", "%" + queryParam["F_BuyerEmail"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_BuyerEmail Like @F_BuyerEmail ");
                }
                if (!queryParam["F_CreateTime"].IsEmpty())
                {
                    dp.Add("F_CreateTime",queryParam["F_CreateTime"].ToString(), DbType.String);
                    strSql.Append(" AND t.F_CreateTime = @F_CreateTime ");
                }
                if (!queryParam["F_OutSN"].IsEmpty())
                {
                    dp.Add("F_OutSN", "%" + queryParam["F_OutSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OutSN Like @F_OutSN ");
                }
                if (!queryParam["F_TradeSN"].IsEmpty())
                {
                    dp.Add("F_TradeSN", "%" + queryParam["F_TradeSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_TradeSN Like @F_TradeSN ");
                }
                if (!queryParam["F_PaymentTime"].IsEmpty())
                {
                    dp.Add("F_PaymentTime", "%" + queryParam["F_PaymentTime"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_PaymentTime Like @F_PaymentTime ");
                }
                if (!queryParam["F_OrderAmount"].IsEmpty())
                {
                    dp.Add("F_OrderAmount", "%" + queryParam["F_OrderAmount"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderAmount Like @F_OrderAmount ");
                }
                if (!queryParam["F_OrderPlatform"].IsEmpty())
                {
                    dp.Add("F_OrderPlatform",queryParam["F_OrderPlatform"].ToString(), DbType.String);
                    strSql.Append(" AND t.F_OrderPlatform = @F_OrderPlatform ");
                }
                if (!queryParam["F_PaymentState"].IsEmpty())
                {
                    dp.Add("F_PaymentState",queryParam["F_PaymentState"].ToString(), DbType.String);
                    strSql.Append(" AND t.F_PaymentState = @F_PaymentState ");
                }
                if (!queryParam["F_OrderState"].IsEmpty())
                {
                    dp.Add("F_OrderState",queryParam["F_OrderState"].ToString(), DbType.String);
                    strSql.Append(" AND t.F_OrderState = @F_OrderState ");
                }
                return this.BaseRepository("PaySystem").FindList<Pay_OrderEntity>(strSql.ToString(),dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取tPay_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Pay_OrderEntity GettPay_OrderEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository("PaySystem").FindEntity<Pay_OrderEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository("PaySystem").Delete<Pay_OrderEntity>(t=>t.F_OrderID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity( UserInfo userInfo, string keyValue, Pay_OrderEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue,userInfo);
                    this.BaseRepository("PaySystem").Update(entity);
                }
                else
                {
                    entity.Create(userInfo);
                    this.BaseRepository("PaySystem").Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        public Pay_OrderEntity GettPay_OrderEntityByOrderSN(string ordersn)
        {
            try
            {
                return this.BaseRepository("PaySystem").FindEntity<Pay_OrderEntity>(p=>p.F_OrderSN==ordersn);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
