﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-17 11:25
    /// 描 述：退款管理
    /// </summary>
    public class PayRefundReturnService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表分页数据
        /// <summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_Refund_ReturnEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_RefundId,
                t.F_OrderID,
                t.F_OrderSN,
                t.F_RefundSN,
                t.F_BuyerID,
                t.F_OrderGoodsID,
                t.F_GoodsName,
                t.F_GoodsNum,
                t.F_RefundAmount,
                t.F_RefundType,
                t.F_RefundState,
                t.F_ReturnType,
                t.F_AdminTime,
                t.F_BuyerMessage,
                t.F_AdminMessage,
                t.F_ExpressName,
                t.F_AuditorStatus
                ");
                strSql.Append("  FROM tPay_Refund_Return t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["F_OrderSN"].IsEmpty())
                {
                    dp.Add("F_OrderSN", "%" + queryParam["F_OrderSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderSN Like @F_OrderSN ");
                }
                if (!queryParam["F_RefundSN"].IsEmpty())
                {
                    dp.Add("F_RefundSN", "%" + queryParam["F_RefundSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_RefundSN Like @F_RefundSN ");
                }
                if (!queryParam["F_RefundAmount"].IsEmpty())
                {
                    dp.Add("F_RefundAmount", "%" + queryParam["F_RefundAmount"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_RefundAmount Like @F_RefundAmount ");
                }
                return this.BaseRepository("PaySystem").FindList<Pay_Refund_ReturnEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_Refund_ReturnEntity> GetList(string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_RefundId,
                t.F_OrderID,
                t.F_OrderSN,
                t.F_RefundSN,
                t.F_BuyerID,
                t.F_OrderGoodsID,
                t.F_GoodsName,
                t.F_GoodsNum,
                t.F_RefundAmount,
                t.F_RefundType,
                t.F_RefundState,
                t.F_ReturnType,
                t.F_AdminTime,
                t.F_BuyerMessage,
                t.F_AdminMessage,
                t.F_ExpressName
                ");
                strSql.Append("  FROM tPay_Refund_Return t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["F_OrderSN"].IsEmpty())
                {
                    dp.Add("F_OrderSN", "%" + queryParam["F_OrderSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderSN Like @F_OrderSN ");
                }
                if (!queryParam["F_RefundSN"].IsEmpty())
                {
                    dp.Add("F_RefundSN", "%" + queryParam["F_RefundSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_RefundSN Like @F_RefundSN ");
                }
                if (!queryParam["F_RefundAmount"].IsEmpty())
                {
                    dp.Add("F_RefundAmount", "%" + queryParam["F_RefundAmount"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_RefundAmount Like @F_RefundAmount ");
                }
                return this.BaseRepository("PaySystem").FindList<Pay_Refund_ReturnEntity>(strSql.ToString(),dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取tPay_Refund_Return表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Pay_Refund_ReturnEntity GettPay_Refund_ReturnEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository("PaySystem").FindEntity<Pay_Refund_ReturnEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository("PaySystem").Delete<Pay_Refund_ReturnEntity>(t=>t.F_RefundId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity( UserInfo userInfo, string keyValue, Pay_Refund_ReturnEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue,userInfo);
                    this.BaseRepository("PaySystem").Update(entity);
                }
                else
                {
                    entity.Create(userInfo);
                    this.BaseRepository("PaySystem").Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
