﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-17 11:25
    /// 描 述：退款管理
    /// </summary>
    public class PayRefundReturnBLL : PayRefundReturnIBLL
    {
        private PayRefundReturnService payRefundReturnService = new PayRefundReturnService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表分页数据
        /// <summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_Refund_ReturnEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return payRefundReturnService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_Refund_ReturnEntity> GetList(string queryJson)
        {
            try
            {
                return payRefundReturnService.GetList(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取tPay_Refund_Return表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Pay_Refund_ReturnEntity GettPay_Refund_ReturnEntity(string keyValue)
        {
            try
            {
                return payRefundReturnService.GettPay_Refund_ReturnEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                payRefundReturnService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(UserInfo userInfo, string keyValue, Pay_Refund_ReturnEntity entity)
        {
            try
            {
                payRefundReturnService.SaveEntity(userInfo, keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
