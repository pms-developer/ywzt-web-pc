﻿using HuNanChangJie.Util;
using HuNanChangJie.Util.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-17 11:25
    /// 描 述：退款管理
    /// </summary>
    public class Pay_Refund_ReturnEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_REFUNDID")]
        public string F_RefundId { get; set; }
        /// <summary>
        /// 订单id
        /// </summary>
        [Column("F_ORDERID")]
        public string F_OrderID { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        [Column("F_ORDERSN")]
        public string F_OrderSN { get; set; }
        /// <summary>
        /// 退款编号
        /// </summary>
        [Column("F_REFUNDSN")]
        public string F_RefundSN { get; set; }
        /// <summary>
        /// 购买人id
        /// </summary>
        [Column("F_BUYERID")]
        public string F_BuyerID { get; set; }
        /// <summary>
        /// 商品ID,全部退款是0
        /// </summary>
        [Column("F_GOODSID")]
        public string F_GoodsID { get; set; }
        /// <summary>
        /// 商品ID,全部退款是0
        /// </summary>
        [Column("F_ORDERGOODSID")]
        public string F_OrderGoodsID { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        [Column("F_GOODSNAME")]
        public string F_GoodsName { get; set; }
        /// <summary>
        /// 商品数量
        /// </summary>
        [Column("F_GOODSNUM")]
        public int? F_GoodsNum { get; set; }
        /// <summary>
        /// 退款金额
        /// </summary>
        [Column("F_REFUNDAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? F_RefundAmount { get; set; } = 0;
        /// <summary>
        /// 申请类型:1为退款,2为退货,默认为1
        /// </summary>
        [Column("F_REFUNDTYPE")]
        public byte? F_RefundType { get; set; }
        /// <summary>
        /// 申请状态:1为处理中,2为待管理员处理,3为已完成,默认为1
        /// </summary>
        [Column("F_REFUNDSTATE")]
        public byte? F_RefundState { get; set; }
        /// <summary>
        /// 退货类型:1为不用退货,2为需要退货,默认为1
        /// </summary>
        [Column("F_RETURNTYPE")]
        public byte? F_ReturnType { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 管理员操作日期
        /// </summary>
        [Column("F_ADMINTIME")]
        public DateTime? F_AdminTime { get; set; }
        /// <summary>
        /// 申请原因
        /// </summary>
        [Column("F_BUYERMESSAGE")]
        public string F_BuyerMessage { get; set; }
        /// <summary>
        /// 管理员备注
        /// </summary>
        [Column("F_ADMINMESSAGE")]
        public string F_AdminMessage { get; set; }
        /// <summary>
        /// 物流公司编号
        /// </summary>
        [Column("F_EXPRESSID")]
        public string F_ExpressId { get; set; }
        /// <summary>
        /// 物流单号
        /// </summary>
        [Column("F_INVOICENO")]
        public string F_InvoiceNo { get; set; }
        /// <summary>
        /// 物流公司名称
        /// </summary>
        [Column("F_EXPRESSNAME")]
        public string F_ExpressName { get; set; }
        /// <summary>
        /// Creation_Id
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// CreationName
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// F_PayingThird
        /// </summary>
        [Column("F_PAYINGTHIRD")]
        public string F_PayingThird { get; set; }

        [Column("F_AuditorStatus")]
        public int? F_AuditorStatus { get; set; }
        
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(UserInfo userInfo)
      {
            this.F_RefundId = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue, UserInfo userInfo)
        {
            this.F_RefundId = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

