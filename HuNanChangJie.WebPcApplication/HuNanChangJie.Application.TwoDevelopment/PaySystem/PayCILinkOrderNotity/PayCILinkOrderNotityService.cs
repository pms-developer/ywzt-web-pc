﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-17 16:30
    /// 描 述：通知类
    /// </summary>
    public class PayCILinkOrderNotityService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表分页数据
        /// <summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_CILink_OrderNotityEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_ONID,
                t.F_Busicd,
                t.F_Txndir,
                t.F_Version,
                t.F_SignType,
                t.F_Charset,
                t.F_OrderNum,
                t.F_Respcd,
                t.F_Inscd,
                t.F_Chcd,
                t.F_Attach,
                t.F_State,
                t.F_OrigOrderNum,
                t.F_tn,
                t.F_Mchntid,
                t.F_Terminalid,
                t.F_Txamt,
                t.F_Currency,
                t.F_GoodsList,
                t.F_ChannelOrderNum,
                t.F_ConsumerAccount,
                t.F_ConsumerId,
                t.F_ErrorDetail,
                t.F_Sign,
                t.F_ChcdDiscount,
                t.F_MerDiscount,
                t.F_OutOrderNum,
                t.F_TransTime,
                t.F_BankType,
                t.F_Subject,
                t.F_QRcode,
                t.F_BackUrl,
                t.F_FrontUrl,
                t.F_PayTime,
                t.F_TimeStart,
                t.F_TimeExpire,
                t.F_Operatorid
                ");
                strSql.Append("  FROM Pay_CILink_OrderNotity t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.F_PayTime >= @startTime AND t.F_PayTime <= @endTime ) ");
                }
                if (!queryParam["F_OrderNum"].IsEmpty())
                {
                    dp.Add("F_OrderNum", "%" + queryParam["F_OrderNum"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderNum Like @F_OrderNum ");
                }
                if (!queryParam["F_OrigOrderNum"].IsEmpty())
                {
                    dp.Add("F_OrigOrderNum", "%" + queryParam["F_OrigOrderNum"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrigOrderNum Like @F_OrigOrderNum ");
                }
                if (!queryParam["F_State"].IsEmpty())
                {
                    dp.Add("F_State", "%" + queryParam["F_State"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_State Like @F_State ");
                }
                if (!queryParam["F_Txamt"].IsEmpty())
                {
                    dp.Add("F_Txamt", "%" + queryParam["F_Txamt"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_Txamt Like @F_Txamt ");
                }
                return this.BaseRepository("PaySystem").FindList<Pay_CILink_OrderNotityEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_CILink_OrderNotityEntity> GetList(string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_ONID,
                t.F_Busicd,
                t.F_Txndir,
                t.F_Version,
                t.F_SignType,
                t.F_Charset,
                t.F_OrderNum,
                t.F_Respcd,
                t.F_Inscd,
                t.F_Chcd,
                t.F_Attach,
                t.F_State,
                t.F_OrigOrderNum,
                t.F_tn,
                t.F_Mchntid,
                t.F_Terminalid,
                t.F_Txamt,
                t.F_Currency,
                t.F_GoodsList,
                t.F_ChannelOrderNum,
                t.F_ConsumerAccount,
                t.F_ConsumerId,
                t.F_ErrorDetail,
                t.F_Sign,
                t.F_ChcdDiscount,
                t.F_MerDiscount,
                t.F_OutOrderNum,
                t.F_TransTime,
                t.F_BankType,
                t.F_Subject,
                t.F_QRcode,
                t.F_BackUrl,
                t.F_FrontUrl,
                t.F_PayTime,
                t.F_TimeStart,
                t.F_TimeExpire,
                t.F_Operatorid
                ");
                strSql.Append("  FROM Pay_CILink_OrderNotity t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.F_PayTime >= @startTime AND t.F_PayTime <= @endTime ) ");
                }
                if (!queryParam["F_OrderNum"].IsEmpty())
                {
                    dp.Add("F_OrderNum", "%" + queryParam["F_OrderNum"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderNum Like @F_OrderNum ");
                }
                if (!queryParam["F_OrigOrderNum"].IsEmpty())
                {
                    dp.Add("F_OrigOrderNum", "%" + queryParam["F_OrigOrderNum"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrigOrderNum Like @F_OrigOrderNum ");
                }
                if (!queryParam["F_State"].IsEmpty())
                {
                    dp.Add("F_State", "%" + queryParam["F_State"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_State Like @F_State ");
                }
                if (!queryParam["F_Txamt"].IsEmpty())
                {
                    dp.Add("F_Txamt", "%" + queryParam["F_Txamt"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_Txamt Like @F_Txamt ");
                }
                return this.BaseRepository("PaySystem").FindList<Pay_CILink_OrderNotityEntity>(strSql.ToString(),dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Pay_CILink_OrderNotity表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Pay_CILink_OrderNotityEntity GetPay_CILink_OrderNotityEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository("PaySystem").FindEntity<Pay_CILink_OrderNotityEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository("PaySystem").Delete<Pay_CILink_OrderNotityEntity>(t=>t.F_ONID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity( UserInfo userInfo, string keyValue, Pay_CILink_OrderNotityEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue,userInfo);
                    this.BaseRepository("PaySystem").Update(entity);
                }
                else
                {
                    entity.Create(userInfo);
                    this.BaseRepository("PaySystem").Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
