﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-17 16:30
    /// 描 述：通知类
    /// </summary>
    public class Pay_CILink_OrderNotityEntity 
    {
        #region  实体成员
        /// <summary>
        /// F_ONID
        /// </summary>
        [Column("F_ONID")]
        public string F_ONID { get; set; }
        /// <summary>
        /// 交易类型
        /// </summary>
        [Column("F_BUSICD")]
        public string F_Busicd { get; set; }
        /// <summary>
        /// 交易方向
        /// </summary>
        [Column("F_TXNDIR")]
        public string F_Txndir { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        [Column("F_VERSION")]
        public string F_Version { get; set; }
        /// <summary>
        /// 签名方式
        /// </summary>
        [Column("F_SIGNTYPE")]
        public string F_SignType { get; set; }
        /// <summary>
        /// 编码格式
        /// </summary>
        [Column("F_CHARSET")]
        public string F_Charset { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        [Column("F_ORDERNUM")]
        public string F_OrderNum { get; set; }
        /// <summary>
        /// 交易结果
        /// </summary>
        [Column("F_RESPCD")]
        public string F_Respcd { get; set; }
        /// <summary>
        /// 机构号
        /// </summary>
        [Column("F_INSCD")]
        public string F_Inscd { get; set; }
        /// <summary>
        /// 渠道
        /// </summary>
        [Column("F_CHCD")]
        public string F_Chcd { get; set; }
        /// <summary>
        /// 商户号
        /// </summary>
        [Column("F_MCHNTID")]
        public string F_Mchntid { get; set; }
        /// <summary>
        /// 终端号
        /// </summary>
        [Column("F_TERMINALID")]
        public string F_Terminalid { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        [Column("F_TXAMT")]
        public string F_Txamt { get; set; }
        /// <summary>
        /// 交易币种
        /// </summary>
        [Column("F_CURRENCY")]
        public string F_Currency { get; set; }
        /// <summary>
        /// 商品列表
        /// </summary>
        [Column("F_GOODSLIST")]
        public string F_GoodsList { get; set; }
        /// <summary>
        /// 渠道交易号
        /// </summary>
        [Column("F_CHANNELORDERNUM")]
        public string F_ChannelOrderNum { get; set; }
        /// <summary>
        /// 用户账号
        /// </summary>
        [Column("F_CONSUMERACCOUNT")]
        public string F_ConsumerAccount { get; set; }
        /// <summary>
        /// 渠道号
        /// </summary>
        [Column("F_CONSUMERID")]
        public string F_ConsumerId { get; set; }
        /// <summary>
        /// 响应信息
        /// </summary>
        [Column("F_ERRORDETAIL")]
        public string F_ErrorDetail { get; set; }
        /// <summary>
        /// 扫码号
        /// </summary>
        [Column("F_SCANCODEID")]
        public string F_ScanCodeId { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        [Column("F_SIGN")]
        public string F_Sign { get; set; }
        /// <summary>
        /// 渠道优惠
        /// </summary>
        [Column("F_CHCDDISCOUNT")]
        public string F_ChcdDiscount { get; set; }
        /// <summary>
        /// 商户优惠
        /// </summary>
        [Column("F_MERDISCOUNT")]
        public string F_MerDiscount { get; set; }
        /// <summary>
        /// 外部订单号
        /// </summary>
        [Column("F_OUTORDERNUM")]
        public string F_OutOrderNum { get; set; }
        /// <summary>
        /// 银行标识
        /// </summary>
        [Column("F_BANKTYPE")]
        public string F_BankType { get; set; }
        /// <summary>
        /// 交易时间
        /// </summary>
        [Column("F_TRANSTIME")]
        public string F_TransTime { get; set; }
        /// <summary>
        /// 订单标题
        /// </summary>
        [Column("F_SUBJECT")]
        public string F_Subject { get; set; }
        /// <summary>
        /// 二维码信息
        /// </summary>
        [Column("F_QRCODE")]
        public string F_QRcode { get; set; }
        /// <summary>
        /// 异步通知地址
        /// </summary>
        [Column("F_BACKURL")]
        public string F_BackUrl { get; set; }
        /// <summary>
        /// 前台地址
        /// </summary>
        [Column("F_FRONTURL")]
        public string F_FrontUrl { get; set; }
        /// <summary>
        /// 附加信息
        /// </summary>
        [Column("F_ATTACH")]
        public string F_Attach { get; set; }
        /// <summary>
        /// 支付状态
        /// </summary>
        [Column("F_STATE")]
        public string F_State { get; set; }
        /// <summary>
        /// 原订单号
        /// </summary>
        [Column("F_ORIGORDERNUM")]
        public string F_OrigOrderNum { get; set; }
        /// <summary>
        /// app支付调起参数
        /// </summary>
        [Column("F_TN")]
        public string F_tn { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        [Column("F_PAYTIME")]
        public string F_PayTime { get; set; }
        /// <summary>
        /// 交易起始时间
        /// </summary>
        [Column("F_TIMESTART")]
        public string F_TimeStart { get; set; }
        /// <summary>
        /// 交易结束时间
        /// </summary>
        [Column("F_TIMEEXPIRE")]
        public string F_TimeExpire { get; set; }
        /// <summary>
        /// 付款限制
        /// </summary>
        [Column("F_PAYLIMIT")]
        public string F_Paylimit { get; set; }
        /// <summary>
        /// 操作员号
        /// </summary>
        [Column("F_OPERATORID")]
        public string F_Operatorid { get; set; }
        /// <summary>
        /// 门店号
        /// </summary>
        [Column("F_STOREID")]
        public string F_Storeid { get; set; }
        /// <summary>
        /// 营销信息
        /// </summary>
        [Column("F_PROMOTIONLIST")]
        public string F_PromotionList { get; set; }
        /// <summary>
        /// 凭证订单号
        /// </summary>
        [Column("F_VOUCHERORDERNUM")]
        public string F_VoucherOrderNum { get; set; }
        /// <summary>
        /// 不打折金额
        /// </summary>
        [Column("F_UNDISCOUNTAMT")]
        public string F_UndiscountAmt { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 订单来源
        /// </summary>
        [Column("F_SOFTTYPE")]
        public int? F_SoftType { get; set; }
        /// <summary>
        /// F_RealName
        /// </summary>
        [Column("F_REALNAME")]
        public string F_RealName { get; set; }
        /// <summary>
        /// F_OldOrderNum
        /// </summary>
        [Column("F_OLDORDERNUM")]
        public string F_OldOrderNum { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(UserInfo userInfo)
      {
            this.F_ONID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue, UserInfo userInfo)
        {
            this.F_ONID = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

