﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-11 10:07
    /// 描 述：发票
    /// </summary>
    public interface OrderInvoiceIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表分页数据
        /// <summary>
        /// <param name="pagination">查询参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Pay_Order_InvoiceEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Pay_Order_InvoiceEntity> GetList(string queryJson);
        /// <summary>
        /// 获取tPay_Order_Invoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Pay_Order_InvoiceEntity GettPay_Order_InvoiceEntity(string keyValue);
        /// <summary>
        /// 获取tPay_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Pay_OrderEntity GettPay_OrderEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(UserInfo userInfo, string keyValue, Pay_OrderEntity entity,Pay_Order_InvoiceEntity tPay_Order_InvoiceEntity);
        void SaveEntity(string keyValue, Pay_Order_InvoiceEntity dbinvocie);
        #endregion

    }
}
