﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-11 10:07
    /// 描 述：发票
    /// </summary>
    public class OrderInvoiceService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表分页数据
        /// <summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_Order_InvoiceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_OrderID,
                t.F_OrderSN,
                t.F_BuyerName,
                t.F_OrderAmount,
                t.F_BuyerMobile,
                t1.F_IsOutInvoice,
                t1.F_Title,
                t1.F_InvId,
                t1.F_InvContent,
                t1.F_Company,
                t1.F_Code,
                t1.F_RegAddr,
                t1.F_RegPhone,
                t1.F_RegBname,
                t1.F_RegBaccount,
                t1.F_RecName,
                t1.F_RecMobphone,
                t1.F_RecProvince,
                t1.F_GotoAddr,
                t1.CreationDate
                ");
                strSql.Append("  FROM tPay_Order t ");
                strSql.Append("  LEFT JOIN tPay_Order_Invoice t1 ON t1.F_OrderID = t.F_OrderID ");
                strSql.Append("  WHERE 1=1 and  t.F_PaymentState=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.F_CreateTime >= @startTime AND t.F_CreateTime <= @endTime ) ");
                }
                if (!queryParam["F_OrderSN"].IsEmpty())
                {
                    dp.Add("F_OrderSN", "%" + queryParam["F_OrderSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderSN Like @F_OrderSN ");
                }
                if (!queryParam["F_BuyerName"].IsEmpty())
                {
                    dp.Add("F_BuyerName", "%" + queryParam["F_BuyerName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_BuyerName Like @F_BuyerName ");
                }
                if (!queryParam["F_Company"].IsEmpty())
                {
                    dp.Add("F_Company", "%" + queryParam["F_Company"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t1.F_Company Like @F_Company ");
                }
                if (!queryParam["F_RecName"].IsEmpty())
                {
                    dp.Add("F_RecName", "%" + queryParam["F_RecName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t1.F_RecName Like @F_RecName ");
                }
                if (!queryParam["F_RecMobphone"].IsEmpty())
                {
                    dp.Add("F_RecMobphone", "%" + queryParam["F_RecMobphone"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t1.F_RecMobphone Like @F_RecMobphone ");
                }
                if (!queryParam["F_IsOutInvoice"].IsEmpty())
                {
                    dp.Add("F_IsOutInvoice",   queryParam["F_IsOutInvoice"].ToString()  , DbType.String);
                    strSql.Append(" AND t1.F_IsOutInvoice = @F_IsOutInvoice ");
                }
                return this.BaseRepository("PaySystem").FindList<Pay_Order_InvoiceEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Pay_Order_InvoiceEntity> GetList(string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_OrderID,
                t.F_OrderSN,
                t.F_BuyerName,
                t.F_OrderAmount,
                t1.F_IsOutInvoice,
                t1.F_Title,
                t1.F_InvContent,
                t1.F_Company,
                t1.F_Code,
                t1.F_RegAddr,
                t1.F_RegPhone,
                t1.F_RegBname,
                t1.F_RegBaccount,
                t1.F_RecName,
                t1.F_RecMobphone,
                t1.F_RecProvince,
                t1.F_GotoAddr,
                t1.CreationDate
                ");
                strSql.Append("  FROM tPay_Order t ");
                strSql.Append("  LEFT JOIN tPay_Order_Invoice t1 ON t1.F_InvId = t.F_OrderID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.F_CreateTime >= @startTime AND t.F_CreateTime <= @endTime ) ");
                }
                if (!queryParam["F_OrderSN"].IsEmpty())
                {
                    dp.Add("F_OrderSN", "%" + queryParam["F_OrderSN"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_OrderSN Like @F_OrderSN ");
                }
                if (!queryParam["F_BuyerName"].IsEmpty())
                {
                    dp.Add("F_BuyerName", "%" + queryParam["F_BuyerName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_BuyerName Like @F_BuyerName ");
                }
                if (!queryParam["F_Company"].IsEmpty())
                {
                    dp.Add("F_Company", "%" + queryParam["F_Company"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t1.F_Company Like @F_Company ");
                }
                if (!queryParam["F_RecName"].IsEmpty())
                {
                    dp.Add("F_RecName", "%" + queryParam["F_RecName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t1.F_RecName Like @F_RecName ");
                }
                if (!queryParam["F_RecMobphone"].IsEmpty())
                {
                    dp.Add("F_RecMobphone", "%" + queryParam["F_RecMobphone"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t1.F_RecMobphone Like @F_RecMobphone ");
                }
                if (!queryParam["F_IsOutInvoice"].IsEmpty())
                {
                    if (queryParam["F_IsOutInvoice"].ToString()=="0")
                    {
                        dp.Add("F_IsOutInvoice", queryParam["F_IsOutInvoice"].ToString(), DbType.String);
                        strSql.Append(" AND ( t1.F_IsOutInvoice = @F_IsOutInvoice  or t1.F_IsOutInvoice is null )");
                    }
                    else
                    {
                        dp.Add("F_IsOutInvoice", queryParam["F_IsOutInvoice"].ToString(), DbType.String);
                        strSql.Append(" AND ( t1.F_IsOutInvoice = @F_IsOutInvoice    )");
                    }
                   
                }
                return this.BaseRepository("PaySystem").FindList<Pay_Order_InvoiceEntity>(strSql.ToString(),dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取tPay_Order_Invoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Pay_Order_InvoiceEntity GettPay_Order_InvoiceEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository("PaySystem").FindEntity<Pay_Order_InvoiceEntity>(t=>t.F_InvId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取tPay_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Pay_OrderEntity GettPay_OrderEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository("PaySystem").FindEntity<Pay_OrderEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository("PaySystem").BeginTrans();
            try
            {
                var tPay_OrderEntity = GettPay_OrderEntity(keyValue); 
                db.Delete<Pay_Order_InvoiceEntity>(t=>t.F_InvId == tPay_OrderEntity.F_OrderID);
                db.Delete<Pay_OrderEntity>(t=>t.F_OrderID == keyValue);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity( UserInfo userInfo, string keyValue, Pay_OrderEntity entity,Pay_Order_InvoiceEntity tPay_Order_InvoiceEntity)
        {
            var db = this.BaseRepository("PaySystem").BeginTrans();
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    var tPay_OrderEntityTmp = GettPay_OrderEntity(keyValue); 
                    entity.Modify(keyValue,userInfo);
                    db.Update(entity);
                    db.Delete<Pay_Order_InvoiceEntity>(t=>t.F_InvId == tPay_OrderEntityTmp.F_OrderID);
                    tPay_Order_InvoiceEntity.Create(userInfo);
                    tPay_Order_InvoiceEntity.F_InvId = tPay_OrderEntityTmp.F_OrderID;
                    db.Insert(tPay_Order_InvoiceEntity);
                }
                else
                {
                    entity.Create(userInfo);
                    db.Insert(entity);
                    tPay_Order_InvoiceEntity.Create(userInfo);
                    tPay_Order_InvoiceEntity.F_InvId = entity.F_OrderID;
                    db.Insert(tPay_Order_InvoiceEntity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        public void SaveEntity(string keyValue, Pay_Order_InvoiceEntity dbinvocie)
        {
            try
            {
                this.BaseRepository("PaySystem").Update(dbinvocie);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
