﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.PaySystem
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-11 10:07
    /// 描 述：发票
    /// </summary>
    public class Pay_Order_InvoiceEntity 
    {
        #region  实体成员
        /// <summary>
        /// F_InvId
        /// </summary>
        [Column("F_INVID")]
        public string F_InvId { get; set; }
        /// <summary>
        /// 会员ID
        /// </summary>
        [Column("F_MEMBERID")]
        public string F_MemberId { get; set; }
        /// <summary>
        /// 订单ID
        /// </summary>
        [Column("F_ORDERID")]
        public string F_OrderId { get; set; }
        /// <summary>
        /// 1普通发票2增值税发票
        /// </summary>
        [Column("F_STATE")]
        public string F_State { get; set; }
        /// <summary>
        /// 发票抬头[普通发票]
        /// </summary>
        [Column("F_TITLE")]
        public string F_Title { get; set; }
        /// <summary>
        /// 发票内容[普通发票]
        /// </summary>
        [Column("F_INVCONTENT")]
        public string F_InvContent { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        [Column("F_COMPANY")]
        public string F_Company { get; set; }
        /// <summary>
        /// 纳税人识别号
        /// </summary>
        [Column("F_CODE")]
        public string F_Code { get; set; }
        /// <summary>
        /// 注册地址
        /// </summary>
        [Column("F_REGADDR")]
        public string F_RegAddr { get; set; }
        /// <summary>
        /// 注册电话
        /// </summary>
        [Column("F_REGPHONE")]
        public string F_RegPhone { get; set; }
        /// <summary>
        /// 开户银行
        /// </summary>
        [Column("F_REGBNAME")]
        public string F_RegBname { get; set; }
        /// <summary>
        /// 银行帐户
        /// </summary>
        [Column("F_REGBACCOUNT")]
        public string F_RegBaccount { get; set; }
        /// <summary>
        /// 收票人姓名
        /// </summary>
        [Column("F_RECNAME")]
        public string F_RecName { get; set; }
        /// <summary>
        /// 收票人手机号
        /// </summary>
        [Column("F_RECMOBPHONE")]
        public string F_RecMobphone { get; set; }
        /// <summary>
        /// 收票人省份
        /// </summary>
        [Column("F_RECPROVINCE")]
        public string F_RecProvince { get; set; }
        /// <summary>
        /// 送票地址
        /// </summary>
        [Column("F_GOTOADDR")]
        public string F_GotoAddr { get; set; }
        /// <summary>
        /// 是否默认得
        /// </summary>
        [Column("F_ISDEFAULT")]
        public int? F_IsDefault { get; set; }
        /// <summary>
        /// 发票类型：电子发票 纸质发票
        /// </summary>
        [Column("F_INVTYPE")]
        public string F_InvType { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }


        [Column("F_IsOutInvoice")]
        public string F_IsOutInvoice { get; set; }


        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(UserInfo userInfo)
      {
            this.F_InvId = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue, UserInfo userInfo)
        {
            this.F_InvId = keyValue;
        }
        #endregion


        #region 扩展字段

        [NotMapped]
        public string F_BuyerName { get; set; }

        [NotMapped]
        public string F_OrderSN { get; set; }

        [NotMapped]
        public string F_OrderAmount { get; set; }
        [NotMapped]
        public string F_BuyerMobile { get; set; }
        #endregion
    }
}

