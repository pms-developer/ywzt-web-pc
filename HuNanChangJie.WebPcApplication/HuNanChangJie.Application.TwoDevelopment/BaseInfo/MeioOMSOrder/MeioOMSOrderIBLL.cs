﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// OMS发货单
    /// </summary>
    public interface MeioOMSOrderIBLL
    {
        MeioOMSOrderEntity GetMeioOMSOrderByOrderCode(string orderCode);
        void SaveEntity(string keyValue, MeioOMSOrderEntity entity, string type);
    }
}
