﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// OMS发货单
    /// </summary>
    public class MeioOMSOrderBLL: MeioOMSOrderIBLL
    {
        private MeioOMSOrderService meioOMSOrderService = new MeioOMSOrderService();
        public MeioOMSOrderEntity GetMeioOMSOrderByOrderCode(string orderCode)
        {
            try
            {
                return meioOMSOrderService.GetMeioOMSOrderByOrderCode(orderCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void SaveEntity(string keyValue, MeioOMSOrderEntity entity, string type)
        {
            try
            {
                meioOMSOrderService.SaveEntity(keyValue, entity, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        
    }
}
