﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS发货单
    /// </summary>
    public class MeioErpWmsXiaoBaoDeliveryBLL : MeioErpWmsXiaoBaoDeliveryIBLL
    {
        private MeioErpWmsXiaoBaoDeliveryService meioErpWmsXiaoBaoDeliveryService = new MeioErpWmsXiaoBaoDeliveryService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsXiaoBaoDeliveryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpWmsXiaoBaoDeliveryService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsXiaoBaoDelivery表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsXiaoBaoDeliveryEntity GetMeioErpWmsXiaoBaoDeliveryEntity(string keyValue)
        {
            try
            {
                return meioErpWmsXiaoBaoDeliveryService.GetMeioErpWmsXiaoBaoDeliveryEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 根据发货单号获取MeioErpWmsXiaoBaoDelivery表实体数据
        /// </summary>
        public MeioErpWmsXiaoBaoDeliveryEntity GetMeioErpWmsXiaoBaoDeliveryEntityByCode(string orderCode)
        {
            try
            {
                return meioErpWmsXiaoBaoDeliveryService.GetMeioErpWmsXiaoBaoDeliveryEntityByCode(orderCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpWmsXiaoBaoDeliveryService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsXiaoBaoDeliveryEntity entity, string deleteList, string type)
        {
            try
            {
                meioErpWmsXiaoBaoDeliveryService.SaveEntity(keyValue, entity, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
    }
}
