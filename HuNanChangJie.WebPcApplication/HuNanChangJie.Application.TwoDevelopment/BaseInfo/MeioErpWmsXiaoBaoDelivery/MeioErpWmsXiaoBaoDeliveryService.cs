﻿using Dapper;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS发货单
    /// </summary>
    public class MeioErpWmsXiaoBaoDeliveryService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private static readonly object objLock = new object();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsXiaoBaoDeliveryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID
                ,t.WarehouseID
                ,t.OwnerID
                ,t.OrderCode
                ,t.CarrierID
                ,t.WaybillNo
                ,t.DeliveryTime
                ,DeliveryType
                ");
                strSql.Append("  FROM MeioErpWmsXiaoBaoDelivery t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpWmsXiaoBaoDeliveryEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取MeioErpWmsXiaoBaoDelivery表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsXiaoBaoDeliveryEntity GetMeioErpWmsXiaoBaoDeliveryEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsXiaoBaoDeliveryEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据发货单号获取MeioErpWmsXiaoBaoDelivery表实体数据
        /// </summary>
        public MeioErpWmsXiaoBaoDeliveryEntity GetMeioErpWmsXiaoBaoDeliveryEntityByCode(string orderCode)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpWmsXiaoBaoDeliveryEntity>(p=>p.OrderCode==orderCode).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpWmsXiaoBaoDeliveryEntity>(t=>t.ID==keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsXiaoBaoDeliveryEntity entity, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    db.Update(entity);
                }
                else
                {
                    #region 按单生成快递账单
                    var ownerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(entity.OwnerID);
                    if (ownerEntity != null)
                    {
                        var nowDate = DateTime.Now.Date;
                        var contractEntity = this.BaseRepository().FindEntity<MeioErpContractEntity>(p => p.ID == ownerEntity.ContractID && p.ServiceStartDate <= nowDate && p.ServiceEndDate >= nowDate && p.LogisticsPackageAccountBillingType == 2);
                        if (contractEntity != null)
                        {
                            var contractDetailEntity = this.BaseRepository().FindList<MeioErpContractDetailEntity>(p => p.ContractID == ownerEntity.ContractID && p.ItemType == 2 && p.ItemDetailMinValue <= 1 && p.ItemDetailMaxValue >= 1).FirstOrDefault();

                            decimal unitPrice = 0;
                            if (contractDetailEntity != null)
                            {
                                if (contractDetailEntity.ContractPrice.HasValue)
                                {
                                    if (contractDetailEntity.ContractPrice.Value > 0)
                                    {
                                        unitPrice = contractDetailEntity.ContractPrice.Value;
                                    }
                                    else
                                    {
                                        unitPrice = contractDetailEntity.Price.Value;
                                    }
                                }
                                else
                                {
                                    unitPrice = contractDetailEntity.Price.Value;
                                }
                            }

                            MeioErpLogisticsPackageAccountEntity meioErpLogisticsPackageAccountEntity = new MeioErpLogisticsPackageAccountEntity
                            {
                                OwnerID = entity.OwnerID,
                                AccountDate = DateTime.Now,
                                State = 0,
                                OrderCount = 1,
                                TotalAmount = unitPrice,
                                IsPushOMS = false
                            };
                            meioErpLogisticsPackageAccountEntity.IsJobRun = true;
                            meioErpLogisticsPackageAccountEntity.Create();
                            db.Insert(meioErpLogisticsPackageAccountEntity);

                            MeioErpLogisticsPackageAccountDetailEntity meioErpLogisticsPackageAccountDetailEntity = new MeioErpLogisticsPackageAccountDetailEntity
                            {
                                LogisticsPackageAccountID = meioErpLogisticsPackageAccountEntity.ID,
                                OrderNo = entity.OrderCode,
                                DeliveryType = entity.DeliveryType,
                                WarehouseID = entity.WarehouseID,
                                Carrier = entity.CarrierID,
                                WaybillNo = entity.WaybillNo,
                                DeliveryTime = entity.DeliveryTime,
                                IsExpressFee=entity.IsExpressFee,
                            };
                            meioErpLogisticsPackageAccountDetailEntity.IsJobRun = true;
                            meioErpLogisticsPackageAccountDetailEntity.Create();
                            db.Insert(meioErpLogisticsPackageAccountDetailEntity);

                            entity.LogisticsPackageAccountID = meioErpLogisticsPackageAccountEntity.ID;

                        }
                    }
                    #endregion

                    entity.Create();
                    db.Insert(entity);
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }

        }

        #endregion
    }
}
