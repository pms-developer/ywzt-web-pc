﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS发货单
    /// </summary>
    public interface MeioErpWmsXiaoBaoDeliveryIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpWmsXiaoBaoDeliveryEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取MeioErpXiaoBaoDeliveryAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsXiaoBaoDeliveryEntity GetMeioErpWmsXiaoBaoDeliveryEntity(string keyValue);
        MeioErpWmsXiaoBaoDeliveryEntity GetMeioErpWmsXiaoBaoDeliveryEntityByCode(string orderCode);
        
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpWmsXiaoBaoDeliveryEntity entity, string deleteList, string type);
        #endregion
    }
}
