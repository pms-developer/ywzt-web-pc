﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-23 10:57
    /// 描 述：结算单信息
    /// </summary>
    public class MeioErpStatementService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpStatementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.StatementAmount,
                t.StatementMethod,
                t.PayMethod,
                t.Currency,
                t.OwnerID,
                t1.Code OwnerCode,
                t1.Name OwnerName,
                t.IsInvoice,
                t.IsCollectionConfirm,
                t.CollectionImageUrl,
                t.CreationDate,
                t.CollectionAccount,
                t.CollectionConfirmPeople,
                t.CollectionConfirmDate,
                t.AuditNumber
                ");
                strSql.Append("  FROM MeioErpStatement t ");
                strSql.Append(" LEFT JOIN MeioErpOwner t1 ON t.OwnerID=t1.ID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", queryParam["Code"].ToString(), DbType.String);
                    strSql.Append(" AND t.Code = @Code ");
                }
                if (!queryParam["IsInvoice"].IsEmpty())
                {
                    dp.Add("IsInvoice", queryParam["IsInvoice"].ToString(), DbType.String);
                    strSql.Append(" AND t.IsInvoice = @IsInvoice ");
                }
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID", queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpStatementEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpStatementDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpStatementDetailEntity> GetMeioErpStatementDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpStatementDetailEntity>(t => t.StatementID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpStatement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpStatementEntity GetMeioErpStatementEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpStatementEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public MeioErpStatementEntity GetMeioErpStatementEntityByStatementNoAndOwnerID(string statementNo, string ownerID)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpStatementEntity>(p => p.Code == statementNo && p.OwnerID == ownerID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpStatementDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpStatementDetailEntity GetMeioErpStatementDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpStatementDetailEntity>(t => t.StatementID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpStatementEntity = GetMeioErpStatementEntity(keyValue);
                db.Delete<MeioErpStatementEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpStatementDetailEntity>(t => t.StatementID == meioErpStatementEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpStatementEntity entity, List<MeioErpStatementDetailEntity> meioErpStatementDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioErpStatementEntityTmp = GetMeioErpStatementEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    db.Delete<MeioErpStatementDetailEntity>(p => p.StatementID == keyValue);
                    foreach (MeioErpStatementDetailEntity item in meioErpStatementDetailList)
                    {
                        item.StatementID = keyValue;
                        item.Create();
                        db.Insert(item);
                    }
                }
                else
                {
                    entity.IsCollectionConfirm = false;
                    entity.Create();
                    db.Insert(entity);
                    foreach (MeioErpStatementDetailEntity item in meioErpStatementDetailList)
                    {
                        item.StatementID = entity.ID;
                        item.Create();
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        public MeioErpStatementEntity GetMeioErpStatementEntityByCode(string code)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpStatementEntity>(p => p.Code == code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void ConfirmCollection(string keyValue, string auditNumber, string collectionImageUrl)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var statementEntity = GetMeioErpStatementEntity(keyValue);
                if (statementEntity != null)
                {
                    statementEntity.AuditNumber = auditNumber;
                    statementEntity.CollectionImageUrl = collectionImageUrl;
                    statementEntity.IsCollectionConfirm = true;
                    statementEntity.CollectionConfirmDate = DateTime.Now;
                    statementEntity.Modify(keyValue);
                    statementEntity.CollectionConfirmPeople = statementEntity.ModificationName;
                    db.Update(statementEntity);

                    //修改结算单对应的账单信息
                    var statementDetailList = GetMeioErpStatementDetailList(keyValue);
                    foreach (var statementDetail in statementDetailList)
                    {
                        if (statementDetail.OrderType == "仓租账单")
                        {
                            var warehouseRentAccountList = this.BaseRepository().FindList<MeioErpWarehouseRentAccountEntity>(t => t.AssociateNo == statementDetail.AssociateNo);
                            if (warehouseRentAccountList != null)
                            {
                                if (warehouseRentAccountList.Count() > 0)
                                {
                                    var warehouseRentAccountEntity = warehouseRentAccountList.FirstOrDefault();
                                    warehouseRentAccountEntity.StatementNo = statementEntity.Code;
                                    warehouseRentAccountEntity.State = 1;
                                    warehouseRentAccountEntity.Modify(warehouseRentAccountEntity.ID);
                                    db.Update(warehouseRentAccountEntity);
                                }
                            }
                        }
                        else if (statementDetail.OrderType == "入库账单")
                        {
                            var inStorageAccountList = this.BaseRepository().FindList<MeioErpInStorageAccountEntity>(t => t.InStorageNo == statementDetail.AssociateNo);
                            if (inStorageAccountList != null)
                            {
                                if (inStorageAccountList.Count() > 0)
                                {
                                    var inStorageAccountEntity = inStorageAccountList.FirstOrDefault();
                                    inStorageAccountEntity.StatementNo = statementEntity.Code;
                                    inStorageAccountEntity.State = 1;
                                    inStorageAccountEntity.Modify(inStorageAccountEntity.ID);
                                    db.Update(inStorageAccountEntity);
                                }
                            }
                        }
                        else if (statementDetail.OrderType == "出库账单")
                        {
                            var deliveryAccountList = this.BaseRepository().FindList<MeioErpDeliveryAccountEntity>(t => t.DeliveryNo == statementDetail.AssociateNo);
                            if (deliveryAccountList != null)
                            {
                                if (deliveryAccountList.Count() > 0)
                                {
                                    var deliveryAccountEntity = deliveryAccountList.FirstOrDefault();
                                    deliveryAccountEntity.StatementNo = statementEntity.Code;
                                    deliveryAccountEntity.State = 1;
                                    deliveryAccountEntity.Modify(deliveryAccountEntity.ID);
                                    db.Update(deliveryAccountEntity);
                                }
                            }
                        }
                        else if (statementDetail.OrderType == "盘点账单")
                        {
                            var checkInventoryAccountList = this.BaseRepository().FindList<MeioErpCheckInventoryAccountEntity>(t => t.CheckInventoryCode == statementDetail.AssociateNo);
                            if (checkInventoryAccountList != null)
                            {
                                if (checkInventoryAccountList.Count() > 0)
                                {
                                    var checkInventoryAccountEntity=checkInventoryAccountList.FirstOrDefault();
                                    checkInventoryAccountEntity.StatementNo = statementEntity.Code;
                                    checkInventoryAccountEntity.State = 1;
                                    checkInventoryAccountEntity.Modify(checkInventoryAccountEntity.ID);
                                    db.Update(checkInventoryAccountEntity);
                                }
                            }
                        }
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
