﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-23 10:57
    /// 描 述：结算单信息
    /// </summary>
    public class MeioErpStatementBLL : MeioErpStatementIBLL
    {
        private MeioErpStatementService meioErpStatementService = new MeioErpStatementService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpStatementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpStatementService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpStatementDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpStatementDetailEntity> GetMeioErpStatementDetailList(string keyValue)
        {
            try
            {
                return meioErpStatementService.GetMeioErpStatementDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpStatement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpStatementEntity GetMeioErpStatementEntity(string keyValue)
        {
            try
            {
                return meioErpStatementService.GetMeioErpStatementEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public MeioErpStatementEntity GetMeioErpStatementEntityByStatementNoAndOwnerID(string statementNo, string ownerID)
        {
            try
            {
                return meioErpStatementService.GetMeioErpStatementEntityByStatementNoAndOwnerID(statementNo,ownerID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取MeioErpStatementDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpStatementDetailEntity GetMeioErpStatementDetailEntity(string keyValue)
        {
            try
            {
                return meioErpStatementService.GetMeioErpStatementDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpStatementService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpStatementEntity entity,List<MeioErpStatementDetailEntity> meioErpStatementDetailList,string deleteList,string type)
        {
            try
            {
                meioErpStatementService.SaveEntity(keyValue, entity,meioErpStatementDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public MeioErpStatementEntity GetMeioErpStatementEntityByCode(string code)
        {
            try
            {
                return meioErpStatementService.GetMeioErpStatementEntityByCode(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 确认收款
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="auditNumber"></param>
        /// <param name="collectionImageUrl"></param>
        public void ConfirmCollection(string keyValue, string auditNumber, string collectionImageUrl)
        {
            try
            {
               meioErpStatementService.ConfirmCollection(keyValue, auditNumber, collectionImageUrl);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
