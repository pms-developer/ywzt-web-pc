﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-23 10:57
    /// 描 述：结算单信息
    /// </summary>
    public interface MeioErpStatementIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpStatementEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpStatementDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpStatementDetailEntity> GetMeioErpStatementDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpStatement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpStatementEntity GetMeioErpStatementEntity(string keyValue);
        MeioErpStatementEntity GetMeioErpStatementEntityByStatementNoAndOwnerID(string statementNo, string ownerID);
        /// <summary>
        /// 获取MeioErpStatementDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpStatementDetailEntity GetMeioErpStatementDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpStatementEntity entity,List<MeioErpStatementDetailEntity> meioErpStatementDetailList,string deleteList,string type);
        #endregion
        MeioErpStatementEntity GetMeioErpStatementEntityByCode(string code);
        void ConfirmCollection(string keyValue, string auditNumber, string collectionImageUrl);
    }
}
