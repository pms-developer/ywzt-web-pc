﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-23 10:57
    /// 描 述：结算单信息
    /// </summary>
    public class MeioErpStatementEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 结算单号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 结算方式
        /// </summary>
        [Column("STATEMENTMETHOD")]
        public string StatementMethod { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        [Column("PAYMETHOD")]
        public string PayMethod { get; set; }
        /// <summary>
        /// 结算金额
        /// </summary>
        [Column("STATEMENTAMOUNT")]
        public decimal? StatementAmount { get; set; }
        /// <summary>
        /// 币种
        /// </summary>
        [Column("CURRENCY")]
        public string Currency { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 货主ID
        /// </summary>
        [Column("OWNERID")]
        public string OwnerID { get; set; }
        /// <summary>
        /// 是否开票
        /// </summary>
        [Column("ISINVOICE")]
        public bool? IsInvoice { get; set; }
        /// <summary>
        /// 查账编号
        /// </summary>
        [Column("AUDITNUMBER")]
        public string AuditNumber { get; set; }
        /// <summary>
        /// 收款确认
        /// </summary>
        [Column("ISCOLLECTIONCONFIRM")]
        public bool? IsCollectionConfirm { get; set; }
        /// <summary>
        /// 收款账户
        /// </summary>
        [Column("COLLECTIONACCOUNT")]
        public string CollectionAccount { get; set; }
        /// <summary>
        /// 收款截图
        /// </summary>
        [Column("COLLECTIONIMAGEURL")]
        public string CollectionImageUrl { get; set; }
        /// <summary>
        /// 收款确认人
        /// </summary>
        [Column("COLLECTIONCONFIRMPEOPLE")]
        public string CollectionConfirmPeople { get; set; }
        /// <summary>
        /// 收款确认时间
        /// </summary>
        [Column("COLLECTIONCONFIRMDATE")]
        public DateTime? CollectionConfirmDate { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            if (userInfo != null)
            {
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
                this.Company_ID = userInfo.companyId;
                this.CompanyCode = userInfo.CompanyCode;
                this.CompanyName = userInfo.CompanyName;
                this.CompanyFullName = userInfo.CompanyFullName;
            }
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            if (userInfo != null)
            {
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
                this.Company_ID = userInfo.companyId;
                this.CompanyCode = userInfo.CompanyCode;
                this.CompanyName = userInfo.CompanyName;
                this.CompanyFullName = userInfo.CompanyFullName;
            }
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            if (userInfo != null)
            {
                this.Modification_Id = userInfo.userId;
                this.ModificationName = userInfo.realName;
            }
        }
        #endregion
        #region  扩展字段
        /// <summary>
        /// 货主编码
        /// </summary>
        [NotMapped]
        public string OwnerCode { get; set; }
        /// <summary>
        /// 货主名称
        /// </summary>
        [NotMapped]
        public string OwnerName { get; set; }
        #endregion
    }
}

