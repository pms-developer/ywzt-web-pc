﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-18 11:47
    /// 描 述：云途账单维护
    /// </summary>
    public interface MeioErpYunTuBillingMaintenanceIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpYunTuBillingMaintenanceEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpYunTuBillingMaintenance表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpYunTuBillingMaintenanceEntity GetMeioErpYunTuBillingMaintenanceEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpYunTuBillingMaintenanceEntity entity,string deleteList,string type);
        #endregion
        /// <summary>
        /// 手动同步
        /// </summary>
        /// <param name="keyValue"></param>
        int Sync(string keyValue);
        /// <summary>
        /// 重算
        /// </summary>
        /// <param name="keyValue"></param>
        void Compute(string keyValue);
        /// <summary>
        /// 获取运单计算错误信息
        /// </summary>
        /// <param name="WaybillNumber"></param>
        IEnumerable<MeioErpYunTuBillingMaintenanceErrorEntity> GetBillingMaintenanceErrorList(string BillingMaintenanceID, string WaybillNumber);
        /// <summary>
        /// 校验账号不重复
        /// </summary>
        /// <param name="code"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        bool CheckIsYunTuBillingMaintenanceCode(string code, string id);
        /// <summary>
        /// 查询重复账号
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        string CheckIsYunTuBillingMaintenanceCode(string code);
    }
}
