﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
using System.Threading;
using System.Threading.Tasks;
using System.CodeDom;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-18 11:47
    /// 描 述：云途账单维护
    /// </summary>
    public class MeioErpYunTuBillingMaintenanceService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private MeioErpYunTuBillBLL yunTuBillBLL = new MeioErpYunTuBillBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpYunTuBillingMaintenanceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Type,
                t.BillingCycleStartTime,
                t.BillingCycleEndTime,
                t.CreationName,
                t.CreationDate,
                t.SyncTime,
                t.SyncState,
                t.YunTuSyncBillCount,
                t.CalculateSuccessCount  
                ");
                strSql.Append("  FROM MeioErpYunTuBillingMaintenance t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["SyncState"].IsEmpty())
                {
                    dp.Add("SyncState", queryParam["SyncState"].ToString(), DbType.String);
                    strSql.Append(" AND t.SyncState = @SyncState ");
                }
                var list = this.BaseRepository().FindList<MeioErpYunTuBillingMaintenanceEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpYunTuBillingMaintenance表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpYunTuBillingMaintenanceEntity GetMeioErpYunTuBillingMaintenanceEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpYunTuBillingMaintenanceEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                db.Delete<MeioErpYunTuBillingMaintenanceEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpYunTuBillingMaintenanceErrorEntity>(t => t.BillingMaintenanceID == keyValue);
                db.Delete<MeioErpYunTuBillDetailExpenditureRecordsEntity>(t => t.BillingMaintenanceID == keyValue);
                db.Delete<MeioErpYunTuBillDetailVatRecordsEntity>(t => t.BillingMaintenanceID == keyValue);
                db.Delete<MeioErpYunTuBillDetailReturnRecordsEntity>(t => t.BillingMaintenanceID == keyValue);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpYunTuBillingMaintenanceEntity entity, string deleteList, string type)
        {

            try
            {
                List<string> WaybillNumbers = new List<string>();
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    
                    this.BaseRepository().Update(entity);
                    if (entity.BillingCycleStartTime == null)
                        this.BaseRepository().ExecuteBySql("UPDATE MeioErpYunTuBillingMaintenance SET BillingCycleStartTime=null WHERE ID='" + keyValue + "'");
                    if (entity.BillingCycleEndTime == null)
                        this.BaseRepository().ExecuteBySql("UPDATE MeioErpYunTuBillingMaintenance SET BillingCycleEndTime=null WHERE ID='" + keyValue + "'");
                }
                else
                {
                    var codeList = entity.Code.Split('\n').ToList();
                    var strcodes = entity.Code.Replace("\n", "','");
                    string sql = string.Format("SELECT Code FROM MeioErpYunTuBillingMaintenance WHERE Code in ('{0}')", strcodes);
                    var codeListQuery = this.BaseRepository().FindList<MeioErpYunTuBillingMaintenanceEntity>(sql);

                    if (codeListQuery.Count() > 0)
                    {
                        foreach (var item in codeListQuery)
                        {
                            if (codeList.Any(p => p == item.Code))
                            {
                                codeList.Remove(item.Code);
                            }
                        }
                    }

                    foreach (var code in codeList)
                    {
                        WaybillNumbers.Clear();
                        var db = this.BaseRepository().BeginTrans();
                        try
                        {
                            entity.Code = code;

                            entity.Create();
                            //this.BaseRepository().Insert(entity);
                            entity.SyncTime = DateTime.Now;

                            //调用获取云途账单明细接口
                            var CarrierList = this.BaseRepository().FindList<MeioERPGeneralCarrierEntity>();
                            var carrierEntity = CarrierList.FirstOrDefault(p => p.Code == "YTWL");
                            if (carrierEntity != null)
                            {
                                if (!string.IsNullOrEmpty(carrierEntity.InterfaceServiceAddress) && !string.IsNullOrEmpty(carrierEntity.InterfaceUserInfo)
                                    && !string.IsNullOrEmpty(carrierEntity.InterfaceAccount) && !string.IsNullOrEmpty(carrierEntity.InterfaceKey))
                                {
                                    YunExpressService.strUrl = carrierEntity.InterfaceServiceAddress;
                                    YunExpressService.appId = carrierEntity.InterfaceAccount;
                                    YunExpressService.appSecret = carrierEntity.InterfaceKey;
                                    YunExpressService.sourceKey = carrierEntity.InterfaceUserInfo;

                                    int page = 1;//100000
                                    int count = 0;
                                    for (int i = 0; i < 1000; i++)
                                    {
                                        var billDetailsExpenditure = YunExpressService.getBillDetails(entity.Code, "N", page, 100);
                                        Thread.Sleep(2000);//延迟2秒
                                        var billDetailsVat = YunExpressService.getBillDetails(entity.Code, "V", page, 100);
                                        Thread.Sleep(2000);//延迟2秒
                                        var billDetailsReturn = YunExpressService.getBillDetails(entity.Code, "R", page, 100);
                                        count = 0;
                                        if (billDetailsExpenditure.success.Value && billDetailsExpenditure.result.expenditure_records != null)
                                        {
                                            db.Delete<MeioErpYunTuBillDetailExpenditureRecordsEntity>(p => p.BillingMaintenanceID == entity.ID);
                                            foreach (var records in billDetailsExpenditure.result.expenditure_records)
                                            {
                                                WaybillNumbers.Add(records.waybill_number);
                                                var entityRecords = new MeioErpYunTuBillDetailExpenditureRecordsEntity
                                                {
                                                    WaybillNumber = records.waybill_number,
                                                    CustomerOrderNumber = records.customer_order_number,
                                                    CustomerCode = records.customer_code,
                                                    DestinationCountry = records.destination_country,
                                                    Partition = records.partition,
                                                    NetWeight = records.net_weight,
                                                    ChargeableWeight = records.chargeable_weight,
                                                    HandlingFee = records.handling_fee,
                                                    BillingMaintenanceID = entity.ID,
                                                    SalesProducts = records.sales_products,
                                                };
                                                entityRecords.Create();
                                                db.Insert(entityRecords);
                                                count++;
                                            }

                                        }

                                        if (billDetailsVat.success.Value && billDetailsVat.result.vat_records != null)
                                        {
                                            db.Delete<MeioErpYunTuBillDetailVatRecordsEntity>(p => p.BillingMaintenanceID == entity.ID);
                                            foreach (var records in billDetailsVat.result.vat_records)
                                            {
                                                var entityRecords = new MeioErpYunTuBillDetailVatRecordsEntity
                                                {
                                                    WaybillNumber = records.waybill_number,
                                                    CustomerOrderNumber = records.customer_order_number,
                                                    CustomerCode = records.customer_code,
                                                    DestinationCountry = records.destination_country,
                                                    Partition = records.partition,
                                                    TotalFee = records.total_fee,
                                                    Remark = records.remark,
                                                    BillingMaintenanceID = entity.ID,
                                                    SalesProducts = records.sales_products,
                                                };
                                                entityRecords.Create();
                                                db.Insert(entityRecords);
                                            }
                                        }

                                        if (billDetailsReturn.success.Value && billDetailsReturn.result.vat_records != null)
                                        {
                                            db.Delete<MeioErpYunTuBillDetailReturnRecordsEntity>(p => p.BillingMaintenanceID == entity.ID);
                                            foreach (var records in billDetailsReturn.result.return_record)
                                            {
                                                var entityRecords = new MeioErpYunTuBillDetailReturnRecordsEntity
                                                {
                                                    WaybillNumber = records.waybill_number,
                                                    CustomerOrderNumber = records.customer_order_number,
                                                    CustomerCode = records.customer_code,
                                                    DestinationCountry = records.destination_country,
                                                    ReturnType = records.return_type,
                                                    BillingMaintenanceID = entity.ID,
                                                    SalesProducts = records.sales_products,
                                                };
                                                entityRecords.Create();
                                                db.Insert(entityRecords);
                                            }
                                        }

                                        if (count > 0)
                                        {
                                            entity.SyncState = 1;
                                            entity.SyncTime = DateTime.Now;
                                            entity.YunTuSyncBillCount = count;
                                            entity.CalculateSuccessCount = 0;
                                            page++;
                                        }
                                        else
                                        {
                                            if (page == 1)
                                            {
                                                entity.SyncState = 2;
                                                entity.YunTuSyncBillCount = count;
                                                entity.CalculateSuccessCount = 0;
                                            }
                                            break;
                                        }
                                    }
                                }

                            }

                            db.Insert(entity);

                            db.Commit();

                            if (entity.SyncState == 1)
                            {
                                CalculateBill(WaybillNumbers, entity.ID);
                                //Task.Run(() =>
                                //{
                                    yunTuBillBLL.UpdateWaybillState();
                                //});
                            }
                        }
                        catch (Exception ex)
                        {
                            db.Rollback();
                            if (ex is ExceptionEx)
                            {
                                throw;
                            }
                            else
                            {
                                throw ExceptionEx.ThrowServiceException(ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        /// <summary>
        /// 手动同步
        /// </summary>
        /// <param name="keyValue"></param>
        public int Sync(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                List<string> WaybillNumbers = new List<string>();
                var entity = this.GetMeioErpYunTuBillingMaintenanceEntity(keyValue);
                //调用获取云途账单明细接口
                var CarrierList = this.BaseRepository().FindList<MeioERPGeneralCarrierEntity>();
                var carrierEntity = CarrierList.FirstOrDefault(p => p.Code == "YTWL");
                if (carrierEntity != null)
                {
                    if (!string.IsNullOrEmpty(carrierEntity.InterfaceServiceAddress) && !string.IsNullOrEmpty(carrierEntity.InterfaceUserInfo)
                        && !string.IsNullOrEmpty(carrierEntity.InterfaceAccount) && !string.IsNullOrEmpty(carrierEntity.InterfaceKey))
                    {
                        YunExpressService.strUrl = carrierEntity.InterfaceServiceAddress;
                        YunExpressService.appId = carrierEntity.InterfaceAccount;
                        YunExpressService.appSecret = carrierEntity.InterfaceKey;
                        YunExpressService.sourceKey = carrierEntity.InterfaceUserInfo;

                        int page = 1;//100000
                        int count = 0;
                        for (int i = 0; i < 1000; i++)
                        {
                            var billDetailsExpenditure = YunExpressService.getBillDetails(entity.Code, "N", page, 100);
                            Thread.Sleep(2000);//延迟2秒
                            var billDetailsVat = YunExpressService.getBillDetails(entity.Code, "V", page, 100);
                            Thread.Sleep(2000);//延迟2秒
                            var billDetailsReturn = YunExpressService.getBillDetails(entity.Code, "R", page, 100);
                            count = 0;
                            if (billDetailsExpenditure.success.Value && billDetailsExpenditure.result.expenditure_records != null)
                            {
                                db.Delete<MeioErpYunTuBillDetailExpenditureRecordsEntity>(p => p.BillingMaintenanceID == entity.ID);
                                foreach (var records in billDetailsExpenditure.result.expenditure_records)
                                {
                                    WaybillNumbers.Add(records.waybill_number);
                                    var entityRecords = new MeioErpYunTuBillDetailExpenditureRecordsEntity
                                    {
                                        WaybillNumber = records.waybill_number,
                                        CustomerOrderNumber = records.customer_order_number,
                                        CustomerCode = records.customer_code,
                                        DestinationCountry = records.destination_country,
                                        Partition = records.partition,
                                        NetWeight = records.net_weight,
                                        ChargeableWeight = records.chargeable_weight,
                                        HandlingFee = records.handling_fee,
                                        BillingMaintenanceID = entity.ID,
                                        SalesProducts = records.sales_products,
                                    };
                                    entityRecords.Create();
                                    db.Insert(entityRecords);
                                    count++;
                                }
                            }

                            if (billDetailsVat.success.Value && billDetailsVat.result.vat_records != null)
                            {
                                db.Delete<MeioErpYunTuBillDetailVatRecordsEntity>(p => p.BillingMaintenanceID == entity.ID);
                                foreach (var records in billDetailsVat.result.vat_records)
                                {
                                    var entityRecords = new MeioErpYunTuBillDetailVatRecordsEntity
                                    {
                                        WaybillNumber = records.waybill_number,
                                        CustomerOrderNumber = records.customer_order_number,
                                        CustomerCode = records.customer_code,
                                        DestinationCountry = records.destination_country,
                                        Partition = records.partition,
                                        TotalFee = records.total_fee,
                                        Remark = records.remark,
                                        BillingMaintenanceID = entity.ID,
                                        SalesProducts = records.sales_products,
                                    };
                                    entityRecords.Create();
                                    db.Insert(entityRecords);
                                }
                            }

                            if (billDetailsReturn.success.Value && billDetailsReturn.result.return_record != null)
                            {
                                db.Delete<MeioErpYunTuBillDetailReturnRecordsEntity>(p => p.BillingMaintenanceID == entity.ID);
                                foreach (var records in billDetailsReturn.result.return_record)
                                {
                                    var entityRecords = new MeioErpYunTuBillDetailReturnRecordsEntity
                                    {
                                        WaybillNumber = records.waybill_number,
                                        CustomerOrderNumber = records.customer_order_number,
                                        CustomerCode = records.customer_code,
                                        DestinationCountry = records.destination_country,
                                        ReturnType = records.return_type,
                                        BillingMaintenanceID = entity.ID,
                                        SalesProducts = records.sales_products,
                                    };
                                    entityRecords.Create();
                                    db.Insert(entityRecords);
                                }
                            }

                            if (count > 0)
                            {
                                entity.SyncState = 1;
                                entity.SyncTime = DateTime.Now;
                                entity.YunTuSyncBillCount = count;
                                page++;
                            }
                            else
                            {
                                if (page == 1)
                                {
                                    entity.SyncState = 2;
                                    entity.YunTuSyncBillCount = count;
                                }
                                break;
                            }
                        }
                    }

                }

                entity.SyncTime = DateTime.Now;
                entity.Modify(keyValue);
                db.Update(entity);

                db.Commit();


                if (entity.SyncState == 1)
                {
                    CalculateBill(WaybillNumbers, keyValue);
                    //Task.Run(() =>
                    //{
                        yunTuBillBLL.UpdateWaybillState();
                    //});
                }

                return entity.SyncState.GetValueOrDefault();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void CalculateBill(List<string> WaybillNumbers, string BillingMaintenanceID)
        {
            var db = this.BaseRepository().BeginTrans();
            int successCount = 0;
            int failCount = 0;
            try
            {
                //计算账单费用
                string sql1 = @"select t.*,t1.Name AS LogisticsChannelName from [dbo].[MeioErpFreightQuotation] t
LEFT JOIN [dbo].[MeioErpLogisticsChannel] t1 ON t.LogisticsChannel=t1.ID
where t.Enabled=1 and t.EffectiveStartTime<=GETDATE() and t.EffectiveEndTime>=GETDATE() ";
                var freightQuotationList = this.BaseRepository().FindList<MeioErpFreightQuotationEntity>(sql1);

                string sql = @"select t.WaybillNumber,t.NetWeight,t.ChargeableWeight,t.DestinationCountry,t.Partition,t.SalesProducts,t.BillingMaintenanceID,t.HandlingFee,
t1.TotalFee,t1.Remark,t2.ReturnType  
from MeioErpYunTuBillDetailExpenditureRecords t
LEFT JOIN MeioErpYunTuBillDetailVatRecords t1 ON t.WaybillNumber=t1.WaybillNumber
LEFT JOIN MeioErpYunTuBillDetailReturnRecords t2 ON t.WaybillNumber=t2.WaybillNumber 
WHERE t.WaybillNumber IN @WaybillNumbers ";
                var dp = new DynamicParameters(new { });
                dp.Add("WaybillNumbers", WaybillNumbers);
                var orderlist = this.BaseRepository().FindList<MeioErpYunTuBillDetailExpenditureRecordsEntity>(sql, dp);

                string sql2 = "select t.* from MeioOMSOrder t WHERE t.WaybillNumber IN @WaybillNumbers";
                var oms_orderlist = this.BaseRepository().FindList<MeioOMSOrderEntity>(sql2, dp);

                var billList = this.BaseRepository().FindList<MeioErpYunTuBillEntity>(p => WaybillNumbers.Contains(p.WaybillNumber));
                successCount = billList.Count(p => p.BillingMaintenanceID == BillingMaintenanceID && p.AuditStatus == "1");
                foreach (var order in orderlist)
                {
                    if (!billList.Any(p => p.WaybillNumber == order.WaybillNumber && p.AuditStatus == "1"))
                    {
                        db.Delete<MeioErpYunTuBillEntity>(p => p.WaybillNumber == order.WaybillNumber);
                        db.Delete<MeioErpYunTuBillingMaintenanceErrorEntity>(p => p.WaybillNumber == order.WaybillNumber);
                        var oms_orderEntity = oms_orderlist.FirstOrDefault(p => p.WaybillNumber == order.WaybillNumber);
                        if (oms_orderEntity != null)
                        {
                            var ordercountry = order.DestinationCountry + (string.IsNullOrEmpty(order.Partition) ? "" : "-" + order.Partition);
                            var freightQuotationEntity = freightQuotationList.Where(p => p.EffectiveStartTime <= oms_orderEntity.DeliveryTime && p.EffectiveEndTime >= oms_orderEntity.DeliveryTime && p.LogisticsChannelName == order.SalesProducts && p.Country.Contains(ordercountry)).OrderByDescending(o => o.ModificationDate).FirstOrDefault();
                            if (freightQuotationEntity != null)
                            {
                                //计算运费
                                decimal Freight = 0;//运费
                                decimal RegistrationFee = 0;//挂号费
                                var freightQuotationDetailList = this.BaseRepository().FindList<MeioErpFreightQuotationDetailEntity>(p => p.FreightQuotationID == freightQuotationEntity.ID);
                                if (decimal.Parse(order.ChargeableWeight) < freightQuotationDetailList.First().MinWeight)
                                    order.ChargeableWeight = freightQuotationDetailList.First().MinWeight.ToString();
                                var freightQuotationDetailListEntity = freightQuotationDetailList.Where(p => p.StartWeight < decimal.Parse(order.ChargeableWeight) && p.EndWeight >= decimal.Parse(order.ChargeableWeight) && p.Country == order.DestinationCountry && p.Area == (order.Partition == null ? "" : order.Partition)).FirstOrDefault();
                                if (freightQuotationDetailListEntity != null)
                                {
                                    if (freightQuotationEntity.BillingMode == "1")//重量段
                                    {
                                        Freight = freightQuotationDetailListEntity.Freight.GetValueOrDefault() * decimal.Parse(order.ChargeableWeight);
                                        RegistrationFee = freightQuotationDetailListEntity.RegistrationFee.GetValueOrDefault();
                                        Freight = Math.Round(Freight, 2);//保留两位小数四舍五入
                                        RegistrationFee = Math.Round(RegistrationFee, 2);//保留两位小数四舍五入
                                    }
                                    else if (freightQuotationEntity.BillingMode == "2")//首重续重
                                    {
                                        var scale = freightQuotationDetailListEntity.Scale.GetValueOrDefault();
                                        if (scale != 0)
                                        {
                                            var num = (decimal.Parse(order.ChargeableWeight) - freightQuotationDetailListEntity.MinWeight.GetValueOrDefault()) / freightQuotationDetailListEntity.Scale.GetValueOrDefault();
                                            int roundedUp = (int)Math.Round(num);//向上取整

                                            Freight = freightQuotationDetailListEntity.FirstWeight.GetValueOrDefault() + roundedUp * freightQuotationDetailListEntity.ContinueWeight.GetValueOrDefault();
                                            Freight = Math.Round(Freight, 2);//保留两位小数四舍五入
                                        }
                                    }

                                    MeioErpYunTuBillEntity billEntity = new MeioErpYunTuBillEntity
                                    {
                                        WaybillNumber = order.WaybillNumber,
                                        WaybillState = oms_orderEntity.WaybillState,
                                        OrderCode = oms_orderEntity.OrderCode,
                                        OwnerCode = oms_orderEntity.OwnerCode,
                                        WarehouseCode = oms_orderEntity.WarehouseCode,
                                        DeliveryTime = oms_orderEntity.DeliveryTime,
                                        NetWeight = order.NetWeight,
                                        ChargeableWeight = order.ChargeableWeight,
                                        DestinationCountry = order.DestinationCountry + (string.IsNullOrEmpty(order.Partition) ? "" : "-" + order.Partition),
                                        Partition = order.Partition,
                                        ProductName = oms_orderEntity.ProductName,
                                        Freight = Freight,
                                        RegistrationFee = RegistrationFee,
                                        HandlingFee = order.HandlingFee,
                                        TotalFee = (order.TotalFee == null ? 0 : order.TotalFee),
                                        ReturnFee = 0, //order.ReturnType == "退件退全款" ? (Freight + RegistrationFee + order.HandlingFee + (order.TotalFee == null ? 0 : order.TotalFee)) : 0,
                                        AdjustmentAmount = 0,
                                        TotalAmount = Freight + RegistrationFee + order.HandlingFee + (order.TotalFee == null ? 0 : order.TotalFee),
                                        AuditStatus = "0",
                                        Remark = order.Remark,
                                        BillingMaintenanceID = order.BillingMaintenanceID,
                                    };
                                    if (freightQuotationDetailListEntity != null)
                                    {
                                        billEntity.FreightQuotationID = freightQuotationDetailListEntity.FreightQuotationID;
                                    }

                                    billEntity.Create();
                                    db.Insert(billEntity);
                                    successCount++;
                                }
                                else
                                {
                                    var BillingMaintenanceErrorEntity = new MeioErpYunTuBillingMaintenanceErrorEntity
                                    {
                                        WaybillNumber = order.WaybillNumber,
                                        BillingMaintenanceID = BillingMaintenanceID,
                                        Error = "无匹配报价单的计费重量段"
                                    };
                                    BillingMaintenanceErrorEntity.Create();
                                    db.Insert(BillingMaintenanceErrorEntity);
                                    failCount++;
                                }
                            }
                            else
                            {
                                //无法匹配到规则的需要记录错误日志，并发送告警信息，给出运单号、云途账单号、失败原因
                                var BillingMaintenanceErrorEntity = new MeioErpYunTuBillingMaintenanceErrorEntity
                                {
                                    WaybillNumber = order.WaybillNumber,
                                    BillingMaintenanceID = BillingMaintenanceID,
                                    Error = "无匹配报价单"
                                };
                                BillingMaintenanceErrorEntity.Create();
                                db.Insert(BillingMaintenanceErrorEntity);
                                failCount++;
                            }
                        }
                        else
                        {
                            var BillingMaintenanceErrorEntity = new MeioErpYunTuBillingMaintenanceErrorEntity
                            {
                                WaybillNumber = order.WaybillNumber,
                                BillingMaintenanceID = BillingMaintenanceID,
                                Error = "关联不到OMS订单"
                            };
                            BillingMaintenanceErrorEntity.Create();
                            db.Insert(BillingMaintenanceErrorEntity);
                            failCount++;
                        }
                    }
                }

                var BillingMaintenanceEntity = this.BaseRepository().FindEntity<MeioErpYunTuBillingMaintenanceEntity>(p => p.ID == BillingMaintenanceID);
                if (BillingMaintenanceEntity != null)
                {
                    BillingMaintenanceEntity.CalculateSuccessCount = successCount;
                    BillingMaintenanceEntity.Modify(BillingMaintenanceID);
                    db.Update(BillingMaintenanceEntity);
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void Compute(string keyValue)
        {
            try
            {
                var WaybillNumbers = this.BaseRepository().FindList<MeioErpYunTuBillDetailExpenditureRecordsEntity>(p => p.BillingMaintenanceID == keyValue).Select(o => o.WaybillNumber).ToList();
                if (WaybillNumbers.Count() > 0)
                {
                    CalculateBill(WaybillNumbers, keyValue);
                    yunTuBillBLL.UpdateWaybillState();
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpYunTuBillingMaintenanceErrorEntity> GetBillingMaintenanceErrorList(string BillingMaintenanceID, string WaybillNumber)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append(@"SELECT t.WaybillNumber,t.Error,t.CreationDate FROM MeioErpYunTuBillingMaintenanceError t WHERE t.BillingMaintenanceID=@BillingMaintenanceID ");
                var dp = new DynamicParameters(new { });
                dp.Add("BillingMaintenanceID", BillingMaintenanceID, DbType.String);
                if (!string.IsNullOrEmpty(WaybillNumber))
                {
                    dp.Add("WaybillNumber", WaybillNumber, DbType.String);
                    strSql.Append(" AND t.WaybillNumber=@WaybillNumber ");
                }

                return this.BaseRepository().FindList<MeioErpYunTuBillingMaintenanceErrorEntity>(strSql.ToString(), dp);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public bool CheckIsYunTuBillingMaintenanceCode(string code, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpYunTuBillingMaintenanceEntity>(t => t.Code == code && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public string CheckIsYunTuBillingMaintenanceCode(string code)
        {
            try
            {
                var codeList = code.Split(',').ToList();

                var info = this.BaseRepository().FindList<MeioErpYunTuBillingMaintenanceEntity>(t => codeList.Contains(t.Code));
                string codes = "";
                if (info.Count() > 0)
                {
                    codes = string.Join(",", info.Select(o => o.Code));
                }
                return codes;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
