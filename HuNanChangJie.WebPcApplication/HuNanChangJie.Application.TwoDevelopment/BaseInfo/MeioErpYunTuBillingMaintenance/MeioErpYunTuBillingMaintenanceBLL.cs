﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-18 11:47
    /// 描 述：云途账单维护
    /// </summary>
    public class MeioErpYunTuBillingMaintenanceBLL : MeioErpYunTuBillingMaintenanceIBLL
    {
        private MeioErpYunTuBillingMaintenanceService meioErpYunTuBillingMaintenanceService = new MeioErpYunTuBillingMaintenanceService();
        private object objLock=new object();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpYunTuBillingMaintenanceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpYunTuBillingMaintenanceService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpYunTuBillingMaintenance表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpYunTuBillingMaintenanceEntity GetMeioErpYunTuBillingMaintenanceEntity(string keyValue)
        {
            try
            {
                return meioErpYunTuBillingMaintenanceService.GetMeioErpYunTuBillingMaintenanceEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpYunTuBillingMaintenanceService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpYunTuBillingMaintenanceEntity entity,string deleteList,string type)
        {
            try
            {
                lock (objLock)
                {
                    meioErpYunTuBillingMaintenanceService.SaveEntity(keyValue, entity, deleteList, type);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public int Sync(string keyValue)
        {
            try
            {
                lock (objLock)
                {
                    return meioErpYunTuBillingMaintenanceService.Sync(keyValue);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void Compute(string keyValue)
        {
            try
            {
                lock (objLock)
                {
                    meioErpYunTuBillingMaintenanceService.Compute(keyValue);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpYunTuBillingMaintenanceErrorEntity> GetBillingMaintenanceErrorList(string BillingMaintenanceID, string WaybillNumber)
        {
            try
            {
               return meioErpYunTuBillingMaintenanceService.GetBillingMaintenanceErrorList(BillingMaintenanceID,WaybillNumber);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public bool CheckIsYunTuBillingMaintenanceCode(string code, string id)
        {
            try
            {

                return meioErpYunTuBillingMaintenanceService.CheckIsYunTuBillingMaintenanceCode(code, id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public string CheckIsYunTuBillingMaintenanceCode(string code)
        {
            try
            {
                return meioErpYunTuBillingMaintenanceService.CheckIsYunTuBillingMaintenanceCode(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
