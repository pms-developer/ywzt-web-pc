﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-05 18:25
    /// 描 述：数据中台同步渠道信息
    /// </summary>
    public class MeioDataCenterChannelInformationEntity 
    {
        #region  实体成员
        /// <summary>
        /// 仓库编码
        /// </summary>
        /// <returns></returns>
        [Column("WHCODE")]
        public string WhCode { get; set; }
        /// <summary>
        /// 渠道编码
        /// </summary>
        /// <returns></returns>
        [Column("CHANNELCODE")]
        public string ChannelCode { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        /// <returns></returns>
        [Column("CHANNELNAME")]
        public string ChannelName { get; set; }
        /// <summary>
        /// 承运商编码
        /// </summary>
        /// <returns></returns>
        [Column("CARRIERCODE")]
        public string CarrierCode { get; set; }
        /// <summary>
        /// 承运商名称
        /// </summary>
        /// <returns></returns>
        [Column("CARRIERNAME")]
        public string CarrierName { get; set; }
        /// <summary>
        /// 渠道的签名类型：DIRECT,INDIRECT,ADULT
        /// </summary>
        /// <returns></returns>
        [Column("SIGNATURETYPE")]
        public string SignatureType { get; set; }
        /// <summary>
        /// 是否支持保险 0不支持 1支持
        /// </summary>
        /// <returns></returns>
        [Column("INSURANCEFLAG")]
        public int? InsuranceFlag { get; set; }
        /// <summary>
        /// 可见状态 1可见2不可见
        /// </summary>
        /// <returns></returns>
        [Column("State")]
        public int? State { get; set; }
        /// <summary>
        /// 渠道类型：1-需获取面单（打单系统）；2-无需面单；3-上传物流面单（用户上传）
        /// </summary>
        /// <returns></returns>
        [Column("CHANNELTYPE")]
        public int? ChannelType { get; set; }
        /// <summary>
        /// ChannelGroupFlag
        /// </summary>
        /// <returns></returns>
        [Column("CHANNELGROUPFLAG")]
        public int? ChannelGroupFlag { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        /// <returns></returns>
        [Column("OWNER")]
        public string Owner { get; set; }
        /// <summary>
        /// 数据来源
        /// </summary>
        /// <returns></returns>
        [Column("SYSTEMSOURCE")]
        public string SystemSource { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        /// <returns></returns>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        /// <returns></returns>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        /// <returns></returns>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        /// <returns></returns>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        /// <returns></returns>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        /// <returns></returns>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        /// <returns></returns>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        /// <returns></returns>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        /// <returns></returns>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        /// <returns></returns>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        /// <returns></returns>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        /// <returns></returns>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        /// <returns></returns>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        /// <returns></returns>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            this.ModificationDate = DateTime.Now;
            if (LoginUserInfo.Get() != null)
            {
                UserInfo userInfo = LoginUserInfo.Get();
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;

                this.Modification_Id = userInfo.userId;
                this.ModificationName = userInfo.realName;
            }
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            this.ModificationDate = DateTime.Now;
            if (LoginUserInfo.Get() != null)
            {
                UserInfo userInfo = LoginUserInfo.Get();
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
                
                this.Modification_Id = userInfo.userId;
                this.ModificationName = userInfo.realName;
            }
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            //if (LoginUserInfo.Get() != null)
            //{
            //    UserInfo userInfo = LoginUserInfo.Get();
            //    this.Modification_Id = userInfo.userId;
            //    this.ModificationName = userInfo.realName;
            //}
        }
        #endregion
    }
}

