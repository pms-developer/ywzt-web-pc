﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-19 11:00
    /// 描 述：云途账单
    /// </summary>
    public class MeioErpYunTuBillService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private DataItemIBLL dataItemIBLL=new DataItemBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpYunTuBillEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.WaybillNumber,
                t.WaybillState,
                t.OrderCode,
                t.OwnerID,
                '['+t1.Code+']'+t1.Name OwnerName,
                t.WarehouseID,
                t2.Name WarehouseName,
                t.DeliveryTime,
                t.NetWeight,
                t.ChargeableWeight,
                t.DestinationCountry,
                t.ProductName,
                t.Freight,
                t.RegistrationFee,
                t.HandlingFee,
                t.TotalFee,
                t.AdjustmentAmount,
                t.TotalAmount,
                t.CreationDate,
                t.AuditStatus,
                t.AuditTime,
                t.AuditorName,
                t.Remark,
                t3.RuleName
                ");
                strSql.Append("  FROM MeioErpYunTuBill t ");
                strSql.Append("  LEFT JOIN MeioErpOwner t1 ON t.OwnerCode=t1.Code ");
                strSql.Append("  LEFT JOIN MeioErpWarehouse t2 ON t.WarehouseCode=t2.Code ");
                strSql.Append("  LEFT JOIN MeioErpFreightQuotation t3 ON t.FreightQuotationID=t3.ID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.DeliveryTime >= @startTime AND t.DeliveryTime <= @endTime ) ");
                }
                if (!queryParam["CreateStartTime"].IsEmpty() && !queryParam["CreateEndTime"].IsEmpty())
                {
                    dp.Add("createStartTime", queryParam["CreateStartTime"].ToDate(), DbType.DateTime);
                    dp.Add("createEndTime", queryParam["CreateEndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @createStartTime AND t.CreationDate <= @createEndTime ) ");
                }
                if (!queryParam["WaybillState"].IsEmpty())
                {
                    dp.Add("WaybillState",queryParam["WaybillState"].ToString(), DbType.String);
                    strSql.Append(" AND t.WaybillState = @WaybillState ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID",queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID",queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                if (!queryParam["WaybillNumber"].IsEmpty())
                {
                    dp.Add("WaybillNumber", "%" + queryParam["WaybillNumber"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.WaybillNumber Like @WaybillNumber ");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    dp.Add("AuditStatus",queryParam["AuditStatus"].ToString(), DbType.String);
                    strSql.Append(" AND t.AuditStatus = @AuditStatus ");
                }
                if (!queryParam["OwnerCode"].IsEmpty())
                {
                    dp.Add("OwnerCode", queryParam["OwnerCode"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerCode = @OwnerCode ");
                }
                var list=this.BaseRepository().FindList<MeioErpYunTuBillEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpYunTuBill表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpYunTuBillEntity GetMeioErpYunTuBillEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpYunTuBillEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpYunTuBillEntity> GetMeioErpYunTuBillEntityByBillingMaintenanceID(string billingMaintenanceID)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpYunTuBillEntity>(p=>p.BillingMaintenanceID==billingMaintenanceID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpYunTuBillEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpYunTuBillEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                    
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion


        /// <summary>
        /// 更改运单状态
        /// </summary>
        public void UpdateWaybillState()
        {
            try
            {
                var orderList = this.BaseRepository().FindList<MeioErpYunTuBillEntity>(p => p.WaybillState != "已签收");
                List<DataItemDetailEntity> listDateItemDetail = dataItemIBLL.GetDetailList("YunTuWaybillState");

                foreach (var order in orderList)
                {
                    var CarrierList = this.BaseRepository().FindList<MeioERPGeneralCarrierEntity>();
                    var carrierEntity = CarrierList.FirstOrDefault(p => p.Code == "YTWL");
                    if (carrierEntity != null)
                    {
                        if (!string.IsNullOrEmpty(carrierEntity.InterfaceServiceAddress) && !string.IsNullOrEmpty(carrierEntity.InterfaceUserInfo)
                            && !string.IsNullOrEmpty(carrierEntity.InterfaceAccount) && !string.IsNullOrEmpty(carrierEntity.InterfaceKey))
                        {
                            YunExpressService.strUrl = carrierEntity.InterfaceServiceAddress;
                            YunExpressService.appId = carrierEntity.InterfaceAccount;
                            YunExpressService.appSecret = carrierEntity.InterfaceKey;
                            YunExpressService.sourceKey = carrierEntity.InterfaceUserInfo;

                            var orderResult = YunExpressService.getOrderInfo(order.WaybillNumber);
                            if (orderResult.success.Value && orderResult.result != null)
                            {
                                var itemvalue = listDateItemDetail.Find(p => p.F_ItemValue == orderResult.result.status);
                                if (itemvalue != null)
                                {
                                    order.WaybillState = itemvalue.F_ItemName;
                                }
                                else
                                {
                                    order.WaybillState = orderResult.result.status;
                                }
                                //order.Modify(order.ID);
                                this.BaseRepository().Update(order);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 审核OR反审核账单
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="auditStatus"></param>
        public void Audit(string keyValue, string auditStatus)
        {
            try
            {
                var ids = keyValue.Replace(",","','");
                string sql = "";
                if (auditStatus == "0")
                {
                    sql = string.Format("UPDATE MeioErpYunTuBill SET AuditStatus='{1}',AuditTime=NULL,AuditorName=NULL WHERE ID IN ('{0}')", ids, auditStatus);
                }
                else
                {
                    UserInfo userInfo = LoginUserInfo.Get();
                    sql = string.Format("UPDATE MeioErpYunTuBill SET AuditStatus='{1}',AuditTime=GETDATE(),AuditorName='{2}' WHERE ID IN ('{0}') And AuditStatus<>'1'", ids, auditStatus,userInfo.realName);
                }
                this.BaseRepository().ExecuteBySql(sql);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 批量备注
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="remark"></param>
        public void BatchRemark(string keyValue, string remark)
        {
            try
            {
                var ids = keyValue.Replace(",", "','");
                string sql = string.Format("UPDATE MeioErpYunTuBill SET Remark='{1}' WHERE ID IN ('{0}')", ids, remark);
                this.BaseRepository().ExecuteBySql(sql);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 调整金额
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="amount"></param>
        /// <param name="remark"></param>
        public void AdjustmentAmount(string keyValue, decimal amount, string remark)
        {
            try
            {
                var entity= GetMeioErpYunTuBillEntity(keyValue);
                if (entity != null)
                {
                   
                    entity.Remark = remark;
                    entity.TotalAmount= entity.TotalAmount-entity.AdjustmentAmount+amount;
                    entity.AdjustmentAmount = amount;
                }
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
