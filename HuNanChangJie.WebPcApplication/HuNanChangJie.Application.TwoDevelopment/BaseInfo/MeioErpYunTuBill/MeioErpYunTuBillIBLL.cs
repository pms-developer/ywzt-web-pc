﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-19 11:00
    /// 描 述：云途账单
    /// </summary>
    public interface MeioErpYunTuBillIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpYunTuBillEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpYunTuBill表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpYunTuBillEntity GetMeioErpYunTuBillEntity(string keyValue);
        IEnumerable<MeioErpYunTuBillEntity> GetMeioErpYunTuBillEntityByBillingMaintenanceID(string billingMaintenanceID);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpYunTuBillEntity entity,string deleteList,string type);
        #endregion

        void UpdateWaybillState();
        void Audit(string keyValue, string auditStatus,string auditorName);
        void BatchRemark(string keyValue, string remark);
        void AdjustmentAmount(string keyValue, decimal amount, string remark, string oldAmount);
    }
}
