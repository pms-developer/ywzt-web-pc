﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-19 11:00
    /// 描 述：云途账单
    /// </summary>
    public class MeioErpYunTuBillEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// 运单号
        /// </summary>
        [Column("WAYBILLNUMBER")]
        public string WaybillNumber { get; set; }
        /// <summary>
        /// 运单状态
        /// </summary>
        [Column("WAYBILLSTATE")]
        public string WaybillState { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        [Column("ORDERCODE")]
        public string OrderCode { get; set; }
        /// <summary>
        /// 货主ID
        /// </summary>
        [Column("OWNERID")]
        public string OwnerID { get; set; }
        /// <summary>
        /// 货主编码
        /// </summary>
        [Column("OWNERCODE")]
        public string OwnerCode { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        [Column("WAREHOUSECODE")]
        public string WarehouseCode { get; set; }
        /// <summary>
        /// 发货时间
        /// </summary>
        [Column("DELIVERYTIME")]
        public DateTime? DeliveryTime { get; set; }
        /// <summary>
        /// 仓库称重
        /// </summary>
        [Column("NETWEIGHT")]
        public string NetWeight { get; set; }
        /// <summary>
        /// 计费重量
        /// </summary>
        [Column("CHARGEABLEWEIGHT")]
        public string ChargeableWeight { get; set; }
        /// <summary>
        /// 目的地
        /// </summary>
        [Column("DESTINATIONCOUNTRY")]
        public string DestinationCountry { get; set; }
        /// <summary>
        /// 分区
        /// </summary>
        [Column("PARTITION")]
        public string Partition { get; set; }
        /// <summary>
        /// 品名
        /// </summary>
        [Column("PRODUCTNAME")]
        public string ProductName { get; set; }
        /// <summary>
        /// 运费
        /// </summary>
        [Column("FREIGHT")]
        public decimal? Freight { get; set; }
        /// <summary>
        /// 挂号费
        /// </summary>
        [Column("REGISTRATIONFEE")]
        public decimal? RegistrationFee { get; set; }
        /// <summary>
        /// 手续费
        /// </summary>
        [Column("HANDLINGFEE")]
        public decimal? HandlingFee { get; set; }
        /// <summary>
        /// 代收税费
        /// </summary>
        [Column("TOTALFEE")]
        public decimal? TotalFee { get; set; }
        /// <summary>
        /// 调整金额
        /// </summary>
        [Column("ADJUSTMENTAMOUNT")]
        public decimal? AdjustmentAmount { get; set; }
        /// <summary>
        /// 总金额
        /// </summary>
        [Column("TOTALAMOUNT")]
        public decimal? TotalAmount { get; set; }
        /// <summary>
        /// 账单号ID
        /// </summary>
        [Column("BILLINGMAINTENANCEID")]
        public string BillingMaintenanceID { get; set; }
        /// <summary>
        /// 审核时间
        /// </summary>
        [Column("AUDITTIME")]
        public DateTime? AuditTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 退件退费
        /// </summary>
        [Column("RETURNFEE")]
        public decimal? ReturnFee { get; set; }
        /// <summary>
        /// 报价规则ID
        /// </summary>
        [Column("FREIGHTQUOTATIONID")]
        public string FreightQuotationID { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.WarehouseID = WarehouseUtil.UserWarehouse;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.WarehouseID = WarehouseUtil.UserWarehouse;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        /// <summary>
        /// 规则名称
        /// </summary>
        [NotMapped]
        public string RuleName { get; set; }
        /// <summary>
        /// 货主名称
        /// </summary>
        [NotMapped]
        public string OwnerName { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        [NotMapped]
        public string WarehouseName { get; set; }
        #endregion
    }
}

