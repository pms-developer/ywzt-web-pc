﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.SystemCommon;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-19 11:00
    /// 描 述：云途账单
    /// </summary>
    public class MeioErpYunTuBillBLL : MeioErpYunTuBillIBLL
    {
        private MeioErpYunTuBillService meioErpYunTuBillService = new MeioErpYunTuBillService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpYunTuBillEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpYunTuBillService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpYunTuBill表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpYunTuBillEntity GetMeioErpYunTuBillEntity(string keyValue)
        {
            try
            {
                return meioErpYunTuBillService.GetMeioErpYunTuBillEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<MeioErpYunTuBillEntity> GetMeioErpYunTuBillEntityByBillingMaintenanceID(string billingMaintenanceID)
        {
            try
            {
                return meioErpYunTuBillService.GetMeioErpYunTuBillEntityByBillingMaintenanceID(billingMaintenanceID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpYunTuBillService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpYunTuBillEntity entity,string deleteList,string type)
        {
            try
            {
                meioErpYunTuBillService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 更改运单状态
        /// </summary>
        public void UpdateWaybillState()
        {
            try
            {
                meioErpYunTuBillService.UpdateWaybillState();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 审核OR反审核账单
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="auditStatus"></param>
        public void Audit(string keyValue, string auditStatus,string auditorName)
        {
            try
            {
                meioErpYunTuBillService.Audit(keyValue,auditStatus);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 批量备注
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="remark"></param>
        public void BatchRemark(string keyValue, string remark)
        {
            try
            {
                meioErpYunTuBillService.BatchRemark(keyValue, remark);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 调整金额
        /// </summary>
        public void AdjustmentAmount(string keyValue, decimal amount, string remark, string oldAmount)
        {
            try
            {
                meioErpYunTuBillService.AdjustmentAmount(keyValue, amount,remark);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
