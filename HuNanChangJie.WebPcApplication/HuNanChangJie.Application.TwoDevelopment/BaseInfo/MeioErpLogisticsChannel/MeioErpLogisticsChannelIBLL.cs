﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-08 14:02
    /// 描 述：物流渠道维护
    /// </summary>
    public interface MeioErpLogisticsChannelIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpLogisticsChannelEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpLogisticsChannel表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpLogisticsChannelEntity GetMeioErpLogisticsChannelEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpLogisticsChannelEntity entity, string deleteList, string type);
        #endregion
        bool CheckIsLogisticsChannelCode(string code, string id);
        bool CheckIsLogisticsChannelName(string name, string id);
        IEnumerable<MeioErpLogisticsChannelEntity> GetLogisticsChannelListByGeneralCarrier(string generalCarrier);
    }
}
