﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-08 14:02
    /// 描 述：物流渠道维护
    /// </summary>
    public class MeioErpLogisticsChannelBLL : MeioErpLogisticsChannelIBLL
    {
        private MeioErpLogisticsChannelService meioErpLogisticsChannelService = new MeioErpLogisticsChannelService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpLogisticsChannelEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpLogisticsChannelService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpLogisticsChannel表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpLogisticsChannelEntity GetMeioErpLogisticsChannelEntity(string keyValue)
        {
            try
            {
                return meioErpLogisticsChannelService.GetMeioErpLogisticsChannelEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpLogisticsChannelService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpLogisticsChannelEntity entity,string deleteList,string type)
        {
            try
            {
                meioErpLogisticsChannelService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public bool CheckIsLogisticsChannelCode(string code, string id)
        {
            try
            {
                return meioErpLogisticsChannelService.CheckIsLogisticsChannelCode(code, id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsLogisticsChannelName(string name, string id)
        {
            try
            {
                return meioErpLogisticsChannelService.CheckIsLogisticsChannelName(name, id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<MeioErpLogisticsChannelEntity> GetLogisticsChannelListByGeneralCarrier(string generalCarrier)
        {
            try
            {
                return meioErpLogisticsChannelService.GetLogisticsChannelListByGeneralCarrier(generalCarrier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
