﻿using Dapper;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS回传中台包材采购入库单
    /// </summary>
    public class MeioErpPurchaseWmsInStorageService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private static readonly object objLock = new object();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseWmsInStorageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.WarehouseID,
                t.OwnerID,
                t.Type,
                t.CreationName,
                t.CreationDate
                ");
                strSql.Append("  FROM MeioErpPurchaseWmsInStorage t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID", queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type = @Type ");
                }
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpPurchaseWmsInStorageEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseWmsInStorageDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseWmsInStorageDetailEntity> GetMeioErpPurchaseWmsInStorageDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpPurchaseWmsInStorageDetailEntity>(t => t.WmsInStorageID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseWmsInStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseWmsInStorageEntity GetMeioErpPurchaseWmsInStorageEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpPurchaseWmsInStorageEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根据采购入库单编号，获取MeioErpPurchaseWmsInStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseWmsInStorageEntity GetMeioErpPurchaseWmsInStorageEntityByCode(string code)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpPurchaseWmsInStorageEntity>(p => p.Code == code).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseWmsInStorageDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseWmsInStorageDetailEntity GetMeioErpPurchaseWmsInStorageDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpPurchaseWmsInStorageDetailEntity>(t => t.WmsInStorageID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpPurchaseWmsInStorageEntity = GetMeioErpPurchaseWmsInStorageEntity(keyValue);
                db.Delete<MeioErpPurchaseWmsInStorageEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpPurchaseWmsInStorageDetailEntity>(t => t.WmsInStorageID == meioErpPurchaseWmsInStorageEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpPurchaseWmsInStorageEntity entity, List<MeioErpPurchaseWmsInStorageDetailEntity> meioErpPurchaseWmsInStorageDetailList, string deleteList, string type)
        {
            lock (objLock)
            {
                var db = this.BaseRepository().BeginTrans();
                try
                {
                    if (type == "edit")
                    {
                        //var meioErpPurchaseWmsInStorageEntityTmp = GetMeioErpPurchaseWmsInStorageEntity(keyValue);
                        entity.Modify(keyValue);
                        db.Update(entity);

                        db.Delete<MeioErpPurchaseWmsInStorageDetailEntity>(p => p.WmsInStorageID == keyValue);
                        foreach (MeioErpPurchaseWmsInStorageDetailEntity item in meioErpPurchaseWmsInStorageDetailList)
                        {
                            item.WmsInStorageID = keyValue;
                            item.Create();
                            db.Insert(item);
                        }
                    }
                    else
                    {
                        entity.Create();
                        db.Insert(entity);
                        foreach (MeioErpPurchaseWmsInStorageDetailEntity item in meioErpPurchaseWmsInStorageDetailList)
                        {
                            item.WmsInStorageID = entity.ID;
                            item.Create();
                            db.Insert(item);
                        }
                    }
                    //修改发货单，采购单入库数量，更改库存数量
                    if (entity != null)
                    {
                        var deliveryEntity = this.BaseRepository().FindEntity<MeioErpDeliveryEntity>(p => p.Code == entity.Code);
                        var inventoryList = this.BaseRepository().FindList<MeioErpInventoryEntity>(p => p.WarehouseID == entity.WarehouseID && p.OwnerID == entity.OwnerID);
                        var meioErpPurchaseWmsInStorageIDListTmp = this.BaseRepository().FindList<MeioErpPurchaseWmsInStorageEntity>(t => t.Code == entity.Code).Select(o => o.ID);
                        var meioErpPurchaseWmsInStorageDetailListTmp = this.BaseRepository().FindList<MeioErpPurchaseWmsInStorageDetailEntity>(t => meioErpPurchaseWmsInStorageIDListTmp.Contains(t.WmsInStorageID));
                        if (deliveryEntity != null)
                        {
                            var deliveryDetailList = this.BaseRepository().FindList<MeioErpDeliveryDetailEntity>(p => p.DeliveryID == deliveryEntity.ID);
                            var purchaseDetailList = this.BaseRepository().FindList<MeioErpPurchaseDetailEntity>(p => p.PurchaseID == deliveryEntity.PurchaseID);

                            if (purchaseDetailList.Count() > 0 && meioErpPurchaseWmsInStorageDetailList != null && meioErpPurchaseWmsInStorageDetailList.Count() > 0)
                            {
                                foreach (var item in purchaseDetailList)
                                {
                                    if (meioErpPurchaseWmsInStorageDetailList.Any(p => p.GoodsCode == item.GoodsCode))
                                    {
                                        var inStorageQuantity= deliveryDetailList.FirstOrDefault(f => f.GoodsCode == item.GoodsCode).InStorageQuantity;
                                        item.InStorageQuantity = (item.InStorageQuantity.HasValue ? item.InStorageQuantity.Value : 0) - (inStorageQuantity.HasValue? inStorageQuantity.Value:0) + meioErpPurchaseWmsInStorageDetailList.FirstOrDefault(f => f.GoodsCode == item.GoodsCode)?.InStorageQuantity;
                                        item.Modify(item.ID);
                                        db.Update(item);
                                    }
                                }
                            }

                            if (deliveryDetailList.Count() > 0 && meioErpPurchaseWmsInStorageDetailList != null && meioErpPurchaseWmsInStorageDetailList.Count() > 0)
                            {
                                foreach (var item in deliveryDetailList)
                                {
                                    if (meioErpPurchaseWmsInStorageDetailList.Any(p => p.GoodsCode == item.GoodsCode))
                                    {
                                        item.InStorageQuantity = meioErpPurchaseWmsInStorageDetailList.FirstOrDefault(f => f.GoodsCode == item.GoodsCode).InStorageQuantity;
                                        item.Modify(item.ID);
                                        db.Update(item);
                                    }
                                }
                            }
                        }
                        //更改库存
                        foreach (var item in meioErpPurchaseWmsInStorageDetailList)
                        {
                            var inventoryEntity = inventoryList.FirstOrDefault(p => p.GoodsCode == item.GoodsCode);
                            if (inventoryEntity != null)
                            {
                                var inStorageDetailEntity = meioErpPurchaseWmsInStorageDetailListTmp.Where(p => p.GoodsCode == item.GoodsCode).OrderByDescending(o => o.InStorageQuantity).FirstOrDefault();
                                if (inStorageDetailEntity != null)
                                {
                                    inventoryEntity.InQuantity = inventoryEntity.InQuantity + item.InStorageQuantity - inStorageDetailEntity.InStorageQuantity;
                                }
                                else
                                {
                                    inventoryEntity.InQuantity = inventoryEntity.InQuantity + item.InStorageQuantity;
                                }
                                inventoryEntity.Modify(inventoryEntity.ID);
                                db.Update(inventoryEntity);
                            }
                            else
                            {
                                inventoryEntity = new MeioErpInventoryEntity();
                                inventoryEntity.Create();
                                inventoryEntity.WarehouseID = entity.WarehouseID;
                                inventoryEntity.OwnerID = entity.OwnerID;
                                inventoryEntity.GoodsCode = item.GoodsCode;
                                inventoryEntity.GoodsName = item.GoodsName;
                                inventoryEntity.GoodsType = "包材";
                                inventoryEntity.InQuantity = item.InStorageQuantity;
                                db.Insert(inventoryEntity);
                            }
                            //添加批次库存
                            db.Delete<MeioErpBatchInventoryEntity>(p => p.GoodsCode == item.GoodsCode && p.BatchNo == item.EntryDetailTakeNo && p.InStorageCode == entity.Code);

                            MeioErpBatchInventoryEntity meioErpBatchInventoryEntity = new MeioErpBatchInventoryEntity
                            {
                                WarehouseID=entity.WarehouseID,
                                OwnerID=entity.OwnerID,
                                GoodsCode=item.GoodsCode,
                                GoodsName=item.GoodsName,
                                InQuantity=item.InStorageQuantity,
                                MigrationQuantity=0,
                                FrozenQuantity=0,
                                BatchNo=item.EntryDetailTakeNo,
                                InStorageCode=entity.Code
                            };
                            meioErpBatchInventoryEntity.Create();
                            db.Insert(meioErpBatchInventoryEntity);

                        }
                    }

                    db.Commit();
                }
                catch (Exception ex)
                {
                    db.Rollback();
                    if (ex is ExceptionEx)
                    {
                        throw;
                    }
                    else
                    {
                        throw ExceptionEx.ThrowServiceException(ex);
                    }
                }
            }
        }

        #endregion
    }
}
