﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    /// </summary>
    public class MeioErpPurchaseWmsInStorageBLL : MeioErpPurchaseWmsInStorageIBLL
    {
        private MeioErpPurchaseWmsInStorageService meioErpPurchaseWmsInStorageService = new MeioErpPurchaseWmsInStorageService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseWmsInStorageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpPurchaseWmsInStorageService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseWmsInStorageDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseWmsInStorageDetailEntity> GetMeioErpPurchaseWmsInStorageDetailList(string keyValue)
        {
            try
            {
                return meioErpPurchaseWmsInStorageService.GetMeioErpPurchaseWmsInStorageDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseWmsInStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseWmsInStorageEntity GetMeioErpPurchaseWmsInStorageEntity(string keyValue)
        {
            try
            {
                return meioErpPurchaseWmsInStorageService.GetMeioErpPurchaseWmsInStorageEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 根据采购入库单编号，获取MeioErpPurchaseWmsInStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseWmsInStorageEntity GetMeioErpPurchaseWmsInStorageEntityByCode(string code)
        {
            try
            {
                return meioErpPurchaseWmsInStorageService.GetMeioErpPurchaseWmsInStorageEntityByCode(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseWmsInStorageDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseWmsInStorageDetailEntity GetMeioErpPurchaseWmsInStorageDetailEntity(string keyValue)
        {
            try
            {
                return meioErpPurchaseWmsInStorageService.GetMeioErpPurchaseWmsInStorageDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpPurchaseWmsInStorageService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpPurchaseWmsInStorageEntity entity, List<MeioErpPurchaseWmsInStorageDetailEntity> meioErpPurchaseWmsInStorageDetailList, string deleteList, string type)
        {
            try
            {
                meioErpPurchaseWmsInStorageService.SaveEntity(keyValue, entity, meioErpPurchaseWmsInStorageDetailList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
    }
}
