﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    /// </summary>
    public interface MeioErpPurchaseWmsInStorageIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpPurchaseWmsInStorageEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpPurchaseWmsInStorageDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpPurchaseWmsInStorageDetailEntity> GetMeioErpPurchaseWmsInStorageDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpPurchaseWmsInStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpPurchaseWmsInStorageEntity GetMeioErpPurchaseWmsInStorageEntity(string keyValue);
        MeioErpPurchaseWmsInStorageEntity GetMeioErpPurchaseWmsInStorageEntityByCode(string code);
        /// <summary>
        /// 获取MeioErpPurchaseWmsInStorageDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpPurchaseWmsInStorageDetailEntity GetMeioErpPurchaseWmsInStorageDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpPurchaseWmsInStorageEntity entity, List<MeioErpPurchaseWmsInStorageDetailEntity> meioErpPurchaseWmsInStorageDetailList, string deleteList, string type);
        #endregion
    }
}
