﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using System.Threading.Tasks;
using KafkaNet.Model;
using KafkaNet;
using HuNanChangJie.Kafka.Dto;
using HuNanChangJie.Kafka.Service;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-13 16:32
    /// 描 述：合同管理
    /// </summary>
    public class ContractService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();

        //private readonly static string contractingParty = Config.GetValue("ContractContractingParty");
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpContractEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.ClientName,
                t.LegalPerson,
                t.ClientPersonCharge,
                t.ClientPersonChargePhone,
                t.ContractingParty,
                t1.F_RealName ContractingPartyName,
                t.CreatedBy,
                t.CreatedDate,
t.Status,
t.AuditStatus,
t.ServiceStartDate,
t.ServiceEndDate,
t.PriceSheetID
                ");
                strSql.Append("  FROM MeioErpContract t ");
                strSql.Append(" LEFT JOIN Base_User t1 ON t.ContractingParty=t1.F_UserId ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", queryParam["Code"].ToString(), DbType.String);
                    strSql.Append(" AND t.Code = @Code ");
                }
                if (!queryParam["ClientCode"].IsEmpty())
                {
                    dp.Add("ClientCode", "%" + queryParam["ClientCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ClientCode Like @ClientCode ");
                }
                if (!queryParam["ClientName"].IsEmpty())
                {
                    dp.Add("ClientName", "%" + queryParam["ClientName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ClientName Like @ClientName ");
                }
                if (!queryParam["PriceSheetType"].IsEmpty())
                {
                    dp.Add("PriceSheetType", queryParam["PriceSheetType"].ToString(), DbType.String);
                    strSql.Append(" AND t.PriceSheetType = @PriceSheetType ");
                }
                if (!queryParam["Status"].IsEmpty())
                {
                    dp.Add("Status", queryParam["Status"].ToString(), DbType.String);
                    strSql.Append(" AND t.Status = @Status ");
                }

                if (!queryParam["ContractingParty"].IsEmpty())
                {
                    dp.Add("ContractingParty", queryParam["ContractingParty"].ToString(), DbType.String);
                    strSql.Append(" AND t.ContractingParty = @ContractingParty ");
                }


                strSql.Append(" ORDER BY t.CreatedDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpContractEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreatedDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContractDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpContractDetailEntity> GetMeioErpContractDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpContractDetailEntity>(t => t.ContractID == keyValue);
                var query = from item in list orderby item.CreatedDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpContractEntity GetMeioErpContractEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpContractEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContractDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpContractDetailEntity GetMeioErpContractDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpContractDetailEntity>(t => t.ContractID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContractRecord表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpContractRecordEntity GetMeioErpContractRecordEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpContractRecordEntity>(t => t.MeioErpContractID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpContractEntity = GetMeioErpContractEntity(keyValue);
                db.Delete<MeioErpContractEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpContractDetailEntity>(t => t.ContractID == meioErpContractEntity.ID);
                //db.Delete<MeioErpContractRecordEntity>(t => t.MeioErpContractID == meioErpContractEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpContractEntity entity, List<MeioErpContractDetailEntity> meioErpContractDetailList, MeioErpContractRecordEntity meioErpContractRecordEntity, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioErpContractEntityTmp = GetMeioErpContractEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);

                    db.Delete<MeioErpContractDetailEntity>(t => t.ContractID == keyValue);
                    foreach (var item in meioErpContractDetailList)
                    {
                        item.Create();
                        item.ContractID = keyValue;
                        db.Insert(item);
                    }
                }
                else
                {
                    entity.Create();
                    entity.Status = 1;
                    entity.AuditStatus = 0;
                    db.Insert(entity);
                    foreach (var item in meioErpContractDetailList)
                    {
                        item.Create();
                        item.ContractID = entity.ID;
                        db.Insert(item);
                    }

                    meioErpContractRecordEntity = new MeioErpContractRecordEntity();
                    meioErpContractRecordEntity.MeioErpContractID = entity.ID;
                    meioErpContractRecordEntity.ContractCode = entity.Code;
                    meioErpContractRecordEntity.ContractStatus = 1;
                    meioErpContractRecordEntity.ServiceStartDate = entity.ServiceStartDate;
                    meioErpContractRecordEntity.ServiceEndDate = entity.ServiceEndDate;
                    meioErpContractRecordEntity.Create();
                    db.Insert(meioErpContractRecordEntity);
                }

                MeioErpContractChangeLogEntity meioErpContractChangeLogEntity = new MeioErpContractChangeLogEntity
                {
                    Code = entity.Code,
                    ContractInformation = entity.ToJson() + "&&" + meioErpContractDetailList.ToJson() + "&&" + meioErpContractRecordEntity.ToJson()
                };
                meioErpContractChangeLogEntity.Create();
                db.Insert(meioErpContractChangeLogEntity);

                db.Commit();

                if (type == "edit")
                {
                    //同步货主信息
                    var ownerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(p => p.Code == entity.ClientCode);
                    if (ownerEntity != null)
                    {
                        var contractDetail = GetMeioErpContractDetailList(keyValue);
                        ownerEntity.contractDetailValueAddedServices = new List<ContractDetailValueAddedServicesEntity>();
                        foreach (var item in contractDetail.Where(p => p.ItemType == 3))
                        {
                            var ValueAddedServicesEntity = new ContractDetailValueAddedServicesEntity
                            {
                                ID = item.ID,
                                ItemID = item.ItemID,
                                ItemName = item.ItemName,
                                ItemDetailName = item.ItemDetailName,
                                Price = item.Price.HasValue?item.Price.Value:0,
                                ContractPrice = item.ContractPrice.HasValue?item.ContractPrice.Value:0,
                                Discount = item.Discount.HasValue?item.Discount.Value:0,
                                Unit = item.Unit,
                                OperationPoint = item.OperationPoint,
                                PriceSheetType = item.Type,
                            };
                            ownerEntity.contractDetailValueAddedServices.Add(ValueAddedServicesEntity);
                        }

                        if (!string.IsNullOrEmpty(ownerEntity.WarehouseID))
                        {
                            var strWarehouseIds = ownerEntity.WarehouseID.Split(',');
                            var warehouseList = this.BaseRepository().FindList<MeioErpWarehouseEntity>(t => strWarehouseIds.Contains(t.ID));
                            ownerEntity.Warehouses = warehouseList.ToList();
                            KafkaService.SendMessage(Kafka.Action.update, "owner", ownerEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        public void ContractProcessing(string keyValue, int status, DateTime serviceStartDate, DateTime serviceEndDate)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                var db = this.BaseRepository().BeginTrans();
                try
                {
                    var meioErpContractEntityTmp = GetMeioErpContractEntity(keyValue);
                    meioErpContractEntityTmp.Status = status;
                    meioErpContractEntityTmp.ServiceStartDate = serviceStartDate;
                    meioErpContractEntityTmp.ServiceEndDate = serviceEndDate;
                    meioErpContractEntityTmp.Modify(keyValue);
                    db.Update(meioErpContractEntityTmp);

                    var meioErpContractRecordEntity = new MeioErpContractRecordEntity
                    {
                        MeioErpContractID = keyValue,
                        ContractCode = meioErpContractEntityTmp.Code,
                        ServiceStartDate = serviceStartDate,
                        ServiceEndDate = serviceEndDate,
                        ContractStatus = status,
                    };
                    meioErpContractRecordEntity.Create();
                    db.Insert(meioErpContractRecordEntity);

                    db.Commit();
                }
                catch (Exception ex)
                {
                    db.Rollback();
                    if (ex is ExceptionEx)
                    {
                        throw;
                    }
                    else
                    {
                        throw ExceptionEx.ThrowServiceException(ex);
                    }
                }
            }
        }

        public IEnumerable<MeioErpContractRecordEntity> GetMeioErpContractRecordList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpContractRecordEntity>(t => t.MeioErpContractID == keyValue);
                var query = from item in list orderby item.CreatedDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void AuditContract(string keyValue, MeioErpDocumentAuditRecordEntity meioErpDocumentAuditRecordEntity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                var db = this.BaseRepository().BeginTrans();
                try
                {
                    var meioErpContractEntityTmp = GetMeioErpContractEntity(keyValue);
                    meioErpContractEntityTmp.AuditStatus = meioErpDocumentAuditRecordEntity.AuditResult;
                    meioErpContractEntityTmp.Modify(keyValue);
                    db.Update(meioErpContractEntityTmp);

                    meioErpDocumentAuditRecordEntity.Create();
                    meioErpDocumentAuditRecordEntity.DocumentType = "Contract";
                    meioErpDocumentAuditRecordEntity.DocumentID = keyValue;
                    db.Insert(meioErpDocumentAuditRecordEntity);


                    if (meioErpDocumentAuditRecordEntity.AuditResult == 1)
                    {
                        var ownerentity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(t => t.Code == meioErpContractEntityTmp.ClientCode);
                        if (ownerentity == null)
                        {
                            var meioErpOwnerEntity = new MeioErpOwnerEntity
                            {
                                ContractID = keyValue,
                                Code = meioErpContractEntityTmp.ClientCode,
                                Name = meioErpContractEntityTmp.ClientName,
                                IsValid = true,
                                AccountNumber = meioErpContractEntityTmp.ClientCode,
                                Password = "Meio123456",
                                ContractingParty = meioErpContractEntityTmp.ContractingParty,
                                ServiceStartDate = meioErpContractEntityTmp.ServiceStartDate,
                                ServiceEndDate = meioErpContractEntityTmp.ServiceEndDate,
                                AccountBalance=0,
                                DbIp = "172.16.0.10",
                                DbPort="3306",
                                DbUsername="root",
                                DbPassword= "meiou1qaz!A",
                            };
                            meioErpOwnerEntity.Create();
                            int count = db.Insert(meioErpOwnerEntity);
                            //if (count > 0)
                            //{
                            //KafkaService.SendMessage(Kafka.Action.add, meioErpOwnerEntity);
                            //}
                        }
                        else
                        {
                            ownerentity.ContractID = keyValue;
                            ownerentity.Code = meioErpContractEntityTmp.ClientCode;
                            ownerentity.Name = meioErpContractEntityTmp.ClientName;
                            ownerentity.IsValid = true;
                            ownerentity.ContractingParty = meioErpContractEntityTmp.ContractingParty;
                            ownerentity.ServiceStartDate = meioErpContractEntityTmp.ServiceStartDate;
                            ownerentity.ServiceEndDate = meioErpContractEntityTmp.ServiceEndDate;
                            ownerentity.Modify(ownerentity.ID);
                            int count = db.Update(ownerentity);
                            //if (count > 0)
                            //{

                            //}
                        }
                    }

                    db.Commit();
                }
                catch (Exception ex)
                {
                    db.Rollback();
                    if (ex is ExceptionEx)
                    {
                        throw;
                    }
                    else
                    {
                        throw ExceptionEx.ThrowServiceException(ex);
                    }
                }
            }
        }

        public IEnumerable<MeioErpDocumentAuditRecordEntity> GetDocumentAuditRecordEntityList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpDocumentAuditRecordEntity>(t => t.DocumentID == keyValue && t.DocumentType == "Contract");
                var query = from item in list orderby item.AuditDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal bool CheckIsClientCode(string code, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpContractEntity>(t => t.ClientCode == code && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal bool IsEditForm(string priceSheetId)
        {
            try
            {
                var strPriceSheetIds = priceSheetId.Split(',');
                if (strPriceSheetIds.Length > 1)
                {
                    var list = this.BaseRepository().FindList<MeioErpContractEntity>(t => strPriceSheetIds.Contains(t.PriceSheetCode));
                    return list.Count() > 0;
                }
                else
                {
                    var list = this.BaseRepository().FindList<MeioErpContractEntity>(t => t.PriceSheetCode == priceSheetId);
                    return list.Count() > 0;
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal bool CheckIsClientContract(string code)
        {
            try
            {
                var date = DateTime.Now.Date;
                var info = this.BaseRepository().FindEntity<MeioErpContractEntity>(
                    t => t.ClientCode == code && t.ServiceStartDate <= date && t.ServiceEndDate >= date && t.Status != 3);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal MeioErpContractEntity GetContractByClientCode(string code)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpContractEntity>(
                    t => t.ClientCode == code && t.Status == 3).OrderByDescending(o => o.ServiceEndDate);
                if (list.Count() > 0)
                    return list.First();
                else
                    return null;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal bool CheckIsCode(string code, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpContractEntity>(t => t.Code == code && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
