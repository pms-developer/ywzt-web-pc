﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-13 16:32
    /// 描 述：合同管理
    /// </summary>
    public class ContractBLL : ContractIBLL
    {
        private ContractService contractService = new ContractService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpContractEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return contractService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContractDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpContractDetailEntity> GetMeioErpContractDetailList(string keyValue)
        {
            try
            {
                return contractService.GetMeioErpContractDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpContractEntity GetMeioErpContractEntity(string keyValue)
        {
            try
            {
                return contractService.GetMeioErpContractEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContractDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpContractDetailEntity GetMeioErpContractDetailEntity(string keyValue)
        {
            try
            {
                return contractService.GetMeioErpContractDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContractRecord表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpContractRecordEntity GetMeioErpContractRecordEntity(string keyValue)
        {
            try
            {
                return contractService.GetMeioErpContractRecordEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                contractService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpContractEntity entity, List<MeioErpContractDetailEntity> meioErpContractDetailList, MeioErpContractRecordEntity meioErpContractRecordEntity, string deleteList, string type)
        {
            try
            {
                contractService.SaveEntity(keyValue, entity, meioErpContractDetailList, meioErpContractRecordEntity, deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("ContractCode1");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public void ContractProcessing(string keyValue, int status, DateTime serviceStartDate, DateTime serviceEndDate)
        {
            try
            {
                contractService.ContractProcessing(keyValue, status, serviceStartDate, serviceEndDate);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<MeioErpContractRecordEntity> GetMeioErpContractRecordList(string keyValue)
        {
            try
            {
                return contractService.GetMeioErpContractRecordList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void AuditContract(string keyValue, MeioErpDocumentAuditRecordEntity meioErpDocumentAuditRecordEntity)
        {
            try
            {
                contractService.AuditContract(keyValue, meioErpDocumentAuditRecordEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<MeioErpDocumentAuditRecordEntity> GetDocumentAuditRecordEntityList(string keyValue)
        {
            try
            {
                return contractService.GetDocumentAuditRecordEntityList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsClientCode(string code, string id)
        {
            try
            {
                return contractService.CheckIsClientCode(code, id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsClientContract(string clientCode)
        {
            try
            {
                return contractService.CheckIsClientContract(clientCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool IsEditForm(string priceSheetId)
        {
            try
            {
                return contractService.IsEditForm(priceSheetId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public MeioErpContractEntity GetContractByClientCode(string code)
        {
            try
            {
                return contractService.GetContractByClientCode(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsCode(string code, string id)
        {
            try
            {
                return contractService.CheckIsCode(code, id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
