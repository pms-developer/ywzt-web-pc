﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 同步合同增值服务信息
    /// </summary>
    public class ContractDetailValueAddedServicesEntity
    {
        public string ID { get; set; }
        public string ItemID{get;set;}
        public string ItemName { get; set; }
        public string ItemDetailName { get; set; }
        public decimal? Price { get; set; }
        public decimal? ContractPrice { get; set; }
        public decimal? Discount { get; set; }
        public string Unit { get; set; }
        public int? OperationPoint { get; set; }
        public int? PriceSheetType { get; set; }
    }
}
