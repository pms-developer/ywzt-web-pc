﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-13 16:32
    /// 描 述：合同管理
    /// </summary>
    public class MeioErpContractEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 合同编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 客户编码
        /// </summary>
        [Column("CLIENTCODE")]
        public string ClientCode { get; set; }
        /// <summary>
        /// 客户名称
        /// </summary>
        [Column("CLIENTNAME")]
        public string ClientName { get; set; }
        /// <summary>
        /// 法人
        /// </summary>
        [Column("LEGALPERSON")]
        public string LegalPerson { get; set; }
        /// <summary>
        /// 客户负责人
        /// </summary>
        [Column("CLIENTPERSONCHARGE")]
        public string ClientPersonCharge { get; set; }
        /// <summary>
        /// 负责人电话
        /// </summary>
        [Column("CLIENTPERSONCHARGEPHONE")]
        public string ClientPersonChargePhone { get; set; }
        /// <summary>
        /// 客户地址
        /// </summary>
        [Column("CLIENTADDRESS")]
        public string ClientAddress { get; set; }
        /// <summary>
        /// 报价类型：1（标准报价）2（折扣报价）3（自定义报价）
        /// </summary>
        [Column("PRICESHEETTYPE")]
        public int? PriceSheetType { get; set; }
        /// <summary>
        /// 大货报价单
        /// </summary>
        [Column("PRICESHEETCODE")]
        public string PriceSheetCode { get; set; }
        /// <summary>
        /// 小包报价单
        /// </summary>
        [Column("PRICESHEETCODEXIAOBAO")]
        public string PriceSheetCodeXiaoBao { get; set; }
        /// <summary>
        /// 计费类型：1（计件）2（包干）
        /// </summary>
        [Column("BILLINGTYPE")]
        public int? BillingType { get; set; }
        /// <summary>
        /// 签约人
        /// </summary>
        [Column("CONTRACTINGPARTY")]
        public string ContractingParty { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        [Column("CREATEDBY")]
        public string CreatedBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// 更新用户
        /// </summary>
        [Column("MODIFYBY")]
        public string ModifyBy { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// 发票抬头
        /// </summary>
        [Column("INVOICETITLE")]
        public string InvoiceTitle { get; set; }
        /// <summary>
        /// 税务登记号
        /// </summary>
        [Column("TAXREGISTRATIONNUMBER")]
        public string TaxRegistrationNumber { get; set; }
        /// <summary>
        /// 开户银行
        /// </summary>
        [Column("BANKNAME")]
        public string BankName { get; set; }
        /// <summary>
        /// 银行账号
        /// </summary>
        [Column("BANKACCOUNT")]
        public string BankAccount { get; set; }
        /// <summary>
        /// 开户行地址
        /// </summary>
        [Column("BANKADDRESS")]
        public string BankAddress { get; set; }
        /// <summary>
        /// 折扣
        /// </summary>
        [Column("DISCOUNT")]
        public decimal? Discount { get; set; }
        /// <summary>
        /// 包干/单价
        /// </summary>
        [Column("PACKAGEPRICE")]
        public decimal? PackagePrice { get; set; }
        /// <summary>
        /// 合同状态
        /// </summary>
        [Column("STATUS")]
        public int? Status { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public int? AuditStatus { get; set; }
        /// <summary>
        /// 服务起始日期
        /// </summary>
        [Column("SERVICESTARTDATE")]
        public DateTime? ServiceStartDate { get; set; }
        /// <summary>
        /// 服务截止日期
        /// </summary>
        [Column("SERVICEENDDATE")]
        public DateTime? ServiceEndDate { get; set; }
        /// <summary>
        /// 是否收仓储费
        /// </summary>
        [Column("ISWAREHOUSERENT")]
        public bool? IsWarehouseRent { get; set; }
        /// <summary>
        /// 快递账单计费方式
        /// </summary>
        [Column("LOGISTICSPACKAGEACCOUNTBILLINGTYPE")]
        public int? LogisticsPackageAccountBillingType { get; set; }
        /// <summary>
        /// 报价单ID，多个用英文逗号分隔
        /// </summary>
        [Column("PRICESHEETID")]
        public string PriceSheetID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreatedDate= DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModifyDate= DateTime.Now;
            this.ModifyBy= LoginUserInfo.UserInfo.realName;
        }
        #endregion
        #region  扩展字段
        /// <summary>
        /// 签约人
        /// </summary>
        [NotMapped]
        public string ContractingPartyName { get; set; }
        #endregion
    }
}

