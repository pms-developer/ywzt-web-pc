﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-13 16:32
    /// 描 述：合同管理
    /// </summary>
    public class MeioErpContractRecordEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 合同ID
        /// </summary>
        [Column("MEIOERPCONTRACTID")]
        public string MeioErpContractID { get; set; }
        /// <summary>
        /// 合同编号
        /// </summary>
        [Column("CONTRACTCODE")]
        public string ContractCode { get; set; }
        /// <summary>
        /// 服务起始日期
        /// </summary>
        [Column("SERVICESTARTDATE")]
        public DateTime? ServiceStartDate { get; set; }
        /// <summary>
        /// 服务结束日期
        /// </summary>
        [Column("SERVICEENDDATE")]
        public DateTime? ServiceEndDate { get; set; }
        /// <summary>
        /// 合同状态
        /// </summary>
        [Column("CONTRACTSTATUS")]
        public int? ContractStatus { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        [Column("CREATEDBY")]
        public string CreatedBy { get; set; }
        /// <summary>
        /// CreatedDate
        /// </summary>
        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

