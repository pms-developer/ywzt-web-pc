﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using System;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-13 16:32
    /// 描 述：合同管理
    /// </summary>
    public interface ContractIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpContractEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpContractDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpContractDetailEntity> GetMeioErpContractDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpContract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpContractEntity GetMeioErpContractEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpContractDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpContractDetailEntity GetMeioErpContractDetailEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpContractRecord表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpContractRecordEntity GetMeioErpContractRecordEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpContractEntity entity,List<MeioErpContractDetailEntity> meioErpContractDetailList,MeioErpContractRecordEntity meioErpContractRecordEntity,string deleteList,string type);
        #endregion
        void ContractProcessing(string keyValue, int status, DateTime serviceStartDate, DateTime serviceEndDate);
        IEnumerable<MeioErpContractRecordEntity> GetMeioErpContractRecordList(string keyValue);
        void AuditContract(string keyValue, MeioErpDocumentAuditRecordEntity meioErpDocumentAuditRecordEntity);
        IEnumerable<MeioErpDocumentAuditRecordEntity> GetDocumentAuditRecordEntityList(string keyValue);
        bool CheckIsClientCode(string code, string id);
        bool IsEditForm(string priceSheetId);
        bool CheckIsClientContract(string clientCode);
        MeioErpContractEntity GetContractByClientCode(string code);
        bool CheckIsCode(string code, string id);
    }
}
