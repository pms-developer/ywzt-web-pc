﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    public class MeioErpDocumentAuditRecordEntity : BaseEntity
    {
        [Column("ID")]
        public string ID { get; set; }

        [Column("DocumentType")]
        public string DocumentType { get; set; }
        [Column("DocumentID")]
        public string DocumentID { get; set; }
        [Column("AuditResult")]
        public int? AuditResult { get; set; }
        [Column("AuditDate")]
        public DateTime? AuditDate { get; set; }
        [Column("Auditor")]
        public string Auditor { get; set; }
        [Column("Remark")]
        public string Remark { get; set; }

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.AuditDate = DateTime.Now;
            this.Auditor = LoginUserInfo.UserInfo.realName;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.AuditDate = DateTime.Now;
            this.Auditor= LoginUserInfo.UserInfo.realName;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}
