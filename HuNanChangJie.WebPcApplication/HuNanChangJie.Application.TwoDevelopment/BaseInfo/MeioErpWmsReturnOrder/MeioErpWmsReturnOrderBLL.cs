﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-06 18:52
    /// 描 述：WMS退货单信息
    /// </summary>
    public class MeioErpWmsReturnOrderBLL : MeioErpWmsReturnOrderIBLL
    {
        private MeioErpWmsReturnOrderService meioErpWmsReturnOrderService = new MeioErpWmsReturnOrderService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsReturnOrderEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpWmsReturnOrderService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsReturnOrderDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsReturnOrderDetailEntity> GetMeioErpWmsReturnOrderDetailList(string keyValue)
        {
            try
            {
                return meioErpWmsReturnOrderService.GetMeioErpWmsReturnOrderDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsReturnOrder表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsReturnOrderEntity GetMeioErpWmsReturnOrderEntity(string keyValue)
        {
            try
            {
                return meioErpWmsReturnOrderService.GetMeioErpWmsReturnOrderEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsReturnOrderDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsReturnOrderDetailEntity GetMeioErpWmsReturnOrderDetailEntity(string keyValue)
        {
            try
            {
                return meioErpWmsReturnOrderService.GetMeioErpWmsReturnOrderDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpWmsReturnOrderService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsReturnOrderEntity entity,List<MeioErpWmsReturnOrderDetailEntity> meioErpWmsReturnOrderDetailList,string deleteList,string type)
        {
            try
            {
                meioErpWmsReturnOrderService.SaveEntity(keyValue, entity,meioErpWmsReturnOrderDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
