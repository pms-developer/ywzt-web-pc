﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-06 18:52
    /// 描 述：WMS退货单信息
    /// </summary>
    public class MeioErpWmsReturnOrderService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsReturnOrderEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.OrderCode,
                t.WarehouseID,
                t.OwnerID,
                t.Remark
                ");
                strSql.Append("  FROM MeioErpWmsReturnOrder t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["OrderCode"].IsEmpty())
                {
                    dp.Add("OrderCode", "%" + queryParam["OrderCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OrderCode Like @OrderCode ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID",queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID",queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                var list=this.BaseRepository().FindList<MeioErpWmsReturnOrderEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsReturnOrderDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsReturnOrderDetailEntity> GetMeioErpWmsReturnOrderDetailList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<MeioErpWmsReturnOrderDetailEntity>(t=>t.WmsReturnOrderID == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsReturnOrder表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsReturnOrderEntity GetMeioErpWmsReturnOrderEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsReturnOrderEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsReturnOrderDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsReturnOrderDetailEntity GetMeioErpWmsReturnOrderDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsReturnOrderDetailEntity>(t=>t.WmsReturnOrderID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpWmsReturnOrderEntity = GetMeioErpWmsReturnOrderEntity(keyValue); 
                db.Delete<MeioErpWmsReturnOrderEntity>(t=>t.ID == keyValue);
                db.Delete<MeioErpWmsReturnOrderDetailEntity>(t=>t.WmsReturnOrderID == meioErpWmsReturnOrderEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsReturnOrderEntity entity,List<MeioErpWmsReturnOrderDetailEntity> meioErpWmsReturnOrderDetailList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var meioErpWmsReturnOrderEntityTmp = GetMeioErpWmsReturnOrderEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    db.Delete<MeioErpWmsReturnOrderDetailEntity>(p => p.WmsReturnOrderID == keyValue);
                    foreach (var item in meioErpWmsReturnOrderDetailList)
                    {
                        item.Create();
                        item.WmsReturnOrderID = keyValue;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                    foreach (MeioErpWmsReturnOrderDetailEntity item in meioErpWmsReturnOrderDetailList)
                    {
                        item.Create();
                        item.WmsReturnOrderID = entity.ID;
                        db.Insert(item);
                    }
                }
                if (type == "add")
                {
                    #region 小包取消发货,生成空出库账单
                    MeioErpDeliveryAccountEntity meioErpDeliveryAccountEntity = new MeioErpDeliveryAccountEntity
                    {
                        WarehouseID = entity.WarehouseID,
                        OwnerID = entity.OwnerID,
                        State = 0,
                        DeliveryNo = entity.OrderCode,
                        OrderType = "小包发货",
                        AccountDate = DateTime.Now,
                        TotalAmount = 0,
                        IsPushOMS = false
                    };
                    meioErpDeliveryAccountEntity.Create();
                    foreach (var returnOrderDetail in meioErpWmsReturnOrderDetailList)
                    {
                        MeioErpDeliveryAccountDetailEntity deliveryAccountDetailEntity = new MeioErpDeliveryAccountDetailEntity
                        {
                            DeliveryAccountID = meioErpDeliveryAccountEntity.ID,
                            GoodsCode = returnOrderDetail.ItemCode,
                            GoodsName = returnOrderDetail.ItemName,
                            //QualityState = deliveryDetail.Quality,
                            Quantity = returnOrderDetail.Quantity,
                            Amount = 0,
                        };
                        deliveryAccountDetailEntity.Create();
                        db.Insert(deliveryAccountDetailEntity);
                    }

                    db.Insert(meioErpDeliveryAccountEntity);
                    #endregion
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
