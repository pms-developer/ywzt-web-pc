﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-06 18:52
    /// 描 述：WMS退货单信息
    /// </summary>
    public interface MeioErpWmsReturnOrderIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpWmsReturnOrderEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpWmsReturnOrderDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpWmsReturnOrderDetailEntity> GetMeioErpWmsReturnOrderDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpWmsReturnOrder表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsReturnOrderEntity GetMeioErpWmsReturnOrderEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpWmsReturnOrderDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsReturnOrderDetailEntity GetMeioErpWmsReturnOrderDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpWmsReturnOrderEntity entity,List<MeioErpWmsReturnOrderDetailEntity> meioErpWmsReturnOrderDetailList,string deleteList,string type);
        #endregion

    }
}
