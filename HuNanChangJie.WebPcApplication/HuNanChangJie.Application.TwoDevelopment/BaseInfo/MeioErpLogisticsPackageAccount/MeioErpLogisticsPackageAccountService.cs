﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
using System.Threading;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:01
    /// 描 述：快递打包账单
    /// </summary>
    public class MeioErpLogisticsPackageAccountService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpLogisticsPackageAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.AccountDate,
                t.OwnerID,
                t.State,
                t.TotalAmount,
                t.OrderCount,
                t.Enabled,
                t.StatementNo,
                t.IsPushOMS,
                t1.Code OwnerCode,
                t1.Name OwnerName,
                t.ModificationName,
                t.ModificationDate,
                ISNULL(t2.AmountSubtotal,0) AmountSubtotal,
                SUM(t3.ExpressFee) ExpressFee,
(select  OrderNo+','  from MeioErpLogisticsPackageAccountDetail a 

   where a.LogisticsPackageAccountID=t.ID  FOR XML PATH('')) as OrderNo
                ");
                strSql.Append("  FROM MeioErpLogisticsPackageAccount t ");
                strSql.Append(" LEFT JOIN MeioErpOwner t1 ON t1.ID=t.OwnerID ");
                strSql.Append(" LEFT JOIN MeioErpAccountAdditionalCost t2 ON t.ID=t2.AccountID AND t2.AccountType=2 ");
                strSql.Append(" LEFT JOIN MeioErpLogisticsPackageAccountDetail t3 ON t.ID=t3.LogisticsPackageAccountID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["State"].IsEmpty())
                {
                    dp.Add("State", queryParam["State"].ToString(), DbType.String);
                    strSql.Append(" AND t.State = @State ");
                }
                //if (!queryParam["WarehouseID"].IsEmpty())
                //{
                //    dp.Add("WarehouseID", queryParam["WarehouseID"].ToString(), DbType.String);
                //    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                //}
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID", queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                if (!queryParam["IsPushOMS"].IsEmpty())
                {
                    dp.Add("IsPushOMS", queryParam["IsPushOMS"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsPushOMS = @IsPushOMS ");
                }
                strSql.Append(" GROUP BY t.ID,t.AccountDate,t.OwnerID,t.State,t.TotalAmount,t.OrderCount,t.Enabled,t.StatementNo,t.IsPushOMS,t1.Code,t1.Name,t.ModificationName,t.ModificationDate,t2.AmountSubtotal ");
                strSql.Append(" ORDER BY t.AccountDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpLogisticsPackageAccountEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpLogisticsPackageAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpLogisticsPackageAccountDetailEntity> GetMeioErpLogisticsPackageAccountDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpLogisticsPackageAccountDetailEntity>(t => t.LogisticsPackageAccountID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpLogisticsPackageAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpLogisticsPackageAccountEntity GetMeioErpLogisticsPackageAccountEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpLogisticsPackageAccountEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpLogisticsPackageAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpLogisticsPackageAccountDetailEntity GetMeioErpLogisticsPackageAccountDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpLogisticsPackageAccountDetailEntity>(t => t.LogisticsPackageAccountID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpLogisticsPackageAccountEntity = GetMeioErpLogisticsPackageAccountEntity(keyValue);
                db.Delete<MeioErpLogisticsPackageAccountEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpLogisticsPackageAccountDetailEntity>(t => t.LogisticsPackageAccountID == meioErpLogisticsPackageAccountEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpLogisticsPackageAccountEntity entity, List<MeioErpLogisticsPackageAccountDetailEntity> meioErpLogisticsPackageAccountDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioErpLogisticsPackageAccountEntityTmp = GetMeioErpLogisticsPackageAccountEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var MeioErpLogisticsPackageAccountDetailUpdateList = meioErpLogisticsPackageAccountDetailList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in MeioErpLogisticsPackageAccountDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var MeioErpLogisticsPackageAccountDetailInserList = meioErpLogisticsPackageAccountDetailList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in MeioErpLogisticsPackageAccountDetailInserList)
                    {
                        item.Create(item.ID);
                        item.LogisticsPackageAccountID = meioErpLogisticsPackageAccountEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (MeioErpLogisticsPackageAccountDetailEntity item in meioErpLogisticsPackageAccountDetailList)
                    {
                        item.LogisticsPackageAccountID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 计算快递账单
        /// </summary>
        public void ComputeLogisticsPackageAccount(List<string> ids,int logisticsPackageAccountBillingType)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                IEnumerable<MeioErpWmsXiaoBaoDeliveryEntity> wmsXiaoBaoDeliveryList = null;
                if (ids == null)
                {
                    wmsXiaoBaoDeliveryList = this.BaseRepository().FindList<MeioErpWmsXiaoBaoDeliveryEntity>(p =>p.LogisticsPackageAccountID == null || p.LogisticsPackageAccountID == "");
                }
                else
                {
                    wmsXiaoBaoDeliveryList = this.BaseRepository().FindList<MeioErpWmsXiaoBaoDeliveryEntity>(p => ids.Contains(p.ID) && (p.LogisticsPackageAccountID == null || p.LogisticsPackageAccountID == ""));
                }
                if (wmsXiaoBaoDeliveryList != null)
                {
                    if (wmsXiaoBaoDeliveryList.Count() > 0)
                    {
                        var wmsXiaoBaoDeliveryListQuery = wmsXiaoBaoDeliveryList.GroupBy(g => g.OwnerID).Select(o => new { OwnerID = o.Key, OrderCount = o.Count() });
                        var ownerIds = wmsXiaoBaoDeliveryListQuery.Select(o => o.OwnerID);
                        var owners = this.BaseRepository().FindList<MeioErpOwnerEntity>(p => ownerIds.Contains(p.ID));
                        var nowDate = DateTime.Now.Date;

                        foreach (var item in wmsXiaoBaoDeliveryListQuery)
                        {
                            var ownerEntity = owners.FirstOrDefault(p => p.ID == item.OwnerID);
                            if (ownerEntity != null)
                            {
                                var contractEntity = this.BaseRepository().FindEntity<MeioErpContractEntity>(p => p.ID == ownerEntity.ContractID && p.ServiceStartDate <= nowDate && p.ServiceEndDate >= nowDate && p.LogisticsPackageAccountBillingType==logisticsPackageAccountBillingType);
                                if (contractEntity != null)
                                {
                                    var contractDetailEntity = this.BaseRepository().FindList<MeioErpContractDetailEntity>(p => p.ContractID == ownerEntity.ContractID && p.ItemType == 2 && p.ItemDetailMinValue <= item.OrderCount && p.ItemDetailMaxValue >= item.OrderCount).FirstOrDefault();

                                    decimal unitPrice = 0;
                                    if (contractDetailEntity != null)
                                    {
                                        if (contractDetailEntity.ContractPrice.HasValue)
                                        {
                                            if (contractDetailEntity.ContractPrice.Value > 0)
                                            {
                                                unitPrice = contractDetailEntity.ContractPrice.Value;
                                            }
                                            else
                                            {
                                                unitPrice = contractDetailEntity.Price.Value;
                                            }
                                        }
                                        else
                                        {
                                            unitPrice = contractDetailEntity.Price.Value;
                                        }
                                    }

                                    MeioErpLogisticsPackageAccountEntity meioErpLogisticsPackageAccountEntity = new MeioErpLogisticsPackageAccountEntity
                                    {
                                        OwnerID = item.OwnerID,
                                        AccountDate = DateTime.Now,
                                        State = 0,
                                        OrderCount = item.OrderCount,
                                        TotalAmount = item.OrderCount * unitPrice,
                                        IsPushOMS = false
                                    };
                                    meioErpLogisticsPackageAccountEntity.IsJobRun = true;
                                    meioErpLogisticsPackageAccountEntity.Create();
                                    db.Insert(meioErpLogisticsPackageAccountEntity);

                                    foreach (var itemDetail in wmsXiaoBaoDeliveryList.Where(p => p.OwnerID == item.OwnerID))
                                    {
                                        MeioErpLogisticsPackageAccountDetailEntity meioErpLogisticsPackageAccountDetailEntity = new MeioErpLogisticsPackageAccountDetailEntity
                                        {
                                            LogisticsPackageAccountID = meioErpLogisticsPackageAccountEntity.ID,
                                            OrderNo = itemDetail.OrderCode,
                                            DeliveryType = itemDetail.DeliveryType,
                                            WarehouseID = itemDetail.WarehouseID,
                                            Carrier = itemDetail.CarrierID,
                                            WaybillNo = itemDetail.WaybillNo,
                                            DeliveryTime = itemDetail.DeliveryTime,
                                            IsExpressFee = itemDetail.IsExpressFee,
                                        };
                                        meioErpLogisticsPackageAccountDetailEntity.IsJobRun = true;
                                        meioErpLogisticsPackageAccountDetailEntity.Create();
                                        db.Insert(meioErpLogisticsPackageAccountDetailEntity);

                                        itemDetail.LogisticsPackageAccountID = meioErpLogisticsPackageAccountEntity.ID;
                                        db.Update(itemDetail);
                                    }
                                }
                            }
                        }
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 推送快递账单至OMS
        /// </summary>
        public bool PushLogisticsPackageAccount(List<string> keyValue, bool IsJobRun = false)
        {
            var result = false;
            try
            {
                if (keyValue != null)
                {
                    if (keyValue.Count() > 0)
                    {
                        var logisticsPackageAccountList = this.BaseRepository().FindList<MeioErpLogisticsPackageAccountEntity>(t => keyValue.Contains(t.ID));
                        var logisticsPackageAccountDetailList = this.BaseRepository().FindList<MeioErpLogisticsPackageAccountDetailEntity>(t => keyValue.Contains(t.LogisticsPackageAccountID));

                        var ownerIds = logisticsPackageAccountList.Select(o => o.OwnerID);
                        var ownerList = this.BaseRepository().FindList<MeioErpOwnerEntity>(p => ownerIds.Contains(p.ID));
                        var accountAdditionalCostList = this.BaseRepository().FindList<MeioErpAccountAdditionalCostEntity>(p => p.AccountType == 1 && keyValue.Contains(p.AccountID));

                        foreach (var logisticsPackageAccount in logisticsPackageAccountList)
                        {
                            var ownerEntity = ownerList.FirstOrDefault(p => p.ID == logisticsPackageAccount.OwnerID);
                            var accountAdditionalCostEntity = accountAdditionalCostList.FirstOrDefault(p => p.AccountID == logisticsPackageAccount.ID);
                            if (ownerEntity != null)
                            {
                                if ((IsJobRun && ownerEntity.PushAccountType == 1) || (!IsJobRun && ownerEntity.PushAccountType == 2))
                                {
                                    LogisticsPackageAccountRequest logisticsPackageAccountRequest = new LogisticsPackageAccountRequest
                                    {
                                        billDate = logisticsPackageAccount.CreationDate.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                                        shipperId = logisticsPackageAccount.OwnerID,
                                        //relationNo = logisticsPackageAccount.,
                                        totalFee = logisticsPackageAccount.TotalAmount,
                                        orderQty = logisticsPackageAccount.OrderCount,
                                        dtls = new List<LogisticsPackageAccountDetailRequest>()
                                    };
                                    //附加费
                                    if (accountAdditionalCostEntity != null)
                                    {
                                        logisticsPackageAccountRequest.surchargeQty = accountAdditionalCostEntity.Quantity;
                                        logisticsPackageAccountRequest.surchargeSum = accountAdditionalCostEntity.AmountSubtotal;
                                        logisticsPackageAccountRequest.surchargeFeeDetail = accountAdditionalCostEntity.ExpenseDetail;
                                    }
                                    foreach (var logisticsPackageAccountDetail in logisticsPackageAccountDetailList.Where(p => p.LogisticsPackageAccountID == logisticsPackageAccount.ID))
                                    {
                                        LogisticsPackageAccountDetailRequest detail = new LogisticsPackageAccountDetailRequest
                                        {
                                            relationNo = logisticsPackageAccountDetail.OrderNo,
                                            orderType = logisticsPackageAccountDetail.DeliveryType,
                                            warehouseId = logisticsPackageAccountDetail.WarehouseID,
                                            carrier = logisticsPackageAccountDetail.Carrier,
                                            trackingNumber = logisticsPackageAccountDetail.WaybillNo,
                                            deliveryDate = logisticsPackageAccountDetail.DeliveryTime.Value.ToString("yyyy-MM-dd HH:mm:ss")
                                        };
                                        logisticsPackageAccountRequest.dtls.Add(detail);

                                    }
                                    //Task.Run(() =>
                                    //{
                                    var tenant = ownerList.FirstOrDefault(p => p.ID == logisticsPackageAccount.OwnerID)?.Code;
                                    OmsService.PushLogisticsPackageAccount(logisticsPackageAccountRequest, tenant);
                                    //});
                                    logisticsPackageAccount.IsJobRun = IsJobRun;
                                    logisticsPackageAccount.IsPushOMS = true;
                                    logisticsPackageAccount.Modify(logisticsPackageAccount.ID);
                                    this.BaseRepository().Update(logisticsPackageAccount);
                                }
                            }
                        }

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 推送所有未推送的快递账单
        /// </summary>
        /// <returns></returns>
        public bool PushLogisticsPackageAccount(bool IsJobRun = false)
        {
            var result = false;
            try
            {
                //获取未推送OMS的仓库租金账单
                var accountList = this.BaseRepository().FindList<MeioErpLogisticsPackageAccountEntity>(p => p.IsPushOMS == false);
                if (accountList != null)
                {
                    if (accountList.Count() > 0)
                    {
                        var ids = accountList.Select(o => o.ID).ToList();
                        result = PushLogisticsPackageAccount(ids, IsJobRun);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 同步快递费
        /// </summary>
        public void SyncExpressFee()
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var LogisticsPackageAccountDetailList = this.BaseRepository().FindList<MeioErpLogisticsPackageAccountDetailEntity>(p => p.ExpressFee == null && p.IsExpressFee.Value);
                if (LogisticsPackageAccountDetailList.Count() > 0)
                {
                    var accountId = LogisticsPackageAccountDetailList.Select(o => o.LogisticsPackageAccountID);
                    var LogisticsPackageAccountList = this.BaseRepository().FindList<MeioErpLogisticsPackageAccountEntity>(p => accountId.Contains(p.ID));
                    var CarrierList = this.BaseRepository().FindList<MeioERPGeneralCarrierEntity>();
                    Dictionary<string, decimal> addExpressFee = new Dictionary<string, decimal>();
                    foreach (var item in LogisticsPackageAccountDetailList)
                    {
                        Thread.Sleep(10000);//延迟10秒
                        var carrierEntity = CarrierList.FirstOrDefault(p => p.ID == item.Carrier);
                        if (carrierEntity != null)
                        {
                            if (carrierEntity.Code == "YTWL")
                            {
                                if (!string.IsNullOrEmpty(carrierEntity.InterfaceServiceAddress) && !string.IsNullOrEmpty(carrierEntity.InterfaceUserInfo)
                                    && !string.IsNullOrEmpty(carrierEntity.InterfaceAccount) && !string.IsNullOrEmpty(carrierEntity.InterfaceKey))
                                {
                                    YunExpressService.strUrl = carrierEntity.InterfaceServiceAddress;
                                    YunExpressService.appId = carrierEntity.InterfaceAccount;
                                    YunExpressService.appSecret = carrierEntity.InterfaceKey;
                                    YunExpressService.sourceKey = carrierEntity.InterfaceUserInfo;

                                    var feeDetails = YunExpressService.getFeeDetails(item.WaybillNo);
                                    if (feeDetails.success.Value)
                                    {
                                        item.ExpressFee = feeDetails.result.fee_details.Sum(s => s.deduction_amount);
                                        if (carrierEntity.EffectiveTime != null)
                                        {
                                            if (carrierEntity.EffectiveTime.Value <= DateTime.Now)
                                            {
                                                item.ServiceCharge = item.ExpressFee * carrierEntity.Price / 100;
                                            }
                                        }
                                        db.Update(item);
                                        if (!addExpressFee.ContainsKey(item.LogisticsPackageAccountID))
                                        {
                                            addExpressFee.Add(item.LogisticsPackageAccountID, item.ExpressFee.Value + (item.ServiceCharge.HasValue ? item.ServiceCharge.Value : 0));
                                        }
                                        else
                                        {
                                            addExpressFee[item.LogisticsPackageAccountID] = addExpressFee[item.LogisticsPackageAccountID] + item.ExpressFee.Value + (item.ServiceCharge.HasValue ? item.ServiceCharge.Value : 0);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach (var item in addExpressFee)
                    {
                        var LogisticsPackageAccount = LogisticsPackageAccountList.FirstOrDefault(p => p.ID == item.Key);
                        if (LogisticsPackageAccount != null)
                        {
                            LogisticsPackageAccount.TotalAmount = LogisticsPackageAccount.TotalAmount + item.Value;
                            db.Update(LogisticsPackageAccount);
                        }
                    }
                    db.Commit();
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
