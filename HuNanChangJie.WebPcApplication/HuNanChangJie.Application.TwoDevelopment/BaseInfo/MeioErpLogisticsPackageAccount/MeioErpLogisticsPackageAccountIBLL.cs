﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:01
    /// 描 述：快递打包账单
    /// </summary>
    public interface MeioErpLogisticsPackageAccountIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpLogisticsPackageAccountEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpLogisticsPackageAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpLogisticsPackageAccountDetailEntity> GetMeioErpLogisticsPackageAccountDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpLogisticsPackageAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpLogisticsPackageAccountEntity GetMeioErpLogisticsPackageAccountEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpLogisticsPackageAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpLogisticsPackageAccountDetailEntity GetMeioErpLogisticsPackageAccountDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpLogisticsPackageAccountEntity entity,List<MeioErpLogisticsPackageAccountDetailEntity> meioErpLogisticsPackageAccountDetailList,string deleteList,string type);
        #endregion
        /// <summary>
        /// 计算快递账单
        /// </summary>
        /// <param name="ids"></param>
        void ComputeLogisticsPackageAccount(List<string> ids, int logisticsPackageAccountBillingType);
        bool PushLogisticsPackageAccount(List<string> keyValue);
        bool PushLogisticsPackageAccount(bool IsJobRun);
        void SyncExpressFee();
    }
}
