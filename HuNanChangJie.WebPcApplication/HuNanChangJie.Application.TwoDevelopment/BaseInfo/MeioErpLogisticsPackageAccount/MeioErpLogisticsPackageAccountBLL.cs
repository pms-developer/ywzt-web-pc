﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.WebRequestService;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:01
    /// 描 述：快递打包账单
    /// </summary>
    public class MeioErpLogisticsPackageAccountBLL : MeioErpLogisticsPackageAccountIBLL
    {
        private MeioErpLogisticsPackageAccountService meioErpLogisticsPackageAccountService = new MeioErpLogisticsPackageAccountService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpLogisticsPackageAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpLogisticsPackageAccountService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpLogisticsPackageAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpLogisticsPackageAccountDetailEntity> GetMeioErpLogisticsPackageAccountDetailList(string keyValue)
        {
            try
            {
                return meioErpLogisticsPackageAccountService.GetMeioErpLogisticsPackageAccountDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpLogisticsPackageAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpLogisticsPackageAccountEntity GetMeioErpLogisticsPackageAccountEntity(string keyValue)
        {
            try
            {
                return meioErpLogisticsPackageAccountService.GetMeioErpLogisticsPackageAccountEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpLogisticsPackageAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpLogisticsPackageAccountDetailEntity GetMeioErpLogisticsPackageAccountDetailEntity(string keyValue)
        {
            try
            {
                return meioErpLogisticsPackageAccountService.GetMeioErpLogisticsPackageAccountDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpLogisticsPackageAccountService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpLogisticsPackageAccountEntity entity, List<MeioErpLogisticsPackageAccountDetailEntity> meioErpLogisticsPackageAccountDetailList, string deleteList, string type)
        {
            try
            {
                meioErpLogisticsPackageAccountService.SaveEntity(keyValue, entity, meioErpLogisticsPackageAccountDetailList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 计算快递账单
        /// </summary>
        public void ComputeLogisticsPackageAccount(List<string> ids,int logisticsPackageAccountBillingType)
        {
            try
            {
                meioErpLogisticsPackageAccountService.ComputeLogisticsPackageAccount(ids,logisticsPackageAccountBillingType);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 推送快递账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public bool PushLogisticsPackageAccount(List<string> keyValue)
        {
            try
            {
                return meioErpLogisticsPackageAccountService.PushLogisticsPackageAccount(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 推送所有未推送的快递账单至OMS
        /// </summary>
        /// <returns></returns>
        public bool PushLogisticsPackageAccount(bool IsJobRun = false)
        {
            try
            {
                return meioErpLogisticsPackageAccountService.PushLogisticsPackageAccount(IsJobRun);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 同步快递费
        /// </summary>
        public void SyncExpressFee()
        {
            try
            {
                meioErpLogisticsPackageAccountService.SyncExpressFee();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
