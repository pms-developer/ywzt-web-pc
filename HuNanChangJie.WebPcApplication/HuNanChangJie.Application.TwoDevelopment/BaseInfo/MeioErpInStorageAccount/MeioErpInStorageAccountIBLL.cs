﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:10
    /// 描 述：入库账单信息
    /// </summary>
    public interface MeioErpInStorageAccountIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpInStorageAccountEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpInStorageAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpInStorageAccountDetailEntity> GetMeioErpInStorageAccountDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpInStorageAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpInStorageAccountEntity GetMeioErpInStorageAccountEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpInStorageAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpInStorageAccountDetailEntity GetMeioErpInStorageAccountDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpInStorageAccountEntity entity,List<MeioErpInStorageAccountDetailEntity> meioErpInStorageAccountDetailList,string deleteList,string type);
        #endregion
        bool PushInStorageAccount(List<string> keyValue);
        bool PushInStorageAccount(bool IsJobRun);
    }
}
