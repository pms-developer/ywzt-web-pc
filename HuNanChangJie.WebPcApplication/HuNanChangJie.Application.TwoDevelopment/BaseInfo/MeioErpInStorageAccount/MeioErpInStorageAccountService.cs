﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
using System.Threading.Tasks;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:10
    /// 描 述：入库账单信息
    /// </summary>
    public class MeioErpInStorageAccountService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpInStorageAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.InStorageNo,
                t.OrderType,
                t.AccountDate,
                t.WarehouseID,
                t.OwnerID,
                t.TotalAmount,
                t.State,
                t.IsPushOMS,
                t.ModificationName,
                t.ModificationDate,
                t1.AmountSubtotal
                ");
                strSql.Append("  FROM MeioErpInStorageAccount t ");
                strSql.Append(" LEFT JOIN MeioErpAccountAdditionalCost t1 ON t.ID=t1.AccountID AND t1.AccountType=3 ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["State"].IsEmpty())
                {
                    dp.Add("State", queryParam["State"].ToString(), DbType.String);
                    strSql.Append(" AND t.State = @State ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID", queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID", queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                if (!queryParam["IsPushOMS"].IsEmpty())
                {
                    dp.Add("IsPushOMS", queryParam["IsPushOMS"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsPushOMS = @IsPushOMS ");
                }
                if (!queryParam["InStorageNo"].IsEmpty())
                {
                    dp.Add("InStorageNo", queryParam["InStorageNo"].ToString(), DbType.String);
                    strSql.Append(" AND t.InStorageNo = @InStorageNo ");
                }
                strSql.Append(" ORDER BY t.AccountDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpInStorageAccountEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpInStorageAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpInStorageAccountDetailEntity> GetMeioErpInStorageAccountDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpInStorageAccountDetailEntity>(t => t.InStorageAccountID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpInStorageAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpInStorageAccountEntity GetMeioErpInStorageAccountEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpInStorageAccountEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpInStorageAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpInStorageAccountDetailEntity GetMeioErpInStorageAccountDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpInStorageAccountDetailEntity>(t => t.InStorageAccountID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpInStorageAccountEntity = GetMeioErpInStorageAccountEntity(keyValue);
                db.Delete<MeioErpInStorageAccountEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpInStorageAccountDetailEntity>(t => t.InStorageAccountID == meioErpInStorageAccountEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpInStorageAccountEntity entity, List<MeioErpInStorageAccountDetailEntity> meioErpInStorageAccountDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioErpInStorageAccountEntityTmp = GetMeioErpInStorageAccountEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var MeioErpInStorageAccountDetailUpdateList = meioErpInStorageAccountDetailList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in MeioErpInStorageAccountDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var MeioErpInStorageAccountDetailInserList = meioErpInStorageAccountDetailList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in MeioErpInStorageAccountDetailInserList)
                    {
                        item.Create(item.ID);
                        item.InStorageAccountID = meioErpInStorageAccountEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (MeioErpInStorageAccountDetailEntity item in meioErpInStorageAccountDetailList)
                    {
                        item.InStorageAccountID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 推送入库账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public bool PushInStorageAccount(List<string> keyValue, bool IsJobRun=false)
        {
            var result = false;
            try
            {
                if (keyValue != null)
                {
                    if (keyValue.Count() > 0)
                    {
                        var inStorageAccountList = this.BaseRepository().FindList<MeioErpInStorageAccountEntity>(t => keyValue.Contains(t.ID));
                        var inStorageAccountDetailList = this.BaseRepository().FindList<MeioErpInStorageAccountDetailEntity>(t => keyValue.Contains(t.InStorageAccountID));

                        var ownerIds = inStorageAccountList.Select(o => o.OwnerID);
                        var ownerList = this.BaseRepository().FindList<MeioErpOwnerEntity>(p => ownerIds.Contains(p.ID));
                        var accountAdditionalCostList = this.BaseRepository().FindList<MeioErpAccountAdditionalCostEntity>(p => p.AccountType == 3 && keyValue.Contains(p.AccountID));

                        foreach (var inStorageAccount in inStorageAccountList)
                        {
                            var ownerEntity = ownerList.FirstOrDefault(p => p.ID == inStorageAccount.OwnerID);
                            var accountAdditionalCostEntity = accountAdditionalCostList.FirstOrDefault(p => p.AccountID == inStorageAccount.ID);
                            if (ownerEntity != null)
                            {
                                if ((IsJobRun && ownerEntity.PushAccountType == 1) || (!IsJobRun && ownerEntity.PushAccountType == 2))
                                {
                                    InStorageAccountRequest inStorageAccountRequest = new InStorageAccountRequest
                                    {
                                        billDate = inStorageAccount.AccountDate.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                                        warehouseId = inStorageAccount.WarehouseID,
                                        shipperId = inStorageAccount.OwnerID,
                                        relationNo = inStorageAccount.InStorageNo,
                                        stockType = 1,
                                        orderType = inStorageAccount.OrderType,
                                        totalFee = inStorageAccount.TotalAmount,
                                        dtls = new List<InStorageAccountDetailRequest>()
                                    };
                                    //附加费
                                    if (accountAdditionalCostEntity != null)
                                    {
                                        inStorageAccountRequest.surchargeQty = accountAdditionalCostEntity.Quantity;
                                        inStorageAccountRequest.surchargeSum = accountAdditionalCostEntity.AmountSubtotal;
                                        inStorageAccountRequest.surchargeFeeDetail = accountAdditionalCostEntity.ExpenseDetail;
                                    }
                                    foreach (var inStorageAccountDetail in inStorageAccountDetailList.Where(p => p.InStorageAccountID == inStorageAccount.ID))
                                    {
                                        InStorageAccountDetailRequest detail = new InStorageAccountDetailRequest
                                        {
                                            materialid = inStorageAccountDetail.GoodsCode,
                                            materialName = inStorageAccountDetail.GoodsName,
                                            qualityState = inStorageAccountDetail.QualityState,
                                            qty = inStorageAccountDetail.Quantity.ToString(),
                                            feeScale = inStorageAccountDetail.ExpenseDetail,
                                            subtotal = inStorageAccountDetail.Amount
                                        };
                                        inStorageAccountRequest.dtls.Add(detail);
                                    }
                                    //Task.Run(() =>
                                    //{
                                    var tenant = ownerList.FirstOrDefault(p => p.ID == inStorageAccount.OwnerID)?.Code;
                                    OmsService.PushInStorageAccount(inStorageAccountRequest, tenant);
                                    //});
                                    inStorageAccount.IsJobRun = IsJobRun;
                                    inStorageAccount.IsPushOMS = true;
                                    inStorageAccount.Modify(inStorageAccount.ID);
                                    this.BaseRepository().Update(inStorageAccount);
                                }
                            }
                        }

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 推送未推送的入库账单至OMS
        /// </summary>
        /// <returns></returns>
        public bool PushInStorageAccount(bool IsJobRun=false)
        {
            var result = false;
            try
            {
                //获取未推送OMS的入库库账单
                var accountList = this.BaseRepository().FindList<MeioErpInStorageAccountEntity>(p => p.IsPushOMS == false);
                if (accountList != null)
                {
                    if (accountList.Count() > 0)
                    {
                        var ids = accountList.Select(o => o.ID).ToList();
                        result = PushInStorageAccount(ids,IsJobRun);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
