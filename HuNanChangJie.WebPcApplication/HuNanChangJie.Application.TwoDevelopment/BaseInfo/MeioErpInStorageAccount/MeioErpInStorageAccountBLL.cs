﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:10
    /// 描 述：入库账单信息
    /// </summary>
    public class MeioErpInStorageAccountBLL : MeioErpInStorageAccountIBLL
    {
        private MeioErpInStorageAccountService meioErpInStorageAccountService = new MeioErpInStorageAccountService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpInStorageAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpInStorageAccountService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpInStorageAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpInStorageAccountDetailEntity> GetMeioErpInStorageAccountDetailList(string keyValue)
        {
            try
            {
                return meioErpInStorageAccountService.GetMeioErpInStorageAccountDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpInStorageAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpInStorageAccountEntity GetMeioErpInStorageAccountEntity(string keyValue)
        {
            try
            {
                return meioErpInStorageAccountService.GetMeioErpInStorageAccountEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpInStorageAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpInStorageAccountDetailEntity GetMeioErpInStorageAccountDetailEntity(string keyValue)
        {
            try
            {
                return meioErpInStorageAccountService.GetMeioErpInStorageAccountDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpInStorageAccountService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpInStorageAccountEntity entity,List<MeioErpInStorageAccountDetailEntity> meioErpInStorageAccountDetailList,string deleteList,string type)
        {
            try
            {
                meioErpInStorageAccountService.SaveEntity(keyValue, entity,meioErpInStorageAccountDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 推送入库账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public bool PushInStorageAccount(List<string> keyValue)
        {
            try
            {
                return meioErpInStorageAccountService.PushInStorageAccount(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 推送未推送的入库账单至OMS
        /// </summary>
        /// <returns></returns>
        public bool PushInStorageAccount(bool IsJobRun=false)
        {
            try
            {
                return meioErpInStorageAccountService.PushInStorageAccount(IsJobRun);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
