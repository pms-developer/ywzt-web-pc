﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-06-04 14:26
    /// 描 述：发货单
    /// </summary>
    public interface MeioWmsWarehouseDeliveryIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取meioWmsWarehouseDeliveryDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseDeliveryDetailEntity> GetmeioWmsWarehouseDeliveryDetailList(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, meioWmsWarehouseDeliveryEntity entity, List<meioWmsWarehouseDeliveryDetailEntity> meioWmsWarehouseDeliveryDetailList, List<meioWmsWarehouseDeliveryDetailAllocationEntity> meioWmsWarehouseDeliveryDetailAllocationList, List<meioWMSWarehouseDeliveryBoxDetailEntity> deliveryBoxDetailList, List<meioWMSWarehouseDeliveryCostItemDetailEntity> deliveryCostItemDetailList, string deleteList, string type, string userName, string warehouseId);

        #endregion

        IEnumerable<meioWmsWarehouseDeliveryDetailEntity> GetMeioWmsWarehouseDeliveryDetailByCode(string deliveryCode, string productCode, string warehouseId = "");

    }
}
