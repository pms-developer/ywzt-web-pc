﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using Castle.Core;
using System.Threading.Tasks;
using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-06-04 14:26
    /// 描 述：发货单
    /// </summary>
    public class MeioWmsWarehouseDeliveryService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private static readonly object objLock = new object();
        private DataItemService dataItemService = new DataItemService();

        #region  获取数据

        /// <summary>
        /// 获取meioWmsWarehouseDeliveryDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseDeliveryDetailEntity> GetmeioWmsWarehouseDeliveryDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<meioWmsWarehouseDeliveryDetailEntity>(t => t.DeliveryID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public meioWmsWarehouseDeliveryEntity GetmeioWmsWarehouseDeliveryEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseDeliveryEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioWmsWarehouseDeliveryEntity = GetmeioWmsWarehouseDeliveryEntity(keyValue);
                db.Delete<meioWmsWarehouseDeliveryEntity>(t => t.ID == keyValue);
                db.Delete<meioWmsWarehouseDeliveryDetailEntity>(t => t.DeliveryID == meioWmsWarehouseDeliveryEntity.ID);
                db.Delete<meioWmsWarehouseDeliveryDetailAllocationEntity>(t => t.DeliveryID == meioWmsWarehouseDeliveryEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public void SaveEntity(string keyValue, meioWmsWarehouseDeliveryEntity entity, List<meioWmsWarehouseDeliveryDetailEntity> meioWmsWarehouseDeliveryDetailList, List<meioWmsWarehouseDeliveryDetailAllocationEntity> meioWmsWarehouseDeliveryDetailAllocationList, List<meioWMSWarehouseDeliveryBoxDetailEntity> deliveryBoxDetailList, List<meioWMSWarehouseDeliveryCostItemDetailEntity> deliveryCostItemDetailList,string deleteList, string type, string userName, string warehouseId)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioWmsWarehouseDeliveryEntityTmp = GetmeioWmsWarehouseDeliveryEntity(keyValue);
                    entity.Modify(keyValue, userName);

                    db.Update(entity);

                    db.Delete<meioWmsWarehouseDeliveryDetailEntity>(p => p.DeliveryID == keyValue);
                    foreach (var deliveryDetail in meioWmsWarehouseDeliveryDetailList)
                    {
                        var productID = deliveryDetail.ID;
                        deliveryDetail.Create(userName, warehouseId);
                        deliveryDetail.DeliveryID = keyValue;
                        deliveryDetail.State = "10";
                        db.Insert(deliveryDetail);
                    }

                    db.Delete<meioWMSWarehouseDeliveryBoxDetailEntity>(p => p.DeliveryID == keyValue);
                    foreach (var deliveryBoxDetail in deliveryBoxDetailList)
                    {
                        deliveryBoxDetail.Create(userName, warehouseId);
                        deliveryBoxDetail.DeliveryID = keyValue;
                        db.Insert(deliveryBoxDetail);
                    }
                    db.Delete<meioWMSWarehouseDeliveryCostItemDetailEntity>(p => p.DeliveryID == keyValue);
                    foreach (var deliveryCostItemDetail in deliveryCostItemDetailList)
                    {
                        deliveryCostItemDetail.Create(userName, warehouseId);
                        deliveryCostItemDetail.DeliveryID = keyValue;
                        db.Insert(deliveryCostItemDetail);
                    }
                }
                else
                {
                    entity.Create(userName, warehouseId);
                    entity.TotalNum = meioWmsWarehouseDeliveryDetailList.Sum(s => s.TotalNum);
                    entity.TotalRows = meioWmsWarehouseDeliveryDetailList.Count();
                    entity.StartState = "10";
                    entity.EndState = "10";
                    db.Insert(entity);
                    foreach (meioWmsWarehouseDeliveryDetailEntity item in meioWmsWarehouseDeliveryDetailList)
                    {
                        var productID = item.ID;
                        item.Create(userName, warehouseId);
                        item.State = "10";
                        item.DeliveryID = entity.ID;
                        db.Insert(item);
                    }
                    foreach (var deliveryBoxDetail in deliveryBoxDetailList)
                    {
                        deliveryBoxDetail.Create(userName, warehouseId);
                        deliveryBoxDetail.DeliveryID = entity.ID;
                        db.Insert(deliveryBoxDetail);
                    }
                    foreach (var deliveryCostItemDetail in deliveryCostItemDetailList)
                    {
                        deliveryCostItemDetail.Create(userName, warehouseId);
                        deliveryCostItemDetail.DeliveryID = entity.ID;
                        db.Insert(deliveryCostItemDetail);
                    }
                }
                db.Commit();

                #region 计算账单
                Compute(entity, meioWmsWarehouseDeliveryDetailList, deliveryBoxDetailList,deliveryCostItemDetailList);
                #endregion
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 计算出库账单
        /// </summary>
        public void Compute(meioWmsWarehouseDeliveryEntity entity, List<meioWmsWarehouseDeliveryDetailEntity> meioWmsWarehouseDeliveryDetailList, List<meioWMSWarehouseDeliveryBoxDetailEntity> deliveryBoxDetailList, List<meioWMSWarehouseDeliveryCostItemDetailEntity> deliveryCostItemDetailList)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var owenerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(p => p.ID == entity.ShipperID);
                if (owenerEntity != null)
                {
                    //查询客户合同
                    var nowDate = DateTime.Now.Date;
                    var contractEntity = this.BaseRepository().FindEntity<MeioErpContractEntity>(p => p.ID == owenerEntity.ContractID && p.ServiceStartDate <= nowDate && p.ServiceEndDate >= nowDate);
                    if (contractEntity != null)
                    {
                        if (contractEntity.BillingType == 1)//只有计件生成账单
                        {
                            //查询原有出库账单
                            //var deliveryFeeAccountList = this.BaseRepository().FindList<MeioErpDeliveryFeeAccountEntity>(p => p.DeliveryCode == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == owenerEntity.ID);
                            var deliveryFeeAccountList = this.BaseRepository().FindList<MeioErpFeeAccountEntity>(p => p.AssociatedNumber == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == owenerEntity.ID);

                            if (!string.IsNullOrEmpty(contractEntity.PriceSheetID))//是否有报价单
                            {
                                var priceSheetIDs = contractEntity.PriceSheetID.Split(',');
                                var priceSheetList = this.BaseRepository().FindList<MeioErpPriceSheetEntity>(p => priceSheetIDs.Contains(p.ID) && p.WarehouseID.Contains(entity.WarehouseID));
                                if (priceSheetList.Count() > 0)
                                {
                                    priceSheetIDs = priceSheetList.Select(p => p.ID).ToArray();
                                    //查询报价单明细计费项
                                    var priceSheetDetailList = this.BaseRepository().FindList<MeioErpPriceSheetDetailEntity>(p => priceSheetIDs.Contains(p.PriceSheetID));

                                    if (priceSheetDetailList.Count() > 0)
                                    {
                                        if (!string.IsNullOrEmpty(entity.OverseasCode) && entity.OrderState == "仓库处理中" && (entity.BusinessType == "一件代发" || entity.BusinessType == "备货中转"))
                                        {
                                            if (entity.BusinessType == "一件代发")
                                            {
                                                //删除原有账单
                                                var deliveryFeeAccountQueryList= deliveryFeeAccountList.Where(p => p.ExpenseType == "出库费" && p.ExpenseItem == "订单处理费");
                                                var accountIds = deliveryFeeAccountList.Select(p => p.ID);
                                                //db.Delete<MeioErpDeliveryFeeAccountEntity>(p => accountIds.Contains(p.ID));
                                                db.Delete<MeioErpFeeAccountEntity>(p => accountIds.Contains(p.ID) && p.FeeAccountType == "DeliveryFee");
                                                db.Delete<MeioErpDeliveryFeeAccountDetailEntity>(p => accountIds.Contains(p.DeliveryFeeAccountID));

                                                var priceSheetDetailQueryList = priceSheetDetailList.Where(p => p.ExpenseType == "出库费" && p.ExpenseItem == "订单处理费");
                                                if (priceSheetDetailQueryList.Count() > 0)
                                                {
                                                    decimal size = meioWmsWarehouseDeliveryDetailList.Sum(s => s.DeliveryNum.GetValueOrDefault() * s.ProductWeight.GetValueOrDefault());
                                                    if (size > 0)
                                                    {
                                                        var priceSheetDetailQuery = priceSheetDetailQueryList.FirstOrDefault(p => p.ItemDetailMinValue < size && p.ItemDetailMaxValue >= size);
                                                        if (priceSheetDetailQuery != null)
                                                        {
                                                            var priceSheetEntity = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQuery.PriceSheetID);

                                                            MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                            {
                                                                AssociatedNumber = entity.Code,
                                                                OwnerID = owenerEntity.ID,
                                                                WarehouseID = entity.WarehouseID,
                                                                PriceSheetID = priceSheetEntity.ID,
                                                                ExpenseItem = "订单处理费",
                                                                ExpenseType = "出库费",
                                                                ItemName = "订单处理费",
                                                                Currency = priceSheetEntity.Currency,
                                                                TotalAmount = 0,
                                                                CalculatedAmount = 0,
                                                                AdjustmentAmount = 0,
                                                                SettlementStatus = 0,
                                                                FeeAccountType = "DeliveryFee",
                                                                AuditStatus = "0",
                                                            };
                                                            meioErpFeeAccountEntity.Create();

                                                           
                                                            foreach (var item in meioWmsWarehouseDeliveryDetailList)
                                                            {
                                                                if (item.DeliveryNum.GetValueOrDefault() * item.ProductWeight.GetValueOrDefault() > 0)
                                                                {
                                                                    MeioErpDeliveryFeeAccountDetailEntity deliveryFeeAccountDetailEntity = new MeioErpDeliveryFeeAccountDetailEntity
                                                                    {
                                                                        DeliveryFeeAccountID = meioErpFeeAccountEntity.ID,
                                                                        OperationID = priceSheetDetailQuery.ItemID,
                                                                        PriceSheetID = priceSheetDetailQuery.PriceSheetID,
                                                                        ItemName = priceSheetDetailQuery.ItemName,
                                                                        ExpenseItem = priceSheetDetailQuery.ExpenseItem,
                                                                        Conditions = priceSheetDetailQuery.Conditions,
                                                                        UnitPrice = priceSheetDetailQuery.Price,
                                                                        Expense = priceSheetDetailQuery.Price * item.DeliveryNum.GetValueOrDefault() * item.ProductWeight.GetValueOrDefault(),
                                                                        Currency = priceSheetEntity.Currency,
                                                                        Quantity = item.DeliveryNum.GetValueOrDefault() * item.ProductWeight.GetValueOrDefault(),
                                                                        DeliveryCode = entity.Code,
                                                                        DeliveryDate = entity.CreationDate,
                                                                        SKU = item.Code,
                                                                        ProductName = item.Name,
                                                                        Weight = item.ProductWeight
                                                                    };
                                                                    meioErpFeeAccountEntity.TotalAmount += deliveryFeeAccountDetailEntity.Expense.GetValueOrDefault();
                                                                    db.Insert(deliveryFeeAccountDetailEntity);
                                                                }
                                                            }

                                                            db.Insert(meioErpFeeAccountEntity);
                                                        }
                                                    }

                                                }
                                            }

                                            if (entity.BusinessType == "备货中转")
                                            {
                                                //删除原有账单
                                                var deliveryFeeAccountQueryList = deliveryFeeAccountList.Where(p => p.ExpenseType == "出库费" && p.ExpenseItem == "中转费");
                                                var accountIds = deliveryFeeAccountList.Select(p => p.ID);
                                                //db.Delete<MeioErpDeliveryFeeAccountEntity>(p => accountIds.Contains(p.ID));
                                                db.Delete<MeioErpFeeAccountEntity>(p => accountIds.Contains(p.ID) && p.FeeAccountType == "DeliveryFee");
                                                db.Delete<MeioErpDeliveryFeeAccountDetailEntity>(p => accountIds.Contains(p.DeliveryFeeAccountID));

                                                var priceSheetDetailQueryList = priceSheetDetailList.Where(p => p.ExpenseType == "出库费" && p.ExpenseItem == "中转费");
                                                if (priceSheetDetailQueryList.Count() > 0)
                                                {
                                                    decimal size = meioWmsWarehouseDeliveryDetailList.Sum(s => s.BoxNumber.GetValueOrDefault());
                                                    if (size > 0)
                                                    {
                                                        var priceSheetDetailQuery = priceSheetDetailQueryList.FirstOrDefault(p => p.ItemDetailMinValue < size && p.ItemDetailMaxValue >= size);
                                                        if (priceSheetDetailQuery != null)
                                                        {
                                                            var priceSheetEntity = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQuery.PriceSheetID);
                                                            MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                            {
                                                                AssociatedNumber = entity.Code,
                                                                OwnerID = owenerEntity.ID,
                                                                WarehouseID = entity.WarehouseID,
                                                                PriceSheetID = priceSheetEntity.ID,
                                                                ExpenseItem = "中转费",
                                                                ExpenseType = "出库费",
                                                                ItemName = "中转费",
                                                                Currency = priceSheetEntity.Currency,
                                                                TotalAmount = 0,
                                                                CalculatedAmount = 0,
                                                                AdjustmentAmount = 0,
                                                                SettlementStatus = 0,
                                                                FeeAccountType = "DeliveryFee",
                                                                AuditStatus = "0",
                                                            };
                                                            meioErpFeeAccountEntity.Create();

                                                            foreach (var item in meioWmsWarehouseDeliveryDetailList)
                                                            {
                                                                if (item.BoxNumber.GetValueOrDefault() > 0)
                                                                {
                                                                    MeioErpDeliveryFeeAccountDetailEntity deliveryFeeAccountDetailEntity = new MeioErpDeliveryFeeAccountDetailEntity
                                                                    {
                                                                        DeliveryFeeAccountID = meioErpFeeAccountEntity.ID,
                                                                        OperationID = priceSheetDetailQuery.ItemID,
                                                                        PriceSheetID = priceSheetDetailQuery.PriceSheetID,
                                                                        ItemName = priceSheetDetailQuery.ItemName,
                                                                        ExpenseItem = priceSheetDetailQuery.ExpenseItem,
                                                                        Conditions = priceSheetDetailQuery.Conditions,
                                                                        UnitPrice = priceSheetDetailQuery.Price,
                                                                        Expense = priceSheetDetailQuery.Price * item.BoxNumber.GetValueOrDefault(),
                                                                        Currency = priceSheetEntity.Currency,
                                                                        Quantity = item.BoxNumber.GetValueOrDefault(),
                                                                        DeliveryCode = entity.Code,
                                                                        DeliveryDate = entity.CreationDate,
                                                                        SKU = item.Code,
                                                                        ProductName = item.Name,
                                                                        Weight = item.ProductWeight
                                                                    };
                                                                    meioErpFeeAccountEntity.TotalAmount += deliveryFeeAccountDetailEntity.Expense.GetValueOrDefault();
                                                                    db.Insert(deliveryFeeAccountDetailEntity);
                                                                }
                                                            }

                                                            db.Insert(meioErpFeeAccountEntity);
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(entity.OverseasCode) && entity.OrderState == "已出库" && (entity.BusinessType == "一件代发" || entity.BusinessType == "备货中转"))
                                        {
                                            if (entity.BusinessType == "一件代发")
                                            {
                                                //自提费
                                                //删除原有账单
                                                var deliveryFeeAccountQueryList = deliveryFeeAccountList.Where(p => p.ExpenseType == "自提费" && p.ExpenseItem == "自提费");
                                                var accountIds = deliveryFeeAccountList.Select(p => p.ID);
                                                //db.Delete<MeioErpDeliveryFeeAccountEntity>(p => accountIds.Contains(p.ID));
                                                db.Delete<MeioErpFeeAccountEntity>(p => accountIds.Contains(p.ID) && p.FeeAccountType == "DeliveryFee");
                                                db.Delete<MeioErpDeliveryFeeAccountDetailEntity>(p => accountIds.Contains(p.DeliveryFeeAccountID));

                                                var priceSheetDetailQueryList2 = priceSheetDetailList.Where(p => p.ExpenseType == "自提费" && p.ExpenseItem == "自提费");
                                                if (priceSheetDetailQueryList2.Count() > 0)
                                                {
                                                    //查询客户渠道信息
                                                    var ownerCarrierChannelEntity = this.BaseRepository().FindEntity<MeioDataCenterChannelInformationEntity>(p => p.Owner == owenerEntity.Code && p.ChannelCode == entity.CarrierChannel);
                                                    if (ownerCarrierChannelEntity != null)
                                                    {
                                                        if (ownerCarrierChannelEntity.ChannelType == 2)//自提
                                                        {
                                                            decimal size = meioWmsWarehouseDeliveryDetailList.Sum(s => s.DeliveryNum.GetValueOrDefault() * s.ProductWeight.GetValueOrDefault());
                                                            if (size > 0)
                                                            {
                                                                var priceSheetDetailQuery = priceSheetDetailQueryList2.FirstOrDefault(p => p.ItemDetailMinValue < size && p.ItemDetailMaxValue >= size);
                                                                if (priceSheetDetailQuery != null)
                                                                {
                                                                    var priceSheetEntity = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQuery.PriceSheetID);
                                                                    MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                                    {
                                                                        AssociatedNumber = entity.Code,
                                                                        OwnerID = owenerEntity.ID,
                                                                        WarehouseID = entity.WarehouseID,
                                                                        PriceSheetID = priceSheetEntity.ID,
                                                                        ExpenseItem = "自提费",
                                                                        ExpenseType = "自提费",
                                                                        ItemName = "自提费",
                                                                        Currency = priceSheetEntity.Currency,
                                                                        TotalAmount = 0,
                                                                        CalculatedAmount = 0,
                                                                        AdjustmentAmount = 0,
                                                                        SettlementStatus = 0,
                                                                        FeeAccountType = "DeliveryFee",
                                                                        AuditStatus = "0",
                                                                    };
                                                                    meioErpFeeAccountEntity.Create();

                                                                    foreach (var item in meioWmsWarehouseDeliveryDetailList)
                                                                    {
                                                                        if (item.DeliveryNum.GetValueOrDefault() * item.ProductWeight.GetValueOrDefault() > 0)
                                                                        {
                                                                            MeioErpDeliveryFeeAccountDetailEntity deliveryFeeAccountDetailEntity = new MeioErpDeliveryFeeAccountDetailEntity
                                                                            {
                                                                                DeliveryFeeAccountID = meioErpFeeAccountEntity.ID,
                                                                                OperationID = priceSheetDetailQuery.ItemID,
                                                                                PriceSheetID = priceSheetDetailQuery.PriceSheetID,
                                                                                ItemName = priceSheetDetailQuery.ItemName,
                                                                                ExpenseItem = priceSheetDetailQuery.ExpenseItem,
                                                                                Conditions = priceSheetDetailQuery.Conditions,
                                                                                UnitPrice = priceSheetDetailQuery.Price,
                                                                                Expense = priceSheetDetailQuery.Price * item.DeliveryNum.GetValueOrDefault() * item.ProductWeight.GetValueOrDefault(),
                                                                                Currency = priceSheetEntity.Currency,
                                                                                Quantity = item.DeliveryNum.GetValueOrDefault() * item.ProductWeight.GetValueOrDefault(),
                                                                                DeliveryCode = entity.Code,
                                                                                DeliveryDate = entity.CreationDate,
                                                                                SKU = item.Code,
                                                                                ProductName = item.Name,
                                                                                Weight = item.ProductWeight
                                                                            };
                                                                            deliveryFeeAccountDetailEntity.Create();
                                                                            db.Insert(deliveryFeeAccountDetailEntity);
                                                                            meioErpFeeAccountEntity.TotalAmount += deliveryFeeAccountDetailEntity.Expense.GetValueOrDefault();
                                                                            
                                                                        }
                                                                    }

                                                                    db.Insert(meioErpFeeAccountEntity);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                //包材费
                                                if (deliveryCostItemDetailList != null)
                                                {
                                                    var deliveryCostItemDetailQueryList = deliveryCostItemDetailList.Where(p => p.BillItemName == "包材费");
                                                    if (deliveryCostItemDetailQueryList.Count() > 0)
                                                    {
                                                        var warehouseEntity = this.BaseRepository().FindEntity<MeioErpWarehouseEntity>(p => p.ID == entity.WarehouseID);
                                                        if (warehouseEntity != null)
                                                        {
                                                            //删除原有包材账单
                                                            //var packingMaterialsFeeAccountList = this.BaseRepository().FindList<MeioErpPackingMaterialsFeeAccountEntity>(p => p.DeliveryCode == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == owenerEntity.ID);
                                                            var packingMaterialsFeeAccountList = this.BaseRepository().FindList<MeioErpFeeAccountEntity>(p => p.AssociatedNumber == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == owenerEntity.ID);
                                                            var packingMaterialsFeeAccountIDs = packingMaterialsFeeAccountList.Select(p => p.ID);
                                                            //db.Delete<MeioErpPackingMaterialsFeeAccountEntity>(p => packingMaterialsFeeAccountIDs.Contains(p.ID));
                                                            db.Delete<MeioErpFeeAccountEntity>(p => packingMaterialsFeeAccountIDs.Contains(p.ID) && p.FeeAccountType == "PackingMaterialsFee");
                                                            db.Delete<MeioErpPackingMaterialsFeeAccountDetailEntity>(p => packingMaterialsFeeAccountIDs.Contains(p.PackingMaterialsFeeAccountID));

                                                           
                                                            MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                            {
                                                                AssociatedNumber = entity.Code,
                                                                OwnerID = owenerEntity.ID,
                                                                WarehouseID = entity.WarehouseID,
                                                                //PriceSheetID = priceSheetEntity.ID,
                                                                ExpenseItem = "包材费",
                                                                ExpenseType = "包材费",
                                                                ItemName = "包材费",
                                                                Currency = entity.CostCurrencyCode,
                                                                TotalAmount = 0,
                                                                CalculatedAmount = 0,
                                                                AdjustmentAmount = 0,
                                                                SettlementStatus = 0,
                                                                FeeAccountType = "PackingMaterialsFee",
                                                                AuditStatus = "0",
                                                            };
                                                            meioErpFeeAccountEntity.Create();

                                                            foreach (var deliveryCostItemDetail in deliveryCostItemDetailQueryList)
                                                            {
                                                                MeioErpPackingMaterialsFeeAccountDetailEntity meioErpPackingMaterialsFeeAccountDetailEntity = new MeioErpPackingMaterialsFeeAccountDetailEntity
                                                                {
                                                                    PackingMaterialsFeeAccountID = meioErpFeeAccountEntity.ID,
                                                                    ItemName="包材费",
                                                                    ExpenseItem = "包材费",
                                                                    DeliveryCode = entity.Code,
                                                                    DeliveryDate = entity.CreationDate,
                                                                    ThirdPartyExpense=deliveryCostItemDetail.BillItemTotal,
                                                                    AdditionRatio=warehouseEntity.PackagingChargeProportional,
                                                                    Expense= deliveryCostItemDetail.BillItemTotal* warehouseEntity.PackagingChargeProportional* 0.01M,
                                                                    Currency=entity.CostCurrencyCode
                                                                };
                                                                meioErpPackingMaterialsFeeAccountDetailEntity.Create();
                                                                db.Insert(meioErpPackingMaterialsFeeAccountDetailEntity);
                                                                meioErpFeeAccountEntity.TotalAmount += meioErpPackingMaterialsFeeAccountDetailEntity.Expense.GetValueOrDefault();
                                                            }
                                                            db.Insert(meioErpFeeAccountEntity);
                                                        }
                                                    }
                                                }

                                                //运费
                                                if (deliveryCostItemDetailList != null)
                                                {
                                                    string[] strBillItemNames = { "基础运费", "偏远附加费", "超偏远附加费", "超级偏远附加费", "燃油附加费" };
                                                    var deliveryCostItemDetailQueryList = deliveryCostItemDetailList.Where(p => strBillItemNames.Contains(p.BillItemName));
                                                    if (deliveryCostItemDetailQueryList.Count() > 0)
                                                    {
                                                        var warehouseEntity = this.BaseRepository().FindEntity<MeioErpWarehouseEntity>(p => p.ID == entity.WarehouseID);
                                                        if (warehouseEntity != null)
                                                        {
                                                            //删除原有运费账单
                                                            //var freightFeeAccountList = this.BaseRepository().FindList<MeioErpFreightFeeAccountEntity>(p => p.DeliveryCode == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == owenerEntity.ID);
                                                            var freightFeeAccountList = this.BaseRepository().FindList<MeioErpFeeAccountEntity>(p => p.AssociatedNumber == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == owenerEntity.ID);
                                                            var freightFeeAccountIDs = freightFeeAccountList.Select(p => p.ID);
                                                            //db.Delete<MeioErpFreightFeeAccountEntity>(p => freightFeeAccountIDs.Contains(p.ID));
                                                            db.Delete<MeioErpFeeAccountEntity>(p => freightFeeAccountIDs.Contains(p.ID) && p.FeeAccountType == "FreightFee");
                                                            db.Delete<MeioErpFreightFeeAccountDetailEntity>(p => freightFeeAccountIDs.Contains(p.FreightFeeAccountID));
                                                          
                                                            MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                            {
                                                                AssociatedNumber = entity.Code,
                                                                OwnerID = owenerEntity.ID,
                                                                WarehouseID = entity.WarehouseID,
                                                                //PriceSheetID = priceSheetEntity.ID,
                                                                ExpenseItem = "运费",
                                                                ExpenseType = "运费",
                                                                ItemName = "运费",
                                                                Currency = entity.CostCurrencyCode,
                                                                TotalAmount = 0,
                                                                CalculatedAmount = 0,
                                                                AdjustmentAmount = 0,
                                                                SettlementStatus = 0,
                                                                FeeAccountType = "FreightFee",
                                                                AuditStatus = "0",
                                                            };
                                                            meioErpFeeAccountEntity.Create();

                                                            foreach (var deliveryCostItemDetail in deliveryCostItemDetailQueryList)
                                                            {
                                                                MeioErpFreightFeeAccountDetailEntity meioErpFreightFeeAccountDetailEntity = new MeioErpFreightFeeAccountDetailEntity
                                                                {
                                                                    FreightFeeAccountID = meioErpFeeAccountEntity.ID,
                                                                    ItemName = "运费",
                                                                    ExpenseItem = deliveryCostItemDetail.BillItemName,
                                                                    DeliveryCode = entity.Code,
                                                                    DeliveryDate = entity.CreationDate,
                                                                    WaybillCode = entity.ReceiveLogisticsCode,
                                                                    ThirdPartyExpense = deliveryCostItemDetail.BillItemTotal,
                                                                    AdditionRatio = warehouseEntity.ExpressChargeProportional.GetValueOrDefault(),
                                                                    Expense = deliveryCostItemDetail.BillItemTotal * warehouseEntity.ExpressChargeProportional.GetValueOrDefault() * 0.01M,
                                                                    Currency = entity.CostCurrencyCode
                                                                };
                                                                meioErpFreightFeeAccountDetailEntity.Create();
                                                                db.Insert(meioErpFreightFeeAccountDetailEntity);
                                                                meioErpFeeAccountEntity.TotalAmount += meioErpFreightFeeAccountDetailEntity.Expense.GetValueOrDefault();
                                                            }
                                                            db.Insert(meioErpFeeAccountEntity);
                                                        }
                                                    }
                                                }
                                            }

                                            if (entity.BusinessType == "备货中转")
                                            {
                                                if (deliveryBoxDetailList != null)
                                                {
                                                    //自提费
                                                    //删除原有账单
                                                    var deliveryFeeAccountQueryList = deliveryFeeAccountList.Where(p => p.ExpenseType == "自提费" && p.ExpenseItem == "自提费");
                                                    var accountIds = deliveryFeeAccountList.Select(p => p.ID);
                                                    //db.Delete<MeioErpDeliveryFeeAccountEntity>(p => accountIds.Contains(p.ID));
                                                    db.Delete<MeioErpFeeAccountEntity>(p => accountIds.Contains(p.ID) && p.FeeAccountType == "DeliveryFee");
                                                    db.Delete<MeioErpDeliveryFeeAccountDetailEntity>(p => accountIds.Contains(p.DeliveryFeeAccountID));

                                                    var priceSheetDetailQueryList2 = priceSheetDetailList.Where(p => p.ExpenseType == "自提费" && p.ExpenseItem == "自提费");
                                                    if (priceSheetDetailQueryList2.Count() > 0)
                                                    {
                                                        //查询客户渠道信息
                                                        var ownerCarrierChannelEntity = this.BaseRepository().FindEntity<MeioDataCenterChannelInformationEntity>(p => p.Owner == owenerEntity.Code && p.ChannelCode == entity.CarrierChannel);
                                                        if (ownerCarrierChannelEntity != null)
                                                        {
                                                            if (ownerCarrierChannelEntity.ChannelType == 2)//自提
                                                            {
                                                                decimal size = deliveryBoxDetailList.Sum(s => s.BoxNum.GetValueOrDefault() * s.Weight.GetValueOrDefault());
                                                                if (size > 0)
                                                                {
                                                                    var priceSheetDetailQuery = priceSheetDetailQueryList2.FirstOrDefault(p => p.ItemDetailMinValue < size && p.ItemDetailMaxValue >= size);
                                                                    if (priceSheetDetailQuery != null)
                                                                    {
                                                                        var priceSheetEntity = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQuery.PriceSheetID);
                                                                        MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                                        {
                                                                            AssociatedNumber = entity.Code,
                                                                            OwnerID = owenerEntity.ID,
                                                                            WarehouseID = entity.WarehouseID,
                                                                            PriceSheetID = priceSheetEntity.ID,
                                                                            ExpenseItem = "自提费",
                                                                            ExpenseType = "自提费",
                                                                            ItemName = "自提费",
                                                                            Currency = priceSheetEntity.Currency,
                                                                            TotalAmount = 0,
                                                                            CalculatedAmount = 0,
                                                                            AdjustmentAmount = 0,
                                                                            SettlementStatus = 0,
                                                                            FeeAccountType = "DeliveryFee",
                                                                            AuditStatus = "0",
                                                                        };
                                                                        meioErpFeeAccountEntity.Create();

                                                                        foreach (var item in deliveryBoxDetailList)
                                                                        {
                                                                            if (item.BoxNum.GetValueOrDefault() * item.Weight.GetValueOrDefault() > 0)
                                                                            {
                                                                                MeioErpDeliveryFeeAccountDetailEntity deliveryFeeAccountDetailEntity = new MeioErpDeliveryFeeAccountDetailEntity
                                                                                {
                                                                                    DeliveryFeeAccountID = meioErpFeeAccountEntity.ID,
                                                                                    OperationID = priceSheetDetailQuery.ItemID,
                                                                                    PriceSheetID = priceSheetDetailQuery.PriceSheetID,
                                                                                    ItemName = priceSheetDetailQuery.ItemName,
                                                                                    ExpenseItem = priceSheetDetailQuery.ExpenseItem,
                                                                                    Conditions = priceSheetDetailQuery.Conditions,
                                                                                    UnitPrice = priceSheetDetailQuery.Price,
                                                                                    Expense = priceSheetDetailQuery.Price * item.BoxNum.GetValueOrDefault() * item.Weight.GetValueOrDefault(),
                                                                                    Currency = priceSheetEntity.Currency,
                                                                                    Quantity = item.BoxNum.GetValueOrDefault() * item.Weight.GetValueOrDefault(),
                                                                                    DeliveryCode = entity.Code,
                                                                                    DeliveryDate = entity.CreationDate,
                                                                                    SKU = item.DiyBoxCode,
                                                                                    ProductName = item.DiyBoxCode,
                                                                                    Weight = item.Weight
                                                                                };
                                                                                deliveryFeeAccountDetailEntity.Create();
                                                                                db.Insert(deliveryFeeAccountDetailEntity);
                                                                                meioErpFeeAccountEntity.TotalAmount += deliveryFeeAccountDetailEntity.Expense.GetValueOrDefault();
                                                                                
                                                                            }
                                                                        }

                                                                        db.Insert(meioErpFeeAccountEntity);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(entity.OverseasCode) && entity.OrderState == "已取消" && (entity.BusinessType == "一件代发" || entity.BusinessType == "备货中转"))
                                        {
                                        }

                                    }

                                }
                            }
                        }
                    }
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        public IEnumerable<meioWmsWarehouseDeliveryDetailEntity> GetMeioWmsWarehouseDeliveryDetailByCode(string deliveryCode, string productCode, string warehouseId = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(deliveryCode))
                {
                    StringBuilder sql = new StringBuilder();
                    warehouseId = string.IsNullOrEmpty(warehouseId) ? WarehouseUtil.UserWarehouse : warehouseId;
                    sql.Append($"SELECT t1.* FROM  meioWmsWarehouseDelivery t INNER JOIN meioWmsWarehouseDeliveryDetail t1 ON t.ID=t1.DeliveryID WHERE t.Code='{deliveryCode}' AND t.WarehouseID='{warehouseId}' ");
                    if (!string.IsNullOrEmpty(productCode))
                        sql.Append($" AND (t1.Code='{productCode}' OR t1.FNSKU='{productCode}')");
                    var deliveryDetailList = this.BaseRepository().FindList<meioWmsWarehouseDeliveryDetailEntity>(sql.ToString());
                    return deliveryDetailList;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
    }
}
