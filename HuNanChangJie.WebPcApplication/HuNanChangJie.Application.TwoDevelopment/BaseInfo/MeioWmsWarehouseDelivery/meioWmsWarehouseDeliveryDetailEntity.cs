﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-06-04 14:26
    /// 描 述：发货单
    /// </summary>
    public class meioWmsWarehouseDeliveryDetailEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 仓库
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        [Column("SHIPPERID")]
        public string ShipperID { get; set; }
        /// <summary>
        /// 收货单
        /// </summary>
        [Column("DELIVERYID")]
        public string DeliveryID { get; set; }
        /// <summary>
        /// 货品编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }

        /// <summary>
        /// 美鸥sku
        /// </summary>
        [Column("meiouSku")]
        public string meiouSku { get; set; }


        /// <summary>
        /// msku
        /// </summary>
        [Column("msku")]
        public string msku { get; set; }

        /// <summary>
        /// 货品名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 质量状态
        /// </summary>
        [Column("QUALITY")]
        public string Quality { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 单位规格
        /// </summary>
        [Column("UNITSPEC")]
        public string UnitSpec { get; set; }
        /// <summary>
        /// 单位数量
        /// </summary>
        [Column("UNITNUM")]
        public int? UnitNum { get; set; }
        /// <summary>
        /// 总数量
        /// </summary>
        [Column("TOTALNUM")]
        public int? TotalNum { get; set; }
        /// <summary>
        /// 发货数量
        /// </summary>
        [Column("DELIVERYNUM")]
        public int? DeliveryNum { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column("STATE")]
        public string State { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// FNSKU
        /// </summary>
        [Column("FNSKU")]
        public string FNSKU { get; set; }
        /// <summary>
        /// FNSKU
        /// </summary>
        [Column("FNSKUPrintJson")]
        public string FNSKUPrintJson { get; set; }
        /// <summary>
        /// 产品重量
        /// </summary>
        [Column("PRODUCTWEIGHT")]
        public decimal? ProductWeight { get; set; }
        /// <summary>
        /// 箱数
        /// </summary>
        [Column("BOXNUMBER")]
        public int? BoxNumber { get; set; }
        /// <summary>
        /// 是否换标
        /// </summary>
        [Column("ISEXCHANGELABLE")]
        public bool? IsExchangeLable { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.WarehouseID = WarehouseUtil.UserWarehouse;
        }

        public void Create(string userName, string warehouseId)
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            this.CreationName = userName;
            this.ModificationDate = DateTime.Now;
            this.ModificationName = userName;
            this.WarehouseID = warehouseId;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.WarehouseID = WarehouseUtil.UserWarehouse;
        }

        public void Create(string keyValue, string userName, string warehouseId)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            this.CreationName = userName;
            this.ModificationDate = DateTime.Now;
            this.ModificationName = userName;
            this.WarehouseID = warehouseId;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        public void Modify(string keyValue, string userName)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            this.ModificationName = userName;
        }
        #endregion

        /// <summary>
        /// 装箱数量
        /// </summary>
        [NotMapped]
        public int? PackageNum { get; set; }

        /// <summary>
        /// 客户订单号
        /// </summary>
        [NotMapped]
        public string CustomerOrderNo { get; set; }
        /// <summary>
        /// 货品图片
        /// </summary>

        [NotMapped]
        public string ProductImage { get; set; }
        /// <summary>
        /// 货主名称
        /// </summary>
        [NotMapped]
        public string ShipperName { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [NotMapped]
        public string Brand { get; set; }

    }
}

