﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Application.Base.SystemModule;
using System.Collections.Generic;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-06-04 14:26
    /// 描 述：发货单
    /// </summary>
    public class meioWmsWarehouseDeliveryEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// 发货单号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }

        /// <summary>
        /// 海外仓单号
        /// </summary>
        [Column("OverseasCode")]
        public string OverseasCode { get; set; }

        /// <summary>
        /// 是否添加分配数量
        /// </summary>
        [Column("MeioWmsAddNum")]
        public bool? MeioWmsAddNum { get; set; }

        /// <summary>
        /// 是否减了分配数量
        /// </summary>
        [Column("MeioWmsMinusNum")]
        public bool? MeioWmsMinusNum { get; set; }
        /// <summary>
        /// 是否扣除库存
        /// </summary>
        [Column("MeioWmsOuting")]
        public bool? MeioWmsOuting { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        [Column("SHIPPERID")]
        public string ShipperID { get; set; }
        /// <summary>
        /// 发货单类型
        /// </summary>
        [Column("DOCTYPE")]
        public string DocType { get; set; }
        /// <summary>
        /// 波次号
        /// </summary>
        [Column("WAVENO")]
        public string WaveNo { get; set; }
        /// <summary>
        /// 路线
        /// </summary>
        [Column("ROUTE")]
        public string Route { get; set; }

        /// <summary>
        /// 运单号来源 0网页 1接口
        /// </summary>
        [Column("waybillSource")]
        public int? waybillSource { get; set; }

        /// <summary>
        /// 承运商
        /// </summary>
        [Column("CARRIER")]
        public string Carrier { get; set; }
        /// <summary>
        /// 承运商编码
        /// </summary>
        [Column("CarrierCode")]
        public string CarrierCode { get; set; }
        /// <summary>
        /// 客户订单号
        /// </summary>
        [Column("CUSTOMERORDERNO")]
        public string CustomerOrderNo { get; set; }


        /// <summary>
        /// 客户建单人
        /// </summary>
        [Column("CustomerOrderUser")]
        public string CustomerOrderUser { get; set; }


        /// <summary>
        /// 客户建单时间
        /// </summary>
        [Column("CustomerOrderTime")]
        public string CustomerOrderTime { get; set; }

        /// <summary>
        /// 集货位
        /// </summary>
        [Column("COLLECTIONLOCATION")]
        public string CollectionLocation { get; set; }
        /// <summary>
        /// 计划发运时间
        /// </summary>
        [Column("PLANSHIPDATE")]
        public string PlanShipDate { get; set; }
        /// <summary>
        /// 实际发运时间
        /// </summary>
        [Column("ACTUALSHIPDATE")]
        public string ActualShipDate { get; set; }
        /// <summary>
        /// 实际发运人
        /// </summary>
        [Column("ACTUALSHIPMAN")]
        public string ActualShipMan { get; set; }
        /// <summary>
        /// 收货单位
        /// </summary>
        [Column("RECEIVEUNIT")]
        public string ReceiveUnit { get; set; }
        /// <summary>
        /// 收货人
        /// </summary>
        [Column("RECEIVEMAN")]
        public string ReceiveMan { get; set; }
        /// <summary>
        /// 总数量
        /// </summary>
        [Column("TOTALNUM")]
        public int? TotalNum { get; set; }
        /// <summary>
        /// 行数
        /// </summary>
        [Column("TOTALROWS")]
        public int? TotalRows { get; set; }
        

        /// <summary>
        /// 首状态
        /// </summary>
        [Column("STARTSTATE")]
        public string StartState { get; set; }
        /// <summary>
        /// 尾状态
        /// </summary>
        [Column("ENDSTATE")]
        public string EndState { get; set; }
        /// <summary>
        /// 失败原因
        /// </summary>
        [Column("NOTE")]
        public string Note { get; set; }
        /// <summary>
        /// 收货方手机
        /// </summary>
        [Column("RECEIVEPHONE")]
        public string ReceivePhone { get; set; }
        /// <summary>
        /// 收货方国家
        /// </summary>
        [Column("RECEIVECOUNTRY")]
        public string ReceiveCountry { get; set; }
        /// <summary>
        /// 收货方省
        /// </summary>
        [Column("RECEIVEPROVINCE")]
        public string ReceiveProvince { get; set; }
        /// <summary>
        /// 收货方市
        /// </summary>
        [Column("RECEIVECITY")]
        public string ReceiveCity { get; set; }
        /// <summary>
        /// 收货方区/县
        /// </summary>
        [Column("RECEIVEAREA")]
        public string ReceiveArea { get; set; }
        /// <summary>
        /// 收货方详细地址
        /// </summary>
        [Column("RECEIVEADDRESS")]
        public string ReceiveAddress { get; set; }
        /// <summary>
        /// 收货方物流单号
        /// </summary>
        [Column("RECEIVELOGISTICSCODE")]
        public string ReceiveLogisticsCode { get; set; }

        /// <summary>
        /// 物流方式
        /// </summary>
        [Column("ReceiveLogisticsType")]
        public string ReceiveLogisticsType { get; set; }

        /// <summary>
        /// 加入波次失败原因
        /// </summary>
        [Column("FAILCAUSE")]
        public string FailCause { get; set; }
        /// <summary>
        /// 是否未绑定店铺
        /// </summary>
        [Column("NotBindShop")]
        public bool? NotBindShop { get; set; }
        /// <summary>
        /// 是否小包
        /// </summary>
        [Column("SmallBag")]
        public bool? SmallBag { get; set; }
        /// <summary>
        /// 小包重量(kg)
        /// </summary>
        [Column("SmallBagWeight")]
        [DecimalPrecision(18, 6)]
        public decimal? SmallBagWeight { get; set; }
        /// <summary>
        /// 快递面单
        /// </summary>
        [Column("ReceiveLogisticsCodeUrl")]
        public string ReceiveLogisticsCodeUrl { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 处理类型（取消/正常）
        /// </summary>
        [Column("ISCANCEL")]
        public string IsCancel { get; set; }


        /// <summary>
        /// 订单状态
        /// </summary>
        [Column("OrderState")]
        public string OrderState { get; set; }

        /// <summary>
        /// 承运商渠道
        /// </summary>
        [Column("CarrierChannel")]
        public string CarrierChannel { get; set; }

        /// <summary>
        /// 期望到仓时间
        /// </summary>
        [Column("ExpectedArrivalTime")]
        public string ExpectedArrivalTime { get; set; }

        /// <summary>
        /// 出库方式
        /// </summary>
        [Column("DeliveryMethod")]
        public string DeliveryMethod { get; set; }


        /// <summary>
        /// 销售平台
        /// </summary>
        [Column("SalesPlatform")]
        public string SalesPlatform { get; set; }


        /// <summary>
        /// 店铺
        /// </summary>
        [Column("Shop")]
        public string Shop { get; set; }
        /// <summary>
        /// 业务类型：一件代发，备货中转
        /// </summary>
        [Column("BUSINESSTYPE")]
        public string BusinessType { get; set; }
        /// <summary>
        /// 费用币种
        /// </summary>
        [Column("COSTCURRENCYCODE")]
        public string CostCurrencyCode { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.WarehouseID = WarehouseUtil.UserWarehouse;

            //CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
            //codeRuleIBLL.UseRuleSeed("WaveCode");
            //this.Code = codeRuleIBLL.GetBillCode("WaveCode");
        }

        public void Create(string userName,string warehouseId)
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            this.CreationName = userName;
            this.ModificationDate = DateTime.Now;
            this.ModificationName = userName;
            this.WarehouseID = warehouseId;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.WarehouseID = WarehouseUtil.UserWarehouse;

            //CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
            //codeRuleIBLL.UseRuleSeed("WaveCode");
            //this.Code = codeRuleIBLL.GetBillCode("WaveCode");
        }

       

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }

        public void Modify(string keyValue,string userName)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            this.ModificationName = userName;
        }
        public void Modify2(string userId)
        {
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userId;
            this.ModificationName = userId;
        }
        #endregion
        #region  扩展字段
        /// <summary>
        /// 发货单明细
        /// </summary>
        [NotMapped]
        public List<meioWmsWarehouseDeliveryDetailEntity> deliveryDetailList { get; set; }
        /// <summary>
        /// 发货单装箱明细
        /// </summary>
        [NotMapped]
        public List<meioWMSWarehouseDeliveryBoxDetailEntity> deliveryBoxDetailList { get; set; }
        /// <summary>
        /// 发货单计费项明细
        /// </summary>
        [NotMapped]
        public List<meioWMSWarehouseDeliveryCostItemDetailEntity> deliveryCostItemDetailList { get; set; }
        #endregion


    }
}

