﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-06-04 14:26
    /// 描 述：发货单
    /// </summary>
    public class MeioWmsWarehouseDeliveryBLL : MeioWmsWarehouseDeliveryIBLL
    {
        private MeioWmsWarehouseDeliveryService meioWmsWarehouseDeliveryService = new MeioWmsWarehouseDeliveryService();

        #region  获取数据

        /// <summary>
        /// 获取meioWmsWarehouseDeliveryDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseDeliveryDetailEntity> GetmeioWmsWarehouseDeliveryDetailList(string keyValue)
        {
            try
            {
                return meioWmsWarehouseDeliveryService.GetmeioWmsWarehouseDeliveryDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
      

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsWarehouseDeliveryService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseDeliveryEntity entity, List<meioWmsWarehouseDeliveryDetailEntity> meioWmsWarehouseDeliveryDetailList, List<meioWmsWarehouseDeliveryDetailAllocationEntity> meioWmsWarehouseDeliveryDetailAllocationList, List<meioWMSWarehouseDeliveryBoxDetailEntity> deliveryBoxDetailList, List<meioWMSWarehouseDeliveryCostItemDetailEntity> deliveryCostItemDetailList, string deleteList, string type,string userName,string warehouseId)
        {
            try
            {
                meioWmsWarehouseDeliveryService.SaveEntity(keyValue, entity, meioWmsWarehouseDeliveryDetailList, meioWmsWarehouseDeliveryDetailAllocationList,deliveryBoxDetailList,deliveryCostItemDetailList, deleteList, type,userName,warehouseId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        public IEnumerable<meioWmsWarehouseDeliveryDetailEntity> GetMeioWmsWarehouseDeliveryDetailByCode(string deliveryCode, string productCode, string warehouseId = "")
        {
            try
            {
                return meioWmsWarehouseDeliveryService.GetMeioWmsWarehouseDeliveryDetailByCode(deliveryCode, productCode, warehouseId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
