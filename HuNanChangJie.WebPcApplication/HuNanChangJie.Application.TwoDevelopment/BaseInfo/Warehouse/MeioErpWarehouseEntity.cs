﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Application.Base.SystemModule;
using System.Linq;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-24 17:47
    /// 描 述：仓库管理
    /// </summary>
    public class MeioErpWarehouseEntity : BaseEntity
    {
        private DataItemIBLL dataItemIBLL = new DataItemBLL();
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("TELPHONE")]
        public string Telphone { get; set; }
        /// <summary>
        /// 仓库地址
        /// </summary>
        [Column("ADDRSS")]
        public string Addrss { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// CreatedBy
        /// </summary>
        [Column("CREATEDBY")]
        public string CreatedBy { get; set; }
        /// <summary>
        /// 更新用户
        /// </summary>
        [Column("MODIFYBY")]
        public string ModifyBy { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        [Column("PERSONCHARGE")]
        public string PersonCharge { get; set; }
        /// <summary>
        /// 仓库类型
        /// </summary>
        [Column("TYPE")]
        public int? Type { get; set; }
        /// <summary>
        /// 国家，地区
        /// </summary>
        [Column("COUNTRY")]
        public string Country { get; set; }
        /// <summary>
        /// 省
        /// </summary>
        [Column("PROVINCE")]
        public string Province { get; set; }
        /// <summary>
        /// 市
        /// </summary>
        [Column("CITY")]
        public string City { get; set; }
        /// <summary>
        /// 区，县
        /// </summary>
        [Column("AREA")]
        public string Area { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        [Column("ZIPCODE")]
        public string ZipCode { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ISVALID")]
        public bool? IsValid { get; set; }
        /// <summary>
        /// 仓库属性
        /// </summary>
        [Column("ATTRIBUTE")]
        public string Attribute { get; set; }
        /// <summary>
        /// 服务商
        /// </summary>
        [Column("SERVICEPROVIDER")]
        public string ServiceProvider { get; set; }
        /// <summary>
        /// 外部仓库编码
        /// </summary>
        [Column("EXTERNALWAREHOUSECODE")]
        public string ExternalWarehouseCode { get; set; }
        /// <summary>
        /// 快递费加成比例
        /// </summary>
        [Column("EXPRESSCHARGEPROPORTIONAL")]
        public decimal? ExpressChargeProportional { get; set; }
        /// <summary>
        /// 包材费加成比例
        /// </summary>
        [Column("PACKAGINGCHARGEPROPORTIONAL")]
        public decimal? PackagingChargeProportional { get; set; }
        /// <summary>
        /// 仓库OMS地址
        /// </summary>
        [Column("WAREHOUSEOMSURL")]
        public string WarehouseOMSUrl { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
            this.IsValid= true;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
            this.IsValid = true;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModifyDate=DateTime.Now;
            this.ModifyBy = LoginUserInfo.UserInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
        [NotMapped]
        public string TypeName
        {
            get
            {
                if (this.Type == null)
                    return null;
                else
                    return dataItemIBLL.GetDetailList("WarehouseType").Where(p => p.F_ItemValue == this.Type.Value.ToString()).FirstOrDefault().F_ItemName;
            }
        }
    }
}

