﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Kafka.Service;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-24 17:47
    /// 描 述：仓库管理
    /// </summary>
    public class WarehouseService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Telphone,
                t.Addrss,
                t.PersonCharge,
                t.Type,
                t.Country,
                t.Province,
                t.City,
                t.Area,
                t.ZipCode,
                t.Remark,
                t.IsValid,
                t.CreatedBy,
                t.CreatedDate,
                t.ModifyBy,
                t.ModifyDate,
                t.Attribute,
                t.ServiceProvider,
                t.ExternalWarehouseCode,
                t.ExpressChargeProportional,
                t.PackagingChargeProportional,
                t.WarehouseOMSUrl
                ");
                strSql.Append("  FROM MeioErpWarehouse t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["Enabled"].IsEmpty())
                {
                    dp.Add("Enabled", queryParam["Enabled"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsValid=@Enabled ");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type=@Type ");
                }
                strSql.Append(" ORDER BY t.CreatedDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpWarehouseEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreatedDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouse表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseEntity GetMeioErpWarehouseEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWarehouseEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpWarehouseEntity> GetMeioErpWarehouseList(List<string> ids)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpWarehouseEntity>(t=>ids.Contains(t.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public MeioErpWarehouseEntity GetMeioErpWarehouseEntityByCode(string code)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWarehouseEntity>(t => t.Code.Trim().ToLower() == code.Trim().ToLower());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpWarehouseEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public async void SaveEntity(string keyValue, MeioErpWarehouseEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    int count = this.BaseRepository().Update(entity);
                    if (count > 0)
                    {
                        KafkaService.SendMessage(Kafka.Action.update, "warehouse", entity);
                    }
                }
                else
                {
                    entity.Create();
                    int count = this.BaseRepository().Insert(entity);
                    if (count > 0)
                    {
                        KafkaService.SendMessage(Kafka.Action.add, "warehouse", entity);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        internal bool CheckIsWarehouseName(string name, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpWarehouseEntity>(t => t.Name.Trim().ToLower() == name.Trim().ToLower() && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        internal bool SetIsValid(string keyValue, bool isValid)
        {
            try
            {
                var strIds = "'" + keyValue.Replace(",", "','") + "'";
                return this.BaseRepository().ExecuteBySql($"UPDATE dbo.MeioErpWarehouse Set IsValid={(isValid ? 1 : 0)} WHERE ID IN ({strIds})") > 0;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        internal bool CheckIsCode(string code, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpWarehouseEntity>(t => t.Code == code && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
