﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-08-14 16:27
    /// 描 述：盘点账单
    /// </summary>
    public interface MeioErpCheckInventoryAccountIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpCheckInventoryAccountEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpCheckInventoryAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpCheckInventoryAccountDetailEntity> GetMeioErpCheckInventoryAccountDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpCheckInventoryAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpCheckInventoryAccountEntity GetMeioErpCheckInventoryAccountEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpCheckInventoryAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpCheckInventoryAccountDetailEntity GetMeioErpCheckInventoryAccountDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpCheckInventoryAccountEntity entity,List<MeioErpCheckInventoryAccountDetailEntity> meioErpCheckInventoryAccountDetailList,string deleteList,string type);
        #endregion
        /// <summary>
        /// 推送盘点账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        bool PushCheckInventoryAccount(List<string> keyValue);
        /// <summary>
        /// 推送所有未推送OMS的盘点账单
        /// </summary>
        /// <returns></returns>
        bool PushCheckInventoryAccount(bool IsJobRun);
    }
}
