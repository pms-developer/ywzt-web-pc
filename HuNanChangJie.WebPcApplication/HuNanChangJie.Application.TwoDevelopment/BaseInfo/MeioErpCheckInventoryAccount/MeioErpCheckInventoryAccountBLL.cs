﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-08-14 16:27
    /// 描 述：盘点账单
    /// </summary>
    public class MeioErpCheckInventoryAccountBLL : MeioErpCheckInventoryAccountIBLL
    {
        private MeioErpCheckInventoryAccountService meioErpCheckInventoryAccountService = new MeioErpCheckInventoryAccountService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpCheckInventoryAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpCheckInventoryAccountService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCheckInventoryAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpCheckInventoryAccountDetailEntity> GetMeioErpCheckInventoryAccountDetailList(string keyValue)
        {
            try
            {
                return meioErpCheckInventoryAccountService.GetMeioErpCheckInventoryAccountDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCheckInventoryAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCheckInventoryAccountEntity GetMeioErpCheckInventoryAccountEntity(string keyValue)
        {
            try
            {
                return meioErpCheckInventoryAccountService.GetMeioErpCheckInventoryAccountEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCheckInventoryAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCheckInventoryAccountDetailEntity GetMeioErpCheckInventoryAccountDetailEntity(string keyValue)
        {
            try
            {
                return meioErpCheckInventoryAccountService.GetMeioErpCheckInventoryAccountDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpCheckInventoryAccountService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpCheckInventoryAccountEntity entity,List<MeioErpCheckInventoryAccountDetailEntity> meioErpCheckInventoryAccountDetailList,string deleteList,string type)
        {
            try
            {
                meioErpCheckInventoryAccountService.SaveEntity(keyValue, entity,meioErpCheckInventoryAccountDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 推送盘点账单至OMS
        /// </summary>
        public bool PushCheckInventoryAccount(List<string> keyValue)
        {
            try
            {
                return meioErpCheckInventoryAccountService.PushCheckInventoryAccount(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 推送所有未推送OMS的盘点账单
        /// </summary>
        /// <returns></returns>
        public bool PushCheckInventoryAccount(bool IsJobRun = false)
        {
            try
            {
                return meioErpCheckInventoryAccountService.PushCheckInventoryAccount(IsJobRun);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
