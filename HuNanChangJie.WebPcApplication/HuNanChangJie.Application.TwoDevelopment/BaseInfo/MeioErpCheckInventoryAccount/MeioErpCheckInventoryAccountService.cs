﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
using System.Threading.Tasks;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-08-14 16:27
    /// 描 述：盘点账单
    /// </summary>
    public class MeioErpCheckInventoryAccountService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpCheckInventoryAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000");
                strSql.Append(@"
                t.ID,
                t.CheckInventoryCode,
                t.WarehouseID,
                t.OwnerID,
                t.CheckInventoryNum,
                t.OperatingCost,
                t.AccountDate,
                t.State,
                t.IsPushOMS,
                t.ModificationName,
                t.ModificationDate,
                t1.AmountSubtotal
                ");
                strSql.Append("  FROM MeioErpCheckInventoryAccount t ");
                strSql.Append(" LEFT JOIN MeioErpAccountAdditionalCost t1 ON t.ID=t1.AccountID AND t1.AccountType=5 ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.AccountDate >= @startTime AND t.AccountDate <= @endTime ) ");
                }
                if (!queryParam["CheckInventoryCode"].IsEmpty())
                {
                    dp.Add("CheckInventoryCode", queryParam["CheckInventoryCode"].ToString(), DbType.String);
                    strSql.Append(" AND t.CheckInventoryCode=@CheckInventoryCode ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID", queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID", queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                if (!queryParam["IsPushOMS"].IsEmpty())
                {
                    dp.Add("IsPushOMS", queryParam["IsPushOMS"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsPushOMS = @IsPushOMS ");
                }
                if (!queryParam["State"].IsEmpty())
                {
                    dp.Add("State", queryParam["State"].ToString(), DbType.String);
                    strSql.Append(" AND t.State = @State ");
                }
                strSql.Append(" ORDER BY t.AccountDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpCheckInventoryAccountEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCheckInventoryAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpCheckInventoryAccountDetailEntity> GetMeioErpCheckInventoryAccountDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpCheckInventoryAccountDetailEntity>(t => t.CheckInventoryAccountID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCheckInventoryAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCheckInventoryAccountEntity GetMeioErpCheckInventoryAccountEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpCheckInventoryAccountEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCheckInventoryAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCheckInventoryAccountDetailEntity GetMeioErpCheckInventoryAccountDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpCheckInventoryAccountDetailEntity>(t => t.CheckInventoryAccountID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpCheckInventoryAccountEntity = GetMeioErpCheckInventoryAccountEntity(keyValue);
                db.Delete<MeioErpCheckInventoryAccountEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpCheckInventoryAccountDetailEntity>(t => t.CheckInventoryAccountID == meioErpCheckInventoryAccountEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpCheckInventoryAccountEntity entity, List<MeioErpCheckInventoryAccountDetailEntity> meioErpCheckInventoryAccountDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioErpCheckInventoryAccountEntityTmp = GetMeioErpCheckInventoryAccountEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var MeioErpCheckInventoryAccountDetailUpdateList = meioErpCheckInventoryAccountDetailList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in MeioErpCheckInventoryAccountDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var MeioErpCheckInventoryAccountDetailInserList = meioErpCheckInventoryAccountDetailList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in MeioErpCheckInventoryAccountDetailInserList)
                    {
                        item.Create(item.ID);
                        item.CheckInventoryAccountID = meioErpCheckInventoryAccountEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (MeioErpCheckInventoryAccountDetailEntity item in meioErpCheckInventoryAccountDetailList)
                    {
                        item.CheckInventoryAccountID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 推送盘点单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public bool PushCheckInventoryAccount(List<string> keyValue,bool IsJobRun = false)
        {
            var result = false;
            try
            {
                if (keyValue != null)
                {
                    if (keyValue.Count() > 0)
                    {
                        var checkInventoryAccountList = this.BaseRepository().FindList<MeioErpCheckInventoryAccountEntity>(t => keyValue.Contains(t.ID));
                        var checkInventoryAccountDetailList = this.BaseRepository().FindList<MeioErpCheckInventoryAccountDetailEntity>(t => keyValue.Contains(t.CheckInventoryAccountID));

                        var ownerIds = checkInventoryAccountList.Select(o => o.OwnerID);
                        var ownerList = this.BaseRepository().FindList<MeioErpOwnerEntity>(p => ownerIds.Contains(p.ID));
                        var accountAdditionalCostList = this.BaseRepository().FindList<MeioErpAccountAdditionalCostEntity>(p => p.AccountType == 4 && keyValue.Contains(p.AccountID));

                        foreach (var checkInventoryAccount in checkInventoryAccountList)
                        {
                            var accountAdditionalCostEntity = accountAdditionalCostList.FirstOrDefault(p => p.AccountID == checkInventoryAccount.ID);
                            var ownerEntity = ownerList.FirstOrDefault(p => p.ID == checkInventoryAccount.OwnerID);
                            if (ownerEntity != null)
                            {
                                if ((IsJobRun && ownerEntity.PushAccountType == 1) || (!IsJobRun && ownerEntity.PushAccountType == 2))
                                {
                                    CheckInventoryAccountRequest checkInventoryAccountRequest = new CheckInventoryAccountRequest
                                    {
                                        billDate = checkInventoryAccount.AccountDate.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                                        warehouseId = checkInventoryAccount.WarehouseID,
                                        shipperId = checkInventoryAccount.OwnerID,
                                        relationNo = checkInventoryAccount.CheckInventoryCode,
                                        totalFee = checkInventoryAccount.OperatingCost,
                                        stockCount = checkInventoryAccount.CheckInventoryNum,
                                        dtls = new List<CheckInventoryAccountDetailRequest>()
                                    };
                                    //附加费
                                    if (accountAdditionalCostEntity != null)
                                    {
                                        checkInventoryAccountRequest.surchargeQty = accountAdditionalCostEntity.Quantity;
                                        checkInventoryAccountRequest.surchargeSum = accountAdditionalCostEntity.AmountSubtotal;
                                        checkInventoryAccountRequest.surchargeFeeDetail = accountAdditionalCostEntity.ExpenseDetail;
                                    }
                                    foreach (var checkInventoryAccountDetail in checkInventoryAccountDetailList.Where(p => p.CheckInventoryAccountID == checkInventoryAccount.ID))
                                    {
                                        CheckInventoryAccountDetailRequest detail = new CheckInventoryAccountDetailRequest
                                        {
                                            sku = checkInventoryAccountDetail.GoodsCode,
                                            materialName = checkInventoryAccountDetail.GoodsName,
                                            qty = checkInventoryAccountDetail.CheckInventoryNum,
                                            feeScale = checkInventoryAccount.OperatingCost / checkInventoryAccount.CheckInventoryNum * checkInventoryAccountDetail.CheckInventoryNum
                                        };
                                        checkInventoryAccountRequest.dtls.Add(detail);
                                    }
                                    //Task.Run(() =>
                                    //{
                                    var tenant = ownerList.FirstOrDefault(p => p.ID == checkInventoryAccount.OwnerID)?.Code;
                                    OmsService.PushCheckInventoryAccount(checkInventoryAccountRequest, tenant);
                                    //});
                                    checkInventoryAccount.IsJobRun = IsJobRun;
                                    checkInventoryAccount.IsPushOMS = true;
                                    checkInventoryAccount.Modify(checkInventoryAccount.ID);
                                    this.BaseRepository().Update(checkInventoryAccount);
                                }
                            }
                        }

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 推送所有未推送OMS的盘点账单
        /// </summary>
        /// <returns></returns>
        public bool PushCheckInventoryAccount(bool IsJobRun = false)
        {
            var result = false;
            try
            {
                //获取未推送OMS的盘点账单
                var accountList = this.BaseRepository().FindList<MeioErpCheckInventoryAccountEntity>(p => p.IsPushOMS == false);
                if (accountList != null)
                {
                    if (accountList.Count() > 0)
                    {
                        var ids = accountList.Select(o => o.ID).ToList();
                        result = PushCheckInventoryAccount(ids,IsJobRun);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
