﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 09:58
    /// 描 述：账单金额调整
    /// </summary>
    public class MeioErpBillPriceAdjustmentBLL : MeioErpBillPriceAdjustmentIBLL
    {
        private MeioErpBillPriceAdjustmentService meioErpBillPriceAdjustmentService = new MeioErpBillPriceAdjustmentService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpBillPriceAdjustmentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpBillPriceAdjustmentService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpBillPriceAdjustment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpBillPriceAdjustmentEntity GetMeioErpBillPriceAdjustmentEntity(string keyValue)
        {
            try
            {
                return meioErpBillPriceAdjustmentService.GetMeioErpBillPriceAdjustmentEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public MeioErpBillPriceAdjustmentEntity GetMeioErpBillPriceAdjustmentEntityByBill(string bId, string bAmount)
        {
            try
            {
                return meioErpBillPriceAdjustmentService.GetMeioErpBillPriceAdjustmentEntityByBill(bId,bAmount);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpBillPriceAdjustmentService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpBillPriceAdjustmentEntity entity,string deleteList,string type)
        {
            try
            {
                meioErpBillPriceAdjustmentService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntityByBill(MeioErpBillPriceAdjustmentEntity entity)
        {
            try
            {
                meioErpBillPriceAdjustmentService.SaveEntityByBill(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
