﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 09:58
    /// 描 述：账单金额调整
    /// </summary>
    public class MeioErpBillPriceAdjustmentService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpBillPriceAdjustmentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.BillID,
                t.Amount,
                t.Atta,
                t.Notes
                ");
                strSql.Append("  FROM MeioErpBillPriceAdjustment t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                var list=this.BaseRepository().FindList<MeioErpBillPriceAdjustmentEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpBillPriceAdjustment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpBillPriceAdjustmentEntity GetMeioErpBillPriceAdjustmentEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpBillPriceAdjustmentEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取MeioErpBillPriceAdjustment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpBillPriceAdjustmentEntity GetMeioErpBillPriceAdjustmentEntityByBill(string bId,string bAmount)
        {
            try
            {
                var obj = this.BaseRepository().FindEntity<MeioErpBillPriceAdjustmentEntity>(x=>x.BillID==bId);
                if (obj != null) {
                    obj.bAmount = bAmount;
                    return obj;
                }
                obj = new MeioErpBillPriceAdjustmentEntity();
                obj.bAmount = bAmount;
                obj.BillID = bId;
                return obj;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpBillPriceAdjustmentEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpBillPriceAdjustmentEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntityByBill(MeioErpBillPriceAdjustmentEntity entity)
        {
            try
            {
                var bpa = this.BaseRepository().FindEntity<MeioErpBillPriceAdjustmentEntity>(t => t.BillID == entity.BillID);
                if (bpa == null)
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                    return;
                }
                bpa.Amount = entity.Amount;
                bpa.Atta = entity.Atta;
                bpa.Notes = entity.Notes;
                bpa.Modify(bpa.ID);
                this.BaseRepository().Update(bpa);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
