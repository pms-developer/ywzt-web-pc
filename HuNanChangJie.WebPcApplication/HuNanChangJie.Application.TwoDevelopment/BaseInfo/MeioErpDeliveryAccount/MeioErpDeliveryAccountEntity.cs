﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:16
    /// 描 述：出库账单信息
    /// </summary>
    public class MeioErpDeliveryAccountEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 仓库ID
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 货主ID
        /// </summary>
        [Column("OWNERID")]
        public string OwnerID { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column("STATE")]
        public int? State { get; set; }
        /// <summary>
        /// 出库单号
        /// </summary>
        [Column("DELIVERYNO")]
        public string DeliveryNo { get; set; }
        /// <summary>
        /// 订单类型
        /// </summary>
        [Column("ORDERTYPE")]
        public string OrderType { get; set; }
        /// <summary>
        /// 账单日期
        /// </summary>
        [Column("ACCOUNTDATE")]
        public DateTime? AccountDate { get; set; }
        /// <summary>
        /// 合计金额
        /// </summary>
        [Column("TOTALAMOUNT")]
        public decimal? TotalAmount { get; set; }
        /// <summary>
        /// 结算单号
        /// </summary>
        [Column("STATEMENTNO")]
        public string StatementNo { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 是否推送OMS
        /// </summary>
        [Column("ISPUSHOMS")]
        public bool? IsPushOMS { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            if (!IsJobRun)
            {
                UserInfo userInfo = LoginUserInfo.Get();
                if (userInfo != null)
                {
                    this.Creation_Id = userInfo.userId;
                    this.CreationName = userInfo.realName;
                    this.Company_ID = userInfo.companyId;
                    this.CompanyCode = userInfo.CompanyCode;
                    this.CompanyName = userInfo.CompanyName;
                    this.CompanyFullName = userInfo.CompanyFullName;
                }
            }
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            if (!IsJobRun)
            {
                UserInfo userInfo = LoginUserInfo.Get();
                if (userInfo != null)
                {
                    this.Creation_Id = userInfo.userId;
                    this.CreationName = userInfo.realName;
                    this.Company_ID = userInfo.companyId;
                    this.CompanyCode = userInfo.CompanyCode;
                    this.CompanyName = userInfo.CompanyName;
                    this.CompanyFullName = userInfo.CompanyFullName;
                }
            }
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            if (!IsJobRun)
            {
                UserInfo userInfo = LoginUserInfo.Get();
                if (userInfo != null)
                {
                    this.Modification_Id = userInfo.userId;
                    this.ModificationName = userInfo.realName;
                }
            }
        }
        #endregion
        #region  扩展字段
        [NotMapped]
        public bool IsJobRun { get; set; } = false;
        [NotMapped]
        public decimal? AmountSubtotal { get; set; }
        #endregion
    }
}

