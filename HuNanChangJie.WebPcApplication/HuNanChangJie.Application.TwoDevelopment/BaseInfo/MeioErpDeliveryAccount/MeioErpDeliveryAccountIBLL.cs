﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:16
    /// 描 述：出库账单信息
    /// </summary>
    public interface MeioErpDeliveryAccountIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpDeliveryAccountEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpDeliveryAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpDeliveryAccountDetailEntity> GetMeioErpDeliveryAccountDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpDeliveryAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpDeliveryAccountEntity GetMeioErpDeliveryAccountEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpDeliveryAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpDeliveryAccountDetailEntity GetMeioErpDeliveryAccountDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpDeliveryAccountEntity entity,List<MeioErpDeliveryAccountDetailEntity> meioErpDeliveryAccountDetailList,string deleteList,string type);
        #endregion
        bool PushDeliveryAccount(List<string> keyValue);
        bool PushDeliveryAccount(bool IsJobRun);
    }
}
