﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
using System.Threading.Tasks;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:16
    /// 描 述：出库账单信息
    /// </summary>
    public class MeioErpDeliveryAccountService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpDeliveryAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.DeliveryNo,
                t.OrderType,
                t.AccountDate,
                t.WarehouseID,
                t.OwnerID,
                t.TotalAmount,
                t.State,
                t.IsPushOMS,
                t.ModificationName,
                t.ModificationDate,
                t1.AmountSubtotal
                ");
                strSql.Append("  FROM MeioErpDeliveryAccount t ");
                strSql.Append(" LEFT JOIN MeioErpAccountAdditionalCost t1 ON t.ID=t1.AccountID AND t1.AccountType=4 ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["State"].IsEmpty())
                {
                    dp.Add("State", queryParam["State"].ToString(), DbType.String);
                    strSql.Append(" AND t.State = @State ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID", queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID", queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                if (!queryParam["IsPushOMS"].IsEmpty())
                {
                    dp.Add("IsPushOMS", queryParam["IsPushOMS"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsPushOMS = @IsPushOMS ");
                }
                if (!queryParam["DeliveryNo"].IsEmpty())
                {
                    dp.Add("DeliveryNo", queryParam["DeliveryNo"].ToString(), DbType.String);
                    strSql.Append(" AND t.DeliveryNo = @DeliveryNo ");
                }
                strSql.Append(" ORDER BY t.AccountDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpDeliveryAccountEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpDeliveryAccountDetailEntity> GetMeioErpDeliveryAccountDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpDeliveryAccountDetailEntity>(t => t.DeliveryAccountID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpDeliveryAccountEntity GetMeioErpDeliveryAccountEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpDeliveryAccountEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpDeliveryAccountDetailEntity GetMeioErpDeliveryAccountDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpDeliveryAccountDetailEntity>(t => t.DeliveryAccountID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpDeliveryAccountEntity = GetMeioErpDeliveryAccountEntity(keyValue);
                db.Delete<MeioErpDeliveryAccountEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpDeliveryAccountDetailEntity>(t => t.DeliveryAccountID == meioErpDeliveryAccountEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpDeliveryAccountEntity entity, List<MeioErpDeliveryAccountDetailEntity> meioErpDeliveryAccountDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioErpDeliveryAccountEntityTmp = GetMeioErpDeliveryAccountEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var MeioErpDeliveryAccountDetailUpdateList = meioErpDeliveryAccountDetailList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in MeioErpDeliveryAccountDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var MeioErpDeliveryAccountDetailInserList = meioErpDeliveryAccountDetailList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in MeioErpDeliveryAccountDetailInserList)
                    {
                        item.Create(item.ID);
                        item.DeliveryAccountID = meioErpDeliveryAccountEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (MeioErpDeliveryAccountDetailEntity item in meioErpDeliveryAccountDetailList)
                    {
                        item.DeliveryAccountID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 推送出库账单至OMS
        /// </summary>
        public bool PushDeliveryAccount(List<string> keyValue, bool IsJobRun = false)
        {
            var result = false;
            try
            {
                if (keyValue != null)
                {
                    if (keyValue.Count() > 0)
                    {
                        var deliveryAccountList = this.BaseRepository().FindList<MeioErpDeliveryAccountEntity>(t => keyValue.Contains(t.ID));
                        var deliveryAccountDetailList = this.BaseRepository().FindList<MeioErpDeliveryAccountDetailEntity>(t => keyValue.Contains(t.DeliveryAccountID));

                        var ownerIds = deliveryAccountList.Select(o => o.OwnerID);
                        var ownerList = this.BaseRepository().FindList<MeioErpOwnerEntity>(p => ownerIds.Contains(p.ID));
                        var accountAdditionalCostList = this.BaseRepository().FindList<MeioErpAccountAdditionalCostEntity>(p => p.AccountType == 4 && keyValue.Contains(p.AccountID));

                        foreach (var deliveryAccount in deliveryAccountList)
                        {
                            var ownerEntity = ownerList.FirstOrDefault(p => p.ID == deliveryAccount.OwnerID);
                            var accountAdditionalCostEntity = accountAdditionalCostList.FirstOrDefault(p => p.AccountID == deliveryAccount.ID);

                            if (ownerEntity != null)
                            {
                                if ((IsJobRun && ownerEntity.PushAccountType == 1) || (!IsJobRun && ownerEntity.PushAccountType == 2))
                                {
                                    DeliveryAccountRequest deliveryAccountRequest = new DeliveryAccountRequest
                                    {
                                        billDate = deliveryAccount.AccountDate.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                                        warehouseId = deliveryAccount.WarehouseID,
                                        shipperId = deliveryAccount.OwnerID,
                                        relationNo = deliveryAccount.DeliveryNo,
                                        stockType = 2,
                                        orderType = deliveryAccount.OrderType,
                                        totalFee = deliveryAccount.TotalAmount,
                                        dtls = new List<DeliveryAccountDetailRequest>()
                                    };
                                    //附加费
                                    if (accountAdditionalCostEntity != null)
                                    {
                                        deliveryAccountRequest.surchargeQty = accountAdditionalCostEntity.Quantity;
                                        deliveryAccountRequest.surchargeSum = accountAdditionalCostEntity.AmountSubtotal;
                                        deliveryAccountRequest.surchargeFeeDetail = accountAdditionalCostEntity.ExpenseDetail;
                                    }
                                    foreach (var deliveryAccountDetail in deliveryAccountDetailList.Where(p => p.DeliveryAccountID == deliveryAccount.ID))
                                    {
                                        DeliveryAccountDetailRequest detail = new DeliveryAccountDetailRequest
                                        {
                                            materialid = deliveryAccountDetail.GoodsCode,
                                            materialName = deliveryAccountDetail.GoodsName,
                                            qualityState = deliveryAccountDetail.QualityState,
                                            qty = deliveryAccountDetail.Quantity.ToString(),
                                            feeScale = deliveryAccountDetail.ExpenseDetail,
                                            subtotal = deliveryAccountDetail.Amount
                                        };
                                        deliveryAccountRequest.dtls.Add(detail);
                                    }
                                    //Task.Run(() =>
                                    //{
                                    var tenant = ownerList.FirstOrDefault(p => p.ID == deliveryAccount.OwnerID)?.Code;
                                    OmsService.PushDeliveryAccount(deliveryAccountRequest, tenant);
                                    //});
                                    deliveryAccount.IsJobRun = IsJobRun;
                                    deliveryAccount.IsPushOMS = true;
                                    deliveryAccount.Modify(deliveryAccount.ID);
                                    this.BaseRepository().Update(deliveryAccount);
                                }
                            }
                        }

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 推送所有未推送的出库账单至OMS
        /// </summary>
        /// <returns></returns>
        public bool PushDeliveryAccount(bool IsJobRun = false)
        {
            var result = false;
            try
            {
                //获取未推送OMS的出库账单
                var accountList = this.BaseRepository().FindList<MeioErpDeliveryAccountEntity>(p => p.IsPushOMS == false);
                if (accountList != null)
                {
                    if (accountList.Count() > 0)
                    {
                        var ids = accountList.Select(o => o.ID).ToList();
                        result = PushDeliveryAccount(ids,IsJobRun);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
