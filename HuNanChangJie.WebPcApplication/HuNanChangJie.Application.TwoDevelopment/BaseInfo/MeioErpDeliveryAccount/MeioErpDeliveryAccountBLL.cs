﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 20:16
    /// 描 述：出库账单信息
    /// </summary>
    public class MeioErpDeliveryAccountBLL : MeioErpDeliveryAccountIBLL
    {
        private MeioErpDeliveryAccountService meioErpDeliveryAccountService = new MeioErpDeliveryAccountService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpDeliveryAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpDeliveryAccountService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpDeliveryAccountDetailEntity> GetMeioErpDeliveryAccountDetailList(string keyValue)
        {
            try
            {
                return meioErpDeliveryAccountService.GetMeioErpDeliveryAccountDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpDeliveryAccountEntity GetMeioErpDeliveryAccountEntity(string keyValue)
        {
            try
            {
                return meioErpDeliveryAccountService.GetMeioErpDeliveryAccountEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpDeliveryAccountDetailEntity GetMeioErpDeliveryAccountDetailEntity(string keyValue)
        {
            try
            {
                return meioErpDeliveryAccountService.GetMeioErpDeliveryAccountDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpDeliveryAccountService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpDeliveryAccountEntity entity,List<MeioErpDeliveryAccountDetailEntity> meioErpDeliveryAccountDetailList,string deleteList,string type)
        {
            try
            {
                meioErpDeliveryAccountService.SaveEntity(keyValue, entity,meioErpDeliveryAccountDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 推送出库账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public bool PushDeliveryAccount(List<string> keyValue)
        {
            try
            {
                return meioErpDeliveryAccountService.PushDeliveryAccount(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 推送所有未推送的出库账单至OMS
        /// </summary>
        /// <returns></returns>
        public bool PushDeliveryAccount(bool IsJobRun = false)
        {
            try
            {
                return meioErpDeliveryAccountService.PushDeliveryAccount(IsJobRun);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
