﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Kafka.Service;
using System.Diagnostics;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-06-27 19:22
    /// 描 述：货品信息
    /// </summary>
    public class GoodsService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private DataItemIBLL dataItemIBLL = new DataItemBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpGoodsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.OwnerID,
                t.Name,
                t.Type,
                t.Enabled,
                t.ImageUrl,
                t.SupplierID,
                t.IsNominatedSupplier
                ");
                strSql.Append("  FROM MeioErpGoods t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID", queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", queryParam["Code"].ToString(), DbType.String);
                    strSql.Append(" AND t.Code = @Code ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["Enabled"].IsEmpty())
                {
                    dp.Add("Enabled", queryParam["Enabled"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.Enabled=@Enabled ");
                }
                if (!queryParam["SupplierID"].IsEmpty())
                {
                    if (!queryParam["IsPurchaseGoodsSearch"].IsEmpty())
                    {
                        dp.Add("SupplierID", queryParam["SupplierID"].ToString(), DbType.String);
                        strSql.Append(" AND (t.SupplierID=@SupplierID OR t.IsNominatedSupplier=0)");
                    }
                    else
                    {
                        dp.Add("SupplierID", queryParam["SupplierID"].ToString(), DbType.String);
                        strSql.Append(" AND t.SupplierID=@SupplierID ");
                    }
                }
                if (!queryParam["IsNominatedSupplier"].IsEmpty())
                {
                    dp.Add("IsNominatedSupplier", queryParam["IsNominatedSupplier"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsNominatedSupplier=@IsNominatedSupplier ");
                }
               
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpGoodsEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpGoods表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpGoodsEntity GetMeioErpGoodsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpGoodsEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpGoodsEntity> GetMeioErpGoodsListByCodes(List<string> codes)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpGoodsEntity>(p=>codes.Contains(p.Code));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpGoodsEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpGoodsEntity entity, string deleteList, string type)
        {
            try
            {
                var ownerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(entity.OwnerID);
                var data = new
                {
                    electron = "否",
                    shipperId = entity.OwnerID,
                    name = entity.Name,
                    type = dataItemIBLL.GetDetailList("GoodsType").Where(p => p.F_ItemValue == entity.Type.ToString()).FirstOrDefault().F_ItemName,
                    code = entity.Code,
                    price = entity.CostPrice,
                    fnsku="",
                    brand = entity.Brand,
                    warehouseID = ownerEntity != null ? ownerEntity.WarehouseID : "",
                    isNewProduct=false,
                    imgs=entity.ImageUrl,
                    specifications="",
                    volume=entity.Volume,
                    weight= entity.Weight,
                    length=entity.Length,
                    width=entity.Width,
                    height=entity.Height,
                    enabled=entity.Enabled
                };

                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                    if (ownerEntity != null)
                        if (!string.IsNullOrEmpty(ownerEntity.WarehouseID))
                            KafkaService.SendMessage(Kafka.Action.update, "SKU", data);
                }
                else
                {
                    //entity.Create(keyValue);
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                    if (ownerEntity != null)
                        if (!string.IsNullOrEmpty(ownerEntity.WarehouseID))
                            KafkaService.SendMessage(Kafka.Action.add, "SKU", data);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        internal bool SetIsEnable(string keyValue, bool isValid)
        {
            try
            {
                var strIds = "'" + keyValue.Replace(",", "','") + "'";
                return this.BaseRepository().ExecuteBySql($"UPDATE dbo.MeioErpGoods Set Enabled={(isValid ? 1 : 0)} WHERE ID IN ({strIds})") > 0;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal bool CheckIsGoodsCode(string code, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpGoodsEntity>(t => t.Code == code && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
