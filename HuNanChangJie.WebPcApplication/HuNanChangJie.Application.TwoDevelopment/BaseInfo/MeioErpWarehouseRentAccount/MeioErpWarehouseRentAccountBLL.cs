﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 19:54
    /// 描 述：仓租账单信息
    /// </summary>
    public class MeioErpWarehouseRentAccountBLL : MeioErpWarehouseRentAccountIBLL
    {
        private MeioErpWarehouseRentAccountService meioErpWarehouseRentAccountService = new MeioErpWarehouseRentAccountService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseRentAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpWarehouseRentAccountService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseRentAccountDetailEntity> GetMeioErpWarehouseRentAccountDetailList(string keyValue)
        {
            try
            {
                return meioErpWarehouseRentAccountService.GetMeioErpWarehouseRentAccountDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseRentAccountEntity GetMeioErpWarehouseRentAccountEntity(string keyValue)
        {
            try
            {
                return meioErpWarehouseRentAccountService.GetMeioErpWarehouseRentAccountEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseRentAccountDetailEntity GetMeioErpWarehouseRentAccountDetailEntity(string keyValue)
        {
            try
            {
                return meioErpWarehouseRentAccountService.GetMeioErpWarehouseRentAccountDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpWarehouseRentAccountService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWarehouseRentAccountEntity entity,List<MeioErpWarehouseRentAccountDetailEntity> meioErpWarehouseRentAccountDetailList,string deleteList,string type)
        {
            try
            {
                meioErpWarehouseRentAccountService.SaveEntity(keyValue, entity,meioErpWarehouseRentAccountDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 推送仓库租金账单至OMS
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public bool PushWarehouseRentAccount(List<string> keyValue)
        {
            try
            {
                return meioErpWarehouseRentAccountService.PushWarehouseRentAccount(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 推送所有未推送的仓库租金账单至OMS
        /// </summary>
        /// <returns></returns>
        public bool PushWarehouseRentAccount(bool IsJobRun=false)
        {
            try
            {
                return meioErpWarehouseRentAccountService.PushWarehouseRentAccount(IsJobRun);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
