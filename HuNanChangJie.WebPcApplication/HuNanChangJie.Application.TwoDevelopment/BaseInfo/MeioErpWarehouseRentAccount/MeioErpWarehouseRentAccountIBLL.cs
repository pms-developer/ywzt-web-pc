﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 19:54
    /// 描 述：仓租账单信息
    /// </summary>
    public interface MeioErpWarehouseRentAccountIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpWarehouseRentAccountEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpWarehouseRentAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpWarehouseRentAccountDetailEntity> GetMeioErpWarehouseRentAccountDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpWarehouseRentAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWarehouseRentAccountEntity GetMeioErpWarehouseRentAccountEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpWarehouseRentAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWarehouseRentAccountDetailEntity GetMeioErpWarehouseRentAccountDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpWarehouseRentAccountEntity entity,List<MeioErpWarehouseRentAccountDetailEntity> meioErpWarehouseRentAccountDetailList,string deleteList,string type);
        #endregion
        bool PushWarehouseRentAccount(List<string> keyValue);
        bool PushWarehouseRentAccount(bool IsJobRun);
    }
}
