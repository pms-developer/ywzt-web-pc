﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
using System.Threading.Tasks;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-10 19:54
    /// 描 述：仓租账单信息
    /// </summary>
    public class MeioErpWarehouseRentAccountService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseRentAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.AssociateNo,
                t.WarehouseID,
                t.OwnerID,
                t.TotalAmount,
                t.State,
                t1.Code WarehouseCode,
                t1.Name WarehouseName,
                t1.Type WarehouseType,
                t2.Code OwnerCode,
                t2.Name OwnerName,
                t.IsPushOMS,
                t.AccountDate,
                t.ModificationName,
                t.ModificationDate,
                t3.AmountSubtotal
                ");
                strSql.Append("  FROM MeioErpWarehouseRentAccount t ");
                strSql.Append(" LEFT JOIN MeioErpWarehouse t1 ON t.WarehouseID=t1.ID ");
                strSql.Append(" LEFT JOIN MeioErpOwner t2 ON t.OwnerID=t2.ID ");
                strSql.Append(" LEFT JOIN MeioErpAccountAdditionalCost t3 ON t.ID=t3.AccountID AND t3.AccountType=1 ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["State"].IsEmpty())
                {
                    dp.Add("State", queryParam["State"].ToString(), DbType.String);
                    strSql.Append(" AND t.State = @State ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID", queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID", queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                if (!queryParam["IsPushOMS"].IsEmpty())
                {
                    dp.Add("IsPushOMS", queryParam["IsPushOMS"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsPushOMS = @IsPushOMS ");
                }
                if (!queryParam["AssociateNo"].IsEmpty())
                {
                    dp.Add("AssociateNo", queryParam["AssociateNo"].ToString(), DbType.String);
                    strSql.Append(" AND t.AssociateNo = @AssociateNo ");
                }
                strSql.Append(" ORDER BY t.AccountDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpWarehouseRentAccountEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseRentAccountDetailEntity> GetMeioErpWarehouseRentAccountDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpWarehouseRentAccountDetailEntity>(t => t.WarehouseRentAccountID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseRentAccountEntity GetMeioErpWarehouseRentAccountEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWarehouseRentAccountEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentAccountDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseRentAccountDetailEntity GetMeioErpWarehouseRentAccountDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWarehouseRentAccountDetailEntity>(t => t.WarehouseRentAccountID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpWarehouseRentAccountEntity = GetMeioErpWarehouseRentAccountEntity(keyValue);
                db.Delete<MeioErpWarehouseRentAccountEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpWarehouseRentAccountDetailEntity>(t => t.WarehouseRentAccountID == meioErpWarehouseRentAccountEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWarehouseRentAccountEntity entity, List<MeioErpWarehouseRentAccountDetailEntity> meioErpWarehouseRentAccountDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioErpWarehouseRentAccountEntityTmp = GetMeioErpWarehouseRentAccountEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var MeioErpWarehouseRentAccountDetailUpdateList = meioErpWarehouseRentAccountDetailList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in MeioErpWarehouseRentAccountDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var MeioErpWarehouseRentAccountDetailInserList = meioErpWarehouseRentAccountDetailList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in MeioErpWarehouseRentAccountDetailInserList)
                    {
                        item.Create(item.ID);
                        item.WarehouseRentAccountID = meioErpWarehouseRentAccountEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (MeioErpWarehouseRentAccountDetailEntity item in meioErpWarehouseRentAccountDetailList)
                    {
                        item.WarehouseRentAccountID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 推送仓库租金账单至OMS
        /// </summary>
        public bool PushWarehouseRentAccount(List<string> keyValue,bool IsJobRun=false)
        {
            var result = false;
            try
            {
                if (keyValue != null)
                {
                    if (keyValue.Count() > 0)
                    {
                        var warehouseRentAccountList = this.BaseRepository().FindList<MeioErpWarehouseRentAccountEntity>(t => keyValue.Contains(t.ID));
                        var warehouseRentAccountDetailList = this.BaseRepository().FindList<MeioErpWarehouseRentAccountDetailEntity>(t => keyValue.Contains(t.WarehouseRentAccountID));
                        var warehouseIds = warehouseRentAccountList.Select(o => o.WarehouseID);
                        var ownerIds = warehouseRentAccountList.Select(o => o.OwnerID);
                        var warehouseList = this.BaseRepository().FindList<MeioErpWarehouseEntity>(p => warehouseIds.Contains(p.ID));
                        var ownerList = this.BaseRepository().FindList<MeioErpOwnerEntity>(p => ownerIds.Contains(p.ID));
                        var accountAdditionalCostList = this.BaseRepository().FindList<MeioErpAccountAdditionalCostEntity>(p => p.AccountType == 1 && keyValue.Contains(p.AccountID));

                        foreach (var warehouseRentAccount in warehouseRentAccountList)
                        {
                            var ownerEntity = ownerList.FirstOrDefault(p => p.ID == warehouseRentAccount.OwnerID);
                            var accountAdditionalCostEntity = accountAdditionalCostList.FirstOrDefault(p => p.AccountID == warehouseRentAccount.ID);
                            if (ownerEntity != null)
                            {
                                if ((IsJobRun && ownerEntity.PushAccountType == 1) || (!IsJobRun && ownerEntity.PushAccountType==2))
                                {
                                    WarehouseRentAccountRequest warehouseRentAccountRequest = new WarehouseRentAccountRequest
                                    {
                                        billDate = warehouseRentAccount.CreationDate.Value.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                                        warehouseId = warehouseRentAccount.WarehouseID,
                                        warehouseName = warehouseList.FirstOrDefault(p => p.ID == warehouseRentAccount.WarehouseID)?.Name,
                                        shipperId = warehouseRentAccount.OwnerID,
                                        shipperName = ownerEntity?.Name,
                                        relationNo = warehouseRentAccount.AssociateNo,
                                        totalFee = warehouseRentAccount.TotalAmount,
                                        dtls = new List<WarehouseRentAccountDetailRequest>()
                                    };
                                    //附加费
                                    if (accountAdditionalCostEntity != null)
                                    {
                                        warehouseRentAccountRequest.surchargeQty = accountAdditionalCostEntity.Quantity;
                                        warehouseRentAccountRequest.surchargeSum = accountAdditionalCostEntity.AmountSubtotal;
                                        warehouseRentAccountRequest.surchargeFeeDetail = accountAdditionalCostEntity.ExpenseDetail;
                                    }
                                    foreach (var warehouseRentAccountDetail in warehouseRentAccountDetailList.Where(p => p.WarehouseRentAccountID == warehouseRentAccount.ID))
                                    {
                                        if (warehouseRentAccountDetail.InStorageDate.HasValue && warehouseRentAccountDetail.DeliveryDate.HasValue)
                                        {
                                            WarehouseRentAccountDetailRequest detail = new WarehouseRentAccountDetailRequest
                                            {
                                                batchNumber = warehouseRentAccountDetail.BatchNumber,
                                                materialid = warehouseRentAccountDetail.Sku,
                                                sku = warehouseRentAccountDetail.Sku,
                                                materialName = warehouseRentAccountDetail.GoodsName,
                                                categoryName = warehouseRentAccountDetail.GoodsName,
                                                productType = warehouseRentAccountDetail.GoodsType,
                                                stockInDate = warehouseRentAccountDetail.InStorageDate.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                                                stockOutDate = warehouseRentAccountDetail.DeliveryDate.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                                                volume = warehouseRentAccountDetail.Volume.ToString(),
                                                weight = warehouseRentAccountDetail.Weight.ToString(),
                                                inStockDays = warehouseRentAccountDetail.InDay,
                                                qty = warehouseRentAccountDetail.Quantity,
                                                totalVolume = warehouseRentAccountDetail.TotalVolume.ToString(),
                                                feeScale = warehouseRentAccountDetail.ChargingStandard,
                                                storageFee = warehouseRentAccountDetail.StorageCharges
                                            };
                                            warehouseRentAccountRequest.dtls.Add(detail);
                                        }
                                    }
                                    //Task.Run(() =>
                                    //{
                                    var tenant = ownerList.FirstOrDefault(p => p.ID == warehouseRentAccount.OwnerID)?.Code;
                                    OmsService.PushWarehouseRentAccount(warehouseRentAccountRequest, tenant);
                                    //});
                                    warehouseRentAccount.IsJobRun = IsJobRun;
                                    warehouseRentAccount.IsPushOMS = true;
                                    warehouseRentAccount.Modify(warehouseRentAccount.ID);
                                    this.BaseRepository().Update(warehouseRentAccount);
                                }
                            }
                        }

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 推送所有未推送的仓库租金账单
        /// </summary>
        /// <returns></returns>
        public bool PushWarehouseRentAccount(bool IsJobRun=false)
        {
            var result = false;
            try
            {
                //获取未推送OMS的仓库租金账单
                var accountList = this.BaseRepository().FindList<MeioErpWarehouseRentAccountEntity>(p => p.IsPushOMS == false);
                if (accountList != null)
                {
                    if (accountList.Count() > 0)
                    {
                        var ids = accountList.Select(o => o.ID).ToList();
                        result = PushWarehouseRentAccount(ids,IsJobRun);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
