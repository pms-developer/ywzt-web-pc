﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 16:37
    /// 描 述：增值服务档案信息维护
    /// </summary>
    public class ValueAddedServicesService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpValueAddedServicesEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.ItemName,
                t.ItemDescription,
                t.Type,
                t.Unit,
                t.IsValid,
                t.Remark,
                t.OperationPoint
                ");
                strSql.Append("  FROM MeioErpValueAddedServices t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ItemName"].IsEmpty())
                {
                    dp.Add("ItemName", "%" + queryParam["ItemName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ItemName Like @ItemName ");
                }
                if (!queryParam["IsValid"].IsEmpty())
                {
                    dp.Add("IsValid", queryParam["IsValid"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsValid=@IsValid");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type = @Type ");
                }
                strSql.Append(" ORDER BY t.CreatedDate DESC ");
                var list=this.BaseRepository().FindList<MeioErpValueAddedServicesEntity>(strSql.ToString(),dp, pagination);
                //var query = from item in list orderby item.CreatedDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpValueAddedServicesEntity> GetListByIds(string ids)
        {
            try
            {
                var strIds = ids.Split(',');
                var list = this.BaseRepository().FindList<MeioErpValueAddedServicesEntity>(p=>strIds.Contains(p.ID));
                var query = from item in list orderby item.CreatedDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpValueAddedServices表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpValueAddedServicesEntity GetMeioErpValueAddedServicesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpValueAddedServicesEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpValueAddedServicesEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpValueAddedServicesEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        internal bool CheckIsItemName(string name, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpValueAddedServicesEntity>(t => t.ItemName.Trim().ToLower() == name.Trim().ToLower() && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
