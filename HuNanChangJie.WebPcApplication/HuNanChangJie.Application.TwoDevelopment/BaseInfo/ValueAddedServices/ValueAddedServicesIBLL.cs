﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 16:37
    /// 描 述：增值服务档案信息维护
    /// </summary>
    public interface ValueAddedServicesIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpValueAddedServicesEntity> GetPageList(XqPagination pagination, string queryJson);
        IEnumerable<MeioErpValueAddedServicesEntity> GetListByIds(string ids);
        /// <summary>
        /// 获取MeioErpValueAddedServices表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpValueAddedServicesEntity GetMeioErpValueAddedServicesEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpValueAddedServicesEntity entity,string deleteList,string type);
        #endregion
        bool CheckIsItemName(string name,string id);
    }
}
