﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-10 15:52
    /// 描 述：货币管理
    /// </summary>
    public class MeioErpMonetaryManagementService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpMonetaryManagementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.CurrencyCode,
                t.CurrencyName,
                t.CurrencyNameEN,
                t.LeftIdentifier,
                t.ModificationName,
                t.ModificationDate,
                t.CreationDate
                ");
                strSql.Append("  FROM MeioErpMonetaryManagement t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["CurrencyCode"].IsEmpty())
                {
                    dp.Add("CurrencyCode", "%" + queryParam["CurrencyCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.CurrencyCode Like @CurrencyCode ");
                }
                var list=this.BaseRepository().FindList<MeioErpMonetaryManagementEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 查询货币下拉框
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MeioErpMonetaryManagementEntity> GetCurrencyList()
        {
            //查询用户货主授权
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.CurrencyCode,
                t.CurrencyName+'('+t.CurrencyCode+')' CurrencyName,
                t.CurrencyNameEN,
                t.LeftIdentifier,
                t.ModificationName,
                t.ModificationDate,
                t.CreationDate
                ");
                strSql.Append("  FROM MeioErpMonetaryManagement t ");
                strSql.Append("  WHERE 1=1 ");
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                dp.Add("WarehouseID", WarehouseUtil.UserWarehouse, DbType.String);
                strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                var list = this.BaseRepository().FindList<MeioErpMonetaryManagementEntity>(strSql.ToString(), dp);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取MeioErpMonetaryManagement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpMonetaryManagementEntity GetMeioErpMonetaryManagementEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpMonetaryManagementEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpMonetaryManagementEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpMonetaryManagementEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
