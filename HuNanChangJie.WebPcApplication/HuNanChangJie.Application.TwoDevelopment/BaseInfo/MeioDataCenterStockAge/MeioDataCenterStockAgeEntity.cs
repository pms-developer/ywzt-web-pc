﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 19:59
    /// 描 述：领星库龄信息
    /// </summary>
    public class MeioDataCenterStockAgeEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 仓库名称
        /// </summary>
        [Column("WHNAME")]
        public string WhName { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        [Column("WHCODE")]
        public string WhCode { get; set; }
        /// <summary>
        /// SKU
        /// </summary>
        [Column("SKU")]
        public string SKU { get; set; }
        /// <summary>
        /// 退货库存fnsku
        /// </summary>
        [Column("FNSKU")]
        public string FnSKU { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        [Column("PRODUCTNAME")]
        public string ProductName { get; set; }
        /// <summary>
        /// 库存类型，0-正品，1-次品
        /// </summary>
        [Column("STOCKTYPE")]
        public int? StockType { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        [Column("TOTALAMOUNT")]
        public int? TotalAmount { get; set; }
        /// <summary>
        /// 库龄天数
        /// </summary>
        [Column("STOCKAGE")]
        public int? StockAge { get; set; }
        /// <summary>
        /// 上架日期
        /// </summary>
        [Column("SHELFDATE")]
        public DateTime? ShelfDate { get; set; }
        /// <summary>
        /// 库存统计日期
        /// </summary>
        [Column("STATISTICDATE")]
        public DateTime? StatisticDate { get; set; }
        /// <summary>
        /// 长
        /// </summary>
        [Column("LENGTH")]
        public decimal? Length { get; set; }
        /// <summary>
        /// 英制长
        /// </summary>
        [Column("LENGTHBS")]
        public decimal? LengthBs { get; set; }
        /// <summary>
        /// 宽
        /// </summary>
        [Column("WIDTH")]
        public decimal? Width { get; set; }
        /// <summary>
        /// 英制宽
        /// </summary>
        [Column("WIDTHBS")]
        public decimal? WidthBs { get; set; }
        /// <summary>
        /// 高
        /// </summary>
        [Column("HEIGHT")]
        public decimal? Height { get; set; }
        /// <summary>
        /// 英制高
        /// </summary>
        [Column("HEIGHTBS")]
        public decimal? HeightBs { get; set; }
        /// <summary>
        /// 体积
        /// </summary>
        [Column("VOLUME")]
        public decimal? Volume { get; set; }
        /// <summary>
        /// 英制体积
        /// </summary>
        [Column("VOLUMEBS")]
        public decimal? VolumeBs { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 客户编码
        /// </summary>
        [Column("OWNERCODE")]
        public string OwnerCode { get; set; }
        /// <summary>
        /// 客户ID
        /// </summary>
        [Column("OWNERID")]
        public string OwnerID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            if (LoginUserInfo.Get() != null)
            {
                UserInfo userInfo = LoginUserInfo.Get();
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
                this.ModificationDate = DateTime.Now;
                this.Modification_Id = userInfo.userId;
                this.ModificationName = userInfo.realName;
                this.Company_ID = userInfo.companyId;
                this.CompanyCode = userInfo.CompanyCode;
                this.CompanyName = userInfo.CompanyName;
                this.CompanyFullName = userInfo.CompanyFullName;
            }
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            if (LoginUserInfo.Get() != null)
            {
                UserInfo userInfo = LoginUserInfo.Get();
                this.Modification_Id = userInfo.userId;
                this.ModificationName = userInfo.realName;
            }
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

