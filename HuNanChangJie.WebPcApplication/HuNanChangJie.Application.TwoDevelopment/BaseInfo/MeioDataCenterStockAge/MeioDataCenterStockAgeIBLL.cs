﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 19:59
    /// 描 述：领星库龄信息
    /// </summary>
    public interface MeioDataCenterStockAgeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioDataCenterStockAgeEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioDataCenterStockAge表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioDataCenterStockAgeEntity GetMeioDataCenterStockAgeEntity(string keyValue);
        /// <summary>
        /// 根据仓库编码和SKU获取库龄信息
        /// </summary>
        /// <param name="whCode"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        MeioDataCenterStockAgeEntity GetMeioDataCenterStockAgeEntityByWhCodeAndSKU(string whCode, string sku);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioDataCenterStockAgeEntity entity,string deleteList,string type);
        /// <summary>
        /// 批量录入库龄信息
        /// </summary>
        /// <param name="ownerCode"></param>
        /// <param name="list"></param>
        void BatchAddMeioDataCenterStockAge(string ownerCode, List<MeioDataCenterStockAgeEntity> list);
        #endregion

    }
}
