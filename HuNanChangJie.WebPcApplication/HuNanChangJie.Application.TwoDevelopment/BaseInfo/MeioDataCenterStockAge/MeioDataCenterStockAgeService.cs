﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 19:59
    /// 描 述：领星库龄信息
    /// </summary>
    public class MeioDataCenterStockAgeService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioDataCenterStockAgeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.WhCode,
                t.SKU,
                t.StockAge
                ");
                strSql.Append("  FROM MeioDataCenterStockAge t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["WhCode"].IsEmpty())
                {
                    dp.Add("WhCode", "%" + queryParam["WhCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.WhCode Like @WhCode ");
                }
                if (!queryParam["SKU"].IsEmpty())
                {
                    dp.Add("SKU", "%" + queryParam["SKU"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.SKU Like @SKU ");
                }
                var list = this.BaseRepository().FindList<MeioDataCenterStockAgeEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioDataCenterStockAge表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioDataCenterStockAgeEntity GetMeioDataCenterStockAgeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioDataCenterStockAgeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根据仓库编码和SKU获取库龄信息
        /// </summary>
        /// <param name="whCode"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public MeioDataCenterStockAgeEntity GetMeioDataCenterStockAgeEntityByWhCodeAndSKU(string whCode, string sku)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioDataCenterStockAgeEntity>(t => t.WhCode == whCode && t.SKU == sku);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioDataCenterStockAgeEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioDataCenterStockAgeEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 批量录入库龄信息
        /// </summary>
        /// <param name="ownerCode"></param>
        /// <param name="list"></param>
        public void BatchAddMeioDataCenterStockAge(string ownerCode, List<MeioDataCenterStockAgeEntity> list)
        {
            try
            {
                var ownerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(p => p.Code == ownerCode);
                if (ownerEntity != null)
                {
                    list.ForEach(f =>
                    {
                        f.OwnerID = ownerEntity.ID;
                        f.OwnerCode = ownerCode;
                        f.Create();
                    });
                    this.BaseRepository().Insert(list);
                    #region 计算仓储费
                    Compute(ownerEntity.ID, ownerCode, list);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 计算仓储费
        /// </summary>
        public void Compute(string ownerID, string ownerCode, List<MeioDataCenterStockAgeEntity> list)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var owenerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(p => p.ID == ownerID);
                if (owenerEntity != null)
                {
                    //查询客户合同
                    var nowDate = DateTime.Now.Date;
                    var contractEntity = this.BaseRepository().FindEntity<MeioErpContractEntity>(p => p.ID == owenerEntity.ContractID && p.ServiceStartDate <= nowDate && p.ServiceEndDate >= nowDate);
                    if (contractEntity != null)
                    {
                        if (contractEntity.IsWarehouseRent.GetValueOrDefault())
                        {
                            if (!string.IsNullOrEmpty(contractEntity.PriceSheetID))//是否有报价单
                            {
                                var priceSheetIDs = contractEntity.PriceSheetID.Split(',');
                                var priceSheetList = this.BaseRepository().FindList<MeioErpPriceSheetEntity>(p => priceSheetIDs.Contains(p.ID));
                                if (priceSheetList.Count() > 0)
                                {
                                    var warehouseCodeList = list.Select(o => o.WhCode).Distinct();
                                    var warehouseList = this.BaseRepository().FindList<MeioErpWarehouseEntity>(p => warehouseCodeList.Contains(p.Code) || warehouseCodeList.Contains(p.ExternalWarehouseCode));

                                    foreach (var warehouse in warehouseList)
                                    {
                                        var priceSheetQueryList = priceSheetList.Where(p => p.WarehouseID == warehouse.ID);
                                        if (priceSheetQueryList.Count() > 0)
                                        {
                                            priceSheetIDs = priceSheetQueryList.Select(p => p.ID).ToArray();
                                            //查询报价单明细计费项
                                            var priceSheetDetailList = this.BaseRepository().FindList<MeioErpPriceSheetDetailEntity>(p => priceSheetIDs.Contains(p.PriceSheetID));

                                            if (priceSheetDetailList.Count() > 0)
                                            {
                                                var priceSheetDetailQueryList = priceSheetDetailList.Where(p => p.ExpenseType == "仓租费" && p.ExpenseItem == "仓租费");
                                                if (priceSheetDetailQueryList.Count() > 0)
                                                {
                                                    var priceSheet = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQueryList.FirstOrDefault().PriceSheetID);

                                                    MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                    {
                                                        OwnerID = ownerID,
                                                        WarehouseID = warehouse.ID,
                                                        PriceSheetID = priceSheet.ID,
                                                        ExpenseItem = "仓租费",
                                                        ExpenseType = "仓租费",
                                                        ItemName = "仓租费",
                                                        Currency= priceSheet.Currency,
                                                        TotalAmount = 0,
                                                        CalculatedAmount=0,
                                                        AdjustmentAmount=0,
                                                        SettlementStatus=0,
                                                        FeeAccountType= "WarehouseRentFee",
                                                        AuditStatus="0",
                                                    };
                                                    meioErpFeeAccountEntity.Create();

                                                    foreach (var stockAge in list.Where(p=>p.WhCode==warehouse.Code || p.WhCode==warehouse.ExternalWarehouseCode))
                                                    {
                                                        decimal size = stockAge.StockAge.GetValueOrDefault();
                                                        if (size > 0)
                                                        {
                                                            var priceSheetDetailQuery = priceSheetDetailQueryList.FirstOrDefault(p => p.ItemDetailMinValue < size && p.ItemDetailMaxValue >= size);
                                                            if (priceSheetDetailQuery != null)
                                                            {
                                                                var priceSheetEntity = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQuery.PriceSheetID);

                                                                MeioErpWarehouseRentFeeAccountDetailEntity warehouseRentFeeAccountDetailEntity = new MeioErpWarehouseRentFeeAccountDetailEntity
                                                                {
                                                                    WarehouseRentFeeAccountID = meioErpFeeAccountEntity.ID,
                                                                    OperationID = priceSheetDetailQuery.ID,
                                                                    PriceSheetID = priceSheetDetailQuery.PriceSheetID,
                                                                    ItemName = "仓租费",
                                                                    ExpenseItem = "仓租费",
                                                                    Conditions = "",
                                                                    BatchCode = stockAge.StatisticDate.ToString(),
                                                                    SKU = stockAge.SKU,
                                                                    ProductName = stockAge.ProductName,
                                                                    Volume = stockAge.Volume,
                                                                    StockAge = stockAge.StockAge,
                                                                    Quantity = stockAge.TotalAmount,
                                                                    Unit = priceSheetDetailQuery.Price,
                                                                    Expense = priceSheetDetailQuery.Price.GetValueOrDefault() *stockAge.TotalAmount.GetValueOrDefault()* stockAge.Volume.GetValueOrDefault(),
                                                                    Currency = priceSheetEntity.Currency,
                                                                };
                                                                warehouseRentFeeAccountDetailEntity.Create();
                                                                db.Insert(warehouseRentFeeAccountDetailEntity);
                                                                meioErpFeeAccountEntity.TotalAmount += warehouseRentFeeAccountDetailEntity.Expense;

                                                            }
                                                        }
                                                    }
                                                    db.Insert(meioErpFeeAccountEntity);

                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
