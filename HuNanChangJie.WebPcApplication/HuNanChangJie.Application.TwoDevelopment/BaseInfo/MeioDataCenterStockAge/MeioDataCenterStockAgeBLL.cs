﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 19:59
    /// 描 述：领星库龄信息
    /// </summary>
    public class MeioDataCenterStockAgeBLL : MeioDataCenterStockAgeIBLL
    {
        private MeioDataCenterStockAgeService meioDataCenterStockAgeService = new MeioDataCenterStockAgeService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioDataCenterStockAgeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioDataCenterStockAgeService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioDataCenterStockAge表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioDataCenterStockAgeEntity GetMeioDataCenterStockAgeEntity(string keyValue)
        {
            try
            {
                return meioDataCenterStockAgeService.GetMeioDataCenterStockAgeEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 根据仓库编码和SKU获取库龄信息
        /// </summary>
        /// <param name="whCode"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public MeioDataCenterStockAgeEntity GetMeioDataCenterStockAgeEntityByWhCodeAndSKU(string whCode, string sku)
        {
            try
            {
                return meioDataCenterStockAgeService.GetMeioDataCenterStockAgeEntityByWhCodeAndSKU(whCode,sku);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioDataCenterStockAgeService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioDataCenterStockAgeEntity entity,string deleteList,string type)
        {
            try
            {
                meioDataCenterStockAgeService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 批量录入库龄信息
        /// </summary>
        /// <param name="ownerCode"></param>
        /// <param name="list"></param>
        public void BatchAddMeioDataCenterStockAge(string ownerCode, List<MeioDataCenterStockAgeEntity> list)
        {
            try
            {
                meioDataCenterStockAgeService.BatchAddMeioDataCenterStockAge(ownerCode, list);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
