﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-19 10:01
    /// 描 述：客户服务信息
    /// </summary>
    public class MeioErpCustomerServiceBLL : MeioErpCustomerServiceIBLL
    {
        private MeioErpCustomerServiceService meioErpCustomerServiceService = new MeioErpCustomerServiceService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpCustomerServiceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpCustomerServiceService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCustomerServiceDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpCustomerServiceDetailEntity> GetMeioErpCustomerServiceDetailList(string keyValue)
        {
            try
            {
                return meioErpCustomerServiceService.GetMeioErpCustomerServiceDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCustomerService表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCustomerServiceEntity GetMeioErpCustomerServiceEntity(string keyValue)
        {
            try
            {
                return meioErpCustomerServiceService.GetMeioErpCustomerServiceEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCustomerServiceDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCustomerServiceDetailEntity GetMeioErpCustomerServiceDetailEntity(string keyValue)
        {
            try
            {
                return meioErpCustomerServiceService.GetMeioErpCustomerServiceDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpCustomerServiceService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpCustomerServiceEntity entity,List<MeioErpCustomerServiceDetailEntity> meioErpCustomerServiceDetailList,string deleteList,string type)
        {
            try
            {
                meioErpCustomerServiceService.SaveEntity(keyValue, entity,meioErpCustomerServiceDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 根据订单号获取客户服务
        /// </summary>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        public MeioErpCustomerServiceEntity GetErpCustomerServiceEntityByOrderCode(string orderCode)
        {
            try
            {
                return meioErpCustomerServiceService.GetErpCustomerServiceEntityByOrderCode(orderCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool PushCustomerService(List<string> ids)
        {
            try
            {
                return meioErpCustomerServiceService.PushCustomerService(ids);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void AuditCustomerService(string orderCode, string warehouseId, string ownerId, string goodsCode,string auditResult)
        {
            try
            {
               meioErpCustomerServiceService.AuditCustomerService(orderCode,warehouseId,ownerId,goodsCode,auditResult);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
