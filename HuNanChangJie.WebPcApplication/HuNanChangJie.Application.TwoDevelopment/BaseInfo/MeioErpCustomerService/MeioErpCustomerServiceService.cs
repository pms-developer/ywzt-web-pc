﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
using System.Threading.Tasks;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-19 10:01
    /// 描 述：客户服务信息
    /// </summary>
    public class MeioErpCustomerServiceService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpCustomerServiceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.OrderCode,
                t.OrderType,
                t.WarehouseID,
                t.OwnerID,
                t.State,
                t.CreationDate,
                t1.ContractingParty
                ");
                strSql.Append("  FROM MeioErpCustomerService t ");
                strSql.Append("  LEFT JOIN MeioErpOwner t1 ON t.OwnerID=t1.ID");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["OrderCode"].IsEmpty())
                {
                    dp.Add("OrderCode", "%" + queryParam["OrderCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OrderCode Like @OrderCode ");
                }
                if (!queryParam["OrderType"].IsEmpty())
                {
                    dp.Add("OrderType", queryParam["OrderType"].ToString(), DbType.String);
                    strSql.Append(" AND t.OrderType = @OrderType ");
                }
                if (!queryParam["State"].IsEmpty())
                {
                    dp.Add("State", queryParam["State"].ToString(), DbType.String);
                    strSql.Append(" AND t.State = @State ");
                }
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpCustomerServiceEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCustomerServiceDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpCustomerServiceDetailEntity> GetMeioErpCustomerServiceDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpCustomerServiceDetailEntity>(t => t.CustomerServiceID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCustomerService表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCustomerServiceEntity GetMeioErpCustomerServiceEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpCustomerServiceEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCustomerServiceDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCustomerServiceDetailEntity GetMeioErpCustomerServiceDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpCustomerServiceDetailEntity>(t => t.CustomerServiceID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpCustomerServiceEntity = GetMeioErpCustomerServiceEntity(keyValue);
                db.Delete<MeioErpCustomerServiceEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpCustomerServiceDetailEntity>(t => t.CustomerServiceID == meioErpCustomerServiceEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpCustomerServiceEntity entity, List<MeioErpCustomerServiceDetailEntity> meioErpCustomerServiceDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    //var meioErpCustomerServiceEntityTmp = GetMeioErpCustomerServiceEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);

                    db.Delete<MeioErpCustomerServiceDetailEntity>(t => t.CustomerServiceID == keyValue);
                    foreach (var item in meioErpCustomerServiceDetailList)
                    {
                        item.CustomerServiceID = keyValue;
                        item.State = "0";
                        item.Create();
                        db.Insert(item);
                    }
                    //var delJson=deleteList.ToObject<List<JObject>>();
                    //foreach (var del in delJson)
                    //{
                    //    var idList = del["idList"].ToString();
                    //    var sql = "";
                    //    if (idList.Contains(","))
                    //    {
                    //        var ids = idList.Split(',');
                    //        foreach (var id in ids)
                    //        {
                    //            sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (!string.IsNullOrWhiteSpace(idList))
                    //        {
                    //            sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //}

                    ////没有生成代码 
                    //var MeioErpCustomerServiceDetailUpdateList= meioErpCustomerServiceDetailList.FindAll(i=>i.EditType==EditType.Update);
                    //foreach (var item in MeioErpCustomerServiceDetailUpdateList)
                    //{
                    //    db.Update(item);
                    //}
                    //var MeioErpCustomerServiceDetailInserList= meioErpCustomerServiceDetailList.FindAll(i=>i.EditType==EditType.Add);
                    //foreach (var item in MeioErpCustomerServiceDetailInserList)
                    //{
                    //    item.Create(item.ID);
                    //    item.CustomerServiceID = meioErpCustomerServiceEntityTmp.ID;
                    //    db.Insert(item);
                    //}

                    ////没有生成代码1 
                }
                else
                {
                    entity.State = "0";
                    entity.Create();
                    db.Insert(entity);
                    foreach (MeioErpCustomerServiceDetailEntity item in meioErpCustomerServiceDetailList)
                    {
                        item.CustomerServiceID = entity.ID;
                        item.State = "0";
                        item.Create();
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        /// <summary>
        /// 根据订单号获取客户服务
        /// </summary>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        public MeioErpCustomerServiceEntity GetErpCustomerServiceEntityByOrderCode(string orderCode)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpCustomerServiceEntity>(p => p.OrderCode == orderCode).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 推送客户服务至OMS
        /// </summary>
        /// <param name="ids"></param>
        public bool PushCustomerService(List<string> ids)
        {
            var result = false;
            try
            {
                if (ids != null)
                {
                    if (ids.Count() > 0)
                    {
                        var customerServiceList = this.BaseRepository().FindList<MeioErpCustomerServiceEntity>(p => ids.Contains(p.ID) && p.State == "0");
                        var customerServiceIds = customerServiceList.Select(p => p.ID);
                        var customerServiceDetailList = this.BaseRepository().FindList<MeioErpCustomerServiceDetailEntity>(p => customerServiceIds.Contains(p.CustomerServiceID));

                        var ownerIds = customerServiceList.Select(o => o.OwnerID);
                        var ownerList = this.BaseRepository().FindList<MeioErpOwnerEntity>(p => ownerIds.Contains(p.ID));

                        foreach (var customerService in customerServiceList)
                        {
                            CustomerServiceRequest customerServiceRequest = new CustomerServiceRequest
                            {
                                number = customerService.OrderCode,
                                dtls = new List<CustomerServiceDetailRequest>()
                            };
                            foreach (var customerServiceDetail in customerServiceDetailList.Where(p => p.CustomerServiceID == customerService.ID))
                            {
                                CustomerServiceDetailRequest customerServiceDetailRequest = new CustomerServiceDetailRequest
                                {
                                    code = customerServiceDetail.GoodsCode,
                                    serviceName = customerServiceDetail.ServiceName,
                                    price = customerServiceDetail.Price,
                                    remark = customerServiceDetail.PriceDescription
                                };
                                customerServiceRequest.dtls.Add(customerServiceDetailRequest);
                            }
                            Task.Run(() =>
                            {
                                var tenant = ownerList.FirstOrDefault(p => p.ID == customerService.OwnerID)?.Code;
                                OmsService.PushCustomerService(customerServiceRequest, tenant);
                            });
                            customerService.State = "1";//已发送
                            customerService.Modify(customerService.ID);
                            this.BaseRepository().Update(customerService);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            return result;
        }
        /// <summary>
        /// 审核客户服务信息
        /// </summary>
        /// <param name="orderCode"></param>
        /// <param name="warehouseId"></param>
        /// <param name="ownerId"></param>
        /// <param name="goodsCode"></param>
        /// <param name="auditResult"></param>
        public void AuditCustomerService(string orderCode, string warehouseId, string ownerId, string goodsCode, string auditResult)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var customerServiceEntity = this.BaseRepository().FindList<MeioErpCustomerServiceEntity>(t => t.OrderCode == orderCode && t.WarehouseID == warehouseId && t.OwnerID == ownerId).FirstOrDefault();
                if (customerServiceEntity != null)
                {
                    var customerServiceDetailEntity = this.BaseRepository().FindList<MeioErpCustomerServiceDetailEntity>(t => t.CustomerServiceID == customerServiceEntity.ID && t.GoodsCode == goodsCode).FirstOrDefault();
                    if (customerServiceDetailEntity != null)
                    {
                        customerServiceDetailEntity.State = auditResult == "接受" ? "2" : "3";
                        db.Update(customerServiceDetailEntity);
                    }


                    //customerServiceEntity.State = auditResult == "接受" ? "2" : "3";
                    //customerServiceEntity.Modify(customerServiceEntity.ID);
                    //this.BaseRepository().Update(customerServiceEntity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
