﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-19 10:01
    /// 描 述：客户服务信息
    /// </summary>
    public interface MeioErpCustomerServiceIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpCustomerServiceEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpCustomerServiceDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpCustomerServiceDetailEntity> GetMeioErpCustomerServiceDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpCustomerService表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpCustomerServiceEntity GetMeioErpCustomerServiceEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpCustomerServiceDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpCustomerServiceDetailEntity GetMeioErpCustomerServiceDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpCustomerServiceEntity entity,List<MeioErpCustomerServiceDetailEntity> meioErpCustomerServiceDetailList,string deleteList,string type);
        #endregion
        /// <summary>
        /// 根据订单号获取客户服务
        /// </summary>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        MeioErpCustomerServiceEntity GetErpCustomerServiceEntityByOrderCode(string orderCode);
        bool PushCustomerService(List<string> ids);
        void AuditCustomerService(string orderCode, string warehouseId, string ownerId, string goodsCode, string auditResult);
    }
}
