﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-10 17:11
    /// 描 述：汇率
    /// </summary>
    public interface MeioErpWarehouseExchangeRateIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpWarehouseExchangeRateEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpWarehouseExchangeRate表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWarehouseExchangeRateEntity GetMeioErpWarehouseExchangeRateEntity(string keyValue);

        /// <summary>
        /// 汇率是否存在
        /// </summary>
        /// <param name="Month"></param>
        /// <param name="Currency"></param>
        /// <param name="ToCurrency"></param>
        /// <returns></returns>
        bool IsExistRate(string Month, string Currency, string ToCurrency);

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpWarehouseExchangeRateEntity entity,string deleteList,string type);
        #endregion

    }
}
