﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-10 17:11
    /// 描 述：汇率
    /// </summary>
    public class MeioErpWarehouseExchangeRateBLL : MeioErpWarehouseExchangeRateIBLL
    {
        private MeioErpWarehouseExchangeRateService meioErpWarehouseExchangeRateService = new MeioErpWarehouseExchangeRateService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseExchangeRateEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpWarehouseExchangeRateService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseExchangeRate表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseExchangeRateEntity GetMeioErpWarehouseExchangeRateEntity(string keyValue)
        {
            try
            {
                return meioErpWarehouseExchangeRateService.GetMeioErpWarehouseExchangeRateEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 汇率是否存在
        /// </summary>
        /// <param name="Month"></param>
        /// <param name="Currency"></param>
        /// <param name="ToCurrency"></param>
        /// <returns></returns>
        public bool IsExistRate(string Month, string Currency, string ToCurrency)
        {
            try
            {
                return meioErpWarehouseExchangeRateService.IsExistRate(Month, Currency, ToCurrency);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpWarehouseExchangeRateService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWarehouseExchangeRateEntity entity,string deleteList,string type)
        {
            try
            {
                meioErpWarehouseExchangeRateService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
