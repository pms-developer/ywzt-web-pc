﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using Newtonsoft.Json;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-10 17:11
    /// 描 述：汇率
    /// </summary>
    public class MeioErpWarehouseExchangeRateService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseExchangeRateEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.RateMonth,
                t.Currency,
                t.ToCurrency,
                t.SetRate,
                t.Notes
                ");
                strSql.Append("  FROM MeioErpWarehouseExchangeRate t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Currency"].IsEmpty())
                {
                    dp.Add("Currency", "%" + queryParam["Currency"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Currency Like @Currency ");
                }
                if (!queryParam["RateMonth"].IsEmpty())
                {
                    dp.Add("RateMonth",queryParam["RateMonth"].ToString(), DbType.String);
                    strSql.Append(" AND t.RateMonth = @RateMonth ");
                }
                var list=this.BaseRepository().FindList<MeioErpWarehouseExchangeRateEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseExchangeRate表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseExchangeRateEntity GetMeioErpWarehouseExchangeRateEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWarehouseExchangeRateEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 汇率是否存在
        /// </summary>
        /// <param name="Month"></param>
        /// <param name="Currency"></param>
        /// <param name="ToCurrency"></param>
        /// <returns></returns>
        public bool IsExistRate(string Month,string Currency,string ToCurrency)
        {
            int checkNum = this.BaseRepository().FindList<MeioErpWarehouseExchangeRateEntity>(x => x.RateMonth == Month && x.Currency == Currency && x.ToCurrency == ToCurrency).Count();
            return checkNum > 0;
        }


        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpWarehouseExchangeRateEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWarehouseExchangeRateEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        public void RefreshExchangeRate()
        {
            DateTime dt = DateTime.Now.Date;
            DateTime lastDt = dt.AddDays(-1);
            var d = dt.Month;
            //取昨天的计算今天的库龄
            var list = this.BaseRepository()
                .FindList<MeioErpWarehouseExchangeRateEntity>(x => x.RateMonth != null && x.RateMonth == d.ToString());
            var db = this.BaseRepository().BeginTrans();
            foreach (var item in list)
            {
                var temp = JsonConvert.DeserializeObject<MeioErpWarehouseExchangeRateEntity>(JsonConvert.SerializeObject(item));
                temp.RateMonth = dt.ToString() ;//新的统计日期
                var ts = temp.RateMonth = temp.RateMonth;
                db.Insert(temp);
            }
            db.Commit();
        }

    }
}
