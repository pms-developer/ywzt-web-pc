﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-12 14:30
    /// 描 述：手工账单
    /// </summary>
    public class MieoErpManualBillingService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MieoErpManualBillingEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.OwnerID,
                eo.Code OwnerCode,
                eo.Name OwnerName,
                t.WarehouseID,
                ew.Name WarehouseName,
                t.FeeType,
                t.BillItem,
                t.FeeItem,
                t.Currency,
                t.Amount,
                t.Cost,
                t.LinkCode,
                t.Atta,
                t.Notes,
                t.CreationDate,
                t.CreationName,
                t.IsAuditing
                ");
                strSql.Append("  FROM MieoErpManualBilling t left join MeioErpOwner eo on t.OwnerID=eo.ID ");
                strSql.Append("  left join MeioErpWarehouse ew on t.WarehouseID=ew.ID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["OwnerCode"].IsEmpty())
                {
                    dp.Add("OwnerCode", "%" + queryParam["OwnerCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND eo.Code Like @OwnerCode ");
                }
                if (!queryParam["OwnerName"].IsEmpty())
                {
                    dp.Add("OwnerName", "%" + queryParam["OwnerName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND eo.Name Like @OwnerName ");
                }
                if (!queryParam["FeeType"].IsEmpty())
                {
                    dp.Add("FeeType", "%" + queryParam["FeeType"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.FeeType Like @FeeType ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID",queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }

                var list=this.BaseRepository().FindList<MieoErpManualBillingEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MieoErpManualBilling表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MieoErpManualBillingEntity GetMieoErpManualBillingEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MieoErpManualBillingEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MieoErpManualBillingEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 审核实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void AuditingEntity(string keyValue)
        {
            try
            {
                var mb = this.BaseRepository().FindEntity<MieoErpManualBillingEntity>(t => t.ID == keyValue);
                if (mb.IsAuditing.GetValueOrDefault())
                {
                    throw new ExceptionEx("已审批不可重复审批", new Exception());
                }
                mb.IsAuditing = true;
                mb.Modify(mb.ID);
                this.BaseRepository().Update(mb);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MieoErpManualBillingEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
