﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-25 17:29
    /// 描 述：发票信息
    /// </summary>
    public class MeioErpInvoiceBLL : MeioErpInvoiceIBLL
    {
        private MeioErpInvoiceService meioErpInvoiceService = new MeioErpInvoiceService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpInvoiceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpInvoiceService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpInvoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpInvoiceEntity GetMeioErpInvoiceEntity(string keyValue)
        {
            try
            {
                return meioErpInvoiceService.GetMeioErpInvoiceEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 根据结算单号获取开票信息
        /// </summary>
        /// <param name="statementNo"></param>
        /// <returns></returns>
        public MeioErpInvoiceEntity GetMeioErpInvoiceEntityByStatementNo(string statementNo)
        {
            try
            {
                return meioErpInvoiceService.GetMeioErpInvoiceEntityByStatementNo(statementNo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpInvoiceService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpInvoiceEntity entity, string deleteList, string type)
        {
            try
            {
                meioErpInvoiceService.SaveEntity(keyValue, entity, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        public void ApplyInvoice(string statementNo, string ownerID, string invoiceTitle, string taxRegistrationNumber, string bankName, string bankAccount, string bankAddress,
            string applyInvoiceType, decimal applyTaxRate, decimal applyTaxAmount, decimal applyTotalAmount, string applyInvoiceRemark, out string error)
        {
            try
            {
                meioErpInvoiceService.ApplyInvoice(statementNo, ownerID,  invoiceTitle,  taxRegistrationNumber,  bankName,  bankAccount,  bankAddress,applyInvoiceType,applyTaxRate,applyTaxAmount,applyTotalAmount,applyInvoiceRemark,out error);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
