﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-25 17:29
    /// 描 述：发票信息
    /// </summary>
    public class MeioErpInvoiceEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 结算单ID
        /// </summary>
        [Column("STATEMENTID")]
        public string StatementID { get; set; }
        /// <summary>
        /// 结算单号
        /// </summary>
        [Column("STATEMENTNO")]
        public string StatementNo { get; set; }
        /// <summary>
        /// 账单金额
        /// </summary>
        [Column("ACCOUNTAMOUNT")]
        public decimal? AccountAmount { get; set; }
        /// <summary>
        /// 付款类型
        /// </summary>
        [Column("PAYTYPE")]
        public string PayType { get; set; }
        /// <summary>
        /// 付款币种
        /// </summary>
        [Column("PAYCURRENCY")]
        public string PayCurrency { get; set; }
        /// <summary>
        /// 发票抬头
        /// </summary>
        [Column("INVOICETITLE")]
        public string InvoiceTitle { get; set; }
        /// <summary>
        /// 税务登记号
        /// </summary>
        [Column("TAXREGISTRATIONNUMBER")]
        public string TaxRegistrationNumber { get; set; }
        /// <summary>
        /// 开户银行
        /// </summary>
        [Column("BANKNAME")]
        public string BankName { get; set; }
        /// <summary>
        /// 银行账号
        /// </summary>
        [Column("BANKACCOUNT")]
        public string BankAccount { get; set; }
        /// <summary>
        /// 银行地址
        /// </summary>
        [Column("BANKADDRESS")]
        public string BankAddress { get; set; }
        /// <summary>
        /// 货主ID
        /// </summary>
        [Column("OWNERID")]
        public string OwnerID { get; set; }
        /// <summary>
        /// 查账编号
        /// </summary>
        [Column("AUDITNUMBER")]
        public string AuditNumber { get; set; }
        /// <summary>
        /// 收款确认
        /// </summary>
        [Column("COLLECTIONCONFIRM")]
        public string CollectionConfirm { get; set; }
        /// <summary>
        /// 收款账户
        /// </summary>
        [Column("COLLECTIONACCOUNT")]
        public string CollectionAccount { get; set; }
        /// <summary>
        /// 电子发票
        /// </summary>
        [Column("ISUPLOADELECTRONICINVOICE")]
        public bool? IsUploadElectronicInvoice { get; set; }
        /// <summary>
        /// 发票图片url
        /// </summary>
        [Column("INVOICEIMAGEURL")]
        public string InvoiceImageUrl { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        [Column("UPLOADDATE")]
        public DateTime? UploadDate { get; set; }
        /// <summary>
        /// 上传人
        /// </summary>
        [Column("UPLOADPEOPLE")]
        public string UploadPeople { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary> 
        /// 发票类型 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICETYPE")]
        public string InvoiceType { get; set; }
        /// <summary> 
        /// 发票名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICENAME")]
        public string InvoiceName { get; set; }
        /// <summary> 
        /// 发票代码 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICECODE")]
        public string InvoiceCode { get; set; }
        /// <summary> 
        /// 发票号码 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICENUM")]
        public string InvoiceNum { get; set; }
        /// <summary> 
        /// 开票日期 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICEDATE")]
        public string InvoiceDate { get; set; }
        /// <summary> 
        /// 销售方名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("SELLERNAME")]
        public string SellerName { get; set; }
        /// <summary> 
        /// 销售方纳税人识别号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SELLERREGISTERNUM")]
        public string SellerRegisterNum { get; set; }
        /// <summary> 
        /// 销售方地址及电话 
        /// </summary> 
        /// <returns></returns> 
        [Column("SELLERADDRESS")]
        public string SellerAddress { get; set; }
        /// <summary> 
        /// 销售方开户行及账号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SELLERBANK")]
        public string SellerBank { get; set; }
        /// <summary> 
        /// 合计金额 
        /// </summary> 
        /// <returns></returns> 
        [Column("TOTALAMOUNT")]
        public decimal? TotalAmount { get; set; }
        /// <summary> 
        /// 合计税额 
        /// </summary> 
        /// <returns></returns> 
        [Column("TOTALTAX")]
        public decimal? TotalTax { get; set; }
        /// <summary>
        /// 申请开票类型
        /// </summary>
        [Column("APPLYINVOICETYPE")]
        public string ApplyInvoiceType { get; set; }
        /// <summary>
        /// 申请开票税额
        /// </summary>
        [Column("APPLYTAXRATE")]
        public decimal? ApplyTaxRate { get; set; }
        /// <summary>
        /// 申请开票金额
        /// </summary>
        [Column("APPLYTAXAMOUNT")]
        public decimal? ApplyTaxAmount { get; set; }
        /// <summary>
        /// 申请合计开票金额
        /// </summary>
        [Column("APPLYTOTALAMOUNT")]
        public decimal? ApplyTotalAmount { get; set; }
        /// <summary>
        /// 申请开票备注
        /// </summary>
        [Column("APPLYINVOICEREMARK")]
        public string ApplyInvoiceRemark { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            if (userInfo != null)
            {
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
                this.Company_ID = userInfo.companyId;
                this.CompanyCode = userInfo.CompanyCode;
                this.CompanyName = userInfo.CompanyName;
                this.CompanyFullName = userInfo.CompanyFullName;
            }
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            if (userInfo != null)
            {
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
                this.Company_ID = userInfo.companyId;
                this.CompanyCode = userInfo.CompanyCode;
                this.CompanyName = userInfo.CompanyName;
                this.CompanyFullName = userInfo.CompanyFullName;
            }
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            if (userInfo != null)
            {
                this.Modification_Id = userInfo.userId;
                this.ModificationName = userInfo.realName;
            }
        }
        #endregion
        #region  扩展字段
        [NotMapped]
        public bool? IsCollectionConfirm { get; set; }
        #endregion
    }
}

