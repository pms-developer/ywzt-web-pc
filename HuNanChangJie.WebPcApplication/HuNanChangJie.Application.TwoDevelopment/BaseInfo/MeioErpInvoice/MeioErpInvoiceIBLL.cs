﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-25 17:29
    /// 描 述：发票信息
    /// </summary>
    public interface MeioErpInvoiceIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpInvoiceEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpInvoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpInvoiceEntity GetMeioErpInvoiceEntity(string keyValue);
        MeioErpInvoiceEntity GetMeioErpInvoiceEntityByStatementNo(string statementNo);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpInvoiceEntity entity, string deleteList, string type);
        #endregion
        void ApplyInvoice(string statementNo, string ownerID, string invoiceTitle, string taxRegistrationNumber, string bankName, string bankAccount, string bankAddress,
            string applyInvoiceType, decimal applyTaxRate, decimal applyTaxAmount,decimal applyTotalAmount, string applyInvoiceRemark, out string error);
    }
}
