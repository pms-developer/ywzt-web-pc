﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-25 17:29
    /// 描 述：发票信息
    /// </summary>
    public class MeioErpInvoiceService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpInvoiceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t1.Code StatementNo,
                t1.StatementAmount AccountAmount,
                t1.PayMethod PayType,
                t1.Currency PayCurrency,
                t.InvoiceTitle,
                t.TaxRegistrationNumber,
                t.BankName,
                t.BankAccount,
                t.OwnerID,
                t.CreationDate,
                t1.IsCollectionConfirm,
                t1.CollectionAccount CollectionAccount,
                t1.AuditNumber AuditNumber,
                t.IsUploadElectronicInvoice,
                t.InvoiceImageUrl,
                t.UploadDate,
                t.UploadPeople,
                t.ApplyInvoiceType,
                t.ApplyTaxRate,
                t.ApplyTaxAmount,
                t.ApplyTotalAmount,
                t.ApplyInvoiceRemark
                ");
                strSql.Append("  FROM MeioErpInvoice t ");
                strSql.Append(" LEFT JOIN MeioErpStatement t1 ON t.StatementID=t1.ID ");
                strSql.Append(" WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["NotID"].IsEmpty())
                {
                    dp.Add("NotID", queryParam["NotID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ID!=@NotID ");
                }
                else
                {
                    strSql.Append("  AND  EXISTS (SELECT t.StatementID,MAX(t2.CreationDate)   FROM MeioErpInvoice t2  group by t2.StatementID having t2.StatementID = t.StatementID and MAX(t2.CreationDate)=t.CreationDate ) ");
                }
                if (!queryParam["StatementNo"].IsEmpty())
                {
                    dp.Add("StatementNo", queryParam["StatementNo"].ToString(), DbType.String);
                    strSql.Append(" AND t1.Code = @StatementNo ");
                }
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID", queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                if (!queryParam["IsUploadElectronicInvoice"].IsEmpty())
                {
                    dp.Add("IsUploadElectronicInvoice", queryParam["IsUploadElectronicInvoice"].ToString(), DbType.String);
                    strSql.Append(" AND t.IsUploadElectronicInvoice = @IsUploadElectronicInvoice ");
                }
                strSql.Append(" ORDER BY t.CreationDate DESC");
                var list = this.BaseRepository().FindList<MeioErpInvoiceEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpInvoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpInvoiceEntity GetMeioErpInvoiceEntity(string keyValue)
        {
            try
            {
                var invoice = this.BaseRepository().FindEntity<MeioErpInvoiceEntity>(keyValue);
                if (invoice != null)
                {
                    var statement = this.BaseRepository().FindEntity<MeioErpStatementEntity>(invoice.StatementID);
                    if (statement != null)
                    {
                        invoice.StatementNo = statement.Code;
                        invoice.AccountAmount = statement.StatementAmount;
                        invoice.PayType = statement.PayMethod;
                        invoice.PayCurrency = statement.Currency;
                        invoice.CollectionAccount = statement.CollectionAccount;
                        invoice.AuditNumber = statement.AuditNumber;
                        invoice.CollectionConfirm = statement.IsCollectionConfirm.Value ? "已确认" : "待确认";
                    }
                }
                return invoice;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据结算单号获取开票信息
        /// </summary>
        /// <param name="statementNo"></param>
        /// <returns></returns>
        public MeioErpInvoiceEntity GetMeioErpInvoiceEntityByStatementNo(string statementNo)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpInvoiceEntity>(p => p.StatementNo == statementNo).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpInvoiceEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpInvoiceEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 申请开票
        /// </summary>
        /// <param name="statementNo"></param>
        /// <param name="ownerID"></param>
        /// <param name="invoiceTitle"></param>
        /// <param name="taxRegistrationNumber"></param>
        /// <param name="bankName"></param>
        /// <param name="bankAccount"></param>
        /// <param name="bankAddress"></param>
        public void ApplyInvoice(string statementNo, string ownerID, string invoiceTitle, string taxRegistrationNumber, string bankName, string bankAccount, string bankAddress,
            string applyInvoiceType,decimal applyTaxRate,decimal applyTaxAmount,decimal applyTotalAmount,string applyInvoiceRemark, out string error)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                error = "";
                //var ownerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(ownerID);
                //var contractEntity = this.BaseRepository().FindList<MeioErpContractEntity>(p => p.ClientCode == ownerEntity.Code).FirstOrDefault();
                var statementEntity = this.BaseRepository().FindList<MeioErpStatementEntity>(p => p.Code == statementNo && p.OwnerID == ownerID).FirstOrDefault();
                if (statementEntity != null)
                {
                    MeioErpInvoiceEntity meioErpInvoiceEntity = new MeioErpInvoiceEntity
                    {
                        StatementID = statementEntity.ID,
                        //StatementNo = statementEntity.Code,
                        //AccountAmount = statementEntity.StatementAmount,
                        //PayType = statementEntity.PayMethod,
                        //PayCurrency = statementEntity.Currency,
                        InvoiceTitle = invoiceTitle,
                        TaxRegistrationNumber = taxRegistrationNumber,
                        BankName = bankName,
                        BankAccount = bankAccount,
                        BankAddress = bankAddress,
                        OwnerID = ownerID,
                        //AuditNumber = statementEntity.AuditNumber,
                        //CollectionConfirm = statementEntity.IsCollectionConfirm.Value ? "已确认" : "待确认",
                        //CollectionAccount = statementEntity.CollectionAccount,
                        IsUploadElectronicInvoice = false,
                        ApplyInvoiceType = applyInvoiceType,
                        ApplyTaxRate = applyTaxRate,
                        ApplyTaxAmount= applyTaxAmount,
                        ApplyTotalAmount= applyTotalAmount,
                        ApplyInvoiceRemark= applyInvoiceRemark,
                    };
                    //SaveEntity("", meioErpInvoiceEntity, "", "add");
                    meioErpInvoiceEntity.Create();
                    db.Insert(meioErpInvoiceEntity);

                    statementEntity.IsInvoice = true;
                    statementEntity.Modify(statementEntity.ID);
                    db.Update(statementEntity);
                }
                else
                {
                    error = "不存在此结算单，申请开票失败";
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
