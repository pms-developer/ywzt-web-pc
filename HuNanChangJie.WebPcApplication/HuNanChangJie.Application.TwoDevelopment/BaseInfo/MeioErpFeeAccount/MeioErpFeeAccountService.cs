﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-14 18:07
    /// 描 述：账单信息
    /// </summary>
    public class MeioErpFeeAccountService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpFeeAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.AssociatedNumber,
                t.WarehouseID,
                t.OwnerID,
                t.ItemName,
                t.ExpenseItem,
                t.PriceSheetID,
                t.Currency,
                t.TotalAmount,
                t.CalculatedAmount,
                t.AdjustmentAmount,
                t.SettlementStatus,
                t.SettlementTime,
                t.AuditStatus,
                t.AuditorName,
                t.AuditTime,
                t.Remark,
                t.AttachmentUrl
                ");
                strSql.Append("  FROM MeioErpFeeAccount t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID",queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID = @OwnerID ");
                }
                if (!queryParam["AssociatedNumber"].IsEmpty())
                {
                    dp.Add("AssociatedNumber", "%" + queryParam["AssociatedNumber"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.AssociatedNumber Like @AssociatedNumber ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID",queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    dp.Add("AuditStatus",queryParam["AuditStatus"].ToString(), DbType.String);
                    strSql.Append(" AND t.AuditStatus = @AuditStatus ");
                }
                if (!queryParam["SettlementStatus"].IsEmpty())
                {
                    dp.Add("SettlementStatus",queryParam["SettlementStatus"].ToString(), DbType.String);
                    strSql.Append(" AND t.SettlementStatus = @SettlementStatus ");
                }
                var list=this.BaseRepository().FindList<MeioErpFeeAccountEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpFeeAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpFeeAccountEntity GetMeioErpFeeAccountEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpFeeAccountEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpFeeAccountEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpFeeAccountEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
