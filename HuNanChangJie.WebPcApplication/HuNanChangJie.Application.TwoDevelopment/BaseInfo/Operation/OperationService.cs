﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 19:00
    /// 描 述：作业档案信息
    /// </summary>
    public class OperationService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpOperationEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.ItemName,
                t.Type,
                t.Unit,
                t.IsValid,
                t.Remark,
                t.ExpenseType,
                t.ExpenseItem,
                t.Conditions,
                t.ChargePoint,
                t.WeightUnit,
                t.IsAutomaticAudit,
                t.CreatedBy,
                t.CreatedDate
                ");
                strSql.Append("  FROM MeioErpOperation t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ItemName"].IsEmpty())
                {
                    dp.Add("ItemName", "%" + queryParam["ItemName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ItemName Like @ItemName ");
                }
                if (!queryParam["IsValid"].IsEmpty())
                {
                    dp.Add("IsValid", queryParam["IsValid"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsValid=@IsValid");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type = @Type ");
                }
                if (!queryParam["ExpenseType"].IsEmpty())
                {
                    dp.Add("ExpenseType", queryParam["ExpenseType"].ToString(), DbType.String);
                    strSql.Append(" AND t.ExpenseType=@ExpenseType ");
                }
                if (!queryParam["ChargePoint"].IsEmpty())
                {
                    dp.Add("ChargePoint", queryParam["ChargePoint"].ToString(), DbType.String);
                    strSql.Append(" AND t.ChargePoint=@ChargePoint ");
                }
                strSql.Append(" ORDER BY t.CreatedDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpOperationEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreatedDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpOperationEntity> GetListByIds(string ids)
        {
            try
            {
                var strIds = ids.Split(',');
                var list = this.BaseRepository().FindList<MeioErpOperationEntity>(p => strIds.Contains(p.ID));
                var query = from item in list orderby item.CreatedDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpOperationDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpOperationDetailEntity> GetMeioErpOperationDetailList(string keyValue)
        {
            try
            {
                IEnumerable<MeioErpOperationDetailEntity> list = null;
                var strIds = keyValue.Split(',');
                if (strIds.Length > 1)
                    list = this.BaseRepository().FindList<MeioErpOperationDetailEntity>(t => strIds.Contains(t.MeioErpOperationID));
                else
                    list = this.BaseRepository().FindList<MeioErpOperationDetailEntity>(t => t.MeioErpOperationID == keyValue);
                //var query = from item in list orderby item.CreatedDate ascending select item;
                var query = from item in list orderby item.MinValue ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public IEnumerable<MeioErpOperationDetailEntity> GetMeioErpOperationDetailListByOperationIDs(string ids)
        {
            try
            {
                List<string> idList = ids.Split(',').ToList<string>();
                var list = this.BaseRepository().FindList<MeioErpOperationDetailEntity>(t => idList.Contains(t.MeioErpOperationID));
                var query = from item in list orderby item.CreatedDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpOperation表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpOperationEntity GetMeioErpOperationEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpOperationEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpOperationDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpOperationDetailEntity GetMeioErpOperationDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpOperationDetailEntity>(t => t.MeioErpOperationID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpDeleteLogEntity> GetMeioErpDeleteOperationList(string itemName)
        {
            try
            {
                if (string.IsNullOrEmpty(itemName))
                {
                    return this.BaseRepository().FindList<MeioErpDeleteLogEntity>(t => t.Model == "MeioErpOperation");
                }
                else
                {
                    return this.BaseRepository().FindList<MeioErpDeleteLogEntity>(t => t.Model == "MeioErpOperation" && t.Code == itemName);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpOperationEntity = GetMeioErpOperationEntity(keyValue);
                db.Delete<MeioErpOperationEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpOperationDetailEntity>(t => t.MeioErpOperationID == meioErpOperationEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpOperationEntity entity, List<MeioErpOperationDetailEntity> meioErpOperationDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    db.Update(entity);

                    db.Delete<MeioErpOperationDetailEntity>(p => p.MeioErpOperationID == keyValue);
                    foreach (var item in meioErpOperationDetailList)
                    {
                        item.Create();
                        item.MeioErpOperationID = keyValue;
                        db.Insert(item);
                    }
                    
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (MeioErpOperationDetailEntity item in meioErpOperationDetailList)
                    {
                        item.Create();
                        item.MeioErpOperationID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        internal bool CheckIsItemName(string name, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpOperationEntity>(t => t.ItemName.Trim().ToLower() == name.Trim().ToLower() && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public bool SetIsEnable(string keyValue, bool isValid)
        {
            try
            {
                var strIds = "'" + keyValue.Replace(",", "','") + "'";
                UserInfo userInfo = LoginUserInfo.Get();
                int count = this.BaseRepository().ExecuteBySql($"UPDATE dbo.MeioErpOperation Set IsValid={(isValid ? 1 : 0)},ModifyDate=GETDATE(),ModifyBy='{userInfo.realName}' WHERE ID IN ({strIds})");
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
