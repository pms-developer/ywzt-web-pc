﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 19:00
    /// 描 述：作业档案信息
    /// </summary>
    public class OperationBLL : OperationIBLL
    {
        private OperationService operationService = new OperationService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpOperationEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return operationService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public IEnumerable<MeioErpOperationEntity> GetListByIds(string ids)
        {
            try
            {
                return operationService.GetListByIds(ids);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpOperationDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpOperationDetailEntity> GetMeioErpOperationDetailList(string keyValue)
        {
            try
            {
                return operationService.GetMeioErpOperationDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public IEnumerable<MeioErpOperationDetailEntity> GetMeioErpOperationDetailListByOperationIDs(string ids)
        {
            try
            {
                return operationService.GetMeioErpOperationDetailListByOperationIDs(ids);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpOperation表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpOperationEntity GetMeioErpOperationEntity(string keyValue)
        {
            try
            {
                return operationService.GetMeioErpOperationEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpOperationDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpOperationDetailEntity GetMeioErpOperationDetailEntity(string keyValue)
        {
            try
            {
                return operationService.GetMeioErpOperationDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<MeioErpDeleteLogEntity> GetMeioErpDeleteOperationList(string itemName)
        {
            try
            {
                return operationService.GetMeioErpDeleteOperationList(itemName);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue, string deleteList)
        {
            try
            {
                operationService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpOperationEntity entity,List<MeioErpOperationDetailEntity> meioErpOperationDetailList,string deleteList,string type)
        {
            try
            {
                operationService.SaveEntity(keyValue, entity,meioErpOperationDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public bool CheckIsItemName(string name, string id)
        {
            return operationService.CheckIsItemName(name,id);
        }
        public bool SetIsEnable(string keyValue, bool isValid)
        {
            try
            {
                return operationService.SetIsEnable(keyValue, isValid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
