﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 19:00
    /// 描 述：作业档案信息
    /// </summary>
    public class MeioErpOperationDetailEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// MeioErpOperationID
        /// </summary>
        [Column("MEIOERPOPERATIONID")]
        public string MeioErpOperationID { get; set; }
        /// <summary>
        /// ItemName
        /// </summary>
        [Column("ITEMNAME")]
        public string ItemName { get; set; }
        /// <summary>
        /// MinValue
        /// </summary>
        [Column("MINVALUE")]
        public int? MinValue { get; set; }
        /// <summary>
        /// MaxValue
        /// </summary>
        [Column("MAXVALUE")]
        public int? MaxValue { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// CreatedBy
        /// </summary>
        [Column("CREATEDBY")]
        public string CreatedBy { get; set; }
        /// <summary>
        /// CreatedDate
        /// </summary>
        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// ModifyBy
        /// </summary>
        [Column("MODIFYBY")]
        public string ModifyBy { get; set; }
        /// <summary>
        /// ModifyDate
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreatedDate=DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModifyDate=DateTime.Now;
            this.ModifyBy = LoginUserInfo.UserInfo.realName;
        }
        #endregion
    }
}

