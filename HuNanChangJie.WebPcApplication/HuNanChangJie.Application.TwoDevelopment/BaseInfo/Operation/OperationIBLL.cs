﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 19:00
    /// 描 述：作业档案信息
    /// </summary>
    public interface OperationIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpOperationEntity> GetPageList(XqPagination pagination, string queryJson);
        IEnumerable<MeioErpOperationEntity> GetListByIds(string ids);
        /// <summary>
        /// 获取MeioErpOperationDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpOperationDetailEntity> GetMeioErpOperationDetailList(string keyValue);
        IEnumerable<MeioErpOperationDetailEntity> GetMeioErpOperationDetailListByOperationIDs(string ids);
        /// <summary>
        /// 获取MeioErpOperation表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpOperationEntity GetMeioErpOperationEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpOperationDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpOperationDetailEntity GetMeioErpOperationDetailEntity(string keyValue);
        IEnumerable<MeioErpDeleteLogEntity> GetMeioErpDeleteOperationList(string itemName);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue,string deleteList);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpOperationEntity entity,List<MeioErpOperationDetailEntity> meioErpOperationDetailList,string deleteList,string type);
        #endregion
        bool CheckIsItemName(string name, string id);
        bool SetIsEnable(string keyValue, bool isValid);
    }
}
