﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-07 14:39
    /// 描 述：账单附加费用明细
    /// </summary>
    public class MeioErpAccountAdditionalCostService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpAccountAdditionalCostEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Quantity,
                t.AmountSubtotal,
                t.ExpenseDetail,
                t.Remark
                ");
                strSql.Append("  FROM MeioErpAccountAdditionalCost t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ExpenseDetail"].IsEmpty())
                {
                    dp.Add("ExpenseDetail", "%" + queryParam["ExpenseDetail"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ExpenseDetail Like @ExpenseDetail ");
                }
                var list=this.BaseRepository().FindList<MeioErpAccountAdditionalCostEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpAccountAdditionalCost表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpAccountAdditionalCostEntity GetMeioErpAccountAdditionalCostEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpAccountAdditionalCostEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public MeioErpAccountAdditionalCostEntity GetMeioErpAccountAdditionalCostEntityByAccountIDAndType(string accountId,int type)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpAccountAdditionalCostEntity>(p=>p.AccountID==accountId && p.AccountType==type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpAccountAdditionalCostEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpAccountAdditionalCostEntity entity,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var oldAccountAdditionalCost= this.BaseRepository().FindEntity<MeioErpAccountAdditionalCostEntity>(keyValue);

                    entity.Modify(keyValue);
                    db.Update(entity);

                    //修改账单费用
                    if (entity.AccountType == 1)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpWarehouseRentAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.TotalAmount = account.TotalAmount- oldAccountAdditionalCost.AmountSubtotal + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                    else if (entity.AccountType == 2)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpLogisticsPackageAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.TotalAmount = account.TotalAmount - oldAccountAdditionalCost.AmountSubtotal + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                    else if (entity.AccountType == 3)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpInStorageAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.TotalAmount = account.TotalAmount - oldAccountAdditionalCost.AmountSubtotal + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                    else if (entity.AccountType == 4)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpDeliveryAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.TotalAmount = account.TotalAmount - oldAccountAdditionalCost.AmountSubtotal + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                    else if (entity.AccountType == 5)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpCheckInventoryAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.OperatingCost = account.OperatingCost - oldAccountAdditionalCost.AmountSubtotal + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);

                    //修改账单费用
                    if (entity.AccountType == 1)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpWarehouseRentAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.TotalAmount = account.TotalAmount + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                    else if (entity.AccountType == 2)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpLogisticsPackageAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.TotalAmount = account.TotalAmount + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                    else if (entity.AccountType == 3)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpInStorageAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.TotalAmount = account.TotalAmount + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                    else if (entity.AccountType == 4)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpDeliveryAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.TotalAmount = account.TotalAmount + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                    else if (entity.AccountType == 5)
                    {
                        var account = this.BaseRepository().FindEntity<MeioErpCheckInventoryAccountEntity>(entity.AccountID);
                        if (account != null)
                        {
                            account.OperatingCost = account.OperatingCost + entity.AmountSubtotal;
                            account.Modify(entity.AccountID);
                            db.Update(account);
                        }
                    }
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
