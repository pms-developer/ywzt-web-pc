﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-07 14:39
    /// 描 述：账单附加费用明细
    /// </summary>
    public interface MeioErpAccountAdditionalCostIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpAccountAdditionalCostEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpAccountAdditionalCost表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpAccountAdditionalCostEntity GetMeioErpAccountAdditionalCostEntity(string keyValue);
        MeioErpAccountAdditionalCostEntity GetMeioErpAccountAdditionalCostEntityByAccountIDAndType(string accountId, int type);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpAccountAdditionalCostEntity entity,string deleteList,string type);
        #endregion

    }
}
