﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-07 14:39
    /// 描 述：账单附加费用明细
    /// </summary>
    public class MeioErpAccountAdditionalCostBLL : MeioErpAccountAdditionalCostIBLL
    {
        private MeioErpAccountAdditionalCostService meioErpAccountAdditionalCostService = new MeioErpAccountAdditionalCostService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpAccountAdditionalCostEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpAccountAdditionalCostService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpAccountAdditionalCost表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpAccountAdditionalCostEntity GetMeioErpAccountAdditionalCostEntity(string keyValue)
        {
            try
            {
                return meioErpAccountAdditionalCostService.GetMeioErpAccountAdditionalCostEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public MeioErpAccountAdditionalCostEntity GetMeioErpAccountAdditionalCostEntityByAccountIDAndType(string accountId, int type)
        {
            try
            {
                return meioErpAccountAdditionalCostService.GetMeioErpAccountAdditionalCostEntityByAccountIDAndType(accountId, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpAccountAdditionalCostService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpAccountAdditionalCostEntity entity,string deleteList,string type)
        {
            try
            {
                meioErpAccountAdditionalCostService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
