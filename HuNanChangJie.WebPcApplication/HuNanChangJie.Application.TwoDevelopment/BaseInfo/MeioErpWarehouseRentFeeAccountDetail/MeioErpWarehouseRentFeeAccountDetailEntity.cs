﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-13 21:04
    /// 描 述：仓储费账单明细
    /// </summary>
    public class MeioErpWarehouseRentFeeAccountDetailEntity 
    {
        #region  实体成员
        /// <summary>
        /// 仓储费账单ID
        /// </summary>
        /// <returns></returns>
        [Column("WAREHOUSERENTFEEACCOUNTID")]
        public string WarehouseRentFeeAccountID { get; set; }
        /// <summary>
        /// 计费项ID
        /// </summary>
        /// <returns></returns>
        [Column("OPERATIONID")]
        public string OperationID { get; set; }
        /// <summary>
        /// 报价单ID
        /// </summary>
        /// <returns></returns>
        [Column("PRICESHEETID")]
        public string PriceSheetID { get; set; }
        /// <summary>
        /// 计费项名称
        /// </summary>
        /// <returns></returns>
        [Column("ITEMNAME")]
        public string ItemName { get; set; }
        /// <summary>
        /// 费用项目
        /// </summary>
        /// <returns></returns>
        [Column("EXPENSEITEM")]
        public string ExpenseItem { get; set; }
        /// <summary>
        /// 条件项
        /// </summary>
        /// <returns></returns>
        [Column("CONDITIONS")]
        public string Conditions { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        /// <returns></returns>
        [Column("BATCHCODE")]
        public string BatchCode { get; set; }
        /// <summary>
        /// SKU
        /// </summary>
        /// <returns></returns>
        [Column("SKU")]
        public string SKU { get; set; }
        /// <summary>
        /// 品名
        /// </summary>
        /// <returns></returns>
        [Column("PRODUCTNAME")]
        public string ProductName { get; set; }
        /// <summary>
        /// 货品分类
        /// </summary>
        /// <returns></returns>
        [Column("PRODUCTTYPE")]
        public string ProductType { get; set; }
        /// <summary>
        /// 入库日期
        /// </summary>
        /// <returns></returns>
        [Column("INSTORAGEDATE")]
        public DateTime? InStorageDate { get; set; }
        /// <summary>
        /// 出库日期
        /// </summary>
        /// <returns></returns>
        [Column("DELIVERYDATE")]
        public DateTime? DeliveryDate { get; set; }
        /// <summary>
        /// 体积
        /// </summary>
        /// <returns></returns>
        [Column("VOLUME")]
        public decimal? Volume { get; set; }
        /// <summary>
        /// 重量
        /// </summary>
        /// <returns></returns>
        [Column("WEIGHT")]
        public decimal? Weight { get; set; }
        /// <summary>
        /// 在库天数
        /// </summary>
        /// <returns></returns>
        [Column("STOCKAGE")]
        public int? StockAge { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        /// <returns></returns>
        [Column("QUANTITY")]
        public int? Quantity { get; set; }
        /// <summary>
        /// 收费标准
        /// </summary>
        /// <returns></returns>
        [Column("UNIT")]
        public decimal? Unit { get; set; }
        /// <summary>
        /// 费用
        /// </summary>
        /// <returns></returns>
        [Column("EXPENSE")]
        public decimal? Expense { get; set; }
        /// <summary>
        /// 币种
        /// </summary>
        /// <returns></returns>
        [Column("CURRENCY")]
        public string Currency { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        /// <returns></returns>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        /// <returns></returns>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        /// <returns></returns>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        /// <returns></returns>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        /// <returns></returns>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        /// <returns></returns>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        /// <returns></returns>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        /// <returns></returns>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        /// <returns></returns>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        /// <returns></returns>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        /// <returns></returns>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        /// <returns></returns>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        /// <returns></returns>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        /// <returns></returns>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
    }
}

