﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-10 16:06
    /// 描 述：报价单管理
    /// </summary>
    public class PriceSheetBLL : PriceSheetIBLL
    {
        private PriceSheetService priceSheetService = new PriceSheetService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpPriceSheetEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return priceSheetService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPriceSheetDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpPriceSheetDetailEntity> GetMeioErpPriceSheetDetailList(string keyValue)
        {
            try
            {
                return priceSheetService.GetMeioErpPriceSheetDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPriceSheetDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPriceSheetDetailEntity GetMeioErpPriceSheetDetailEntity(string keyValue)
        {
            try
            {
                return priceSheetService.GetMeioErpPriceSheetDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPriceSheet表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPriceSheetEntity GetMeioErpPriceSheetEntity(string keyValue)
        {
            try
            {
                return priceSheetService.GetMeioErpPriceSheetEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取删除报价单日志信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IEnumerable<MeioErpDeleteLogEntity> GetMeioErpDeletePriceSheetList(string name)
        {
            try
            {
                return priceSheetService.GetMeioErpDeletePriceSheetList(name);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue, string deleteList)
        {
            try
            {
                priceSheetService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpPriceSheetEntity entity, List<MeioErpPriceSheetDetailEntity> meioErpPriceSheetDetailList, string deleteList, string type)
        {
            try
            {
                priceSheetService.SaveEntity(keyValue, entity, meioErpPriceSheetDetailList, deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("PriceSheetCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public bool SetIsEnable(string keyValue, bool isValid)
        {
            try
            {
                return priceSheetService.SetIsValid(keyValue, isValid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool IsEditForm(string itemId)
        {
            try
            {
                return priceSheetService.IsEditForm(itemId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public bool CheckIsCode(string code, string id)
        {
            try
            {
                return priceSheetService.CheckIsCode(code,id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
