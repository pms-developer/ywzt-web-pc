﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-10 16:06
    /// 描 述：报价单管理
    /// </summary>
    public class PriceSheetService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpPriceSheetEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Type,
                t.EffectiveTime,
                t.ExpirationTime,
                t.IsValid,
                t.CreatedBy,
                t.CreatedDate,
                t.Currency,
                t.WarehouseID
                ");
                strSql.Append("  FROM MeioErpPriceSheet t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type = @Type ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["IsValid"].IsEmpty())
                {
                    dp.Add("IsValid", queryParam["IsValid"].ToString(), DbType.String);
                    strSql.Append(" AND t.IsValid = @IsValid ");
                }
                strSql.Append(" ORDER BY t.CreatedDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpPriceSheetEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreatedDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPriceSheetDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpPriceSheetDetailEntity> GetMeioErpPriceSheetDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpPriceSheetDetailEntity>(t => t.PriceSheetID == keyValue);
                var query = from item in list orderby item.CreatedDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取MeioErpPriceSheetDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPriceSheetDetailEntity GetMeioErpPriceSheetDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpPriceSheetDetailEntity>(t => t.PriceSheetID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPriceSheet表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPriceSheetEntity GetMeioErpPriceSheetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpPriceSheetEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取删除报价单日志信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IEnumerable<MeioErpDeleteLogEntity> GetMeioErpDeletePriceSheetList(string name)
        {
            try
            {
                if (string.IsNullOrEmpty(name))
                {
                    return this.BaseRepository().FindList<MeioErpDeleteLogEntity>(t => t.Model == "MeioErpPriceSheet");
                }
                else
                {
                    return this.BaseRepository().FindList<MeioErpDeleteLogEntity>(t => t.Model == "MeioErpPriceSheet" && t.Code == name);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpPriceSheetEntity = GetMeioErpPriceSheetEntity(keyValue);
                db.Delete<MeioErpPriceSheetDetailEntity>(t => t.PriceSheetID == meioErpPriceSheetEntity.ID);
                db.Delete<MeioErpPriceSheetEntity>(t => t.ID == keyValue);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpPriceSheetEntity entity, List<MeioErpPriceSheetDetailEntity> meioErpPriceSheetDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    #region

                    //var meioErpPriceSheetEntityTmp = GetMeioErpPriceSheetEntity(keyValue); 
                    //entity.Modify(keyValue);
                    //db.Update(entity);
                    //var delJson=deleteList.ToObject<List<JObject>>();
                    //foreach (var del in delJson)
                    //{
                    //    var idList = del["idList"].ToString();
                    //    var sql = "";
                    //    if (idList.Contains(","))
                    //    {
                    //        var ids = idList.Split(',');
                    //        foreach (var id in ids)
                    //        {
                    //            sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (!string.IsNullOrWhiteSpace(idList))
                    //        {
                    //            sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //}

                    ////没有生成代码 
                    //var MeioErpPriceSheetDetailUpdateList= meioErpPriceSheetDetailList.FindAll(i=>i.EditType==EditType.Update);
                    //foreach (var item in MeioErpPriceSheetDetailUpdateList)
                    //{
                    //    db.Update(item);
                    //}
                    //var MeioErpPriceSheetDetailInserList= meioErpPriceSheetDetailList.FindAll(i=>i.EditType==EditType.Add);
                    //foreach (var item in MeioErpPriceSheetDetailInserList)
                    //{
                    //    //item.Create(item.ID);
                    //    item.Create();
                    //    item.PriceSheetID = meioErpPriceSheetEntityTmp.ID;
                    //    db.Insert(item);
                    //}

                    ////没有生成代码1 
                    #endregion

                    var meioErpPriceSheetEntityTmp = GetMeioErpPriceSheetEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);

                    db.Delete<MeioErpPriceSheetDetailEntity>(t => t.PriceSheetID == keyValue);
                    foreach (MeioErpPriceSheetDetailEntity item in meioErpPriceSheetDetailList)
                    {
                        item.Create();
                        item.PriceSheetID = keyValue;
                        db.Insert(item);
                    }
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                    foreach (MeioErpPriceSheetDetailEntity item in meioErpPriceSheetDetailList)
                    {
                        item.Create();
                        item.PriceSheetID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        internal bool SetIsValid(string keyValue, bool isValid)
        {
            try
            {
                var strIds = "'" + keyValue.Replace(",", "','") + "'";
                return this.BaseRepository().ExecuteBySql($"UPDATE dbo.MeioErpPriceSheet Set IsValid={(isValid ? 1 : 0)} WHERE ID IN ({strIds})") > 0;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal bool IsEditForm(string itemId)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpPriceSheetDetailEntity>(t => t.ItemID==itemId);
                return list.Count() > 0;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        internal bool CheckIsCode(string code, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpPriceSheetEntity>(t => t.Code == code && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
