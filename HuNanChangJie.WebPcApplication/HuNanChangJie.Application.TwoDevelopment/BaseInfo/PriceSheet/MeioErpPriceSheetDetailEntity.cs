﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-10 16:06
    /// 描 述：报价单管理
    /// </summary>
    public class MeioErpPriceSheetDetailEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// PriceSheetID
        /// </summary>
        [Column("PRICESHEETID")]
        public string PriceSheetID { get; set; }
        /// <summary>
        /// 报价单项类型：1（仓库租金阶梯维护）2（发货阶梯维护）3（增值服务档案）
        /// </summary>
        [Column("ITEMTYPE")]
        public int? ItemType { get; set; }
        /// <summary>
        /// 报价单项ID
        /// </summary>
        [Column("ITEMID")]
        public string ItemID { get; set; }
        /// <summary>
        /// 报价单项明细ID
        /// </summary>
        [Column("ITEMDETAILID")]
        public string ItemDetailID { get; set; }
        /// <summary>
        /// 报价单项说明
        /// </summary>
        [Column("ITEMNAME")]
        public string ItemName { get; set; }
        /// <summary>
        /// ItemDetailName
        /// </summary>
        [Column("ITEMDETAILNAME")]
        public string ItemDetailName { get; set; }
        /// <summary>
        /// 报价单项明细最小值
        /// </summary>
        [Column("ITEMDETAILMINVALUE")]
        public int? ItemDetailMinValue { get; set; }
        /// <summary>
        /// 报价单项明细最大值
        /// </summary>
        [Column("ITEMDETAILMAXVALUE")]
        public int? ItemDetailMaxValue { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 定价
        /// </summary>
        [Column("PRICE")]
        public decimal? Price { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        [Column("CREATEDBY")]
        public string CreatedBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        [Column("MODIFYBY")]
        public string ModifyBy { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// 作业点
        /// </summary>
        [Column("OPERATIONPOINT")]
        public int? OperationPoint { get; set; }
        /// <summary>
        /// 费用项目
        /// </summary>
        [Column("EXPENSEITEM")]
        public string ExpenseItem { get; set; }
        /// <summary>
        /// 条件项
        /// </summary>
        [Column("CONDITIONS")]
        public string Conditions { get; set; }
        /// <summary>
        /// 费用类型
        /// </summary>
        [Column("EXPENSETYPE")]
        public string ExpenseType { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreatedDate=DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModifyDate= DateTime.Now;
            this.ModifyBy = LoginUserInfo.UserInfo.realName;
        }
        #endregion
    }
}

