﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-10 16:06
    /// 描 述：报价单管理
    /// </summary>
    public interface PriceSheetIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpPriceSheetEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpPriceSheetDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpPriceSheetDetailEntity> GetMeioErpPriceSheetDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpPriceSheetDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpPriceSheetDetailEntity GetMeioErpPriceSheetDetailEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpPriceSheet表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpPriceSheetEntity GetMeioErpPriceSheetEntity(string keyValue);
        /// <summary>
        /// 获取删除报价单日志信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        IEnumerable<MeioErpDeleteLogEntity> GetMeioErpDeletePriceSheetList(string name);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue,string deleteList);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpPriceSheetEntity entity,List<MeioErpPriceSheetDetailEntity> meioErpPriceSheetDetailList,string deleteList,string type);
        #endregion
        bool SetIsEnable(string keyValue, bool isValid);
        bool IsEditForm(string itemId);
        bool CheckIsCode(string code, string id);
    }
}
