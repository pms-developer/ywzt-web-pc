﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-10 16:06
    /// 描 述：报价单管理
    /// </summary>
    public class MeioErpPriceSheetEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 报价单号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 报价单类型：1（小包）2（大货）
        /// </summary>
        [Column("TYPE")]
        public int? Type { get; set; }
        /// <summary>
        /// 生效时间
        /// </summary>
        [Column("EFFECTIVETIME")]
        public DateTime? EffectiveTime { get; set; }
        /// <summary>
        /// 失效时间
        /// </summary>
        [Column("EXPIRATIONTIME")]
        public DateTime? ExpirationTime { get; set; }
        /// <summary>
        /// 启用状态
        /// </summary>
        [Column("ISVALID")]
        public bool? IsValid { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        [Column("CREATEDBY")]
        public string CreatedBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        [Column("MODIFYBY")]
        public string ModifyBy { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// 币种
        /// </summary>
        [Column("CURRENCY")]
        public string Currency { get; set; }
        /// <summary>
        /// 仓库ID，多个用英文逗号分割
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreatedDate=DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModifyDate = DateTime.Now;
            this.ModifyBy= LoginUserInfo.UserInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

