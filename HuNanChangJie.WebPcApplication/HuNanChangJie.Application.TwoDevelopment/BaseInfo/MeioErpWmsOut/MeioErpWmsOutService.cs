﻿using Dapper;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using HuNanChangJie.WebRequestService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS出库单
    /// </summary>
    public class MeioErpWmsOutService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private static readonly object objLock = new object();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsOutEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID
                ,t.WarehouseID
                ,t.OwnerID
                ,t.Code
                ,t.Type
                ,t.OrderType
                ,t.CreationDate
                ");
                strSql.Append("  FROM MeioErpWmsOut t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpWmsOutEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsOutDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsOutDetailEntity> GetMeioErpWmsOutDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpWmsOutDetailEntity>(t => t.WmsOutID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsOut表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsOutEntity GetMeioErpWmsOutEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsOutEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据出库单号获取出库单信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MeioErpWmsOutEntity GetMeioErpWmsOutEntityByCode(string code)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpWmsOutEntity>(p => p.Code == code).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsOutDetailEntity表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsOutDetailEntity GetMeioErpWmsOutDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsOutDetailEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpOutEntity = GetMeioErpWmsOutEntity(keyValue);
                db.Delete<MeioErpWmsOutEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpWmsOutDetailEntity>(t => t.WmsOutID == meioErpOutEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsOutEntity entity, List<MeioErpWmsOutDetailEntity> meioErpWmsOutDetailList, string deleteList, string type)
        {
            lock (objLock)
            {
                var db = this.BaseRepository().BeginTrans();
                try
                {
                    if (type == "edit")
                    {
                        //var meioErpDeliveryAccountEntityTmp = GetMeioErpDeliveryAccountEntity(keyValue);
                        entity.Modify(keyValue);
                        db.Update(entity);
                        this.BaseRepository().Delete<MeioErpWmsOutDetailEntity>(p => p.WmsOutID == keyValue);
                        foreach (MeioErpWmsOutDetailEntity item in meioErpWmsOutDetailList)
                        {
                            item.Create();
                            item.WmsOutID = keyValue;
                            db.Insert(item);
                        }
                    }
                    else
                    {
                        entity.Create();
                        db.Insert(entity);
                        foreach (MeioErpWmsOutDetailEntity item in meioErpWmsOutDetailList)
                        {
                            item.Create();
                            item.WmsOutID = entity.ID;
                            db.Insert(item);
                        }
                    }
                    var goodsCods = meioErpWmsOutDetailList.Select(o => o.GoodsCode);
                    var inventoryList = this.BaseRepository().FindList<MeioErpInventoryEntity>(p => goodsCods.Contains(p.GoodsCode));
                    var goodsList = this.BaseRepository().FindList<MeioErpGoodsEntity>(p => goodsCods.Contains(p.Code));
                    #region 计算货主账单
                    //查询货主合同
                    var owenerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(p => p.ID == entity.OwnerID);
                    if (owenerEntity != null)
                    {
                        var nowDate = DateTime.Now.Date;
                        var contractEntity = this.BaseRepository().FindEntity<MeioErpContractEntity>(p => p.ID == owenerEntity.ContractID && p.ServiceStartDate <= nowDate && p.ServiceEndDate >= nowDate);
                        if (contractEntity != null)
                        {
                            var contractDetailList = this.BaseRepository().FindList<MeioErpContractDetailEntity>(p => p.ContractID == owenerEntity.ContractID);
                            var holidayList = HolidayService.GetHoliday();//假期
                            int wage = 1;//工资倍数
                            var holiday = holidayList.FirstOrDefault(p => p.date == DateTime.Now.ToString("yyyy-MM-dd"));
                            if (holiday != null)
                            {
                                wage = holiday.wage;
                            }
                            #region 计算出库账单
                            MeioErpDeliveryAccountEntity meioErpDeliveryAccountEntity = new MeioErpDeliveryAccountEntity
                            {
                                WarehouseID = entity.WarehouseID,
                                OwnerID = entity.OwnerID,
                                State = 0,
                                DeliveryNo = entity.Code,
                                OrderType = entity.Type,
                                AccountDate = DateTime.Now,
                                TotalAmount = 0,
                                IsPushOMS = false
                            };
                            meioErpDeliveryAccountEntity.Create();
                            foreach (var deliveryDetail in meioErpWmsOutDetailList)
                            {
                                if (deliveryDetail.PurchasePrice == null || deliveryDetail.PurchasePrice == 0)
                                {
                                    var goodsEntity = goodsList.FirstOrDefault(p => p.Code == deliveryDetail.GoodsCode);
                                    if (goodsEntity != null)
                                    {
                                        deliveryDetail.PurchasePrice = goodsEntity.CostPrice;
                                    }
                                }

                                MeioErpDeliveryAccountDetailEntity deliveryAccountDetailEntity = new MeioErpDeliveryAccountDetailEntity
                                {
                                    DeliveryAccountID = meioErpDeliveryAccountEntity.ID,
                                    GoodsCode = deliveryDetail.GoodsCode,
                                    GoodsName = deliveryDetail.GoodsName,
                                    QualityState = deliveryDetail.Quality,
                                    Quantity = deliveryDetail.OutQuantity,
                                    Amount = 0,
                                };
                                if (contractEntity.BillingType == 1)
                                {
                                    if (!string.IsNullOrEmpty(deliveryDetail.ItemCode))
                                    {
                                        var itemCodes = deliveryDetail.ItemCode.Split(',');
                                        var deliveryItemServiceList = contractDetailList.Where(p => itemCodes.Contains(p.ItemID));
                                        StringBuilder strExpenseDetail = new StringBuilder();
                                        int num = 0;
                                        foreach (var item in deliveryItemServiceList)
                                        {
                                            num++;
                                            var amount = deliveryDetail.OutQuantity * item.ContractPrice * wage;
                                            deliveryAccountDetailEntity.Amount = deliveryAccountDetailEntity.Amount + amount;
                                            meioErpDeliveryAccountEntity.TotalAmount = meioErpDeliveryAccountEntity.TotalAmount + amount;
                                            strExpenseDetail.Append($"{num}，{item.ItemName}  单价：{item.ContractPrice}  费用：{amount}<br/>");
                                        }
                                        deliveryAccountDetailEntity.ExpenseDetail = strExpenseDetail.ToString();

                                        deliveryAccountDetailEntity.Create();
                                        db.Insert(deliveryAccountDetailEntity);
                                    }
                                    else
                                    {
                                        if (!deliveryDetail.IsProcessingConsumables.Value)
                                        {
                                            if (inventoryList.Any(a => a.GoodsCode == deliveryDetail.GoodsCode))
                                            {
                                                var amount = deliveryDetail.OutQuantity * deliveryDetail.PurchasePrice;
                                                deliveryAccountDetailEntity.Amount = amount;
                                                meioErpDeliveryAccountEntity.TotalAmount = meioErpDeliveryAccountEntity.TotalAmount + amount;
                                                deliveryAccountDetailEntity.ExpenseDetail = $"批次号：{deliveryDetail.EntryDetailTakeNo} 单价：{deliveryDetail.PurchasePrice}";

                                                deliveryAccountDetailEntity.Create();
                                                db.Insert(deliveryAccountDetailEntity);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (inventoryList.Any(a => a.GoodsCode == deliveryDetail.GoodsCode))
                                    {
                                        var amount = deliveryDetail.OutQuantity * deliveryDetail.PurchasePrice;
                                        deliveryAccountDetailEntity.Amount = amount;
                                        meioErpDeliveryAccountEntity.TotalAmount = meioErpDeliveryAccountEntity.TotalAmount + amount;
                                        deliveryAccountDetailEntity.ExpenseDetail = $"批次号：{deliveryDetail.EntryDetailTakeNo} 单价：{deliveryDetail.PurchasePrice}";
                                    }
                                    else
                                    {
                                        if (deliveryDetail.IsProcessingConsumables.HasValue)
                                        {
                                            if (entity.Type == "FBA出库" && !deliveryDetail.IsProcessingConsumables.Value)
                                            {
                                                deliveryAccountDetailEntity.Amount = deliveryDetail.OutQuantity * contractEntity.PackagePrice;
                                                meioErpDeliveryAccountEntity.TotalAmount = meioErpDeliveryAccountEntity.TotalAmount + deliveryDetail.OutQuantity * contractEntity.PackagePrice;
                                                deliveryAccountDetailEntity.ExpenseDetail = $"包干单价：{contractEntity.PackagePrice}元/sku";
                                            }
                                        }
                                        else
                                        {
                                            if (entity.Type == "FBA出库")
                                            {
                                                deliveryAccountDetailEntity.Amount = deliveryDetail.OutQuantity * contractEntity.PackagePrice;
                                                meioErpDeliveryAccountEntity.TotalAmount = meioErpDeliveryAccountEntity.TotalAmount + deliveryDetail.OutQuantity * contractEntity.PackagePrice;
                                                deliveryAccountDetailEntity.ExpenseDetail = $"包干单价：{contractEntity.PackagePrice}元/sku";
                                            }
                                        }
                                    }
                                    deliveryAccountDetailEntity.Create();
                                    db.Insert(deliveryAccountDetailEntity);
                                }
                            }

                            db.Insert(meioErpDeliveryAccountEntity);
                            #endregion
                        }
                    }
                    #endregion
                    #region 修改包材，耗材库存
                    foreach (var inventory in inventoryList)
                    {
                        var outDetailEntity = meioErpWmsOutDetailList.Where(p => p.GoodsCode == inventory.GoodsCode).FirstOrDefault();
                        if (outDetailEntity != null)
                        {
                            inventory.InQuantity = inventory.InQuantity - outDetailEntity.OutQuantity;
                            inventory.Modify(inventory.ID);
                            db.Update(inventory);
                        }
                    }

                    //修改批次库存
                    var batchInventoryList = this.BaseRepository().FindList<MeioErpBatchInventoryEntity>(p => goodsCods.Contains(p.GoodsCode));
                    foreach (var batchInventory in batchInventoryList)
                    {
                        var outDetailEntity = meioErpWmsOutDetailList.Where(p => p.GoodsCode == batchInventory.GoodsCode && p.EntryDetailTakeNo == batchInventory.BatchNo).FirstOrDefault();
                        if (outDetailEntity != null)
                        {
                            batchInventory.InQuantity = batchInventory.InQuantity - outDetailEntity.OutQuantity;
                            batchInventory.Modify(batchInventory.ID);
                            db.Update(batchInventory);
                        }
                    }
                    #endregion
                    db.Commit();
                }
                catch (Exception ex)
                {
                    db.Rollback();
                    if (ex is ExceptionEx)
                    {
                        throw;
                    }
                    else
                    {
                        throw ExceptionEx.ThrowServiceException(ex);
                    }
                }
            }
        }

        #endregion
    }
}
