﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS出库单
    /// </summary>
    public interface MeioErpWmsOutIBLL
    {

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpWmsOutEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpWmsOutDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpWmsOutDetailEntity> GetMeioErpWmsOutDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpOutAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsOutEntity GetMeioErpWmsOutEntity(string keyValue);
        MeioErpWmsOutEntity GetMeioErpWmsOutEntityByCode(string code);
        /// <summary>
        /// 获取MeioErpWmsOutDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsOutDetailEntity GetMeioErpWmsOutDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpWmsOutEntity entity, List<MeioErpWmsOutDetailEntity> meioErpWmsOutDetailList, string deleteList, string type);
        #endregion
    }
}
