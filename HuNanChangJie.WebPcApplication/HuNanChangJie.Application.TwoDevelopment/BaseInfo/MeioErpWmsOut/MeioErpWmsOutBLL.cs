﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS出库单
    /// </summary>
    public class MeioErpWmsOutBLL : MeioErpWmsOutIBLL
    {

        private MeioErpWmsOutService meioErpWmsOutService = new MeioErpWmsOutService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsOutEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpWmsOutService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsOutDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsOutDetailEntity> GetMeioErpWmsOutDetailList(string keyValue)
        {
            try
            {
                return meioErpWmsOutService.GetMeioErpWmsOutDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsOut表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsOutEntity GetMeioErpWmsOutEntity(string keyValue)
        {
            try
            {
                return meioErpWmsOutService.GetMeioErpWmsOutEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 根据出库单号，获取MeioErpWmsOut表实体数据
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MeioErpWmsOutEntity GetMeioErpWmsOutEntityByCode(string code)
        {
            try
            {
                return meioErpWmsOutService.GetMeioErpWmsOutEntityByCode(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsDeliveryDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsOutDetailEntity GetMeioErpWmsOutDetailEntity(string keyValue)
        {
            try
            {
                return meioErpWmsOutService.GetMeioErpWmsOutDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpWmsOutService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsOutEntity entity, List<MeioErpWmsOutDetailEntity> meioErpWmsOutDetailList, string deleteList, string type)
        {
            try
            {
                meioErpWmsOutService.SaveEntity(keyValue, entity, meioErpWmsOutDetailList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
    }
}
