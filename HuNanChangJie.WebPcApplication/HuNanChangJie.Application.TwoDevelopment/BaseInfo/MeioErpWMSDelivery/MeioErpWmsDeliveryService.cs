﻿using Dapper;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS发货单
    /// </summary>
    public class MeioErpWmsDeliveryService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private static readonly object objLock = new object();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsDeliveryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID
                ,t.WarehouseID
                ,t.OwnerID
                ,t.Code
                ,t.Carrier
                ,t.ShipDate
                ,t.LogisticsCode
                ,t.DocType
                ,t.CreationDate
                ");
                strSql.Append("  FROM MeioErpWmsDelivery t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpWmsDeliveryEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsDeliveryDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsDeliveryDetailEntity> GetMeioErpWmsDeliveryDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpWmsDeliveryDetailEntity>(t => t.DeliveryID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsDelivery表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsDeliveryEntity GetMeioErpWmsDeliveryEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsDeliveryEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据发货单号获取MeioErpWmsDelivery表实体数据
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MeioErpWmsDeliveryEntity GetMeioErpWmsDeliveryEntityByCode(string code)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpWmsDeliveryEntity>(p=>p.Code==code).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsDeliveryDetailEntity表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsDeliveryDetailEntity GetMeioErpWmsDeliveryDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsDeliveryDetailEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpDeliveryAccountEntity = GetMeioErpWmsDeliveryEntity(keyValue);
                db.Delete<MeioErpWmsDeliveryEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpWmsDeliveryDetailEntity>(t => t.DeliveryID == meioErpDeliveryAccountEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsDeliveryEntity entity, List<MeioErpWmsDeliveryDetailEntity> meioErpWmsDeliveryDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    //var meioErpDeliveryAccountEntityTmp = GetMeioErpDeliveryAccountEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    this.BaseRepository().Delete<MeioErpWmsDeliveryDetailEntity>(p => p.DeliveryID == keyValue);
                    foreach (MeioErpWmsDeliveryDetailEntity item in meioErpWmsDeliveryDetailList)
                    {
                        item.Create();
                        item.DeliveryID = keyValue;
                        db.Insert(item);
                    }
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (MeioErpWmsDeliveryDetailEntity item in meioErpWmsDeliveryDetailList)
                    {
                        item.Create();
                        item.DeliveryID = entity.ID;
                        db.Insert(item);
                    }
                }
                #region 计算货主账单
                var isCheckAssociateNo = this.BaseRepository().FindList<MeioErpWarehouseRentAccountEntity>(p=>p.AssociateNo==entity.Code && p.WarehouseID==entity.WarehouseID && p.OwnerID==entity.OwnerID).Count() > 0;
                if (!isCheckAssociateNo)
                {
                    //查询货主合同
                    var owenerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(p => p.ID == entity.OwnerID);
                    if (owenerEntity != null)
                    {
                        var nowDate = DateTime.Now.Date;
                        var contractEntity = this.BaseRepository().FindEntity<MeioErpContractEntity>(p => p.ID == owenerEntity.ContractID && p.ServiceStartDate <= nowDate && p.ServiceEndDate >= nowDate);
                        if (contractEntity != null)
                        {
                            #region 计算仓库账单
                            if (contractEntity.IsWarehouseRent.Value)
                            {
                                var contractDetailList = this.BaseRepository().FindList<MeioErpContractDetailEntity>(p => p.ContractID == owenerEntity.ContractID);
                                MeioErpWarehouseRentAccountEntity warehouseRentAccount = new MeioErpWarehouseRentAccountEntity
                                {
                                    WarehouseID = entity.WarehouseID,
                                    OwnerID = entity.OwnerID,
                                    State = 0,
                                    AssociateNo = entity.Code,
                                    TotalAmount = 0,
                                    AccountDate = DateTime.Now,
                                    TotalAmountText = "0",
                                    IsPushOMS = false,
                                };
                                warehouseRentAccount.Create();

                                foreach (var item in meioErpWmsDeliveryDetailList)
                                {
                                    MeioErpWarehouseRentAccountDetailEntity warehouseRentAccountDetail = new MeioErpWarehouseRentAccountDetailEntity
                                    {
                                        WarehouseRentAccountID = warehouseRentAccount.ID,
                                        BatchNumber = item.BatchCode,
                                        Sku = item.SKU,
                                        GoodsName = item.GoodsName,
                                        GoodsType = item.GoodsType,
                                        InStorageDate = item.InStorageDate,
                                        DeliveryDate = item.DeliveryStorageDate,
                                        InDay = item.InDays,
                                        Volume = item.Volume,
                                        VolumeText = item.Volume.ToString(),
                                        Weight = item.Weight,
                                        WeightText = item.Weight.ToString(),
                                        TotalVolume = item.TotalVolume,
                                        TotalVolumeText = item.TotalVolume.ToString(),
                                        Quantity = item.Quantity,
                                        StorageCharges = 0,
                                        StorageChargesText = "0",

                                    };

                                    var warehouseRent = contractDetailList.FirstOrDefault(p => p.ItemType == 1 && p.ItemDetailMinValue <= item.InDays && p.ItemDetailMaxValue > item.InDays);
                                    if (warehouseRent != null)
                                    {
                                        warehouseRentAccountDetail.StorageCharges = item.TotalVolume * item.InDays * warehouseRent.ContractPrice;
                                        warehouseRentAccountDetail.StorageChargesText = warehouseRentAccountDetail.StorageCharges.ToString();
                                        warehouseRentAccountDetail.ChargingStandard = warehouseRent.ContractPrice + "/m³/" + warehouseRent.Unit;

                                        warehouseRentAccount.TotalAmount = warehouseRentAccount.TotalAmount + warehouseRentAccountDetail.StorageCharges;
                                        warehouseRentAccount.TotalAmountText = warehouseRentAccount.TotalAmount.ToString();
                                    }
                                    warehouseRentAccountDetail.Create();
                                    warehouseRentAccountDetail.WarehouseRentAccountID = warehouseRentAccount.ID;
                                    db.Insert(warehouseRentAccountDetail);//decimal类型数据插入数据库小数自动保留2位
                                }
                                
                                db.Insert(warehouseRentAccount);
                            }
                            #endregion
                        }
                    }
                }
                #endregion

                db.Commit();

            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }

        }

        #endregion
    }
}
