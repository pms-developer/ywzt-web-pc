﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-15 11:01
    /// 描 述：WMS回传中台FBA发货单
    /// </summary>
    public class MeioErpWmsDeliveryDetailEntity 
    {
        #region  实体成员
        /// <summary>
        /// 发货单ID
        /// </summary>
        /// <returns></returns>
        [Column("DELIVERYID")]
        public string DeliveryID { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        /// <returns></returns>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        /// <returns></returns>
        [Column("BATCHCODE")]
        public string BatchCode { get; set; }
        /// <summary>
        /// SKU
        /// </summary>
        /// <returns></returns>
        [Column("SKU")]
        public string SKU { get; set; }
        /// <summary>
        /// 货品名称
        /// </summary>
        /// <returns></returns>
        [Column("GOODSNAME")]
        public string GoodsName { get; set; }
        /// <summary>
        /// 货品分类
        /// </summary>
        /// <returns></returns>
        [Column("GOODSTYPE")]
        public string GoodsType { get; set; }
        /// <summary>
        /// 入库日期
        /// </summary>
        /// <returns></returns>
        [Column("INSTORAGEDATE")]
        public DateTime? InStorageDate { get; set; }
        /// <summary>
        /// 出库日期
        /// </summary>
        /// <returns></returns>
        [Column("DELIVERYSTORAGEDATE")]
        public DateTime? DeliveryStorageDate { get; set; }
        /// <summary>
        /// 体积
        /// </summary>
        /// <returns></returns>
        [Column("VOLUME")]
        public decimal? Volume { get; set; }
        /// <summary>
        /// 重量
        /// </summary>
        /// <returns></returns>
        [Column("WEIGHT")]
        public decimal? Weight { get; set; }
        /// <summary>
        /// 在库天数
        /// </summary>
        /// <returns></returns>
        [Column("INDAYS")]
        public int? InDays { get; set; }
        /// <summary>
        /// 总体积
        /// </summary>
        /// <returns></returns>
        [Column("TOTALVOLUME")]
        public decimal? TotalVolume { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        /// <returns></returns>
        [Column("QUANTITY")]
        public int? Quantity { get; set; }
        /// <summary>
        /// 质量状态
        /// </summary>
        /// <returns></returns>
        [Column("QUALITY")]
        public string Quality { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        /// <returns></returns>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        /// <returns></returns>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        /// <returns></returns>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        /// <returns></returns>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        /// <returns></returns>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        /// <returns></returns>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        /// <returns></returns>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        /// <returns></returns>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        /// <returns></returns>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        /// <returns></returns>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        /// <returns></returns>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        /// <returns></returns>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        /// <returns></returns>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        /// <returns></returns>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 服务项ID
        /// </summary>
        [Column("ITEMCODE")]
        public string ItemCode { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            if (userInfo != null)
            {
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
            }
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            if (userInfo != null)
            {
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
            }
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            if (userInfo != null)
            {
                this.Modification_Id = userInfo.userId;
                this.ModificationName = userInfo.realName;
            }
        }
        #endregion
    }
}

