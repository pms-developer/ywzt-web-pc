﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS发货单
    /// </summary>
    public class MeioErpWmsDeliveryBLL : MeioErpWmsDeliveryIBLL
    {
        private MeioErpWmsDeliveryService meioErpWmsDeliveryService = new MeioErpWmsDeliveryService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsDeliveryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpWmsDeliveryService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryAccountDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsDeliveryDetailEntity> GetMeioErpWmsDeliveryDetailList(string keyValue)
        {
            try
            {
                return meioErpWmsDeliveryService.GetMeioErpWmsDeliveryDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsDelivery表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsDeliveryEntity GetMeioErpWmsDeliveryEntity(string keyValue)
        {
            try
            {
                return meioErpWmsDeliveryService.GetMeioErpWmsDeliveryEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 根据发货单号获取MeioErpWmsDelivery表实体数据
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MeioErpWmsDeliveryEntity GetMeioErpWmsDeliveryEntityByCode(string code)
        {
            try
            {
                return meioErpWmsDeliveryService.GetMeioErpWmsDeliveryEntityByCode(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsDeliveryDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsDeliveryDetailEntity GetMeioErpWmsDeliveryDetailEntity(string keyValue)
        {
            try
            {
                return meioErpWmsDeliveryService.GetMeioErpWmsDeliveryDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpWmsDeliveryService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsDeliveryEntity entity, List<MeioErpWmsDeliveryDetailEntity> meioErpWmsDeliveryDetailList, string deleteList, string type)
        {
            try
            {
                meioErpWmsDeliveryService.SaveEntity(keyValue, entity, meioErpWmsDeliveryDetailList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
    }
}
