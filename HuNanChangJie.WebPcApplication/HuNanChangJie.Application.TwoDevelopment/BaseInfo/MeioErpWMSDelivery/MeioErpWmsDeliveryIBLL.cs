﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS发货单
    /// </summary>
    public interface MeioErpWmsDeliveryIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpWmsDeliveryEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpWmsDeliveryDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpWmsDeliveryDetailEntity> GetMeioErpWmsDeliveryDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpDeliveryAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsDeliveryEntity GetMeioErpWmsDeliveryEntity(string keyValue);
        MeioErpWmsDeliveryEntity GetMeioErpWmsDeliveryEntityByCode(string code);
        /// <summary>
        /// 获取MeioErpWmsDeliveryDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsDeliveryDetailEntity GetMeioErpWmsDeliveryDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpWmsDeliveryEntity entity, List<MeioErpWmsDeliveryDetailEntity> meioErpWmsDeliveryDetailList, string deleteList, string type);
        #endregion
    }
}
