﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-02 11:16
    /// 描 述：WMS入库单
    /// </summary>
    public class MeioErpWmsInStorageBLL : MeioErpWmsInStorageIBLL
    {
        private MeioErpWmsInStorageService meioErpWmsInStorageService = new MeioErpWmsInStorageService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsInStorageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpWmsInStorageService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsInStorageDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsInStorageDetailEntity> GetMeioErpWmsInStorageDetailList(string keyValue)
        {
            try
            {
                return meioErpWmsInStorageService.GetMeioErpWmsInStorageDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsInStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsInStorageEntity GetMeioErpWmsInStorageEntity(string keyValue)
        {
            try
            {
                return meioErpWmsInStorageService.GetMeioErpWmsInStorageEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 根据入库单号获取入库单
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MeioErpWmsInStorageEntity GetMeioErpWmsInStorageEntityByCode(string code)
        {
            try
            {
                return meioErpWmsInStorageService.GetMeioErpWmsInStorageEntityByCode(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsInStorageDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsInStorageDetailEntity GetMeioErpWmsInStorageDetailEntity(string keyValue)
        {
            try
            {
                return meioErpWmsInStorageService.GetMeioErpWmsInStorageDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpWmsInStorageService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsInStorageEntity entity,List<MeioErpWmsInStorageDetailEntity> meioErpWmsInStorageDetailList, List<MeioErpWmsInStorageAuxiliaryMaterialDetailEntity> meioErpWmsInStorageAuxiliaryMaterialDetailList, string deleteList,string type)
        {
            try
            {
                meioErpWmsInStorageService.SaveEntity(keyValue, entity,meioErpWmsInStorageDetailList, meioErpWmsInStorageAuxiliaryMaterialDetailList, deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
