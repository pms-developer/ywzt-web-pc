﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-02 11:16
    /// 描 述：WMS入库单
    /// </summary>
    public interface MeioErpWmsInStorageIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpWmsInStorageEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpWmsInStorageDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpWmsInStorageDetailEntity> GetMeioErpWmsInStorageDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpWmsInStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsInStorageEntity GetMeioErpWmsInStorageEntity(string keyValue);
        MeioErpWmsInStorageEntity GetMeioErpWmsInStorageEntityByCode(string code);
        /// <summary>
        /// 获取MeioErpWmsInStorageDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsInStorageDetailEntity GetMeioErpWmsInStorageDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpWmsInStorageEntity entity,List<MeioErpWmsInStorageDetailEntity> meioErpWmsInStorageDetailList,List<MeioErpWmsInStorageAuxiliaryMaterialDetailEntity> meioErpWmsInStorageAuxiliaryMaterialDetailList, string deleteList,string type);
        #endregion

    }
}
