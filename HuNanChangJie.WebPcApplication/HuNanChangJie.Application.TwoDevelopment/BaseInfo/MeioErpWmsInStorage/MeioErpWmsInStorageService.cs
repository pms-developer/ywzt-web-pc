﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-02 11:16
    /// 描 述：WMS入库单
    /// </summary>
    public class MeioErpWmsInStorageService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private static readonly object objLock = new object();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsInStorageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.WarehouseID,
                t.OwnerID,
                t.Type,
                t.CreationName,
                t.CreationDate
                ");
                strSql.Append("  FROM MeioErpWmsInStorage t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID", queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type = @Type ");
                }
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpWmsInStorageEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsInStorageDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsInStorageDetailEntity> GetMeioErpWmsInStorageDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpWmsInStorageDetailEntity>(t => t.WmsInStorageID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsInStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsInStorageEntity GetMeioErpWmsInStorageEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsInStorageEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public MeioErpWmsInStorageEntity GetMeioErpWmsInStorageEntityByCode(string code)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpWmsInStorageEntity>(p => p.Code == code).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsInStorageDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsInStorageDetailEntity GetMeioErpWmsInStorageDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsInStorageDetailEntity>(t => t.WmsInStorageID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpWmsInStorageEntity = GetMeioErpWmsInStorageEntity(keyValue);
                db.Delete<MeioErpWmsInStorageEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpWmsInStorageDetailEntity>(t => t.WmsInStorageID == meioErpWmsInStorageEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsInStorageEntity entity, List<MeioErpWmsInStorageDetailEntity> meioErpWmsInStorageDetailList, List<MeioErpWmsInStorageAuxiliaryMaterialDetailEntity> meioErpWmsInStorageAuxiliaryMaterialDetailList, string deleteList, string type)
        {
            lock (objLock)
            {
                var db = this.BaseRepository().BeginTrans();
                try
                {
                    if (type == "edit")
                    {
                        var meioErpWmsInStorageEntityTmp = GetMeioErpWmsInStorageEntity(keyValue);
                        entity.Modify(keyValue);
                        db.Update(entity);

                        db.Delete<MeioErpWmsInStorageDetailEntity>(p => p.WmsInStorageID == keyValue);
                        foreach (MeioErpWmsInStorageDetailEntity item in meioErpWmsInStorageDetailList)
                        {
                            item.WmsInStorageID = keyValue;
                            item.Create();
                            db.Insert(item);
                        }
                        db.Delete<MeioErpWmsInStorageAuxiliaryMaterialDetailEntity>(p => p.WmsInStorageID == keyValue);
                        foreach (MeioErpWmsInStorageAuxiliaryMaterialDetailEntity item in meioErpWmsInStorageAuxiliaryMaterialDetailList)
                        {
                            item.WmsInStorageID = keyValue;
                            item.Create();
                            db.Insert(item);
                        }
                    }
                    else
                    {
                        entity.Create();
                        db.Insert(entity);
                        foreach (MeioErpWmsInStorageDetailEntity item in meioErpWmsInStorageDetailList)
                        {
                            item.WmsInStorageID = entity.ID;
                            item.Create();
                            db.Insert(item);
                        }
                        foreach (MeioErpWmsInStorageAuxiliaryMaterialDetailEntity item in meioErpWmsInStorageAuxiliaryMaterialDetailList)
                        {
                            item.WmsInStorageID = entity.ID;
                            item.Create();
                            db.Insert(item);
                        }
                    }

                    string inStorageAccountID = string.Empty;
                    IEnumerable<MeioErpInventoryEntity> inventoryHCList = null;
                    IEnumerable<MeioErpBatchInventoryEntity> batchInventoryHCList = null;
                    IEnumerable<MeioErpGoodsEntity> goodsList = null;
                    if (meioErpWmsInStorageAuxiliaryMaterialDetailList != null)
                    {
                        var hccodes = meioErpWmsInStorageAuxiliaryMaterialDetailList.Select(o => o.Code);
                        inventoryHCList = this.BaseRepository().FindList<MeioErpInventoryEntity>(p => hccodes.Contains(p.GoodsCode));
                        batchInventoryHCList = this.BaseRepository().FindList<MeioErpBatchInventoryEntity>(p => hccodes.Contains(p.GoodsCode));
                        goodsList = this.BaseRepository().FindList<MeioErpGoodsEntity>(p => hccodes.Contains(p.Code));
                    }
                    #region 计算货主账单
                    if (entity.Type == "成品入库")
                    {
                        var inStorageAccountList = this.BaseRepository().FindList<MeioErpInStorageAccountEntity>(p => p.InStorageNo == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == entity.OwnerID);
                        if (inStorageAccountList.Count() > 0)
                        {
                            var ids = inStorageAccountList.Select(o => o.ID);
                            db.Delete<MeioErpInStorageAccountEntity>(p => ids.Contains(p.ID));
                            db.Delete<MeioErpInStorageAccountDetailEntity>(p => ids.Contains(p.InStorageAccountID));
                        }
                        //查询货主合同
                        var owenerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(p => p.ID == entity.OwnerID);
                        if (owenerEntity != null)
                        {
                            var nowDate = DateTime.Now.Date;
                            var contractEntity = this.BaseRepository().FindEntity<MeioErpContractEntity>(p => p.ID == owenerEntity.ContractID && p.ServiceStartDate <= nowDate && p.ServiceEndDate >= nowDate);
                            var customerServiceList = this.BaseRepository().FindList<MeioErpCustomerServiceEntity>(p => p.OrderCode == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == entity.OwnerID);//其它服务项
                            IEnumerable<MeioErpCustomerServiceDetailEntity> customerServiceDetailList = null;
                            if (customerServiceList.Count() > 0)
                            {
                                var ids = customerServiceList.Select(o => o.ID);
                                customerServiceDetailList = this.BaseRepository().FindList<MeioErpCustomerServiceDetailEntity>(p => p.State == "2" && ids.Contains(p.CustomerServiceID));
                            }
                            if (contractEntity != null)
                            {
                                if (contractEntity.BillingType == 1)//只有计件生成账单
                                {
                                    var contractDetailList = this.BaseRepository().FindList<MeioErpContractDetailEntity>(p => p.ContractID == owenerEntity.ContractID);
                                    var holidayList = HolidayService.GetHoliday();//假期
                                    int wage = 1;//工资倍数
                                    var holiday = holidayList.FirstOrDefault(p => p.date == DateTime.Now.ToString("yyyy-MM-dd"));
                                    if (holiday != null)
                                    {
                                        wage = holiday.wage;
                                    }
                                    #region 计算入库单账单
                                    MeioErpInStorageAccountEntity meioErpInStorageAccountEntity = new MeioErpInStorageAccountEntity
                                    {
                                        WarehouseID = entity.WarehouseID,
                                        OwnerID = entity.OwnerID,
                                        State = 0,
                                        InStorageNo = entity.Code,
                                        OrderType = entity.Type,
                                        AccountDate = DateTime.Now,
                                        TotalAmount = 0,
                                        IsPushOMS = false
                                    };
                                    meioErpInStorageAccountEntity.Create();

                                    inStorageAccountID = meioErpInStorageAccountEntity.ID;

                                    foreach (var inStorageDetail in meioErpWmsInStorageDetailList)
                                    {
                                        MeioErpInStorageAccountDetailEntity inStorageAccountDetailEntity = new MeioErpInStorageAccountDetailEntity
                                        {
                                            InStorageAccountID = meioErpInStorageAccountEntity.ID,
                                            GoodsCode = inStorageDetail.GoodsCode,
                                            GoodsName = inStorageDetail.GoodsName,
                                            QualityState = inStorageDetail.Quality,
                                            Quantity = inStorageDetail.InStorageQuantity,
                                            Amount = 0,
                                        };
                                        int num = 0;
                                        StringBuilder strExpenseDetail = new StringBuilder();
                                        if (!string.IsNullOrEmpty(inStorageDetail.ItemCode))
                                        {
                                            var itemCodes = inStorageDetail.ItemCode.Split(',');
                                            var inStorageItemServiceList = contractDetailList.Where(p => itemCodes.Contains(p.ItemID));
                                            foreach (var item in inStorageItemServiceList)
                                            {
                                                num++;
                                                var amount = inStorageDetail.InStorageQuantity * item.ContractPrice * wage;
                                                inStorageAccountDetailEntity.Amount = contractEntity.BillingType == 1 ? (inStorageAccountDetailEntity.Amount + amount) : 0;
                                                meioErpInStorageAccountEntity.TotalAmount = contractEntity.BillingType == 1 ? (meioErpInStorageAccountEntity.TotalAmount + amount) : 0;
                                                if (contractEntity.BillingType == 1)
                                                    strExpenseDetail.Append($"{num}，{item.ItemName}  单价：{item.ContractPrice}  费用：{amount}<br/>");
                                                else
                                                    strExpenseDetail.Append($"{num}，{item.ItemName}<br/>");
                                            }

                                        }
                                        //其它服务费用计算
                                        if (customerServiceDetailList != null)
                                        {
                                            var customerServiceDetailQuery = customerServiceDetailList.Where(p => p.GoodsCode == inStorageDetail.GoodsCode);
                                            foreach (var customerServiceDetail in customerServiceDetailQuery)
                                            {
                                                num++;
                                                var amount = inStorageDetail.InStorageQuantity * customerServiceDetail.Price * wage;
                                                inStorageAccountDetailEntity.Amount = contractEntity.BillingType == 1 ? (inStorageAccountDetailEntity.Amount + amount) : 0;
                                                meioErpInStorageAccountEntity.TotalAmount = contractEntity.BillingType == 1 ? (meioErpInStorageAccountEntity.TotalAmount + amount) : 0;
                                                if (contractEntity.BillingType == 1)
                                                    strExpenseDetail.Append($"{num}，{customerServiceDetail.ServiceName}  单价：{customerServiceDetail.Price}  费用：{amount}<br/>");
                                                else
                                                    strExpenseDetail.Append($"{num}，{customerServiceDetail.ServiceName} <br/>");
                                            }
                                        }
                                        inStorageAccountDetailEntity.ExpenseDetail = strExpenseDetail.ToString();

                                        inStorageAccountDetailEntity.Create();
                                        db.Insert(inStorageAccountDetailEntity);
                                    }
                                    //自供包材费用计算
                                    if (meioErpWmsInStorageAuxiliaryMaterialDetailList != null)
                                    {
                                        foreach (var item in meioErpWmsInStorageAuxiliaryMaterialDetailList)
                                        {
                                            if (item.PurchasePrice == null || item.PurchasePrice == 0)
                                            {
                                                var goodsEntity = goodsList.FirstOrDefault(p => p.Code == item.Code);
                                                if (goodsEntity != null)
                                                {
                                                    item.PurchasePrice = goodsEntity.CostPrice;
                                                }
                                            }

                                            var inventoryEntity = inventoryHCList.Where(p => p.GoodsCode == item.Code).FirstOrDefault();
                                            if (inventoryEntity != null)
                                            {
                                                MeioErpInStorageAccountDetailEntity inStorageAccountDetailEntity = new MeioErpInStorageAccountDetailEntity
                                                {
                                                    InStorageAccountID = inStorageAccountID,
                                                    GoodsCode = item.Code,
                                                    GoodsName = item.Name,
                                                    Quantity = item.Quantity,
                                                    Amount = item.PurchasePrice * item.Quantity,
                                                    ExpenseDetail = $"批次号：{item.EntryDetailTakeNo} 单价：{item.PurchasePrice}",
                                                };
                                                inStorageAccountDetailEntity.Create();
                                                db.Insert(inStorageAccountDetailEntity);

                                                meioErpInStorageAccountEntity.TotalAmount = meioErpInStorageAccountEntity.TotalAmount+ item.PurchasePrice * item.Quantity;
                                            }
                                        }
                                    }

                                    db.Insert(meioErpInStorageAccountEntity);
                                    #endregion
                                }
                            }
                        }
                    }
                    #endregion

                    #region 修改包材，耗材货品库存
                    if (entity.Type != "成品入库")
                    {
                        var owner = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(entity.OwnerID);
                        if (owner != null)
                        {
                            if (owner.Code == "MOYC")
                            {
                                var goodsCodes = meioErpWmsInStorageDetailList.Select(o => o.GoodsCode);
                                var inventoryList = this.BaseRepository().FindList<MeioErpInventoryEntity>(p => goodsCodes.Contains(p.GoodsCode));
                                foreach (MeioErpWmsInStorageDetailEntity item in meioErpWmsInStorageDetailList)
                                {
                                    var inventoryEntity = inventoryList.Where(p => p.GoodsCode == item.GoodsCode).FirstOrDefault();
                                    if (inventoryEntity == null)
                                    {
                                        var inventory = new MeioErpInventoryEntity
                                        {
                                            OwnerID = entity.OwnerID,
                                            WarehouseID = entity.WarehouseID,
                                            GoodsCode = item.GoodsCode,
                                            GoodsName = item.GoodsName,
                                            GoodsType = entity.Type.Replace("入库", ""),
                                            InQuantity = item.InStorageQuantity,
                                            MigrationQuantity = 0,
                                            FrozenQuantity = 0
                                        };
                                        inventory.Create();
                                        db.Insert(inventory);
                                    }
                                    else
                                    {
                                        inventoryEntity.InQuantity = inventoryEntity.InQuantity + item.InStorageQuantity;
                                        inventoryEntity.Modify(inventoryEntity.ID);
                                        db.Update(inventoryEntity);
                                    }

                                    //添加批次库存
                                    MeioErpBatchInventoryEntity meioErpBatchInventoryEntity = new MeioErpBatchInventoryEntity
                                    {
                                        WarehouseID = entity.WarehouseID,
                                        OwnerID = entity.OwnerID,
                                        GoodsCode = item.GoodsCode,
                                        GoodsName = item.GoodsName,
                                        InQuantity = item.InStorageQuantity,
                                        MigrationQuantity = 0,
                                        FrozenQuantity = 0,
                                        BatchNo = "",
                                        InStorageCode = entity.Code
                                    };
                                    meioErpBatchInventoryEntity.Create();
                                    db.Insert(meioErpBatchInventoryEntity);
                                }
                            }
                        }

                    }
                    else
                    {
                        if (meioErpWmsInStorageAuxiliaryMaterialDetailList != null)
                        {
                            foreach (var item in meioErpWmsInStorageAuxiliaryMaterialDetailList)
                            {
                                var inventoryEntity = inventoryHCList.Where(p => p.GoodsCode == item.Code).FirstOrDefault();
                                if (inventoryEntity != null)
                                {
                                    //扣减包材，耗材库存
                                    inventoryEntity.InQuantity = inventoryEntity.InQuantity - item.Quantity;
                                    inventoryEntity.Modify(inventoryEntity.ID);
                                    db.Update(inventoryEntity);
                                }

                                //扣减批次库存
                                var batchInventoryEntity = batchInventoryHCList.Where(p => p.GoodsCode == item.Code && p.BatchNo == item.EntryDetailTakeNo).FirstOrDefault();
                                if (batchInventoryEntity != null)
                                {
                                    batchInventoryEntity.InQuantity = batchInventoryEntity.InQuantity - item.Quantity;
                                    batchInventoryEntity.Modify(batchInventoryEntity.ID);
                                    db.Update(batchInventoryEntity);
                                }
                            }
                        }
                    }
                    #endregion

                    db.Commit();
                }
                catch (Exception ex)
                {
                    db.Rollback();
                    if (ex is ExceptionEx)
                    {
                        throw;
                    }
                    else
                    {
                        throw ExceptionEx.ThrowServiceException(ex);
                    }
                }
            }
        }

        #endregion

    }
}
