﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-03 10:46
    /// 描 述：采购单
    /// </summary>
    public class MeioErpPurchaseService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.WarehouseID,
                t.SupplierID,
                t.PurchasePersonID,
t.PurchasePersonName,
                t.Type,
                t.CreationDate,
t.Remark,
t.State,
t.TotalAmount,
t.Auditor_ID,
t.AuditorName,
t.AuditStatus,
t.AuditTime,
t.EstimatedArrivalTime
                ");
                strSql.Append("  FROM MeioErpPurchase t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type",queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type = @Type ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID", queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list=this.BaseRepository().FindList<MeioErpPurchaseEntity>(strSql.ToString(),dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseDetailEntity> GetMeioErpPurchaseDetailList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<MeioErpPurchaseDetailEntity>(t=>t.PurchaseID == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchase表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseEntity GetMeioErpPurchaseEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpPurchaseEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseDetailEntity GetMeioErpPurchaseDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpPurchaseDetailEntity>(t=>t.PurchaseID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpPurchaseEntity = GetMeioErpPurchaseEntity(keyValue); 
                db.Delete<MeioErpPurchaseEntity>(t=>t.ID == keyValue);
                db.Delete<MeioErpPurchaseDetailEntity>(t=>t.PurchaseID == meioErpPurchaseEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpPurchaseEntity entity,List<MeioErpPurchaseDetailEntity> meioErpPurchaseDetailList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var meioErpPurchaseEntityTmp = GetMeioErpPurchaseEntity(keyValue); 
                    entity.Modify(keyValue);
                    entity.TotalAmount = meioErpPurchaseDetailList.Sum(s => s.PurchaseQuantity * s.UnitPrice);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var MeioErpPurchaseDetailUpdateList= meioErpPurchaseDetailList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in MeioErpPurchaseDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var MeioErpPurchaseDetailInserList= meioErpPurchaseDetailList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in MeioErpPurchaseDetailInserList)
                    {
                        //item.Create(item.ID);
                        item.Create();
                        item.PurchaseID = meioErpPurchaseEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    //entity.Create(keyValue);
                    entity.Create();
                    entity.TotalAmount = meioErpPurchaseDetailList.Sum(s => s.PurchaseQuantity * s.UnitPrice);
                    db.Insert(entity);
                    foreach (MeioErpPurchaseDetailEntity item in meioErpPurchaseDetailList)
                    {
                        item.Create();
                        item.PurchaseID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        public void AuditPurchase(string keyValue, MeioErpDocumentAuditRecordEntity meioErpDocumentAuditRecordEntity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                var db = this.BaseRepository().BeginTrans();
                try
                {
                    var meioErpPurchaseEntityTmp = GetMeioErpPurchaseEntity(keyValue);
                    meioErpPurchaseEntityTmp.AuditStatus = meioErpDocumentAuditRecordEntity.AuditResult.ToString();
                    meioErpPurchaseEntityTmp.Modify(keyValue);
                    if (meioErpDocumentAuditRecordEntity.AuditResult == 0)
                    {
                        meioErpPurchaseEntityTmp.Auditor_ID = "";
                        meioErpPurchaseEntityTmp.AuditorName = "";
                        meioErpPurchaseEntityTmp.AuditTime = null;
                    }
                    else
                    {
                        meioErpPurchaseEntityTmp.Auditor_ID = meioErpPurchaseEntityTmp.Modification_Id;
                        meioErpPurchaseEntityTmp.AuditorName = meioErpPurchaseEntityTmp.ModificationName;
                        meioErpPurchaseEntityTmp.AuditTime = DateTime.Now;
                    }
                    db.Update(meioErpPurchaseEntityTmp);

                    meioErpDocumentAuditRecordEntity.Create();
                    meioErpDocumentAuditRecordEntity.DocumentType = "Purchase";
                    meioErpDocumentAuditRecordEntity.DocumentID = keyValue;
                    db.Insert(meioErpDocumentAuditRecordEntity);


                    db.Commit();
                }
                catch (Exception ex)
                {
                    db.Rollback();
                    if (ex is ExceptionEx)
                    {
                        throw;
                    }
                    else
                    {
                        throw ExceptionEx.ThrowServiceException(ex);
                    }
                }
            }
        }
        /// <summary>
        /// 获取待发货的采购单
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseEntity> GetAwaitDeliveryPurchaseList()
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpPurchaseEntity>(p => p.AuditStatus == "1" && p.State!="1").OrderByDescending(o => o.CreationDate);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
