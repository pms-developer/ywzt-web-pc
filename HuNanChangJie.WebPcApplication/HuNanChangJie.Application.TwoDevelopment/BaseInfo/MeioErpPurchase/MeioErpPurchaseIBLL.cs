﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-03 10:46
    /// 描 述：采购单
    /// </summary>
    public interface MeioErpPurchaseIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpPurchaseEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpPurchaseDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpPurchaseDetailEntity> GetMeioErpPurchaseDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpPurchase表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpPurchaseEntity GetMeioErpPurchaseEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpPurchaseDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpPurchaseDetailEntity GetMeioErpPurchaseDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpPurchaseEntity entity,List<MeioErpPurchaseDetailEntity> meioErpPurchaseDetailList,string deleteList,string type);
        #endregion
        void AuditPurchase(string keyValue, MeioErpDocumentAuditRecordEntity meioErpDocumentAuditRecordEntity);
        IEnumerable<MeioErpPurchaseEntity> GetAwaitDeliveryPurchaseList();
    }
}
