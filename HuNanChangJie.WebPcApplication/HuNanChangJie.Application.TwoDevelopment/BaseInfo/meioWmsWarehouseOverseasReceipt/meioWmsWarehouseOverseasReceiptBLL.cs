﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-20 11:46
    /// 描 述：海外仓收货单
    /// </summary>
    public class meioWmsWarehouseOverseasReceiptBLL : meioWmsWarehouseOverseasReceiptIBLL
    {
        private meioWmsWarehouseOverseasReceiptService meioWmsWarehouseOverseasReceiptService = new meioWmsWarehouseOverseasReceiptService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioWmsWarehouseOverseasReceiptService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseOverseasReceiptEntity GetmeioWmsWarehouseOverseasReceiptEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseOverseasReceiptService.GetmeioWmsWarehouseOverseasReceiptEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseOverseasReceiptEntity GetOverseasReceipt(string keyValue)
        {
            try
            {
                return meioWmsWarehouseOverseasReceiptService.GetOverseasReceipt(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public meioWmsWarehouseOverseasReceiptBoxDetailEntity GeteOverseasReceiptBox(string keyValue, string BoxTypeNo)
        {
            try
            {
                return meioWmsWarehouseOverseasReceiptService.GeteOverseasReceiptBox(keyValue, BoxTypeNo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptBoxDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptBoxDetailEntity> GetmeioWmsWarehouseOverseasReceiptBoxDetailEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseOverseasReceiptService.GetmeioWmsWarehouseOverseasReceiptBoxDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptSkuDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptSkuDetailEntity> GetmeioWmsWarehouseOverseasReceiptSkuDetailEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseOverseasReceiptService.GetmeioWmsWarehouseOverseasReceiptSkuDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<meioWmsWarehouseOverseasReceiptSkuDetailEntity> GetOverseasReceiptSkuDetailList(string keyValue)
        {
            try
            {
                return meioWmsWarehouseOverseasReceiptService.GetOverseasReceiptSkuDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptLogistics表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptLogisticsEntity> GetmeioWmsWarehouseOverseasReceiptLogisticsEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseOverseasReceiptService.GetmeioWmsWarehouseOverseasReceiptLogisticsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }

        }


        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptAtta表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptAttaEntity> GetmeioWmsWarehouseOverseasReceiptAttaEntity(string keyValue)
        {
            try
            {
                return meioWmsWarehouseOverseasReceiptService.GetmeioWmsWarehouseOverseasReceiptAttaEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioWmsWarehouseOverseasReceiptService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="boxDetails"></param>
        /// <param name="skuDetails"></param>
        /// <param name="logistices"></param>
        /// <param name="attas"></param>
        public void AddEntity(meioWmsWarehouseOverseasReceiptEntity entity
            , List<meioWmsWarehouseOverseasReceiptBoxDetailEntity> boxDetails
            , List<meioWmsWarehouseOverseasReceiptSkuDetailEntity> skuDetails
            , List<meioWmsWarehouseOverseasReceiptLogisticsEntity> logistices
            , List<meioWmsWarehouseOverseasReceiptAttaEntity> attas)
        {
            try
            {
                meioWmsWarehouseOverseasReceiptService.AddEntity(entity, boxDetails, skuDetails, logistices,attas);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseOverseasReceiptEntity entity,meioWmsWarehouseOverseasReceiptBoxDetailEntity meioWmsWarehouseOverseasReceiptBoxDetailEntity,meioWmsWarehouseOverseasReceiptSkuDetailEntity meioWmsWarehouseOverseasReceiptSkuDetailEntity,string deleteList,string type)
        {
            try
            {
                meioWmsWarehouseOverseasReceiptService.SaveEntity(keyValue, entity,meioWmsWarehouseOverseasReceiptBoxDetailEntity,meioWmsWarehouseOverseasReceiptSkuDetailEntity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        ///// <summary>
        ///// 保存实体数据（新增、修改）
        ///// <param name="keyValue">主键</param>
        ///// <summary>
        ///// <returns></returns>
        //public string SaveEntity2(string Code, string UserWarehouse)
        //{
        //    try
        //    {
        //        return meioWmsWarehouseOverseasReceiptService.SaveEntity2(Code ,UserWarehouse);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is ExceptionEx)
        //        {
        //            throw;
        //        }
        //        else
        //        {
        //            throw ExceptionEx.ThrowBusinessException(ex);
        //        }
        //    }
        //}
        #endregion



        ///// <summary>
        ///// 取消收货单
        ///// </summary>
        ///// <param name="Code"></param>
        ///// <returns></returns>
        //public async Task<bool> OrderCancel(string Code)
        //{
        //    try
        //    {
        //        return await meioWmsWarehouseOverseasReceiptService.OrderCancel(Code);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is ExceptionEx)
        //        {
        //            throw;
        //        }
        //        else
        //        {
        //            throw ExceptionEx.ThrowBusinessException(ex);
        //        }
        //    }
        //}

    }
}
