﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-20 11:46
    /// 描 述：海外仓收货单
    /// </summary>
    public class meioWmsWarehouseOverseasReceiptEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 入库单号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 客户
        /// </summary>
        [Column("SHIPPERCODE")]
        public string ShipperCode { get; set; }
        /// <summary>
        /// 入库类型
        /// </summary>
        [Column("RECEIPTTYPE")]
        public string ReceiptType { get; set; }
        /// <summary>
        /// 到仓方式
        /// </summary>
        [Column("DELIVERYMETHOD")]
        public string DeliveryMethod { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        [Column("BUSINESSTYPE")]
        public string BusinessType { get; set; }
        /// <summary>
        /// 箱数
        /// </summary>
        [Column("BOXNUM")]
        public int? BoxNum { get; set; }
        /// <summary>
        /// 产品数
        /// </summary>
        [Column("SKUNUM")]
        public int? SkuNum { get; set; }
        /// <summary>
        /// 入库成本
        /// </summary>
        [Column("STORAGECOST")]
        public decimal? StorageCost { get; set; }
        /// <summary>
        /// 入库状态
        /// </summary>
        [Column("STORAGESTATUS")]
        public string StorageStatus { get; set; }
        /// <summary>
        /// 收货时间
        /// </summary>
        [Column("RECEIPTTIME")]
        public DateTime? ReceiptTime { get; set; }
        /// <summary>
        /// 上架时间
        /// </summary>
        [Column("LISTINGTIME")]
        public DateTime? ListingTime { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("MYAUDITSTATUS")]
        public string MyAuditStatus { get; set; }
        /// <summary>
        /// 审核时间
        /// </summary>
        [Column("MYAUDITTIME")]
        public DateTime? MyAuditTime { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        [Column("MYAUDITUSER")]
        public string MyAuditUser { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("NOTES")]
        public string Notes { get; set; }
        /// <summary>
        /// 是否形成库存
        /// </summary>
        [Column("MeioWmsListing")]
        public bool? MeioWmsListing { get; set; }
        /// <summary>
        /// 是否形成移入数量
        /// </summary>
        [Column("MeioWmsMove")]
        public bool? MeioWmsMove { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 预报托盘数
        /// </summary>
        [Column("FORECASTNUMBERPALLETS")]
        public int? ForecastNumberPallets { get; set; }
        /// <summary>
        /// 预报到达日期
        /// </summary>
        [Column("FORECASTARRIVALDATE")]
        public DateTime? ForecastArrivalDate { get; set; }
        /// <summary>
        /// 货柜号/跟踪号
        /// </summary>
        [Column("CONTAINERTRACKINGNUMBER")]
        public string ContainerTrackingNumber { get; set; }
        /// <summary>
        /// 入库数量
        /// </summary>
        [Column("INVENTORYQUANTITY")]
        public int? InventoryQuantity { get; set; }
        /// <summary>
        /// 入库金额
        /// </summary>
        [Column("INBOUNDAMOUNT")]
        public decimal? InboundAmount { get; set; }
        /// <summary>
        /// 关联单据类型
        /// </summary>
        [Column("ASSOCIATEDDOCUMENTTYPE")]
        public string AssociatedDocumentType { get; set; }
        /// <summary>
        /// 关联单据号
        /// </summary>
        [Column("ASSOCIATEDDOCUMENTNO")]
        public string AssociatedDocumentNo { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        [Column("SUPPLIER")]
        public string Supplier { get; set; }
        /// <summary>
        /// 海外仓单号
        /// </summary>
        [Column("OVERSEASTRACKINGNO")]
        public string OverseasTrackingNo { get; set; }
        /// <summary>
        /// 已收托盘数
        /// </summary>
        [Column("NUMBERRECEIVEDPALLETS")]
        public int? NumberReceivedPallets { get; set; }
        /// <summary>
        /// 已收箱数
        /// </summary>
        [Column("NUMBERRECEIVEDBOX")]
        public int? NumberReceivedBox { get; set; }
        /// <summary>
        /// 已收货数量
        /// </summary>
        [Column("NUMBERRECEIVEDSKU")]
        public int? NumberReceivedSku { get; set; }
        /// <summary>
        /// 上架数量（良）
        /// </summary>
        [Column("NUMBERLISTINGSKUA")]
        public int? NumberListingSkuA { get; set; }
        /// <summary>
        /// 上架数量（次）
        /// </summary>
        [Column("NUMBERLISTINGSKUB")]
        public int? NumberListingSkuB { get; set; }
        /// <summary>
        /// 仓库留言记录
        /// </summary>
        [Column("MESSAGERECORD")]
        public string MessageRecord { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 附件
        /// </summary>
        [Column("AttaFile")]
        public string AttaFile { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.WarehouseID = WarehouseUtil.UserWarehouse;
        }


        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create2()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = "系统推送";
            this.CreationName = "系统推送";
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = "系统推送";
            this.ModificationName = "系统推送";
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.WarehouseID = WarehouseUtil.UserWarehouse;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify2(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = "系统推送";
            this.ModificationName = "系统推送";
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

