﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;

using System.Threading.Tasks;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-20 11:46
    /// 描 述：海外仓收货单
    /// </summary>
    public class meioWmsWarehouseOverseasReceiptService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        //private MeioWmsWarehouseInventoryService meioWmsWarehouseInventoryService = new MeioWmsWarehouseInventoryService();



        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.*
                ");
                strSql.Append("  FROM meioWmsWarehouseOverseasReceipt t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                dp.Add("WarehouseID", WarehouseUtil.UserWarehouse, DbType.String);
                strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["ShipperCode"].IsEmpty())
                {
                    dp.Add("ShipperCode", queryParam["ShipperCode"].ToString(), DbType.String);
                    strSql.Append(" AND t.ShipperCode = @ShipperCode ");
                }
                if (!queryParam["inType"].IsEmpty())
                {
                    dp.Add("inType", queryParam["inType"].ToString(), DbType.String);
                    strSql.Append(" AND t.ReceiptType = @inType ");
                }


                if (!queryParam["inStates"].IsEmpty())
                {
                    dp.Add("inStates", queryParam["inStates"].ToString(), DbType.String);
                    strSql.Append(" AND t.StorageStatus = @inStates ");
                }
                if (!queryParam["bType"].IsEmpty())
                {
                    dp.Add("bType", queryParam["bType"].ToString(), DbType.String);
                    strSql.Append(" AND t.BusinessType = @bType ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }

                /*if (!queryParam["bType"].IsEmpty())
                {
                    dp.Add("bType", queryParam["bType"].ToString(), DbType.String);
                    strSql.Append(" AND t.BusinessType = @bType ");
                }*/



                var list = this.BaseRepository().FindList<meioWmsWarehouseOverseasReceiptEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseOverseasReceiptEntity GetmeioWmsWarehouseOverseasReceiptEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseOverseasReceiptEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public meioWmsWarehouseOverseasReceiptEntity GetOverseasReceipt(string Code)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseOverseasReceiptEntity>(x => x.Code.Equals(Code));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptBoxDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptBoxDetailEntity> GetmeioWmsWarehouseOverseasReceiptBoxDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindList<meioWmsWarehouseOverseasReceiptBoxDetailEntity>(t => t.ReceiptID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public meioWmsWarehouseOverseasReceiptBoxDetailEntity GeteOverseasReceiptBox(string keyValue, string BoxTypeNo)
        {
            try
            {
                return this.BaseRepository().FindEntity<meioWmsWarehouseOverseasReceiptBoxDetailEntity>(x => x.ReceiptID.Equals(keyValue) && x.BoxTypeNo.Equals(BoxTypeNo));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptSkuDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptSkuDetailEntity> GetmeioWmsWarehouseOverseasReceiptSkuDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindList<meioWmsWarehouseOverseasReceiptSkuDetailEntity>(t => t.ReceiptID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<meioWmsWarehouseOverseasReceiptSkuDetailEntity> GetOverseasReceiptSkuDetailList(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindList<meioWmsWarehouseOverseasReceiptSkuDetailEntity>(t => t.ReceiptBoxID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptLogistics表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptLogisticsEntity> GetmeioWmsWarehouseOverseasReceiptLogisticsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindList<meioWmsWarehouseOverseasReceiptLogisticsEntity>(t => t.ReceiptID == keyValue).OrderByDescending(x => x.Sort);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptAtta表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meioWmsWarehouseOverseasReceiptAttaEntity> GetmeioWmsWarehouseOverseasReceiptAttaEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindList<meioWmsWarehouseOverseasReceiptAttaEntity>(t => t.ReceiptID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioWmsWarehouseOverseasReceiptEntity = GetmeioWmsWarehouseOverseasReceiptEntity(keyValue);
                db.Delete<meioWmsWarehouseOverseasReceiptEntity>(t => t.ID == keyValue);
                db.Delete<meioWmsWarehouseOverseasReceiptBoxDetailEntity>(t => t.ReceiptID == meioWmsWarehouseOverseasReceiptEntity.ID);
                db.Delete<meioWmsWarehouseOverseasReceiptSkuDetailEntity>(t => t.ReceiptID == meioWmsWarehouseOverseasReceiptEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        private static readonly object listingLock = new object();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="boxDetails"></param>
        /// <param name="skuDetails"></param>
        /// <param name="logistices"></param>
        /// <param name="attas"></param>
        public void AddEntity(meioWmsWarehouseOverseasReceiptEntity entity
            , List<meioWmsWarehouseOverseasReceiptBoxDetailEntity> boxDetails
            , List<meioWmsWarehouseOverseasReceiptSkuDetailEntity> skuDetails
            , List<meioWmsWarehouseOverseasReceiptLogisticsEntity> logistices
            , List<meioWmsWarehouseOverseasReceiptAttaEntity> attas)
        {
            var receiptOne = this.BaseRepository().FindEntity<meioWmsWarehouseOverseasReceiptEntity>(x => x.Code == entity.Code && x.WarehouseID == entity.WarehouseID);
            var db = this.BaseRepository().BeginTrans();

            try
            {
                if (receiptOne == null)
                {
                    entity.Create2();
                    db.Insert(entity);
                    foreach (var item in boxDetails)
                    {
                        item.ReceiptID = entity.ID;
                        item.WarehouseID = entity.WarehouseID;
                        item.Create2();
                        db.Insert(item);
                    }
                    foreach (var item in skuDetails)
                    {
                        item.ReceiptID = entity.ID;
                        item.WarehouseID = entity.WarehouseID;
                        item.Create2();
                        db.Insert(item);
                    }
                    foreach (var item in logistices)
                    {
                        item.ReceiptID = entity.ID;
                        item.Create2();
                        db.Insert(item);
                    }
                    foreach (var item in attas)
                    {
                        item.ReceiptID = entity.ID;
                        item.Create2();
                        db.Insert(item);
                    }
                    db.Commit();
                }
                else
                {
                    receiptOne.Modify2(receiptOne.ID);
                    receiptOne.BoxNum = entity.BoxNum;
                    receiptOne.SkuNum = entity.SkuNum;
                    receiptOne.StorageCost = entity.StorageCost;
                    receiptOne.StorageStatus = entity.StorageStatus;
                    receiptOne.ReceiptTime = entity.ReceiptTime;
                    receiptOne.ListingTime = entity.ListingTime;
                    receiptOne.MyAuditStatus = entity.MyAuditStatus;
                    receiptOne.MyAuditTime = entity.MyAuditTime;
                    receiptOne.MyAuditUser = entity.MyAuditUser;
                    receiptOne.Notes = entity.Notes;
                    receiptOne.ForecastNumberPallets = entity.ForecastNumberPallets;
                    receiptOne.ForecastArrivalDate = entity.ForecastArrivalDate;
                    receiptOne.ContainerTrackingNumber = entity.ContainerTrackingNumber;
                    receiptOne.InventoryQuantity = entity.InventoryQuantity;
                    receiptOne.InboundAmount = entity.InboundAmount;
                    receiptOne.AssociatedDocumentType = entity.AssociatedDocumentType;
                    receiptOne.AssociatedDocumentNo = entity.AssociatedDocumentNo;
                    receiptOne.Supplier = entity.Supplier;
                    receiptOne.OverseasTrackingNo = entity.OverseasTrackingNo;
                    receiptOne.NumberReceivedPallets = entity.NumberReceivedPallets;
                    receiptOne.NumberReceivedBox = entity.NumberReceivedBox;
                    receiptOne.NumberReceivedSku = entity.NumberReceivedSku;
                    receiptOne.NumberListingSkuA = entity.NumberListingSkuA;
                    receiptOne.NumberListingSkuB = entity.NumberListingSkuB;
                    receiptOne.MessageRecord = entity.MessageRecord;
                    receiptOne.DeliveryMethod = entity.DeliveryMethod;
                    receiptOne.BusinessType = entity.BusinessType;
                    receiptOne.ShipperCode = entity.ShipperCode;
                    db.Update(receiptOne);
                    //直接删除重新新增
                    db.Delete<meioWmsWarehouseOverseasReceiptBoxDetailEntity>(x => x.ReceiptID == receiptOne.ID);
                    foreach (var item in boxDetails)
                    {
                        item.ReceiptID = receiptOne.ID;
                        item.WarehouseID = receiptOne.WarehouseID;
                        item.Create2();
                        db.Insert(item);
                    }
                    db.Delete<meioWmsWarehouseOverseasReceiptSkuDetailEntity>(x => x.ReceiptID == receiptOne.ID);
                    foreach (var item in skuDetails)
                    {
                        item.ReceiptID = receiptOne.ID;
                        item.WarehouseID = receiptOne.WarehouseID;
                        item.Create2();
                        db.Insert(item);
                    }
                    db.Delete<meioWmsWarehouseOverseasReceiptLogisticsEntity>(x => x.ReceiptID == receiptOne.ID);
                    foreach (var item in logistices)
                    {
                        item.ReceiptID = receiptOne.ID;
                        item.Create2();
                        db.Insert(item);
                    }

                    db.Delete<meioWmsWarehouseOverseasReceiptAttaEntity>(x => x.ReceiptID == receiptOne.ID);
                    foreach (var item in attas)
                    {
                        item.ReceiptID = receiptOne.ID;
                        item.Create2();
                        db.Insert(item);
                    }
                    db.Commit();
                }
                #region 计算账单
                Compute(entity, boxDetails, skuDetails, logistices, attas);
                #endregion
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }

        }

        /// <summary>
        /// 计算入库账单
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="boxDetails"></param>
        /// <param name="skuDetails"></param>
        /// <param name="logistices"></param>
        /// <param name="attas"></param>
        public void Compute(meioWmsWarehouseOverseasReceiptEntity entity
            , List<meioWmsWarehouseOverseasReceiptBoxDetailEntity> boxDetails
            , List<meioWmsWarehouseOverseasReceiptSkuDetailEntity> skuDetails
            , List<meioWmsWarehouseOverseasReceiptLogisticsEntity> logistices
            , List<meioWmsWarehouseOverseasReceiptAttaEntity> attas)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var owenerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(p => p.Code == entity.ShipperCode);
                if (owenerEntity != null)
                {
                    //var inStorageAccountList = this.BaseRepository().FindList<MeioErpInStorageFeeAccountEntity>(p => p.InStorageCode == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == owenerEntity.ID);
                    //if (inStorageAccountList.Count() > 0)
                    //{
                    //    var ids = inStorageAccountList.Select(o => o.ID);
                    //    db.Delete<MeioErpInStorageFeeAccountEntity>(p => ids.Contains(p.ID));
                    //    db.Delete<MeioErpInStorageFeeAccountDetailEntity>(p => ids.Contains(p.InStorageFeeAccountID));
                    //}

                    //查询客户合同
                    var nowDate = DateTime.Now.Date;
                    var contractEntity = this.BaseRepository().FindEntity<MeioErpContractEntity>(p => p.ID == owenerEntity.ContractID && p.ServiceStartDate <= nowDate && p.ServiceEndDate >= nowDate);
                    if (contractEntity != null)
                    {
                        if (contractEntity.BillingType == 1)//只有计件生成账单
                        {
                            //删除原有入库账单
                            //var inStorageFeeAccountList = this.BaseRepository().FindList<MeioErpInStorageFeeAccountEntity>(p => p.InStorageCode == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == owenerEntity.ID);
                            var inStorageFeeAccountList = this.BaseRepository().FindList<MeioErpFeeAccountEntity>(p => p.AssociatedNumber == entity.Code && p.WarehouseID == entity.WarehouseID && p.OwnerID == owenerEntity.ID);
                            var accountIds = inStorageFeeAccountList.Select(p => p.ID);
                            //db.Delete<MeioErpInStorageFeeAccountEntity>(p => accountIds.Contains(p.ID));
                            db.Delete<MeioErpFeeAccountEntity>(p => accountIds.Contains(p.ID) && p.FeeAccountType== "InStorageFee");
                            db.Delete<MeioErpInStorageFeeAccountDetailEntity>(p => accountIds.Contains(p.InStorageFeeAccountID));

                            if (!string.IsNullOrEmpty(contractEntity.PriceSheetID))//是否有报价单
                            {
                                var priceSheetIDs = contractEntity.PriceSheetID.Split(',');
                                var priceSheetList = this.BaseRepository().FindList<MeioErpPriceSheetEntity>(p => priceSheetIDs.Contains(p.ID) && p.WarehouseID.Contains(entity.WarehouseID));
                                if (priceSheetList.Count() > 0)
                                {
                                    priceSheetIDs = priceSheetList.Select(p => p.ID).ToArray();
                                    //查询报价单明细计费项
                                    var priceSheetDetailList = this.BaseRepository().FindList<MeioErpPriceSheetDetailEntity>(p => priceSheetIDs.Contains(p.PriceSheetID));

                                    if (priceSheetDetailList.Count() > 0)
                                    {
                                        //计算入库费,卸货费
                                        if (entity.StorageStatus == "已收货" && (entity.BusinessType == "一件代发" || entity.BusinessType == "备货中转"))
                                        {
                                            //柜
                                            int size = 0;
                                            if (entity.DeliveryMethod == "4" || entity.DeliveryMethod == "5")
                                            {
                                                size = 20;
                                            }
                                            if (entity.DeliveryMethod == "6" || entity.DeliveryMethod == "7")
                                            {
                                                size = 40;
                                            }
                                            if (entity.DeliveryMethod == "8")
                                            {
                                                size = 45;
                                            }
                                            if (size > 0)
                                            {
                                                var priceSheetDetailQuery1 = priceSheetDetailList.FirstOrDefault(p => p.ExpenseType == "入库费" && p.ExpenseItem == "卸货费-整柜" && p.ItemDetailMaxValue == size);
                                                if (priceSheetDetailQuery1 != null)
                                                {
                                                    var priceSheetEntity = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQuery1.PriceSheetID);
                                                    MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                    {
                                                        AssociatedNumber = entity.Code,
                                                        OwnerID = owenerEntity.ID,
                                                        WarehouseID = entity.WarehouseID,
                                                        PriceSheetID = priceSheetEntity.ID,
                                                        ExpenseItem = "卸货费-整柜",
                                                        ExpenseType = "入库费",
                                                        ItemName = "卸货费-整柜",
                                                        Currency = priceSheetEntity.Currency,
                                                        TotalAmount = 0,
                                                        CalculatedAmount = 0,
                                                        AdjustmentAmount = 0,
                                                        SettlementStatus = 0,
                                                        FeeAccountType = "InStorageFee",
                                                        AuditStatus = "0",
                                                    };
                                                    meioErpFeeAccountEntity.Create();


                                                    MeioErpInStorageFeeAccountDetailEntity meioErpInStorageFeeAccountDetailEntity = new MeioErpInStorageFeeAccountDetailEntity
                                                    {
                                                        InStorageFeeAccountID = meioErpFeeAccountEntity.ID,
                                                        OperationID = priceSheetDetailQuery1.ItemID,
                                                        PriceSheetID = priceSheetDetailQuery1.PriceSheetID,
                                                        ItemName = priceSheetDetailQuery1.ItemName,
                                                        ExpenseItem = priceSheetDetailQuery1.ExpenseItem,
                                                        Conditions = priceSheetDetailQuery1.Conditions,
                                                        UnitPrice = priceSheetDetailQuery1.Price,
                                                        Expense = priceSheetDetailQuery1.Price,
                                                        Currency = priceSheetEntity?.Currency,
                                                        Quantity = 1,
                                                        InStorageCode = entity.Code,
                                                        InStorageDate = entity.ReceiptTime
                                                    };

                                                    meioErpInStorageFeeAccountDetailEntity.Create();
                                                    db.Insert(meioErpInStorageFeeAccountDetailEntity);

                                                    meioErpFeeAccountEntity.TotalAmount += meioErpInStorageFeeAccountDetailEntity.Expense;
                                                    db.Insert(meioErpFeeAccountEntity);
                                                }
                                            }

                                            //托
                                            if (entity.NumberReceivedPallets > 0)
                                            {
                                                var priceSheetDetailQuery2 = priceSheetDetailList.FirstOrDefault(p => p.ExpenseType == "入库费" && p.ExpenseItem == "卸货费-托盘" && p.ItemDetailMinValue < entity.NumberReceivedPallets && p.ItemDetailMaxValue >= entity.NumberReceivedPallets);
                                                if (priceSheetDetailQuery2 != null)
                                                {
                                                    var priceSheetEntity = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQuery2.PriceSheetID);
                                                    MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                    {
                                                        AssociatedNumber = entity.Code,
                                                        OwnerID = owenerEntity.ID,
                                                        WarehouseID = entity.WarehouseID,
                                                        PriceSheetID = priceSheetEntity.ID,
                                                        ExpenseItem = "卸货费-托盘",
                                                        ExpenseType = "入库费",
                                                        ItemName = "卸货费-托盘",
                                                        Currency = priceSheetEntity.Currency,
                                                        TotalAmount = 0,
                                                        CalculatedAmount = 0,
                                                        AdjustmentAmount = 0,
                                                        SettlementStatus = 0,
                                                        FeeAccountType = "InStorageFee",
                                                        AuditStatus = "0",
                                                    };
                                                    meioErpFeeAccountEntity.Create();

                                                    MeioErpInStorageFeeAccountDetailEntity meioErpInStorageFeeAccountDetailEntity = new MeioErpInStorageFeeAccountDetailEntity
                                                    {
                                                        InStorageFeeAccountID = meioErpFeeAccountEntity.ID,
                                                        OperationID = priceSheetDetailQuery2.ItemID,
                                                        PriceSheetID = priceSheetDetailQuery2.PriceSheetID,
                                                        ItemName = priceSheetDetailQuery2.ItemName,
                                                        ExpenseItem = priceSheetDetailQuery2.ExpenseItem,
                                                        Conditions = priceSheetDetailQuery2.Conditions,
                                                        UnitPrice = priceSheetDetailQuery2.Price,
                                                        Expense = priceSheetDetailQuery2.Price * entity.NumberReceivedPallets,
                                                        Currency = priceSheetEntity?.Currency,
                                                        Quantity = entity.NumberReceivedPallets,
                                                        InStorageCode = entity.Code,
                                                        InStorageDate = entity.ReceiptTime
                                                    };

                                                    meioErpInStorageFeeAccountDetailEntity.Create();
                                                    db.Insert(meioErpInStorageFeeAccountDetailEntity);

                                                    meioErpFeeAccountEntity.TotalAmount += meioErpInStorageFeeAccountDetailEntity.Expense;
                                                    db.Insert(meioErpFeeAccountEntity);
                                                }
                                            }

                                            //箱
                                            if(entity.NumberReceivedBox>0)
                                            {
                                                var priceSheetDetailQuery3 = priceSheetDetailList.FirstOrDefault(p => p.ExpenseType == "入库费" && p.ExpenseItem == "卸货费-散箱" && p.ItemDetailMinValue < entity.NumberReceivedBox && p.ItemDetailMaxValue >= entity.NumberReceivedBox);
                                                if (priceSheetDetailQuery3 != null)
                                                {
                                                    var priceSheetEntity = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQuery3.PriceSheetID);
                                                    MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                    {
                                                        AssociatedNumber = entity.Code,
                                                        OwnerID = owenerEntity.ID,
                                                        WarehouseID = entity.WarehouseID,
                                                        PriceSheetID = priceSheetEntity.ID,
                                                        ExpenseItem = "卸货费-散箱",
                                                        ExpenseType = "入库费",
                                                        ItemName = "卸货费-散箱",
                                                        Currency = priceSheetEntity.Currency,
                                                        TotalAmount = 0,
                                                        CalculatedAmount = 0,
                                                        AdjustmentAmount = 0,
                                                        SettlementStatus = 0,
                                                        FeeAccountType = "InStorageFee",
                                                        AuditStatus = "0",
                                                    };
                                                    meioErpFeeAccountEntity.Create();

                                                    MeioErpInStorageFeeAccountDetailEntity meioErpInStorageFeeAccountDetailEntity = new MeioErpInStorageFeeAccountDetailEntity
                                                    {
                                                        InStorageFeeAccountID = meioErpFeeAccountEntity.ID,
                                                        OperationID = priceSheetDetailQuery3.ItemID,
                                                        PriceSheetID = priceSheetDetailQuery3.PriceSheetID,
                                                        ItemName = priceSheetDetailQuery3.ItemName,
                                                        ExpenseItem = priceSheetDetailQuery3.ExpenseItem,
                                                        Conditions = priceSheetDetailQuery3.Conditions,
                                                        UnitPrice = priceSheetDetailQuery3.Price,
                                                        Expense = priceSheetDetailQuery3.Price * entity.NumberReceivedBox,
                                                        Currency = priceSheetEntity?.Currency,
                                                        Quantity = entity.NumberReceivedBox,
                                                        InStorageCode = entity.Code,
                                                        InStorageDate = entity.ReceiptTime
                                                    };

                                                    meioErpInStorageFeeAccountDetailEntity.Create();
                                                    db.Insert(meioErpInStorageFeeAccountDetailEntity);

                                                    meioErpFeeAccountEntity.TotalAmount += meioErpInStorageFeeAccountDetailEntity.Expense;
                                                    db.Insert(meioErpFeeAccountEntity);
                                                }
                                            }
                                        }
                                        //计算入库费,上架费
                                        if (entity.StorageStatus == "已上架" && (entity.BusinessType == "一件代发" || entity.BusinessType=="备货中转"))
                                        {
                                            var priceSheetDetailQueryList = priceSheetDetailList.Where(p => p.ExpenseType == "入库费" && p.ExpenseItem == "上架费");
                                            if (priceSheetDetailQueryList.Count() > 0)
                                            {
                                                int quantity = 0;
                                                if (entity.BusinessType == "一件代发")
                                                {
                                                    quantity = entity.NumberReceivedSku.GetValueOrDefault();
                                                }
                                                else if (entity.BusinessType == "备货中转")
                                                {
                                                    quantity = entity.NumberReceivedBox.GetValueOrDefault();
                                                    
                                                }
                                                if (quantity > 0)
                                                {
                                                    var priceSheetDetailQuery = priceSheetDetailQueryList.FirstOrDefault(p => p.ItemDetailMinValue < quantity && p.ItemDetailMaxValue >= quantity);
                                                    if (priceSheetDetailQuery != null)
                                                    {
                                                        var priceSheetEntity = priceSheetList.FirstOrDefault(p => p.ID == priceSheetDetailQuery.PriceSheetID);
                                                        MeioErpFeeAccountEntity meioErpFeeAccountEntity = new MeioErpFeeAccountEntity
                                                        {
                                                            AssociatedNumber = entity.Code,
                                                            OwnerID = owenerEntity.ID,
                                                            WarehouseID = entity.WarehouseID,
                                                            PriceSheetID = priceSheetEntity.ID,
                                                            ExpenseItem = "上架费",
                                                            ExpenseType = "入库费",
                                                            ItemName = "上架费",
                                                            Currency = priceSheetEntity.Currency,
                                                            TotalAmount = 0,
                                                            CalculatedAmount = 0,
                                                            AdjustmentAmount = 0,
                                                            SettlementStatus = 0,
                                                            FeeAccountType = "InStorageFee",
                                                            AuditStatus = "0",
                                                        };
                                                        meioErpFeeAccountEntity.Create();

                                                        MeioErpInStorageFeeAccountDetailEntity meioErpInStorageFeeAccountDetailEntity = new MeioErpInStorageFeeAccountDetailEntity
                                                        {
                                                            InStorageFeeAccountID = meioErpFeeAccountEntity.ID,
                                                            OperationID = priceSheetDetailQuery.ItemID,
                                                            PriceSheetID = priceSheetDetailQuery.PriceSheetID,
                                                            ItemName = priceSheetDetailQuery.ItemName,
                                                            ExpenseItem = priceSheetDetailQuery.ExpenseItem,
                                                            Conditions = priceSheetDetailQuery.Conditions,
                                                            UnitPrice = priceSheetDetailQuery.Price,
                                                            Expense = priceSheetDetailQuery.Price * quantity,
                                                            Currency = priceSheetEntity?.Currency,
                                                            Quantity = quantity,
                                                            InStorageCode = entity.Code,
                                                            InStorageDate = entity.ReceiptTime
                                                        };

                                                        meioErpInStorageFeeAccountDetailEntity.Create();
                                                        db.Insert(meioErpInStorageFeeAccountDetailEntity);

                                                        meioErpFeeAccountEntity.TotalAmount += meioErpInStorageFeeAccountDetailEntity.Expense;
                                                        db.Insert(meioErpFeeAccountEntity);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// </summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, meioWmsWarehouseOverseasReceiptEntity entity, meioWmsWarehouseOverseasReceiptBoxDetailEntity meioWmsWarehouseOverseasReceiptBoxDetailEntity, meioWmsWarehouseOverseasReceiptSkuDetailEntity meioWmsWarehouseOverseasReceiptSkuDetailEntity, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioWmsWarehouseOverseasReceiptEntityTmp = GetmeioWmsWarehouseOverseasReceiptEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    meioWmsWarehouseOverseasReceiptBoxDetailEntity.Create();
                    meioWmsWarehouseOverseasReceiptBoxDetailEntity.ReceiptID = entity.ID;
                    db.Insert(meioWmsWarehouseOverseasReceiptBoxDetailEntity);
                    meioWmsWarehouseOverseasReceiptSkuDetailEntity.Create();
                    meioWmsWarehouseOverseasReceiptSkuDetailEntity.ReceiptID = entity.ID;
                    db.Insert(meioWmsWarehouseOverseasReceiptSkuDetailEntity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        //public string SaveEntity2(string Code,string UserWarehouse,DateTime ListingDate=default,int type=0) 
        //{
        //    var db = this.BaseRepository().BeginTrans();
        //    string erorr = "";
        //    try
        //    {
        //        if (ListingDate == default)
        //        {
        //            ListingDate = DateTime.Now;
        //        }
        //        var WarehouseID = UserWarehouse;
        //        var OverseasReceiptEntity = this.BaseRepository().FindEntity<meioWmsWarehouseOverseasReceiptEntity>(x=>x.Code.Equals(Code) && x.WarehouseID.Equals(WarehouseID));
        //        if (OverseasReceiptEntity != null)
        //        {
        //            var Area = this.BaseRepository().FindEntity<meioWmsAreaEntity>(x => x.name.Contains("海外") && x.WarehouseID == WarehouseID);
        //            if (Area == null)
        //            {
        //                erorr = "[海外仓]区域不存在";
        //                return erorr;
        //            }
        //            var Stor = this.BaseRepository().FindEntity<meioWmsStorageLocationEntity>(x => x.Area == Area.ID && x.WarehouseID == WarehouseID);
        //            if (Stor == null)//
        //            {
        //                erorr = "货位不存在";
        //                return erorr;
        //            }
        //            var ship = this.BaseRepository().FindEntity<meioWmsShipperEntity>(x => x.Code == OverseasReceiptEntity.ShipperCode && x.WarehouseID == WarehouseID);
        //            if (ship == null)//货主控制
        //            {
        //                erorr = "客户不存在";
        //                return erorr;
        //            }
        //            var ShipperCode = ship.ID;
        //            var ShipperName = ship.Name;
        //            var SkuType = "";
        //            var StorageLocation = Stor.Code;
        //            if (OverseasReceiptEntity.BusinessType.Equals("备货中转"))//按箱加库存
        //            {
        //                var meioWmsWarehouseOverseasReceiptBoxDetail = this.BaseRepository().FindList<meioWmsWarehouseOverseasReceiptBoxDetailEntity>(x => x.ReceiptID.Equals(OverseasReceiptEntity.ID));
        //                if (meioWmsWarehouseOverseasReceiptBoxDetail != null && meioWmsWarehouseOverseasReceiptBoxDetail.Count() > 0)
        //                {
        //                    foreach (var Boxitem in meioWmsWarehouseOverseasReceiptBoxDetail)
        //                    {
        //                        //string InType = "按箱";
        //                        var BoxTypeNo = Boxitem.BoxTypeNo;
        //                        var DiyBoxCode = Boxitem.DiyBoxCode;
        //                        var Num = Boxitem.BoxNum.GetValueOrDefault();
        //                        if (type == 1)
        //                        {
        //                            //meioWmsWarehouseInventoryService.InventoryAllInit2(StorageLocation, BoxTypeNo, DiyBoxCode, "A", WarehouseID);//库存初始化
        //                            meioWmsWarehouseInventoryService.OverseasInventoryInit(Code,BoxTypeNo, DiyBoxCode, WarehouseID,ShipperCode, Boxitem.Size, Boxitem.Weight);//箱库存初始化
        //                        }
        //                        else
        //                        {
        //                          //  CreateInventoryAge(db, InType, BoxTypeNo, ShipperCode, Num, DiyBoxCode, ListingDate, WarehouseID);
        //                            CreateBoxInventory(db, Code, BoxTypeNo, DiyBoxCode, ShipperCode, Boxitem.Size, Boxitem.Weight, Num, Num, StorageLocation, WarehouseID);
        //                            CreateWarehousingEntry(db, Code, Num, BoxTypeNo, DiyBoxCode, StorageLocation, WarehouseID, ShipperCode, ShipperName, "箱子", 2);
        //                            var OverseasReceiptSkuDetail = this.BaseRepository().FindList<meioWmsWarehouseOverseasReceiptSkuDetailEntity>(x => x.ReceiptID.Equals(OverseasReceiptEntity.ID) && x.ReceiptBoxID.Equals(Boxitem.ID));
        //                            if (OverseasReceiptSkuDetail != null && OverseasReceiptSkuDetail.Count() > 0)
        //                            {
        //                                foreach (var SKUitem in OverseasReceiptSkuDetail)
        //                                {
        //                                    var goods = this.BaseRepository().FindEntity<meioWmsGoodsEntity>(x => x.Code.Equals(SKUitem.SKU) && x.WarehouseID.Equals(WarehouseID) && x.ShipperID.Equals(ShipperCode));
        //                                    if (goods == null)
        //                                    {
        //                                        erorr = "仓库【" + WarehouseID + "】没有商品【" + SKUitem.SKU + "】客户【" + ShipperName + "】收货单" + Code;
        //                                        return erorr;
        //                                    }
        //                                    SkuType = goods.Type;
        //                                    var Sku = SKUitem.SKU;
        //                                    var Name = SKUitem.Name;
        //                                    var BoxNum = SKUitem.Num.GetValueOrDefault() * Num;
        //                                    CreateWarehousingEntry(db, Code, BoxNum, Sku, Name, StorageLocation, WarehouseID, ShipperCode, ShipperName, SkuType, 3);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                erorr = "没有产品：海外收货单" + Code + "；箱号：" + BoxTypeNo;
        //                                return erorr;
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    erorr = "没有箱子：海外收货单" + Code;
        //                    return erorr;
        //                }
        //            }
        //            else if (OverseasReceiptEntity.BusinessType.Equals("一件代发"))//按商品加库存
        //            {
        //                var meioWmsWarehouseOverseasReceiptSkuDetail = this.BaseRepository().FindList<meioWmsWarehouseOverseasReceiptSkuDetailEntity>(x => x.ReceiptID.Equals(OverseasReceiptEntity.ID));
        //                if (meioWmsWarehouseOverseasReceiptSkuDetail != null && meioWmsWarehouseOverseasReceiptSkuDetail.Count() > 0)
        //                {
        //                    List<meioWmsWarehouseListingTaskItemEntity> taskItemList = new List<meioWmsWarehouseListingTaskItemEntity>();
        //                    foreach (var Skuitem in meioWmsWarehouseOverseasReceiptSkuDetail)
        //                    {
        //                        var goods = this.BaseRepository().FindEntity<meioWmsGoodsEntity>(x=>x.Code.Equals(Skuitem.SKU) && x.WarehouseID.Equals(WarehouseID) && x.ShipperID.Equals(ShipperCode));
        //                        if (goods==null)
        //                        {
        //                            erorr = "仓库【"+WarehouseID+"】没有商品【"+ Skuitem.SKU + "】客户【"+ ShipperName + "】收货单" + Code;
        //                            return erorr;
        //                        }
        //                        SkuType = goods.Type;
        //                        string InType = "按产品";
        //                        var Sku = Skuitem.SKU;
        //                        var Name = Skuitem.Name;
        //                        var Num = Skuitem.Num.GetValueOrDefault();
        //                        if (type==1)
        //                        {
        //                            meioWmsWarehouseListingTaskItemEntity listingTaskItemEntity = new meioWmsWarehouseListingTaskItemEntity();
        //                            listingTaskItemEntity.GoodCode = Sku;
        //                            listingTaskItemEntity.GoodName = Name;
        //                            listingTaskItemEntity.TotalNum = Num;
        //                            listingTaskItemEntity.FromStorageLocation = StorageLocation;
        //                            listingTaskItemEntity.ToStorageLocation = StorageLocation;
        //                            taskItemList.Add(listingTaskItemEntity);
        //                        }
        //                        else
        //                        {
        //                            //CreateInventoryAge(db, InType, Sku, ShipperCode, Num, Name, ListingDate, WarehouseID);
        //                            CreateWarehousingEntry(db, Code, Num, Sku, Name, StorageLocation, WarehouseID, ShipperCode, ShipperName, SkuType);
        //                        }

        //                    }
        //                    if (type == 1)
        //                    {
        //                        CreateListingTask(db, Code, ShipperCode, WarehouseID, taskItemList);
        //                    }
        //                    else
        //                    {
        //                        UpdateListingTask(db,Code,ShipperCode,WarehouseID);
        //                    }
        //                }
        //                else
        //                {
        //                    erorr = "没有产品：海外收货单" + Code;
        //                    return erorr;
        //                }
        //            }
        //            else
        //            {
        //                erorr = "没有业务类型;"+ OverseasReceiptEntity.BusinessType + " ：海外收货单" + Code;
        //                return erorr;
        //            }
        //        }
        //        else
        //        {
        //            erorr = "未找到海外收货单" + Code;
        //            return erorr;
        //        }
        //        db.Commit();
        //        return erorr;
        //    }
        //    catch (Exception ex)
        //    {
        //        db.Rollback();
        //        if (ex is ExceptionEx)
        //        {
        //            throw;
        //        }
        //        else
        //        {
        //            throw ExceptionEx.ThrowServiceException(ex);
        //        }
        //    }
        //}

        //public void CreateBoxInventory(IRepository db,string OverseasCode, string BoxTypeNo, string DiyBoxCode, string ShipperCode, string Size, string Weight, int TotalNum, int CanNum,string StorageLocation, string WarehouseID, int LockNum = 0, int InLineNum = 0)
        //{
        //    meioWmsWarehouseInventoryService.OverseasInventoryInit(OverseasCode, BoxTypeNo, DiyBoxCode, WarehouseID);//箱库存初始化
        //    var boxinventory = this.BaseRepository().FindList<meioWmsWarehouseBoxInventoryEntity>(x => x.BoxTypeNo.Equals(BoxTypeNo) && x.WarehouseID.Equals(WarehouseID));
        //    if (boxinventory.Count()==1)
        //    {
        //        int oldNum = 0;
        //        int NewNum = 0;
        //        meioWmsWarehouseBoxInventoryEntity entity = boxinventory.FirstOrDefault();
        //        oldNum = entity.TotalNum.GetValueOrDefault();
        //        NewNum = entity.TotalNum.GetValueOrDefault() + TotalNum;
        //       // entity.OverseasCode = OverseasCode;
        //        entity.BoxTypeNo = BoxTypeNo;
        //        entity.DiyBoxCode = DiyBoxCode;
        //        entity.ShipperCode = ShipperCode;
        //        entity.Size = Size;
        //        entity.Weight = Weight;
        //        entity.TotalNum = entity.TotalNum+ TotalNum;
        //        entity.LockNum = LockNum;
        //        entity.InLineNum = InLineNum;
        //        entity.CanNum = entity.TotalNum;
        //        entity.WarehouseID = WarehouseID;
        //        db.Update(entity);



        //        meioWmsWarehouseInventoryRcordsEntity InventoryRcords = new meioWmsWarehouseInventoryRcordsEntity();
        //        InventoryRcords.Create2();
        //        InventoryRcords.OverseasType = 1;
        //        InventoryRcords.ShipperID = ShipperCode;
        //        InventoryRcords.ShipperName = ShipperCode;
        //        InventoryRcords.InventoryID = entity.ID;
        //        InventoryRcords.Type = "海外收货入库";
        //        InventoryRcords.No = OverseasCode;//生成单号
        //        InventoryRcords.GoodCode = BoxTypeNo;
        //        InventoryRcords.GoodName = DiyBoxCode;
        //        InventoryRcords.OperUser = InventoryRcords.CreationName;
        //        InventoryRcords.OperDate = InventoryRcords.CreationDate;
        //        InventoryRcords.OperID = InventoryRcords.Creation_Id;
        //        InventoryRcords.StorageLocation = StorageLocation;
        //        InventoryRcords.InOut = "IN";
        //        InventoryRcords.Num = TotalNum;
        //        InventoryRcords.OldNum = oldNum;
        //        InventoryRcords.NewNum = NewNum;
        //        InventoryRcords.WarehouseID = WarehouseID;
        //        db.Insert(InventoryRcords);
        //    }
        //}


        //public void CreateInventoryAge(IRepository db,string InType, string Code, string ShipperCode, int Num, string Name,DateTime ListingDate, string WarehouseID)
        //{
        //    meioWmsWarehouseInventoryAgeEntity entity = new meioWmsWarehouseInventoryAgeEntity();
        //    entity.Create2(WarehouseID);
        //    entity.Num = Num;
        //    entity.Name = Name;
        //    entity.Code = Code;
        //    entity.ShipperCode = ShipperCode;
        //    entity.InType = InType;
        //    entity.InventoryStatisticsDate = DateTime.Now.Date;
        //    var ts = entity.InventoryStatisticsDate.Value.Subtract(ListingDate);
        //    entity.InventoryAge = ts.Days;
        //    entity.ListingDate = ListingDate;

        //    db.Insert(entity);
        //}

        //public void CreateWarehousingEntry(IRepository db,string Code, int num,string ProductCode,string ProductName,string StorageLocation,string WarehouseID,string ShipperID,string ShipperName,string Skutype,int type=1)
        //{
        //   // string erorr = "";
        //    try
        //    {
        //        if (type!=3)
        //        {
        //            //创建入库单
        //            var entry = new meioWmsWarehouseWarehousingEntryEntity();
        //            entry.Create2();
        //            entry.InstructionsID = "";
        //            entry.Code = DateTime.Now.ToString("yyyy-MM-ddHH:mm:ss") + Guid.NewGuid().ToString().Substring(0, 8);
        //            entry.ShipperID = ShipperID;
        //            entry.ReceiptCode = Code;
        //            entry.Type = "海外收货入库";
        //            entry.StartDate = DateTime.Now;
        //            entry.WarehouseID = WarehouseID;
        //            db.Insert(entry);
        //            //创建入库单明细
        //            var entryDetail = new meioWmsWarehouseWarehousingEntryDetailEntity();
        //            entryDetail.Create2();
        //            entryDetail.EntryID = entry.ID;
        //            entryDetail.Code = ProductCode;
        //            entryDetail.Name = ProductName;
        //            entryDetail.Unit = "";
        //            entryDetail.UnitNum = 1;
        //            entryDetail.TotalNum = num;
        //            entryDetail.ReceivedNum = num;
        //            entryDetail.CollectedNum = 0;
        //            entryDetail.ReceiptDetailID = "";
        //            db.Insert(entryDetail);
        //            //创建库存批次
        //            meioWmsWarehouseWarehousingEntryDetailTakeRecordEntity takeRecord = new meioWmsWarehouseWarehousingEntryDetailTakeRecordEntity();
        //            takeRecord.Create2();
        //            takeRecord.EntryID = entry.ID;
        //            takeRecord.EntryDetailID = entryDetail.ID;
        //            takeRecord.Num = num;
        //            takeRecord.InNum = num;
        //            takeRecord.Unit = "";
        //            takeRecord.UnitNum = 1;
        //            takeRecord.QualityStatus = "A";
        //            takeRecord.EntryDetailTakeNo = Code;
        //            takeRecord.GoodsCode = entryDetail.Code;
        //            takeRecord.GoodsName = entryDetail.Name;
        //            takeRecord.ShipperID = entry.ShipperID;
        //            takeRecord.WarehouseID = WarehouseID;
        //            db.Insert(takeRecord);
        //            //创建库存批次记录
        //            int tNum = this.BaseRepository()
        //                   .FindList<meioWmsWarehouseWarehousingEntryDetailTakeRecordEntity>(x => x.EntryDetailTakeNo == takeRecord.EntryDetailTakeNo
        //                   && x.GoodsCode == entryDetail.Code && x.WarehouseID == WarehouseID).Sum(x => x.InNum).GetValueOrDefault();

        //            meioWmsWarehouseWarehousingEntryDetailTakeTransactionRecordEntity tr = new meioWmsWarehouseWarehousingEntryDetailTakeTransactionRecordEntity();
        //            tr.Create2();
        //            tr.ShipperID = entry.ShipperID;
        //            tr.ShipperName = ShipperName;
        //            tr.EntryDetailTakeNo = takeRecord.EntryDetailTakeNo;
        //            tr.Code = entry.Code;
        //            tr.GoodsCode = ProductCode;
        //            tr.GoodsName = ProductName;
        //            tr.GoodsCate = Skutype;
        //            tr.QualityStatus = "A";
        //            tr.InOut = "IN";
        //            tr.Num = num;
        //            tr.OldNum = tNum;
        //            tr.NewNum = tNum + num;
        //            tr.WarehouseID = WarehouseID;
        //            db.Insert(tr);
        //        }
        //        if (type!=2)
        //        {
        //            //加库存
        //            meioWmsWarehouseInventoryService.OverseasAddInventory(StorageLocation, num, ProductCode, ProductName, "A", "海外收货入库", Code, ShipperID, type, WarehouseID);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is ExceptionEx)
        //        {
        //            throw;
        //        }
        //        else
        //        {
        //            throw ExceptionEx.ThrowServiceException(ex);
        //        }
        //    }
        //}

        //public void CreateBoxMove(List<meioWmsWarehouseOverseasReceiptBoxDetailEntity> boxDetailEntities)
        //{

        //}

        //public void CreateListingTask(IRepository db,string Code,string ShipperID,string WarehouseID, List<meioWmsWarehouseListingTaskItemEntity> taskItemList)
        //{
        //    meioWmsWarehouseListingTaskEntity listingTask = new meioWmsWarehouseListingTaskEntity();
        //    listingTask.Create2(WarehouseID);
        //    listingTask.Type = "上架";
        //    listingTask.State = "待执行";
        //    listingTask.Code = TimeCode("PL");
        //    listingTask.OrderNo = Code;
        //    listingTask.ExecuteStartTime=DateTime.Now;
        //    listingTask.ExecuteEndTime=DateTime.Now;
        //    listingTask.ShipperID= ShipperID;
        //    db.Insert(listingTask);

        //    List<meioWmsWarehouseListingTaskItemEntity> MoveTaskItems = new List<meioWmsWarehouseListingTaskItemEntity>();
        //    taskItemList.ForEach(x=>{
        //        //创建上架任务
        //        meioWmsWarehouseListingTaskItemEntity taskItem = new meioWmsWarehouseListingTaskItemEntity();
        //        taskItem.Create2();
        //        taskItem.TaskID = listingTask.ID;
        //        taskItem.GoodCode = x.GoodCode;
        //        taskItem.GoodName = x.GoodName;
        //        taskItem.TotalNum = x.TotalNum;
        //        taskItem.FromStorageLocation = x.FromStorageLocation;
        //        taskItem.FromNum = 0;
        //        taskItem.ToStorageLocation = x.ToStorageLocation;
        //        taskItem.ToNum = 0;
        //        taskItem.State = "执行中";
        //        taskItem.QualityState = "A";
        //        taskItem.WarehouseID = WarehouseID;
        //        //taskItem.WorkOrderDetailID = d.ID;
        //        db.Insert(taskItem);
        //        MoveTaskItems.Add(taskItem);

        //    });

        //    #region 根据上架任务到货位及商品创建库存以便展示移入数量
        //    MoveTaskItems.ForEach(t =>
        //    {
        //        meioWmsWarehouseInventoryService.InventoryAllInit2(t.ToStorageLocation, t.GoodCode, t.GoodName, "A",WarehouseID);
        //    });
        //    #endregion
        //}

        //public void UpdateListingTask(IRepository db, string Code, string ShipperID, string WarehouseID,int type=0)
        //{
        //    var listingtask = this.BaseRepository().FindEntity<meioWmsWarehouseListingTaskEntity>(x=>x.OrderNo.Equals(Code) && x.ShipperID.Equals(ShipperID) && x.WarehouseID.Equals(WarehouseID));
        //    if (type == 1)
        //    {
        //        listingtask = this.BaseRepository().FindEntity<meioWmsWarehouseListingTaskEntity>(x => x.OrderNo.Equals(Code) && x.WarehouseID.Equals(WarehouseID));
        //    }
        //    listingtask.Modify2(listingtask.ID);
        //    var listingtasklist = this.BaseRepository().FindList<meioWmsWarehouseListingTaskItemEntity>(x => x.TaskID.Equals(listingtask.ID));
        //    foreach (var item in listingtasklist)
        //    {
        //        item.State = "已关闭";
        //        db.Update(item);
        //    }
        //}

        ///// <summary>
        ///// 当前时间生成单号
        ///// </summary>
        ///// <returns></returns>
        //public string TimeCode(string Code)
        //{
        //    DateTime time = DateTime.Now; // 将DateTime转换为字符串
        //    string year = time.Year.ToString("D4"); // 至少4位数字的年份
        //    string month = time.Month.ToString("D2"); // 至少2位数字的月份
        //    string day = time.Day.ToString("D2"); // 至少2位数字的日期
        //    string hour = time.Hour.ToString("D2"); // 至少2位数字的小时
        //    string minute = time.Minute.ToString("D2"); // 至少2位数字的分钟
        //    string second = time.Second.ToString("D2"); // 至少2位数字的秒
        //    return Code + year + month + day + hour + minute + second;
        //}
        //#endregion



        ///// <summary>
        ///// 取消收货单
        ///// </summary>
        ///// <param name="Code"></param>
        ///// <returns></returns>
        //public async Task<bool> OrderCancel(string Code)
        //{
        //    try
        //    {
        //        var r = this.BaseRepository()
        //        .FindEntity<meioWmsWarehouseOverseasReceiptEntity>(x => x.Code == Code && x.WarehouseID == WarehouseUtil.UserWarehouse);

        //        if (r == null)
        //        {
        //            throw new ExceptionEx("未找到该订单", new Exception());
        //        }

        //        if (r.StorageStatus != "待入库")
        //        {
        //            throw new ExceptionEx("只有待入库状态可以取消订单", new Exception());
        //        }
        //        Console.WriteLine(r.ShipperCode);
        //        //根据货主编码获取appkey和appScrect
        //        ILingxingWmsExternalRequest lingxingWmsExternalRequest = new LingxingWmsExternalRequest("dfc02d83a71e497e9bf356903dcc5829", "966e5c15992145f9bfcbf91c6ca759f5");
        //        batchCancelSendModel bcs = new batchCancelSendModel();
        //        bcs.data = new batchCancelSendModelData();
        //        bcs.data.inboundOrderNoList = new List<string>
        //        {
        //            r.OverseasTrackingNo
        //        };
        //        return await lingxingWmsExternalRequest.batchCancelSend(bcs);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is ExceptionEx)
        //        {
        //            throw;
        //        }
        //        else
        //        {
        //            throw ExceptionEx.ThrowServiceException(ex);
        //        }
        //    }

        //}

        #endregion
    }
}
