﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-20 11:46
    /// 描 述：海外仓收货单
    /// </summary>
    public interface meioWmsWarehouseOverseasReceiptIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseOverseasReceiptEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        meioWmsWarehouseOverseasReceiptEntity GetmeioWmsWarehouseOverseasReceiptEntity(string keyValue);
        meioWmsWarehouseOverseasReceiptEntity GetOverseasReceipt(string keyValue);
        meioWmsWarehouseOverseasReceiptBoxDetailEntity GeteOverseasReceiptBox(string keyValue, string BoxTypeNo);
        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptBoxDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseOverseasReceiptBoxDetailEntity> GetmeioWmsWarehouseOverseasReceiptBoxDetailEntity(string keyValue);
        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptSkuDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseOverseasReceiptSkuDetailEntity> GetmeioWmsWarehouseOverseasReceiptSkuDetailEntity(string keyValue);
        IEnumerable<meioWmsWarehouseOverseasReceiptSkuDetailEntity> GetOverseasReceiptSkuDetailList(string keyValue);



        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptLogistics表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseOverseasReceiptLogisticsEntity> GetmeioWmsWarehouseOverseasReceiptLogisticsEntity(string keyValue);


        /// <summary>
        /// 获取meioWmsWarehouseOverseasReceiptAtta表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        IEnumerable<meioWmsWarehouseOverseasReceiptAttaEntity> GetmeioWmsWarehouseOverseasReceiptAttaEntity(string keyValue);


        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="boxDetails"></param>
        /// <param name="skuDetails"></param>
        /// <param name="logistices"></param>
        /// <param name="attas"></param>
        void AddEntity(meioWmsWarehouseOverseasReceiptEntity entity
            , List<meioWmsWarehouseOverseasReceiptBoxDetailEntity> boxDetails
            , List<meioWmsWarehouseOverseasReceiptSkuDetailEntity> skuDetails
            , List<meioWmsWarehouseOverseasReceiptLogisticsEntity> logistices
            , List<meioWmsWarehouseOverseasReceiptAttaEntity> attas);

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, meioWmsWarehouseOverseasReceiptEntity entity,meioWmsWarehouseOverseasReceiptBoxDetailEntity meioWmsWarehouseOverseasReceiptBoxDetailEntity,meioWmsWarehouseOverseasReceiptSkuDetailEntity meioWmsWarehouseOverseasReceiptSkuDetailEntity,string deleteList,string type);
        //string SaveEntity2(string Code, string UserWarehouse);
        #endregion


        ///// <summary>
        ///// 取消收货单
        ///// </summary>
        ///// <param name="Code"></param>
        ///// <returns></returns>
        //Task<bool> OrderCancel(string Code);

    }
}
