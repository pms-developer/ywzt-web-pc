﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-08-29 20:38
    /// 描 述：包材，耗材批次库存
    /// </summary>
    public class MeioErpBatchInventoryService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpBatchInventoryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.WarehouseID,
                t.OwnerID,
                t.GoodsCode,
                t.GoodsName,
                t.GoodsType,
                t.InQuantity,
                t.BatchNo,
                t.InStorageCode
                ");
                strSql.Append("  FROM MeioErpBatchInventory t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["GoodsCode"].IsEmpty())
                {
                    dp.Add("GoodsCode", "%" + queryParam["GoodsCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.GoodsCode Like @GoodsCode ");
                }
                if (!queryParam["BatchNo"].IsEmpty())
                {
                    dp.Add("BatchNo", "%" + queryParam["BatchNo"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.BatchNo Like @BatchNo ");
                }
                if (!queryParam["InStorageCode"].IsEmpty())
                {
                    dp.Add("InStorageCode", "%" + queryParam["InStorageCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.InStorageCode Like @InStorageCode ");
                }
                var list=this.BaseRepository().FindList<MeioErpBatchInventoryEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpBatchInventory表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpBatchInventoryEntity GetMeioErpBatchInventoryEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpBatchInventoryEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpBatchInventoryEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpBatchInventoryEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
