﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-10 18:06
    /// 描 述：电商平台
    /// </summary>
    public class MeioErpWarehouseCommercePlatformBLL : MeioErpWarehouseCommercePlatformIBLL
    {
        private MeioErpWarehouseCommercePlatformService meioErpWarehouseCommercePlatformService = new MeioErpWarehouseCommercePlatformService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseCommercePlatformEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpWarehouseCommercePlatformService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseCommercePlatform表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseCommercePlatformEntity GetMeioErpWarehouseCommercePlatformEntity(string keyValue)
        {
            try
            {
                return meioErpWarehouseCommercePlatformService.GetMeioErpWarehouseCommercePlatformEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpWarehouseCommercePlatformService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 编码是否存在
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        public bool IsExistRate(string Code)
        {
            try
            {
               return meioErpWarehouseCommercePlatformService.IsExistRate(Code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWarehouseCommercePlatformEntity entity,string deleteList,string type)
        {
            try
            {
                meioErpWarehouseCommercePlatformService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
