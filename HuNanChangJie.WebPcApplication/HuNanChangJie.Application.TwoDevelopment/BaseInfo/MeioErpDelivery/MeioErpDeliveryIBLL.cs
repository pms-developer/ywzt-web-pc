﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-04 14:45
    /// 描 述：发货单
    /// </summary>
    public interface MeioErpDeliveryIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpDeliveryEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpDeliveryDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpDeliveryDetailEntity> GetMeioErpDeliveryDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpDelivery表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpDeliveryEntity GetMeioErpDeliveryEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpDeliveryDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpDeliveryDetailEntity GetMeioErpDeliveryDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpDeliveryEntity entity,List<MeioErpDeliveryDetailEntity> meioErpDeliveryDetailList,string deleteList,string type);
        #endregion
        void AuditDelivery(string keyValue, MeioErpDocumentAuditRecordEntity meioErpDocumentAuditRecordEntity,MeioErpDeliveryEntity delivery);
        IEnumerable<MeioErpDeliveryEntity> GetMeioErpDeliveryListByPurchaseID(string keyValue);
    }
}
