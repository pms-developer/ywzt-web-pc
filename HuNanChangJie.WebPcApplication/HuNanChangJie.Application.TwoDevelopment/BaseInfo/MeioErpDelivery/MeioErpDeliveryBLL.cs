﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.WebRequestService;
using System.Threading.Tasks;
using System.Linq;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-04 14:45
    /// 描 述：发货单
    /// </summary>
    public class MeioErpDeliveryBLL : MeioErpDeliveryIBLL
    {
        private MeioErpDeliveryService meioErpDeliveryService = new MeioErpDeliveryService();
        private OwnerService ownerService = new OwnerService(); 
        private GoodsService goodsService=new GoodsService();
        private MeioErpPurchaseService meioErpPurchaseService = new MeioErpPurchaseService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpDeliveryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpDeliveryService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpDeliveryDetailEntity> GetMeioErpDeliveryDetailList(string keyValue)
        {
            try
            {
                return meioErpDeliveryService.GetMeioErpDeliveryDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDelivery表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpDeliveryEntity GetMeioErpDeliveryEntity(string keyValue)
        {
            try
            {
                return meioErpDeliveryService.GetMeioErpDeliveryEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpDeliveryDetailEntity GetMeioErpDeliveryDetailEntity(string keyValue)
        {
            try
            {
                return meioErpDeliveryService.GetMeioErpDeliveryDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpDeliveryService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpDeliveryEntity entity,List<MeioErpDeliveryDetailEntity> meioErpDeliveryDetailList,string deleteList,string type)
        {
            try
            {
                meioErpDeliveryService.SaveEntity(keyValue, entity,meioErpDeliveryDetailList,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("10002");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        /// <summary>
        /// 审核包材，耗材发货单
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="meioErpDocumentAuditRecordEntity"></param>
        /// <param name="delivery"></param>
        public void AuditDelivery(string keyValue, MeioErpDocumentAuditRecordEntity meioErpDocumentAuditRecordEntity, MeioErpDeliveryEntity delivery)
        {
            try
            {
                meioErpDeliveryService.AuditDelivery(keyValue, meioErpDocumentAuditRecordEntity);
                if (meioErpDocumentAuditRecordEntity.AuditResult == 1)
                {
                    var ownerEntity = ownerService.GetMeioErpOwnerEntityByCode("MOYC");//货主：美鸥云仓
                    if (ownerEntity != null)
                    {
                        Task.Run(() =>
                        {
                            //调用WMS收货单接口
                            var receiptRequestEntity = new ReceiptRequest
                            {
                                WarehouseID = delivery.ReceivingWarehouseID,
                                model = new ReceiptModelRequest
                                {
                                    Code = delivery.Code,
                                    ID = delivery.ID,
                                    ShipperID = ownerEntity.ID,
                                    endState = "待到货登记",
                                    logisticsCode = delivery.LogisticsNumber,
                                    logisticsTitle = delivery.LogisticsCompany,
                                    startState = "待到货登记",
                                },
                                detail = new List<ReceiptDetailRequest>()
                            };
                            var deliveryDetailList = meioErpDeliveryService.GetMeioErpDeliveryDetailList(keyValue);
                            var goodsCodes = deliveryDetailList.Select(o => o.GoodsCode).ToList();
                            var goodsList = goodsService.GetMeioErpGoodsListByCodes(goodsCodes);
                            var purchaseDetailList=meioErpPurchaseService.GetMeioErpPurchaseDetailList(delivery.PurchaseID);
                            foreach (var item in deliveryDetailList)
                            {
                                var goods = goodsList.FirstOrDefault(p => p.Code == item.GoodsCode);
                                var detail = new ReceiptDetailRequest
                                {
                                    code = item.GoodsCode,
                                    name = item.GoodsName,
                                    PurchaseCode = item.PurchaseCode,
                                    purchaseNum = item.PurchaseQuantity,
                                    receivedNum = 0,
                                    collectedNum = item.DeliveryQuantity,
                                    sendNum = item.DeliveryQuantity,
                                    Lenght = goods.Length,
                                    Width = goods.Width,
                                    Height = goods.Height,
                                    Weight = goods.Weight,
                                    Volume = goods.Volume,
                                    PurchasePrice= purchaseDetailList.FirstOrDefault(f=>f.GoodsCode==item.GoodsCode).UnitPrice,
                                };
                                receiptRequestEntity.detail.Add(detail);
                            }

                            WmsService.PushReceipt(receiptRequestEntity);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<MeioErpDeliveryEntity> GetMeioErpDeliveryListByPurchaseID(string keyValue)
        {
            try
            {
               return  meioErpDeliveryService.GetMeioErpDeliveryListByPurchaseID(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
