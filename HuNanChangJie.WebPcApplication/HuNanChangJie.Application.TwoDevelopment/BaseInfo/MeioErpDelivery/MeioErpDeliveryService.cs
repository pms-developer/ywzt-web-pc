﻿
using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-07-04 14:45
    /// 描 述：发货单
    /// </summary>
    public class MeioErpDeliveryService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpDeliveryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.ReceivingWarehouseID,
                t.PurchaseType,
                t.LogisticsNumber,
                t.LogisticsCompany,
                t.LogisticsState,
                t.AuditStatus,
                t.AuditorName,
                t.AuditTime,
                t.Remark,
                t.CreationDate,
                t.PurchaseCode 
                ");
                strSql.Append("  FROM MeioErpDelivery t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["ReceivingWarehouseID"].IsEmpty())
                {
                    dp.Add("ReceivingWarehouseID", queryParam["ReceivingWarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ReceivingWarehouseID = @ReceivingWarehouseID ");
                }
                if (!queryParam["PurchaseType"].IsEmpty())
                {
                    dp.Add("PurchaseType", queryParam["PurchaseType"].ToString(), DbType.String);
                    strSql.Append(" AND t.PurchaseType = @PurchaseType ");
                }
                if (!queryParam["PurchaseCode"].IsEmpty())
                {
                    dp.Add("PurchaseCode", queryParam["PurchaseCode"].ToString(), DbType.String);
                    strSql.Append(" AND t.PurchaseCode = @PurchaseCode ");
                }
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpDeliveryEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpDeliveryDetailEntity> GetMeioErpDeliveryDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpDeliveryDetailEntity>(t => t.DeliveryID == keyValue);
                var purchaseIDList = list.Select(o => o.PurchaseID).Distinct();
                var purchaseDetailList = this.BaseRepository().FindList<MeioErpPurchaseDetailEntity>(p => purchaseIDList.Contains(p.PurchaseID));
                foreach (var item in list)
                {
                    var purchaseDetailQuery = purchaseDetailList.Where(p => p.PurchaseID == item.PurchaseID && p.GoodsCode == item.GoodsCode);
                    if (purchaseDetailQuery != null)
                    {
                        item.PurchaseDeliveryQuantity = purchaseDetailQuery.Sum(o => o.DeliveryQuantity);
                    }
                    else
                    {
                        item.PurchaseDeliveryQuantity = 0;
                    }
                }
                var query = from item in list orderby item.CreationDate ascending select item;

                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDelivery表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpDeliveryEntity GetMeioErpDeliveryEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpDeliveryEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpDeliveryDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpDeliveryDetailEntity GetMeioErpDeliveryDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpDeliveryDetailEntity>(t => t.DeliveryID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpDeliveryEntity = GetMeioErpDeliveryEntity(keyValue);
                db.Delete<MeioErpDeliveryEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpDeliveryDetailEntity>(t => t.DeliveryID == meioErpDeliveryEntity.ID);

                //开启采购单
                string sql = $"UPDATE MeioErpPurchase SET State='0' WHERE ID='{meioErpDeliveryEntity.PurchaseID}'";
                db.ExecuteBySql(sql);
                //修改采购单商品的已发货数量
                var meioErpDeliveryDetailList = GetMeioErpDeliveryDetailList(keyValue);
                foreach (var item in meioErpDeliveryDetailList)
                {
                    sql = $"UPDATE MeioErpPurchaseDetail SET DeliveryQuantity=DeliveryQuantity-{item.DeliveryQuantity} WHERE PurchaseID='{meioErpDeliveryEntity.PurchaseID}' AND GoodsCode='{item.GoodsCode}'";
                    db.ExecuteBySql(sql);
                }
               
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpDeliveryEntity entity, List<MeioErpDeliveryDetailEntity> meioErpDeliveryDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    //var meioErpDeliveryEntityTmp = GetMeioErpDeliveryEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    this.BaseRepository().Delete<MeioErpDeliveryDetailEntity>(p => p.DeliveryID == keyValue);
                    foreach (MeioErpDeliveryDetailEntity item in meioErpDeliveryDetailList)
                    {
                        item.Create();
                        item.PurchaseID = entity.PurchaseID;
                        item.DeliveryID = keyValue;
                        db.Insert(item);
                    }

                    //var delJson = deleteList.ToObject<List<JObject>>();
                    //foreach (var del in delJson)
                    //{
                    //    var idList = del["idList"].ToString();
                    //    var sql = "";
                    //    if (idList.Contains(","))
                    //    {
                    //        var ids = idList.Split(',');
                    //        foreach (var id in ids)
                    //        {
                    //            sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{id}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (!string.IsNullOrWhiteSpace(idList))
                    //        {
                    //            sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{idList}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //}

                    ////没有生成代码 
                    //var MeioErpDeliveryDetailUpdateList = meioErpDeliveryDetailList.FindAll(i => i.EditType == EditType.Update);
                    //foreach (var item in MeioErpDeliveryDetailUpdateList)
                    //{
                    //    db.Update(item);
                    //}
                    //var MeioErpDeliveryDetailInserList = meioErpDeliveryDetailList.FindAll(i => i.EditType == EditType.Add);
                    //foreach (var item in MeioErpDeliveryDetailInserList)
                    //{
                    //    //item.Create(item.ID);
                    //    item.Create();
                    //    item.PurchaseID = entity.PurchaseID;
                    //    item.DeliveryID = meioErpDeliveryEntityTmp.ID;
                    //    db.Insert(item);
                    //}

                    ////没有生成代码1 

                }
                else
                {
                    //entity.Create(keyValue);
                    entity.Create();
                    db.Insert(entity);
                    foreach (MeioErpDeliveryDetailEntity item in meioErpDeliveryDetailList)
                    {
                        item.Create();
                        item.PurchaseID = entity.PurchaseID;
                        item.DeliveryID = entity.ID;
                        db.Insert(item);
                    }
                }

                //修改采购单发货数量
                if (!string.IsNullOrEmpty(entity.PurchaseID))
                {
                    var purchaseDetailList = this.BaseRepository().FindList<MeioErpPurchaseDetailEntity>(p => p.PurchaseID == entity.PurchaseID);
                    var deliveryDetailList1 = this.BaseRepository().FindList<MeioErpDeliveryDetailEntity>(p => p.PurchaseID == entity.PurchaseID && p.DeliveryID != entity.ID);
                    foreach (var detailEntity in purchaseDetailList)
                    {
                        var deliveryDetailList = meioErpDeliveryDetailList.Where(p => p.GoodsCode == detailEntity.GoodsCode);
                        if (deliveryDetailList.Count() > 0)
                        {
                            detailEntity.DeliveryQuantity = deliveryDetailList1.Sum(o => o.DeliveryQuantity) + deliveryDetailList.Sum(o => o.DeliveryQuantity);
                            db.Update(detailEntity);
                        }
                    }

                    //关闭采购单
                    if (purchaseDetailList.Any(p => p.PurchaseQuantity == p.DeliveryQuantity))
                    {
                        string sql = $"UPDATE MeioErpPurchase SET State='1' WHERE ID='{entity.PurchaseID}'";
                        db.ExecuteBySql(sql);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        public void AuditDelivery(string keyValue, MeioErpDocumentAuditRecordEntity meioErpDocumentAuditRecordEntity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                var db = this.BaseRepository().BeginTrans();
                try
                {
                    var meioErpDeliveryEntityTmp = GetMeioErpDeliveryEntity(keyValue);
                    meioErpDeliveryEntityTmp.AuditStatus = meioErpDocumentAuditRecordEntity.AuditResult.ToString();
                    meioErpDeliveryEntityTmp.Modify(keyValue);
                    if (meioErpDocumentAuditRecordEntity.AuditResult == 0)
                    {
                        meioErpDeliveryEntityTmp.Auditor_ID = "";
                        meioErpDeliveryEntityTmp.AuditorName = "";
                        meioErpDeliveryEntityTmp.AuditTime = null;
                    }
                    else
                    {
                        meioErpDeliveryEntityTmp.Auditor_ID = meioErpDeliveryEntityTmp.Modification_Id;
                        meioErpDeliveryEntityTmp.AuditorName = meioErpDeliveryEntityTmp.ModificationName;
                        meioErpDeliveryEntityTmp.AuditTime = DateTime.Now;
                    }
                    db.Update(meioErpDeliveryEntityTmp);

                    meioErpDocumentAuditRecordEntity.Create();
                    meioErpDocumentAuditRecordEntity.DocumentType = "Delivery";
                    meioErpDocumentAuditRecordEntity.DocumentID = keyValue;
                    db.Insert(meioErpDocumentAuditRecordEntity);


                    db.Commit();
                }
                catch (Exception ex)
                {
                    db.Rollback();
                    if (ex is ExceptionEx)
                    {
                        throw;
                    }
                    else
                    {
                        throw ExceptionEx.ThrowServiceException(ex);
                    }
                }
            }
        }

        public IEnumerable<MeioErpDeliveryEntity> GetMeioErpDeliveryListByPurchaseID(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpDeliveryEntity>(t => t.PurchaseID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
