﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-12 19:45
    /// 描 述：资金流水
    /// </summary>
    public class MeioErpCapitalFlowService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpCapitalFlowEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.OwnerCode,
                t.OwnerName,
                w.Name   WarehouseName,
                t.TransactionType,
                t.SettlementNumber,
                t.TransactionAmount,
                t.TransactionCurrency,
                t.SettlementCurrency,
                t.RateConversionAmount,
                t.CreationDate,
                t.CreationName,
                t.AllAmount
                ");
                strSql.Append("  FROM MeioErpCapitalFlow t  left join  MeioErpWarehouse w on t.WarehouseID=w.ID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["OwnerCode"].IsEmpty())
                {
                    dp.Add("OwnerCode", "%" + queryParam["OwnerCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OwnerCode Like @OwnerCode ");
                }
                if (!queryParam["OwnerName"].IsEmpty())
                {
                    dp.Add("OwnerName", "%" + queryParam["OwnerName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OwnerName Like @OwnerName ");
                }
                if (!queryParam["WarehouseName"].IsEmpty())
                {
                    dp.Add("WarehouseName", "%" + queryParam["WarehouseName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.WarehouseName Like @WarehouseName ");
                }
                if (!queryParam["TransactionType"].IsEmpty())
                {
                    dp.Add("TransactionType", "%" + queryParam["TransactionType"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.TransactionType Like @TransactionType ");
                }
                var list=this.BaseRepository().FindList<MeioErpCapitalFlowEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCapitalFlow表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCapitalFlowEntity GetMeioErpCapitalFlowEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpCapitalFlowEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioErpCapitalFlowEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpCapitalFlowEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion


        /// <summary>
        /// 充值与结算
        /// </summary>
        /// <param name="keyValue">客户ID</param>
        /// <param name="Currency">充值币种</param>
        /// <param name="amount">交易金额</param>
        /// <param name="fullFileName">凭证</param>
        /// <param name="remark">备注</param>
        /// <param name="type">type=1代表充值，type=2代表结算</param>
        /// <returns></returns>
        public bool RechargeAndSettlement(string keyValue,string Currency,decimal amount, string fullFileName, string remark,int type,out string SettlementCode)
        {
            bool result = false;
            var db = this.BaseRepository().BeginTrans();
            SettlementCode = "";
            try
            {
                var ownerentity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(keyValue);
                if (ownerentity!=null)
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("SettlementCode");
                    if (!string.IsNullOrWhiteSpace(ownerentity.Currency) && ownerentity.Currency.Equals(Currency))//充值币种跟结算币种相同
                    {
                        ownerentity.Modify(keyValue);
                        if (type==1)
                        {
                            ownerentity.AccountBalance = ownerentity.AccountBalance + amount;
                        }
                        else
                        {
                            ownerentity.AccountBalance = ownerentity.AccountBalance - amount;//结算不用管信用额度，只要往里扣钱
                            SettlementCode = codeRuleIBLL.GetBillCode("SettlementCode");
                        }
                           
                        db.Update(ownerentity);

                        CreateCapitalFlow(db, ownerentity, Currency, amount, fullFileName, remark, SettlementCode,type);
                        result = true;
                    }
                    else//充值币种跟结算币种不相同
                    {
                        var yearmonth = DateTime.Now.Year+"-"+DateTime.Now.Month;
                        var ToCurrency = ownerentity.Currency;
                        var rateentity= this.BaseRepository().FindEntity<MeioErpWarehouseExchangeRateEntity>(x => x.RateMonth == yearmonth && x.Currency == Currency && x.ToCurrency == ToCurrency);
                        if (rateentity==null)
                        {
                            rateentity = this.BaseRepository().FindEntity<MeioErpWarehouseExchangeRateEntity>(x => x.RateMonth == yearmonth && x.Currency == ToCurrency && x.ToCurrency == Currency);
                            if (rateentity==null)
                            {
                                return result;
                            }
                            var RateConversionAmount = amount / Convert.ToDecimal(rateentity.SetRate);
                            if (type==1)
                            {
                                ownerentity.AccountBalance = ownerentity.AccountBalance + RateConversionAmount;
                            }
                            else
                            {
                                ownerentity.AccountBalance = ownerentity.AccountBalance - RateConversionAmount;
                                SettlementCode = codeRuleIBLL.GetBillCode("SettlementCode");
                            }
                            ownerentity.Modify(keyValue);
                            db.Update(ownerentity);
                            CreateCapitalFlow(db, ownerentity, Currency, amount, fullFileName, remark, SettlementCode,type, RateConversionAmount);
                            result = true;
                        }
                        else
                        {
                            var RateConversionAmount = amount * Convert.ToDecimal(rateentity.SetRate);
                            if (type == 1)
                            {
                                ownerentity.AccountBalance = ownerentity.AccountBalance + RateConversionAmount;
                            }
                            else
                            {
                                ownerentity.AccountBalance = ownerentity.AccountBalance - RateConversionAmount;
                                SettlementCode = codeRuleIBLL.GetBillCode("SettlementCode");
                            }
                            ownerentity.Modify(keyValue);
                            db.Update(ownerentity);
                            CreateCapitalFlow(db, ownerentity, Currency, amount, fullFileName, remark, SettlementCode,type, RateConversionAmount);
                            result = true;
                        }
                            
                    }
                }
                db.Commit();
                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void CreateCapitalFlow(IRepository db, MeioErpOwnerEntity ownerentity, string Currency, decimal amount, string fullFileName, string remark,string SettlementCode, int type,decimal RateConversionAmount=0.00m) {
            UserInfo userInfo = LoginUserInfo.Get();
            MeioErpCapitalFlowEntity meioErpCapitalFlow = new MeioErpCapitalFlowEntity()
            {
                OwnerCode = ownerentity.Code,
                OwnerName = ownerentity.Name,
                TransactionType = type==1?"充值":"结算",
                Note = remark,
                OperationCertificate = fullFileName,
                TransactionAmount = amount,
                SettlementNumber= SettlementCode,
                TransactionCurrency = Currency,
                SettlementCurrency = ownerentity.Currency,
                RateConversionAmount= RateConversionAmount,
                AllAmount = ownerentity.AccountBalance
            };
            meioErpCapitalFlow.Create();
            db.Insert(meioErpCapitalFlow);
        }

    }
}
