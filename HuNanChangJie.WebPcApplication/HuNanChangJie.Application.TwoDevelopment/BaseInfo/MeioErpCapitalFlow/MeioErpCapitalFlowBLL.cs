﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-03-12 19:45
    /// 描 述：资金流水
    /// </summary>
    public class MeioErpCapitalFlowBLL : MeioErpCapitalFlowIBLL
    {
        private MeioErpCapitalFlowService meioErpCapitalFlowService = new MeioErpCapitalFlowService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpCapitalFlowEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpCapitalFlowService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpCapitalFlow表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpCapitalFlowEntity GetMeioErpCapitalFlowEntity(string keyValue)
        {
            try
            {
                return meioErpCapitalFlowService.GetMeioErpCapitalFlowEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpCapitalFlowService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpCapitalFlowEntity entity,string deleteList,string type)
        {
            try
            {
                meioErpCapitalFlowService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion


        /// <summary>
        /// 充值与结算
        /// </summary>
        /// <param name="keyValue">客户ID</param>
        /// <param name="Currency">充值币种</param>
        /// <param name="amount">交易金额</param>
        /// <param name="fullFileName">凭证</param>
        /// <param name="remark">备注</param>
        /// <param name="type">type=1代表充值，type=2代表结算</param>
        /// <returns></returns>
        public bool RechargeAndSettlement(string keyValue, string Currency, decimal amount, string fullFileName, string remark, int type)
        {
            try
            {
               return meioErpCapitalFlowService.RechargeAndSettlement(keyValue, Currency, amount, fullFileName, remark, type, out string SettlementCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
