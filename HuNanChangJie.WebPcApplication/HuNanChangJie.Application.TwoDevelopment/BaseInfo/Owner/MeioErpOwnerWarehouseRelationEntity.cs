﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo.Owner
{
    /// <summary>
    /// 货主仓库关系表
    /// </summary>
    public class MeioErpOwnerWarehouseRelationEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 货主ID
        /// </summary>
        [Column("MEIOERPOWNERID")]
        public string MeioErpOwnerID { get; set; }
        /// <summary>
        /// 货主编码
        /// </summary>
        [Column("OWNERCODE")]
        public string OwnerCode { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        [Column("MEIOERPWAREHOUSEID")]
        public string MeioErpWarehouseID { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        [Column("WAREHOUSECODE")]
        public string WarehouseCode { get; set; }
        /// <summary>
        /// CreatedBy
        /// </summary>
        [Column("CREATEDBY")]
        public string CreatedBy { get; set; }
        /// <summary>
        /// CreatedDate
        /// </summary>
        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }

        #endregion
    }
}
