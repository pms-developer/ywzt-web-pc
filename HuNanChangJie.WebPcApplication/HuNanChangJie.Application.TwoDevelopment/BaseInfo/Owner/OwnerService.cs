﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo.Owner;
using HuNanChangJie.Kafka.Service;
using HuNanChangJie.Util.Operat;
using System.Text.RegularExpressions;
using Microsoft.International.Converters.PinYinConverter;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-15 15:22
    /// 描 述：货主管理
    /// </summary>
    public class OwnerService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private PriceSheetIBLL priceSheetIBLL = new PriceSheetBLL();
        private WarehouseRentIBLL warehouseRentIBLL = new WarehouseRentBLL();
        private OperationIBLL operationIBLL = new OperationBLL();
        private ValueAddedServicesIBLL valueAddedServicesIBLL = new ValueAddedServicesBLL();
        private ContractIBLL contractIBLL = new ContractBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpOwnerEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t1.ServiceStartDate,
                t1.ServiceEndDate,
                t.IsValid,
                t.CreatedBy,
                t.CreatedDate,
                t.WarehouseID,
t.DbUrl,
t.DbIp,
t.DbPort,
t.DbName,
t.DbUsername,
t.DbPassword,
t.ContractingParty,
t.PushAccountType,
t.AccountBalance
                ");
                strSql.Append("  FROM MeioErpOwner t ");
                strSql.Append("  LEFT JOIN MeioErpContract t1 ON t.ContractID = t1.ID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["IsValid"].IsEmpty())
                {
                    dp.Add("IsValid", queryParam["IsValid"].ToString(), DbType.String);
                    strSql.Append(" AND t.IsValid = @IsValid ");
                }
                strSql.Append(" ORDER BY t.CreatedDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpOwnerEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreatedDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpOwnerAccountBalanceChangeRecordEntity> GetAccountBalancePageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.OwnerID,
                t.OperationBy,
                t.OperationTime,
                t.Amount,
                t.OperationType,
                t.OperationCertificate,
                t.CreationName,
                t.CreationDate,
                t.Remark,
                t.OperationID
                ");
                strSql.Append("  FROM MeioErpOwnerAccountBalanceChangeRecord t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["OwnerID"].IsEmpty())
                {
                    dp.Add("OwnerID", queryParam["OwnerID"].ToString(), DbType.String);
                    strSql.Append(" AND t.OwnerID=@OwnerID ");
                }
                if (!queryParam["OperationType"].IsEmpty())
                {
                    dp.Add("OperationType", queryParam["OperationType"].ToString(), DbType.Int32);
                    strSql.Append(" AND t.OperationType=@OperationType ");
                }
                if (!queryParam["Enabled"].IsEmpty())
                {
                    dp.Add("Enabled", queryParam["Enabled"].ToString(), DbType.String);
                    strSql.Append(" AND t.Enabled = @Enabled ");
                }
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpOwnerAccountBalanceChangeRecordEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpOwner表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpOwnerEntity GetMeioErpOwnerEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpOwnerEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根据货主编码获取货主信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MeioErpOwnerEntity GetMeioErpOwnerEntityByCode(string code)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpOwnerEntity>(t => t.Code == code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpContractEntity GetMeioErpContractEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpContractEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根据货主ID获取第三方系统信息
        /// </summary>
        /// <param name="ownerID"></param>
        /// <returns></returns>
        public IEnumerable<MeioErpOwnerThirdpartySystemEntity> GetOwnerThirdpartySystemList(string ownerID)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpOwnerThirdpartySystemEntity>(p => p.OwnerID == ownerID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据货主ID获取第三方系统仓库信息
        /// </summary>
        /// <param name="ownerCode"></param>
        /// <returns></returns>
        public IEnumerable<MeioDataCenterWarehouseEntity> GetOwnerWarehouseList(string ownerCode)
        {
            try
            {
                return this.BaseRepository().FindList<MeioDataCenterWarehouseEntity>(p => p.Owner == ownerCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据货主ID获取第三方系统物流聚道信息
        /// </summary>
        /// <param name="ownerCode"></param>
        /// <returns></returns>
        public IEnumerable<MeioDataCenterChannelInformationEntity> GetOwnerChannelInformationList(string ownerCode)
        {
            try
            {
                return this.BaseRepository().FindList<MeioDataCenterChannelInformationEntity>(p => p.Owner == ownerCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据货主ID获取销售平台信息
        /// </summary>
        /// <param name="ownerID"></param>
        /// <returns></returns>
        public IEnumerable<MeioErpOwnerSalesPlatformEntity> GetOwnerSalesPlatformList(string ownerID)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpOwnerSalesPlatformEntity>(p => p.OwnerID == ownerID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            //var db = this.BaseRepository().BeginTrans();
            try
            {
                //var meioErpContractEntity = GetMeioErpContractEntity(keyValue); 
                //db.Delete<MeioErpOwnerEntity>(t=>t.ContractID == meioErpContractEntity.ID);
                //db.Delete<MeioErpContractEntity>(t=>t.ID == keyValue);
                //db.Commit();
                this.BaseRepository().Delete<MeioErpOwnerEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                //db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public async void SaveEntity(string keyValue, MeioErpContractEntity entity, MeioErpOwnerEntity meioErpOwnerEntity, List<MeioErpOwnerThirdpartySystemEntity> thirdpartySystemList, List<MeioErpOwnerSalesPlatformEntity> salesPlatformList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var OwareList = GetOwnerWarehouseList(meioErpOwnerEntity.Code);
                if (OwareList!=null && OwareList.Count()>0)
                {
                    meioErpOwnerEntity.CenterWarehouse = OwareList.ToList();
                }
                var OchanneList = GetOwnerChannelInformationList(meioErpOwnerEntity.Code);
                if (OchanneList != null && OchanneList.Count() > 0)
                {
                    meioErpOwnerEntity.CenterChannelInformation= OchanneList.ToList();
                }
                if (type == "edit")
                {
                    var entityOld = GetMeioErpOwnerEntity(keyValue);

                    if (entityOld != null)
                    {
                        var MeioErpContractData = contractIBLL.GetMeioErpContractEntity(entityOld.ContractID);
                        if (MeioErpContractData != null)
                        {
                            var MeioErpContractDetailData = contractIBLL.GetMeioErpContractDetailList(MeioErpContractData.ID);
                            //var MeioErpContractRecordData = contractIBLL.GetMeioErpContractRecordList(MeioErpContractData.ID);
                            //var MeioErpDocumentAuditRecordData = contractIBLL.GetDocumentAuditRecordEntityList(MeioErpContractData.ID);

                            string strIds = string.Join(",", MeioErpContractDetailData.Select(o => o.ItemID));
                            string strDetailIds = string.Join(",", MeioErpContractDetailData.Select(o => o.ItemDetailID));
                            var MeioErpWarehouseRentDetail = warehouseRentIBLL.GetMeioErpWarehouseRentDetailList(strDetailIds);
                            var MeioErpOperationDetail = operationIBLL.GetMeioErpOperationDetailList(strDetailIds);
                            var MeioErpWarehouseRentList = warehouseRentIBLL.GetListByIds(strIds);
                            var MeioErpOperationList = operationIBLL.GetListByIds(strDetailIds);
                            var MeioErpValueAddedServices = valueAddedServicesIBLL.GetListByIds(strIds);

                            foreach (var detail in MeioErpContractDetailData)
                            {
                                if (MeioErpWarehouseRentDetail.Any(o => o.ID == detail.ItemDetailID))
                                {
                                    var item = MeioErpWarehouseRentDetail.FirstOrDefault(p => p.ID == detail.ItemDetailID);
                                    detail.ItemDetailName = item.ItemName;
                                    detail.ItemDetailMinValue = item.MinValue;
                                    detail.ItemDetailMaxValue = item.MaxValue;
                                    detail.Unit = MeioErpWarehouseRentList.FirstOrDefault(p => p.ID == detail.ItemID).Unit;
                                }
                                if (MeioErpOperationDetail.Any(o => o.ID == detail.ItemDetailID))
                                {
                                    var item = MeioErpOperationDetail.FirstOrDefault(p => p.ID == detail.ItemDetailID);
                                    detail.ItemDetailName = item.ItemName;
                                    detail.ItemDetailMinValue = item.MinValue;
                                    detail.ItemDetailMaxValue = item.MaxValue;
                                    detail.Unit = MeioErpOperationList.FirstOrDefault(p => p.ID == detail.ItemID).Unit;
                                }
                                if (MeioErpValueAddedServices.Any(o => o.ID == detail.ItemID))
                                {
                                    var item = MeioErpValueAddedServices.FirstOrDefault(p => p.ID == detail.ItemID);
                                    detail.ItemName = item.ItemName;
                                    detail.ItemDetailName = item.ItemDescription;
                                    detail.Unit = item.Unit;
                                    detail.OperationPoint = item.OperationPoint;
                                }
                            }

                            meioErpOwnerEntity.contractDetailValueAddedServices = new List<ContractDetailValueAddedServicesEntity>();
                            foreach (var item in MeioErpContractDetailData.Where(p => p.ItemType == 3))
                            {
                                var ValueAddedServicesEntity = new ContractDetailValueAddedServicesEntity
                                {
                                    ID = item.ID,
                                    ItemID = item.ItemID,
                                    ItemName = item.ItemName,
                                    ItemDetailName = item.ItemDetailName,
                                    Price = item.Price.HasValue ? item.Price.Value : 0,
                                    ContractPrice = item.ContractPrice.HasValue ? item.ContractPrice.Value : 0,
                                    Discount = item.Discount.HasValue ? item.Discount.Value : 0,
                                    Unit = item.Unit,
                                    OperationPoint = item.OperationPoint,
                                    PriceSheetType = item.Type
                                };
                                meioErpOwnerEntity.contractDetailValueAddedServices.Add(ValueAddedServicesEntity);
                            }
                        }
                    }

                    meioErpOwnerEntity.Modify(keyValue);
                    db.Update(meioErpOwnerEntity);
                    if (string.IsNullOrEmpty(entityOld.WarehouseID))
                    {
                        if (!string.IsNullOrEmpty(meioErpOwnerEntity.WarehouseID))
                        {
                            var strWarehouseIds = meioErpOwnerEntity.WarehouseID.Split(',');
                            var warehouseList = this.BaseRepository().FindList<MeioErpWarehouseEntity>(t => strWarehouseIds.Contains(t.ID));
                            meioErpOwnerEntity.Warehouses = warehouseList.ToList();
                            meioErpOwnerEntity.thirdpartySystems = thirdpartySystemList;
                            meioErpOwnerEntity.salesPlatforms = salesPlatformList;
                            meioErpOwnerEntity.UseSystem = string.Join(",", thirdpartySystemList.Select(o => o.SystemName));
                            meioErpOwnerEntity.Platform = string.Join(",", salesPlatformList.Select(o => o.PlatformName));
                            KafkaService.SendMessage(Kafka.Action.add, "owner", meioErpOwnerEntity);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(meioErpOwnerEntity.WarehouseID))
                        {
                            var strWarehouseIds = meioErpOwnerEntity.WarehouseID.Split(',');
                            var warehouseList = this.BaseRepository().FindList<MeioErpWarehouseEntity>(t => strWarehouseIds.Contains(t.ID));
                            meioErpOwnerEntity.Warehouses = warehouseList.ToList();
                            meioErpOwnerEntity.thirdpartySystems = thirdpartySystemList;
                            meioErpOwnerEntity.salesPlatforms = salesPlatformList;
                            meioErpOwnerEntity.UseSystem = string.Join(",", thirdpartySystemList.Select(o => o.SystemName));
                            meioErpOwnerEntity.Platform = string.Join(",", salesPlatformList.Select(o => o.PlatformName));
                            KafkaService.SendMessage(Kafka.Action.update, "owner", meioErpOwnerEntity);
                        }

                    }

                    db.Delete<MeioErpOwnerThirdpartySystemEntity>(p => p.OwnerID == keyValue);
                    foreach (var item in thirdpartySystemList)
                    {
                        item.OwnerID = keyValue;
                        item.Create();
                        db.Insert(item);
                    }
                    db.Delete<MeioErpOwnerSalesPlatformEntity>(p => p.OwnerID == keyValue);
                    foreach (var item in salesPlatformList)
                    {
                        item.OwnerID = keyValue;
                        item.Create();
                        db.Insert(item);
                    }
                }
                else
                {
                    //entity.Create(keyValue);
                    //db.Insert(entity);
                    meioErpOwnerEntity.Create();
                    if (entity != null)
                    {
                        meioErpOwnerEntity.ContractID = entity.ID;
                    }
                    meioErpOwnerEntity.AccountBalance = 0;
                    db.Insert(meioErpOwnerEntity);

                    if (!string.IsNullOrEmpty(meioErpOwnerEntity.WarehouseID))
                    {
                        meioErpOwnerEntity.thirdpartySystems = thirdpartySystemList;
                        meioErpOwnerEntity.salesPlatforms = salesPlatformList;
                        meioErpOwnerEntity.UseSystem = string.Join(",", thirdpartySystemList.Select(o => o.SystemName));
                        meioErpOwnerEntity.Platform = string.Join(",", salesPlatformList.Select(o => o.PlatformName));
                        KafkaService.SendMessage(Kafka.Action.add, "owner", meioErpOwnerEntity);
                    }

                    foreach (var item in thirdpartySystemList)
                    {
                        item.OwnerID = meioErpOwnerEntity.ID;
                        item.Create();
                        db.Insert(item);
                    }

                    foreach (var item in salesPlatformList)
                    {
                        item.OwnerID = meioErpOwnerEntity.ID;
                        item.Create();
                        db.Insert(item);
                    }
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public void SaveWarehouseExternal(string keyValue, List<MeioDataCenterWarehouseEntity> MeioDataCenterWarehouseList, List<MeioDataCenterChannelInformationEntity> MeioDataCenterChannelInformationList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    //没有生成代码 
                    var meioWmsWarehousePrintTemplateGroupDetailUpdateList = MeioDataCenterWarehouseList.ToList();
                    foreach (var item in MeioDataCenterWarehouseList)
                    {
                        item.Modify(item.ID);
                        db.Update(item);
                    }
                    var meioWmsWarehousePrintTemplateGroupDetailInserList = MeioDataCenterChannelInformationList.ToList();
                    foreach (var item in MeioDataCenterChannelInformationList)
                    {
                        item.Modify(item.ID);
                        db.Update(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion
        internal bool SetIsValid(string keyValue, bool isValid)
        {
            try
            {
                var strIds = "'" + keyValue.Replace(",", "','") + "'";
                return this.BaseRepository().ExecuteBySql($"UPDATE dbo.MeioErpOwner Set IsValid={(isValid ? 1 : 0)} WHERE ID IN ({strIds})") > 0;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal bool CheckIsOwnerCode(string code, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(t => t.Code == code && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 充值
        /// </summary>
        /// <returns></returns>
        internal bool TopUp(string keyValue, decimal amount, string fullFileName, string remark)
        {
            var result = false;
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var ownerentity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(keyValue);
                if (ownerentity != null)
                {
                    ownerentity.AccountBalance = ownerentity.AccountBalance + amount;
                    ownerentity.Modify(keyValue);
                    db.Update(ownerentity);

                    UserInfo userInfo = LoginUserInfo.Get();
                    MeioErpOwnerAccountBalanceChangeRecordEntity meioErpOwnerAccountBalanceChangeRecordEntity = new MeioErpOwnerAccountBalanceChangeRecordEntity
                    {
                        OwnerID = ownerentity.ID,
                        OperationBy = userInfo.realName,
                        OperationTime = DateTime.Now,
                        Amount = amount,
                        OperationType = 1,
                        OperationCertificate = fullFileName,
                        Remark = remark,
                        OperationID = Guid.NewGuid().ToString()
                    };
                    meioErpOwnerAccountBalanceChangeRecordEntity.Create();
                    db.Insert(meioErpOwnerAccountBalanceChangeRecordEntity);
                    db.Commit();
                    result = true;
                }

            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            return result;
        }

        /// <summary>
        /// 扣除余额
        /// </summary>
        /// <returns></returns>
        internal bool Deduction(string keyValue, decimal amount, string operationBy, string remark, string operationID)
        {
            var result = false;
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var RecordData = this.BaseRepository().FindList<MeioErpOwnerAccountBalanceChangeRecordEntity>(p => p.OperationID == operationID);
                if (RecordData.Count() == 0)
                {
                    var ownerentity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(keyValue);
                    if (ownerentity != null)
                    {
                        if (ownerentity.AccountBalance >= amount)
                        {
                            ownerentity.AccountBalance = ownerentity.AccountBalance - amount;
                            ownerentity.Modify(keyValue);
                            db.Update(ownerentity);

                            MeioErpOwnerAccountBalanceChangeRecordEntity meioErpOwnerAccountBalanceChangeRecordEntity = new MeioErpOwnerAccountBalanceChangeRecordEntity
                            {
                                OwnerID = ownerentity.ID,
                                OperationBy = operationBy,
                                OperationTime = DateTime.Now,
                                Amount = amount,
                                OperationType = 2,
                                Remark = remark
                            };
                            meioErpOwnerAccountBalanceChangeRecordEntity.Create();
                            db.Insert(meioErpOwnerAccountBalanceChangeRecordEntity);
                            db.Commit();
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            return result;
        }
        /// <summary>
        /// 提现
        /// </summary>
        internal bool Withdraw(string keyValue, string remark)
        {
            var result = false;
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var ownerentity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(keyValue);
                if (ownerentity != null)
                {
                    var amount = ownerentity.AccountBalance;
                    if (ownerentity.AccountBalance > 0)
                    {
                        ownerentity.AccountBalance = 0;
                        ownerentity.Modify(keyValue);
                        db.Update(ownerentity);

                        UserInfo userInfo = LoginUserInfo.Get();
                        MeioErpOwnerAccountBalanceChangeRecordEntity meioErpOwnerAccountBalanceChangeRecordEntity = new MeioErpOwnerAccountBalanceChangeRecordEntity
                        {
                            OwnerID = ownerentity.ID,
                            OperationBy = userInfo.realName,
                            OperationTime = DateTime.Now,
                            Amount = amount,
                            OperationType = 2,
                            Remark = "提现  " + remark
                        };
                        meioErpOwnerAccountBalanceChangeRecordEntity.Create();
                        db.Insert(meioErpOwnerAccountBalanceChangeRecordEntity);
                        db.Commit();
                        result = true;
                    }
                }

            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            return result;
        }

        /// <summary>
        /// 修改信用额度
        /// </summary>
        /// <returns></returns>
        internal bool UpdateCreditLimits(string keyValue, decimal amount)
        {
            var result = false;
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var ownerentity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(keyValue);
                if (ownerentity != null)
                {
                    ownerentity.CreditLimit = amount;
                    ownerentity.Modify(keyValue);
                    db.Update(ownerentity);
                    db.Commit();
                    result = true;
                }

            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            return result;
        }

        /// <summary>
        /// 添加，更新数据中台渠道信息
        /// </summary>
        /// <param name="entity"></param>
        public void SaveDataCenterChannelInformation(MeioDataCenterChannelInformationEntity entity)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (entity != null)
                {
                    var queryEntity= this.BaseRepository().FindEntity<MeioDataCenterChannelInformationEntity>(p => p.Owner == entity.Owner && p.WhCode == entity.WhCode && p.ChannelCode == entity.ChannelCode);
                    if (queryEntity != null)
                    {
                        db.Delete<MeioDataCenterChannelInformationEntity>(p => p.ID == queryEntity.ID);
                        entity.Create(queryEntity.ID);
                    }
                    else
                    {
                        entity.Create();
                    }
                    db.Insert(entity);

                    //新增承运商
                    var carrierEntity= this.BaseRepository().FindEntity<MeioERPGeneralCarrierEntity>(p => p.Name == entity.CarrierName);
                    if (carrierEntity == null)
                    {
                        var carriercode = "";
                        bool isAlpha = Regex.IsMatch(entity.CarrierName, @"^[a-zA-Z]+$");
                        if (isAlpha)
                        {
                            carriercode = entity.CarrierName.ToUpper();
                        }
                        else
                        {
                            carriercode = Str.PinYin(entity.CarrierName).ToUpper(); 
                        }
                        MeioERPGeneralCarrierEntity generalCarrierEntity = new MeioERPGeneralCarrierEntity
                        {
                            Code= carriercode,
                            Name=entity.CarrierName,
                            Price=0,
                            EffectiveTime=DateTime.Now
                        };
                        generalCarrierEntity.Create();
                        db.Insert(generalCarrierEntity);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 添加，更新数据中台仓库信息
        /// </summary>
        /// <param name="entity"></param>
        public void SaveDataCenterWarehouse(MeioDataCenterWarehouseEntity entity)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (entity != null)
                {
                    var queryEntity = this.BaseRepository().FindEntity<MeioDataCenterWarehouseEntity>(p => p.Owner == entity.Owner && p.WhCode == entity.WhCode);
                    if (queryEntity != null)
                    {
                        db.Delete<MeioDataCenterWarehouseEntity>(p => p.ID == queryEntity.ID);
                        entity.Create(queryEntity.ID);
                    }
                    else
                    {
                        entity.Create();
                    }
                    db.Insert(entity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取汉字首字母
        /// </summary>
        /// <param name="chinese"></param>
        /// <returns></returns>
        public string GetFirstLetter(string chinese)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (char c in chinese)
            {
                if (c >= 0x4E00 && c <= 0x9FA5) // 汉字Unicode编码范围
                {
                    int unicode = c - '\0';
                    int index = (unicode > 0 && unicode < 160) ? 0 : (unicode - 160) / 100 + 1; // 根据Unicode值计算汉字的位置
                    stringBuilder.Append("ABCDEFGHJKLMNOPQRSTWXYZ"[index]);
                }
                else
                {
                    stringBuilder.Append(c);
                }
            }
            return stringBuilder.ToString();
        }
    }
}
