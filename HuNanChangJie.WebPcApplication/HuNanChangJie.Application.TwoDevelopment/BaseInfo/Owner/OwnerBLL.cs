﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo.Owner;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-15 15:22
    /// 描 述：货主管理
    /// </summary>
    public class OwnerBLL : OwnerIBLL
    {
        private OwnerService ownerService = new OwnerService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpOwnerEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return ownerService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<MeioErpOwnerAccountBalanceChangeRecordEntity> GetAccountBalancePageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return ownerService.GetAccountBalancePageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpOwner表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpOwnerEntity GetMeioErpOwnerEntity(string keyValue)
        {
            try
            {
                return ownerService.GetMeioErpOwnerEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 根据货主编码获取货主信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MeioErpOwnerEntity GetMeioErpOwnerEntityByCode(string code)
        {
            try
            {
                return ownerService.GetMeioErpOwnerEntityByCode(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpContract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpContractEntity GetMeioErpContractEntity(string keyValue)
        {
            try
            {
                return ownerService.GetMeioErpContractEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 根据货主ID获取第三方信息信息
        /// </summary>
        /// <param name="ownerID"></param>
        /// <returns></returns>
        public IEnumerable<MeioErpOwnerThirdpartySystemEntity> GetOwnerThirdpartySystemList(string ownerID)
        {
            try
            {
                return ownerService.GetOwnerThirdpartySystemList(ownerID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据货主ID获取第三方系统仓库信息
        /// </summary>
        /// <param name="ownerCode"></param>
        /// <returns></returns>
        public IEnumerable<MeioDataCenterWarehouseEntity> GetOwnerWarehouseList(string ownerCode)
        {
            try
            {
                return ownerService.GetOwnerWarehouseList(ownerCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据货主ID获取第三方系统物流聚道信息
        /// </summary>
        /// <param name="ownerCode"></param>
        /// <returns></returns>
        public IEnumerable<MeioDataCenterChannelInformationEntity> GetOwnerChannelInformationList(string ownerCode)
        {
            try
            {
                return ownerService.GetOwnerChannelInformationList(ownerCode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpOwnerSalesPlatformEntity> GetOwnerSalesPlatformList(string ownerID)
        {
            try
            {
                return ownerService.GetOwnerSalesPlatformList(ownerID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                ownerService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void SaveWarehouseExternal(string keyValue, List<MeioDataCenterWarehouseEntity> MeioDataCenterWarehouseList, List<MeioDataCenterChannelInformationEntity> MeioDataCenterChannelInformationList, string deleteList, string type)
        {

            try
            {
                ownerService.SaveWarehouseExternal(keyValue, MeioDataCenterWarehouseList, MeioDataCenterChannelInformationList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpContractEntity entity, MeioErpOwnerEntity meioErpOwnerEntity, List<MeioErpOwnerThirdpartySystemEntity> thirdpartySystemList, List<MeioErpOwnerSalesPlatformEntity> salesPlatformList, string deleteList, string type)
        {
            try
            {
                ownerService.SaveEntity(keyValue, entity, meioErpOwnerEntity,thirdpartySystemList,salesPlatformList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public bool SetIsValid(string keyValue, bool isValid)
        {
            try
            {
                return ownerService.SetIsValid(keyValue, isValid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public bool CheckIsOwnerCode(string code, string id)
        {
            try
            {
                return ownerService.CheckIsOwnerCode(code, id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 充值
        /// </summary>
        /// <returns></returns>
        public bool TopUp(string keyValue, decimal amount, string fullFileName, string remark)
        {
            try
            {
                return ownerService.TopUp(keyValue, amount, fullFileName, remark);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 扣除余额
        /// </summary>
        /// <returns></returns>
        public bool Deduction(string keyValue, decimal amount, string operationBy, string remark, string operationID)
        {
            try
            {
                return ownerService.Deduction(keyValue, amount, operationBy, remark, operationID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 修改信用额度
        /// </summary>
        public bool UpdateCreditLimits(string keyValue, decimal amount)
        {
            try
            {
                return ownerService.UpdateCreditLimits(keyValue, amount);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 提现
        /// </summary>
        public bool Withdraw(string keyValue, string remark)
        {
            try
            {
                return ownerService.Withdraw(keyValue, remark);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public void SaveDataCenterChannelInformation(MeioDataCenterChannelInformationEntity entity)
        {
            try
            {
                ownerService.SaveDataCenterChannelInformation(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void SaveDataCenterWarehouse(MeioDataCenterWarehouseEntity entity)
        {
            try
            {
                ownerService.SaveDataCenterWarehouse(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

    }
}
