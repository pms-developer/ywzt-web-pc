﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.BaseInfo.Owner;
using Microsoft.SqlServer.Server;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-15 15:22
    /// 描 述：货主管理
    /// </summary>
    public interface OwnerIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpOwnerEntity> GetPageList(XqPagination pagination, string queryJson);
        IEnumerable<MeioErpOwnerAccountBalanceChangeRecordEntity> GetAccountBalancePageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpOwner表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpOwnerEntity GetMeioErpOwnerEntity(string keyValue);
        /// <summary>
        /// 根据货主编号获取货主信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        MeioErpOwnerEntity GetMeioErpOwnerEntityByCode(string code);
        /// <summary>
        /// 获取MeioErpContract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpContractEntity GetMeioErpContractEntity(string keyValue);
        /// <summary>
        /// 根据货主ID获取第三方信息
        /// </summary>
        /// <param name="ownerID"></param>
        /// <returns></returns>
        IEnumerable<MeioErpOwnerThirdpartySystemEntity> GetOwnerThirdpartySystemList(string ownerID);
        /// <summary>
        /// 根据货主ID获取第三方系统仓库信息
        /// </summary>
        /// <param name="ownerCode"></param>
        /// <returns></returns>
        IEnumerable<MeioDataCenterWarehouseEntity> GetOwnerWarehouseList(string ownerCode);
        /// <summary>
        /// 根据货主ID获取第三方系统物流聚道信息
        /// </summary>
        /// <param name="ownerCode"></param>
        /// <returns></returns>
        IEnumerable<MeioDataCenterChannelInformationEntity> GetOwnerChannelInformationList(string ownerCode);
        /// <summary>
        /// 根据货主ID获取销售平台信息
        /// </summary>
        /// <param name="ownerID"></param>
        /// <returns></returns>
        IEnumerable<MeioErpOwnerSalesPlatformEntity> GetOwnerSalesPlatformList(string ownerID);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpContractEntity entity,MeioErpOwnerEntity meioErpOwnerEntity, List<MeioErpOwnerThirdpartySystemEntity> thirdpartySystemList, List<MeioErpOwnerSalesPlatformEntity> salesPlatformList, string deleteList,string type);
         void SaveWarehouseExternal(string keyValue, List<MeioDataCenterWarehouseEntity> MeioDataCenterWarehouseList, List<MeioDataCenterChannelInformationEntity> MeioDataCenterChannelInformationList, string deleteList, string type);
        #endregion
        bool SetIsValid(string keyValue, bool isValid);
        bool CheckIsOwnerCode(string code, string id);
        /// <summary>
        /// 充值
        /// </summary>
        /// <returns></returns>
        bool TopUp(string keyValue,decimal amount,string fullFileName, string remark);
        /// <summary>
        /// 扣除余额
        /// </summary>
        /// <returns></returns>
        bool Deduction(string keyValue, decimal amount, string operationBy, string remark, string operationID);
        /// <summary>
        /// 提现
        /// </summary>
        bool Withdraw(string keyValue, string remark);
        /// <summary>
        /// 修改信用额度
        /// </summary>
        bool UpdateCreditLimits(string keyValue, decimal amount);
        void SaveDataCenterChannelInformation(MeioDataCenterChannelInformationEntity entity);
        void SaveDataCenterWarehouse(MeioDataCenterWarehouseEntity entity);
    }
}
