﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-15 15:22
    /// 描 述：货主管理
    /// </summary>
    public class MeioErpOwnerEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 货主编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 货主名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ISVALID")]
        public bool? IsValid { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 合同ID
        /// </summary>
        [Column("CONTRACTID")]
        public string ContractID { get; set; }
        /// <summary>
        /// 货主账号
        /// </summary>
        [Column("ACCOUNTNUMBER")]
        public string AccountNumber { get; set; }
        /// <summary>
        /// 货主密码
        /// </summary>
        [Column("PASSWORD")]
        public string Password { get; set; }
        /// <summary>
        /// CreatedBy
        /// </summary>
        [Column("CREATEDBY")]
        public string CreatedBy { get; set; }
        /// <summary>
        /// CreatedDate
        /// </summary>
        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// ModifyBy
        /// </summary>
        [Column("MODIFYBY")]
        public string ModifyBy { get; set; }
        /// <summary>
        /// ModifyDate
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary> 
        /// 数据库连接Url 
        /// </summary> 
        /// <returns></returns> 
        [Column("DBURL")]
        public string DbUrl { get; set; }
        /// <summary> 
        /// 数据库主机 
        /// </summary> 
        /// <returns></returns> 
        [Column("DBIP")]
        public string DbIp { get; set; }
        /// <summary> 
        /// 数据库端口 
        /// </summary> 
        /// <returns></returns> 
        [Column("DBPORT")]
        public string DbPort { get; set; }
        /// <summary> 
        /// 数据库名 
        /// </summary> 
        /// <returns></returns> 
        [Column("DBNAME")]
        public string DbName { get; set; }
        /// <summary> 
        /// 数据库用户名 
        /// </summary> 
        /// <returns></returns> 
        [Column("DBUSERNAME")]
        public string DbUsername { get; set; }
        /// <summary> 
        /// 数据库密码 
        /// </summary> 
        /// <returns></returns> 
        [Column("DBPASSWORD")]
        public string DbPassword { get; set; }
        /// <summary>
        /// 合同签约人
        /// </summary>
        [Column("CONTRACTINGPARTY")]
        public string ContractingParty { get; set; }
        /// <summary>
        /// 推送账单类型 1，自动推送  2，手动推送
        /// </summary>
        [Column("PUSHACCOUNTTYPE")]
        public int? PushAccountType { get; set; }
        /// <summary>
        /// 账户余额
        /// </summary>
        [Column("ACCOUNTBALANCE")]
        public decimal? AccountBalance { get; set; }
        /// <summary>
        /// 平台
        /// </summary>
        [Column("PLATFORM")]
        public string Platform { get; set; }
        /// <summary>
        /// 使用的系统
        /// </summary>
        [Column("USESYSTEM")]
        public string UseSystem { get; set; }
        /// <summary>
        /// 结算币种
        /// </summary>
        [Column("Currency")]
        public string Currency { get; set; }
        /// <summary>
        /// 结算币种
        /// </summary>
        [Column("PaymentDay")]
        public int? PaymentDay { get; set; }
        /// <summary>
        /// 结算方式1.人工结算2.实时自动结算3.周期自动结算
        /// </summary>
        [Column("PaymentMethod")]
        public int? PaymentMethod { get; set; }
        /// <summary>
        /// 信用额度
        /// </summary>
        [Column("CreditLimit")]
        public decimal? CreditLimit { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreatedDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            if (userInfo != null)
            {
                this.CreatedBy = userInfo.realName;
            }
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreatedDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            if (userInfo != null)
            {
                this.CreatedBy = userInfo.realName;
            }
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModifyDate=DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            if (userInfo != null)
            {
                this.ModifyBy = userInfo.realName;
            }
        }
        #endregion

        [NotMapped]
        public DateTime? ServiceStartDate { get; set; }
        [NotMapped]
        public DateTime? ServiceEndDate { get; set; }
        [NotMapped]
        public List<MeioErpWarehouseEntity> Warehouses { get; set; }
        //[NotMapped]
        //public MeioErpContractEntity contract { get; set; }
        //[NotMapped]
        //public List<MeioErpContractDetailEntity> contractDetailWarehouseRents { get; set; }
        //[NotMapped]
        //public List<MeioErpContractDetailEntity> contractDetailOperations { get; set; }
        //[NotMapped]
        //public List<MeioErpContractDetailEntity> contractDetailValueAddedServices { get; set; }
        //[NotMapped]
        //public List<MeioErpContractRecordEntity> contractRecords { get; set; }
        //[NotMapped]
        //public List<MeioErpDocumentAuditRecordEntity> documentAuditRecords { get; set; } 
        [NotMapped]
        public List<ContractDetailValueAddedServicesEntity> contractDetailValueAddedServices { get; set; }
        [NotMapped]
        public List<MeioErpOwnerThirdpartySystemEntity> thirdpartySystems { get; set; }
        [NotMapped]
        public List<MeioErpOwnerSalesPlatformEntity> salesPlatforms { get; set; }
        [NotMapped]
        public List<MeioDataCenterWarehouseEntity> CenterWarehouse { get; set; }
        [NotMapped]
        public List<MeioDataCenterChannelInformationEntity> CenterChannelInformation { get; set; }
    }
}

