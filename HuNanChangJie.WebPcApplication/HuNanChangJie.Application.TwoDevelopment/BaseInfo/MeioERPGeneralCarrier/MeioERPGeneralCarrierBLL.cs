﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-02 16:39
    /// 描 述：承运商信息
    /// </summary>
    public class MeioERPGeneralCarrierBLL : MeioERPGeneralCarrierIBLL
    {
        private MeioERPGeneralCarrierService meioERPGeneralCarrierService = new MeioERPGeneralCarrierService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioERPGeneralCarrierEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioERPGeneralCarrierService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioERPGeneralCarrierHistoricalRecord表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioERPGeneralCarrierHistoricalRecordEntity> GetMeioERPGeneralCarrierHistoricalRecordList(string keyValue)
        {
            try
            {
                return meioERPGeneralCarrierService.GetMeioERPGeneralCarrierHistoricalRecordList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioERPGeneralCarrier表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioERPGeneralCarrierEntity GetMeioERPGeneralCarrierEntity(string keyValue)
        {
            try
            {
                return meioERPGeneralCarrierService.GetMeioERPGeneralCarrierEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioERPGeneralCarrierHistoricalRecord表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioERPGeneralCarrierHistoricalRecordEntity GetMeioERPGeneralCarrierHistoricalRecordEntity(string keyValue)
        {
            try
            {
                return meioERPGeneralCarrierService.GetMeioERPGeneralCarrierHistoricalRecordEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioERPGeneralCarrierService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioERPGeneralCarrierEntity entity,string deleteList,string type)
        {
            try
            {
                meioERPGeneralCarrierService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public bool SetIsEnable(string keyValue, bool isValid)
        {
            try
            {
               return meioERPGeneralCarrierService.SetIsEnable(keyValue, isValid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsGeneralCarrierCode(string code, string id)
        {
            try
            {
                return meioERPGeneralCarrierService.CheckIsGeneralCarrierCode(code, id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
