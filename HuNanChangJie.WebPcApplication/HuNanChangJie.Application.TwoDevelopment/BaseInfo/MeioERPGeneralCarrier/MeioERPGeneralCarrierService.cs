﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
using System.Diagnostics;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-02 16:39
    /// 描 述：承运商信息
    /// </summary>
    public class MeioERPGeneralCarrierService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioERPGeneralCarrierEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Price,
                t.EffectiveTime,
                t.MonthlyAccount,
                t.InterfaceAccount,
                t.InterfaceServiceAddress,
                t.EDIServiceAddress,
                t.CreationName,
                t.CreationDate,
                t.Enabled
                ");
                strSql.Append("  FROM MeioERPGeneralCarrier t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", queryParam["Code"].ToString(), DbType.String);
                    strSql.Append(" AND t.Code=@Code ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["Enabled"].IsEmpty())
                {
                    dp.Add("Enabled", queryParam["Enabled"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.Enabled=@Enabled ");
                }
                var list = this.BaseRepository().FindList<MeioERPGeneralCarrierEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioERPGeneralCarrierHistoricalRecord表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioERPGeneralCarrierHistoricalRecordEntity> GetMeioERPGeneralCarrierHistoricalRecordList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioERPGeneralCarrierHistoricalRecordEntity>(t => t.GeneralCarrierID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioERPGeneralCarrier表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioERPGeneralCarrierEntity GetMeioERPGeneralCarrierEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioERPGeneralCarrierEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioERPGeneralCarrierHistoricalRecord表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioERPGeneralCarrierHistoricalRecordEntity GetMeioERPGeneralCarrierHistoricalRecordEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioERPGeneralCarrierHistoricalRecordEntity>(t => t.GeneralCarrierID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioERPGeneralCarrierEntity = GetMeioERPGeneralCarrierEntity(keyValue);
                db.Delete<MeioERPGeneralCarrierEntity>(t => t.ID == keyValue);
                db.Delete<MeioERPGeneralCarrierHistoricalRecordEntity>(t => t.GeneralCarrierID == meioERPGeneralCarrierEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioERPGeneralCarrierEntity entity, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    db.Update(entity);
                }
                else
                {
                    entity.Enabled = true;
                    entity.Create();
                    db.Insert(entity);
                }

                MeioERPGeneralCarrierHistoricalRecordEntity generalCarrierHistoricalRecordEntity = new MeioERPGeneralCarrierHistoricalRecordEntity
                {
                    GeneralCarrierID = entity.ID,
                    Code = entity.Code,
                    Name = entity.Name,
                    Price = entity.Price,
                    EffectiveTime = entity.EffectiveTime,
                    MonthlyAccount = entity.MonthlyAccount,
                    InterfaceAccount = entity.InterfaceAccount,
                    InterfaceKey = entity.InterfaceKey,
                    EDIServiceAddress = entity.EDIServiceAddress,
                    SheetTemplate = entity.SheetTemplate,
                };
                generalCarrierHistoricalRecordEntity.Create();
                db.Insert(generalCarrierHistoricalRecordEntity);
                db.Commit();

                MeioERPGeneralCarrierEntity carrierEntity = null;
                if (type == "edit")
                {
                    carrierEntity = this.GetMeioERPGeneralCarrierEntity(keyValue);
                    var logistics = new LogisticsRequest
                    {
                        code = entity.Code,
                        name = entity.Name,
                        status = carrierEntity.Enabled.Value ? 1 : 2,
                        appId = entity.InterfaceAccount,
                        appSecret = entity.InterfaceKey
                    };
                    OmsService.PushLogistics(logistics);
                }
                else
                {
                    var logistics = new LogisticsRequest
                    {
                        code = entity.Code,
                        name = entity.Name,
                        status = 1,
                        appId = entity.InterfaceAccount,
                        appSecret = entity.InterfaceKey
                    };
                    OmsService.PushLogistics(logistics);
                }

                WmsLogisticsRequest request = new WmsLogisticsRequest
                {
                    ID = type == "edit" ? keyValue : entity.ID,
                    Code = entity.Code,
                    Name = entity.Name,
                    Price = entity.Price,
                    EffectiveTime = entity.EffectiveTime,
                    MonthlyAccount = entity.MonthlyAccount,
                    InterfaceAccount = entity.InterfaceAccount,
                    InterfaceKey = entity.InterfaceKey,
                    InterfaceServiceAddress = entity.InterfaceServiceAddress,
                    InterfaceUserInfo = entity.InterfaceUserInfo,
                    EDIServiceAddress = entity.EDIServiceAddress,
                    SheetTemplate = entity.SheetTemplate,
                    Enabled = type == "edit" ? carrierEntity.Enabled : true
                };
                WmsService.PushLogistics(request);
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        public bool SetIsEnable(string keyValue, bool isValid)
        {
            try
            {
                var strIds = "'" + keyValue.Replace(",", "','") + "'";
                int count = this.BaseRepository().ExecuteBySql($"UPDATE dbo.MeioERPGeneralCarrier Set Enabled={(isValid ? 1 : 0)} WHERE ID IN ({strIds})");
                if (count > 0)
                {
                    var entity = this.GetMeioERPGeneralCarrierEntity(keyValue);
                    if (entity != null)
                    {
                        var logistics = new LogisticsRequest
                        {
                            code = entity.Code,
                            name = entity.Name,
                            status = isValid ? 1 : 2
                        };
                        OmsService.PushLogistics(logistics);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsGeneralCarrierCode(string code, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioERPGeneralCarrierEntity>(t => t.Code == code && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
