﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-02 16:39
    /// 描 述：承运商信息
    /// </summary>
    public interface MeioERPGeneralCarrierIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioERPGeneralCarrierEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioERPGeneralCarrierHistoricalRecord表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioERPGeneralCarrierHistoricalRecordEntity> GetMeioERPGeneralCarrierHistoricalRecordList(string keyValue);
        /// <summary>
        /// 获取MeioERPGeneralCarrier表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioERPGeneralCarrierEntity GetMeioERPGeneralCarrierEntity(string keyValue);
        /// <summary>
        /// 获取MeioERPGeneralCarrierHistoricalRecord表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioERPGeneralCarrierHistoricalRecordEntity GetMeioERPGeneralCarrierHistoricalRecordEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioERPGeneralCarrierEntity entity,string deleteList,string type);
        bool SetIsEnable(string keyValue, bool isValid);
        bool CheckIsGeneralCarrierCode(string code, string id);
        #endregion

    }
}
