﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 18:11
    /// 描 述：仓库租金档案信息
    /// </summary>
    public class MeioErpWarehouseRentEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 仓库租金档案名称
        /// </summary>
        [Column("ITEMNAME")]
        public string ItemName { get; set; }
        /// <summary>
        /// 是否有效
        /// </summary>
        [Column("ISVALID")]
        public bool? IsValid { get; set; }
        /// <summary>
        /// 报价单类型：1（小包）2(大货)
        /// </summary>
        [Column("TYPE")]
        public int? Type { get; set; }
        /// <summary>
        /// Unit
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        [Column("CREATEDBY")]
        public string CreatedBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        [Column("MODIFYBY")]
        public string ModifyBy { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreatedDate=DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = LoginUserInfo.UserInfo.realName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModifyDate=DateTime.Now;
            this.ModifyBy = LoginUserInfo.UserInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

