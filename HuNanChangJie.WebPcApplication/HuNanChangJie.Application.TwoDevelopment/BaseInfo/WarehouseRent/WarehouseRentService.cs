﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 18:11
    /// 描 述：仓库租金档案信息
    /// </summary>
    public class WarehouseRentService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseRentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID,
                t.ItemName,
                t.Type,
                t.Unit,
                t.IsValid,
                t.Remark
                ");
                strSql.Append("  FROM MeioErpWarehouseRent t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ItemName"].IsEmpty())
                {
                    dp.Add("ItemName", "%" + queryParam["ItemName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ItemName Like @ItemName ");
                }
                if (!queryParam["IsValid"].IsEmpty())
                {
                    dp.Add("IsValid", queryParam["IsValid"].ToString(), DbType.Boolean);
                    strSql.Append(" AND t.IsValid=@IsValid");
                }
                if (!queryParam["Type"].IsEmpty())
                {
                    dp.Add("Type", queryParam["Type"].ToString(), DbType.String);
                    strSql.Append(" AND t.Type = @Type ");
                }
                strSql.Append(" ORDER BY t.CreatedDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpWarehouseRentEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreatedDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public IEnumerable<MeioErpWarehouseRentEntity> GetListByIds(string ids)
        {
            try
            {
                var strIds = ids.Split(',');
                var list = this.BaseRepository().FindList<MeioErpWarehouseRentEntity>(p => strIds.Contains(p.ID));
                var query = from item in list orderby item.CreatedDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseRentDetailEntity> GetMeioErpWarehouseRentDetailList(string keyValue)
        {
            try
            {
                IEnumerable<MeioErpWarehouseRentDetailEntity> list = null;
                var strIds = keyValue.Split(',');
                if (strIds.Length > 1)
                    list = this.BaseRepository().FindList<MeioErpWarehouseRentDetailEntity>(t => strIds.Contains(t.MeioErpWarehouseRentID));
                else
                    list = this.BaseRepository().FindList<MeioErpWarehouseRentDetailEntity>(t => t.MeioErpWarehouseRentID == keyValue);
                //var query = from item in list orderby item.CreatedDate ascending select item;
                var query = from item in list orderby item.MinValue ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public IEnumerable<MeioErpWarehouseRentDetailEntity> GetMeioErpWarehouseRentDetailListByWarehouseRentIDs(string ids)
        {
            try
            {
                List<string> idList = ids.Split(',').ToList<string>();
                var list = this.BaseRepository().FindList<MeioErpWarehouseRentDetailEntity>(t => idList.Contains(t.MeioErpWarehouseRentID));
                var query = from item in list orderby item.CreatedDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRent表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseRentEntity GetMeioErpWarehouseRentEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWarehouseRentEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseRentDetailEntity GetMeioErpWarehouseRentDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWarehouseRentDetailEntity>(t => t.MeioErpWarehouseRentID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpWarehouseRentEntity = GetMeioErpWarehouseRentEntity(keyValue);
                db.Delete<MeioErpWarehouseRentEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpWarehouseRentDetailEntity>(t => t.MeioErpWarehouseRentID == meioErpWarehouseRentEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWarehouseRentEntity entity, List<MeioErpWarehouseRentDetailEntity> meioErpWarehouseRentDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var meioErpWarehouseRentEntityTmp = GetMeioErpWarehouseRentEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);

                    db.Delete<MeioErpWarehouseRentDetailEntity>(p => p.MeioErpWarehouseRentID == keyValue);
                    foreach (var item in meioErpWarehouseRentDetailList)
                    {
                        item.Create();
                        item.MeioErpWarehouseRentID = keyValue;
                        db.Insert(item);
                    }
                    //var delJson = deleteList.ToObject<List<JObject>>();
                    //foreach (var del in delJson)
                    //{
                    //    var idList = del["idList"].ToString();
                    //    var sql = "";
                    //    if (idList.Contains(","))
                    //    {
                    //        var ids = idList.Split(',');
                    //        foreach (var id in ids)
                    //        {
                    //            sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{id}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (!string.IsNullOrWhiteSpace(idList))
                    //        {
                    //            sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{idList}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //}

                    ////没有生成代码 
                    //var MeioErpWarehouseRentDetailUpdateList = meioErpWarehouseRentDetailList.FindAll(i => i.EditType == EditType.Update);
                    //foreach (var item in MeioErpWarehouseRentDetailUpdateList)
                    //{
                    //    db.Update(item);
                    //}
                    //var MeioErpWarehouseRentDetailInserList = meioErpWarehouseRentDetailList.FindAll(i => i.EditType == EditType.Add);
                    //foreach (var item in MeioErpWarehouseRentDetailInserList)
                    //{
                    //    item.Create(item.ID);
                    //    item.MeioErpWarehouseRentID = meioErpWarehouseRentEntityTmp.ID;
                    //    db.Insert(item);
                    //}

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (MeioErpWarehouseRentDetailEntity item in meioErpWarehouseRentDetailList)
                    {
                        item.MeioErpWarehouseRentID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        internal bool CheckIsItemName(string name, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpWarehouseRentEntity>(t => t.ItemName.Trim().ToLower() == name.Trim().ToLower() && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
