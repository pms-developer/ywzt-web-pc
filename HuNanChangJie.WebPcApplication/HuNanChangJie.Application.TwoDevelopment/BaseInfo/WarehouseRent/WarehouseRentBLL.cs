﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 18:11
    /// 描 述：仓库租金档案信息
    /// </summary>
    public class WarehouseRentBLL : WarehouseRentIBLL
    {
        private WarehouseRentService warehouseRentService = new WarehouseRentService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseRentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return warehouseRentService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<MeioErpWarehouseRentEntity> GetListByIds(string ids)
        {
            try
            {
                return warehouseRentService.GetListByIds(ids);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWarehouseRentDetailEntity> GetMeioErpWarehouseRentDetailList(string keyValue)
        {
            try
            {
                return warehouseRentService.GetMeioErpWarehouseRentDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public IEnumerable<MeioErpWarehouseRentDetailEntity> GetMeioErpWarehouseRentDetailListByWarehouseRentIDs(string ids)
        {
            try
            {
                return warehouseRentService.GetMeioErpWarehouseRentDetailListByWarehouseRentIDs(ids);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRent表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseRentEntity GetMeioErpWarehouseRentEntity(string keyValue)
        {
            try
            {
                return warehouseRentService.GetMeioErpWarehouseRentEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWarehouseRentDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWarehouseRentDetailEntity GetMeioErpWarehouseRentDetailEntity(string keyValue)
        {
            try
            {
                return warehouseRentService.GetMeioErpWarehouseRentDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                warehouseRentService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWarehouseRentEntity entity,List<MeioErpWarehouseRentDetailEntity> meioErpWarehouseRentDetailList,string deleteList,string type)
        {
            try
            {
                warehouseRentService.SaveEntity(keyValue, entity,meioErpWarehouseRentDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public bool CheckIsItemName(string name, string id)
        {
            return warehouseRentService.CheckIsItemName(name,id);
        }
    }
}
