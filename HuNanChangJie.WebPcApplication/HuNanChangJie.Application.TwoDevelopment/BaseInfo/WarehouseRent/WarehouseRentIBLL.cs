﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-05-09 18:11
    /// 描 述：仓库租金档案信息
    /// </summary>
    public interface WarehouseRentIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpWarehouseRentEntity> GetPageList(XqPagination pagination, string queryJson);
        IEnumerable<MeioErpWarehouseRentEntity> GetListByIds(string ids);
        /// <summary>
        /// 获取MeioErpWarehouseRentDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpWarehouseRentDetailEntity> GetMeioErpWarehouseRentDetailList(string keyValue);
        IEnumerable<MeioErpWarehouseRentDetailEntity> GetMeioErpWarehouseRentDetailListByWarehouseRentIDs(string ids);
        /// <summary>
        /// 获取MeioErpWarehouseRent表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWarehouseRentEntity GetMeioErpWarehouseRentEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpWarehouseRentDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWarehouseRentDetailEntity GetMeioErpWarehouseRentDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpWarehouseRentEntity entity,List<MeioErpWarehouseRentDetailEntity> meioErpWarehouseRentDetailList,string deleteList,string type);
        #endregion
        bool CheckIsItemName(string name, string id);
    }
}
