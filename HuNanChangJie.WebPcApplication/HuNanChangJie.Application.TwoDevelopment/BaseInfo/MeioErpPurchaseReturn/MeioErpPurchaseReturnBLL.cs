﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-05 15:34
    /// 描 述：采购退货
    /// </summary>
    public class MeioErpPurchaseReturnBLL : MeioErpPurchaseReturnIBLL
    {
        private MeioErpPurchaseReturnService meioErpPurchaseReturnService = new MeioErpPurchaseReturnService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseReturnEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpPurchaseReturnService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseReturnDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseReturnDetailEntity> GetMeioErpPurchaseReturnDetailList(string keyValue)
        {
            try
            {
                return meioErpPurchaseReturnService.GetMeioErpPurchaseReturnDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseReturn表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseReturnEntity GetMeioErpPurchaseReturnEntity(string keyValue)
        {
            try
            {
                return meioErpPurchaseReturnService.GetMeioErpPurchaseReturnEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseReturnDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseReturnDetailEntity GetMeioErpPurchaseReturnDetailEntity(string keyValue)
        {
            try
            {
                return meioErpPurchaseReturnService.GetMeioErpPurchaseReturnDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpPurchaseReturnService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpPurchaseReturnEntity entity,List<MeioErpPurchaseReturnDetailEntity> meioErpPurchaseReturnDetailList,string deleteList,string type)
        {
            try
            {
                meioErpPurchaseReturnService.SaveEntity(keyValue, entity,meioErpPurchaseReturnDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
