﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-05 15:34
    /// 描 述：采购退货
    /// </summary>
    public class MeioErpPurchaseReturnEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 原采购单ID
        /// </summary>
        [Column("PURCHASEID")]
        public string PurchaseID { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column("STATE")]
        public string State { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATE")]
        public string AuditState { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        [Column("AUDITOR")]
        public string Auditor { get; set; }
        /// <summary>
        /// 审核时间
        /// </summary>
        [Column("AUDITTIME")]
        public DateTime? AuditTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 退货单号
        /// </summary>
        [Column("RETURNCODE")]
        public string ReturnCode { get; set; }
        /// <summary>
        /// 退货地址
        /// </summary>
        [Column("RETURNADDRESS")]
        public string ReturnAddress { get; set; }
        /// <summary>
        /// PurchaseCode
        /// </summary>
        [Column("PURCHASECODE")]
        public string PurchaseCode { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        [Column("WAREHOUSEID")]
        public string WarehouseID { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        [Column("SUPPLIERID")]
        public string SupplierID { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        [Column("CONTACTPERSON")]
        public string ContactPerson { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("CONTACTPHONE")]
        public string ContactPhone { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            if (userInfo != null)
            {
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
                this.Company_ID = userInfo.companyId;
                this.CompanyCode = userInfo.CompanyCode;
                this.CompanyName = userInfo.CompanyName;
                this.CompanyFullName = userInfo.CompanyFullName;
            }
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            if (userInfo != null)
            {
                this.Creation_Id = userInfo.userId;
                this.CreationName = userInfo.realName;
                this.Company_ID = userInfo.companyId;
                this.CompanyCode = userInfo.CompanyCode;
                this.CompanyName = userInfo.CompanyName;
                this.CompanyFullName = userInfo.CompanyFullName;
            }
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            if (userInfo != null)
            {
                this.Modification_Id = userInfo.userId;
                this.ModificationName = userInfo.realName;
            }
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

