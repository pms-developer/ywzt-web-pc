﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-05 15:34
    /// 描 述：采购退货
    /// </summary>
    public class MeioErpPurchaseReturnService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseReturnEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.ReturnCode,
                t.PurchaseCode,
                t.WarehouseID,
                t.SupplierID,
                t.Remark,
                t.CreationDate
                ");
                strSql.Append("  FROM MeioErpPurchaseReturn t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ReturnCode"].IsEmpty())
                {
                    dp.Add("ReturnCode", "%" + queryParam["ReturnCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ReturnCode Like @ReturnCode ");
                }
                if (!queryParam["PurchaseCode"].IsEmpty())
                {
                    dp.Add("PurchaseCode", "%" + queryParam["PurchaseCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.PurchaseCode Like @PurchaseCode ");
                }
                if (!queryParam["WarehouseID"].IsEmpty())
                {
                    dp.Add("WarehouseID",queryParam["WarehouseID"].ToString(), DbType.String);
                    strSql.Append(" AND t.WarehouseID = @WarehouseID ");
                }
                var list=this.BaseRepository().FindList<MeioErpPurchaseReturnEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseReturnDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpPurchaseReturnDetailEntity> GetMeioErpPurchaseReturnDetailList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<MeioErpPurchaseReturnDetailEntity>(t=>t.PurchaseReturnID == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseReturn表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseReturnEntity GetMeioErpPurchaseReturnEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpPurchaseReturnEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpPurchaseReturnDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpPurchaseReturnDetailEntity GetMeioErpPurchaseReturnDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpPurchaseReturnDetailEntity>(t=>t.PurchaseReturnID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpPurchaseReturnEntity = GetMeioErpPurchaseReturnEntity(keyValue); 
                db.Delete<MeioErpPurchaseReturnEntity>(t=>t.ID == keyValue);
                db.Delete<MeioErpPurchaseReturnDetailEntity>(t=>t.PurchaseReturnID == meioErpPurchaseReturnEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpPurchaseReturnEntity entity,List<MeioErpPurchaseReturnDetailEntity> meioErpPurchaseReturnDetailList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var meioErpPurchaseReturnEntityTmp = GetMeioErpPurchaseReturnEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var MeioErpPurchaseReturnDetailUpdateList= meioErpPurchaseReturnDetailList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in MeioErpPurchaseReturnDetailUpdateList)
                    {
                        db.Update(item);
                    }
                    var MeioErpPurchaseReturnDetailInserList= meioErpPurchaseReturnDetailList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in MeioErpPurchaseReturnDetailInserList)
                    {
                        item.Create(item.ID);
                        item.PurchaseReturnID = meioErpPurchaseReturnEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (MeioErpPurchaseReturnDetailEntity item in meioErpPurchaseReturnDetailList)
                    {
                        item.PurchaseReturnID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
