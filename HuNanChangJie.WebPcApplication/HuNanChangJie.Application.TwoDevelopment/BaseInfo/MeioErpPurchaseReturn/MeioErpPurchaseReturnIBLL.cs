﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-09-05 15:34
    /// 描 述：采购退货
    /// </summary>
    public interface MeioErpPurchaseReturnIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpPurchaseReturnEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpPurchaseReturnDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpPurchaseReturnDetailEntity> GetMeioErpPurchaseReturnDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpPurchaseReturn表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpPurchaseReturnEntity GetMeioErpPurchaseReturnEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpPurchaseReturnDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpPurchaseReturnDetailEntity GetMeioErpPurchaseReturnDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpPurchaseReturnEntity entity,List<MeioErpPurchaseReturnDetailEntity> meioErpPurchaseReturnDetailList,string deleteList,string type);
        #endregion

    }
}
