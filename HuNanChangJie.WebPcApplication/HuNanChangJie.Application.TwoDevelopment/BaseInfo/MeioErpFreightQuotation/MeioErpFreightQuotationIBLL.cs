﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using System;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-08 16:38
    /// 描 述：运费报价
    /// </summary>
    public interface MeioErpFreightQuotationIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpFreightQuotationEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpFreightQuotationDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpFreightQuotationDetailEntity> GetMeioErpFreightQuotationDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpFreightQuotation表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpFreightQuotationEntity GetMeioErpFreightQuotationEntity(string keyValue);
        /// <summary>
        /// 获取MeioErpFreightQuotationDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpFreightQuotationDetailEntity GetMeioErpFreightQuotationDetailEntity(string keyValue);
        IEnumerable<MeioErpFreightQuotationEntity> GetMeioErpFreightQuotationDetailListById(string keyValue);
        IEnumerable<MeioErpDeleteLogEntity> GetMeioErpDeleteFreightQuotationList(string ruleName);
        #endregion

        #region  提交数据


        void DeleteEntity(string keyValue, string deleteList);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpFreightQuotationEntity entity,List<MeioErpFreightQuotationDetailEntity> meioErpFreightQuotationDetailList,string deleteList,string type);
        #endregion
        bool SetIsEnable(string keyValue, bool isValid);
        bool CheckIsRuleName(string name, string id);
        void UpdateEffectiveTime(string keyValue, DateTime EffectiveStartTime, DateTime EffectiveEndTime,List<MeioErpFreightQuotationEntity> list);
    }
}
