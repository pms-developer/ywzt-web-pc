﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.WebRequestService;
using Newtonsoft.Json;
namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-08 16:38
    /// 描 述：运费报价
    /// </summary>
    public class MeioErpFreightQuotationService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();

        private MyInterceptor myInterceptor = new MyInterceptor();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpFreightQuotationEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000");
                strSql.Append(@"
                t.ID,
                t.RuleName,
                t.Enabled,
                t.EffectiveStartTime,
                t.EffectiveEndTime,
                t.GeneralCarrier,
                t.LogisticsChannel,
                t.BillingMode,
                t.Country,
                t.ModificationName,
                t.ModificationDate
                ");
                strSql.Append("  FROM MeioErpFreightQuotation t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.ModificationDate >= @startTime AND t.ModificationDate <= @endTime ) ");
                }
                if (!queryParam["Enabled"].IsEmpty())
                {
                    dp.Add("Enabled", queryParam["Enabled"].ToString(), DbType.String);
                    strSql.Append(" AND t.Enabled = @Enabled ");
                }
                if (!queryParam["RuleName"].IsEmpty())
                {
                    dp.Add("RuleName", "%" + queryParam["RuleName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.RuleName Like @RuleName ");
                }
                if (!queryParam["Country"].IsEmpty())
                {
                    dp.Add("Country", "%" + queryParam["Country"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Country Like @Country ");
                }
                //strSql.Append(" group by t.ID,t.RuleName,t.Enabled,t.EffectiveStartTime,t.EffectiveEndTime,t.GeneralCarrier,t.LogisticsChannel,t.BillingMode,t.ModificationName,t.ModificationDate,t.Country");
                var list = this.BaseRepository().FindList<MeioErpFreightQuotationEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpFreightQuotationDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpFreightQuotationDetailEntity> GetMeioErpFreightQuotationDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpFreightQuotationDetailEntity>(t => t.FreightQuotationID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpFreightQuotation表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpFreightQuotationEntity GetMeioErpFreightQuotationEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpFreightQuotationEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpFreightQuotationDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpFreightQuotationDetailEntity GetMeioErpFreightQuotationDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpFreightQuotationDetailEntity>(t => t.FreightQuotationID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpFreightQuotationEntity> GetMeioErpFreightQuotationDetailListById(string keyValue)
        {
            try
            {
                var Ids = keyValue.Split(',');

                return this.BaseRepository().FindList<MeioErpFreightQuotationEntity>(t => Ids.Contains(t.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpDeleteLogEntity> GetMeioErpDeleteFreightQuotationList(string ruleName)
        {
            try
            {
                if (string.IsNullOrEmpty(ruleName))
                {
                    return this.BaseRepository().FindList<MeioErpDeleteLogEntity>(t => t.Model == "MeioErpFreightQuotation");
                }
                else
                {
                    return this.BaseRepository().FindList<MeioErpDeleteLogEntity>(t => t.Model == "MeioErpFreightQuotation" && t.Code==ruleName);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var strIds = keyValue.Split(',');
                foreach (var strId in strIds)
                {
                    var meioErpFreightQuotationEntity = GetMeioErpFreightQuotationEntity(strId);
                    db.Delete<MeioErpFreightQuotationEntity>(t => t.ID == strId);
                    db.Delete<MeioErpFreightQuotationDetailEntity>(t => t.FreightQuotationID == meioErpFreightQuotationEntity.ID);
                    var deletelog = new MeioErpDeleteLogEntity
                    {
                        DeleteID = strId,
                        Model = "MeioErpFreightQuotation",
                        Code = meioErpFreightQuotationEntity.RuleName
                    };
                    deletelog.Create();
                    db.Insert(deletelog);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpFreightQuotationEntity entity, List<MeioErpFreightQuotationDetailEntity> meioErpFreightQuotationDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                List<LogUpdateInfo> list = new List<LogUpdateInfo>();
                if (meioErpFreightQuotationDetailList != null)
                {
                    if (meioErpFreightQuotationDetailList.Count > 0)
                    {
                        entity.Country = string.Join(",", meioErpFreightQuotationDetailList.Select(o => o.Country + (string.IsNullOrEmpty(o.Area) ? "" : "-" + o.Area)).Distinct());
                    }
                }

                if (type == "edit")
                {
                    var meioErpFreightQuotationEntityTmp = GetMeioErpFreightQuotationEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);

                    db.Delete<MeioErpFreightQuotationDetailEntity>(p => p.FreightQuotationID == keyValue);
                }
                else
                {
                    entity.Create(keyValue);
                    entity.Enabled = true;
                    db.Insert(entity);
                }
                foreach (MeioErpFreightQuotationDetailEntity item in meioErpFreightQuotationDetailList)
                {
                    item.Create();
                    item.FreightQuotationID = entity.ID;
                    db.Insert(item);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
        public bool SetIsEnable(string keyValue, bool isValid)
        {
            try
            {
                var strIds = "'" + keyValue.Replace(",", "','") + "'";
                UserInfo userInfo = LoginUserInfo.Get();
                int count = this.BaseRepository().ExecuteBySql($"UPDATE dbo.MeioErpFreightQuotation Set Enabled={(isValid ? 1 : 0)},ModificationDate=GETDATE(),Modification_Id='{userInfo.userId}',ModificationName='{userInfo.realName}' WHERE ID IN ({strIds})");
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsRuleName(string name, string id)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioErpFreightQuotationEntity>(t => t.RuleName == name && t.ID != id);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UpdateEffectiveTime(string keyValue, DateTime EffectiveStartTime, DateTime EffectiveEndTime)
        {
            try
            {
                var strIds = "'" + keyValue.Replace(",", "','") + "'";
                UserInfo userInfo = LoginUserInfo.Get();
                this.BaseRepository().ExecuteBySql($"UPDATE dbo.MeioErpFreightQuotation Set EffectiveStartTime='{EffectiveStartTime}',EffectiveEndTime='{EffectiveEndTime}',ModificationDate=GETDATE(),Modification_Id='{userInfo.userId}',ModificationName='{userInfo.realName}' WHERE ID IN ({strIds})");

                //var entity= this.BaseRepository().FindEntity<MeioErpFreightQuotationEntity>(t => t.ID == keyValue);
                //entity.EffectiveStartTime= EffectiveStartTime;
                //entity.EffectiveEndTime= EffectiveEndTime;
                //entity.Modify(keyValue);
                //this.BaseRepository().Update(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
