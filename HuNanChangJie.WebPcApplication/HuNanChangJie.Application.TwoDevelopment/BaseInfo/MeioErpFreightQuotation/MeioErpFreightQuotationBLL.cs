﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Threading;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2025-02-08 16:38
    /// 描 述：运费报价
    /// </summary>
    public class MeioErpFreightQuotationBLL : MeioErpFreightQuotationIBLL
    {
        private MeioErpFreightQuotationService meioErpFreightQuotationService = new MeioErpFreightQuotationService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpFreightQuotationEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpFreightQuotationService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpFreightQuotationDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpFreightQuotationDetailEntity> GetMeioErpFreightQuotationDetailList(string keyValue)
        {
            try
            {
                return meioErpFreightQuotationService.GetMeioErpFreightQuotationDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpFreightQuotation表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpFreightQuotationEntity GetMeioErpFreightQuotationEntity(string keyValue)
        {
            try
            {
                return meioErpFreightQuotationService.GetMeioErpFreightQuotationEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpFreightQuotationDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpFreightQuotationDetailEntity GetMeioErpFreightQuotationDetailEntity(string keyValue)
        {
            try
            {
                return meioErpFreightQuotationService.GetMeioErpFreightQuotationDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<MeioErpFreightQuotationEntity> GetMeioErpFreightQuotationDetailListById(string keyValue)
        {
            try
            {
                return meioErpFreightQuotationService.GetMeioErpFreightQuotationDetailListById(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<MeioErpDeleteLogEntity> GetMeioErpDeleteFreightQuotationList(string ruleName)
        {
            try
            {
                return meioErpFreightQuotationService.GetMeioErpDeleteFreightQuotationList(ruleName);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue,string deleteList)
        {
            try
            {
                meioErpFreightQuotationService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpFreightQuotationEntity entity,List<MeioErpFreightQuotationDetailEntity> meioErpFreightQuotationDetailList,string deleteList,string type)
        {
            try
            {
                //Thread.Sleep(3000);//延迟3秒
                meioErpFreightQuotationService.SaveEntity(keyValue, entity,meioErpFreightQuotationDetailList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
        public bool SetIsEnable(string keyValue, bool isValid)
        {
            try
            {
                return meioErpFreightQuotationService.SetIsEnable(keyValue, isValid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsRuleName(string name, string id)
        {
            try
            {
                return meioErpFreightQuotationService.CheckIsRuleName(name, id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UpdateEffectiveTime(string keyValue, DateTime EffectiveStartTime, DateTime EffectiveEndTime, List<MeioErpFreightQuotationEntity> list)
        {
            try
            {
                meioErpFreightQuotationService.UpdateEffectiveTime(keyValue, EffectiveStartTime, EffectiveEndTime);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
