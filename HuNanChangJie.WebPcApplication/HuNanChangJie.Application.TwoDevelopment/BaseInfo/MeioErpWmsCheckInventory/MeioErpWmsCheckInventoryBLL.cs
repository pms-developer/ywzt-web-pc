﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    public class MeioErpWmsCheckInventoryBLL : MeioErpWmsCheckInventoryIBLL
    {
        private MeioErpWmsCheckInventoryService meioErpWmsCheckInventoryService = new MeioErpWmsCheckInventoryService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsCheckInventoryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioErpWmsCheckInventoryService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsCheckInventoryDetail表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsCheckInventoryDetailEntity> GetMeioErpWmsCheckInventoryDetailList(string keyValue)
        {
            try
            {
                return meioErpWmsCheckInventoryService.GetMeioErpWmsCheckInventoryDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsCheckInventory表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsCheckInventoryEntity GetMeioErpWmsCheckInventoryEntity(string keyValue)
        {
            try
            {
                return meioErpWmsCheckInventoryService.GetMeioErpWmsCheckInventoryEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 根据盘点单号，获取MeioErpWmsCheckInventory表实体数据
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MeioErpWmsCheckInventoryEntity GetMeioErpWmsCheckInventoryEntityByCode(string code)
        {
            try
            {
                return meioErpWmsCheckInventoryService.GetMeioErpWmsCheckInventoryEntityByCode(code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsCheckInventoryDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsCheckInventoryDetailEntity GetMeioErpWmsCheckInventoryDetailEntity(string keyValue)
        {
            try
            {
                return meioErpWmsCheckInventoryService.GetMeioErpWmsCheckInventoryDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioErpWmsCheckInventoryService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsCheckInventoryEntity entity, List<MeioErpWmsCheckInventoryDetailEntity> meioErpWmsCheckInventoryDetailList, string deleteList, string type)
        {
            try
            {
                meioErpWmsCheckInventoryService.SaveEntity(keyValue, entity, meioErpWmsCheckInventoryDetailList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
    }
}
