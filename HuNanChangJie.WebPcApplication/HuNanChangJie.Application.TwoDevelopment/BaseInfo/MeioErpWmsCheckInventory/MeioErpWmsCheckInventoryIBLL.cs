﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS盘点信息
    /// </summary>
    public interface MeioErpWmsCheckInventoryIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioErpWmsCheckInventoryEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MeioErpWmsCheckInventoryDetail表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MeioErpWmsCheckInventoryDetailEntity> GetMeioErpWmsCheckInventoryDetailList(string keyValue);
        /// <summary>
        /// 获取MeioErpWmsCheckInventory表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsCheckInventoryEntity GetMeioErpWmsCheckInventoryEntity(string keyValue);
        MeioErpWmsCheckInventoryEntity GetMeioErpWmsCheckInventoryEntityByCode(string code);
        /// <summary>
        /// 获取MeioErpWmsCheckInventoryDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioErpWmsCheckInventoryDetailEntity GetMeioErpWmsCheckInventoryDetailEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioErpWmsCheckInventoryEntity entity, List<MeioErpWmsCheckInventoryDetailEntity> meioErpWmsCheckInventoryDetailList, string deleteList, string type);
        #endregion
    }
}
