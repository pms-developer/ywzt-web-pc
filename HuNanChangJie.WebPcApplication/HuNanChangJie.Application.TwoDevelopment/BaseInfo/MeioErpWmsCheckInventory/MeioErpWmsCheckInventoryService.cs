﻿using Dapper;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using HuNanChangJie.WebRequestService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.BaseInfo
{
    /// <summary>
    /// WMS盘点信息
    /// </summary>
    public class MeioErpWmsCheckInventoryService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private static readonly object objLock = new object();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsCheckInventoryEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT TOP 100000 ");
                strSql.Append(@"
                t.ID
                ,t.WarehouseID
                ,t.OwnerID
                ,t.Code
                ,t.Type
                ,t.Remark
                ,t.CreationDate
                ");
                strSql.Append("  FROM MeioErpWmsCheckInventory t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                strSql.Append(" ORDER BY t.CreationDate DESC ");
                var list = this.BaseRepository().FindList<MeioErpWmsCheckInventoryEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsCheckInventoryEntity表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<MeioErpWmsCheckInventoryDetailEntity> GetMeioErpWmsCheckInventoryDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<MeioErpWmsCheckInventoryDetailEntity>(t => t.WmsCheckInventoryID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsCheckInventory表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsCheckInventoryEntity GetMeioErpWmsCheckInventoryEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsCheckInventoryEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 根据盘点单号获取盘点信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MeioErpWmsCheckInventoryEntity GetMeioErpWmsCheckInventoryEntityByCode(string code)
        {
            try
            {
                return this.BaseRepository().FindList<MeioErpWmsCheckInventoryEntity>(p => p.Code == code).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MeioErpWmsCheckInventoryDetailEntity表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioErpWmsCheckInventoryDetailEntity GetMeioErpWmsCheckInventoryDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioErpWmsCheckInventoryDetailEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var meioErpOutEntity = GetMeioErpWmsCheckInventoryEntity(keyValue);
                db.Delete<MeioErpWmsCheckInventoryEntity>(t => t.ID == keyValue);
                db.Delete<MeioErpWmsCheckInventoryDetailEntity>(t => t.WmsCheckInventoryID == meioErpOutEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioErpWmsCheckInventoryEntity entity, List<MeioErpWmsCheckInventoryDetailEntity> meioErpWmsCheckInventoryDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    db.Update(entity);
                    this.BaseRepository().Delete<MeioErpWmsCheckInventoryDetailEntity>(p => p.WmsCheckInventoryID == keyValue);
                    foreach (MeioErpWmsCheckInventoryDetailEntity item in meioErpWmsCheckInventoryDetailList)
                    {
                        item.Create();
                        item.WmsCheckInventoryID = keyValue;
                        db.Insert(item);
                    }
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                    foreach (MeioErpWmsCheckInventoryDetailEntity item in meioErpWmsCheckInventoryDetailList)
                    {
                        item.Create();
                        item.WmsCheckInventoryID = entity.ID;
                        db.Insert(item);
                    }
                }
                #region 计算盘点账单
                //查询货主合同
                var owenerEntity = this.BaseRepository().FindEntity<MeioErpOwnerEntity>(p => p.ID == entity.OwnerID);
                if (owenerEntity != null)
                {
                    var nowDate = DateTime.Now.Date;
                    var contractEntity = this.BaseRepository().FindEntity<MeioErpContractEntity>(p => p.ID == owenerEntity.ContractID && p.ServiceStartDate <= nowDate && p.ServiceEndDate >= nowDate);
                    if (contractEntity != null)
                    {
                        var contractDetailList = this.BaseRepository().FindList<MeioErpContractDetailEntity>(p => p.ContractID == owenerEntity.ContractID);
                        var contractDetailEntity = contractDetailList.Where(p => p.ItemName=="临时盘点").FirstOrDefault();
                        decimal checkInventoryPrice = 0;
                        if (contractDetailEntity != null)
                        {
                            checkInventoryPrice = contractDetailEntity.ContractPrice.Value;
                        }
                        #region 计算盘点账单
                        MeioErpCheckInventoryAccountEntity meioErpCheckInventoryAccountEntity = new MeioErpCheckInventoryAccountEntity
                        {
                            WarehouseID = entity.WarehouseID,
                            OwnerID = entity.OwnerID,
                            State = 0,
                            CheckInventoryCode = entity.Code,
                            AccountDate = DateTime.Now,
                            CheckInventoryNum = meioErpWmsCheckInventoryDetailList.Sum(s => s.RealityNum),
                            OperatingCost = meioErpWmsCheckInventoryDetailList.Sum(s => s.RealityNum) * checkInventoryPrice,
                            IsPushOMS = false
                        };
                        meioErpCheckInventoryAccountEntity.Create();

                        var meioErpWmsCheckInventoryDetailListQuery = meioErpWmsCheckInventoryDetailList
                            .GroupBy(g => new { g.GoodsCode, g.GoodsName }).Select(o => new { o.Key.GoodsCode, o.Key.GoodsName, RealityNum=o.Sum(s=>s.RealityNum) });
                        foreach (var checkInventoryDetail in meioErpWmsCheckInventoryDetailListQuery)
                        {
                            MeioErpCheckInventoryAccountDetailEntity checkInventoryAccountDetailEntity = new MeioErpCheckInventoryAccountDetailEntity
                            {
                                CheckInventoryAccountID = meioErpCheckInventoryAccountEntity.ID,
                                GoodsCode = checkInventoryDetail.GoodsCode,
                                GoodsName = checkInventoryDetail.GoodsName,
                                CheckInventoryNum = checkInventoryDetail.RealityNum,
                            };
                            checkInventoryAccountDetailEntity.Create();
                            db.Insert(checkInventoryAccountDetailEntity);
                        }

                        db.Insert(meioErpCheckInventoryAccountEntity);
                        #endregion
                    }
                }
                #endregion

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }

        }

        #endregion
    }
}
