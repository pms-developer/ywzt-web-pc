﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-19 09:31
    /// 描 述：材料品牌
    /// </summary>
    public class MeioMaterialsBrandBLL : MeioMaterialsBrandIBLL
    {
        private MeioMaterialsBrandService baseMaterialsBrandService = new MeioMaterialsBrandService();

        #region  获取数据

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<MeioMaterialsBrandEntity> GetSelectList(string projectId)
        {
            try
            {
                return baseMaterialsBrandService.GetSelectList(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioMaterialsBrandEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return baseMaterialsBrandService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_MaterialsBrand表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioMaterialsBrandEntity GetBase_MaterialsBrandEntity(string keyValue)
        {
            try
            {
                return baseMaterialsBrandService.GetBase_MaterialsBrandEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                baseMaterialsBrandService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioMaterialsBrandEntity entity,string deleteList,string type)
        {
            try
            {
                baseMaterialsBrandService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckRename(string name, string type = "cn")
        {
            try
            {
                return baseMaterialsBrandService.CheckRename(name,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
