﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-18 14:43
    /// 描 述：材料分类
    /// </summary>
    public interface MeioMaterialsTypeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioMaterialsTypeEntity> GetPageList(string queryJson);
        /// <summary>
        /// 获取Base_MaterialsType表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioMaterialsTypeEntity GetBase_MaterialsTypeEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioMaterialsTypeEntity entity,string deleteList,string type);

        bool CheckRename(string value, string type);
        List<TreeModel> GetTreeList(string projectId,bool showProject);
        #endregion

    }
}
