﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Collections;
using HuNanChangJie.Application.TwoDevelopment.MeioWmsSetting;

namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-18 14:42
    /// 描 述：材料分类
    /// </summary>
    public class MeioMaterialsTypeBLL : MeioMaterialsTypeIBLL
    {
        private MeioMaterialsTypeService baseMaterialsTypeService = new MeioMaterialsTypeService();

        private MeioWmsShipperService customerService = new MeioWmsShipperService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioMaterialsTypeEntity> GetPageList(string queryJson)
        {
            try
            {
                return baseMaterialsTypeService.GetPageList(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_MaterialsType表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioMaterialsTypeEntity GetBase_MaterialsTypeEntity(string keyValue)
        {
            try
            {
                return baseMaterialsTypeService.GetBase_MaterialsTypeEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                baseMaterialsTypeService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioMaterialsTypeEntity entity,string deleteList,string type)
        {
            try
            {
                baseMaterialsTypeService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="showProject"></param>
        /// <returns></returns>
        public List<TreeModel> GetTreeList(string projectId, bool showProject)
        {
            try
            {
                var list = baseMaterialsTypeService.GetInfoList(projectId);
                List<TreeModel> treeList = new List<TreeModel>();
                foreach (var item in list)
                {
                    TreeModel node = new TreeModel
                    {
                        id = item.ID,
                        text = item.Name,
                        value = item.ID,
                        showcheck = false,
                        checkstate = 0,
                        isexpand = true,
                        parentId = item.ParentId,
                        FullName = item.FullName
                    };


                    if (string.IsNullOrEmpty(node.parentId)&& showProject && !string.IsNullOrEmpty(item.ProjectID))
                    {
                        var cList = customerService.GetAllList();
                        var cOne = cList.Where(x => x.ID == item.ProjectID).FirstOrDefault();
                        if(cOne != null)
                        {
                            node.parentId = cOne.ID;
                            if (treeList.Where(x => x.id == cOne.ID).Count() < 1)
                            {
                                TreeModel nodeP = new TreeModel
                                {
                                    id = cOne.ID,
                                    text = cOne.Name,
                                    value = cOne.ID,
                                    showcheck = false,
                                    checkstate = 0,
                                    isexpand = true,
                                    parentId = "",
                                    FullName = cOne.Name
                                };
                                treeList.Add(nodeP);
                            }
                        }

                    }
                    treeList.Add(node);
                }
                return treeList.ToTree();
               
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckRename(string value, string type)
        {
            try
            {
                return baseMaterialsTypeService.CheckRename(value,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
