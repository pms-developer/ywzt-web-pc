﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-18 14:42
    /// 描 述：材料分类
    /// </summary>
    public class MeioMaterialsTypeEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 分类编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 采购员
        /// </summary>
        [Column("BUYERID")]
        public string BuyerId { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 分类全名
        /// </summary>
        [Column("FULLNAME")]
        public string FullName { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ISENABLE")]
        public bool? IsEnable { get; set; }
        /// <summary>
        /// 父级ID
        /// </summary>
        [Column("PARENTID")]
        public string ParentId { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column("ProjectID")]
        public string ProjectID { get; set; }


        

        /// <summary>
        /// 是否系统预置
        /// </summary>
        public bool? IsSystem { get; set; } = false;
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
        #region  扩展字段

        [NotMapped]
        public string ParentName { get; set; }
        #endregion
    }
}

