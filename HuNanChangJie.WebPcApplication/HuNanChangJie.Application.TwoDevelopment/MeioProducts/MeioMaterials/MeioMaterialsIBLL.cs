﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.MeioProducts;

namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-19 11:20
    /// 描 述：材料档案
    /// </summary>
    public interface MeioMaterialsIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioMaterialsEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取下拉列表数据
        /// <summary>
        /// <param name="projectId">查询参数</param>
        /// <returns></returns>
        IEnumerable<MeioMaterialsEntity> GetSelectList(string projectId);


        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        int checkNoCount(string kyeValue);

        /// <summary>
        /// 获取箱规列表
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        IEnumerable<meio_product_box_gaugeEntity> Getmeio_product_box_gaugeList(string keyValue);

        /// <summary>
        /// 获取产品辅料数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<meio_product_pick_accessoriesEntity> Getmeio_product_pick_accessoriesList(string keyValue);


        /// <summary>
        /// 获取组合产品包含单品数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<meio_product_pick_comboEntity> Getmeio_product_pick_comboList(string keyValue);


        /// <summary>
        /// 获取Base_Materials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MeioMaterialsEntity GetBase_MaterialsEntity(string keyValue);



        /// <summary>
        /// 获取材料档案清单
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        IEnumerable<MeioMaterialsEntity> GetMaterials(XqPagination pagination, string projectId, string queryJson);

        IEnumerable<MeioMaterialsEntity> GetMaterialsList(XqPagination pagination, string typeId, string projectId, string queryJson);

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue, string logList);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MeioMaterialsEntity entity
            , List<meio_product_box_gaugeEntity> meio_product_box_gaugeList
            , List<meio_product_pick_accessoriesEntity> meio_product_pick_accessoriesList
            , List<meio_product_pick_comboEntity> meio_product_pick_comboList
            , string logList
            , string deleteList
            , string type);
        bool CheckIsSameName(string name);
        #endregion

    }
}
