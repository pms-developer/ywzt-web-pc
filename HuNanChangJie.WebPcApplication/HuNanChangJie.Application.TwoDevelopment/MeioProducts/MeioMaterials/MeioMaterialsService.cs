﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.MeioProducts;
namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-05-19 11:20
    /// 描 述：材料档案
    /// </summary>
    public class MeioMaterialsService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MeioMaterialsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT * from  (");
                strSql.Append(@"
                select b.Name as UnitName,
                c.CnName as BrandName,
                d.Name as TypeName,
                a.* from MeioMaterials as a 
                left join Base_MaterialsUnit as b on a.UnitId=b.Id
                left join MeioMaterialsBrand as c on a.BrandId=c.Id
                left join MeioMaterialsType as d on a.TypeId=d.id
                ");
                strSql.Append("  )  t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });

                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", queryParam["Code"].ToString(), DbType.String);
                    strSql.Append(" AND code=@Code");
                }

                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append("    AND  name like @Name");
                }

                if (!queryParam["TypeId"].IsEmpty() && queryParam["TypeName"].ToString() != "材料")
                {
                    dp.Add("TypeId", queryParam["TypeId"].ToString(), DbType.String);
                    strSql.Append(" AND TypeId=@TypeId");

                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", "%" + queryParam["ProjectID"].ToString() + "%", DbType.String);
                    strSql.Append(" AND ProjectID like @ProjectID");
                }

                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "Code";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<MeioMaterialsEntity>(strSql.ToString(), dp, pagination);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<MeioMaterialsEntity> GetSelectList(string projectId)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT * from  (");
                strSql.Append(@"
                select b.Name as UnitName,
                c.CnName as BrandName,
                d.Name as TypeName,
                a.* from MeioMaterials as a 
                left join Base_MaterialsUnit as b on a.UnitId=b.Id
                left join MeioMaterialsBrand as c on a.BrandId=c.Id
                left join MeioMaterialsType as d on a.TypeId=d.id
                ");
                strSql.Append("  )  t ");
                strSql.Append("  WHERE 1=1 ");
                // 虚拟参数
                var dp = new DynamicParameters(new { });

                if (!projectId.IsEmpty())
                {
                    dp.Add("ProjectID", "%" + projectId + "%", DbType.String);
                    strSql.Append(" AND ProjectID like @ProjectID");
                }
                var list = this.BaseRepository().FindList<MeioMaterialsEntity>(strSql.ToString(), dp);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 查询编号总数
        /// </summary>
        /// <param name="kyeValue"></param>
        /// <returns></returns>
        public int checkNoCount(string kyeValue)
        {
            string sql = "select count(1) as c from [dbo].[MeioMaterials] where Code='" + kyeValue + "'";
            DataTable dt = this.BaseRepository().FindTable(sql);
            return int.Parse(dt.Rows[0]["c"].ToString());
        }

        /// <summary>
        /// 获取箱规表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meio_product_box_gaugeEntity> Getmeio_product_box_gaugeList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<meio_product_box_gaugeEntity>(t => t.ProId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取产品辅料数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meio_product_pick_accessoriesEntity> Getmeio_product_pick_accessoriesList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<meio_product_pick_accessoriesEntity>(t => t.ProId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取产品辅料数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<meio_product_pick_comboEntity> Getmeio_product_pick_comboList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<meio_product_pick_comboEntity>(t => t.ProId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取根据项目id获取材料档案 以及项目id为空的材料档案
        /// <summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        public IEnumerable<MeioMaterialsEntity> GetMaterials(XqPagination pagination, string projectId, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT * from  (");
                strSql.Append(@"
                select b.Name as UnitName,
                c.CnName as BrandName,
                d.Name as TypeName,
                a.* from MeioMaterials as a 
                left join Base_MaterialsUnit as b on a.UnitId=b.Id
                left join MeioMaterialsBrand as c on a.BrandId=c.Id
                left join MeioMaterialsType as d on a.TypeId=d.id
                ");
                strSql.Append("  )  t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "Code";
                    pagination.sord = "DESC";
                }

                if (!projectId.IsEmpty())
                {
                    dp.Add("ProjectId", projectId, DbType.String);
                    strSql.Append(" And (IsProject=0 or projectid=@ProjectId) ");
                }

                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append("    AND  name like @Name");
                }

                var list = this.BaseRepository().FindList<MeioMaterialsEntity>(strSql.ToString(), dp, pagination);

                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }




        /// <summary>
        /// 获取根据项目id获取材料档案 以及项目id为空的材料档案
        /// <summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        public IEnumerable<MeioMaterialsEntity> GetMaterialsList(XqPagination pagination, string typeId, string projectId, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT * from  (");
                strSql.Append(@"
                select b.Name as UnitName,
                c.CnName as BrandName,
                d.Name as TypeName,
                a.* from MeioMaterials as a 
                left join Base_MaterialsUnit as b on a.UnitId=b.Id
                left join MeioMaterialsBrand as c on a.BrandId=c.Id
                left join MeioMaterialsType as d on a.TypeId=d.id
                ");
                strSql.Append("  )  t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "Code";
                    pagination.sord = "DESC";
                }

                if (!projectId.IsEmpty())
                {
                    strSql.Append(" And (IsProject=0 or projectid='" + projectId + "') ");
                }

                //if (!typeId.IsEmpty())
                //{
                //    strSql.Append(" And t.TypeId='" + typeId + "' ");
                //}

                string typeName = queryParam["TypeName"].ToString();

                if (typeName == "材料")
                {
                    if (!queryParam["Name"].IsEmpty())
                    {
                        dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                        strSql.Append("    AND  name like @Name");
                    }
                    else
                    {
                        dp.Add("TypeId", queryParam["TypeId"].ToString(), DbType.String);
                        strSql.Append(" AND t.TypeId = @TypeId ");
                    }
                }
                else
                {
                    if (!queryParam["TypeId"].IsEmpty())
                    {
                        dp.Add("TypeId", queryParam["TypeId"].ToString(), DbType.String);
                        strSql.Append(" AND t.TypeId = @TypeId ");
                    }

                    if (!queryParam["Name"].IsEmpty())
                    {
                        dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                        strSql.Append("    AND  name like @Name");
                    }
                }

                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" And (IsProject=0 or projectid=@ProjectId )");
                }




                var list = this.BaseRepository().FindList<MeioMaterialsEntity>(strSql.ToString(), dp, pagination);

                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }





        /// <summary>
        /// 获取Base_Materials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MeioMaterialsEntity GetBase_MaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MeioMaterialsEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MeioMaterialsEntity>(t => t.ID == keyValue);
                this.BaseRepository().Delete<meio_product_box_gaugeEntity>(t => t.ProId == keyValue);//箱规
                this.BaseRepository().Delete<meio_product_pick_accessoriesEntity>(t => t.ProId == keyValue);//辅料
                this.BaseRepository().Delete<meio_product_pick_comboEntity>(t => t.ProId == keyValue);//包含产品
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal bool CheckIsSameName(string name)
        {
            try
            {
                var info = this.BaseRepository().FindEntity<MeioMaterialsEntity>(t => t.Name.Trim().ToLower() == name.Trim().ToLower());
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MeioMaterialsEntity entity
            , List<meio_product_box_gaugeEntity> meio_product_box_gaugeList
            , List<meio_product_pick_accessoriesEntity> meio_product_pick_accessoriesList
            , List<meio_product_pick_comboEntity> meio_product_pick_comboList
            , string deleteList
            , string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, this.BaseRepository());
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM {del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, this.BaseRepository());
                            }
                        }
                    }

                    //没有生成代码 
                    var zhibin_j_descUpdateList = meio_product_box_gaugeList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in zhibin_j_descUpdateList)
                    {
                        this.BaseRepository().Update(item);
                    }
                    var zhibin_j_descInserList = meio_product_box_gaugeList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in zhibin_j_descInserList)
                    {
                        item.Create(item.ID);
                        item.ProId = entity.ID;
                        this.BaseRepository().Insert(item);
                    }

                    //没有生成代码 
                    var accessoriesUpdateList = meio_product_pick_accessoriesList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in accessoriesUpdateList)
                    {
                        this.BaseRepository().Update(item);
                    }
                    var accessoriesInserList = meio_product_pick_accessoriesList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in accessoriesInserList)
                    {
                        item.Create(item.ID);
                        item.ProId = entity.ID;
                        this.BaseRepository().Insert(item);
                    }

                    if (entity.Origin == "组合产品")
                    {
                        //没有生成代码 
                        var meio_product_pick_comboUpdateList = meio_product_pick_comboList.FindAll(i => i.EditType == EditType.Update);
                        foreach (var item in meio_product_pick_comboUpdateList)
                        {
                            this.BaseRepository().Update(item);
                        }
                        var meio_product_pick_comboInserList = meio_product_pick_comboList.FindAll(i => i.EditType == EditType.Add);
                        foreach (var item in meio_product_pick_comboInserList)
                        {
                            item.Create(item.ID);
                            item.ProId = entity.ID;
                            this.BaseRepository().Insert(item);
                        }
                    }
                    else
                    {
                        this.BaseRepository().Delete<meio_product_pick_comboEntity>(x=>x.ProId == entity.ID);
                    }

                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                    foreach (meio_product_box_gaugeEntity item in meio_product_box_gaugeList)
                    {
                        item.ProId = entity.ID;
                        this.BaseRepository().Insert(item);
                    }
                    foreach (meio_product_pick_accessoriesEntity item in meio_product_pick_accessoriesList)
                    {
                        item.ProId = entity.ID;
                        this.BaseRepository().Insert(item);
                    }
                    if (entity.Origin == "组合产品")
                    {
                        foreach (meio_product_pick_comboEntity item in meio_product_pick_comboList)
                        {
                            item.ProId = entity.ID;
                            this.BaseRepository().Insert(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
