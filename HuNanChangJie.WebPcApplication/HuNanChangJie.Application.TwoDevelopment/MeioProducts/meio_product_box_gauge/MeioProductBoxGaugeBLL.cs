﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2024-04-10 08:47 
    /// 描 述：美鸥产品采购箱规 
    /// </summary> 
    public class MeioProductBoxGaugeBLL : MeioProductBoxGaugeIBLL
    {
        private MeioProductBoxGaugeService meioProductBoxGaugeService = new MeioProductBoxGaugeService();

        #region  获取数据 

        /// <summary> 
        /// 获取列表数据 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<meio_product_box_gaugeEntity> GetList(string queryJson)
        {
            try
            {
                return meioProductBoxGaugeService.GetList(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取列表分页数据 
        /// <param name="pagination">分页参数</param> 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<meio_product_box_gaugeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return meioProductBoxGaugeService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public meio_product_box_gaugeEntity GetEntity(string keyValue)
        {
            try
            {
                return meioProductBoxGaugeService.GetEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void DeleteEntity(string keyValue)
        {
            try
            {
                meioProductBoxGaugeService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void SaveEntity(string keyValue, meio_product_box_gaugeEntity entity)
        {
            try
            {
                meioProductBoxGaugeService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}