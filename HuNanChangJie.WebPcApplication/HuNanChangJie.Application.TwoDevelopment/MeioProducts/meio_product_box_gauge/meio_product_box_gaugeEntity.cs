﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts

{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2024-04-10 08:47 
    /// 描 述：美鸥产品采购箱规 
    /// </summary> 
    public class meio_product_box_gaugeEntity: BaseEntity
    {
        #region  实体成员 
        /// <summary> 
        /// 箱规名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary> 
        /// 单箱数量 
        /// </summary> 
        /// <returns></returns> 
        [Column("NUM")]
        public int? Num { get; set; }
        /// <summary> 
        /// 外箱规格1 
        /// </summary> 
        /// <returns></returns> 
        [Column("OUTBOXSPECONE")]
        public decimal? OutBoxSpecOne { get; set; }
        /// <summary> 
        /// 外箱规格2 
        /// </summary> 
        /// <returns></returns> 
        [Column("OUTBOXSPECTWO")]
        public decimal? OutBoxSpecTwo { get; set; }
        /// <summary> 
        /// 外箱规格3 
        /// </summary> 
        /// <returns></returns> 
        [Column("OUTBOXSPECTHREE")]
        public decimal? OutBoxSpecThree { get; set; }
        /// <summary> 
        /// 包装规格1 
        /// </summary> 
        /// <returns></returns> 
        [Column("PACKSPECONE")]
        public decimal? PackSpecOne { get; set; }
        /// <summary> 
        /// 包装规格2 
        /// </summary> 
        /// <returns></returns> 
        [Column("PACKSPECTWO")]
        public decimal? PackSpecTwo { get; set; }
        /// <summary> 
        /// 包装规格3 
        /// </summary> 
        /// <returns></returns> 
        [Column("PACKSPECTHREE")]
        public decimal? PackSpecThree { get; set; }
        /// <summary> 
        /// 单箱重量 
        /// </summary> 
        /// <returns></returns> 
        [Column("SINGLEBOXWEIGHT")]
        public decimal? SingleBoxWeight { get; set; }
        /// <summary> 
        /// 单箱重量单位 
        /// </summary> 
        /// <returns></returns> 
        [Column("SINGLEBOXWEIGHTUNIT")]
        public string SingleBoxWeightUnit { get; set; }
        /// <summary> 
        /// 单箱毛重 
        /// </summary> 
        /// <returns></returns> 
        [Column("SINGLEBOXGROSSWEIGHT")]
        public decimal? SingleBoxGrossWeight { get; set; }
        /// <summary> 
        /// 单箱毛重单位 
        /// </summary> 
        /// <returns></returns> 
        [Column("SINGLEBOXGROSSWEIGHTUNIT")]
        public string SingleBoxGrossWeightUnit { get; set; }

        /// <summary> 
        /// 是否默认箱规 
        /// </summary> 
        /// <returns></returns> 
        [Column("IsDefault")]
        public bool IsDefault { get; set; }

        
        /// <summary> 
        /// 产品id 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROID")]
        public string ProId { get; set; }
        /// <summary> 
        /// 主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("ID")]
        public string ID { get; set; }
        /// <summary> 
        /// 是否启用 
        /// </summary> 
        /// <returns></returns> 
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary> 
        /// 创建时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary> 
        /// 创建者ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary> 
        /// 创建者名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary> 
        /// 修改时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary> 
        /// 修改者ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary> 
        /// 修改名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary> 
        /// 排序号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary> 
        /// 公司编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary> 
        /// 公司ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary> 
        /// 公司全称 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary> 
        /// 公司简称 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary> 
        /// 拼音码 
        /// </summary> 
        /// <returns></returns> 
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary> 
        /// 拼音码首字母简写 
        /// </summary> 
        /// <returns></returns> 
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary> 
        /// 审核者ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary> 
        /// 审核者名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary> 
        /// 审核状态 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary> 
        /// 店铺ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary> 
        /// 店铺名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary> 
        /// 审批流ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
    }
}