﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2024-04-10 08:47 
    /// 描 述：美鸥产品采购箱规 
    /// </summary> 
    public interface MeioProductBoxGaugeIBLL
    {
        #region  获取数据 

        /// <summary> 
        /// 获取列表数据 
        /// <summary> 
        /// <returns></returns> 
        IEnumerable<meio_product_box_gaugeEntity> GetList(string queryJson);
        /// <summary> 
        /// 获取列表分页数据 
        /// <param name="pagination">分页参数</param> 
        /// <summary> 
        /// <returns></returns> 
        IEnumerable<meio_product_box_gaugeEntity> GetPageList(XqPagination pagination, string queryJson);
        
        /// <summary> 
        /// 获取实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        meio_product_box_gaugeEntity GetEntity(string keyValue);
        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        void DeleteEntity(string keyValue);
        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        void SaveEntity(string keyValue, meio_product_box_gaugeEntity entity);
        #endregion

    }
}