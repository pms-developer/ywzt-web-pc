﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2024-04-10 08:47 
    /// 描 述：美鸥产品采购箱规 
    /// </summary> 
    public class MeioProductBoxGaugeService : RepositoryFactory
    {
        #region  构造函数和属性 

        private string fieldSql;
        public MeioProductBoxGaugeService()
        {
            fieldSql = @" 
                t.Name, 
                t.Num, 
                t.OutBoxSpecOne, 
                t.OutBoxSpecTwo, 
                t.OutBoxSpecThree, 
                t.PackSpecOne, 
                t.PackSpecTwo, 
                t.PackSpecThree, 
                t.SingleBoxWeight, 
                t.SingleBoxWeightUnit, 
                t.SingleBoxGrossWeight, 
                t.SingleBoxGrossWeightUnit, 
                t.ProId, 
                t.ID, 
                t.Enabled, 
                t.CreationDate, 
                t.Creation_Id, 
                t.CreationName, 
                t.ModificationDate, 
                t.Modification_Id, 
                t.ModificationName, 
                t.SortCode, 
                t.CompanyCode, 
                t.Company_ID, 
                t.CompanyFullName, 
                t.CompanyName, 
                t.PinYin, 
                t.PinYinShort, 
                t.Auditor_ID, 
                t.AuditorName, 
                t.AuditStatus, 
                t.Base_ShopInfoID, 
                t.ShopName, 
                t.Workflow_ID 
            ";
        }
        #endregion

        #region  获取数据 

        /// <summary> 
        /// 获取列表数据 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<meio_product_box_gaugeEntity> GetList(string queryJson)
        {
            try
            {
                //参考写法 
                //var queryParam = queryJson.ToJObject(); 
                // 虚拟参数 
                //var dp = new DynamicParameters(new { }); 
                //dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime); 
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM meio_product_box_gauge t ");
                return this.BaseRepository().FindList<meio_product_box_gaugeEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取列表分页数据 
        /// <param name="pagination">分页参数</param> 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<meio_product_box_gaugeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM meio_product_box_gauge t ");
                return this.BaseRepository().FindList<meio_product_box_gaugeEntity>(strSql.ToString(), pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public meio_product_box_gaugeEntity GetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<meio_product_box_gaugeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<meio_product_box_gaugeEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void SaveEntity(string keyValue, meio_product_box_gaugeEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}