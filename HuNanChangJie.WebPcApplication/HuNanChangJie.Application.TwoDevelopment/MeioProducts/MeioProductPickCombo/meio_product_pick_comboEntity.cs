﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.MeioProducts

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-10 19:15
    /// 描 述：组合产品包含关系
    /// </summary>
    public class meio_product_pick_comboEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// 品名
        /// </summary>
        /// <returns></returns>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// SKU
        /// </summary>
        /// <returns></returns>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 型号
        /// </summary>
        /// <returns></returns>
        [Column("MODEL")]
        public string Model { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        /// <returns></returns>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 采购交期
        /// </summary>
        /// <returns></returns>
        [Column("LEADDAY")]
        public int? LeadDay { get; set; }
        /// <summary>
        /// 采购成本
        /// </summary>
        /// <returns></returns>
        [Column("PURCHASECOST")]
        public decimal? PurchaseCost { get; set; }
        /// <summary>
        /// 单品规格-长(cm)
        /// </summary>
        /// <returns></returns>
        [Column("SINGLESPECONE")]
        public decimal? SingleSpecOne { get; set; }
        /// <summary>
        /// 单品规格-宽(cm)
        /// </summary>
        /// <returns></returns>
        [Column("SINGLESPECTWO")]
        public decimal? SingleSpecTwo { get; set; }
        /// <summary>
        /// 单品规格-高(cm)
        /// </summary>
        /// <returns></returns>
        [Column("SINGLESPECTHREE")]
        public decimal? SingleSpecThree { get; set; }
        /// <summary>
        /// 单品净重
        /// </summary>
        /// <returns></returns>
        [Column("SINGLENETWEIGHT")]
        public decimal? SingleNetWeight { get; set; }

        /// <summary>
        /// 单品毛重
        /// </summary>
        /// <returns></returns>
        [Column("SingleBoxGrossWeight")]
        public decimal? SingleBoxGrossWeight { get; set; }

        
        /// <summary>
        /// 产品材质
        /// </summary>
        /// <returns></returns>
        [Column("MATERIAL")]
        public string Material { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        /// <returns></returns>
        [Column("Imgs")]
        public string Imgs { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        /// <returns></returns>
        [Column("NUM")]
        public int? Num { get; set; }
        /// <summary>
        /// 产品id
        /// </summary>
        /// <returns></returns>
        [Column("PROID")]
        public string ProId { get; set; }
        /// <summary>
        /// 组合产品id
        /// </summary>
        /// <returns></returns>
        [Column("COMBOID")]
        public string ComboId { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        /// <returns></returns>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        /// <returns></returns>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        /// <returns></returns>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        /// <returns></returns>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        /// <returns></returns>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        /// <returns></returns>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        /// <returns></returns>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        /// <returns></returns>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        /// <returns></returns>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        /// <returns></returns>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        /// <returns></returns>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        /// <returns></returns>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        /// <returns></returns>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        /// <returns></returns>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        /// <returns></returns>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion


        /// <summary>
        /// 产品图片
        /// </summary>
        [NotMapped]
        public string baseImg { get; set; }

        /// <summary>
        /// 产品图片id
        /// </summary>
        [NotMapped]
        public string baseImgId { get; set; }

    }
}

