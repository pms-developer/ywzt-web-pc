﻿using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.Extend
{
    /// <summary>
    /// 付款审核
    /// </summary>
    public class AuditPaymentExtend : IDisposable
    {
        private IRepository _db;
        private string _projectid;
        private ReceiptEnum _billType;
        private string _billId;
        private string _subjectId;
        private decimal _amount;
        private decimal _rate;
        private string _accountId;
        private string _receiptId;
        private bool _isdisposed;

        ~AuditPaymentExtend()
        {
            Dispose(false);
        }
        /// <summary>
        /// 扩展审核
        /// </summary>
        /// <param name="db">数据库链接对象</param>
        /// <param name="projectid">项目Id</param>
        /// <param name="billType">单据类型</param>
        /// <param name="billId">关联ID（主表keyValue）</param>
        /// <param name="receiptId">单据ID（子单据keyValue）</param>
        /// <param name="amount">发生额</param>
        /// <param name="accountId">项目资金账户ID</param>
        /// <param name="subjectId">基础科目ID(BaseSubjectId)</param>
        /// <param name="rate">税率</param>
        public AuditPaymentExtend(IRepository db, string projectid, ReceiptEnum billType, string billId, string receiptId, decimal amount, string accountId, string subjectId = null, decimal rate = 0)
        {
            _db = db;
            _projectid = projectid;
            _billType = billType;
            _billId = billId;
            _subjectId = subjectId;
            _amount = amount;
            _rate = rate;
            _accountId = accountId;
            _receiptId = receiptId;
        }

        /// <summary>
        /// 审核
        /// </summary>
        public Util.Common.OperateResultEntity Audit(string abstractStr)
        {
            if (!string.IsNullOrWhiteSpace(_subjectId) && !string.IsNullOrWhiteSpace(_projectid))
            {
                var subjectInfoID = new ProjectSubjectBll().GetEntity(_subjectId, _projectid);
                if (subjectInfoID != null)
                {
                    var subjectInfo = _db.FindEntity<ProjectSubjectEntity>(i => i.ID == subjectInfoID.ID);
                    if (subjectInfo == null) return new Util.Common.OperateResultEntity() { Success = false, Message = "未找到对应的科目" };
                    subjectInfo.ActuallyOccurred = (subjectInfo.ActuallyOccurred ?? 0) + _amount;
                    decimal afterTax = _amount - (_amount * _rate / 100);
                    subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;
                    subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) - _amount;

                    _db.Update<ProjectSubjectEntity>(subjectInfo);
                }

            }
            return ChangeFundBalance(AuditOperation.Audit, abstractStr);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_isdisposed) return;
            if (disposing)
            { }
            _isdisposed = true;
        }

        /// <summary>
        /// 反审核
        /// </summary>
        public Util.Common.OperateResultEntity UnAudit(string abstractStr)
        {
            if (!string.IsNullOrWhiteSpace(_subjectId))
            {
                var subjectInfo = new ProjectSubjectBll().GetEntity(_subjectId, _projectid);
                if (subjectInfo == null) return new Util.Common.OperateResultEntity() { Success = false, Message = "未找到对应的科目" };
                subjectInfo.ActuallyOccurred = (subjectInfo.ActuallyOccurred ?? 0) - _amount;
                decimal afterTax = _amount - (_amount * _rate);
                subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) - afterTax;
                subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) + _amount;
                _db.Update(subjectInfo);
            }
            return ChangeFundBalance(AuditOperation.UnAudit,abstractStr);
        }

        /// <summary>
        /// 改变账户余额
        /// </summary>
        /// <param name="changes"></param>
        /// <param name="type"></param>
        private OperateResultEntity ChangeFundBalance(AuditOperation type,string abstractStr)
        {
            var fundBll = new Base_FundAccountBLL();
            var billName = ReceiptInfo.GetReceiptName(_billType);

            var fundInfo = new DoFundAccountEntity();
            fundInfo.FundAccountFlag = _accountId;
            fundInfo.Abstract = type == AuditOperation.Audit ? $"{abstractStr}[《{billName}》付款审核]" : $"{abstractStr}[《{billName}》付款反审核";
            fundInfo.AmountOccurrence = _amount;
            fundInfo.OccurrenceType = type == AuditOperation.Audit ? OccurrenceEnum.Outlay : OccurrenceEnum.Income;
            fundInfo.ProjectId = _projectid;
            fundInfo.ProjectSubjectId = _subjectId;
            fundInfo.ReceiptType = _billType;
            fundInfo.ReceiptId = _receiptId;
            fundInfo.RelevanceId = _billId;
            fundInfo.RelevanceType = string.IsNullOrEmpty(_projectid) ? RelevanceEnum.Other : RelevanceEnum.Project;
            return fundBll.DoFundAccount(fundInfo, _db);
        }


    }
}
