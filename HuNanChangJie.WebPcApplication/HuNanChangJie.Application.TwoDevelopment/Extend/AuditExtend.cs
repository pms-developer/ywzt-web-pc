﻿using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Application.TwoDevelopment.Formula;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using HuNanChangJie.Util.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.TwoDevelopment.Extend
{
    /// <summary>
    /// 审核扩展
    /// </summary>
    public class AuditExtend:IDisposable
    {
        private IRepository _db;
        private string _projectid;
        private ReceiptEnum _billType;
        private string _billId;
        private bool _disposed;
        private string _currentId;

        ~AuditExtend()
        {
            Dispose(false);
        }
        /// <summary>
        /// 扩展审核
        /// </summary>
        /// <param name="db">数据库链接对象</param>
        /// <param name="projectid">项目Id</param>
        /// <param name="billType">单据类型</param>
        /// <param name="billId">单据ID（即keyValue）</param>
        /// <param name="currentId">当前单据ID</param>
        public AuditExtend(IRepository db, string projectid, ReceiptEnum billType,string billId,string currentId)
        {
            _db = db;
            _projectid = projectid;
            _billType = billType;
            _billId = billId;
            _currentId = currentId;
        }

        /// <summary>
        /// 审核
        /// </summary>
        public string Audit()
        {
            var changeList = new List<BudgetChangeInfo>();
            AuditOperat(changeList, AuditOperation.Audit);
            return ChangeFundBalance(changeList, AuditOperation.Audit);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            { }
            _disposed = true;
        }

        /// <summary>
        /// 反审核
        /// </summary>
        public string UnAudit()
        {
            var changeList = new List<BudgetChangeInfo>();
            AuditOperat(changeList, AuditOperation.UnAudit);
            return ChangeFundBalance(changeList, AuditOperation.UnAudit);
        }

        private void AuditOperat(List<BudgetChangeInfo> changeList, AuditOperation type)
        {
            var formula = new Formula.Formula();
            var runtimes = formula.Analysis(_projectid,_currentId,type);
            if (runtimes == null) return;
            if (runtimes.Count <= 0) return;
            var projectSubjectIdList = runtimes.Select(i => i.ProjectSubjectId);
            var sbujectBll = new ProjectSubjectBll();
            var sbujectList = sbujectBll.GetList(projectSubjectIdList);
            foreach (var item in sbujectList)//获取预算信息
            {
                var changeInfo = new BudgetChangeInfo();
                changeInfo.ProjectSubjectId = item.ID;
                changeInfo.Befor = item.TempValue.ToDecimal();
                changeList.Add(changeInfo);
            }

            foreach (var runtime in runtimes)
            {
                var changeInfo = changeList.FirstOrDefault(i => i.ProjectSubjectId == runtime.ProjectSubjectId);
                if (changeInfo == null) continue;
                changeInfo.Result = runtime.Result;
                changeInfo.After = runtime.Result >= 0 ? runtime.Result : 0;

                if (runtime.Result < 0) continue;
                var subjectInfo = new ProjectSubjectEntity();
                subjectInfo.ID = runtime.ProjectSubjectId;

                subjectInfo.ActuallyOccurred = (subjectInfo.ActuallyOccurred??0)-(subjectInfo.TempValue??0)+runtime.Result;
                subjectInfo.AOAfterTax = subjectInfo.ActuallyOccurred;
                subjectInfo.TempValue = runtime.Result;

                _db.Update(subjectInfo);
            }
        }

        /// <summary>
        /// 改变账户余额
        /// </summary>
        /// <param name="changes"></param>
        /// <param name="type"></param>
        private string ChangeFundBalance(List<BudgetChangeInfo> changes, AuditOperation type)
        {
            Base_FundAccountIBLL fundBll = new Base_FundAccountBLL();
            var billName = ReceiptInfo.GetReceiptName(_billType);
            var list = new List<OperateResultEntity>();

            var fundAccount = _db.FindEntity<Base_FundAccountEntity>(entity => entity.Name == _projectid
           || entity.BankCode == _projectid
           || entity.ProjectId == _projectid
           || entity.ID == _projectid);
            if (fundAccount == null)  return ""; 
            foreach (var item in changes)
            {
                var fundDetails = _db.FindEntity<Base_FundAccoutDetailsEntity>(a => a.ProjectId == _projectid && a.ProjectSubjectId == item.ProjectSubjectId);
                if (fundDetails == null)
                {
                    fundDetails = new Base_FundAccoutDetailsEntity
                    {
                        ID = Guid.NewGuid().ToString(),
                        FundAccountId = fundAccount.ID,
                        AmountOccurrence = item.Result,
                        OccurrenceType = OccurrenceEnum.Outlay.ToString(),
                        RelevanceType = RelevanceEnum.Project.ToString(),
                        RelevanceId = _billId,
                        ReceiptType = _billType.ToString(),
                        ReceiptId = _billId,
                        OccurrenceDate = DateTime.Now,
                        OperatorId = LoginUserInfo.UserInfo.userId,
                        Abstract = $"{billName}审核,系统自动进行成本分摊",
                        ProjectSubjectId = item.ProjectSubjectId,
                        ProjectId = _projectid
                    };
                    _db.Insert(fundDetails);
                }
                else
                {
                    fundDetails.OperatorId = LoginUserInfo.UserInfo.userId;
                    fundDetails.OccurrenceDate = DateTime.Now;
                    fundDetails.AmountOccurrence = item.Result;
                    _db.Update(fundDetails);
                }
            }
            return fundAccount.ID;
            
        }


    }
}
