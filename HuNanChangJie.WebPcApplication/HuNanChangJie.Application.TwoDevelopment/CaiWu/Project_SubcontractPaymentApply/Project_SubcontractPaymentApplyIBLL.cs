﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 16:21
    /// 描 述：分包付款申请
    /// </summary>
    public interface Project_SubcontractPaymentApplyIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_SubcontractPaymentApplyEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_SubcontractPaymentApplyDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_SubcontractPaymentApplyDetailsEntity> GetProject_SubcontractPaymentApplyDetailsList(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractPaymentApply表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractPaymentApplyEntity GetProject_SubcontractPaymentApplyEntity(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractPaymentApplyDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractPaymentApplyDetailsEntity GetProject_SubcontractPaymentApplyDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_SubcontractPaymentApplyEntity entity,List<Project_SubcontractPaymentApplyDetailsEntity> project_SubcontractPaymentApplyDetailsList,string deleteList,string type);
        #endregion

    }
}
