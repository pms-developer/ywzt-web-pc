﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 16:21
    /// 描 述：分包付款申请
    /// </summary>
    public class Project_SubcontractPaymentApplyDetailsEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// 罚款金额
        /// </summary>
        [Column("FINEEXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? FineExpend { get; set; }

        /// <summary>
        /// 其它收入
        /// </summary>
        [Column("OTHERINCOME")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherIncome { get; set; }
        /// <summary>
        /// 其它支出
        /// </summary>
        [Column("OTHEREXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherExpend { get; set; }
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 付款申请单ID
        /// </summary>
        [Column("PROJECTSUBCONTRACTPAYMENTAPPLYID")]
        public string ProjectSubcontractPaymentApplyId { get; set; }
        /// <summary>
        /// 分包商Id
        /// </summary>
        [Column("CUSTOMERID")]
        public string CustomerId { get; set; }
        /// <summary>
        /// 分包合同Id
        /// </summary>
        [Column("PROJECTSUBCONTRACTID")]
        public string ProjectSubcontractId { get; set; }
        /// <summary>
        /// 合同名称
        /// </summary>
        /// [
        [NotMapped]
        public string Name { get; set; }
        /// <summary>
        /// 本次申请
        /// </summary>
        [Column("APPLYAMOUNT")]
        public decimal? ApplyAmount { get; set; }
        /// <summary>
        /// 本次分包产值
        /// </summary>
        [Column("OUTPUT")]
        public decimal? Output { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 本次应付款
        /// </summary>
        [Column("ACCOUNTPAYABLE")]
        public decimal? AccountPayable { get; set; }
        /// <summary>
        /// 实际应付款
        /// </summary>
        [Column("ACTUALPAYABLE")]
        public decimal? ActualPayable { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region 扩展成员

        /// <summary>
        /// 当前合同金额
        /// </summary>
        [NotMapped]
        public decimal? TotalAmount { get; set; }

        /// <summary>
        /// 分包结算金额
        /// </summary>
        [NotMapped]
        public decimal? SettlementAmount { get; set; }

        /// <summary>
        /// 已收票金额
        /// </summary>
        [NotMapped]
        public decimal? InvioceAmount { get; set; }

        /// <summary>
        /// 已付款金额
        /// </summary>
        [NotMapped]
        public decimal? AccountPaidAmount { get; set; }

        /// <summary>
        /// 实际付款比例(%)
        /// </summary>
        [NotMapped]
        public decimal? Sjfkbl
        {
            get
            {
                if (SettlementAmount == null) return null;
                if (SettlementAmount == 0) return null;
                return (AccountPaidAmount ?? 0) / SettlementAmount * 100;
            }
        }

        /// <summary>
        /// 约定付账比例(%)
        /// </summary>
        [NotMapped]
        public decimal? YdfkBiLi { get; set; }

        /// <summary>
        /// 约定应付账款
        /// </summary>
        [NotMapped]
        public decimal? YdyfKuan
        {
            get
            {
                decimal? YdyfKuans = 0;

                if (YdfkBiLi == null || YdfkBiLi <= 0)
                {
                    YdyfKuans = 0 - FineExpend - OtherExpend + OtherIncome;
                    return YdyfKuans;
                }

                YdyfKuans = (SettlementAmount ?? 0) * YdfkBiLi / 100 - (AccountPaidAmount ?? 0);

                YdyfKuans = YdyfKuans - FineExpend - OtherExpend + OtherIncome;

                return YdyfKuans;
            }
        }

        #endregion
    }
}

