﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Loger;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 16:21
    /// 描 述：分包付款申请
    /// </summary>
    public class Project_SubcontractPaymentApplyService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        private Log log = LogFactory.GetLogger(typeof(Project_SubcontractPaymentApplyService));
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractPaymentApplyEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                c.Name CustomerId,
                t.ApplyDate,
                t.PlanPaymentDate,
                t.BankId,
                d.f_realname OperatorId,
                t.ApplyPaymentAmount,
                t.Abstract,
                t.AuditStatus,
                t.Workflow_ID,
                t.AccountName,
                t.BankName,
                t.PayModel,
                t.BankAccount,
                t.CreationDate,
                t.PayCompanyId,
                cmp.F_FullName as PayCompanyName,
                isnull(counts,0) printcount
                ");
                strSql.Append("  FROM Project_SubcontractPaymentApply t  left join (select infoid,counts from Base_PrintStatistics where tablename = 'Project_SubcontractPaymentApply') i on t.id = i.infoid left join Base_SubcontractingUnit c on t.CustomerId = c.id left join Base_User d on t.OperatorId = d.f_userid");
                strSql.Append(" left join Base_Company as cmp on t.PayCompanyId=cmp.F_CompanyId ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Project_ID"].IsEmpty())
                {
                    dp.Add("Project_ID", queryParam["Project_ID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID=@Project_ID");
                }
                if (!queryParam["PayCompanyId"].IsEmpty() && queryParam["PayCompanyId"].ToString()!="-1")
                {
                    dp.Add("PayCompanyId", queryParam["PayCompanyId"].ToString(), DbType.String);
                    strSql.Append(" AND PayCompanyId = @PayCompanyId");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }

                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["PayModel"].IsEmpty())
                {
                    dp.Add("PayModel", queryParam["PayModel"].ToString(), DbType.String);
                    strSql.Append(" AND t.PayModel = @PayModel ");
                }
                if (!queryParam["CustomerId"].IsEmpty())
                {
                    dp.Add("CustomerId", "%" + queryParam["CustomerId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND c.Name Like @CustomerId ");
                }
                if (!queryParam["Abstract"].IsEmpty())
                {
                    dp.Add("Abstract", "%" + queryParam["Abstract"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Abstract Like @Abstract ");

                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }

                if (!queryParam["apStartTime"].IsEmpty() && !queryParam["apEndTime"].IsEmpty())
                {
                    dp.Add("apStartTime", queryParam["apStartTime"].ToDate(), DbType.DateTime);
                    dp.Add("apEndTime", queryParam["apEndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.ApplyDate >= @apStartTime AND t.ApplyDate <= @apEndTime ) ");
                }

                if (!queryParam["SAmount"].IsEmpty())
                {
                    dp.Add("SAmount", queryParam["SAmount"].ToString(), DbType.String);
                    strSql.Append(" AND t.ApplyPaymentAmount>=@SAmount");
                }
                if (!queryParam["EAmount"].IsEmpty())
                {
                    dp.Add("EAmount", queryParam["EAmount"].ToString(), DbType.String);
                    strSql.Append(" AND t.ApplyPaymentAmount<=@EAmount");
                }

                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Project_SubcontractPaymentApplyEntity>(strSql.ToString(), dp, pagination);

                if (list != null && list.Count() > 0)
                {
                    List<string> ids = new List<string>();
                    foreach (Project_SubcontractPaymentApplyEntity item in list)
                    {
                        ids.Add(item.ID);
                    }

                    string idstr = string.Join("','", ids.ToArray());

                    var listdetails = this.BaseRepository().FindList<Project_SubcontractPaymentApplyDetailsEntity>($"select * from Project_SubcontractPaymentApplyDetails where ProjectSubcontractPaymentApplyId in('{idstr}')");

                    foreach (Project_SubcontractPaymentApplyEntity item in list)
                    {
                        item.TypeList = "";

                        foreach (Project_SubcontractPaymentApplyDetailsEntity itemdetails in listdetails.Where(m => m.ProjectSubcontractPaymentApplyId == item.ID))
                        {
                            if (!string.IsNullOrEmpty(item.TypeList))
                                item.TypeList += ",";

                            var subcontract = this.BaseRepository().FindEntity<Project_SubcontractEntity>(i => i.ID == itemdetails.ProjectSubcontractId);
                            if (subcontract != null)
                                item.TypeList += $"【{subcontract.Name}-{subcontract.ProjectName}】";
                        }
                    }
                }

                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractPaymentApplyDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractPaymentApplyDetailsEntity> GetProject_SubcontractPaymentApplyDetailsList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append("select a.*,");
                sb.Append(" b.name, b.TotalAmount, b.SettlementAmount, b.InvioceAmount, b.AccountPaidAmount,b.YdfkBiLi ");
                sb.Append(" from Project_SubcontractPaymentApplyDetails a left join ");
                sb.Append(" Project_Subcontract b on a.ProjectSubcontractId = b.ID where ProjectSubcontractPaymentApplyId = '" + keyValue + "'");

                var list = this.BaseRepository().FindList<Project_SubcontractPaymentApplyDetailsEntity>(sb.ToString()); ;
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractPaymentApply表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractPaymentApplyEntity GetProject_SubcontractPaymentApplyEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractPaymentApplyEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractPaymentApplyDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractPaymentApplyDetailsEntity GetProject_SubcontractPaymentApplyDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractPaymentApplyDetailsEntity>(t => t.ProjectSubcontractPaymentApplyId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {

                Project_SubcontractPaymentApplyEntity project_SubcontractPaymentApplyEntity = db.FindEntity<Project_SubcontractPaymentApplyEntity>(keyValue);
                if (project_SubcontractPaymentApplyEntity.IsClose == true)
                {
                    db.Commit();
                    return new Util.Common.OperateResultEntity() { Success = false, Message = "单据已关闭，不能进行反审核" };
                }

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();
                if (project_SubcontractPaymentApplyEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                project_SubcontractPaymentApplyEntity.AuditStatus = "4";
                db.Update<Project_SubcontractPaymentApplyEntity>(project_SubcontractPaymentApplyEntity);

                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(m => m.Name == "ProjectSubcontractHandleNode");
                if (config != null && config.Value == 0)
                {
                    var project_SubcontractPaymentApplyDetailsEntityList = db.FindList<Project_SubcontractPaymentApplyDetailsEntity>(m => m.ProjectSubcontractPaymentApplyId == keyValue);
                    foreach (var item in project_SubcontractPaymentApplyDetailsEntityList)
                    {
                        Project_SubcontractEntity project_SubcontractEntity = db.FindEntity<Project_SubcontractEntity>(item.ProjectSubcontractId);

                        if (!project_SubcontractEntity.AccountPayable.HasValue)
                            project_SubcontractEntity.AccountPayable = 0;
                        project_SubcontractEntity.AccountPayable -= project_SubcontractPaymentApplyEntity.ApplyPaymentAmount;

                        db.Update<Project_SubcontractEntity>(project_SubcontractEntity);


                        Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(m => m.ID == project_SubcontractEntity.SubcontractType);

                        if (base_ContractTypeEntity != null)
                        {
                            var subjectInfoID = new ProjectSubjectBll().GetEntity(base_ContractTypeEntity.BaseSubjectId, project_SubcontractEntity.ProjectID);
                            if (subjectInfoID != null)
                            //   resultList.Add(new OperateResultEntity() { Success = false, Message = "未找到对应的科目" });

                            //else
                            {
                                var subjectInfo = db.FindEntity<ProjectSubjectEntity>(subjectInfoID.ID);
                                subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) - (item.ApplyAmount ?? 0);

                                //decimal afterTax = item.PayAmount.Value - (item.PayAmount.Value * purchase_ContractEntity.TaxRage.Value / 100);
                                //subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;

                                db.Update<ProjectSubjectEntity>(subjectInfo);
                            }
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true };

            }
            catch (Exception ex)
            {
                db.Rollback();
                log.Error(ex);
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();
                Project_SubcontractPaymentApplyEntity project_SubcontractPaymentApplyEntity = db.FindEntity<Project_SubcontractPaymentApplyEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (project_SubcontractPaymentApplyEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                project_SubcontractPaymentApplyEntity.AuditStatus = "2";
                db.Update<Project_SubcontractPaymentApplyEntity>(project_SubcontractPaymentApplyEntity);

                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(m => m.Name == "ProjectSubcontractHandleNode");
                if (config != null && config.Value == 0)
                {
                    var project_SubcontractPaymentApplyDetailsEntityList = db.FindList<Project_SubcontractPaymentApplyDetailsEntity>(m => m.ProjectSubcontractPaymentApplyId == keyValue);
                    foreach (var item in project_SubcontractPaymentApplyDetailsEntityList)
                    {
                        Project_SubcontractEntity project_SubcontractEntity = db.FindEntity<Project_SubcontractEntity>(item.ProjectSubcontractId);

                        if (!project_SubcontractEntity.AccountPayable.HasValue)
                            project_SubcontractEntity.AccountPayable = 0;
                        project_SubcontractEntity.AccountPayable += project_SubcontractPaymentApplyEntity.ApplyPaymentAmount;

                        db.Update<Project_SubcontractEntity>(project_SubcontractEntity);

                        Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(m => m.ID == project_SubcontractEntity.SubcontractType);

                        if (base_ContractTypeEntity != null)
                        {
                            var subjectInfoID = new ProjectSubjectBll().GetEntity(base_ContractTypeEntity.BaseSubjectId, project_SubcontractEntity.ProjectID);

                            if (subjectInfoID != null)
                            //   resultList.Add(new OperateResultEntity() { Success = false, Message = "未找到对应的科目" });
                            //else
                            {
                                var subjectInfo = db.FindEntity<ProjectSubjectEntity>(subjectInfoID.ID);
                                subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) + (item.ApplyAmount ?? 0);

                                //decimal afterTax = item.PayAmount.Value - (item.PayAmount.Value * purchase_ContractEntity.TaxRage.Value / 100);
                                //subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;

                                db.Update<ProjectSubjectEntity>(subjectInfo);
                            }
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true };

            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_SubcontractPaymentApplyEntity = GetProject_SubcontractPaymentApplyEntity(keyValue);
                db.Delete<Project_SubcontractPaymentApplyEntity>(t => t.ID == keyValue);
                db.Delete<Project_SubcontractPaymentApplyDetailsEntity>(t => t.ProjectSubcontractPaymentApplyId == project_SubcontractPaymentApplyEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SubcontractPaymentApplyEntity entity, List<Project_SubcontractPaymentApplyDetailsEntity> project_SubcontractPaymentApplyDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var project_SubcontractPaymentApplyEntityTmp = GetProject_SubcontractPaymentApplyEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Project_SubcontractPaymentApplyDetailsUpdateList = project_SubcontractPaymentApplyDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_SubcontractPaymentApplyDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_SubcontractPaymentApplyDetailsInserList = project_SubcontractPaymentApplyDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_SubcontractPaymentApplyDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectSubcontractPaymentApplyId = project_SubcontractPaymentApplyEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_SubcontractPaymentApplyDetailsEntity item in project_SubcontractPaymentApplyDetailsList)
                    {
                        item.ProjectSubcontractPaymentApplyId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
