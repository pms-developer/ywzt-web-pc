﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 16:21
    /// 描 述：分包付款申请
    /// </summary>
    public class Project_SubcontractPaymentApplyBLL : Project_SubcontractPaymentApplyIBLL
    {
        private Project_SubcontractPaymentApplyService project_SubcontractPaymentApplyService = new Project_SubcontractPaymentApplyService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractPaymentApplyEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return project_SubcontractPaymentApplyService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractPaymentApplyDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractPaymentApplyDetailsEntity> GetProject_SubcontractPaymentApplyDetailsList(string keyValue)
        {
            try
            {
                return project_SubcontractPaymentApplyService.GetProject_SubcontractPaymentApplyDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractPaymentApply表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractPaymentApplyEntity GetProject_SubcontractPaymentApplyEntity(string keyValue)
        {
            try
            {
                return project_SubcontractPaymentApplyService.GetProject_SubcontractPaymentApplyEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractPaymentApplyDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractPaymentApplyDetailsEntity GetProject_SubcontractPaymentApplyDetailsEntity(string keyValue)
        {
            try
            {
                return project_SubcontractPaymentApplyService.GetProject_SubcontractPaymentApplyDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return project_SubcontractPaymentApplyService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return project_SubcontractPaymentApplyService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                project_SubcontractPaymentApplyService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SubcontractPaymentApplyEntity entity,List<Project_SubcontractPaymentApplyDetailsEntity> project_SubcontractPaymentApplyDetailsList,string deleteList,string type)
        {
            try
            {
                project_SubcontractPaymentApplyService.SaveEntity(keyValue, entity,project_SubcontractPaymentApplyDetailsList,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("CJ_BASE_0026");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
