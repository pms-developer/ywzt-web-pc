﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-05 14:19
    /// 描 述：保证金支付管理
    /// </summary>
    public class Base_CJ_BaoZhengJinZhiFuService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_BaoZhengJinZhiFuEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select  t.ID,
                t.ProjectManager,
                t.BianHao,
                bb.BianHao ShenQinDan,
                t.FuKuanDanWei,
                t.FuKuanFangShi,
                t.FuKuanShiJian,
                cc.f_realname JinBanRen,
                dd.name FuKuanZhangHu,
                t.ShuoMing,
                t.ProjectID,
                t.AuditStatus,
                t.Workflow_ID,t.CreationDate,bb.JinE  from Base_CJ_BaoZhengJinZhiFu t left join Base_CJ_BaoZhengJin bb on t.ShenQinDan = bb.id 
				left join base_user cc on t.JinBanRen = cc.f_userid left join Base_FundAccount dd on t.FuKuanZhangHu = dd.id WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }

                if (!queryParam["FuKuanZhangHu"].IsEmpty())
                {
                    dp.Add("FuKuanZhangHu", "%" + queryParam["FuKuanZhangHu"].ToString() + "%", DbType.String);
                    strSql.Append(" AND dd.name Like @FuKuanZhangHu ");
                }

                if (!queryParam["ZhaiYao"].IsEmpty())
                {
                    dp.Add("ZhaiYao", "%" + queryParam["ZhaiYao"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ZhaiYao Like @ZhaiYao ");
                }


                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND (t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Base_CJ_BaoZhengJinZhiFuEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_BaoZhengJinZhiFu表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_BaoZhengJinZhiFuEntity GetBase_CJ_BaoZhengJinZhiFuEntity(string keyValue)
        {
            try
            {
                string sql = "select a.*,fac.Bank+'('+fac.Name+')'as FuKuanZhangHuName from Base_CJ_BaoZhengJinZhiFu as a left join Base_FundAccount as fac on a.FuKuanZhangHu=fac.id where a.id='" + keyValue + "'";
                return this.BaseRepository().FindEntity<Base_CJ_BaoZhengJinZhiFuEntity>(sql, null);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_CJ_BaoZhengJinZhiFuEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_BaoZhengJinZhiFuEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Base_CJ_BaoZhengJinZhiFuEntity baoZhengJinZhiFuEntity = db.FindEntity<Base_CJ_BaoZhengJinZhiFuEntity>(keyValue);
                Base_CJ_BaoZhengJinEntity baoZhengJinEntity = db.FindEntity<Base_CJ_BaoZhengJinEntity>(baoZhengJinZhiFuEntity.ShenQinDan);
                baoZhengJinEntity.ZhiFuZhuangTai = "未付款";
                db.Update<Base_CJ_BaoZhengJinEntity>(baoZhengJinEntity);

                if (baoZhengJinZhiFuEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                baoZhengJinZhiFuEntity.AuditStatus = "4";
                db.Update<Base_CJ_BaoZhengJinZhiFuEntity>(baoZhengJinZhiFuEntity);


                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = baoZhengJinZhiFuEntity.FuKuanZhangHu,
                    AmountOccurrence = baoZhengJinEntity.JinE.HasValue ? baoZhengJinEntity.JinE.Value : 0,
                    OccurrenceType = OccurrenceEnum.Income,
                    RelevanceType = RelevanceEnum.Project,
                   // ReceiptType = ReceiptEnum.BondPay,
                    RelevanceId = baoZhengJinZhiFuEntity.ID,
                    ReceiptId = baoZhengJinZhiFuEntity.ID,
                    Abstract = $"{baoZhengJinZhiFuEntity.ShuoMing}[保证金退回]",
                    ProjectId = baoZhengJinZhiFuEntity.ProjectID
                }, db);

                Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == baoZhengJinEntity.Project_ID);

                if (base_FundAccountEntity != null && base_FundAccountEntity.ID != baoZhengJinZhiFuEntity.FuKuanZhangHu)
                {
                    //var result1 = 
                    new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = base_FundAccountEntity.ID,
                        AmountOccurrence = baoZhengJinEntity.JinE.HasValue ? baoZhengJinEntity.JinE.Value : 0,
                        OccurrenceType = OccurrenceEnum.Income,
                        RelevanceType = RelevanceEnum.Project,
                       // ReceiptType = ReceiptEnum.BondPay,
                        RelevanceId = baoZhengJinZhiFuEntity.ID,
                        ReceiptId = baoZhengJinZhiFuEntity.ID,
                        Abstract = $"{baoZhengJinZhiFuEntity.ShuoMing}[保证金退回]",
                        ProjectId = baoZhengJinZhiFuEntity.ProjectID
                    }, db, false);
                    //if (!result1.Success)
                    //    throw new Exception(result1.Message);
                }

                if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }

                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Base_CJ_BaoZhengJinZhiFuEntity baoZhengJinZhiFuEntity = db.FindEntity<Base_CJ_BaoZhengJinZhiFuEntity>(keyValue);
                Base_CJ_BaoZhengJinEntity baoZhengJinEntity = db.FindEntity<Base_CJ_BaoZhengJinEntity>(baoZhengJinZhiFuEntity.ShenQinDan);
                baoZhengJinEntity.ZhiFuZhuangTai = "已付款";
                db.Update<Base_CJ_BaoZhengJinEntity>(baoZhengJinEntity);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (baoZhengJinZhiFuEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                baoZhengJinZhiFuEntity.AuditStatus = "2";
                db.Update<Base_CJ_BaoZhengJinZhiFuEntity>(baoZhengJinZhiFuEntity);

                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = baoZhengJinZhiFuEntity.FuKuanZhangHu,
                    AmountOccurrence = baoZhengJinEntity.JinE.HasValue ? baoZhengJinEntity.JinE.Value : 0,
                    OccurrenceType = OccurrenceEnum.Outlay,
                    RelevanceType = RelevanceEnum.Project,
                   // ReceiptType = ReceiptEnum.BondPay,
                    RelevanceId = baoZhengJinZhiFuEntity.ID,
                    ReceiptId = baoZhengJinZhiFuEntity.ID,
                    Abstract = $"{baoZhengJinZhiFuEntity.ShuoMing}[保证金付款]",
                    ProjectId = baoZhengJinZhiFuEntity.ProjectID
                }, db);

                Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == baoZhengJinEntity.Project_ID);

                if (base_FundAccountEntity != null && base_FundAccountEntity.ID != baoZhengJinZhiFuEntity.FuKuanZhangHu)
                {
                    //var result1 = 
                    new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = base_FundAccountEntity.ID,
                        AmountOccurrence = baoZhengJinEntity.JinE.HasValue ? baoZhengJinEntity.JinE.Value : 0,
                        OccurrenceType = OccurrenceEnum.Outlay,
                        RelevanceType = RelevanceEnum.Project,
                       // ReceiptType = ReceiptEnum.BondPay,
                        RelevanceId = baoZhengJinZhiFuEntity.ID,
                        ReceiptId = baoZhengJinZhiFuEntity.ID,
                        Abstract = $"{baoZhengJinZhiFuEntity.ShuoMing}[保证金付款]",
                        ProjectId = baoZhengJinZhiFuEntity.ProjectID
                    }, db, false);
                    //if (!result1.Success)
                    //    throw new Exception(result1.Message);
                }

                if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }


                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        #endregion

    }
}
