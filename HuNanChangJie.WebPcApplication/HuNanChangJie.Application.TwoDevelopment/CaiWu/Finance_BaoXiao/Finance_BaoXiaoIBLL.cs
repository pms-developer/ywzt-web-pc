﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-07 10:21
    /// 描 述：报销单
    /// </summary>
    public interface Finance_BaoXiaoIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_BaoXiaoEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_BaoXiaoDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_BaoXiaoDetailsEntity> GetFinance_BaoXiaoDetailsList(string keyValue);
        /// <summary>
        /// 获取Finance_BaoXiao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_BaoXiaoEntity GetFinance_BaoXiaoEntity(string keyValue);
        /// <summary>
        /// 获取Finance_BaoXiaoDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_BaoXiaoDetailsEntity GetFinance_BaoXiaoDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_BaoXiaoEntity entity,List<Finance_BaoXiaoDetailsEntity> finance_BaoXiaoDetailsList,string deleteList,string type);
        #endregion

    }
}
