﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-07 10:21
    /// 描 述：报销单
    /// </summary>
    public class Finance_BaoXiaoService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_BaoXiaoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT t.*,b.f_realname BaoXiaoRen FROM Finance_BaoXiao t left join Base_User b on t.baoxiaouserid = b.f_userid WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID",queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID  ");
                }

                if (!queryParam["BaoXiaoType"].IsEmpty())
                {
                    dp.Add("BaoXiaoType",queryParam["BaoXiaoType"].ToString(), DbType.String);
                    strSql.Append(" AND t.BaoXiaoType = @BaoXiaoType ");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append($" AND t.AuditStatus in({auditStatus}) ");
                    }
                }

                if (!queryParam["BaoXiaoRen"].IsEmpty())
                {
                    dp.Add("BaoXiaoRen", "%" + queryParam["BaoXiaoRen"].ToString() + "%", DbType.String);
                    strSql.Append(" AND b.f_realname Like @BaoXiaoRen ");
                }
                if (!queryParam["Abstract"].IsEmpty())
                {
                    dp.Add("Abstract", "%" + queryParam["Abstract"].ToString() + "%", DbType.String);
                    strSql.Append(" AND Abstract Like @Abstract ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND (BaoXiaoDate >= @startTime AND BaoXiaoDate <= @endTime ) ");
                }


                var list=this.BaseRepository().FindList<Finance_BaoXiaoEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_BaoXiaoDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_BaoXiaoDetailsEntity> GetFinance_BaoXiaoDetailsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Finance_BaoXiaoDetailsEntity>(t=>t.FinanceBaoXiaoId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_BaoXiao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_BaoXiaoEntity GetFinance_BaoXiaoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_BaoXiaoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_BaoXiaoDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_BaoXiaoDetailsEntity GetFinance_BaoXiaoDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_BaoXiaoDetailsEntity>(t=>t.FinanceBaoXiaoId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_BaoXiaoEntity = GetFinance_BaoXiaoEntity(keyValue); 
                db.Delete<Finance_BaoXiaoEntity>(t=>t.ID == keyValue);
                db.Delete<Finance_BaoXiaoDetailsEntity>(t=>t.FinanceBaoXiaoId == finance_BaoXiaoEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_BaoXiaoEntity entity,List<Finance_BaoXiaoDetailsEntity> finance_BaoXiaoDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var finance_BaoXiaoEntityTmp = GetFinance_BaoXiaoEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Finance_BaoXiaoDetailsUpdateList= finance_BaoXiaoDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Finance_BaoXiaoDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_BaoXiaoDetailsInserList= finance_BaoXiaoDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Finance_BaoXiaoDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.FinanceBaoXiaoId = finance_BaoXiaoEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_BaoXiaoDetailsEntity item in finance_BaoXiaoDetailsList)
                    {
                        item.FinanceBaoXiaoId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
