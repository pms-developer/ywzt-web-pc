﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-07 10:21
    /// 描 述：报销单
    /// </summary>
    public class Finance_BaoXiaoBLL : Finance_BaoXiaoIBLL
    {
        private Finance_BaoXiaoService finance_BaoXiaoService = new Finance_BaoXiaoService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_BaoXiaoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return finance_BaoXiaoService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_BaoXiaoDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_BaoXiaoDetailsEntity> GetFinance_BaoXiaoDetailsList(string keyValue)
        {
            try
            {
                return finance_BaoXiaoService.GetFinance_BaoXiaoDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_BaoXiao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_BaoXiaoEntity GetFinance_BaoXiaoEntity(string keyValue)
        {
            try
            {
                return finance_BaoXiaoService.GetFinance_BaoXiaoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_BaoXiaoDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_BaoXiaoDetailsEntity GetFinance_BaoXiaoDetailsEntity(string keyValue)
        {
            try
            {
                return finance_BaoXiaoService.GetFinance_BaoXiaoDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                finance_BaoXiaoService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_BaoXiaoEntity entity,List<Finance_BaoXiaoDetailsEntity> finance_BaoXiaoDetailsList,string deleteList,string type)
        {
            try
            {
                finance_BaoXiaoService.SaveEntity(keyValue, entity,finance_BaoXiaoDetailsList,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0015");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
