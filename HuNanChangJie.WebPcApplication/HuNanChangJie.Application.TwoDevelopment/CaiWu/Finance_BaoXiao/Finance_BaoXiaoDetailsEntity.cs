﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-07 10:21
    /// 描 述：报销单
    /// </summary>
    public class Finance_BaoXiaoDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 报销申请单ID
        /// </summary>
        [Column("FINANCEBAOXIAOID")]
        public string FinanceBaoXiaoId { get; set; }
        /// <summary>
        /// 报销对象（项目/公司）
        /// </summary>
        [Column("BAOXIAOOBJECT")]
        public string BaoXiaoObject { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 科目ID
        /// </summary>
        [Column("SUBJECTID")]
        public string SubjectId { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        [Column("AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Amount { get; set; }
        /// <summary>
        /// 发票类型
        /// </summary>
        [Column("INVOICETYPE")]
        public string InvoiceType { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        [Column("TAXRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRate { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        [Column("TAXAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxAmount { get; set; }
        /// <summary>
        /// 不含税金额
        /// </summary>
        [Column("TAXEXCLUSIVE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxExclusive { get; set; }
        /// <summary>
        /// 特殊说明
        /// </summary>
        [Column("SPECICAL")]
        public string Specical { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        [Column("SUBJECTNAME")]
        public string SubjectName { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

