﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-21 15:38
    /// 描 述：还款单
    /// </summary>
    public interface Finance_RepaymentIBLL
    {
        #region  获取数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_RepaymentEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_RepaymentDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_RepaymentDetailsEntity> GetFinance_RepaymentDetailsList(string keyValue);
        /// <summary>
        /// 获取Finance_Repayment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_RepaymentEntity GetFinance_RepaymentEntity(string keyValue);
        /// <summary>
        /// 获取Finance_RepaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_RepaymentDetailsEntity GetFinance_RepaymentDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_RepaymentEntity entity,List<Finance_RepaymentDetailsEntity> finance_RepaymentDetailsList,string deleteList,string type);
        #endregion

    }
}
