﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.Organization;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-21 15:38
    /// 描 述：还款单
    /// </summary>
    public class Finance_RepaymentService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_RepaymentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.JingBanRen,
                t.RepaymentDate,
                t.Abstract,
                t.CompanyCode,
                t.CompanyName,
                t.AuditStatus,
                t.Workflow_ID,
                t.TotalAmount,
                t.CreationDate,
                t.ProjectID,
                t.TotalGuaAmount,
                t.ProjectName 
                ");
                strSql.Append("  FROM Finance_Repayment t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                var list=this.BaseRepository().FindList<Finance_RepaymentEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_RepaymentDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_RepaymentDetailsEntity> GetFinance_RepaymentDetailsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Finance_RepaymentDetailsEntity>("select a.*,fa.Bank + '(' + fa.Name + ')' as BankNames,isnull(b.Loan,0) Loan,isnull(b.GuaZhang,0) GuaZhang,b.F_RealName BorrowerName from Finance_RepaymentDetails a left join Base_User b on a.borrower = b.F_UserId left join Base_FundAccount as fa on a.Account=fa.id where a.Fk_RepaymentId='" + keyValue +"'");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_Repayment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_RepaymentEntity GetFinance_RepaymentEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_RepaymentEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_RepaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_RepaymentDetailsEntity GetFinance_RepaymentDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_RepaymentDetailsEntity>(t=>t.Fk_RepaymentId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_RepaymentEntity finance_RepaymentEntity = db.FindEntity<Finance_RepaymentEntity>(keyValue);
                if (finance_RepaymentEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_RepaymentEntity.AuditStatus = "4";
                db.Update<Finance_RepaymentEntity>(finance_RepaymentEntity);

                List<Finance_RepaymentDetailsEntity> listFinance_RepaymentDetailsEntity = db.FindList<Finance_RepaymentDetailsEntity>(m => m.Fk_RepaymentId == keyValue).ToList();
                List<string> messageList = new List<string>();

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                foreach (var item in listFinance_RepaymentDetailsEntity)
                {
                    UserEntity userEntity = db.FindEntity<UserEntity>(item.Borrower);
                    if (userEntity == null)
                    {
                        messageList.Add(item.ID +":用户不存在");
                        continue;
                    }
                    else
                    {
                        //if (item.Amount.HasValue && userEntity.Loan.HasValue && userEntity.Loan.Value < item.Amount.Value)
                        //{
                        //    messageList.Add(item.ID + ":用户未清借款小于当前还款");
                        //    continue;
                        //}

                        //if (item.GuaAmount.HasValue && userEntity.GuaZhang.HasValue && userEntity.GuaZhang.Value < item.GuaAmount.Value)
                        //{
                        //    messageList.Add(item.ID + ":用户未清挂账小于当前挂账");
                        //    continue;
                        //}

                        if (item.Amount.HasValue)
                        userEntity.Loan += item.Amount.Value;
                        if (item.GuaAmount.HasValue)
                            userEntity.GuaZhang += item.GuaAmount.Value;

                        db.Update<UserEntity>(userEntity);

                        var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                        {
                            FundAccountFlag = item.Account,
                            AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                            OccurrenceType = OccurrenceEnum.Outlay,
                            RelevanceType = RelevanceEnum.Other,
                            ReceiptType = ReceiptEnum.Other,
                            RelevanceId = item.Fk_RepaymentId,
                            ReceiptId = item.ID,
                            Abstract = $"{finance_RepaymentEntity.Abstract}[还款单取消审核]",
                            ProjectId = item.ProjectID
                        }, db);

                        if (!result.Success)
                            resultList.Add(result);


                        var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceRepayment");
                        if (configInfo != null)
                        {
                            var flag = configInfo.Value;

                            if (flag)
                            {
                                if (!string.IsNullOrEmpty(item.ProjectID))
                                {
                                    Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == item.ProjectID);

                                    if (base_FundAccountEntity != null && base_FundAccountEntity.ID != item.Account)
                                    {
                                        var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                        {
                                            FundAccountFlag = base_FundAccountEntity.ID,
                                            AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                                            OccurrenceType = OccurrenceEnum.Outlay,
                                            RelevanceType = RelevanceEnum.Other,
                                            ReceiptType = ReceiptEnum.Other,
                                            RelevanceId = item.Fk_RepaymentId,
                                            ReceiptId = item.ID,
                                            Abstract = $"{finance_RepaymentEntity.Abstract}[还款单取消审核]",
                                            ProjectId = item.ProjectID
                                        }, db, false);
                                        if (!result1.Success)
                                            resultList.Add(result);
                                    }
                                }
                            }
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true};
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_RepaymentEntity finance_RepaymentEntity = db.FindEntity<Finance_RepaymentEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_RepaymentEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                finance_RepaymentEntity.AuditStatus = "2";
                db.Update<Finance_RepaymentEntity>(finance_RepaymentEntity);

                List<Finance_RepaymentDetailsEntity> listFinance_RepaymentDetailsEntity = db.FindList<Finance_RepaymentDetailsEntity>(m => m.Fk_RepaymentId == keyValue).ToList();
                List<string> messageList = new List<string>();

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                foreach (var item in listFinance_RepaymentDetailsEntity)
                {
                    UserEntity userEntity = db.FindEntity<UserEntity>(item.Borrower);
                    if (userEntity == null)
                    {
                        messageList.Add(item.ID + ":用户不存在");
                        continue;
                    }
                    else
                    {
                        if (item.Amount.HasValue && userEntity.Loan.HasValue && userEntity.Loan.Value < item.Amount.Value)
                        {
                            messageList.Add(item.ID + ":用户未清借款小于当前还款");
                            continue;
                        }

                        if (item.GuaAmount.HasValue && userEntity.GuaZhang.HasValue && userEntity.GuaZhang.Value < item.GuaAmount.Value)
                        {
                            messageList.Add(item.ID + ":用户未清挂账小于当前挂账");
                            continue;
                        }

                        if (item.Amount.HasValue)
                            userEntity.Loan -= item.Amount.Value;
                        if (item.GuaAmount.HasValue)
                            userEntity.GuaZhang -= item.GuaAmount.Value;

                        db.Update<UserEntity>(userEntity);

                        var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                        {
                            FundAccountFlag = item.Account,
                            AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                            OccurrenceType = OccurrenceEnum.Income,
                            RelevanceType = RelevanceEnum.Other,
                            ReceiptType = ReceiptEnum.Other,
                            RelevanceId = item.Fk_RepaymentId,
                            ReceiptId = item.ID,
                            Abstract = $"{finance_RepaymentEntity.Abstract}[还款单审核]",
                            ProjectId = item.ProjectID
                        }, db);

                        if (!result.Success)
                            resultList.Add(result);
                        var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceRepayment");
                        if (configInfo != null)
                        {
                            var flag = configInfo.Value;

                            if (flag)
                            {
                                if (!string.IsNullOrEmpty(item.ProjectID))
                                {
                                    Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == item.ProjectID);

                                    if (base_FundAccountEntity != null && base_FundAccountEntity.ID != item.Account)
                                    {
                                        var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                        {
                                            FundAccountFlag = base_FundAccountEntity.ID,
                                            AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                                            OccurrenceType = OccurrenceEnum.Income,
                                            RelevanceType = RelevanceEnum.Other,
                                            ReceiptType = ReceiptEnum.Other,
                                            RelevanceId = item.Fk_RepaymentId,
                                            ReceiptId = item.ID,
                                            Abstract = $"{finance_RepaymentEntity.Abstract}[还款单审核]",
                                            ProjectId = item.ProjectID
                                        }, db, false);
                                        if (!result1.Success)
                                            resultList.Add(result);
                                    }
                                }
                            }
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_RepaymentEntity = GetFinance_RepaymentEntity(keyValue); 
                db.Delete<Finance_RepaymentEntity>(t=>t.ID == keyValue);
                db.Delete<Finance_RepaymentDetailsEntity>(t=>t.Fk_RepaymentId == finance_RepaymentEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_RepaymentEntity entity,List<Finance_RepaymentDetailsEntity> finance_RepaymentDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var finance_RepaymentEntityTmp = GetFinance_RepaymentEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Finance_RepaymentDetailsUpdateList= finance_RepaymentDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Finance_RepaymentDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_RepaymentDetailsInserList= finance_RepaymentDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Finance_RepaymentDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.Fk_RepaymentId = finance_RepaymentEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_RepaymentDetailsEntity item in finance_RepaymentDetailsList)
                    {
                        item.Fk_RepaymentId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
