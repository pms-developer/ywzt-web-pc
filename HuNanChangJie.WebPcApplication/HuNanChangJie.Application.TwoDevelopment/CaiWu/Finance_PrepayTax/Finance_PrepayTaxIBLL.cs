﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 20:20
    /// 描 述：开票预缴
    /// </summary>
    public interface Finance_PrepayTaxIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_PrepayTaxEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_PrepayDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_PrepayDetailsEntity> GetFinance_PrepayDetailsList(string keyValue);
        /// <summary>
        /// 获取Finance_PrepayTax表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_PrepayTaxEntity GetFinance_PrepayTaxEntity(string keyValue);
        /// <summary>
        /// 获取Finance_PrepayDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_PrepayDetailsEntity GetFinance_PrepayDetailsEntity(string keyValue);
        #endregion

        #region  提交数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_PrepayTaxEntity entity,List<Finance_PrepayDetailsEntity> finance_PrepayDetailsList,string deleteList,string type);
        #endregion

    }
}
