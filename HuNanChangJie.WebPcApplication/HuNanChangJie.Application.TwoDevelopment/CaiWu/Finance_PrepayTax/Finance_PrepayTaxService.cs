﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.Extend;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 20:20
    /// 描 述：开票预缴
    /// </summary>
    public class Finance_PrepayTaxService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_PrepayTaxEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,p.ProjectName,u.F_RealName,t.InvoiceAmount,
t.ProjectManager, fir.InvoiceCode,
                t.TicketNumber,
                t.TaxpayerNumber,
                t.TaxpayerName,
                t.CertificateDate,
                t.TaxAuthority,
                t.CertificateAbstract,
                t.ContractType,
                t.PrepayType,
                t.Fk_ContractId,
                t.Fk_InvoiceId,
                t.PrepayCompany,
                t.PrepayDate,
                t.PrepayAmount,
                t.Remark,
                t.AuditStatus,
                t.CreationDate,
                t.Workflow_ID 
                ");
                strSql.Append("  FROM Finance_PrepayTax t left join Base_CJ_Project as p on t.ProjectID=p.ID left join Base_User as u on t.ProjectManager=u.F_UserId ");
                strSql.Append("  left join  Finance_InvoiceRegister as fir on t.InvoiceCodeID=fir.ID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }

                if (!queryParam["ProjectName"].IsEmpty())
                {
                    dp.Add("ProjectName", "%" + queryParam["ProjectName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND p.ProjectName Like @ProjectName ");
                }

                if (!queryParam["InvoiceCode"].IsEmpty())
                {
                    dp.Add("InvoiceCode", "%" + queryParam["InvoiceCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND fir.InvoiceCode Like @InvoiceCode ");
                }
                if (!queryParam["Abstract"].IsEmpty())
                {
                    dp.Add("Remark", "%" + queryParam["Remark"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Remark Like @Abstract ");
                }
                if (!queryParam["F_RealName"].IsEmpty())
                {
                    dp.Add("F_RealName", "%" + queryParam["F_RealName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND u.F_RealName Like @F_RealName ");
                }
                if (!queryParam["SAmount"].IsEmpty())
                {
                    dp.Add("SAmount", queryParam["SAmount"].ToString(), DbType.String);
                    strSql.Append(" AND t.PrepayAmount>=@SAmount");
                }
                if (!queryParam["EAmount"].IsEmpty())
                {
                    dp.Add("EAmount", queryParam["EAmount"].ToString(), DbType.String);
                    strSql.Append(" AND t.PrepayAmount<=@EAmount");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID",queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }


                var list=this.BaseRepository().FindList<Finance_PrepayTaxEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_PrepayDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_PrepayDetailsEntity> GetFinance_PrepayDetailsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Finance_PrepayDetailsEntity>(t=>t.FinancePrepayTaxId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_PrepayTax表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_PrepayTaxEntity GetFinance_PrepayTaxEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_PrepayTaxEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_PrepayDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_PrepayDetailsEntity GetFinance_PrepayDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_PrepayDetailsEntity>(t=>t.FinancePrepayTaxId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_PrepayTaxEntity finance_PrepayTaxEntity = db.FindEntity<Finance_PrepayTaxEntity>(keyValue);
                if (finance_PrepayTaxEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_PrepayTaxEntity.AuditStatus = "4";
                db.Update<Finance_PrepayTaxEntity>(finance_PrepayTaxEntity);
                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "IsAutoDeductTaxes");
                if (config == null || config.Value == false)
                {
                    db.Commit();
                }
                else
                {
                    var fundId=new AuditExtend(db, finance_PrepayTaxEntity.ProjectID, ReceiptEnum.PrepayTax, finance_PrepayTaxEntity.ID, keyValue).UnAudit();
                    db.Commit();
                    new Base_FundAccountBLL().VerificationFundBalanceAndFreeze(fundId);

                }

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_PrepayTaxEntity finance_PrepayTaxEntity = db.FindEntity<Finance_PrepayTaxEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_PrepayTaxEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                finance_PrepayTaxEntity.AuditStatus = "2";
                db.Update<Finance_PrepayTaxEntity>(finance_PrepayTaxEntity);

                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "IsAutoDeductTaxes");
                if (config == null || config.Value == false)
                {
                    db.Commit();
                }
                else
                {
                    string fundId = new AuditExtend(db, finance_PrepayTaxEntity.ProjectID, ReceiptEnum.PrepayTax, finance_PrepayTaxEntity.ID, keyValue).Audit();
                    db.Commit();
                    new Base_FundAccountBLL().VerificationFundBalanceAndFreeze(fundId);
                }
                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_PrepayTaxEntity = GetFinance_PrepayTaxEntity(keyValue); 
                db.Delete<Finance_PrepayTaxEntity>(t=>t.ID == keyValue);
                db.Delete<Finance_PrepayDetailsEntity>(t=>t.FinancePrepayTaxId == finance_PrepayTaxEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_PrepayTaxEntity entity,List<Finance_PrepayDetailsEntity> finance_PrepayDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var finance_PrepayTaxEntityTmp = GetFinance_PrepayTaxEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Finance_PrepayDetailsUpdateList= finance_PrepayDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Finance_PrepayDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_PrepayDetailsInserList= finance_PrepayDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Finance_PrepayDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.FinancePrepayTaxId = finance_PrepayTaxEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_PrepayDetailsEntity item in finance_PrepayDetailsList)
                    {
                        item.FinancePrepayTaxId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
