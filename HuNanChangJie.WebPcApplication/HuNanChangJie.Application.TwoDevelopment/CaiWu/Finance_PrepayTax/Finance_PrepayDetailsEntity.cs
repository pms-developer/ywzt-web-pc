﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 20:20
    /// 描 述：开票预缴
    /// </summary>
    public class Finance_PrepayDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 预缴税种ID
        /// </summary>
        [Column("BASEPREPAYSUBJECTID")]
        public string BasePrepaySubjectId { get; set; }
        /// <summary>
        /// 税费预算ID
        /// </summary>
        [Column("FINANCEPREPAYTAXID")]
        public string FinancePrepayTaxId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 原凭证号
        /// </summary>
        [Column("VOUCHERNUMBER")]
        public string VoucherNumber { get; set; }
        /// <summary>
        /// 预缴税种
        /// </summary>
        [Column("TAXTYPE")]
        public string TaxType { get; set; }
        /// <summary>
        /// 品目名称
        /// </summary>
        [Column("ITEMNAME")]
        public string ItemName { get; set; }
        /// <summary>
        /// 税款所时期(开始)
        /// </summary>
        [Column("STARTDATE")]
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 税款所时期(结束)
        /// </summary>
        [Column("ENDDATE")]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 入(退)库日期
        /// </summary>
        [Column("OUTORINDATE")]
        public DateTime? OutOrInDate { get; set; }
        /// <summary>
        /// 实缴(退)金额
        /// </summary>
        [Column("PAIDORINAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? PaidOrInAmount { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

