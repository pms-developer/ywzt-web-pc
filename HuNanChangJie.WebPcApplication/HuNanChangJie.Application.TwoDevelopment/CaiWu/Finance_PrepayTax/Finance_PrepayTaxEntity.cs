﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 20:19
    /// 描 述：开票预缴
    /// </summary>
    public class Finance_PrepayTaxEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 票号
        /// </summary>
        [Column("TICKETNUMBER")]
        public string TicketNumber { get; set; }
        /// <summary>
        /// ContractType
        /// </summary>
        [Column("CONTRACTTYPE")]
        public string ContractType { get; set; }
        /// <summary>
        /// Fk_ContractId
        /// </summary>
        [Column("FK_CONTRACTID")]
        public string Fk_ContractId { get; set; }
        /// <summary>
        /// Fk_InvoiceId
        /// </summary>
        [Column("FK_INVOICEID")]
        public string Fk_InvoiceId { get; set; }
        /// <summary>
        /// InvoiceType
        /// </summary>
        [Column("INVOICETYPE")]
        public string InvoiceType { get; set; }
        /// <summary>
        /// 预缴类型
        /// </summary>
        [Column("PREPAYTYPE")]
        public string PrepayType { get; set; }
        /// <summary>
        /// 预缴公司
        /// </summary>
        [Column("PREPAYCOMPANY")]
        public string PrepayCompany { get; set; }
        /// <summary>
        /// 预缴日期
        /// </summary>
        [Column("PREPAYDATE")]
        public DateTime? PrepayDate { get; set; }
        /// <summary>
        /// 预缴总额
        /// </summary>
        [Column("PREPAYAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? PrepayAmount { get; set; }
        /// <summary>
        /// 税务机关
        /// </summary>
        [Column("TAXAUTHORITY")]
        public string TaxAuthority { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 完税金额总计
        /// </summary>
        [Column("CERTIFICATETOTALAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? CertificateTotalAmount { get; set; }
        /// <summary>
        /// 完税证明备注
        /// </summary>
        [Column("CERTIFICATEABSTRACT")]
        public string CertificateAbstract { get; set; }
        /// <summary>
        /// 纳税人名称
        /// </summary>
        [Column("TAXPAYERNAME")]
        public string TaxpayerName { get; set; }
        /// <summary>
        /// 纳税人识别号
        /// </summary>
        [Column("TAXPAYERNUMBER")]
        public string TaxpayerNumber { get; set; }
        /// <summary>
        /// 完税证明填发时间
        /// </summary>
        [Column("CERTIFICATEDATE")]
        public DateTime? CertificateDate { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }

        /// <summary>
        /// 开票登记ID
        /// </summary>
        [Column("INVOICECODEID")]
        public string InvoiceCodeID { get; set; }

        /// <summary>
        /// 项目经理
        /// </summary>
        [Column("PROJECTMANAGER")]
        public string ProjectManager { get; set; }


        /// <summary>
        /// 开票时间
        /// </summary>
        [Column("INVOICETIME")]
        public DateTime? InvoiceTime { get; set; }
        /// <summary>
        /// 开票金额
        /// </summary>
        [Column("INVOICEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? InvoiceAmount { get; set; }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        [NotMapped]
        public string InvoiceCode
        {
            get; set;
        }

        [NotMapped]
        public string F_RealName
        {
            get; set;
        }



        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

