﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 20:20
    /// 描 述：开票预缴
    /// </summary>
    public class Finance_PrepayTaxBLL : Finance_PrepayTaxIBLL
    {
        private Finance_PrepayTaxService finance_PrepayTaxService = new Finance_PrepayTaxService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_PrepayTaxEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return finance_PrepayTaxService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_PrepayDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_PrepayDetailsEntity> GetFinance_PrepayDetailsList(string keyValue)
        {
            try
            {
                return finance_PrepayTaxService.GetFinance_PrepayDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_PrepayTax表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_PrepayTaxEntity GetFinance_PrepayTaxEntity(string keyValue)
        {
            try
            {
                return finance_PrepayTaxService.GetFinance_PrepayTaxEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_PrepayDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_PrepayDetailsEntity GetFinance_PrepayDetailsEntity(string keyValue)
        {
            try
            {
                return finance_PrepayTaxService.GetFinance_PrepayDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return finance_PrepayTaxService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return finance_PrepayTaxService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                finance_PrepayTaxService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_PrepayTaxEntity entity,List<Finance_PrepayDetailsEntity> finance_PrepayDetailsList,string deleteList,string type)
        {
            try
            {
                finance_PrepayTaxService.SaveEntity(keyValue, entity,finance_PrepayDetailsList,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0025");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
