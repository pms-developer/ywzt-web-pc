﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-16 17:48
    /// 描 述：其它收款
    /// </summary>
    public class Finance_OtherProceedsService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_OtherProceedsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.PaymentCompany,
                t.ProceedsDate,
                t.FundType,
                t.ProjectID,
                t.PaymentUnit,
                t.ProjectManager,
                t.FundProperty,
                t.ProceedsAmount,
                t.OperatorId,
                t.PaymentType,
                t.BankAccount,
                t.TicketNumber,
                t.ProjectSubjectId,
                t.Remark,
                t.AuditStatus,
                t.CreationDate,
                t.Workflow_ID ,
				cus.FullName as CustomerName,
				fund.Name as FundName,
				sub.Name as SubjectName
                ");
                strSql.Append("  FROM Finance_OtherProceeds as t left join Base_Customer as cus on t.PaymentCompany=cus.id ");
                strSql.Append("  left join Base_FundAccount as fund on t.BankAccount=fund.id ");
                strSql.Append("  left join( select * from Base_Subject where [PaymentType] = 'in' and parentid !='0') as sub on t.ProjectSubjectId=sub.id ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                var list = this.BaseRepository().FindList<Finance_OtherProceedsEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_OtherProceeds表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_OtherProceedsEntity GetFinance_OtherProceedsEntity(string keyValue)
        {
            try
            {
                string sql = "select a.*,fac.Bank+'('+fac.Name+')' as BankAccountName from Finance_OtherProceeds as a left join Base_FundAccount as fac on a.BankAccount = fac.id where a.id='" + keyValue + "'";

                return this.BaseRepository().FindEntity<Finance_OtherProceedsEntity>(sql, null);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_OtherProceedsEntity finance_OtherProceedsEntity = db.FindEntity<Finance_OtherProceedsEntity>(keyValue);
                if (finance_OtherProceedsEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_OtherProceedsEntity.AuditStatus = "4";
                db.Update<Finance_OtherProceedsEntity>(finance_OtherProceedsEntity);

                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = !string.IsNullOrEmpty(finance_OtherProceedsEntity.BankAccount) ? finance_OtherProceedsEntity.BankAccount : finance_OtherProceedsEntity.ProjectID,
                    AmountOccurrence = finance_OtherProceedsEntity.ProceedsAmount.Value,
                    OccurrenceType = OccurrenceEnum.Outlay,
                    RelevanceType = RelevanceEnum.Other,
                    ReceiptType = ReceiptEnum.OtherProceeds,
                    RelevanceId = finance_OtherProceedsEntity.ID,
                    ReceiptId = finance_OtherProceedsEntity.ID,
                    Abstract = $"{finance_OtherProceedsEntity.Remark}[其它收款单取消审核]",
                    ProjectId = finance_OtherProceedsEntity.ProjectID,
                    ProjectSubjectId = finance_OtherProceedsEntity.ProjectSubjectId
                }, db);
                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceOtherProceeds");
                if (configInfo != null)
                {
                    var flag = configInfo.Value;

                    if (flag)
                    {
                        Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == finance_OtherProceedsEntity.ProjectID);
                        if (base_FundAccountEntity != null)
                        {
                            var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                            {
                                FundAccountFlag = base_FundAccountEntity.ID,
                                AmountOccurrence = finance_OtherProceedsEntity.ProceedsAmount.Value,
                                OccurrenceType = OccurrenceEnum.Outlay,
                                RelevanceType = RelevanceEnum.Other,
                                ReceiptType = ReceiptEnum.OtherProceeds,
                                RelevanceId = finance_OtherProceedsEntity.ID,
                                ReceiptId = finance_OtherProceedsEntity.ID,
                                Abstract = $"{finance_OtherProceedsEntity.Remark}[其它收款单取消审核]",
                                ProjectId = finance_OtherProceedsEntity.ProjectID,
                                ProjectSubjectId = finance_OtherProceedsEntity.ProjectSubjectId
                            }, db, false);
                        }
                    }
                }
                        if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }

                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_OtherProceedsEntity finance_OtherProceedsEntity = db.FindEntity<Finance_OtherProceedsEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_OtherProceedsEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                finance_OtherProceedsEntity.AuditStatus = "2";
                db.Update<Finance_OtherProceedsEntity>(finance_OtherProceedsEntity);

                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = !string.IsNullOrEmpty(finance_OtherProceedsEntity.BankAccount) ? finance_OtherProceedsEntity.BankAccount : finance_OtherProceedsEntity.ProjectID,
                    AmountOccurrence = finance_OtherProceedsEntity.ProceedsAmount.Value,
                    OccurrenceType = OccurrenceEnum.Income,
                    RelevanceType = RelevanceEnum.Other,
                    ReceiptType = ReceiptEnum.OtherProceeds,
                    RelevanceId = finance_OtherProceedsEntity.ID,
                    ReceiptId = finance_OtherProceedsEntity.ID,
                    Abstract = $"{finance_OtherProceedsEntity.Remark}[其它收款单审核]",
                    ProjectId = finance_OtherProceedsEntity.ProjectID,
                    ProjectSubjectId = finance_OtherProceedsEntity.ProjectSubjectId
                }, db);
                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceOtherProceeds");
                if (configInfo != null)
                {
                    var flag = configInfo.Value;

                    if (flag)
                    {
                        Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == finance_OtherProceedsEntity.ProjectID);
                        if (base_FundAccountEntity != null)
                        {
                            var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                            {
                                FundAccountFlag = base_FundAccountEntity.ID,
                                AmountOccurrence = finance_OtherProceedsEntity.ProceedsAmount.Value,
                                OccurrenceType = OccurrenceEnum.Income,
                                RelevanceType = RelevanceEnum.Other,
                                ReceiptType = ReceiptEnum.OtherProceeds,
                                RelevanceId = finance_OtherProceedsEntity.ID,
                                ReceiptId = finance_OtherProceedsEntity.ID,
                                Abstract = $"{finance_OtherProceedsEntity.Remark}[其它收款单审核]",
                                ProjectId = finance_OtherProceedsEntity.ProjectID,
                                ProjectSubjectId = finance_OtherProceedsEntity.ProjectSubjectId
                            }, db, false);
                        }
                    }
                }
                if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }

                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Finance_OtherProceedsEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_OtherProceedsEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
