﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-12-14 11:43
    /// 描 述：税费核算
    /// </summary>
    public class AccountingBLL : AccountingIBLL
    {
        private AccountingService accountingService = new AccountingService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<AccountingEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return accountingService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

      


        /// <summary>
        /// 获取Accounting表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public AccountingEntity GetAccountingEntity(string keyValue)
        {
            try
            {
                return accountingService.GetAccountingEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }




        /// <summary>
        /// 获取Accounting表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public AccountingTnvoiceDetailEntity GetAccountingTnvoiceDetailEntity(string keyValue)
        {
            try
            {
                return accountingService.GetAccountingTnvoiceDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public IEnumerable<AccountingTnvoiceDetailEntity> GetAccountingTnvoiceDetailList(string keyValue)
        {
            try
            {
                return accountingService.GetAccountingTnvoiceDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Accounting表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public AccountingTaxDetailEntity GetAccountingTaxDetailEntity(string keyValue)
        {
            try
            {
                return accountingService.GetAccountingTaxDetailEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public IEnumerable<AccountingTaxDetailEntity> GetAccountingTaxDetailList(string keyValue)
        {
            try
            {
                return accountingService.GetAccountingTaxDetailList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        #endregion

        #region  提交数据

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return accountingService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return accountingService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                accountingService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, AccountingEntity entity, List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList, List<AccountingTaxDetailEntity> accountingTaxDetailList, string deleteList,string type)
        {
            try
            {
                accountingService.SaveEntity(keyValue, entity, accountingTnvoiceDetailList, accountingTaxDetailList,deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public Util.Common.OperateResultEntity SaveEntityPiao(string keyValue, AccountingEntity entity, List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList, List<AccountingTaxDetailEntity> accountingTaxDetailList, string deleteList, string type)
        {
            try
            {
               return  accountingService.SaveEntityPiao(keyValue, entity, accountingTnvoiceDetailList, accountingTaxDetailList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
