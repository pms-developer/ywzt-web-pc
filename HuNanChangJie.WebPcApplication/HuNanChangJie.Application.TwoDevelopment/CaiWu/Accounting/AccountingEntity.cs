﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;


namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-12-14 11:43
    /// 描 述：税费核算
    /// </summary>
    public class AccountingEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// 项目经理
        /// </summary>
        [Column("PROJECTMANAGER")]
        public string ProjectManager { get; set; }
        /// <summary>
        /// 开票单位
        /// </summary>
        [Column("MAKEINVOICECOMPANYID")]
        public string MakeInvoiceCompanyId { get; set; }
        /// <summary>
        /// 核算时间
        /// </summary>
        [Column("CALCULATIONTIME")]
        public DateTime? Calculationtime { get; set; }
        /// <summary>
        /// 开票登记id
        /// </summary>
        [Column("FINANCE_INVOICEREGISTERID")]
        public string Finance_InvoiceRegisterId { get; set; }

        /// <summary>
        /// 开票预缴id
        /// </summary>
        [Column("FINANCE_PREPAYTAXID")]
        public string Finance_PrepayTaxID { get; set; }



      

        /// <summary>
        /// 开票金额
        /// </summary>
        [Column("INVOICEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? InvoiceAmount { get; set; }
        /// <summary>
        /// 不含税收入
        /// </summary>
        [Column("TAXINCOMENOTINCLUDED")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxIncomeNotIncluded { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        [Column("RATE")]
        [DecimalPrecision(18, 4)]
        public decimal? Rate { get; set; }
        /// <summary>
        /// 销售税金
        /// </summary>
        [Column("SALESTAX")]
        [DecimalPrecision(18, 4)]
        public decimal? SalesTax { get; set; }
        /// <summary>
        /// 不含税税率
        /// </summary>
        [Column("TAXRATEEXCLUSIVE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRateExclusive { get; set; }
        /// <summary>
        /// 应预缴
        /// </summary>
        [Column("PREPAY")]
        [DecimalPrecision(18, 4)]
        public decimal? Prepay { get; set; }
        /// <summary>
        /// 需抵扣增值税
        /// </summary>
        [Column("VATDEDUCTIBLE")]
        [DecimalPrecision(18, 4)]
        public decimal? VatDeductible { get; set; }
        /// <summary>
        /// 含税金额
        /// </summary>
        [Column("TAXAMOUNTSUM")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxAmountSum { get; set; }

        /// <summary>
        /// 配票金额
        /// </summary>
        [Column("WITHTICKETAMOUNTSUM")]
        [DecimalPrecision(18, 4)]
        public decimal? WithTicketAmountSum { get; set; }
        /// <summary>
        /// 可抵税金
        /// </summary>
        [Column("ADJUSTABLETAXSUM")]
        [DecimalPrecision(18, 4)]
        public decimal? AdjustableTaxSum { get; set; }

        /// <summary>
        /// 综合税率
        /// </summary>
        [Column("RATETOTAL")]
        [DecimalPrecision(18, 4)]
        public decimal? RateTotal { get; set; }
        /// <summary>
        /// 实缴税费合计
        /// </summary>
        [Column("ACCOUNTINGTAXDETAILSUM")]
        [DecimalPrecision(18, 4)]
        public decimal? AccountingTaxDetailSum { get; set; }

        /// <summary>
        /// 预缴税费合计
        /// </summary>
        [Column("PREPAYTAXSUM")]
        [DecimalPrecision(18, 4)]
        public decimal? PrepayTaxSum { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }

        /// <summary>
        /// 公司账户
        /// </summary>
        [Column("COMPANYACCOUNT")]
        public string CompanyAccount { get; set; }

        /// <summary>
        /// 项目账户
        /// </summary>
        [Column("PROJECTACCOUNT")]
        public string ProjectAccount { get; set; }

        /// <summary>
        /// 项目经理名称
        /// </summary>
        [Column("PROJECTMANAGERNAME")]
        public string ProjectManagerName { get; set; }

        /// <summary>
        /// 不含税税率文本
        /// </summary>
        [Column("TAXRATEEXCLUSIVETEXT")]
        public string TaxRateExclusiveText { get; set; }


        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 项目客户名称
        /// </summary>
        [Column("CUSTOMERNAME")]
        public string CustomerName { get; set; }

        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }

        [NotMapped]
        public string CompanyAccountName { get; set; }

        [NotMapped]
        public string ProjectAccountName { get; set; }


        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段

        [NotMapped]
        public string RateTotalDisplay
        {
            get
            {
                if (RateTotal == null) return null;

                return ((RateTotal ?? 0) * 100).ToString("0.00")+"%";
            }
        }

        [NotMapped]
        public string InvoiceCode { get; set; }
        [NotMapped]
        public DateTime? MakeInvoiceDate { get; set; }

        #endregion
    }
}

