﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-12-14 11:43
    /// 描 述：税费核算
    /// </summary>
    public interface AccountingIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<AccountingEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Accounting表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        AccountingEntity GetAccountingEntity(string keyValue);

        AccountingTnvoiceDetailEntity GetAccountingTnvoiceDetailEntity(string keyValue);

        AccountingTaxDetailEntity GetAccountingTaxDetailEntity(string keyValue);

        IEnumerable<AccountingTnvoiceDetailEntity> GetAccountingTnvoiceDetailList(string keyValue);

        IEnumerable<AccountingTaxDetailEntity> GetAccountingTaxDetailList(string keyValue);
        #endregion

        #region  提交数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, AccountingEntity entity, List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList, List<AccountingTaxDetailEntity> accountingTaxDetailList, string deleteList, string type);
        OperateResultEntity SaveEntityPiao(string keyValue, AccountingEntity entity, List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList, List<AccountingTaxDetailEntity> accountingTaxDetailList, string deleteList, string type);

        #endregion

    }
}
