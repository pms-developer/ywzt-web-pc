﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using HuNanChangJie.Util.Common;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangeJie.Application.Project.BaseInfo.Model;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-12-14 11:43
    /// 描 述：税费核算
    /// </summary>
    public class AccountingService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<AccountingEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@" select t.ID,
                t.AuditStatus,t.CustomerName,
                c.projectname ProjectID,
                t.ProjectManagerName,
                t.Finance_InvoiceRegisterId,
                t.MakeInvoiceCompanyId,
                t.InvoiceAmount,
                t.TaxIncomeNotIncluded,
                t.Rate,
                t.SalesTax,
                t.Prepay,
                t.VatDeductible,
				t.CreationDate,
				t.RateTotal,
				t.AccountingTaxDetailSum,
				t.PrepayTaxSum,
				t.CreationName,
				fir.MakeInvoiceDate,
				fir.InvoiceCode FROM Accounting t join Finance_InvoiceRegister as fir on t.Finance_InvoiceRegisterId = fir.ID left join base_cj_project c on t.ProjectID = c.id WHERE 1=1 ");

                var queryParam = queryJson.ToJObject();

                // 虚拟参数  
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID=@ProjectID ");
                }
                if (!queryParam["ProjectName"].IsEmpty())
                {
                    dp.Add("ProjectName", "%" + queryParam["ProjectName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND c.projectname Like @ProjectName ");
                }
                if (!queryParam["ProjectManagerName"].IsEmpty())
                {
                    dp.Add("ProjectManagerName", "%" + queryParam["ProjectManagerName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ProjectManagerName Like @ProjectManagerName ");
                }

                if (!queryParam["InvoiceCode"].IsEmpty())
                {
                    dp.Add("InvoiceCode", "%" + queryParam["InvoiceCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND fir.InvoiceCode Like @InvoiceCode ");
                }

                if (!queryParam["InvoiceAmount"].IsEmpty())
                {
                    dp.Add("InvoiceAmount", "%" + queryParam["InvoiceAmount"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.InvoiceAmount Like @InvoiceAmount ");
                }

                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }

                var list = this.BaseRepository().FindList<AccountingEntity>(strSql.ToString(), dp, pagination);

                var query = from item in list orderby item.CreationDate descending select item;

                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Accounting表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public AccountingEntity GetAccountingEntity(string keyValue)
        {
            try
            {
                string sql = "select a.*,fac.Bank+'('+fac.Name+')' as CompanyAccountName,fap.Bank+'('+fap.Name+')' as ProjectAccountName from Accounting as a left join Base_FundAccount as fac on a.CompanyAccount=fac.id left join Base_FundAccount as fap on a.ProjectAccount=fap.id where a.id='" + keyValue + "'";

                return this.BaseRepository().FindEntity<AccountingEntity>(sql, null);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public AccountingTnvoiceDetailEntity GetAccountingTnvoiceDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<AccountingTnvoiceDetailEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_BaoXiaoDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<AccountingTnvoiceDetailEntity> GetAccountingTnvoiceDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<AccountingTnvoiceDetailEntity>(t => t.AccountingId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public AccountingTaxDetailEntity GetAccountingTaxDetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<AccountingTaxDetailEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Finance_BaoXiaoDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<AccountingTaxDetailEntity> GetAccountingTaxDetailList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<AccountingTaxDetailEntity>(t => t.AccountingId == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据


        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<string> messageList = new List<string>();

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();

                AccountingEntity accountingEntity = db.FindEntity<AccountingEntity>(keyValue);

                if (accountingEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }

                accountingEntity.AuditStatus = "4";
                db.Update<AccountingEntity>(accountingEntity);

                List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList = db.FindList<AccountingTnvoiceDetailEntity>(m => m.AccountingId == keyValue).ToList();
                foreach (var item in accountingTnvoiceDetailList)
                {
                    switch (item.InvoiceType)
                    {
                        case "1"://采购发票
                            Purchase_InvoiceEntity purchase_InvoiceEntity = db.FindEntity<Purchase_InvoiceEntity>(item.Invoiceid);
                            if (purchase_InvoiceEntity != null)
                            {
                                if (purchase_InvoiceEntity.AccountingInvoiceAmount == null)
                                {
                                    purchase_InvoiceEntity.AccountingInvoiceAmount = 0;
                                }
                                else
                                {
                                    purchase_InvoiceEntity.AccountingInvoiceAmount -= item.TaxAmount;
                                }
                                db.Update<Purchase_InvoiceEntity>(purchase_InvoiceEntity);
                            }
                            break;
                        case "2":

                            Finance_SubcontractInvoiceEntity finance_SubcontractInvoiceEntity = db.FindEntity<Finance_SubcontractInvoiceEntity>(item.Invoiceid);
                            if (finance_SubcontractInvoiceEntity != null)
                            {
                                if (finance_SubcontractInvoiceEntity.AccountingInvoiceAmount == null)
                                {
                                    finance_SubcontractInvoiceEntity.AccountingInvoiceAmount = 0;
                                }
                                else
                                {
                                    finance_SubcontractInvoiceEntity.AccountingInvoiceAmount -= item.TaxAmount;
                                }

                                db.Update<Finance_SubcontractInvoiceEntity>(finance_SubcontractInvoiceEntity);
                            }

                            break;
                        default:
                            break;
                    }
                }

                Base_FundAccountEntity base_CompanyFundAccountEntity = db.FindEntity<Base_FundAccountEntity>(accountingEntity.CompanyAccount);

                Base_FundAccountEntity base_ProjectFundAccountEntity = db.FindEntity<Base_FundAccountEntity>(accountingEntity.ProjectAccount);
                if (base_ProjectFundAccountEntity != null )
                {
                    var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = base_ProjectFundAccountEntity.ID,
                        AmountOccurrence = accountingEntity.AccountingTaxDetailSum.HasValue ? accountingEntity.AccountingTaxDetailSum.Value : 0,
                        OccurrenceType = OccurrenceEnum.Income,
                        RelevanceType = RelevanceEnum.Other,
                        ReceiptType = ReceiptEnum.Other,
                        RelevanceId = accountingEntity.ID,
                        ReceiptId = accountingEntity.ID,
                        Abstract = "税费核销取消审核",
                        ProjectId = accountingEntity.ProjectID
                    }, db, false);
                    if (!result.Success)
                        resultList.Add(result);
                }
                    if (base_CompanyFundAccountEntity != null)
                    {
                        var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = base_CompanyFundAccountEntity.ID,
                        AmountOccurrence = accountingEntity.AccountingTaxDetailSum.HasValue ? accountingEntity.AccountingTaxDetailSum.Value : 0,
                        OccurrenceType = OccurrenceEnum.Outlay,
                        RelevanceType = RelevanceEnum.Other,
                        ReceiptType = ReceiptEnum.Other,
                        RelevanceId = accountingEntity.ID,
                        ReceiptId = accountingEntity.ID,
                        Abstract = "税费核销取消审核",
                        ProjectId = accountingEntity.ProjectID
                    }, db, false);
                    if (!result1.Success)
                        resultList.Add(result1);
                }


                //List<AccountingTaxDetailEntity> accountingTaxDetailEntityList = db.FindList<AccountingTaxDetailEntity>(m => m.AccountingId == keyValue).ToList();
                //foreach (var item in accountingTaxDetailEntityList)
                //{

                //}
                //List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList = db.FindList<AccountingTnvoiceDetailEntity>(m => m.AccountingId == keyValue).ToList();
                //foreach (var item in accountingTnvoiceDetailList)
                //{

                //}


                List<AccountingTaxDetailEntity> accountingTaxDetailList = db.FindList<AccountingTaxDetailEntity>(m => m.AccountingId == keyValue).ToList();

                foreach (var item in accountingTaxDetailList)
                {
                    string subject_code = item.SubjectId;
                    SubjectEntity subjectEntity = db.FindEntity<SubjectEntity>(m => m.Code == subject_code);
                    if (subjectEntity != null)
                    {
                        string projectId = accountingEntity.ProjectID;

                        var subjectInfoID = new ProjectSubjectBll().GetEntity(subjectEntity.ID, projectId);
                        if (subjectInfoID != null)
                        {
                            var subjectInfo = db.FindEntity<ProjectSubjectEntity>(subjectInfoID.ID);
                            subjectInfo.ActuallyOccurred = (subjectInfo.ActuallyOccurred ?? 0) - (item.ActualTax ?? 0);
                            db.Update<ProjectSubjectEntity>(subjectInfo);
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }
                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<string> messageList = new List<string>();

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();

                AccountingEntity accountingEntity = db.FindEntity<AccountingEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (accountingEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}

                accountingEntity.AuditStatus = "2";
                db.Update<AccountingEntity>(accountingEntity);

                List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList = db.FindList<AccountingTnvoiceDetailEntity>(m => m.AccountingId == keyValue).ToList();
                foreach (var item in accountingTnvoiceDetailList)
                {
                    switch (item.InvoiceType)
                    {
                        case "1"://采购发票
                            Purchase_InvoiceEntity purchase_InvoiceEntity = db.FindEntity<Purchase_InvoiceEntity>(item.Invoiceid);
                            if (purchase_InvoiceEntity != null)
                            {
                                if (purchase_InvoiceEntity.AccountingInvoiceAmount == null)
                                {
                                    purchase_InvoiceEntity.AccountingInvoiceAmount = 0;
                                }
                                purchase_InvoiceEntity.AccountingInvoiceAmount += item.TaxAmount;

                                db.Update<Purchase_InvoiceEntity>(purchase_InvoiceEntity);

                            }
                            break;
                        case "2":

                            Finance_SubcontractInvoiceEntity finance_SubcontractInvoiceEntity = db.FindEntity<Finance_SubcontractInvoiceEntity>(item.Invoiceid);
                            if (finance_SubcontractInvoiceEntity != null)
                            {
                                if (finance_SubcontractInvoiceEntity.AccountingInvoiceAmount == null)
                                {
                                    finance_SubcontractInvoiceEntity.AccountingInvoiceAmount = 0;
                                }
                                finance_SubcontractInvoiceEntity.AccountingInvoiceAmount += item.TaxAmount;

                                db.Update<Finance_SubcontractInvoiceEntity>(finance_SubcontractInvoiceEntity);
                            }

                            break;
                        default:
                            break;
                    }
                }

                Base_FundAccountEntity base_CompanyFundAccountEntity = db.FindEntity<Base_FundAccountEntity>(accountingEntity.CompanyAccount);
                Base_FundAccountEntity base_ProjectFundAccountEntity = db.FindEntity<Base_FundAccountEntity>(accountingEntity.ProjectAccount);
                if (base_ProjectFundAccountEntity != null )
                {
                    var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = base_ProjectFundAccountEntity.ID,
                        AmountOccurrence = accountingEntity.AccountingTaxDetailSum.HasValue ? accountingEntity.AccountingTaxDetailSum.Value : 0,
                        OccurrenceType = OccurrenceEnum.Outlay,
                        RelevanceType = RelevanceEnum.Other,
                        ReceiptType = ReceiptEnum.Other,
                        RelevanceId = accountingEntity.ID,
                        ReceiptId = accountingEntity.ID,
                        Abstract = "税费核销",
                        ProjectId = accountingEntity.ProjectID
                    }, db, false);
                    if (!result.Success)
                        resultList.Add(result);
                }
                if (base_CompanyFundAccountEntity != null)
                {
                    var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = base_CompanyFundAccountEntity.ID,
                        AmountOccurrence = accountingEntity.AccountingTaxDetailSum.HasValue ? accountingEntity.AccountingTaxDetailSum.Value : 0,
                        OccurrenceType = OccurrenceEnum.Income,
                        RelevanceType = RelevanceEnum.Other,
                        ReceiptType = ReceiptEnum.Other,
                        RelevanceId = accountingEntity.ID,
                        ReceiptId = accountingEntity.ID,
                        Abstract = "税费核销",
                        ProjectId = accountingEntity.ProjectID
                    }, db, false);
                    if (!result1.Success)
                        resultList.Add(result1);
                }
                List<AccountingTaxDetailEntity> accountingTaxDetailList = db.FindList<AccountingTaxDetailEntity>(m => m.AccountingId == keyValue).ToList();

                foreach (var item in accountingTaxDetailList)
                {
                    string subject_code = item.SubjectId;
                    SubjectEntity subjectEntity = db.FindEntity<SubjectEntity>(m => m.Code == subject_code);
                    if (subjectEntity != null)
                    {
                        string projectId = accountingEntity.ProjectID;

                        var subjectInfoID = new ProjectSubjectBll().GetEntity(subjectEntity.ID, projectId);
                        if (subjectInfoID != null)
                        {
                            var subjectInfo = db.FindEntity<ProjectSubjectEntity>(subjectInfoID.ID);
                            subjectInfo.ActuallyOccurred = (subjectInfo.ActuallyOccurred ?? 0) + (item.ActualTax ?? 0);
                            db.Update<ProjectSubjectEntity>(subjectInfo);
                        }
                    }
                }


                //List<AccountingTaxDetailEntity> accountingTaxDetailEntityList = db.FindList<AccountingTaxDetailEntity>(m => m.AccountingId == keyValue).ToList();
                //foreach (var item in accountingTaxDetailEntityList)
                //{

                //}
                //List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList = db.FindList<AccountingTnvoiceDetailEntity>(m => m.AccountingId == keyValue).ToList();
                //foreach (var item in accountingTnvoiceDetailList)
                //{

                //}

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }
                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }



        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_AccountingEntity = GetAccountingEntity(keyValue);
                db.Delete<AccountingEntity>(t => t.ID == keyValue);
                db.Delete<AccountingTnvoiceDetailEntity>(t => t.AccountingId == finance_AccountingEntity.ID);
                db.Delete<AccountingTaxDetailEntity>(t => t.AccountingId == finance_AccountingEntity.ID);
                db.Commit();

            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity SaveEntityPiao(string keyValue, AccountingEntity entity, List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList, List<AccountingTaxDetailEntity> accountingTaxDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                var finance_AccountingEntityTmp = GetAccountingEntity(keyValue);

                if (finance_AccountingEntityTmp == null)
                    throw new Exception("单据不存在");
                if (finance_AccountingEntityTmp.AuditStatus != "2")
                    throw new Exception("单据状态不支持此操作,仅支持审核通过单据");

                string finance_AccountingId = finance_AccountingEntityTmp.ID;
                List<string> messageList = new List<string>();

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();

                decimal difference = (finance_AccountingEntityTmp.AccountingTaxDetailSum ?? 0.00M) - (entity.AccountingTaxDetailSum ?? 0.00M);
                Base_FundAccountEntity base_CompanyFundAccountEntity = db.FindEntity<Base_FundAccountEntity>(entity.CompanyAccount);
                Base_FundAccountEntity base_ProjectFundAccountEntity = db.FindEntity<Base_FundAccountEntity>(finance_AccountingEntityTmp.ProjectAccount);
                if (difference < 0)
                {
                    if (base_ProjectFundAccountEntity != null )
                    {
                        var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                        {
                            FundAccountFlag = base_ProjectFundAccountEntity.ID,
                            AmountOccurrence = difference,
                            OccurrenceType = OccurrenceEnum.Outlay,
                            RelevanceType = RelevanceEnum.Other,
                            ReceiptType = ReceiptEnum.Other,
                            RelevanceId = finance_AccountingEntityTmp.ID,
                            ReceiptId = finance_AccountingEntityTmp.ID,
                            Abstract = "税费核销补缴",
                            ProjectId = finance_AccountingEntityTmp.ProjectID
                        }, db);
                        if (!result.Success)
                            resultList.Add(result);
                    }
                    if (base_CompanyFundAccountEntity != null)
                    {
                        var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                        {
                            FundAccountFlag = base_CompanyFundAccountEntity.ID,
                            AmountOccurrence = difference,
                            OccurrenceType = OccurrenceEnum.Income,
                            RelevanceType = RelevanceEnum.Other,
                            ReceiptType = ReceiptEnum.Other,
                            RelevanceId = finance_AccountingEntityTmp.ID,
                            ReceiptId = finance_AccountingEntityTmp.ID,
                            Abstract = "税费核销补缴",
                            ProjectId = finance_AccountingEntityTmp.ProjectID
                        }, db);
                        if (!result1.Success)
                            resultList.Add(result1);
                    }
                }
                else if (difference > 0)
                {
                    if (base_ProjectFundAccountEntity != null )
                    {
                        var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                        {
                            FundAccountFlag = base_ProjectFundAccountEntity.ID,
                            AmountOccurrence = difference,
                            OccurrenceType = OccurrenceEnum.Income,
                            RelevanceType = RelevanceEnum.Other,
                            ReceiptType = ReceiptEnum.Other,
                            RelevanceId = finance_AccountingEntityTmp.ID,
                            ReceiptId = finance_AccountingEntityTmp.ID,
                            Abstract = "税费核销退税",
                            ProjectId = finance_AccountingEntityTmp.ProjectID
                        }, db);
                        if (!result.Success)
                            resultList.Add(result);
                    }

                    if (base_CompanyFundAccountEntity != null)
                    {
                        var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                        {
                            FundAccountFlag = base_CompanyFundAccountEntity.ID,
                            AmountOccurrence = difference,
                            OccurrenceType = OccurrenceEnum.Outlay,
                            RelevanceType = RelevanceEnum.Other,
                            ReceiptType = ReceiptEnum.Other,
                            RelevanceId = finance_AccountingEntityTmp.ID,
                            ReceiptId = finance_AccountingEntityTmp.ID,
                            Abstract = "税费核销退税",
                            ProjectId = finance_AccountingEntityTmp.ProjectID
                        }, db);
                        if (!result1.Success)
                            resultList.Add(result1);
                    }
                }
                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                entity.Modify(keyValue);
                db.Update(entity);
                var delJson = deleteList.ToObject<List<JObject>>();
                foreach (var del in delJson)
                {
                    var idList = del["idList"].ToString();
                    var sql = "";
                    if (idList.Contains(","))
                    {
                        var ids = idList.Split(',');
                        foreach (var id in ids)
                        {
                            sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(idList))
                        {
                            sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                        }
                    }
                }

                //没有生成代码 
                var accountingTnvoiceDetailUpdateList = accountingTnvoiceDetailList.FindAll(i => i.EditType == EditType.Update);
                foreach (var item in accountingTnvoiceDetailUpdateList)
                {
                    db.Update(item);
                }
                var accountingTnvoiceDetailInserList = accountingTnvoiceDetailList.FindAll(i => i.EditType == EditType.Add);
                foreach (var item in accountingTnvoiceDetailInserList)
                {
                    item.Create(item.ID);
                    item.AccountingId = finance_AccountingId;
                    item.CreationDate = DateTime.Now;
                    db.Insert(item);
                }

                //没有生成代码 
                var accountingTaxDetailListUpdateList = accountingTaxDetailList.FindAll(i => i.EditType == EditType.Update);
                foreach (var item in accountingTaxDetailListUpdateList)
                {
                    db.Update(item);
                }
                var accountingTaxDetailListInserList = accountingTaxDetailList.FindAll(i => i.EditType == EditType.Add);
                foreach (var item in accountingTaxDetailListInserList)
                {
                    item.Create(item.ID);
                    item.AccountingId = finance_AccountingId;
                    item.CreationDate = DateTime.Now;
                    db.Insert(item);
                }

                //没有生成代码1 

                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, AccountingEntity entity, List<AccountingTnvoiceDetailEntity> accountingTnvoiceDetailList, List<AccountingTaxDetailEntity> accountingTaxDetailList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var finance_AccountingEntityTmp = GetAccountingEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var accountingTnvoiceDetailUpdateList = accountingTnvoiceDetailList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in accountingTnvoiceDetailUpdateList)
                    {
                        db.Update(item);
                    }

                    var accountingTnvoiceDetailInserList = accountingTnvoiceDetailList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in accountingTnvoiceDetailInserList)
                    {
                        item.Create(item.ID);
                        item.AccountingId = finance_AccountingEntityTmp.ID;
                        item.CreationDate = DateTime.Now;
                        db.Insert(item);
                    }

                    //没有生成代码 
                    var accountingTaxDetailListUpdateList = accountingTaxDetailList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in accountingTaxDetailListUpdateList)
                    {
                        db.Update(item);
                    }
                    var accountingTaxDetailListInserList = accountingTaxDetailList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in accountingTaxDetailListInserList)
                    {
                        item.Create(item.ID);
                        item.AccountingId = finance_AccountingEntityTmp.ID;
                        item.CreationDate = DateTime.Now;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (AccountingTnvoiceDetailEntity item in accountingTnvoiceDetailList)
                    {
                        item.AccountingId = entity.ID;
                        item.CreationDate = DateTime.Now;
                        db.Insert(item);
                    }

                    foreach (AccountingTaxDetailEntity item in accountingTaxDetailList)
                    {
                        item.AccountingId = entity.ID;
                        item.CreationDate = DateTime.Now;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
