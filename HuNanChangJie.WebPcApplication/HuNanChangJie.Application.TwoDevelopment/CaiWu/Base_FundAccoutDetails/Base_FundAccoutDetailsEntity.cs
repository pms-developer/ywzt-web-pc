﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using HuNanChangJie.Util.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.CaiWu

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-05 14:36
    /// 描 述：资金帐户明细
    /// </summary>
    public class Base_FundAccoutDetailsEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 账户ID
        /// </summary>
        /// <returns></returns>
        [Column("FUNDACCOUNTID")]
        public string FundAccountId { get; set; }
        /// <summary>
        /// 发生额
        /// </summary>
        /// <returns></returns>
        [Column("AMOUNTOCCURRENCE")]
        [DecimalPrecision(18, 4)]
        public decimal? AmountOccurrence { get; set; } = 0;
        /// <summary>
        /// 交易前余额
        /// </summary>
        /// <returns></returns>
        [Column("PREVBALANCE")]
        [DecimalPrecision(18, 4)]
        public decimal? PrevBalance { get; set; } = 0;
        /// <summary>
        /// 交易后余额
        /// </summary>
        /// <returns></returns>
        [Column("BALANCE")]
        [DecimalPrecision(18, 4)]
        public decimal? Balance { get; set; } = 0;
        /// <summary>
        /// 发生类型
        /// </summary>
        /// <returns></returns>
        [Column("OCCURRENCETYPE")]
        public string OccurrenceType { get; set; }
        /// <summary>
        /// 关联类型
        /// </summary>
        /// <returns></returns>
        [Column("RELEVANCETYPE")]
        public string RelevanceType { get; set; }
        /// <summary>
        /// 关联ID
        /// </summary>
        /// <returns></returns>
        [Column("RELEVANCEID")]
        public string RelevanceId { get; set; }
        /// <summary>
        /// 单据ID
        /// </summary>
        /// <returns></returns>
        [Column("RECEIPTID")]
        public string ReceiptId { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        /// <returns></returns>
        [Column("RECEIPTTYPE")]
        public string ReceiptType { get; set; }
        /// <summary>
        /// 发生日期
        /// </summary>
        /// <returns></returns>
        [Column("OCCURRENCEDATE")]
        public DateTime? OccurrenceDate { get; set; }
        /// <summary>
        /// 经办人ID
        /// </summary>
        /// <returns></returns>
        [Column("OPERATORID")]
        public string OperatorId { get; set; }
        /// <summary>
        /// 摘要
        /// </summary>
        /// <returns></returns>
        [Column("ABSTRACT")]
        public string Abstract { get; set; }

        /// <summary>
        /// 发生科目
        /// </summary>
        [Column("PROJECTSUBJECTID")]
        public string ProjectSubjectId { get; set; }
        /// <summary>
        /// 发生项目
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region 扩展成员
        [NotMapped]
        public string ProjectName { get; set; }

        [NotMapped]
        public string F_Account { get; set; }

        [NotMapped]
        public string CustomerFullName { get; set; }

        /// <summary>
        /// 单据编号
        /// </summary>
        [NotMapped]
        public string Code { get; set; }



        [NotMapped]
        public string FullName { get; set; }

        [NotMapped]
        public string SubjectName { get; set; }

        [NotMapped]
        public string OccurrenceTypeValue
        {
            get
            {
                if (OccurrenceType == "Outlay") return "支出";
                return "收入";
            }
        }

        private ReceiptEnum _type;
        [NotMapped]
        public string ReceiptTypeValue
        {
            get
            {
                _type = (ReceiptEnum)Enum.Parse(typeof(ReceiptEnum), ReceiptType);
                return ReceiptInfo.GetReceiptName(_type);
            }
        }

        [NotMapped]
        public string F_RealName { get; set; }

        [NotMapped]
        public string Url
        {
            get
            {
                var url = "";
                switch (_type)
                {
                    case ReceiptEnum.ProjectContract:
                        //工程合同收款单
                        url = $"/CaiWu/Finance_Proceeds/Form?keyValue={RelevanceId}&viewState=1";
                        break;
                    case ReceiptEnum.OtherProceeds:
                        //其他收款单
                        url = $"/CaiWu/Finance_OtherPayment/Form?keyValue={RelevanceId}&viewState=1";
                        break;
                    case ReceiptEnum.Financing:
                        //融资借款单
                        url ="";
                        break;
                    case ReceiptEnum.Acting:
                        //代垫资金单
                        if (OccurrenceTypeValue == "收入")
                        {
                            url = $"/CaiWu/Base_CJ_DianZi/Form?keyValue={RelevanceId}&viewState=1";
                        }
                        else
                        {
                            url = $"/CaiWu/Base_CJ_DianZiHuan/Form?keyValue={RelevanceId}&viewState=0";
                        }
                        break;
                    case ReceiptEnum.Interior:
                        //内部转账单;
                        url = $"/CaiWu/Finance_InternalTransfer/Form?keyValue=keyValue={RelevanceId}&viewState=1";
                        break;
                    case ReceiptEnum.Cash:
                        //现金存取单
                        url = "";
                        break;
                    case ReceiptEnum.Adjustment:
                        //调账单
                        url = "";
                        break;
                    case ReceiptEnum.SubContract:
                        //分包合同
                        url = $"/CaiWu/Finance_ExternalPayment/Form?keyValue={RelevanceId}&viewState=1";
                        break;
                    case ReceiptEnum.ProcurementContract:
                        //采购合同
                        url = $"/CaiWu/Finance_ExternalPayment/Form?keyValue={RelevanceId}&viewState=1";
                        break;
                    case ReceiptEnum.OtherPayment:
                        //其他付款单
                        url = $"/CaiWu/Finance_OtherPayment/Form?keyValue={RelevanceId}&viewState=1";
                        break;
                    case ReceiptEnum.AdministrativeCost:
                        //管理成本（对应 盈余利润、社会保险费、代扣代缴税金等费用）
                        url = $"/CaiWu/Finance_OtherPayment/Form?keyValue={RelevanceId}&viewState=1";
                        break;
                    case ReceiptEnum.DynamicTaxes:
                        return "动态税金";
                    case ReceiptEnum.StaffWage:
                        return "人员工资";
                    case ReceiptEnum.CertificateWage:
                        return "证件使用工资";
                    case ReceiptEnum.BaoXiao:
                        //报销单
                        url = $"/CaiWu/Finance_InternalPayment/Form?keyValue={RelevanceId}&viewState=1";
                        break;
                    case ReceiptEnum.Contract:
                        return "合同管理";
                    case ReceiptEnum.Other:
                        return "其它";
                    case ReceiptEnum.PrepayTax:
                        return "税费预交（完税凭证）";
                    case ReceiptEnum.Invoice:
                        return "开票登记";
                    //case ReceiptEnum.BondPay:
                    //    url = $"/CaiWu/Base_CJ_BaoZhengJinZhiFu/Form?keyValue={RelevanceId}&viewState=0";
                    //    break;
                    //case ReceiptEnum.BondReturn:
                    //    url = $"/CaiWu/Base_CJ_BaoZhengJinTui/Form?keyValue={RelevanceId}&viewState=1";
                    //    break;
                    default:
                        url ="";
                        break;
                }
                return url;
            }
        }
        #endregion
    }
}

