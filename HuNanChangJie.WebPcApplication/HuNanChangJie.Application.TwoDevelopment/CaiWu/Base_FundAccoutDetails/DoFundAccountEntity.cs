﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    public class DoFundAccountEntity
    {
        public DoFundAccountEntity()
        {
            try
            {        
                this.OperatorId = LoginUserInfo.UserInfo.userId;
            }
            catch (NullReferenceException)
            {
                this.OperatorId = "system";
            }
            catch (Exception)
            {

            }
        }
        
        #region  实体成员
        /// <summary>
        /// 帐户标识 账户名称/账户银行卡号/关联项目ID（项目多帐户默认取第一个）
        /// </summary>
        /// <returns></returns>
        public string FundAccountFlag { get; set; }
        /// <summary>
        /// 发生额
        /// </summary>
        /// <returns></returns>
        public decimal AmountOccurrence { get; set; } = 0;
        /// <summary>
        /// 账户明细发生类型 支出(0)/收入(1)/冻结(2)/解冻(3)
        /// </summary>
        /// <returns></returns>
        public OccurrenceEnum OccurrenceType { get; set; }
        /// <summary>
        /// 关联类型 项目(1)/其它(99)
        /// </summary>
        /// <returns></returns>
        public RelevanceEnum RelevanceType { get; set; }
        /// <summary>
        /// 关联ID
        /// </summary>
        /// <returns></returns>
        public string RelevanceId { get; set; }
        /// <summary>
        /// 单据ID
        /// </summary>
        /// <returns></returns>
        public string ReceiptId { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        /// <returns></returns>
        public ReceiptEnum ReceiptType { get; set; }
        /// <summary>
        /// 经办人ID
        /// </summary>
        /// <returns></returns>
        public string OperatorId { get; set; } 
 
        /// <summary>
        /// 摘要
        /// </summary>
        /// <returns></returns>
        public string Abstract { get; set; }

        /// <summary>
        /// 成本科目ID(实际值为BaseSubjectId)
        /// </summary>
        public string ProjectSubjectId { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        public string ProjectId { get; set; }
        #endregion
    }
}
