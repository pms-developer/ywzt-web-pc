﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-05 19:09
    /// 描 述：财务-对外付款单
    /// </summary>
    public class Finance_ExternalPaymentDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 对外付款单Id
        /// </summary>
        [Column("FINANCEEXTERNALPAYMENTID")]
        public string FinanceExternalPaymentId { get; set; }
        ///// <summary>
        ///// 帐户所属公司
        ///// </summary>
        //[Column("AMOUNTCOMPANYID")]
        //public string AmountCompanyId { get; set; }
        
        /// <summary>
        /// 合同ID
        /// </summary>
        [Column("CONTRACTID")]
        public string ContractId { get; set; }
        /// <summary>
        /// 还款单Id
        /// </summary>
        [Column("REPAYMENTID")]
        public string RepaymentId { get; set; }
        /// <summary>
        /// 借款申请单ID
        /// </summary>
        [Column("FINANCELOANAPPLYID")]
        public string FinanceLoanApplyId { get; set; }
        /// <summary>
        /// 款项性质
        /// </summary>
        [Column("CONTRACTTYPE")]
        public string ContractType { get; set; }

        [Column("CONTRACTTYPEID")]
        public string ContractTypeId { get; set; }
        /// <summary>
        /// 合同细分类型
        /// </summary>
        [Column("CONTRACTTYPE2")]
        public string ContractType2 { get; set; }
        /// <summary>
        /// 付款申请单Id
        /// </summary>
        [Column("PAYMENTAPPLYID")]
        public string PaymentApplyId { get; set; }
        /// <summary>
        /// 付款申请单类型
        /// </summary>
        [Column("PAYMENTAPPLYTYPE")]
        public string PaymentApplyType { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        [Column("PAYMENTTYPE")]
        public string PaymentType { get; set; }
        /// <summary>
        /// 项目Id
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 开户行
        /// </summary>
        [Column("BANKID")]
        public string BankId { get; set; }
        /// <summary>
        /// 银行账号/票据号
        /// </summary>
        [Column("ACCOUNTCODE")]
        public string AccountCode { get; set; }
        /// <summary>
        /// 本次付款
        /// </summary>
        [Column("PAYMENTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? PaymentAmount { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }


        [Column("ACCOUNTNAME")]
        public string AccountName { get; set; }

        [Column("BANKNAME")]
        public string BankName { get; set; }

        [Column("BANKACCOUNT")]
        public string BankAccount { get; set; }


        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region 扩展属性
        /// <summary>
        /// 收款单位
        /// </summary>
        [NotMapped]
        public string Payee { get; set; }

        /// <summary>
        /// 合同名称
        /// </summary>
        [NotMapped]
        public string ContractName { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        [NotMapped]
        public string ProjectName { get; set; }

        /// <summary>
        /// 付款申请单号
        /// </summary>
        [NotMapped]
        public string ApplyCode { get; set; }

        /// <summary>
        /// 申请日期
        /// </summary>
        [NotMapped]
        public DateTime? ApplyDate { get; set; }

        /// <summary>
        /// 申请金额
        /// </summary>
        [NotMapped]
        public decimal ApplyPaymentAmount { get; set; }
        /// <summary>
        /// 批准金额
        /// </summary>
        [NotMapped]
        public decimal RatifyAmount { get; set; }

        /// <summary>
        /// 已收票金额
        /// </summary>
        [NotMapped]
        public decimal? InvoiceAmount { get; set; }

        /// <summary>
        /// 已付款金额
        /// </summary>
        [NotMapped] 
        public decimal? AccountPaidAmount { get; set; }

        /// <summary>
        /// 默认付款账户
        /// </summary>
        [NotMapped] 
        public string DefaultAccountName { get; set; }


        [NotMapped]
        public string AmountCompany { get; set; }


        [NotMapped]
        public string AmountCompanyId { get; set; }

        #endregion
        [NotMapped]
        public string OpenBank { get; set; }
        [NotMapped]
        public string BankCode { get; set; }
        [NotMapped]
        public string Holder { get; set; }

        [NotMapped]
        public string BankNames { get; set; }
        [NotMapped]
        public string ProjectManager { get; set; }
        

    }
}

