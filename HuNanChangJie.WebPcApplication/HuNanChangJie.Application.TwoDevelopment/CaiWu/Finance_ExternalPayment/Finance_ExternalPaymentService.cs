﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu.Finance_InternalPayment;
using HuNanChangJie.Application.TwoDevelopment.Extend;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-05 19:09
    /// 描 述：财务-对外付款单
    /// </summary>
    public class Finance_ExternalPaymentService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据


        public IEnumerable<Finance_ExternalPaymentSelectDanJuEntity> GetHeTongList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@" select * from View_DWFKD where waitpay > 0 ");



                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });


                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "FuKuanShengQingShiFouDuoCiFuKuan");
                if (configInfo != null)
                {
                    var flag = configInfo.Value;

                    if (flag == false)
                    {
                        strSql.Append(" AND (isclose is null or isclose='0') ");
                    }
                }

                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND Code Like @Code ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append("  AND projectname Like @Name ");
                }

                if (!queryParam["word"].IsEmpty())
                {
                    dp.Add("word", "%" + queryParam["word"].ToString() + "%", DbType.String);
                    strSql.Append(" AND (Payee Like @word or hetongname Like @word  or projectname Like @word  or danwei Like @word ) ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "sdate";
                    pagination.sord = "DESC";
                }
                //var list = this.BaseRepository().FindList<Finance_ExternalPaymentSelectDanJuEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                //return query;
                pagination.rows = 10000000;

                var list = this.BaseRepository().FindList<Finance_ExternalPaymentSelectDanJuEntity>(strSql.ToString(), dp, pagination);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }


        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_ExternalPaymentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@" select 
                t.ID,
                t.Code,
                t.ApplyPaymentAmount,
                t.PaymentType,
                e.F_RealName OperatorId,
                t.PaymentDate,
                t.PaymentAmount,
                t.PaymentCompanyId,
                t.AuditStatus,
                t.CreationDate,
                t.Workflow_ID, t.Abstract
                  FROM Finance_ExternalPayment t left join Base_User e on t.OperatorId = e.F_UserId  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["SAmount"].IsEmpty())
                {
                    dp.Add("SAmount", queryParam["SAmount"].ToString(), DbType.Decimal);
                    strSql.Append(" AND t.PaymentAmount >= @SAmount ");
                }
                if (!queryParam["EAmount"].IsEmpty())
                {
                    dp.Add("EAmount", queryParam["EAmount"].ToString(), DbType.Decimal);
                    strSql.Append(" AND t.PaymentAmount <= @EAmount ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID= @ProjectID ");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }

                if (!queryParam["PaymentType"].IsEmpty())
                {
                    dp.Add("PaymentType", queryParam["PaymentType"].ToString(), DbType.String);
                    strSql.Append(" AND t.PaymentType = @PaymentType ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.PaymentDate >= @startTime AND t.PaymentDate <= @endTime ) ");
                }
                if (pagination != null)
                {
                    if (string.IsNullOrEmpty(pagination.sidx))
                    {
                        pagination.sidx = "Code";
                        pagination.sord = "DESC";
                    }
                    else
                    {
                        if (pagination.sidx == "CodeList" || pagination.sidx == "TypeList" || pagination.sidx == "PayeeList"
                             || pagination.sidx == "PaymentTypeList" || pagination.sidx == "PaymentCompanyList" || pagination.sidx == "PaymentZhanghuList")
                        {
                            pagination.sidx = "";
                        }
                    }
                }

                int pageIndex = pagination.page;
                int pageSize = pagination.rows;
                if (!queryParam["ProjectContractName"].IsEmpty() || !queryParam["Payee"].IsEmpty())
                {
                    pagination.page = 1;
                    pagination.rows = 1000000;
                }

                var list = this.BaseRepository().FindList<Finance_ExternalPaymentEntity>(strSql.ToString(), dp, pagination);

                if (list != null && list.Count() > 0)
                {
                    List<string> ids = new List<string>();
                    foreach (Finance_ExternalPaymentEntity item in list)
                    {
                        ids.Add(item.ID);
                    }
              
                    string idstr = string.Join("','", ids.ToArray());

                    var listdetails = this.BaseRepository().FindList<Finance_ExternalPaymentDetailsEntity>($"select * from View_DWFDInfo where FinanceExternalPaymentId in('{idstr}')");

                    foreach (Finance_ExternalPaymentEntity item in list)
                    {
                        item.TypeList = "";
                        item.CodeList = "";
                        item.PayeeList = "";

                        item.ProjectManager = "";
                        item.PaymentTypeList = ""; 
                        item.PaymentZhanghuList = "";


                        foreach (Finance_ExternalPaymentDetailsEntity itemdetails in listdetails.Where(m => m.FinanceExternalPaymentId == item.ID))
                        {

                            if (!string.IsNullOrEmpty(item.TypeList))
                                item.TypeList += ",";
                            //item.TypeList += itemdetails.ContractType2;

                            var contractType = this.BaseRepository().FindEntity<Base_ContractTypeEntity>(ct => ct.Name == itemdetails.ContractType2);
                            if (contractType != null)
                            {
                                if (contractType.PropertyEnum == ContractProperty.SubContract)
                                {
                                    var subcontract = this.BaseRepository().FindEntity<Project_SubcontractEntity>(i => i.ID == itemdetails.ContractId);
                                    if (subcontract != null)
                                        item.TypeList += $"{itemdetails.ContractType2}:{subcontract.Name}-{subcontract.ProjectName}";
                                }
                                else if (contractType.PropertyEnum == ContractProperty.PurchaseContract) 
                                {
                                    var purchaseContract = this.BaseRepository().FindEntity<Purchase_ContractEntity>(i => i.ID == itemdetails.ContractId);
                                    if (purchaseContract != null)
                                        item.TypeList += $"{itemdetails.ContractType2}:{purchaseContract.Name}-{purchaseContract.ProjectName}";
                                }

                            }
                            if (!string.IsNullOrEmpty(item.CodeList))
                                item.CodeList += ",";
                            item.CodeList += itemdetails.ApplyCode;

                            if (!string.IsNullOrEmpty(item.PayeeList))
                                item.PayeeList += ",";
                            item.PayeeList += itemdetails.Payee;


                            if (!string.IsNullOrEmpty(item.PaymentTypeList))
                                item.PaymentTypeList += ",";
                            item.PaymentTypeList += itemdetails.PaymentType;


                            if (!string.IsNullOrEmpty(item.ProjectManager))
                                item.ProjectManager += ",";
                            item.ProjectManager += itemdetails.ProjectManager;

                            if (!string.IsNullOrEmpty(item.PaymentZhanghuList))
                                item.PaymentZhanghuList += ",";
                            item.PaymentZhanghuList += itemdetails.BankNames;
                        }
                    }
                }
                if (!queryParam["ProjectContractName"].IsEmpty() || !queryParam["Payee"].IsEmpty())
                {
                    var query = from item in list select item;
                    if (!queryParam["ProjectContractName"].IsEmpty())
                    {
                        query = query.Where(m=>m.TypeList.Contains(queryParam["ProjectContractName"].ToString()));
                    }
                    if (!queryParam["Payee"].IsEmpty())
                    {
                        query = query.Where(m => m.PayeeList.Contains(queryParam["Payee"].ToString()));
                    }
                    pagination.records = query.Count();
                    return query.OrderByDescending(m => m.CreationDate).Skip(pageSize * (pageIndex - 1)).Take(pageSize);
                }
                else
                {
                    var query = from item in list orderby item.CreationDate descending select item; 
                    return query;
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal OperateResultEntity BeforeSendingCheckData(string keyValue)
        {
            try
            {
                var op = new OperateResultEntity();
                op.Success = true;
                var details = this.BaseRepository().FindList<Finance_ExternalPaymentDetailsEntity>(i => i.FinanceExternalPaymentId == keyValue);
                if (details.Any())
                {
                    foreach (var item in details)
                    {
                        var info = BaseRepository().FindEntity<Base_FundAccountEntity>(i => i.ID == item.BankId);
                        if (item.PaymentAmount > info.UsableBalance)
                        {
                            op.Success = false;
                            op.Message = $"流程发送失败，当前账记的可用余额不足以完成【{item.PaymentAmount}】的转账";
                            return op;
                        }

                    }
                }
                else
                {
                    op.Success = false;
                    op.Message = "流程发送失败，未检查到详情数据";
                }
                return op;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Finance_ExternalPaymentSelectDanJuEntity> GetHeTongListByTask(string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                var queryParam = queryJson.ToJObject();
                if (queryParam["id"].IsEmpty())
                    return null;
                else
                {
                    strSql.Append($"select * from View_DWFKD where applyid = '{queryParam["id"].ToString()}'");
                }

                var list = this.BaseRepository().FindList<Finance_ExternalPaymentSelectDanJuEntity>(strSql.ToString());
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }


        }


        public IEnumerable<Finance_ExternalPaymentEntity> GetPageListByTask(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                var queryParam = queryJson.ToJObject();
                if (!queryParam["id"].IsEmpty())
                {
                    strSql.Append($"SELECT distinct t.ID,t.Code, t.PaymentType, t.OperatorId, t.PaymentDate, b.PaymentAmount, t.PaymentCompanyId, t.AuditStatus from Finance_ExternalPayment t right join (select * from Finance_ExternalPaymentDetails where ContractId= '{queryParam["id"].ToString()}') b on t.ID = b.FinanceExternalPaymentId where 1= 1 ");
                }
                else
                {
                    strSql.Append($"SELECT distinct t.ID,t.Code, t.PaymentType, t.OperatorId, t.PaymentDate, b.PaymentAmount, t.PaymentCompanyId, t.AuditStatus from Finance_ExternalPayment t left join Finance_ExternalPaymentDetails b on t.ID = b.FinanceExternalPaymentId where 1= 1 ");
                }



                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "Code";
                    pagination.sord = "DESC";
                    pagination.rows = 100000;
                }

                var list = this.BaseRepository().FindList<Finance_ExternalPaymentEntity>(strSql.ToString(), pagination);

                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ExternalPaymentDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_ExternalPaymentDetailsEntity> GetFinance_ExternalPaymentDetailsList(string keyValue)
        {
            try
            {
                var sql = "select * from View_DWFDInfo where FinanceExternalPaymentId=@keyValue";
                var list = this.BaseRepository().FindList<Finance_ExternalPaymentDetailsEntity>(sql, new { keyValue });
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ExternalPayment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ExternalPaymentEntity GetFinance_ExternalPaymentEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_ExternalPaymentEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ExternalPaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ExternalPaymentDetailsEntity GetFinance_ExternalPaymentDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_ExternalPaymentDetailsEntity>(t => t.FinanceExternalPaymentId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_ExternalPaymentEntity finance_ExternalPaymentEntity = db.FindEntity<Finance_ExternalPaymentEntity>(keyValue);

                if (finance_ExternalPaymentEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_ExternalPaymentEntity.AuditStatus = "4";
                db.Update<Finance_ExternalPaymentEntity>(finance_ExternalPaymentEntity);

                var details = db.FindList<Finance_ExternalPaymentDetailsEntity>(i => i.FinanceExternalPaymentId == keyValue);

                var base_ContractTypeList = db.FindList<Base_ContractTypeEntity>();

                foreach (var item in details)
                {
                    var contractType = base_ContractTypeList.FirstOrDefault(ct => ct.Name == item.ContractType2);
                    if (contractType == null)
                        continue;

                    ReceiptEnum receiptEnum;
                    string subjectId = null;
                    decimal rate = 0;
                    var getSubjectSql = "";
                    dynamic info;
                    var projectId = "";
                    switch (contractType.PropertyEnum)
                    {
                        case ContractProperty.SubContract:
                            receiptEnum = ReceiptEnum.SubContract;
                            getSubjectSql = "select b.BaseSubjectId,a.TaxRate from Project_Subcontract as a left join Base_ContractType as b  on a.SubcontractType=b.id where a.id=@ContractId";
                            info = db.FindEntity<dynamic>(getSubjectSql, new { item.ContractId });
                            subjectId = info.BaseSubjectId;
                            rate = info.TaxRate;
                            var subcontract = db.FindEntity<Project_SubcontractEntity>(i => i.ID == item.ContractId);
                            if (subcontract == null) throw new Exception("合同信息为空");
                            projectId = subcontract.ProjectID;
                            subcontract.AccountPaidAmount = (subcontract.AccountPaidAmount ?? 0) - (item.PaymentAmount ?? 0);
                            subcontract.AccountPayable = (subcontract.AccountPayable ?? 0) + (item.PaymentAmount ?? 0);
                            db.Update(subcontract);

                            var project_SubcontractPaymentApply = db.FindEntity<Project_SubcontractPaymentApplyEntity>(i => i.ID == item.PaymentApplyId);
                            if (project_SubcontractPaymentApply != null)
                            {
                                project_SubcontractPaymentApply.AccountPaidAmount = (project_SubcontractPaymentApply.AccountPaidAmount ?? 0) - (item.PaymentAmount ?? 0);

                                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "FuKuanShengQingShiFouDuoCiFuKuan");
                                if (configInfo != null)
                                {
                                    var flag = configInfo.Value;
                                    if (flag == false)
                                    {
                                        project_SubcontractPaymentApply.IsClose = false;
                                    }
                                }

                                db.Update<Project_SubcontractPaymentApplyEntity>(project_SubcontractPaymentApply);
                            }
                            break;
                        case ContractProperty.PurchaseContract:
                            receiptEnum = ReceiptEnum.ProcurementContract;
                            getSubjectSql = "select b.BaseSubjectId,a.TaxRage from Purchase_Contract as a left join Base_ContractType as b  on a.ContractType=b.id where a.id=@ContractId";
                            info = db.FindEntity<dynamic>(getSubjectSql, new { item.ContractId });
                            subjectId = info.BaseSubjectId;
                            rate = info.TaxRage;
                            var purchaseContract = db.FindEntity<Purchase_ContractEntity>(i => i.ID == item.ContractId);
                            if (purchaseContract == null) throw new Exception("合同信息为空");
                            projectId = purchaseContract.ProjectID;
                            purchaseContract.AccountPaid = (purchaseContract.AccountPaid ?? 0) - (item.PaymentAmount ?? 0);

                            purchaseContract.AccountPayable = (purchaseContract.AccountPayable ?? 0) + (item.PaymentAmount ?? 0);

                            db.Update(purchaseContract);

                            var purchase_OrderPay = db.FindEntity<Purchase_OrderPayEntity>(i => i.ID == item.PaymentApplyId);
                            if (purchase_OrderPay != null)
                            {
                                purchase_OrderPay.AccountPaidAmount = (purchase_OrderPay.AccountPaidAmount ?? 0) - (item.PaymentAmount ?? 0);

                                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "FuKuanShengQingShiFouDuoCiFuKuan");
                                if (configInfo != null)
                                {
                                    var flag = configInfo.Value;
                                    if (flag == false)
                                    {
                                        purchase_OrderPay.IsClose = false;
                                    }
                                }

                                db.Update<Purchase_OrderPayEntity>(purchase_OrderPay);
                            }
                            break;
                        default:
                            receiptEnum = ReceiptEnum.Other;
                            break;

                    }
                    using (var auditExtend = new AuditPaymentExtend(db, projectId, receiptEnum, finance_ExternalPaymentEntity.ID, item.ID, item.PaymentAmount ?? 0, item.BankId, subjectId, rate))
                    {
                        var des = $"{finance_ExternalPaymentEntity.Abstract}[对外付款单取消审核]";
                        OperateResultEntity op = auditExtend.UnAudit(des);

                        if (!op.Success)
                            throw new Exception(op.Message);

                        var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceExternalPayment");
                        if (configInfo != null)
                        {
                            var flag = configInfo.Value;

                            if (flag)
                            {
                                Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == projectId);

                                if (base_FundAccountEntity != null)
                                {
                                    var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                    {
                                        FundAccountFlag = base_FundAccountEntity.ID,
                                        AmountOccurrence = item.PaymentAmount.HasValue ? item.PaymentAmount.Value : 0,
                                        OccurrenceType = OccurrenceEnum.Income,
                                        RelevanceType = RelevanceEnum.Project,
                                        ReceiptType = receiptEnum,
                                        RelevanceId = finance_ExternalPaymentEntity.ID,
                                        ReceiptId = item.ID,
                                        Abstract = des,
                                        ProjectId = projectId
                                    }, db, false);
                                    if (!result1.Success)
                                        throw new Exception(result1.Message);
                                }
                            }
                        }
                    }
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_ExternalPaymentEntity finance_ExternalPaymentEntity = db.FindEntity<Finance_ExternalPaymentEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_ExternalPaymentEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                finance_ExternalPaymentEntity.AuditStatus = "2";
                db.Update<Finance_ExternalPaymentEntity>(finance_ExternalPaymentEntity);

                var details = db.FindList<Finance_ExternalPaymentDetailsEntity>(i => i.FinanceExternalPaymentId == keyValue);
                var base_ContractTypeList = db.FindList<Base_ContractTypeEntity>();
                //if(finance_ExternalPaymentEntity.PaymentType == "合同付款")

                foreach (var item in details)
                {
                    var contractType = base_ContractTypeList.FirstOrDefault(ct => ct.Name == item.ContractType2);
                    if (contractType == null)
                        continue;
                    ReceiptEnum receiptEnum;
                    string subjectId = null;
                    decimal rate = 0;
                    var getSubjectSql = "";
                    dynamic info;
                    var projectId = "";
                    switch (contractType.PropertyEnum)
                    {
                        case ContractProperty.SubContract:
                            receiptEnum = ReceiptEnum.SubContract;
                            getSubjectSql = "select b.BaseSubjectId,a.TaxRate from Project_Subcontract as a left join Base_ContractType as b  on a.SubcontractType=b.id where a.id=@ContractId";
                            info = db.FindEntity<dynamic>(getSubjectSql, new { item.ContractId });
                            subjectId = info.BaseSubjectId;
                            rate = info.TaxRate;
                            var subcontract = db.FindEntity<Project_SubcontractEntity>(i => i.ID == item.ContractId);
                            if (subcontract == null) throw new Exception("合同信息为空");
                            projectId = subcontract.ProjectID;
                            subcontract.AccountPaidAmount = (subcontract.AccountPaidAmount ?? 0) + (item.PaymentAmount ?? 0);
                            if ((subcontract.AccountPayable ?? 0) >= (item.PaymentAmount ?? 0))
                            {
                                subcontract.AccountPayable = (subcontract.AccountPayable ?? 0) - (item.PaymentAmount ?? 0);
                            }
                            else
                            {
                                subcontract.AccountPayable = 0;
                            }
                            db.Update(subcontract);

                            var project_SubcontractPaymentApply = db.FindEntity<Project_SubcontractPaymentApplyEntity>(i => i.ID == item.PaymentApplyId);
                            if (project_SubcontractPaymentApply != null)
                            {
                                project_SubcontractPaymentApply.AccountPaidAmount = (project_SubcontractPaymentApply.AccountPaidAmount ?? 0) + (item.PaymentAmount ?? 0);
                                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "FuKuanShengQingShiFouDuoCiFuKuan");
                                if (configInfo != null)
                                {
                                    var flag = configInfo.Value;
                                    if (flag == false)
                                    {
                                        project_SubcontractPaymentApply.IsClose = true;
                                    }
                                }
                                db.Update<Project_SubcontractPaymentApplyEntity>(project_SubcontractPaymentApply);
                            }



                            break;
                        case ContractProperty.PurchaseContract:
                            receiptEnum = ReceiptEnum.ProcurementContract;
                            getSubjectSql = "select b.BaseSubjectId,a.TaxRage from Purchase_Contract as a left join Base_ContractType as b  on a.ContractType=b.id where a.id=@ContractId";
                            info = db.FindEntity<dynamic>(getSubjectSql, new { item.ContractId });
                            subjectId = info.BaseSubjectId;
                            rate = info.TaxRage;
                            var purchaseContract = db.FindEntity<Purchase_ContractEntity>(i => i.ID == item.ContractId);
                            if (purchaseContract == null) throw new Exception("合同信息为空");
                            projectId = purchaseContract.ProjectID;
                            purchaseContract.AccountPaid = (purchaseContract.AccountPaid ?? 0) + (item.PaymentAmount ?? 0);
                            if ((purchaseContract.AccountPayable ?? 0) >= (item.PaymentAmount ?? 0))
                            {
                                purchaseContract.AccountPayable = (purchaseContract.AccountPayable ?? 0) - (item.PaymentAmount ?? 0);
                            }
                            else
                            {
                                purchaseContract.AccountPayable = 0;
                            }
                            db.Update(purchaseContract);


                            var purchase_OrderPay = db.FindEntity<Purchase_OrderPayEntity>(i => i.ID == item.PaymentApplyId);
                            if (purchase_OrderPay != null)
                            {
                                purchase_OrderPay.AccountPaidAmount = (purchase_OrderPay.AccountPaidAmount ?? 0) + (item.PaymentAmount ?? 0);

                                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "FuKuanShengQingShiFouDuoCiFuKuan");
                                if (configInfo != null)
                                {
                                    var flag = configInfo.Value;
                                    if (flag == false)
                                    {
                                        purchase_OrderPay.IsClose = true;
                                    }
                                }

                                db.Update<Purchase_OrderPayEntity>(purchase_OrderPay);
                            }

                            break;
                        default:
                            receiptEnum = ReceiptEnum.Other;
                            break;

                    }
                    using (var auditExtend = new AuditPaymentExtend(db, projectId, receiptEnum, finance_ExternalPaymentEntity.ID, item.ID, item.PaymentAmount ?? 0, item.BankId, subjectId, rate))
                    {
                        var des = $"{finance_ExternalPaymentEntity.Abstract}[对外付款单审核]";
                        OperateResultEntity op = auditExtend.Audit(des);
                        if (!op.Success)
                            throw new Exception(op.Message);

                        var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceExternalPayment");
                        if (configInfo != null)
                        {
                            var flag = configInfo.Value;

                            if (flag)
                            {
                                Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == projectId);

                                if (base_FundAccountEntity != null)
                                {
                                    var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                    {
                                        FundAccountFlag = base_FundAccountEntity.ID,
                                        AmountOccurrence = item.PaymentAmount.HasValue ? item.PaymentAmount.Value : 0,
                                        OccurrenceType = OccurrenceEnum.Outlay,
                                        RelevanceType = RelevanceEnum.Project,
                                        ReceiptType = receiptEnum,
                                        RelevanceId = finance_ExternalPaymentEntity.ID,
                                        ReceiptId = item.ID,
                                        Abstract = des,
                                        ProjectId = projectId
                                    }, db, false);
                                    if (!result1.Success)
                                        throw new Exception(result1.Message);
                                }
                            }
                        }
                    }
                }
                  db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_ExternalPaymentEntity = GetFinance_ExternalPaymentEntity(keyValue);
                db.Delete<Finance_ExternalPaymentEntity>(t => t.ID == keyValue);
                db.Delete<Finance_ExternalPaymentDetailsEntity>(t => t.FinanceExternalPaymentId == finance_ExternalPaymentEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_ExternalPaymentEntity entity, List<Finance_ExternalPaymentDetailsEntity> finance_ExternalPaymentDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var projectIds = "";

                    foreach (var item in finance_ExternalPaymentDetailsList)
                    {
                        projectIds += item.ProjectId + ",";
                    }

                    if (projectIds != "")
                    {
                        projectIds = projectIds.Substring(0, projectIds.Length - 1);
                    }
                    var finance_ExternalPaymentEntityTmp = GetFinance_ExternalPaymentEntity(keyValue);
                    entity.Modify(keyValue);
                    entity.ProjectID = projectIds;
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }


                    //没有生成代码 
                    var Finance_ExternalPaymentDetailsUpdateList = finance_ExternalPaymentDetailsList.FindAll(i => i.EditType == EditType.Update);
                    for (int i = 0; i < Finance_ExternalPaymentDetailsUpdateList.Count(); i++)
                    {
                        db.Update<Finance_ExternalPaymentDetailsEntity>(Finance_ExternalPaymentDetailsUpdateList[i]);
                    }
                    //foreach (var item in Finance_ExternalPaymentDetailsUpdateList)
                    //{

                    //}
                    var Finance_ExternalPaymentDetailsInserList = finance_ExternalPaymentDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Finance_ExternalPaymentDetailsInserList)
                    {
                        item.Create(item.ID);

                        item.FinanceExternalPaymentId = finance_ExternalPaymentEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    var projectIds = "";
                    entity.Create(keyValue);
                    foreach (Finance_ExternalPaymentDetailsEntity item in finance_ExternalPaymentDetailsList)
                    {
                        item.FinanceExternalPaymentId = entity.ID;
                        projectIds += item.ProjectId + ",";

                        db.Insert(item);
                    }
                    if (projectIds != "")
                    {
                        projectIds = projectIds.Substring(0, projectIds.Length - 1);
                    }
                    entity.ProjectID = projectIds;

                    db.Insert(entity);

                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
