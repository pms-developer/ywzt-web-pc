﻿using HuNanChangJie.SystemCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu.Finance_InternalPayment
{
    public class Finance_ExternalPaymentSelectDanJuEntity : BaseEntity
    {
        /// <summary>
        /// 付款申请单ID
        /// </summary>
        [NotMapped]
        public string ApplyId { get; set; }
        [NotMapped]
        public string ContractType { get; set; }

        /// <summary>
        /// 付款模式
        /// </summary>
        [NotMapped]
        public string PayModel { get; set; }
        /// <summary>
        /// 合同ID
        /// </summary>
        [NotMapped]
        public string ContractId { get; set; }

        [NotMapped]
        public string mode { get; set; }

        [NotMapped]
        public string mode2 { get; set; }


        [NotMapped]
        public string F_RealName { get; set; }


        [NotMapped]
        public DateTime? ApplyDate { get; set; }

        [NotMapped]
        public string PayCompanyId { get; set; }

        [NotMapped]
        public string  PayCompanyName { get; set; }


        [NotMapped]
        public DateTime sdate { get; set; }

        [NotMapped]
        public bool? IsClose { get; set; }

        [NotMapped]
        public string hetongname { get; set; }

        [NotMapped]
        public string code { get; set; }
        [NotMapped]
        public string AmountCompany  { get; set; }
        [NotMapped]
        public string AmountCompanyId { get; set; }

        [NotMapped]
        public string danwei { get; set; }
        [NotMapped]
        public int printcount { get; set; }
        [NotMapped]
        public decimal payed { get; set; }
        [NotMapped]
        public decimal waitpay { get; set; }
        [NotMapped]
        public decimal amount { get; set; }

        [NotMapped]
        public string Remark { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// 关联科目
        /// </summary>
        public string SubjectName { get; set; }

        /// <summary>
        /// 合同金额
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// 申请付款金额
        /// </summary>
        public decimal ApplyPaymentAmount { get; set; }
        /// <summary>
        /// 默认付款账号
        /// </summary>
        public string DefaultAccountName { get; set; }

        /// <summary>
        /// 默认付款账号ID
        /// </summary>
        public string DefaultAccountId { get; set; }

        /// <summary>
        /// 收票金额
        /// </summary>
        public decimal? InvioceAmount { get;set;}

        /// <summary>
        /// 已付款金额
        /// </summary>
        public decimal? AccountPaidAmount { get; set; }

        /// <summary>
        /// 收款单位
        /// </summary>
        public string Payee { get; set; }

        /// <summary>
        /// 银行账号
        /// </summary>
        public string BankAccount { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// 开户名
        /// </summary>
        public string AccountName { get; set; }

        

    }
}
