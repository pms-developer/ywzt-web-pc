﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu.Finance_InternalPayment;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-05 19:09
    /// 描 述：财务-对外付款单
    /// </summary>
    public interface Finance_ExternalPaymentIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_ExternalPaymentEntity> GetPageList(XqPagination pagination, string queryJson);
        IEnumerable<Finance_ExternalPaymentEntity> GetPageListByTask(XqPagination pagination, string queryJson);
        
        /// <summary>
        /// 获取Finance_ExternalPaymentDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_ExternalPaymentDetailsEntity> GetFinance_ExternalPaymentDetailsList(string keyValue);
        /// <summary>
        /// 获取Finance_ExternalPayment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_ExternalPaymentEntity GetFinance_ExternalPaymentEntity(string keyValue);

        IEnumerable<Finance_ExternalPaymentSelectDanJuEntity> GetHeTongList(XqPagination pagination, string queryJson);
        IEnumerable<Finance_ExternalPaymentSelectDanJuEntity> GetHeTongListByTask(string queryJson);

        /// <summary>
        /// 获取Finance_ExternalPaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_ExternalPaymentDetailsEntity GetFinance_ExternalPaymentDetailsEntity(string keyValue);
        #endregion

        #region  提交数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_ExternalPaymentEntity entity,List<Finance_ExternalPaymentDetailsEntity> finance_ExternalPaymentDetailsList,string deleteList,string type);
        /// <summary>
        /// 发送流程前数据检查
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns>true:可以发送流程 false:不可以发送流程</returns>
        OperateResultEntity BeforeSendingCheckData(string keyValue);


        #endregion

    }
}
