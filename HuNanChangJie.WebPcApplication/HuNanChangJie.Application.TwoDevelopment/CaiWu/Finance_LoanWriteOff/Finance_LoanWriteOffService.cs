﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.Organization;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-06-29 18:37
    /// 描 述：借款冲销
    /// </summary>
    public class Finance_LoanWriteOffService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_LoanWriteOffEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT * FROM Finance_LoanWriteOff t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }

                if (pagination !=null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "Code";
                    pagination.sord = "DESC";
                }
                var list=this.BaseRepository().FindList<Finance_LoanWriteOffEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_LoanWriteOffDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_LoanWriteOffDetailsEntity> GetFinance_LoanWriteOffDetailsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Finance_LoanWriteOffDetailsEntity>("select a.*,b.Code,b.Amount from Finance_LoanWriteOffDetails a left join Project_SporadicPurchase b on a.Fk_BillId = b.ID where a.Fk_LoanWriteOffId = '" + keyValue + "'");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_LoanWriteOff表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_LoanWriteOffEntity GetFinance_LoanWriteOffEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_LoanWriteOffEntity>("select a.*,b.code LoanCode,b.Loaner,b.PayedAmount from Finance_LoanWriteOff a left join Finance_Loan b on a.Fk_LoanId = b.id where a.id = '"+ keyValue+"'",null);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_LoanWriteOffDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_LoanWriteOffDetailsEntity GetFinance_LoanWriteOffDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_LoanWriteOffDetailsEntity>(t=>t.Fk_LoanWriteOffId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_LoanWriteOffEntity finance_LoanWriteOffEntity = db.FindEntity<Finance_LoanWriteOffEntity>(keyValue);
                if (finance_LoanWriteOffEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_LoanWriteOffEntity.AuditStatus = "4";
                db.Update<Finance_LoanWriteOffEntity>(finance_LoanWriteOffEntity);

                Finance_LoanEntity finance_LoanEntity = db.FindEntity<Finance_LoanEntity>(finance_LoanWriteOffEntity.Fk_LoanId);
                if (finance_LoanEntity == null)
                {
                    throw new Exception(finance_LoanEntity.Code + ":借款单不存在");
                }

                //if (finance_LoanWriteOffEntity.WriteOffAmount.HasValue && finance_LoanEntity.PayedAmount.HasValue && finance_LoanEntity.PayedAmount.Value < finance_LoanWriteOffEntity.WriteOffAmount.Value)
                //{
                //    throw new Exception(finance_LoanEntity.Code + ":借款单已支付金额小于当前冲销金额");
                //}

                UserEntity userEntity = db.FindEntity<UserEntity>(finance_LoanEntity.Loaner);
                if (userEntity == null)
                {
                    throw new Exception(userEntity.F_UserId + ":借款人不存在");
                }
                if (!userEntity.Loan.HasValue)
                    userEntity.Loan = 0;

                //if (finance_LoanWriteOffEntity.WriteOffAmount.HasValue && userEntity.Loan.HasValue && userEntity.Loan.Value < finance_LoanWriteOffEntity.WriteOffAmount.Value)
                //{
                //    throw new Exception(userEntity.F_UserId + ":用户未清借款小于当前冲销金额");
                //}

                //if (finance_LoanWriteOffEntity.WriteOffAmount.HasValue)
                userEntity.Loan += finance_LoanWriteOffEntity.WriteOffAmount.Value;

                db.Update<UserEntity>(userEntity);
                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true, Message = "" };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_LoanWriteOffEntity finance_LoanWriteOffEntity = db.FindEntity<Finance_LoanWriteOffEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_LoanWriteOffEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                finance_LoanWriteOffEntity.AuditStatus = "2";
                db.Update<Finance_LoanWriteOffEntity>(finance_LoanWriteOffEntity);

                Finance_LoanEntity finance_LoanEntity = db.FindEntity<Finance_LoanEntity>(finance_LoanWriteOffEntity.Fk_LoanId);
                if (finance_LoanEntity == null)
                {
                    throw new Exception(finance_LoanEntity.Code + ":借款单不存在");
                }

                if (finance_LoanWriteOffEntity.WriteOffAmount.HasValue && finance_LoanEntity.PayedAmount.HasValue && finance_LoanEntity.PayedAmount.Value < finance_LoanWriteOffEntity.WriteOffAmount.Value)
                {
                    throw new Exception(finance_LoanEntity.Code + ":借款单已支付金额小于当前冲销金额");
                }

                UserEntity userEntity = db.FindEntity<UserEntity>(finance_LoanEntity.Loaner);
                if (userEntity == null)
                {
                    throw new Exception(userEntity.F_UserId + ":借款人不存在");
                }
                if (!userEntity.Loan.HasValue)
                    userEntity.Loan = 0;

                if (finance_LoanWriteOffEntity.WriteOffAmount.HasValue && userEntity.Loan.HasValue && userEntity.Loan.Value < finance_LoanWriteOffEntity.WriteOffAmount.Value)
                {
                    throw new Exception(userEntity.F_UserId + ":用户未清借款小于当前冲销金额");
                }

                //if (finance_LoanWriteOffEntity.WriteOffAmount.HasValue)
                    userEntity.Loan -= finance_LoanWriteOffEntity.WriteOffAmount.Value;

                db.Update<UserEntity>(userEntity);
                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true, Message = "" };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                Finance_LoanWriteOffEntity finance_LoanWriteOffEntity = db.FindEntity<Finance_LoanWriteOffEntity>(keyValue);

                finance_LoanWriteOffEntity.AuditStatus = "4";
                db.Delete<Finance_LoanWriteOffEntity>(finance_LoanWriteOffEntity);

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_LoanWriteOffEntity entity,List<Finance_LoanWriteOffDetailsEntity> finance_LoanWriteOffDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var finance_LoanWriteOffEntityTmp = GetFinance_LoanWriteOffEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Finance_LoanWriteOffDetailsUpdateList= finance_LoanWriteOffDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Finance_LoanWriteOffDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_LoanWriteOffDetailsInserList= finance_LoanWriteOffDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Finance_LoanWriteOffDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.Fk_LoanWriteOffId = finance_LoanWriteOffEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_LoanWriteOffDetailsEntity item in finance_LoanWriteOffDetailsList)
                    {
                        item.Fk_LoanWriteOffId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
