﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-06-29 18:37
    /// 描 述：借款冲销
    /// </summary>
    public interface Finance_LoanWriteOffIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_LoanWriteOffEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取Finance_LoanWriteOffDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_LoanWriteOffDetailsEntity> GetFinance_LoanWriteOffDetailsList(string keyValue);
        /// <summary>
        /// 获取Finance_LoanWriteOff表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_LoanWriteOffEntity GetFinance_LoanWriteOffEntity(string keyValue);
        /// <summary>
        /// 获取Finance_LoanWriteOffDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_LoanWriteOffDetailsEntity GetFinance_LoanWriteOffDetailsEntity(string keyValue);
        #endregion

        #region  提交数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_LoanWriteOffEntity entity,List<Finance_LoanWriteOffDetailsEntity> finance_LoanWriteOffDetailsList,string deleteList,string type);
        #endregion

    }
}
