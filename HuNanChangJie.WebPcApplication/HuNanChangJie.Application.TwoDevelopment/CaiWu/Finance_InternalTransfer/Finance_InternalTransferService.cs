﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-02 20:14
    /// 描 述：内部转账
    /// </summary>
    public class Finance_InternalTransferService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_InternalTransferEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.JingBanRen,
                t.TransferDate,
                t.TransferAmount,
                t.Abstract,
                t.AuditStatus,
                t.Workflow_ID,CreationDate
                ");
                strSql.Append("  FROM Finance_InternalTransfer t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID Like @ProjectID ");
                }

                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                //if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                //{
                //    pagination.sidx = "CreationDate";
                //    pagination.sord = "DESC";
                //}

                var list = this.BaseRepository().FindList<Finance_InternalTransferEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InternalTransferDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_InternalTransferDetailsEntity> GetFinance_InternalTransferDetailsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_InternalTransferDetailsEntity>($"select a.*,b.Bank + '(' + b.Name + ')' as TransferredAccountBankNames,c.Bank + '(' + c.Name + ')' as BankNames, d.projectname TransferredAccountProject,e.projectname TransferAccountProject from Finance_InternalTransferdetails a left join Base_FundAccount b on a.TransferredAccount = b.id left join Base_FundAccount c on a.TransferAccount = c.id left join base_cj_project d on b.projectid = d.id  left join base_cj_project e on c.projectid = e.id  where a.Fk_InternalTransferId='{keyValue}'");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InternalTransfer表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InternalTransferEntity GetFinance_InternalTransferEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_InternalTransferEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InternalTransferDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InternalTransferDetailsEntity GetFinance_InternalTransferDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_InternalTransferDetailsEntity>($"select a.*,d.projectname TransferredAccountProject,e.projectname TransferAccountProject from Finance_InternalTransferdetails a left join Base_FundAccount b on a.TransferredAccount = b.id left join Base_FundAccount c on a.TransferAccount = c.id left join base_cj_project d on b.projectid = d.id  left join base_cj_project e on c.projectid = e.id  where a.id='{keyValue}'");
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据


        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_InternalTransferEntity finance_InternalTransferEntity = db.FindEntity<Finance_InternalTransferEntity>(keyValue);

                List<Finance_InternalTransferDetailsEntity> finance_InternalTransferDetailsEntity = db.FindList<Finance_InternalTransferDetailsEntity>(m => m.Fk_InternalTransferId == keyValue).ToList();

                if (finance_InternalTransferEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }

                finance_InternalTransferEntity.AuditStatus = "4";
                db.Update<Finance_InternalTransferEntity>(finance_InternalTransferEntity);

                int finance_InternalTransferDetailsEntityCount = finance_InternalTransferDetailsEntity.Count;
                int addCount = 0;

                string errorMessage = "";
                foreach (var item in finance_InternalTransferDetailsEntity)
                {
                    Base_FundAccountEntity transferAccount = db.FindEntity<Base_FundAccountEntity>(item.TransferAccount);
                    Base_FundAccountEntity transferredAccount = db.FindEntity<Base_FundAccountEntity>(item.TransferredAccount);
                    if (transferAccount == null || transferredAccount == null)
                        continue;
                    var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = item.TransferAccount,
                        AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                        OccurrenceType = OccurrenceEnum.Outlay,
                        RelevanceType = RelevanceEnum.Other,
                        ReceiptType = ReceiptEnum.Interior,
                        RelevanceId = item.ID,
                        ReceiptId = item.ID,
                        Abstract = $"{finance_InternalTransferEntity.Abstract}[内部转账]",
                        ProjectId = transferAccount.ProjectId
                    }, db);

                    var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = item.TransferredAccount,
                        AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                        OccurrenceType = OccurrenceEnum.Income,
                        RelevanceType = RelevanceEnum.Other,
                        ReceiptType = ReceiptEnum.Interior,
                        RelevanceId = item.ID,
                        ReceiptId = item.ID,
                        Abstract = $"{finance_InternalTransferEntity.Abstract}[内部转账]",
                        ProjectId = transferredAccount.ProjectId
                    }, db);


                    if (result.Success && result1.Success)
                    {
                        addCount += 1;
                    }
                    if (!result.Success)
                    {
                        errorMessage += result.Message;
                    }
                    if (!result1.Success)
                    {
                        errorMessage += result.Message;
                    }
                }

                if (finance_InternalTransferDetailsEntityCount == addCount)
                {
                    db.Commit();
                    return new Util.Common.OperateResultEntity() { Success = true, Message = "" };
                }
                else
                {
                    db.Rollback();
                    return new Util.Common.OperateResultEntity() { Success = false, Message = "审核失败" + errorMessage };
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_InternalTransferEntity finance_InternalTransferEntity = db.FindEntity<Finance_InternalTransferEntity>(keyValue);

                List<Finance_InternalTransferDetailsEntity> finance_InternalTransferDetailsEntity = db.FindList<Finance_InternalTransferDetailsEntity>(m => m.Fk_InternalTransferId == keyValue).ToList();

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_InternalTransferEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}

                finance_InternalTransferEntity.AuditStatus = "2";
                db.Update<Finance_InternalTransferEntity>(finance_InternalTransferEntity);

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();

                 
                foreach (var item in finance_InternalTransferDetailsEntity)
                {
                    var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = item.TransferredAccount,
                        AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                        OccurrenceType = OccurrenceEnum.Outlay,
                        RelevanceType = RelevanceEnum.Other,
                        ReceiptType = ReceiptEnum.Interior,
                        RelevanceId = item.ID,
                        ReceiptId = item.ID,
                        Abstract = $"{finance_InternalTransferEntity.Abstract}[内部转账]",
                        ProjectId = ""
                    }, db);

                    var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = item.TransferAccount,
                        AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                        OccurrenceType = OccurrenceEnum.Income,
                        RelevanceType = RelevanceEnum.Other,
                        ReceiptType = ReceiptEnum.Interior,
                        RelevanceId = item.ID,
                        ReceiptId = item.ID,
                        Abstract = $"{finance_InternalTransferEntity.Abstract}[内部转账]",
                        ProjectId = ""
                    }, db);

                    if (!result.Success)
                    {
                        resultList.Add(result);
                    }
                    if (!result1.Success)
                    {
                        resultList.Add(result1);
                    }
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }
                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true, Message = "" };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }


        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_InternalTransferEntity = GetFinance_InternalTransferEntity(keyValue);
                db.Delete<Finance_InternalTransferEntity>(t => t.ID == keyValue);
                db.Delete<Finance_InternalTransferDetailsEntity>(t => t.Fk_InternalTransferId == finance_InternalTransferEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_InternalTransferEntity entity, List<Finance_InternalTransferDetailsEntity> finance_InternalTransferDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var finance_InternalTransferEntityTmp = GetFinance_InternalTransferEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Finance_InternalTransferDetailsUpdateList = finance_InternalTransferDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Finance_InternalTransferDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_InternalTransferDetailsInserList = finance_InternalTransferDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Finance_InternalTransferDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.Fk_InternalTransferId = finance_InternalTransferEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_InternalTransferDetailsEntity item in finance_InternalTransferDetailsList)
                    {
                        item.Fk_InternalTransferId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 发送流程前数据检查
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns>true:可以发送流程 false:不可以发送流程</returns>
        internal OperateResultEntity BeforeSendingCheckData(string keyValue)
        {
            try
            {
                var op = new OperateResultEntity();
                op.Success = true;
                var details = this.BaseRepository().FindList<Finance_InternalTransferDetailsEntity>(i => i.Fk_InternalTransferId == keyValue);
                if (details.Any())
                {
                    foreach (var item in details)
                    {
                        var info = BaseRepository().FindEntity<Base_FundAccountEntity>(i => i.ID == item.TransferredAccount);
                        if (item.Amount > info.UsableBalance)
                        {
                            op.Success = false;
                            op.Message = $"流程发送失败，当前账记的可用余额不足以完成【{item.Amount}】的转账";
                            return op;
                        }

                    }
                }
                else
                {
                    op.Success = false;
                    op.Message = "流程发送失败，未检查到详情数据";
                }
                return op;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
