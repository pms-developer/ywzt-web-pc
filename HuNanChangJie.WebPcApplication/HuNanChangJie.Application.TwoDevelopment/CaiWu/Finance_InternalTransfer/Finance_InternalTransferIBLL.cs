﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-02 20:14
    /// 描 述：内部转账
    /// </summary>
    public interface Finance_InternalTransferIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_InternalTransferEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_InternalTransferDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_InternalTransferDetailsEntity> GetFinance_InternalTransferDetailsList(string keyValue);
        /// <summary>
        /// 获取Finance_InternalTransfer表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_InternalTransferEntity GetFinance_InternalTransferEntity(string keyValue);
        /// <summary>
        /// 获取Finance_InternalTransferDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_InternalTransferDetailsEntity GetFinance_InternalTransferDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);

        /// <summary>
        /// 发送流程前数据检查
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns>true:可以发送流程 false:不可以发送流程</returns>
        OperateResultEntity BeforeSendingCheckData(string keyValue);

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_InternalTransferEntity entity,List<Finance_InternalTransferDetailsEntity> finance_InternalTransferDetailsList,string deleteList,string type);
        #endregion

    }
}
