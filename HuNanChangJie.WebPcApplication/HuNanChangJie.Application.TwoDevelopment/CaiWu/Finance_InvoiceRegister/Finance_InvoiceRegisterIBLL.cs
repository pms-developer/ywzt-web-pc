﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-29 09:58
    /// 描 述：开票登记
    /// </summary>
    public interface Finance_InvoiceRegisterIBLL
    {
        #region  获取数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_InvoiceRegisterEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_InvoiceRegister_Proceeds表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_InvoiceRegister_ProceedsEntity> GetFinance_InvoiceRegister_ProceedsList(string keyValue);
        /// <summary>
        /// 获取Finance_InvoiceRegister表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_InvoiceRegisterEntity GetFinance_InvoiceRegisterEntity(string keyValue);
        /// <summary>
        /// 获取Finance_InvoiceRegister_Proceeds表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_InvoiceRegister_ProceedsEntity GetFinance_InvoiceRegister_ProceedsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_InvoiceRegisterEntity entity,List<Finance_InvoiceRegister_ProceedsEntity> finance_InvoiceRegister_ProceedsList,string deleteList,string type);
        #endregion

    }
}
