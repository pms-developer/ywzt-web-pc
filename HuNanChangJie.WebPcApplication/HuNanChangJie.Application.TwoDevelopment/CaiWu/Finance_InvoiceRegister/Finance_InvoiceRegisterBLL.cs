﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-29 09:58
    /// 描 述：开票登记
    /// </summary>
    public class Finance_InvoiceRegisterBLL : Finance_InvoiceRegisterIBLL
    {
        private Finance_InvoiceRegisterService finance_InvoiceRegisterService = new Finance_InvoiceRegisterService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceRegisterEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return finance_InvoiceRegisterService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceRegister_Proceeds表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceRegister_ProceedsEntity> GetFinance_InvoiceRegister_ProceedsList(string keyValue)
        {
            try
            {
                return finance_InvoiceRegisterService.GetFinance_InvoiceRegister_ProceedsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceRegister表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceRegisterEntity GetFinance_InvoiceRegisterEntity(string keyValue)
        {
            try
            {
                return finance_InvoiceRegisterService.GetFinance_InvoiceRegisterEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceRegister_Proceeds表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceRegister_ProceedsEntity GetFinance_InvoiceRegister_ProceedsEntity(string keyValue)
        {
            try
            {
                return finance_InvoiceRegisterService.GetFinance_InvoiceRegister_ProceedsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return finance_InvoiceRegisterService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return finance_InvoiceRegisterService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                finance_InvoiceRegisterService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_InvoiceRegisterEntity entity,List<Finance_InvoiceRegister_ProceedsEntity> finance_InvoiceRegister_ProceedsList,string deleteList,string type)
        {
            try
            {
                finance_InvoiceRegisterService.SaveEntity(keyValue, entity,finance_InvoiceRegister_ProceedsList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
