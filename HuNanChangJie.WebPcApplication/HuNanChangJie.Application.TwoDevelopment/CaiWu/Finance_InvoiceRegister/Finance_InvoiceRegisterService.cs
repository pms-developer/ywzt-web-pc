﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.Extend;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-29 09:58
    /// 描 述：开票登记
    /// </summary>
    public class Finance_InvoiceRegisterService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceRegisterEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select t.ID,
                t.ProjectID,
                user2.F_RealName as ProjectManager,
                t.InvoiceCode,
                t.InvoiceType,
                t.FinanceInvoiceApplyId,
                t.MakeInvoiceCompanyId,
                t.InvoiceAmount,
                t.ProjectContractId,
                t.TaxRate,
                t.TaxAmount,
                user1.F_RealName as ApplyUserId,
                t.MakeInvoiceUserId,
                t.MakeInvoiceDate,
                t.BankId,t.Abstract,
                t.InvoiceCompany,
                t.AuditStatus,t.CreationDate,t.Workflow_ID,c.projectname,d.name ProjectContractName,e.fullname customername from Finance_InvoiceRegister t left join Base_User user1 on t.ApplyUserId=user1.F_UserId 
				 left join Base_User user2 on t.ProjectManager=user2.F_UserId left join base_cj_project c on t.ProjectID = c.id left join Project_Contract d on t.ProjectContractId =d.id left join base_customer e on d.jia= e.id 
WHERE 1 =1 ");
                var queryParam = queryJson.ToJObject();

                // 虚拟参数 
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID",queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }

                if (!queryParam["InvoiceCode"].IsEmpty())
                {
                    dp.Add("InvoiceCode", "%" + queryParam["InvoiceCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.InvoiceCode Like @InvoiceCode ");
                }
                if (!queryParam["ProjectContractName"].IsEmpty())
                {
                    dp.Add("ProjectContractName", "%" + queryParam["ProjectContractName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND c.projectname Like @ProjectContractName ");
                }
                if (!queryParam["customername"].IsEmpty())
                {
                    dp.Add("customername", "%" + queryParam["customername"].ToString() + "%", DbType.String);
                    strSql.Append(" AND e.fullname Like @customername ");
                }
                if (!queryParam["ProjectManager"].IsEmpty())
                {
                    dp.Add("ProjectManager", "%" + queryParam["ProjectManager"].ToString() + "%", DbType.String);
                    strSql.Append(" AND user2.F_RealName Like @ProjectManager ");
                }
                if (!queryParam["InvoiceType"].IsEmpty())
                {
                    dp.Add("InvoiceType", "%" + queryParam["InvoiceType"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.InvoiceType Like @InvoiceType ");
                }
                if (!queryParam["ProjectName"].IsEmpty())
                {
                    dp.Add("ProjectName", "%" + queryParam["ProjectName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.InvoiceType Like @ProjectName ");
                }
                if (!queryParam["Abstract"].IsEmpty())
                {
                    dp.Add("Abstract", "%" + queryParam["Abstract"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Abstract Like @Abstract ");
                }

                if (!queryParam["ApplyUserId"].IsEmpty())
                {
                    dp.Add("ApplyUserId", "%" + queryParam["ApplyUserId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND user1.F_RealName Like @ApplyUserId ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.MakeInvoiceDate >= @startTime AND t.MakeInvoiceDate <= @endTime ) ");
                }
                if (pagination !=null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }

                var list =this.BaseRepository().FindList<Finance_InvoiceRegisterEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceRegister_Proceeds表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceRegister_ProceedsEntity> GetFinance_InvoiceRegister_ProceedsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Finance_InvoiceRegister_ProceedsEntity>("select a.*,b.Code, b.ReceivablesDate, b.WrittenOff, b.UnWrittenOff,b.PreceedsAmount from Finance_InvoiceRegister_Proceeds a left join Finance_Proceeds b on a.Fk_ProceedsId = b.ID where Fk_InvoiceRegisterID = '" + keyValue +"'");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceRegister表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceRegisterEntity GetFinance_InvoiceRegisterEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_InvoiceRegisterEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceRegister_Proceeds表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceRegister_ProceedsEntity GetFinance_InvoiceRegister_ProceedsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_InvoiceRegister_ProceedsEntity>(t=>t.Fk_InvoiceRegisterID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_InvoiceRegisterEntity finance_InvoiceRegisterEntity = db.FindEntity<Finance_InvoiceRegisterEntity>(keyValue);
                if (finance_InvoiceRegisterEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_InvoiceRegisterEntity.AuditStatus = "4";

                if (!finance_InvoiceRegisterEntity.WrittenOff.HasValue)
                    finance_InvoiceRegisterEntity.WrittenOff = 0;
                if (!finance_InvoiceRegisterEntity.UnWrittenOff.HasValue)
                    finance_InvoiceRegisterEntity.UnWrittenOff = 0;
                //finance_InvoiceRegisterEntity.WrittenOff -= finance_InvoiceRegisterEntity.InvoiceAmount;

                //db.Update<Finance_InvoiceRegisterEntity>(finance_InvoiceRegisterEntity);



                Project_ContractEntity project_ContractEntity = db.FindEntity<Project_ContractEntity>(m => m.ID == finance_InvoiceRegisterEntity.ProjectContractId);
                project_ContractEntity.InvioceAmount = (project_ContractEntity.InvioceAmount ?? 0) - finance_InvoiceRegisterEntity.InvoiceAmount;
                project_ContractEntity.NoInvioceAmount = (project_ContractEntity.TotalAmount ?? 0) - project_ContractEntity.InvioceAmount;

                //project_ContractEntity.NoInvioceAmount = (project_ContractEntity.NoInvioceAmount ?? 0) + finance_InvoiceRegisterEntity.InvoiceAmount;
                db.Update<Project_ContractEntity>(project_ContractEntity);

                List<Finance_InvoiceRegister_ProceedsEntity> listFinance_InvoiceRegister_ProceedsEntity = db.FindList<Finance_InvoiceRegister_ProceedsEntity>(m => m.Fk_InvoiceRegisterID == keyValue).ToList();
                List<string> messageList = new List<string>();

                decimal writeOffAmount = 0;
                foreach (var item in listFinance_InvoiceRegister_ProceedsEntity)
                {
                    Finance_ProceedsEntity finance_ProceedsEntity = db.FindEntity<Finance_ProceedsEntity>(m => m.ID == item.Fk_ProceedsId);
                    if (finance_ProceedsEntity != null)
                    {
                        if (!finance_ProceedsEntity.WrittenOff.HasValue)
                            finance_ProceedsEntity.WrittenOff = 0;

                        if (!finance_ProceedsEntity.UnWrittenOff.HasValue)
                            finance_ProceedsEntity.UnWrittenOff = 0;

                        if (item.WriteOffAmount.HasValue)
                            writeOffAmount += item.WriteOffAmount.Value;
                        finance_ProceedsEntity.WrittenOff -= item.WriteOffAmount;
                        finance_ProceedsEntity.UnWrittenOff = finance_ProceedsEntity.PreceedsAmount + item.WriteOffAmount;

                        if (finance_ProceedsEntity.WrittenOff > finance_ProceedsEntity.PreceedsAmount)
                        {
                            messageList.Add(item.Code + ":总核销金额大于还款单金额");
                            continue;
                        }

                        if (finance_ProceedsEntity.WrittenOff < 0)
                        {
                            messageList.Add(item.ID + ":收款单本次核销金额大于已核销金额");
                            continue;
                        }

                        db.Update<Finance_ProceedsEntity>(finance_ProceedsEntity);
                    }
                }

                finance_InvoiceRegisterEntity.WrittenOff -= writeOffAmount;
                finance_InvoiceRegisterEntity.UnWrittenOff = finance_InvoiceRegisterEntity.InvoiceAmount - finance_InvoiceRegisterEntity.WrittenOff;
                db.Update<Finance_InvoiceRegisterEntity>(finance_InvoiceRegisterEntity);

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }
                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "IsAutoDeductTaxes");
                if (config == null || config.Value == false)
                {
                    db.Commit();
                }
                else
                {
                    var fundId = new AuditExtend(db, finance_InvoiceRegisterEntity.ProjectID, ReceiptEnum.Invoice, finance_InvoiceRegisterEntity.ID, keyValue).UnAudit();
                    db.Commit();
                    new Base_FundAccountBLL().VerificationFundBalanceAndFreeze(fundId);
                }

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_InvoiceRegisterEntity finance_InvoiceRegisterEntity = db.FindEntity<Finance_InvoiceRegisterEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_InvoiceRegisterEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}

                finance_InvoiceRegisterEntity.AuditStatus = "2";
                if (!finance_InvoiceRegisterEntity.WrittenOff.HasValue)
                    finance_InvoiceRegisterEntity.WrittenOff = 0;
                if (!finance_InvoiceRegisterEntity.UnWrittenOff.HasValue)
                    finance_InvoiceRegisterEntity.UnWrittenOff = 0;
               

                Project_ContractEntity project_ContractEntity = db.FindEntity<Project_ContractEntity>(m => m.ID == finance_InvoiceRegisterEntity.ProjectContractId);
                project_ContractEntity.InvioceAmount = (project_ContractEntity.InvioceAmount ?? 0) + finance_InvoiceRegisterEntity.InvoiceAmount;
                project_ContractEntity.NoInvioceAmount = (project_ContractEntity.TotalAmount ?? 0) - project_ContractEntity.InvioceAmount;
                db.Update<Project_ContractEntity>(project_ContractEntity);

                List<Finance_InvoiceRegister_ProceedsEntity> listFinance_InvoiceRegister_ProceedsEntity = db.FindList<Finance_InvoiceRegister_ProceedsEntity>(m => m.Fk_InvoiceRegisterID == keyValue).ToList();
                List<string> messageList = new List<string>();

                decimal writeOffAmount = 0;
                foreach (var item in listFinance_InvoiceRegister_ProceedsEntity)
                {
                    Finance_ProceedsEntity finance_ProceedsEntity = db.FindEntity<Finance_ProceedsEntity>(m => m.ID == item.Fk_ProceedsId);
                    if (finance_ProceedsEntity != null)
                    {
                        if (!finance_ProceedsEntity.WrittenOff.HasValue)
                            finance_ProceedsEntity.WrittenOff = 0;
                        if (!finance_ProceedsEntity.UnWrittenOff.HasValue)
                            finance_ProceedsEntity.UnWrittenOff = 0;

                        if(item.WriteOffAmount.HasValue)
                        writeOffAmount += item.WriteOffAmount.Value;

                        finance_ProceedsEntity.WrittenOff += item.WriteOffAmount;
                        finance_ProceedsEntity.UnWrittenOff = finance_ProceedsEntity.PreceedsAmount - item.WriteOffAmount;

                        if (finance_ProceedsEntity.WrittenOff > finance_ProceedsEntity.PreceedsAmount)
                        {
                            messageList.Add(item.Code + ":总核销金额大于还款单金额");
                            continue;
                        }

                        if (finance_ProceedsEntity.UnWrittenOff < 0)
                        {
                            messageList.Add(item.Code + ":收款单本次核销金额大于可核销金额");
                            continue;
                        }

                        db.Update<Finance_ProceedsEntity>(finance_ProceedsEntity);
                    }
                }

                finance_InvoiceRegisterEntity.WrittenOff += writeOffAmount;
                finance_InvoiceRegisterEntity.UnWrittenOff = finance_InvoiceRegisterEntity.InvoiceAmount - finance_InvoiceRegisterEntity.WrittenOff;
                db.Update<Finance_InvoiceRegisterEntity>(finance_InvoiceRegisterEntity);


                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }
                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "IsAutoDeductTaxes");
                if (config == null || config.Value==false)
                {
                    db.Commit();
                }
                else
                {
                    var fundId = new AuditExtend(db, finance_InvoiceRegisterEntity.ProjectID, ReceiptEnum.Invoice, finance_InvoiceRegisterEntity.ID, keyValue).Audit();
                    db.Commit();
                    new Base_FundAccountBLL().VerificationFundBalanceAndFreeze(fundId);
                }

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_InvoiceRegisterEntity = GetFinance_InvoiceRegisterEntity(keyValue); 
                db.Delete<Finance_InvoiceRegisterEntity>(t=>t.ID == keyValue);
                db.Delete<Finance_InvoiceRegister_ProceedsEntity>(t=>t.Fk_InvoiceRegisterID == finance_InvoiceRegisterEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_InvoiceRegisterEntity entity,List<Finance_InvoiceRegister_ProceedsEntity> finance_InvoiceRegister_ProceedsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var finance_InvoiceRegisterEntityTmp = GetFinance_InvoiceRegisterEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Finance_InvoiceRegister_ProceedsUpdateList= finance_InvoiceRegister_ProceedsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Finance_InvoiceRegister_ProceedsUpdateList)
                    {
                        if (!item.WriteOffAmount.HasValue)
                            item.WriteOffAmount = 0;

                        if (item.WriteOffAmount <= 0)
                            db.Delete(item);
                        else
                            db.Update(item);
                    }
                    var Finance_InvoiceRegister_ProceedsInserList= finance_InvoiceRegister_ProceedsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Finance_InvoiceRegister_ProceedsInserList)
                    {
                        if (!item.WriteOffAmount.HasValue || item.WriteOffAmount <= 0)
                            continue;
                        item.Create(item.ID);
                        item.Fk_InvoiceRegisterID = finance_InvoiceRegisterEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_InvoiceRegister_ProceedsEntity item in finance_InvoiceRegister_ProceedsList)
                    {
                        item.Fk_InvoiceRegisterID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
