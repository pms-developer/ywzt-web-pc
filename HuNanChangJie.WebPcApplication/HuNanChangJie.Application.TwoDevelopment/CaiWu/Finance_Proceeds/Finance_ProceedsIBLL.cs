﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 17:44
    /// 描 述：收款单管理
    /// </summary>
    public interface Finance_ProceedsIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_ProceedsEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_ProceedsAccount表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_ProceedsAccountEntity> GetFinance_ProceedsAccountList(string keyValue);
        /// <summary>
        /// 获取Finance_ProceedsAgreement表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_ProceedsAgreementEntity> GetFinance_ProceedsAgreementList(string keyValue);
        /// <summary>
        /// 获取Finance_ProceedsVerification表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_ProceedsVerificationEntity> GetFinance_ProceedsVerificationList(string keyValue);

        IEnumerable<Finance_ProceedsAgreementEntity> GetFinance_ProceedsAgreementListByContractid(string  contractid);
       
        IEnumerable<Finance_ProceedsVerificationEntity> GetFinance_ProceedsVerificationListByContractid(string contractid);

        /// <summary>
        /// 获取Finance_Proceeds表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_ProceedsEntity GetFinance_ProceedsEntity(string keyValue);
        /// <summary>
        /// 获取Finance_ProceedsAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_ProceedsAccountEntity GetFinance_ProceedsAccountEntity(string keyValue);
        /// <summary>
        /// 获取Finance_ProceedsAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_ProceedsAgreementEntity GetFinance_ProceedsAgreementEntity(string keyValue);
        /// <summary>
        /// 获取Finance_ProceedsVerification表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_ProceedsVerificationEntity GetFinance_ProceedsVerificationEntity(string keyValue);
        #endregion

        #region  提交数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_ProceedsEntity entity,List<Finance_ProceedsAccountEntity> finance_ProceedsAccountList,List<Finance_ProceedsAgreementEntity> finance_ProceedsAgreementList,List<Finance_ProceedsVerificationEntity> finance_ProceedsVerificationList,string deleteList,string type);
        #endregion

    }
}
