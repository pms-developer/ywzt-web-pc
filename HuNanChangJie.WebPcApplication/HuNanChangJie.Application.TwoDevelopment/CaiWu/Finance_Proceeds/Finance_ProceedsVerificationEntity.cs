﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 17:44
    /// 描 述：收款单管理
    /// </summary>
    public class Finance_ProceedsVerificationEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }

        /// <summary>
        /// 收款单ID
        /// </summary>
        [Column("FINANCEPROCEEDSID")]
        public string FinanceProceedsId { get; set; }

        /// <summary>
        /// 开票申请ID
        /// </summary>
        [Column("FK_INVOICEAPPLYID")]
        public string Fk_InvoiceApplyId { get; set; }

        /// <summary>
        /// 核销金额
        /// </summary>
        [Column("AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Amount { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// 发票编号
        /// </summary>
        [NotMapped]
        public string InvoiceCode { get; set; }

        [NotMapped]
        public DateTime? MakeInvoiceDate { get; set; }

        /// <summary>
        /// 发票类型
        /// </summary>
        [NotMapped]
        public string InvoiceType { get; set; }

        /// <summary>
        /// 开票金额
        /// </summary>
        [NotMapped]
        public decimal? InvoiceAmount { get; set; }

        [NotMapped]
        public decimal? WrittenOff { get; set; }

        [NotMapped]
        public decimal? UnWrittenOff { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

