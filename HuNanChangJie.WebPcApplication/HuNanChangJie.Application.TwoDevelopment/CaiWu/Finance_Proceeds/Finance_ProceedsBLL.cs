﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 17:44
    /// 描 述：收款单管理
    /// </summary>
    public class Finance_ProceedsBLL : Finance_ProceedsIBLL
    {
        private Finance_ProceedsService finance_ProceedsService = new Finance_ProceedsService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_ProceedsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return finance_ProceedsService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsAccount表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_ProceedsAccountEntity> GetFinance_ProceedsAccountList(string keyValue)
        {
            try
            {
                return finance_ProceedsService.GetFinance_ProceedsAccountList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsAgreement表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_ProceedsAgreementEntity> GetFinance_ProceedsAgreementList(string keyValue)
        {
            try
            {
                return finance_ProceedsService.GetFinance_ProceedsAgreementList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsVerification表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_ProceedsVerificationEntity> GetFinance_ProceedsVerificationList(string keyValue)
        {
            try
            {
                return finance_ProceedsService.GetFinance_ProceedsVerificationList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public IEnumerable<Finance_ProceedsAgreementEntity> GetFinance_ProceedsAgreementListByContractid(string contractid)
        {
            try
            {
                return finance_ProceedsService.GetFinance_ProceedsAgreementListByContractid(contractid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Finance_ProceedsVerificationEntity> GetFinance_ProceedsVerificationListByContractid(string contractid)
        {
            try
            {
                return finance_ProceedsService.GetFinance_ProceedsVerificationListByContractid(contractid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取Finance_Proceeds表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ProceedsEntity GetFinance_ProceedsEntity(string keyValue)
        {
            try
            {
                return finance_ProceedsService.GetFinance_ProceedsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ProceedsAccountEntity GetFinance_ProceedsAccountEntity(string keyValue)
        {
            try
            {
                return finance_ProceedsService.GetFinance_ProceedsAccountEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ProceedsAgreementEntity GetFinance_ProceedsAgreementEntity(string keyValue)
        {
            try
            {
                return finance_ProceedsService.GetFinance_ProceedsAgreementEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsVerification表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ProceedsVerificationEntity GetFinance_ProceedsVerificationEntity(string keyValue)
        {
            try
            {
                return finance_ProceedsService.GetFinance_ProceedsVerificationEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return finance_ProceedsService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return finance_ProceedsService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                finance_ProceedsService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_ProceedsEntity entity,List<Finance_ProceedsAccountEntity> finance_ProceedsAccountList,List<Finance_ProceedsAgreementEntity> finance_ProceedsAgreementList,List<Finance_ProceedsVerificationEntity> finance_ProceedsVerificationList,string deleteList,string type)
        {
            try
            {
                finance_ProceedsService.SaveEntity(keyValue, entity,finance_ProceedsAccountList,finance_ProceedsAgreementList,finance_ProceedsVerificationList,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0018");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
