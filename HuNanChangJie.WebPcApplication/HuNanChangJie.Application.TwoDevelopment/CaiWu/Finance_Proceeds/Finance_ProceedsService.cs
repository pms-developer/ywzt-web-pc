﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.Extend;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 17:44
    /// 描 述：收款单管理
    /// </summary>
    public class Finance_ProceedsService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_ProceedsEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
              
                strSql.Append(@"select t.ID,
                d.F_RealName ProjectManager,
                t.Code,
                t.ProceedsType,
                t.CreationDate, t.ReceivablesDate,
                t.ProjectContractId,
                b.name ProjectContractName,
                ff.ProjectName,
                t.LoanBillId,
                t.PreceedsAmount,
                t.ProceedsCompany,
                c.fullname Jia,
                t.Abstract,
                t.AuditStatus,
                t.Workflow_ID,e.F_RealName OperatorId,
				(select top 1 bb.bank from Finance_ProceedsAccount aa left join Base_FundAccount bb  on aa.bank = bb.id where aa.FinanceProceedsid=t.id) PreceedsZhanghu ,
				(select top 1 aa.PaymentType from Finance_ProceedsAccount aa left join Base_FundAccount bb  on aa.bank = bb.id where aa.FinanceProceedsid=t.id) PreceedsType 
				  FROM Finance_Proceeds t left join Project_Contract b on t.ProjectContractId = b.id 
				  left join base_cj_project ff on b.projectid = ff.id 
				  left join Base_Customer c on b.jia = c.id left join base_user d on t.ProjectManager = d.F_UserId 
			 left join Base_User e on t.OperatorId=e.F_UserId WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Jia"].IsEmpty())
                {
                    dp.Add("Jia", "%" + queryParam["Jia"].ToString() + "%", DbType.String);
                    strSql.Append(" AND c.fullname Like @Jia ");
                }
                if (!queryParam["ProjectManager"].IsEmpty())
                {
                    dp.Add("ProjectManager", "%" + queryParam["ProjectManager"].ToString() + "%", DbType.String);
                    strSql.Append(" AND d.F_RealName Like @ProjectManager ");
                }
                if (!queryParam["SAmount"].IsEmpty())
                {
                    dp.Add("SAmount", queryParam["SAmount"].ToString(), DbType.Decimal);
                    strSql.Append(" AND t.PreceedsAmount >= @SAmount ");
                }
                if (!queryParam["EAmount"].IsEmpty())
                {
                    dp.Add("EAmount", queryParam["EAmount"].ToString(), DbType.Decimal);
                    strSql.Append(" AND t.PreceedsAmount <= @EAmount ");
                }
               
                if (!queryParam["ProceedsType"].IsEmpty())
                {
                    dp.Add("ProceedsType", queryParam["ProceedsType"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProceedsType = @ProceedsType ");
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }

                var list = this.BaseRepository().FindList<Finance_ProceedsEntity>(strSql.ToString(), dp, pagination);

                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsAccount表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_ProceedsAccountEntity> GetFinance_ProceedsAccountList(string keyValue)
        {
            try
            {
                string strSql = "SELECT t.*,fa.Bank as openbank, fa.Bank + '('+fa.Name+')' as BankNames, fa.bankcode,fa.holder FROM Finance_ProceedsAccount as t join Base_FundAccount as fa on t.Bank=fa.ID WHERE t.FinanceProceedsId='" + keyValue + "'";
                var list = this.BaseRepository().FindList<Finance_ProceedsAccountEntity>(strSql.ToString());
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;


                //var list= this.BaseRepository().FindList<Finance_ProceedsAccountEntity>(t=>t.FinanceProceedsId == keyValue );
                //var query = from item in list orderby item.CreationDate ascending select item;
                //return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public IEnumerable<Finance_ProceedsAgreementEntity> GetFinance_ProceedsAgreementListByContractid(string contractid)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_ProceedsAgreementEntity>("select FundType,PaymentType,ProceedsCondition,PaymentPercent,Amount sAmount,PracticalAmount,id ProjectContractAgreementId from Project_ContractAgreement where ProjectContractId = '" + contractid + "' order by SortCode ");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsVerification表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_ProceedsVerificationEntity> GetFinance_ProceedsVerificationListByContractid(string contractid)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_ProceedsVerificationEntity>("select InvoiceCode,MakeInvoiceDate,InvoiceType,InvoiceAmount,WrittenOff,UnWrittenOff,id Fk_InvoiceApplyId from Finance_InvoiceRegister  where ProjectContractId = '" + contractid + "' and AuditStatus = 2 order by MakeInvoiceDate");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsAgreement表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_ProceedsAgreementEntity> GetFinance_ProceedsAgreementList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_ProceedsAgreementEntity>("select a.*,b.FundType,b.PaymentType,b.ProceedsCondition,b.PaymentPercent,b.Amount,b.PracticalAmount from Finance_ProceedsAgreement a left join Project_ContractAgreement b on a.ProjectContractAgreementId = b.id where a.FinanceProceedsId = '" + keyValue + "'");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsVerification表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_ProceedsVerificationEntity> GetFinance_ProceedsVerificationList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_ProceedsVerificationEntity>("select a.*,b.InvoiceCode,MakeInvoiceDate,InvoiceType,InvoiceAmount,WrittenOff,UnWrittenOff from Finance_ProceedsVerification a left join Finance_InvoiceRegister b on a.Fk_InvoiceApplyId = b.ID where FinanceProceedsId = '" + keyValue + "'");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_Proceeds表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ProceedsEntity GetFinance_ProceedsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_ProceedsEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ProceedsAccountEntity GetFinance_ProceedsAccountEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_ProceedsAccountEntity>(t => t.FinanceProceedsId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ProceedsAgreementEntity GetFinance_ProceedsAgreementEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_ProceedsAgreementEntity>(t => t.FinanceProceedsId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_ProceedsVerification表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_ProceedsVerificationEntity GetFinance_ProceedsVerificationEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_ProceedsVerificationEntity>(t => t.FinanceProceedsId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {

                Finance_ProceedsEntity finance_ProceedsEntity = db.FindEntity<Finance_ProceedsEntity>(keyValue);
                if (finance_ProceedsEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_ProceedsEntity.AuditStatus = "4";
                db.Update<Finance_ProceedsEntity>(finance_ProceedsEntity);

                Project_ContractEntity project_ContractEntity = db.FindEntity<Project_ContractEntity>(m => m.ID == finance_ProceedsEntity.ProjectContractId);
                project_ContractEntity.ReceivedAmount = (project_ContractEntity.ReceivedAmount ?? 0) - finance_ProceedsEntity.PreceedsAmount;
                db.Update<Project_ContractEntity>(project_ContractEntity);

               
                List<string> messageList = new List<string>();
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();

                IEnumerable<Finance_ProceedsAccountEntity> finance_ProceedsAccountEntityList = db.FindList<Finance_ProceedsAccountEntity>(m => m.FinanceProceedsId == keyValue);
                foreach (Finance_ProceedsAccountEntity item in finance_ProceedsAccountEntityList)
                {
                    var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = item.Bank,
                        AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                        OccurrenceType = OccurrenceEnum.Outlay,
                        RelevanceType = RelevanceEnum.Project,
                        ReceiptType = ReceiptEnum.ProjectContract,
                        RelevanceId = finance_ProceedsEntity.ID,
                        ReceiptId = item.ID,
                        Abstract = $"{finance_ProceedsEntity.Abstract}[收款单取消审核]",
                        ProjectId = finance_ProceedsEntity.ProjectID
                    }, db, false);

                    if (!result.Success)
                        resultList.Add(result);

                    var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceProceeds");
                    if (configInfo != null)
                    {
                        var flag = configInfo.Value;

                        if (flag)
                        {
                            Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == project_ContractEntity.ProjectID);

                            if (base_FundAccountEntity != null)
                            {
                                var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                {
                                    FundAccountFlag = base_FundAccountEntity.ID,
                                    AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                                    OccurrenceType = OccurrenceEnum.Outlay,
                                    RelevanceType = RelevanceEnum.Project,
                                    ReceiptType = ReceiptEnum.ProjectContract,
                                    RelevanceId = finance_ProceedsEntity.ID,
                                    ReceiptId = item.ID,
                                    Abstract = $"{finance_ProceedsEntity.Abstract}[收款单取消审核]",
                                    ProjectId = finance_ProceedsEntity.ProjectID
                                }, db, false);
                                if (!result1.Success)
                                    resultList.Add(result1);
                            }
                        }
                    }

                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                IEnumerable<Finance_ProceedsAgreementEntity> finance_ProceedsAgreementEntityList = db.FindList<Finance_ProceedsAgreementEntity>(m => m.FinanceProceedsId == keyValue);
                foreach (Finance_ProceedsAgreementEntity item in finance_ProceedsAgreementEntityList)
                {
                    Project_ContractAgreementEntity project_ContractAgreementEntity = db.FindEntity<Project_ContractAgreementEntity>(item.ProjectContractAgreementId);
                    if (!project_ContractAgreementEntity.PracticalAmount.HasValue)
                        project_ContractAgreementEntity.PracticalAmount = 0;

                    project_ContractAgreementEntity.PracticalAmount -= item.Amount;
                    db.Update<Project_ContractAgreementEntity>(project_ContractAgreementEntity);
                }

                IEnumerable<Finance_ProceedsVerificationEntity> finance_ProceedsVerificationList = db.FindList<Finance_ProceedsVerificationEntity>(m => m.FinanceProceedsId == keyValue);
                foreach (Finance_ProceedsVerificationEntity item in finance_ProceedsVerificationList)
                {
                    Finance_InvoiceRegisterEntity finance_InvoiceRegisterEntity = db.FindEntity<Finance_InvoiceRegisterEntity>(item.Fk_InvoiceApplyId);
                    if(finance_InvoiceRegisterEntity != null)
                    {
                        if (!finance_InvoiceRegisterEntity.WrittenOff.HasValue)
                            finance_InvoiceRegisterEntity.WrittenOff = 0;
                        if (!finance_InvoiceRegisterEntity.UnWrittenOff.HasValue)
                            finance_InvoiceRegisterEntity.UnWrittenOff = 0;

                        finance_InvoiceRegisterEntity.WrittenOff -= item.Amount;
                        finance_InvoiceRegisterEntity.UnWrittenOff = finance_InvoiceRegisterEntity.InvoiceAmount - finance_InvoiceRegisterEntity.WrittenOff;

                        db.Update<Finance_InvoiceRegisterEntity>(finance_InvoiceRegisterEntity);
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }


                if (finance_ProceedsEntity.ProceedsType == "合同收款")
                {
                    var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "IsAutoDeductTaxes");
                    if (config == null || config.Value == false)
                    {
                        db.Commit();
                    }
                    else
                    {
                        var fundId = new AuditExtend(db, finance_ProceedsEntity.ProjectID, ReceiptEnum.ProjectContract, finance_ProceedsEntity.ID, keyValue).UnAudit();
                        db.Commit();
                        new Base_FundAccountBLL().VerificationFundBalanceAndFreeze(fundId);
                    }
                }
                else
                {
                    db.Commit();
                }

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_ProceedsEntity finance_ProceedsEntity = db.FindEntity<Finance_ProceedsEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_ProceedsEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                finance_ProceedsEntity.AuditStatus = "2";
                db.Update<Finance_ProceedsEntity>(finance_ProceedsEntity);
                Project_ContractEntity project_ContractEntity = db.FindEntity<Project_ContractEntity>(m => m.ID == finance_ProceedsEntity.ProjectContractId);
                project_ContractEntity.ReceivedAmount = (project_ContractEntity.ReceivedAmount ?? 0) + finance_ProceedsEntity.PreceedsAmount;
                db.Update<Project_ContractEntity>(project_ContractEntity);
                List<string> messageList = new List<string>();
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
              
                IEnumerable<Finance_ProceedsAccountEntity> finance_ProceedsAccountEntityList = db.FindList<Finance_ProceedsAccountEntity>(m => m.FinanceProceedsId == keyValue);
                foreach (Finance_ProceedsAccountEntity item in finance_ProceedsAccountEntityList)
                {
                    var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = item.Bank,
                        AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                        OccurrenceType = OccurrenceEnum.Income,
                        RelevanceType = RelevanceEnum.Project,
                        ReceiptType = ReceiptEnum.ProjectContract,
                        RelevanceId = finance_ProceedsEntity.ID,
                        ReceiptId = item.ID,
                        Abstract = $"{finance_ProceedsEntity.Abstract}[收款单审核]",
                        ProjectId = finance_ProceedsEntity.ProjectID
                    }, db);

                    if (!result.Success)
                        resultList.Add(result);

                    var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceProceeds");
                    if (configInfo != null)
                    {
                        var flag = configInfo.Value;

                        if (flag)
                        {
                            Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == project_ContractEntity.ProjectID);

                            if (base_FundAccountEntity != null)
                            {
                                var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                {
                                    FundAccountFlag = base_FundAccountEntity.ID,
                                    AmountOccurrence = item.Amount.HasValue ? item.Amount.Value : 0,
                                    OccurrenceType = OccurrenceEnum.Income,
                                    RelevanceType = RelevanceEnum.Project,
                                    ReceiptType = ReceiptEnum.ProjectContract,
                                    RelevanceId = finance_ProceedsEntity.ID,
                                    ReceiptId = item.ID,
                                    Abstract = $"{finance_ProceedsEntity.Abstract}[收款单审核]",
                                    ProjectId = finance_ProceedsEntity.ProjectID
                                }, db, false);
                                if (!result1.Success)
                                    resultList.Add(result1);
                            }
                        }
                    }
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                IEnumerable<Finance_ProceedsAgreementEntity> finance_ProceedsAgreementEntityList = db.FindList<Finance_ProceedsAgreementEntity>(m => m.FinanceProceedsId == keyValue);
                foreach (Finance_ProceedsAgreementEntity item in finance_ProceedsAgreementEntityList)
                {
                    Project_ContractAgreementEntity project_ContractAgreementEntity = db.FindEntity<Project_ContractAgreementEntity>(item.ProjectContractAgreementId);
                    if (!project_ContractAgreementEntity.PracticalAmount.HasValue)
                        project_ContractAgreementEntity.PracticalAmount = 0;
                    project_ContractAgreementEntity.PracticalAmount += item.Amount;
                    db.Update<Project_ContractAgreementEntity>(project_ContractAgreementEntity);
                }

                IEnumerable<Finance_ProceedsVerificationEntity> finance_ProceedsVerificationList = db.FindList<Finance_ProceedsVerificationEntity>(m => m.FinanceProceedsId == keyValue);
                foreach (Finance_ProceedsVerificationEntity item in finance_ProceedsVerificationList)
                {
                    Finance_InvoiceRegisterEntity finance_InvoiceRegisterEntity = db.FindEntity<Finance_InvoiceRegisterEntity>(item.Fk_InvoiceApplyId);
                    if (!finance_InvoiceRegisterEntity.WrittenOff.HasValue)
                        finance_InvoiceRegisterEntity.WrittenOff = 0;
                    if (!finance_InvoiceRegisterEntity.UnWrittenOff.HasValue)
                        finance_InvoiceRegisterEntity.UnWrittenOff = 0;

                    finance_InvoiceRegisterEntity.WrittenOff += item.Amount;
                    finance_InvoiceRegisterEntity.UnWrittenOff = finance_InvoiceRegisterEntity.InvoiceAmount - finance_InvoiceRegisterEntity.WrittenOff;


                    //if (finance_InvoiceRegisterEntity.WrittenOff > item..PreceedsAmount)
                    //{
                    //    messageList.Add(item.Code + ":总核销金额大于还款单金额");
                    //    continue;
                    //}

                    //if (finance_ProceedsEntity.WrittenOff < 0)
                    //{
                    //    messageList.Add(item.ID + ":收款单本次核销金额大于已核销金额");
                    //    continue;
                    //}


                    db.Update<Finance_InvoiceRegisterEntity>(finance_InvoiceRegisterEntity);
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (finance_ProceedsEntity.ProceedsType == "合同收款")
                {
                    var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "IsAutoDeductTaxes");
                    if (config == null || config.Value == false)
                    {
                        db.Commit();
                    }
                    else
                    {
                        var fundId = new AuditExtend(db, finance_ProceedsEntity.ProjectID, ReceiptEnum.ProjectContract, finance_ProceedsEntity.ID, keyValue).Audit();
                        db.Commit();
                        new Base_FundAccountBLL().VerificationFundBalanceAndFreeze(fundId);
                    }
                }
                else
                {
                    db.Commit();
                }

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_ProceedsEntity = GetFinance_ProceedsEntity(keyValue);
                db.Delete<Finance_ProceedsEntity>(t => t.ID == keyValue);
                db.Delete<Finance_ProceedsAccountEntity>(t => t.FinanceProceedsId == finance_ProceedsEntity.ID);
                db.Delete<Finance_ProceedsAgreementEntity>(t => t.FinanceProceedsId == finance_ProceedsEntity.ID);
                db.Delete<Finance_ProceedsVerificationEntity>(t => t.FinanceProceedsId == finance_ProceedsEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_ProceedsEntity entity, List<Finance_ProceedsAccountEntity> finance_ProceedsAccountList, List<Finance_ProceedsAgreementEntity> finance_ProceedsAgreementList, List<Finance_ProceedsVerificationEntity> finance_ProceedsVerificationList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var finance_ProceedsEntityTmp = GetFinance_ProceedsEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Finance_ProceedsAccountUpdateList = finance_ProceedsAccountList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Finance_ProceedsAccountUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_ProceedsAccountInserList = finance_ProceedsAccountList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Finance_ProceedsAccountInserList)
                    {
                        item.Create(item.ID);
                        item.FinanceProceedsId = finance_ProceedsEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Finance_ProceedsAgreementUpdateList = finance_ProceedsAgreementList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Finance_ProceedsAgreementUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_ProceedsAgreementInserList = finance_ProceedsAgreementList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Finance_ProceedsAgreementInserList)
                    {
                        item.Create(item.ID);
                        item.FinanceProceedsId = finance_ProceedsEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码 
                    var Finance_ProceedsVerificationUpdateList = finance_ProceedsVerificationList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Finance_ProceedsVerificationUpdateList)
                    {
                        if (item.Amount <= 0)
                            db.Delete(item);
                        else
                            db.Update(item);
                    }
                    var Finance_ProceedsVerificationInserList = finance_ProceedsVerificationList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Finance_ProceedsVerificationInserList)
                    {
                        if (item.Amount <= 0)
                            continue;

                        item.Create(item.ID);
                        item.FinanceProceedsId = finance_ProceedsEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_ProceedsAccountEntity item in finance_ProceedsAccountList)
                    {
                        item.FinanceProceedsId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Finance_ProceedsAgreementEntity item in finance_ProceedsAgreementList)
                    {
                        item.FinanceProceedsId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Finance_ProceedsVerificationEntity item in finance_ProceedsVerificationList)
                    {
                        item.FinanceProceedsId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
