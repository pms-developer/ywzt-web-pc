﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 17:44
    /// 描 述：收款单管理
    /// </summary>
    public class Finance_ProceedsAgreementEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 收款单Id
        /// </summary>
        [Column("FINANCEPROCEEDSID")]
        public string FinanceProceedsId { get; set; }
        /// <summary>
        /// 合同收款协议Id
        /// </summary>
        [Column("PROJECTCONTRACTAGREEMENTID")]
        public string ProjectContractAgreementId { get; set; }
        /// <summary>
        /// 本次收款金额
        /// </summary>
        [Column("AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Amount { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATA")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 款项性质
        /// </summary>
        [NotMapped]
        public string FundType { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        [NotMapped]
        public string PaymentType { get; set; }
        /// <summary>
        /// 收款条件
        /// </summary>
        [NotMapped]
        public string ProceedsCondition { get; set; }
        /// <summary>
        /// 支付比例
        /// </summary>
        [NotMapped]
        public decimal? PaymentPercent { get; set; }
        /// <summary>
        /// 协议收款金额
        /// </summary>
        [NotMapped]
        public decimal? SAmount { get; set; }
        /// <summary>
        /// 实际收款金额
        /// </summary>
        [NotMapped]
        public decimal? PracticalAmount { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

