﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 17:44
    /// 描 述：收款单管理
    /// </summary>
    public class Finance_ProceedsAccountEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 收款ID
        /// </summary>
        [Column("FINANCEPROCEEDSID")]
        public string FinanceProceedsId { get; set; }
        /// <summary>
        /// 支付式
        /// </summary>
        [Column("PAYMENTTYPE")]
        public string PaymentType { get; set; }
        /// <summary>
        /// 开户行
        /// </summary>
        [Column("BANK")]
        public string Bank { get; set; }
        /// <summary>
        /// 账行账号/票据号
        /// </summary>
        [Column("BANKACCOUNT")]
        public string BankAccount { get; set; }
        /// <summary>
        /// 收款金额
        /// </summary>
        [Column("AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Amount { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        #endregion

        #region  扩展操作
       [NotMapped]
        public string OpenBank { get; set; }
        [NotMapped]
        public string BankCode { get; set; }
        [NotMapped]
        public string Holder { get; set; }

        [NotMapped]
        public string BankNames { get; set; }


        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

