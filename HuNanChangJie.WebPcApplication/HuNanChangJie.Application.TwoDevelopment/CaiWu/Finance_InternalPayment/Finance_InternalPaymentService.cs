﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu.Finance_InternalPayment;
using HuNanChangJie.Application.TwoDevelopment.Extend;
using HuNanChangJie.Application.Organization;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 16:04
    /// 描 述：对内付款单
    /// </summary>
    public class Finance_InternalPaymentService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        
            public IEnumerable<Finance_InternalPaymentSelectDanJuEntity> GetFuKuanList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select top 100 percent * from (
                            select  a.ID,'借款单' mode,code,applydate sdate,b.F_RealName realname,amount,loaner userid,b.F_DepartmentId deptid,
                            isnull(PayedAmount,0) payed, amount - isnull(PayedAmount,0) waitpay ,c.Name as AccountName,c.Id as AccountId,'' shoukuanmingcheng
                            from Finance_Loan a left join base_user b on a.Loaner = b.F_UserId 
                            left join Base_FundAccount c on a.ProjectID=c.ProjectId where AuditStatus = 2
                            union 
                            select a.ID,'报销单' ,code,baoxiaodate,b.F_RealName,baoxiaoamount,baoxiaouserid,b.F_DepartmentId ,isnull(PayedAmount,0), baoxiaoamount - isnull(PayedAmount,0) 
                            ,c.Name as AccountName,c.Id as AccountId,a.AccountName shoukuanmingcheng
                            from Finance_BaoXiao a left join base_user b on a.baoxiaouserid = b.F_UserId
                            left join Base_FundAccount c on a.ProjectID=c.ProjectId
                             where AuditStatus = 2 
                             ) as t 
                            where  waitpay >0 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND realname Like @Name ");
                }
                strSql.Append(" order by sdate");
                var list = this.BaseRepository().FindList<Finance_InternalPaymentSelectDanJuEntity>(strSql.ToString(), dp, pagination);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public Finance_InternalPaymentSelectDanJuEntity LoadDanJuData(int source,string keyid)
        {
            try
            {
                string sql = "";
                if (source == 1)
                {
                    sql = "select ID,'报销单' mode,code,baoxiaodate sdate, b.F_RealName realname, baoxiaoamount amount,baoxiaouserid userid, b.F_DepartmentId deptid, isnull(PayedAmount, 0) payed, baoxiaoamount - isnull(PayedAmount, 0) waitpay from Finance_BaoXiao a left join base_user b on a.baoxiaouserid = b.F_UserId where a.id = '" + keyid + "'  and AuditStatus = 2";
                }
                else
                {
                    sql = "select ID,'借款单' mode,code,applydate sdate, b.F_RealName realname, amount, loaner userid,b.F_DepartmentId deptid, isnull(PayedAmount, 0) payed, amount - isnull(PayedAmount, 0) waitpay from Finance_Loan a left join base_user b on a.Loaner = b.F_UserId where a.id = '" + keyid + "' and AuditStatus = 2";
                }

                var list = this.BaseRepository().FindTable(sql);
                if (list != null && list.Rows.Count > 0)
                {
                    DataRow dr = list.Rows[0];
                    decimal amount = 0;
                    decimal.TryParse(dr["amount"].ToString(), out amount);
                    DateTime sdate = DateTime.Now;
                    DateTime.TryParse(dr["sdate"].ToString(), out sdate);

                    decimal payed = 0;
                    decimal.TryParse(dr["payed"].ToString(), out payed);

                    decimal waitpay = 0;
                    decimal.TryParse(dr["waitpay"].ToString(), out waitpay);

                    return new Finance_InternalPaymentSelectDanJuEntity()
                    {
                        ID = dr["ID"].ToString(),
                        code = dr["code"].ToString(),
                        deptid = dr["deptid"].ToString(),
                        amount = amount,
                        mode = dr["mode"].ToString(),
                        realname = dr["realname"].ToString(),
                        sdate = sdate,
                        userid = dr["userid"].ToString(),
                        payed = payed,
                        waitpay = waitpay
                    };
                }
                return null;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_InternalPaymentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                b.f_realname OperatorId,
                t.PaymentDate,
                t.PaymentAmount,
                t.Abstract,
                t.AuditStatus,
                t.CreationDate,
                t.Workflow_ID
                ");
                strSql.Append("  FROM Finance_InternalPayment t left join base_user b on t.OperatorId = b.f_userid");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Abstract"].IsEmpty())
                {
                    dp.Add("Abstract", "%" + queryParam["Abstract"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Abstract Like @Abstract ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.PaymentDate >= @startTime AND t.PaymentDate <= @endTime ) ");
                }
                if (!queryParam["SAmount"].IsEmpty())
                {
                    dp.Add("SAmount", queryParam["SAmount"].ToString(), DbType.String);
                    strSql.Append(" AND t.PaymentAmount>=@SAmount");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                if (!queryParam["EAmount"].IsEmpty())
                {
                    dp.Add("EAmount", queryParam["EAmount"].ToString(), DbType.String);
                    strSql.Append(" AND t.PaymentAmount<=@EAmount");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list =this.BaseRepository().FindList<Finance_InternalPaymentEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public OperateResultEntity BeforeSendingCheckData(string keyValue)
        {
            try
            {
                var op = new OperateResultEntity();
                op.Success = true;
                var details = this.BaseRepository().FindList<Finance_InternalPaymentDetailsEntity>(i => i.FinanceInternalPaymentId == keyValue);
                if (details.Any())
                {
                    foreach (var item in details)
                    {
                        var info = BaseRepository().FindEntity<Base_FundAccountEntity>(i => i.ID == item.BankId);
                        if (item.PaymentAmount > info.UsableBalance)
                        {
                            op.Success = false;
                            op.Message = $"流程发送失败，当前账记的可用余额不足以完成【{item.PaymentAmount}】的转账";
                            return op;
                        }

                    }
                }
                else
                {
                    op.Success = false;
                    op.Message = "流程发送失败，未检查到详情数据";
                }
                return op;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InternalPaymentDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_InternalPaymentDetailsEntity> GetFinance_InternalPaymentDetailsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Finance_InternalPaymentDetailsEntity>("select a.*,fa.Bank as openbank,fa.Bank + '(' + fa.Name + ')' as BankNames, fa.bankcode,fa.holder,b.F_RealName PayeeName,b.F_DepartmentId bumeng from Finance_InternalPaymentDetails a left join Base_User b on a.PayeeId = b.F_UserId join Base_FundAccount as fa on a.BankId=fa.ID where a.FinanceInternalPaymentId = '" + keyValue+"'" );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InternalPayment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InternalPaymentEntity GetFinance_InternalPaymentEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_InternalPaymentEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InternalPaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InternalPaymentDetailsEntity GetFinance_InternalPaymentDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_InternalPaymentDetailsEntity>(t=>t.FinanceInternalPaymentId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_InternalPaymentEntity finance_InternalPaymentEntity = db.FindEntity<Finance_InternalPaymentEntity>(keyValue);

                if (finance_InternalPaymentEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_InternalPaymentEntity.AuditStatus = "4";

                db.Update<Finance_InternalPaymentEntity>(finance_InternalPaymentEntity);

                IEnumerable<Finance_InternalPaymentDetailsEntity> finance_InternalPaymentDetailsEntityList = db.FindList<Finance_InternalPaymentDetailsEntity>(m => m.FinanceInternalPaymentId == keyValue);
                List<string> messageList = new List<string>();

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();

                foreach (Finance_InternalPaymentDetailsEntity item in finance_InternalPaymentDetailsEntityList)
                {
                    string projectId = "", id = "";
                    ReceiptEnum receiptEnum = ReceiptEnum.Other;
                    if (item.BaoXiaoType == "借款单")
                    {
                        Finance_LoanEntity finance_LoanEntity = db.FindEntity<Finance_LoanEntity>(item.BaoXiaoId);
                        if (!finance_LoanEntity.PayedAmount.HasValue)
                            finance_LoanEntity.PayedAmount = 0;

                        //if (item.PaymentAmount > (finance_LoanEntity.Amount - finance_LoanEntity.PayedAmount))
                        //{
                        //    messageList.Add("单据" + item.Code + "付款金额大于待付金额");
                        //    continue;
                        //}

                        projectId = finance_LoanEntity.ProjectID;
                        id = finance_LoanEntity.ID;
                        receiptEnum = ReceiptEnum.Financing;

                        finance_LoanEntity.PayedAmount -= item.PaymentAmount;
                        finance_LoanEntity.PayStatus = "0";
                        db.Update<Finance_LoanEntity>(finance_LoanEntity);


                        UserEntity userEntity = db.FindEntity<UserEntity>(finance_LoanEntity.Loaner);
                        if (!userEntity.Loan.HasValue)
                            userEntity.Loan = 0;
                        userEntity.Loan -= finance_LoanEntity.Amount;
                        db.Update<UserEntity>(userEntity);

                        var des = $"{finance_InternalPaymentEntity.Abstract}[对内付款单取消审核]";
                        var auditExtend = new AuditPaymentExtend(db, projectId, receiptEnum, finance_InternalPaymentEntity.ID, id, item.PaymentAmount ?? 0, item.BankId);
                        var result = auditExtend.UnAudit(des);

                        if (!result.Success)
                            resultList.Add(result);


                               var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceInternalPayment");
                        if (configInfo != null)
                        {
                            var flag = configInfo.Value;

                            if (flag)
                            {
                                if (!string.IsNullOrEmpty(finance_LoanEntity.ProjectID))
                                {
                                    Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == finance_LoanEntity.ProjectID);

                                    if (base_FundAccountEntity != null)
                                    {
                                        var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                        {
                                            FundAccountFlag = base_FundAccountEntity.ID,
                                            AmountOccurrence = finance_LoanEntity.Amount.HasValue ? finance_LoanEntity.Amount.Value : 0,
                                            OccurrenceType = OccurrenceEnum.Income,
                                            RelevanceType = RelevanceEnum.Project,
                                            ReceiptType = receiptEnum,
                                            RelevanceId = finance_LoanEntity.ID,
                                            ReceiptId = item.ID,
                                            Abstract = des,
                                            ProjectId = projectId
                                        }, db, false);
                                        if (!result1.Success)
                                            resultList.Add(result);
                                    }
                                }
                            }
                        }
                    }
                    else if (item.BaoXiaoType == "报销单")
                    {
                        Finance_BaoXiaoEntity finance_BaoXiaoEntity = db.FindEntity<Finance_BaoXiaoEntity>(item.BaoXiaoId);

                        if (!finance_BaoXiaoEntity.PayedAmount.HasValue)
                            finance_BaoXiaoEntity.PayedAmount = 0;

                        //if (item.PaymentAmount > (finance_BaoXiaoEntity.BaoXiaoAmount - finance_BaoXiaoEntity.PayedAmount))
                        //{
                        //    messageList.Add("单据" + item.Code + "付款金额大于待付金额");
                        //    continue;
                        //}

                        projectId = finance_BaoXiaoEntity.ProjectID;
                        id = finance_BaoXiaoEntity.ID;
                        receiptEnum = ReceiptEnum.BaoXiao;

                        finance_BaoXiaoEntity.PayedAmount -= item.PaymentAmount;
                        finance_BaoXiaoEntity.IsPay = false;
                        db.Update<Finance_BaoXiaoEntity>(finance_BaoXiaoEntity);

                        var bxDetails = db.FindList<Finance_BaoXiaoDetailsEntity>(i => i.FinanceBaoXiaoId == item.BaoXiaoId);
                        var subjectId = "";//基础科目ID
                        decimal rate = 0;//税率
                        foreach (var info in bxDetails)
                        {
                            subjectId = info.SubjectId;
                            rate = info.TaxRate ?? 0;

                            var des = $"{finance_InternalPaymentEntity.Abstract}[对内付款单取消审核]";
                            var auditExtend = new AuditPaymentExtend(db, projectId, receiptEnum, finance_InternalPaymentEntity.ID, id, item.PaymentAmount ?? 0, item.BankId, subjectId, rate);
                            var result = auditExtend.UnAudit(des);

                            if (!result.Success)
                                resultList.Add(result);

                            var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceInternalPayment");
                            if (configInfo != null)
                            {
                                var flag = configInfo.Value;

                                if (flag)
                                {
                                    if (finance_BaoXiaoEntity.BaoXiaoType == "项目费用" && !string.IsNullOrEmpty(finance_BaoXiaoEntity.ProjectID))
                                    {
                                        Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == finance_BaoXiaoEntity.ProjectID);

                                        if (base_FundAccountEntity != null)
                                        {
                                            var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                            {
                                                FundAccountFlag = base_FundAccountEntity.ID,
                                                AmountOccurrence = finance_BaoXiaoEntity.BaoXiaoAmount.HasValue ? finance_BaoXiaoEntity.BaoXiaoAmount.Value : 0,
                                                OccurrenceType = OccurrenceEnum.Income,
                                                RelevanceType = RelevanceEnum.Project,
                                                ReceiptType = receiptEnum,
                                                RelevanceId = finance_BaoXiaoEntity.ID,
                                                ReceiptId = item.ID,
                                                Abstract =des,
                                                ProjectId = projectId
                                            }, db, false);
                                            if (!result1.Success)
                                                resultList.Add(result);
                                        }
                                    }
                                }
                            }
                           
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_InternalPaymentEntity finance_InternalPaymentEntity = db.FindEntity<Finance_InternalPaymentEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_InternalPaymentEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                finance_InternalPaymentEntity.AuditStatus = "2";
                
                db.Update(finance_InternalPaymentEntity);

                IEnumerable<Finance_InternalPaymentDetailsEntity> finance_InternalPaymentDetailsEntityList = db.FindList<Finance_InternalPaymentDetailsEntity>(m=>m.FinanceInternalPaymentId == keyValue);
                List<string> messageList = new List<string>();

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();


                foreach (Finance_InternalPaymentDetailsEntity item in finance_InternalPaymentDetailsEntityList)
                {
                    string projectId = "", id = "";
                    
                    ReceiptEnum receiptEnum = ReceiptEnum.Other;
                    if (item.BaoXiaoType == "借款单")
                    {
                        Finance_LoanEntity finance_LoanEntity = db.FindEntity<Finance_LoanEntity>(item.BaoXiaoId);
                        if (!finance_LoanEntity.PayedAmount.HasValue)
                            finance_LoanEntity.PayedAmount = 0;

                        if (item.PaymentAmount > (finance_LoanEntity.Amount - finance_LoanEntity.PayedAmount))
                        {
                            messageList.Add("单据" + item.Code + "付款金额大于待付金额");
                            continue;
                        }

                        projectId = finance_LoanEntity.ProjectID;
                        id = finance_LoanEntity.ID;
                        receiptEnum = ReceiptEnum.Financing;

                        finance_LoanEntity.PayedAmount += item.PaymentAmount;

                        if (finance_LoanEntity.PayedAmount >= finance_LoanEntity.Amount)
                            finance_LoanEntity.PayStatus = "1";
                        db.Update<Finance_LoanEntity>(finance_LoanEntity);

                        UserEntity userEntity = db.FindEntity<UserEntity>(finance_LoanEntity.Loaner);
                        if (!userEntity.Loan.HasValue)
                            userEntity.Loan = 0;
                        userEntity.Loan += finance_LoanEntity.Amount;
                        db.Update<UserEntity>(userEntity);

                        var des = $"{finance_InternalPaymentEntity.Abstract}[对内付款单审核]";
                        var auditExtend = new AuditPaymentExtend(db, projectId, receiptEnum, finance_InternalPaymentEntity.ID, id, item.PaymentAmount ?? 0, item.BankId);
                        var result = auditExtend.Audit(des);

                        if (!result.Success)
                            resultList.Add(result);
                        var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceInternalPayment");
                        if (configInfo != null)
                        {
                            var flag = configInfo.Value;

                            if (flag)
                            {
                                if (!string.IsNullOrEmpty(finance_LoanEntity.ProjectID))
                                {
                                    Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == finance_LoanEntity.ProjectID);

                                    if (base_FundAccountEntity != null)
                                    {
                                        var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                        {
                                            FundAccountFlag = base_FundAccountEntity.ID,
                                            AmountOccurrence = finance_LoanEntity.Amount.HasValue ? finance_LoanEntity.Amount.Value : 0,
                                            OccurrenceType = OccurrenceEnum.Outlay,
                                            RelevanceType = RelevanceEnum.Project,
                                            ReceiptType = receiptEnum,
                                            RelevanceId = finance_LoanEntity.ID,
                                            ReceiptId = item.ID,
                                            Abstract = des,
                                            ProjectId = projectId
                                        }, db, false);
                                        if (!result1.Success)
                                            resultList.Add(result);
                                    }
                                }
                            }
                        }
                    }
                    else if (item.BaoXiaoType == "报销单")
                    {
                        Finance_BaoXiaoEntity finance_BaoXiaoEntity = db.FindEntity<Finance_BaoXiaoEntity>(item.BaoXiaoId);

                        if (!finance_BaoXiaoEntity.PayedAmount.HasValue)
                            finance_BaoXiaoEntity.PayedAmount = 0;

                        if (item.PaymentAmount > (finance_BaoXiaoEntity.BaoXiaoAmount - finance_BaoXiaoEntity.PayedAmount))
                        {
                            messageList.Add("单据" + item.Code + "付款金额大于待付金额");
                            continue;
                        }
                        projectId = finance_BaoXiaoEntity.ProjectID; 
                        id = finance_BaoXiaoEntity.ID;
                        receiptEnum = ReceiptEnum.BaoXiao;

                        finance_BaoXiaoEntity.PayedAmount += item.PaymentAmount;

                        if (finance_BaoXiaoEntity.PayedAmount >= finance_BaoXiaoEntity.BaoXiaoAmount)
                            finance_BaoXiaoEntity.IsPay = true;

                        db.Update<Finance_BaoXiaoEntity>(finance_BaoXiaoEntity);

                        var bxDetails = db.FindList<Finance_BaoXiaoDetailsEntity>(i => i.FinanceBaoXiaoId == item.BaoXiaoId);
                        var subjectId = "";//基础科目ID
                        decimal rate = 0;//税率
                        foreach (var info in bxDetails)
                        {
                            subjectId = info.SubjectId;
                            rate = info.TaxRate ?? 0;

                            var des = $"{finance_InternalPaymentEntity.Abstract}[对内付款单审核]";
                            var auditExtend = new AuditPaymentExtend(db, projectId, receiptEnum, finance_InternalPaymentEntity.ID, id, item.PaymentAmount ?? 0, item.BankId, subjectId, rate);
                            var result = auditExtend.Audit(des);

                            if (!result.Success)
                                resultList.Add(result);

                            var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceInternalPayment");
                            if (configInfo != null)
                            {
                                var flag = configInfo.Value;

                                if (flag)
                                {
                                    if (finance_BaoXiaoEntity.BaoXiaoType == "项目费用" && !string.IsNullOrEmpty(finance_BaoXiaoEntity.ProjectID))
                                    {
                                        Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == finance_BaoXiaoEntity.ProjectID);

                                        if (base_FundAccountEntity != null)
                                        {
                                            var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                                            {
                                                FundAccountFlag = base_FundAccountEntity.ID,
                                                AmountOccurrence = finance_BaoXiaoEntity.BaoXiaoAmount.HasValue ? finance_BaoXiaoEntity.BaoXiaoAmount.Value : 0,
                                                OccurrenceType = OccurrenceEnum.Outlay,
                                                RelevanceType = RelevanceEnum.Project,
                                                ReceiptType = receiptEnum,
                                                RelevanceId = finance_BaoXiaoEntity.ID,
                                                ReceiptId = item.ID,
                                                Abstract = des,
                                                ProjectId = projectId
                                            }, db, false);
                                            if (!result1.Success)
                                                resultList.Add(result);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_InternalPaymentEntity = GetFinance_InternalPaymentEntity(keyValue); 
                db.Delete<Finance_InternalPaymentEntity>(t=>t.ID == keyValue);
                db.Delete<Finance_InternalPaymentDetailsEntity>(t=>t.FinanceInternalPaymentId == finance_InternalPaymentEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_InternalPaymentEntity entity,List<Finance_InternalPaymentDetailsEntity> finance_InternalPaymentDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var finance_InternalPaymentEntityTmp = GetFinance_InternalPaymentEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Finance_InternalPaymentDetailsUpdateList= finance_InternalPaymentDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Finance_InternalPaymentDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_InternalPaymentDetailsInserList= finance_InternalPaymentDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Finance_InternalPaymentDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.FinanceInternalPaymentId = finance_InternalPaymentEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_InternalPaymentDetailsEntity item in finance_InternalPaymentDetailsList)
                    {
                        item.FinanceInternalPaymentId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
