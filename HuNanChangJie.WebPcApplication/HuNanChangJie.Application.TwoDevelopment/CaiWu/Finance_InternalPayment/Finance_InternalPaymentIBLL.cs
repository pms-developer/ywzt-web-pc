﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu.Finance_InternalPayment;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 16:04
    /// 描 述：对内付款单
    /// </summary>
    public interface Finance_InternalPaymentIBLL
    {
        #region  获取数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_InternalPaymentEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_InternalPaymentDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_InternalPaymentDetailsEntity> GetFinance_InternalPaymentDetailsList(string keyValue);

        IEnumerable<Finance_InternalPaymentSelectDanJuEntity> GetFuKuanList(XqPagination pagination, string queryJson);

        Finance_InternalPaymentSelectDanJuEntity LoadDanJuData(int source, string keyid);

        /// <summary>
        /// 获取Finance_InternalPayment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_InternalPaymentEntity GetFinance_InternalPaymentEntity(string keyValue);
        /// <summary>
        /// 获取Finance_InternalPaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_InternalPaymentDetailsEntity GetFinance_InternalPaymentDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_InternalPaymentEntity entity,List<Finance_InternalPaymentDetailsEntity> finance_InternalPaymentDetailsList,string deleteList,string type);
        /// <summary>
        /// 发送流程前数据检查
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns>true:可以发送流程 false:不可以发送流程</returns>
        OperateResultEntity BeforeSendingCheckData(string keyValue);
        #endregion

    }
}
