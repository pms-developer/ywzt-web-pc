﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 16:04
    /// 描 述：对内付款单
    /// </summary>
    public class Finance_InternalPaymentDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 对内付款单ID
        /// </summary>
        [Column("FINANCEINTERNALPAYMENTID")]
        public string FinanceInternalPaymentId { get; set; }
        /// <summary>
        /// 报销单ID
        /// </summary>
        [Column("BAOXIAOID")]
        public string BaoXiaoId { get; set; }
        /// <summary>
        /// 报销单类型
        /// </summary>
        [Column("BAOXIAOTYPE")]
        public string BaoXiaoType { get; set; }
        /// <summary>
        /// 收款人ID
        /// </summary>
        [Column("PAYEEID")]
        public string PayeeId { get; set; }

        /// <summary>
        /// 收款人
        /// </summary>
        [NotMapped]
        public string PayeeName { get; set; }

        /// <summary>
        /// 单据编号
        /// </summary>

        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        [NotMapped]
        public string BuMeng { get; set; }
        [Column("AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal Amount { get; set; }
        /// <summary>
        /// 付款金额
        /// </summary>
        [Column("PAYMENTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? PaymentAmount { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        [Column("PAYMENTTYPE")]
        public string PaymentType { get; set; }
        /// <summary>
        /// 付款银行ID
        /// </summary>
        [Column("BANKID")]
        public string BankId { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region
        /// <summary>
        /// 默认付款账户
        /// </summary>
        public string DefaultAccountName { get; set; }

        /// <summary>
        /// 默认付款账户ID
        /// </summary>
        public string DefaultAccountID { get; set; }

        [NotMapped]
        public string OpenBank { get; set; }
        [NotMapped]
        public string BankCode { get; set; }
        [NotMapped]
        public string Holder { get; set; }

        [NotMapped]
        public string BankNames { get; set; }

        #endregion
    }
}

