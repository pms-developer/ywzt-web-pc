﻿using HuNanChangJie.SystemCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu.Finance_InternalPayment
{
    public class Finance_InternalPaymentSelectDanJuEntity : BaseEntity
    {
        [NotMapped]
        public string ID { get; set; }

        [NotMapped]
        public string mode { get; set; }

        [NotMapped]
        public DateTime sdate { get; set; }

        [NotMapped]
        public string realname { get; set; }

        [NotMapped]
        public string code { get; set; }
        [NotMapped]
        public string userid { get; set; }

        [NotMapped]
        public string deptid { get; set; }
        [NotMapped]
        public decimal payed { get; set; }
        [NotMapped]
        public decimal waitpay { get; set; }
        [NotMapped]
        public decimal amount { get; set; }

        [NotMapped]
        public string shoukuanmingcheng { get; set; }
        /// <summary>
        /// 默认付款账户
        /// </summary>
        [NotMapped]
        public string AccountName { get; set;}

        /// <summary>
        /// 付款账户ID
        /// </summary>
        [NotMapped]
        public string AccountId { get; set; }
    }
}
