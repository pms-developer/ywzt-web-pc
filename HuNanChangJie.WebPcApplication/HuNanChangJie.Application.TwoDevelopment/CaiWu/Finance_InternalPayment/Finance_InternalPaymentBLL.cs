﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu.Finance_InternalPayment;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 16:04
    /// 描 述：对内付款单
    /// </summary>
    public class Finance_InternalPaymentBLL : Finance_InternalPaymentIBLL
    {
        private Finance_InternalPaymentService finance_InternalPaymentService = new Finance_InternalPaymentService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_InternalPaymentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return finance_InternalPaymentService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public IEnumerable<Finance_InternalPaymentSelectDanJuEntity> GetFuKuanList(XqPagination pagination, string queryJson)
        {
            try
            {
                return finance_InternalPaymentService.GetFuKuanList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Finance_InternalPaymentSelectDanJuEntity LoadDanJuData(int source, string keyid)
        {
            try
            {
                return finance_InternalPaymentService.LoadDanJuData(source, keyid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InternalPaymentDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_InternalPaymentDetailsEntity> GetFinance_InternalPaymentDetailsList(string keyValue)
        {
            try
            {
                return finance_InternalPaymentService.GetFinance_InternalPaymentDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InternalPayment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InternalPaymentEntity GetFinance_InternalPaymentEntity(string keyValue)
        {
            try
            {
                return finance_InternalPaymentService.GetFinance_InternalPaymentEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InternalPaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InternalPaymentDetailsEntity GetFinance_InternalPaymentDetailsEntity(string keyValue)
        {
            try
            {
                return finance_InternalPaymentService.GetFinance_InternalPaymentDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return finance_InternalPaymentService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return finance_InternalPaymentService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                finance_InternalPaymentService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_InternalPaymentEntity entity,List<Finance_InternalPaymentDetailsEntity> finance_InternalPaymentDetailsList,string deleteList,string type)
        {
            try
            {
                finance_InternalPaymentService.SaveEntity(keyValue, entity,finance_InternalPaymentDetailsList,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0016");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 发送流程前数据检查
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns>true:可以发送流程 false:不可以发送流程</returns>
        public OperateResultEntity BeforeSendingCheckData(string keyValue)
        {
            try
            {
                return finance_InternalPaymentService.BeforeSendingCheckData(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
