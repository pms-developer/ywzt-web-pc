﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-02-08 13:48
    /// 描 述：预算管理
    /// </summary>
    public class BudgetManageService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Remark,
                t.TotalPrice,
                t.Month,
                t.ProjectName,
                t.Department_Id,
                t.CompanyName,
                t.Year
                ");
                strSql.Append("  FROM project_constructionbudget t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Year"].IsEmpty())
                {
                    dp.Add("Year", "%" + queryParam["Year"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Year Like @Year ");
                }
                if (!queryParam["Month"].IsEmpty())
                {
                    dp.Add("Month", "%" + queryParam["Month"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Month Like @Month ");
                }
                if (!queryParam["ProjectName"].IsEmpty())
                {
                    dp.Add("ProjectName",queryParam["ProjectName"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectName = @ProjectName ");
                }
                if (!queryParam["Department_Id"].IsEmpty())
                {
                    dp.Add("Department_Id",queryParam["Department_Id"].ToString(), DbType.String);
                    strSql.Append(" AND t.Department_Id = @Department_Id ");
                }
                if (!queryParam["CompanyName"].IsEmpty())
                {
                    dp.Add("CompanyName",queryParam["CompanyName"].ToString(), DbType.String);
                    strSql.Append(" AND t.CompanyName = @CompanyName ");
                }
                var list=this.BaseRepository().FindList<Project_ConstructionBudgetEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取project_constructionbudget表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ConstructionBudgetEntity Getproject_constructionbudgetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ConstructionBudgetEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Project_ConstructionBudgetEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_ConstructionBudgetEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
