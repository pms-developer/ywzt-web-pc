﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-27 16:34
    /// 描 述：保证金管理
    /// </summary>
    public class Base_CJ_BaoZhengJinService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_BaoZhengJinEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@" select   t.BianHao,
t.SourcesFunds,
t.ProjectManager,
                bb.projectname Project_ID,
                t.ZhaoBiaoBianHao,
                t.JinE,
                t.LeiXing,
                t.ZhiFuShiJian,
                t.TuiHuanShiJian,
                t.JinBanBuMeng,
                c.f_realname JinBanRen,
                t.DanWeiMingChen,
                t.KaiHuMingChen,
                t.KaiHuYinHang,
                t.YinHangZhanHao,
                t.LianXiRen,
                t.ID,
                t.Workflow_ID,
                t.AuditStatus,
                t.LianXiDianHua,
                t.ZhaiYao,
                t.BeiZhu,t.ZhiFuZhuangTai,t.CreationDate  FROM Base_CJ_BaoZhengJin t left join Base_CJ_Project as bb on t.Project_ID = bb.ID left join base_user c on t.JinBanRen = c.f_userid WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["BianHao"].IsEmpty())
                {
                    dp.Add("BianHao", "%" + queryParam["BianHao"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.BianHao Like @BianHao ");
                }
                if (!queryParam["Project_ID"].IsEmpty())
                {
                    dp.Add("Project_ID",queryParam["Project_ID"].ToString(), DbType.String);
                    strSql.Append(" AND t.Project_ID = @Project_ID ");
                }
                if (!queryParam["DanWeiMingChen"].IsEmpty())
                {
                    dp.Add("DanWeiMingChen", "%" + queryParam["DanWeiMingChen"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.DanWeiMingChen Like @DanWeiMingChen ");
                }

                if (!queryParam["ProjectName"].IsEmpty())
                {
                    dp.Add("ProjectName", "%" + queryParam["ProjectName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND bb.name Like @ProjectName ");
                }
                if (!queryParam["ZhaiYao"].IsEmpty())
                {
                    dp.Add("ZhaiYao", "%" + queryParam["ZhaiYao"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ZhaiYao Like @ZhaiYao ");
                }
                
                if (!queryParam["ZhiFuZhuangTai"].IsEmpty())
                {
                    dp.Add("ZhiFuZhuangTai", "%" + queryParam["ZhiFuZhuangTai"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ZhiFuZhuangTai Like @ZhiFuZhuangTai ");
                }
                if (!queryParam["LeiXing"].IsEmpty())
                {
                    dp.Add("LeiXing",queryParam["LeiXing"].ToString(), DbType.String);
                    strSql.Append(" AND t.LeiXing = @LeiXing ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND (t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }

                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate"; 
                    pagination.sord = "DESC";
                }

                var list =this.BaseRepository().FindList<Base_CJ_BaoZhengJinEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_BaoZhengJin表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_BaoZhengJinEntity GetBase_CJ_BaoZhengJinEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_CJ_BaoZhengJinEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_CJ_BaoZhengJinEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_BaoZhengJinEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    entity.ZhiFuZhuangTai = "未支付";
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
