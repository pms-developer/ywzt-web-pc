﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-27 16:34
    /// 描 述：保证金管理
    /// </summary>
    public interface Base_CJ_BaoZhengJinIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_CJ_BaoZhengJinEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_CJ_BaoZhengJin表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_CJ_BaoZhengJinEntity GetBase_CJ_BaoZhengJinEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_CJ_BaoZhengJinEntity entity,string deleteList,string type);
        #endregion

    }
}
