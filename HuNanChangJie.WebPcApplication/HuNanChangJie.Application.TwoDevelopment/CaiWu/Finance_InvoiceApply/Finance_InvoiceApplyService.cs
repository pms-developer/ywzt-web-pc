﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-27 20:15
    /// 描 述：财务 开票申请
    /// </summary>
    public class Finance_InvoiceApplyService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceApplyEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select * from (select t.id,t.Code,t.ApplyDate,t.CreationDate,t.ProjectID,t.Enabled,t.ProjectName,t.InvoiceAmount,t.AuditStatus,t.ApplyDepartment,t.Address, user1.F_RealName as ApplyUserId,
                user2.F_RealName as ProjectManager,(select top 1 e.fullname from Project_Contract d left join base_customer e on d.jia= e.id where d.auditstatus = '2' and d.projectid = t.projectid ) ShouPiaoCompany,
				(select top 1 isnull(taxrate,0)  from Finance_InvoiceApplyInfo where FinanceInvoiceApplyId=t.id) TaxRate
				 from Finance_InvoiceApply t  left join Base_User user1 on t.ApplyUserId=user1.F_UserId 
				 left join Base_User user2 on t.ProjectManager=user2.F_UserId) as temp WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND Code Like @Code ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND ProjectID = @ProjectID ");
                }

                if (!queryParam["ProjectName"].IsEmpty())
                {
                    dp.Add("ProjectName", "%" + queryParam["ProjectName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND ProjectName Like @ProjectName ");
                }

                if (!queryParam["ShouPiaoCompany"].IsEmpty())
                {
                    dp.Add("ShouPiaoCompany", "%" + queryParam["ShouPiaoCompany"].ToString() + "%", DbType.String);
                    strSql.Append(" AND ShouPiaoCompany Like @ShouPiaoCompany ");
                }
                if (!queryParam["ProjectManager"].IsEmpty())
                {
                    dp.Add("ProjectManager", "%" + queryParam["ProjectManager"].ToString() + "%", DbType.String);
                    strSql.Append(" AND ProjectManager Like @ProjectManager ");
                }

                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND (ApplyDate >= @startTime AND ApplyDate <= @endTime ) ");
                }

                if (!queryParam["Enabled"].IsEmpty())
                {
                    dp.Add("Enabled", queryParam["Enabled"].ToString(), DbType.String);
                    strSql.Append(" AND Enabled = @Enabled ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate"; 
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Finance_InvoiceApplyEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceApplyInfo表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceApplyInfoEntity> GetFinance_InvoiceApplyInfoList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_InvoiceApplyInfoEntity>(@"SELECT a.ID
      ,a.FinanceInvoiceApplyId
      ,a.CustomerId
      ,a.TaxRate
      ,a.AfterTaxAmount
      ,a.TaxPrice
      ,a.ProjectContractId
      ,a.ApplyAmount
      ,a.AppliedAmount
      ,a.AuditAmount
      ,a.Bank
      ,a.BankAccount
      ,a.Phone
      ,a.Remark
      ,a.CreationDate
      ,a.InvoiceTitle
      ,a.InvoiceType
      ,a.InvoiceCompany,c.Name,a.tax
	  ,c.TotalAmount,c.ReceivedAmount,c.FinalAmount,c.InvioceAmount,c.NoInvioceAmount,c.SettlementAmount
  FROM Finance_InvoiceApplyInfo a 
  left join Finance_InvoiceApply b on a.FinanceInvoiceApplyId = b.Id
  left join Project_Contract c on c.id = a.ProjectContractId 
  left join Base_Customer d on c.jia = d.id
  where FinanceInvoiceApplyId = '" + keyValue + "'");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public IEnumerable<Finance_InvoiceApplyInfoEntity> GetFinance_InvoiceApplyInfoListByProjectId(string projectId)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_InvoiceApplyInfoEntity>(@"  select lower(newid())  ID
      ,c.FinanceInvoiceApplyId
      ,a.jia CustomerId
      ,a.id ProjectContractId
      ,c.ApplyAmount
      ,c.AppliedAmount
      ,c.AuditAmount
      ,b.BankName Bank
      ,b.BankAccount
      ,b.Phone
      ,a.Remark
      ,a.CreationDate
      ,b.InvoiceTitle
      ,a.InvoiceType 
	  ,a.Name
	  ,a.TotalAmount,a.ReceivedAmount,a.FinalAmount,a.InvioceAmount,a.NoInvioceAmount,a.SettlementAmount
	  ,b.FullName InvoiceCompany from Project_Contract a left join Base_Customer b on a.jia = b.id left join Finance_InvoiceApplyInfo c on a.id = c.Id where a.ProjectID = '" + projectId + "' and a.auditstatus = 2");
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取Finance_InvoiceApplyDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceApplyDetailsEntity> GetFinance_InvoiceApplyDetailsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_InvoiceApplyDetailsEntity>(t => t.FinanceInvoiceApplyId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceApply表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceApplyEntity GetFinance_InvoiceApplyEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_InvoiceApplyEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceApplyInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceApplyInfoEntity GetFinance_InvoiceApplyInfoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_InvoiceApplyInfoEntity>(t => t.FinanceInvoiceApplyId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceApplyDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceApplyDetailsEntity GetFinance_InvoiceApplyDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_InvoiceApplyDetailsEntity>(t => t.FinanceInvoiceApplyId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_InvoiceApplyEntity = GetFinance_InvoiceApplyEntity(keyValue);
                db.Delete<Finance_InvoiceApplyEntity>(t => t.ID == keyValue);
                db.Delete<Finance_InvoiceApplyInfoEntity>(t => t.FinanceInvoiceApplyId == finance_InvoiceApplyEntity.ID);
                db.Delete<Finance_InvoiceApplyDetailsEntity>(t => t.FinanceInvoiceApplyId == finance_InvoiceApplyEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_InvoiceApplyEntity entity, List<Finance_InvoiceApplyInfoEntity> finance_InvoiceApplyInfoList, List<Finance_InvoiceApplyDetailsEntity> finance_InvoiceApplyDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var finance_InvoiceApplyEntityTmp = GetFinance_InvoiceApplyEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Finance_InvoiceApplyInfoUpdateList = finance_InvoiceApplyInfoList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Finance_InvoiceApplyInfoUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_InvoiceApplyInfoInserList = finance_InvoiceApplyInfoList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Finance_InvoiceApplyInfoInserList)
                    {
                        item.Create(item.ID);
                        item.FinanceInvoiceApplyId = finance_InvoiceApplyEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Finance_InvoiceApplyDetailsUpdateList = finance_InvoiceApplyDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Finance_InvoiceApplyDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_InvoiceApplyDetailsInserList = finance_InvoiceApplyDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Finance_InvoiceApplyDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.FinanceInvoiceApplyId = finance_InvoiceApplyEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_InvoiceApplyInfoEntity item in finance_InvoiceApplyInfoList)
                    {
                        item.FinanceInvoiceApplyId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Finance_InvoiceApplyDetailsEntity item in finance_InvoiceApplyDetailsList)
                    {
                        item.FinanceInvoiceApplyId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
