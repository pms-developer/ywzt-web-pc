﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-27 20:15
    /// 描 述：财务 开票申请
    /// </summary>
    public class Finance_InvoiceApplyInfoEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 开票申请ID
        /// </summary>
        [Column("FINANCEINVOICEAPPLYID")]
        public string FinanceInvoiceApplyId { get; set; }
        /// <summary>
        /// 客户ID
        /// </summary>
        [Column("CUSTOMERID")]
        public string CustomerId { get; set; }
        /// <summary>
        /// 合同ID
        /// </summary>
        [Column("PROJECTCONTRACTID")]
        public string ProjectContractId { get; set; }
        /// <summary>
        /// 申请金额
        /// </summary>
        [Column("APPLYAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? ApplyAmount { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        [Column("TAX")]
        [DecimalPrecision(18, 4)]
        public decimal? Tax { get; set; }
        /// <summary>
        /// 已申请开票金额
        /// </summary>
        [Column("APPLIEDAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? AppliedAmount { get; set; }
        /// <summary>
        /// 审计金额
        /// </summary>
        [Column("AUDITAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? AuditAmount { get; set; }
        /// <summary>
        /// 开户银行
        /// </summary>
        [Column("BANK")]
        public string Bank { get; set; }
        /// <summary>
        /// 银行账号
        /// </summary>
        [Column("BANKACCOUNT")]
        public string BankAccount { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("PHONE")]
        public string Phone { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 发票抬头
        /// </summary>
        [Column("INVOICETITLE")]
        public string InvoiceTitle { get; set; }
        /// <summary>
        /// 发票类型
        /// </summary>
        [Column("INVOICETYPE")]
        public string InvoiceType { get; set; }


        /// <summary>
        /// 收票单位
        /// </summary>
        [Column("INVOICECOMPANY")]
        public string InvoiceCompany { get; set; }


        /// <summary>
        /// 税率
        /// </summary>
        [Column("TAXRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRate { get; set; }

        /// <summary>
        /// 税后金额
        /// </summary>
        [Column("AFTERTAXAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterTaxAmount { get; set; }

        /// <summary>
        /// 含税价
        /// </summary>
        [Column("TAXPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxPrice { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public decimal? TotalAmount { get; set; }

        [NotMapped]
        public decimal? ReceivedAmount { get; set; }


        [NotMapped]
        public decimal? FinalAmount { get; set; }

        [NotMapped]
        public decimal? InvioceAmount { get; set; }

        [NotMapped]
        public decimal? NoInvioceAmount { get; set; }

        [NotMapped]
        public decimal? SettlementAmount { get; set; }
    }
}

