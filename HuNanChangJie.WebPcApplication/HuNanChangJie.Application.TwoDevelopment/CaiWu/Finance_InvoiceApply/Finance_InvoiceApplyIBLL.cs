﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-27 20:15
    /// 描 述：财务 开票申请
    /// </summary>
    public interface Finance_InvoiceApplyIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_InvoiceApplyEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_InvoiceApplyInfo表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_InvoiceApplyInfoEntity> GetFinance_InvoiceApplyInfoList(string keyValue);


        IEnumerable<Finance_InvoiceApplyInfoEntity> GetFinance_InvoiceApplyInfoListByProjectId(string projectId);
        
        /// <summary>
        /// 获取Finance_InvoiceApplyDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_InvoiceApplyDetailsEntity> GetFinance_InvoiceApplyDetailsList(string keyValue);
        /// <summary>
        /// 获取Finance_InvoiceApply表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_InvoiceApplyEntity GetFinance_InvoiceApplyEntity(string keyValue);
        /// <summary>
        /// 获取Finance_InvoiceApplyInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_InvoiceApplyInfoEntity GetFinance_InvoiceApplyInfoEntity(string keyValue);
        /// <summary>
        /// 获取Finance_InvoiceApplyDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_InvoiceApplyDetailsEntity GetFinance_InvoiceApplyDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_InvoiceApplyEntity entity,List<Finance_InvoiceApplyInfoEntity> finance_InvoiceApplyInfoList,List<Finance_InvoiceApplyDetailsEntity> finance_InvoiceApplyDetailsList,string deleteList,string type);
        #endregion

    }
}
