﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-27 20:15
    /// 描 述：财务 开票申请
    /// </summary>
    public class Finance_InvoiceApplyBLL : Finance_InvoiceApplyIBLL
    {
        private Finance_InvoiceApplyService finance_InvoiceApplyService = new Finance_InvoiceApplyService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceApplyEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return finance_InvoiceApplyService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceApplyInfo表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceApplyInfoEntity> GetFinance_InvoiceApplyInfoList(string keyValue)
        {
            try
            {
                return finance_InvoiceApplyService.GetFinance_InvoiceApplyInfoList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public IEnumerable<Finance_InvoiceApplyInfoEntity> GetFinance_InvoiceApplyInfoListByProjectId(string projectId)
        {
            try
            {
                return finance_InvoiceApplyService.GetFinance_InvoiceApplyInfoListByProjectId(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceApplyDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_InvoiceApplyDetailsEntity> GetFinance_InvoiceApplyDetailsList(string keyValue)
        {
            try
            {
                return finance_InvoiceApplyService.GetFinance_InvoiceApplyDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceApply表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceApplyEntity GetFinance_InvoiceApplyEntity(string keyValue)
        {
            try
            {
                return finance_InvoiceApplyService.GetFinance_InvoiceApplyEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceApplyInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceApplyInfoEntity GetFinance_InvoiceApplyInfoEntity(string keyValue)
        {
            try
            {
                return finance_InvoiceApplyService.GetFinance_InvoiceApplyInfoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_InvoiceApplyDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_InvoiceApplyDetailsEntity GetFinance_InvoiceApplyDetailsEntity(string keyValue)
        {
            try
            {
                return finance_InvoiceApplyService.GetFinance_InvoiceApplyDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                finance_InvoiceApplyService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_InvoiceApplyEntity entity,List<Finance_InvoiceApplyInfoEntity> finance_InvoiceApplyInfoList,List<Finance_InvoiceApplyDetailsEntity> finance_InvoiceApplyDetailsList,string deleteList,string type)
        {
            try
            {
                finance_InvoiceApplyService.SaveEntity(keyValue, entity,finance_InvoiceApplyInfoList,finance_InvoiceApplyDetailsList,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0010");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
