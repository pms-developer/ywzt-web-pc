﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.CaiWu.Finance_InternalPayment;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-23 12:14
    /// 描 述：财务 分包收票
    /// </summary>
    public class Finance_PaymentTaskService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_ExternalPaymentSelectDanJuEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@" select * from View_DWFKD_ALL where 1= 1 ");

                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["word"].IsEmpty())
                {
                    dp.Add("word", "%" + queryParam["word"].ToString() + "%", DbType.String);
                    strSql.Append(" AND (Payee Like @word or hetongname Like @word  or projectname Like @word  or danwei Like @word or Code Like @word  ) ");
                }

                if (!queryParam["hetongname"].IsEmpty())
                {
                    dp.Add("hetongname", "%" + queryParam["hetongname"].ToString() + "%", DbType.String);
                    strSql.Append(" AND hetongname Like @hetongname ");
                }

                if (!queryParam["projectname"].IsEmpty())
                {
                    dp.Add("projectname", "%" + queryParam["projectname"].ToString() + "%", DbType.String);
                    strSql.Append(" AND  projectname Like @projectname");
                }

                if (!queryParam["PayModel"].IsEmpty())
                {
                    dp.Add("PayModel", queryParam["PayModel"].ToString(), DbType.String);
                    strSql.Append(" AND PayModel = @PayModel");
                }

                if (!queryParam["PayCompanyId"].IsEmpty() && queryParam["PayCompanyId"].ToString() != "-1")
                {
                    dp.Add("PayCompanyId", queryParam["PayCompanyId"].ToString(), DbType.String);
                    strSql.Append(" AND PayCompanyId = @PayCompanyId");
                }

                if (!queryParam["danwei"].IsEmpty())
                {
                    dp.Add("danwei", "%" + queryParam["danwei"].ToString() + "%", DbType.String);
                    strSql.Append(" AND danwei Like @danwei");
                }

                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND Code Like @Code ");
                }
                if (!queryParam["paystatus"].IsEmpty())
                {
                    if (queryParam["paystatus"].ToString() == "1")
                        strSql.Append(" AND waitpay = 0 ");
                    else if (queryParam["paystatus"].ToString() == "-1")
                        strSql.Append(" AND waitpay > 0 ");
                }
                if (pagination !=null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "sdate";
                    pagination.sord = "DESC";
                }
                //var list = this.BaseRepository().FindList<Finance_ExternalPaymentSelectDanJuEntity>(strSql.ToString(), dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                //return query;

                var list = this.BaseRepository().FindList<Finance_ExternalPaymentSelectDanJuEntity>(strSql.ToString(), dp, pagination);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion
    }
}
