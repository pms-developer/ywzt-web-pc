﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-23 12:14
    /// 描 述：财务 分包收票
    /// </summary>
    public class Finance_SubcontractInvoiceEntity
    {
        #region  实体成员 
        /// <summary> 
        /// 分包合同Id 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROJECTSUBCONTRACTID")]
        public string ProjectSubcontractId { get; set; }
        /// <summary>
        /// 项目模式
        /// </summary>
        [Column("PROJECTMODE")]
        public string ProjectMode { get; set; }
        /// <summary> 
        /// 发票金额 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? InvoiceAmount { get; set; }
        /// <summary> 
        /// 登记时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("REGISTERDATE")]
        public DateTime? RegisterDate { get; set; }
        /// <summary> 
        /// 开票时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("MAKEDATE")]
        public DateTime? MakeDate { get; set; }
        /// <summary> 
        /// 开票单位 
        /// </summary> 
        /// <returns></returns> 
        [Column("MAKECOMPANYNAME")]
        public string MakeCompanyName { get; set; }
        /// <summary> 
        /// 开票单位ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("MAKECOMPANYID")]
        public string MakeCompanyId { get; set; }
        /// <summary> 
        /// 发票类型 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICETYPE")]
        public string InvoiceType { get; set; }
        /// <summary> 
        /// 税率 
        /// </summary> 
        /// <returns></returns> 
        [Column("TAXRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRate { get; set; }
        /// <summary> 
        /// 税额 
        /// </summary> 
        /// <returns></returns> 
        [Column("TAXAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxAmount { get; set; }

        /// <summary> 
        /// 已抵含税金额 
        /// </summary> 
        /// <returns></returns> 
        [Column("ACCOUNTINGINVOICEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? AccountingInvoiceAmount { get; set; }


        /// <summary> 
        /// 收票公司Id 
        /// </summary> 
        /// <returns></returns> 
        [Column("COLLECTCOMPANYID")]
        public string CollectCompanyId { get; set; }
        /// <summary> 
        /// 摘要 
        /// </summary> 
        /// <returns></returns> 
        [Column("ABSTRACT")]
        public string Abstract { get; set; }
        /// <summary> 
        /// 是否作废 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISOBSOLETE")]
        public bool? IsObsolete { get; set; }
        /// <summary> 
        /// 主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("ID")]
        public string ID { get; set; }
        /// <summary> 
        /// 是否启用 
        /// </summary> 
        /// <returns></returns> 
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary> 
        /// 创建时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary> 
        /// 创建者ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary> 
        /// 创建者名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary> 
        /// 修改时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary> 
        /// 修改者ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary> 
        /// 修改名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary> 
        /// 排序号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary> 
        /// 公司编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary> 
        /// 公司ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary> 
        /// 公司全称 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary> 
        /// 公司简称 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary> 
        /// 拼音码 
        /// </summary> 
        /// <returns></returns> 
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary> 
        /// 拼音码首字母简写 
        /// </summary> 
        /// <returns></returns> 
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary> 
        /// 审核者ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary> 
        /// 审核者名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary> 
        /// 审核状态 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary> 
        /// 项目ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary> 
        /// 项目名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary> 
        /// 审批流ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary> 
        /// 经办人 
        /// </summary> 
        /// <returns></returns> 
        [Column("JINGBANREN")]
        public string JingBanRen { get; set; }
        /// <summary> 
        /// 发票金额合计 
        /// </summary> 
        /// <returns></returns> 
        [Column("TOTALAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalAmount { get; set; }
        /// <summary> 
        /// 发票税额合计 
        /// </summary> 
        /// <returns></returns> 
        [Column("TOTALTAX")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalTax { get; set; }
        /// <summary> 
        /// 发票价税总计 
        /// </summary> 
        /// <returns></returns> 
        [Column("TOTALTAXAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalTaxAmount { get; set; }
        /// <summary> 
        /// 发票接收时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVEDATE")]
        public DateTime? ReceiveDate { get; set; }
        /// <summary> 
        /// 完税金额总计 
        /// </summary> 
        /// <returns></returns> 
        [Column("CERTIFICATETOTALAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? CertificateTotalAmount { get; set; }
        /// <summary> 
        /// 完税证明备注 
        /// </summary> 
        /// <returns></returns> 
        [Column("CERTIFICATEABSTRACT")]
        public string CertificateAbstract { get; set; }
        /// <summary> 
        /// 完税证明编号 
        /// </summary> 
        /// <returns></returns> 
        [Column("CERTIFICATENO")]
        public string CertificateNo { get; set; }
        /// <summary> 
        /// 完税证明开具时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("CERTIFICATEDATE")]
        public DateTime? CertificateDate { get; set; }
        /// <summary> 
        /// 纳税人名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("TAXPAYERNAME")]
        public string TaxpayerName { get; set; }
        /// <summary> 
        /// 纳税人识别号 
        /// </summary> 
        /// <returns></returns> 
        [Column("TAXPAYERNUMBER")]
        public string TaxpayerNumber { get; set; }
        /// <summary> 
        /// 发票地址 电话 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICEADDRESS")]
        public string InvoiceAddress { get; set; }
        /// <summary> 
        /// 发票开户行账号 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICEBANK")]
        public string InvoiceBank { get; set; }
        /// <summary> 
        /// 销售方名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("SALENAME")]
        public string SaleName { get; set; }
        /// <summary> 
        /// 销售方纳税人识别号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SALETAXPAYER")]
        public string SaleTaxpayer { get; set; }
        /// <summary> 
        /// 销售方地址电话 
        /// </summary> 
        /// <returns></returns> 
        [Column("SALEADDRESS")]
        public string SaleAddress { get; set; }
        /// <summary> 
        /// 销售方开户行账号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SALEBANK")]
        public string SaleBank { get; set; }
        /// <summary> 
        /// 发票编号 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICENO")]
        public string InvoiceNo { get; set; }
        /// <summary> 
        /// 开票人 
        /// </summary> 
        /// <returns></returns> 
        [Column("DRAWER")]
        public string Drawer { get; set; }
        #endregion

        /// <summary> 
        /// 发票余额 
        /// </summary> 
        /// <returns></returns> 
        [NotMapped]
        public decimal? balance { get; set; }



        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }

        #endregion
        #region  扩展字段
        #endregion
    }
}

