﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-23 12:14
    /// 描 述：财务 分包收票
    /// </summary>
    public interface Finance_SubcontractInvoiceIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_SubcontractInvoiceEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_SubcontractInvoice_Invoice表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_SubcontractInvoice_InvoiceEntity> GetFinance_SubcontractInvoice_InvoiceList(string keyValue);
        /// <summary>
        /// 获取Finance_SubcontractInvoice_Certificate表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_SubcontractInvoice_CertificateEntity> GetFinance_SubcontractInvoice_CertificateList(string keyValue);
        /// <summary>
        /// 获取Finance_SubcontractInvoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_SubcontractInvoiceEntity GetFinance_SubcontractInvoiceEntity(string keyValue);
        /// <summary>
        /// 获取Finance_SubcontractInvoice_Certificate表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_SubcontractInvoice_CertificateEntity GetFinance_SubcontractInvoice_CertificateEntity(string keyValue);
        /// <summary>
        /// 获取Finance_SubcontractInvoice_Invoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_SubcontractInvoice_InvoiceEntity GetFinance_SubcontractInvoice_InvoiceEntity(string keyValue);
        #endregion

        #region  提交数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_SubcontractInvoiceEntity entity,List<Finance_SubcontractInvoice_CertificateEntity> finance_SubcontractInvoice_CertificateList,List<Finance_SubcontractInvoice_InvoiceEntity> finance_SubcontractInvoice_InvoiceList,string deleteList,string type);
        #endregion

    }
}
