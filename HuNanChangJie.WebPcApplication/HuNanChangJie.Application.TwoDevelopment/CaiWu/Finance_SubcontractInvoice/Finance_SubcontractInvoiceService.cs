﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-23 12:14
    /// 描 述：财务 分包收票
    /// </summary>
    public class Finance_SubcontractInvoiceService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_SubcontractInvoiceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                b.name ProjectSubcontractId,
                isnull(t.AccountingInvoiceAmount,0) as AccountingInvoiceAmount,
                t.InvoiceAmount,
                t.InvoiceType,
                t.TaxAmount,
                t.TaxRate,
                t.RegisterDate,
                t.MakeDate,
                t.MakeCompanyName,
                t.MakeCompanyId,
                d.f_realname Creation_Id,
                t.ModificationDate,
                t.CollectCompanyId,
                t.Abstract,
                t.CompanyName,
                t.CompanyFullName,
                t.Company_ID,
                t.Modification_Id,
                t.AuditStatus,
                t.CreationDate,
                t.ReceiveDate,
                t.TotalTaxAmount,
                t.Workflow_ID,t.InvoiceAmount-isnull(t.AccountingInvoiceAmount,0) as balance
                ");
                strSql.Append(" FROM Finance_SubcontractInvoice t left join Project_Subcontract b on t.ProjectSubcontractId = b.id left join Base_User d on t.Creation_Id = d.f_userid ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["MakeCompanyId"].IsEmpty())
                {
                    dp.Add("MakeCompanyId", "%" + queryParam["MakeCompanyId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.MakeCompanyId Like @MakeCompanyId ");
                }
                if (!queryParam["MakeCompanyName"].IsEmpty())
                {
                    dp.Add("MakeCompanyName", "%" + queryParam["MakeCompanyName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.MakeCompanyName Like @MakeCompanyName ");
                }
                if (!queryParam["CollectCompanyId"].IsEmpty())
                {
                    dp.Add("CollectCompanyId", queryParam["CollectCompanyId"].ToString(), DbType.String);
                    strSql.Append(" AND t.CollectCompanyId = @CollectCompanyId ");
                }

                if (!queryParam["CollectCompanyId"].IsEmpty())
                {
                    dp.Add("CollectCompanyId", queryParam["CollectCompanyId"].ToString(), DbType.String);
                    strSql.Append(" AND t.CollectCompanyId = @CollectCompanyId ");
                }
                if (!queryParam["ProjectSubcontractId"].IsEmpty())
                {
                    dp.Add("ProjectSubcontractId", "%" + queryParam["ProjectSubcontractId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND b.name Like @ProjectSubcontractId ");
                }
                if (!queryParam["Amount"].IsEmpty())
                {
                    strSql.Append(" AND (t.InvoiceAmount > t.AccountingInvoiceAmount or  t.AccountingInvoiceAmount is null)");
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (!queryParam["Abstract"].IsEmpty())
                {
                    dp.Add("Abstract", "%" + queryParam["Abstract"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Abstract Like @Abstract ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.MakeDate >= @startTime AND t.MakeDate <= @endTime ) ");
                }
                if (!queryParam["SAmount"].IsEmpty())
                {
                    dp.Add("SAmount", queryParam["SAmount"].ToString(), DbType.String);
                    strSql.Append(" AND t.InvoiceAmount>=@SAmount");
                }
                if (!queryParam["EAmount"].IsEmpty())
                {
                    dp.Add("EAmount", queryParam["EAmount"].ToString(), DbType.String);
                    strSql.Append(" AND t.InvoiceAmount<=@EAmount");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Finance_SubcontractInvoiceEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice_Invoice表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_SubcontractInvoice_InvoiceEntity> GetFinance_SubcontractInvoice_InvoiceList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_SubcontractInvoice_InvoiceEntity>(t => t.Fk_SubcontractInvoiceID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice_Certificate表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_SubcontractInvoice_CertificateEntity> GetFinance_SubcontractInvoice_CertificateList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Finance_SubcontractInvoice_CertificateEntity>(t => t.Fk_SubcontractInvoiceID == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_SubcontractInvoiceEntity GetFinance_SubcontractInvoiceEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_SubcontractInvoiceEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice_Certificate表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_SubcontractInvoice_CertificateEntity GetFinance_SubcontractInvoice_CertificateEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_SubcontractInvoice_CertificateEntity>(t => t.Fk_SubcontractInvoiceID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice_Invoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_SubcontractInvoice_InvoiceEntity GetFinance_SubcontractInvoice_InvoiceEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Finance_SubcontractInvoice_InvoiceEntity>(t => t.Fk_SubcontractInvoiceID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Finance_SubcontractInvoiceEntity finance_SubcontractInvoiceEntity = db.FindEntity<Finance_SubcontractInvoiceEntity>(keyValue);
                if (finance_SubcontractInvoiceEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_SubcontractInvoiceEntity.AuditStatus = "4";
                db.Update<Finance_SubcontractInvoiceEntity>(finance_SubcontractInvoiceEntity);

                Project_SubcontractEntity project_SubcontractEntity = db.FindEntity<Project_SubcontractEntity>(finance_SubcontractInvoiceEntity.ProjectSubcontractId);
                if (!project_SubcontractEntity.InvioceAmount.HasValue)
                    project_SubcontractEntity.InvioceAmount = 0;
                if (!project_SubcontractEntity.NoInvioceAmount.HasValue)
                    project_SubcontractEntity.NoInvioceAmount = 0;
                project_SubcontractEntity.InvioceAmount -= finance_SubcontractInvoiceEntity.InvoiceAmount;
                project_SubcontractEntity.NoInvioceAmount = project_SubcontractEntity.TotalAmount - finance_SubcontractInvoiceEntity.InvoiceAmount;
                db.Update<Project_SubcontractEntity>(project_SubcontractEntity);

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true };

            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Finance_SubcontractInvoiceEntity finance_SubcontractInvoiceEntity = db.FindEntity<Finance_SubcontractInvoiceEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_SubcontractInvoiceEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}

                finance_SubcontractInvoiceEntity.AuditStatus = "2";
                db.Update<Finance_SubcontractInvoiceEntity>(finance_SubcontractInvoiceEntity);

                Project_SubcontractEntity project_SubcontractEntity = db.FindEntity<Project_SubcontractEntity>(finance_SubcontractInvoiceEntity.ProjectSubcontractId);
                if (!project_SubcontractEntity.InvioceAmount.HasValue)
                    project_SubcontractEntity.InvioceAmount = 0;
                if (!project_SubcontractEntity.NoInvioceAmount.HasValue)
                    project_SubcontractEntity.NoInvioceAmount = 0;
                project_SubcontractEntity.InvioceAmount += finance_SubcontractInvoiceEntity.InvoiceAmount;
                project_SubcontractEntity.NoInvioceAmount = project_SubcontractEntity.TotalAmount - finance_SubcontractInvoiceEntity.InvoiceAmount;
                db.Update<Project_SubcontractEntity>(project_SubcontractEntity);

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true };

            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var finance_SubcontractInvoiceEntity = GetFinance_SubcontractInvoiceEntity(keyValue);
                db.Delete<Finance_SubcontractInvoiceEntity>(t => t.ID == keyValue);
                db.Delete<Finance_SubcontractInvoice_CertificateEntity>(t => t.Fk_SubcontractInvoiceID == finance_SubcontractInvoiceEntity.ID);
                db.Delete<Finance_SubcontractInvoice_InvoiceEntity>(t => t.Fk_SubcontractInvoiceID == finance_SubcontractInvoiceEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_SubcontractInvoiceEntity entity, List<Finance_SubcontractInvoice_CertificateEntity> finance_SubcontractInvoice_CertificateList, List<Finance_SubcontractInvoice_InvoiceEntity> finance_SubcontractInvoice_InvoiceList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var finance_SubcontractInvoiceEntityTmp = GetFinance_SubcontractInvoiceEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Finance_SubcontractInvoice_CertificateUpdateList = finance_SubcontractInvoice_CertificateList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Finance_SubcontractInvoice_CertificateUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_SubcontractInvoice_CertificateInserList = finance_SubcontractInvoice_CertificateList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Finance_SubcontractInvoice_CertificateInserList)
                    {
                        item.Create(item.ID);
                        item.Fk_SubcontractInvoiceID = finance_SubcontractInvoiceEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Finance_SubcontractInvoice_InvoiceUpdateList = finance_SubcontractInvoice_InvoiceList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Finance_SubcontractInvoice_InvoiceUpdateList)
                    {
                        db.Update(item);
                    }
                    var Finance_SubcontractInvoice_InvoiceInserList = finance_SubcontractInvoice_InvoiceList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Finance_SubcontractInvoice_InvoiceInserList)
                    {
                        item.Create(item.ID);
                        item.Fk_SubcontractInvoiceID = finance_SubcontractInvoiceEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Finance_SubcontractInvoice_CertificateEntity item in finance_SubcontractInvoice_CertificateList)
                    {
                        item.Fk_SubcontractInvoiceID = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Finance_SubcontractInvoice_InvoiceEntity item in finance_SubcontractInvoice_InvoiceList)
                    {
                        item.Fk_SubcontractInvoiceID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
