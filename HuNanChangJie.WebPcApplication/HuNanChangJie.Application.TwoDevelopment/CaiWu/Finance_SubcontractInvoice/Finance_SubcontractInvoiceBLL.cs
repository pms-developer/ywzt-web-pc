﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-23 12:14
    /// 描 述：财务 分包收票
    /// </summary>
    public class Finance_SubcontractInvoiceBLL : Finance_SubcontractInvoiceIBLL
    {
        private Finance_SubcontractInvoiceService finance_SubcontractInvoiceService = new Finance_SubcontractInvoiceService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_SubcontractInvoiceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return finance_SubcontractInvoiceService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice_Invoice表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_SubcontractInvoice_InvoiceEntity> GetFinance_SubcontractInvoice_InvoiceList(string keyValue)
        {
            try
            {
                return finance_SubcontractInvoiceService.GetFinance_SubcontractInvoice_InvoiceList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice_Certificate表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_SubcontractInvoice_CertificateEntity> GetFinance_SubcontractInvoice_CertificateList(string keyValue)
        {
            try
            {
                return finance_SubcontractInvoiceService.GetFinance_SubcontractInvoice_CertificateList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_SubcontractInvoiceEntity GetFinance_SubcontractInvoiceEntity(string keyValue)
        {
            try
            {
                return finance_SubcontractInvoiceService.GetFinance_SubcontractInvoiceEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice_Certificate表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_SubcontractInvoice_CertificateEntity GetFinance_SubcontractInvoice_CertificateEntity(string keyValue)
        {
            try
            {
                return finance_SubcontractInvoiceService.GetFinance_SubcontractInvoice_CertificateEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_SubcontractInvoice_Invoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_SubcontractInvoice_InvoiceEntity GetFinance_SubcontractInvoice_InvoiceEntity(string keyValue)
        {
            try
            {
                return finance_SubcontractInvoiceService.GetFinance_SubcontractInvoice_InvoiceEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return finance_SubcontractInvoiceService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return finance_SubcontractInvoiceService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                finance_SubcontractInvoiceService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_SubcontractInvoiceEntity entity,List<Finance_SubcontractInvoice_CertificateEntity> finance_SubcontractInvoice_CertificateList,List<Finance_SubcontractInvoice_InvoiceEntity> finance_SubcontractInvoice_InvoiceList,string deleteList,string type)
        {
            try
            {
                finance_SubcontractInvoiceService.SaveEntity(keyValue, entity,finance_SubcontractInvoice_CertificateList,finance_SubcontractInvoice_InvoiceList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
