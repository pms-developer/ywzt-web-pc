﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-20 17:13
    /// 描 述：借款申请
    /// </summary>
    public interface Finance_LoanIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Finance_LoanEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Finance_LoanDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_LoanDetailsEntity> GetFinance_LoanDetailsList(string keyValue);
        /// <summary>
        /// 获取Finance_Loan表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_LoanEntity GetFinance_LoanEntity(string keyValue);
        /// <summary>
        /// 获取Finance_LoanDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Finance_LoanDetailsEntity GetFinance_LoanDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Finance_LoanEntity entity,List<Finance_LoanDetailsEntity> finance_LoanDetailsList,string deleteList,string type);
        #endregion

    }
}
