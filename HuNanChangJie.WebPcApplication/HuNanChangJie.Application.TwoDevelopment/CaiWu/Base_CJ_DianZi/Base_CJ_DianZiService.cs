﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-12 23:52
    /// 描 述：垫资管理
    /// </summary>
    public class Base_CJ_DianZiService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_DianZiEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select t.id, t.ShuoMing,t.AuditStatus,t.CreationDate,t.YiTuiJinE,t.LianXiFangShi,t.BianHao,t.DianZiShiJian, user1.F_RealName as ProjectManager,c.projectname,t.DianZiRen,t.JinE,f.Name ShouKuanZhangHu,user2.F_RealName as JinBanRen FROM Base_CJ_DianZi t left join Base_User user1 on t.ProjectManager=user1.F_UserId 
 left join base_cj_project c on t.ProjectID = c.id  left join Base_FundAccount f on t.ShouKuanZhangHu = f.id 
  left join Base_User user2 on t.JinBanRen=user2.F_UserId  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (!queryParam["DianZiRen"].IsEmpty())
                {
                    dp.Add("DianZiRen", "%" + queryParam["DianZiRen"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.DianZiRen Like @DianZiRen ");
                }
                if (!queryParam["ProjectName"].IsEmpty())
                {
                    dp.Add("ProjectName", "%" + queryParam["ProjectName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND c.projectname Like @ProjectName ");
                }

                if (!queryParam["ShouKuanZhangHu"].IsEmpty())
                {
                    dp.Add("ShouKuanZhangHu", "%" + queryParam["ShouKuanZhangHu"].ToString() + "%", DbType.String);
                    strSql.Append(" AND f.Name Like @ShouKuanZhangHu ");
                }
                if (!queryParam["ProjectManager"].IsEmpty())
                {
                    dp.Add("ProjectManager", "%" + queryParam["ProjectManager"].ToString() + "%", DbType.String);
                    strSql.Append(" AND user1.F_RealName Like @ProjectManager ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND (DianZiShiJian >= @startTime AND DianZiShiJian <= @endTime ) ");
                }
                var list = this.BaseRepository().FindList<Base_CJ_DianZiEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_DianZi表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_DianZiEntity GetBase_CJ_DianZiEntity(string keyValue)
        {
            try
            {
                string sql = "select a.*,fac.Bank+'('+fac.Name+')'as ShouKuanZhangHuName from Base_CJ_DianZi as a left join Base_FundAccount as fac on a.ShouKuanZhangHu=fac.id where a.id='" + keyValue + "'";
                return this.BaseRepository().FindEntity<Base_CJ_DianZiEntity>(sql, null);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Base_CJ_DianZiEntity base_CJ_DianZiEntity = db.FindEntity<Base_CJ_DianZiEntity>(keyValue);

                if (base_CJ_DianZiEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                base_CJ_DianZiEntity.AuditStatus = "4";
                db.Update<Base_CJ_DianZiEntity>(base_CJ_DianZiEntity);

                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = base_CJ_DianZiEntity.ShouKuanZhangHu,
                    AmountOccurrence = base_CJ_DianZiEntity.JinE.HasValue ? base_CJ_DianZiEntity.JinE.Value : 0,
                    OccurrenceType = OccurrenceEnum.Outlay,
                    RelevanceType = RelevanceEnum.Project,
                    ReceiptType = ReceiptEnum.Acting,
                    RelevanceId = base_CJ_DianZiEntity.ID,
                    ReceiptId = base_CJ_DianZiEntity.ID,
                    Abstract = $"{base_CJ_DianZiEntity.ShuoMing}[垫资单取消审核]",
                    ProjectId = base_CJ_DianZiEntity.ProjectID
                }, db);

                Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == base_CJ_DianZiEntity.ProjectID);

                if (base_FundAccountEntity != null && base_FundAccountEntity.ID != base_CJ_DianZiEntity.ShouKuanZhangHu)
                {
                    //var result1 = 
                    new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = base_FundAccountEntity.ID,
                        AmountOccurrence = base_CJ_DianZiEntity.JinE.HasValue ? base_CJ_DianZiEntity.JinE.Value : 0,
                        OccurrenceType = OccurrenceEnum.Outlay,
                        RelevanceType = RelevanceEnum.Project,
                        ReceiptType = ReceiptEnum.Acting,
                        RelevanceId = base_CJ_DianZiEntity.ID,
                        ReceiptId = base_CJ_DianZiEntity.ID,
                        Abstract = $"{base_CJ_DianZiEntity.ShuoMing}[垫资单取消审核]",
                        ProjectId = base_CJ_DianZiEntity.ProjectID
                    }, db, false);
                    //if (!result1.Success)
                    //    throw new Exception(result1.Message);
                }

                if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }

                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Base_CJ_DianZiEntity base_CJ_DianZiEntity = db.FindEntity<Base_CJ_DianZiEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (base_CJ_DianZiEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                base_CJ_DianZiEntity.AuditStatus = "2";
                db.Update<Base_CJ_DianZiEntity>(base_CJ_DianZiEntity);

                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = base_CJ_DianZiEntity.ShouKuanZhangHu,
                    AmountOccurrence = base_CJ_DianZiEntity.JinE.HasValue ? base_CJ_DianZiEntity.JinE.Value : 0,
                    OccurrenceType = OccurrenceEnum.Income,
                    RelevanceType = RelevanceEnum.Project,
                    ReceiptType = ReceiptEnum.Acting,
                    RelevanceId = base_CJ_DianZiEntity.ID,
                    ReceiptId = base_CJ_DianZiEntity.ID,
                    Abstract = $"{base_CJ_DianZiEntity.ShuoMing}[垫资单审核]",
                    ProjectId = base_CJ_DianZiEntity.ProjectID
                }, db);

                Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == base_CJ_DianZiEntity.ProjectID);

                if (base_FundAccountEntity != null && base_FundAccountEntity.ID != base_CJ_DianZiEntity.ShouKuanZhangHu)
                {
                    //var result1 = 
                    new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = base_FundAccountEntity.ID,
                        AmountOccurrence = base_CJ_DianZiEntity.JinE.HasValue ? base_CJ_DianZiEntity.JinE.Value : 0,
                        OccurrenceType = OccurrenceEnum.Income,
                        RelevanceType = RelevanceEnum.Project,
                        ReceiptType = ReceiptEnum.Acting,
                        RelevanceId = base_CJ_DianZiEntity.ID,
                        ReceiptId = base_CJ_DianZiEntity.ID,
                        Abstract = $"{base_CJ_DianZiEntity.ShuoMing}[垫资单审核]",
                        ProjectId = base_CJ_DianZiEntity.ProjectID
                    }, db, false);
                    //if (!result1.Success)
                    //    throw new Exception(result1.Message);
                }
                if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }

                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_CJ_DianZiEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_DianZiEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
