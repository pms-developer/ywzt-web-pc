﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-02 18:14
    /// 描 述：资金帐户
    /// </summary>
    public class Base_FundAccountBLL : Base_FundAccountIBLL
    {
        private Base_FundAccountService base_FundAccountService = new Base_FundAccountService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_FundAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return base_FundAccountService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_FundAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_FundAccountEntity GetBase_FundAccountEntity(string keyValue)
        {
            try
            {
                return base_FundAccountService.GetBase_FundAccountEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                base_FundAccountService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_FundAccountEntity entity,string deleteList,string type)
        {
            try
            {
                base_FundAccountService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 账户操作
        /// </summary>
        /// <param name="doFundAccountEntity">记录明细</param>
        /// <returns>操作结果</returns>
        public Util.Common.OperateResultEntity DoFundAccount(DoFundAccountEntity doFundAccountEntity, IRepository db=null)
        {
            try
            {
               return base_FundAccountService.DoFundAccount(doFundAccountEntity,db);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Base_FundAccoutDetailsEntity> GetDetails(XqPagination pagination, string queryJson)
        {
            try
            {
                return base_FundAccountService.GetDetails(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 效验资金账户余额及冻结额
        /// </summary>
        /// <param name="fundId"></param>
        /// <returns></returns>
        public Util.Common.OperateResultEntity VerificationFundBalanceAndFreeze(string fundId,IRepository db = null)
        {
            try
            {
                return base_FundAccountService.VerificationFundBalanceAndFreeze(fundId, db);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
