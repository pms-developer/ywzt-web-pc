﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-02 18:14
    /// 描 述：资金帐户
    /// </summary>
    public interface Base_FundAccountIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_FundAccountEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_FundAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_FundAccountEntity GetBase_FundAccountEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_FundAccountEntity entity,string deleteList,string type);
        /// <summary>
        /// 账户操作
        /// </summary>
        /// <param name="doFundAccountEntity">记录明细</param>
        /// <returns>操作结果</returns>
        OperateResultEntity DoFundAccount(DoFundAccountEntity doFundAccountEntity, IRepository db = null);
        IEnumerable<Base_FundAccoutDetailsEntity> GetDetails(XqPagination paginationobj, string queryJson);

        /// <summary>
        /// 效验资金账户余额及冻结额
        /// </summary>
        /// <param name="fundId"></param>
        /// <returns></returns>
        OperateResultEntity VerificationFundBalanceAndFreeze(string fundId, IRepository db = null);

        #endregion

    }
}
