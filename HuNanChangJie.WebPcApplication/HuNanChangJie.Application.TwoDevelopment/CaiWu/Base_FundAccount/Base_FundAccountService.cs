﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-02 18:14
    /// 描 述：资金帐户
    /// </summary>
    public class Base_FundAccountService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_FundAccountEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                var queryParam = queryJson.ToJObject();
                var dp = new DynamicParameters(new { });

                if (!queryParam["type"].IsEmpty())
                {
                    var type = queryParam["type"].ToString();

                    if (type == "ZjzhNoProject")
                    {
                        strSql.Append("SELECT t.ID,t.Name,t.Bank,t.BankCode,t.Holder,t.CompanyId,t.ProjectId,t.Remark, t.UsableBalance,t.Balance,t.Freezed ");
                        strSql.Append("from Base_FundAccount t left join base_company b on t.companyid = b.f_companyid where isnull(projectid,'') = '' ");

                        // 虚拟参数
                        if (!queryParam["Name"].IsEmpty())
                        {
                            dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                            strSql.Append(" AND t.Name Like @Name ");
                        }
                        if (!queryParam["companyId"].IsEmpty())
                        {
                            dp.Add("companyId", queryParam["companyId"].ToString(), DbType.String);
                            strSql.Append(" AND t.companyId= @companyId ");
                        }

                        if (!queryParam["Bank"].IsEmpty())
                        {
                            dp.Add("Bank", "%" + queryParam["Bank"].ToString() + "%", DbType.String);
                            strSql.Append(" AND t.Bank Like @Bank ");
                        }
                    }
                    else
                    {
                        strSql.Append("SELECT t.ID,t.Name,t.Bank, t.BankCode, t.Holder,t.CompanyId,t.ProjectId,t.Remark,t.UsableBalance,t.Balance,t.Freezed,bu.F_RealName ");
                        strSql.Append("FROM Base_FundAccount t ");
                        strSql.Append("left join Base_CJ_Project as bcp on t.ProjectId = bcp.ID ");
                        strSql.Append("left join Base_User as bu on bcp.ProjectManager = bu.F_UserId ");

                        strSql.Append("  WHERE 1=1 ");
                        // 虚拟参数

                        if (!queryParam["Name"].IsEmpty())
                        {
                            dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                            strSql.Append(" AND t.Name Like @Name ");
                        }
                        if (!queryParam["Bank"].IsEmpty())
                        {
                            dp.Add("Bank", "%" + queryParam["Bank"].ToString() + "%", DbType.String);
                            strSql.Append(" AND t.Bank Like @Bank ");
                        }

                        if (!queryParam["ProjectId"].IsEmpty())
                        {
                            dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                            strSql.Append(" AND t.ProjectId= @ProjectId ");
                        }
                    }
                }
                else
                {
                    strSql.Append("SELECT t.ID,t.Name,t.Bank, t.BankCode, t.Holder,t.CompanyId,t.ProjectId,t.Remark,t.UsableBalance,t.Balance,t.Freezed,bu.F_RealName ");
                    strSql.Append("FROM Base_FundAccount t ");
                    strSql.Append("left join Base_CJ_Project as bcp on t.ProjectId = bcp.ID ");
                    strSql.Append("left join Base_User as bu on bcp.ProjectManager = bu.F_UserId ");

                    strSql.Append("  WHERE 1=1 ");
                    // 虚拟参数

                    if (!queryParam["Name"].IsEmpty())
                    {
                        dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                        strSql.Append(" AND t.Name Like @Name ");
                    }
                    if (!queryParam["Bank"].IsEmpty())
                    {
                        dp.Add("Bank", "%" + queryParam["Bank"].ToString() + "%", DbType.String);
                        strSql.Append(" AND t.Bank Like @Bank ");
                    }

                    if (!queryParam["ProjectId"].IsEmpty())
                    {
                        dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                        strSql.Append(" AND t.ProjectId= @ProjectId ");
                    }

                }

                var list = this.BaseRepository().FindList<Base_FundAccountEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list select item;

                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_FundAccount表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_FundAccountEntity GetBase_FundAccountEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_FundAccountEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_FundAccountEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_FundAccountEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    var db = this.BaseRepository();
                    db.BeginTrans();
                    try
                    {
                        entity.Create(keyValue);
                        if (entity.InitBalance.HasValue && entity.InitBalance > 0)
                        {
                            entity.Balance = entity.InitBalance;
                            entity.UsableBalance = entity.InitBalance;
                        }
                        db.Insert(entity);
                        if (entity.Balance > 0)
                        {
                            UserInfo userInfo = LoginUserInfo.Get();
                            string sql = "";
                            string occurrenceDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            sql += string.Format("INSERT INTO Base_FundAccoutDetails(ID,FundAccountId,AmountOccurrence,OccurrenceType,RelevanceType,RelevanceId,ReceiptType,ReceiptId,OccurrenceDate,OperatorId,Abstract,ProjectSubjectId,ProjectId,PrevBalance,Balance) VALUES('{0}','{1}',{2},'{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}',0,{13});",
                               Guid.NewGuid().ToString(),
                               keyValue,
                               entity.Balance ?? 0,
                              OccurrenceEnum.Income.ToString(),
                               RelevanceEnum.Other,
                               keyValue,
                               ReceiptEnum.Other,
                              keyValue,
                               occurrenceDate,
                              userInfo.userId,
                               "开账金额初始化",
                               "",
                               entity.ProjectId, entity.Balance
                               );
                            int affRow = db.ExecuteBySql(sql);
                        }
                        db.Commit();
                    }
                    catch (Exception)
                    {
                        db.Rollback();
                    }
                    //entity.Create(keyValue);
                    //this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 效验资金账户余额及冻结额
        /// </summary>
        /// <param name="fundId"></param>
        /// <returns></returns>
        internal OperateResultEntity VerificationFundBalanceAndFreeze(string fundId, IRepository db = null)
        {
            try
            {
                var fundInfo = (db ?? BaseRepository()).FindEntity<Base_FundAccountEntity>(i => i.ID == fundId);
                if (fundInfo == null)
                {
                    return new Util.Common.OperateResultEntity() { Success = false, Message = "标记账户为空" };
                }
                var details = (db ?? BaseRepository()).FindList<Base_FundAccoutDetailsEntity>(i => i.FundAccountId == fundId);
                decimal inAmount = 0;
                decimal outAmount = 0;
                decimal freezaAmount = 0;
                decimal unFreezaAmount = 0;
                foreach (var item in details)
                {
                    if (item.OccurrenceType == OccurrenceEnum.Income.ToString())
                        inAmount += (item.AmountOccurrence ?? 0);
                    if (item.OccurrenceType == OccurrenceEnum.Outlay.ToString())
                        outAmount += (item.AmountOccurrence ?? 0);
                    if (item.OccurrenceType == OccurrenceEnum.Freeze.ToString())
                        freezaAmount += (item.AmountOccurrence ?? 0);
                    if (item.OccurrenceType == OccurrenceEnum.Unfreeze.ToString())
                        unFreezaAmount += (item.AmountOccurrence ?? 0);
                }
                fundInfo.Balance = inAmount - outAmount;
                fundInfo.Freezed = freezaAmount - unFreezaAmount;
                fundInfo.UsableBalance = fundInfo.Balance - fundInfo.Freezed;

                (db ?? BaseRepository()).Update(fundInfo);

                return new Util.Common.OperateResultEntity() { Success = true, Message = "" };
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<Base_FundAccoutDetailsEntity> GetDetails(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                //strSql.Append("SELECT * from(");
                //strSql.Append(@" select
                // a.*,
                // b.ProjectName,
                // c.Name as SubjectName,
                // d.F_RealName
                // from Base_FundAccoutDetails as a left join 
                // Base_CJ_Project as b on a.ProjectId=b.Id left join 
                // Project_Subject as c on a.ProjectSubjectId=c.id  
                // left join Base_User as d on a.OperatorId=d.f_UserId
                // ) as t
                //");

                //F_Account:项目经理   CustomerFullName：客户名称
                strSql.Append(" SELECT * from(");
                strSql.Append(@"select
                 cc.*,
				 e.F_RealName as F_Account,
                 b.ProjectName,
                 d.F_RealName,
				 g.FullName as CustomerFullName
                 from Base_FundAccoutDetails as a left join
                 zijinzhanhumingxi AS  cc on a.ID = cc.ID  left join
                 Base_CJ_Project as b on a.ProjectId = b.Id left join
                 Base_User e  on b.MarketingStaffID = e.F_UserId  left join
                 Base_User as d on a.OperatorId = d.f_UserId left join 
                 Base_Customer g on b.CustomerID = g.id 
                 
                 ) as t");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["FundAccountId"].IsEmpty())//账户ID
                {
                    dp.Add("FundAccountId", queryParam["FundAccountId"].ToString(), DbType.String);
                    strSql.Append(" AND t.FundAccountId = @FundAccountId ");
                }
                if (!queryParam["OccurrenceType"].IsEmpty())//发生类型
                {
                    dp.Add("OccurrenceType", queryParam["OccurrenceType"].ToString(), DbType.String);
                    strSql.Append(" AND t.OccurrenceType = @OccurrenceType ");
                }
                if (!queryParam["ReceiptType"].IsEmpty())//单据类型
                {
                    dp.Add("ReceiptType", queryParam["ReceiptType"].ToString(), DbType.String);
                    strSql.Append(" AND t.ReceiptType = @ReceiptType ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())//发生日期
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.OccurrenceDate >= @startTime AND t.OccurrenceDate <= @endTime ) ");
                }
                if (!queryParam["ProjectName"].IsEmpty())//发生日期
                {
                    dp.Add("ProjectName", $"%{queryParam["ProjectName"].ToString()}%", DbType.String);
                    strSql.Append(" AND t.ProjectName like @ProjectName  ");
                }
                if (!queryParam["ProjectManager"].IsEmpty())//发生日期
                {
                    dp.Add("ProjectManager", $"%{queryParam["ProjectManager"].ToString()}%", DbType.String);
                    strSql.Append(" AND e.F_RealName like @ProjectManager  ");
                }
                if (!queryParam["CustomerFullName"].IsEmpty())//发生日期
                {
                    dp.Add("CustomerFullName", $"%{queryParam["CustomerFullName"].ToString()}%", DbType.String);
                    strSql.Append(" AND g.FullName like @CustomerFullName  ");
                }
                if (!queryParam["JingBanRen"].IsEmpty())//发生日期
                {
                    dp.Add("JingBanRen", $"%{queryParam["JingBanRen"].ToString()}%", DbType.String);
                    strSql.Append(" AND d.F_RealName like @JingBanRen  ");
                }
                
                //if (!queryParam["SubjectName"].IsEmpty())//发生日期
                //{
                //    dp.Add("SubjectName", $"%{queryParam["SubjectName"].ToString()}%", DbType.String);
                //    strSql.Append(" AND t.SubjectName = @SubjectName  ");
                //}
                if (pagination != null)
                {
                    if (string.IsNullOrEmpty(pagination.sidx))
                    {
                        pagination.sidx = "OccurrenceDate";
                        pagination.sord = "DESC";
                    }
                    else if (pagination.sidx == "OccurrenceTypeValue")
                    {
                        pagination.sidx = "OccurrenceType";
                    }
                    else if (pagination.sidx == "ReceiptTypeValue")
                    {
                        pagination.sidx = "ReceiptType";
                    }
                }

                var list = this.BaseRepository().FindList<Base_FundAccoutDetailsEntity>(strSql.ToString(), dp, pagination);
                //  var query = from item in list orderby item.OccurrenceDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 账户操作
        /// </summary>
        /// <param name="doFundAccountEntity">记录明细</param>
        /// <param name="db">db访问对象</param>
        /// <param name="isCheckBalance">是否检测余额 true:不允许为余额负数, false:允许为余额负数</param>
        /// <returns>操作结果</returns>
        public Util.Common.OperateResultEntity DoFundAccount(DoFundAccountEntity doFundAccountEntity, IRepository db = null, bool isCheckBalance = true)
        {
            if (string.IsNullOrEmpty(doFundAccountEntity.FundAccountFlag) || doFundAccountEntity.AmountOccurrence == 0)
                return new Util.Common.OperateResultEntity() { Success = false, Message = "参数异常" };
            var dbs = db ?? this.BaseRepository();
            var fundAccount = dbs.FindEntity<Base_FundAccountEntity>(entity => entity.Name == doFundAccountEntity.FundAccountFlag
            || entity.BankCode == doFundAccountEntity.FundAccountFlag
            || entity.ProjectId == doFundAccountEntity.FundAccountFlag
            || entity.ID == doFundAccountEntity.FundAccountFlag);

            if (fundAccount == null)
                return new Util.Common.OperateResultEntity() { Success = false, Message = "标记账户为空" };

            if (!fundAccount.UsableBalance.HasValue)
                fundAccount.UsableBalance = 0;

            if (!fundAccount.Freezed.HasValue)
                fundAccount.Freezed = 0;

            if (!fundAccount.Balance.HasValue)
                fundAccount.Balance = 0;
            if (isCheckBalance)
            {
                if (doFundAccountEntity.OccurrenceType == OccurrenceEnum.Outlay && fundAccount.UsableBalance.Value < doFundAccountEntity.AmountOccurrence)
                    return new Util.Common.OperateResultEntity() { Success = false, Message = $"{fundAccount.Name}可用余额不足" };

                if (doFundAccountEntity.OccurrenceType == OccurrenceEnum.Freeze && fundAccount.UsableBalance.Value < doFundAccountEntity.AmountOccurrence)
                    return new Util.Common.OperateResultEntity() { Success = false, Message = $"{fundAccount.Name}可冻结余额不足" };

                if (doFundAccountEntity.OccurrenceType == OccurrenceEnum.Unfreeze && fundAccount.Freezed.Value < doFundAccountEntity.AmountOccurrence)
                    return new Util.Common.OperateResultEntity() { Success = false, Message = $"{fundAccount.Name}可解冻余额不足" };
            }
            //string sql = "";
            //string occurrenceDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //sql += string.Format("INSERT INTO Base_FundAccoutDetails(ID,FundAccountId,AmountOccurrence,OccurrenceType,RelevanceType,RelevanceId,ReceiptType,ReceiptId,OccurrenceDate,OperatorId,Abstract,ProjectSubjectId,ProjectId) VALUES('{0}','{1}',{2},'{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}');",
            //   Guid.NewGuid().ToString(),
            //   fundAccount.ID,
            //   doFundAccountEntity.AmountOccurrence,
            //   doFundAccountEntity.OccurrenceType.ToString(),
            //   doFundAccountEntity.RelevanceType, 
            //   doFundAccountEntity.RelevanceId, 
            //   doFundAccountEntity.ReceiptType, 
            //   doFundAccountEntity.ReceiptId,
            //   occurrenceDate, 
            //   doFundAccountEntity.OperatorId, 
            //   doFundAccountEntity.Abstract,
            //   doFundAccountEntity.ProjectSubjectId,
            //   doFundAccountEntity.ProjectId
            //   );
            //int affRow = dbs.ExecuteBySql(sql);


            decimal prevBalance = fundAccount.Balance ?? 0;
            if (doFundAccountEntity.OccurrenceType == OccurrenceEnum.Outlay)
            {
                fundAccount.UsableBalance -= doFundAccountEntity.AmountOccurrence;
                fundAccount.Balance -= doFundAccountEntity.AmountOccurrence;

                //sql += string.Format("update Base_FundAccount set UsableBalance = UsableBalance-{1},Balance = Balance -{1} where id = '{0}' and UsableBalance ={2} and Balance ={3};", 
                //    fundAccount.ID,
                //    doFundAccountEntity.AmountOccurrence,
                //    fundAccount.UsableBalance,
                //    fundAccount.Balance);
            }
            else if (doFundAccountEntity.OccurrenceType == OccurrenceEnum.Income)
            {
                fundAccount.UsableBalance += doFundAccountEntity.AmountOccurrence;
                fundAccount.Balance += doFundAccountEntity.AmountOccurrence;
                //sql += string.Format("update Base_FundAccount set UsableBalance = UsableBalance+{1},Balance = Balance +{1} where id = '{0}' and UsableBalance ={2} and Balance ={3};",
                //    fundAccount.ID,
                //    doFundAccountEntity.AmountOccurrence,
                //    fundAccount.UsableBalance,
                //    fundAccount.Balance);
            }
            else if (doFundAccountEntity.OccurrenceType == OccurrenceEnum.Freeze)
            {
                fundAccount.UsableBalance -= doFundAccountEntity.AmountOccurrence;
                fundAccount.Freezed += doFundAccountEntity.AmountOccurrence;
                //sql += string.Format("update Base_FundAccount set UsableBalance = UsableBalance-{1},Freezed = Freezed +{1} where id = '{0}' and UsableBalance ={2} and Freezed ={3};",
                //    fundAccount.ID,
                //    doFundAccountEntity.AmountOccurrence,
                //    fundAccount.UsableBalance,
                //    fundAccount.Freezed);
            }
            else if (doFundAccountEntity.OccurrenceType == OccurrenceEnum.Unfreeze)
            {
                fundAccount.UsableBalance += doFundAccountEntity.AmountOccurrence;
                fundAccount.Freezed -= doFundAccountEntity.AmountOccurrence;
                //sql += string.Format("update Base_FundAccount set UsableBalance = UsableBalance+{1},Freezed = Freezed -{1} where id = '{0}' and UsableBalance ={2} and Freezed ={3};",
                //    fundAccount.ID,
                //    doFundAccountEntity.AmountOccurrence,
                //    fundAccount.UsableBalance,
                //    fundAccount.Freezed);
            }

            Base_FundAccoutDetailsEntity base_FundAccoutDetailsEntity = new Base_FundAccoutDetailsEntity()
            {
                ID = Guid.NewGuid().ToString(),
                FundAccountId = fundAccount.ID,
                AmountOccurrence = doFundAccountEntity.AmountOccurrence,
                OccurrenceType = doFundAccountEntity.OccurrenceType.ToString(),
                RelevanceType = doFundAccountEntity.RelevanceType.ToString(),
                RelevanceId = doFundAccountEntity.RelevanceId,
                ReceiptType = doFundAccountEntity.ReceiptType.ToString(),
                ReceiptId = doFundAccountEntity.ReceiptId,
                OccurrenceDate = DateTime.Now,
                OperatorId = doFundAccountEntity.OperatorId,
                Abstract = doFundAccountEntity.Abstract,
                ProjectSubjectId = doFundAccountEntity.ProjectSubjectId,
                ProjectId = doFundAccountEntity.ProjectId,
                PrevBalance = prevBalance,
                Balance = fundAccount.Balance ?? 0
            };

            dbs.Insert<Base_FundAccoutDetailsEntity>(base_FundAccoutDetailsEntity);

            dbs.Update<Base_FundAccountEntity>(fundAccount);

            return new Util.Common.OperateResultEntity() { Success = true, Message = "" };
        }

        #endregion

    }
}
