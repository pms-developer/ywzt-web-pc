﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-02 18:14
    /// 描 述：资金帐户
    /// </summary>
    public class Base_FundAccountEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 账户名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 开户人
        /// </summary>
        [Column("HOLDER")]
        public string Holder { get; set; }
        /// <summary>
        /// 开户银行
        /// </summary>
        [Column("BANK")]
        public string Bank { get; set; }
        /// <summary>
        /// 账号
        /// </summary>
        [Column("BANKCODE")]
        public string BankCode { get; set; }
        /// <summary>
        /// 所属公司
        /// </summary>
        [Column("COMPANYID")]
        public string CompanyId { get; set; }
        /// <summary>
        /// 可用余额
        /// </summary>
        [Column("USABLEBALANCE")]
        [DecimalPrecision(18, 4)]
        public decimal? UsableBalance { get; set; } = 0;
        /// <summary>
        /// 余额
        /// </summary>
        [Column("BALANCE")]
        [DecimalPrecision(18, 4)]
        public decimal? Balance { get; set; } = 0;
        /// <summary>
        /// 冻结额
        /// </summary>
        [Column("FREEZED")]
        [DecimalPrecision(18, 4)]
        public decimal? Freezed { get; set; } = 0;
        /// <summary>
        /// 绑定项目
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// IsCanDelete
        /// </summary>
        [Column("ISCANDELETE")]
        public bool? IsCanDelete { get; set; }


        [Column("IsVirtual")]
        public bool? IsVirtual { get; set; }
        [NotMapped]
        public decimal? InitBalance { get; set; } = 0;
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
        #region  扩展字段


        [NotMapped]
        public string F_RealName { get; set; }


        #endregion
    }
}

