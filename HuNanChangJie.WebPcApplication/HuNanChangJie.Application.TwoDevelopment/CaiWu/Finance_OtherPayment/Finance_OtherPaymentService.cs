﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-16 18:32
    /// 描 述：其它付款
    /// </summary>
    public class Finance_OtherPaymentService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Finance_OtherPaymentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select  t.ID,
                t.is_verification,
                t.Code,
                b.Name as CustomerName,
                e.name BankId,
                t.ProjectID,
                t.PaymentDate,
                t.FundType,
                t.PaymentAmount,
                f.fullname ProjectSubjectId,
				t.ProjectManager,
                t.HuiPiaoId,
                t.BenPiaoId,
                t.ZhiPiaoId,
                t.Remark,
                t.PaymentType,
                d.f_realname OperatorId,
                t.CreationDate,
                t.AuditStatus,t.Workflow_ID  FROM Finance_OtherPayment t left join 
                (select * from (
                    select ID,Code,Name from Base_SubcontractingUnit
                    union 
                    select ID,Code,Name from Base_Supplier) as ttt)
                b on t.CustomerId = b.id left join Base_User d on t.OperatorId = d.f_userid
				 left join Base_FundAccount e on t.BankId = e.id left join Base_Subject f on t.ProjectSubjectId = f.id WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID= @ProjectID ");
                }
                if (!queryParam["is_verification"].IsEmpty())
                {
                    strSql.Append(" AND t.is_verification= 1 and (already_verification=0 or already_verification is null) ");
                }
                if (!queryParam["BankId"].IsEmpty())
                {
                    dp.Add("BankId", "%" + queryParam["BankId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND e.name Like @BankId ");
                }
                if (!queryParam["ProjectSubjectId"].IsEmpty())
                {
                    dp.Add("ProjectSubjectId", "%" + queryParam["ProjectSubjectId"].ToString() + "%", DbType.String);
                    strSql.Append(" AND f.fullname Like @ProjectSubjectId ");
                }
                if (!queryParam["Remark"].IsEmpty())
                {
                    dp.Add("Remark", "%" + queryParam["Remark"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Remark Like @Remark ");
                }
                if (!queryParam["PaymentAmount"].IsEmpty())
                {
                    dp.Add("PaymentAmount", queryParam["PaymentAmount"].ToString(), DbType.String);
                    strSql.Append(" AND t.PaymentAmount= @PaymentAmount ");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                var list = this.BaseRepository().FindList<Finance_OtherPaymentEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal OperateResultEntity BeforeSendingCheckData(string keyValue)
        {
            try
            {
                var op = new OperateResultEntity();
                op.Success = true;
                var mainInfo = this.BaseRepository().FindEntity<Finance_OtherPaymentEntity>(i => i.ID == keyValue);
                var bankinfo = BaseRepository().FindEntity<Base_FundAccountEntity>(i => i.ID == mainInfo.BankId);
                if (mainInfo.PaymentAmount > bankinfo.UsableBalance)
                {
                    op.Success = false;
                    op.Message = $"流程发送失败，当前账记的可用余额不足以完成【{mainInfo.PaymentAmount}】的转账";
                    return op;
                }

                
                return op;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Finance_OtherPayment表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Finance_OtherPaymentEntity GetFinance_OtherPaymentEntity(string keyValue)
        {
            try
            {
                string sql = " select a.*,fac.Bank+'('+fac.Name+')' as BankIdName from Finance_OtherPayment as a left join Base_FundAccount as fac on a.BankId=fac.id where a.id='" + keyValue + "'";
                return this.BaseRepository().FindEntity<Finance_OtherPaymentEntity>(sql, null);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_OtherPaymentEntity finance_OtherPaymentEntity = db.FindEntity<Finance_OtherPaymentEntity>(keyValue);
                if (finance_OtherPaymentEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                finance_OtherPaymentEntity.AuditStatus = "4";
                db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity);

                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = !string.IsNullOrEmpty(finance_OtherPaymentEntity.BankId) ? finance_OtherPaymentEntity.BankId : finance_OtherPaymentEntity.ProjectID,
                    AmountOccurrence = finance_OtherPaymentEntity.PaymentAmount.Value,
                    OccurrenceType = OccurrenceEnum.Income,
                    RelevanceType = RelevanceEnum.Other,
                    ReceiptType = ReceiptEnum.OtherPayment,
                    RelevanceId = finance_OtherPaymentEntity.ID,
                    ReceiptId = finance_OtherPaymentEntity.ID,
                    Abstract = $"{finance_OtherPaymentEntity.Remark}[其它付款单取消审核]",
                    ProjectId = finance_OtherPaymentEntity.ProjectID,
                    ProjectSubjectId = finance_OtherPaymentEntity.ProjectSubjectId
                }, db);

                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceOtherPayment");
                if (configInfo != null)
                {
                    var flag = configInfo.Value;

                    if (flag)
                    {
                        Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == finance_OtherPaymentEntity.ProjectID);
                        if (base_FundAccountEntity != null)
                        {
                            var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                            {
                                FundAccountFlag = base_FundAccountEntity.ID,
                                AmountOccurrence = finance_OtherPaymentEntity.PaymentAmount.Value,
                                OccurrenceType = OccurrenceEnum.Income,
                                RelevanceType = RelevanceEnum.Other,
                                ReceiptType = ReceiptEnum.OtherPayment,
                                RelevanceId = finance_OtherPaymentEntity.ID,
                                ReceiptId = finance_OtherPaymentEntity.ID,
                                Abstract = $"{finance_OtherPaymentEntity.Remark}[其它付款单取消审核]",
                                ProjectId = finance_OtherPaymentEntity.ProjectID,
                                ProjectSubjectId = finance_OtherPaymentEntity.ProjectSubjectId
                            }, db, false);
                        }
                    }
                }
                var subjectInfo = new ProjectSubjectBll().GetEntity(finance_OtherPaymentEntity.ProjectSubjectId, finance_OtherPaymentEntity.ProjectID);
                if (subjectInfo != null)
                //    return new Util.Common.OperateResultEntity() { Success = false, Message = "未找到对应的科目" };
                //else
                {
                    subjectInfo.ActuallyOccurred = (subjectInfo.ActuallyOccurred ?? 0) - (finance_OtherPaymentEntity.PaymentAmount ?? 0);
                    subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) - (finance_OtherPaymentEntity.PaymentAmount ?? 0);

                    //decimal afterTax = item.PayAmount.Value - (item.PayAmount.Value * purchase_ContractEntity.TaxRage.Value / 100);
                    //subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;

                    db.Update<ProjectSubjectEntity>(subjectInfo);
                }
                if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }

                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Finance_OtherPaymentEntity finance_OtherPaymentEntity = db.FindEntity<Finance_OtherPaymentEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (finance_OtherPaymentEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                finance_OtherPaymentEntity.AuditStatus = "2";
                db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity);

                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = !string.IsNullOrEmpty(finance_OtherPaymentEntity.BankId) ? finance_OtherPaymentEntity.BankId : finance_OtherPaymentEntity.ProjectID,
                    AmountOccurrence = finance_OtherPaymentEntity.PaymentAmount.Value,
                    OccurrenceType = OccurrenceEnum.Outlay,
                    RelevanceType = RelevanceEnum.Other,
                    ReceiptType = ReceiptEnum.OtherPayment,
                    RelevanceId = finance_OtherPaymentEntity.ID,
                    ReceiptId = finance_OtherPaymentEntity.ID,
                    Abstract = $"{finance_OtherPaymentEntity.Remark}[其它付款单审核]",
                    ProjectId = finance_OtherPaymentEntity.ProjectID,
                    ProjectSubjectId = finance_OtherPaymentEntity.ProjectSubjectId
                }, db);

                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-FinanceOtherPayment");
                if (configInfo != null)
                {
                    var flag = configInfo.Value;

                    if (flag)
                    {
                        Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == finance_OtherPaymentEntity.ProjectID);
                        if (base_FundAccountEntity != null)
                        {
                            var result1 = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                            {
                                FundAccountFlag = base_FundAccountEntity.ID,
                                AmountOccurrence = finance_OtherPaymentEntity.PaymentAmount.Value,
                                OccurrenceType = OccurrenceEnum.Outlay,
                                RelevanceType = RelevanceEnum.Other,
                                ReceiptType = ReceiptEnum.OtherPayment,
                                RelevanceId = finance_OtherPaymentEntity.ID,
                                ReceiptId = finance_OtherPaymentEntity.ID,
                                Abstract = $"{finance_OtherPaymentEntity.Remark}[其它付款单审核]",
                                ProjectId = finance_OtherPaymentEntity.ProjectID,
                                ProjectSubjectId = finance_OtherPaymentEntity.ProjectSubjectId
                            }, db, false);
                        }
                    }
                }
                var subjectInfo = new ProjectSubjectBll().GetEntity(finance_OtherPaymentEntity.ProjectSubjectId, finance_OtherPaymentEntity.ProjectID);
                if (subjectInfo != null)
                //    return new Util.Common.OperateResultEntity() { Success = false, Message = "未找到对应的科目" };
                //else
                {
                    subjectInfo.ActuallyOccurred = (subjectInfo.ActuallyOccurred ?? 0) + (finance_OtherPaymentEntity.PaymentAmount ?? 0);
                    subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + (finance_OtherPaymentEntity.PaymentAmount ?? 0);

                    db.Update<ProjectSubjectEntity>(subjectInfo);
                }
                if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }

                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Finance_OtherPaymentEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Finance_OtherPaymentEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
