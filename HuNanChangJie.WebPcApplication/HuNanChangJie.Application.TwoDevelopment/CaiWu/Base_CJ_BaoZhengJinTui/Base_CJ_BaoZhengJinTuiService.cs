﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-05 14:12
    /// 描 述：保证金退回管理
    /// </summary>
    public class Base_CJ_BaoZhengJinTuiService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_BaoZhengJinTuiEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select  t.ID,
t.Interest,
t.ActualAmountReturned,
t.ProjectManager,
t.ServiceManagementFee,
                t.BianHao,
                ee.bianhao FuKuanDan,
                t.TuiHuiFangShi,
                dd.name TuiHuiZhangHu,
                cc.f_realname JinBanRen,
                t.TuiHuiShiJian,
                t.ShuoMing,
                t.AuditStatus,
                t.ProjectID,
                t.Workflow_ID,
                t.CreationDate  FROM Base_CJ_BaoZhengJinTui t left join base_user cc on t.JinBanRen = cc.f_userid left join Base_FundAccount dd on t.TuiHuiZhangHu = dd.id left join Base_CJ_BaoZhengJinzhifu ee on t.FuKuanDan = ee.id WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND (t.TuiHuiShiJian >= @startTime AND t.TuiHuiShiJian <= @endTime ) ");
                }
                if (!queryParam["TuiHuiZhangHu"].IsEmpty())
                {
                    dp.Add("TuiHuiZhangHu", "%" + queryParam["TuiHuiZhangHu"].ToString() + "%", DbType.String);
                    strSql.Append(" AND dd.name Like @TuiHuiZhangHu ");
                }

                if (!queryParam["ShuoMing"].IsEmpty())
                {
                    dp.Add("ShuoMing", "%" + queryParam["ShuoMing"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ShuoMing Like @ShuoMing ");
                }

                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Base_CJ_BaoZhengJinTuiEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_BaoZhengJinTui表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_BaoZhengJinTuiEntity GetBase_CJ_BaoZhengJinTuiEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_CJ_BaoZhengJinTuiEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据


        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Base_CJ_BaoZhengJinTuiEntity baoZhengJinTuiEntity = db.FindEntity<Base_CJ_BaoZhengJinTuiEntity>(keyValue);

                Base_CJ_BaoZhengJinZhiFuEntity baoZhengJinZhiFuEntity = db.FindEntity<Base_CJ_BaoZhengJinZhiFuEntity>(baoZhengJinTuiEntity.FuKuanDan);

                Base_CJ_BaoZhengJinEntity baoZhengJinEntity = db.FindEntity<Base_CJ_BaoZhengJinEntity>(baoZhengJinZhiFuEntity.ShenQinDan);

                baoZhengJinEntity.ZhiFuZhuangTai = "未退回";
                db.Update<Base_CJ_BaoZhengJinEntity>(baoZhengJinEntity);

                if (baoZhengJinTuiEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                baoZhengJinTuiEntity.AuditStatus = "4";
                db.Update<Base_CJ_BaoZhengJinTuiEntity>(baoZhengJinTuiEntity);

                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = baoZhengJinZhiFuEntity.FuKuanZhangHu,

                    AmountOccurrence = baoZhengJinTuiEntity.ActualAmountReturned.HasValue ? baoZhengJinTuiEntity.ActualAmountReturned.Value : 0,
                    OccurrenceType = OccurrenceEnum.Outlay,
                    RelevanceType = RelevanceEnum.Project,
                   // ReceiptType = ReceiptEnum.BondReturn,
                    RelevanceId = baoZhengJinTuiEntity.ID,
                    ReceiptId = baoZhengJinTuiEntity.ID,
                    Abstract = $"{baoZhengJinTuiEntity.ShuoMing}[保证金退回取消审核]",
                    ProjectId = baoZhengJinZhiFuEntity.ProjectID
                }, db);

                Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == baoZhengJinEntity.Project_ID);

                if (base_FundAccountEntity != null && base_FundAccountEntity.ID != baoZhengJinZhiFuEntity.FuKuanZhangHu)
                {
                    //var result1 = 
                    new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        
                        FundAccountFlag = base_FundAccountEntity.ID,
                        AmountOccurrence = baoZhengJinTuiEntity.ActualAmountReturned.HasValue ? baoZhengJinTuiEntity.ActualAmountReturned.Value : 0,
                        OccurrenceType = OccurrenceEnum.Outlay,
                        RelevanceType = RelevanceEnum.Project,
                       // ReceiptType = ReceiptEnum.BondReturn,
                        RelevanceId = baoZhengJinTuiEntity.ID,
                        ReceiptId = baoZhengJinTuiEntity.ID,
                        Abstract = $"{baoZhengJinTuiEntity.ShuoMing}保证金退回取消审核",
                        ProjectId = baoZhengJinZhiFuEntity.ProjectID
                    }, db, false);
                    //if (!result1.Success)
                    //    throw new Exception(result1.Message);
                }

                if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }

                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Base_CJ_BaoZhengJinTuiEntity baoZhengJinTuiEntity = db.FindEntity<Base_CJ_BaoZhengJinTuiEntity>(keyValue);

                Base_CJ_BaoZhengJinZhiFuEntity baoZhengJinZhiFuEntity = db.FindEntity<Base_CJ_BaoZhengJinZhiFuEntity>(baoZhengJinTuiEntity.FuKuanDan);

                Base_CJ_BaoZhengJinEntity baoZhengJinEntity = db.FindEntity<Base_CJ_BaoZhengJinEntity>(baoZhengJinZhiFuEntity.ShenQinDan);
                baoZhengJinEntity.ZhiFuZhuangTai = "已退回";
                db.Update<Base_CJ_BaoZhengJinEntity>(baoZhengJinEntity);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (baoZhengJinTuiEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                baoZhengJinTuiEntity.AuditStatus = "2";
                db.Update<Base_CJ_BaoZhengJinTuiEntity>(baoZhengJinTuiEntity);

                var result = new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                {
                    FundAccountFlag = baoZhengJinZhiFuEntity.FuKuanZhangHu,
                    // AmountOccurrence = baoZhengJinEntity.JinE.HasValue ? baoZhengJinEntity.JinE.Value : 0,
                    AmountOccurrence = baoZhengJinTuiEntity.ActualAmountReturned.HasValue ? baoZhengJinTuiEntity.ActualAmountReturned.Value : 0,

                    OccurrenceType = OccurrenceEnum.Income,
                    RelevanceType = RelevanceEnum.Project,
                   // ReceiptType = ReceiptEnum.BondReturn,
                    RelevanceId = baoZhengJinTuiEntity.ID,
                    ReceiptId = baoZhengJinTuiEntity.ID,
                    Abstract = $"{baoZhengJinTuiEntity.ShuoMing}[保证金退回审核]",
                    ProjectId = baoZhengJinZhiFuEntity.ProjectID
                }, db);
                Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == baoZhengJinEntity.Project_ID);

                if (base_FundAccountEntity != null && base_FundAccountEntity.ID != baoZhengJinZhiFuEntity.FuKuanZhangHu)
                {
                    //var result1 = 
                    new Base_FundAccountService().DoFundAccount(new DoFundAccountEntity()
                    {
                        FundAccountFlag = base_FundAccountEntity.ID,
                        AmountOccurrence = baoZhengJinTuiEntity.ActualAmountReturned.HasValue ? baoZhengJinTuiEntity.ActualAmountReturned.Value : 0,
                        OccurrenceType = OccurrenceEnum.Income,
                        RelevanceType = RelevanceEnum.Project,
                       // ReceiptType = ReceiptEnum.BondReturn,
                        RelevanceId = baoZhengJinTuiEntity.ID,
                        ReceiptId = baoZhengJinTuiEntity.ID,
                        Abstract = $"{baoZhengJinTuiEntity.ShuoMing}[保证金退回审核]",
                        ProjectId = baoZhengJinZhiFuEntity.ProjectID
                    }, db, false);
                    //if (!result1.Success)
                    //    throw new Exception(result1.Message);
                }

                if (result.Success)
                {
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                }

                return result;
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_CJ_BaoZhengJinTuiEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_BaoZhengJinTuiEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
