﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-05 14:12
    /// 描 述：保证金退回管理
    /// </summary>
    public interface Base_CJ_BaoZhengJinTuiIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_CJ_BaoZhengJinTuiEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_CJ_BaoZhengJinTui表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_CJ_BaoZhengJinTuiEntity GetBase_CJ_BaoZhengJinTuiEntity(string keyValue);
        #endregion

        #region  提交数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_CJ_BaoZhengJinTuiEntity entity,string deleteList,string type);
        #endregion

    }
}
