﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.CaiWu
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-01-05 14:12
    /// 描 述：保证金退回管理
    /// </summary>
    public class Base_CJ_BaoZhengJinTuiBLL : Base_CJ_BaoZhengJinTuiIBLL
    {
        private Base_CJ_BaoZhengJinTuiService base_CJ_BaoZhengJinTuiService = new Base_CJ_BaoZhengJinTuiService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_CJ_BaoZhengJinTuiEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return base_CJ_BaoZhengJinTuiService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_CJ_BaoZhengJinTui表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_CJ_BaoZhengJinTuiEntity GetBase_CJ_BaoZhengJinTuiEntity(string keyValue)
        {
            try
            {
                return base_CJ_BaoZhengJinTuiService.GetBase_CJ_BaoZhengJinTuiEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return base_CJ_BaoZhengJinTuiService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return base_CJ_BaoZhengJinTuiService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                base_CJ_BaoZhengJinTuiService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_CJ_BaoZhengJinTuiEntity entity, string deleteList, string type)
        {
            try
            {
                base_CJ_BaoZhengJinTuiService.SaveEntity(keyValue, entity, deleteList, type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0007");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
    }
}
