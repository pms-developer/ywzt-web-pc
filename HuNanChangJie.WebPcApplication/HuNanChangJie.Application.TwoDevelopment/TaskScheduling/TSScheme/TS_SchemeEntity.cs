﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.TaskScheduling
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-18 12:52
    /// 描 述：TSScheme
    /// </summary>
    public class TS_SchemeEntity 
    {
        #region  实体成员
        /// <summary>
        /// 任务模板信息主键
        /// </summary>
        [Column("F_SCHEMEINFOID")]
        public string F_SchemeInfoId { get; set; }
        /// <summary>
        /// 标志1.启用2.未启用
        /// </summary>
        [Column("F_ISACTIVE")]
        public int? F_IsActive { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建人主键
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建人姓名
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// F_Scheme
        /// </summary>
        [Column("F_SCHEME")]
        public string F_Scheme { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

