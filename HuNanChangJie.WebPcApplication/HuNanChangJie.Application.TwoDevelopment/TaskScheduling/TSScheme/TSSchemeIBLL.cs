﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.TaskScheduling
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-18 12:52
    /// 描 述：TSScheme
    /// </summary>
    public interface TSSchemeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<TS_SchemeEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取TS_Scheme表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        TS_SchemeEntity GetTS_SchemeEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, TS_SchemeEntity entity);
        #endregion

    }
}
