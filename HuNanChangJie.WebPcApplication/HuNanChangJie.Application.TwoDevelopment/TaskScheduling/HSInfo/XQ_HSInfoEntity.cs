﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.TaskScheduling
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-04-08 17:59
    /// 描 述：222
    /// </summary>
    public class XQ_HSInfoEntity 
    {
        #region  实体成员
        /// <summary>
        /// F_ID
        /// </summary>
        [Column("F_ID")]
        public string F_ID { get; set; }
        /// <summary>
        /// 信息标题
        /// </summary>
        [Column("F_TITLE")]
        public string F_Title { get; set; }
        /// <summary>
        /// 预约看货时间
        /// </summary>
        [Column("F_YYKH_TIME")]
        public DateTime? F_YYKH_Time { get; set; }
        /// <summary>
        /// 看货时段
        /// </summary>
        [Column("F_KHSD")]
        public string F_KHSD { get; set; }
        /// <summary>
        /// 产品单位
        /// </summary>
        [Column("F_CPUNIT")]
        public string F_CPUnit { get; set; }
        /// <summary>
        /// 产品数量
        /// </summary>
        [Column("F_CPCOUNT")]
        public string F_CPCount { get; set; }
        /// <summary>
        /// 处理单价
        /// </summary>
        [Column("F_CLDL")]
        public string F_CLDL { get; set; }
        /// <summary>
        /// 价值细信息
        /// </summary>
        [Column("F_JZXX")]
        public string F_JZXX { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        [Column("F_LXR")]
        public string F_LXR { get; set; }
        /// <summary>
        /// 货源地址
        /// </summary>
        [Column("F_ADDRESS")]
        public string F_Address { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [Column("F_PHONENUMBER")]
        public string F_PhoneNumber { get; set; }
        /// <summary>
        /// 排序码
        /// </summary>
        [Column("F_SORTCODE")]
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 删除标记
        /// </summary>
        [Column("F_DELETEMARK")]
        public int? F_DeleteMark { get; set; }
        /// <summary>
        /// 有效标志
        /// </summary>
        [Column("F_ENABLEDMARK")]
        public int? F_EnabledMark { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建用户主键
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        [Column("ModificationDate")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改用户主键
        /// </summary>
        [Column("Modification_Id")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改用户
        /// </summary>
        [Column("ModificationName")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column("F_STATUS")]
        public string F_Status { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(UserInfo userInfo)
      {
            this.F_ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue, UserInfo userInfo)
        {
            this.F_ID = keyValue;
            this.ModificationDate = DateTime.Now;
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

