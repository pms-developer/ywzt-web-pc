﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.TaskScheduling
{
    /// <summary> 
        ///  
        ///   
        /// 创 建：超级管理员 
        /// 日 期：2019-01-22 14:09 
        /// 描 述：服务器列表 
        /// </summary> 
    public class ServiceInfoBLL : ServiceInfoIBLL
    {
        private ServiceInfoService serviceInfoService = new ServiceInfoService();

        #region  获取数据 

        /// <summary> 
                /// 获取页面显示列表数据 
                /// <summary> 
                /// <param name="queryJson">查询参数</param> 
                /// <returns></returns> 
        public IEnumerable<serv_infoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return serviceInfoService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
                /// 获取serv_info表实体数据 
                /// <param name="keyValue">主键</param> 
                /// <summary> 
                /// <returns></returns> 
        public serv_infoEntity Getserv_infoEntity(string keyValue)
        {
            try
            {
                return serviceInfoService.Getserv_infoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据 

        /// <summary> 
                /// 删除实体数据 
                /// <param name="keyValue">主键</param> 
                /// <summary> 
                /// <returns></returns> 
        public void DeleteEntity(string keyValue)
        {
            try
            {
                serviceInfoService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
                /// 保存实体数据（新增、修改） 
                /// <param name="keyValue">主键</param> 
                /// <summary> 
                /// <returns></returns> 
        public void SaveEntity(string keyValue, serv_infoEntity entity)
        {
            try
            {
                serviceInfoService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}