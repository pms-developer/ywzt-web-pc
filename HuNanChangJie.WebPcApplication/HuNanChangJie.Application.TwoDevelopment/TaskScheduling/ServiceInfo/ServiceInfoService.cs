﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.TaskScheduling
{
    /// <summary> 
        ///  
        ///   
        /// 创 建：超级管理员 
        /// 日 期：2019-01-22 14:09 
        /// 描 述：服务器列表 
        /// </summary> 
    public class ServiceInfoService : RepositoryFactory
    {
        #region  获取数据 

        /// <summary> 
                /// 获取页面显示列表数据 
                /// <summary> 
                /// <param name="queryJson">查询参数</param> 
                /// <returns></returns> 
        public IEnumerable<serv_infoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@" 
	            t.`servid`,
				t.`ip`,
				t.`updatetime`,
				t.`addtime`,
				t.`status`,
				t.`isleader`");
                strSql.Append(" FROM serv_info t");
                strSql.Append(" WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数 
                var dp = new DynamicParameters(new { });
                return this.BaseRepository("TaskScheduling").FindList<serv_infoEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
                /// 获取serv_info表实体数据 
                /// <param name="keyValue">主键</param> 
                /// <summary> 
                /// <returns></returns> 
        public serv_infoEntity Getserv_infoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository("TaskScheduling").FindEntity<serv_infoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据 

        /// <summary> 
                /// 删除实体数据 
                /// <param name="keyValue">主键</param> 
                /// <summary> 
                /// <returns></returns> 
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository("TaskScheduling").Delete<serv_infoEntity>(t => t.servid == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
                /// 保存实体数据（新增、修改） 
                /// <param name="keyValue">主键</param> 
                /// <summary> 
                /// <returns></returns> 
        public void SaveEntity(string keyValue, serv_infoEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository("TaskScheduling").Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository("TaskScheduling").Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}