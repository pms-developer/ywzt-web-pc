﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.TaskScheduling
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2019-01-22 14:09 
    /// 描 述：服务器列表 
    /// </summary> 
    public class serv_infoEntity
    {
        #region  实体成员 
        /// <summary> 
        /// 编号 
        /// </summary> 
        [Column("SERVID")]
        public string servid { get; set; }
        /// <summary> 
        /// ip地址 
        /// </summary> 
        [Column("IP")]
        public string ip { get; set; }
        /// <summary> 
        /// 更新时间 
        /// </summary> 
        [Column("UPDATETIME")]
        public DateTime? updatetime { get; set; }
        /// <summary> 
        /// 添加时间 
        /// </summary> 
        [Column("ADDTIME")]
        public DateTime? addtime { get; set; }
        /// <summary> 
        /// 状态[10正常、20已销毁] 
        /// </summary> 
        [Column("STATUS")]
        public int? status { get; set; }
        /// <summary> 
        /// 是否Leader[0否、1是] 
        /// </summary> 
        [Column("ISLEADER")]
        public int? isleader { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.servid = Guid.NewGuid().ToString();
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.servid = keyValue;
        }
        #endregion
        #region  扩展字段 
        #endregion
    }
}