﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.AuthorizeModule;
using HuNanChangJie.Application.Organization;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:59
    /// 描 述：NWF_Task
    /// </summary>
    public class NWFTaskBLL : NWFTaskIBLL
    {
        private NWFTaskService nWFTaskService = new NWFTaskService();
        private UserIBLL userIBLL = new UserBLL();
        private UserRelationIBLL userRelationIBLL = new UserRelationBLL();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<NWF_TaskEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return nWFTaskService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取NWF_Task表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public NWF_TaskEntity GetNWF_TaskEntity(string keyValue)
        {
            try
            {
                return nWFTaskService.GetNWF_TaskEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                nWFTaskService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, NWF_TaskEntity entity)
        {
            try
            {
                nWFTaskService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取未处理任务列表
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <param name="pagination">翻页信息</param>
        /// <param name="queryJson">查询条件</param>
        /// <returns></returns>
        public IEnumerable<NWF_ProcessEntity> GetActiveList(UserInfo userInfo, XqPagination pagination, string queryJson)
        {
            try
            {
                List<UserInfo> delegateList = GetDelegateTask(userInfo.userId);
                return nWFTaskService.GetActiveList(userInfo, pagination, queryJson, delegateList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取委托信息列表
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<UserInfo> GetDelegateTask(string userId)
        {
            try
            {
                List<UserInfo> list = nWFTaskService.GetDelegateTask(userId);

                foreach (var item in list)
                {
                    UserEntity userEntity = userIBLL.GetEntityByUserId(item.userId);
                    item.companyId = userEntity.F_CompanyId;
                    item.departmentId = userEntity.F_DepartmentId;
                    item.roleIds = userRelationIBLL.GetObjectIds(userEntity.F_UserId, 1);
                    item.postIds = userRelationIBLL.GetObjectIds(userEntity.F_UserId, 2);
                }

                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public IEnumerable<NWF_ProcessEntity> GetHasList(string userId, XqPagination pagination, string queryJson)
        {
            try
            {
                return nWFTaskService.GetHasList(userId, pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
