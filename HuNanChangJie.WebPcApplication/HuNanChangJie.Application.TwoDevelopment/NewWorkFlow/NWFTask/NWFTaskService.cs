﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:59
    /// 描 述：NWF_Task
    /// </summary>
    public class NWFTaskService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<NWF_TaskEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_Id,
                t.F_ProcessId,
                t.F_NodeId,
                t.F_NodeName,
                t.F_FirstUserId,
                t.F_ChildProcessId,
                t.F_Type,
                t.F_IsFinished,
                t.F_TimeoutAction,
                t.F_TimeoutNotice,
                t.F_TimeoutInterval,
                t.F_TimeoutStrategy,
                t.F_PrevNodeId,
                t.F_PrevNodeName,
                t.CreationDate,
                t.Creation_Id,
                t.ModificationDate,
                t.Modification_Id,
                t.CreationName
                ");
                strSql.Append("  FROM NWF_Task t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                return this.BaseRepository().FindList<NWF_TaskEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取NWF_Task表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public NWF_TaskEntity GetNWF_TaskEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<NWF_TaskEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<NWF_TaskEntity>(t=>t.F_Id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, NWF_TaskEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取未处理任务列表
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <param name="pagination">翻页信息</param>
        /// <param name="queryJson">查询条件</param>
        /// <returns></returns>
        public IEnumerable<NWF_ProcessEntity> GetActiveList(UserInfo userInfo, XqPagination pagination, string queryJson, List<UserInfo> delegateList)
        {
            try
            {
                string userId = userInfo.userId;
                string postIds = "'" + userInfo.postIds.Replace(",", "','") + "'";
                string roleIds = "'" + userInfo.roleIds.Replace(",", "','") + "'";
                string companyId = userInfo.companyId;
                string departmentId = userInfo.departmentId;

                // 获取委托信息
                var strSql = new StringBuilder();
                strSql.Append(@"SELECT
	                                 p.F_Id
			                        ,p.F_SchemeId
			                        ,p.F_SchemeCode
			                        ,p.F_SchemeName
			                        ,p.F_Title
			                        ,p.F_Level
			                        ,p.F_EnabledMark
			                        ,p.F_IsAgain
			                        ,p.F_IsFinished
			                        ,p.F_IsChild
			                        ,p.F_IsAsyn
			                        ,p.F_ParentNodeId
			                        ,p.F_ParentTaskId
			                        ,p.F_ParentProcessId
			                        ,p.F_IsStart
			                        ,p.CreationDate
			                        ,p.Creation_Id
			                        ,p.CreationName
			                        ,a.F_TaskName
			                        ,a.F_TaskId
			                        ,a.F_TaskType
			                        ,a.F_IsUrge  
                                FROM
                                (
                                    SELECT
	                                   MAX(t.F_Id) AS F_TaskId,
			                           MAX(t.F_Type) AS F_TaskType,
			                           t.F_ProcessId,
			                           t.F_NodeName AS F_TaskName,
                
	                                      t.F_IsUrge
                                    FROM
	                                    NWF_Task t WHERE t.F_IsFinished = 0 ");
                strSql.Append(" AND ( t.F_AuditorId = '1' OR ");
                strSql.Append("  t.F_AuditorId = @userId ");
                if (!string.IsNullOrEmpty(userInfo.postIds))
                {
                    strSql.Append("  OR (t.F_AuditorId in (" + postIds + ") AND t.F_CompanyId = '1' AND t.F_DepartmentId = '1' ) ");
                    strSql.Append("  OR (t.F_AuditorId in (" + postIds + ") AND t.F_CompanyId = @companyId ) ");
                    strSql.Append("  OR (t.F_AuditorId in (" + postIds + ") AND t.F_DepartmentId = @departmentId ) ");
                }
                if (!string.IsNullOrEmpty(userInfo.roleIds))
                {
                    strSql.Append("  OR (t.F_AuditorId in (" + roleIds + ") AND t.F_CompanyId = '1' AND t.F_DepartmentId = '1' ) ");
                    strSql.Append("  OR (t.F_AuditorId in (" + roleIds + ") AND t.F_CompanyId = @companyId ) ");
                    strSql.Append("  OR (t.F_AuditorId in (" + roleIds + ") AND t.F_DepartmentId = @departmentId) ");
                }
                // 添加委托信息
                foreach (var item in delegateList)
                {
                    string processId = "'" + item.wfProcessId.Replace(",", "','") + "'";
                    string postIds2 = "'" + item.postIds.Replace(",", "','") + "'";
                    string roleIds2 = "'" + item.roleIds.Replace(",", "','") + "'";
                    string userI2 = "'" + item.userId + "'";
                    string companyId2 = "'" + item.companyId + "'";
                    string departmentId2 = "'" + item.departmentId + "'";

                    strSql.Append("  OR (t.F_AuditorId =" + userI2 + " AND t.F_ProcessId in (" + processId + ") )");

                    if (!string.IsNullOrEmpty(item.postIds))
                    {
                        strSql.Append("  OR (t.F_AuditorId in (" + postIds2 + ") AND t.F_CompanyId = '1' AND t.F_DepartmentId = '1' AND t.F_ProcessId in (" + processId + ")  ) ");
                        strSql.Append("  OR (t.F_AuditorId in (" + postIds2 + ") AND t.F_CompanyId = " + companyId2 + " AND t.F_ProcessId in (" + processId + ") ) ");
                        strSql.Append("  OR (t.F_AuditorId in (" + postIds2 + ") AND t.F_DepartmentId = " + departmentId2 + " AND t.F_ProcessId in (" + processId + ") ) ");
                    }

                    if (!string.IsNullOrEmpty(item.roleIds))
                    {
                        strSql.Append("  OR (t.F_AuditorId in (" + roleIds2 + ") AND t.F_CompanyId = '1' AND t.F_DepartmentId = '1' AND t.F_ProcessId in (" + processId + ")  ) ");
                        strSql.Append("  OR (t.F_AuditorId in (" + roleIds2 + ") AND t.F_CompanyId = " + companyId2 + " AND t.F_ProcessId in (" + processId + ") ) ");
                        strSql.Append("  OR (t.F_AuditorId in (" + roleIds2 + ") AND t.F_DepartmentId = " + departmentId2 + " AND t.F_ProcessId in (" + processId + ") ) ");
                    }
                }


                strSql.Append(@" ) GROUP BY
	                                t.F_ProcessId,
	                                t.F_NodeId,
	                                t.F_NodeName,

	t.F_IsUrge)a LEFT JOIN NWF_Process p ON p.F_Id = a.F_ProcessId WHERE 1=1 AND (p.F_IsFinished = 0 OR a.F_TaskType = 3) AND p.F_EnabledMark = 1 ");

                var queryParam = queryJson.ToJObject();
                DateTime startTime = DateTime.Now, endTime = DateTime.Now;

                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    startTime = queryParam["StartTime"].ToDate();
                    endTime = queryParam["EndTime"].ToDate();
                    strSql.Append(" AND ( p.CreationDate >= @startTime AND  p.CreationDate <= @endTime ) ");
                }
                string keyword = "";
                if (!queryParam["keyword"].IsEmpty())
                {
                    keyword = "%" + queryParam["keyword"].ToString() + "%";
                    strSql.Append(" AND ( p.F_Title like @keyword OR  p.F_SchemeName like @keyword ) ");
                }


                return this.BaseRepository().FindList<NWF_ProcessEntity>(strSql.ToString(), new { userId, companyId, departmentId, startTime, endTime, keyword }, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal List<UserInfo> GetDelegateTask(string userId)
        {
            try
            {
                List<UserInfo> delegateUserlist = new List<UserInfo>();
                DateTime datatime = DateTime.Now;
                IEnumerable<NWF_DelegateRuleEntity> wfDelegateRuleList = this.BaseRepository().FindList<NWF_DelegateRuleEntity>(t => t.F_ToUserId == userId && t.F_BeginDate >= datatime && t.F_EndDate <= datatime);
                foreach (var item in wfDelegateRuleList)
                {
                    UserInfo userinfo = new UserInfo();
                    userinfo.userId = item.Creation_Id;

                    var strSql = new StringBuilder();
                    strSql.Append(@"SELECT
                                    p.F_Id
                                FROM

                                    NWF_DelegateRelation d
                                LEFT JOIN NWF_SchemeInfo s ON s.F_Id = d.F_SchemeInfoId
                                LEFT JOIN NWF_Process p ON p.F_SchemeCode = s.F_Code
                                WHERE
                                    p.F_Id IS NOT NULL
                                AND p.F_IsFinished = 0 AND d.F_DelegateRuleId = @DelegateRuleId ");

                    DataTable dt = this.BaseRepository().FindTable(strSql.ToString(), new { DelegateRuleId = item.F_Id });
                    userinfo.wfProcessId = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (!string.IsNullOrEmpty(dr[0].ToString()))
                        {
                            if (!string.IsNullOrEmpty(userinfo.wfProcessId))
                            {
                                userinfo.wfProcessId += ",";
                            }
                            userinfo.wfProcessId += dr[0].ToString();
                        }
                    }

                    if (!string.IsNullOrEmpty(userinfo.wfProcessId))
                    {
                        delegateUserlist.Add(userinfo);
                    }
                }
                return delegateUserlist;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        /// <summary>
        /// 获取已处理任务列表
        /// </summary>
        /// <param name="userId">用户主键</param>
        /// <param name="pagination">翻页信息</param>
        /// <param name="queryJson">查询条件</param>
        /// <returns></returns>
        public IEnumerable<NWF_ProcessEntity> GetHasList(string userId, XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"SELECT
	                                 p.F_Id
			                        ,p.F_SchemeId
			                        ,p.F_SchemeCode
			                        ,p.F_SchemeName
			                        ,p.F_Title
			                        ,p.F_Level
			                        ,p.F_EnabledMark
			                        ,p.F_IsAgain
			                        ,p.F_IsFinished
			                        ,p.F_IsChild
			                        ,p.F_IsAsyn
			                        ,p.F_ParentNodeId
			                        ,p.F_ParentTaskId
			                        ,p.F_ParentProcessId
			                        ,p.F_IsStart
			                        ,p.CreationDate
			                        ,p.Creation_Id
			                        ,p.CreationName
			                        ,t.F_NodeName as F_TaskName
			                        ,t.F_ID as F_TaskId
			                        ,t.F_Type as  F_TaskType
			                        ,t.F_IsUrge  
                                FROM
	                                NWF_Task t
                                LEFT JOIN NWF_Process p ON t.F_ProcessId = p.F_Id
                                WHERE
	                                t.F_IsFinished = 1 AND t.Modification_Id = @userId
                ");


                var queryParam = queryJson.ToJObject();
                DateTime startTime = DateTime.Now, endTime = DateTime.Now;

                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    startTime = queryParam["StartTime"].ToDate();
                    endTime = queryParam["EndTime"].ToDate();
                    strSql.Append(" AND ( p.CreationDate >= @startTime AND  p.CreationDate <= @endTime ) ");
                }
                string keyword = "";
                if (!queryParam["keyword"].IsEmpty())
                {
                    keyword = "%" + queryParam["keyword"].ToString() + "%";
                    strSql.Append(" AND ( p.F_Title like @keyword OR  p.F_SchemeName like @keyword ) ");
                }

                return this.BaseRepository().FindList<NWF_ProcessEntity>(strSql.ToString(), new { userId = userId, startTime = startTime, endTime = endTime, keyword = keyword }, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }

        }
    }
}
