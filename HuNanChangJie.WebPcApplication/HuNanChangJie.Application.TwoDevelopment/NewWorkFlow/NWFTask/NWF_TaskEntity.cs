﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:59
    /// 描 述：NWF_Task
    /// </summary>
    public class NWF_TaskEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 流程实例主键
        /// </summary>
        [Column("F_PROCESSID")]
        public string F_ProcessId { get; set; }
        /// <summary>
        /// 流程节点ID
        /// </summary>
        [Column("F_NODEID")]
        public string F_NodeId { get; set; }
        /// <summary>
        /// 流程节点名称
        /// </summary>
        [Column("F_NODENAME")]
        public string F_NodeName { get; set; }
        /// <summary>
        /// 任务类型1审批2传阅3加签4子流程5重新创建
        /// </summary>
        [Column("F_TYPE")]
        public int? F_Type { get; set; }
        /// <summary>
        /// 是否完成1完成2关闭0未完成
        /// </summary>
        [Column("F_ISFINISHED")]
        public int? F_IsFinished { get; set; }
        /// <summary>
        /// 任务超时流转到下一个节点时间
        /// </summary>
        [Column("F_TIMEOUTACTION")]
        public int? F_TimeoutAction { get; set; }
        /// <summary>
        /// 任务超时提醒消息时间
        /// </summary>
        [Column("F_TIMEOUTNOTICE")]
        public int? F_TimeoutNotice { get; set; }
        /// <summary>
        /// 任务超时消息提醒间隔时间
        /// </summary>
        [Column("F_TIMEOUTINTERVAL")]
        public int? F_TimeoutInterval { get; set; }
        /// <summary>
        /// 任务超时消息发送策略编码
        /// </summary>
        [Column("F_TIMEOUTSTRATEGY")]
        public string F_TimeoutStrategy { get; set; }
        /// <summary>
        /// 上一个任务节点Id
        /// </summary>
        [Column("F_PREVNODEID")]
        public string F_PrevNodeId { get; set; }
        /// <summary>
        /// 上一个节点名称
        /// </summary>
        [Column("F_PREVNODENAME")]
        public string F_PrevNodeName { get; set; }
        /// <summary>
        /// 任务创建时间
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 任务创建人员
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 任务创建人员名称
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 任务变更时间
        /// </summary>
        [Column("ModificationDate")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 任务变更人员信息
        /// </summary>
        [Column("Modification_Id")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 任务变更人员名称
        /// </summary>
        [Column("ModificationName")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 是否被催办
        /// </summary>
        [Column("F_ISURGE")]
        public int? F_IsUrge { get; set; }
        /// <summary>
        /// 加签情况下最初的审核者
        /// </summary>
        [Column("F_FIRSTUSERID")]
        public string F_FirstUserId { get; set; }
        /// <summary>
        /// 子流程进程主键
        /// </summary>
        [Column("F_CHILDPROCESSID")]
        public string F_ChildProcessId { get; set; }

        /// <summary>
        /// 任务变更人员信息
        /// </summary>
        [Column("F_AUDITORID")]
        public string F_AuditorId { get; set; }
        /// <summary>
        /// 任务变更人员名称
        /// </summary>
        [Column("F_AUDITORNAME")]
        public string F_AuditorName { get; set; }

        /// <summary>
        /// 任务变更人员信息
        /// </summary>
        [Column("F_COMPANYID")]
        public string F_CompanyId { get; set; }
        /// <summary>
        /// 任务变更人员名称
        /// </summary>
        [Column("F_DEPARTMENTID")]
        public string F_DepartmentId { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion

        #region 扩展字段

        public List<WfAuditor> auditors { get; set; }

        #endregion

    }
}

