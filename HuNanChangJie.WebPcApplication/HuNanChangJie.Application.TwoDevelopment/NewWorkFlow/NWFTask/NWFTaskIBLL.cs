﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:59
    /// 描 述：NWF_Task
    /// </summary>
    public interface NWFTaskIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<NWF_TaskEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取NWF_Task表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        NWF_TaskEntity GetNWF_TaskEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, NWF_TaskEntity entity);
        #endregion

        IEnumerable<NWF_ProcessEntity> GetActiveList(UserInfo userInfo, XqPagination paginationobj, string queryJson);
        IEnumerable<NWF_ProcessEntity> GetHasList(string userInfoUserId, XqPagination paginationobj, string queryJson);
    }
}
