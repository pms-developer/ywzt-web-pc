﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:55
    /// 描 述：NWF_SchemeAuth
    /// </summary>
    public class NWF_SchemeAuthEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 流程模板信息主键
        /// </summary>
        [Column("F_SCHEMEINFOID")]
        public string F_SchemeInfoId { get; set; }
        /// <summary>
        /// 对象名称
        /// </summary>
        [Column("F_OBJNAME")]
        public string F_ObjName { get; set; }
        /// <summary>
        /// 对应对象主键
        /// </summary>
        [Column("F_OBJID")]
        public string F_ObjId { get; set; }
        /// <summary>
        /// 对应对象类型1岗位2角色3用户4所用人可看
        /// </summary>
        [Column("F_OBJTYPE")]
        public int? F_ObjType { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

