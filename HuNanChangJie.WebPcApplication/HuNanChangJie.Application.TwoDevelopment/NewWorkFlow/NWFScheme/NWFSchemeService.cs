﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:54
    /// 描 述：NWFScheme
    /// </summary>
    public class NWFSchemeService : RepositoryFactory
    {
        private string schemeInfoFieldSql;
        private string schemeFieldSql;

        public NWFSchemeService()
        {
            schemeInfoFieldSql = @" 
                        t.F_Id,
                        t.F_Code,
                        t.F_Name,
                        t.F_Category,
                        
                        t.F_SchemeId,
                        
                        t.F_EnabledMark,
                        t.F_Description
                        ";

            schemeFieldSql = @" 
                        t.F_Id,
                        t.F_SchemeInfoId,
                        t.F_Type,
                        t.CreationName,
                        t.CreationDate,
                        t.Creation_Id
                        ";
        }


        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<NWF_SchemeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_Id,
                t.F_SchemeInfoId,
                t.F_Type,
                t.CreationDate,
                t.Creation_Id,
                t.CreationName,
                t.F_Content
                ");
                strSql.Append("  FROM NWF_Scheme t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                return this.BaseRepository().FindList<NWF_SchemeEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取NWF_Scheme表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public NWF_SchemeEntity GetNWF_SchemeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<NWF_SchemeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<NWF_SchemeEntity>(t=>t.F_Id == keyValue);
                this.BaseRepository().Delete<NWF_SchemeInfoEntity>(t => t.F_SchemeId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, NWF_SchemeEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal NWF_SchemeEntity GetWfSchemeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<NWF_SchemeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }

        }

        internal void SaveEntity(string keyValue, NWF_SchemeInfoEntity wfSchemeInfoEntity, NWF_SchemeEntity wfSchemeEntity, List<NWF_SchemeAuthEntity> wfSchemeAuthorizeList)
        {
            IRepository db = new RepositoryFactory().BaseRepository().BeginTrans();
            try
            {
                if (string.IsNullOrEmpty(keyValue))
                {
                    wfSchemeInfoEntity.Create();
                }
                else
                {
                    wfSchemeInfoEntity.Modify(keyValue);
                }
                #region  模板信息
                if (wfSchemeEntity != null)
                {
                    wfSchemeEntity.F_SchemeInfoId = wfSchemeInfoEntity.F_Id;
                    wfSchemeEntity.Create();
                    db.Insert(wfSchemeEntity);
                    wfSchemeInfoEntity.F_SchemeId = wfSchemeEntity.F_Id;
                }
                #endregion

                #region  模板基础信息
                if (!string.IsNullOrEmpty(keyValue))
                {
                    db.Update(wfSchemeInfoEntity);
                }
                else
                {
                    db.Insert(wfSchemeInfoEntity);
                }
                #endregion

                #region  流程模板权限信息
                string schemeInfoId = wfSchemeInfoEntity.F_Id;
                db.Delete<NWF_SchemeAuthEntity>(t => t.F_SchemeInfoId == schemeInfoId);
                if (wfSchemeAuthorizeList!=null)
                {
                    foreach (var wfSchemeAuthorize in wfSchemeAuthorizeList)
                    {
                        wfSchemeAuthorize.F_SchemeInfoId = schemeInfoId;
                        db.Insert(wfSchemeAuthorize);
                    }
                }
               
                #endregion

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<NWF_SchemeAuthEntity> GetWfSchemeAuthorizeList(string schemeInfoId)
        {
            try
            {
                return this.BaseRepository().FindList<NWF_SchemeAuthEntity>(t => t.F_SchemeInfoId == schemeInfoId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<NWF_SchemeInfoEntity> GetSchemeInfoPageList(XqPagination pagination, string keyword, string category)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(schemeInfoFieldSql);
                strSql.Append(",t1.F_Type,t1.CreationDate,t1.Creation_Id,t1.CreationName ");
                strSql.Append(" FROM NWF_SchemeInfo t LEFT JOIN NWF_Scheme t1 ON t.F_SchemeId = t1.F_Id WHERE 1=1  ");

                if (!string.IsNullOrEmpty(keyword))
                {
                    strSql.Append(" AND ( t.F_Name like @keyword OR t.F_Code like @keyword ) ");
                    keyword = "%" + keyword + "%";
                }
                if (!string.IsNullOrEmpty(category))
                {
                    strSql.Append(" AND t.F_Category = @category ");
                }
                return this.BaseRepository().FindList<NWF_SchemeInfoEntity>(strSql.ToString(), new { keyword = keyword, category = category }, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal void UpdateState(string schemeInfoId, int state)
        {
            try
            {
                NWF_SchemeInfoEntity entity = new NWF_SchemeInfoEntity
                {
                    F_Id = schemeInfoId,
                    F_EnabledMark = state
                };
                this.BaseRepository().Update(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal void UpdateScheme(string schemeInfoId, string schemeId)
        {
            try
            {
                NWF_SchemeEntity wfSchemeEntity = GetWfSchemeEntity(schemeId);

                NWF_SchemeInfoEntity entity = new NWF_SchemeInfoEntity
                {
                    F_Id = schemeInfoId,
                    F_SchemeId = schemeId
                };
                if (wfSchemeEntity.F_Type != 1)
                {
                    entity.F_EnabledMark = 0;
                }
                this.BaseRepository().Update(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal NWF_SchemeInfoEntity GetWfSchemeInfoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<NWF_SchemeInfoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        public NWF_SchemeInfoEntity GetWfSchemeInfoEntityByCode(string code)
        {
            try
            {
                return this.BaseRepository().FindEntity<NWF_SchemeInfoEntity>(t => t.F_Code == code);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<NWF_SchemeEntity> GetSchemePageList(XqPagination pagination, string schemeInfoId)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(schemeFieldSql);
                strSql.Append(" FROM NWF_Scheme t WHERE 1=1 ");
                strSql.Append(" AND t.F_SchemeInfoId = @schemeInfoId ");

                return this.BaseRepository().FindList<NWF_SchemeEntity>(strSql.ToString(), new { schemeInfoId = schemeInfoId }, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<NWF_SchemeInfoEntity> GetMyInfoList(UserInfo userInfo)
        {
            try
            {
                string userId = userInfo.userId;
                string postIds = userInfo.postIds;
                string roleIds = userInfo.roleIds;
                List<NWF_SchemeAuthEntity> list = (List<NWF_SchemeAuthEntity>)this.BaseRepository().FindList<NWF_SchemeAuthEntity>(t => t.F_ObjId == null
                                                                                                                                                 || userId.Contains(t.F_ObjId)
                                                                                                                                                 || postIds.Contains(t.F_ObjId)
                                                                                                                                                 || roleIds.Contains(t.F_ObjId)
                );
                string schemeinfoIds = "";
                foreach (var item in list)
                {
                    schemeinfoIds += "'" + item.F_SchemeInfoId + "',";
                }

                if (!string.IsNullOrEmpty(schemeinfoIds))
                {
                    schemeinfoIds = "(" + schemeinfoIds.Remove(schemeinfoIds.Length - 1, 1) + ")";
                }
               

                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(schemeInfoFieldSql);
                strSql.Append(" FROM NWF_SchemeInfo t WHERE 1=1 AND t.F_EnabledMark = 1 " );
                if (!string.IsNullOrEmpty(schemeinfoIds))
                {
                    strSql.Append(" t.F_ID in " + schemeinfoIds);
                }

                return this.BaseRepository().FindList<NWF_SchemeInfoEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
