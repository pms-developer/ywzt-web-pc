﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:54
    /// 描 述：NWFScheme
    /// </summary>
    public interface NWFSchemeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<NWF_SchemeEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取NWF_Scheme表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        NWF_SchemeEntity GetNWF_SchemeEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, NWF_SchemeEntity entity);
        void SaveEntity(string keyValue, NWF_SchemeInfoEntity schemeInfoEntity, NWF_SchemeEntity schemeEntity, List<NWF_SchemeAuthEntity> wfSchemeAuthorizeList);
        IEnumerable<NWF_SchemeInfoEntity> GetSchemeInfoPageList(XqPagination paginationobj, string keyword, string category);
        NWF_SchemeInfoEntity GetWfSchemeInfoEntityByCode(string schemeCode);
        IEnumerable<NWF_SchemeAuthEntity> GetWfSchemeAuthorizeList(string code);
        #endregion

        IEnumerable<NWF_SchemeEntity> GetSchemePageList(XqPagination paginationobj, string schemeInfoId);
        void UpdateScheme(string schemeInfoId, string schemeId);
        void UpdateState(string keyValue, int state);
        IEnumerable<NWF_SchemeInfoEntity>   GetMyInfoList(UserInfo userInfo);
    }
}
