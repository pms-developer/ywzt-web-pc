﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 15:54
    /// 描 述：NWFScheme
    /// </summary>
    public class NWFSchemeBLL : NWFSchemeIBLL
    {
        private NWFSchemeService wfSchemeService = new NWFSchemeService();
        private ICache cache = CacheFactory.CaChe();
        private string cacheKey = "hncjpms_nwfscheme_";// +模板主键

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<NWF_SchemeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return wfSchemeService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取NWF_Scheme表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public NWF_SchemeEntity GetNWF_SchemeEntity(string keyValue)
        {
            try
            {
                return wfSchemeService.GetNWF_SchemeEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                wfSchemeService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, NWF_SchemeEntity entity)
        {
            try
            {
                wfSchemeService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="schemeInfoEntity"></param>
        /// <param name="schemeEntity"></param>
        /// <param name="wfSchemeAuthorizeList"></param>
        public void SaveEntity(string keyValue, NWF_SchemeInfoEntity schemeInfoEntity, NWF_SchemeEntity schemeEntity,
            List<NWF_SchemeAuthEntity> wfSchemeAuthorizeList)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    NWF_SchemeEntity wfSchemeEntityOld = GetWfSchemeEntity(schemeInfoEntity.F_SchemeId);
                    if (wfSchemeEntityOld.F_Content == schemeEntity.F_Content && wfSchemeEntityOld.F_Type == schemeEntity.F_Type)
                    {
                        schemeEntity = null;
                    }
                    cache.Remove(cacheKey + schemeInfoEntity.F_Code, CacheId.workflow);
                    cache.Remove(cacheKey + schemeInfoEntity.F_SchemeId, CacheId.workflow);
                }
                wfSchemeService.SaveEntity(keyValue, schemeInfoEntity, schemeEntity, wfSchemeAuthorizeList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        private NWF_SchemeEntity GetWfSchemeEntity(string keyValue)
        {
            try
            {
                NWF_SchemeEntity wfSchemeEntity = cache.Read<NWF_SchemeEntity>(cacheKey + keyValue, CacheId.workflow);
                if (wfSchemeEntity == null)
                {
                    wfSchemeEntity = wfSchemeService.GetWfSchemeEntity(keyValue);
                    cache.Write<NWF_SchemeEntity>(cacheKey + keyValue, wfSchemeEntity, CacheId.workflow);
                }

                return wfSchemeEntity;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<NWF_SchemeInfoEntity> GetSchemeInfoPageList(XqPagination pagination, string keyword, string category)
        {
            try
            {
                return wfSchemeService.GetSchemeInfoPageList(pagination, keyword, category);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public NWF_SchemeInfoEntity GetWfSchemeInfoEntityByCode(string schemeCode)
        {
            try
            {
                NWF_SchemeInfoEntity wfSchemeInfoEntity = cache.Read<NWF_SchemeInfoEntity>(cacheKey + schemeCode, CacheId.workflow);
                if (wfSchemeInfoEntity == null)
                {
                    wfSchemeInfoEntity = wfSchemeService.GetWfSchemeInfoEntityByCode(schemeCode);
                    cache.Write<NWF_SchemeInfoEntity>(cacheKey + schemeCode, wfSchemeInfoEntity, CacheId.workflow);
                }
                return wfSchemeInfoEntity;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<NWF_SchemeAuthEntity> GetWfSchemeAuthorizeList(string schemeInfoId)
        {
            try
            {
                return wfSchemeService.GetWfSchemeAuthorizeList(schemeInfoId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<NWF_SchemeEntity> GetSchemePageList(XqPagination paginationobj, string schemeInfoId)
        {
            try
            {
                return wfSchemeService.GetSchemePageList(paginationobj, schemeInfoId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UpdateScheme(string schemeInfoId, string schemeId)
        {
            try
            {
                NWF_SchemeInfoEntity wfSchemeInfoEntity = wfSchemeService.GetWfSchemeInfoEntity(schemeInfoId);
                cache.Remove(cacheKey + wfSchemeInfoEntity.F_Code, CacheId.workflow);
                cache.Remove(cacheKey + wfSchemeInfoEntity.F_SchemeId, CacheId.workflow);
                wfSchemeService.UpdateScheme(schemeInfoId, schemeId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UpdateState(string keyValue, int state)
        {
            try
            {
                wfSchemeService.UpdateState(keyValue, state);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<NWF_SchemeInfoEntity> GetMyInfoList(UserInfo userInfo)
        {
            try
            {
                return wfSchemeService.GetMyInfoList(userInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
