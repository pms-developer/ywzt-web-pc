﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 14:41
    /// 描 述：流程会签统计
    /// </summary>
    public class NWF_ConfluenceEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 流程进程主键
        /// </summary>
        [Column("F_PROCESSID")]
        public string F_ProcessId { get; set; }
        /// <summary>
        /// 会签节点ID
        /// </summary>
        [Column("F_NODEID")]
        public string F_NodeId { get; set; }
        /// <summary>
        /// 上一节点ID 
        /// </summary>
        [Column("F_FORMNODEID")]
        public string F_FormNodeId { get; set; }
        /// <summary>
        /// 状态1同意0不同意
        /// </summary>
        [Column("F_STATE")]
        public int? F_State { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

