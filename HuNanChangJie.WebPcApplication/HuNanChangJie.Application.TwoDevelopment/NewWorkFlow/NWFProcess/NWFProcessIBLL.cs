﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 14:46
    /// 描 述：222
    /// </summary>
    public interface NWFProcessIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<NWF_ProcessEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取NWF_Process表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        NWF_ProcessEntity GetNWF_ProcessEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, NWF_ProcessEntity entity);
        #endregion

        IEnumerable<NWF_ProcessEntity> GetMyPageList(string userInfoUserId, XqPagination paginationobj, string queryJson);
       
    }
}
