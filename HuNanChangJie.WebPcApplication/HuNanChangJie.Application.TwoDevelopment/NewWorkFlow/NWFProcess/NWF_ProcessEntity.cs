﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 14:46
    /// 描 述：222
    /// </summary>
    public class NWF_ProcessEntity
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 流程模板主键
        /// </summary>
        [Column("F_SCHEMEID")]
        public string F_SchemeId { get; set; }
        /// <summary>
        /// 流程模板编码
        /// </summary>
        [Column("F_SCHEMECODE")]
        public string F_SchemeCode { get; set; }
        /// <summary>
        /// 流程模板名称
        /// </summary>
        [Column("F_SCHEMENAME")]
        public string F_SchemeName { get; set; }
        /// <summary>
        /// 流程进程自定义标题
        /// </summary>
        [Column("F_TITLE")]
        public string F_Title { get; set; }
        /// <summary>
        /// 流程进程等级1普通 2重要 3紧急
        /// </summary>
        [Column("F_LEVEL")]
        public int? F_Level { get; set; }
        /// <summary>
        /// 流程进程有效标志 1正常2草稿3作废
        /// </summary>
        [Column("F_ENABLEDMARK")]
        public int? F_EnabledMark { get; set; }
        /// <summary>
        /// 是否重新发起1是0不是
        /// </summary>
        [Column("F_ISAGAIN")]
        public int? F_IsAgain { get; set; }
        /// <summary>
        /// 流程进程是否结束1是0不是
        /// </summary>
        [Column("F_ISFINISHED")]
        public int? F_IsFinished { get; set; }
        /// <summary>
        /// 是否是子流程进程1是0不是
        /// </summary>
        [Column("F_ISCHILD")]
        public int? F_IsChild { get; set; }
        /// <summary>
        /// 子流程执行方式 0同步1异步
        /// </summary>
        [Column("F_ISASYN")]
        public int? F_IsAsyn { get; set; }
        /// <summary>
        /// 父流程的发起子流程的节点Id
        /// </summary>
        [Column("F_PARENTNODEID")]
        public string F_ParentNodeId { get; set; }
        /// <summary>
        /// 流程进程父进程任务主键
        /// </summary>
        [Column("F_PARENTTASKID")]
        public string F_ParentTaskId { get; set; }
        /// <summary>
        /// 流程进程父进程主键
        /// </summary>
        [Column("F_PARENTPROCESSID")]
        public string F_ParentProcessId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建人主键
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 1表示开始处理过了 0 还没人处理过
        /// </summary>
        [Column("F_ISSTART")]
        public int? F_IsStart { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion



        #region  扩展字段
        /// <summary>
        /// 任务名称
        /// </summary>
        [NotMapped]
        public string F_TaskName { get; set; }
        /// <summary>
        /// 任务主键
        /// </summary>
        [NotMapped]
        public string F_TaskId { get; set; }
        /// <summary>
        /// 任务类型
        /// </summary>
        [NotMapped]
        public int? F_TaskType { get; set; }
        #endregion
    }
}

