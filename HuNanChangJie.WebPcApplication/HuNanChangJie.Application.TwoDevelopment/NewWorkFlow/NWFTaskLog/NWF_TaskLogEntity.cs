﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 16:03
    /// 描 述：_NWF_TaskLog
    /// </summary>
    public class NWF_TaskLogEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 流程进程主键
        /// </summary>
        [Column("F_PROCESSID")]
        public string F_ProcessId { get; set; }
        /// <summary>
        /// 流程任务主键
        /// </summary>
        [Column("F_TASKID")]
        public string F_TaskId { get; set; }
        /// <summary>
        /// 操作码agree 同意 disagree 不同意 lrtimeout 超时
        /// </summary>
        [Column("F_OPERATIONCODE")]
        public string F_OperationCode { get; set; }
        /// <summary>
        /// F_OperationName
        /// </summary>
        [Column("F_OPERATIONNAME")]
        public string F_OperationName { get; set; }
        /// <summary>
        /// 流程节点ID
        /// </summary>
        [Column("F_NODEID")]
        public string F_NodeId { get; set; }
        /// <summary>
        /// 流程节点名称
        /// </summary>
        [Column("F_NODENAME")]
        public string F_NodeName { get; set; }
        /// <summary>
        /// 流程任务类型 0创建1审批2传阅3加签审核4子流程5重新创建
        /// </summary>
        [Column("F_TASKTYPE")]
        public int? F_TaskType { get; set; }
        /// <summary>
        /// 上一流程节点ID
        /// </summary>
        [Column("F_PREVNODEID")]
        public string F_PrevNodeId { get; set; }
        /// <summary>
        /// 上一流程节点名称
        /// </summary>
        [Column("F_PREVNODENAME")]
        public string F_PrevNodeName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建人主键
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建人员名称
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 任务人主键
        /// </summary>
        [Column("F_TASKUSERID")]
        public string F_TaskUserId { get; set; }
        /// <summary>
        /// F_TaskUserName
        /// </summary>
        [Column("F_TASKUSERNAME")]
        public string F_TaskUserName { get; set; }
        /// <summary>
        /// 备注信息
        /// </summary>
        [Column("F_DES")]
        public string F_Des { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

