﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 16:05
    /// 描 述：NWFTaskRelation
    /// </summary>
    public class NWF_TaskRelationEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 任务主键
        /// </summary>
        [Column("F_TASKID")]
        public string F_TaskId { get; set; }
        /// <summary>
        /// 任务执行人员主键
        /// </summary>
        [Column("F_USERID")]
        public string F_UserId { get; set; }
        /// <summary>
        /// 标记0需要处理1暂时不需要处理
        /// </summary>
        [Column("F_MARK")]
        public int? F_Mark { get; set; }
        /// <summary>
        /// 处理结果1同意2不同意
        /// </summary>
        [Column("F_RESULT")]
        public int? F_Result { get; set; }
        /// <summary>
        /// F_Sort
        /// </summary>
        [Column("F_SORT")]
        public int? F_Sort { get; set; }
        /// <summary>
        /// F_Time
        /// </summary>
        [Column("F_TIME")]
        public DateTime? F_Time { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

