﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 14:43
    /// 描 述：111
    /// </summary>
    public class NWF_DelegateRuleEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 被委托人Id
        /// </summary>
        [Column("F_TOUSERID")]
        public string F_ToUserId { get; set; }
        /// <summary>
        /// 被委托人名称
        /// </summary>
        [Column("F_TOUSERNAME")]
        public string F_ToUserName { get; set; }
        /// <summary>
        /// 委托开始时间
        /// </summary>
        [Column("F_BEGINDATE")]
        public DateTime? F_BeginDate { get; set; }
        /// <summary>
        /// 委托结束时间
        /// </summary>
        [Column("F_ENDDATE")]
        public DateTime? F_EndDate { get; set; }
        /// <summary>
        /// 委托人Id
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 委托人名称
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 有效标志1有效 0 无效
        /// </summary>
        [Column("F_ENABLEDMARK")]
        public int? F_EnabledMark { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

