﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.NewWorkFlow
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-03-16 16:04
    /// 描 述：NWFTaskMsg
    /// </summary>
    public class NWF_TaskMsgEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 流程进程主键
        /// </summary>
        [Column("F_PROCESSID")]
        public string F_ProcessId { get; set; }
        /// <summary>
        /// 流程任务主键
        /// </summary>
        [Column("F_TASKID")]
        public string F_TaskId { get; set; }
        /// <summary>
        /// 任务发送人主键
        /// </summary>
        [Column("F_FROMUSERID")]
        public string F_FromUserId { get; set; }
        /// <summary>
        /// 任务发送人账号
        /// </summary>
        [Column("F_FROMUSERACCOUNT")]
        public string F_FromUserAccount { get; set; }
        /// <summary>
        /// 任务发送人名称
        /// </summary>
        [Column("F_FROMUSERNAME")]
        public string F_FromUserName { get; set; }
        /// <summary>
        /// 任务接收人主键
        /// </summary>
        [Column("F_TOUSERID")]
        public string F_ToUserId { get; set; }
        /// <summary>
        /// 任务接收人账号
        /// </summary>
        [Column("F_TOACCOUNT")]
        public string F_ToAccount { get; set; }
        /// <summary>
        /// 任务接收人名称
        /// </summary>
        [Column("F_TONAME")]
        public string F_ToName { get; set; }
        /// <summary>
        /// 任务标题
        /// </summary>
        [Column("F_TITLE")]
        public string F_Title { get; set; }
        /// <summary>
        /// 任务内容
        /// </summary>
        [Column("F_CONTENT")]
        public string F_Content { get; set; }
        /// <summary>
        /// 任务创建时间
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 是否结束1结束0未结束
        /// </summary>
        [Column("F_ISFINISHED")]
        public int? F_IsFinished { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

