﻿using Dapper;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.Formula
{
    public class Formula: RepositoryFactory
    {
        private ProjectFundPartitionBLL fundBll = new ProjectFundPartitionBLL();
        private BaseFormulaSettingsBLL formulaBll = new BaseFormulaSettingsBLL();

        /// <summary>
        /// 测试公式的取值
        /// </summary>
        /// <param name="projectid">项目ID</param>
        /// <returns>解析后的公式及结果信息</returns>
        public List<FormulaRuntimeInfo> TestFormula(string projectid,string currentId="", AuditOperation type = AuditOperation.Audit)
        {
            var mainInfo = fundBll.GetFormInfo(projectid);
            if (mainInfo == null) return null;
            return AnalysisFormulas(projectid, mainInfo,currentId, type);
        }

        /// <summary>
        /// 解析公式
        /// </summary>
        /// <param name="projectid"></param>
        /// <param name="mainInfo"></param>
        /// <returns></returns>
        private List<FormulaRuntimeInfo> AnalysisFormulas(string projectid, Project_FundPartitionEntity mainInfo,string currentId, AuditOperation type)
        {
            var runtimes = new List<FormulaRuntimeInfo>();
            GetPrimitiveFormulas(projectid, mainInfo, runtimes,currentId, type);
            AnalysisThis(runtimes);
            AnalysisMax(runtimes);
         
            Run(runtimes);
            return runtimes;
        }

        /// <summary>
        /// 解析this关键字
        /// </summary>
        /// <param name="runtimes"></param>
        private void AnalysisThis(List<FormulaRuntimeInfo> runtimes)
        {
            foreach (var info in runtimes)
            {
                var item = info.Analysis;
                var search = "$this.";
                if (item.Contains(search))
                {
                    var tempStr = item;
                    var index = 0;
                    while ((index = tempStr.IndexOf(search)) >= 0)
                    {
                        var str = tempStr.Substring(index, tempStr.Length - index);// 
                        if (str[0] == '$')
                            str = str.Substring(1, str.Length-1);
                        var endIndex = str.IndexOf('$');
                        var maxStr = tempStr.Substring(index, endIndex + 2);
                        var subjectName = maxStr.Replace(search, "").Replace("$", "");
                        var model = runtimes.FirstOrDefault(i => i.SubjectName == subjectName);
                        if (model == null)
                        {
                            tempStr = tempStr.Replace(maxStr, "0");
                        }
                        else
                        {
                            tempStr = tempStr.Replace(maxStr, $"({model.Analysis})");
                        }
                        info.Analysis = tempStr;
                    }
                }
            }
        }

        /// <summary>
        /// 获取审核控制点类型
        /// </summary>
        /// <param name="projectid">项目ID</param>
        /// <returns></returns>
        public AuditControlPoint GetControlPoint(string projectid)
        {
            var mainInfo = fundBll.GetFormInfo(projectid);
            if (mainInfo == null) return AuditControlPoint.Unknown;
            var list = fundBll.GetProject_FundPartitionDetailsList(mainInfo.ID);
            var info = list.FirstOrDefault();
            if (info == null) return AuditControlPoint.Unknown;
            if (string.IsNullOrEmpty(info.ControlPoint))
            {
                return AuditControlPoint.Invoice;
            }
            return (AuditControlPoint)Enum.Parse(typeof(AuditControlPoint), info.ControlPoint);
        }

        /// <summary>
        /// 解析公式
        /// </summary>
        /// <param name="projectid">项目ID</param>
        /// <returns>解析后的公式及结果信息</returns>
        public List<FormulaRuntimeInfo> Analysis(string projectid,string currentId, AuditOperation type= AuditOperation.Audit)
        {
            var mainInfo = fundBll.GetFormInfo(projectid);
            if (mainInfo == null) return null;
            if (mainInfo.AuditStatus != "2") return null;
            return AnalysisFormulas(projectid, mainInfo, currentId, type);
        }

        private void Run(List<FormulaRuntimeInfo> runtimes)
        {
            RPN rpn = new RPN();
            foreach (var item in runtimes)
            {
                if(rpn.Parse(item.Runnable))
                {
                    item.Result=rpn.Evaluate().ToDecimal();
                }
            }
        }

        /// <summary>
        /// 解析最大值
        /// </summary>
        /// <param name="formulas"></param>
        /// <param name="analysisList"></param>
        private static void AnalysisMax(List<FormulaRuntimeInfo> runtimes)
        {
            foreach (var info in runtimes)
            {
                var item = info.Analysis;
                var search = "max(";
                if (item.Contains(search))
                {
                    var tempStr = item;
                    var index = 0;
                    while ((index = tempStr.IndexOf(search)) >= 0)
                    {
                        var str = tempStr.Substring(index, tempStr.Length - index);//把“max(”前面的字符串去掉
                        var maxEndIndex = str.IndexOf(')');
                        var maxStr = tempStr.Substring(index, maxEndIndex + 1);
                        var value = maxStr.Replace(search, "").Replace(")", "");
                        var values = value.Split(',');
                        decimal max = 0;
                        for (var i = 0; i < values.Length; i++)
                        {
                            for (var j = i + 1; j < values.Length; j++)
                            {
                                var v1 = values[i].ToDecimal();
                                var v2 = values[j].ToDecimal();
                                max = v1 > v2 ? v1 : v2;
                            }
                        }
                        tempStr = tempStr.Replace(maxStr, max.ToString());
                        info.Runnable = tempStr;
                    }
                }
                else
                {
                    info.Runnable=item;
                }
            }

        }

        /// <summary>
        /// 获取原始公式
        /// </summary>
        /// <param name="projectid"></param>
        /// <param name="mainInfo"></param>
        /// <returns></returns>
        private void GetPrimitiveFormulas(string projectid, Project_FundPartitionEntity mainInfo, List<FormulaRuntimeInfo> runtimes,string currentId, AuditOperation type)
        {
            var details = fundBll.GetProject_FundPartitionDetailsList(mainInfo.ID);
            foreach (var item in details)
            {
                var runtime = new FormulaRuntimeInfo();
                runtime.ID = item.ID;
                
                var settingsInfo = item.FormulaSettings.ToObject<FormulaSettings>();
                foreach (var info in settingsInfo.infoList)
                {
                    var formulaInfo = formulaBll.GetBase_FormulaSettingsEntity(info.id);
                    var sql = formulaInfo.Sql.ToLower();
                    if (type == AuditOperation.UnAudit)
                    {
                        sql = sql.Replace("or id=@currentid", "and id<>@currentid");
                        sql = sql.Replace("or a.financeprepaytaxid=@financeprepaytaxid", "and a.financeprepaytaxid<>@financeprepaytaxid");
                    }
                    var dp = new DynamicParameters(new { });
                    if (sql.Contains("@projectid"))
                    {
                        dp.Add("projectid", projectid, DbType.String);
                    }
                    //if (!string.IsNullOrWhiteSpace(currentId))
                    //{
                        if (sql.Contains("@currentid"))
                        {
                            dp.Add("currentid", currentId, DbType.String);
                        }
                        if (sql.Contains("@financeprepaytaxid"))
                        {
                            dp.Add("financeprepaytaxid", currentId, DbType.String);
                        }
                    //}
                    var result = BaseRepository().ExecuteScalar(sql, dp).ToDecimal();
                    settingsInfo.value = settingsInfo.value.Replace(info.id, result.ToString());
                }
                runtime.Analysis = settingsInfo.value;
                runtime.ProjectSubjectId = item.ProjectSubjectId;
                runtime.SubjectName = item.SubjectName;
                runtimes.Add(runtime);
            }
        }
    }
}
