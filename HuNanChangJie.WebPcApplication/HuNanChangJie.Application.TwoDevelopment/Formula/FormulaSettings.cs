﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.Formula
{
    public class FormulaSettings
    {
        public List<InfoList> infoList { get; set; }

        public string value { get; set; }
    }

    public class InfoList {

        public string id { get; set; }

        public string name { get; set; }


    }

    public class FormulaRuntimeInfo
    {
        /// <summary>
        /// 公式信息ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 项目科目ID
        /// </summary>
        public string ProjectSubjectId { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }

        /// <summary>
        /// 解析后公式
        /// </summary>
        public string Analysis { get; set; }

        /// <summary>
        /// 可运行公式
        /// </summary>
        public string Runnable { get; set; }

        /// <summary>
        /// 运行结果
        /// </summary>
        public decimal Result { get; set; }
    }

    public class BudgetChangeInfo
    {
        /// <summary>
        /// 项目成本科目ID
        /// </summary>
        public string ProjectSubjectId { get; set; }

        /// <summary>
        /// 反审核之前的金额
        /// </summary>
        public decimal Befor { get; set; }

        /// <summary>
        /// 反审核之后的金额
        /// </summary>
        public decimal After { get; set; }

        /// <summary>
        /// 发生额
        /// </summary>
        public decimal Amount => After-Befor;

        /// <summary>
        /// 公式计算的结果
        /// </summary>
        public decimal Result { get; set; }
    }
}
