﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.Desktop
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-09-25 11:32
    /// 描 述：图标配置
    /// </summary>
    public class DTChartEntity 
    {
        #region  实体成员
        /// <summary>
        /// F_Id
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// F_Name
        /// </summary>
        [Column("F_NAME")]
        public string F_Name { get; set; }
        /// <summary>
        /// F_Sql
        /// </summary>
        [Column("F_SQL")]
        public string F_Sql { get; set; }
        /// <summary>
        /// F_Sort
        /// </summary>
        [Column("F_SORT")]
        public int? F_Sort { get; set; }
        /// <summary>
        /// Creation_Id
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// CreationName
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// CreationDate
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// F_Description
        /// </summary>
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        /// <summary>
        /// F_Proportion1
        /// </summary>
        [Column("F_PROPORTION1")]
        public int? F_Proportion1 { get; set; }
        /// <summary>
        /// F_Proportion2
        /// </summary>
        [Column("F_PROPORTION2")]
        public int? F_Proportion2 { get; set; }
        /// <summary>
        /// F_Proportion3
        /// </summary>
        [Column("F_PROPORTION3")]
        public int? F_Proportion3 { get; set; }
        /// <summary>
        /// F_Proportion4
        /// </summary>
        [Column("F_PROPORTION4")]
        public int? F_Proportion4 { get; set; }
        /// <summary>
        /// F_DataSourceId
        /// </summary>
        [Column("F_DATASOURCEID")]
        public string F_DataSourceId { get; set; }
        /// <summary>
        /// F_Icon
        /// </summary>
        [Column("F_ICON")]
        public string F_Icon { get; set; }
        /// <summary>
        /// F_Type
        /// </summary>
        [Column("F_TYPE")]
        public int? F_Type { get; set; }

        [Column("F_ParentId")]
        public string F_ParentId { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

