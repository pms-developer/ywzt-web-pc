﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Application.Organization;
using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;

namespace HuNanChangJie.Application.TwoDevelopment.Desktop
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2018-09-25 11:32
    /// 描 述：图标配置
    /// </summary>
    public class DTChartBLL : DTChartIBLL
    {
        private DTChartService dTChartService = new DTChartService();
        private DatabaseLinkIBLL databaseLinkIbll = new DatabaseLinkBLL();
        private DataSourceIBLL dataSourceIbll = new DataSourceBLL();
        #region  缓存定义
        private ICache cache = CacheFactory.CaChe();
        private string cacheKey = "hncjpms_dtcharts";
        #endregion
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<DTChartEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return dTChartService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取LR_DT_Chart表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public DTChartEntity GetLR_DT_ChartEntity(string keyValue)
        {
            try
            {
                return dTChartService.GetLR_DT_ChartEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                dTChartService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, DTChartEntity entity)
        {
            try
            {
                dTChartService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<NameValueCollection> GetSqlData(string id)
        {
            try
            {
              return   dTChartService.GetSqlData(id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<TreeModel> GetTree(string parentId)
        {
            try
            {
                List<DTChartEntity> list = GetList();
                List<TreeModel> treeList = new List<TreeModel>();
                foreach (var item in list)
                {
                    TreeModel node = new TreeModel
                    {
                        id = item.F_Id,
                        text = item.F_Name,
                        value = item.F_Id,
                        showcheck = false,
                        checkstate = 0,
                        isexpand = true,
                        parentId = item.F_ParentId
                    };
                    treeList.Add(node);
                }
                return treeList.ToTree(parentId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取公司列表数据
        /// </summary>
        /// <returns></returns>
        public List<DTChartEntity> GetList()
        {
            try
            {
                List<DTChartEntity> list = cache.Read<List<DTChartEntity>>(cacheKey, CacheId.company);
                if (list == null || list.Count==0)
                {
                    list = (List<DTChartEntity>)dTChartService.GetList();
                    cache.Write<List<DTChartEntity>>(cacheKey, list, CacheId.company);
                }
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
