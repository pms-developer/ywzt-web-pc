﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.Desktop
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-06 15:24
    /// 描 述：WorkflowTest
    /// </summary>
    public class WorkflowTestService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<WorkflowTestMainEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.UserCode,
                t.UserName,
                t.Address,
                t.Phone,
                t.Remark,
                t.AuditStatus
                ");
                strSql.Append("  FROM WorkflowTestMain t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["UserCode"].IsEmpty())
                {
                    dp.Add("UserCode", "%" + queryParam["UserCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.UserCode Like @UserCode ");
                }
                if (!queryParam["UserName"].IsEmpty())
                {
                    dp.Add("UserName", "%" + queryParam["UserName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.UserName Like @UserName ");
                }
                if (!queryParam["Address"].IsEmpty())
                {
                    dp.Add("Address", "%" + queryParam["Address"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Address Like @Address ");
                }
                if (!queryParam["Phone"].IsEmpty())
                {
                    dp.Add("Phone", "%" + queryParam["Phone"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Phone Like @Phone ");
                }
                var list=this.BaseRepository().FindList<WorkflowTestMainEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取WorkflowTestSub表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<WorkflowTestSubEntity> GetWorkflowTestSubList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<WorkflowTestSubEntity>(t=>t.MainID == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取WorkflowTestMain表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public WorkflowTestMainEntity GetWorkflowTestMainEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<WorkflowTestMainEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取WorkflowTestSub表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public WorkflowTestSubEntity GetWorkflowTestSubEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<WorkflowTestSubEntity>(t=>t.MainID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var workflowTestMainEntity = GetWorkflowTestMainEntity(keyValue); 
                db.Delete<WorkflowTestMainEntity>(t=>t.ID == keyValue);
                db.Delete<WorkflowTestSubEntity>(t=>t.MainID == workflowTestMainEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, WorkflowTestMainEntity entity,List<WorkflowTestSubEntity> workflowTestSubList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var workflowTestMainEntityTmp = GetWorkflowTestMainEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var WorkflowTestSubUpdateList= workflowTestSubList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in WorkflowTestSubUpdateList)
                    {
                        db.Update(item);
                    }
                    var WorkflowTestSubInserList= workflowTestSubList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in WorkflowTestSubInserList)
                    {
                        item.Create(item.ID);
                        item.MainID = workflowTestMainEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (WorkflowTestSubEntity item in workflowTestSubList)
                    {
                        item.MainID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
