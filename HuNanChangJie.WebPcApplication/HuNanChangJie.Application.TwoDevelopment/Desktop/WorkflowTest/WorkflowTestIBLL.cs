﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.Desktop
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-12-06 15:24
    /// 描 述：WorkflowTest
    /// </summary>
    public interface WorkflowTestIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<WorkflowTestMainEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取WorkflowTestSub表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<WorkflowTestSubEntity> GetWorkflowTestSubList(string keyValue);
        /// <summary>
        /// 获取WorkflowTestMain表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        WorkflowTestMainEntity GetWorkflowTestMainEntity(string keyValue);
        /// <summary>
        /// 获取WorkflowTestSub表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        WorkflowTestSubEntity GetWorkflowTestSubEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, WorkflowTestMainEntity entity,List<WorkflowTestSubEntity> workflowTestSubList,string deleteList,string type);
        #endregion

    }
}
