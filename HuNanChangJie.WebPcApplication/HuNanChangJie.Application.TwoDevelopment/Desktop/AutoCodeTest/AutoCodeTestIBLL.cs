﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.Desktop
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-11-19 17:27
    /// 描 述：AutoCodeTest
    /// </summary>
    public interface AutoCodeTestIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MainAEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MainGridSub表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<MainGridSubEntity> GetMainGridSubList(string keyValue);
        /// <summary>
        /// 获取MainA表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MainAEntity GetMainAEntity(string keyValue);
        /// <summary>
        /// 获取MainGridSub表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MainGridSubEntity GetMainGridSubEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MainAEntity entity,List<MainGridSubEntity> mainGridSubList,string deleteList,string type);
        #endregion

    }
}
