﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.Desktop
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-06-28 16:40
    /// 描 述：报表测试
    /// </summary>
    public class 测试报表Service : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取报表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public DataTable GetList(string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT  ");
                strSql.Append(@" t.f_userid,  t.f_encode,  t.f_account,  t.f_password,  t.f_secretkey,  t.f_realname,  t.f_nickname,  t.f_headicon,  t.f_quickquery,  t.f_simplespelling,  t.f_gender,  t.f_birthday,  t.f_mobile,  t.f_telephone,  t.f_email,  t.f_oicq,  t.f_wechat,  t.f_msn,  t.f_companyid,  t.f_departmentid,  t.f_securitylevel,  t.f_openid,  t.f_question,  t.f_answerquestion,  t.f_checkonline,  t.f_allowstarttime,  t.f_allowendtime,  t.f_lockstartdate,  t.f_lockenddate,  t.f_sortcode,  t.f_deletemark,  t.f_enabledmark,  t.f_description,  t.CreationDate,  t.Creation_Id,  t.CreationName,  t.ModificationDate,  t.Modification_Id,  t.ModificationName ");
                strSql.Append(@"  FROM (select * from Base_User)t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["f_encode"].IsEmpty())
                {
                    dp.Add("f_encode", "%" + queryParam["f_encode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.f_encode Like @f_encode ");
                }
                if (!queryParam["f_account"].IsEmpty())
                {
                    dp.Add("f_account", "%" + queryParam["f_account"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.f_account Like @f_account ");
                }
                if (!queryParam["f_realname"].IsEmpty())
                {
                    dp.Add("f_realname", "%" + queryParam["f_realname"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.f_realname Like @f_realname ");
                }
                return this.BaseRepository().FindTable(strSql.ToString(),dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

    }
}
        #endregion 

