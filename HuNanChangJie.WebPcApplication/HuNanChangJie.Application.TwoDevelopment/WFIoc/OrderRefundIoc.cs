﻿
using HuNanChangJie.Application.TwoDevelopment.PaySystem;
using HuNanChangJie.Application.WeChat;
using HuNanChangJie.Application.WorkFlow;
using HuNanChangJie.Util;
using System;
using HuNanChangJie.Util.Model;

namespace HuNanChangJie.Application.TwoDevelopment.WFIoc
{
    class OrderRefundIoc : INodeMethod
    {
        PayRefundReturnIBLL payRefundReturnIbll = new PayRefundReturnBLL();
        PayOrderIBLL payOrderIbll = new PayOrderBLL();
        private PayCILinkOrderNotityIBLL payCiLinkOrderNotityIbll = new PayCILinkOrderNotityBLL();

        /// <summary>
        /// 通过的方法
        /// </summary>
        /// <param name="processId"></param>
        public void Sucess(string processId)
        {

            var refundEntity = payRefundReturnIbll.GettPay_Refund_ReturnEntity(processId);
            var dbOrderInfo = payOrderIbll.GettPay_OrderEntityByOrderSN(refundEntity.F_OrderSN);
            //退款流程
          
            //真正的退款
            if (refundEntity.F_RefundAmount!=null)
            {
                var refamount = Math.Round((double)refundEntity.F_RefundAmount * 100);
                string token = HuNanChangJie.Util.DESEncrypt.SHA256Encrypt(refundEntity.F_OrderSN + (int)refamount + "Changjie123456");
                //加签后提交请求
                var reqData = new
                {
                    OrderNumber = refundEntity.F_OrderSN,
                    RefundAmount = refamount,
                    myToken = token,
                    PayType = refundEntity.F_PayingThird
                };
                //向支付构件系统 申请退款操作
                var facdeResult = CommonRequestApi<FacadeCardInfoLinkEntity>.CommonRequest(reqData.ToJson(), Config.GetValue("xunlianRefundURL"));
                UserInfo userinfo = LoginUserInfo.Get();
                //保存通知
                payCiLinkOrderNotityIbll.SaveEntity(userinfo, "", new Pay_CILink_OrderNotityEntity
                {
                    F_Attach = facdeResult.attach,
                    F_BackUrl = facdeResult.backUrl,
                    F_BankType = facdeResult.bankType,
                    F_Busicd = facdeResult.busicd,
                    F_ChannelOrderNum = facdeResult.channelOrderNum,
                    F_Charset = facdeResult.charset,
                    F_Chcd = facdeResult.chcd,
                    F_ChcdDiscount = facdeResult.chcdDiscount,
                    F_ConsumerAccount = facdeResult.consumerAccount,
                    F_ConsumerId = facdeResult.consumerId,
                    CreationDate = DateTime.Now,
                    F_Currency = facdeResult.currency,
                    F_ErrorDetail = facdeResult.errorDetail,
                    F_FrontUrl = facdeResult.frontUrl,
                    F_GoodsList = facdeResult.goodsList,
                    F_Inscd = facdeResult.inscd,
                    F_MerDiscount = facdeResult.merDiscount,
                    F_Mchntid = facdeResult.mchntid,
                    F_OldOrderNum = facdeResult.origOrderNum,
                    F_ONID = Guid.NewGuid().ToString(),
                    F_Operatorid = facdeResult.operatorid,
                    F_OrderNum = facdeResult.orderNum,
                    F_OrigOrderNum = facdeResult.origOrderNum,
                    F_OutOrderNum = facdeResult.outOrderNum,
                    F_PayTime = facdeResult.payTime,
                    F_Paylimit = facdeResult.paylimit,
                    F_PromotionList = facdeResult.promotionList,
                    F_QRcode = facdeResult.qrcode,
                    F_RealName = dbOrderInfo.F_BuyerName,
                    F_Respcd = facdeResult.respcd,
                    F_ScanCodeId = facdeResult.scanCodeId,
                    F_Sign = facdeResult.sign,
                    F_SignType = facdeResult.signType,
                    F_State = facdeResult.state,
                    F_Storeid = facdeResult.storeid,
                    F_Subject = facdeResult.subject,
                    F_Terminalid = facdeResult.terminalid,
                    F_TimeExpire = facdeResult.timeExpire,
                    F_TimeStart = facdeResult.timeStart,
                    F_tn = facdeResult.tn,
                    F_TransTime = facdeResult.transTime,
                    F_Txamt = facdeResult.txamt,
                    F_Txndir = facdeResult.txndir,
                    F_UndiscountAmt = facdeResult.undiscountAmt,
                    F_Version = facdeResult.version,
                    F_VoucherOrderNum = facdeResult.voucherOrderNum
                });
                //原订单已经退款   
                if (facdeResult.respcd == "YG"|| facdeResult.respcd == "YK")
                {
                    if (dbOrderInfo != null)
                    {
                        refundEntity.F_OrderID = dbOrderInfo.F_OrderID;
                        //payRefundReturn.F_AdminMessage = "驳回";
                        refundEntity.F_AuditorStatus = 2;
                        refundEntity.F_RefundState = 2;
                        refundEntity.F_AdminMessage += "  " + facdeResult.errorDetail;
                       
                        payRefundReturnIbll.SaveEntity(userinfo, refundEntity.F_RefundId, refundEntity);
                    }
                }
                else if(facdeResult.respcd == "00")
                {//退款成功
                    if (dbOrderInfo != null)
                    {
                        refundEntity.F_OrderID = dbOrderInfo.F_OrderID;
                        refundEntity.F_AuditorStatus = 2;
                        refundEntity.F_RefundState = 3; 
                        payRefundReturnIbll.SaveEntity(userinfo, refundEntity.F_RefundId, refundEntity);

                        dbOrderInfo.F_RefundAmount = refundEntity.F_RefundAmount;
                        dbOrderInfo.F_RefundState = 3;
                        dbOrderInfo.F_RefundCause = refundEntity.F_BuyerMessage;
                        refundEntity.F_AdminMessage += "  " + facdeResult.errorDetail;
                        payOrderIbll.SaveEntity(userinfo, dbOrderInfo.F_OrderID, dbOrderInfo);
                    }
                }
            }



        }

        //驳回的方法
        public void Fail(string processId)
        {
            //+processId 有2种含义 流程id主键 和 退款id主键
            //+1.查询退款记录信息
            var payRefundReturn = payRefundReturnIbll.GettPay_Refund_ReturnEntity(processId);

            if (payRefundReturn != null)
            {

                var dbOrderInfo = payOrderIbll.GettPay_OrderEntityByOrderSN(payRefundReturn.F_OrderSN);
                if (dbOrderInfo != null)
                {
                    payRefundReturn.F_OrderID = dbOrderInfo.F_OrderID;
                    //payRefundReturn.F_AdminMessage = "驳回";
                    payRefundReturn.F_AuditorStatus = 1;
                    payRefundReturn.F_RefundState = 2;

                    UserInfo userinfo = LoginUserInfo.Get();
                    payRefundReturnIbll.SaveEntity(userinfo, payRefundReturn.F_RefundId, payRefundReturn);
                }


            }
        }


        public void xunlianorderrefund(Pay_Refund_ReturnEntity payRefundReturn, Pay_OrderEntity dbOrderInfo)
        {
            if (payRefundReturn != null)
            {

                
               


            }
        }
    }
}