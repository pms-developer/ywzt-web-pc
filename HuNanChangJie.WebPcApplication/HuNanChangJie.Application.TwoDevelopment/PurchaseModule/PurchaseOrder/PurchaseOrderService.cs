﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 11:09
    /// 描 述：采购订单
    /// </summary>
    public class PurchaseOrderService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.ProjectID,
                t.PurchaseContractId,
                t.Name,
                t.Code,
                t.OrderAmount,
                t.OrderDate,
                t.BuyerId,
                t.SupplierId,
                t.Workflow_ID,
                t.AuditStatus,
                t.IsFinishInStorage,
                t.Name + '(' + t.Code+ ')' Abstract,t.CreationDate 
                ");
                strSql.Append("  FROM Purchase_Order t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.OrderDate >= @startTime AND t.OrderDate <= @endTime ) ");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["IsFinishInStorage"].IsEmpty())
                {
                    dp.Add("IsFinishInStorage", queryParam["IsFinishInStorage"].ToString(), DbType.Byte);
                    strSql.Append(" AND isnull(t.IsFinishInStorage,0) = @IsFinishInStorage ");
                }
                if (!queryParam["SupplierId"].IsEmpty())
                {
                    dp.Add("SupplierId", queryParam["SupplierId"].ToString(), DbType.String);
                    strSql.Append(" AND t.SupplierId = @SupplierId ");
                }
                if (!queryParam["PurchaseContractId"].IsEmpty())
                {
                    dp.Add("PurchaseContractId", queryParam["PurchaseContractId"].ToString(), DbType.String);
                    strSql.Append(" AND t.PurchaseContractId = @PurchaseContractId ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }

                var list = this.BaseRepository().FindList<Purchase_OrderEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderDetailsEntity> GetPurchase_OrderDetailsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Purchase_OrderDetailsEntity>(t => t.PurchaseOrderId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取订单编号根据供应商
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderChangeEntity> GetBase_SupplierName(string queryJson)
        {
            try
            {

                var queryParam = queryJson.ToJObject();
                var strSql = new StringBuilder();
                strSql.Append(@"select b.name  ,a.code , a.id from Purchase_Contract as a   
                    left join Base_Supplier as b on a.yi = b.ID");
                strSql.Append("  WHERE 1=1 ");
                var dp = new DynamicParameters(new { });
                if (!queryParam["name"].IsEmpty() && !queryParam["name"].IsEmpty())
                {
                    dp.Add("name", queryParam["name"].ToDate(), DbType.String);
                    strSql.Append(" AND ( b.name= @name)");
                }
                var list = this.BaseRepository().FindList<Purchase_OrderChangeEntity>(strSql.ToString(), dp);
                var query = from item in list select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Purchase_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderEntity GetPurchase_OrderEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_OrderEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderDetailsEntity GetPurchase_OrderDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_OrderDetailsEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var purchase_OrderEntity = GetPurchase_OrderEntity(keyValue);
                db.Delete<Purchase_OrderEntity>(t => t.ID == keyValue);
                db.Delete<Purchase_OrderDetailsEntity>(t => t.PurchaseOrderId == purchase_OrderEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void UpdateRelationData(List<Project_ConstructionBudgetMaterialsEntity> updateBudgetList, List<Purchase_ContractDetailsEntity> updateContractList)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                db.Update(updateBudgetList);
                db.Update(updateContractList);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void Update(Purchase_OrderEntity model, List<Purchase_OrderDetailsEntity> detailsList)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                db.Update(model);
                db.Update(detailsList);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Purchase_OrderDetailsEntity> GetDetails(IEnumerable<string> idList)
        {
            try
            {
                return BaseRepository().FindList<Purchase_OrderDetailsEntity>(i => idList.Contains(i.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_OrderEntity entity, List<Purchase_OrderDetailsEntity> purchase_OrderDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var purchase_OrderEntityTmp = GetPurchase_OrderEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Purchase_OrderDetailsUpdateList = purchase_OrderDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Purchase_OrderDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Purchase_OrderDetailsInserList = purchase_OrderDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Purchase_OrderDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.PurchaseOrderId = purchase_OrderEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Purchase_OrderDetailsEntity item in purchase_OrderDetailsList)
                    {
                        item.PurchaseOrderId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存MaterialsBuyRecord数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <param name="type">类型 audit:审核  unaudit:反审核</param>
        public void SaveMaterialsBuyRecord(string keyValue, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "audit")
                {
                    StringBuilder strSql = new StringBuilder();
                    strSql.Append("select");
                    strSql.Append("   a.PurchaseOrderId OrderId,a.PurchaseContractId ContractId,b.ProjectId,c.ID MaterialsID");
                    strSql.Append(" from Purchase_OrderDetails a");
                    strSql.Append(" join Project_ConstructionBudgetMaterials b on a.projectConstructionBudgetMaterialsId=b.id");
                    strSql.Append(" join Base_Materials c on b.Code=c.Code");
                    strSql.Append("  WHERE 1=1 ");
                    // 虚拟参数
                    var dp = new DynamicParameters(new { });
                    if (!keyValue.IsEmpty())
                    {
                        dp.Add("PurchaseOrderId", keyValue, DbType.String);
                        strSql.Append(" AND a.PurchaseOrderId=@PurchaseOrderId ");
                    }
                    List<Base_MaterialsBuyRecordEntity> list = this.BaseRepository().FindList<Base_MaterialsBuyRecordEntity>(strSql.ToString(), dp).ToList();

                    foreach (Base_MaterialsBuyRecordEntity item in list)
                    {
                        item.ID = System.Guid.NewGuid().ToString();
                        item.EditType = EditType.Add;

                        db.Insert(item);
                    }
                }
                else if (type == "unaudit")
                {
                    StringBuilder strSql = new StringBuilder();
                    strSql.Append("select * from Base_MaterialsBuyRecord");

                    // 虚拟参数
                    var dp = new DynamicParameters(new { });
                    if (!keyValue.IsEmpty())
                    {
                        dp.Add("OrderId", keyValue, DbType.String);
                        strSql.Append("  where OrderId=@OrderId ");
                    }
                    List<Base_MaterialsBuyRecordEntity> list = this.BaseRepository().FindList<Base_MaterialsBuyRecordEntity>(strSql.ToString(), dp).ToList();
                    db.Delete(list);
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取项目采购订单
        /// </summary>
        /// <param name="projectId">项目id</param>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderEntity> GetProjectOrders(string projectId)
        {
            try
            {
                if(string.IsNullOrWhiteSpace(projectId))
                {
                    return this.BaseRepository().FindList<Purchase_OrderEntity>(i=>i.AuditStatus=="2");
                }
                else
                { 
                    return this.BaseRepository().FindList<Purchase_OrderEntity>(i=>i.ProjectID==projectId);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<Purchase_OrderDetailsEntity> GetOrderDetailList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();

                strSql.Append("SELECT * from(");
                strSql.Append(@"
                select 
                a.*,
                d.BudgetPrice,
                c.id as contractId,
                d.Code,
                d.Name,
                d.Brand,
                d.ModelNumber,

                d.Unit,
                d.ContractQuantity,
                b.TaxRage,
                b.Code as OrderCode,
                c.Name as ContractName from Purchase_OrderDetails as a left join 
                Purchase_Order as b on a.PurchaseOrderId=b.ID left join
                Purchase_Contract as c on b.PurchaseContractId=c.ID left join
                Purchase_ContractDetails as d on d.id=a.PurchaseContractDetailsId)
                ");
                strSql.Append("  as t ");
                strSql.Append("  WHERE 1=1 and ISNULL(Quantity,0)-ISNULL(ReceivedInvoiceQuantity,0)>0 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["contractId"].IsEmpty())
                {
                    dp.Add("contractId", queryParam["contractId"].ToString(), DbType.String);

                    strSql.Append(" AND t.contractId = @contractId");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }

                var list = this.BaseRepository().FindList<Purchase_OrderDetailsEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<Purchase_OrderDetailsEntity> GetDetailListByProjectId(XqPagination pagination, string queryJson)
        {
            //   d.Unit,
            var strSql = new StringBuilder();
            strSql.Append(@"select 
                        d.Code,
                        d.Name,
                     
(select bmu.ID from Base_Materials as bm join Base_MaterialsUnit as bmu on bm.UnitId = bmu.ID where bm.ID = a.Fk_BaseMaterialsId) as Unit,
                        d.Brand,
                        d.ModelNumber,
                        b.Code as OrderCode,
                        c.ProjectName,
                        e.Name as SupplierName,
                        a.* 
                        from Purchase_OrderDetails as a ");
            strSql.Append(" left join Purchase_Order as b on a.PurchaseOrderId=b.ID  ");
            strSql.Append(" left join Base_CJ_Project as c on b.ProjectID=c.ID ");
            strSql.Append(" left join Purchase_ContractDetails as d on a.PurchaseContractDetailsId=d.ID ");
            strSql.Append(" left join Base_Supplier as e on b.SupplierId = e.ID");
            strSql.Append(" where b.AuditStatus='2' ");
            strSql.Append(" and (a.quantity-(case  when a.InStorage is null then 0 else a.InStorage end))>0");
            var queryParam = queryJson.ToJObject();
            // 虚拟参数
            var dp = new DynamicParameters(new { });
            if (!queryParam["projectId"].IsEmpty())
            {
                dp.Add("projectId", queryParam["projectId"].ToString(), DbType.String);
                strSql.Append("   AND b.projectId=@projectId");
            }
            //if (!queryParam["ContractId"].IsEmpty())
            //{
            //    dp.Add("ContractId", queryParam["ContractId"].ToString(), DbType.String);
            //    strSql.Append(" And t.ProjectContractId=@ContractId");
            //}
            if (!queryParam["Name"].IsEmpty())
            {
                dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                strSql.Append("    AND  ProjectName like @Name");
            }
            var list = this.BaseRepository().FindList<Purchase_OrderDetailsEntity>(strSql.ToString(), dp, pagination);//new { projectId }
            var query = from item in list orderby item.SortCode ascending select item;
            return query;
        }

        public IEnumerable<Purchase_OrderDetailsEntity> GetDetailList(XqPagination pagination, string queryJson)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append("select * from ( ");
                sb.Append("select case isnull(f.id,'') when '' then  g.id else f.id end MaterialsId,a.*,b.ProjectID,b.SupplierId,");
                sb.Append("c.ProjectName,");
                sb.Append("case isnull(d.name,'') when '' then  gg.name else d.name end Name,");
                sb.Append("case isnull(d.Code,'') when '' then  gg.Code else d.Code end Code,");
                sb.Append("case isnull(d.Unit,'') when '' then  ggg.ID else d.Unit end Unit,");
                sb.Append("case isnull(d.Brand,'') when '' then  gg.BrandId else d.Brand end Brand,");
                sb.Append("case isnull(d.ModelNumber,'') when '' then  gg.Model else d.ModelNumber end ModelNumber,");
                sb.Append("d.BudgetPrice,case isnull(d.MaterialsSource,'') when '' then 1 else d.MaterialsSource end MaterialsSource ");
                sb.Append("from ");
                sb.Append("Purchase_OrderDetails as a");
                sb.Append(" left join Purchase_Order as b on a.PurchaseOrderId=b.ID ");
                sb.Append(" left join Base_CJ_Project as c on b.ProjectID = c.ID ");
                sb.Append(" left join Purchase_ContractDetails as d on a.PurchaseContractDetailsId = d.ID ");
                sb.Append(" left join Project_ConstructionBudgetMaterials e on a.ProjectConstructionBudgetMaterialsId = e.id ");
                sb.Append(" left join Base_Materials f on e.code = f.code  ");
                sb.Append(" left join Base_Materials g on d.code = g.code  ");
                sb.Append(" left join Base_Materials gg on a.Fk_BaseMaterialsId = gg.id ");
                sb.Append(" left join Base_MaterialsUnit ggg on gg.unitid = ggg.id ");
                sb.Append(" where b.AuditStatus = '2' ");//and PurchaseOrderId =@keyValue
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["keyValue"].IsEmpty())
                {
                    dp.Add("keyValue", queryParam["keyValue"].ToString(), DbType.String);
                    sb.Append(" AND PurchaseOrderId=@keyValue");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    sb.Append("    AND  c.ProjectName like @Name");
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    sb.Append(" AND b.ProjectID = @ProjectID ");
                }
                if (!queryParam["MaterialsSource"].IsEmpty())
                {
                    dp.Add("MaterialsSource", queryParam["MaterialsSource"].ToString(), DbType.Byte);
                    sb.Append(" AND (d.MaterialsSource = @MaterialsSource or gg.id is not null)");
                }
                if (!queryParam["CheckQuantity"].IsEmpty())
                {
                    sb.Append(" AND isnull(a.Quantity,0) - isnull(a.InStorageQuantity,0) > 0");
                }

                sb.Append(" ) t where 1=1 ");

                if (!queryParam["MaterialsName"].IsEmpty())
                {
                    dp.Add("MaterialsName", "%" + queryParam["MaterialsName"].ToString() + "%", DbType.String);
                    sb.Append("    AND ( Name like @MaterialsName or Code like @MaterialsName)");
                }

                var list = this.BaseRepository().FindList<Purchase_OrderDetailsEntity>(sb.ToString(), dp, pagination);//new {keyValue }
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Purchase_OrderEntity> GetOrderList(string supplierId)
        {
            try
            {
                var sql = "select a.ID,a.Code,b.Yi from Purchase_Order as a left join Purchase_Contract as b on a.PurchaseContractId = b.ID  where a.AuditStatus = '2' and b.AuditStatus = '2' and b.Yi=@Yi";
                return BaseRepository().FindList<Purchase_OrderEntity>(sql, new { Yi = supplierId });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
