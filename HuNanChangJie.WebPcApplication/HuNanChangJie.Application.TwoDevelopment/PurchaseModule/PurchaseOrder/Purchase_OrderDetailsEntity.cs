﻿using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;
using HuNanChangJie.SystemCommon.Extensions;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 11:09
    /// 描 述：采购订单
    /// </summary>
    public class Purchase_OrderDetailsEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }

        /// <summary>
        /// 材料库材料ID
        /// </summary>
        [Column("FK_BASEMATERIALSID")]
        public string Fk_BaseMaterialsId { get; set; }
        /// <summary>
        /// 采购订单主键
        /// </summary>
        [Column("PURCHASEORDERID")]
        public string PurchaseOrderId { get; set; }
        /// <summary>
        /// 采购合同ID
        /// </summary>
        [Column("PURCHASECONTRACTID")]
        public string PurchaseContractId { get; set; }
        /// <summary>
        /// 采购合同详情ID
        /// </summary>
        [Column("PURCHASECONTRACTDETAILSID")]
        public string PurchaseContractDetailsId { get; set; }
        /// <summary>
        /// 施工预算材料清单ID
        /// </summary>
        [Column("PROJECTCONSTRUCTIONBUDGETMATERIALSID")]
        public string ProjectConstructionBudgetMaterialsId { get; set; }
        /// <summary>
        /// 采购数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 入库数量
        /// </summary>
        [Column("INSTORAGEQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? InStorageQuantity { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }

        /// <summary>
        /// 结算数量
        /// </summary>
        [Column("SETTLEMENTQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementQuantity { get; set; }

        /// <summary>
        /// 计划到货日期
        /// </summary>
        [Column("PLANDATE")]
        public DateTime? PlanDate { get; set; }
        /// <summary>
        /// 要求交货周期
        /// </summary>
        [Column("PERIOD")]
        public int? Period { get; set; }
        /// <summary>
        /// 交货地点
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }

        /// <summary>
        /// 已收票数量
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? ReceivedInvoiceQuantity { get; set; }

        /// <summary>
        /// 收货未收票数量
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? SHWSP { get; set; }

        /// <summary>
        /// 入库数量
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? InStorage { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }

        [DecimalPrecision(18, 4)]
        public decimal? ChangePrice { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region 扩展字段
        [NotMapped]
        public string Code { get; set; }

        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public string Brand { get; set; }

        [NotMapped]
        public string ModelNumber { get; set; }

        [NotMapped]
        public string Unit { get; set; }

        [NotMapped]
        public string ProjectName { get; set; }

        [NotMapped]
        public string ProjectID { get; set; }



        [NotMapped]
        public decimal? BudgetPrice { get; set; }

        [NotMapped]
        public string OrderCode { get; set; }
        [NotMapped]
        public string MaterialsId { get; set; }
        [NotMapped]
        public int MaterialsSource { get; set; }
        
        [NotMapped]
        public string SupplierName { get; set; }
        [NotMapped]
        public string SupplierId { get; set; }
        [NotMapped]
        public decimal? WaitInStorage => (Quantity ?? 0) - (InStorage ?? 0);

        /// <summary>
        /// 合同数量
        /// </summary>
        [NotMapped]
        public decimal ContractQuantity { get; set; }

        /// <summary>
        /// 税率
        /// </summary>
        [NotMapped]
        public decimal TaxRage { get; set; }

        /// <summary>
        /// 不含税单价
        /// </summary>
        [NotMapped]
        public decimal NotTaxUnit
        {
            get
            {
                if (Quantity == null) return 0;
                return (NotTaxAmount / (Quantity ?? 0)).ToRound();
            }
        }

        /// <summary>
        /// 不含税金额
        /// </summary>
        [NotMapped]
        public decimal NotTaxAmount
        {
            get
            {
                if (TotalPrice == null) return 0;
                return ((TotalPrice??0)- TaxAmount).ToRound();
            }
        }


        /// <summary>
        /// 税额
        /// </summary>
        [NotMapped]
        public decimal TaxAmount
        {
            get
            {
                var taxRage = TaxRage / 100;
                return ((TotalPrice ?? 0) / (1 + taxRage) * taxRage).ToRound();
            }
        }

        /// <summary>
        /// 合同名称
        /// </summary>
        [NotMapped]
        public string ContractName { get; set; }
        #endregion
    }
}

