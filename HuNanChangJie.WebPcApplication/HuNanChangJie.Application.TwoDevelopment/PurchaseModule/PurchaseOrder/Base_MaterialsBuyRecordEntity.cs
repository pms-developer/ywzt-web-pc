﻿using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 订单购买记录
    /// </summary>
    public class Base_MaterialsBuyRecordEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }

        /// <summary>
        /// 材料ID
        /// </summary>
        [Column("MATERIALSID")]
        public string MaterialsID { get; set; }

        /// <summary>
        /// 合同ID
        /// </summary>
        [Column("CONTRACTID")]
        public string ContractId { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }

        /// <summary>
        /// 订单ID
        /// </summary>
        [Column("ORDERID")]
        public string OrderId { get; set; }

        #endregion
    }
}
