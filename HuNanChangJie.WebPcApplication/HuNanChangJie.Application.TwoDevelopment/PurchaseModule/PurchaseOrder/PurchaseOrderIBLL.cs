﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 11:09
    /// 描 述：采购订单
    /// </summary>
    public interface PurchaseOrderIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Purchase_OrderEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取项目采购订单
        /// </summary>
        /// <param name="projectId">项目id</param>
        /// <returns></returns>
        IEnumerable<Purchase_OrderEntity> GetProjectOrders(string projectId);
        /// <summary>
        /// 获取Purchase_OrderDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Purchase_OrderDetailsEntity> GetPurchase_OrderDetailsList(string keyValue);
        /// <summary>
        /// 获取Purchase_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_OrderEntity GetPurchase_OrderEntity(string keyValue);
        /// <summary>
        /// 获取Purchase_OrderDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_OrderDetailsEntity GetPurchase_OrderDetailsEntity(string keyValue);
        /// <summary>
        /// 获取订单编号根据供应商
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        IEnumerable<Purchase_OrderChangeEntity> GetBase_SupplierNameList(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Purchase_OrderEntity entity,List<Purchase_OrderDetailsEntity> purchase_OrderDetailsList,string deleteList,string type);
        void Audit(string keyValue);
        IEnumerable<Purchase_OrderEntity> GetOrderList(string supplierId);
        void UnAudit(string keyValue);
        IEnumerable<Purchase_OrderDetailsEntity> GetDetailList(XqPagination paginationobj,string queryJson);
        IEnumerable<Purchase_OrderDetailsEntity> GetDetailListByProjectId(XqPagination paginationobj,string queryJson);
        IEnumerable<Purchase_OrderDetailsEntity> GetOrderDetailList(XqPagination paginationobj, string queryJson);

        void SaveMaterialsBuyRecord(string keyValue, string type);
        #endregion

    }
}
