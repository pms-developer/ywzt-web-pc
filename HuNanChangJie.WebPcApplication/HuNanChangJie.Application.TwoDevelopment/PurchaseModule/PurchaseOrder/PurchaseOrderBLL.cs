﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.SystemCommon;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 11:09
    /// 描 述：采购订单
    /// </summary>
    public class PurchaseOrderBLL : PurchaseOrderIBLL
    {
        private PurchaseOrderService purchaseOrderService = new PurchaseOrderService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return purchaseOrderService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderDetailsEntity> GetPurchase_OrderDetailsList(string keyValue)
        {
            try
            {
                return purchaseOrderService.GetPurchase_OrderDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取订单编号根据供应商
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderChangeEntity> GetBase_SupplierNameList(string keyValue)
        {
            try
            {
                return purchaseOrderService.GetBase_SupplierName(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        

        /// <summary>
        /// 获取Purchase_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderEntity GetPurchase_OrderEntity(string keyValue)
        {
            try
            {
                return purchaseOrderService.GetPurchase_OrderEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderDetailsEntity GetPurchase_OrderDetailsEntity(string keyValue)
        {
            try
            {
                return purchaseOrderService.GetPurchase_OrderDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                purchaseOrderService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_OrderEntity entity,List<Purchase_OrderDetailsEntity> purchase_OrderDetailsList,string deleteList,string type)
        {
            try
            {
                purchaseOrderService.SaveEntity(keyValue, entity,purchase_OrderDetailsList,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("PurchaseOrderCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void Audit(string keyValue)
        {
            try
            {
                var contractBll = new PurchaseContractBLL();
                Purchase_OrderEntity purchase_OrderEntity = purchaseOrderService.GetPurchase_OrderEntity(keyValue);

                if (purchase_OrderEntity != null)
                {
                    Purchase_ContractEntity purchase_ContractEntity= contractBll.GetPurchase_ContractEntity(purchase_OrderEntity.PurchaseContractId);
                    if (purchase_ContractEntity.PriceTypeEnum == PriceType.Framework)
                    {
                        purchase_ContractEntity.ActualAmount = (purchase_ContractEntity.ActualAmount ??0) + purchase_OrderEntity.OrderAmount;
                        contractBll.UpdateEntity(purchase_ContractEntity);
                    }
                }

                var details = purchaseOrderService.GetPurchase_OrderDetailsList(keyValue);
                if (details.Any())
                {
                    var updateBudgetList = new List<Project_ConstructionBudgetMaterialsEntity>();
                    var updateContractList = new List<Purchase_ContractDetailsEntity>();

                    var budgetBll = new ProjectConstructionBudgetBLL();
                    
                    var contractId = details.First().PurchaseContractId;
                    var contractDetailsIdList = details.Select(i => i.PurchaseContractDetailsId);
                    var budgetDetailsIdList = details.Select(i => i.ProjectConstructionBudgetMaterialsId);
                    var budgetList = budgetBll.GetMaterials(budgetDetailsIdList);
                    var contractList = contractBll.GetDetails(contractDetailsIdList);
                    foreach (var item in details)
                    {
                        //不是施工预算内的材料不更新
                        if (item.ProjectConstructionBudgetMaterialsId.IsEmpty())
                            continue;

                        //更新预算中的已采购数
                        var budgetInfo = budgetList.First(i => i.ID == item.ProjectConstructionBudgetMaterialsId);
                        budgetInfo.PurchasedQuantity = (budgetInfo.PurchasedQuantity ?? 0) + (item.Quantity ?? 0);
                        budgetInfo.PurchasedTotalPrice = (budgetInfo.PurchasedTotalPrice ?? 0) + (item.TotalPrice ?? 0);
                        if (budgetInfo.PurchasedQuantity > 0)
                        {
                            budgetInfo.PurchasedPrice = budgetInfo.PurchasedTotalPrice / budgetInfo.PurchasedQuantity;
                        }
                        updateBudgetList.Add(budgetInfo);

                        //更新合同中的已下单数量
                        var contractInfo = contractList.First(i => i.ID == item.PurchaseContractDetailsId);
                        contractInfo.TotalOrderQuantity = (contractInfo.TotalOrderQuantity ?? 0) + (item.Quantity ?? 0);
                        updateContractList.Add(contractInfo);

                    }

                    purchaseOrderService.UpdateRelationData(updateBudgetList, updateContractList);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void Update(Purchase_OrderEntity model, List<Purchase_OrderDetailsEntity> detailsList)
        {
            try
            {
                purchaseOrderService.Update(model, detailsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_OrderDetailsEntity> GetDetails(IEnumerable<string> idList)
        {
            try
            {
                return purchaseOrderService.GetDetails(idList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UnAudit(string keyValue)
        {
            try
            {
                var contractBll = new PurchaseContractBLL();
                Purchase_OrderEntity purchase_OrderEntity = purchaseOrderService.GetPurchase_OrderEntity(keyValue);

                if (purchase_OrderEntity != null)
                {
                    Purchase_ContractEntity purchase_ContractEntity = contractBll.GetPurchase_ContractEntity(purchase_OrderEntity.PurchaseContractId);
                    if (purchase_ContractEntity.PriceTypeEnum == PriceType.Framework)
                    {
                        purchase_ContractEntity.ActualAmount = (purchase_ContractEntity.ActualAmount ?? 0) - purchase_OrderEntity.OrderAmount;
                        contractBll.UpdateEntity(purchase_ContractEntity);
                    }
                }

                var details = purchaseOrderService.GetPurchase_OrderDetailsList(keyValue);
                if (details.Any())
                {
                    var updateBudgetList = new List<Project_ConstructionBudgetMaterialsEntity>();
                    var updateContractList = new List<Purchase_ContractDetailsEntity>();

                    var budgetBll = new ProjectConstructionBudgetBLL();
                    var contractId = details.First().PurchaseContractId;
                    var contractDetailsIdList = details.Select(i => i.PurchaseContractDetailsId);
                    var budgetDetailsIdList = details.Select(i => i.ProjectConstructionBudgetMaterialsId);
                    var budgetList = budgetBll.GetMaterials(budgetDetailsIdList);
                    var contractList = contractBll.GetDetails(contractDetailsIdList);
                    foreach (var item in details)
                    {
                        //不是施工预算内的材料不更新
                        if (item.ProjectConstructionBudgetMaterialsId.IsEmpty())
                            continue;

                        //更新预算中的已采购数
                        var budgetInfo = budgetList.First(i => i.ID == item.ProjectConstructionBudgetMaterialsId);
                        budgetInfo.PurchasedQuantity = (budgetInfo.PurchasedQuantity ?? 0) - (item.Quantity ?? 0);
                        budgetInfo.PurchasedTotalPrice = (budgetInfo.PurchasedTotalPrice ?? 0) - (item.TotalPrice ?? 0);
                        if (budgetInfo.PurchasedQuantity > 0)
                        {
                            budgetInfo.PurchasedPrice = budgetInfo.PurchasedTotalPrice / budgetInfo.PurchasedQuantity;
                        }
                        updateBudgetList.Add(budgetInfo);

                        //更新合同中的已下单数量
                        var contractInfo = contractList.First(i => i.ID == item.PurchaseContractDetailsId);
                        contractInfo.TotalOrderQuantity = (contractInfo.TotalOrderQuantity ?? 0) - (item.Quantity ?? 0);
                        updateContractList.Add(contractInfo);

                    }

                    purchaseOrderService.UpdateRelationData(updateBudgetList, updateContractList);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_OrderEntity> GetOrderList(string supplierId)
        {
            try
            {
                return purchaseOrderService.GetOrderList(supplierId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_OrderDetailsEntity> GetDetailList(XqPagination pagination,string queryJson)
        {
            try
            {
                return purchaseOrderService.GetDetailList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_OrderDetailsEntity> GetDetailListByProjectId(XqPagination pagination,string queryJson)
        {
            try
            {
                return purchaseOrderService.GetDetailListByProjectId(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_OrderDetailsEntity> GetOrderDetailList(XqPagination pagination, string queryJson)
        {
            try
            {
                return purchaseOrderService.GetOrderDetailList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void SaveMaterialsBuyRecord(string keyValue, string type)
        {
            try
            {
                purchaseOrderService.SaveMaterialsBuyRecord(keyValue, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取项目采购订单
        /// </summary>
        /// <param name="projectId">项目id</param>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderEntity> GetProjectOrders(string projectId)
        {
            try
            {
                return purchaseOrderService.GetProjectOrders(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
