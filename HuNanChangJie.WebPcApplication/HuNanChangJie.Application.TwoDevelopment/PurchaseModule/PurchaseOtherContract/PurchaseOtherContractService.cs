﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-16 15:02
    /// 描 述：其他采购合同
    /// </summary>
    public class PurchaseOtherContractService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_OtherContractEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.SignDate,
                t.Jia,
                t.Yi,
                t.YiDesc,
                t.YiCode,
                t.ContractType,
                t.SignAmount,
                t.IsContract,
                t.ProjectID,
                t.IsBudget,
                t.pinyin,
                t.AuditStatus,
                t.CreationDate,
                t.Workflow_ID
                ");
                strSql.Append("  FROM Purchase_OtherContract t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (pagination !=null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "Code";
                    pagination.sord = "DESC";
                }

                var list =this.BaseRepository().FindList<Purchase_OtherContractEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OtherContract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OtherContractEntity GetPurchase_OtherContractEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_OtherContractEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        #endregion

        #region  提交数据


        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {

                Purchase_OtherContractEntity purchase_OtherContractEntity = db.FindEntity<Purchase_OtherContractEntity>(keyValue);
                if (purchase_OtherContractEntity == null)
                    throw new Exception("您选择的采购合同不存在");
                if (purchase_OtherContractEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                purchase_OtherContractEntity.AuditStatus = "4";

                //if (string.IsNullOrEmpty(purchase_OtherContractEntity.Yi))
                //{
                //    if (!string.IsNullOrEmpty(purchase_OtherContractEntity.YiDesc) && !string.IsNullOrEmpty(purchase_OtherContractEntity.YiCode))
                //    {

                //        Base_SupplierEntity base_SupplierEntity = db.FindEntity<Base_SupplierEntity>(m => m.Name == purchase_OtherContractEntity.YiDesc);

                //        if (base_SupplierEntity == null)
                //        {
                //            string yi = Guid.NewGuid().ToString();
                //            purchase_OtherContractEntity.Yi = yi;
                //            base_SupplierEntity = new Base_SupplierEntity() { ID = yi, Code = new CodeRuleBLL().GetBillCode("CJ_BASE_0017"), Name = purchase_OtherContractEntity.YiDesc, Flag = purchase_OtherContractEntity.YiCode };
                //            db.Insert<Base_SupplierEntity>(base_SupplierEntity);
                //            new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0017");
                //        }
                //        else
                //        {
                //            purchase_OtherContractEntity.Yi = base_SupplierEntity.ID;
                //        }
                //    }
                //}

                db.Update<Purchase_OtherContractEntity>(purchase_OtherContractEntity);

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {

                Purchase_OtherContractEntity purchase_OtherContractEntity = db.FindEntity<Purchase_OtherContractEntity>(keyValue);
                if (purchase_OtherContractEntity == null)
                    throw new Exception("您选择的采购合同不存在");


                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (purchase_OtherContractEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                purchase_OtherContractEntity.AuditStatus = "2";

                if (string.IsNullOrEmpty(purchase_OtherContractEntity.Yi))
                {
                    if (!string.IsNullOrEmpty(purchase_OtherContractEntity.YiDesc) && !string.IsNullOrEmpty(purchase_OtherContractEntity.YiCode))
                    {

                        Base_SupplierEntity base_SupplierEntity = db.FindEntity<Base_SupplierEntity>(m => m.Name == purchase_OtherContractEntity.YiDesc);

                        if (base_SupplierEntity == null)
                        {
                            string yi = Guid.NewGuid().ToString();
                            purchase_OtherContractEntity.Yi = yi;
                            base_SupplierEntity = new Base_SupplierEntity() { ID = yi, AuditStatus="2", Code = new CodeRuleBLL().GetBillCode("CJ_BASE_0017"), Name = purchase_OtherContractEntity.YiDesc, Flag = purchase_OtherContractEntity.YiCode };
                            db.Insert<Base_SupplierEntity>(base_SupplierEntity);
                            new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0017");
                        }
                        else
                        {
                            purchase_OtherContractEntity.Yi = base_SupplierEntity.ID;
                        }
                    }
                }

                db.Update<Purchase_OtherContractEntity>(purchase_OtherContractEntity);

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Purchase_OtherContractEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_OtherContractEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
