﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2020-03-19 09:55
    /// 描 述：现场收货单
    /// </summary>
    public class StorageLocaleMatesReceiptService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Storage_LocaleMatesReceiptEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.ReceiptUserId,
                t.ProjectID,
                t.ReceiptDate,
                t.AuditStatus,
                t.CreationDate,
                t.Workflow_ID,
                t.Abstract
                ");
                strSql.Append("  FROM Storage_LocaleMatesReceipt t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectId=@ProjectId ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.ReceiptDate >= @startTime AND t.ReceiptDate <= @endTime ) ");
                }
                var list = this.BaseRepository().FindList<Storage_LocaleMatesReceiptEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Storage_LocalematesReceiptDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Storage_LocalematesReceiptDetailsEntity> GetStorage_LocalematesReceiptDetailsList(string keyValue)
        {
            try
            {
                // d.Unit,
                var sb = new StringBuilder();
                sb.Append(@"select DISTINCT a.*,
                            b.InStorage,
                            b.Quantity as OrderQuantity,
                            c.Code as OrderCode,
                            d.Code, 
                            d.Brand,
                           (select bmu.ID from Base_Materials as bm join Base_MaterialsUnit as bmu on bm.UnitId = bmu.ID where bm.ID = b.Fk_BaseMaterialsId) as Unit,
                            d.ModelNumber,
                            d.Name,
                            e.Name as SupplierName, 
                            f.ProjectName
                            from Storage_LocalematesReceiptDetails as a
                            left join Purchase_OrderDetails as b on a.PurchaseOrderDetailsID = b.ID
                            left join Purchase_Order as c on a.PurchaseOrderId = c.ID
                            left join Purchase_ContractDetails as d on a.PurchaseContractDetailsId = d.ID
                            left join Base_Supplier as e on c.SupplierId = e.ID
                            left join Base_CJ_Project as f on c.ProjectID = f.ID
                            left join Storage_LocaleMatesReceipt as g on a.StorageLocaleMatesReceiptId=g.ID ");
                sb.Append("where a.StorageLocaleMatesReceiptId=@keyValue ");
                var list = this.BaseRepository().FindList<Storage_LocalematesReceiptDetailsEntity>(sb.ToString(), new { keyValue });
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Storage_LocaleMatesReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Storage_LocaleMatesReceiptEntity GetStorage_LocaleMatesReceiptEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Storage_LocaleMatesReceiptEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Storage_LocalematesReceiptDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Storage_LocalematesReceiptDetailsEntity GetStorage_LocalematesReceiptDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Storage_LocalematesReceiptDetailsEntity>(t => t.StorageLocaleMatesReceiptId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var storage_LocaleMatesReceiptEntity = GetStorage_LocaleMatesReceiptEntity(keyValue);
                db.Delete<Storage_LocaleMatesReceiptEntity>(t => t.ID == keyValue);
                db.Delete<Storage_LocalematesReceiptDetailsEntity>(t => t.StorageLocaleMatesReceiptId == storage_LocaleMatesReceiptEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Storage_LocaleMatesReceiptEntity entity, List<Storage_LocalematesReceiptDetailsEntity> storage_LocalematesReceiptDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var storage_LocaleMatesReceiptEntityTmp = GetStorage_LocaleMatesReceiptEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Storage_LocalematesReceiptDetailsUpdateList = storage_LocalematesReceiptDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Storage_LocalematesReceiptDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Storage_LocalematesReceiptDetailsInserList = storage_LocalematesReceiptDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Storage_LocalematesReceiptDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.StorageLocaleMatesReceiptId = storage_LocaleMatesReceiptEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Storage_LocalematesReceiptDetailsEntity item in storage_LocalematesReceiptDetailsList)
                    {
                        item.StorageLocaleMatesReceiptId = entity.ID;
                        item.CreationDate = DateTime.Now;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void Audit(string type, string keyValue, List<Purchase_OrderDetailsEntity> orderDetailList, List<Purchase_ContractDetailsEntity> contractDetailList, List<Project_ConstructionBudgetMaterialsEntity> budgetMaterailsList)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                Storage_LocaleMatesReceiptEntity storage_LocaleMatesReceiptEntity = db.FindEntity<Storage_LocaleMatesReceiptEntity>(keyValue);
                

                if (type == "Audit")
                {
                    if (storage_LocaleMatesReceiptEntity.AuditStatus == "2")
                    {
                        throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                    }
                    storage_LocaleMatesReceiptEntity.AuditStatus = "2";
                }
                else
                {
                    if (storage_LocaleMatesReceiptEntity.AuditStatus == "4")
                    {
                        throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                    }
                    storage_LocaleMatesReceiptEntity.AuditStatus = "4";
                }
                db.Update<Storage_LocaleMatesReceiptEntity>(storage_LocaleMatesReceiptEntity);

                var flag = false;
                if (orderDetailList.Count > 0)
                {
                    flag = true;
                    db.Update(orderDetailList);
                }
                if (contractDetailList.Count > 0)
                {
                    flag = true;
                    db.Update(contractDetailList);
                }
                if (budgetMaterailsList.Count > 0)
                {
                    flag = true;
                    db.Update(budgetMaterailsList);
                }
                if (flag)
                {
                    db.Commit();
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
    }
}