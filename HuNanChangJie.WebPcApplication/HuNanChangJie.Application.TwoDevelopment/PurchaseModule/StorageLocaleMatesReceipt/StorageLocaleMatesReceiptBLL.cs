﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-19 09:55
    /// 描 述：现场收货单
    /// </summary>
    public class StorageLocaleMatesReceiptBLL : StorageLocaleMatesReceiptIBLL
    {
        private StorageLocaleMatesReceiptService storageLocaleMatesReceiptService = new StorageLocaleMatesReceiptService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Storage_LocaleMatesReceiptEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return storageLocaleMatesReceiptService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Storage_LocalematesReceiptDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Storage_LocalematesReceiptDetailsEntity> GetStorage_LocalematesReceiptDetailsList(string keyValue)
        {
            try
            {
                return storageLocaleMatesReceiptService.GetStorage_LocalematesReceiptDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Storage_LocaleMatesReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Storage_LocaleMatesReceiptEntity GetStorage_LocaleMatesReceiptEntity(string keyValue)
        {
            try
            {
                return storageLocaleMatesReceiptService.GetStorage_LocaleMatesReceiptEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Storage_LocalematesReceiptDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Storage_LocalematesReceiptDetailsEntity GetStorage_LocalematesReceiptDetailsEntity(string keyValue)
        {
            try
            {
                return storageLocaleMatesReceiptService.GetStorage_LocalematesReceiptDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                storageLocaleMatesReceiptService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Storage_LocaleMatesReceiptEntity entity, List<Storage_LocalematesReceiptDetailsEntity> storage_LocalematesReceiptDetailsList, string deleteList, string type)
        {
            try
            {
                storageLocaleMatesReceiptService.SaveEntity(keyValue, entity, storage_LocalematesReceiptDetailsList, deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("StorageLocaleMatesReceiptCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void Audit(string keyValue)
        {
            try
            {
                var details = storageLocaleMatesReceiptService.GetStorage_LocalematesReceiptDetailsList(keyValue);
                if (details.Any())
                {
                    PurchaseOrderIBLL orderBll = new PurchaseOrderBLL();
                    PurchaseContractIBLL contractBll = new PurchaseContractBLL();
                    ProjectConstructionBudgetIBLL budgetBll = new ProjectConstructionBudgetBLL();

                    var orderDetailList = new List<Purchase_OrderDetailsEntity>();
                    var contractDetailList = new List<Purchase_ContractDetailsEntity>();
                    var budgetMaterailsList = new List<Project_ConstructionBudgetMaterialsEntity>();

                    var purchase_ContractDetailsList = new List<Purchase_ContractDetails>();
                    Purchase_ContractDetails purchase_ContractDetails = new Purchase_ContractDetails();
                    List<string> list1 = new List<string>();

                    var Project_ConstructionBudgetMaterialsList = new List<Project_ConstructionBudgetMaterials>();
                    Project_ConstructionBudgetMaterials Project_ConstructionBudgetMaterials = new Project_ConstructionBudgetMaterials();
                    List<string> list2 = new List<string>();


                    foreach (var item in details)
                    {
                        var orderDetailId = item.PurchaseOrderDetailsID;
                        var orderDetailInfo = orderBll.GetPurchase_OrderDetailsEntity(orderDetailId);
                        orderDetailInfo.InStorage = (orderDetailInfo.InStorage ?? 0) + (item.Quantity ?? 0);
                        orderDetailInfo.SHWSP = (orderDetailInfo.SHWSP ?? 0) + (item.Quantity ?? 0);
                        orderDetailList.Add(orderDetailInfo);

                        if (list1.Contains(item.PurchaseContractDetailsId))
                        {
                            var purchase_ContractDetailsList1 = purchase_ContractDetailsList.FindAll(i => i.ID == item.PurchaseContractDetailsId);

                            foreach (var a in purchase_ContractDetailsList1)
                            {
                                var contractDetailId = item.PurchaseContractDetailsId;
                                var contractDetailInfo = contractBll.GetPurchase_ContractDetailsEntity(contractDetailId);
                                for (var b = 0; b < contractDetailList.Count; b++)
                                {
                                    var bID = contractDetailList[b].ID;

                                    if (bID == a.ID)
                                    {
                                        contractDetailList.RemoveAt(b);
                                    }
                                }
                                contractDetailInfo.TotalInStorageQuantity = (a.TotalInStorageQuantity ?? 0) + (item.Quantity ?? 0);
                                contractDetailList.Add(contractDetailInfo);
                            }
                        }
                        else
                        {
                            var contractDetailId = item.PurchaseContractDetailsId;
                            var contractDetailInfo = contractBll.GetPurchase_ContractDetailsEntity(contractDetailId);
                            contractDetailInfo.TotalInStorageQuantity = (contractDetailInfo.TotalInStorageQuantity ?? 0) + (item.Quantity ?? 0);
                            contractDetailList.Add(contractDetailInfo);

                            purchase_ContractDetails.ID = item.PurchaseContractDetailsId;
                            purchase_ContractDetails.TotalInStorageQuantity = contractDetailInfo.TotalInStorageQuantity;
                            purchase_ContractDetailsList.Add(purchase_ContractDetails);
                            list1.Add(item.PurchaseContractDetailsId);
                        }

                        if (list2.Contains(item.ProjectConstructionBudgetMaterialsId))
                        {
                            var ProjectConstructionBudgetMaterialsId1 = Project_ConstructionBudgetMaterialsList.FindAll(i => i.ID == item.ProjectConstructionBudgetMaterialsId);

                            foreach (var a in ProjectConstructionBudgetMaterialsId1)
                            {
                                var budgetMaterailsId = item.ProjectConstructionBudgetMaterialsId;
                                var budgetMaterailsInfo = budgetBll.GetProject_ConstructionBudgetMaterialsEntity(budgetMaterailsId);
                                for (var b = 0; b < Project_ConstructionBudgetMaterialsList.Count; b++)
                                {
                                    var bID = Project_ConstructionBudgetMaterialsList[b].ID;

                                    if (bID == a.ID)
                                    {
                                        budgetMaterailsList.RemoveAt(b);
                                    }
                                }

                                if (budgetMaterailsInfo == null) continue;
                                budgetMaterailsInfo.StorageQuantity = (a.StorageQuantity ?? 0) + (item.Quantity ?? 0);
                                budgetMaterailsInfo.StorageTotalPrice = (budgetMaterailsInfo.StorageQuantity ?? 0) * (orderDetailInfo.Price ?? 0);
                                if (budgetMaterailsInfo.StorageQuantity != 0)
                                {
                                    budgetMaterailsInfo.StoragePrice = (budgetMaterailsInfo.StorageTotalPrice ?? 0) / (budgetMaterailsInfo.StorageQuantity ?? 0);
                                }
                                budgetMaterailsList.Add(budgetMaterailsInfo);
                            }
                        }
                        else
                        {
                            var budgetMaterailsId = item.ProjectConstructionBudgetMaterialsId;
                            var budgetMaterailsInfo = budgetBll.GetProject_ConstructionBudgetMaterialsEntity(budgetMaterailsId);
                            if (budgetMaterailsInfo == null) continue;
                            budgetMaterailsInfo.StorageQuantity = (budgetMaterailsInfo.StorageQuantity ?? 0) + (item.Quantity ?? 0);
                            budgetMaterailsInfo.StorageTotalPrice = (item.Quantity ?? 0) * (orderDetailInfo.Price ?? 0);
                            if (budgetMaterailsInfo.StorageQuantity != 0)
                            {
                                budgetMaterailsInfo.StoragePrice = (budgetMaterailsInfo.StorageTotalPrice ?? 0) / (budgetMaterailsInfo.StorageQuantity ?? 0);
                            }
                            budgetMaterailsList.Add(budgetMaterailsInfo);

                            Project_ConstructionBudgetMaterials.ID = item.ProjectConstructionBudgetMaterialsId;
                            Project_ConstructionBudgetMaterials.StorageQuantity = budgetMaterailsInfo.StorageQuantity;

                            Project_ConstructionBudgetMaterialsList.Add(Project_ConstructionBudgetMaterials);

                            list2.Add(item.ProjectConstructionBudgetMaterialsId);
                        }
                    }

                    storageLocaleMatesReceiptService.Audit("Audit", keyValue, orderDetailList, contractDetailList, budgetMaterailsList);
                }


            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UnAudit(string keyValue)
        {
            try
            {
                var details = storageLocaleMatesReceiptService.GetStorage_LocalematesReceiptDetailsList(keyValue);
                if (details.Any())
                {
                    PurchaseOrderIBLL orderBll = new PurchaseOrderBLL();
                    PurchaseContractIBLL contractBll = new PurchaseContractBLL();
                    ProjectConstructionBudgetIBLL budgetBll = new ProjectConstructionBudgetBLL();

                    var orderDetailList = new List<Purchase_OrderDetailsEntity>();
                    var contractDetailList = new List<Purchase_ContractDetailsEntity>();
                    var budgetMaterailsList = new List<Project_ConstructionBudgetMaterialsEntity>();

                    var purchase_ContractDetailsList = new List<Purchase_ContractDetails>();
                    Purchase_ContractDetails purchase_ContractDetails = new Purchase_ContractDetails();
                    List<string> list1 = new List<string>();

                    var Project_ConstructionBudgetMaterialsList = new List<Project_ConstructionBudgetMaterials>();
                    Project_ConstructionBudgetMaterials Project_ConstructionBudgetMaterials = new Project_ConstructionBudgetMaterials();
                    List<string> list2 = new List<string>();


                    foreach (var item in details)
                    {
                        var orderDetailId = item.PurchaseOrderDetailsID;
                        var orderDetailInfo = orderBll.GetPurchase_OrderDetailsEntity(orderDetailId);
                        orderDetailInfo.InStorage = (orderDetailInfo.InStorage ?? 0) - (item.Quantity ?? 0);
                        orderDetailInfo.SHWSP = (orderDetailInfo.SHWSP ?? 0) - (item.Quantity ?? 0);
                        orderDetailList.Add(orderDetailInfo);




                        //var contractDetailId = item.PurchaseContractDetailsId;
                        //var contractDetailInfo = contractBll.GetPurchase_ContractDetailsEntity(contractDetailId);
                        //contractDetailInfo.TotalInStorageQuantity = (contractDetailInfo.TotalInStorageQuantity ?? 0) - (item.Quantity ?? 0);
                        //contractDetailList.Add(contractDetailInfo);

                        //var budgetMaterailsId = item.ProjectConstructionBudgetMaterialsId;
                        //var budgetMaterailsInfo = budgetBll.GetProject_ConstructionBudgetMaterialsEntity(budgetMaterailsId);
                        //if (budgetMaterailsInfo == null) continue;
                        //budgetMaterailsInfo.StorageQuantity = (budgetMaterailsInfo.StorageQuantity ?? 0) - (item.Quantity ?? 0);
                        //budgetMaterailsInfo.StorageTotalPrice = (item.Quantity ?? 0) * (orderDetailInfo.Price ?? 0);
                        //if (budgetMaterailsInfo.StorageQuantity != 0)
                        //{
                        //    budgetMaterailsInfo.StoragePrice = (budgetMaterailsInfo.StorageTotalPrice ?? 0) / (budgetMaterailsInfo.StorageQuantity ?? 0);
                        //}
                        //budgetMaterailsList.Add(budgetMaterailsInfo);

                        if (list1.Contains(item.PurchaseContractDetailsId))
                        {
                            var purchase_ContractDetailsList1 = purchase_ContractDetailsList.FindAll(i => i.ID == item.PurchaseContractDetailsId);

                            foreach (var a in purchase_ContractDetailsList1)
                            {
                                var contractDetailId = item.PurchaseContractDetailsId;
                                var contractDetailInfo = contractBll.GetPurchase_ContractDetailsEntity(contractDetailId);
                                for (var b = 0; b < contractDetailList.Count; b++)
                                {
                                    var bID = contractDetailList[b].ID;

                                    if (bID == a.ID)
                                    {
                                        contractDetailList.RemoveAt(b);
                                    }
                                }
                                contractDetailInfo.TotalInStorageQuantity = (a.TotalInStorageQuantity ?? 0) - (item.Quantity ?? 0);
                                contractDetailList.Add(contractDetailInfo);
                            }
                        }
                        else
                        {
                            var contractDetailId = item.PurchaseContractDetailsId;
                            var contractDetailInfo = contractBll.GetPurchase_ContractDetailsEntity(contractDetailId);
                            contractDetailInfo.TotalInStorageQuantity = (contractDetailInfo.TotalInStorageQuantity ?? 0) - (item.Quantity ?? 0);
                            contractDetailList.Add(contractDetailInfo);

                            purchase_ContractDetails.ID = item.PurchaseContractDetailsId;
                            purchase_ContractDetails.TotalInStorageQuantity = contractDetailInfo.TotalInStorageQuantity;
                            purchase_ContractDetailsList.Add(purchase_ContractDetails);
                            list1.Add(item.PurchaseContractDetailsId);
                        }

                        if (list2.Contains(item.ProjectConstructionBudgetMaterialsId))
                        {
                            var ProjectConstructionBudgetMaterialsId1 = Project_ConstructionBudgetMaterialsList.FindAll(i => i.ID == item.ProjectConstructionBudgetMaterialsId);

                            foreach (var a in ProjectConstructionBudgetMaterialsId1)
                            {
                                var budgetMaterailsId = item.ProjectConstructionBudgetMaterialsId;
                                var budgetMaterailsInfo = budgetBll.GetProject_ConstructionBudgetMaterialsEntity(budgetMaterailsId);
                                for (var b = 0; b < Project_ConstructionBudgetMaterialsList.Count; b++)
                                {
                                    var bID = Project_ConstructionBudgetMaterialsList[b].ID;

                                    if (bID == a.ID)
                                    {
                                        budgetMaterailsList.RemoveAt(b);
                                    }
                                }

                                if (budgetMaterailsInfo == null) continue;
                                budgetMaterailsInfo.StorageQuantity = (a.StorageQuantity ?? 0) - (item.Quantity ?? 0);
                                budgetMaterailsInfo.StorageTotalPrice = (budgetMaterailsInfo.StorageQuantity ?? 0) * (orderDetailInfo.Price ?? 0);
                                if (budgetMaterailsInfo.StorageQuantity != 0)
                                {
                                    budgetMaterailsInfo.StoragePrice = (budgetMaterailsInfo.StorageTotalPrice ?? 0) / (budgetMaterailsInfo.StorageQuantity ?? 0);
                                }
                                budgetMaterailsList.Add(budgetMaterailsInfo);
                            }
                        }
                        else
                        {
                            var budgetMaterailsId = item.ProjectConstructionBudgetMaterialsId;
                            var budgetMaterailsInfo = budgetBll.GetProject_ConstructionBudgetMaterialsEntity(budgetMaterailsId);
                            if (budgetMaterailsInfo == null) continue;
                            budgetMaterailsInfo.StorageQuantity = (budgetMaterailsInfo.StorageQuantity ?? 0) - (item.Quantity ?? 0);
                            budgetMaterailsInfo.StorageTotalPrice = (item.Quantity ?? 0) * (orderDetailInfo.Price ?? 0);
                            if (budgetMaterailsInfo.StorageQuantity != 0)
                            {
                                budgetMaterailsInfo.StoragePrice = (budgetMaterailsInfo.StorageTotalPrice ?? 0) / (budgetMaterailsInfo.StorageQuantity ?? 0);
                            }
                            budgetMaterailsList.Add(budgetMaterailsInfo);

                            Project_ConstructionBudgetMaterials.ID = item.ProjectConstructionBudgetMaterialsId;
                            Project_ConstructionBudgetMaterials.StorageQuantity = budgetMaterailsInfo.StorageQuantity;

                            Project_ConstructionBudgetMaterialsList.Add(Project_ConstructionBudgetMaterials);

                            list2.Add(item.ProjectConstructionBudgetMaterialsId);
                        }
                    }

                    storageLocaleMatesReceiptService.Audit("UnAudit", keyValue, orderDetailList, contractDetailList, budgetMaterailsList);
                }

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
