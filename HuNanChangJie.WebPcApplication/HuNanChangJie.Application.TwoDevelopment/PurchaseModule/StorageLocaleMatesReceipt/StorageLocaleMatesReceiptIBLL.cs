﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-19 09:55
    /// 描 述：现场收货单
    /// </summary>
    public interface StorageLocaleMatesReceiptIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Storage_LocaleMatesReceiptEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Storage_LocalematesReceiptDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Storage_LocalematesReceiptDetailsEntity> GetStorage_LocalematesReceiptDetailsList(string keyValue);

        /// <summary>
        /// 获取Storage_LocaleMatesReceipt表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Storage_LocaleMatesReceiptEntity GetStorage_LocaleMatesReceiptEntity(string keyValue);
        /// <summary>
        /// 获取Storage_LocalematesReceiptDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Storage_LocalematesReceiptDetailsEntity GetStorage_LocalematesReceiptDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Storage_LocaleMatesReceiptEntity entity,List<Storage_LocalematesReceiptDetailsEntity> storage_LocalematesReceiptDetailsList,string deleteList,string type);
        void Audit(string keyValue);
        void UnAudit(string keyValue);
        #endregion

    }
}
