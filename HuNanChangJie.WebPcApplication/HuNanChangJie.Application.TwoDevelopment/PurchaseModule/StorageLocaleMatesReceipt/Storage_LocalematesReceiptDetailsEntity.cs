﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-19 09:55
    /// 描 述：现场收货单
    /// </summary>
    public class Storage_LocalematesReceiptDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 现场收货单Id
        /// </summary>
        [Column("STORAGELOCALEMATESRECEIPTID")]
        public string StorageLocaleMatesReceiptId { get; set; }
        /// <summary>
        /// 采购合同ID
        /// </summary>
        [Column("PURCHASECONTRACTID")]
        public string PurchaseContractId { get; set; }
        /// <summary>
        /// 采购订单ID
        /// </summary>
        [Column("PURCHASEORDERID")]
        public string PurchaseOrderId { get; set; }
        /// <summary>
        /// 采购详情Id
        /// </summary>
        [Column("PURCHASEORDERDETAILSID")]
        public string PurchaseOrderDetailsID { get; set; }
        /// <summary>
        /// 施工预算材料ID
        /// </summary>
        [Column("PROJECTCONSTRUCTIONBUDGETMATERIALSID")]
        public string ProjectConstructionBudgetMaterialsId { get; set; }
        /// <summary>
        /// 本次收货数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }

        /// <summary>
        /// 采购合同详情ID
        /// </summary>
        public string PurchaseContractDetailsId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }


        [Column("CODE")]
        public string Code { get; set; }

        [Column("NAME")]
        public string Name { get; set; }

        [Column("BRAND")]
        public string Brand { get; set; }

        [Column("MODELNUMBER")]
        public string ModelNumber { get; set; }

        [Column("UNIT")]
        public string Unit { get; set; }

        //采购数量
        [Column("ORDERQUANTITY")]
        public decimal? OrderQuantity { get; set; }

        //收货数量
        [Column("INSTORAGE")]
        public decimal? InStorage { get; set; }


        [Column("ProjectName")]
        public string ProjectName { get; set; }

        [Column("ProjectID")]
        public string ProjectID { get; set; }

        [Column("SUPPLIERNAME")]
        public string SupplierName { get; set; }

        
        [Column("ORDERCODE")]
        public string OrderCode { get; set; }

        [Column("FK_BASEMATERIALSID")]
        public string Fk_BaseMaterialsId { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

