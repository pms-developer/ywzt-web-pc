﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Application.TwoDevelopment.Extend;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-06 10:09
    /// 描 述：采购订单付款申请
    /// </summary>
    public class Purchase_OrderPayService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderPayEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.BaesSupplierId,
                t.OperatorId,
                t.OperatorDepartmentId,
                t.pinyin,
                t.ApplyDate,
                t.PlanPayDate,
                t.PayAmount,
                t.PayCompanyId,
                t.HasInvoice,
                t.Remark,
                t.AuditStatus,
                t.Workflow_ID,
                t.AccountName,
                t.BankName,
                t.BankAccount,
                t.CreationDate,
                t.PayModel,
                t.ProjectID,
                isnull(counts,0) printcount,
                cmp.F_FullName as PayCompanyName,
                ");
                strSql.Append("  us.F_RealName as OperatorName, ");
                strSql.Append("  dept.F_FullName as OperatorDepartmentName, ");
                strSql.Append("  sup.Name as SupplierName ");

                strSql.Append("  FROM Purchase_OrderPay t left join (select infoid,counts from Base_PrintStatistics where tablename = 'Purchase_OrderPay') i on t.id = i.infoid");
                strSql.Append("  left join Base_User as us on t.OperatorId=us.F_UserId");
                strSql.Append("  left join Base_Department as dept on t.OperatorDepartmentId=dept.F_DepartmentId");
                strSql.Append(" left join Base_Supplier as sup on t.BaesSupplierId=sup.ID ");
                strSql.Append(" left join Base_Company as cmp on t.PayCompanyId=cmp.F_CompanyId ");

                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }

                if (!queryParam["BaesSupplierId"].IsEmpty())
                {
                    dp.Add("BaesSupplierId", queryParam["BaesSupplierId"].ToString(), DbType.String);
                    strSql.Append(" AND t.BaesSupplierId = @BaesSupplierId ");
                }
                if (!queryParam["PayModel"].IsEmpty())
                {
                    dp.Add("PayModel", queryParam["PayModel"].ToString(), DbType.String);
                    strSql.Append(" AND t.PayModel = @PayModel ");
                }
                if (!queryParam["PayCompanyId"].IsEmpty() && queryParam["PayCompanyId"].ToString() != "-1")
                {
                    dp.Add("PayCompanyId", queryParam["PayCompanyId"].ToString(), DbType.String);
                    strSql.Append(" AND PayCompanyId = @PayCompanyId");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }

                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.ApplyDate >= @startTime AND t.ApplyDate <= @endTime ) ");
                }

                if (!queryParam["Remark"].IsEmpty())
                {
                    dp.Add("Remark", $"%{ queryParam["Remark"].ToString()}%", DbType.String);
                    strSql.Append(" AND t.Remark like @Remark ");
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (pagination !=null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "Code";
                    pagination.sord = "DESC";
                }

                var list =this.BaseRepository().FindList<Purchase_OrderPayEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderPayDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderPayDetailsEntity> GetPurchase_OrderPayDetailsList(string keyValue)
        {
            try
            {
                var sql = @"select 
                          a.*,b.name 
                        PurchaseContractName,
                        b.code PurchaseContractCode,
                        b.jia,
                        b.yi,
                        b.signamount,
                        b.AccountPaid,
                        b.SettlementAmount,
                        b.YdfkBiLi,
                        c.ProjectName
                        from Purchase_OrderPayDetails a left join 
                        Purchase_Contract b on a.PurchaseContractId = b.id left join
                        Base_CJ_Project c on b.ProjectId=c.ID
                        where a.Fk_OrderPayId = '" + keyValue + "'";
                var list= this.BaseRepository().FindList<Purchase_OrderPayDetailsEntity>(sql);
                return list;
                //var query = from item in list select item;
                //return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderPay表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderPayEntity GetPurchase_OrderPayEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_OrderPayEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderPayDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderPayDetailsEntity GetPurchase_OrderPayDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_OrderPayDetailsEntity>(t=>t.Fk_OrderPayId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Purchase_OrderPayEntity purchase_OrderPayEntity = db.FindEntity<Purchase_OrderPayEntity>(keyValue);
                if (purchase_OrderPayEntity.IsClose==true)
                {
                    db.Commit();
                    return new Util.Common.OperateResultEntity() { Success = false,Message="单据已关闭，不能进行反审核" };
                }
                if (purchase_OrderPayEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                purchase_OrderPayEntity.AuditStatus = "4";
                db.Update<Purchase_OrderPayEntity>(purchase_OrderPayEntity);

                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(m=>m.Name== "PurchaseOrderHandleNode");
                if (config != null && config.Value == 0)
                {
                    Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(m => m.Name == "材料采购");
                    IEnumerable<Purchase_OrderPayDetailsEntity> purchase_OrderPayDetailsEntityList = db.FindList<Purchase_OrderPayDetailsEntity>(m => m.Fk_OrderPayId == keyValue);
                    foreach (var item in purchase_OrderPayDetailsEntityList)
                    {
                        Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(item.PurchaseContractId);
                        if (!purchase_ContractEntity.AccountPayable.HasValue)
                            purchase_ContractEntity.AccountPayable = 0;
                        purchase_ContractEntity.AccountPayable -= item.PayAmount;

                        db.Update<Purchase_ContractEntity>(purchase_ContractEntity);

                        if (base_ContractTypeEntity != null)
                        {
                            var subjectInfo = new ProjectSubjectBll().GetEntity(base_ContractTypeEntity.BaseSubjectId, purchase_ContractEntity.ProjectID);
                            if (subjectInfo != null)
                             //   resultList.Add(new OperateResultEntity() { Success = false, Message = "未找到对应的科目" });
                            { 
                            subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) - (item.PayAmount ?? 0);

                            //decimal afterTax = item.PayAmount.Value - (item.PayAmount.Value * purchase_ContractEntity.TaxRage.Value / 100);
                            //subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;

                            db.Update<ProjectSubjectEntity>(subjectInfo);
                            }
                        }
                    }
                }


                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Purchase_OrderPayEntity purchase_OrderPayEntity = db.FindEntity<Purchase_OrderPayEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (purchase_OrderPayEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                purchase_OrderPayEntity.AuditStatus = "2";
                db.Update<Purchase_OrderPayEntity>(purchase_OrderPayEntity);

                Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(m => m.Name == "材料采购");

                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(m => m.Name == "PurchaseOrderHandleNode");
                if (config != null && config.Value == 0)
                {
                    IEnumerable<Purchase_OrderPayDetailsEntity> purchase_OrderPayDetailsEntityList = db.FindList<Purchase_OrderPayDetailsEntity>(m => m.Fk_OrderPayId == keyValue);
                    foreach (var item in purchase_OrderPayDetailsEntityList)
                    {
                        Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(item.PurchaseContractId);
                        if (!purchase_ContractEntity.AccountPayable.HasValue)
                            purchase_ContractEntity.AccountPayable = 0;
                        purchase_ContractEntity.AccountPayable += item.PayAmount;

                        db.Update<Purchase_ContractEntity>(purchase_ContractEntity);

                        if (base_ContractTypeEntity != null)
                        {
                            var subjectInfo = new ProjectSubjectBll().GetEntity(base_ContractTypeEntity.BaseSubjectId, purchase_ContractEntity.ProjectID);
                            if (subjectInfo != null)
                            //    resultList.Add(new OperateResultEntity() { Success = false, Message = "未找到对应的科目" });
                            //else
                            {
                                subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) + (item.PayAmount ?? 0);
                                //decimal afterTax = item.PayAmount.Value - (item.PayAmount.Value * purchase_ContractEntity.TaxRage.Value / 100);
                                //subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;
                                db.Update<ProjectSubjectEntity>(subjectInfo);
                            }
                            
                        }
                        //SettlementAmount
                    }
                }
                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var purchase_OrderPayEntity = GetPurchase_OrderPayEntity(keyValue); 
                db.Delete<Purchase_OrderPayEntity>(t=>t.ID == keyValue);
                db.Delete<Purchase_OrderPayDetailsEntity>(t=>t.Fk_OrderPayId == purchase_OrderPayEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_OrderPayEntity entity,List<Purchase_OrderPayDetailsEntity> purchase_OrderPayDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var purchase_OrderPayEntityTmp = GetPurchase_OrderPayEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Purchase_OrderPayDetailsUpdateList= purchase_OrderPayDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Purchase_OrderPayDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Purchase_OrderPayDetailsInserList= purchase_OrderPayDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Purchase_OrderPayDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.Fk_OrderPayId = purchase_OrderPayEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Purchase_OrderPayDetailsEntity item in purchase_OrderPayDetailsList)
                    {
                        item.Fk_OrderPayId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
