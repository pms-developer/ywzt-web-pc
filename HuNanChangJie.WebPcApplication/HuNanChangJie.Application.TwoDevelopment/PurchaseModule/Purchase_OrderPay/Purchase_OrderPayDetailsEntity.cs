﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-06 10:09
    /// 描 述：采购订单付款申请
    /// </summary>
    public class Purchase_OrderPayDetailsEntity:BaseEntity 
    {
        #region  实体成员

        /// <summary>
        /// 罚款金额
        /// </summary>
        [Column("FINEEXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? FineExpend { get; set; }

        /// <summary>
        /// 其它收入
        /// </summary>
        [Column("OTHERINCOME")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherIncome { get; set; }
        /// <summary>
        /// 其它支出
        /// </summary>
        [Column("OTHEREXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherExpend { get; set; }
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 采购合同Id
        /// </summary>
        [Column("PURCHASECONTRACTID")]
        public string PurchaseContractId { get; set; }
        /// <summary>
        /// 付款金额
        /// </summary>
        [Column("PAYAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? PayAmount { get; set; }
        /// <summary>
        /// 结算说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// Fk_OrderPayId
        /// </summary>
        [Column("FK_ORDERPAYID")]
        public string Fk_OrderPayId { get; set; }

        [NotMapped]
        public string PurchaseContractCode { get; set; }

        [NotMapped]
        public string PurchaseContractName { get; set; }


        [NotMapped]
        public string Jia { get; set; }

        [NotMapped]
        public string Yi { get; set; }

        [NotMapped]
        public decimal? SignAmount { get; set; }

        [NotMapped]
        public string ProjectName { get; set; }

        /// <summary>
        /// 累计结算金额
        /// </summary>
        [NotMapped]
        public decimal? SettlementAmount { get; set; }
        /// <summary>
        /// 已付款金额
        /// </summary>
        [NotMapped]
        public decimal? AccountPaid { get; set; }

        /// <summary>
        /// 实际付款比例(%)
        /// </summary>
        [NotMapped]
        public decimal? Sjfkbl
        {
            get
            {
                if (SettlementAmount == null) return null;
                if (SettlementAmount == 0) return null;
                return (AccountPaid ?? 0) / SettlementAmount * 100;
            }
        }

        /// <summary>
        /// 约定付款比例(%)
        /// </summary>
        [NotMapped]
        public decimal? YdfkBiLi { get; set; }

        /// <summary>
        /// 约定应付账款
        /// </summary>
        [NotMapped]
        public decimal? YdyfKuan
        {
            get
            {
                if (YdfkBiLi == null) return null;
                if (YdfkBiLi <= 0) return null;
                return (SettlementAmount ?? 0) * YdfkBiLi / 100 - (AccountPaid ?? 0);
            }
        }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

