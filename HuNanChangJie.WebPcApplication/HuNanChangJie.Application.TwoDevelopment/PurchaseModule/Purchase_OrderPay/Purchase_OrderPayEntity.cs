﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-06 10:09
    /// 描 述：采购订单付款申请
    /// </summary>
    public class Purchase_OrderPayEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 项目模式
        /// </summary>
        [Column("PROJECTMODE")]
        public string ProjectMode { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 经办人ID
        /// </summary>
        [Column("OPERATORID")]
        public string OperatorId { get; set; }
        /// <summary>
        /// 经办部门ID
        /// </summary>
        [Column("OPERATORDEPARTMENTID")]
        public string OperatorDepartmentId { get; set; }
        /// <summary>
        /// 结算说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 供应商Id
        /// </summary>
        [Column("BAESSUPPLIERID")]
        public string BaesSupplierId { get; set; }
        /// <summary>
        /// BankId
        /// </summary>
        [Column("BANKID")]
        public string BankId { get; set; }
        /// <summary>
        /// 申请日期
        /// </summary>
        [Column("APPLYDATE")]
        public DateTime? ApplyDate { get; set; }
        /// <summary>
        /// 计划付款日期
        /// </summary>
        [Column("PLANPAYDATE")]
        public DateTime? PlanPayDate { get; set; }
        /// <summary>
        /// 付款金额
        /// </summary>
        [Column("PAYAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? PayAmount { get; set; }
        /// <summary>
        /// 付款单位
        /// </summary>
        [Column("PAYCOMPANYID")]
        public string PayCompanyId { get; set; }
        /// <summary>
        /// 是否有发票
        /// </summary>
        [Column("HASINVOICE")]
        public string HasInvoice { get; set; }

        [Column("ACCOUNTNAME")]
        public string AccountName { get; set; }

        [Column("BANKNAME")]
        public string BankName { get; set; }

        [Column("BANKACCOUNT")]
        public string BankAccount { get; set; }

        /// <summary>
        /// 付款模式
        /// </summary>
        public string PayModel { get; set; }

        /// <summary>
        /// 是否已关闭
        /// </summary>
        public bool? IsClose { get; set; } = false;

        [NotMapped]
        public int printcount { get; set; }
        
        /// <summary>
        /// 批准金额
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? RatifyAmount { get; set; }

        /// <summary>
        /// 已付款金额
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? AccountPaidAmount { get; set; }

        /// <summary>
        /// 是否完成付款
        /// </summary>
        public bool? IsFinish { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段

        /// <summary>
        /// 经办人
        /// </summary>
        [NotMapped]
        public string OperatorName { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// 付款单位（甲方）
        /// </summary>
        [NotMapped]
        public string PayCompanyName { get; set; }

        /// <summary>
        /// 经办部门
        /// </summary>
        [NotMapped]
        public string OperatorDepartmentName { get; set; }
        #endregion
    }
}

