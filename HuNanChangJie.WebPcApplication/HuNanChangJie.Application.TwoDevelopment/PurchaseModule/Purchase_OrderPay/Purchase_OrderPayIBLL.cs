﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-06 10:09
    /// 描 述：采购订单付款申请
    /// </summary>
    public interface Purchase_OrderPayIBLL
    {
        #region  获取数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Purchase_OrderPayEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Purchase_OrderPayDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Purchase_OrderPayDetailsEntity> GetPurchase_OrderPayDetailsList(string keyValue);
        /// <summary>
        /// 获取Purchase_OrderPay表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_OrderPayEntity GetPurchase_OrderPayEntity(string keyValue);
        /// <summary>
        /// 获取Purchase_OrderPayDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_OrderPayDetailsEntity GetPurchase_OrderPayDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Purchase_OrderPayEntity entity,List<Purchase_OrderPayDetailsEntity> purchase_OrderPayDetailsList,string deleteList,string type);
        #endregion

    }
}
