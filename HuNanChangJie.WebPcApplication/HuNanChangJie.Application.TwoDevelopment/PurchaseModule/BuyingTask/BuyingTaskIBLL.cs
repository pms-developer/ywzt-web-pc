﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangJie.Util;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule.BuyingTask
{
    public interface BuyingTaskIBLL
    {
        DataTable GetPageList(XqPagination paginationobj, string queryJson);
    }
}
