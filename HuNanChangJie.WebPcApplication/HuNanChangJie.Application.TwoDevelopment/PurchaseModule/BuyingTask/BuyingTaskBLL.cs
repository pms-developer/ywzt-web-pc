﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangJie.Util;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule.BuyingTask
{
    public class BuyingTaskBLL: BuyingTaskIBLL
    {
        private BuyingTaskService service = new BuyingTaskService();

        public DataTable GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return service.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
