﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule.BuyingTask
{
    public class BuyingTaskService : RepositoryFactory
    {
        public DataTable GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                var prefix = BaseRepository().DataBaseType.ToString();
                var sql = ConfigurationManager.AppSettings[$"{prefix}BuyingTask"].ToString();
                strSql.Append(sql);
                strSql.Append("  WHERE CurrentAllowQuantity>0 and AuditStatus=2 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" And ProjectId=@ProjectId");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND ApplyCode Like @Code ");
                }

                if (!queryParam["MaterialsName"].IsEmpty())
                {
                    dp.Add("MaterialsName", "%" + queryParam["MaterialsName"].ToString() + "%", DbType.String);
                    strSql.Append("    AND ( name like @MaterialsName or code like @MaterialsName)");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( ApplyDate >= @startTime AND ApplyDate<= @endTime ) ");
                }


                //2020-11-02 取消项目角色采购员限制
                //if (LoginUserInfo.UserInfo.isSystem == false)
                //{
                //    dp.Add("UserId", LoginUserInfo.UserInfo.userId, DbType.String);
                //    strSql.Append(" And UserId=@UserId");
                //}
                var datatalbe = this.BaseRepository().FindTable(strSql.ToString(), dp, pagination);
                var dv = datatalbe.DefaultView;
                var query = from item in datatalbe.AsEnumerable() orderby (item["creationdate"].ToDate()) descending
                            select item;
                dv = query.AsDataView();

                return dv.ToTable();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}
