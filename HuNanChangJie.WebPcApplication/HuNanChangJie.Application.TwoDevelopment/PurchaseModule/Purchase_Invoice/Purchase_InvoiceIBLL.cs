﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-03 15:51
    /// 描 述：采购发票
    /// </summary>
    public interface Purchase_InvoiceIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Purchase_InvoiceEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Purchase_InvoiceDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Purchase_InvoiceDetailsEntity> GetPurchase_InvoiceDetailsList(string keyValue);
        /// <summary>
        /// 获取Purchase_Invoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_InvoiceEntity GetPurchase_InvoiceEntity(string keyValue);
        /// <summary>
        /// 获取Purchase_InvoiceDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_InvoiceDetailsEntity GetPurchase_InvoiceDetailsEntity(string keyValue);
        #endregion

        #region  提交数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Purchase_InvoiceEntity entity,List<Purchase_InvoiceDetailsEntity> purchase_InvoiceDetailsList,string deleteList,string type);
        #endregion

    }
}
