﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-03 15:51
    /// 描 述：采购发票
    /// </summary>
    public class Purchase_InvoiceDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 采购发票ID
        /// </summary>
        [Column("PURCHASEINVOICEID")]
        public string PurchaseInvoiceId { get; set; }
        /// <summary>
        /// 采购合同ID
        /// </summary>
        [Column("PURCHASECONTRACTID")]
        public string PurchaseContractId { get; set; }
        /// <summary>
        /// 采购订单ID
        /// </summary>
        [Column("PURCHASEORDERID")]
        public string PurchaseOrderId { get; set; }

        /// <summary>
        /// 订单详情ID
        /// </summary>
        public string PurchaseOrderDetailsId { get; set; }

        /// <summary>
        /// 合同详情ID
        /// </summary>
        public string PurchaseContractDetailsId { get; set; }
        /// <summary>
        /// 本次开票数据
        /// </summary>
        [Column("INVOICEQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? InvoiceQuantity { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        [Column("TAXAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxAmount { get; set; }
        /// <summary>
        /// 不含税单价
        /// </summary>
        [Column("AFTERTAXPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterTaxPrice { get; set; }
        /// <summary>
        /// 不含税金额
        /// </summary>
        [Column("AFTERTAXAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterTaxAmount { get; set; }
        /// <summary>
        /// 含税单价
        /// </summary>
        [Column("TAXINCLUSIVEPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxInclusivePrice { get; set; }
        /// <summary>
        /// 含税金额
        /// </summary>
        [Column("TAXINCLUSIVEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxInclusiveAmount { get; set; }
        /// <summary>
        /// 计划到货日期
        /// </summary>
        [Column("PLANDATE")]
        public DateTime? PlanDate { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region
        /// <summary>
        /// 订单编号
        /// </summary>
        [NotMapped]
        public string OrderCode { get; set; }

        /// <summary>
        /// 税率
        /// </summary>
        [NotMapped]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRate { get; set; }

        /// <summary>
        /// 合同名称
        /// </summary>
        [NotMapped]
        public string ContractName { get; set; }

        /// <summary>
        /// 材料编码
        /// </summary>
        [NotMapped]
        public string MaterialsCode { get; set; }

        /// <summary>
        /// 材料名称
        /// </summary>
        [NotMapped]
        public string MaterialsName { get; set; }

        /// <summary>
        /// 品牌
        /// </summary>
        [NotMapped]
        public string Brand { get; set; }

        /// <summary>
        /// 规格型号
        /// </summary>
        [NotMapped]
        public string ModelNumber { get; set; }

        /// <summary>
        /// 计量单位
        /// </summary>
        [NotMapped]
        public string Unit { get; set; }

        /// <summary>
        /// 预算单价
        /// </summary>
        [NotMapped]
        [DecimalPrecision(18, 4)]
        public decimal? BudgetPrice { get; set; }

        /// <summary>
        /// 订单数量
        /// </summary>
        [NotMapped]
        [DecimalPrecision(18, 4)]
        public decimal? OrderQuantity { get; set; }

        /// <summary>
        /// 收票数量
        /// </summary>
        [NotMapped]
        [DecimalPrecision(18, 4)]
        public decimal? ReceivedInvoiceQuantity { get; set; }

        /// <summary>
        /// 未收票数量
        /// </summary>
        [NotMapped]
        [DecimalPrecision(18, 4)]
        public decimal? UnreceivedInvoiceQuantity 
        { 
            get 
            {
                return (OrderQuantity ?? 0) - (ReceivedInvoiceQuantity ?? 0);
            } 
        }

        #endregion
    }
}

