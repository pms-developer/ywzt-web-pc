﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.SqlScript.Purchase_Invoice;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-03 15:51
    /// 描 述：采购发票
    /// </summary>
    public class Purchase_InvoiceService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_InvoiceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT *,t.InvoiceAmount-ISNULL(t.AccountingInvoiceAmount,0) as balance FROM Purchase_Invoice t ");
                strSql.Append(" WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["InvoiceCode"].IsEmpty())
                {
                    dp.Add("InvoiceCode", "%" + queryParam["InvoiceCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.InvoiceCode Like @InvoiceCode ");
                }
                if (!queryParam["BaesSupplierId"].IsEmpty())
                {
                    dp.Add("BaesSupplierId", queryParam["BaesSupplierId"].ToString(), DbType.String);
                    strSql.Append(" AND t.BaesSupplierId = @BaesSupplierId ");
                }

                if (!queryParam["Amount"].IsEmpty())
                {
                    strSql.Append(" AND (t.InvoiceAmount>t.AccountingInvoiceAmount or t.AccountingInvoiceAmount is null)");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Purchase_InvoiceEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }




        /// <summary>
        /// 获取Purchase_InvoiceDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_InvoiceDetailsEntity> GetPurchase_InvoiceDetailsList(string keyValue)
        {
            try
            {
                var sql = new PurchaseInvoiceSqlFactory(BaseRepository()).GetPurchaseInvoiceSql.GetDetailsSql;
                sql += " and t.PurchaseInvoiceId=@keyValue";
                var list = this.BaseRepository().FindList<Purchase_InvoiceDetailsEntity>(sql, new { keyValue });
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_Invoice表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_InvoiceEntity GetPurchase_InvoiceEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_InvoiceEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_InvoiceDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_InvoiceDetailsEntity GetPurchase_InvoiceDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_InvoiceDetailsEntity>(t => t.PurchaseInvoiceId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Purchase_InvoiceEntity purchase_InvoiceEntity = db.FindEntity<Purchase_InvoiceEntity>(keyValue);
                if (purchase_InvoiceEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                purchase_InvoiceEntity.AuditStatus = "4";
                db.Update<Purchase_InvoiceEntity>(purchase_InvoiceEntity);


                var invoiceDetailsList = db.FindList<Purchase_InvoiceDetailsEntity>(i => i.PurchaseInvoiceId == keyValue);
                var orderDetailsIdList = invoiceDetailsList.Select(i => i.PurchaseOrderDetailsId);
                var orderDetailsList = db.FindList<Purchase_OrderDetailsEntity>(i => orderDetailsIdList.Contains(i.ID));
                foreach (var invoice in invoiceDetailsList)
                {
                    var order = orderDetailsList.FirstOrDefault(i => i.ID == invoice.PurchaseOrderDetailsId);
                    if (order == null) continue;
                    order.ReceivedInvoiceQuantity = (order.ReceivedInvoiceQuantity ?? 0) - (invoice.InvoiceQuantity ?? 0);
                    order.SHWSP = (order.SHWSP ?? 0) + (invoice.InvoiceQuantity ?? 0);//收货时该值增加，开票时该值减少
                    db.Update(order);
                }

                Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(purchase_InvoiceEntity.PurchaseContractId);
                if (purchase_ContractEntity != null)
                {
                    if (!purchase_ContractEntity.ReceivedInvoiceAmount.HasValue)
                        purchase_ContractEntity.ReceivedInvoiceAmount = 0;
                    purchase_ContractEntity.ReceivedInvoiceAmount -= purchase_InvoiceEntity.InvoiceAmount;
                }


                db.Update<Purchase_ContractEntity>(purchase_ContractEntity);
                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            UserInfo userInfo = LoginUserInfo.Get();

            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Purchase_InvoiceEntity purchase_InvoiceEntity = db.FindEntity<Purchase_InvoiceEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (purchase_InvoiceEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                purchase_InvoiceEntity.AuditStatus = "2";
                purchase_InvoiceEntity.Auditor_ID = userInfo.userId;
                purchase_InvoiceEntity.AuditorName = userInfo.realName;
                var invoiceDetailsList = db.FindList<Purchase_InvoiceDetailsEntity>(i => i.PurchaseInvoiceId == keyValue);
                var orderDetailsIdList = invoiceDetailsList.Select(i => i.PurchaseOrderDetailsId);
                var orderDetailsList = db.FindList<Purchase_OrderDetailsEntity>(i => orderDetailsIdList.Contains(i.ID));
                foreach (var invoice in invoiceDetailsList)
                {
                    var order = orderDetailsList.FirstOrDefault(i => i.ID == invoice.PurchaseOrderDetailsId);
                    if (order == null) continue;
                    order.ReceivedInvoiceQuantity = (order.ReceivedInvoiceQuantity ?? 0) + (invoice.InvoiceQuantity ?? 0);
                    order.SHWSP = (order.SHWSP ?? 0) - (invoice.InvoiceQuantity ?? 0);//收货时该值增加，开票时该值减少
                    db.Update(order);
                }
                db.Update<Purchase_InvoiceEntity>(purchase_InvoiceEntity);

                Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(purchase_InvoiceEntity.PurchaseContractId);
                if (purchase_ContractEntity != null)
                {
                    if (!purchase_ContractEntity.ReceivedInvoiceAmount.HasValue)
                        purchase_ContractEntity.ReceivedInvoiceAmount = 0;
                    purchase_ContractEntity.ReceivedInvoiceAmount += purchase_InvoiceEntity.InvoiceAmount;
                }
                db.Update<Purchase_ContractEntity>(purchase_ContractEntity);
                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var purchase_InvoiceEntity = GetPurchase_InvoiceEntity(keyValue);
                db.Delete<Purchase_InvoiceEntity>(t => t.ID == keyValue);
                db.Delete<Purchase_InvoiceDetailsEntity>(t => t.PurchaseInvoiceId == purchase_InvoiceEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_InvoiceEntity entity, List<Purchase_InvoiceDetailsEntity> purchase_InvoiceDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var purchase_InvoiceEntityTmp = GetPurchase_InvoiceEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Purchase_InvoiceDetailsUpdateList = purchase_InvoiceDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Purchase_InvoiceDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Purchase_InvoiceDetailsInserList = purchase_InvoiceDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Purchase_InvoiceDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.PurchaseInvoiceId = purchase_InvoiceEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Purchase_InvoiceDetailsEntity item in purchase_InvoiceDetailsList)
                    {
                        item.PurchaseInvoiceId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
