﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 19:12
    /// 描 述：采购合同
    /// </summary>
    public class PurchaseContractService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                //t.Finance_OtherPaymentID as Finance_OtherPaymentCode,
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
t.ProjectManager,(select Code from Finance_OtherPayment as fop where fop.ID=t.Finance_OtherPaymentID) as Finance_OtherPaymentCode,
t.Finance_OtherPaymentID,
                t.Code,
                t.Name,
                t.ProjectID,
                t.SignDate,
                t.Jia,
                t.Yi,
                t.YiDesc,
                t.YiCode,
t.ExternalCode,
t.AccountName,
t.BankName,
t.BankNumber,t.Ticket_Amount,t.AccountPayable,
                t.PriceType,
                t.PurchaseType,
                t.SignAmount,
                t.SortCode,
                t.Period,
                t.InvoiceType,
                t.DiscountsAmount,
                t.CreationDate,
                t.DiscountsContractAmount,
                t.AuditStatus,t.AccountPaid,isnull(fineexpend,0) FineExpend,isnull(otherexpend,0) OtherExpend,isnull(otherincome,0) OtherIncome,
                t.Workflow_ID,SettlementAmount,
                p.ProjectName,p.mode ProjectMode, 
                t.YdfkBiLi,isnull(settlementamount,0) * isnull(YdfkBiLi,0) / 100 -  isnull(fineexpend,0) - isnull(otherexpend,0) + isnull(otherincome,0) YdyfKuan,
                cmp.F_FullName as JiaCompanyName,
                sup.Name as SupplierName,
                us.F_RealName as ProjectManagerName,
                pro.ProjectName as ProName
                ");
                strSql.Append("  FROM Purchase_Contract t left join Base_CJ_Project p on t.ProjectId=p.ID ");
                strSql.Append("  left join Base_Company as cmp on t.Jia=cmp.F_CompanyId");
                strSql.Append("  left join Base_Supplier as sup on t.Yi=sup.id");
                strSql.Append("  left join Base_User as us on t.ProjectManager=us.F_UserId");
                strSql.Append("  left join Base_CJ_Project as pro on t.ProjectID=pro.Id");

                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                if (!queryParam["Yi"].IsEmpty())
                {
                    dp.Add("Yi", queryParam["Yi"].ToString(), DbType.String);
                    strSql.Append(" AND t.Yi = @Yi ");
                }
                if (!queryParam["Choose"].IsEmpty())
                {
                    strSql.Append(" AND SignAmount > AccountPaid ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "Code";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Purchase_ContractEntity>(strSql.ToString(), dp, pagination);

                //foreach (var item in list)
                //{
                //    var gcmcList = this.BaseRepository().FindList<string>($" select fop.Code from VerificationTable as vt join Finance_OtherPayment as fop on vt.Finance_OtherPaymentID=fop.ID where vt.Purchase_ContractID='{item.ID}'");
                //    foreach (var itemGcmc in gcmcList)
                //    {
                //        if (!string.IsNullOrEmpty(item.Finance_OtherPaymentCode))
                //            item.Finance_OtherPaymentCode += ",";
                //        item.Finance_OtherPaymentCode += itemGcmc;
                //    }
                //}

                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Purchase_ContractDetailsEntity> GetDetailList(IEnumerable<string> idlist)
        {
            try
            {
                return BaseRepository().FindList<Purchase_ContractDetailsEntity>(i => idlist.Contains(i.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractDetailsEntity> GetPurchase_ContractDetailsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Purchase_ContractDetailsEntity>(t => t.PurchaseContractId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_OtherPaymentEntity> GetPageList(string purchase_ContractID)
        {
            try
            {
                var strSql = new StringBuilder();

                strSql.Append(" select fop.ID,fop.Code,fop.CustomerId,fop.FundType,fop.PaymentDate,vt.Verification_Amount,fop.ProjectSubjectId,fop.BankId from VerificationTable as vt join Finance_OtherPayment as fop on");
                strSql.Append(" vt.Finance_OtherPaymentID = fop.ID where vt.Purchase_ContractID = '" + purchase_ContractID + "' ");

                var list = this.BaseRepository().FindList<Finance_OtherPaymentEntity>(strSql.ToString());
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Project_PaymentAgreement表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_PaymentAgreementEntity> GetProject_PaymentAgreementList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_PaymentAgreementEntity>(t => t.MainID == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_Contract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractEntity GetPurchase_ContractEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_ContractEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Purchase_ContractDetailsEntity> GetDetails(IEnumerable<string> contractDetailsIdList)
        {
            try
            {
                return this.BaseRepository().FindList<Purchase_ContractDetailsEntity>(i => contractDetailsIdList.Contains(i.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractDetailsEntity GetPurchase_ContractDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_ContractDetailsEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public VerificationTableEntity GetVerificationTableEntity(string purchase_ContractID)
        {
            try
            {
                return this.BaseRepository().FindEntity<VerificationTableEntity>(t => t.Purchase_ContractID == purchase_ContractID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        /// <summary>
        /// 获取Project_PaymentAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_PaymentAgreementEntity GetProject_PaymentAgreementEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_PaymentAgreementEntity>(t => t.MainID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public int GetIsExceed(string contractId)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                a.CurrentAllowQuantity-b.ContractQuantity as num from Project_PurchaseApplyDetails a 
                join Purchase_ContractDetails b on a.projectpurchaseapplyid=b.projectpurchaseapplyid
                and a.ProjectConstructionBudgetMaterialsId=b.ProjectConstructionBudgetMaterialsId
                ");
                strSql.Append("  WHERE 1=1 ");
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!contractId.IsEmpty())
                {
                    dp.Add("PurchaseContractId", contractId, DbType.String);
                    strSql.Append(" AND b.PurchaseContractId=@PurchaseContractId ");
                }
                DataTable dt = this.BaseRepository().FindTable(strSql.ToString(), dp);
                int exceedNum = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToDecimal((dt.Rows[i][0].ToString())) < 0)
                        exceedNum++;
                }
                return exceedNum;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var purchase_ContractEntity = GetPurchase_ContractEntity(keyValue);
                db.Delete<Purchase_ContractEntity>(t => t.ID == keyValue);
                db.Delete<Purchase_ContractDetailsEntity>(t => t.PurchaseContractId == purchase_ContractEntity.ID);
                db.Delete<Project_PaymentAgreementEntity>(t => t.MainID == purchase_ContractEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void UpdateRelationData(IEnumerable<Purchase_ContractDetailsEntity> details, List<Project_ConstructionBudgetMaterialsEntity> updateBudgetList, List<Project_PurchaseApplyDetailsEntity> updateApplyList, ProjectSubjectEntity projectSubjectEntity)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var flag = false;
                if (projectSubjectEntity != null)
                {
                    db.Update(projectSubjectEntity);
                    flag = true;
                }
                if (details.Count() > 0)
                {
                    db.Update(details.ToList());
                    flag = true;
                }
                if (updateBudgetList.Count > 0)
                {
                    db.Update(updateBudgetList);
                    flag = true;
                }
                if (updateApplyList.Count > 0)
                {
                    db.Update(updateApplyList);
                }
                if (flag)
                {
                    db.Commit();
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void UpdateEntity(Purchase_ContractEntity contractInfo)
        {
            try
            {
                BaseRepository().Update(contractInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal void UpdateDetails(List<Purchase_ContractDetailsEntity> updateList, List<Purchase_ContractDetailsEntity> insertList)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                var flag = false;
                if (updateList.Count > 0)
                {
                    flag = true;
                    db.Update(updateList);
                }
                if (insertList.Count > 0)
                {
                    flag = true;
                    db.Insert(insertList);
                }
                if (flag)
                {
                    db.Commit();
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_ContractEntity entity, List<Purchase_ContractDetailsEntity> purchase_ContractDetailsList, List<Project_PaymentAgreementEntity> project_PaymentAgreementList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (string.IsNullOrEmpty(entity.Yi) && (string.IsNullOrEmpty(entity.YiDesc) || string.IsNullOrEmpty(entity.YiCode)))
                {
                    throw new Exception("请选择供应商或者输入供应商名称和证件号码");
                }

                //if (string.IsNullOrEmpty(entity.Yi) && !string.IsNullOrEmpty(entity.YiDesc) && !string.IsNullOrEmpty(entity.YiDesc))
                //{
                //    Base_SupplierEntity base_SupplierEntity = db.FindEntity<Base_SupplierEntity>(m=>m.Name == entity.YiDesc);
                //    if (base_SupplierEntity == null)
                //    {
                //        string yi = Guid.NewGuid().ToString();
                //        entity.Yi = yi;
                //        base_SupplierEntity = new Base_SupplierEntity() { ID= yi, Flag= entity.YiCode, Code = new CodeRuleBLL().GetBillCode("CJ_BASE_0017"), Name = entity.YiDesc };
                //        db.Insert<Base_SupplierEntity>(base_SupplierEntity);

                //        new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0017");
                //    }
                //    else
                //    {
                //        entity.Yi = base_SupplierEntity.ID;
                //    }
                //}


                if (type == "edit")
                {
                    var purchase_ContractEntityTmp = GetPurchase_ContractEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Purchase_ContractDetailsUpdateList = purchase_ContractDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Purchase_ContractDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Purchase_ContractDetailsInserList = purchase_ContractDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Purchase_ContractDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.PurchaseContractId = purchase_ContractEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Project_PaymentAgreementUpdateList = project_PaymentAgreementList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_PaymentAgreementUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_PaymentAgreementInserList = project_PaymentAgreementList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_PaymentAgreementInserList)
                    {
                        item.Create(item.ID);
                        item.MainID = purchase_ContractEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    if (entity.PriceTypeEnum == PriceType.UnitPrice)
                    {
                        entity.ActualAmount = entity.SignAmount;
                    }
                    else
                    {
                        entity.ActualAmount = 0;
                    }

                    db.Insert(entity);
                    foreach (Purchase_ContractDetailsEntity item in purchase_ContractDetailsList)
                    {
                        item.PurchaseContractId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Project_PaymentAgreementEntity item in project_PaymentAgreementList)
                    {
                        item.MainID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public void VerificationSaveEntity(string purchaseContractid, VerificationTableEntity entity, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    VerificationTableEntity verificationTableEntity = db.FindEntity<VerificationTableEntity>(m => m.Purchase_ContractID == purchaseContractid);
                    string Finance_OtherPaymentID = verificationTableEntity.Finance_OtherPaymentID;//订单id
                    decimal? Verification_Amount = verificationTableEntity.Verification_Amount;//核销金额
                    decimal? Ticket_Amount = verificationTableEntity.Ticket_Amount;//收票金额


                    verificationTableEntity.Finance_OtherPaymentID = entity.Finance_OtherPaymentID;
                    verificationTableEntity.Verification_Amount = entity.Verification_Amount;
                    verificationTableEntity.Ticket_Amount = entity.Ticket_Amount;

                    db.Update<VerificationTableEntity>(verificationTableEntity);


                    //entity.Modify(keyValue);
                    // db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    if (entity.Finance_OtherPaymentID != Finance_OtherPaymentID)
                    {
                        Finance_OtherPaymentEntity finance_OtherPaymentEntity = db.FindEntity<Finance_OtherPaymentEntity>(m => m.ID == entity.Finance_OtherPaymentID);
                        Finance_OtherPaymentEntity finance_OtherPaymentEntity1 = db.FindEntity<Finance_OtherPaymentEntity>(m => m.ID == Finance_OtherPaymentID);

                        if (finance_OtherPaymentEntity != null && finance_OtherPaymentEntity1 != null)
                        {
                            finance_OtherPaymentEntity.already_verification = 1;
                            finance_OtherPaymentEntity1.already_verification = 0;
                            db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity);
                            db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity1);
                        }
                    }

                    Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(m => m.ID == purchaseContractid);
                    if (purchase_ContractEntity != null)
                    {
                        purchase_ContractEntity.Finance_OtherPaymentID = entity.Finance_OtherPaymentID;
                        purchase_ContractEntity.Verification_Amount = entity.Verification_Amount;
                        purchase_ContractEntity.Ticket_Amount = entity.Ticket_Amount;

                        if (purchase_ContractEntity.ReceivedInvoiceAmount == null)
                        {
                            purchase_ContractEntity.ReceivedInvoiceAmount = 0;
                        }
                        if (purchase_ContractEntity.AccountPaid == null)
                        {
                            purchase_ContractEntity.AccountPaid = 0;
                        }
                        if (purchase_ContractEntity.SettlementAmount == null)
                        {
                            purchase_ContractEntity.SettlementAmount = 0;
                        }
                        purchase_ContractEntity.ReceivedInvoiceAmount += entity.Ticket_Amount - Ticket_Amount;//ReceivedInvoiceAmount 已收票金额
                        purchase_ContractEntity.AccountPaid += entity.Verification_Amount - Verification_Amount; //AccountPaid  已付款金额
                        purchase_ContractEntity.SettlementAmount += entity.Verification_Amount - Verification_Amount; //SettlementAmount 结算金额

                        db.Update<Purchase_ContractEntity>(purchase_ContractEntity);
                    }
                }
                else
                {
                    entity.Create();
                    entity.Purchase_ContractID = purchaseContractid;
                    db.Insert(entity);


                    Finance_OtherPaymentEntity finance_OtherPaymentEntity = db.FindEntity<Finance_OtherPaymentEntity>(m => m.ID == entity.Finance_OtherPaymentID);
                    if (finance_OtherPaymentEntity != null)
                    {
                        finance_OtherPaymentEntity.already_verification = 1;
                        db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity);
                    }

                    Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(m => m.ID == purchaseContractid);
                    if (purchase_ContractEntity != null)
                    {
                        purchase_ContractEntity.Finance_OtherPaymentID = entity.Finance_OtherPaymentID;
                        purchase_ContractEntity.Verification_Amount = entity.Verification_Amount;
                        purchase_ContractEntity.Ticket_Amount = entity.Ticket_Amount;

                        if (purchase_ContractEntity.ReceivedInvoiceAmount == null)
                        {
                            purchase_ContractEntity.ReceivedInvoiceAmount = 0;
                        }
                        if (purchase_ContractEntity.AccountPaid == null)
                        {
                            purchase_ContractEntity.AccountPaid = 0;
                        }
                        if (purchase_ContractEntity.SettlementAmount == null)
                        {
                            purchase_ContractEntity.SettlementAmount = 0;
                        }
                        purchase_ContractEntity.ReceivedInvoiceAmount += entity.Ticket_Amount;//ReceivedInvoiceAmount 已收票金额
                        purchase_ContractEntity.AccountPaid += entity.Verification_Amount; //AccountPaid  已付款金额
                        purchase_ContractEntity.SettlementAmount += entity.Verification_Amount; //SettlementAmount 结算金额

                        db.Update<Purchase_ContractEntity>(purchase_ContractEntity);
                    }
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public void VerificationSaveListEntity(string purchaseContractid, Purchase_ContractEntity entity, List<VerificationTableEntity> verificationTableList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    #region 

                    //VerificationTableEntity verificationTableEntity = db.FindEntity<VerificationTableEntity>(m => m.Purchase_ContractID == purchaseContractid);
                    //string Finance_OtherPaymentID = verificationTableEntity.Finance_OtherPaymentID;//订单id
                    //decimal? Verification_Amount = verificationTableEntity.Verification_Amount;//核销金额
                    //decimal? Ticket_Amount = verificationTableEntity.Ticket_Amount;//收票金额


                    //verificationTableEntity.Finance_OtherPaymentID = entity.Finance_OtherPaymentID;
                    //verificationTableEntity.Verification_Amount = entity.Verification_Amount;
                    //verificationTableEntity.Ticket_Amount = entity.Ticket_Amount;

                    //db.Update<VerificationTableEntity>(verificationTableEntity);


                    ////entity.Modify(keyValue);
                    //// db.Update(entity);
                    //var delJson = deleteList.ToObject<List<JObject>>();
                    //foreach (var del in delJson)
                    //{
                    //    var idList = del["idList"].ToString();
                    //    var sql = "";
                    //    if (idList.Contains(","))
                    //    {
                    //        var ids = idList.Split(',');
                    //        foreach (var id in ids)
                    //        {
                    //            sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (!string.IsNullOrWhiteSpace(idList))
                    //        {
                    //            sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                    //            databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                    //        }
                    //    }
                    //}

                    //if (entity.Finance_OtherPaymentID != Finance_OtherPaymentID)
                    //{
                    //    Finance_OtherPaymentEntity finance_OtherPaymentEntity = db.FindEntity<Finance_OtherPaymentEntity>(m => m.ID == entity.Finance_OtherPaymentID);
                    //    Finance_OtherPaymentEntity finance_OtherPaymentEntity1 = db.FindEntity<Finance_OtherPaymentEntity>(m => m.ID == Finance_OtherPaymentID);

                    //    if (finance_OtherPaymentEntity != null && finance_OtherPaymentEntity1 != null)
                    //    {
                    //        finance_OtherPaymentEntity.already_verification = 1;
                    //        finance_OtherPaymentEntity1.already_verification = 0;
                    //        db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity);
                    //        db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity1);
                    //    }
                    //}

                    //Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(m => m.ID == purchaseContractid);
                    //if (purchase_ContractEntity != null)
                    //{
                    //    purchase_ContractEntity.Finance_OtherPaymentID = entity.Finance_OtherPaymentID;
                    //    purchase_ContractEntity.Verification_Amount = entity.Verification_Amount;
                    //    purchase_ContractEntity.Ticket_Amount = entity.Ticket_Amount;

                    //    if (purchase_ContractEntity.ReceivedInvoiceAmount == null)
                    //    {
                    //        purchase_ContractEntity.ReceivedInvoiceAmount = 0;
                    //    }
                    //    if (purchase_ContractEntity.AccountPaid == null)
                    //    {
                    //        purchase_ContractEntity.AccountPaid = 0;
                    //    }
                    //    if (purchase_ContractEntity.SettlementAmount == null)
                    //    {
                    //        purchase_ContractEntity.SettlementAmount = 0;
                    //    }
                    //    purchase_ContractEntity.ReceivedInvoiceAmount += entity.Ticket_Amount - Ticket_Amount;//ReceivedInvoiceAmount 已收票金额
                    //    purchase_ContractEntity.AccountPaid += entity.Verification_Amount - Verification_Amount; //AccountPaid  已付款金额
                    //    purchase_ContractEntity.SettlementAmount += entity.Verification_Amount - Verification_Amount; //SettlementAmount 结算金额

                    //    db.Update<Purchase_ContractEntity>(purchase_ContractEntity);
                    //}


                    #endregion

                    IEnumerable<VerificationTableEntity> verificationTableEntity = db.FindList<VerificationTableEntity>(m => m.Purchase_ContractID == purchaseContractid);
                    decimal? sum_Verification_Amount = 0;
                    string[] str_a = new string[50];

                    foreach (var item in verificationTableEntity)
                    {
                        sum_Verification_Amount += item.Verification_Amount;
                        str_a[0] = item.Finance_OtherPaymentID;
                    }

                    db.Delete<VerificationTableEntity>(t => t.Purchase_ContractID == purchaseContractid);

                    Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(m => m.ID == purchaseContractid);
                    if (purchase_ContractEntity != null)
                    {
                        decimal? ticket_Amount = purchase_ContractEntity.Ticket_Amount;
                        purchase_ContractEntity.Verification_Amount = entity.Verification_Amount;
                        purchase_ContractEntity.Ticket_Amount = entity.Ticket_Amount;

                        if (purchase_ContractEntity.ReceivedInvoiceAmount == null)
                        {
                            purchase_ContractEntity.ReceivedInvoiceAmount = 0;
                        }
                        if (purchase_ContractEntity.AccountPaid == null)
                        {
                            purchase_ContractEntity.AccountPaid = 0;
                        }
                        if (purchase_ContractEntity.SettlementAmount == null)
                        {
                            purchase_ContractEntity.SettlementAmount = 0;
                        }
                        purchase_ContractEntity.ReceivedInvoiceAmount += entity.Ticket_Amount - ticket_Amount;//ReceivedInvoiceAmount 已收票金额
                        purchase_ContractEntity.AccountPaid += entity.Verification_Amount - sum_Verification_Amount; //AccountPaid  已付款金额
                        purchase_ContractEntity.SettlementAmount += entity.Verification_Amount - sum_Verification_Amount; //SettlementAmount 结算金额

                    }
                    var finance_OtherPaymentID = "";
                    string[] str_b = new string[1];
                    foreach (VerificationTableEntity item in verificationTableList)
                    {
                        item.Ticket_Amount = entity.Ticket_Amount;
                        item.ProjectID = entity.ProjectID;
                        item.ProjectName = entity.ProjectName;
                        item.CreationDate = DateTime.Now;
                        db.Insert(item);

                        Finance_OtherPaymentEntity finance_OtherPaymentEntity = db.FindEntity<Finance_OtherPaymentEntity>(m => m.ID == item.Finance_OtherPaymentID);
                        if (finance_OtherPaymentEntity != null)
                        {
                            str_b[0] = finance_OtherPaymentEntity.ID;
                            finance_OtherPaymentEntity.already_verification = 1;
                            finance_OtherPaymentID += finance_OtherPaymentEntity.Code + ",";

                            if (!str_a.Contains(finance_OtherPaymentEntity.ID))
                            {
                                db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity);
                            }
                        }
                    }

                    foreach (var item in str_a)
                    {
                        if (!str_b.Contains(item))
                        {
                            Finance_OtherPaymentEntity finance_OtherPaymentEntity = db.FindEntity<Finance_OtherPaymentEntity>(m => m.ID == item);
                            finance_OtherPaymentEntity.already_verification = 0;
                            db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity);
                        }
                    }


                    if (!string.IsNullOrEmpty(finance_OtherPaymentID))
                    {
                        finance_OtherPaymentID = finance_OtherPaymentID.Substring(0, finance_OtherPaymentID.Length - 1);
                    }
                    purchase_ContractEntity.Finance_OtherPaymentID = finance_OtherPaymentID;

                    db.Update<Purchase_ContractEntity>(purchase_ContractEntity);
                }
                else
                {

                    Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(m => m.ID == purchaseContractid);
                    if (purchase_ContractEntity != null)
                    {
                        purchase_ContractEntity.Verification_Amount = entity.Verification_Amount;
                        purchase_ContractEntity.Ticket_Amount = entity.Ticket_Amount;

                        if (purchase_ContractEntity.ReceivedInvoiceAmount == null)
                        {
                            purchase_ContractEntity.ReceivedInvoiceAmount = 0;
                        }
                        if (purchase_ContractEntity.AccountPaid == null)
                        {
                            purchase_ContractEntity.AccountPaid = 0;
                        }
                        if (purchase_ContractEntity.SettlementAmount == null)
                        {
                            purchase_ContractEntity.SettlementAmount = 0;
                        }
                        purchase_ContractEntity.ReceivedInvoiceAmount += entity.Ticket_Amount;//ReceivedInvoiceAmount 已收票金额
                        purchase_ContractEntity.AccountPaid += entity.Verification_Amount; //AccountPaid  已付款金额
                        purchase_ContractEntity.SettlementAmount += entity.Verification_Amount; //SettlementAmount 结算金额
                    }
                    var finance_OtherPaymentID = "";
                    foreach (VerificationTableEntity item in verificationTableList)
                    {
                        item.Ticket_Amount = entity.Ticket_Amount;
                        item.ProjectID = entity.ProjectID;
                        item.ProjectName = entity.ProjectName;
                        item.CreationDate = DateTime.Now;
                        db.Insert(item);

                        Finance_OtherPaymentEntity finance_OtherPaymentEntity = db.FindEntity<Finance_OtherPaymentEntity>(m => m.ID == item.Finance_OtherPaymentID);
                        if (finance_OtherPaymentEntity != null)
                        {
                            finance_OtherPaymentEntity.already_verification = 1;
                            finance_OtherPaymentID += finance_OtherPaymentEntity.Code + ",";

                            db.Update<Finance_OtherPaymentEntity>(finance_OtherPaymentEntity);
                        }

                    }
                    if (!string.IsNullOrEmpty(finance_OtherPaymentID))
                    {
                        finance_OtherPaymentID = finance_OtherPaymentID.Substring(0, finance_OtherPaymentID.Length - 1);
                    }
                    purchase_ContractEntity.Finance_OtherPaymentID = finance_OtherPaymentID;

                    db.Update<Purchase_ContractEntity>(purchase_ContractEntity);
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        internal IEnumerable<Purchase_ContractEntity> GetList(string projectId)
        {
            try
            {
                return BaseRepository().FindList<Purchase_ContractEntity>(i => i.ProjectID == projectId && i.AuditStatus == "2");
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void UpdateAndDelDetails(List<Purchase_ContractDetailsEntity> updateList, List<Purchase_ContractDetailsEntity> delList)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                var flag = false;
                if (updateList.Count > 0)
                {
                    flag = true;
                    db.Update(updateList);
                }
                if (delList.Count > 0)
                {
                    flag = true;
                    db.Delete(delList);
                }
                if (flag)
                {
                    db.Commit();
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Purchase_ContractEntity> GetContractList(string queryJson)
        {
            var sb = new StringBuilder();
            sb.Append(" select a.*,");
            sb.Append("b.F_FullName,");
            sb.Append("c.Name as SupplierName,");
            sb.Append("d.ProjectName as ProjName,");
            sb.Append("e.F_ItemName,");
            sb.Append("f.Name as ContractTypeName ");
            sb.Append(" from Purchase_Contract as a ");
            sb.Append(" left join Base_Company as b on a.Jia = b.F_CompanyId ");
            sb.Append(" left join Base_Supplier as c on a.Yi = c.ID ");
            sb.Append(" left join Base_CJ_Project as d on a.ProjectID = d.ID ");
            sb.Append(" left join Base_DataItemDetail as e on a.ContractType = e.F_ItemValue ");
            sb.Append(" left join Base_ContractType as f on a.ContractType=f.id ");
            sb.Append(" where a.AuditStatus='2' ");

            var queryParam = queryJson.ToJObject();
            // 虚拟参数
            var dp = new DynamicParameters(new { });
            if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
            {
                dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                sb.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
            }
            if (!queryParam["Code"].IsEmpty())
            {
                dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                sb.Append(" AND t.Code Like @Code ");
            }
            if (!queryParam["Name"].IsEmpty())
            {
                dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                sb.Append(" AND a.Name Like @Name ");
            }
            if (!queryParam["ProjectID"].IsEmpty())
            {
                dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                sb.Append(" AND d.ProjectID = @ProjectID ");
            }

            if (!queryParam["word"].IsEmpty())
            {
                dp.Add("word", "%" + queryParam["word"].ToString() + "%", DbType.String);
                sb.Append(" AND (a.Name Like @word or d.ProjectName Like @word ) ");
            }

            var list = this.BaseRepository().FindList<Purchase_ContractEntity>(sb.ToString(), dp);
            var query = from item in list orderby item.CreationDate descending select item;
            return query;


        }

        public IEnumerable<Purchase_ContractDetailsEntity> GetDetailList(string contractId)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(" select pcd.*,ppad.Fk_BaseMaterialsId from Purchase_Contract as pc left join Purchase_ContractDetails as pcd on pc.ID=pcd.PurchaseContractId ");
                sb.Append(" left join Project_PurchaseApplyDetails as ppad on ppad.ID=pcd.ProjectPurchaseApplyDetailsId");
                sb.Append(" where pc.AuditStatus='2'");
                // 虚拟参数

                if (!string.IsNullOrEmpty(contractId))
                {
                    sb.Append(" AND pcd.PurchaseContractId='" + contractId + "' ");
                }

                var list = this.BaseRepository().FindList<Purchase_ContractDetailsEntity>(sb.ToString());
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        //public IEnumerable<Purchase_ContractDetailsEntity> GetDetailList(string contractId)
        //{
        //    try
        //    {
        //        var info = BaseRepository().FindEntity<Purchase_ContractEntity>(i => i.ID == contractId);
        //        var isAudit = info.AuditStatus == "2";
        //        if (isAudit)
        //        {
        //            return BaseRepository().FindList<Purchase_ContractDetailsEntity>(i => i.PurchaseContractId == contractId);
        //        }
        //        else
        //            return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is ExceptionEx)
        //        {
        //            throw;
        //        }
        //        else
        //        {
        //            throw ExceptionEx.ThrowServiceException(ex);
        //        }
        //    }
        //}




        #endregion

    }
}
