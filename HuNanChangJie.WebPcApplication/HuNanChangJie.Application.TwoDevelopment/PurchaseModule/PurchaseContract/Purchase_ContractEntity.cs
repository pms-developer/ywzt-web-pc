﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 19:12
    /// 描 述：采购合同
    /// </summary>
    public class Purchase_ContractEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 罚款金额
        /// </summary>
        [Column("FINEEXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? FineExpend { get; set; }
        /// <summary>
        /// 框架合同实际产生金额
        /// </summary>
        [Column("ACTUALAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? ActualAmount { get; set; }

        /// <summary>
        /// 付款单号
        /// </summary>
        [Column("FINANCE_OTHERPAYMENTID")]
        public string Finance_OtherPaymentID { get; set; }

        /// <summary>
        /// 核销金额
        /// </summary>
        [Column("VERIFICATION_AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Verification_Amount { get; set; }


        /// <summary>
        /// 收票金额
        /// </summary>
        [Column("TICKET_AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Ticket_Amount { get; set; }




        /// <summary>
        /// 其它收入
        /// </summary>
        [Column("OTHERINCOME")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherIncome { get; set; }


        /// <summary>
        /// 其它支出
        /// </summary>
        [Column("OTHEREXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherExpend { get; set; }
        /// <summary>
        /// 合同编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 合同名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 外部合同编码
        /// </summary>
        [Column("EXTERNALCODE")]
        public string ExternalCode { get; set; }

        /// <summary>
        /// 签定日期
        /// </summary>
        [Column("SIGNDATE")]
        public DateTime? SignDate { get; set; }
        /// <summary>
        /// 甲方
        /// </summary>
        [Column("JIA")]
        public string Jia { get; set; }
        /// <summary>
        /// 乙方
        /// </summary>
        [Column("YI")]
        public string Yi { get; set; }
        /// <summary>
        /// 计价方式
        /// </summary>
        [Obsolete]
        [Column("PRICETYPE")]
        public string PriceType { get; set; }
        /// <summary>
        /// 项目模式
        /// </summary>
        [Column("PROJECTMODE")]
        public string ProjectMode { get; set; }
        /// <summary>
        /// 计价方式
        /// </summary>
        [NotMapped]
        public PriceType PriceTypeEnum
        {
            get { return (PriceType)Enum.Parse(typeof(PriceType), this.PriceType); }
            set { this.PriceType = value.ToString(); }
        }

        /// <summary>
        /// 计价方式名
        /// </summary>
        [NotMapped]

        public string PriceTypeName {
            get {
                if (PriceTypeEnum == HuNanChangJie.SystemCommon.PriceType.Framework)
                    return "框架采购合同";
                return "单次采购合同";
            }
        }

        /// <summary>
        /// 合同类型
        /// </summary>
        [Column("CONTRACTTYPE")]
        public string ContractType { get; set; }

        /// <summary>
        /// 合同签定金额
        /// </summary>
        [Column("SIGNAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? SignAmount { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        [Column("TAXAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxAmount { get; set; }
        /// <summary>
        /// 应付类型
        /// </summary>
        [Column("COPETYPE")]
        public string CopeType { get; set; }
        /// <summary>
        /// 采购方式
        /// </summary>
        [Column("PURCHASETYPE")]
        public string PurchaseType { get; set; }
        /// <summary>
        /// 合同有效期
        /// </summary>
        [Column("PERIOD")]
        public DateTime? Period { get; set; }
        /// <summary>
        /// 到货日期
        /// </summary>
        [Column("DELIVERYDATE")]
        public DateTime? DeliveryDate { get; set; }
        /// <summary>
        /// 发票类型
        /// </summary>
        [Column("INVOICETYPE")]
        public string InvoiceType { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        [Column("TAXRAGE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRage { get; set; }
        /// <summary>
        /// 质保金比例
        /// </summary>
        [Column("DEPOSITRAGE")]
        [DecimalPrecision(18, 4)]
        public decimal? DepositRage { get; set; }
        /// <summary>
        /// 质保金额
        /// </summary>
        [Column("DEPOSITAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? DepositAmount { get; set; }
        /// <summary>
        /// 运费
        /// </summary>
        [Column("FREIGHT")]
        [DecimalPrecision(18, 4)]
        public decimal? Freight { get; set; }
        /// <summary>
        /// 优惠金额
        /// </summary>
        [Column("DISCOUNTSAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? DiscountsAmount { get; set; }
        /// <summary>
        /// 优惠合同金额
        /// </summary>
        [Column("DISCOUNTSCONTRACTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? DiscountsContractAmount { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        [Column("ORDERAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? OrderAmount { get; set; }
        /// <summary>
        /// 已收票金额
        /// </summary>
        [Column("RECEIVEDINVOICEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? ReceivedInvoiceAmount { get; set; }
        /// <summary>
        /// 入库金额
        /// </summary>
        [Column("INSTORAGEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? InStorageAmount { get; set; }
        /// <summary>
        /// 已付款金额
        /// </summary>
        [Column("ACCOUNTPAID")]
        [DecimalPrecision(18, 4)]
        public decimal? AccountPaid { get; set; }
        /// <summary>
        /// 结算金额
        /// </summary>
        [Column("SETTLEMENTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementAmount { get; set; }
        /// <summary>
        /// 应付款
        /// </summary>
        [Column("ACCOUNTPAYABLE")]
        [DecimalPrecision(18, 4)]
        public decimal? AccountPayable { get; set; }
        /// <summary>
        /// 是否已完成
        /// </summary>
        [Column("ISFINISHED")]
        public bool? IsFinished { get; set; }
        /// <summary>
        /// 合同说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 付款条件
        /// </summary>
        [Column("PAYMENTCONDITION")]
        public string PaymentCondition { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 是否预算控制
        /// </summary>
        [Column("ISBUDGET")]
        public bool? IsBudget { get; set; }

        /// <summary>
        /// 约定付款比例
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? YdfkBiLi { get; set; }

        [NotMapped]
        public string F_FullName { get; set; }

        [NotMapped]
        public string F_ItemName { get; set; }

        [NotMapped]
        public string JiaName { get; set; }

        [NotMapped]
        public string ProjName { get; set; }

        [Column("YIDESC")]
        public string YiDesc { get; set; }

        [Column("YICODE")]
        public string YiCode { get; set; }


        [Column("ACCOUNTNAME")]
        public string AccountName { get; set; }

        [Column("BANKNAME")]
        public string BankName { get; set; }


        [Column("BANKNUMBER")]
        public string BankNumber { get; set; }


        [Column("PROJECTMANAGER")]
        public string ProjectManager { get; set; }


        /// <summary>
        /// 合同类型名称
        /// </summary>
        [NotMapped]
        public string ContractTypeName { get; set; }

        /// <summary>
        /// 约定应付账款  约定应付账款=累计结算金额*约定付款比例-累计已付金额
        /// </summary>
        [NotMapped]
        public decimal? YdyfKuan
        {
            get; set;
            //get
            //{
            //    if (YdfkBiLi == null) return null;
            //    if (YdfkBiLi <= 0) return null;
            //    return (SettlementAmount ?? 0) * YdfkBiLi / 100 - (AccountPaid ?? 0);
            //}
        }

        /// <summary>
        /// 实际付款比例(%)
        /// </summary>
        [NotMapped]
        public decimal? Sjfkbl
        {
            get
            {
                if (SettlementAmount == null) return null;
                if (SettlementAmount == 0) return null;
                return (AccountPaid ?? 0) / SettlementAmount * 100;
            }
        }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段


        /// <summary>
        /// 
        /// </summary>
        [NotMapped]
        public string Finance_OtherPaymentCode { get; set; }

        /// <summary>
        /// 供应商名称（乙方）
        /// </summary>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// 公司名称（甲方）
        /// </summary>
        [NotMapped]
        public string JiaCompanyName { get; set; }

        /// <summary>
        /// 项目名称（与表关联）
        /// </summary>
        [NotMapped]
        public string ProName { get; set; }

        /// <summary>
        /// 项目经理名称
        /// </summary>
        [NotMapped]
        public string ProjectManagerName { get; set; }


        #endregion
    }
}

