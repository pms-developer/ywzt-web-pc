﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 19:12
    /// 描 述：采购合同
    /// </summary>
    public class Project_PaymentAgreementEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 分包合同ID/采购合同ID
        /// </summary>
        [Column("MAINID")]
        public string MainID { get; set; }
        /// <summary>
        /// 款项性质
        /// </summary>
        [Column("FUNDTYPE")]
        public string FundType { get; set; }
        /// <summary>
        /// 付款比率
        /// </summary>
        [Column("PAYMENTRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? PaymentRate { get; set; }
        /// <summary>
        /// 付款金额
        /// </summary>
        [Column("PAYMENTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? PaymentAmount { get; set; }
        /// <summary>
        /// 预计付款日期
        /// </summary>
        [Column("PREPAYPAYMENTDATE")]
        public DateTime? PrepayPaymentDate { get; set; }
        /// <summary>
        /// 付款责任人
        /// </summary>
        [Column("OPERATORID")]
        public string OperatorId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

