﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 19:12
    /// 描 述：采购合同
    /// </summary>
    public class PurchaseContractBLL : PurchaseContractIBLL
    {
        private PurchaseContractService purchaseContractService = new PurchaseContractService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return purchaseContractService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractDetailsEntity> GetPurchase_ContractDetailsList(string keyValue)
        {
            try
            {
                return purchaseContractService.GetPurchase_ContractDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Finance_OtherPaymentEntity> GetPageList(string purchase_ContractID)
        {
            try
            {
                return purchaseContractService.GetPageList(purchase_ContractID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Project_PaymentAgreement表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_PaymentAgreementEntity> GetProject_PaymentAgreementList(string keyValue)
        {
            try
            {
                return purchaseContractService.GetProject_PaymentAgreementList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_ContractDetailsEntity> GetDetailList(IEnumerable<string> idlist)
        {
            try
            {
                return purchaseContractService.GetDetailList(idlist);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_Contract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractEntity GetPurchase_ContractEntity(string keyValue)
        {
            try
            {
                return purchaseContractService.GetPurchase_ContractEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractDetailsEntity GetPurchase_ContractDetailsEntity(string keyValue)
        {
            try
            {
                return purchaseContractService.GetPurchase_ContractDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public VerificationTableEntity GetVerificationTableEntity(string purchase_ContractID)
        {
            try
            {
                return purchaseContractService.GetVerificationTableEntity(purchase_ContractID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Project_PaymentAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_PaymentAgreementEntity GetProject_PaymentAgreementEntity(string keyValue)
        {
            try
            {
                return purchaseContractService.GetProject_PaymentAgreementEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                purchaseContractService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_ContractDetailsEntity> GetDetails(IEnumerable<string> contractDetailsIdList)
        {
            try
            {
                return purchaseContractService.GetDetails(contractDetailsIdList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_ContractEntity entity, List<Purchase_ContractDetailsEntity> purchase_ContractDetailsList, List<Project_PaymentAgreementEntity> project_PaymentAgreementList, string deleteList, string type)
        {
            try
            {
                purchaseContractService.SaveEntity(keyValue, entity, purchase_ContractDetailsList, project_PaymentAgreementList, deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("PurchaseContractCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void VerificationSaveEntity(string purchaseContractid, VerificationTableEntity entity, string deleteList, string type)
        {
            try
            {
                purchaseContractService.VerificationSaveEntity(purchaseContractid, entity, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public void VerificationSaveListEntity(string purchaseContractid, Purchase_ContractEntity entity, List<VerificationTableEntity> verificationTableList, string deleteList, string type)
        {
            try
            {
                purchaseContractService.VerificationSaveListEntity(purchaseContractid, entity, verificationTableList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public void Audit(string keyValue)
        {
            try
            {
                var details = purchaseContractService.GetPurchase_ContractDetailsList(keyValue);
                var projectSubjectBll = new ProjectSubjectBll();
                var updateBudgetList = new List<Project_ConstructionBudgetMaterialsEntity>();
                var updateApplyList = new List<Project_PurchaseApplyDetailsEntity>();
                if (details.Any())
                { 
                    var applyBll = new PurchaseApplyBLL();
                    var projectId = details.First().ProjectId;
                    var applyDetailsIdList = details.Select(i => i.ProjectPurchaseApplyDetailsId);
                    //var budgetDetailsIdList = details.Select(i => i.ProjectConstructionBudgetMaterialsId);
                    //var budgetList = budgetBll.GetMaterials(budgetDetailsIdList);
                    var applyList = applyBll.GetDetails(applyDetailsIdList);
                    foreach (var item in details)
                    {
                        //更新预算中的已采购数
                        //var budgetInfo = budgetList.First(i => i.ID == item.ProjectConstructionBudgetMaterialsId);
                        //budgetInfo.PurchasedQuantity = (budgetInfo.PurchasedQuantity ?? 0) + (item.ContractQuantity ?? 0);
                        //budgetInfo.PurchasedTotalPrice = (budgetInfo.PurchasedTotalPrice ?? 0) + (item.Amount ?? 0);
                        //if (budgetInfo.PurchasedQuantity > 0)
                        //{
                        //    budgetInfo.PurchasedPrice = budgetInfo.PurchasedTotalPrice / budgetInfo.PurchasedQuantity;
                        //}
                        //updateBudgetList.Add(budgetInfo);

                        //更新申请单中的可采购数
                        if (applyList.Count() > 0)
                        {
                            var applyInfo = applyList.First(i => i.ID == item.ProjectPurchaseApplyDetailsId);
                            applyInfo.CurrentAllowQuantity = (applyInfo.CurrentAllowQuantity ?? 0) - (item.ContractQuantity ?? 0);
                            updateApplyList.Add(applyInfo);

                            //更新待采购数
                            item.CurrentAllowQuantity = (item.CurrentAllowQuantity ?? 0) - (item.ContractQuantity ?? 0);
                        }
                    }

                }

                var mainInfo = purchaseContractService.GetPurchase_ContractEntity(keyValue);

                if (string.IsNullOrEmpty(mainInfo.Yi))
                {
                    if (!string.IsNullOrEmpty(mainInfo.YiDesc) && !string.IsNullOrEmpty(mainInfo.YiCode))
                    {
                        Base_SupplierEntity base_SupplierEntity = new Base_SupplierBLL().GetBase_SupplierEntityByName(mainInfo.YiDesc);
                        if (base_SupplierEntity == null)
                        {
                            string yi = Guid.NewGuid().ToString();
                            mainInfo.Yi = yi;
                            base_SupplierEntity = new Base_SupplierEntity() { ID = yi, AuditStatus = "2", Code = new CodeRuleBLL().GetBillCode("CJ_BASE_0017"), Name = mainInfo.YiDesc, Flag = mainInfo.YiCode };
                            //db.Insert<Base_SupplierEntity>(base_SupplierEntity);
                            new Base_SupplierBLL().SaveEntity(yi, base_SupplierEntity,new List<Base_SupplierBankEntity>(),new List<Base_SupplierLinkmanEntity>(),"","add");
                            //new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0017");
                        }
                        else
                        {
                            mainInfo.Yi = base_SupplierEntity.ID;
                        }

                        new PurchaseContractBLL().UpdateEntity(mainInfo);
                    }
                }

                var typeInfo = new BaseContractTypeBLL().GetBase_ContractTypeEntity(mainInfo.ContractType);
                var subjectInfo = projectSubjectBll.GetEntity(typeInfo.BaseSubjectId, mainInfo.ProjectID);
                if (subjectInfo != null)
                {
                    subjectInfo.ActuallyOccurred = (subjectInfo.ActuallyOccurred ?? 0) + (mainInfo.SignAmount ?? 0);
                    decimal afterTax = (mainInfo.SignAmount ?? 0) - (mainInfo.TaxAmount ?? 0);
                    subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;

                    projectSubjectBll.UpdateInfo(subjectInfo);
                }

                purchaseContractService.UpdateRelationData(details, updateBudgetList, updateApplyList, subjectInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UpdateEntity(Purchase_ContractEntity contractInfo)
        {
            try
            {
                purchaseContractService.UpdateEntity(contractInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UpdateDetails(List<Purchase_ContractDetailsEntity> updateList, List<Purchase_ContractDetailsEntity> insertList)
        {
            try
            {
                purchaseContractService.UpdateDetails(updateList, insertList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UnAudit(string keyValue)
        {
            try
            {
                var details = purchaseContractService.GetPurchase_ContractDetailsList(keyValue);
                var updateBudgetList = new List<Project_ConstructionBudgetMaterialsEntity>();
                var updateApplyList = new List<Project_PurchaseApplyDetailsEntity>();

                var projectSubjectBll = new ProjectSubjectBll();
                if (details.Any())
                {

                    var applyBll = new PurchaseApplyBLL();
                    var projectId = details.First().ProjectId;
                    var applyDetailsIdList = details.Select(i => i.ProjectPurchaseApplyDetailsId);
                    //var budgetDetailsIdList = details.Select(i => i.ProjectConstructionBudgetMaterialsId);
                    //var budgetList = budgetBll.GetMaterials(budgetDetailsIdList);
                    var applyList = applyBll.GetDetails(applyDetailsIdList);
                    foreach (var item in details)
                    {
                        //更新预算中的已采购数
                        //var budgetInfo = budgetList.First(i => i.ID == item.ProjectConstructionBudgetMaterialsId);
                        //budgetInfo.PurchasedQuantity = (budgetInfo.PurchasedQuantity ?? 0) - (item.ContractQuantity ?? 0);
                        //budgetInfo.PurchasedTotalPrice = (budgetInfo.PurchasedTotalPrice ?? 0) - (item.Amount ?? 0);
                        //if (budgetInfo.PurchasedQuantity > 0)
                        //{
                        //    budgetInfo.PurchasedPrice = budgetInfo.PurchasedTotalPrice / budgetInfo.PurchasedQuantity;
                        //}
                        //updateBudgetList.Add(budgetInfo);

                        //更新申请单中的可采购数
                        if (applyList.Count() > 0)
                        {
                            var applyInfo = applyList.First(i => i.ID == item.ProjectPurchaseApplyDetailsId);
                            applyInfo.CurrentAllowQuantity = (applyInfo.CurrentAllowQuantity ?? 0) + (item.ContractQuantity ?? 0);
                            updateApplyList.Add(applyInfo);

                            //更新待采购数
                            item.CurrentAllowQuantity = (item.CurrentAllowQuantity ?? 0) + (item.ContractQuantity ?? 0);
                        }
                    }


                }

                var mainInfo = purchaseContractService.GetPurchase_ContractEntity(keyValue);
                var typeInfo = new BaseContractTypeBLL().GetBase_ContractTypeEntity(mainInfo.ContractType);
                var subjectInfo = projectSubjectBll.GetEntity(typeInfo.BaseSubjectId, mainInfo.ProjectID);
                if (subjectInfo != null)
                {
                    subjectInfo.ActuallyOccurred = (subjectInfo.ActuallyOccurred ?? 0) - (mainInfo.SignAmount ?? 0);
                    decimal afterTax = (mainInfo.SignAmount ?? 0) - (mainInfo.TaxAmount ?? 0);
                    subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) - afterTax;

                    projectSubjectBll.UpdateInfo(subjectInfo);
                }

                purchaseContractService.UpdateRelationData(details, updateBudgetList, updateApplyList, subjectInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void EditStatus(string keyValue)
        {
            try
            {
                var contract = purchaseContractService.GetPurchase_ContractEntity(keyValue);
                if (contract != null)
                {
                    contract.AuditStatus = "0";
                }
                purchaseContractService.UpdateEntity(contract);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        internal void UpdateAndDelDetails(List<Purchase_ContractDetailsEntity> updateList, List<Purchase_ContractDetailsEntity> delList)
        {
            try
            {
                purchaseContractService.UpdateAndDelDetails(updateList, delList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_ContractDetailsEntity> GetDetailList(string contractId)
        {
            try
            {
                var list = purchaseContractService.GetDetailList(contractId);
                var query = from item in list
                            where ((item.ContractQuantity ?? 0) - (item.TotalOrderQuantity ?? 0)) > 0
                            orderby item.CreationDate descending
                            select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_ContractEntity> GetContractList(XqPagination paginationobj, string queryJson)
        {
            try
            {
                return purchaseContractService.GetContractList(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public int GetIsExceed(string contractId)
        {
            try
            {
                var exceedNum = purchaseContractService.GetIsExceed(contractId);
                return exceedNum;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Purchase_ContractEntity> GetList(string projectId)
        {
            try
            {
                return purchaseContractService.GetList(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
