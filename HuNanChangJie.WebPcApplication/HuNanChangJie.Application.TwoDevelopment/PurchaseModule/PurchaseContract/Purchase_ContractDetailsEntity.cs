﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 19:12
    /// 描 述：采购合同
    /// </summary>
    public class Purchase_ContractDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 采购合同ID
        /// </summary>
        [Column("PURCHASECONTRACTID")]
        public string PurchaseContractId { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 采购申请单ID
        /// </summary>
        [Column("PROJECTPURCHASEAPPLYID")]
        public string ProjectPurchaseApplyId { get; set; }
        /// <summary>
        /// 采购申请详情ID
        /// </summary>
        [Column("PROJECTPURCHASEAPPLYDETAILSID")]
        public string ProjectPurchaseApplyDetailsId { get; set; }
        /// <summary>
        /// 施工预算材料清单Id
        /// </summary>
        [Column("PROJECTCONSTRUCTIONBUDGETMATERIALSID")]
        public string ProjectConstructionBudgetMaterialsId { get; set; }
        /// <summary>
        /// 合同数量
        /// </summary>
        [Column("CONTRACTQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? ContractQuantity { get; set; }

        /// <summary>
        /// 结算数量
        /// </summary>
        [Column("SETTLEMENTQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementQuantity { get; set; }

        /// <summary>
        /// 累计订单数量
        /// </summary>
        [Column("TOTALORDERQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalOrderQuantity { get; set; }
        /// <summary>
        /// 累计入库数量
        /// </summary>
        [Column("TOTALINSTORAGEQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalInStorageQuantity { get; set; }
        /// <summary>
        /// 合同单价
        /// </summary>
        [Column("CONTRACTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ContractPrice { get; set; }
        /// <summary>
        /// 优惠单价
        /// </summary>
        [Column("DISCOUNTSPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? DiscountsPrice { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        [Column("AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Amount { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// 优惠后金额
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? AfterDiscountPrice { get; set; }
 
        /// <summary> 
        /// 材料编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary> 
        /// 名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary> 
        /// 品牌 
        /// </summary> 
        /// <returns></returns> 
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary> 
        /// 规格型号 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODELNUMBER")]
        public string ModelNumber { get; set; }
        /// <summary> 
        /// 计量单位 
        /// </summary> 
        /// <returns></returns> 
        [Column("UNIT")]
        public string Unit { get; set; }

        /// <summary> 
        /// 材料来源 
        /// </summary> 
        /// <returns></returns> 
        [Column("MATERIALSSOURCE")]
        public bool? MaterialsSource { get; set; }

        /// <summary>
        /// 当前允许采购数量(即申请数中未采购的部分)
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? CurrentAllowQuantity { get; set; }

        /// <summary>
        /// 预算单价
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? BudgetPrice { get; set; }


        [NotMapped]
        public string Fk_BaseMaterialsId { get; set; }


        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }


    public class Purchase_ContractDetails {
        public string ID { get; set; }

        [DecimalPrecision(18, 4)]
        public decimal? TotalInStorageQuantity { get; set; }

        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }


    }



}

