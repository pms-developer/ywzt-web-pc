﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 19:12
    /// 描 述：采购合同
    /// </summary>
    public interface PurchaseContractIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Purchase_ContractEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取VerificationTableEntity表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Finance_OtherPaymentEntity> GetPageList(string purchase_ContractID);

        /// <summary>
        /// 获取Purchase_ContractDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Purchase_ContractDetailsEntity> GetPurchase_ContractDetailsList(string keyValue);
        /// <summary>
        /// 获取Project_PaymentAgreement表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_PaymentAgreementEntity> GetProject_PaymentAgreementList(string keyValue);
        /// <summary>
        /// 获取Purchase_Contract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_ContractEntity GetPurchase_ContractEntity(string keyValue);

        VerificationTableEntity GetVerificationTableEntity(string purchase_ContractID);

        /// <summary>
        /// 获取Purchase_ContractDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_ContractDetailsEntity GetPurchase_ContractDetailsEntity(string keyValue);
        /// <summary>
        /// 获取Project_PaymentAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_PaymentAgreementEntity GetProject_PaymentAgreementEntity(string keyValue);
        IEnumerable<Purchase_ContractDetailsEntity> GetDetailList(string contractId);
        /// <summary>
        /// 是否超出申请数量
        /// <param name="keyValue">合同ID</param>
        /// <summary>
        /// <returns></returns>
        int GetIsExceed(string keyValue);
        IEnumerable<Purchase_ContractEntity> GetList(string projectId);

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Purchase_ContractEntity entity,List<Purchase_ContractDetailsEntity> purchase_ContractDetailsList,List<Project_PaymentAgreementEntity> project_PaymentAgreementList,string deleteList,string type);


        void VerificationSaveEntity(string purchaseContractid, VerificationTableEntity entity, string deleteList, string type);


        void VerificationSaveListEntity(string purchaseContractid, Purchase_ContractEntity entity, List<VerificationTableEntity> verificationTableList, string deleteList, string type);

        void Audit(string keyValue);
        void UnAudit(string keyValue);
        /// <summary>
        /// 修改审核状态
        /// <param name="keyValue">合同ID</param>
        /// <summary>
        /// <returns></returns>
        void EditStatus(string keyValue);
        IEnumerable<Purchase_ContractEntity> GetContractList(XqPagination paginationobj, string queryJson);
        #endregion

    }
}
