﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 14:19
    /// 描 述：采购入库
    /// </summary>
    public interface PurchaseInStorageIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Purchase_InStorageEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Purchase_InStorageDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Purchase_InStorageDetailsEntity> GetPurchase_InStorageDetailsList(string keyValue);


        IEnumerable<Purchase_InStorageDetailsEntity> GetPurchase_InStorageDetailsListConnectionMaterials(string keyValue);

        /// <summary>
        /// 获取Purchase_InStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_InStorageEntity GetPurchase_InStorageEntity(string keyValue);
        /// <summary>
        /// 获取Purchase_InStorageDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_InStorageDetailsEntity GetPurchase_InStorageDetailsEntity(string keyValue);
        #endregion

        #region  提交数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);

        OperateResultEntity AuditSporadicPurchase(string keyValue);
        OperateResultEntity UnAuditPassSporadicPurchase(string keyValue);

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Purchase_InStorageEntity entity,List<Purchase_InStorageDetailsEntity> purchase_InStorageDetailsList,string deleteList,string type);
        #endregion

    }
}
