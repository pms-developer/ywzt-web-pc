﻿using HuNanChangJie.Util;
using HuNanChangJie.Util.Common;
using System;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 14:19
    /// 描 述：采购入库
    /// </summary>
    public class PurchaseInStorageBLL : PurchaseInStorageIBLL
    {
        private PurchaseInStorageService purchaseInStorageService = new PurchaseInStorageService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_InStorageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return purchaseInStorageService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_InStorageDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_InStorageDetailsEntity> GetPurchase_InStorageDetailsList(string keyValue)
        {
            try
            {
                return purchaseInStorageService.GetPurchase_InStorageDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_InStorageDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_InStorageDetailsEntity> GetPurchase_InStorageDetailsListConnectionMaterials(string keyValue)
        {
            try
            {
                return purchaseInStorageService.GetPurchase_InStorageDetailsListConnectionMaterials(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Purchase_InStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_InStorageEntity GetPurchase_InStorageEntity(string keyValue)
        {
            try
            {
                return purchaseInStorageService.GetPurchase_InStorageEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_InStorageDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_InStorageDetailsEntity GetPurchase_InStorageDetailsEntity(string keyValue)
        {
            try
            {
                return purchaseInStorageService.GetPurchase_InStorageDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return purchaseInStorageService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return purchaseInStorageService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public Util.Common.OperateResultEntity AuditSporadicPurchase(string keyValue)
        {
            try
            {
                OperateResultEntity purchaseInStorage = purchaseInStorageService.AuditSporadicPurchase(keyValue);

                new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("PurchaseInStorageCode");

                return purchaseInStorage;

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public Util.Common.OperateResultEntity UnAuditPassSporadicPurchase(string keyValue)
        {
            try
            {
                return purchaseInStorageService.UnAuditPassSporadicPurchase(keyValue);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                purchaseInStorageService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_InStorageEntity entity,List<Purchase_InStorageDetailsEntity> purchase_InStorageDetailsList,string deleteList,string type)
        {
            try
            {
                purchaseInStorageService.SaveEntity(keyValue, entity,purchase_InStorageDetailsList,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("PurchaseInStorageCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
