﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 14:19
    /// 描 述：采购入库
    /// </summary>
    public class PurchaseInStorageService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_InStorageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.ProjectID,
                t.Base_WarehouseId,
                t.InTime,
                t.Period,
                t.InMoney,
                t.OperatorId,
                t.Workflow_ID,
                t.Remark,t.AuditStatus ,t.CreationDate 
                ");
                strSql.Append("  FROM Purchase_InStorage t ");
                strSql.Append("  WHERE t.DataType is null ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (!queryParam["Base_WarehouseId"].IsEmpty())
                {
                    dp.Add("Base_WarehouseId", queryParam["Base_WarehouseId"].ToString(), DbType.String);
                    strSql.Append(" AND t.Base_WarehouseId = @Base_WarehouseId ");
                }
                if (!queryParam["InTime"].IsEmpty())
                {
                    dp.Add("InTime", queryParam["InTime"].ToString(), DbType.String);
                    strSql.Append(" AND convert(nvarchar,t.InTime,23)  = @InTime ");
                }
                if (!queryParam["Period"].IsEmpty())
                {
                    dp.Add("Period", "%" + queryParam["Period"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Period Like @Period ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Purchase_InStorageEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_InStorageDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_InStorageDetailsEntity> GetPurchase_InStorageDetailsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Purchase_InStorageDetailsEntity>(t => t.Purchase_InStorageId == keyValue);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public IEnumerable<Purchase_InStorageDetailsEntity> GetPurchase_InStorageDetailsListConnectionMaterials(string keyValue)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(" select pis.*,");
                strSql.Append(" (select bmu.ID from Base_Materials as bm join Base_MaterialsUnit as bmu on bm.UnitId = bmu.ID where bm.ID = pis.Base_MaterialsId) as Unit");
                strSql.Append(" from Purchase_InStorageDetails as pis");
                strSql.Append(" where pis.Purchase_InStorageId = '" + keyValue + "'");

                var list = this.BaseRepository().FindList<Purchase_InStorageDetailsEntity>(strSql.ToString());
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Purchase_InStorage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_InStorageEntity GetPurchase_InStorageEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_InStorageEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_InStorageDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_InStorageDetailsEntity GetPurchase_InStorageDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_InStorageDetailsEntity>(t => t.Purchase_InStorageId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<string> messageList = new List<string>();

                Purchase_InStorageEntity purchase_InStorageEntity = db.FindEntity<Purchase_InStorageEntity>(keyValue);
                if (purchase_InStorageEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                purchase_InStorageEntity.AuditStatus = "4";
                db.Update<Purchase_InStorageEntity>(purchase_InStorageEntity);
                int? dataType = purchase_InStorageEntity.DataType;

                if (dataType != 1)
                {
                    Base_WarehouseEntity base_WarehouseEntity = db.FindEntity<Base_WarehouseEntity>(purchase_InStorageEntity.Base_WarehouseId);
                    if (base_WarehouseEntity == null)
                        messageList.Add($"仓库不存在");

                    IEnumerable<Purchase_InStorageDetailsEntity> purchase_InStorageDetailsEntityList = db.FindList<Purchase_InStorageDetailsEntity>(m => m.Purchase_InStorageId == keyValue);
                    foreach (var item in purchase_InStorageDetailsEntityList)
                    {
                        Purchase_OrderDetailsEntity purchase_OrderDetailsEntity = db.FindEntity<Purchase_OrderDetailsEntity>(item.Purchase_OrderDetailsId);
                        if (purchase_OrderDetailsEntity != null)
                        {
                            Base_MaterialsEntity base_MaterialsEntity = db.FindEntity<Base_MaterialsEntity>(item.Base_MaterialsId);
                            if (!purchase_OrderDetailsEntity.InStorageQuantity.HasValue)
                                purchase_OrderDetailsEntity.InStorageQuantity = 0;

                            purchase_OrderDetailsEntity.InStorageQuantity -= item.Quantity;

                            if (purchase_OrderDetailsEntity.InStorageQuantity < 0)
                            {
                                messageList.Add($"材料【{base_MaterialsEntity.Name}】可退库数量低于采购数量");
                            }

                            db.Update<Purchase_OrderDetailsEntity>(purchase_OrderDetailsEntity);
                        }

                        Base_WarehouseDetailsEntity base_WarehouseDetailsEntity = db.FindEntity<Base_WarehouseDetailsEntity>(m => m.Fk_WarehouseId == purchase_InStorageEntity.Base_WarehouseId && m.Fk_MaterialId == item.Base_MaterialsId);
                        if (base_WarehouseDetailsEntity != null)
                        {
                            if (!base_WarehouseDetailsEntity.Quantity.HasValue)
                                base_WarehouseDetailsEntity.Quantity = 0;
                            if (!base_WarehouseDetailsEntity.TotalMoney.HasValue)
                                base_WarehouseDetailsEntity.TotalMoney = 0;
                            if (!base_WarehouseDetailsEntity.Price1.HasValue)
                                base_WarehouseDetailsEntity.Price1 = 0;

                            base_WarehouseDetailsEntity.Quantity -= item.Quantity;
                            base_WarehouseDetailsEntity.TotalMoney -= item.TotalPrice;
                            if (base_WarehouseDetailsEntity.Quantity > 0)
                                base_WarehouseDetailsEntity.Price1 = Math.Round(base_WarehouseDetailsEntity.TotalMoney.Value / base_WarehouseDetailsEntity.Quantity.Value, 4);
                            else
                                base_WarehouseDetailsEntity.Price1 = 0;
                            if (base_WarehouseDetailsEntity.Quantity < 0)
                            {
                                messageList.Add($"仓库可退库数量低于采购数量");
                            }
                            db.Update<Base_WarehouseDetailsEntity>(base_WarehouseDetailsEntity);
                        }
                        else
                        {
                            messageList.Add($"仓库可退库数量低于采购数量");
                            //db.Insert<Base_WarehouseDetailsEntity>(new Base_WarehouseDetailsEntity()
                            //{
                            //    Quantity = item.Quantity,
                            //    TotalMoney = item.TotalPrice,
                            //    Price1 = item.Price,
                            //    Fk_WarehouseId = purchase_InStorageEntity.Base_WarehouseId,
                            //    Fk_MaterialId = item.Base_MaterialsId,
                            //    ID = Guid.NewGuid().ToString()
                            //});
                        }

                        if (base_WarehouseEntity != null)
                        {
                            if (!base_WarehouseEntity.OccupyMoney.HasValue)
                                base_WarehouseEntity.OccupyMoney = 0;
                            base_WarehouseEntity.OccupyMoney -= item.TotalPrice;

                        }
                    }
                    db.Update<Base_WarehouseEntity>(base_WarehouseEntity);

                    var purchase_OrderDetailsList = db.FindList<Purchase_OrderDetailsEntity>(m => m.PurchaseOrderId == purchase_InStorageEntity.fk_PurchaseOrderId);
                    foreach (var item in purchase_OrderDetailsList)
                    {
                        if (!item.InStorageQuantity.HasValue)
                            item.InStorageQuantity = 0;
                        if (!item.Quantity.HasValue)
                            item.Quantity = 0;

                        if (item.Quantity - item.InStorageQuantity > 0)
                        {
                            Purchase_OrderEntity purchase_OrderEntity = db.FindEntity<Purchase_OrderEntity>(m => m.ID == purchase_InStorageEntity.fk_PurchaseOrderId);
                            purchase_OrderEntity.IsFinishInStorage = false;
                            db.Update<Purchase_OrderEntity>(purchase_OrderEntity);
                            break;
                        }
                    }

                    //Purchase_OrderDetailsEntity purchase_OrderDetailsEntityE = db.FindEntity<Purchase_OrderDetailsEntity>($"select 1 from Purchase_OrderDetails a left join Project_ConstructionBudgetMaterials b on a.ProjectConstructionBudgetMaterialsId = b.id where b.MaterialsSource = 1 and isnull(a.InStorageQuantity,0) -  isnull(a.Quantity,0) <0 and a.PurchaseOrderId ='{purchase_InStorageEntity.fk_PurchaseOrderId}'", null);
                    //if (purchase_OrderDetailsEntityE == null && purchase_OrderDetailsEntityE.ID != null)
                    //{
                    //    Purchase_OrderEntity purchase_OrderEntity = db.FindEntity<Purchase_OrderEntity>(m => m.ID == purchase_InStorageEntity.fk_PurchaseOrderId);
                    //    purchase_OrderEntity.IsFinishInStorage = false;
                    //    db.Update<Purchase_OrderEntity>(purchase_OrderEntity);
                    //}

                }
                else
                {
                    messageList.Add($"该数据为零星采购入库,无法在该列表中进行反审核操作！");
                }
                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();


            db.BeginTrans();
            try
            {
                List<string> messageList = new List<string>();

                Purchase_InStorageEntity purchase_InStorageEntity = db.FindEntity<Purchase_InStorageEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (purchase_InStorageEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                purchase_InStorageEntity.AuditStatus = "2";
                db.Update<Purchase_InStorageEntity>(purchase_InStorageEntity);
                int? dataType = purchase_InStorageEntity.DataType;

                if (dataType != 1)
                {

                    Base_WarehouseEntity base_WarehouseEntity = db.FindEntity<Base_WarehouseEntity>(purchase_InStorageEntity.Base_WarehouseId);
                    if (base_WarehouseEntity == null)
                        messageList.Add($"仓库不存在");


                    IEnumerable<Purchase_InStorageDetailsEntity> purchase_InStorageDetailsEntityList = db.FindList<Purchase_InStorageDetailsEntity>(m => m.Purchase_InStorageId == keyValue);
                    foreach (var item in purchase_InStorageDetailsEntityList)
                    {
                        Purchase_OrderDetailsEntity purchase_OrderDetailsEntity = db.FindEntity<Purchase_OrderDetailsEntity>(item.Purchase_OrderDetailsId);
                        if (purchase_OrderDetailsEntity != null)
                        {
                            Base_MaterialsEntity base_MaterialsEntity = db.FindEntity<Base_MaterialsEntity>(item.Base_MaterialsId);
                            if (!purchase_OrderDetailsEntity.InStorageQuantity.HasValue)
                                purchase_OrderDetailsEntity.InStorageQuantity = 0;

                            purchase_OrderDetailsEntity.InStorageQuantity += item.Quantity;

                            if (purchase_OrderDetailsEntity.InStorageQuantity > purchase_OrderDetailsEntity.Quantity)
                            {
                                messageList.Add($"材料【{base_MaterialsEntity.Name}】入库数量超出采购数量");
                            }

                            db.Update<Purchase_OrderDetailsEntity>(purchase_OrderDetailsEntity);
                        }

                        Base_WarehouseDetailsEntity base_WarehouseDetailsEntity = db.FindEntity<Base_WarehouseDetailsEntity>(m => m.Fk_WarehouseId == purchase_InStorageEntity.Base_WarehouseId && m.Fk_MaterialId == item.Base_MaterialsId);
                        if (base_WarehouseDetailsEntity != null)
                        {
                            if (!base_WarehouseDetailsEntity.Quantity.HasValue)
                                base_WarehouseDetailsEntity.Quantity = 0;
                            if (!base_WarehouseDetailsEntity.TotalMoney.HasValue)
                                base_WarehouseDetailsEntity.TotalMoney = 0;
                            if (!base_WarehouseDetailsEntity.Price1.HasValue)
                                base_WarehouseDetailsEntity.Price1 = 0;

                            base_WarehouseDetailsEntity.Quantity += item.Quantity;
                            base_WarehouseDetailsEntity.TotalMoney += item.TotalPrice;
                            base_WarehouseDetailsEntity.Price1 = Math.Round(base_WarehouseDetailsEntity.TotalMoney.Value / base_WarehouseDetailsEntity.Quantity.Value, 4);

                            db.Update<Base_WarehouseDetailsEntity>(base_WarehouseDetailsEntity);
                        }
                        else
                        {
                            db.Insert<Base_WarehouseDetailsEntity>(new Base_WarehouseDetailsEntity()
                            {
                                Quantity = item.Quantity,
                                TotalMoney = item.TotalPrice,
                                Price1 = item.Price,
                                Fk_WarehouseId = purchase_InStorageEntity.Base_WarehouseId,
                                Fk_MaterialId = item.Base_MaterialsId,
                                ID = Guid.NewGuid().ToString()
                            });
                        }

                        if (base_WarehouseEntity != null)
                        {
                            if (!base_WarehouseEntity.OccupyMoney.HasValue)
                                base_WarehouseEntity.OccupyMoney = 0;
                            base_WarehouseEntity.OccupyMoney += item.TotalPrice;

                        }
                    }
                    db.Update<Base_WarehouseEntity>(base_WarehouseEntity);

                    var purchase_OrderDetailsList = db.FindList<Purchase_OrderDetailsEntity>(m => m.PurchaseOrderId == purchase_InStorageEntity.fk_PurchaseOrderId);
                    bool isFinish = true;
                    foreach (var item in purchase_OrderDetailsList)
                    {
                        if (!item.InStorageQuantity.HasValue)
                            item.InStorageQuantity = 0;
                        if (!item.Quantity.HasValue)
                            item.Quantity = 0;

                        if (item.Quantity - item.InStorageQuantity > 0)
                        {
                            isFinish = false;
                            break;
                        }
                    }
                    if (isFinish)
                    {
                        Purchase_OrderEntity purchase_OrderEntity = db.FindEntity<Purchase_OrderEntity>(m => m.ID == purchase_InStorageEntity.fk_PurchaseOrderId);
                        if (purchase_OrderEntity != null)
                        {
                            purchase_OrderEntity.IsFinishInStorage = true;
                            db.Update<Purchase_OrderEntity>(purchase_OrderEntity);
                        }
                    }
                }
                else
                {
                    messageList.Add($"该数据为零星采购入库,无法在该列表中进行审核操作！");
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }



        public Util.Common.OperateResultEntity AuditSporadicPurchase(string keyValue)
        {
            throw new Exception("系统异常，方法已弃用");
            /*var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Project_SporadicPurchaseEntity project_SporadicPurchaseEntity = db.FindEntity<Project_SporadicPurchaseEntity>(keyValue);

                List<string> messageList = new List<string>();

                if (project_SporadicPurchaseEntity != null)
                {
                    CodeRuleBLL CodeRuleBll = new CodeRuleBLL();

                    IEnumerable<Project_SporadicPurchaseDetailsEntity> Project_SporadicPurchaseDetailsEntityList = db.FindList<Project_SporadicPurchaseDetailsEntity>(m => m.ProjectSporadicPurchaseId == keyValue && m.IsPutInStorage == true);

                    DateTime? dateTime = DateTime.Now;
                    UserInfo userInfo = LoginUserInfo.Get();

                    foreach (var items in Project_SporadicPurchaseDetailsEntityList)
                    {
                        Base_MaterialsEntity base_MaterialsEntitys = db.FindEntity<Base_MaterialsEntity>(items.Base_MaterialsId);

                        if (base_MaterialsEntitys != null)
                        {
                            Purchase_InStorageEntity Purchase_InStorageEntity = db.FindEntity<Purchase_InStorageEntity>(m => m.fk_PurchaseOrderId == keyValue && m.DataType == 1 && m.SporadicPurchaseDetails_ID == items.ID);

                            Purchase_InStorageDetailsEntity Purchase_InStorageDetailsEntity = db.FindEntity<Purchase_InStorageDetailsEntity>(m => m.DataType == 1 && m.SporadicPurchaseDetails_ID == items.ID);

                            if (Purchase_InStorageEntity != null && Purchase_InStorageDetailsEntity != null)
                            {
                                Purchase_InStorageEntity.Base_WarehouseId = items.Base_WarehouseId;
                                Purchase_InStorageEntity.InMoney = items.Amount;
                                db.Update<Purchase_InStorageEntity>(Purchase_InStorageEntity);

                                Purchase_InStorageDetailsEntity.Base_MaterialsId = items.Base_MaterialsId;//零星采购材料id
                                Purchase_InStorageDetailsEntity.Code = items.Code;
                                Purchase_InStorageDetailsEntity.Model = items.ModelNumber;
                                Purchase_InStorageDetailsEntity.Brand = items.Brand;
                                Purchase_InStorageDetailsEntity.Price = items.ContainTaxPrice;
                                Purchase_InStorageDetailsEntity.TotalPrice = items.NoContainTaxPrice;
                                Purchase_InStorageDetailsEntity.ContainTaxTotalPrice = items.Amount;
                                Purchase_InStorageDetailsEntity.TaxRate = items.TaxRate;
                                Purchase_InStorageDetailsEntity.ContainTaxPrice = items.Price;
                                Purchase_InStorageDetailsEntity.NoContainTaxPrice = items.ContainTaxPrice;
                                Purchase_InStorageDetailsEntity.Name = items.Name;
                                Purchase_InStorageDetailsEntity.Quantity = items.Quantity;

                                db.Update<Purchase_InStorageDetailsEntity>(Purchase_InStorageDetailsEntity);
                            }
                            else
                            {
                                var data = CodeRuleBll.GetBillCode("PurchaseInStorageCode");
                                var guid = Guid.NewGuid().ToString();
                                db.Insert<Purchase_InStorageEntity>(new Purchase_InStorageEntity()
                                {
                                    ID = guid,
                                    Code = data,
                                    fk_PurchaseOrderId = project_SporadicPurchaseEntity.ID,//材料订单id-零星采购id
                                    SporadicPurchaseDetails_ID = items.ID,
                                    DataType = 1,//1为零星采购
                                    Base_WarehouseId = items.Base_WarehouseId,
                                    InTime = dateTime,
                                    InMoney = items.Amount,
                                    Period = Convert.ToInt32(dateTime.Value.ToString("yyyyMM")),
                                    OperatorId = userInfo.userId,
                                    OperatorName = userInfo.realName,
                                    Enabled = true,
                                    CreationDate = dateTime,
                                    Creation_Id = userInfo.userId,
                                    CreationName = userInfo.realName,
                                    Company_ID = userInfo.companyId,
                                    CompanyCode = userInfo.CompanyCode,
                                    CompanyName = userInfo.CompanyName,
                                    CompanyFullName = userInfo.CompanyFullName,
                                    AuditStatus = "2",
                                    ProjectID = project_SporadicPurchaseEntity.ProjectID,
                                    ProjectName = project_SporadicPurchaseEntity.ProjectName,
                                    Workflow_ID = project_SporadicPurchaseEntity.Workflow_ID,
                                }); ;

                                db.Insert<Purchase_InStorageDetailsEntity>(new Purchase_InStorageDetailsEntity()
                                {
                                    ID = Guid.NewGuid().ToString(),
                                    Purchase_InStorageId = guid,
                                    Base_MaterialsId = items.Base_MaterialsId,//零星采购材料id
                                    SporadicPurchaseDetails_ID = items.ID,
                                    DataType = 1,//1为零星采购
                                    Code = items.Code,
                                    Model = items.ModelNumber,
                                    Brand = items.Brand,
                                    Period = Convert.ToInt32(dateTime.Value.ToString("yyyyMM")),
                                    Price = items.ContainTaxPrice,
                                    TotalPrice = items.NoContainTaxPrice,
                                    ContainTaxTotalPrice = items.Amount,
                                    InTime = dateTime,
                                    TaxRate = items.TaxRate,
                                    ContainTaxPrice = items.Price,
                                    NoContainTaxPrice = items.ContainTaxPrice,
                                    Name = items.Name,
                                    Quantity = items.Quantity,
                                    ProjectId = project_SporadicPurchaseEntity.ProjectID,
                                    IsAllOut = false,
                                    OutQuantity = 0,
                                    Remark = project_SporadicPurchaseEntity.Abstract,
                                    DbQuantity = 0,
                                });
                            }

                            #region 

                            Base_WarehouseEntity base_WarehouseEntity = db.FindEntity<Base_WarehouseEntity>(items.Base_WarehouseId);
                            if (base_WarehouseEntity == null)
                                messageList.Add($"仓库不存在");


                            Base_MaterialsEntity base_MaterialsEntity = db.FindEntity<Base_MaterialsEntity>(items.Base_MaterialsId);

                            if (base_MaterialsEntity.SafeStock < items.Quantity)
                            {
                                messageList.Add($"材料【{base_MaterialsEntity.Name}】入库数量超出材料安全库存");
                            }

                            Base_WarehouseDetailsEntity base_WarehouseDetailsEntity = db.FindEntity<Base_WarehouseDetailsEntity>(m => m.Fk_WarehouseId == items.Base_WarehouseId && m.Fk_MaterialId == items.Base_MaterialsId);
                            if (base_WarehouseDetailsEntity != null)
                            {
                                if (!base_WarehouseDetailsEntity.Quantity.HasValue)
                                    base_WarehouseDetailsEntity.Quantity = 0;
                                if (!base_WarehouseDetailsEntity.TotalMoney.HasValue)
                                    base_WarehouseDetailsEntity.TotalMoney = 0;
                                if (!base_WarehouseDetailsEntity.Price1.HasValue)
                                    base_WarehouseDetailsEntity.Price1 = 0;

                                base_WarehouseDetailsEntity.Quantity += items.Quantity;
                                base_WarehouseDetailsEntity.TotalMoney += items.NoContainTaxPrice;
                                base_WarehouseDetailsEntity.Price1 = Math.Round(base_WarehouseDetailsEntity.TotalMoney.Value / base_WarehouseDetailsEntity.Quantity.Value, 4);

                                db.Update<Base_WarehouseDetailsEntity>(base_WarehouseDetailsEntity);
                            }
                            else
                            {
                                db.Insert<Base_WarehouseDetailsEntity>(new Base_WarehouseDetailsEntity()
                                {
                                    Quantity = items.Quantity,
                                    TotalMoney = items.NoContainTaxPrice,
                                    Price1 = items.ContainTaxPrice,
                                    Fk_WarehouseId = items.Base_WarehouseId,
                                    Fk_MaterialId = items.Base_MaterialsId,
                                    ID = Guid.NewGuid().ToString()
                                });
                            }

                            if (base_WarehouseEntity != null)
                            {
                                if (!base_WarehouseEntity.OccupyMoney.HasValue)
                                    base_WarehouseEntity.OccupyMoney = 0;
                                base_WarehouseEntity.OccupyMoney += items.NoContainTaxPrice;
                            }
                            //}
                            db.Update<Base_WarehouseEntity>(base_WarehouseEntity);

                            #endregion
                        }
                    }

                    project_SporadicPurchaseEntity.AuditStatus = "2";
                    db.Update<Project_SporadicPurchaseEntity>(project_SporadicPurchaseEntity);//修改零星采购状态为已审核


                    if (messageList.Count() > 0)
                    {
                        string tempMessage = "";
                        foreach (var item in messageList)
                        {
                            tempMessage += item;
                        }
                        throw new Exception(tempMessage);
                    }

                    db.Commit();

                    return new Util.Common.OperateResultEntity() { Success = true };
                }
                else
                {
                    return new Util.Common.OperateResultEntity() { Success = false, Message = "入库数据不存在！" };
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }*/


        }



        #region  提交数据
        public Util.Common.OperateResultEntity UnAuditPassSporadicPurchase(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<string> messageList = new List<string>();

                Project_SporadicPurchaseEntity project_SporadicPurchaseEntity = db.FindEntity<Project_SporadicPurchaseEntity>(keyValue);

                if (project_SporadicPurchaseEntity != null)
                {
                    project_SporadicPurchaseEntity.AuditStatus = "4";
                    db.Update<Project_SporadicPurchaseEntity>(project_SporadicPurchaseEntity);//修改零星采购状态为未通过

                    IEnumerable<Purchase_InStorageEntity> Purchase_InStorageEntityList = db.FindList<Purchase_InStorageEntity>(m => m.fk_PurchaseOrderId == keyValue);

                    foreach (var items in Purchase_InStorageEntityList)
                    {
                        Purchase_InStorageEntity purchase_InStorageEntity = db.FindEntity<Purchase_InStorageEntity>(items.ID);
                        purchase_InStorageEntity.AuditStatus = "4";
                        db.Update<Purchase_InStorageEntity>(purchase_InStorageEntity);

                        Base_WarehouseEntity base_WarehouseEntity = db.FindEntity<Base_WarehouseEntity>(purchase_InStorageEntity.Base_WarehouseId);
                        if (base_WarehouseEntity == null)
                            messageList.Add($"仓库不存在");


                        IEnumerable<Purchase_InStorageDetailsEntity> purchase_InStorageDetailsEntityList = db.FindList<Purchase_InStorageDetailsEntity>(m => m.Purchase_InStorageId == items.ID);
                        foreach (var item in purchase_InStorageDetailsEntityList)
                        {
                            //Purchase_OrderDetailsEntity purchase_OrderDetailsEntity = db.FindEntity<Purchase_OrderDetailsEntity>(item.Purchase_OrderDetailsId);
                            //if (purchase_OrderDetailsEntity != null)
                            //{
                            //    Base_MaterialsEntity base_MaterialsEntity = db.FindEntity<Base_MaterialsEntity>(item.Base_MaterialsId);
                            //    if (!purchase_OrderDetailsEntity.InStorageQuantity.HasValue)
                            //        purchase_OrderDetailsEntity.InStorageQuantity = 0;

                            //    purchase_OrderDetailsEntity.InStorageQuantity -= item.Quantity;

                            //    if (purchase_OrderDetailsEntity.InStorageQuantity < 0)
                            //    {
                            //        messageList.Add($"材料【{base_MaterialsEntity.Name}】可退库数量低于采购数量");
                            //    }

                            //    db.Update<Purchase_OrderDetailsEntity>(purchase_OrderDetailsEntity);
                            //}

                            Base_WarehouseDetailsEntity base_WarehouseDetailsEntity = db.FindEntity<Base_WarehouseDetailsEntity>(m => m.Fk_WarehouseId == purchase_InStorageEntity.Base_WarehouseId && m.Fk_MaterialId == item.Base_MaterialsId);
                            if (base_WarehouseDetailsEntity != null)
                            {
                                if (!base_WarehouseDetailsEntity.Quantity.HasValue)
                                    base_WarehouseDetailsEntity.Quantity = 0;
                                if (!base_WarehouseDetailsEntity.TotalMoney.HasValue)
                                    base_WarehouseDetailsEntity.TotalMoney = 0;
                                if (!base_WarehouseDetailsEntity.Price1.HasValue)
                                    base_WarehouseDetailsEntity.Price1 = 0;

                                base_WarehouseDetailsEntity.Quantity -= item.Quantity;
                                base_WarehouseDetailsEntity.TotalMoney -= item.TotalPrice;
                                if (base_WarehouseDetailsEntity.Quantity > 0)
                                    base_WarehouseDetailsEntity.Price1 = Math.Round(base_WarehouseDetailsEntity.TotalMoney.Value / base_WarehouseDetailsEntity.Quantity.Value, 4);
                                else
                                    base_WarehouseDetailsEntity.Price1 = 0;
                                if (base_WarehouseDetailsEntity.Quantity < 0)
                                {
                                    messageList.Add($"仓库可退库数量低于采购数量");
                                }
                                db.Update<Base_WarehouseDetailsEntity>(base_WarehouseDetailsEntity);
                            }
                            else
                            {
                                messageList.Add($"仓库可退库数量低于采购数量");
                                //db.Insert<Base_WarehouseDetailsEntity>(new Base_WarehouseDetailsEntity()
                                //{
                                //    Quantity = item.Quantity,
                                //    TotalMoney = item.TotalPrice,
                                //    Price1 = item.Price,
                                //    Fk_WarehouseId = purchase_InStorageEntity.Base_WarehouseId,
                                //    Fk_MaterialId = item.Base_MaterialsId,
                                //    ID = Guid.NewGuid().ToString()
                                //});
                            }

                            if (base_WarehouseEntity != null)
                            {
                                if (!base_WarehouseEntity.OccupyMoney.HasValue)
                                    base_WarehouseEntity.OccupyMoney = 0;
                                base_WarehouseEntity.OccupyMoney -= item.TotalPrice;

                            }
                        }

                        db.Update<Base_WarehouseEntity>(base_WarehouseEntity);

                        //var purchase_OrderDetailsList = db.FindList<Purchase_OrderDetailsEntity>(m => m.PurchaseOrderId == purchase_InStorageEntity.fk_PurchaseOrderId);
                        //foreach (var item in purchase_OrderDetailsList)
                        //{
                        //    if (!item.InStorageQuantity.HasValue)
                        //        item.InStorageQuantity = 0;
                        //    if (!item.Quantity.HasValue)
                        //        item.Quantity = 0;

                        //    if (item.Quantity - item.InStorageQuantity > 0)
                        //    {
                        //        Purchase_OrderEntity purchase_OrderEntity = db.FindEntity<Purchase_OrderEntity>(m => m.ID == purchase_InStorageEntity.fk_PurchaseOrderId);
                        //        purchase_OrderEntity.IsFinishInStorage = false;
                        //        db.Update<Purchase_OrderEntity>(purchase_OrderEntity);
                        //        break;
                        //    }
                        //}
                    }

                    if (messageList.Count() > 0)
                    {
                        string tempMessage = "";
                        foreach (var item in messageList)
                        {
                            tempMessage += item;
                        }
                        throw new Exception(tempMessage);
                    }

                    db.Commit();

                    return new Util.Common.OperateResultEntity() { Success = true };
                }
                else
                {
                    return new Util.Common.OperateResultEntity() { Success = false, Message = "入库数据不存在！" };
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }




        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var purchase_InStorageEntity = GetPurchase_InStorageEntity(keyValue);

                //if (purchase_InStorageEntity.DataType != 1)
                //{
                db.Delete<Purchase_InStorageEntity>(t => t.ID == keyValue);
                db.Delete<Purchase_InStorageDetailsEntity>(t => t.Purchase_InStorageId == purchase_InStorageEntity.ID);
                db.Commit();
                //}
                //else
                //{
                //    throw new Exception("该数据为零星采购入库,无法在该列表中进行删除操作！");
                //}
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_InStorageEntity entity, List<Purchase_InStorageDetailsEntity> purchase_InStorageDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var purchase_InStorageEntityTmp = GetPurchase_InStorageEntity(keyValue);

                    entity.Modify(keyValue);
                    if (entity.InTime.HasValue)
                        entity.Period = Convert.ToInt32(entity.InTime.Value.ToString("yyyyMM"));
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Purchase_InStorageDetailsUpdateList = purchase_InStorageDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Purchase_InStorageDetailsUpdateList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.UnitName = base_MaterialsUnitEntity.Name;
                        }

                        if (item.InTime.HasValue)
                            item.Period = Convert.ToInt32(item.InTime.Value.ToString("yyyyMM"));
                        db.Update(item);
                    }
                    var Purchase_InStorageDetailsInserList = purchase_InStorageDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Purchase_InStorageDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.Purchase_InStorageId = purchase_InStorageEntityTmp.ID;
                        item.InTime = purchase_InStorageEntityTmp.InTime;

                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.UnitName = base_MaterialsUnitEntity.Name;
                        }
                        if (item.InTime.HasValue)
                            item.Period = Convert.ToInt32(item.InTime.Value.ToString("yyyyMM"));
                        db.Insert(item);
                    }


                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    if (entity.InTime.HasValue)
                        entity.Period = Convert.ToInt32(entity.InTime.Value.ToString("yyyyMM"));
                    db.Insert(entity);
                    foreach (Purchase_InStorageDetailsEntity item in purchase_InStorageDetailsList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.UnitName = base_MaterialsUnitEntity.Name;
                        }

                        item.Purchase_InStorageId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion
    }
}