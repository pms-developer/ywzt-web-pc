﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 14:19
    /// 描 述：采购入库
    /// </summary>
    public class Purchase_InStorageDetailsEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 入库单ID
        /// </summary>
        [Column("PURCHASE_INSTORAGEID")]
        public string Purchase_InStorageId { get; set; }
        /// <summary>
        /// 材料ID
        /// </summary>
        [Column("BASE_MATERIALSID")]
        public string Base_MaterialsId { get; set; }
        /// <summary>
        /// 材料编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODEL")]
        public string Model { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary>
        /// 入库期间
        /// </summary>
        [Column("PERIOD")]
        public int? Period { get; set; }
        /// <summary>
        /// 入库单价（不含税）
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
        /// <summary>
        /// 入库总价（不含税）
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// 入库含税总价
        /// </summary>
        [Column("CONTAINTAXTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ContainTaxTotalPrice { get; set; }

        /// <summary>
        /// 入库时间
        /// </summary>
        [Column("INTIME")]
        public DateTime? InTime { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        [Column("TAXRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRate { get; set; }
        /// <summary>
        /// 含税单价
        /// </summary>
        [Column("CONTAINTAXPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ContainTaxPrice { get; set; }
        /// <summary>
        /// 不含税单价（采购单入库此字段为空？）
        /// </summary>
        [Column("NOCONTAINTAXPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? NoContainTaxPrice { get; set; }
        /// <summary>
        /// 材料名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 采购合同Id
        /// </summary>
        [Column("PURCHASE_CONTRACTID")]
        public string Purchase_ContractId { get; set; }
        /// <summary>
        /// 采购订单Id
        /// </summary>
        [Column("PURCHASE_ORDERID")]
        public string Purchase_OrderId { get; set; }
        /// <summary>
        /// 采购订单明细Id
        /// </summary>
        [Column("PURCHASE_ORDERDETAILSID")]
        public string Purchase_OrderDetailsId { get; set; }
        /// <summary>
        /// 入库数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 供应商Id
        /// </summary>
        [Column("BASE_SUPPLIERID")]
        public string Base_SupplierId { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 是否已全部出库 0：否  1：是
        /// </summary>
        [Column("ISALLOUT")]
        public bool? IsAllOut { get; set; }
        /// <summary>
        /// 出库数量
        /// </summary>
        [Column("OUTQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? OutQuantity { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 调拨数量
        /// </summary>
        [Column("DBQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? DbQuantity { get; set; }
        /// <summary>
        /// 1:零星采购入库
        /// </summary>
        [Column("DATATYPE")]
        public int? DataType { get; set; }

        /// <summary>
        /// 零星采购详细ID
        /// </summary>
        [Column("SPORADICPURCHASEDETAILS_ID")]
        public string SporadicPurchaseDetails_ID { get; set; }

        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNITNAME")]
        public string UnitName
        {
            get; set;
        }

        /// <summary>
        /// 计量单位ID
        /// </summary>
        [NotMapped]
        public string Unit
        {
            get; set;
        }


        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

