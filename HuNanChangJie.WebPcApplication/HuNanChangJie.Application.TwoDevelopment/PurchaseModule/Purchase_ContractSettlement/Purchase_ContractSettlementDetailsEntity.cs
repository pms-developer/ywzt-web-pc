﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 20:25
    /// 描 述：采购合同结算
    /// </summary>
    public class Purchase_ContractSettlementDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 采购合同结算ID
        /// </summary>
        [Column("PURCHASECONTRACTSETTLEMENTID")]
        public string PurchaseContractSettlementId { get; set; }
        /// <summary>
        /// 采购合同ID
        /// </summary>
        [Column("PURCHASECONTRACTID")]
        public string PurchaseContractId { get; set; }
        /// <summary>
        /// 采购合同详情ID
        /// </summary>
        [Column("PURCHASECONTRACTDETAILSID")]
        public string PurchaseContractDetailsId { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 施工预算材料清单ID
        /// </summary>
        [Column("PROJECTCONSTRUCTIONBUDGETMATERIALSID")]
        public string ProjectConstructionBudgetMaterialsId { get; set; }
        /// <summary>
        /// 结算单价
        /// </summary>
        [Column("SETTLEMENTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementPrice { get; set; }
        /// <summary>
        /// 结算数量
        /// </summary>
        [Column("SETTLEMENTQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementQuantity { get; set; }
        /// <summary>
        /// 结算金额
        /// </summary>
        [Column("SETTLEMENTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementAmount { get; set; }

        /// <summary>
        /// 结算单价不含税
        /// </summary>
        [Column("SETTLEMENTPRICENOTAX")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementPriceNoTax { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        [Column("TAXRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRate { get; set; }

        /// <summary>
        /// 结算金额不含税
        /// </summary>
        [Column("SETTLEMENTAMOUNTNOTAX")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementAmountNoTax { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        [Column("PURCHASEORDERID")]
        public string PurchaseOrderId { get; set; }
        /// <summary>
        /// 采购合同详情ID
        /// </summary>
        [Column("PURCHASEORDERDETAILSID")]
        public string PurchaseOrderDetailsId { get; set; }

        [NotMapped]
        public string Code { get; set; }


        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public string Brand { get; set; }


        [NotMapped]
        public string ModelNumber { get; set; }


        [NotMapped]
        public string Unit { get; set; }


        [NotMapped]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        [NotMapped]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
       
        [NotMapped]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

