﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 20:25
    /// 描 述：采购合同结算
    /// </summary>
    public interface Purchase_ContractSettlementIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Purchase_ContractSettlementEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Purchase_ContractSettlementDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Purchase_ContractSettlementDetailsEntity> GetPurchase_ContractSettlementDetailsList(string keyValue);
        /// <summary>
        /// 获取Purchase_ContractSettlement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_ContractSettlementEntity GetPurchase_ContractSettlementEntity(string keyValue);
        /// <summary>
        /// 获取Purchase_ContractSettlementDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_ContractSettlementDetailsEntity GetPurchase_ContractSettlementDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Purchase_ContractSettlementEntity entity,List<Purchase_ContractSettlementDetailsEntity> purchase_ContractSettlementDetailsList,string deleteList,string type);
        #endregion

    }
}
