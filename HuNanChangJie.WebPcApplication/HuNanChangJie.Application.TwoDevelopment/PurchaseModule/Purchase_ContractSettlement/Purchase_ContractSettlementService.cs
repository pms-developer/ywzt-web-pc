﻿using Dapper;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using HuNanChangJie.Util.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-02 20:25
    /// 描 述：采购合同结算
    /// </summary>
    public class Purchase_ContractSettlementService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractSettlementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT t.*, ");
                strSql.Append("  us.F_RealName as OperatorName, ");
                strSql.Append("  pro.ProjectName as ProName, ");
                strSql.Append("  pur.Name as PurchaseContractName ");

                strSql.Append(" FROM Purchase_ContractSettlement t left join Purchase_Contract t2 on t.PurchaseContractId=t2.id ");
                strSql.Append("  left join Base_User as us on t.OperatorId=us.F_UserId");
                strSql.Append("  left join Base_CJ_Project as pro on t.ProjectID=pro.Id");
                strSql.Append(" left join Purchase_Contract as pur on t.PurchaseContractId=pur.ID ");  
                strSql.Append(" WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                if (!queryParam["ContractName"].IsEmpty())
                {
                    dp.Add("ContractName", "%" + queryParam["ContractName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t2.name Like @ContractName ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }

                if (!queryParam["jsStartTime"].IsEmpty() && !queryParam["jsEndTime"].IsEmpty())
                {
                    dp.Add("jsstartTime", queryParam["jsStartTime"].ToDate(), DbType.DateTime);
                    dp.Add("jsendTime", queryParam["jsEndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.SettlementDate >= @jsstartTime AND t.SettlementDate <= @jsendTime ) ");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }

                if (!queryParam["Remark"].IsEmpty())
                {
                    dp.Add("Remark", "%" + queryParam["Remark"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Remark Like @Remark ");
                }

                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Purchase_ContractSettlementEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractSettlementDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractSettlementDetailsEntity> GetPurchase_ContractSettlementDetailsList(string keyValue)
        {
            try
            {
                //var sql = @"select a.*,
                //            case when c.Code is not null then c.Code when e.Code is not null then e.Code  when d.Code is not null then d.Code else e.Code end Code,
                //            case when c.Name is not null then c.Name when e.Name is not null then e.Name  when d.Name is not null then d.Name else e.Name end Name,
                //            case when c.Brand is not null then c.Brand when e.Brand is not null then e.Brand  when d.Brand is not null then d.Brand else e.Brand end Brand,
                //            case when c.ModelNumber is not null then c.ModelNumber when e.ModelNumber is not null then e.ModelNumber  when d.ModelNumber is not null then d.ModelNumber else e.ModelNumber end ModelNumber,
                //            case when c.Unit is not null then c.Unit when e.Unit is not null then e.Unit  when d.Unit is not null then d.Unit else e.Unit end Unit,
                //            b.Quantity,b.Price,b.TotalPrice,f.name,f.code
                //            from Purchase_ContractSettlementDetails a 
                //            left join Purchase_OrderDetails b on a.PurchaseOrderDetailsId = b.ID 
                //            left join Project_ConstructionBudgetMaterials c on a.ProjectConstructionBudgetMaterialsId = c.ID 
                //            left join Purchase_ContractDetails d on a.PurchaseContractDetailsId=d.ID 
                //            left join Project_PurchaseApplyDetails e on d.ProjectPurchaseApplyDetailsId=e.ID 
                //            left join (select b.Name as Unit,c.CnName as Brand,a.Code,a.Name,a.Model as ModelNumber from Base_Materials as a 
                //               left join Base_MaterialsUnit as b on a.UnitId=b.Id
                //               left join Base_MaterialsBrand as c on a.BrandId=c.Id) f on d.code=f.code
                //             where a.PurchaseContractSettlementId = '" + keyValue + "'";


                var sql = new StringBuilder();
                sql.Append("select a.*,bm.Code,bm.Name,bmb.CnName as Brand,bm.UnitId as Unit,bm.Model as ModelNumber,");
                sql.Append(" b.Quantity,b.Price,b.TotalPrice,a.TaxRate ");
                sql.Append(" from Purchase_ContractSettlementDetails a");
                sql.Append(" left join Purchase_OrderDetails b on a.PurchaseOrderDetailsId = b.ID");
                sql.Append(" left join Project_ConstructionBudgetMaterials c on a.ProjectConstructionBudgetMaterialsId = c.ID");
                sql.Append(" left join Purchase_ContractDetails d on a.PurchaseContractDetailsId = d.ID");
                sql.Append(" left join Project_PurchaseApplyDetails e on d.ProjectPurchaseApplyDetailsId = e.ID");
                sql.Append(" left join Base_Materials as bm on bm.ID = b.Fk_BaseMaterialsId");
                sql.Append(" left join Base_MaterialsBrand as bmb on bmb.ID = bm.BrandId");
                sql.Append(" left join Base_MaterialsUnit as bmu on bmu.ID = bm.UnitId where a.PurchaseContractSettlementId = '" + keyValue + "'");

                var list = this.BaseRepository().FindList<Purchase_ContractSettlementDetailsEntity>(sql.ToString());
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractSettlement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractSettlementEntity GetPurchase_ContractSettlementEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_ContractSettlementEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractSettlementDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractSettlementDetailsEntity GetPurchase_ContractSettlementDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_ContractSettlementDetailsEntity>(t => t.PurchaseContractSettlementId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Purchase_ContractSettlementEntity purchase_ContractSettlementEntity = db.FindEntity<Purchase_ContractSettlementEntity>(keyValue);
                if (purchase_ContractSettlementEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                purchase_ContractSettlementEntity.AuditStatus = "4";
                db.Update<Purchase_ContractSettlementEntity>(purchase_ContractSettlementEntity);

                IEnumerable<Purchase_ContractSettlementDetailsEntity> purchase_ContractSettlementDetailsEntityList = db.FindList<Purchase_ContractSettlementDetailsEntity>(m => m.PurchaseContractSettlementId == keyValue);
                Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(m => m.Name == "材料采购");

                foreach (var item in purchase_ContractSettlementDetailsEntityList)
                {
                    Purchase_ContractDetailsEntity purchase_ContractDetailsEntity = db.FindEntity<Purchase_ContractDetailsEntity>(m => m.ID == item.PurchaseOrderDetailsId);
                    if (purchase_ContractDetailsEntity == null)
                        continue;

                    if (!purchase_ContractDetailsEntity.SettlementQuantity.HasValue)
                        purchase_ContractDetailsEntity.SettlementQuantity = 0;

                    purchase_ContractDetailsEntity.SettlementQuantity -= item.SettlementQuantity;
                    db.Update<Purchase_ContractDetailsEntity>(purchase_ContractDetailsEntity);

                    Purchase_OrderDetailsEntity purchase_OrderDetailsEntity = db.FindEntity<Purchase_OrderDetailsEntity>(m => m.ID == item.PurchaseOrderDetailsId);
                    if (purchase_OrderDetailsEntity == null)
                        continue;

                    if (!purchase_OrderDetailsEntity.SettlementQuantity.HasValue)
                        purchase_OrderDetailsEntity.SettlementQuantity = 0;
                    purchase_OrderDetailsEntity.SettlementQuantity -= item.SettlementQuantity;

                    db.Update<Purchase_OrderDetailsEntity>(purchase_OrderDetailsEntity);
                }


                Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(purchase_ContractSettlementEntity.PurchaseContractId);
                purchase_ContractEntity.SettlementAmount = (purchase_ContractEntity.SettlementAmount ?? 0) - (purchase_ContractSettlementEntity.SettlementAmount ?? 0);
                if (!purchase_ContractEntity.FineExpend.HasValue)
                    purchase_ContractEntity.FineExpend = 0;
                if (!purchase_ContractEntity.OtherExpend.HasValue)
                    purchase_ContractEntity.OtherExpend = 0;
                if (!purchase_ContractEntity.OtherIncome.HasValue)
                    purchase_ContractEntity.OtherIncome = 0;

                purchase_ContractEntity.FineExpend -= purchase_ContractSettlementEntity.FineExpend ?? 0;
                purchase_ContractEntity.OtherExpend -= purchase_ContractSettlementEntity.OtherExpend ?? 0;
                purchase_ContractEntity.OtherIncome -= purchase_ContractSettlementEntity.OtherIncome ?? 0;


                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(m => m.Name == "PurchaseOrderHandleNode");
                if (config != null && config.Value == 1)
                {
                    if (!purchase_ContractEntity.AccountPayable.HasValue)
                        purchase_ContractEntity.AccountPayable = 0;
                    purchase_ContractEntity.AccountPayable -= purchase_ContractSettlementEntity.SettlementAmount;

                    if (base_ContractTypeEntity != null)
                    {
                        var subjectInfo = new ProjectSubjectBll().GetEntity(base_ContractTypeEntity.BaseSubjectId, purchase_ContractEntity.ProjectID);
                        if (subjectInfo != null)
                        //    resultList.Add(new OperateResultEntity() { Success = false, Message = "未找到对应的科目" });
                        {
                            subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) - (purchase_ContractSettlementEntity.SettlementAmount ?? 0);

                            //decimal afterTax = item.PayAmount.Value - (item.PayAmount.Value * purchase_ContractEntity.TaxRage.Value / 100);
                            //subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;

                            db.Update<ProjectSubjectEntity>(subjectInfo);
                        }
                    }
                }

                db.Update<Purchase_ContractEntity>(purchase_ContractEntity);

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Purchase_ContractSettlementEntity purchase_ContractSettlementEntity = db.FindEntity<Purchase_ContractSettlementEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (purchase_ContractSettlementEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                purchase_ContractSettlementEntity.AuditStatus = "2";
                db.Update<Purchase_ContractSettlementEntity>(purchase_ContractSettlementEntity);
                Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(m => m.Name == "材料采购");

                IEnumerable<Purchase_ContractSettlementDetailsEntity> purchase_ContractSettlementDetailsEntityList = db.FindList<Purchase_ContractSettlementDetailsEntity>(m => m.PurchaseContractSettlementId == keyValue);



                foreach (var item in purchase_ContractSettlementDetailsEntityList)
                {
                    Purchase_ContractDetailsEntity purchase_ContractDetailsEntity = db.FindEntity<Purchase_ContractDetailsEntity>(m => m.ID == item.PurchaseOrderDetailsId);
                    if (purchase_ContractDetailsEntity == null)
                        continue;

                    if (!purchase_ContractDetailsEntity.SettlementQuantity.HasValue)
                        purchase_ContractDetailsEntity.SettlementQuantity = 0;

                    purchase_ContractDetailsEntity.SettlementQuantity += item.SettlementQuantity;
                    db.Update<Purchase_ContractDetailsEntity>(purchase_ContractDetailsEntity);


                    Purchase_OrderDetailsEntity purchase_OrderDetailsEntity = db.FindEntity<Purchase_OrderDetailsEntity>(m => m.ID == item.PurchaseOrderDetailsId);
                    if (purchase_OrderDetailsEntity == null)
                        continue;

                    if (!purchase_OrderDetailsEntity.SettlementQuantity.HasValue)
                        purchase_OrderDetailsEntity.SettlementQuantity = 0;
                    purchase_OrderDetailsEntity.SettlementQuantity += item.SettlementQuantity;

                    db.Update<Purchase_OrderDetailsEntity>(purchase_OrderDetailsEntity);


                }

                Purchase_ContractEntity purchase_ContractEntity = db.FindEntity<Purchase_ContractEntity>(purchase_ContractSettlementEntity.PurchaseContractId);
                purchase_ContractEntity.SettlementAmount = (purchase_ContractEntity.SettlementAmount ?? 0) + (purchase_ContractSettlementEntity.SettlementAmount ?? 0);
                if (!purchase_ContractEntity.FineExpend.HasValue)
                    purchase_ContractEntity.FineExpend = 0;
                if (!purchase_ContractEntity.OtherExpend.HasValue)
                    purchase_ContractEntity.OtherExpend = 0;
                if (!purchase_ContractEntity.OtherIncome.HasValue)
                    purchase_ContractEntity.OtherIncome = 0;

                purchase_ContractEntity.FineExpend += purchase_ContractSettlementEntity.FineExpend ?? 0;
                purchase_ContractEntity.OtherExpend += purchase_ContractSettlementEntity.OtherExpend ?? 0;
                purchase_ContractEntity.OtherIncome += purchase_ContractSettlementEntity.OtherIncome ?? 0;

                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(m => m.Name == "PurchaseOrderHandleNode");
                if (config != null && config.Value == 1)
                {

                    if (!purchase_ContractEntity.AccountPayable.HasValue)
                        purchase_ContractEntity.AccountPayable = 0;
                    purchase_ContractEntity.AccountPayable += purchase_ContractSettlementEntity.SettlementAmount;

                    if (base_ContractTypeEntity != null)
                    {
                        var subjectInfo = new ProjectSubjectBll().GetEntity(base_ContractTypeEntity.BaseSubjectId, purchase_ContractEntity.ProjectID);
                        if (subjectInfo != null)
                        //   resultList.Add(new OperateResultEntity() { Success = false, Message = "未找到对应的科目" });
                        {
                            subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) + (purchase_ContractSettlementEntity.SettlementAmount ?? 0);

                            //decimal afterTax = item.PayAmount.Value - (item.PayAmount.Value * purchase_ContractEntity.TaxRage.Value / 100);
                            //subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;

                            db.Update<ProjectSubjectEntity>(subjectInfo);
                        }
                    }
                }

                db.Update<Purchase_ContractEntity>(purchase_ContractEntity);
                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var purchase_ContractSettlementEntity = GetPurchase_ContractSettlementEntity(keyValue);
                db.Delete<Purchase_ContractSettlementEntity>(t => t.ID == keyValue);
                db.Delete<Purchase_ContractSettlementDetailsEntity>(t => t.PurchaseContractSettlementId == purchase_ContractSettlementEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_ContractSettlementEntity entity, List<Purchase_ContractSettlementDetailsEntity> purchase_ContractSettlementDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var purchase_ContractSettlementEntityTmp = GetPurchase_ContractSettlementEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Purchase_ContractSettlementDetailsUpdateList = purchase_ContractSettlementDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Purchase_ContractSettlementDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Purchase_ContractSettlementDetailsInserList = purchase_ContractSettlementDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Purchase_ContractSettlementDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.PurchaseContractSettlementId = purchase_ContractSettlementEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Purchase_ContractSettlementDetailsEntity item in purchase_ContractSettlementDetailsList)
                    {
                        item.PurchaseContractSettlementId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
