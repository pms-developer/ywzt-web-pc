﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-15 17:08
    /// 描 述：采购订单变更
    /// </summary>
    public class PurchaseOrderChangeService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderChangeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ProjectID,
                t.ID,
                t.Code,
                t.SupplierId,
                t.PurchaseOrderId,
                t.ChangeDate,
                t.OperatorId,
                t.AuditStatus,
                t.CreationDate,
                t.Workflow_ID
                ");
                strSql.Append("  FROM Purchase_OrderChange t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }
                var list = this.BaseRepository().FindList<Purchase_OrderChangeEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderChangeDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderChangeDetailsEntity> GetPurchase_OrderChangeDetailsList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append("select a.*,c.ProjectName," +
                   // "e.Name,e.Code,e.Unit,e.Brand,e.ModelNumber,e.BudgetPrice," +

                   "case isnull(e.name, '') when '' then  gg.name else e.name end Name, " +
                  "case isnull(e.Code, '') when '' then gg.Code else e.Code end Code, " +
                  "case isnull(e.Unit, '') when '' then ggg.ID else e.Unit end Unit, " +
                  "case isnull(e.Brand, '') when '' then gg.BrandId else e.Brand end Brand, " +
                  "case isnull(e.ModelNumber, '') when '' then gg.Model else e.ModelNumber end ModelNumber,e.BudgetPrice, " +

                    "b.Quantity OrderQuantity,b.Price OrderPrice," +
                    "b.Quantity-(case when b.InStorage is not null then b.InStorage else 0 end) WaitInStorage " +
                    "from Purchase_OrderChangeDetails a " +
                    "left join Purchase_OrderDetails b on a.PurchaseOrderDetailsId=b.ID " +
                    "left join Purchase_Order c on b.PurchaseOrderId=c.ID " +
                    "left join Base_CJ_Project d on c.ProjectID = d.ID " +
                    "left join Purchase_ContractDetails e on b.PurchaseContractDetailsId = e.ID " +
                     "left join Base_Materials g on e.code = g.code " +

                   "left join Base_Materials gg on b.Fk_BaseMaterialsId = gg.id " +

                   "left join Base_MaterialsUnit ggg on gg.unitid = ggg.id "
                    );
                sb.Append(" where c.AuditStatus = '2' ");
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!keyValue.IsEmpty())
                {
                    dp.Add("PurchaseOrderChangeId", keyValue, DbType.String);
                    sb.Append(" AND a.PurchaseOrderChangeId=@PurchaseOrderChangeId");
                }
                var list = this.BaseRepository().FindList<Purchase_OrderChangeDetailsEntity>(sb.ToString(), dp);
                var query = from item in list orderby item.SortCode ascending select item;

                //var list= this.BaseRepository().FindList<Purchase_OrderChangeDetailsEntity>(t=>t.PurchaseOrderChangeId == keyValue );
                //var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderChange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderChangeEntity GetPurchase_OrderChangeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_OrderChangeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderChangeDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderChangeDetailsEntity GetPurchase_OrderChangeDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_OrderChangeDetailsEntity>(t => t.PurchaseOrderChangeId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var purchase_OrderChangeEntity = GetPurchase_OrderChangeEntity(keyValue);
                db.Delete<Purchase_OrderChangeEntity>(t => t.ID == keyValue);
                db.Delete<Purchase_OrderChangeDetailsEntity>(t => t.PurchaseOrderChangeId == purchase_OrderChangeEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_OrderChangeEntity entity, List<Purchase_OrderChangeDetailsEntity> purchase_OrderChangeDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var purchase_OrderChangeEntityTmp = GetPurchase_OrderChangeEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Purchase_OrderChangeDetailsUpdateList = purchase_OrderChangeDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Purchase_OrderChangeDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Purchase_OrderChangeDetailsInserList = purchase_OrderChangeDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Purchase_OrderChangeDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.PurchaseOrderChangeId = purchase_OrderChangeEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Purchase_OrderChangeDetailsEntity item in purchase_OrderChangeDetailsList)
                    {
                        item.PurchaseOrderChangeId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
