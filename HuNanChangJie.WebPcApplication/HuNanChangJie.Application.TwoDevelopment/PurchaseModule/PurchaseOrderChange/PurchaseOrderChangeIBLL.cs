﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-15 17:08
    /// 描 述：采购订单变更
    /// </summary>
    public interface PurchaseOrderChangeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Purchase_OrderChangeEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Purchase_OrderChangeDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Purchase_OrderChangeDetailsEntity> GetPurchase_OrderChangeDetailsList(string keyValue);
        /// <summary>
        /// 获取Purchase_OrderChange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_OrderChangeEntity GetPurchase_OrderChangeEntity(string keyValue);
        /// <summary>
        /// 获取Purchase_OrderChangeDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_OrderChangeDetailsEntity GetPurchase_OrderChangeDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Purchase_OrderChangeEntity entity,List<Purchase_OrderChangeDetailsEntity> purchase_OrderChangeDetailsList,string deleteList,string type);
        void UnAudit(string keyValue);
        void Audit(string keyValue);
        #endregion

    }
}
