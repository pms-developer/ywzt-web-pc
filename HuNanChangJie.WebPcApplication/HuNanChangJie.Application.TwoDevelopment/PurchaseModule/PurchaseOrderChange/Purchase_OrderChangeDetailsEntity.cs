﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-15 17:08
    /// 描 述：采购订单变更
    /// </summary>
    public class Purchase_OrderChangeDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 采购变更单ID
        /// </summary>
        [Column("PURCHASEORDERCHANGEID")]
        public string PurchaseOrderChangeId { get; set; }
        /// <summary>
        /// 采购订单详情ID
        /// </summary>
        [Column("PURCHASEORDERDETAILSID")]
        public string PurchaseOrderDetailsId { get; set; }
        /// <summary>
        /// 施工预算材料清单Id
        /// </summary>
        [Column("PROJECTCONSTRUCTIONBUDGETMATERIALSID")]
        public string ProjectConstructionBudgetMaterialsId { get; set; }
        /// <summary>
        /// 变更前数量
        /// </summary>
        [Column("BEFOREQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? BeforeQuantity { get; set; }
        /// <summary>
        /// 变更前单价
        /// </summary>
        [Column("BEFORPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? BeforPrice { get; set; }
        /// <summary>
        /// 变更后数量
        /// </summary>
        [Column("AFTERQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterQuantity { get; set; }
        /// <summary>
        /// 变更后单价
        /// </summary>
        [Column("AFTERPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterPrice { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        #endregion

        #region 扩展字段
        [NotMapped]
        public string Code { get; set; }

        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public string Brand { get; set; }

        [NotMapped]
        public string ModelNumber { get; set; }

        [NotMapped]
        public string Unit { get; set; }

        [NotMapped]
        public string ProjectName { get; set; }

        [NotMapped]
        public decimal? BudgetPrice { get; set; }

        [NotMapped]
        public decimal? OrderQuantity;

        [NotMapped]
        public decimal? OrderPrice;

        [NotMapped]
        public decimal? WaitInStorage;
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

