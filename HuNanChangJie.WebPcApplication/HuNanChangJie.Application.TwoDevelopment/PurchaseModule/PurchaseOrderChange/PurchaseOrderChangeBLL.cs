﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-15 17:08
    /// 描 述：采购订单变更
    /// </summary>
    public class PurchaseOrderChangeBLL : PurchaseOrderChangeIBLL
    {
        private PurchaseOrderChangeService purchaseOrderChangeService = new PurchaseOrderChangeService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderChangeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return purchaseOrderChangeService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderChangeDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_OrderChangeDetailsEntity> GetPurchase_OrderChangeDetailsList(string keyValue)
        {
            try
            {
                return purchaseOrderChangeService.GetPurchase_OrderChangeDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderChange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderChangeEntity GetPurchase_OrderChangeEntity(string keyValue)
        {
            try
            {
                return purchaseOrderChangeService.GetPurchase_OrderChangeEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_OrderChangeDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_OrderChangeDetailsEntity GetPurchase_OrderChangeDetailsEntity(string keyValue)
        {
            try
            {
                return purchaseOrderChangeService.GetPurchase_OrderChangeDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                purchaseOrderChangeService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_OrderChangeEntity entity,List<Purchase_OrderChangeDetailsEntity> purchase_OrderChangeDetailsList,string deleteList,string type)
        {
            try
            {
                purchaseOrderChangeService.SaveEntity(keyValue, entity,purchase_OrderChangeDetailsList,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("PurchaseOrderChangeCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UnAudit(string keyValue)
        {
            try
            {
                var details = purchaseOrderChangeService.GetPurchase_OrderChangeDetailsList(keyValue);
                if (details.Any())
                {
                    var updateList = new List<Purchase_OrderDetailsEntity>();
                    var orderBll = new PurchaseOrderBLL();
                    var orderIdList = details.Select(i => i.PurchaseOrderDetailsId);
                    var orderList = orderBll.GetDetails(orderIdList);
                    if (orderList.Any())
                    {
                        var orderId = orderList.First().PurchaseOrderId;
                        var orderInfo = orderBll.GetPurchase_OrderEntity(orderId);
                        decimal changeAmount = 0;//订单变更金额
                        foreach (var item in details)
                        {
                            var order = orderList.FirstOrDefault(i => i.ID == item.PurchaseOrderDetailsId);
                            if (order == null) continue;
                            order.Quantity = (order.Quantity ?? 0) + (item.BeforeQuantity ?? 0);

                            changeAmount += (order.Quantity ?? 0) * (item.BeforPrice ?? 0);
                            order.TotalPrice = (order.Quantity ?? 0) * (item.BeforPrice ?? 0);
                            updateList.Add(order);
                        }
                      
                        var currentAmount = changeAmount + (orderInfo.Freight ?? 0);
                        orderInfo.OrderAmount = currentAmount;

                        orderBll.Update(orderInfo, updateList);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void Audit(string keyValue)
        {
            try
            {
                var details = purchaseOrderChangeService.GetPurchase_OrderChangeDetailsList(keyValue);
                if (details.Any())
                {
                    var updateList = new List<Purchase_OrderDetailsEntity>();
                    var orderBll = new PurchaseOrderBLL();
                    var orderIdList = details.Select(i => i.PurchaseOrderDetailsId);
                    var orderList = orderBll.GetDetails(orderIdList);
                    if (orderList.Any())
                    {
                        var orderId = orderList.First().PurchaseOrderId;
                        var orderInfo = orderBll.GetPurchase_OrderEntity(orderId);
                        decimal changeAmount = 0;//订单变更金额
                        foreach (var item in details)
                        {
                            var order = orderList.FirstOrDefault(i => i.ID == item.PurchaseOrderDetailsId);
                            if (order == null) continue;
                            order.Quantity = (order.Quantity ?? 0) - (item.BeforeQuantity ?? 0);
                            order.ChangePrice = item.BeforPrice;
                            changeAmount += (order.Quantity??0) * (item.BeforPrice ?? 0);
                            order.TotalPrice= (order.Quantity ?? 0) * (item.BeforPrice ?? 0);
                            updateList.Add(order);
                        }

                        orderInfo.OrderAmount = changeAmount + (orderInfo.Freight ?? 0);

                        orderBll.Update(orderInfo, updateList);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
