﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-17 12:04
    /// 描 述：采购合同增补
    /// </summary>
    public class Purchase_ContractSupplementDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 合同增补ID
        /// </summary>
        [Column("PURCHASECONTRACTSUPPLEMENTID")]
        public string PurchaseContractSupplementId { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 采购申请单ID
        /// </summary>
        [Column("PROJECTPURCHASEAPPLYID")]
        public string ProjectPurchaseApplyId { get; set; }
        /// <summary>
        /// 采购申请详情ID
        /// </summary>
        [Column("PROJECTPURCHASEAPPLYDETAILSID")]
        public string ProjectPurchaseApplyDetailsId { get; set; }
        /// <summary>
        /// 施工预算材料清单Id
        /// </summary>
        [Column("PROJECTCONSTRUCTIONBUDGETMATERIALSID")]
        public string ProjectConstructionBudgetMaterialsId { get; set; }
        /// <summary>
        /// 增补数量
        /// </summary>
        [Column("SUPPLEMENTQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? SupplementQuantity { get; set; }
        /// <summary>
        /// 增补金额
        /// </summary>
        [Column("SUPPLEMENTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SupplementPrice { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// 合同单价
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? ContractPrice { get; set; }

        [NotMapped]
        public string Code { get; set; }
        [NotMapped]
        public string Unit { get; set; }
        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string Brand { get; set; }
        [NotMapped]
        public string ModelNumber { get; set; }
        [NotMapped]
        public decimal? BudgetPrice { get; set; }
        [NotMapped]
        public decimal? ContractQuantity { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

