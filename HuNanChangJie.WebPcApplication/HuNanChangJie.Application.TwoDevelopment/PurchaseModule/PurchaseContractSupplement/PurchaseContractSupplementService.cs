﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-17 12:04
    /// 描 述：采购合同增补
    /// </summary>
    public class PurchaseContractSupplementService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractSupplementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ProjectID,
                t.ID,
                t.Code,
                t.PurchaseContractId,
                t.SupplementAmount,
                t.OperatorId,
                t.AuditStatus,
                t.Workflow_ID,
                t.CreationDate,
                t.Remark
                ");
                strSql.Append("  FROM Purchase_ContractSupplement t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }

                if (pagination !=null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "Code";
                    pagination.sord = "DESC";
                }
                var list=this.BaseRepository().FindList<Purchase_ContractSupplementEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractSupplementDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractSupplementDetailsEntity> GetPurchase_ContractSupplementDetailsList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            --c.ContractPrice,
                            d.ContractQuantity,
                            case when c.Code is not null then c.Code else e.Code end Code,
                            case when c.Name is not null then c.Name else e.Name end Name,
                            case when c.Brand is not null then c.Brand else e.Brand end Brand,
                            case when c.ModelNumber is not null then c.ModelNumber else e.ModelNumber end ModelNumber,
                            case when c.Unit is not null then c.Unit else e.Unit end Unit,
                            c.BudgetPrice 
                            from 
                            Purchase_ContractSupplementDetails as a 
                            join Purchase_ContractSupplement b on a.PurchaseContractSupplementId=b.ID
                            left join Project_ConstructionBudgetMaterials c on a.ProjectConstructionBudgetMaterialsId = c.ID
                            left join Purchase_ContractDetails d on a.ProjectConstructionBudgetMaterialsId = d.ProjectConstructionBudgetMaterialsId 
		                        and b.PurchaseContractId=d.PurchaseContractId
                            left join Project_PurchaseApplyDetails e on a.projectPurchaseApplyDetailsId=e.ID
                            ");
                sb.Append(" where a.PurchaseContractSupplementId=@keyValue");
                var list= this.BaseRepository().FindList<Purchase_ContractSupplementDetailsEntity>(sb.ToString(),new { keyValue});
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractSupplement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractSupplementEntity GetPurchase_ContractSupplementEntity(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,b.Name ContractName,c.Name SupplierName,d.Name ContractType,bc.F_FullName
                                from Purchase_ContractSupplement a
	                            join Purchase_Contract b on a.PurchaseContractId=b.ID
	                            left join Base_Supplier c on b.Yi=c.ID 
	                            left join Base_ContractType d on b.ContractType=d.ID
                                left join Base_Company as bc on b.Jia = bc.F_CompanyId 
                            ");
                sb.Append(" where a.ID=@keyValue");
                var entity = this.BaseRepository().FindEntity<Purchase_ContractSupplementEntity>(sb.ToString(), new { keyValue });
                return entity;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractSupplementDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractSupplementDetailsEntity GetPurchase_ContractSupplementDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Purchase_ContractSupplementDetailsEntity>(t=>t.PurchaseContractSupplementId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Purchase_ContractSupplementDetailsEntity> GetDetails(string keyValue)
        {
            try
            {
                return BaseRepository().FindList<Purchase_ContractSupplementDetailsEntity>(i => i.PurchaseContractSupplementId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var purchase_ContractSupplementEntity = GetPurchase_ContractSupplementEntity(keyValue); 
                db.Delete<Purchase_ContractSupplementEntity>(t=>t.ID == keyValue);
                db.Delete<Purchase_ContractSupplementDetailsEntity>(t=>t.PurchaseContractSupplementId == purchase_ContractSupplementEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_ContractSupplementEntity entity,List<Purchase_ContractSupplementDetailsEntity> purchase_ContractSupplementDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var purchase_ContractSupplementEntityTmp = GetPurchase_ContractSupplementEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Purchase_ContractSupplementDetailsUpdateList= purchase_ContractSupplementDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Purchase_ContractSupplementDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Purchase_ContractSupplementDetailsInserList= purchase_ContractSupplementDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Purchase_ContractSupplementDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.PurchaseContractSupplementId = purchase_ContractSupplementEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Purchase_ContractSupplementDetailsEntity item in purchase_ContractSupplementDetailsList)
                    {
                        item.PurchaseContractSupplementId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
