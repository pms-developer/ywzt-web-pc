﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-17 12:04
    /// 描 述：采购合同增补
    /// </summary>
    public interface PurchaseContractSupplementIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Purchase_ContractSupplementEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Purchase_ContractSupplementDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Purchase_ContractSupplementDetailsEntity> GetPurchase_ContractSupplementDetailsList(string keyValue);
        /// <summary>
        /// 获取Purchase_ContractSupplement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_ContractSupplementEntity GetPurchase_ContractSupplementEntity(string keyValue);
        /// <summary>
        /// 获取Purchase_ContractSupplementDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Purchase_ContractSupplementDetailsEntity GetPurchase_ContractSupplementDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Purchase_ContractSupplementEntity entity,List<Purchase_ContractSupplementDetailsEntity> purchase_ContractSupplementDetailsList,string deleteList,string type);
        void Audit(string keyValue);
        void UnAudit(string keyValue);
        #endregion

    }
}
