﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.PurchaseModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-17 12:04
    /// 描 述：采购合同增补
    /// </summary>
    public class PurchaseContractSupplementBLL : PurchaseContractSupplementIBLL
    {
        private PurchaseContractSupplementService purchaseContractSupplementService = new PurchaseContractSupplementService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractSupplementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return purchaseContractSupplementService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractSupplementDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Purchase_ContractSupplementDetailsEntity> GetPurchase_ContractSupplementDetailsList(string keyValue)
        {
            try
            {
                return purchaseContractSupplementService.GetPurchase_ContractSupplementDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractSupplement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractSupplementEntity GetPurchase_ContractSupplementEntity(string keyValue)
        {
            try
            {
                return purchaseContractSupplementService.GetPurchase_ContractSupplementEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Purchase_ContractSupplementDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Purchase_ContractSupplementDetailsEntity GetPurchase_ContractSupplementDetailsEntity(string keyValue)
        {
            try
            {
                return purchaseContractSupplementService.GetPurchase_ContractSupplementDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                purchaseContractSupplementService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Purchase_ContractSupplementEntity entity, List<Purchase_ContractSupplementDetailsEntity> purchase_ContractSupplementDetailsList, string deleteList, string type)
        {
            try
            {
                purchaseContractSupplementService.SaveEntity(keyValue, entity, purchase_ContractSupplementDetailsList, deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("PurchaseContractSupplementCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void Audit(string keyValue)
        {
            try
            {
                var info = purchaseContractSupplementService.GetPurchase_ContractSupplementEntity(keyValue);
                var details = purchaseContractSupplementService.GetDetails(keyValue);
                if (details.Any())
                {
                    var updateList = new List<Purchase_ContractDetailsEntity>();
                    var insertList = new List<Purchase_ContractDetailsEntity>();
                    var contractBll = new PurchaseContractBLL();
                    var contractId = info.PurchaseContractId;
                    var contractDetails = contractBll.GetPurchase_ContractDetailsList(contractId);
                    var contractInfo = contractBll.GetPurchase_ContractEntity(contractId);
                    var sortCode = 0;
                    foreach (var item in details)
                    {
                        var model = contractDetails.FirstOrDefault(i => item.ProjectConstructionBudgetMaterialsId != null &&
                            i.ProjectConstructionBudgetMaterialsId == item.ProjectConstructionBudgetMaterialsId);
                        if (model != null)
                        {
                            model.ContractQuantity = (model.ContractQuantity ?? 0) + (item.SupplementQuantity ?? 0);
                            model.ContractPrice = item.ContractPrice ?? 0;
                            model.CurrentAllowQuantity = model.ContractQuantity;
                            model.Amount = model.ContractQuantity * model.ContractPrice;
                            model.AfterDiscountPrice = (model.DiscountsPrice * model.ContractQuantity).ToDecimal();

                            updateList.Add(model);
                        }
                        else
                        {
                            sortCode++;
                            sortCode = contractDetails.Count() + sortCode;
                            var entity = new Purchase_ContractDetailsEntity();
                            entity.Create();
                            entity.ContractQuantity = (item.SupplementQuantity ?? 0);
                            entity.ContractPrice = item.ContractPrice ?? 0;
                            entity.ProjectConstructionBudgetMaterialsId = item.ProjectConstructionBudgetMaterialsId;
                            entity.ProjectId = item.ProjectId;
                            entity.ProjectPurchaseApplyDetailsId = item.ProjectPurchaseApplyDetailsId;
                            entity.ProjectPurchaseApplyId = item.ProjectPurchaseApplyId;
                            entity.PurchaseContractId = contractId;
                            entity.Remark = item.Remark;
                            entity.Amount = entity.ContractQuantity * entity.ContractPrice;
                            entity.DiscountsPrice = entity.ContractPrice;
                            entity.AfterDiscountPrice = entity.Amount;

                            var applyInfo = new PurchaseApplyBLL().GetProject_PurchaseApplyDetailsEntity(item.ProjectPurchaseApplyDetailsId);
                            entity.CurrentAllowQuantity = applyInfo.CurrentAllowQuantity;

                            var applyDetailsInfo = new PurchaseApplyBLL().GetProject_PurchaseApplyDetailsEntity(item.ProjectPurchaseApplyDetailsId);
                            entity.Code = applyDetailsInfo.Code;
                            entity.Name = applyDetailsInfo.Name;
                            entity.ModelNumber = applyDetailsInfo.ModelNumber;
                            entity.Brand = applyDetailsInfo.Brand;
                            entity.Unit = applyDetailsInfo.Unit;

                            var budgetInfo = new ProjectConstructionBudgetBLL().GetProject_ConstructionBudgetMaterialsEntity(item.ProjectConstructionBudgetMaterialsId);
                            if (budgetInfo != null)
                                entity.BudgetPrice = budgetInfo.BudgetPrice;

                            entity.SortCode = sortCode;
                            insertList.Add(entity);
                        }
                    }

                    contractBll.UpdateDetails(updateList, insertList);

                    contractDetails = contractBll.GetPurchase_ContractDetailsList(contractId);
                    decimal total = 0;
                    decimal distotal = 0;

                    foreach (var item in contractDetails)
                    {
                        total += (item.Amount ?? 0);
                        distotal += (item.AfterDiscountPrice ?? 0);
                    }
                    contractInfo.SignAmount = total + (contractInfo.Freight ?? 0);
                    contractInfo.DiscountsContractAmount = distotal;
                    contractInfo.DiscountsAmount = total - distotal;

                    contractBll.UpdateEntity(contractInfo);

                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UnAudit(string keyValue)
        {
            try
            {
                var info = purchaseContractSupplementService.GetPurchase_ContractSupplementEntity(keyValue);
                var details = purchaseContractSupplementService.GetDetails(keyValue);
                if (details.Any())
                {
                    var updateList = new List<Purchase_ContractDetailsEntity>();
                    var delList = new List<Purchase_ContractDetailsEntity>();
                    var contractBll = new PurchaseContractBLL();
                    var contractId = info.PurchaseContractId;
                    var contractDetails = contractBll.GetPurchase_ContractDetailsList(contractId);
                    var contractInfo = contractBll.GetPurchase_ContractEntity(contractId);

                    foreach (var item in details)
                    {
                        var model = contractDetails.FirstOrDefault(i => item.ProjectConstructionBudgetMaterialsId != null &&
                            i.ProjectConstructionBudgetMaterialsId == item.ProjectConstructionBudgetMaterialsId);
                        if (model != null)
                        {
                            model.ContractQuantity = (model.ContractQuantity ?? 0) - (item.SupplementQuantity ?? 0);
                            model.ContractPrice = (item.ContractPrice ?? 0);
                            model.CurrentAllowQuantity = model.ContractQuantity;
                            model.Amount = model.ContractQuantity * model.ContractPrice;
                            model.AfterDiscountPrice = (model.DiscountsPrice * model.ContractQuantity).ToDecimal();

                            updateList.Add(model);

                            if (model.ContractQuantity == 0)
                            {
                                delList.Add(model);
                            }
                        }
                        else
                        {
                            var delModel = contractDetails.FirstOrDefault(i => i.ProjectPurchaseApplyDetailsId == item.ProjectPurchaseApplyDetailsId);
                            if (delModel != null)
                                delList.Add(delModel);
                        }
                    }

                    contractBll.UpdateAndDelDetails(updateList, delList);

                    contractDetails = contractBll.GetPurchase_ContractDetailsList(contractId);
                    decimal total = 0;
                    decimal distotal = 0;

                    foreach (var item in contractDetails)
                    {
                        total += (item.Amount ?? 0);
                        distotal += (item.AfterDiscountPrice ?? 0);
                    }
                    contractInfo.SignAmount = total + (contractInfo.Freight ?? 0);
                    contractInfo.DiscountsContractAmount = distotal;
                    contractInfo.DiscountsAmount = total - distotal;

                    contractBll.UpdateEntity(contractInfo);

                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        #endregion

    }
}
