﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.Message
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-02-27 16:27
    /// 描 述：消息策略
    /// </summary>
    public class StrategyInfoService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<MS_StrategyInfoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_Id,
                t.F_StrategyName,
                t.F_StrategyCode,
                t.F_SendRole,
                t.F_MessageType,
                t.F_Description,
                t.CreationDate
                ");
                strSql.Append("  FROM MS_StrategyInfo t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["dateBegin"].IsEmpty() && !queryParam["dateEnd"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["dateBegin"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["dateEnd"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["F_StrategyName"].IsEmpty())
                {
                    dp.Add("F_StrategyName", "%" + queryParam["F_StrategyName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_StrategyName Like @F_StrategyName ");
                }
                if (!queryParam["F_StrategyCode"].IsEmpty())
                {
                    dp.Add("F_StrategyCode", "%" + queryParam["F_StrategyCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.F_StrategyCode Like @F_StrategyCode ");
                }
                //keyword
                if (!queryParam["keyword"].IsEmpty())
                {
                    dp.Add("keyword", "%" + queryParam["keyword"].ToString() + "%", DbType.String);
                    strSql.Append(" AND (t.F_StrategyCode Like @keyword or F_StrategyName like @keyword ) ");
                }
                return this.BaseRepository().FindList<MS_StrategyInfoEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取MS_StrategyInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public MS_StrategyInfoEntity GetMS_StrategyInfoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<MS_StrategyInfoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<MS_StrategyInfoEntity>(t=>t.F_Id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, MS_StrategyInfoEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
