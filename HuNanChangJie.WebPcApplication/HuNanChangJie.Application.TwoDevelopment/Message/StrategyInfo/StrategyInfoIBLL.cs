﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.Message
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-02-27 16:27
    /// 描 述：消息策略
    /// </summary>
    public interface StrategyInfoIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<MS_StrategyInfoEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取MS_StrategyInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        MS_StrategyInfoEntity GetMS_StrategyInfoEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, MS_StrategyInfoEntity entity);
        #endregion

    }
}
