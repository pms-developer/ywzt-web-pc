﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.Message
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-02-27 16:27
    /// 描 述：消息策略
    /// </summary>
    public class MS_StrategyInfoEntity 
    {
        #region  实体成员
        /// <summary>
        /// 策略主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 策略名称
        /// </summary>
        [Column("F_STRATEGYNAME")]
        public string F_StrategyName { get; set; }
        /// <summary>
        /// 策略编码
        /// </summary>
        [Column("F_STRATEGYCODE")]
        public string F_StrategyCode { get; set; }
        /// <summary>
        /// 发送角色
        /// </summary>
        [Column("F_SENDROLE")]
        public string F_SendRole { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        [Column("F_MESSAGETYPE")]
        public string F_MessageType { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建用户主键
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        [Column("ModificationDate")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改用户主键
        /// </summary>
        [Column("Modification_Id")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改用户
        /// </summary>
        [Column("ModificationName")]
        public string ModificationName { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

