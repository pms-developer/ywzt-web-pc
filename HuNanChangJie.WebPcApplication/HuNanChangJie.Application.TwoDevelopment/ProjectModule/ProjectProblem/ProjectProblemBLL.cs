﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-28 19:56
    /// 描 述：问题反馈
    /// </summary>
    public class ProjectProblemBLL : ProjectProblemIBLL
    {
        private ProjectProblemService projectProblemService = new ProjectProblemService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ProblemEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectProblemService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_Problem表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ProblemEntity GetProject_ProblemEntity(string keyValue)
        {
            try
            {
                return projectProblemService.GetProject_ProblemEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectProblemService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_ProblemEntity entity, string deleteList, string type)
        {
            try
            {
                projectProblemService.SaveEntity(keyValue, entity, deleteList, type);
                BaseMessageIBLL basebll = new BaseMessageBLL();
                var userList = new List<string>();
                if (!string.IsNullOrWhiteSpace(entity.SendTo))
                {
                    userList.Add(entity.SendTo);
                }
                if (!string.IsNullOrWhiteSpace(entity.CopytTos))
                {
                    if (entity.CopytTos.Contains(","))
                    {
                        userList.AddRange(entity.CopytTos.Split(','));
                    }
                    else
                    {
                        userList.Add(entity.CopytTos);
                    }
                }
                var title = $"【{entity.ProjectName}】的【{entity.Title}】问题反馈";
                var url = "ProjectModule/ProjectProblem/Form";
                basebll.SaveMessage(userList, keyValue, SystemMessageType.Problems, title,url);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("ProjectProblemCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 关闭问题反馈
        /// </summary>
        /// <param name="keyValue"></param>
        public void Close(string keyValue)
        {
            try
            {
                projectProblemService.Close(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
