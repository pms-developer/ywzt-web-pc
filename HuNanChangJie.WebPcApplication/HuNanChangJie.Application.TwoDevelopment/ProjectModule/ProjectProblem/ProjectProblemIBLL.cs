﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-28 19:56
    /// 描 述：问题反馈
    /// </summary>
    public interface ProjectProblemIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_ProblemEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_Problem表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ProblemEntity GetProject_ProblemEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_ProblemEntity entity,string deleteList,string type);

        /// <summary>
        /// 关闭问题反馈
        /// </summary>
        /// <param name="keyValue"></param>
        void Close(string keyValue);
        #endregion

    }
}
