﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-23 11:52
    /// 描 述：分包变更
    /// </summary>
    public class SubcontractChangeService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractChangeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.ProjectSubcontractId,
                t.Creation_Id,
                t.SortCode,
                t.ChangeType,
                t.ChangeAmount,
                t.ChangeDate,
                t.AfterAmount,
                t.ProjectName,
                t.OparetorId,
                t.OparetorDepartmentId,
                t.AuditStatus,
                t.Workflow_ID,
                t.Abstract,
                t.CreationDate,
                b.Name as SubcontractName
                ");
                strSql.Append("  FROM Project_SubcontractChange t left join Project_Subcontract as b on t.ProjectSubcontractId=b.Id");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();

                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID");
                }
                var list=this.BaseRepository().FindList<Project_SubcontractChangeEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChangeQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractChangeQuantitiesEntity> GetProject_SubcontractChangeQuantitiesList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.ChangePrice,
                            b.ChangeQuantities,
                            b.ChangeTotalPrice,
                            b.Price as InitPrice,
                            b.Quantities as InitQuantities,
                            b.TotalPrice as InitTotalPrice
                             from Project_SubcontractChangeQuantities as a left join Project_SubcontractQuantities as b on
                            a.ProjectSubcontractQuantitiesId=b.ID ");
                sb.Append(" where a.ProjectSubcontractChangeId=@keyValue");
                var list= this.BaseRepository().FindList<Project_SubcontractChangeQuantitiesEntity>(sb.ToString(),new { keyValue});
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public IEnumerable<CB_Leasehold> GetCB_LeaseholdList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select * from CB_Leasehold where 1=1  ");
                sb.Append("  and  ID=@keyValue");
                var list = this.BaseRepository().FindList<CB_Leasehold>(sb.ToString(), new { keyValue });
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChangeMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractChangeMaterialsEntity> GetProject_SubcontractChangeMaterialsList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.ChangePrice,
                            b.ChangeQuantity,
                            b.ChangeTotalPrice,
                            b.Price as InitPrice,
                            b.Quantity as InitQuantity,
                            b.TotalPrice as InitTotalPrice
                            from Project_SubcontractChangeMaterials as a left join Project_SubcontractMaterials as b on
                            a.ProjectSubcontactMaterialsId=b.ID");
                sb.Append(" where a.ProjectSubcontractChangeId=@keyValue");
                var list= this.BaseRepository().FindList<Project_SubcontractChangeMaterialsEntity>(sb.ToString(),new { keyValue});
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractChangeEntity GetProject_SubcontractChangeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractChangeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChangeMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractChangeMaterialsEntity GetProject_SubcontractChangeMaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractChangeMaterialsEntity>(t=>t.ProjectSubcontractChangeId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChangeQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractChangeQuantitiesEntity GetProject_SubcontractChangeQuantitiesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractChangeQuantitiesEntity>(t=>t.ProjectSubcontractChangeId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_SubcontractChangeEntity = GetProject_SubcontractChangeEntity(keyValue); 
                db.Delete<Project_SubcontractChangeEntity>(t=>t.ID == keyValue);
                db.Delete<Project_SubcontractChangeMaterialsEntity>(t=>t.ProjectSubcontractChangeId == project_SubcontractChangeEntity.ID);
                db.Delete<Project_SubcontractChangeQuantitiesEntity>(t=>t.ProjectSubcontractChangeId == project_SubcontractChangeEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SubcontractChangeEntity entity,List<Project_SubcontractChangeMaterialsEntity> project_SubcontractChangeMaterialsList,List<Project_SubcontractChangeQuantitiesEntity> project_SubcontractChangeQuantitiesList, List<CB_Leasehold> CB_LeaseholdList, string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_SubcontractChangeEntityTmp = GetProject_SubcontractChangeEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_SubcontractChangeMaterialsUpdateList= project_SubcontractChangeMaterialsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_SubcontractChangeMaterialsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_SubcontractChangeMaterialsInserList= project_SubcontractChangeMaterialsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_SubcontractChangeMaterialsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectSubcontractChangeId = project_SubcontractChangeEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码 
                    if (CB_LeaseholdList != null)
                    {
                        var CB_LeaseholdUpdateList = CB_LeaseholdList.FindAll(i => i.EditType == EditType.Update);
                        foreach (var item in CB_LeaseholdUpdateList)
                        {
                            db.Update(item);
                        }
                        var CB_LeaseholdInserList = CB_LeaseholdList.FindAll(i => i.EditType == EditType.Add);
                        foreach (var item in CB_LeaseholdInserList)
                        {
                            item.Create(item.ID);
                            item.ProjectID = project_SubcontractChangeEntityTmp.ID;
                            db.Insert(item);
                        }
                    }
                    else
                    {
                        entity.Create(keyValue);
                        db.Insert(entity);
                    }
                        //没有生成代码1 

                        //没有生成代码 
                        var Project_SubcontractChangeQuantitiesUpdateList= project_SubcontractChangeQuantitiesList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_SubcontractChangeQuantitiesUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_SubcontractChangeQuantitiesInserList= project_SubcontractChangeQuantitiesList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_SubcontractChangeQuantitiesInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectSubcontractChangeId = project_SubcontractChangeEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_SubcontractChangeMaterialsEntity item in project_SubcontractChangeMaterialsList)
                    {
                        item.ProjectSubcontractChangeId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Project_SubcontractChangeQuantitiesEntity item in project_SubcontractChangeQuantitiesList)
                    {
                        item.ProjectSubcontractChangeId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (CB_Leasehold item in CB_LeaseholdList)
                    {
                        item.ID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion


    }
}
