﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-23 11:52
    /// 描 述：分包变更
    /// </summary>
    public class SubcontractChangeBLL : SubcontractChangeIBLL
    {
        private SubcontractChangeService subcontractChangeService = new SubcontractChangeService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractChangeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return subcontractChangeService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChangeQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractChangeQuantitiesEntity> GetProject_SubcontractChangeQuantitiesList(string keyValue)
        {
            try
            {
                return subcontractChangeService.GetProject_SubcontractChangeQuantitiesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<CB_Leasehold> GetCB_LeaseholdList(string keyValue)
        {
            try
            {
                return subcontractChangeService.GetCB_LeaseholdList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChangeMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractChangeMaterialsEntity> GetProject_SubcontractChangeMaterialsList(string keyValue)
        {
            try
            {
                return subcontractChangeService.GetProject_SubcontractChangeMaterialsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractChangeEntity GetProject_SubcontractChangeEntity(string keyValue)
        {
            try
            {
                return subcontractChangeService.GetProject_SubcontractChangeEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChangeMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractChangeMaterialsEntity GetProject_SubcontractChangeMaterialsEntity(string keyValue)
        {
            try
            {
                return subcontractChangeService.GetProject_SubcontractChangeMaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractChangeQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractChangeQuantitiesEntity GetProject_SubcontractChangeQuantitiesEntity(string keyValue)
        {
            try
            {
                return subcontractChangeService.GetProject_SubcontractChangeQuantitiesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                subcontractChangeService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SubcontractChangeEntity entity,List<Project_SubcontractChangeMaterialsEntity> project_SubcontractChangeMaterialsList,List<Project_SubcontractChangeQuantitiesEntity> project_SubcontractChangeQuantitiesList,List<CB_Leasehold> CB_LeaseholdList, string deleteList,string type)
        {
            try
            {
                subcontractChangeService.SaveEntity(keyValue, entity,project_SubcontractChangeMaterialsList,project_SubcontractChangeQuantitiesList, CB_LeaseholdList,deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("ProjectSubcontractChangeCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void Audit(string keyValue)
        {
            try
            {
                var mainInfo = subcontractChangeService.GetProject_SubcontractChangeEntity(keyValue);
                var contractBll = new ProjectSubcontractBLL();
                var contractInfo = contractBll.GetProject_SubcontractEntity(mainInfo.ProjectSubcontractId);

                var mainGCList = subcontractChangeService.GetProject_SubcontractChangeQuantitiesList(keyValue);
                var mainCLList = subcontractChangeService.GetProject_SubcontractChangeMaterialsList(keyValue);

                var contractGCList = contractBll.GetProject_SubcontractQuantitiesList(contractInfo.ID);
                var contractCLList = contractBll.GetProject_SubcontractMaterialsList(contractInfo.ID);

                var gclList = new List<Project_SubcontractQuantitiesEntity>();
                var clList = new List<Project_SubcontractMaterialsEntity>();

                Decimal total = 0;
                foreach (var item in mainGCList)
                {
                    var info = contractGCList.FirstOrDefault(i=>i.ID==item.ProjectSubcontractQuantitiesId);
                    if (info == null) continue;
                    total += (item.TotalPrice ?? 0);
                    info.ChangeQuantities = (info.ChangeQuantities ?? 0) + (item.Quantities ?? 0);
                    info.ChangeTotalPrice = (info.ChangeTotalPrice ?? 0) + (item.TotalPrice ?? 0);
                    if (info.ChangeQuantities > 0)
                        info.ChangePrice = info.ChangeTotalPrice / info.ChangeQuantities;
                    else
                        info.ChangePrice = 0;

                    gclList.Add(info);
                }

                foreach (var item in mainCLList)
                {
                    var info = contractCLList.FirstOrDefault(i => i.ID == item.ProjectSubcontactMaterialsId);
                    if (info == null) continue;
                    info.ChangeQuantity = (info.ChangeQuantity ?? 0) + (item.Quantity ?? 0);
                    info.ChangeTotalPrice = (info.ChangeTotalPrice ?? 0) + (item.TotalPrice ?? 0);
                    if (info.ChangeQuantity > 0)
                        info.ChangePrice = info.ChangeTotalPrice / info.ChangeQuantity;
                    else
                        info.ChangePrice = 0;

                    clList.Add(info);
                }
                contractInfo.ChangeAmount = (contractInfo.ChangeAmount ?? 0) + (mainInfo.ChangeAmount??0);
                contractInfo.TotalAmount = (contractInfo.TotalAmount ?? 0) + (mainInfo.ChangeAmount ?? 0);

                contractBll.Update(contractInfo, gclList, clList);

            }
            catch(Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UnAudit(string keyValue)
        {
            try
            {
                var mainInfo = subcontractChangeService.GetProject_SubcontractChangeEntity(keyValue);
                var contractBll = new ProjectSubcontractBLL();
                var contractInfo = contractBll.GetProject_SubcontractEntity(mainInfo.ProjectSubcontractId);

                var mainGCList = subcontractChangeService.GetProject_SubcontractChangeQuantitiesList(keyValue);
                var mainCLList = subcontractChangeService.GetProject_SubcontractChangeMaterialsList(keyValue);

                var contractGCList = contractBll.GetProject_SubcontractQuantitiesList(contractInfo.ID);
                var contractCLList = contractBll.GetProject_SubcontractMaterialsList(contractInfo.ID);

                var gclList = new List<Project_SubcontractQuantitiesEntity>();
                var clList = new List<Project_SubcontractMaterialsEntity>();

                Decimal total = 0;
                foreach (var item in mainGCList)
                {
                    var info = contractGCList.FirstOrDefault(i => i.ID == item.ProjectSubcontractQuantitiesId);
                    if (info == null) continue;
                    total += (item.TotalPrice ?? 0);
                    info.ChangeQuantities = (info.ChangeQuantities ?? 0) - (item.Quantities ?? 0);
                    info.ChangeTotalPrice = (info.ChangeTotalPrice ?? 0) - (item.TotalPrice ?? 0);
                    if (info.ChangeQuantities > 0)
                        info.ChangePrice = info.ChangeTotalPrice / info.ChangeQuantities;
                    else
                        info.ChangePrice = 0;

                    gclList.Add(info);
                }

                foreach (var item in mainCLList)
                {
                    var info = contractCLList.FirstOrDefault(i => i.ID == item.ProjectSubcontactMaterialsId);
                    if (info == null) continue;
                    info.ChangeQuantity = (info.ChangeQuantity ?? 0) - (item.Quantity ?? 0);
                    info.ChangeTotalPrice = (info.ChangeTotalPrice ?? 0) - (item.TotalPrice ?? 0);
                    if (info.ChangeQuantity > 0)
                        info.ChangePrice = info.ChangeTotalPrice / info.ChangeQuantity;
                    else
                        info.ChangePrice = 0;

                    clList.Add(info);
                }
                contractInfo.ChangeAmount = (contractInfo.ChangeAmount ?? 0) - (mainInfo.ChangeAmount ?? 0);
                contractInfo.TotalAmount = (contractInfo.TotalAmount ?? 0) - (mainInfo.ChangeAmount ?? 0);

                contractBll.Update(contractInfo, gclList, clList);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public decimal? GetTotalAmount(string ProjectID) {
            try
            {
               
                var contractBll = new ProjectSubcontractBLL();
                return contractBll.GetProject_ContractTotalAmount(ProjectID);
            }
            catch (Exception ex)
            {

                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
         
        }


        #endregion

    }
}
