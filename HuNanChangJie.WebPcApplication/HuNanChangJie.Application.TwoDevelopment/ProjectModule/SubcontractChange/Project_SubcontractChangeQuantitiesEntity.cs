﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-23 11:52
    /// 描 述：分包变更
    /// </summary>
    public class Project_SubcontractChangeQuantitiesEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 分包合同工程量清单ID
        /// </summary>
        [Column("PROJECTSUBCONTRACTQUANTITIESID")]
        public string ProjectSubcontractQuantitiesId { get; set; }
        /// <summary>
        /// 分包合同变更ID
        /// </summary>
        [Column("PROJECTSUBCONTRACTCHANGEID")]
        public string ProjectSubcontractChangeId { get; set; }
        /// <summary>
        /// 清单编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 清单名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 项目特征
        /// </summary>
        [Column("FEATURE")]
        public string Feature { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 本次变更工程量
        /// </summary>
        [Column("QUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantities { get; set; }
        /// <summary>
        /// 本次变更综合单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
        /// <summary>
        /// 本次变更合价
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// 本次变更后工程量
        /// </summary>
        [Column("AFTERQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterQuantities { get; set; }
        /// <summary>
        /// 本次变更后综合单价
        /// </summary>
        [Column("AFTERPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterPrice { get; set; }
        /// <summary>
        /// 本次变更后合价
        /// </summary>
        [Column("AFTERTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterTotalPrice { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// SortCode
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region 扩展属性
        [NotMapped]
        public decimal? InitPrice { get; set; }

        [NotMapped]
        public decimal? InitQuantities { get; set; }

        [NotMapped]
        public decimal? InitTotalPrice { get; set; }

        [NotMapped]
        public decimal? ChangePrice { get; set; }

        [NotMapped]
        public decimal? ChangeQuantities { get; set; }

        [NotMapped]
        public decimal? ChangeTotalPrice { get; set; }
        #endregion
    }
}

