﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-23 11:52
    /// 描 述：分包变更
    /// </summary>
    public interface SubcontractChangeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_SubcontractChangeEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_SubcontractChangeQuantities表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_SubcontractChangeQuantitiesEntity> GetProject_SubcontractChangeQuantitiesList(string keyValue);


        IEnumerable<CB_Leasehold> GetCB_LeaseholdList(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractChangeMaterials表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_SubcontractChangeMaterialsEntity> GetProject_SubcontractChangeMaterialsList(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractChange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractChangeEntity GetProject_SubcontractChangeEntity(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractChangeMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractChangeMaterialsEntity GetProject_SubcontractChangeMaterialsEntity(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractChangeQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractChangeQuantitiesEntity GetProject_SubcontractChangeQuantitiesEntity(string keyValue);
        /// <summary>
        /// 获取当前工程合同总价格
        /// </summary>
        /// <param name="ProjectID"></param>
        /// <returns></returns>
        decimal? GetTotalAmount(string ProjectID);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_SubcontractChangeEntity entity,List<Project_SubcontractChangeMaterialsEntity> project_SubcontractChangeMaterialsList,List<Project_SubcontractChangeQuantitiesEntity> project_SubcontractChangeQuantitiesList,List<CB_Leasehold> CB_LeaseholdList, string deleteList,string type);

        void Audit(string keyValue);

        void UnAudit(string keyValue);
        #endregion

    }
}
