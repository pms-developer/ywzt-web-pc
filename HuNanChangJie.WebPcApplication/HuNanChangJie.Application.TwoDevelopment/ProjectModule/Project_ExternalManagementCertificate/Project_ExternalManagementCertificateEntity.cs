﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-14 15:29
    /// 描 述：外出经营管理许可证
    /// </summary>
    public class Project_ExternalManagementCertificateEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 项目类型
        /// </summary>
        [Column("PROJECTTYPE")]
        public string ProjectType { get; set; }
        /// <summary>
        /// 项目负责人
        /// </summary>
        [Column("LEAD")]
        public string Lead { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("PHONE")]
        public string Phone { get; set; }
        /// <summary>
        /// 建设方名称
        /// </summary>
        [Column("BUILDER")]
        public string Builder { get; set; }
        /// <summary>
        /// 项目地址
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 合同金额
        /// </summary>
        [Column("CONTRACTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? ContractAmount { get; set; }
        /// <summary>
        /// 开票税率
        /// </summary>
        [Column("RATE")]
        public string Rate { get; set; }
        /// <summary>
        /// 预缴税率
        /// </summary>
        [Column("ADVANCEPAYMENTRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? AdvancePaymentRate { get; set; }
        /// <summary>
        /// 工期
        /// </summary>
        [Column("TIMELIMIT")]
        public int? TimeLimit { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 开具日期
        /// </summary>
        [Column("OPENINGDATE")]
        public DateTime? OpeningDate { get; set; }
        /// <summary>
        /// 到期日期
        /// </summary>
        [Column("EXPIREDATE")]
        public DateTime? ExpireDate { get; set; }
        /// <summary>
        /// 是否注销
        /// </summary>
        [Column("ISCANCELLATION")]
        public bool? IsCancellation { get; set; }
        /// <summary>
        /// 延长日期开始
        /// </summary>
        [Column("EXTENDSTARTDATE")]
        public DateTime? ExtendStartDate { get; set; }
        /// <summary>
        /// 延长日期结束
        /// </summary>
        [Column("EXTENDENDDATE")]
        public DateTime? ExtendEndDate { get; set; }
        /// <summary>
        /// 注销日期
        /// </summary>
        [Column("CANCELLATIONDATE")]
        public DateTime? CancellationDate { get; set; }
        /// <summary>
        /// 已开具发票金额
        /// </summary>
        [Column("INVOICEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? InvoiceAmount { get; set; }
        /// <summary>
        /// 已抵减分包金额
        /// </summary>
        [Column("SUBCONTRACTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? SubcontractAmount { get; set; }
        /// <summary>
        /// 已预缴税款金额
        /// </summary>
        [Column("ADVANCEPAYMENTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? AdvancePaymentAmount { get; set; }
        /// <summary>
        /// 其它金额
        /// </summary>
        [Column("OTHERAMOUNT1")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherAmount1 { get; set; }
        /// <summary>
        /// 其它金额
        /// </summary>
        [Column("OTHERAMOUNT2")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherAmount2 { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 摘要
        /// </summary>
        [Column("ABSTRACT")]
        public string Abstract { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

