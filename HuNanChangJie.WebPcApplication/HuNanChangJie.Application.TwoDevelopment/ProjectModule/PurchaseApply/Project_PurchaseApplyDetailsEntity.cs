﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-10 15:13
    /// 描 述：采购申请
    /// </summary>
    public class Project_PurchaseApplyDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 采购申请单ID
        /// </summary>
        [Column("PROJECTPURCHASEAPPLYID")]
        public string ProjectPurchaseApplyId { get; set; }
        /// <summary>
        /// 施工预算材料清单ID
        /// </summary>
        [Column("PROJECTCONSTRUCTIONBUDGETMATERIALSID")]
        public string ProjectConstructionBudgetMaterialsId { get; set; }

        /// <summary>
        /// 材料库材料ID
        /// </summary>
        [Column("FK_BASEMATERIALSID")]
        public string Fk_BaseMaterialsId { get; set; }
        /// <summary>
        /// 申请数量
        /// </summary>
        [Column("APPLYQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? ApplyQuantity { get; set; }
        /// <summary>
        /// 签定合同数量
        /// </summary>
        [Column("CONTRACTQUNTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? ContractQuntity { get; set; }
        /// <summary>
        /// 清单单价
        /// </summary>
        [Column("LISTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ListPrice { get; set; }
        /// <summary>
        /// 需求日期
        /// </summary>
        [Column("NEEDDATE")]
        public DateTime? NeedDate { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// 材料来源
        /// </summary>
        [Column("MATERIALSSOURCE")]
        public bool? MaterialsSource { get; set; }

        /// <summary>
        /// 清单编码
        /// </summary>
        [Column("LISTCODE")]
        public string ListCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODELNUMBER")]
        public string ModelNumber { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }

        /// <summary>
        /// 材料编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 本次申请总额
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? ApplyTotal { get; set; }

        /// <summary>
        /// 当前允许采购数量
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? CurrentAllowQuantity { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            CurrentAllowQuantity = this.ApplyQuantity;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            CurrentAllowQuantity = this.ApplyQuantity;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

