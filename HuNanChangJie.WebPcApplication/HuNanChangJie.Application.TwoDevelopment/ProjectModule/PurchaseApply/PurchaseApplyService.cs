﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-10 15:13
    /// 描 述：采购申请
    /// </summary>
    public class PurchaseApplyService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_PurchaseApplyEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
(select bd.F_FullName from Base_Department as bd where bd.F_DepartmentId=t.ApplyDepartmentId ) as ApplyDepartmentName,
                t.Code,
               t.Name,
                t.ApplyUserId,
                t.ProjectName,
                t.ApplyDate,
                t.AuditStatus,
                t.Workflow_ID,
                t.Abstract,
                t.CreationDate
                ");
                strSql.Append("  FROM Project_PurchaseApply t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID  ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.ApplyDate >= @startTime AND t.ApplyDate <= @endTime ) ");
                }
                var list = this.BaseRepository().FindList<Project_PurchaseApplyEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_PurchaseApplyDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_PurchaseApplyDetailsEntity> GetProject_PurchaseApplyDetailsList(string keyValue)
        {
            try
            {
                #region MyRegion
                //var strSql = new StringBuilder();
                //strSql.Append("SELECT ");
                //strSql.Append(@"
                //a.*,
                //b.Quantity,
                //b.BudgetPrice Price,
                //b.ApplyQuantity AppliedQuantity,
                //Quantity - ISNULL (b.ApplyQuantity,0) AllowQuantity                
                //");
                //strSql.Append(" from Project_PurchaseApplyDetails a ");
                //strSql.Append(" left join Project_ConstructionBudgetMaterials b on a.ProjectConstructionBudgetMaterialsId=b.ID");
                //strSql.Append(" WHERE 1=1 ");
                //// 虚拟参数
                //var dp = new DynamicParameters(new { });
                //if (!keyValue.IsEmpty())
                //{
                //    dp.Add("ProjectPurchaseApplyId", keyValue, DbType.String);
                //    strSql.Append(" AND ProjectPurchaseApplyId = @ProjectPurchaseApplyId  ");
                //}
                //var list = this.BaseRepository().FindList<Project_PurchaseApplyDetailsEntity>(strSql.ToString(), dp);
                #endregion

                var list = this.BaseRepository().FindList<Project_PurchaseApplyDetailsEntity>(t => t.ProjectPurchaseApplyId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_PurchaseApply表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_PurchaseApplyEntity GetProject_PurchaseApplyEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_PurchaseApplyEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_PurchaseApplyDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_PurchaseApplyDetailsEntity GetProject_PurchaseApplyDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_PurchaseApplyDetailsEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_PurchaseApplyEntity = GetProject_PurchaseApplyEntity(keyValue);
                db.Delete<Project_PurchaseApplyEntity>(t => t.ID == keyValue);
                db.Delete<Project_PurchaseApplyDetailsEntity>(t => t.ProjectPurchaseApplyId == project_PurchaseApplyEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_PurchaseApplyEntity entity, List<Project_PurchaseApplyDetailsEntity> project_PurchaseApplyDetailsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var project_PurchaseApplyEntityTmp = GetProject_PurchaseApplyEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Project_PurchaseApplyDetailsUpdateList = project_PurchaseApplyDetailsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_PurchaseApplyDetailsUpdateList)
                    {
                        item.CurrentAllowQuantity = item.ApplyQuantity;
                        db.Update(item);
                    }
                    var Project_PurchaseApplyDetailsInserList = project_PurchaseApplyDetailsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_PurchaseApplyDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectPurchaseApplyId = project_PurchaseApplyEntityTmp.ID;
                        item.CurrentAllowQuantity = item.ApplyQuantity;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_PurchaseApplyDetailsEntity item in project_PurchaseApplyDetailsList)
                    {
                        item.ProjectPurchaseApplyId = entity.ID;
                        item.CurrentAllowQuantity = item.ApplyQuantity;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_PurchaseApplyDetailsEntity> GetDetails(IEnumerable<string> detailsIdList)
        {
            try
            {
                return BaseRepository().FindList<Project_PurchaseApplyDetailsEntity>(i => detailsIdList.Contains(i.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
