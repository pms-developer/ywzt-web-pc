﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-10 15:13
    /// 描 述：采购申请
    /// </summary>
    public interface PurchaseApplyIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_PurchaseApplyEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_PurchaseApplyDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_PurchaseApplyDetailsEntity> GetProject_PurchaseApplyDetailsList(string keyValue);
        /// <summary>
        /// 获取Project_PurchaseApply表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_PurchaseApplyEntity GetProject_PurchaseApplyEntity(string keyValue);
        /// <summary>
        /// 获取Project_PurchaseApplyDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_PurchaseApplyDetailsEntity GetProject_PurchaseApplyDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_PurchaseApplyEntity entity,List<Project_PurchaseApplyDetailsEntity> project_PurchaseApplyDetailsList,string deleteList,string type);
        void Audit(string keyValue);
        void UnAudit(string keyValue);
        #endregion

    }
}
