﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-10 15:13
    /// 描 述：采购申请
    /// </summary>
    public class PurchaseApplyBLL : PurchaseApplyIBLL
    {
        private PurchaseApplyService purchaseApplyService = new PurchaseApplyService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_PurchaseApplyEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return purchaseApplyService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_PurchaseApplyDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_PurchaseApplyDetailsEntity> GetProject_PurchaseApplyDetailsList(string keyValue)
        {
            try
            {
                return purchaseApplyService.GetProject_PurchaseApplyDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_PurchaseApply表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_PurchaseApplyEntity GetProject_PurchaseApplyEntity(string keyValue)
        {
            try
            {
                return purchaseApplyService.GetProject_PurchaseApplyEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_PurchaseApplyDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_PurchaseApplyDetailsEntity GetProject_PurchaseApplyDetailsEntity(string keyValue)
        {
            try
            {
                return purchaseApplyService.GetProject_PurchaseApplyDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                purchaseApplyService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_PurchaseApplyEntity entity,List<Project_PurchaseApplyDetailsEntity> project_PurchaseApplyDetailsList,string deleteList,string type)
        {
            try
            {
                purchaseApplyService.SaveEntity(keyValue, entity,project_PurchaseApplyDetailsList,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("PurchaseApplyCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void Audit(string keyValue)
        {
            try
            {
                var materialsList = new List<Project_ConstructionBudgetMaterialsEntity>();

                var budgetBll = new ProjectConstructionBudgetBLL();

                var materials = purchaseApplyService.GetProject_PurchaseApplyDetailsList(keyValue);
                if (materials.Any())
                {
                    var projectId = materials.First().ProjectId;
                    var infoList = budgetBll.GetMaterialsListByProjectId(projectId);
                    foreach (var model in materials)
                    {
                        var info = infoList.FirstOrDefault(i=>i.ID==model.ProjectConstructionBudgetMaterialsId);
                        if (info == null) continue;
                        info.ApplyQuantity = (info.ApplyQuantity ?? 0) + model.ApplyQuantity;
                        info.ApplyTotalPrice=(info.ApplyTotalPrice??0)+(model.ApplyTotal??0);
                        if (info.ApplyQuantity > 0)
                        {
                            info.ApplyPrice = info.ApplyTotalPrice / info.ApplyQuantity;
                        }

                        materialsList.Add(info);
                    }

                }
                budgetBll.UpdateMaterials(materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public void UnAudit(string keyValue)
        {
            try
            {
                var materialsList = new List<Project_ConstructionBudgetMaterialsEntity>();

                var budgetBll = new ProjectConstructionBudgetBLL();

                var materials = purchaseApplyService.GetProject_PurchaseApplyDetailsList(keyValue);
                if (materials.Any())
                {
                    var projectId = materials.First().ProjectId;
                    var infoList = budgetBll.GetMaterialsListByProjectId(projectId);
                    foreach (var model in materials)
                    {
                        var info = infoList.FirstOrDefault(i => i.ID == model.ProjectConstructionBudgetMaterialsId);
                        if (info == null) continue;
                        info.ApplyQuantity = (info.ApplyQuantity ?? 0) - model.ApplyQuantity;
                        info.ApplyTotalPrice = (info.ApplyTotalPrice ?? 0) - (model.ApplyTotal ?? 0);
                        if (info.ApplyQuantity > 0)
                        {
                            info.ApplyPrice = info.ApplyTotalPrice / info.ApplyQuantity;
                        }

                        materialsList.Add(info);
                    }

                }
                budgetBll.UpdateMaterials(materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        public IEnumerable<Project_PurchaseApplyDetailsEntity> GetDetails(IEnumerable<string> detailsIdList)
        {
            try
            {
                return purchaseApplyService.GetDetails(detailsIdList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowBusinessException(ex);
            }
        }

        #endregion

    }
}
