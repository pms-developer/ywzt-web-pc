﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-01 15:02
    /// 描 述：简历处理
    /// </summary>
    public class zhibinJianliBLL : zhibinJianliIBLL
    {
        private zhibinJianliService zhibinJianliService = new zhibinJianliService();

        #region  获取数据

        /// <summary>
        /// 查询合同编号是否唯一
        /// </summary>
        /// <param name="no"></param>
        /// <returns></returns>
        public int checkNoCount(string no)
        {
            int num = zhibinJianliService.getNoCount(no);
            return num;
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<zhibin_jianliEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return zhibinJianliService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取zhibin_j_desc表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<zhibin_j_descEntity> Getzhibin_j_descList(string keyValue)
        {
            try
            {
                return zhibinJianliService.Getzhibin_j_descList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取zhibin_jianli表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public zhibin_jianliEntity Getzhibin_jianliEntity(string keyValue)
        {
            try
            {
                return zhibinJianliService.Getzhibin_jianliEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取zhibin_j_desc表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public zhibin_j_descEntity Getzhibin_j_descEntity(string keyValue)
        {
            try
            {
                return zhibinJianliService.Getzhibin_j_descEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                zhibinJianliService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, zhibin_jianliEntity entity,List<zhibin_j_descEntity> zhibin_j_descList,string deleteList,string type)
        {
            try
            {
                zhibinJianliService.SaveEntity(keyValue, entity, zhibin_j_descList, deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("zhibinCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
