﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-01 15:02
    /// 描 述：简历处理
    /// </summary>
    public interface zhibinJianliIBLL
    {
        #region  获取数据

        /// <summary>
        /// 查询合同编号是否唯一
        /// </summary>
        /// <param name="no"></param>
        /// <returns></returns>
        int checkNoCount(string no);

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<zhibin_jianliEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取zhibin_j_desc表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<zhibin_j_descEntity> Getzhibin_j_descList(string keyValue);
        /// <summary>
        /// 获取zhibin_jianli表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        zhibin_jianliEntity Getzhibin_jianliEntity(string keyValue);
        /// <summary>
        /// 获取zhibin_j_desc表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        zhibin_j_descEntity Getzhibin_j_descEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, zhibin_jianliEntity entity,List<zhibin_j_descEntity> zhibin_j_descList,string deleteList,string type);
        #endregion

    }
}
