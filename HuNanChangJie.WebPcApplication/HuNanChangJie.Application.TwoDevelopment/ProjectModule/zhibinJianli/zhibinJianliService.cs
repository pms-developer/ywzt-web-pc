﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2024-04-01 15:02
    /// 描 述：简历处理
    /// </summary>
    public class zhibinJianliService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 查询编号是否存在
        /// </summary>
        /// <returns></returns>
        public int getNoCount(string no)
        {
            string sql = "select count(1) as c from [dbo].[zhibin_jianli] where jl_no='" + no + "'";
            DataTable dt = this.BaseRepository().FindTable(sql);
            return int.Parse(dt.Rows[0]["c"].ToString());
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<zhibin_jianliEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.jl_no,
                t.name,
                t.age,
                t.phone,
                t.job_search,
                t.salary_expectation,
                t.city,
                t.work_experience,
                t.birthday
                ");
                strSql.Append("  FROM zhibin_jianli t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["name"].IsEmpty())
                {
                    dp.Add("name", "%" + queryParam["name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.name Like @name ");
                }
                if (!queryParam["age"].IsEmpty())
                {
                    dp.Add("age", "%" + queryParam["age"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.age Like @age ");
                }
                if (!queryParam["phone"].IsEmpty())
                {
                    dp.Add("phone", "%" + queryParam["phone"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.phone Like @phone ");
                }
                if (!queryParam["job_search"].IsEmpty())
                {
                    dp.Add("job_search", "%" + queryParam["job_search"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.job_search Like @job_search ");
                }
                if (!queryParam["salary_expectation"].IsEmpty())
                {
                    dp.Add("salary_expectation",queryParam["salary_expectation"].ToString(), DbType.String);
                    strSql.Append(" AND t.salary_expectation = @salary_expectation ");
                }
                if (!queryParam["city"].IsEmpty())
                {
                    dp.Add("city", "%" + queryParam["city"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.city Like @city ");
                }
                if (!queryParam["birthday"].IsEmpty())
                {
                    dp.Add("birthday",queryParam["birthday"].ToString(), DbType.String);
                    strSql.Append(" AND t.birthday = @birthday ");
                }
                var list=this.BaseRepository().FindList<zhibin_jianliEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取zhibin_j_desc表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<zhibin_j_descEntity> Getzhibin_j_descList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<zhibin_j_descEntity>(t=>t.j_id == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取zhibin_jianli表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public zhibin_jianliEntity Getzhibin_jianliEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<zhibin_jianliEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取zhibin_j_desc表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public zhibin_j_descEntity Getzhibin_j_descEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<zhibin_j_descEntity>(t=>t.j_id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var zhibin_jianliEntity = Getzhibin_jianliEntity(keyValue); 
                db.Delete<zhibin_jianliEntity>(t=>t.ID == keyValue);
                db.Delete<zhibin_j_descEntity>(t=>t.j_id == zhibin_jianliEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, zhibin_jianliEntity entity,List<zhibin_j_descEntity> zhibin_j_descList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var zhibin_jianliEntityTmp = Getzhibin_jianliEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var zhibin_j_descUpdateList= zhibin_j_descList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in zhibin_j_descUpdateList)
                    {
                        db.Update(item);
                    }
                    var zhibin_j_descInserList= zhibin_j_descList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in zhibin_j_descInserList)
                    {
                        item.Create(item.ID);
                        item.j_id = zhibin_jianliEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (zhibin_j_descEntity item in zhibin_j_descList)
                    {
                        item.j_id = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
