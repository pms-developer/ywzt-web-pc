﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-12 18:00
    /// 描 述：供应商管理
    /// </summary>
    public class Base_SupplierService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_SupplierEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Company_ID,
                t.SupplierType,
                t.TaxpayerNature,
                t.TaxRage,
                t.ProvinceId,
                t.CityId,
                t.RegisterFund,
                t.DeliveryPeriod,
                t.PaymentDates,
                t.Flag,
                t.Remark,
                t.AuditStatus,
                t.Workflow_ID,
                t.Grade,
                t.CreationDate,
                t.Status
                ");
                strSql.Append("  FROM Base_Supplier t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                //if (!queryParam["SupType"].IsEmpty())
                //{
                //    dp.Add("SupType", queryParam["SupType"].ToString(), DbType.Int32);
                //    strSql.Append(" AND t.SupType = @SupType ");
                //}
                //else
                //{
                //    dp.Add("SupType", 0, DbType.Int32);
                //    strSql.Append(" AND t.SupType = @SupType ");
                //}
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                if (!queryParam["SupplierType"].IsEmpty())
                {
                    dp.Add("SupplierType",queryParam["SupplierType"].ToString(), DbType.String);
                    strSql.Append(" AND t.SupplierType = @SupplierType ");
                }
                if (!queryParam["TaxpayerNature"].IsEmpty())
                {
                    dp.Add("TaxpayerNature",queryParam["TaxpayerNature"].ToString(), DbType.String);
                    strSql.Append(" AND t.TaxpayerNature = @TaxpayerNature ");
                }
                if (!queryParam["Status"].IsEmpty())
                {
                    dp.Add("Status",queryParam["Status"].ToString(), DbType.String);
                    strSql.Append(" AND t.Status = @Status ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list=this.BaseRepository().FindList<Base_SupplierEntity>(strSql.ToString(),dp, pagination);
                //var query = from item in list orderby item.CreationDate descending select item;
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_SupplierLinkman表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Base_SupplierLinkmanEntity> GetBase_SupplierLinkmanList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Base_SupplierLinkmanEntity>(t=>t.BaseSupplierId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_SupplierBank表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Base_SupplierBankEntity> GetBase_SupplierBankList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Base_SupplierBankEntity>(t=>t.BaseSupplierId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_Supplier表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_SupplierEntity GetBase_SupplierEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_SupplierEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public Base_SupplierEntity GetBase_SupplierEntityByName(string supplierName)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_SupplierEntity>(m=>m.Name == supplierName);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_SupplierBank表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_SupplierBankEntity GetBase_SupplierBankEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_SupplierBankEntity>(t=>t.BaseSupplierId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_SupplierLinkman表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_SupplierLinkmanEntity GetBase_SupplierLinkmanEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_SupplierLinkmanEntity>(t=>t.BaseSupplierId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var base_SupplierEntity = GetBase_SupplierEntity(keyValue); 
                db.Delete<Base_SupplierEntity>(t=>t.ID == keyValue);
                db.Delete<Base_SupplierBankEntity>(t=>t.BaseSupplierId == base_SupplierEntity.ID);
                db.Delete<Base_SupplierLinkmanEntity>(t=>t.BaseSupplierId == base_SupplierEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        public void Review(string keyValue, Base_SupplierEntity entity)
        {
            var db = this.BaseRepository().BeginTrans();

            try
            {
                var base_SupplierEntityTmp = GetBase_SupplierEntity(keyValue);
                base_SupplierEntityTmp.Grade = entity.Grade;
                base_SupplierEntityTmp.ProductQuality = entity.ProductQuality;
                base_SupplierEntityTmp.AfterService = entity.AfterService;
                base_SupplierEntityTmp.TechnicalSupport = entity.TechnicalSupport;
                base_SupplierEntityTmp.Integrity = entity.Integrity;
                base_SupplierEntityTmp.ReviewOpinion = entity.ReviewOpinion;
                base_SupplierEntityTmp.Status = entity.Status;
                db.Update(base_SupplierEntityTmp);

                Base_SupplierReviewEntity reviewEntity = new Base_SupplierReviewEntity();
                reviewEntity.Create();
                reviewEntity.FK_SupplierId = base_SupplierEntityTmp.ID;
                reviewEntity.CreateDate = DateTime.Now;
                reviewEntity.Grade = entity.Grade;
                reviewEntity.ProductQuality = entity.ProductQuality;
                reviewEntity.AfterService = entity.AfterService;
                reviewEntity.TechnicalSupport = entity.TechnicalSupport;
                reviewEntity.Integrity = entity.Integrity;
                reviewEntity.ReviewOpinion = entity.ReviewOpinion;
                reviewEntity.Status = entity.Status;
                db.Insert(reviewEntity);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_SupplierEntity entity,List<Base_SupplierBankEntity> base_SupplierBankList,List<Base_SupplierLinkmanEntity> base_SupplierLinkmanList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var base_SupplierEntityTmp = GetBase_SupplierEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Base_SupplierBankUpdateList= base_SupplierBankList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Base_SupplierBankUpdateList)
                    {
                        db.Update(item);
                    }
                    var Base_SupplierBankInserList= base_SupplierBankList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Base_SupplierBankInserList)
                    {
                        item.Create(item.ID);
                        item.BaseSupplierId = base_SupplierEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                    
                    //没有生成代码 
                    var Base_SupplierLinkmanUpdateList= base_SupplierLinkmanList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Base_SupplierLinkmanUpdateList)
                    {
                        db.Update(item);
                    }
                    var Base_SupplierLinkmanInserList= base_SupplierLinkmanList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Base_SupplierLinkmanInserList)
                    {
                        item.Create(item.ID);
                        item.BaseSupplierId = base_SupplierEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Base_SupplierBankEntity item in base_SupplierBankList)
                    {
                        item.BaseSupplierId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Base_SupplierLinkmanEntity item in base_SupplierLinkmanList)
                    {
                        item.BaseSupplierId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
