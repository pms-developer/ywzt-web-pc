﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-12 18:00
    /// 描 述：供应商管理
    /// </summary>
    public class Base_SupplierEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 供应商名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 供应商类别
        /// </summary>
        [Column("SUPPLIERTYPE")]
        public string SupplierType { get; set; }
        /// <summary>
        /// 纳税人性质
        /// </summary>
        [Column("TAXPAYERNATURE")]
        public string TaxpayerNature { get; set; }
        /// <summary>
        /// 属性
        /// </summary>
        [Column("PROPERTY")]
        public string Property { get; set; }
        /// <summary>
        /// 合作状态
        /// </summary>
        [Column("STATUS")]
        public string Status { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        [Column("GRADE")]
        public string Grade { get; set; }
        /// <summary>
        /// 开票税率
        /// </summary>
        [Column("TAXRAGE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRage { get; set; }
        /// <summary>
        /// 公司地址省
        /// </summary>
        [Column("PROVINCEID")]
        public string ProvinceId { get; set; }
        /// <summary>
        /// 公司地址市
        /// </summary>
        [Column("CITYID")]
        public string CityId { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 注册资金
        /// </summary>
        [Column("REGISTERFUND")]
        [DecimalPrecision(18, 4)]
        public decimal? RegisterFund { get; set; }
        /// <summary>
        /// 账期
        /// </summary>
        [Column("PAYMENTDATES")]
        public int? PaymentDates { get; set; }
        /// <summary>
        /// 供货周期
        /// </summary>
        [Column("DELIVERYPERIOD")]
        public int? DeliveryPeriod { get; set; }
        /// <summary>
        /// 标签
        /// </summary>
        [Column("FLAG")]
        public string Flag { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 提交部门
        /// </summary>
        [Column("SubmitDepartment")]
        public string SubmitDepartment { get; set; }
        
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 产品质量
        /// </summary>
        [Column("PRODUCTQUALITY")]
        public string ProductQuality { get; set; }
        /// <summary>
        /// 售后服务
        /// </summary>
        [Column("AFTERSERVICE")]
        public string AfterService { get; set; }
        /// <summary>
        /// 技术支持
        /// </summary>
        [Column("TECHNICALSUPPORT")]
        public string TechnicalSupport { get; set; }
        /// <summary>
        /// 诚信度
        /// </summary>
        [Column("INTEGRITY")]
        public string Integrity { get; set; }
        /// <summary>
        /// 评审意见
        /// </summary>
        [Column("REVIEWOPINION")]
        public string ReviewOpinion { get; set; }

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

