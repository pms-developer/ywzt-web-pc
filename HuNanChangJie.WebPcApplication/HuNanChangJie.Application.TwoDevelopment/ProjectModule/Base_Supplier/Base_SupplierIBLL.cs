﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-12 18:00
    /// 描 述：供应商管理
    /// </summary>
    public interface Base_SupplierIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_SupplierEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_SupplierLinkman表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Base_SupplierLinkmanEntity> GetBase_SupplierLinkmanList(string keyValue);
        /// <summary>
        /// 获取Base_SupplierBank表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Base_SupplierBankEntity> GetBase_SupplierBankList(string keyValue);
        /// <summary>
        /// 获取Base_Supplier表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_SupplierEntity GetBase_SupplierEntity(string keyValue);

        Base_SupplierEntity GetBase_SupplierEntityByName(string supplierName);

        /// <summary>
        /// 获取Base_SupplierBank表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_SupplierBankEntity GetBase_SupplierBankEntity(string keyValue);
        /// <summary>
        /// 获取Base_SupplierLinkman表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_SupplierLinkmanEntity GetBase_SupplierLinkmanEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_SupplierEntity entity,List<Base_SupplierBankEntity> base_SupplierBankList,List<Base_SupplierLinkmanEntity> base_SupplierLinkmanList,string deleteList,string type);

        void Review(string keyValue, Base_SupplierEntity strEntity);
        #endregion

    }
}
