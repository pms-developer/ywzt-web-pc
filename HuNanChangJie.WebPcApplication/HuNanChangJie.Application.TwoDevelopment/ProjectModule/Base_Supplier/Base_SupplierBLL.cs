﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-12 18:00
    /// 描 述：供应商管理
    /// </summary>
    public class Base_SupplierBLL : Base_SupplierIBLL
    {
        private Base_SupplierService base_SupplierService = new Base_SupplierService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_SupplierEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return base_SupplierService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_SupplierLinkman表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Base_SupplierLinkmanEntity> GetBase_SupplierLinkmanList(string keyValue)
        {
            try
            {
                return base_SupplierService.GetBase_SupplierLinkmanList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_SupplierBank表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Base_SupplierBankEntity> GetBase_SupplierBankList(string keyValue)
        {
            try
            {
                return base_SupplierService.GetBase_SupplierBankList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_Supplier表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_SupplierEntity GetBase_SupplierEntity(string keyValue)
        {
            try
            {
                return base_SupplierService.GetBase_SupplierEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public Base_SupplierEntity GetBase_SupplierEntityByName(string supplierName)
        {
            try
            {
                return base_SupplierService.GetBase_SupplierEntityByName(supplierName);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取Base_SupplierBank表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_SupplierBankEntity GetBase_SupplierBankEntity(string keyValue)
        {
            try
            {
                return base_SupplierService.GetBase_SupplierBankEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_SupplierLinkman表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_SupplierLinkmanEntity GetBase_SupplierLinkmanEntity(string keyValue)
        {
            try
            {
                return base_SupplierService.GetBase_SupplierLinkmanEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                base_SupplierService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_SupplierEntity entity,List<Base_SupplierBankEntity> base_SupplierBankList,List<Base_SupplierLinkmanEntity> base_SupplierLinkmanList,string deleteList,string type)
        {
            try
            {
                base_SupplierService.SaveEntity(keyValue, entity,base_SupplierBankList,base_SupplierLinkmanList,deleteList,type);
                if (type == "add")
                {
                    new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0017");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 保存实体数据（评审）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void Review(string keyValue, Base_SupplierEntity entity)
        {
            try
            {
                base_SupplierService.Review(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
