﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-04-27 17:00
    /// 描 述：店铺基本信息管理
    /// </summary>
    public class base_shopinfoEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string PinYin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string PinYinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审批流
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 店铺名程
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// 开店时间
        /// </summary>
        [Column("OPENDATE")]
        public DateTime? OpenDate { get; set; }
        /// <summary>
        /// 关店时间
        /// </summary>
        [Column("CLOSEDATE")]
        public DateTime? CloseDate { get; set; }
        /// <summary>
        /// 运营状态(ShopOpening：开店中；Running：运营中；Closing：关店中；Closed：已关闭)
        /// </summary>
        [Column("OPERATIONSTATE")]
        public string OperationState { get; set; }
        /// <summary>
        /// 所属部门
        /// </summary>
        [Column("DEPARTMENTID")]
        public string DepartmentId { get; set; }
        /// <summary>
        /// 店铺负责人ID
        /// </summary>
        [Column("MANAGERID")]
        public string ManagerId { get; set; }
        /// <summary>
        /// 店长ID
        /// </summary>
        [Column("SHOPMANAGERID")]
        public string ShopManagerId { get; set; }
        /// <summary>
        /// 绑定邮箱
        /// </summary>
        [Column("BINDEMAIL")]
        public string BindEmail { get; set; }
        /// <summary>
        /// 绑定电话
        /// </summary>
        [Column("BINDPHONE")]
        public string BindPhone { get; set; }
        /// <summary>
        /// 所属平台
        /// </summary>
        [Column("PLATFORMNAME")]
        public string PlatformName { get; set; }

        /// <summary>
        /// 店铺编码
        /// </summary>
        [Column("ShopCode")]
        public string ShopCode { get; set; }


        /// <summary>
        /// 运营时间
        /// </summary>
        [Column("OPERATIONDATE")]
        public DateTime? OperationDate { get; set; }
        
       
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            //this.Company_ID = userInfo.companyId;
            //this.CompanyCode = userInfo.CompanyCode;
            //this.CompanyName = userInfo.CompanyName;
            //this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            //this.Company_ID = userInfo.companyId;
            //this.CompanyCode = userInfo.CompanyCode;
            //this.CompanyName = userInfo.CompanyName;
            //this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

