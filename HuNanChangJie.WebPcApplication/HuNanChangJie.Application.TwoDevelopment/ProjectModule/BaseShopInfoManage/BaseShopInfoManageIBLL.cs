﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule.BaseShopInfoManage;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-04-27 17:00
    /// 描 述：店铺基本信息管理
    /// </summary>
    public interface BaseShopInfoManageIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<base_shopinfoEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取base_shopinfo_account表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<base_shopinfo_accountEntity> Getbase_shopinfo_accountList(string keyValue);
        /// <summary>
        /// 获取base_shopinfo_criticalpath表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<base_shopinfo_criticalpathEntity> Getbase_shopinfo_criticalpathList(string keyValue);
        /// <summary>
        /// 获取base_shopinfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        base_shopinfoEntity Getbase_shopinfoEntity(string keyValue);
        /// <summary>
        /// 获取base_shopinfo_account表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        base_shopinfo_accountEntity Getbase_shopinfo_accountEntity(string keyValue);
        /// <summary>
        /// 获取base_shopinfo_criticalpath表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        base_shopinfo_criticalpathEntity Getbase_shopinfo_criticalpathEntity(string keyValue);


        /// <summary>
        /// 获取base_shopinfo_id by shopcode
        /// <param name="shopcode">shopcode</param>
        /// <summary>
        /// <returns></returns>
        base_shopinfoEntity GetShopInfoByShopCode(string shopcode);

        /// <summary>
        /// 获取店铺平台信息
        /// </summary>
        /// <returns></returns>
        IEnumerable<ShopPlatformMappingEntity> GetShopPlatformMappingList();
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, base_shopinfoEntity entity,List<base_shopinfo_accountEntity> base_shopinfo_accountList,List<base_shopinfo_criticalpathEntity> base_shopinfo_criticalpathList,string deleteList,string type);
        #endregion

    }
}
