﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule.BaseShopInfoManage;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-04-27 17:00
    /// 描 述：店铺基本信息管理
    /// </summary>
    public class BaseShopInfoManageService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<base_shopinfoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.ShopName,
                t.OpenDate,
                t.OperationDate,
                t.OperationState,
                t.Company_ID,
                t.DepartmentId,
                t.ManagerId,
                t.ShopManagerId,
                t.BindEmail,
                t.BindPhone,
                t.CreationDate,
                t.PlatformName               
                ");
                strSql.Append("  FROM base_shopinfo t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["OpenDateStart"].IsEmpty())
                {
                    dp.Add("OpenDateStart", queryParam["OpenDateStart"].ToString(), DbType.String); 
                    strSql.Append(" AND str_to_date(t.OpenDate, '%Y-%m-%d')>= @OpenDateStart");
                }
                if (!queryParam["OpenDateEnd"].IsEmpty())
                {
                    dp.Add("OpenDateEnd", queryParam["OpenDateEnd"].ToString(), DbType.String);
                    strSql.Append(" AND str_to_date(t.OpenDate, '%Y-%m-%d')<=@OpenDateEnd");
                }
                if (!queryParam["Company_ID"].IsEmpty())
                {
                    dp.Add("Company_ID",queryParam["Company_ID"].ToString(), DbType.String);
                    strSql.Append(" AND t.Company_ID = @Company_ID ");
                }
                if (!queryParam["DepartmentId"].IsEmpty())
                {
                    dp.Add("DepartmentId",queryParam["DepartmentId"].ToString(), DbType.String);
                    strSql.Append(" AND t.DepartmentId = @DepartmentId ");
                }
                if (!queryParam["ManagerId"].IsEmpty())
                {
                    dp.Add("ManagerId",queryParam["ManagerId"].ToString(), DbType.String);
                    strSql.Append(" AND t.ManagerId = @ManagerId ");
                }
                if (!queryParam["PlatformName"].IsEmpty())
                {
                    dp.Add("PlatformName",queryParam["PlatformName"].ToString(), DbType.String);
                    strSql.Append(" AND t.PlatformName = @PlatformName ");
                }
                if (!queryParam["ShopName"].IsEmpty())
                {
                    dp.Add("ShopName", "%" + queryParam["ShopName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ShopName Like @ShopName ");
                }
                if (!queryParam["OperationDateStart"].IsEmpty())
                {
                    dp.Add("OperationDateStart", queryParam["OperationDateStart"].ToString(), DbType.String); 
                    strSql.Append(" AND str_to_date(t.OperationDate, '%Y-%m-%d')>= @OperationDateStart");
                }
                if (!queryParam["OperationDateEnd"].IsEmpty())
                {
                    dp.Add("OperationDateEnd", queryParam["OperationDateEnd"].ToString(), DbType.String);                   
                    strSql.Append(" AND str_to_date(t.OperationDate, '%Y-%m-%d')<=@OperationDateEnd");
                }
                var list=this.BaseRepository().FindList<base_shopinfoEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public base_shopinfoEntity GetShopInfoByShopCode(string shopcode)
        {
            try
            {
               // var strSql =$"select * from base_shopinfo where ShopCode='{shopcode}'";   

                return this.BaseRepository().FindEntity<base_shopinfoEntity>(x=>x.ShopCode== shopcode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo_account表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<base_shopinfo_accountEntity> Getbase_shopinfo_accountList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<base_shopinfo_accountEntity>(t=>t.Base_ShopInfoID == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo_criticalpath表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<base_shopinfo_criticalpathEntity> Getbase_shopinfo_criticalpathList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<base_shopinfo_criticalpathEntity>(t=>t.Base_ShopInfoID == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public base_shopinfoEntity Getbase_shopinfoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<base_shopinfoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo_account表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public base_shopinfo_accountEntity Getbase_shopinfo_accountEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<base_shopinfo_accountEntity>(t=>t.Base_ShopInfoID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo_criticalpath表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public base_shopinfo_criticalpathEntity Getbase_shopinfo_criticalpathEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<base_shopinfo_criticalpathEntity>(t=>t.Base_ShopInfoID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var base_shopinfoEntity = Getbase_shopinfoEntity(keyValue);
                db.Delete<base_shopinfo_accountEntity>(t => t.Base_ShopInfoID == base_shopinfoEntity.ID);
                db.Delete<base_shopinfo_criticalpathEntity>(t => t.Base_ShopInfoID == base_shopinfoEntity.ID);
                db.Delete<base_shopinfoEntity>(t=>t.ID == keyValue);
                
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, base_shopinfoEntity entity,List<base_shopinfo_accountEntity> base_shopinfo_accountList,List<base_shopinfo_criticalpathEntity> base_shopinfo_criticalpathList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var base_shopinfoEntityTmp = Getbase_shopinfoEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var base_shopinfo_accountUpdateList= base_shopinfo_accountList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in base_shopinfo_accountUpdateList)
                    {
                        db.Update(item);
                    }
                    var base_shopinfo_accountInserList= base_shopinfo_accountList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in base_shopinfo_accountInserList)
                    {
                        if(item.AccontType==null)
                        {
                            item.AccontType = 0;
                        }

                        item.Create(item.ID);
                        item.Base_ShopInfoID = base_shopinfoEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                    
                    //没有生成代码 
                    var base_shopinfo_criticalpathUpdateList= base_shopinfo_criticalpathList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in base_shopinfo_criticalpathUpdateList)
                    {
                        db.Update(item);
                    }
                    var base_shopinfo_criticalpathInserList= base_shopinfo_criticalpathList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in base_shopinfo_criticalpathInserList)
                    {
                        item.Create(item.ID);
                        item.Base_ShopInfoID = base_shopinfoEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (base_shopinfo_accountEntity item in base_shopinfo_accountList)
                    {
                        if (item.AccontType == null)
                        {
                            item.AccontType = 0;
                        }
                        item.Base_ShopInfoID = entity.ID;
                        db.Insert(item);
                    }
                    foreach (base_shopinfo_criticalpathEntity item in base_shopinfo_criticalpathList)
                    {
                        item.Base_ShopInfoID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取店铺平台ID信息
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ShopPlatformMappingEntity> GetShopPlatformMappingList()
        {
            try
            {

                var strSql = new StringBuilder();
                strSql.Append(@"SELECT
	                b.PlatformCode,
	                b.WDTPlatformCode,
	                a.ShopCode 
                FROM
	                base_shopinfo a
	                JOIN base_shopplatform b ON a.PlatformName = b.PlatformCode ");
                
                var list = this.BaseRepository().FindList<ShopPlatformMappingEntity>(strSql.ToString()); 
                return list;
            }
            catch (Exception ex)
            {
                //db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
