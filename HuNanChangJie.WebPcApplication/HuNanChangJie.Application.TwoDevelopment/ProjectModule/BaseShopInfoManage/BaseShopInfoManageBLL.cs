﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.ProjectModule.BaseShopInfoManage;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-04-27 17:00
    /// 描 述：店铺基本信息管理
    /// </summary>
    public class BaseShopInfoManageBLL : BaseShopInfoManageIBLL
    {
        private BaseShopInfoManageService baseShopInfoManageService = new BaseShopInfoManageService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<base_shopinfoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return baseShopInfoManageService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo_account表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<base_shopinfo_accountEntity> Getbase_shopinfo_accountList(string keyValue)
        {
            try
            {
                return baseShopInfoManageService.Getbase_shopinfo_accountList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo_criticalpath表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<base_shopinfo_criticalpathEntity> Getbase_shopinfo_criticalpathList(string keyValue)
        {
            try
            {
                return baseShopInfoManageService.Getbase_shopinfo_criticalpathList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public base_shopinfoEntity Getbase_shopinfoEntity(string keyValue)
        {
            try
            {
                return baseShopInfoManageService.Getbase_shopinfoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo_account表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public base_shopinfo_accountEntity Getbase_shopinfo_accountEntity(string keyValue)
        {
            try
            {
                return baseShopInfoManageService.Getbase_shopinfo_accountEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取base_shopinfo_criticalpath表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public base_shopinfo_criticalpathEntity Getbase_shopinfo_criticalpathEntity(string keyValue)
        {
            try
            {
                return baseShopInfoManageService.Getbase_shopinfo_criticalpathEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取店铺平台ID信息
        /// </summary>
        public IEnumerable<ShopPlatformMappingEntity> GetShopPlatformMappingList()
        {
            try
            {
                return baseShopInfoManageService.GetShopPlatformMappingList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                baseShopInfoManageService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, base_shopinfoEntity entity, List<base_shopinfo_accountEntity> base_shopinfo_accountList, List<base_shopinfo_criticalpathEntity> base_shopinfo_criticalpathList, string deleteList, string type)
        {
            try
            {
                baseShopInfoManageService.SaveEntity(keyValue, entity, base_shopinfo_accountList, base_shopinfo_criticalpathList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        /// <summary>
        /// 获取base_shopinfo_id by shopcode
        /// <param name="shopcode">shopcode</param>
        /// <summary>
        /// <returns></returns>
        public base_shopinfoEntity GetShopInfoByShopCode(string shopcode)
        {
            try
            {
                return baseShopInfoManageService.GetShopInfoByShopCode(shopcode);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

    }
}
