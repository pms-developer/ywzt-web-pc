﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule.BaseShopInfoManage
{
    /// <summary>
    /// 店铺与平台关联实体
    /// </summary>
    public class ShopPlatformMappingEntity
    {
        /// <summary>
        /// 平台编码
        /// </summary> 
        public string PlatformCode { get; set; }
         /// <summary>
         /// 旺店通编号-平台ID
         /// </summary>
        public string WDTPlatformCode { get; set; }
         
        /// <summary>
        /// 店铺编号
        /// </summary>
        public string ShopCode { get; set; }

    }
}
