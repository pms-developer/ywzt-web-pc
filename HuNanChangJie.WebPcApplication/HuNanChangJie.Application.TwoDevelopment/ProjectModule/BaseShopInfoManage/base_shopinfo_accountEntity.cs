﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-04-27 17:00
    /// 描 述：店铺基本信息管理
    /// </summary>
    public class base_shopinfo_accountEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        [Column("BASE_SHOPINFOID")]
        public string Base_ShopInfoID { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [Column("USERNAME")]
        public string UserName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Column("PASSWORD")]
        public string Password { get; set; }
        /// <summary>
        /// 是否主账号
        /// </summary>
        [Column("ISMAIN")]
        public bool? IsMain { get; set; }
        /// <summary>
        /// 是否信息部专用账号
        /// </summary>
        [Column("ISITEXCLUSIVE")]
        public bool? IsItExclusive { get; set; }

        /// <summary>
        /// 帐号是否验证
        /// </summary>
        [Column("IsVerified")]
        public bool? IsVerified { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("AccontType")]
        public int? AccontType { get; set; }


        /// <summary>
        /// 账户名称
        /// </summary>
        [Column("AccountName")]
        public string AccountName { get; set; }

        #endregion



        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

