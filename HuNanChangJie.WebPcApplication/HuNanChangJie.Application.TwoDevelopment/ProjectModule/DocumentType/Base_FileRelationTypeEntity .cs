﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-08 16:58
    /// 描 述：文件分类关联表
    /// </summary>
    public class Base_FileRelationTypeEntity : BaseEntity 
    {
        #region  实体成员        
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 父ID
        /// </summary>
        [Column("FILETYPEID")]
        public string FileTypeID { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("FILEID")]
        public string FileID { get; set; }
        #endregion

        #region  扩展操作
        #endregion
        #region  扩展字段
        [NotMapped]
        public string FileTypeName { get; set; }

        [NotMapped]
        public string F_FileName { get; set; }

        [NotMapped]
        public string ProjectName { get; set; }
        #endregion
    }
}

