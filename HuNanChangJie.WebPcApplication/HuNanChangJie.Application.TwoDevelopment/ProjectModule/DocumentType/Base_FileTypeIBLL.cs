﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-08 16:58
    /// 描 述：投标文档
    /// </summary>
    public interface Base_FileTypeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取树
        /// <summary>
        /// <returns></returns>
        List<TreeModel> GetFileTypeTree(string projectid);

        /// <summary>
        /// 获取类型实体
        /// <summary>.
        /// <returns></returns>
        Base_FileTypeEntity GetFileTypeEntity(string keyValue);

        /// <summary>
        /// 获取类型关联实体
        /// <summary>
        /// <returns></returns>
        Base_FileRelationTypeEntity GetBaseFileRelationTypeEntity(string keyValue);

        IEnumerable<Base_FileRelationTypeEntity> GetFileList(string keyValue,string projectId);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_FileTypeEntity entity, string type);

        void SaveRelationEntity(string fileTypeID, string fileID);

        #endregion

    }
}
