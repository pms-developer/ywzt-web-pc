﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-08 16:58
    /// 描 述：文档分类
    /// </summary>
    public class Base_FileTypeEntity : BaseEntity 
    {
        #region  实体成员        
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 父ID
        /// </summary>
        [Column("PARENTID")]
        public string ParentID { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        [Column("FILETYPENAME")]
        public string FileTypeName { get; set; }        
        #endregion

        #region  扩展操作
        #endregion
        #region  扩展字段
        #endregion
    }
}

