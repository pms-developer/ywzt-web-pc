﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2020-04-08 16:58
    /// 描 述：投标文档
    /// </summary>
    public class Base_FileTypeBLL : Base_FileTypeIBLL
    {
        private Base_FileTypeService service = new Base_FileTypeService();

        #region  获取数据

        /// <summary>
        /// 获取功能列表的树形数据
        /// </summary>
        /// <returns></returns>
        public List<TreeModel> GetFileTypeTree(string projectid)
        {
            try
            {
                List<Base_FileTypeEntity> typelist = service.GetFileTypeTree(projectid);
                List<TreeModel> treeList = new List<TreeModel>();

                foreach (var item in typelist)
                {
                    TreeModel node = new TreeModel();
                    node.id = item.ID;
                    node.text = item.FileTypeName;
                    node.value = item.ID;
                    node.showcheck = false;
                    node.checkstate = 0;
                    node.isexpand = false;
                    //node.icon = item.F_Icon;
                    node.parentId = item.ParentID;
                    treeList.Add(node);
                }
                return treeList.ToTree();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取实体数据
        /// </summary>
        /// <returns></returns>
        public Base_FileTypeEntity GetFileTypeEntity(string keyValue)
        {
            try
            {
                var entity = service.GetBaseFileTypeEntity(keyValue);
                return entity;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Base_FileRelationTypeEntity GetBaseFileRelationTypeEntity(string keyValue)
        {
            try
            {
                var entity = service.GetBaseFileRelationTypeEntity(keyValue);
                return entity;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Base_FileRelationTypeEntity> GetFileList(string keyValue,string projectId)
        {
            try
            {
                return service.GetFileList(keyValue, projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        public void SaveEntity(string keyValue, Base_FileTypeEntity entity, string type)
        {
            try
            {
                service.SaveEntity(keyValue, entity, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void SaveRelationEntity(string fileTypeID, string fileID)
        {
            try
            {
                service.SaveRelationEntity(fileTypeID, fileID);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void DeleteEntity(string keyValue)
        {
            try
            {
                service.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
