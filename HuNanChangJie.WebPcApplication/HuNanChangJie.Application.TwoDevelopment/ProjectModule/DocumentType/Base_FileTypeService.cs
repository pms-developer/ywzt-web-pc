﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2020-04-08 16:58
    /// 描 述：投标文档
    /// </summary>
    public class Base_FileTypeService : RepositoryFactory
    {
        #region  获取数据        

        public List<Base_FileTypeEntity> GetFileTypeTree(string projectid)
        {
            List<Base_FileTypeEntity> list = this.BaseRepository().FindList<Base_FileTypeEntity>(i => i.ProjectID == projectid).ToList();
            return list;
        }

        /// <summary>
        /// 获取Base_FileTypeEntity表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_FileTypeEntity GetBaseFileTypeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_FileTypeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Base_FileRelationTypeEntity GetBaseFileRelationTypeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_FileRelationTypeEntity>(i => i.FileID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Base_FileRelationTypeEntity> GetFileList(string keyValue,string projectId)
        {
            try
            {
                StringBuilder Str = new StringBuilder();
                Str.Append(@"select a.*,b.FileTypeName,c.F_FileName,d.ProjectName from Base_FileRelationType a 
                                left join Base_FileType b on a.FileTypeID=b.ID 
	                            left join Base_AnnexesFile c on a.FileID=c.F_Id
	                            left join Base_CJ_Project d on b.ProjectID=d.ID
	                              where 1=1");
                var dp = new DynamicParameters(new { });

                if (!projectId.IsEmpty())
                {
                    dp.Add("projectId", projectId, DbType.String);
                    Str.Append(" and d.ID = @projectId");
                }
                if (!keyValue.IsEmpty())
                {
                    dp.Add("fileTypeid", keyValue, DbType.String);
                    Str.Append(" and (a.fileTypeid = @fileTypeid or b.ParentID=@fileTypeid)");
                }
                return this.BaseRepository().FindList<Base_FileRelationTypeEntity>(Str.ToString(),dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_FileTypeEntity entity, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.ID = keyValue;
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.ID = keyValue;
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void SaveRelationEntity(string fileTypeID, string fileID)
        {
            try
            {
                var relation = BaseRepository().FindEntity<Base_FileRelationTypeEntity>(i => i.FileID == fileID);
                if (relation == null)
                {
                    relation = new Base_FileRelationTypeEntity();
                    relation.ID = Guid.NewGuid().ToString();
                    relation.FileTypeID = fileTypeID;
                    relation.FileID = fileID;
                    this.BaseRepository().Insert(relation);
                }
                else
                {
                    relation.FileTypeID = fileTypeID;
                    this.BaseRepository().Update(relation);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                db.Delete<Base_FileRelationTypeEntity>(t => t.FileTypeID == keyValue);
                db.Delete<Base_FileTypeEntity>(t => t.ID == keyValue);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
