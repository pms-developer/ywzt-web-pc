﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 18:57
    /// 描 述：工程合同产值结算
    /// </summary>
    public interface ProjectOutputSettlementIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_OutputSettlementEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_OutputSettlementQuantities表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_OutputSettlementQuantitiesEntity> GetProject_OutputSettlementQuantitiesList(string keyValue);
        /// <summary>
        /// 获取Project_OutputSettlementMaterials表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_OutputSettlementMaterialsEntity> GetProject_OutputSettlementMaterialsList(string keyValue);
        /// <summary>
        /// 获取Project_OutputSettlement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_OutputSettlementEntity GetProject_OutputSettlementEntity(string keyValue);
        /// <summary>
        /// 获取Project_OutputSettlementMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_OutputSettlementMaterialsEntity GetProject_OutputSettlementMaterialsEntity(string keyValue);
        /// <summary>
        /// 获取Project_OutputSettlementQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_OutputSettlementQuantitiesEntity GetProject_OutputSettlementQuantitiesEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_OutputSettlementEntity entity,List<Project_OutputSettlementMaterialsEntity> project_OutputSettlementMaterialsList,List<Project_OutputSettlementQuantitiesEntity> project_OutputSettlementQuantitiesList,string deleteList,string type);

        /// <summary>
        /// 反审核后事件
        /// </summary>
        /// <param name="keyValue"></param>
        void UnAudit(string keyValue);

        /// <summary>
        /// 审核通过后事件
        /// </summary>
        /// <param name="keyValue"></param>
        void Audit(string keyValue);
        #endregion

    }
}
