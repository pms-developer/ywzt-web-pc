﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 18:57
    /// 描 述：工程合同产值结算
    /// </summary>
    public class ProjectOutputSettlementBLL : ProjectOutputSettlementIBLL
    {
        private ProjectOutputSettlementService projectOutputSettlementService = new ProjectOutputSettlementService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_OutputSettlementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectOutputSettlementService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_OutputSettlementQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_OutputSettlementQuantitiesEntity> GetProject_OutputSettlementQuantitiesList(string keyValue)
        {
            try
            {
                return projectOutputSettlementService.GetProject_OutputSettlementQuantitiesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_OutputSettlementMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_OutputSettlementMaterialsEntity> GetProject_OutputSettlementMaterialsList(string keyValue)
        {
            try
            {
                return projectOutputSettlementService.GetProject_OutputSettlementMaterialsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_OutputSettlement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_OutputSettlementEntity GetProject_OutputSettlementEntity(string keyValue)
        {
            try
            {
                return projectOutputSettlementService.GetProject_OutputSettlementEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_OutputSettlementMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_OutputSettlementMaterialsEntity GetProject_OutputSettlementMaterialsEntity(string keyValue)
        {
            try
            {
                return projectOutputSettlementService.GetProject_OutputSettlementMaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_OutputSettlementQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_OutputSettlementQuantitiesEntity GetProject_OutputSettlementQuantitiesEntity(string keyValue)
        {
            try
            {
                return projectOutputSettlementService.GetProject_OutputSettlementQuantitiesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectOutputSettlementService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_OutputSettlementEntity entity,List<Project_OutputSettlementMaterialsEntity> project_OutputSettlementMaterialsList,List<Project_OutputSettlementQuantitiesEntity> project_OutputSettlementQuantitiesList,string deleteList,string type)
        {
            try
            {
                projectOutputSettlementService.SaveEntity(keyValue, entity,project_OutputSettlementMaterialsList,project_OutputSettlementQuantitiesList,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("OutputSettlement");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 审核通过后事件
        /// </summary>
        /// <param name="keyValue"></param>
        public void Audit(string keyValue)
        {
            var contractBll = new ProjectContractBLL();
            try
            {
                var quantitiesList = new List<Project_ContractQuantitiesEntity>();
                var materialsList = new List<Project_ContractMaterialsEntity>();

                var outputInfo = projectOutputSettlementService.GetProject_OutputSettlementEntity(keyValue);
                var contractId = outputInfo.ProjectContractId;

                var contractModel = contractBll.GetProject_ContractEntity(contractId);
                contractModel.SettlementAmount = (contractModel.SettlementAmount ?? 0) + outputInfo.ApplyAmount;

                //2020-11-03 添加罚款金额 罚款事由 其它收入 其它支出
                if (!contractModel.FineExpend.HasValue)
                    contractModel.FineExpend = 0;
                if (!contractModel.OtherExpend.HasValue)
                    contractModel.OtherExpend = 0;
                if (!contractModel.OtherIncome.HasValue)
                    contractModel.OtherIncome = 0;

                contractModel.FineExpend += outputInfo.FineExpend ?? 0;
                contractModel.OtherExpend += outputInfo.OtherExpend ?? 0;
                contractModel.OtherIncome += outputInfo.OtherIncome ?? 0;

                var ouputQuantities = projectOutputSettlementService.GetProject_OutputSettlementQuantitiesList(keyValue);
                if (ouputQuantities.Any())
                {
                    var quantities = contractBll.GetProject_ContractQuantitiesList(contractId);
                    foreach (var item in ouputQuantities)
                    {
                        var info = quantities.FirstOrDefault(i => i.ID == item.ProjectContractQuantitiesId);
                        if (info == null) continue;

                        info.SettlementQuantities = (info.SettlementQuantities ?? 0) + item.ApprovalQuantities;
                        info.SettlementTotalPrice = (info.SettlementTotalPrice ?? 0) + item.ApprovalTotal;

                        if (info.SettlementTotalPrice != 0)
                        {
                            info.SettlementPrice = info.SettlementTotalPrice / info.SettlementQuantities;
                        }
                        else
                        {
                            info.SettlementPrice = 0;
                        }

                        quantitiesList.Add(info);
                    }
                }

                var outputMaterials = projectOutputSettlementService.GetProject_OutputSettlementMaterialsList(keyValue);
                if (outputMaterials.Any())
                {
                    var materials = contractBll.GetProject_ContractMaterialsList(contractId);
                    foreach (var item in outputMaterials)
                    {
                        var info = materials.FirstOrDefault(i => i.ID == item.ProjectContractMaterialsId);
                        if (info == null) continue;
                        info.SettlementQuantities = (info.SettlementQuantities ?? 0) + item.ApprovalQuantities;
                        info.SettlementTotalPrice = (info.SettlementTotalPrice ?? 0) + item.ApprovalTotal;

                        if (info.SettlementPrice != 0)
                        {
                            info.SettlementPrice = info.SettlementTotalPrice / info.SettlementQuantities;
                        }
                        else
                        {
                            info.SettlementPrice = 0;
                        }

                        materialsList.Add(info);
                    }
                }
                contractBll.UpdateContract(contractModel, quantitiesList, materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UnAudit(string keyValue)
        {
            var contractBll = new ProjectContractBLL();
            try
            {
                var quantitiesList = new List<Project_ContractQuantitiesEntity>();
                var materialsList = new List<Project_ContractMaterialsEntity>();

                var outputInfo = projectOutputSettlementService.GetProject_OutputSettlementEntity(keyValue);
                var contractId = outputInfo.ProjectContractId;

                var contractModel = contractBll.GetProject_ContractEntity(contractId);
                contractModel.SettlementAmount = (contractModel.SettlementAmount ?? 0) - outputInfo.ApplyAmount;

                //2020-11-03 添加罚款金额 罚款事由 其它收入 其它支出
                if (!contractModel.FineExpend.HasValue)
                    contractModel.FineExpend = 0;
                if (!contractModel.OtherExpend.HasValue)
                    contractModel.OtherExpend = 0;
                if (!contractModel.OtherIncome.HasValue)
                    contractModel.OtherIncome = 0;

                contractModel.FineExpend -= outputInfo.FineExpend ?? 0;
                contractModel.OtherExpend -= outputInfo.OtherExpend ?? 0;
                contractModel.OtherIncome -= outputInfo.OtherIncome ?? 0;


                var ouputQuantities = projectOutputSettlementService.GetProject_OutputSettlementQuantitiesList(keyValue);
                if (ouputQuantities.Any())
                {
                    var quantities = contractBll.GetProject_ContractQuantitiesList(contractId);
                    foreach (var item in ouputQuantities)
                    {
                        var info = quantities.FirstOrDefault(i => i.ID == item.ProjectContractQuantitiesId);
                        if (info == null) continue;

                        info.SettlementQuantities = (info.SettlementQuantities ?? 0) - item.ApprovalQuantities;
                        info.SettlementTotalPrice = (info.SettlementTotalPrice ?? 0) - item.ApprovalTotal;

                        if (info.SettlementTotalPrice != 0)
                        {
                            info.SettlementPrice = info.SettlementTotalPrice / info.SettlementQuantities;
                        }
                        else
                        {
                            info.SettlementPrice = 0;
                        }

                        quantitiesList.Add(info);
                    }
                }

                var outputMaterials = projectOutputSettlementService.GetProject_OutputSettlementMaterialsList(keyValue);
                if (outputMaterials.Any())
                {
                    var materials = contractBll.GetProject_ContractMaterialsList(contractId);
                    foreach (var item in outputMaterials)
                    {
                        var info = materials.FirstOrDefault(i => i.ID == item.ProjectContractMaterialsId);
                        if (info == null) continue;
                        info.SettlementQuantities = (info.SettlementQuantities ?? 0) - item.ApprovalQuantities;
                        info.SettlementTotalPrice = (info.SettlementTotalPrice ?? 0) - item.ApprovalTotal;

                        if (info.SettlementPrice != 0)
                        {
                            info.SettlementPrice = info.SettlementTotalPrice / info.SettlementQuantities;
                        }
                        else
                        {
                            info.SettlementPrice = 0;
                        }

                        materialsList.Add(info);
                    }
                }
                contractBll.UpdateContract(contractModel, quantitiesList, materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        #endregion

    }
}
