﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 18:57
    /// 描 述：工程合同产值结算
    /// </summary>
    public class Project_OutputSettlementQuantitiesEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 产值结算ID
        /// </summary>
        [Column("PROJECTOUTPUTSETTLEMENTID")]
        public string ProjectOutputSettlementId { get; set; }
        /// <summary>
        /// 合同工程量清单ID
        /// </summary>
        [Column("PROJECTCONTRACTQUANTITIESID")]
        public string ProjectContractQuantitiesId { get; set; }
        /// <summary>
        /// 清单编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 清单名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 项目特征
        /// </summary>
        [Column("FEATURE")]
        public string Feature { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 本次申请单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
        /// <summary>
        /// 本次申请量
        /// </summary>
        [Column("QUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantities { get; set; }
        /// <summary>
        /// 本次申请合计
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// 形象进度
        /// </summary>
        [Column("VISUALPROGRESS")]
        public string VisualProgress { get; set; }
        /// <summary>
        /// 本次审批量
        /// </summary>
        [Column("APPROVALQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? ApprovalQuantities { get; set; }
        /// <summary>
        /// 本次审批单价
        /// </summary>
        [Column("APPROVALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ApprovalPrice { get; set; }
        /// <summary>
        /// 本次审批合计金额
        /// </summary>
        [Column("APPROVALTOTAL")]
        [DecimalPrecision(18, 4)]
        public decimal? ApprovalTotal { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        /// <summary>
        /// 当前合同数量 
        /// </summary>
        [NotMapped]
        public decimal? CurrentQuantities { get; set; } = 0;

        /// <summary>
        /// 当前单价
        /// </summary>
        [NotMapped]
        public decimal? CurrentPrice { get; set; } = 0;

        /// <summary>
        /// 累计结算数量
        /// </summary>
        [NotMapped]
        public decimal? SettlementQuantities { get; set; } = 0;

        /// <summary>
        /// 剩余结算数量（当前合同数量-累计结算数量）
        /// </summary>
        [NotMapped]
        public decimal? SurplusAmount { get; set; } = 0;



        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

