﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 18:57
    /// 描 述：工程合同产值结算
    /// </summary>
    public class ProjectOutputSettlementService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_OutputSettlementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.SettlementDate,
                t.ProjectContractId,
                t.FinishedAmount,
                t.ProjectID,
                t.ProjectName,
                t.ApplyAmount,
                t.SortCode,
                t.ReceivableAmount,
                t.OperatorId,
                t.OperatorDepartmentId,
                t.AuditStatus,
                t.Workflow_ID,
                t.Abstract,
                t.Econometrics ,
                t.CurrentProduction,
                t.CreationDate,
                t.CurrentProductionDate,
                b.Name as ContractName,
                c.FullName as CustomerName
                ");
                strSql.Append("  FROM Project_OutputSettlement t left join Project_Contract as b on t.ProjectContractId=b.ID ");
                strSql.Append("  left join Base_Customer as c on b.Jia=c.id ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" and t.ProjectID=@ProjectID");
                }
                var list=this.BaseRepository().FindList<Project_OutputSettlementEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public string GetYearData(string projectId,string ye) {
            
            var strSql = new StringBuilder();
            strSql.Append("select (case  when  sum(CurrentProduction) is NULL then 0 else sum(CurrentProduction) end) as YearCurrentProduction from Project_OutputSettlement ");

            var dp = new DynamicParameters(new { });
            
            strSql.Append(" WHERE 1=1 and AuditStatus='2'");
            dp.Add("ProjectID", projectId, DbType.String);
            strSql.Append(" and ProjectID=@ProjectID");
            if (ye == "1")
            {
                int year = DateTime.Now.Year;
                DateTime firstDay = new DateTime(year, 1, 1);
                strSql.Append($" and CurrentProductionDate>='{firstDay}'");
            }
            return this.BaseRepository().FindList<Project_OutputSettlementEntity>(strSql.ToString(), dp).ToList()[0].YearCurrentProduction;
        }
        /// <summary>
        /// 获取Project_OutputSettlementQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_OutputSettlementQuantitiesEntity> GetProject_OutputSettlementQuantitiesList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Project_OutputSettlementQuantitiesEntity>(t=>t.ProjectOutputSettlementId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_OutputSettlementMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_OutputSettlementMaterialsEntity> GetProject_OutputSettlementMaterialsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Project_OutputSettlementMaterialsEntity>(t=>t.ProjectOutputSettlementId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_OutputSettlement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_OutputSettlementEntity GetProject_OutputSettlementEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_OutputSettlementEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_OutputSettlementMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_OutputSettlementMaterialsEntity GetProject_OutputSettlementMaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_OutputSettlementMaterialsEntity>(t=>t.ProjectOutputSettlementId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_OutputSettlementQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_OutputSettlementQuantitiesEntity GetProject_OutputSettlementQuantitiesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_OutputSettlementQuantitiesEntity>(t=>t.ProjectOutputSettlementId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_OutputSettlementEntity = GetProject_OutputSettlementEntity(keyValue); 
                db.Delete<Project_OutputSettlementEntity>(t=>t.ID == keyValue);
                db.Delete<Project_OutputSettlementMaterialsEntity>(t=>t.ProjectOutputSettlementId == project_OutputSettlementEntity.ID);
                db.Delete<Project_OutputSettlementQuantitiesEntity>(t=>t.ProjectOutputSettlementId == project_OutputSettlementEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_OutputSettlementEntity entity,List<Project_OutputSettlementMaterialsEntity> project_OutputSettlementMaterialsList,List<Project_OutputSettlementQuantitiesEntity> project_OutputSettlementQuantitiesList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_OutputSettlementEntityTmp = GetProject_OutputSettlementEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_OutputSettlementMaterialsUpdateList= project_OutputSettlementMaterialsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_OutputSettlementMaterialsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_OutputSettlementMaterialsInserList= project_OutputSettlementMaterialsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_OutputSettlementMaterialsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectOutputSettlementId = project_OutputSettlementEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                    
                    //没有生成代码 
                    var Project_OutputSettlementQuantitiesUpdateList= project_OutputSettlementQuantitiesList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_OutputSettlementQuantitiesUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_OutputSettlementQuantitiesInserList= project_OutputSettlementQuantitiesList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_OutputSettlementQuantitiesInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectOutputSettlementId = project_OutputSettlementEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_OutputSettlementMaterialsEntity item in project_OutputSettlementMaterialsList)
                    {
                        item.ProjectOutputSettlementId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Project_OutputSettlementQuantitiesEntity item in project_OutputSettlementQuantitiesList)
                    {
                        item.ProjectOutputSettlementId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
