﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-25 19:58
    /// 描 述：项目立项
    /// </summary>
    public interface ProjectApprovalIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_ApprovalEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_ApprovalMembers表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_ApprovalMembersEntity> GetProject_ApprovalMembersList(string keyValue);
        /// <summary>
        /// 获取Project_Approval表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ApprovalEntity GetProject_ApprovalEntity(string keyValue);
        /// <summary>
        /// 获取Project_ApprovalMembers表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ApprovalMembersEntity GetProject_ApprovalMembersEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_ApprovalEntity entity,List<Project_ApprovalMembersEntity> project_ApprovalMembersList,string deleteList,string type);
        Project_ApprovalEntity GetKeyValue(string projectId);
        #endregion

    }
}
