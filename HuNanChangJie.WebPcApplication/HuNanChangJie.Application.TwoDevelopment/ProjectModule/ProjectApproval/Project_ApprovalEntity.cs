﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-25 19:58
    /// 描 述：项目立项
    /// </summary>
    public class Project_ApprovalEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目经理
        /// </summary>
        [Column("PROJECTMANAGERID")]
        public string ProjectManagerId { get; set; }
        /// <summary>
        /// 业务人员
        /// </summary>
        [Column("BUSINESSID")]
        public string BusinessId { get; set; }
        /// <summary>
        /// 施工部门
        /// </summary>
        [Column("CONSTRUCTIONDEPARTMENTID")]
        public string ConstructionDepartmentId { get; set; }
        /// <summary>
        /// 项目合同金额
        /// </summary>
        [Column("CONTRACTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? ContractAmount { get; set; }
        /// <summary>
        /// 项目质量要求
        /// </summary>
        [Column("QUALITYREQUIREMENTS")]
        public string QualityRequirements { get; set; }
        /// <summary>
        /// 预计开工日期
        /// </summary>
        [Column("PRESTARTDATE")]
        public DateTime? PreStartDate { get; set; }
        /// <summary>
        /// 预计完工日期
        /// </summary>
        [Column("PREENDDATE")]
        public DateTime? PreEndDate { get; set; }
        /// <summary>
        /// 预计总工期
        /// </summary>
        [Column("PRETOTALPERIOD")]
        public int? PreTotalPeriod { get; set; }
        /// <summary>
        /// 项目地址
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 建设单位(业主)
        /// </summary>
        [Column("CUSTOMERID")]
        public string CustomerId { get; set; }

        /// <summary>
        /// 勘察单位
        /// </summary>
        public string Reconnaissance { get; set; }

        /// <summary>
        /// 工程概括
        /// </summary>
        public string Generalize { get; set; }

        /// <summary>
        /// 监理单位
        /// </summary>
        [Column("SUPERVISION")]
        public string Supervision { get; set; }
        /// <summary>
        /// 设计单位
        /// </summary>
        [Column("DESIGN")]
        public string Design { get; set; }
        /// <summary>
        /// 总包单位
        /// </summary>
        [Column("TURNKEY")]
        public string Turnkey { get; set; }
        /// <summary>
        /// 施工单位
        /// </summary>
        [Column("CONSTRUCTION")]
        public string Construction { get; set; }
        /// <summary>
        /// 项目说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        /// <summary>
        /// 建设单位(业主)
        /// </summary>
        [NotMapped]
        public string CustomerName { get; set; }
        #endregion
    }
}

