﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 12:02
    /// 描 述：分包合同
    /// </summary>
    public interface ProjectSubcontractIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_SubcontractEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_SubcontractMaterials表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_SubcontractMaterialsEntity> GetProject_SubcontractMaterialsList(string keyValue);

        /// <summary>
        /// 获取Project_SubcontractQuantities表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_SubcontractQuantitiesEntity> GetProject_SubcontractQuantitiesList(string keyValue);

        IEnumerable<CB_Leasehold> GetCB_LeaseholdList(string keyValue);
        /// <summary>
        /// 获取Project_PaymentAgreement表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_PaymentAgreementEntity> GetProject_PaymentAgreementList(string keyValue);
        /// <summary>
        /// 获取Project_Subcontract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractEntity GetProject_SubcontractEntity(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractMaterialsEntity GetProject_SubcontractMaterialsEntity(string keyValue);
        IEnumerable<Project_SubcontractEntity> GetList(string projectId);

        /// <summary>
        /// 获取Project_SubcontractQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractQuantitiesEntity GetProject_SubcontractQuantitiesEntity(string keyValue);
        IEnumerable<Project_SubcontractMaterialsEntity> GetMaterials(XqPagination pagination, string queryJson);

        IEnumerable<CB_Leasehold> GetLeasehold(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取Project_PaymentAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_PaymentAgreementEntity GetProject_PaymentAgreementEntity(string keyValue);
        IEnumerable<Project_SubcontractQuantitiesEntity> GetQuantities(XqPagination pagination, string queryJson);


        /// <summary>
        /// 获取Project_PaymentAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
         CB_Leasehold GetCB_Leasehold(string keyValue);
        IEnumerable<CB_Leasehold> GetCbLeasehold(XqPagination pagination, string queryJson);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_SubcontractEntity entity,List<Project_SubcontractMaterialsEntity> project_SubcontractMaterialsList,List<Project_SubcontractQuantitiesEntity> project_SubcontractQuantitiesList,List<Project_PaymentAgreementEntity> project_PaymentAgreementList,string deleteList,string type);
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);

        void SaveEntityse(string keyValue, Project_SubcontractEntity entity, List<CB_Leasehold> CB_LeaseholdList, string deleteList, string type);
        IEnumerable<Project_SubcontractEntity> GetSubcontractList(string projectId);
        #endregion

    }
}
