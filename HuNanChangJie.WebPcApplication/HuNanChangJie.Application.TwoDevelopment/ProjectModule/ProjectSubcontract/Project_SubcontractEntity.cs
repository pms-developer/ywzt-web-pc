﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 12:02
    /// 描 述：分包合同
    /// </summary>
    public class Project_SubcontractEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }


        /// <summary>
        /// 罚款金额
        /// </summary>
        [Column("FINEEXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? FineExpend { get; set; }

        /// <summary>
        /// 其它收入
        /// </summary>
        [Column("OTHERINCOME")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherIncome { get; set; }

        /// <summary>
        /// 其它支出
        /// </summary>
        [Column("OTHEREXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherExpend { get; set; }

        /// <summary>
        /// 所属协议
        /// </summary>
        [Column("AGREEMENTID")]
        public string AgreementId { get; set; }
        /// <summary>
        /// 合同名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
       
        /// <summary>
        /// 合同编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 外部合同编码
        /// </summary>
        [Column("EXTERNALCODE")]
        public string ExternalCode { get; set; }
        /// <summary>
        /// 分包类型
        /// </summary>
        [Column("SUBCONTRACTTYPE")]
        public string SubcontractType { get; set; }
        /// <summary>
        /// 分包属性
        /// </summary>
        [Column("PROPERTY")]
        public string Property { get; set; }
        /// <summary>
        /// 甲方
        /// </summary>
        [Column("JIA")]
        public string Jia { get; set; }
        /// <summary>
        /// 乙方
        /// </summary>
        [Column("YI")]
        public string Yi { get; set; }


        [Column("YIDESC")]
        public string YiDesc { get; set; }

        [Column("YICODE")]
        public string YiCode { get; set; }


        /// <summary>
        /// 项目模式
        /// </summary>
        [Column("PROJECTMODE")]
        public string ProjectMode { get; set; }

        /// <summary>
        /// 应付款
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? AccountPayable { get; set; }
        /// <summary>
        /// 计价方式
        /// </summary>
        [Column("PRICETYPE")]
        public string PriceType { get; set; }
        /// <summary>
        /// 签定日期
        /// </summary>
        [Column("SIGNDATE")]
        public DateTime? SignDate { get; set; }
        /// <summary>
        /// 累计变更金额
        /// </summary>
        [Column("CHANGEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangeAmount { get; set; }
        /// <summary>
        /// 合同总金额
        /// </summary>
        [Column("TOTALAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalAmount { get; set; }
        /// <summary>
        /// 合同签订金额/合同暂估金额
        /// </summary>
        [Column("INITAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? InitAmount { get; set; }
        /// <summary>
        /// 是否预算控制
        /// </summary>
        [Column("ISBUDGET")]
        public bool? IsBudget { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        [Column("TAXAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxAmount { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        [Column("TAXRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRate { get; set; }
        /// <summary>
        /// 目标成本/控制价
        /// </summary>
        [Column("TARGETCOST")]
        [DecimalPrecision(18, 4)]
        public decimal? TargetCost { get; set; }
        /// <summary>
        /// 采购类型
        /// </summary>
        [Column("PURCHASETYPE")]
        public string PurchaseType { get; set; }
        /// <summary>
        /// 发票类型
        /// </summary>
        [Column("INVOICETYPE")]
        public string InvoiceType { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        [Column("STARTDATE")]
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        [Column("ENDDATE")]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 维保期限
        /// </summary>
        [Column("WARRANTY")]
        [DecimalPrecision(18, 4)]
        public decimal? Warranty { get; set; }
        /// <summary>
        /// 维保期限类型
        /// </summary>
        [Column("WARRANTYTYPE")]
        public string WarrantyType { get; set; }
        /// <summary>
        /// 履约保证金
        /// </summary>
        [Column("DEPOSIT")]
        [DecimalPrecision(18, 4)]
        public decimal? Deposit { get; set; }
        /// <summary>
        /// 质保金
        /// </summary>
        [Column("QUALITYDEPOSIT")]
        [DecimalPrecision(18, 4)]
        public decimal? QualityDeposit { get; set; }
        /// <summary>
        /// 质保金比率
        /// </summary>
        [Column("DEPOSITRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? DepositRate { get; set; }
        /// <summary>
        /// 收票金额
        /// </summary>
        [Column("INVIOCEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? InvioceAmount { get; set; }
        /// <summary>
        /// 未收票金额
        /// </summary>
        [Column("NOINVIOCEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? NoInvioceAmount { get; set; }
        /// <summary>
        /// 结算金额
        /// </summary>
        [Column("SETTLEMENTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementAmount { get; set; }
        /// <summary>
        /// 已付款金额
        /// </summary>
        [Column("ACCOUNTPAIDAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? AccountPaidAmount { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("DESCRIPTION")]
        public string Description { get; set; }
        /// <summary>
        /// 合同说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }

        /// <summary>
        /// 项目经理
        /// </summary>
        [Column("PROJECTMANAGER")]
        public string ProjectManager { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        [Column("Contacts")]
        public string Contacts { get; set; }
        /// <summary>
        /// 电话号码
        /// </summary>
        [Column("Telephone")]
        public string Telephone { get; set; }
        ///// <summary>
        ///// 工程合同金额
        ///// </summary>
        //[Column("EngineeringAmount")]
        //public decimal? EngineeringAmount { get; set; }

        /// <summary>
        /// 分包合同占工程合同比例
        /// </summary>
        [Column("subpackageEngineeringProportion")]
        public string subpackageEngineeringProportion { get; set; }


        /// <summary>
        /// 约定付款比例
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? YdfkBiLi { get; set; }





        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.TotalAmount = this.InitAmount;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
            this.TotalAmount = this.InitAmount;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
            this.TotalAmount = this.InitAmount;
        }
        #endregion
        #region  扩展字段

        [NotMapped]
        public string FullName => $"{Name}({Code})";

        [NotMapped]
        public string SubcontractTypeName { get; set; }


        /// <summary>
        /// 约定应付账款  约定应付账款=累计结算金额*约定付款比例-累计已付金额
        /// </summary>
        [NotMapped]
        public decimal? YdyfKuan
        {
            get
            {
                if (YdfkBiLi == null) return null;
                if (YdfkBiLi <= 0) return null;
                return (SettlementAmount ?? 0) * YdfkBiLi / 100 - (AccountPaidAmount ?? 0);
            }
        }

        /// <summary>
        /// 公司名称(甲方)
        /// </summary>
        [NotMapped]
        public string JiaCompanyName { get; set; }

        /// <summary>
        /// 分包商名称（乙方）
        /// </summary>
        [NotMapped]
        public string SubcontractorName { get; set; }

        #endregion
    }
}

