﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangeJie.Application.Project.ProjectSubject;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 12:02
    /// 描 述：分包合同
    /// </summary>
    public class ProjectSubcontractService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
t.ProjectManager,
                t.ID,
t.ExternalCode,
                t.Code,
                t.Name,
                t.SubcontractType,
                t.Jia,
                t.Yi,
                t.YiDesc,
                t.YiCode,
                t.InitAmount,
t.SignDate,
                t.InvoiceType,
                t.AuditStatus,
                t.Workflow_ID,
                t.CreationDate,
                b.Name as SubcontractTypeName,t.ProjectMode ,
                t.InvioceAmount, t.SettlementAmount, t.AccountPaidAmount, t.AccountPayable, t.TotalAmount, t.ChangeAmount,
                cmp.F_FullName as JiaCompanyName,
                subUnit.Name as SubcontractorName
                ");
                strSql.Append("  FROM Project_Subcontract t ");
                strSql.Append("  left join Base_ContractType as b on t.SubcontractType=b.id");
                strSql.Append("  left join Base_Company as cmp on t.Jia=cmp.F_CompanyId");
                strSql.Append("  left join Base_SubcontractingUnit as subUnit on t.Yi=subUnit.id");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }

                if (!queryParam["Yi"].IsEmpty())
                {
                    dp.Add("Yi", "%" + queryParam["Yi"].ToString() + "%", DbType.String);
                    strSql.Append(" AND subUnit.Name Like @Yi ");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }

                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectId ");
                }
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }
                if (!queryParam["ExternalCode"].IsEmpty())
                {
                    dp.Add("ExternalCode", "%" + queryParam["ExternalCode"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ExternalCode Like @ExternalCode ");
                }
                var list = this.BaseRepository().FindList<Project_SubcontractEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractMaterialsEntity> GetProject_SubcontractMaterialsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_SubcontractMaterialsEntity>(t => t.ProjectSubcontractId == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractQuantitiesEntity> GetProject_SubcontractQuantitiesList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_SubcontractQuantitiesEntity>(t => t.ProjectSubcontractId == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<CB_Leasehold> GetCB_LeaseholdList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<CB_Leasehold>(t => t.ID == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_PaymentAgreement表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_PaymentAgreementEntity> GetProject_PaymentAgreementList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_PaymentAgreementEntity>(t => t.MainID == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_Subcontract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractEntity GetProject_SubcontractEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 查询总合同金额
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public decimal? GetProject_ContractTotalAmount(string keyValue)
        {
            try
            {
                decimal? TotalAmountl = 0;
                //Project_ContractEntity Pc = new Project_ContractEntity();

                //2020-11-02 铭湛需求 修改工程合同总价为取所有审核过工程合同总价合计
                var Pc = this.BaseRepository().FindList<Project_ContractEntity>(t => t.ProjectID == keyValue && t.AuditStatus == "2");
                if (Pc != null && Pc.Count() > 0)
                    TotalAmountl = Pc.Sum(m => m.TotalAmount);
                return TotalAmountl;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractMaterialsEntity GetProject_SubcontractMaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractMaterialsEntity>(t => t.ProjectSubcontractId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractQuantitiesEntity GetProject_SubcontractQuantitiesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractQuantitiesEntity>(t => t.ProjectSubcontractId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Project_SubcontractEntity project_SubcontractEntity = db.FindEntity<Project_SubcontractEntity>(keyValue);
                if (project_SubcontractEntity == null)
                    throw new Exception("您选择的分包合同不存在");
                if (project_SubcontractEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                project_SubcontractEntity.AuditStatus = "4";

                Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(project_SubcontractEntity.SubcontractType);
                if (base_ContractTypeEntity != null)
                {
                    ProjectSubjectEntity projectSubjectEntity = db.FindEntity<ProjectSubjectEntity>(m => m.BaseSubjectId == base_ContractTypeEntity.BaseSubjectId && m.ProjectId == project_SubcontractEntity.ProjectID);

                    if (projectSubjectEntity != null)
                    {
                        projectSubjectEntity.PendingCost = (projectSubjectEntity.PendingCost ?? 0) - (project_SubcontractEntity.InitAmount ?? 0);
                        //projectSubjectEntity.PendingCost = (projectSubjectEntity.PendingCost ?? 0) + (project_SubcontractEntity.InitAmount ?? 0);
                        db.Update<ProjectSubjectEntity>(projectSubjectEntity);
                    }
                }

                //if (string.IsNullOrEmpty(project_SubcontractEntity.Yi))
                //{
                //    if (!string.IsNullOrEmpty(project_SubcontractEntity.YiDesc) && !string.IsNullOrEmpty(project_SubcontractEntity.YiCode))
                //    {
                //        Base_SubcontractingUnitEntity base_SubcontractingUnitEntity = db.FindEntity<Base_SubcontractingUnitEntity>(m => m.Name == project_SubcontractEntity.YiDesc);

                //        if (base_SubcontractingUnitEntity == null)
                //        {
                //            string yi = Guid.NewGuid().ToString();
                //            project_SubcontractEntity.Yi = yi;
                //            base_SubcontractingUnitEntity = new Base_SubcontractingUnitEntity() { ID = yi, Code = new CodeRuleBLL().GetBillCode("CJ_BASE_0022"), Name = project_SubcontractEntity.YiDesc, IdNumber = project_SubcontractEntity.YiCode };
                //            db.Insert<Base_SubcontractingUnitEntity>(base_SubcontractingUnitEntity);
                //            new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0022");
                //        }
                //        else
                //        {
                //            project_SubcontractEntity.Yi = base_SubcontractingUnitEntity.ID;
                //        }
                //    }
                //}

                db.Update<Project_SubcontractEntity>(project_SubcontractEntity);

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                Project_SubcontractEntity project_SubcontractEntity = db.FindEntity<Project_SubcontractEntity>(keyValue);
                if (project_SubcontractEntity == null)
                    throw new Exception("您选择的分包合同不存在");
                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (project_SubcontractEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                project_SubcontractEntity.AuditStatus = "2";

                Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(project_SubcontractEntity.SubcontractType);
                if (base_ContractTypeEntity != null)
                {
                    ProjectSubjectEntity projectSubjectEntity = db.FindEntity<ProjectSubjectEntity>(m => m.BaseSubjectId == base_ContractTypeEntity.BaseSubjectId && m.ProjectId == project_SubcontractEntity.ProjectID);

                    if (projectSubjectEntity != null)
                    {
                        projectSubjectEntity.PendingCost = (projectSubjectEntity.PendingCost ?? 0) + (project_SubcontractEntity.InitAmount ?? 0);
                        db.Update<ProjectSubjectEntity>(projectSubjectEntity);
                    }
                }

                if (string.IsNullOrEmpty(project_SubcontractEntity.Yi))
                {
                    if (!string.IsNullOrEmpty(project_SubcontractEntity.YiDesc) && !string.IsNullOrEmpty(project_SubcontractEntity.YiCode))
                    {
                        Base_SubcontractingUnitEntity base_SubcontractingUnitEntity = db.FindEntity<Base_SubcontractingUnitEntity>(m => m.Name == project_SubcontractEntity.YiDesc);

                        if (base_SubcontractingUnitEntity == null)
                        {
                            string yi = Guid.NewGuid().ToString();
                            project_SubcontractEntity.Yi = yi;
                            base_SubcontractingUnitEntity = new Base_SubcontractingUnitEntity() { ID = yi, AuditStatus = "2", Code = new CodeRuleBLL().GetBillCode("CJ_BASE_0022"), Name = project_SubcontractEntity.YiDesc, IdNumber = project_SubcontractEntity.YiCode };
                            db.Insert<Base_SubcontractingUnitEntity>(base_SubcontractingUnitEntity);
                            new HuNanChangJie.Application.Base.SystemModule.CodeRuleBLL().UseRuleSeed("CJ_BASE_0022");
                        }
                        else
                        {
                            project_SubcontractEntity.Yi = base_SubcontractingUnitEntity.ID;
                        }
                    }
                }

                db.Update<Project_SubcontractEntity>(project_SubcontractEntity);

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }



        public CB_Leasehold GetCB_Leasehold(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<CB_Leasehold>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_PaymentAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_PaymentAgreementEntity GetProject_PaymentAgreementEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_PaymentAgreementEntity>(t => t.MainID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<Project_SubcontractEntity> GetSubcontractList(string projectId)
        {
            try
            {
                return this.BaseRepository().FindList<Project_SubcontractEntity>(i => i.ProjectID == projectId && i.AuditStatus == "2");
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_SubcontractEntity = GetProject_SubcontractEntity(keyValue);
                db.Delete<Project_SubcontractEntity>(t => t.ID == keyValue);
                db.Delete<Project_SubcontractMaterialsEntity>(t => t.ProjectSubcontractId == project_SubcontractEntity.ID);
                db.Delete<Project_SubcontractQuantitiesEntity>(t => t.ProjectSubcontractId == project_SubcontractEntity.ID);
                db.Delete<Project_PaymentAgreementEntity>(t => t.MainID == project_SubcontractEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void Update(Project_SubcontractEntity contractInfo, List<Project_SubcontractQuantitiesEntity> gclList, List<Project_SubcontractMaterialsEntity> clList)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                var flag = false;
                if (contractInfo != null)
                {
                    flag = true;
                    db.Update(contractInfo);
                }
                if (gclList.Count > 0)
                {
                    flag = true;
                    db.Update(gclList);
                }
                if (clList.Count > 0)
                {
                    flag = true;
                    db.Update(clList);
                }
                if (flag)
                {
                    db.Commit();
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SubcontractEntity entity, List<Project_SubcontractMaterialsEntity> project_SubcontractMaterialsList, List<Project_SubcontractQuantitiesEntity> project_SubcontractQuantitiesList, List<Project_PaymentAgreementEntity> project_PaymentAgreementList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var project_SubcontractEntityTmp = GetProject_SubcontractEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Project_SubcontractMaterialsUpdateList = project_SubcontractMaterialsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_SubcontractMaterialsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_SubcontractMaterialsInserList = project_SubcontractMaterialsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_SubcontractMaterialsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectSubcontractId = project_SubcontractEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Project_SubcontractQuantitiesUpdateList = project_SubcontractQuantitiesList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_SubcontractQuantitiesUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_SubcontractQuantitiesInserList = project_SubcontractQuantitiesList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_SubcontractQuantitiesInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectSubcontractId = project_SubcontractEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Project_PaymentAgreementUpdateList = project_PaymentAgreementList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_PaymentAgreementUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_PaymentAgreementInserList = project_PaymentAgreementList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_PaymentAgreementInserList)
                    {
                        item.Create(item.ID);
                        item.MainID = project_SubcontractEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_SubcontractMaterialsEntity item in project_SubcontractMaterialsList)
                    {
                        item.ProjectSubcontractId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Project_SubcontractQuantitiesEntity item in project_SubcontractQuantitiesList)
                    {
                        item.ProjectSubcontractId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Project_PaymentAgreementEntity item in project_PaymentAgreementList)
                    {
                        item.MainID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntityse(string keyValue, Project_SubcontractEntity entity, List<CB_Leasehold> CB_LeaseholdList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var project_SubcontractEntityTmp = GetProject_SubcontractEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    //var Project_SubcontractMaterialsUpdateList = project_SubcontractMaterialsList.FindAll(i => i.EditType == EditType.Update);
                    //foreach (var item in Project_SubcontractMaterialsUpdateList)
                    //{
                    //    db.Update(item);
                    //}
                    //var Project_SubcontractMaterialsInserList = project_SubcontractMaterialsList.FindAll(i => i.EditType == EditType.Add);
                    //foreach (var item in Project_SubcontractMaterialsInserList)
                    //{
                    //    item.Create(item.ID);
                    //    item.ProjectSubcontractId = project_SubcontractEntityTmp.ID;
                    //    db.Insert(item);
                    //}

                    //没有生成代码1 

                    if (CB_LeaseholdList != null)
                    {
                        //没有生成代码 
                        var Project_SubcontractQuantitiesUpdateList = CB_LeaseholdList.FindAll(i => i.EditType == EditType.Update);
                        foreach (var item in Project_SubcontractQuantitiesUpdateList)
                        {
                            db.Update(item);
                        }
                        var Project_SubcontractQuantitiesInserList = CB_LeaseholdList.FindAll(i => i.EditType == EditType.Add);
                        foreach (var item in Project_SubcontractQuantitiesInserList)
                        {
                            item.Create(item.ID);
                            item.ID = project_SubcontractEntityTmp.ID;
                            db.Insert(item);
                        }
                    }




                    //没有生成代码1 

                    //没有生成代码 
                    //var Project_PaymentAgreementUpdateList = project_PaymentAgreementList.FindAll(i => i.EditType == EditType.Update);
                    //foreach (var item in Project_PaymentAgreementUpdateList)
                    //{
                    //    db.Update(item);
                    //}
                    //var Project_PaymentAgreementInserList = project_PaymentAgreementList.FindAll(i => i.EditType == EditType.Add);
                    //foreach (var item in Project_PaymentAgreementInserList)
                    //{
                    //    item.Create(item.ID);
                    //    item.MainID = project_SubcontractEntityTmp.ID;
                    //    db.Insert(item);
                    //}

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    //foreach (Project_SubcontractMaterialsEntity item in project_SubcontractMaterialsList)
                    //{
                    //    item.ProjectSubcontractId = entity.ID;
                    //    db.Insert(item);
                    //}
                    foreach (CB_Leasehold item in CB_LeaseholdList)
                    {
                        item.ID = entity.ID;
                        db.Insert(item);
                    }
                    //foreach (Project_PaymentAgreementEntity item in project_PaymentAgreementList)
                    //{
                    //    item.MainID = entity.ID;
                    //    db.Insert(item);
                    //}
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }




        public IEnumerable<Project_SubcontractQuantitiesEntity> GetQuantities(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("select * from Project_SubcontractQuantities where 1=1");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["contractId"].IsEmpty())
                {
                    dp.Add("contractId", queryParam["contractId"].ToString(), DbType.String);

                    strSql.Append(" AND ProjectSubcontractId = @contractId");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append("    AND  name like @Name");
                }
                var list = this.BaseRepository().FindList<Project_SubcontractQuantitiesEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;

                //var list = BaseRepository().FindList<Project_SubcontractQuantitiesEntity>(i => i.ProjectSubcontractId == contractId);
                //var query = from item in list orderby item.SortCode ascending select item;
                //return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public IEnumerable<CB_Leasehold> GetCbLeasehold(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("select * from CB_Leasehold where 1=1");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ID"].IsEmpty())
                {
                    dp.Add("ID", queryParam["ID"].ToString(), DbType.String);

                    strSql.Append(" AND ID = @ID");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append("    AND  name like @Name");
                }
                var list = this.BaseRepository().FindList<CB_Leasehold>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }




        public IEnumerable<Project_SubcontractMaterialsEntity> GetMaterials(XqPagination pagination, string queryJson)
        {
            try
            {

                var strSql = new StringBuilder();
                strSql.Append("select * from Project_SubcontractMaterials where 1=1");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["contractId"].IsEmpty())
                {
                    dp.Add("contractId", queryParam["contractId"].ToString(), DbType.String);

                    strSql.Append(" AND ProjectSubcontractId = @contractId");
                }
                if (!queryParam["projectid"].IsEmpty())
                {
                    dp.Add("projectid", queryParam["projectid"].ToString(), DbType.String);

                    strSql.Append("  AND projectid = @projectid");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append("    AND  name like @Name");
                }
                var list = this.BaseRepository().FindList<Project_SubcontractMaterialsEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;

                //var queryParam = queryJson.ToJObject();
                // 虚拟参数
                //var dp = new DynamicParameters(new { });
                //IEnumerable<Project_SubcontractMaterialsEntity> project_SubcontractMaterialsEntityList = null;
                //if (!queryParam["contractId"].IsEmpty()) {
                //    string contractId = queryParam["contractId"].ToString();
                //    project_SubcontractMaterialsEntityList = BaseRepository().FindList<Project_SubcontractMaterialsEntity>(i => i.ProjectSubcontractId == contractId, pagination);
                //    //var list = this.BaseRepository().FindList<Project_SubcontractMaterialsEntity>(strSql.);
                //}
                //else
                //{
                //    project_SubcontractMaterialsEntityList = BaseRepository().FindList<Project_SubcontractMaterialsEntity>(pagination);
                //}
                //var query = from item in project_SubcontractMaterialsEntityList orderby item.SortCode ascending select item;
                //return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        public IEnumerable<CB_Leasehold> GetLeasehold(XqPagination pagination, string queryJson)
        {
            try
            {

                var strSql = new StringBuilder();
                strSql.Append("select * from CB_Leasehold where 1=1");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["projectid"].IsEmpty())
                {
                    dp.Add("projectid", queryParam["projectid"].ToString(), DbType.String);

                    strSql.Append("  AND ID = @projectid");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append("    AND  name like @Name");
                }
                var list = this.BaseRepository().FindList<CB_Leasehold>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }




        public IEnumerable<Project_SubcontractEntity> GetList(string projectId)
        {
            try
            {

                var strSql = new StringBuilder();
                strSql.Append(@"select a.Code,a.SubcontractType,b.name as SubcontractTypeName,
                a.InitAmount,a.TotalAmount,a.*,(select Name from Base_SubcontractingUnit as bu where bu.ID=a.Yi) as Yi from Project_Subcontract a left join Base_ContractType b on a.SubcontractType=b.id where 1=1 ");
                //var queryParam = projectId.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!projectId.IsEmpty())
                {
                    dp.Add("projectid", projectId, DbType.String);

                    strSql.Append("  AND a.projectid = @projectid");
                }
                if (!((int)AuditStatus.Pass).ToString().IsEmpty())
                {
                    //dp.Add("AuditStatus", "%" + projectId.ToString() + "%", DbType.String);
                    strSql.Append("  AND  a.AuditStatus ='" + ((int)AuditStatus.Pass).ToString() + "'");
                }
                var list = this.BaseRepository().FindList<Project_SubcontractEntity>(strSql.ToString(), dp);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;


                //var list= BaseRepository().FindList<Project_SubcontractEntity>(i => i.ProjectID == projectId && i.AuditStatus == ((int)AuditStatus.Pass).ToString());
                //var query = from i in list orderby i.CreationDate descending select i;
                //return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
