﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 12:02
    /// 描 述：分包合同
    /// </summary>
    public class ProjectSubcontractBLL : ProjectSubcontractIBLL
    {
        private ProjectSubcontractService projectSubcontractService = new ProjectSubcontractService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectSubcontractService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractMaterialsEntity> GetProject_SubcontractMaterialsList(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetProject_SubcontractMaterialsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractQuantitiesEntity> GetProject_SubcontractQuantitiesList(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetProject_SubcontractQuantitiesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取CB_Leasehold表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<CB_Leasehold> GetCB_LeaseholdList(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetCB_LeaseholdList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_PaymentAgreement表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_PaymentAgreementEntity> GetProject_PaymentAgreementList(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetProject_PaymentAgreementList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_Subcontract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractEntity GetProject_SubcontractEntity(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetProject_SubcontractEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractMaterialsEntity GetProject_SubcontractMaterialsEntity(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetProject_SubcontractMaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractQuantitiesEntity GetProject_SubcontractQuantitiesEntity(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetProject_SubcontractQuantitiesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取CB_Leasehold表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public CB_Leasehold GetCB_Leasehold(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetCB_Leasehold(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        public decimal? GetProject_ContractTotalAmount(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetProject_ContractTotalAmount(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        
        /// <summary>
        /// 获取Project_PaymentAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_PaymentAgreementEntity GetProject_PaymentAgreementEntity(string keyValue)
        {
            try
            {
                return projectSubcontractService.GetProject_PaymentAgreementEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectSubcontractService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SubcontractEntity entity,List<Project_SubcontractMaterialsEntity> project_SubcontractMaterialsList,List<Project_SubcontractQuantitiesEntity> project_SubcontractQuantitiesList,List<Project_PaymentAgreementEntity> project_PaymentAgreementList,string deleteList,string type)
        {
            try
            {
                projectSubcontractService.SaveEntity(keyValue, entity,project_SubcontractMaterialsList,project_SubcontractQuantitiesList,project_PaymentAgreementList,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("ProjectSubcontractCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        //public void Audit(string keyValue)
        //{
        //    try
        //    {
        //        var mainInfo = projectSubcontractService.GetProject_SubcontractEntity(keyValue);
        //        var typeInfo = new BaseContractTypeBLL().GetBase_ContractTypeEntity(mainInfo.SubcontractType);
        //        var projectSubjectBll = new ProjectSubjectBll();
        //        var projectSubjectInfo = projectSubjectBll.GetEntity(typeInfo.BaseSubjectId, mainInfo.ProjectID);
        //        if (projectSubjectInfo != null)
        //        {
        //            projectSubjectInfo.PendingCost = (projectSubjectInfo.PendingCost ?? 0) + (mainInfo.InitAmount ?? 0);
        //            projectSubjectBll.UpdateInfo(projectSubjectInfo);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is ExceptionEx)
        //        {
        //            throw;
        //        }
        //        else
        //        {
        //            throw ExceptionEx.ThrowBusinessException(ex);
        //        }
        //    }
        //}

        public void Update(Project_SubcontractEntity contractInfo, List<Project_SubcontractQuantitiesEntity> gclList, List<Project_SubcontractMaterialsEntity> clList)
        {
            try
            {
                projectSubcontractService.Update(contractInfo, gclList, clList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return projectSubcontractService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return projectSubcontractService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        //public void UnAudit(string keyValue)
        //{
        //    try
        //    {
        //        var mainInfo = projectSubcontractService.GetProject_SubcontractEntity(keyValue);
        //        var typeInfo = new BaseContractTypeBLL().GetBase_ContractTypeEntity(mainInfo.SubcontractType);
        //        var projectSubjectBll = new ProjectSubjectBll();
        //        var projectSubjectInfo = projectSubjectBll.GetEntity(typeInfo.BaseSubjectId, mainInfo.ProjectID);
        //        if (projectSubjectInfo != null)
        //        {
        //            projectSubjectInfo.PendingCost = (projectSubjectInfo.PendingCost ?? 0) - (mainInfo.InitAmount ?? 0);


        //            projectSubjectBll.UpdateInfo(projectSubjectInfo);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is ExceptionEx)
        //        {
        //            throw;
        //        }
        //        else
        //        {
        //            throw ExceptionEx.ThrowBusinessException(ex);
        //        }
        //    }
        //}

        public IEnumerable<Project_SubcontractEntity> GetList(string projectId)
        {
            try
            {
                return projectSubcontractService.GetList(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_SubcontractMaterialsEntity> GetMaterials(XqPagination pagination,string queryJson)
        {
            try
            {
                return projectSubcontractService.GetMaterials(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<CB_Leasehold> GetLeasehold(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectSubcontractService.GetLeasehold(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public IEnumerable<Project_SubcontractQuantitiesEntity> GetQuantities(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectSubcontractService.GetQuantities(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<CB_Leasehold> GetCbLeasehold(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectSubcontractService.GetCbLeasehold(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void SaveEntityse(string keyValue, Project_SubcontractEntity entity,  List<CB_Leasehold> CB_LeaseholdList, string deleteList, string type)
        {
            try
            {
                projectSubcontractService.SaveEntityse(keyValue, entity, CB_LeaseholdList, deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("ProjectSubcontractCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_SubcontractEntity> GetSubcontractList(string projectId)
        {
            try
            {
                return projectSubcontractService.GetSubcontractList(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
