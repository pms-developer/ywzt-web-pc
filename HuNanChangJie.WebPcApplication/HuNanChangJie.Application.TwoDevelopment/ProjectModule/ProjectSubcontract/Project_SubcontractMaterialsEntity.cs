﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 12:02
    /// 描 述：分包合同
    /// </summary>
    public class Project_SubcontractMaterialsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 材料库材料ID
        /// </summary>
        [Column("FK_BASEMATERIALSID")]
        public string Fk_BaseMaterialsId { get; set; }
        /// <summary>
        /// 项目编码
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 分包合同ID
        /// </summary>
        [Column("PROJECTSUBCONTRACTID")]
        public string ProjectSubcontractId { get; set; }
        /// <summary>
        /// 清单编码
        /// </summary>
        [Column("LISTCODE")]
        public string ListCode { get; set; }
        /// <summary>
        /// 材料编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODELNUMBER")]
        public string ModelNumber { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
        /// <summary>
        /// 合计金额
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// 累计变更数量
        /// </summary>
        [Column("CHANGEQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangeQuantity { get; set; }
        /// <summary>
        /// 累计变更单价
        /// </summary>
        [Column("CHANGEPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangePrice { get; set; }
        /// <summary>
        /// 累计变更合计金额
        /// </summary>
        [Column("CHANGETOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangeTotalPrice { get; set; }
        /// <summary>
        /// 累计结算数量
        /// </summary>
        [Column("SETTLEMENTQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementQuantities { get; set; }
        /// <summary>
        /// 累计结算单价
        /// </summary>
        [Column("SETTLEMENTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementPrice { get; set; }
        /// <summary>
        /// 累计结算合价
        /// </summary>
        [Column("SETTLEMENTTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementTotalPrice { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// 材料来源
        /// </summary>
        [Column("MaterialsSource")]
        public bool? MaterialsSource { get; set; }

        public int? SortCode { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

