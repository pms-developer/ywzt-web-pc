﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-22 12:02
    /// 描 述：分包合同
    /// </summary>
    public class Project_SubcontractQuantitiesEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 分包合同ID
        /// </summary>
        [Column("PROJECTSUBCONTRACTID")]
        public string ProjectSubcontractId { get; set; }
        /// <summary>
        /// 清单编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 清单名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 项目特征
        /// </summary>
        [Column("FEATURE")]
        public string Feature { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 工程量
        /// </summary>
        [Column("QUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantities { get; set; }
        /// <summary>
        /// 综合单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
        /// <summary>
        /// 合价
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// 累计变更工程量
        /// </summary>
        [Column("CHANGEQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangeQuantities { get; set; }
        /// <summary>
        /// 累计变更综合单价
        /// </summary>
        [Column("CHANGEPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangePrice { get; set; }
        /// <summary>
        /// 累计变更合价
        /// </summary>
        [Column("CHANGETOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangeTotalPrice { get; set; }
        /// <summary>
        /// 累计结算合价
        /// </summary>
        [Column("SETTLEMENTTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementTotalPrice { get; set; }
        /// <summary>
        /// 累计结算工程量
        /// </summary>
        [Column("SETTLEMENTQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementQuantities { get; set; }
        /// <summary>
        /// 累计结算综合单价
        /// </summary>
        [Column("SETTLEMENTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementPrice { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        public int? SortCode { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

