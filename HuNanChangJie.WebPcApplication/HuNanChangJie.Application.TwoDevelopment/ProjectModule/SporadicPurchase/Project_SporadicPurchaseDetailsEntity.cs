﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 09:22
    /// 描 述：零星采购
    /// </summary>
    public class Project_SporadicPurchaseDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 零星采购ID
        /// </summary>
        [Column("PROJECTSPORADICPURCHASEID")]
        public string ProjectSporadicPurchaseId { get; set; }
        /// <summary>
        /// 材料编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 采购名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODELNUMBER")]
        public string ModelNumber { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }

        /// <summary>
        /// 是否自动入库 0：否  1：是
        /// </summary>
        [Column("ISPUTINSTORAGE")]
        public bool? IsPutInStorage { get; set; }

        /// <summary>
        /// 入库仓库id
        /// </summary>
        [Column("BASE_WAREHOUSEID")]
        public string Base_WarehouseId { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        [Column("TAXRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRate { get; set; }
        /// <summary>
        /// 单价(不含税)
        /// </summary>
        [Column("CONTAINTAXPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ContainTaxPrice { get; set; }
        /// <summary>
        /// 总金额(不含税)
        /// </summary>
        [Column("NOCONTAINTAXPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? NoContainTaxPrice { get; set; }

        /// <summary>
        /// 材料id
        /// </summary>
        [Column("BASE_MATERIALSID")]
        public string Base_MaterialsId { get; set; }

        /// <summary>
        /// 采购供应商
        /// </summary>
        [Column("SUPPLIER")]
        public string Supplier { get; set; }
        /// <summary>
        /// 单价(元)（含税）
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 金额（含税）
        /// </summary>
        [Column("AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Amount { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 材料来源
        /// </summary>
        [Column("MATERIALSSOURCE")]
        public bool? MaterialsSource { get; set; }


        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

