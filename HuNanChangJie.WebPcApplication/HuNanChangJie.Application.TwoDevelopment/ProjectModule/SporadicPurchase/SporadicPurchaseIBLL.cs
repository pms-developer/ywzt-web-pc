﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 09:22
    /// 描 述：零星采购
    /// </summary>
    public interface SporadicPurchaseIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_SporadicPurchaseEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_SporadicPurchaseDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_SporadicPurchaseDetailsEntity> GetProject_SporadicPurchaseDetailsList(string keyValue);
        /// <summary>
        /// 获取Project_SporadicPurchase表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SporadicPurchaseEntity GetProject_SporadicPurchaseEntity(string keyValue);
        /// <summary>
        /// 获取Project_SporadicPurchaseDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SporadicPurchaseDetailsEntity GetProject_SporadicPurchaseDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_SporadicPurchaseEntity entity,List<Project_SporadicPurchaseDetailsEntity> project_SporadicPurchaseDetailsList,string deleteList,string type);
        #endregion

    }
}
