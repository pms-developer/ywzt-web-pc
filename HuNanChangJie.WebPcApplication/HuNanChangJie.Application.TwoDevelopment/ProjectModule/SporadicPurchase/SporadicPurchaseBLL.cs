﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 09:22
    /// 描 述：零星采购
    /// </summary>
    public class SporadicPurchaseBLL : SporadicPurchaseIBLL
    {
        private SporadicPurchaseService sporadicPurchaseService = new SporadicPurchaseService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SporadicPurchaseEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return sporadicPurchaseService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SporadicPurchaseDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SporadicPurchaseDetailsEntity> GetProject_SporadicPurchaseDetailsList(string keyValue)
        {
            try
            {
                return sporadicPurchaseService.GetProject_SporadicPurchaseDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SporadicPurchase表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SporadicPurchaseEntity GetProject_SporadicPurchaseEntity(string keyValue)
        {
            try
            {
                return sporadicPurchaseService.GetProject_SporadicPurchaseEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SporadicPurchaseDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SporadicPurchaseDetailsEntity GetProject_SporadicPurchaseDetailsEntity(string keyValue)
        {
            try
            {
                return sporadicPurchaseService.GetProject_SporadicPurchaseDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                sporadicPurchaseService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SporadicPurchaseEntity entity,List<Project_SporadicPurchaseDetailsEntity> project_SporadicPurchaseDetailsList,string deleteList,string type)
        {
            try
            {
                sporadicPurchaseService.SaveEntity(keyValue, entity,project_SporadicPurchaseDetailsList,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("SporadicPurchaseCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
