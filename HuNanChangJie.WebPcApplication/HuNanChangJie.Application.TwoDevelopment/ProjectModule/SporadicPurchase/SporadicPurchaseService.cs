﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-11 09:22
    /// 描 述：零星采购
    /// </summary>
    public class SporadicPurchaseService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SporadicPurchaseEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT *  FROM Project_SporadicPurchase t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                if (!queryParam["sword"].IsEmpty())
                {
                    dp.Add("sword", "%" + queryParam["sword"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @sword ");
                }
                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                if (!queryParam["PurchaseUserId"].IsEmpty())
                {
                    dp.Add("PurchaseUserId", queryParam["PurchaseUserId"].ToString(), DbType.String);
                    strSql.Append(" AND PurchaseUserId = @PurchaseUserId");
                }
                
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND ProjectID = @ProjectID");
                }
                
                var list=this.BaseRepository().FindList<Project_SporadicPurchaseEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SporadicPurchaseDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SporadicPurchaseDetailsEntity> GetProject_SporadicPurchaseDetailsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Project_SporadicPurchaseDetailsEntity>(t=>t.ProjectSporadicPurchaseId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SporadicPurchase表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SporadicPurchaseEntity GetProject_SporadicPurchaseEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SporadicPurchaseEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SporadicPurchaseDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SporadicPurchaseDetailsEntity GetProject_SporadicPurchaseDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SporadicPurchaseDetailsEntity>(t=>t.ProjectSporadicPurchaseId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_SporadicPurchaseEntity = GetProject_SporadicPurchaseEntity(keyValue); 
                db.Delete<Project_SporadicPurchaseEntity>(t=>t.ID == keyValue);
                db.Delete<Project_SporadicPurchaseDetailsEntity>(t=>t.ProjectSporadicPurchaseId == project_SporadicPurchaseEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SporadicPurchaseEntity entity,List<Project_SporadicPurchaseDetailsEntity> project_SporadicPurchaseDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_SporadicPurchaseEntityTmp = GetProject_SporadicPurchaseEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_SporadicPurchaseDetailsUpdateList= project_SporadicPurchaseDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_SporadicPurchaseDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_SporadicPurchaseDetailsInserList= project_SporadicPurchaseDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_SporadicPurchaseDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectSporadicPurchaseId = project_SporadicPurchaseEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_SporadicPurchaseDetailsEntity item in project_SporadicPurchaseDetailsList)
                    {
                        item.Create(item.ID);
                        item.ProjectSporadicPurchaseId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
