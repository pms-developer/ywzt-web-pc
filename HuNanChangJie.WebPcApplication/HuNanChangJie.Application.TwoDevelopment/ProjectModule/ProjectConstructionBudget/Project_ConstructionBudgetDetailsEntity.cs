﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-03 20:43
    /// 描 述：施工预算
    /// </summary>
    public class Project_ConstructionBudgetDetailsEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目科目ID
        /// </summary>
        [Column("PROJECTSUBJECTID")]
        public string ProjectSubjectId { get; set; }

        public string SubjectParenId { get; set; }
        /// <summary>
        /// 施工预算ID
        /// </summary>
        [Column("PROJECTCONSTRUCTIONBUDGETID")]
        public string ProjectConstructionBudgetId { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 取值方式
        /// </summary>
        [Column("VALUETYPE")]
        public string ValueType { get; set; }
        /// <summary>
        /// 费率
        /// </summary>
        [Column("RATE")]
        [DecimalPrecision(18, 4)]
        public decimal? Rate { get; set; }
        /// <summary>
        /// 计算公式
        /// </summary>
        [Column("FORMULAS")]
        public string Formulas { get; set; }
        /// <summary>
        /// 预算金额
        /// </summary>
        [Column("BUDGETAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? BudgetAmount { get; set; }
        /// <summary>
        /// 占比
        /// </summary>
        [Column("RATIO")]
        [DecimalPrecision(18, 4)]
        public decimal? Ratio { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }


        /// <summary>
        /// 实际发生成本（含税）
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? ActuallyOccurred { get; set; }

        /// <summary>
        /// 实际发生成本（不含税）
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? AOAfterTax { get; set; }

        /// <summary>
        /// 待发生成本
        /// </summary>

        [DecimalPrecision(18, 4)] 
        public decimal? PendingCost { get; set; }

        /// <summary>
        /// 竣工结算成本
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? FinalCost { get; set; }

        /// <summary>
        /// 超预算金额
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? ExceedBudgetAmount { get; set; }

        /// <summary>
        /// 超预算比例
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? ExceedBudgetRate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region  扩展属性

        /// <summary>
        /// 科目编码
        /// </summary>
        [NotMapped]
        public string Code { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// 成本属性
        /// </summary>
        [NotMapped]
        public string CostAttributeName { get; set; }

        /// <summary>
        /// 收支科目ID
        /// </summary>
        [NotMapped]
        public string BaseSubjectId { get; set; }

        /// <summary>
        /// 收支科目父级ID
        /// </summary>
        [NotMapped]
        public string ParentId { get; set; }
        #endregion
    }
}

