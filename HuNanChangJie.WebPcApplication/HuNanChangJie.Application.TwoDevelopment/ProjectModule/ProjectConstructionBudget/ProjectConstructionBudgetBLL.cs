﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.ProjectSubject;
using System.Linq;
using HuNanChangJie.Util.Property;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-03 20:43
    /// 描 述：施工预算
    /// </summary>
    public class ProjectConstructionBudgetBLL : ProjectConstructionBudgetIBLL
    {
        private ProjectConstructionBudgetService projectConstructionBudgetService = new ProjectConstructionBudgetService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectConstructionBudgetService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取根据项目ID材料档案
        /// <summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        public IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination, string projectId,string queryJson)
        {
            try
            {
                return projectConstructionBudgetService.GetMaterialsInfoList(pagination, projectId, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetProject_ConstructionBudgetMaterialsLists(IEnumerable<string> ids)
        {
            try
            {
                return projectConstructionBudgetService.GetProject_ConstructionBudgetMaterialsLists(ids);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ConstructionBudgetQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetQuantitiesEntity> GetProject_ConstructionBudgetQuantitiesList(string keyValue)
        {
            try
            {
                return projectConstructionBudgetService.GetProject_ConstructionBudgetQuantitiesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public DataTable GetQuantities(string keyValue)
        {
            try
            {
                var table = projectConstructionBudgetService.GetQuantities(keyValue);
                var dv = table.DefaultView;
                var query = from item in table.AsEnumerable()
                            orderby (item["SortCode"].ToDecimal()) ascending
                            select item;
                dv = query.AsDataView();
                table = dv.ToTable();
                var info = new Project_ConstructionBudgetQuantitiesEntity();
                var fields = FieldProperty.GetPropertyNames(info);

                for (var i = 0; i < table.Columns.Count; i++)
                {
                    var colName = table.Columns[i].ColumnName;
                    foreach (var name in fields)
                    {
                        if (colName == name.ToLower())
                        {
                            table.Columns[i].ColumnName = name;
                        }
                    }
                }
                return table;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ConstructionBudgetMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetProject_ConstructionBudgetMaterialsList(string keyValue)
        {
            try
            {
                return projectConstructionBudgetService.GetProject_ConstructionBudgetMaterialsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetMaterialsListByProjectId(string projectId)
        {
            try
            {
                return projectConstructionBudgetService.GetMaterialsListByProjectId(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UpdateMaterials(List<Project_ConstructionBudgetMaterialsEntity> materialsList)
        {
            try
            {
                projectConstructionBudgetService.UpdateMaterials(materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetMaterials(IEnumerable<string> materialIds)
        {
            try
            {
                return projectConstructionBudgetService.GetMaterials(materialIds);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ConstructionBudget表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ConstructionBudgetEntity GetProject_ConstructionBudgetEntity(string keyValue)
        {
            try
            {
                return projectConstructionBudgetService.GetProject_ConstructionBudgetEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取预算列表
        /// <param name="KeyId">公司ID，部门ID或者店铺ID</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetEntity> GetBudgetList(BudgetQueryModel query)
        {
            try
            {
                return projectConstructionBudgetService.GetBudgetLis(query);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取预算列表
        /// <param name="KeyId">公司ID，部门ID或者店铺ID</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubjectFeeDetailsEntity> Project_SubjectFeeDetailsEntityList(string budgetId)
        {
            try
            {
                return projectConstructionBudgetService.Project_SubjectFeeDetailsEntityList(budgetId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        

        /// <summary>
        /// 获取Project_ConstructionBudgetQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ConstructionBudgetQuantitiesEntity GetProject_ConstructionBudgetQuantitiesEntity(string keyValue)
        {
            try
            {
                return projectConstructionBudgetService.GetProject_ConstructionBudgetQuantitiesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ConstructionBudgetMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ConstructionBudgetMaterialsEntity GetProject_ConstructionBudgetMaterialsEntity(string keyValue)
        {
            try
            {
                return projectConstructionBudgetService.GetProject_ConstructionBudgetMaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectConstructionBudgetService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_ConstructionBudgetEntity entity, List<ProjectSubjectEntity> projectSubjectList, List<Project_ConstructionBudgetQuantitiesEntity> project_ConstructionBudgetQuantitiesList, List<Project_ConstructionBudgetMaterialsEntity> project_ConstructionBudgetMaterialsList, string deleteList, string type)
        {
            try
            {
                projectConstructionBudgetService.SaveEntity(keyValue, entity, projectSubjectList, project_ConstructionBudgetQuantitiesList, project_ConstructionBudgetMaterialsList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveBudget(string budgetId, Project_ConstructionBudgetEntity entity, List<ProjectSubjectEntity> projectSubjectList, List<Project_SubjectFeeDetailsEntity> projectSubjectFeeDetailsEntity, List<Project_ConstructionBudgetMaterialsEntity> project_ConstructionBudgetMaterialsList, string deleteList, string type)
        {
            try
            {
                projectConstructionBudgetService.SaveBudget(budgetId, entity, projectSubjectList, projectSubjectFeeDetailsEntity, project_ConstructionBudgetMaterialsList, deleteList, type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

       

        /// <summary>
        /// 获取施工预算材料清单
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetMaterials(string projectId)
        {
            try
            {
                var list = projectConstructionBudgetService.GetMaterials(projectId);
                var query = from item in list
                            where item.AllowQuantity > 0
                            orderby item.SortCode ascending
                            select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsExceedBudget(string subjectId, string projectId, decimal amount)
        {
            try
            {
                return projectConstructionBudgetService.CheckIsExceedBudget(subjectId, projectId, amount);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsExceedBudgetByBaseId(string subjectId, string projectId, decimal amount, string id)
        {
            try
            {
                return projectConstructionBudgetService.CheckIsExceedBudgetByBaseId(subjectId, projectId, amount, id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool CheckIsExceedBudgetByProjectId(string projectId, decimal amount, string id,string typeid)
        {
            try
            {
                return projectConstructionBudgetService.CheckIsExceedBudgetByProjectId(projectId, amount, id,typeid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetBudgetInfo(string projectId)
        {
            try
            {
                return projectConstructionBudgetService.GetBudgetInfo(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Project_ConstructionBudgetEntity GetFormInfo(string projectId)
        {
            try
            {
                return projectConstructionBudgetService.GetFormInfo(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Project_ConstructionBudgetEntity GetFormInfoByCompanyId(string companyId)
        {
            try
            {
                return projectConstructionBudgetService.GetFormInfoByCompanyId(companyId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Project_ConstructionBudgetEntity GetFormInfoByDepartmentId(string departmentId)
        {
            try
            {
                return projectConstructionBudgetService.GetFormInfoByDepartmentId(departmentId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
