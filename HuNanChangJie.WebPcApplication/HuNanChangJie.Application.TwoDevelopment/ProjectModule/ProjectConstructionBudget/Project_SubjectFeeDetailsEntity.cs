﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2022-01-17 11:15
    /// 描 述：预算科目费用清单
    /// </summary>
    public class Project_SubjectFeeDetailsEntity:BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        /// <returns></returns>
        [Column("SUBJECTFEEDETAILS_ID")]
        public string SubjectFeeDetails_Id { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        /// <returns></returns>
        [Column("PROJECTSUBJECTID")]
        public string ProjectSubjectId { get; set; }
        /// <summary>
        /// 费用清单名称
        /// </summary>
        /// <returns></returns>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        /// <returns></returns>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 购买数量
        /// </summary>
        /// <returns></returns>
        [Column("QUANTITY")]
        public int? Quantity { get; set; }
        /// <summary>
        /// 购买单价
        /// </summary>
        /// <returns></returns>
        [Column("PRICE")]
        public decimal? Price { get; set; }
        /// <summary>
        /// 合计金额
        /// </summary>
        /// <returns></returns>
        [Column("TOTALPRICE")]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// CreationDate
        /// </summary>
        /// <returns></returns>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// Creation_Id
        /// </summary>
        /// <returns></returns>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// ModificationDate
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATIONDATE")]
        public DateTime ModificationDate { get; set; }
        /// <summary>
        /// Modification_Id
        /// </summary>
        /// <returns></returns>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.SubjectFeeDetails_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.SubjectFeeDetails_Id = keyValue;
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.SubjectFeeDetails_Id = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
        }
        #endregion
    }
}

