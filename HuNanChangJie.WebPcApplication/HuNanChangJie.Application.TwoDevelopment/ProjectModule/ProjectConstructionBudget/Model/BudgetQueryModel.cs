﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    public class BudgetQueryModel
    {
        public string KeyId { get; set; }

        public int Year { get; set; }

        public int? MinMonth { get; set; }
        public int? MaxMonth { get; set; }
    }
}