﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-03 20:43
    /// 描 述：施工预算
    /// </summary>
    public class Project_ConstructionBudgetMaterialsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目编码
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }

        /// <summary>
        /// 材料库材料ID
        /// </summary>
        [Column("FK_BASEMATERIALSID")]
        public string Fk_BaseMaterialsId { get; set; }
        /// <summary>
        /// 项目投标报价ID
        /// </summary>
        [Column("PROJECTCONSTRUCTIONBUDGETID")]
        public string ProjectConstructionBudgetId { get; set; }
        /// <summary>
        /// 清单编码
        /// </summary>
        [Column("LISTCODE")]
        public string ListCode { get; set; }
        /// <summary>
        /// 材料编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODELNUMBER")]
        public string ModelNumber { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 投标预算单价(含税单价)
        /// </summary>
        [Column("BUDGETPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? BudgetPrice { get; set; }

        /// <summary>
        /// 不含税单价
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? AfterTaxPrices { get; set; }
        /// <summary>
        /// 合计金额
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// 已申请采购数量
        /// </summary>
        [Column("APPLYQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? ApplyQuantity { get; set; }
        /// <summary>
        /// 已申请采单价
        /// </summary>
        [Column("APPLYPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ApplyPrice { get; set; }
        /// <summary>
        /// 已申请采购合计金额
        /// </summary>
        [Column("APPLYTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ApplyTotalPrice { get; set; }
        /// <summary>
        /// 销售价
        /// </summary>
        [Column("SELLPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SellPrice { get; set; }
        /// <summary>
        /// 成本价
        /// </summary>
        [Column("COSTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? CostPrice { get; set; }
        /// <summary>
        /// 市场价
        /// </summary>
        [Column("BAZAARPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? BazaarPrice { get; set; }
        /// <summary>
        /// 已入库数量
        /// </summary>
        [Column("STORAGEQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? StorageQuantity { get; set; }
        /// <summary>
        /// 已入库单价
        /// </summary>
        [Column("STORAGEPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? StoragePrice { get; set; }
        /// <summary>
        /// 已入库合计金额
        /// </summary>
        [Column("STORAGETOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? StorageTotalPrice { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATA")]
        public DateTime? CreationData { get; set; }
        /// <summary>
        /// 已购买数量
        /// </summary>
        [Column("PURCHASEDQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? PurchasedQuantity { get; set; }
        /// <summary>
        /// 已购买单价
        /// </summary>
        [Column("PURCHASEDPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? PurchasedPrice { get; set; }
        /// <summary>
        /// 已购合计金额
        /// </summary>
        [Column("PURCHASEDTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? PurchasedTotalPrice { get; set; }

        /// <summary>
        /// 材料来源
        /// </summary>
        [Column("MATERIALSSOURCE")]
        public bool? MaterialsSource { get; set; }
        /// <summary>
        /// Project_Subject_Id
        /// </summary>
        [Column("Project_Subject_Id")]
        public string Project_Subject_Id { get; set; }

        /// <summary>
        /// 可以申请数量
        /// </summary>
        [NotMapped]
        public decimal? AllowQuantity => Quantity - (ApplyQuantity??0);


        public int? SortCode { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion




    }


    public class Project_ConstructionBudgetMaterials
    {
        public string ID { get; set; }

        [DecimalPrecision(18, 4)]
        public decimal? StorageQuantity { get; set; }

        [DecimalPrecision(18, 4)]
        public decimal? StorageTotalPrice { get; set; }

        [DecimalPrecision(18, 4)]
        public decimal? StoragePrice { get; set; }


    }

}

