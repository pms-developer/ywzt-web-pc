﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-03 20:43
    /// 描 述：施工预算
    /// </summary>
    public interface ProjectConstructionBudgetIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_ConstructionBudgetEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取根据项目ID材料档案
        /// <summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination, string projectId,string queryJson);

        /// <summary>
        /// 获取Project_ConstructionBudgetQuantities表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_ConstructionBudgetQuantitiesEntity> GetProject_ConstructionBudgetQuantitiesList(string keyValue);

        DataTable GetQuantities(string keyValue);

        /// <summary>
        /// 获取Project_ConstructionBudgetMaterials表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetProject_ConstructionBudgetMaterialsList(string keyValue);
        /// <summary>
        /// 获取Project_ConstructionBudget表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ConstructionBudgetEntity GetProject_ConstructionBudgetEntity(string keyValue);

        IEnumerable<Project_ConstructionBudgetEntity> GetBudgetList(BudgetQueryModel query);

        /// <summary>
        /// 获取Project_ConstructionBudgetMaterials表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_SubjectFeeDetailsEntity> Project_SubjectFeeDetailsEntityList(string budgetId);


        /// <summary>
        /// 获取施工预算材料清单
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetMaterials(string projectId);

        /// <summary>
        /// 获取Project_ConstructionBudgetQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ConstructionBudgetQuantitiesEntity GetProject_ConstructionBudgetQuantitiesEntity(string keyValue);
        /// <summary>
        /// 获取Project_ConstructionBudgetMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ConstructionBudgetMaterialsEntity GetProject_ConstructionBudgetMaterialsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_ConstructionBudgetEntity entity, List<ProjectSubjectEntity> project_ConstructionBudgetDetailsList, List<Project_ConstructionBudgetQuantitiesEntity> project_ConstructionBudgetQuantitiesList, List<Project_ConstructionBudgetMaterialsEntity> project_ConstructionBudgetMaterialsList, string deleteList, string type);

        void SaveBudget(string budgetId, Project_ConstructionBudgetEntity entity, List<ProjectSubjectEntity> projectSubjectList, List<Project_SubjectFeeDetailsEntity> projectSubjectFeeDetailsEntity, List<Project_ConstructionBudgetMaterialsEntity> project_ConstructionBudgetMaterialsList, string deleteList, string type);
      
        Project_ConstructionBudgetEntity GetFormInfo(string projectId);
        Project_ConstructionBudgetEntity GetFormInfoByCompanyId(string companyId);
        Project_ConstructionBudgetEntity GetFormInfoByDepartmentId(string departmentId);
        bool CheckIsExceedBudget(string subjectId, string projectId, decimal amount);

        bool CheckIsExceedBudgetByBaseId(string subjectId, string projectId, decimal amount, string id);

        bool CheckIsExceedBudgetByProjectId(string projectId, decimal amount, string id, string typeid);

        #endregion

    }
}
