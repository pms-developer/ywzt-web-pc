﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangJie.Application.TwoDevelopment.PurchaseModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-03 20:43
    /// 描 述：施工预算
    /// </summary>
    public class ProjectConstructionBudgetService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.ProjectName,
                t.CompanyCode,
                t.TotalPrice,
                t.GrossProfitRate,
                t.Taxes,
                t.BidUserId,
                t.SortCode,
                t.Remark,
                t.AuditStatus,
                t.Workflow_ID,
                t.ProjectId,
                t.CreationDate
                ");
                strSql.Append("  FROM Project_ConstructionBudget t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID=@ProjectID ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }
                var list = this.BaseRepository().FindList<Project_ConstructionBudgetEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取根据项目ID材料档案
        /// <summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        public IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination, string projectId,string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.Model,
                t.BrandId,
                t.Origin,
                t.UnitId,
                t.ProjectId,
                t.Price,
                t.BugetCost,
                t.DirectLabor,
                t.MachineryPrice,
                t.SubjectCost
                ");
                strSql.Append("  FROM Base_Materials t ");
                strSql.Append("  WHERE 1=1 ");
                // 虚拟参数


                var queryParam = queryJson.ToJObject();

                var dp = new DynamicParameters(new { });


                if (!queryParam["word"].IsEmpty())
                {
                    dp.Add("word", "%" + queryParam["word"].ToString() + "%", DbType.String);
                    strSql.Append(" AND (t.Code Like @word or t.Name Like @word ) ");
                }

                if (!projectId.IsEmpty())
                {
                    dp.Add("ProjectId", projectId, DbType.String);
                    strSql.Append(" And (IsProject=0 or projectid=@ProjectId) ");
                }


                if (!queryParam["TypeId"].IsEmpty())
                {
                    dp.Add("TypeId", queryParam["TypeId"].ToString(), DbType.String);
                    strSql.Append(" AND t.TypeId = @TypeId ");
                }

                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" And (IsProject=0 or projectid=@ProjectId )");
                }

                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append("    AND  name like @Name");
                }

                var list = this.BaseRepository().FindList<Base_MaterialsEntity>(strSql.ToString(), dp, pagination);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetProject_ConstructionBudgetMaterialsLists(IEnumerable<string> ids)
        {
            try
            {
                return BaseRepository().FindList<Project_ConstructionBudgetMaterialsEntity>(i => ids.Contains(i.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Project_ConstructionBudgetQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetQuantitiesEntity> GetProject_ConstructionBudgetQuantitiesList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_ConstructionBudgetQuantitiesEntity>(t => t.ProjectConstructionBudgetId == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public DataTable GetQuantities(string keyValue)
        {
            try
            {
                var sql = "select * from Project_ConstructionBudgetQuantities where ProjectConstructionBudgetId=@keyValue";
                return BaseRepository().FindTable(sql, new { keyValue = keyValue });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ConstructionBudgetMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetProject_ConstructionBudgetMaterialsList(string budgetId)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select b.* from Project_Subject a join project_constructionbudgetmaterials b on a.ID=b.Project_Subject_Id
							 where a.ConstructionBudgetId=@budgetId ");
              
                var list = BaseRepository().FindList<Project_ConstructionBudgetMaterialsEntity>(sb.ToString(), new { budgetId });

               
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_SubjectFeeDetailsEntity> Project_SubjectFeeDetailsEntityList(string budgetId)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@" select b.* from Project_Subject  a join project_constructionbudgesubjectfeedetails b on a.ID=b.ProjectSubjectId
							 where a.ConstructionBudgetId=@budgetId");

                var list = BaseRepository().FindList<Project_SubjectFeeDetailsEntity>(sb.ToString(), new { budgetId });
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }

        }

        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetMaterialsListByProjectId(string projectId)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_ConstructionBudgetMaterialsEntity>(t => t.ProjectId == projectId);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void UpdateMaterials(List<Project_ConstructionBudgetMaterialsEntity> materialsList)
        {
            try
            {
                BaseRepository().Update(materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetMaterials(IEnumerable<string> materialIds)
        {
            try
            {
                return BaseRepository().FindList<Project_ConstructionBudgetMaterialsEntity>(i => materialIds.Contains(i.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ConstructionBudget表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ConstructionBudgetEntity GetProject_ConstructionBudgetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ConstructionBudgetEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Project_ConstructionBudgetQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ConstructionBudgetQuantitiesEntity GetProject_ConstructionBudgetQuantitiesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ConstructionBudgetQuantitiesEntity>(t => t.ProjectConstructionBudgetId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ConstructionBudgetMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ConstructionBudgetMaterialsEntity GetProject_ConstructionBudgetMaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ConstructionBudgetMaterialsEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_ConstructionBudgetEntity = GetProject_ConstructionBudgetEntity(keyValue);
                db.Delete<Project_ConstructionBudgetEntity>(t => t.ID == keyValue);
                db.Delete<Project_ConstructionBudgetDetailsEntity>(t => t.ProjectConstructionBudgetId == project_ConstructionBudgetEntity.ID);
                db.Delete<Project_ConstructionBudgetQuantitiesEntity>(t => t.ProjectConstructionBudgetId == project_ConstructionBudgetEntity.ID);
                db.Delete<Project_ConstructionBudgetMaterialsEntity>(t => t.ProjectConstructionBudgetId == project_ConstructionBudgetEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_ConstructionBudgetEntity entity, List<ProjectSubjectEntity> projectSubjectList, List<Project_ConstructionBudgetQuantitiesEntity> project_ConstructionBudgetQuantitiesList, List<Project_ConstructionBudgetMaterialsEntity> project_ConstructionBudgetMaterialsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var project_ConstructionBudgetEntityTmp = GetProject_ConstructionBudgetEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    var costAttributeList = new List<string>();
                    var Project_ConstructionBudgetQuantitiesUpdateList = project_ConstructionBudgetQuantitiesList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_ConstructionBudgetQuantitiesUpdateList)
                    {
                        db.Update(item);
                        if (item.CostInfos != null)
                        {
                            var sql = "update Project_ConstructionBudgetQuantities set ";
                            foreach (var info in item.CostInfos)
                            {
                                if (info.Value == null) continue;
                                if (string.IsNullOrWhiteSpace(info.ColumnName)) continue;
                                sql += $"{info.ColumnName}={info.Value},";
                            }
                            sql = sql.Substring(0, sql.Length - 1);
                            sql += $" where ID='{item.ID}'";
                            db.ExecuteBySql(sql);
                        }
                    }
                    var Project_ConstructionBudgetQuantitiesInserList = project_ConstructionBudgetQuantitiesList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_ConstructionBudgetQuantitiesInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectId = project_ConstructionBudgetEntityTmp.ProjectId;
                        item.ProjectConstructionBudgetId = project_ConstructionBudgetEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Project_ConstructionBudgetMaterialsUpdateList = project_ConstructionBudgetMaterialsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_ConstructionBudgetMaterialsUpdateList)
                    {
                        item.ProjectId = project_ConstructionBudgetEntityTmp.ProjectId;
                        db.Update(item);
                    }
                    var Project_ConstructionBudgetMaterialsInserList = project_ConstructionBudgetMaterialsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_ConstructionBudgetMaterialsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectId = project_ConstructionBudgetEntityTmp.ProjectId;
                        item.ProjectConstructionBudgetId = project_ConstructionBudgetEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_ConstructionBudgetQuantitiesEntity item in project_ConstructionBudgetQuantitiesList)
                    {
                        item.ProjectId = entity.ProjectId;
                        item.ProjectConstructionBudgetId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Project_ConstructionBudgetMaterialsEntity item in project_ConstructionBudgetMaterialsList)
                    {
                        item.ProjectId = entity.ProjectId;
                        item.ProjectConstructionBudgetId = entity.ID;
                        db.Insert(item);
                    }
                }

                var projectSubjectUpdateList = projectSubjectList.FindAll(i => i.EditType == "2");
                db.Update(projectSubjectUpdateList);

                db.Commit();

                UpdateCostAttribute(project_ConstructionBudgetQuantitiesList, type);
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveBudget(string budgetId, Project_ConstructionBudgetEntity entity, List<ProjectSubjectEntity> projectSubjectList, List<Project_SubjectFeeDetailsEntity> projectSubjectFeeList, List<Project_ConstructionBudgetMaterialsEntity> project_ConstructionBudgetMaterialsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    entity.Modify(budgetId);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                   
                   db.Update(projectSubjectFeeList.FindAll(x => x.EditType == EditType.Update));

                   db.Insert(projectSubjectFeeList.FindAll(x => x.EditType == EditType.Add));

                    db.Update(project_ConstructionBudgetMaterialsList.FindAll(x => x.EditType == EditType.Update));

                    db.Insert(project_ConstructionBudgetMaterialsList.FindAll(x => x.EditType == EditType.Add));
                     
                }
                else
                {
                    entity.Create(budgetId);
                    db.Insert(entity);
                    db.Insert(projectSubjectFeeList);

                    db.Insert(project_ConstructionBudgetMaterialsList);
                }

                var projectSubjectUpdateList = projectSubjectList.FindAll(i => i.EditType == "2");
                db.Update(projectSubjectUpdateList);

                db.Commit(); 
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        /// <summary>
        /// 更新成本属性信息
        /// </summary>
        /// <param name="project_ConstructionBudgetQuantitiesList"></param>
        /// <param name="type"></param>
        private void UpdateCostAttribute(List<Project_ConstructionBudgetQuantitiesEntity> project_ConstructionBudgetQuantitiesList, string type)
        {
            var connection = BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var Project_ConstructionBudgetQuantitiesInserList = project_ConstructionBudgetQuantitiesList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_ConstructionBudgetQuantitiesInserList)
                    {
                        if (item.CostInfos != null)
                        {
                            var sql = "update Project_ConstructionBudgetQuantities set ";
                            foreach (var info in item.CostInfos)
                            {
                                if (info.Value == null) continue;
                                if (string.IsNullOrWhiteSpace(info.ColumnName)) continue;
                                sql += $"{info.ColumnName}={info.Value},";
                            }
                            sql = sql.Substring(0, sql.Length - 1);
                            sql += $" where ID='{item.ID}'";
                            connection.ExecuteBySql(sql);
                        }
                    }
                }
                else
                {
                    foreach (Project_ConstructionBudgetQuantitiesEntity item in project_ConstructionBudgetQuantitiesList)
                    {
                        if (item.CostInfos != null)
                        {
                            var sql = "update Project_ConstructionBudgetQuantities set ";
                            foreach (var info in item.CostInfos)
                            {
                                if (info.Value == null) continue;
                                if (string.IsNullOrWhiteSpace(info.ColumnName)) continue;
                                sql += $"{info.ColumnName}={info.Value},";
                            }
                            sql = sql.Substring(0, sql.Length - 1);
                            sql += $" where ID='{item.ID}'";
                            connection.ExecuteBySql(sql);
                        }
                    }
                }
                connection.Commit();
            }
            catch (Exception ex)
            {
                connection.Rollback();
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        public Project_ConstructionBudgetEntity GetFormInfo(string projectId)
        {
            try
            {
                return BaseRepository().FindEntity<Project_ConstructionBudgetEntity>(i => i.ProjectId == projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_ConstructionBudgetEntity GetFormInfoByCompanyId(string companyId)
        {
            try
            {
                return BaseRepository().FindEntity<Project_ConstructionBudgetEntity>(i => i.Company_ID == companyId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_ConstructionBudgetEntity GetFormInfoByDepartmentId(string departmentId)
        {
            try
            {
                return BaseRepository().FindEntity<Project_ConstructionBudgetEntity>(i => i.Department_Id == departmentId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetBudgetInfo(string projectId)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select 
                            a.*,
                            b.Name as CostAttributeName ");
                sb.Append(" from Project_Subject as a ");
                sb.Append(" left join Base_CostAttribute as b on a.CostAttributeId=b.ID ");
                sb.Append(" where a.ProjectId=@projectId ");
                sb.Append(" order by a.SortCode asc ");
                var list = BaseRepository().FindList<ProjectSubjectEntity>(sb.ToString(), new { projectId });
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public IEnumerable<Project_ConstructionBudgetEntity> GetBudgetLis(BudgetQueryModel query)
        {
            try
            {                

                var sb = new StringBuilder();
                sb.Append(@"select * from project_constructionbudget 
                            where Enabled=1 and (ProjectId=@KeyId or Company_ID=@KeyId or Department_Id=@KeyId) 
                            and `Year`=@Year");
                if(query.MinMonth.HasValue)
                {
                    sb.Append(" and month>=@MinMonth");
                }

                if (query.MaxMonth.HasValue)
                {
                    sb.Append(" and month<=@MaxMonth");
                }

                var list = BaseRepository().FindList<Project_ConstructionBudgetEntity>(sb.ToString(), new { KeyId=query.KeyId,Year=query.Year, MinMonth=query.MinMonth??1, MaxMonth=query.MaxMonth??12 });
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public bool CheckIsExceedBudget(string subjectId, string projectId, decimal amount)
        {
            try
            {
                var info = BaseRepository().FindEntity<ProjectSubjectEntity>(i => i.ID == subjectId && i.ProjectId == projectId);
                if (info == null) return true;
                info.ActuallyOccurred = (info.ActuallyOccurred ?? 0) + amount;

                if (info.ActuallyOccurred <= info.BudgetAmount.ToDecimal())
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public bool CheckIsExceedBudgetByBaseId(string subjectId, string projectId, decimal amount, string id)
        {
            try
            {
                var info = BaseRepository().FindEntity<ProjectSubjectEntity>(i => i.BaseSubjectId == subjectId && i.ProjectId == projectId);
                if (info == null) return true;
                var sb = new StringBuilder();
                sb.Append(@"select 
                            DISTINCT a.id,
                            a.* ");
                sb.Append(" from Project_Subcontract a ");
                sb.Append(" left join Base_ContractType b on a.subcontractType=b.id ");
                sb.Append(" left join Project_Subject c on b.BaseSubjectId=c.BaseSubjectId");
                sb.Append(" where c.BaseSubjectId=@subjectId and a.ProjectId =@projectId and a.id!=@id ");
                List<Project_SubcontractEntity> list = BaseRepository().FindList<Project_SubcontractEntity>(sb.ToString(), new { subjectId, projectId, id }).ToList<Project_SubcontractEntity>();
                decimal sumVaule = 0;
                sumVaule = Convert.ToDecimal(list.Sum(t => t.TotalAmount));
                info.ActuallyOccurred = amount + sumVaule;
                if (info.ActuallyOccurred <= info.BudgetAmount.ToDecimal())
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public bool CheckIsExceedBudgetByProjectId(string projectId, decimal amount, string id, string typeid)
        {
            try
            {
                var str = new StringBuilder();
                str.Append(@"select a.* from Project_Subject a 
	                            left join Base_ContractType b on a.BaseSubjectId=b.BaseSubjectId");
                str.Append(" where a.ProjectId =@projectId and b.ID=@typeid ");
                var info = BaseRepository().FindEntity<ProjectSubjectEntity>(str.ToString(), new { typeid, projectId });
                if (info == null) return true;

                var sb = new StringBuilder();
                sb.Append(@"select 
                            DISTINCT a.id,
                            a.* ");
                sb.Append(" from Purchase_Contract a ");
                sb.Append(" left join Base_ContractType b on a.ContractType=b.id ");
                sb.Append(" left join Project_Subject c on b.BaseSubjectId=c.BaseSubjectId");
                sb.Append(" where b.ID=@typeid and a.ProjectId =@projectId and a.id!=@id ");
                List<Purchase_ContractEntity> list = BaseRepository().FindList<Purchase_ContractEntity>(sb.ToString(), new { typeid, projectId, id }).ToList();
                decimal sumVaule = 0;
                sumVaule = Convert.ToDecimal(list.Sum(t => t.SignAmount));
                info.ActuallyOccurred = amount + sumVaule;
                if (info.ActuallyOccurred <= info.BudgetAmount.ToDecimal())
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取施工预算材料清单
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<Project_ConstructionBudgetMaterialsEntity> GetMaterials(string projectId)
        {
            try
            {
                var list = BaseRepository().FindList<Project_ConstructionBudgetMaterialsEntity>(i => i.ProjectId == projectId);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx) throw;
                throw ExceptionEx.ThrowServiceException(ex);
            }
        }

        #endregion

    }
}
