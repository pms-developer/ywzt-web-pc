﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 10:17
    /// 描 述：成本分割设置
    /// </summary>
    public class Project_FundPartitionDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 资金分割设置ID
        /// </summary>
        [Column("PROJECTFUNDPARTITIONSETTINGSID")]
        public string ProjectFundPartitionSettingsId { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 项目成本科目ID
        /// </summary>
        [Column("PROJECTSUBJECTID")]
        public string ProjectSubjectId { get; set; }
        /// <summary>
        /// 是否自动计算
        /// </summary>
        [Column("ISAUTO")]
        public bool? IsAuto { get; set; }
        /// <summary>
        /// 控件点
        /// </summary>
        [Column("CONTROLPOINT")]
        public string ControlPoint { get; set; }
        /// <summary>
        /// 公式
        /// </summary>
        [Column("FORMULA")]
        public string Formula { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        /// <summary>
        /// 公式设置信息
        /// </summary>
        public string FormulaSettings { get; set; }

    
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region 扩展属性
        /// <summary>
        /// 科目名称
        /// </summary>
        [NotMapped]
        public string SubjectName { get; set; }
        #endregion
    }
}

