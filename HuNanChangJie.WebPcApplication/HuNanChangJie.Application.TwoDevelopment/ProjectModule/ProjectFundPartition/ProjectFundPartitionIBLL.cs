﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 10:17
    /// 描 述：成本分割设置
    /// </summary>
    public interface ProjectFundPartitionIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_FundPartitionEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_FundPartitionDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_FundPartitionDetailsEntity> GetProject_FundPartitionDetailsList(string keyValue);
        /// <summary>
        /// 获取Project_FundPartition表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_FundPartitionEntity GetProject_FundPartitionEntity(string keyValue);
        /// <summary>
        /// 获取Project_FundPartitionDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_FundPartitionDetailsEntity GetProject_FundPartitionDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_FundPartitionEntity entity,List<Project_FundPartitionDetailsEntity> project_FundPartitionDetailsList,string deleteList,string type);
        Project_FundPartitionEntity GetFormInfo(string projectId);
        #endregion

    }
}
