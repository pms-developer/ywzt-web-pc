﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-31 10:17
    /// 描 述：成本分割设置
    /// </summary>
    public class ProjectFundPartitionService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();

        public bool CheckMainInfoIsAudit(string projectid)
        {
            try
            {
                var info = BaseRepository().FindEntity<Project_FundPartitionEntity>(i => i.ProjectID == projectid);
                if (info == null) return false;
                return info.AuditStatus == "2" ? true : false;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_FundPartitionEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.IsNative,
                t.TaxType,
                t.Rate,
                t.Workflow_ID,t.AuditStatus
                ");
                strSql.Append("  FROM Project_FundPartition t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                var list=this.BaseRepository().FindList<Project_FundPartitionEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_FundPartitionDetailsEntity> GetDetailsList(string projectid)
        {
            try
            {
                return BaseRepository().FindList<Project_FundPartitionDetailsEntity>(i=>i.ProjectId== projectid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_FundPartitionDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_FundPartitionDetailsEntity> GetProject_FundPartitionDetailsList(string keyValue)
        {
            try
            {
                var sql = @"select a.*,b.Name as SubjectName from Project_FundPartitionDetails as a left join
                            Project_Subject as b on a.ProjectSubjectId=b.Id where a.ProjectFundPartitionSettingsId=@keyValue";
                var list= this.BaseRepository().FindList<Project_FundPartitionDetailsEntity>(sql,new { keyValue });
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_FundPartition表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_FundPartitionEntity GetProject_FundPartitionEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_FundPartitionEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_FundPartitionDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_FundPartitionDetailsEntity GetProject_FundPartitionDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_FundPartitionDetailsEntity>(t=>t.ProjectFundPartitionSettingsId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_FundPartitionEntity = GetProject_FundPartitionEntity(keyValue); 
                db.Delete<Project_FundPartitionEntity>(t=>t.ID == keyValue);
                db.Delete<Project_FundPartitionDetailsEntity>(t=>t.ProjectFundPartitionSettingsId == project_FundPartitionEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_FundPartitionEntity GetFormInfo(string projectId)
        {
            try
            {
                return BaseRepository().FindEntity<Project_FundPartitionEntity>(i => i.ProjectID == projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_FundPartitionEntity entity,List<Project_FundPartitionDetailsEntity> project_FundPartitionDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_FundPartitionEntityTmp = GetProject_FundPartitionEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_FundPartitionDetailsUpdateList= project_FundPartitionDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_FundPartitionDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_FundPartitionDetailsInserList= project_FundPartitionDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_FundPartitionDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectFundPartitionSettingsId = project_FundPartitionEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_FundPartitionDetailsEntity item in project_FundPartitionDetailsList)
                    {
                        item.ProjectFundPartitionSettingsId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
