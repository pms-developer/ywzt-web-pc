﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.Model;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 19:09
    /// 描 述：外派人员工资
    /// </summary>
    public class ProjectDispatchedWageBLL : ProjectDispatchedWageIBLL
    {
        private ProjectDispatchedWageService projectDispatchedWageService = new ProjectDispatchedWageService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_DispatchedWageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectDispatchedWageService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_DispatchedDetailsEntity> GetProject_DispatchedDetailsList(string keyValue)
        {
            try
            {
                return projectDispatchedWageService.GetProject_DispatchedDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedWage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_DispatchedWageEntity GetProject_DispatchedWageEntity(string keyValue)
        {
            try
            {
                return projectDispatchedWageService.GetProject_DispatchedWageEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_DispatchedDetailsEntity GetProject_DispatchedDetailsEntity(string keyValue)
        {
            try
            {
                return projectDispatchedWageService.GetProject_DispatchedDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<ProjectEntity> GetAllProjectList()
        {
            try
            {
                return projectDispatchedWageService.GetAllProjectList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        IEnumerable<Project_DispatchedWageEntity> ProjectDispatchedWageIBLL.GetDispatchedWageList(IEnumerable<string> projectIds)
        {
            try
            {
                return projectDispatchedWageService.GetDispatchedWageList(projectIds);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectDispatchedWageService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_DispatchedWageEntity entity,List<Project_DispatchedDetailsEntity> project_DispatchedDetailsList,string deleteList,string type)
        {
            try
            {
                projectDispatchedWageService.SaveEntity(keyValue, entity,project_DispatchedDetailsList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Project_DispatchedWageEntity GetFormInfo(string projectId)
        {
            try
            {
                return projectDispatchedWageService.GetFormInfo(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_DispatchedDetailsEntity> GetDetails(XqPagination pagination,string queryJson)
        {
            try
            {
                return projectDispatchedWageService.GetDetails(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
