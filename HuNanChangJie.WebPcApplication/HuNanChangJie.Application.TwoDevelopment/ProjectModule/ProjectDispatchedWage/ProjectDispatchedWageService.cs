﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangeJie.Application.Project.Model;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 19:09
    /// 描 述：外派人员工资
    /// </summary>
    public class ProjectDispatchedWageService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_DispatchedWageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.TotalFinish,
                t.TotalAmount,
                t.TotalCount,
                t.FinishCount,
                t.LastTotalAmount,
                t.MonthTotalAmount,
                t.ProjectSubjectId,
                t.AccrueDate,
                t.Workflow_ID,
                t.AuditStatus,
                t.ProjectID,
                t.CreationDate
                ");
                strSql.Append("  FROM Project_DispatchedWage t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" and t.ProjectId=@ProjectId");
                }
                var list=this.BaseRepository().FindList<Project_DispatchedWageEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_DispatchedDetailsEntity> GetProject_DispatchedDetailsList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.F_RealName as UserName 
                            from Project_DispatchedDetails as a left join Base_user as b
                            on a.userId=b.F_UserId
                            where 1=1 ");
                sb.Append(" and a.ProjectDispatchedStaffWageId=@keyValue");
                var list = BaseRepository().FindList<Project_DispatchedDetailsEntity>(sb.ToString(), new { keyValue });
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedWage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_DispatchedWageEntity GetProject_DispatchedWageEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_DispatchedWageEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_DispatchedDetailsEntity GetProject_DispatchedDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_DispatchedDetailsEntity>(t=>t.ProjectDispatchedStaffWageId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        internal IEnumerable<ProjectEntity> GetAllProjectList()
        {
            try
            {
                return this.BaseRepository().FindList<ProjectEntity>(i => i.AuditStatus == "2");
            }
            catch (Exception ex)
            {

                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        internal IEnumerable<Project_DispatchedWageEntity> GetDispatchedWageList(IEnumerable<string> projectIds)
        {
            try
            {
                return this.BaseRepository().FindList<Project_DispatchedWageEntity>(i => projectIds.Contains(i.ProjectID) && i.AuditStatus == "2" && i.AccrueState != true);
            }
            catch (Exception ex)
            {

                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_DispatchedWageEntity = GetProject_DispatchedWageEntity(keyValue); 
                db.Delete<Project_DispatchedWageEntity>(t=>t.ID == keyValue);
                db.Delete<Project_DispatchedDetailsEntity>(t=>t.ProjectDispatchedStaffWageId == project_DispatchedWageEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_DispatchedWageEntity GetFormInfo(string projectId)
        {
            try
            {
                return BaseRepository().FindEntity<Project_DispatchedWageEntity>(i => i.ProjectID == projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_DispatchedDetailsEntity> GetDetails(XqPagination pagination,string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select a.*,
                            b.F_RealName as UserName 
                            from Project_DispatchedDetails as a left join Base_user as b
                            on a.userId=b.F_UserId
                            left join Project_DispatchedWage as c on a.ProjectDispatchedStaffWageId=c.id
                            where c.AuditStatus='2' ");
                //strSql.Append(" and a.projectId=@projectId");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND b.RealName like @Name");
                }
                if (!queryParam["projectId"].IsEmpty())
                {
                    dp.Add("projectId", queryParam["projectId"].ToString(), DbType.String);
                    strSql.Append(" AND a.projectId = @projectId");
                }
                var list=this.BaseRepository().FindList<Project_DispatchedDetailsEntity>(strSql.ToString(),dp, pagination);//new { projectId }
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_DispatchedWageEntity entity,List<Project_DispatchedDetailsEntity> project_DispatchedDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_DispatchedWageEntityTmp = GetProject_DispatchedWageEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_DispatchedDetailsUpdateList= project_DispatchedDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_DispatchedDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_DispatchedDetailsInserList= project_DispatchedDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_DispatchedDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectDispatchedStaffWageId = project_DispatchedWageEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_DispatchedDetailsEntity item in project_DispatchedDetailsList)
                    {
                        item.ProjectDispatchedStaffWageId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
