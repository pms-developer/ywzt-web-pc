﻿using HuNanChangJie.Util;
using HuNanChangJie.Util.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-21 15:29
    /// 描 述：仓库材料明细
    /// </summary>
    public class Base_WarehouseDetailsEntity  
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// Fk_WarehouseId
        /// </summary>
        /// <returns></returns>
        [Column("FK_WAREHOUSEID")]
        public string Fk_WarehouseId { get; set; }
        /// <summary>
        /// 材料ID
        /// </summary>
        /// <returns></returns>
        [Column("FK_MATERIALID")]
        public string Fk_MaterialId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        /// <returns></returns>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 价格1
        /// </summary>
        /// <returns></returns>
        [Column("PRICE1")]
        [DecimalPrecision(18, 4)]
        public decimal? Price1 { get; set; }
        /// <summary>
        /// 总价
        /// </summary>
        [Column("TOTALMONEY")]
        [DecimalPrecision(18, 4)]

        public decimal? TotalMoney { get; set; }
        /// <summary>
        /// Price2
        /// </summary>
        /// <returns></returns>
        [Column("PRICE2")]
        [DecimalPrecision(18, 4)]
        public decimal? Price2 { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

