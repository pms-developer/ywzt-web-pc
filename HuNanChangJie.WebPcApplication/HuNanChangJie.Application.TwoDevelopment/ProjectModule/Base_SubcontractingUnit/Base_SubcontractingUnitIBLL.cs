﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-20 11:46
    /// 描 述：分包单位
    /// </summary>
    public interface Base_SubcontractingUnitIBLL
    {
        #region  获取数据
        IEnumerable<Base_SubcontractingUnitEntity> GetPageListByProject(string projectId);
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_SubcontractingUnitEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_SubcontractingUnit表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_SubcontractingUnitEntity GetBase_SubcontractingUnitEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_SubcontractingUnitEntity entity,string deleteList,string type);
        #endregion

    }
}
