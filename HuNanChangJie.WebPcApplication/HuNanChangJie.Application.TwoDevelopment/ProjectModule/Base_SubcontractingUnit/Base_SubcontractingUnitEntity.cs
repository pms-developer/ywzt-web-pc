﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-20 11:46
    /// 描 述：分包单位
    /// </summary>
    public class Base_SubcontractingUnitEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 注册日期
        /// </summary>
        [Column("REGISTERDATE")]
        public DateTime? RegisterDate { get; set; }
        /// <summary>
        /// 单位来源
        /// </summary>
        [Column("SOURCE")]
        public string Source { get; set; }
        /// <summary>
        /// 单位类型
        /// </summary>
        [Column("UNITTYPE")]
        public string UnitType { get; set; }
        /// <summary>
        /// 单位性质
        /// </summary>
        [Column("NATURE")]
        public string Nature { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        [Column("GRADE")]
        public string Grade { get; set; }
        /// <summary>
        /// 合作状态
        /// </summary>
        [Column("COOPERATIONSTATUS")]
        public string CooperationStatus { get; set; }
        /// <summary>
        /// 账期
        /// </summary>
        [Column("PAYMENTDATES")]
        public int? PaymentDates { get; set; }
        /// <summary>
        /// 单位简介
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 注册资金
        /// </summary>
        [Column("REGISTERFUND")]
        public int? RegisterFund { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        [Column("FRACTION")]
        public int? Fraction { get; set; }
        /// <summary>
        /// 省
        /// </summary>
        [Column("PROVINCEID")]
        public string ProvinceId { get; set; }
        /// <summary>
        /// 市
        /// </summary>
        [Column("CITYID")]
        public string CityId { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        [Column("FAX")]
        public string Fax { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        [Column("ZIPCODE")]
        public string ZipCode { get; set; }

        [Column("IDNUMBER")]
        public string IdNumber { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        [Column("TEL")]
        public string Tel { get; set; }
        /// <summary>
        /// 网址
        /// </summary>
        [Column("URL")]
        public string Url { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        /// <summary>
        /// 法人
        /// </summary>
        [Column("LEGALPERSON")]
        public string LegalPerson { get; set; }

        /// <summary>
        /// 账户名称
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// 银行账号
        /// </summary>
        public string BankAccount { get; set; }

        /// <summary>
        /// 服务项目
        /// </summary>
        public string ServiceProject { get; set; }

        

        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

