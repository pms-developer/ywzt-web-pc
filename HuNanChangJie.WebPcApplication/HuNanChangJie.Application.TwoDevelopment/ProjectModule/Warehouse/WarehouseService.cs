﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 11:32
    /// 描 述：仓库管理
    /// </summary>
    public class WarehouseService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据
        public IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination, string queryJson, string warehouseId)
        {
            try
            {
                string sql = $"select b.ID,b.Code,b.Name,b.Model,b.Origin,b.BrandId,b.UnitId, c.Price1 Price,c.Quantity Inventory,c.totalmoney SalePrice from  Base_Materials b left join  Base_WarehouseDetails c on b.id = c.fk_materialid where c.fk_warehouseid='{warehouseId}' ";// where c.Quantity>0
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Quantity"].IsEmpty())
                {
                    sql += " and c.Quantity>0 ";
                }

                if (!queryParam["MaterialsName"].IsEmpty())
                {
                    string materialsName = queryParam["MaterialsName"].ToString();

                    sql += " AND ( b.Name like '%" + materialsName + "%' or b.Code='" + materialsName + "')";
                }

                var list = this.BaseRepository().FindList<Base_MaterialsEntity>(sql, dp, pagination);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_WarehouseEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.Name,
                t.ProjectId,
                t.ManagerId,
                t.OccupyMoney,
                t.Telphone,
                t.Address,
                t.Remark
                ");
                strSql.Append("  FROM Base_Warehouse t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectId = @ProjectId ");
                }
                if (!queryParam["ManagerId"].IsEmpty())
                {
                    dp.Add("ManagerId", queryParam["ManagerId"].ToString(), DbType.String);
                    strSql.Append(" AND t.ManagerId = @ManagerId ");
                }
                var list = this.BaseRepository().FindList<Base_WarehouseEntity>(strSql.ToString(), dp, pagination);
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取项目仓库
        /// </summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        public IEnumerable<Base_WarehouseEntity> GetProjectWarehouses(string projectId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(projectId))
                {
                    return this.BaseRepository().FindList<Base_WarehouseEntity>();
                }
                else
                {
                    return this.BaseRepository().FindList<Base_WarehouseEntity>(i => i.ProjectId == projectId);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_Warehouse表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_WarehouseEntity GetBase_WarehouseEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_WarehouseEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Base_WarehouseEntity>(t => t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_WarehouseEntity entity, string deleteList, string type)
        {
            try
            {
                if (type == "edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
