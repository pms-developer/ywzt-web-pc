﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 11:32
    /// 描 述：仓库管理
    /// </summary>
    public interface WarehouseIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取材料档案
        /// <summary>
        /// <param name="subcontractId">分包合同</param>
        /// <returns></returns>
        IEnumerable<Base_MaterialsEntity> GetMaterialsInfoList(XqPagination pagination, string queryJson, string warehouseId);
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_WarehouseEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取项目仓库
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        IEnumerable<Base_WarehouseEntity> GetProjectWarehouses(string projectId);
        /// <summary>
        /// 获取Base_Warehouse表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_WarehouseEntity GetBase_WarehouseEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_WarehouseEntity entity,string deleteList,string type);
        #endregion

    }
}
