﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-10-19 11:32
    /// 描 述：仓库管理
    /// </summary>
    public class Base_WarehouseEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 管理员id
        /// </summary>
        [Column("MANAGERID")]
        public string ManagerId { get; set; }
        /// <summary>
        /// 管理员名称
        /// </summary>
        [Column("MANAGERNAME")]
        public string ManagerName { get; set; }
        /// <summary>
        /// 管理员电话
        /// </summary>
        [Column("TELPHONE")]
        public string Telphone { get; set; }
        /// <summary>
        /// 占用资金
        /// </summary>
        [Column("OCCUPYMONEY")]
        [DecimalPrecision(18, 4)]
        public decimal? OccupyMoney { get; set; }
        /// <summary>
        /// 仓库地址
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 公司Id
        /// </summary>
        [Column("F_CompanyId")]
        public string F_CompanyId { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
        #region  扩展字段

        /// <summary>
        /// 是否为关注
        /// </summary>
        [NotMapped]
        public string CompanyName { get; set; }
        #endregion
    }
}

