﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-20 11:49
    /// 描 述：工程合同变更
    /// </summary>
    public class ProjectContractChangeService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ContractChangeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT  * from (");
                strSql.Append("SELECT ");
                strSql.Append(@"
                a.ID,
                a.Code,
                a.ChangeDate,
                a.ProjectContractId,
                a.ProjectID,
                a.SortCode,
                a.CurrentAmount,
                a.ChangeType,
                a.ProjectName,
                a.CurrentChangeAmount,
                a.AfterAmount,
                a.AuditStatus,
                a.Workflow_ID,
                a.CreationDate,
                a.Abstract,
                b.Name as ContractName,
                b.Code as ContractCode,
                b.InitAmount
                from Project_ContractChange as a left join  Project_Contract as b on a.ProjectContractId=b.Id
                ");
                strSql.Append("  ) t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" and t.ProjectID=@ProjectID");
                }
                var list=this.BaseRepository().FindList<Project_ContractChangeEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChangeQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractChangeQuantitiesEntity> GetProject_ContractChangeQuantitiesList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Project_ContractChangeQuantitiesEntity>(t=>t.ProjectContractChangeId == keyValue );
                var query = from item in list orderby item.SortCode ascending select item;
               
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChangeMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractChangeMaterialsEntity> GetProject_ContractChangeMaterialsList(string keyValue)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                a.*,
                b.Price InitPrice,
                b.Quantity InitQuantity,
	            b.TotalPrice InitTotal,
	            b.ChangePrice ChangePrice,
	            b.ChangeQuantity ChangeQuantity,
	            b.ChangeTotalPrice ChangeTotalPrice 
                ");
                strSql.Append(" from Project_ContractChangeMaterials a ");
                strSql.Append(" left join Project_ContractMaterials b on a.ProjectContractMaterialsId=b.ID");
                strSql.Append(" WHERE 1=1 ");
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!keyValue.IsEmpty())
                {
                    dp.Add("ProjectContractChangeId", keyValue, DbType.String);
                    strSql.Append(" AND ProjectContractChangeId = @ProjectContractChangeId  ");
                }
                var list= this.BaseRepository().FindList<Project_ContractChangeMaterialsEntity>(strSql.ToString(), dp);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractChangeEntity GetProject_ContractChangeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ContractChangeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChangeMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractChangeMaterialsEntity GetProject_ContractChangeMaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ContractChangeMaterialsEntity>(t=>t.ProjectContractChangeId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChangeQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractChangeQuantitiesEntity GetProject_ContractChangeQuantitiesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ContractChangeQuantitiesEntity>(t=>t.ProjectContractChangeId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_ContractChangeEntity = GetProject_ContractChangeEntity(keyValue); 
                db.Delete<Project_ContractChangeEntity>(t=>t.ID == keyValue);
                db.Delete<Project_ContractChangeMaterialsEntity>(t=>t.ProjectContractChangeId == project_ContractChangeEntity.ID);
                db.Delete<Project_ContractChangeQuantitiesEntity>(t=>t.ProjectContractChangeId == project_ContractChangeEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_ContractChangeEntity entity,List<Project_ContractChangeMaterialsEntity> project_ContractChangeMaterialsList,List<Project_ContractChangeQuantitiesEntity> project_ContractChangeQuantitiesList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_ContractChangeEntityTmp = GetProject_ContractChangeEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_ContractChangeMaterialsUpdateList= project_ContractChangeMaterialsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_ContractChangeMaterialsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_ContractChangeMaterialsInserList= project_ContractChangeMaterialsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_ContractChangeMaterialsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectContractChangeId = project_ContractChangeEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                    
                    //没有生成代码 
                    var Project_ContractChangeQuantitiesUpdateList= project_ContractChangeQuantitiesList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_ContractChangeQuantitiesUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_ContractChangeQuantitiesInserList= project_ContractChangeQuantitiesList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_ContractChangeQuantitiesInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectContractChangeId = project_ContractChangeEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_ContractChangeMaterialsEntity item in project_ContractChangeMaterialsList)
                    {
                        item.ProjectContractChangeId = entity.ID;
                        item.Create(item.ID);
                        db.Insert(item);
                    }
                    foreach (Project_ContractChangeQuantitiesEntity item in project_ContractChangeQuantitiesList)
                    {
                        item.ProjectContractChangeId = entity.ID;
                        item.Create(item.ID);
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
