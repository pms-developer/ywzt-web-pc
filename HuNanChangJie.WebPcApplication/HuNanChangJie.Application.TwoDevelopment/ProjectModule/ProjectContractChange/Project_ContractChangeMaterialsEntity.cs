﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-20 11:49
    /// 描 述：工程合同变更
    /// </summary>
    public class Project_ContractChangeMaterialsEntity:BaseEntity 
    {

        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目编码
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 工程合同材料清单合同
        /// </summary>
        [Column("ProjectContractMaterialsId")]
        public string ProjectContractMaterialsId { get; set; }
        /// <summary>
        /// 合同变更ID
        /// </summary>
        [Column("PROJECTCONTRACTCHANGEID")]
        public string ProjectContractChangeId { get; set; }
        /// <summary>
        /// 清单编码
        /// </summary>
        [Column("LISTCODE")]
        public string ListCode { get; set; }
        /// <summary>
        /// 材料编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODELNUMBER")]
        public string ModelNumber { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 本次变更数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; } = 0;
        /// <summary>
        /// 本次变更单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; } = 0;
        /// <summary>
        /// 本次变更合计金额
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; } = 0;
        /// <summary>
        /// 本次变更后数量
        /// </summary>
        [Column("AFTERQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterQuantity { get; set; } = 0;
        /// <summary>
        /// 本次变更后单价
        /// </summary>
        [Column("AFTERPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterPrice { get; set; } = 0;
        /// <summary>
        /// 本次变更后合计金额
        /// </summary>
        [Column("AFTERTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? AfterTotalPrice { get; set; } = 0;
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATA")]
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// 材料来源
        /// </summary>
        [Column("MaterialsSource")]
        public bool? MaterialsSource { get; set; }

        public int? SortCode { get; set; }

        /// <summary>
        /// 合同初始单价
        /// </summary>
        [NotMapped]
        public decimal? InitPrice { get; set; } = 0;

        /// <summary>
        /// 合同初始数量 
        /// </summary>
        [NotMapped]
        public decimal? InitQuantity { get; set; } = 0;

        /// <summary>
        /// 合同初始合计金额
        /// </summary>
        [NotMapped]
        public decimal? InitTotal { get; set; } = 0;

        /// <summary>
        ///  累计变更综合单价
        /// </summary>
        [NotMapped]
        public decimal? ChangePrice { get; set; } = 0;

        /// <summary>
        /// 累计变更数量
        /// </summary>
        [NotMapped]
        public decimal? ChangeQuantity { get; set; } = 0;

        /// <summary>
        /// 累计变更金额
        /// </summary>
        [NotMapped]
        public decimal? ChangeTotalPrice { get; set; } = 0;
        #endregion




        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }

        #endregion
    }
}

