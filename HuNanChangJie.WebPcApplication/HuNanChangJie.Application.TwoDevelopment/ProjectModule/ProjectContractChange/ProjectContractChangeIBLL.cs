﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-20 11:49
    /// 描 述：工程合同变更
    /// </summary>
    public interface ProjectContractChangeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_ContractChangeEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_ContractChangeQuantities表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_ContractChangeQuantitiesEntity> GetProject_ContractChangeQuantitiesList(string keyValue);
        /// <summary>
        /// 获取Project_ContractChangeMaterials表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_ContractChangeMaterialsEntity> GetProject_ContractChangeMaterialsList(string keyValue);
        /// <summary>
        /// 获取Project_ContractChange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ContractChangeEntity GetProject_ContractChangeEntity(string keyValue);
        /// <summary>
        /// 获取Project_ContractChangeMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ContractChangeMaterialsEntity GetProject_ContractChangeMaterialsEntity(string keyValue);
        /// <summary>
        /// 获取Project_ContractChangeQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ContractChangeQuantitiesEntity GetProject_ContractChangeQuantitiesEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_ContractChangeEntity entity,List<Project_ContractChangeMaterialsEntity> project_ContractChangeMaterialsList,List<Project_ContractChangeQuantitiesEntity> project_ContractChangeQuantitiesList,string deleteList,string type);

        /// <summary>
        /// 单据审核通过后调用的事件
        /// </summary>
        /// <param name="keyValue"></param>
        void Audit(string keyValue);

        /// <summary>
        /// 单据反审核通过后调用的事件
        /// </summary>
        /// <param name="keyValue"></param>
        void UnAudit(string keyValue);
        #endregion

    }
}
